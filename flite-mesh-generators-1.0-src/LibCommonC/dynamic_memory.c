/*
 *  Copyright (C) 2017 College of Engineering, Swansea University
 *
 *  This file is part of the SwanSim FLITE suite of tools.
 *
 *  SwanSim FLITE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwanSim FLITE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this SwanSim FLITE product. 
 *  If not, see <http://www.gnu.org/licenses/>.
*/


/***************************************************************************/
/*        Module Name : dynamem                                            */
/*        Description : Dynamic memory allocation routines                 */
/***************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <fc.h>

/*                           Function allc                 */
/* This function allcs memory for 'mem' of size 'size'     */
/* In case of failure it outputs a message 'message'       */

void FC_GLOBAL(allc,ALLC)(int **mem , int *size, char message[200])
{
  long  rsize = ((long)(*size));


  if ( rsize < 0)
    {
      printf (" allc - STORAGE ALLOCATION FAILED: %s \n", message);
      printf (" Number of words requested: %d\n", (*size));
      printf (" Length is <= 0\n");
      exit (1);
    }
  *mem = (int *) calloc ( ( rsize + 1L ) * sizeof (int),1);
  if (*mem == 0)
    {
      printf (" allc - STORAGE ALLOCATION FAILED: %s \n", message);
      printf (" Number of words requested: %ld\n", rsize);
      exit (1);
    }
}

/*******************************************************************************/
/*                    Function deallc               */
/*            This function frees memory for 'mem'      */
void FC_GLOBAL(deallc,DEALLC)(int **mem, char message[200])
{
  free (*mem); 
}
/*******************************************************************************/

/*                           Function reallc               */
/* This function reallcs memory for 'mem' of size 'size'   */
/* In case of failure it outputs a message 'message'       */

void FC_GLOBAL(reallc,REALLC)(int **mem, int *size,  char message[200])
{
  long  rsize = ((long)(*size));


  if ( rsize < 0)
    {
      printf (" reallc - STORAGE ALLOCATION FAILED:%s \n", message);
      printf (" Number of words requested: %d\n", (*size));
      printf (" Length is <= 0\n");
      exit (1);
    }


  *mem = (int *) realloc (*mem, (rsize+1L) * sizeof (int));

  if (*mem == 0)
    {
      printf ("reallc - STORAGE ALLOCATION FAILED: %s \n", message);
      printf (" Number of words requested: %ld\n", rsize);
      exit (1);
    }  
}
/*******************************************************************************/
