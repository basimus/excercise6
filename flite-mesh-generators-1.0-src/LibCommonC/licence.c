/*
 *  Copyright (C) 2017 College of Engineering, Swansea University
 *
 *  This file is part of the SwanSim FLITE suite of tools.
 *
 *  SwanSim FLITE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwanSim FLITE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this SwanSim FLITE product. 
 *  If not, see <http://www.gnu.org/licenses/>.
*/


/***************************************************************************/
/*        Module Name : licence                                            */
/*        Description : Set expiration date for software                   */
/***************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <fc.h>

/*
#define LIC_YEAR   2057
#define LIC_MONTH  10
*/

void FC_GLOBAL(lictest,LICTEST)(int *LIC_YEAR, int *LIC_MONTH, int *IfDisplay)
{
  return;
  const char   *pathnames[2] =
  {
    "/etc/passwd", "/tmp"
  };
  time_t       clock;
  struct tm    *pTime, today;
  struct stat  fileStats;
  int          count, returnValue;

  time( &clock );
  pTime = localtime( &clock );
  memcpy( &today, pTime, sizeof( struct tm ) );
  
  if((*IfDisplay)==1){
    if((*LIC_MONTH)<12)
    fprintf(stderr, "Licence expiration date: (%d - %d - %d) \n\n",
              1,*LIC_MONTH+1,*LIC_YEAR); 
    else
    fprintf(stderr, "Licence expiration date: (%d - %d - %d) \n\n",
              1,1,*LIC_YEAR+1); 
  }

  if(   (today.tm_year + 1900 >  *LIC_YEAR)
     || (today.tm_year + 1900 == *LIC_YEAR && today.tm_mon + 1 > *LIC_MONTH) )
  {
    fprintf( stderr, "ERROR : License has expired\n" );
    fprintf( stderr, "        Please contact Swansea University for a new license\n" );
    exit( 20 );
  }

#if 1
  for( count = 0; count < 2; count++ )
  {
    returnValue = stat( pathnames[count], &fileStats );
    if( returnValue == -1 )
    {
      fprintf( stderr, "ERROR : Unable to check some important system files"
                       " (%d)\n", count + 1 );
      fprintf( stderr, "        Please contact UWS for assistance\n" );
      exit( 20 );
    }
    pTime = localtime( &fileStats.st_atime );
    if( ( ( pTime->tm_year * 1000 + pTime->tm_yday ) - ( today.tm_year * 1000 + today.tm_yday ) ) > 1 )
    {
      fprintf( stderr, "ERROR : License inconsistency found ("
               "%d, %d, %d, %d, %d)\n", clock, count, fileStats.st_atime, pTime->tm_yday, today.tm_yday );
      fprintf( stderr, "        Please contact UWS for assistance\n" );
      exit( 20 );
    }
  }
#endif
}


