/*
 *  Copyright (C) 2017 College of Engineering, Swansea University
 *
 *  This file is part of the SwanSim FLITE suite of tools.
 *
 *  SwanSim FLITE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwanSim FLITE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this SwanSim FLITE product. 
 *  If not, see <http://www.gnu.org/licenses/>.
*/



#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#ifdef __APPLE__
#include "malloc/malloc.h"
#else
#include "malloc.h"
#endif
#include "math.h"
#include <fc.h>

void FC_GLOBAL(binarystlreador,BINARYSTLREADOR)(char jobname[], double *pPosit, int *nbb)
{
  unsigned long int nb;
  int i, j, N;
  unsigned int k;
  float x[13];
  char *finname, *foutname;
  char ch[80];
  FILE *fin;
  char *ptr;  
  double *p;

  N=strlen(jobname);
  
  finname =malloc((size_t)(N+4)*sizeof(char *));  
  strcpy( finname,  jobname);
  for (ptr = finname; (ptr < finname + N ) && (! isspace(*ptr)); ++ptr);
  ptr[0] = '\0';
  strcat( finname,  ".stl");

  fin=fopen(finname,"rb");
 
  fread(ch, sizeof(char), 80, fin);          
          
  fread(&nb,sizeof(unsigned long),1,fin);
  printf(" nb = %u  \n",nb);          
  
  i = *nbb;
  *nbb = (int) nb;
  
  if(*nbb>i){
     fclose(fin);
     return;
  }
          
  N = 0;
  p = pPosit;
  for (i=1; i<=nb; i++) {             
    for (j=1; j<=12; j++) fread(&x[j], sizeof(float), 1, fin);
    fread(&k,2,1,fin);

    for (j=4; j<=12; j++, p++) p[0] = x[j];
  }

  fclose(fin);

}

void FC_GLOBAL(binaryugridreador,BINARYUGRIDREADOR)(char jobname[], int *JobNameLength, double *pPosit, int *ip, int *istep)
{
  unsigned long int nb;
  int i, j, N;
  unsigned int k;
  char *finname;
  char ch[80];
  FILE *fin;
  char *ptr;  

  N = *JobNameLength;
  
  finname =malloc((size_t)(N+4)*sizeof(char *));  
  strcpy( finname,  jobname);
  for (ptr = finname; (ptr < finname + N ) && (! isspace(*ptr)); ++ptr);
  ptr[0] = '\0';
  strcat( finname,  ".ugrid");

  fin=fopen(finname,"rb");
  
  for (j=0; j<7; j++) fread(&ip[j], sizeof(int), 1, fin);
  
  if(*istep==1){
    fclose(fin);
    return;
  }
  
  nb = 4*ip[1] + 5*ip[2] + 4*ip[3] + 5*ip[4] + 6*ip[5] + 8*ip[6];
          
  for (j=0; j<3*ip[0]; j++) fread(&pPosit[j], sizeof(double), 1, fin);
  for (j=0; j<nb; j++) fread(&ip[j], sizeof(int), 1, fin);

  fclose(fin);

}


