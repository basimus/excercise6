/*
 *  Copyright (C) 2017 College of Engineering, Swansea University
 *
 *  This file is part of the SwanSim FLITE suite of tools.
 *
 *  SwanSim FLITE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwanSim FLITE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this SwanSim FLITE product. 
 *  If not, see <http://www.gnu.org/licenses/>.
*/


#include "stdio.h"
#include "stdlib.h"

#include <fc.h>

/* Function Declarations */

int FC_GLOBAL(detectfortranbinary,DETECTFORTRANBINARY)(char filename[ ], int *slen, int *num) 
{
  FILE *fin;
  filename[*slen] = '\0';
  fin = fopen(filename,"rb");
  fread(num,sizeof(int),1,fin);
  *num = *num / 4;
  fclose(fin);
  return 1;
}
