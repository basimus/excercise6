/*
 *  Copyright (C) 2017 College of Engineering, Swansea University
 *
 *  This file is part of the SwanSim FLITE suite of tools.
 *
 *  SwanSim FLITE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwanSim FLITE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this SwanSim FLITE product.
 *  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef  __OCCT_FLITE_H
#define  __OCCT_FLITE_H

// IGEScpp.cpp : Defines the entry point for the console application.
//

#include <vector>
#include <string>
#include <algorithm>
#include <config.h>
//#include <AIS_Drawer.hxx>
//#include <AIS_InteractiveContext.hxx>
//#include <AIS_InteractiveObject.hxx>
//#include <AIS_ListOfInteractive.hxx>
//#include <AIS_ListIteratorOfListOfInteractive.hxx>
//#include <AIS_Shape.hxx>
//#include <AIS_Trihedron.hxx>
#include <Adaptor3d_Curve.hxx>
#include <Aspect_Background.hxx>
#include <Aspect_TypeOfLine.hxx>
//#include <Aspect_TypeOfText.hxx>
#include <Aspect_WidthOfLine.hxx>
//#include <Aspect_Window.hxx>
#include <Bnd_Box2d.hxx>
#include <BndLib_Add2dCurve.hxx>
#include <BRep_Builder.hxx>
#include <BRep_Tool.hxx>
#include <GCPnts_QuasiUniformDeflection.hxx>
#include <GCPnts_UniformDeflection.hxx>
#include <Geom_Curve.hxx>
#include <TopoDS_Edge.hxx>
#include <GeomAPI_IntSS.hxx>
#include <GeomAPI_ProjectPointOnCurve.hxx>
#include <ShapeUpgrade_RemoveInternalWires.hxx>
#include <GeomAPI_ProjectPointOnSurf.hxx>
#include <Quantity_Parameter.hxx>
#include <Extrema_ExtAlgo.hxx>
#include <TColStd_SequenceOfReal.hxx>
#include <Extrema_FuncExtPS.hxx>
#include <Extrema_SequenceOfPOnSurf.hxx>
#include <Extrema_GenExtPS.hxx>
#include <Extrema_ExtPS.hxx>
#include <BRepGProp.hxx>
#include <BRepLib.hxx>
#include <BRepBuilderAPI_MakeSolid.hxx>
#include <BRepCheck_Analyzer.hxx>
#include <BRepOffsetAPI_Sewing.hxx>
#include <BRepBuilderAPI.hxx>
#include <BRepAlgo.hxx>
#include <BRepTools.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRep_Curve3D.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <BRepAdaptor_Surface.hxx>
#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepExtrema_DistShapeShape.hxx>
#include <BRepMesh_IncrementalMesh.hxx>
#include <Standard_DefineHandle.hxx>
//#include <DsgPrs_LengthPresentation.hxx>
#include <GCE2d_MakeSegment.hxx>
#include <GCPnts_UniformAbscissa.hxx>
#include <GCPnts_TangentialDeflection.hxx>
#include <GCPnts_AbscissaPoint.hxx>
#include <Geom_CartesianPoint.hxx>
#include <Geom_Axis2Placement.hxx>
#include <Geom_CartesianPoint.hxx>
#include <Geom_Line.hxx>
#include <Geom_Surface.hxx>
#include <Geom2d_BezierCurve.hxx>
#include <Geom2d_BSplineCurve.hxx>
#include <Geom2d_Curve.hxx>
#include <Geom2d_TrimmedCurve.hxx>
#include <Geom2dAdaptor_Curve.hxx>
#include <GeomAdaptor.hxx>
#include <GeomAbs_CurveType.hxx>
#include <GeomAdaptor_Curve.hxx>
#include <GeomTools_Curve2dSet.hxx>
#include <gp_Ax2d.hxx>
#include <gp_Circ2d.hxx>
#include <gp_Dir2d.hxx>
#include <gp_Lin2d.hxx>
#include <gp_Pnt2d.hxx>
#include <gp_Vec.hxx>
#include <gp_Vec2d.hxx>
#include <GProp_GProps.hxx>
//#include <Graphic3d_WNTGraphicDevice.hxx>
#include <MMgt_TShared.hxx>
#include <OSD_Environment.hxx>
#include <Precision.hxx>
#include <Prs3d_IsoAspect.hxx>
#include <Prs3d_LineAspect.hxx>
//#include <Prs3d_Text.hxx>
//#include <PrsMgr_PresentationManager2d.hxx>
#include <Quantity_Factor.hxx>
#include <Quantity_Length.hxx>
#include <Quantity_NameOfColor.hxx>
#include <Quantity_PhysicalQuantity.hxx>
#include <Quantity_PlaneAngle.hxx>
#include <Quantity_TypeOfColor.hxx>
//#include <SelectBasics_BasicTool.hxx>
//#include <SelectBasics_ListOfBox2d.hxx>
//#include <SelectMgr_EntityOwner.hxx>
//#include <SelectMgr_SelectableObject.hxx>
//#include <SelectMgr_Selection.hxx>
//#include <SelectMgr_SelectionManager.hxx>
#include <ShapeSchema.hxx>
#include <Standard_Boolean.hxx>
#include <Standard_CString.hxx>
#include <Standard_ErrorHandler.hxx>
#include <Standard_Integer.hxx>
#include <Standard_IStream.hxx>
#include <Standard_Macro.hxx>
#include <Standard_NotImplemented.hxx>
#include <Standard_OStream.hxx>
#include <Standard_Real.hxx>
//#include <StdPrs_Curve.hxx>
//#include <StdPrs_Point.hxx>
//#include <StdPrs_PoleCurve.hxx>
#include <ShapeBuild_ReShape.hxx>
#include <ShapeFix_Face.hxx>
#include <ShapeFix_Wire.hxx>
#include <ShapeFix_Wireframe.hxx>
#include <ShapeFix_FixSmallFace.hxx>
#include <ShapeFix_Shape.hxx>
#include <ShapeUpgrade_ShapeDivideClosed.hxx>
//#include <StdSelect_SensitiveText2d.hxx>
//#include <StdSelect_TextProjector2d.hxx>
//#include <StdSelect_ViewerSelector2d.hxx>
#include <TCollection_AsciiString.hxx>
#include <TColgp_Array1OfPnt2d.hxx>
#include <TColgp_HArray1OfPnt2d.hxx>
#include <TCollection_AsciiString.hxx>
#include <TColStd_HSequenceOfTransient.hxx>
#include <TColStd_MapIteratorOfMapOfTransient.hxx>
#include <TColStd_MapOfTransient.hxx>
#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Compound.hxx>
#include <TopoDS_ListIteratorOfListOfShape.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Solid.hxx>
#include <TopoDS_Vertex.hxx>
#include <TopExp.hxx>

#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <TopTools_HSequenceOfShape.hxx>
#include <TopTools_DataMapOfShapeInteger.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <UnitsAPI.hxx>
//#include <V3d_View.hxx>
//#include <V3d_Viewer.hxx>
//#include <WNT_WDriver.hxx>
//#include <WNT_Window.hxx>
#include <ShapeUpgrade.hxx>
#include <ShapeUpgrade_EdgeDivide.hxx>
#include <ShapeUpgrade_SplitCurve3d.hxx>

// specific STEP

#include <STEPControl_Controller.hxx>
#include <STEPControl_Reader.hxx>
#include <STEPControl_Writer.hxx>


// specific IGES
#include <Interface_InterfaceModel.hxx>
#include <Interface_Static.hxx>

#include <IGESControl_Reader.hxx>
#include <IGESControl_Controller.hxx>
#include <IGESControl_Writer.hxx>
#include <IGESCAFControl_Reader.hxx>
#include <IGESBasic.hxx>
#include <IGESBasic_Group.hxx>

#include <IGESToBRep_Actor.hxx>
#include <IGESToBRep_Reader.hxx>
#include <XSControl_WorkSession.hxx>

// specific CSFDB
#include <FSD_File.hxx>
#include <MgtBRep.hxx>
#include <MgtBRep_TriangleMode.hxx>
//#include <MgtBRep_PurgeMode.hxx>
#include <PTColStd_PersistentTransientMap.hxx>
#include <PTColStd_TransientPersistentMap.hxx>
#include <PTopoDS_HShape.hxx>
#include <Storage_Data.hxx>
#include <Storage_Error.hxx>
#include <Storage_HSeqOfRoot.hxx>
#include <Storage_Root.hxx>

#include <STEPControl_StepModelType.hxx>

//#include <TransferBRep_Analyzer.hxx>

// specific STL VRML
#include "StlAPI_Writer.hxx"
#include "VrmlAPI_Writer.hxx"


//Meshing specifics
#include "BRepMesh.hxx"
#include "Poly_Array1OfTriangle.hxx"
#include "Poly_Triangulation.hxx"
#include "Poly_Triangle.hxx"
#include "Poly.hxx"
#include "GeomLProp_SLProps.hxx"
#include <ShapeAnalysis_Surface.hxx>

class OCCT_FLITE
{
  //This class contains functions to communicate between OCCT and FLITE
public:
  struct CurveInfo
  {
    TopoDS_Edge        edge;
    BRepAdaptor_Curve  curve;
    Handle_Geom_Curve  curveHandle;
    Standard_Real      minT, maxT;
  };

  struct FaceInfo
  {
    Standard_Real minU, maxU, minV, maxV;
    Standard_Real uScale, vScale;
    TopoDS_Face  face;
    BRepAdaptor_Surface  surface;
    Handle_Geom_Surface  surfaceHandle;
    ShapeAnalysis_Surface  *pSurfaceAnalyser;
  };

#ifdef JJ
  IGESControl_Reader IGESreader;
  STEPControl_Reader STEPreader;
#endif
  Standard_Integer nshapes;
  Standard_Integer ncurves;
  Standard_Integer nsurfaces;
  Standard_Real precision;
  TopTools_IndexedMapOfShape edgemap, facemap, edgeNameMap, faceNameMap;

  std::vector<std::string> edgeNames, faceNames;
  //Standard_Real * pVertices1,*pVertices2,*pVertices3,*pVertices4,*pVertices5,*pVertices6,*pVertices7;

  TopoDS_Shape shape;
  TopTools_IndexedMapOfShape fmap, emap, vmap, somap, shmap, wmap;

  std::vector<CurveInfo>  _curveInfo;
  std::vector<FaceInfo>  _faceInfo;

  struct visMesh_ {

    //Simple mesh structure for visualisation

    //Number of elements and number of nodes
    int numTri, numNodes;

    //Columns giving node connectivity, node indexes start at 0 end at numTri-1
    std::vector<Standard_Integer> N1, N2, N3;

    //Columns of node coordinates
    std::vector<Standard_Real> x, y, z;

    //Columns of normal direction (unit vectors) at each node
  //	std::vector<Standard_Real> xnorm, ynorm, znorm; 

    //Example use
    /*

      To get the first elements x ordinates...
      x1 = visMesh.x.at( visMesh.N1.at(0) );
      x2 = visMesh.x.at( visMesh.N2.at(0) );
      x3 = visMesh.x.at( visMesh.N3.at(0) );


    */

  };



  //public:

  Standard_Real ** buildVisCurve( Standard_Integer curveNum, Standard_Integer & numVertices,
    Standard_Real deviation );

  visMesh_ buildVisMesh( Standard_Real deviation, Standard_Integer surfNum );

  void buildLists();
  void CacheCurveObjects();
  void ComputeFaceScaling();
  void addShapeToLists( TopoDS_Shape shape );

  void OCCT_LoadAllIGES( Standard_CString filename );

  void OCCT_WriteBRep( Standard_CString filename );

  void OCCT_RunHeal( void );

  void OCCT_LoadSTEP( Standard_CString filename, Standard_Real &PreOut );

  void OCCT_LoadIGES( Standard_CString filename, Standard_Real &PreOut );

  void OCCT_LoadBRep( Standard_CString filename );

  void healGeometry( double tolerance, bool fixdegenerated,
    bool fixsmalledges, bool fixspotstripfaces,
    bool sewfaces, bool makesolids = false,
    bool connect = false );
  void FixShape( TopoDS_Shape shape );

  void OCCT_GetNumCurves( Standard_Integer &ncurves )
  {
    ncurves = emap.Extent();
  };

  void OCCT_GetNumSurfaces( Standard_Integer &nsurfaces )
  {
    nsurfaces = fmap.Extent();
  };

  void OCCT_GetLineTBox( Standard_Integer curveNum, Standard_Real &tMin, Standard_Real &tMax );

  void OCCT_GetLineXYZFromT( Standard_Integer CurveNum, Standard_Real u, Standard_Real* XYZ );

  void OCCT_GetLinePointDeriv( Standard_Integer CurveNum, Standard_Real u, Standard_Real* Ru, Standard_Real *Ruu );

  void OCCT_SurfaceUVBox( Standard_Integer surfNum, Standard_Real &uMin, Standard_Real &vMin,
    Standard_Real &uMax, Standard_Real &vMax );

  void OCCT_GetUVPointInfoAll
  ( Standard_Integer surfNum, Standard_Real u, Standard_Real v,
    Standard_Real* R, Standard_Real* Ru, Standard_Real* Rv,
    Standard_Real* Ruv, Standard_Real* Ruu, Standard_Real* Rvv );

  void OCCT_GetXYZFromUV1
  ( Standard_Integer surfNum, Standard_Real u, Standard_Real v,
    Standard_Real* R );

  void OCCT_GetSurfaceNumCurves( Standard_Integer surfNum, Standard_Integer &numCurves );

  void OCCT_GetUVFromXYZ( Standard_Integer surfNum, Standard_Real tol, Standard_Integer useUV, Standard_Real& u, Standard_Real& v, Standard_Real* R );

  void OCCT_GetUFromXYZ( Standard_Integer curvNum, Standard_Real tol, Standard_Real& u, Standard_Real* R );

  std::string  OCCT_GetSurfName( Standard_Integer surfNum );

  std::string  OCCT_GetCurvName( Standard_Integer curvNum );

  Standard_Integer* OCCT_GetSurfaceCurves( Standard_Integer surfNum, Standard_Integer &numCurves );

  Standard_Real**  OCCT_CalculateCurveFacets( Standard_Integer curveNum, Standard_Integer &numVertices,
    Standard_Real maxEdgeLength );
};

#endif  //__OCCT_FLITE_H

