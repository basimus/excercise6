/*
 *  Copyright (C) 2017 College of Engineering, Swansea University
 *
 *  This file is part of the SwanSim FLITE suite of tools.
 *
 *  SwanSim FLITE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwanSim FLITE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this SwanSim FLITE product. 
 *  If not, see <http://www.gnu.org/licenses/>.
*/

#include "occt_flite.h"
#include "occt_inc.h"
#include <iostream>
#include <fstream>
#include <string>

#include <fc.h>
#include <Bnd_Box.hxx>
#include <BRepBndLib.hxx>
#include <ShapeAnalysis.hxx>

#ifdef WIN32
#define _CRT_SECURE_NO_WARNINGS 1
#endif

#define SCALE_UV
#define SCALE_FACTOR 24.0

//#define JWJ_PRINT

void OCCT_FLITE::CacheCurveObjects()
{
  Standard_Real  tMin, tMax;

  _curveInfo.resize( emap.Extent() );
  for( int i = 0; i < _curveInfo.size(); i++ )
  {
    const int curveNum = i + 1;
    _curveInfo[i].edge = TopoDS::Edge(emap(curveNum));
    _curveInfo[i].curve = BRepAdaptor_Curve( _curveInfo[i].edge );
    _curveInfo[i].curveHandle = BRep_Tool::Curve(_curveInfo[i].edge,
                                                 _curveInfo[i].minT,_curveInfo[i].maxT);
  }
}

void OCCT_FLITE::ComputeFaceScaling()
{
  _faceInfo.resize( fmap.Extent() );
  for( int i = 0; i < _faceInfo.size(); i++ )
  {
    const int surfaceNum = i + 1;
    std::cerr << "Computing Face Scaling for " << surfaceNum << std::endl;
    _faceInfo[i].face = TopoDS::Face( fmap( surfaceNum ) );

    ShapeAnalysis::GetFaceUVBounds( _faceInfo[i].face, _faceInfo[ i ].minU, _faceInfo[ i ].maxU,
                                    _faceInfo[ i ].minV, _faceInfo[ i ].maxV );
    std::cerr << "  U(" << _faceInfo[ i ].minU << "-" << _faceInfo[ i ].maxU << ") - V("
              << _faceInfo[ i ].minV << "," << _faceInfo[ i ].maxV << ")" << std::endl;

    //
    // We want to scale to between 0 and SCALE_FACTOR
    //
    _faceInfo[ i ].uScale = ( _faceInfo[ i ].maxU - _faceInfo[ i ].minU ) / SCALE_FACTOR;
    _faceInfo[ i ].vScale = ( _faceInfo[ i ].maxV - _faceInfo[ i ].minV ) / SCALE_FACTOR;
    std::cerr << "  Scale - " << _faceInfo[ i ].uScale << ", " << _faceInfo[ i ].vScale << std::endl;
    _faceInfo[i].surface = BRepAdaptor_Surface(_faceInfo[i].face, true);
    _faceInfo[i].surfaceHandle = BRep_Tool::Surface( _faceInfo[i].face );
    _faceInfo[i].pSurfaceAnalyser = new ShapeAnalysis_Surface( _faceInfo[i].surfaceHandle );
    _faceInfo[i].pSurfaceAnalyser->SetDomain( _faceInfo[i].minU, _faceInfo[i].maxU,
                                              _faceInfo[i].minV, _faceInfo[i].maxV );
  }
}

Standard_Real ** OCCT_FLITE::buildVisCurve(Standard_Integer curveNum, Standard_Integer & numVertices,
            Standard_Real deviation)
{
  
  Standard_Real ** pVertices;

  //Re-cast the edge to an adaptor curve
  TopoDS_Edge anEdge = TopoDS::Edge(emap(curveNum));

  Standard_Real first_p;
  Standard_Real last_p;
  Handle(Geom_Curve) curve = BRep_Tool::Curve (anEdge, first_p, last_p);
  GeomAdaptor_Curve curve_adaptor (curve);

  GCPnts_UniformDeflection discretizer;
  discretizer.Initialize (curve_adaptor,deviation);
  
  //Now extract the information
  numVertices = discretizer.NbPoints();

  //Allocate
  pVertices = (Standard_Real **)calloc(numVertices,sizeof(Standard_Real *));
  int i;

  for  (i=0;i<numVertices;i++)
  {
    pVertices[i] = (Standard_Real*)calloc(3,sizeof(Standard_Real));
  };

  
  gp_Pnt P;

  for (i=0;i<numVertices;i++)
  {
    curve_adaptor.D0(discretizer.Parameter(i+1) , P);

    pVertices[i][0] = P.X();
    pVertices[i][1] = P.Y();
    pVertices[i][2] = P.Z();

  };
  
  return pVertices;

}


OCCT_FLITE::visMesh_ OCCT_FLITE::buildVisMesh(Standard_Real deviation, Standard_Integer surfNum)
  {

    //Returns structure visMesh containing triangulation of surface surfNum
    //The structure is defined and explained in occt_flite.h
    
    //deviation is the maximum distance of an edge from the curve

    //Copy shape so we don't mess it up
    //BRepMesh adds the triangulation to the shape
    TopoDS_Face anFace = TopoDS::Face(fmap(surfNum));

    TopoDS_Shape shapeLoc = anFace;
    
    BRepMesh_IncrementalMesh(shapeLoc,deviation);

    //Now need to extract the triangles from the surfaces
    TopExp_Explorer ex;
    visMesh_ visMesh;
    int startI;
    startI = 0; //Integer to keep track of location in triangulation matrix

    for (ex.Init(shapeLoc,TopAbs_FACE);ex.More();ex.Next())
    {

      TopoDS_Face F = TopoDS::Face(ex.Current());
      
      TopLoc_Location loc;

      Handle(Poly_Triangulation) tri = BRep_Tool::Triangulation(F,loc);
      //Poly::ComputeNormals(tri);



      if (!tri.IsNull())
      {
        
        //Transfer connectivity
        for (int i=1; i<=tri->NbTriangles();i++)
        {

          Poly_Triangle triangle = (tri->Triangles())(i);
          Standard_Integer N1, N2, N3;

          triangle.Get(N1,N2,N3);

          visMesh.N1.push_back(N1-1);
          visMesh.N2.push_back(N2-1);
          visMesh.N3.push_back(N3-1);
          
          /*
          //Handle(Geom_Surface) S = BRep_Tool::Surface(F);


           //Get the UV coordinates to calculate normal
                                        GeomLProp_SLProps props(S,
            ( tri->UVNodes()(N1).X() + 
              tri->UVNodes()(N2).X() +
              tri->UVNodes()(N3).X())/3.0,
             ( tri->UVNodes()(N1).Y() +
                                                  tri->UVNodes()(N2).Y() +
                                                  tri->UVNodes()(N3).Y())/3.0,
              1,0.01);

          visMesh.xnorm.push_back(props.Normal().X());
                                        visMesh.ynorm.push_back(props.Normal().Y());
                                        visMesh.znorm.push_back(props.Normal().Z());
          */


        }


        //Transfer nodes and normals
        for (int i=1;i<=tri->NbNodes();i++)
        {
          
          visMesh.x.push_back(  tri->Nodes()(i).Transformed(loc).X()   );
          visMesh.y.push_back(  tri->Nodes()(i).Transformed(loc).Y()   );
          visMesh.z.push_back(  tri->Nodes()(i).Transformed(loc).Z()   );

          
          
          

        }

      }
      

    }
    
    visMesh.numTri = (int)visMesh.N1.size();
    visMesh.numNodes = (int)visMesh.x.size();
    return visMesh;

}


void OCCT_FLITE::buildLists()
{
  somap.Clear();
  shmap.Clear();
  fmap.Clear();
  wmap.Clear();
  emap.Clear();
  vmap.Clear();
  addShapeToLists(shape);
  edgemap = emap;
  facemap = fmap;



}

void OCCT_FLITE::addShapeToLists(TopoDS_Shape _shape)
{
  // Solids
  TopExp_Explorer exp0, exp1, exp2, exp3, exp4, exp5;
  for(exp0.Init(_shape, TopAbs_SOLID); exp0.More(); exp0.Next()){
    TopoDS_Solid solid = TopoDS::Solid(exp0.Current());
    if(somap.FindIndex(solid) < 1){
      somap.Add(solid);

      for(exp1.Init(solid, TopAbs_SHELL); exp1.More(); exp1.Next()){
        TopoDS_Shell shell = TopoDS::Shell(exp1.Current());
        if(shmap.FindIndex(shell) < 1){
          shmap.Add(shell);

          for(exp2.Init(shell, TopAbs_FACE); exp2.More(); exp2.Next()){
            TopoDS_Face face = TopoDS::Face(exp2.Current());
            if(fmap.FindIndex(face) < 1){
              fmap.Add(face);

              for(exp3.Init(exp2.Current(), TopAbs_WIRE); exp3.More(); exp3.Next()){
                TopoDS_Wire wire = TopoDS::Wire(exp3.Current());
                if(wmap.FindIndex(wire) < 1){
                  wmap.Add(wire);

                  for(exp4.Init(exp3.Current(), TopAbs_EDGE); exp4.More(); exp4.Next()){
                    TopoDS_Edge edge = TopoDS::Edge(exp4.Current());
                    if(emap.FindIndex(edge) < 1){
                      emap.Add(edge);

                      for(exp5.Init(exp4.Current(), TopAbs_VERTEX); exp5.More(); exp5.Next()){
                        TopoDS_Vertex vertex = TopoDS::Vertex(exp5.Current());
                        if(vmap.FindIndex(vertex) < 1)
                          vmap.Add(vertex);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  // Free Shells
  for (exp1.Init(_shape, TopAbs_SHELL, TopAbs_SOLID); exp1.More(); exp1.Next()){
//  for(exp1.Init(exp0.Current(), TopAbs_SHELL, TopAbs_SOLID); exp1.More(); exp1.Next()){
    TopoDS_Shape shell = exp1.Current();
    if(shmap.FindIndex(shell) < 1){
      shmap.Add(shell);

      for(exp2.Init(shell, TopAbs_FACE); exp2.More(); exp2.Next()){
        TopoDS_Face face = TopoDS::Face(exp2.Current());
        if(fmap.FindIndex(face) < 1){
          fmap.Add(face);

          for(exp3.Init(exp2.Current(), TopAbs_WIRE); exp3.More(); exp3.Next()){
            TopoDS_Wire wire = TopoDS::Wire(exp3.Current());
            if(wmap.FindIndex(wire) < 1){
              wmap.Add(wire);

              for(exp4.Init(exp3.Current(), TopAbs_EDGE); exp4.More(); exp4.Next()){
                TopoDS_Edge edge = TopoDS::Edge(exp4.Current());
                if(emap.FindIndex(edge) < 1){
                  emap.Add(edge);

                  for(exp5.Init(exp4.Current(), TopAbs_VERTEX); exp5.More(); exp5.Next()){
                    TopoDS_Vertex vertex = TopoDS::Vertex(exp5.Current());
                    if(vmap.FindIndex(vertex) < 1)
                      vmap.Add(vertex);
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  // Free Faces
  for(exp2.Init(_shape, TopAbs_FACE, TopAbs_SHELL); exp2.More(); exp2.Next()){
    TopoDS_Face face = TopoDS::Face(exp2.Current());
    if(fmap.FindIndex(face) < 1){
      fmap.Add(face);

      for(exp3.Init(exp2.Current(), TopAbs_WIRE); exp3.More(); exp3.Next()){
        TopoDS_Wire wire = TopoDS::Wire(exp3.Current());
        if(wmap.FindIndex(wire) < 1){
          wmap.Add(wire);

          for(exp4.Init(exp3.Current(), TopAbs_EDGE); exp4.More(); exp4.Next()){
            TopoDS_Edge edge = TopoDS::Edge(exp4.Current());
            if(emap.FindIndex(edge) < 1){
              emap.Add(edge);

              for(exp5.Init(exp4.Current(), TopAbs_VERTEX); exp5.More(); exp5.Next()){
                TopoDS_Vertex vertex = TopoDS::Vertex(exp5.Current());
                if(vmap.FindIndex(vertex) < 1)
                  vmap.Add(vertex);
              }
            }
          }
        }
      }
    }
  }

  // Free Wires
  for(exp3.Init(_shape, TopAbs_WIRE, TopAbs_FACE); exp3.More(); exp3.Next()){
    TopoDS_Wire wire = TopoDS::Wire(exp3.Current());
    if(wmap.FindIndex(wire) < 1){
      wmap.Add(wire);

      for(exp4.Init(exp3.Current(), TopAbs_EDGE); exp4.More(); exp4.Next()){
        TopoDS_Edge edge = TopoDS::Edge(exp4.Current());
        if(emap.FindIndex(edge) < 1){
          emap.Add(edge);

          for(exp5.Init(exp4.Current(), TopAbs_VERTEX); exp5.More(); exp5.Next()){
            TopoDS_Vertex vertex = TopoDS::Vertex(exp5.Current());
            if(vmap.FindIndex(vertex) < 1)
              vmap.Add(vertex);
          }
        }
      }
    }
  }

  // Free Edges
  for(exp4.Init(_shape, TopAbs_EDGE, TopAbs_WIRE); exp4.More(); exp4.Next()){
    TopoDS_Edge edge = TopoDS::Edge(exp4.Current());
    if(emap.FindIndex(edge) < 1){
      emap.Add(edge);

      for(exp5.Init(exp4.Current(), TopAbs_VERTEX); exp5.More(); exp5.Next()){
        TopoDS_Vertex vertex = TopoDS::Vertex(exp5.Current());
        if(vmap.FindIndex(vertex) < 1)
          vmap.Add(vertex);
      }
    }
  }

  // Free Vertices
  for(exp5.Init(_shape, TopAbs_VERTEX, TopAbs_EDGE); exp5.More(); exp5.Next()){
    TopoDS_Vertex vertex = TopoDS::Vertex(exp5.Current());
    if(vmap.FindIndex(vertex) < 1)
      vmap.Add(vertex);
  }

}


void OCCT_FLITE :: OCCT_LoadAllIGES(Standard_CString filename)
{
  //This function loads iges data with entity names
  
  
  //In this case the compound shape is built manually
  
   
    BRep_Builder B;
    TopoDS_Compound C;
    B.MakeCompound(C);

               
  
  
  //Reads all curves as 3D
  IGESControl_Controller::Init();
  Interface_Static::SetIVal("read.surfacecurve.mode",-3);
  //Interface_Static::SetCVal ("xstep.cascade.unit", "M");
  
  
  IGESControl_Reader aReader;
  IFSelect_ReturnStatus stat=aReader.ReadFile(filename);
  
  aReader.NbRootsForTransfer();
    aReader.TransferRoots();
    shape = aReader.OneShape();
}
  
  





void OCCT_FLITE :: OCCT_RunHeal()
{
  BRepTools::Clean(shape);
  healGeometry(precision,
               true,  //degenerated t-f (true)
               true,  //small edges <t-f (true)  fails when false but creates valid dat file
               true,  //spot faces  (true)
               true,  // sew surfaces (true)
               true,  // make solid 
               true);
  BRepTools::Clean(shape);
}


void OCCT_FLITE :: OCCT_LoadSTEP(Standard_CString filename, Standard_Real &PreOut)
{
  STEPControl_Reader STEPreader;

  //This function reads the geometry file and translates to OCCT
//  cout << "About to read STEP file \n";
  //reads file
  Interface_Static::SetIVal("read.surfacecurve.mode",-3);
#ifdef STEP_USE_MM
  std::cout << "STEP Reader using unit: mm" << std::endl;
  Interface_Static::SetCVal( "xstep.cascade.unit", "MM" );
#else
  std::cout << "STEP Reader using unit: m" << std::endl;
  Interface_Static::SetCVal( "xstep.cascade.unit", "M" );
#endif

  IFSelect_ReturnStatus stat = STEPreader.ReadFile(filename);
//  cout << "File read \n";
  //check file
  IFSelect_PrintCount mode = IFSelect_ItemsByEntity;
  STEPreader.PrintCheckLoad(Standard_False,mode);
  //precision = Interface_Static::RVal("read.precision.val");
  precision = PreOut;
  //precision = Precision::Confusion()*0.0000001f;
  //Translate to OCCT
  STEPreader.TransferRoots();

  //Store these in a single compound shape
  shape = STEPreader.OneShape();
  //BRepTools::Clean(shape);

   OCCT_RunHeal();

  /*healGeometry(precision,
               false,
               false,
               false,
               false,
               false,
               false);*/
  //OCCT_FLITE::FixShape(shape);
  //BRepTools::Clean(shape);    
  buildLists();

  char  outFilename[1000];
  sprintf( outFilename, "%s.brep", filename );
  OCCT_WriteBRep( outFilename );
  CacheCurveObjects();
  ComputeFaceScaling();
};

void OCCT_FLITE :: OCCT_LoadIGES(Standard_CString filename, Standard_Real &PreOut)
{
  //This function reads the geometry file and translates to OCCT

  //IGES file needs to be saved as Manifold Solid B-Rep Objects (type 186) 
  //for this to work correctly
  
  
  OCCT_FLITE::OCCT_LoadAllIGES(filename);

  precision = PreOut;
  
  //BRepTools::Clean(shape);

  //ShapeUpgrade_RemoveInternalWires tool(shape);
  //tool.SetPrecision(precision);
  //tool.Perform();
  //shape = tool.GetResult();

  
  //Should the shape fixing happen during the read???c
  // healGeometry(precision,
 //               false,  //degenerated
 //              false,  //small edges
 //               false,  //spot faces
 //               false,  // sew surfaces
 //               false,  // make solid
 //               false);
  OCCT_FLITE::OCCT_RunHeal();
  //OCCT_FLITE::FixShape(shape);

        //ShapeUpgrade_ShapeDivideClosed tool(shape);
        //tool.SetEdgeMode(2);
        //tool.SetPrecision(precision);
        //tool.Perform();
        //shape = tool.Result();



  //BRepTools::Clean(shape);    
  buildLists();

  
  char  outFilename[1000];
  sprintf( outFilename, "%s.brep", filename );
  OCCT_WriteBRep( outFilename );
  CacheCurveObjects();
  ComputeFaceScaling();
};

void OCCT_FLITE::OCCT_LoadBRep( Standard_CString filename )
{
  //This function reads the geometry file and translates to OCCT

  //IGES file needs to be saved as Manifold Solid B-Rep Objects (type 186) 
  //for this to work correctly

  IGESControl_Controller::Init();

  std::ifstream  brepStream( filename );
  BRep_Builder  builder;
  BRepTools::Read( shape, brepStream, builder );

  buildLists();
  CacheCurveObjects();
  ComputeFaceScaling();
};

//This routine is from gmsh
void OCCT_FLITE::healGeometry(double tolerance, bool fixdegenerated,
                                 bool fixsmalledges, bool fixspotstripfaces,
                                 bool sewfaces, bool makesolids, bool connect)
{
  if(!fixdegenerated && !fixsmalledges && !fixspotstripfaces &&
     !sewfaces && !makesolids && !connect) return;

  

  buildLists();
  TopExp_Explorer exp0, exp1;
  int nrc = 0, nrcs = 0;
  int nrso = somap.Extent(), nrsh = shmap.Extent(), nrf = fmap.Extent();
  int nrw = wmap.Extent(), nre = emap.Extent(), nrv = vmap.Extent();
  for(exp0.Init(shape, TopAbs_COMPOUND); exp0.More(); exp0.Next()) nrc++;
  for(exp0.Init(shape, TopAbs_COMPSOLID); exp0.More(); exp0.Next()) nrcs++;

  double surfacecont = 0;
  for(exp0.Init(shape, TopAbs_FACE); exp0.More(); exp0.Next()){
    TopoDS_Face face = TopoDS::Face(exp0.Current());
    GProp_GProps system;
    BRepGProp::SurfaceProperties(face, system);
    surfacecont += system.Mass();
  }

  if(fixdegenerated){
  

    {
      Handle_ShapeBuild_ReShape rebuild = new ShapeBuild_ReShape;
      rebuild->Apply(shape);
      for(exp1.Init(shape, TopAbs_EDGE); exp1.More(); exp1.Next()){
        TopoDS_Edge edge = TopoDS::Edge(exp1.Current());
        if(BRep_Tool::Degenerated(edge))
          rebuild->Remove(edge, false);
      }
      shape = rebuild->Apply(shape);
    }
    buildLists();

    {
      Handle(ShapeFix_Face) sff;
      Handle_ShapeBuild_ReShape rebuild = new ShapeBuild_ReShape;
      rebuild->Apply(shape);

      for(exp0.Init(shape, TopAbs_FACE); exp0.More(); exp0.Next()){
        TopoDS_Face face = TopoDS::Face(exp0.Current());

        sff = new ShapeFix_Face(face);
        sff->FixAddNaturalBoundMode() = Standard_True;
        sff->FixSmallAreaWireMode() = Standard_True;
        sff->Perform();

        if(sff->Status(ShapeExtend_DONE1) ||
           sff->Status(ShapeExtend_DONE2) ||
           sff->Status(ShapeExtend_DONE3) ||
           sff->Status(ShapeExtend_DONE4) ||
           sff->Status(ShapeExtend_DONE5))
          {
           
            //if(sff->Status(ShapeExtend_DONE1))
             
            //else if(sff->Status(ShapeExtend_DONE2))
             
            //else if(sff->Status(ShapeExtend_DONE3))
             
            //else if(sff->Status(ShapeExtend_DONE4))
             
            //else if(sff->Status(ShapeExtend_DONE5))
              
            TopoDS_Face newface = sff->Face();

            rebuild->Replace(face, newface, Standard_False);
          }
      }
      shape = rebuild->Apply(shape);
    }

    {
      Handle_ShapeBuild_ReShape rebuild = new ShapeBuild_ReShape;
      rebuild->Apply(shape);
      for(exp1.Init(shape, TopAbs_EDGE); exp1.More(); exp1.Next()){
        TopoDS_Edge edge = TopoDS::Edge(exp1.Current());
        if(BRep_Tool::Degenerated(edge))
          rebuild->Remove(edge, false);
      }
      shape = rebuild->Apply(shape);
    }
  }

  if(fixsmalledges){
    

    Handle(ShapeFix_Wire) sfw;
    Handle_ShapeBuild_ReShape rebuild = new ShapeBuild_ReShape;
    rebuild->Apply(shape);

    for(exp0.Init(shape, TopAbs_FACE); exp0.More(); exp0.Next()){
      TopoDS_Face face = TopoDS::Face(exp0.Current());

      for(exp1.Init(face, TopAbs_WIRE); exp1.More(); exp1.Next()){
        TopoDS_Wire oldwire = TopoDS::Wire(exp1.Current());
        sfw = new ShapeFix_Wire(oldwire, face ,tolerance);
        sfw->ModifyTopologyMode() = Standard_True;

        sfw->ClosedWireMode() = Standard_True;

        bool replace = false;
        replace = sfw->FixReorder() || replace;
        replace = sfw->FixConnected() || replace;

        if(sfw->FixSmall(Standard_False, tolerance) &&
           ! (sfw->StatusSmall(ShapeExtend_FAIL1) ||
              sfw->StatusSmall(ShapeExtend_FAIL2) ||
              sfw->StatusSmall(ShapeExtend_FAIL3))){
          
          replace = true;
        }
        //else if(sfw->StatusSmall(ShapeExtend_FAIL1))
          
        //else if(sfw->StatusSmall(ShapeExtend_FAIL2))
          
        //else if(sfw->StatusSmall(ShapeExtend_FAIL3))
          

        replace = sfw->FixEdgeCurves() || replace;
        replace = sfw->FixDegenerated() || replace;
        replace = sfw->FixSelfIntersection() || replace;
        replace = sfw->FixLacking(Standard_True) || replace;
        if(replace){
          TopoDS_Wire newwire = sfw->Wire();
          rebuild->Replace(oldwire, newwire, Standard_False);
        }
      }
    }

    shape = rebuild->Apply(shape);

    {
      buildLists();
      Handle_ShapeBuild_ReShape rebuild = new ShapeBuild_ReShape;
      rebuild->Apply(shape);

      for(exp1.Init(shape, TopAbs_EDGE); exp1.More(); exp1.Next()){
        TopoDS_Edge edge = TopoDS::Edge(exp1.Current());
        if(vmap.FindIndex(TopExp::FirstVertex(edge)) ==
           vmap.FindIndex(TopExp::LastVertex(edge))){
          GProp_GProps system;
          BRepGProp::LinearProperties(edge, system);
          if(system.Mass() < tolerance){
           
            rebuild->Remove(edge, false);
          }
        }
      }
      shape = rebuild->Apply(shape);
    }

    {
      Handle_ShapeBuild_ReShape rebuild = new ShapeBuild_ReShape;
      rebuild->Apply(shape);
      for(exp1.Init(shape, TopAbs_EDGE); exp1.More(); exp1.Next()){
        TopoDS_Edge edge = TopoDS::Edge(exp1.Current());
        if(BRep_Tool::Degenerated(edge) )
          rebuild->Remove(edge, false);
      }
      shape = rebuild->Apply(shape);
    }

    Handle(ShapeFix_Wireframe) sfwf = new ShapeFix_Wireframe;
    sfwf->SetPrecision(tolerance);
    sfwf->Load(shape);
    sfwf->ModeDropSmallEdges() = Standard_True;

    if(sfwf->FixWireGaps()){
      
      //if(sfwf->StatusWireGaps(ShapeExtend_OK))
        
      //if(sfwf->StatusWireGaps(ShapeExtend_DONE1))
        
      //if(sfwf->StatusWireGaps(ShapeExtend_DONE2))
       
      //if(sfwf->StatusWireGaps(ShapeExtend_FAIL1))
        
      //if(sfwf->StatusWireGaps(ShapeExtend_FAIL2))
       
    }

    sfwf->SetPrecision(tolerance);

    if(sfwf->FixSmallEdges()){
      
      //if(sfwf->StatusSmallEdges(ShapeExtend_OK))
        
      //if(sfwf->StatusSmallEdges(ShapeExtend_DONE1))
       
      //if(sfwf->StatusSmallEdges(ShapeExtend_FAIL1))
       
    }

    shape = sfwf->Shape();
  }

  if(fixspotstripfaces){
    
    Handle(ShapeFix_FixSmallFace) sffsm = new ShapeFix_FixSmallFace();
    sffsm->Init(shape);
    sffsm->SetPrecision(tolerance);
    sffsm->Perform();

    shape = sffsm->FixShape();
  }

  if(sewfaces){
   

    BRepOffsetAPI_Sewing sewedObj(tolerance);

    for(exp0.Init(shape, TopAbs_FACE); exp0.More(); exp0.Next()){
      TopoDS_Face face = TopoDS::Face(exp0.Current());
      sewedObj.Add(face);
    }

    sewedObj.Perform();

    if(!sewedObj.SewedShape().IsNull())
      shape = sewedObj.SewedShape();
    
    int lim = (int)edgeNames.size();
    for (int ie=0;ie<lim;ie++)
    {
      if(!sewedObj.ModifiedSubShape(edgeNameMap(ie+1)).IsNull())
      {
        edgeNameMap.Add(  sewedObj.ModifiedSubShape(edgeNameMap(ie+1)));
        edgeNames.push_back(edgeNames.at(ie));
      }
    }
    
    lim = (int)faceNames.size();
    for (int ie=0;ie<lim;ie++)
    {
      if(!sewedObj.ModifiedSubShape(faceNameMap(ie+1)).IsNull())
      {
        faceNameMap.Add(  sewedObj.ModifiedSubShape(faceNameMap(ie+1)));
        faceNames.push_back(faceNames.at(ie));
      }
    }
  
     
  }

  {
    Handle_ShapeBuild_ReShape rebuild = new ShapeBuild_ReShape;
    rebuild->Apply(shape);
    for(exp1.Init(shape, TopAbs_EDGE); exp1.More(); exp1.Next()){
      TopoDS_Edge edge = TopoDS::Edge(exp1.Current());
      if(BRep_Tool::Degenerated(edge))
        rebuild->Remove(edge, false);
    }
    shape = rebuild->Apply(shape);
  }

  if(makesolids){
    

    BRepBuilderAPI_MakeSolid ms;
    int count = 0;
    for(exp0.Init(shape, TopAbs_SHELL); exp0.More(); exp0.Next()){
      count++;
      ms.Add(TopoDS::Shell(exp0.Current()));
    }

    if(!count){
      
    }
    else{
      BRepCheck_Analyzer ba(ms);
      if(ba.IsValid()){
        Handle(ShapeFix_Shape) sfs = new ShapeFix_Shape;
        sfs->Init(ms);
        sfs->SetPrecision(tolerance);
        sfs->SetMaxTolerance(tolerance);
        sfs->Perform();
        shape = sfs->Shape();

        for(exp0.Init(shape, TopAbs_SOLID); exp0.More(); exp0.Next()){
          TopoDS_Solid solid = TopoDS::Solid(exp0.Current());
          TopoDS_Solid newsolid = solid;
          BRepLib::OrientClosedSolid(newsolid);
          Handle_ShapeBuild_ReShape rebuild = new ShapeBuild_ReShape;
          //      rebuild->Apply(shape);
          rebuild->Replace(solid, newsolid, Standard_False);
          TopoDS_Shape newshape = rebuild->Apply(shape, TopAbs_COMPSOLID);//, 1);
          //      TopoDS_Shape newshape = rebuild->Apply(shape);
          shape = newshape;
        }
      }
      
        
    }
  }

  if(connect){

  //  OCC_Connect connect(1);
  //  for(TopExp_Explorer p(shape, TopAbs_SOLID); p.More(); p.Next())
  //    connect.Add(p.Current());
  //  connect.Connect();
  //  shape = connect;

  }

  double newsurfacecont = 0;
  for(exp0.Init(shape, TopAbs_FACE); exp0.More(); exp0.Next()){
    TopoDS_Face face = TopoDS::Face(exp0.Current());
    GProp_GProps system;
    BRepGProp::SurfaceProperties(face, system);
    newsurfacecont += system.Mass();
  }

  buildLists();
  int nnrc = 0, nnrcs = 0;
  int nnrso = somap.Extent(), nnrsh = shmap.Extent(), nnrf = fmap.Extent();
  int nnrw = wmap.Extent(), nnre = emap.Extent(), nnrv = vmap.Extent();
  for(exp0.Init(shape, TopAbs_COMPOUND); exp0.More(); exp0.Next()) nnrc++;
  for(exp0.Init(shape, TopAbs_COMPSOLID); exp0.More(); exp0.Next()) nnrcs++;

  
}


void OCCT_FLITE :: FixShape(TopoDS_Shape shape)
{

    //Split into two indexed maps, edgemap and facemap
                //edgemap(i) gives the ith edge
                TopTools_IndexedMapOfShape tempedgemap, tempfacemap,vmap;
                TopExp::MapShapes(shape,TopAbs_EDGE,tempedgemap);
                TopExp::MapShapes(shape,TopAbs_FACE,tempfacemap);

                //Fix geometry
                BRep_Builder B;
                TopoDS_Compound C;
                B.MakeCompound(C);

                for(int i = 1; i <= tempedgemap.Extent(); i++) B.Add(C, tempedgemap(i));
                for(int i = 1; i <= tempfacemap.Extent(); i++) B.Add(C, tempfacemap(i));

                shape = C;

      
           Handle(ShapeFix_Shape) sfsw = new ShapeFix_Shape;
          sfsw -> Init(shape);
    sfsw -> SetPrecision(precision);
          sfsw-> Perform();
          shape = sfsw->Shape();


//                ShapeUpgrade_ShapeDivideClosed tool(shape);
//          tool.SetEdgeMode(2);
//    tool.SetPrecision(precision);
//          tool.Perform();
//          shape = tool.Result();


    TopExp::MapShapes(shape,TopAbs_EDGE,tempedgemap);
                TopExp::MapShapes(shape,TopAbs_FACE,tempfacemap);

                Handle_ShapeBuild_ReShape rebuild = new ShapeBuild_ReShape;
                rebuild->Apply(shape);

                TopExp_Explorer exp0,exp1;

     //Check for degenerated edges
//                for (exp1.Init(shape,TopAbs_EDGE); exp1.More(); exp1.Next()){
//                        TopoDS_Edge edge = TopoDS::Edge(exp1.Current());
//                        if(BRep_Tool::Degenerated(edge)){
//                                rebuild->Remove(edge,false);
//                        }
//                }
//                shape = rebuild->Apply(shape);

                TopExp::MapShapes(shape,TopAbs_EDGE,tempedgemap);
                TopExp::MapShapes(shape,TopAbs_FACE,tempfacemap);

//                Handle(ShapeFix_Face) sff;
//                for (exp0.Init(shape,TopAbs_FACE);exp0.More();exp0.Next()){
//                        TopoDS_Face face = TopoDS::Face(exp0.Current());
//                        sff = new ShapeFix_Face(face);
//                        sff->FixAddNaturalBoundMode() = Standard_True;
//                        sff->FixSmallAreaWireMode() = Standard_True;
//                        sff->Perform();
//                       TopoDS_Face newface = sff->Face();
//                        rebuild->Replace(face,newface,Standard_False);
//                }
//                shape = rebuild->Apply(shape);

//                rebuild = new ShapeBuild_ReShape;
//                rebuild->Apply(shape);
                //Check for degenerated edges
//                for (exp1.Init(shape,TopAbs_EDGE);exp1.More();exp1.Next()){
//                        TopoDS_Edge edge = TopoDS::Edge(exp1.Current());
//                        if(BRep_Tool::Degenerated(edge)){
//                                rebuild->Remove(edge,false);
//                        }
//                }

//                shape = rebuild->Apply(shape);



     //Fix wires



//                Handle(ShapeFix_Wire) sfw;
//                rebuild = new ShapeBuild_ReShape;
//                rebuild->Apply(shape);

//                for(exp0.Init(shape, TopAbs_FACE);exp0.More();exp0.Next()){
//                        TopoDS_Face face = TopoDS::Face(exp0.Current());
                        //Standard_Real tolerance = BRep_Tool::Tolerance(face);
//                        for(exp1.Init(face, TopAbs_WIRE); exp1.More(); exp1.Next()){
//                                TopoDS_Wire oldwire = TopoDS::Wire(exp1.Current());
//                                sfw = new ShapeFix_Wire(oldwire,face,precision);
//                                sfw->ModifyTopologyMode() = Standard_True;
//                                sfw->ClosedWireMode() = Standard_True;
//
//                                sfw->FixSmall(Standard_False,precision);
//                                sfw->FixEdgeCurves();
//                                sfw->FixDegenerated();
//                                sfw->FixSelfIntersection();
//                                sfw->FixLacking(Standard_True);

//                                TopoDS_Wire newwire = sfw->Wire();
//                                rebuild->Replace(oldwire,newwire,Standard_False);


//                        }

//                }

//                shape = rebuild->Apply(shape);


//    TopExp::MapShapes(shape,TopAbs_VERTEX,vmap);

//                rebuild = new ShapeBuild_ReShape;
//                rebuild->Apply(shape);

//                for(exp1.Init(shape,TopAbs_EDGE);exp1.More();exp1.Next()){
//                        TopoDS_Edge edge = TopoDS::Edge(exp1.Current());
//                        //Standard_Real tolerance = BRepTool::Tolerance(edge);
//                        if(vmap.FindIndex(TopExp::FirstVertex(edge))==
//                           vmap.FindIndex(TopExp::LastVertex(edge))){
//                                rebuild->Remove(edge,false);
//                        }
//                }

//                shape = rebuild->Apply(shape);


  
//                rebuild = new ShapeBuild_ReShape;
//                rebuild->Apply(shape);
                //Check for degenerated edges
//                for (exp1.Init(shape,TopAbs_EDGE);exp1.More();exp1.Next()){
//                        TopoDS_Edge edge = TopoDS::Edge(exp1.Current());
//                        if(BRep_Tool::Degenerated(edge)){
//                                rebuild->Remove(edge,false);
//                        }
//                }

//                shape = rebuild->Apply(shape);




//                Handle(ShapeFix_Wireframe) sfwf = new ShapeFix_Wireframe;
//                sfwf->SetPrecision(precision);
//                sfwf->Load(shape);
//                sfwf->ModeDropSmallEdges() = Standard_True;
//                sfwf->FixWireGaps();
//                sfwf->SetPrecision(precision);
//                sfwf->FixSmallEdges();
//               shape = sfwf->Shape();



//                rebuild = new ShapeBuild_ReShape;

//                rebuild->Apply(shape);


    //Check and fix small faces
//    Handle(ShapeFix_FixSmallFace) sffsm = new ShapeFix_FixSmallFace();
//    sffsm->Init(shape);
//    sffsm->SetPrecision(precision);
//    sffsm->Perform();
//    shape = sffsm->FixShape();

    //Sew faces
//    BRepOffsetAPI_Sewing sewedObj(precision);
//    for(exp0.Init(shape, TopAbs_FACE);exp0.More();exp0.Next()){
//                        TopoDS_Face face = TopoDS::Face(exp0.Current());
//                        sewedObj.Add(face);
//
  //              }
//    sewedObj.Perform();
//    if(!sewedObj.SewedShape().IsNull()){
//      shape = sewedObj.SewedShape();
//    }

//    rebuild = new ShapeBuild_ReShape;
//        rebuild->Apply(shape);
        //Check for degenerated edges
//        for (exp1.Init(shape,TopAbs_EDGE);exp1.More();exp1.Next()){
//            TopoDS_Edge edge = TopoDS::Edge(exp1.Current());
//            if(BRep_Tool::Degenerated(edge)){
//                rebuild->Remove(edge,false);
//            }
//         }

//        shape = rebuild->Apply(shape);

  
  
  
//  BRepBuilderAPI_MakeSolid ms;
//  int count = 0;
//  for(exp0.Init(shape, TopAbs_SHELL); exp0.More(); exp0.Next()){
//    count++;
//    ms.Add(TopoDS::Shell(exp0.Current()));
//  }
   
//         if(!count){
         
 //        }else{
 //          BRepCheck_Analyzer ba(ms);
 //          if(ba.IsValid()){
 //           Handle(ShapeFix_Shape) sfs = new ShapeFix_Shape;
 //          sfs->Init(ms);
 //          sfs->SetPrecision(precision);
 //          sfs->SetMaxTolerance(precision);
 //          sfs->Perform();
 //          shape = sfs->Shape();
   
 //          for(exp0.Init(shape, TopAbs_SOLID); exp0.More(); exp0.Next()){
 //            TopoDS_Solid solid = TopoDS::Solid(exp0.Current());
 //            TopoDS_Solid newsolid = solid;
 //            BRepLib::OrientClosedSolid(newsolid);
 //            Handle_ShapeBuild_ReShape rebuild = new ShapeBuild_ReShape;
 //                rebuild->Apply(shape);
 //            rebuild->Replace(solid, newsolid, Standard_False);
 //            TopoDS_Shape newshape = rebuild->Apply(shape, TopAbs_COMPSOLID);//, 1);
             //      TopoDS_Shape newshape = rebuild->Apply(shape);
 //            shape = newshape;
 //          }
 //        }
        
 //      }
  

    
  
  //rebuild = new ShapeBuild_ReShape;
        //rebuild->Apply(shape);

        TopoDS_Shape newShape = rebuild->Apply(shape);

        TopExp::MapShapes(newShape,TopAbs_EDGE,edgemap);
        TopExp::MapShapes(newShape,TopAbs_FACE,facemap);



}

void OCCT_FLITE :: OCCT_GetLineTBox(Standard_Integer curveNum, Standard_Real &tMin, Standard_Real &tMax)
{
  //Function to return the maximum and minimum values of the parameter defining a curve
  const CurveInfo& curveInfo = _curveInfo[curveNum-1];

  tMin = curveInfo.curve.FirstParameter();
  tMax = curveInfo.curve.LastParameter();
}

void OCCT_FLITE :: OCCT_GetLineXYZFromT( Standard_Integer CurveNum, Standard_Real u, Standard_Real* XYZ)
{  
  //Returns point at parameter u along a curve
  const CurveInfo& curveInfo = _curveInfo[CurveNum-1];

  gp_Pnt P;

  //Get the coordinates
  curveInfo.curve.D0(u,P);
  XYZ[0] = P.X();
  XYZ[1] = P.Y();
  XYZ[2] = P.Z();
}


void OCCT_FLITE :: OCCT_GetLinePointDeriv( Standard_Integer CurveNum, Standard_Real u, Standard_Real* Ru, Standard_Real *Ruu)
{  
  //Returns derivatives at parameter u along a curve
  const CurveInfo& curveInfo = _curveInfo[CurveNum-1];

  gp_Pnt P;
  gp_Vec RuV, RuuV;

  //Get the first and second derivative
  curveInfo.curveHandle->D2(u,P,RuV,RuuV);
  
  Ru[0] = RuV.X();
  Ru[1] = RuV.Y();
  Ru[2] = RuV.Z();

  Ruu[0] = RuuV.X();
  Ruu[1] = RuuV.Y();
  Ruu[2] = RuuV.Z();
};

void OCCT_FLITE :: OCCT_SurfaceUVBox(Standard_Integer surfNum, Standard_Real &uMin, Standard_Real &vMin, 
                  Standard_Real &uMax, Standard_Real &vMax )
{
  // Function to return the maximum and minimum values of the parameter defining 
  // a surface

#ifdef SCALE_UV
  uMin = 0.0;
  vMin = 0.0;
  uMax = SCALE_FACTOR;
  vMax = SCALE_FACTOR;
#else

  TopoDS_Face anFace = TopoDS::Face(fmap(surfNum));
  
  BRepAdaptor_Surface S (anFace, true);
  uMin = S.FirstUParameter();
  uMax = S.LastUParameter();
  vMin = S.FirstVParameter();
  vMax = S.LastVParameter();
#endif

};

void OCCT_FLITE :: OCCT_GetUVPointInfoAll
  (Standard_Integer surfNum, Standard_Real u, Standard_Real v,
     Standard_Real* R, Standard_Real* Ru, Standard_Real* Rv,
     Standard_Real* Ruv, Standard_Real* Ruu, Standard_Real* Rvv)
{
  const FaceInfo& faceInfo = _faceInfo[surfNum-1];

  gp_Pnt RV;
  gp_Vec RuV, RvV, RuvV, RuuV, RvvV;

  // Function to return all info on a point at u,v of suface surfNum
#ifdef SCALE_UV
#ifdef JWJ_PRINT
  if( ( u < 0.0 - 1e-8 ) || ( u >SCALE_FACTOR + 1e-8 ) )
  {
    std::cerr << "OOPS: OCCT_GetUVPointInfoAll: Input U out of range " << u << std::endl;
  }
  if( ( v < 0.0 - 1e-8 ) || ( v > SCALE_FACTOR + 1e-8 ) )
  {
    std::cerr << "OOPS: OCCT_GetUVPointInfoAll: Input V out of range " << v << std::endl;
  }
#endif
  u = u * faceInfo.uScale + faceInfo.minU;
  v = v * faceInfo.vScale + faceInfo.minV;
#ifdef JWJ_PRINT
  if( ( u < faceInfo.minU - 1e-8 ) || ( u > faceInfo.maxU + 1e-8 ) )
  {
    std::cerr << "OOPS: OCCT_GetUVPointInfoAll: Unscaled input U out of range " << u << ", (" << faceInfo.minU << " - " << faceInfo.maxU << ")" << std::endl;
  }
  if( ( v < faceInfo.minV - 1e-8 ) || ( v > faceInfo.maxV + 1e-8 ) )
  {
    std::cerr << "OOPS: OCCT_GetUVPointInfoAll: Unscaled input V out of range  " << v << ", (" << faceInfo.minV << " - " << faceInfo.maxV << ")" << std::endl;
  }
#endif
#endif
  faceInfo.surface.D2(u,v,RV,RuV,RvV,RuuV,RvvV,RuvV);

  R[0] = RV.X();
  R[1] = RV.Y();
  R[2] = RV.Z();

#ifdef SCALE_UV
  Ru[ 0 ] = RuV.X() * faceInfo.uScale;
  Ru[ 1 ] = RuV.Y() * faceInfo.uScale;
  Ru[ 2 ] = RuV.Z() * faceInfo.uScale;

  Rv[ 0 ] = RvV.X() * faceInfo.vScale;
  Rv[ 1 ] = RvV.Y() * faceInfo.vScale;
  Rv[ 2 ] = RvV.Z() * faceInfo.vScale;

  Ruu[ 0 ] = RuuV.X() * ( faceInfo.uScale * faceInfo.uScale );
  Ruu[ 1 ] = RuuV.Y() * ( faceInfo.uScale * faceInfo.uScale );
  Ruu[ 2 ] = RuuV.Z() * ( faceInfo.uScale * faceInfo.uScale );

  Rvv[ 0 ] = RvvV.X() * ( faceInfo.vScale * faceInfo.vScale );
  Rvv[ 1 ] = RvvV.Y() * ( faceInfo.vScale * faceInfo.vScale );
  Rvv[ 2 ] = RvvV.Z() * ( faceInfo.vScale * faceInfo.vScale );

  Ruv[ 0 ] = RuvV.X() * ( faceInfo.uScale * faceInfo.vScale );
  Ruv[ 1 ] = RuvV.Y() * ( faceInfo.uScale * faceInfo.vScale );
  Ruv[ 2 ] = RuvV.Z() * ( faceInfo.uScale * faceInfo.vScale );
#else
  Ru[0] = RuV.X();
  Ru[1] = RuV.Y();
  Ru[2] = RuV.Z();
  
  Rv[0] = RvV.X();
  Rv[1] = RvV.Y();
  Rv[2] = RvV.Z();

  Ruu[0] = RuuV.X();
  Ruu[1] = RuuV.Y();
  Ruu[2] = RuuV.Z();

  Rvv[0] = RvvV.X();
  Rvv[1] = RvvV.Y();
  Rvv[2] = RvvV.Z();

  Ruv[0] = RuvV.X();
  Ruv[1] = RuvV.Y();
  Ruv[2] = RuvV.Z();
#endif
};

ostream& operator <<( ostream& str, const gp_Pnt& p )
{
  str << "(" << p.X() << "," << p.Y() << "," << p.Z() << ")";
  return( str );
}

void OCCT_FLITE :: OCCT_GetXYZFromUV1
  (Standard_Integer surfNum, Standard_Real u, Standard_Real v,
     Standard_Real* R)
{
  // Function to return all info on a point at u,v of suface surfNum
  const FaceInfo& faceInfo = _faceInfo[surfNum-1];
  gp_Pnt RV;
#ifdef SCALE_UV
#ifdef JWJ_PRINT
  if( ( u < 0.0 - 1e-8 ) || ( u > SCALE_FACTOR + 1e-8 ) )
  {
    std::cerr << "OOPS: OCCT_GetXYZFromUV1: Input U out of range " << u << std::endl;
  }
  if( ( v < 0.0 - 1e-8 ) || ( v >SCALE_FACTOR + 1e-8 ) )
  {
    std::cerr << "OOPS: OCCT_GetXYZFromUV1: Input U out of range " << v << std::endl;
  }
#endif
  u = u * faceInfo.uScale + faceInfo.minU;
  v = v * faceInfo.vScale + faceInfo.minV;
#ifdef JWJ_PRINT
  if( ( u < faceInfo.minU - 1e-8 ) || ( u > faceInfo.maxU + 1e-8 ) )
  {
    std::cerr << "OOPS: OCCT_GetXYZFromUV1: Unscaled input U out of range " << u << ",(" << faceInfo.minU << " - " << faceInfo.maxU << ")" << std::endl;
  }
  if( ( v < faceInfo.minV - 1e-8 ) || ( v > faceInfo.maxV + 1e-8 ) )
  {
    std::cerr << "OOPS: OCCT_GetXYZFromUV1: Unscaled input V out of range " << v << ",(" << faceInfo.minV << " - " << faceInfo.maxV << ")" << std::endl;
  }
#endif

#endif
  faceInfo.surface.D0(u,v,RV);

  R[0] = RV.X();
  R[1] = RV.Y();
  R[2] = RV.Z();
};

#include <gp_Pnt2d.hxx>

void OCCT_FLITE :: OCCT_GetUVFromXYZ
  (Standard_Integer surfNum, Standard_Real tol, Standard_Integer useUV, Standard_Real& u, Standard_Real& v,
    Standard_Real* R)
{
#ifdef JWJ_PRINT
  std::cout << "OCCT_GetUVFromXYZ( " << surfNum << "," << tol << ",UseUV(" << useUV << "), (" << u << ","
            << v << "), (" << R[ 0 ] << "," << R[ 1 ] << "," << R[ 2 ] << ")" << std::endl;
#endif
  //Function to return neares UV on suface to point XYZ

  const FaceInfo& faceInfo = _faceInfo[surfNum-1];
  ShapeAnalysis_Surface *pSurfaceAnalyser = _faceInfo[surfNum-1].pSurfaceAnalyser;
  gp_Pnt RV( R[0], R[1], R[2] );

#ifdef JWJ_PRINT
  Standard_Real uMin;
  Standard_Real uMax;
  Standard_Real vMin;
  Standard_Real vMax;
  BRepTools::UVBounds(anFace,uMin,uMax,vMin,vMax);
  std::cout << "  UV Bounds (" << uMin << "," << vMin << ") - (" << uMax << "," << vMax << ")" << std::endl;
#endif

#ifdef JWJ_PRINT
  Bnd_Box  box;
  BRepBndLib::Add( anFace, box );
  box.SetGap( 0.0 );
  std::cout << "  BBox " << box.CornerMin() << " - " << box.CornerMax() << std::endl;
#endif

  Standard_Real  tempU[3], tempV[3], tempDiff[3] = { 1e8 };
  Standard_Integer  minProj = -1;

  if( useUV ) {
    gp_Pnt2d  oldUV( u, v );
    gp_Pnt2d  projUV = pSurfaceAnalyser->NextValueOfUV( oldUV, RV, tol );
    tempU[0] = projUV.X();
    tempV[0] = projUV.Y();
#ifdef JWJ_PRINT
    faceInfo.surface.D0( tempU[0], tempV[0], RV );
    double  dx = RV.X() - R[ 0 ];
    double  dy = RV.Y() - R[ 1 ];
    double  dz = RV.Z() - R[ 2 ];
    double  d = sqrt( dx * dx + dy * dy + dz * dz );
    tempDiff[0] = d;
    std::cout << "  UV 1 " << tempU[0] << "," << tempV[0] << " - DIFF 1 " << d << std::endl;
#endif
    minProj = 0;
  }
  else
  {
    gp_Pnt2d  projUV = pSurfaceAnalyser->ValueOfUV( RV, tol );
    tempU[1] = projUV.X();
    tempV[1] = projUV.Y();
#ifdef JWJ_PRINT
    faceInfo.surface.D0( tempU[1], tempV[1], RV );
    double  dx = RV.X() - R[ 0 ];
    double  dy = RV.Y() - R[ 1 ];
    double  dz = RV.Z() - R[ 2 ];
    double  d = sqrt( dx * dx + dy * dy + dz * dz );
    tempDiff[1] = d;
    std::cout << "  UV 2 " << tempU[1] << "," << tempV[1] << " - DIFF 2 " << d << std::endl;
#endif
    if( ( minProj == -1 ) || ( tempDiff[1] < tempDiff[0] * 0.99 ) )
    {
      minProj = 1;
    }
  }
#ifdef JWJ_PRINT
  {
    Extrema_ExtAlgo Algo = Extrema_ExtAlgo_Grad;
    GeomAPI_ProjectPointOnSurf proj;
    proj.Init( RV, S, uMin, uMax, vMin, vMax, tol, Algo );
    Standard_Integer numPoint = proj.NbPoints();
    std::cout << "  We have " << numPoint << " projected points" << std::endl;
    if( numPoint > 0 ) {
      proj.LowerDistanceParameters( tempU[2], tempV[2] );
      Sb.D0( tempU[1], tempV[1], RV );
      double  dx = RV.X() - R[ 0 ];
      double  dy = RV.Y() - R[ 1 ];
      double  dz = RV.Z() - R[ 2 ];
      double  d = sqrt( dx * dx + dy * dy + dz * dz );
      tempDiff[2] = d;
      std::cout << "  UV 3 = " << tempU[2] << "," << tempV[2] << " - DIFF 2 " << d << std::endl;
      if( tempDiff[2] < tempDiff[minProj] * 0.99 )
      {
        minProj = 2;
      }
    }
  }
#endif
  {
    u = tempU[minProj];
    v = tempV[minProj];
#ifdef JWJ_PRINT
    std::cout << "  Min = " << ( minProj  + 1 ) << ", UV (" << u << ", " << v << ") - DIFF - " << tempDiff[minProj] << std::endl;
#endif
  }
    
#ifdef SCALE_UV
#ifdef JWJ_PRINT
  if( ( u < faceInfo.minU - 1e-8 ) || ( u > faceInfo.maxU + 1e-8 ) )
  {
    std::cerr << "OOPS: OCCT_GetUVFromXYZ: Input U out of range " << u << ", (" << faceInfo.minU << " - " << faceInfo.maxU << ")" << std::endl;
  }
  if( ( v < faceInfo.minV - 1e-8 ) || ( v > faceInfo.maxV + 1e-8 ) )
  {
    std::cerr << "OOPS: OCCT_GetUVFromXYZ: Input V out of range " << v << ", (" << faceInfo.minV << " - " << faceInfo.maxV << ")" << std::endl;
  }
#endif

  u = ( u - faceInfo.minU ) / faceInfo.uScale;
  v = ( v - faceInfo.minV ) / faceInfo.vScale;
#ifdef JWJ_PRINT
  cout << "Scaled UV - " << u << ", " << v << std::endl;

  if( ( u < 0.0 - 1e-8 ) || ( u > SCALE_FACTOR + 1e-8 ) )
  {
    std::cerr << "OOPS: OCCT_GetUVFromXYZ: Scaled Input U out of range " << u << std::endl;
  }
  if( ( v < 0.0 - 1e-8 ) || ( v > SCALE_FACTOR + 1e-8 ) )
  {
    std::cerr << "OOPS: OCCT_GetUVFromXYZ: Input V out of range " << v << std::endl;
  }
#endif
#endif
};

void OCCT_FLITE :: OCCT_GetUFromXYZ
  (Standard_Integer curvNum, Standard_Real tol, Standard_Real& u,
    Standard_Real* R)
{
  //Function to return nearest U on curve to point XYZ
  const CurveInfo& curveInfo= _curveInfo[curvNum-1];

  gp_Pnt RV;

  RV.SetX(R[0]);
  RV.SetY(R[1]);
  RV.SetZ(R[2]);

  GeomAPI_ProjectPointOnCurve proj;

  proj.Init(RV, curveInfo.curveHandle,curveInfo.minT, curveInfo.maxT);

  Standard_Integer numPoint = proj.NbPoints();

  if (numPoint>0){
    proj.Parameter(1, u);
  }else{
    u = 0.0;
  }

  if (u<curveInfo.minT){
    u = curveInfo.minT;
  }
  if (u>curveInfo.maxT){
    u = curveInfo.maxT;
  }
};

std::string OCCT_FLITE :: OCCT_GetSurfName(Standard_Integer surfNum)
{
  //Function returns surface name if one exists
  
  
  //Get the shape
  TopoDS_Face aShape = TopoDS::Face(fmap(surfNum));
  //Handle(Geom_Surface) S1 = BRep_Tool::Surface(TopoDS::Face(aShape));
  //TopoDS_Face face1 = TopoDS::Face(aShape);
  
  std::string cstring;
  
  //for (int is = 0; is<namemap.Extent();is++)
  //{
   
   //Handle(Geom_Surface) S2 = BRep_Tool::Surface(TopoDS::Face(namemap(is+1)));
   //TopoDS_Face face2 = TopoDS::Face(namemap(is+1));
  
  //if (face1.IsEqual(face2) || face1.IsPartner(face2) || face1.IsSame(face2) )
  //{
    
  //  cstring = names.at(is);
    
    
  //}else{
  //  cstring = "OCCT";
  //};
  
  
  
  //}
  
  if (faceNameMap.Contains(aShape))
  {
    int i = faceNameMap.FindIndex(aShape);
    cstring = faceNames.at(i-1);
    
    
  }else{
    cstring = "OCCT";
  };
  
  return cstring;
}


std::string OCCT_FLITE :: OCCT_GetCurvName(Standard_Integer curvNum)
{
  //Function returns surface name if one exists
  
  TopoDS_Edge aShape = TopoDS::Edge(emap(curvNum));
  
  std::string cstring;
  
  if (edgeNameMap.Contains(aShape))
  {
    int i = edgeNameMap.FindIndex(aShape);
    cstring = edgeNames.at(i-1);
  }
  else {
    cstring = "OCCT";
  };
  
  return cstring;
}

void OCCT_FLITE :: OCCT_GetSurfaceNumCurves(Standard_Integer surfNum, Standard_Integer &numCurves)
{
  // Returns the number of curves in a suface and their identifiers in 
  // list

  TopoDS_Face anFace = TopoDS::Face(fmap(surfNum));

  // Create indexed map of edges
  TopTools_IndexedMapOfShape localmap;
  TopExp::MapShapes(anFace,TopAbs_EDGE,localmap);

   //Check for degenerated edges
    //    for (int i=1;i<templocalmap.Extent()+1;i++){
    //            TopoDS_Edge edge = TopoDS::Edge(templocalmap(i));
    //            if(!BRep_Tool::Degenerated(edge)){
    //                    localmap.Add(edge);
    //            }
    //    }

  
  numCurves = localmap.Extent();

};

Standard_Integer* OCCT_FLITE :: OCCT_GetSurfaceCurves(Standard_Integer surfNum, Standard_Integer &numCurves )
              
{
  // Returns the number of curves in a suface and their identifiers in 
  // list
  Standard_Integer* list;
  TopoDS_Face anFace = TopoDS::Face(fmap(surfNum));

  // Create indexed map of edges
   // Create indexed map of edges
        TopTools_IndexedMapOfShape localmap;
        TopExp::MapShapes(anFace,TopAbs_EDGE,localmap);

         //Check for degenerated edges
      //  for (int i=1;i<templocalmap.Extent()+1;i++){
      //          TopoDS_Edge edge = TopoDS::Edge(templocalmap(i));
      //          if(!BRep_Tool::Degenerated(edge)){
      //                  localmap.Add(edge);
      //          }
      //  }
  






  numCurves = localmap.Extent();
//  std::vector<Standard_Integer> list;
//  Standard_Integer* list;
  list = (Standard_Integer*) calloc((numCurves),sizeof(Standard_Integer ));

  // Now need to find the indices of curves in this local map in the 
  // global map

  //NOTE: Indices in C++ start at 0 and in OPENCascade they
  //start at 1

  int count = 0;
  for (count =0; count < numCurves; count++)
  {
    //Get the edge 
    TopoDS_Edge anEdge = TopoDS::Edge(localmap(count+1));
    list[count] = emap.FindIndex(anEdge);
  };

  return list;
};


Standard_Real **  OCCT_FLITE :: OCCT_CalculateCurveFacets(Standard_Integer curveNum, Standard_Integer &numVertices, 
  Standard_Real maxEdgeLength)
{

  
  
  Standard_Real ** pVertices;

  //Splits a curve into Facets
  
  //First we need to extract the curve
  Standard_Real tMin, tMax;

  //Get the edge 
  TopoDS_Edge anEdge = TopoDS::Edge(emap(curveNum));

  BRepAdaptor_Curve C (anEdge);
  tMin = C.FirstParameter();
  tMax = C.LastParameter();

  //Need to use GCPnts_UniformAbscis


  //Need to ensure even number of vertices
  Standard_Real tlength;
  Standard_Real clength;
  tlength = fabs(tMax-tMin);

  //GProp_GProps System;
  //BRepGProp::LinearProperties(anEdge,System);
  clength = GCPnts_AbscissaPoint::Length(C);  

  Standard_Real EdgeLength = maxEdgeLength*(2.0);
  Standard_Integer oldVert = (int)(clength/EdgeLength);
  
  numVertices = 2*(oldVert);
  if (numVertices < 3) {numVertices = 3;}

  pVertices = (Standard_Real **)calloc(numVertices,sizeof(Standard_Real *));
  
  int i;

  for  (i=0;i<numVertices;i++)
  {
    pVertices[i] = (Standard_Real*)calloc(7,sizeof(Standard_Real));
  };

  gp_Pnt P;
  gp_Vec Ru;
  Standard_Real u;
  Standard_Real param;
  Standard_Real edgeL = clength/((Standard_Real)(numVertices-1));

  for (i=0;i<numVertices;i++)
  {
    u = ((Standard_Real)i*edgeL);

    GCPnts_AbscissaPoint anAP(C,u,tMin);
    param = anAP.Parameter();
    
    C.D1(param,P,Ru);  
    pVertices[i][0] = P.X();
    pVertices[i][1] = P.Y();
    pVertices[i][2] = P.Z();
    pVertices[i][3] = Ru.X();
    pVertices[i][4] = Ru.Y();
    pVertices[i][5] = Ru.Z();
    pVertices[i][6] = param;
  };

  return pVertices;

};

void OCCT_FLITE::OCCT_WriteBRep( Standard_CString filename )
{
  std::cout << "OCCT_WriteBRep " << filename << std::endl;
  std::ofstream brepStream( filename );
  BRepTools::Write( shape, brepStream );
  brepStream.close();

  {
    TopoDS_Shape  shapeBRep;
    std::ifstream  brepStream( filename );
    BRep_Builder  builder;
    BRepTools::Read( shapeBRep, brepStream, builder );
    brepStream.close();
  }
}

//TODO: check data types
// cpp -> fortran
// int -> integer
// double -> real*8

static OCCT_FLITE ourclass;

// C wrapper interfaces to C++ routines

extern "C" {
  
  //Don't need all this pointer stuff, used static ourclass
  
  //Need to write new and delete functions to pass
  //pointers to FORTRAN which point to the object
  
  //void cocct_flite_new_(void *This){ 
  //  cout << "Set object pointer \n";
  //  
  //  OCCT_FLITE* aclass = new OCCT_FLITE;
  //  This = static_cast<void *>(&aclass);
    
  //}

  //void cocct_flite_delete_(void *This){
  //  delete This;
  //}

  //Now the functions, the pointer This allows the 
  //OCCT_FLITE object to remain in memory

  void FC_GLOBAL_( cocct_loadstep, COCCT_LOADSTEP )(char *filename, double *prec){
    //cout << filename << "\n";
    //cout << This << "\n";
    //This = new OCCT_FLITE;
    ourclass.OCCT_LoadSTEP(filename, *prec);
      
    //static_cast<OCCT_FLITE*>(This)->OCCT_LoadSTEP(filename);
    
    
  }  

  void FC_GLOBAL_( cocct_loadiges, COCCT_LOADIGES )(char *filename, double *prec){
    ourclass.OCCT_LoadIGES(filename, *prec);
    
  }

  void FC_GLOBAL_( cocct_loadbrep, COCCT_LOADBREP )( char *filename )
  {
    ourclass.OCCT_LoadBRep( filename );
  }

  void FC_GLOBAL_( cocct_getnumcurves, COCCT_GETNUMCURVES )(int *n){
    ourclass.OCCT_GetNumCurves(*n);
    //This->OCCT_GetNumCurves(*n);
  }

  void FC_GLOBAL_( cocct_getnumsurfaces, COCCT_GETNUMSURFACES )(int *n){
    ourclass.OCCT_GetNumSurfaces(*n);
  }

  void FC_GLOBAL_( cocct_getlinetbox, COCCT_GETLINETBOX )(int *curveNum, double *tMin, double *tMax){
    ourclass.OCCT_GetLineTBox(*curveNum, *tMin, *tMax);
  }

  void FC_GLOBAL_( cocct_getlinexyzfromt, COCCT_GETLINEXYZFROMT )(int *CurveNum, double *u, double* XYZ){
    ourclass.OCCT_GetLineXYZFromT(*CurveNum,*u,XYZ);
  }

  void FC_GLOBAL_( cocct_getlinepointderiv, COCCT_GETLINEPOINTDERIV)(int *CurveNum, double *u, double *Ru, double *Ruu){
    ourclass.OCCT_GetLinePointDeriv(*CurveNum,*u,Ru,Ruu);
  }

  void FC_GLOBAL_( cocct_surfaceuvbox, COCCT_SURFACEUVBOX )(int *surfNum, double *uMin, double *vMin, double *uMax, double *vMax){
    ourclass.OCCT_SurfaceUVBox(*surfNum,*uMin,*vMin,*uMax,*vMax);
  }

  void FC_GLOBAL_( cocct_getuvpointinfoall, COCCT_GETUVPOINTINFOALL)(int *surfNum, double *u, double *v, double *R, double *Ru, double *Rv,
          double *Ruv, double *Ruu, double *Rvv){
    ourclass.OCCT_GetUVPointInfoAll(*surfNum,*u,*v,R,Ru,Rv,Ruv,Ruu,Rvv);
  }

  void FC_GLOBAL_(cocct_getxyzfromuv1, COCCT_GETXYZFROMUV1)(int *surfNum, double *u,double *v, double *R){
    ourclass.OCCT_GetXYZFromUV1(*surfNum,*u,*v,R);
  }

  void FC_GLOBAL_(cocct_getsurfacenumcurves, COCCT_GETSURFACENUMCURVES)(int *surfNum, int *numCurves){
    ourclass.OCCT_GetSurfaceNumCurves(*surfNum,*numCurves);
  }
  

  void FC_GLOBAL_(cocct_getuvfromxyz, COCCT_GETUVFROMXYZ)(int *surfNum, double *tol, int *useUV, double* u, double* v, double* R){
    ourclass.OCCT_GetUVFromXYZ(*surfNum,*tol,*useUV,*u,*v,R);
  }

  void FC_GLOBAL_( cocct_getufromxyz, COCCT_GETUFROMXYZ)(int *curvNum, double *tol, double* u, double* R){
    ourclass.OCCT_GetUFromXYZ(*curvNum,*tol,*u,R);
  }

  void FC_GLOBAL_(cocct_getsurfacecurves, COCCT_GETSURFACECURVES)
             (int *surfNum, int *numCurves, int *Curves){
    int* CurvesC = ourclass.OCCT_GetSurfaceCurves(*surfNum,*numCurves);
    int i,n;
    n = (int) *numCurves;
    for (i=0;i<n;i++){
      Curves[i] = CurvesC[i];
    };  
  }


  void FC_GLOBAL_(cocct_calculatecurvefacets, COCCT_CALCULATECURVEFACETS)
               (int *curveNum, int *numVertices, double *maxEdgeLength, 
    double* Points1,double* Points2,double* Points3,double* Points4,double* Points5,
            double* Points6,double* Points7     ){

  //  ourclass.OCCT_CalculateCurveFacets(*curveNum,*numVertices,*maxEdgeLength);
  //  Points1 =  ourclass.pVertices1;
  //  Points2 =  ourclass.pVertices2;
  //  Points3 =  ourclass.pVertices3;
  //  Points4 =  ourclass.pVertices4;
  //  Points5 =  ourclass.pVertices5;
  //  Points6 =  ourclass.pVertices6;
  //  Points7 =  ourclass.pVertices7;


    double** PointsC = ourclass.OCCT_CalculateCurveFacets(*curveNum,*numVertices,*maxEdgeLength);
    

    int i,n;
                n = (int) *numVertices;
                for (i=0;i<n;i++){
      
                        Points1[i] = PointsC[i][0];
      Points2[i] = PointsC[i][1];
      Points3[i] = PointsC[i][2];
      Points4[i] = PointsC[i][3];
      Points5[i] = PointsC[i][4];
      Points6[i] = PointsC[i][5];
      Points7[i] = PointsC[i][6];
      
                };
  }
  
  void FC_GLOBAL_(cocct_setsurfname, COCCT_SETSURFNAME)(int *surfNum, char* nameOut){
    std::string name = ourclass.OCCT_GetSurfName(*surfNum);
    
    
    
    
    for (int i=0;i<255;i++){
      nameOut[i] = ' ';
    }
    
    for (int j=0; j<name.length(); j++)
    {
      nameOut[j] = name[j];
    }
    
    
  }
  
  void FC_GLOBAL_(cocct_setcurvname, COCCT_SETCURVNAME)(int *surfNum, char* nameOut){
    std::string name = ourclass.OCCT_GetCurvName(*surfNum);
    
    
    
    
    for (int i=0;i<255;i++){
      nameOut[i] = ' ';
    }
    
    for (int j=0; j<name.length(); j++)
    {
      nameOut[j] = name[j];
    }
    
    
  }

  void FC_GLOBAL_(cocct_getsurfvis, COCCT_GETSURFVIS)(int *surfNum)
  {
    // This subroutine is a c-wrapper to write the visualisation mesh to a file
    OCCT_FLITE::visMesh_ msh = ourclass.buildVisMesh(0.1,*surfNum);
    
    //Generate file name
    std::ostringstream ss;
    ss << *surfNum;
    
    std::string filename = ss.str()+".FacVis";
    
    const char * s = filename.c_str();

    std::ofstream outFile(s, std::ofstream::out);
    
    outFile << msh.numTri << " \t" << msh.numNodes << "\n";

    for (int i=0;i<msh.numNodes;i++)
    {
      outFile << i+1 << " \t" << msh.x.at(i) 
                     << " \t" << msh.y.at(i)
               << " \t" << msh.z.at(i) << "\n";
    }

    for (int i=0;i<msh.numTri;i++)
    {
      outFile << i+1 << " \t" << msh.N1.at(i)
               << " \t" << msh.N2.at(i)
                << " \t" << msh.N3.at(i) << "\n";  
    }

    outFile.close();  
  }
  
  void FC_GLOBAL_(cocct_writecurvvis,COCCT_WRITECURVVIS)( int *curvNum )
        {
                // This subroutine is a c-wrapper to write the visualisation curve to a file
                
    int n;

    double** Points = ourclass.buildVisCurve(*curvNum, n,0.1);
 
    
    

                //Generate file name
                std::ostringstream ss;
                ss << *curvNum;

                std::string filename = ss.str()+".CurVis";

                const char * s = filename.c_str();

                std::ofstream outFile(s, std::ofstream::out);

                

                for (int i=0;i<n;i++)
                {
                        outFile << i+1 << " \t" << Points[i][0]
                                       << " \t" << Points[i][1]
                                       << " \t" << Points[i][2] << "\n";
                }


                outFile.close();
        }


}; 


