cmake_minimum_required( VERSION 2.8 )

add_subdirectory( MUMPS_c )
add_subdirectory( MUMPS_f )
add_subdirectory( libseq_c )