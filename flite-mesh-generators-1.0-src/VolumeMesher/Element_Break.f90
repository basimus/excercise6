!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!     Break large elements by inserting a node at the centroid.       
!<       
!*******************************************************************************
SUBROUTINE Element_Break()

  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  USE Mapping3D
  IMPLICIT NONE

  INTEGER :: IT, IP, ipp(4), i, itry,IB
  INTEGER :: NBold_Tet, NBold_Pt, NBcount_Pt, Idou, ILoop, indmax, indmin, Isucc, ILoopMax
  REAL*8  :: pp(3,4),p0(3),xl,fMap(3,3)
  REAL*8  :: RAsq,RAstand,RAfac,RAmaxloc,RAminloc,RAvol,RAsph

  IF(.NOT. CircumUpdated) CALL Get_Tet_Circum(0)
  IF(.NOT. VolumeUpdated) CALL Get_Tet_Volume(0)

  Idou    = 1
  RAfac   = 2.0d0
  RAstand = BGSpacing%BasicSize / dsqrt(2.d0)
  RAvol   = IdealVolume
  RAsph   = dsqrt(6.d0)/4.d0      !--- the radius of an unit equal-size tet.
  RAsph   = 1.2*BGSpacing%BasicSize*RAsph

  DO i=1,Idou
     RAstand = RAstand * RAfac
     RAsph   = RAsph   * RAfac
  ENDDO

  NBcount_Pt = 0
  ILoop = 0

  print *,' basic size is:',   BGSpacing%BasicSize




  IF(Element_Break_Method==8)THEN
    ILoopMax = -10
  ELSE
    ILoopMax = 15
  END IF


  Loop_Idou : DO WHILE(Idou>=0)

     NBold_Tet = NB_Tet
     NBold_Pt  = NB_Point

     Loop_IT : DO IT = 1,NBold_Tet

        
        IF(IP_Tet(4,IT) <=0)           CYCLE   !--- a false element
       
        IF(IP_Tet(4,IT) <= NBcount_Pt) CYCLE   !--- a checked element
        
        IF(IP_Tet(4,IT) >  NBold_Pt)   CYCLE   !--- a new generated element in this loop
        
        IF(Sphere_Tet(4,IT)<=RAsph)    CYCLE   !--- a small element
        
        IF(Volume_Tet(IT)  < RAvol)    CYCLE   !--- a small element
        

        ipp(1:4)  = IP_Tet(1:4,IT)
        pp(:,1:4) = Posit(:,ipp(1:4))
        IF(BGSpacing%Model<0)THEN
           IF(BGSpacing%Model==-1)THEN
              fMap(:,:) = BGSpacing%BasicMap(:,:)
           ELSE
              fMap(:,:) = fMap_Tet(:,:,IT)
           ENDIF
           DO i=1,4
              pp(:,i) = Mapping3D_Posit_Transf(pp(:,i),fMap,1)
           ENDDO
        ELSE IF(BGSpacing%Model>1)THEN
           DO i=1,4
              pp(:,i) = pp(:,i) / Scale_Tet(IT)
           ENDDO
        ENDIF
        p0(:) = (pp(:,1)+pp(:,2)+pp(:,3)+pp(:,4))/4.d0

        !--- modify the position of the inserted point
        DO itry=1,4
           RAmaxloc = 0.
           RAminloc = HUGE(0.0d0)

           DO i=1,4
              RAsq  =  Geo3D_Distance(p0,pp(:,i))
              IF(RAsq>RAmaxloc)THEN
                 RAmaxloc = RAsq
                 indmax = i
              ENDIF
              IF(RAsq<=RAminloc)THEN
                 RAminloc = RAsq
                 indmin = i
              ENDIF
           ENDDO
           
           IF(RAmaxloc+RAminloc <= 2.0*RAstand) CYCLE Loop_IT    !--- too small
           IF(RAmaxloc <= 0.1*Sphere_Tet(4,IT)) CYCLE Loop_IT    !--- too sharp
           IF(RAminloc >= RAstand) EXIT           !--- a large element, split
           IF(itry==4) CYCLE Loop_IT              !--- fail 4 times, skip
           xl = 0.5d0 * (1.d0 - RAstand/RAmaxloc)
           p0 = p0*(1.d0-xl)+pp(:,indmax)*xl      !--- move p0, and try again
        ENDDO

        NB_Point = NB_Point + 1
        IF(NB_Point>nallc_point) CALL ReAllocate_Point()
        IF(BGSpacing%Model<0)THEN
           p0 = Mapping3D_Posit_Transf(p0,fMap,-1)
        ELSE IF(BGSpacing%Model>1)THEN
           p0 = p0 * Scale_Tet(IT)
        ENDIF
        Posit(:,NB_Point) = p0(:)
        IF(BGSpacing%Model<-1 .OR. BGSpacing%Model>1)THEN
           CALL Get_Point_Mapping(NB_Point)
        ENDIF

        Isucc   = 0
        CALL Insert_Point_Delaunay(NB_Point,IT,Isucc,0)
        IF(Isucc<=0)THEN
           NB_Point = NB_Point-1
        ENDIF

        IF(Debug_Display>2 .AND. MOD(NB_Point,100000)==0)THEN
           WRITE(*,'(a,I3,a,I3,a,i8,a,i8,a)') ' ILoop=',Idou,' :',ILoop,     &
                ' Total elements.:', NB_Tet,    &
                ' Total points.: ',NB_Point,CHAR(13)
           CALL Check_Mesh_Geometry('Element_Break')
        ENDIF

     ENDDO Loop_IT

     CALL Remove_tet()
   

     IF(NBold_Pt == NB_Point .OR. ILoop==ILoopMax)THEN
        IF(ILoop==15) WRITE(29,'(/,a)')'WARNING--- Element_Break: too many loops for breaking'
        Idou       = Idou-1
        RAstand    = RAstand / RAfac
        RAsph      = RAsph   / RAfac
        NBcount_Pt = 0
        ILoop      = 0
        WRITE(*,*)
     ELSE
        WRITE(*,'(a,I3,a,I3,a,i8,a,i8,a)') ' ILoop=',Idou,' :',ILoop,     &
             ' Total elements.:', NB_Tet,    &
             ' Total points.: ',NB_Point,CHAR(13)
        NBcount_Pt =  NBold_Pt
        ILoop     = ILoop + 1
     ENDIF

  ENDDO Loop_Idou
  WRITE(*,*)

  IF(Debug_Display>3)THEN
     Mark_Point(1:NB_Point) = 0
     DO IT=1,NB_Tet
        Mark_Point(IP_Tet(1:4,IT)) = 1
     ENDDO
     IF(Element_Break_Method/=8)THEN
       DO IP=1,NB_Point
          IF(Mark_Point(IP)==0)THEN
             WRITE(29,*)'Error--- : a node lost: ',IP
             CALL Error_Stop ('Element_Break')
          ENDIF
       ENDDO
     


     

     
       IF(Recovery_Time<=1)THEN
          CALL Check_Next(-2)
       ELSE
          CALL Check_Next(-3)
       ENDIF
    END IF

  ENDIF

  RETURN
END SUBROUTINE Element_Break

!*******************************************************************************
!>
!!     Break large elements by inserting a node at the centroid.  
!!     The largest element chosen first.     
!<       
!*******************************************************************************
SUBROUTINE Element_Break2()

  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  USE Mapping3D
  USE HeapTree
  IMPLICIT NONE

  INTEGER :: IT, IP, ipp(4), i, itry,IB
  INTEGER :: indmax, Isucc
  REAL*8  :: pp(3,4),p0(3),xl,fMap(3,3),vt
  REAL*8  :: RAsq,RAstand,RAmaxloc,RAminloc,RAvol,RAsph
  TYPE(HeapTreeType) :: Heap

  IF(.NOT. CircumUpdated) CALL Get_Tet_Circum(0)
  IF(.NOT. VolumeUpdated) CALL Get_Tet_Volume(0)

  RAstand = BGSpacing%BasicSize / dsqrt(2.d0)
  RAvol   = IdealVolume
  RAsph   = dsqrt(6.d0)/4.d0      !--- the radius of an unit equal-size tet.
  RAsph   = 1.2 * BGSpacing%BasicSize * RAsph

  !--- set descending heap-tree with volume (the logest edge takes the first)

  Heap%Ascending = .FALSE.
  CALL HeapTree_Allocate( Heap, NB_Tet )
  DO IT = 1, NB_Tet
     vt = Volume_Tet(IT)
     IF(Sphere_Tet(4,IT)<=RAsph) vt = 0.d0
     IF(vt<RAvol)                vt = 0.d0
     CALL HeapTree_AddValue(Heap, vt )
  ENDDO

  Loop_IT : DO 

     IT = Heap%toList(1)
     vt = Heap%v(IT)
     IF(vt <= RAvol)       EXIT   !--- a small element

     IF(IP_Tet(4,IT) <= 0)THEN
        !--- a false element
        vt = 0.d0
        CALL HeapTree_ResetNodeValue( Heap, IT, vt )
        CYCLE
     ENDIF

     ipp(1:4)  = IP_Tet(1:4,IT)
     pp(:,1:4) = Posit(:,ipp(1:4))
     IF(BGSpacing%Model<0)THEN
        IF(BGSpacing%Model==-1)THEN
           fMap(:,:) = BGSpacing%BasicMap(:,:)
        ELSE
           fMap(:,:) = fMap_Tet(:,:,IT)
        ENDIF
        DO i=1,4
           pp(:,i) = Mapping3D_Posit_Transf(pp(:,i),fMap,1)
        ENDDO
     ELSE IF(BGSpacing%Model>1)THEN
        DO i=1,4
           pp(:,i) = pp(:,i) / Scale_Tet(IT)
        ENDDO
     ENDIF
     p0(:) = (pp(:,1)+pp(:,2)+pp(:,3)+pp(:,4))/4.d0

     !--- modify the position of the inserted point
     Isucc = 0
     DO itry=1,4
        RAmaxloc = 0.
        RAminloc = HUGE(0.0d0)

        DO i=1,4
           RAsq  =  Geo3D_Distance(p0,pp(:,i))
           IF(RAsq>RAmaxloc)THEN
              RAmaxloc = RAsq
              indmax = i
           ENDIF
           IF(RAsq<=RAminloc)THEN
              RAminloc = RAsq
           ENDIF
        ENDDO
        IF(RAmaxloc+RAminloc <= 2.0*RAstand) EXIT     !--- too small
        IF(RAmaxloc <= 0.1*Sphere_Tet(4,IT)) EXIT     !--- too sharp
        IF(RAminloc >= RAstand)THEN
           Isucc = 1
           EXIT           !--- a large element, split
        ENDIF
        xl = 0.5d0 * (1.d0 - RAstand/RAmaxloc)
        p0 = p0*(1.d0-xl)+pp(:,indmax)*xl      !--- move p0, and try again
     ENDDO

     IF(Isucc==0)THEN
        vt = 0.d0
        CALL HeapTree_ResetNodeValue( Heap, IT, vt )
        CYCLE
     ENDIF

     NB_Point = NB_Point + 1
     IF(NB_Point>nallc_point) CALL ReAllocate_Point()
     IF(BGSpacing%Model<0)THEN
        p0 = Mapping3D_Posit_Transf(p0,fMap,-1)
     ELSE IF(BGSpacing%Model>1)THEN
        p0 = p0 * Scale_Tet(IT)
     ENDIF
     Posit(:,NB_Point) = p0(:)
     IF(BGSpacing%Model<-1 .OR. BGSpacing%Model>1)THEN
        CALL Get_Point_Mapping(NB_Point)
     ENDIF

     Isucc   = 0
     CALL Insert_Point_Delaunay(NB_Point,IT,Isucc,3)

     IF(Isucc<=0)THEN
        NB_Point = NB_Point - 1
        vt = 0.d0
        CALL HeapTree_ResetNodeValue( Heap, IT, vt )
     ELSE
        DO i = 1, NB_PoleTet
           IT = IT_PoleTet(i)
           vt = Volume_Tet(IT)
           IF(Sphere_Tet(4,IT)<=RAsph) vt = 0.d0
           IF(vt<RAvol)                vt = 0.d0
           IF(IT<=Heap%numNodes)THEN
              CALL HeapTree_ResetNodeValue (Heap, IT, vt)
           ELSE
              CALL HeapTree_AddValue(Heap, vt )
           ENDIF
        ENDDO
     ENDIF

     IF(Debug_Display>2 .AND. MOD(NB_Point,100000)==0)THEN
        WRITE(*,'(a,i8,a,i8,a)') ' Total elements.:', NB_Tet,    &
             ' Total points.: ',NB_Point,CHAR(13)
        CALL Check_Mesh_Geometry('Element_Break2')
     ENDIF

  ENDDO Loop_IT

  CALL Remove_tet()


  WRITE(*,*)

  CALL HeapTree_Clear(Heap)

  IF(Debug_Display>2)THEN
     WRITE(*,'(a,i8,a,i8,a)') ' Total elements.:', NB_Tet,    &
          ' Total points.: ',NB_Point,CHAR(13)
     CALL Check_Mesh_Geometry('Element_Break2')
  ENDIF

  IF(Debug_Display>3)THEN
     Mark_Point(1:NB_Point) = 0
     DO IT=1,NB_Tet
        ipp(1:4) = IP_Tet(1:4,IT)
        Mark_Point(ipp(1:4)) = 1
     ENDDO
     DO IP=1,NB_Point
        IF(Mark_Point(IP)==0)THEN
           WRITE(29,*)'Error--- : a node lost: ',IP
           CALL Error_Stop ('Element_Break2')
        ENDIF
     ENDDO

     IF(Recovery_Time<=1)THEN
        CALL Check_Next(-2)
     ELSE
        CALL Check_Next(-3)
     ENDIF

  ENDIF

  RETURN
END SUBROUTINE Element_Break2


!*******************************************************************************
!>
!!     Break those elements with all four nodes being boundary nodes
!!         by splitting edge of face (not by Delaunay).             
!!
!<
!*******************************************************************************
SUBROUTINE BoundCell_Break()

  USE common_Parameters
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: IP, IT, i, ip1, ip2, ip3, idr, ipp(2), js
  INTEGER :: Isucc, NBold_Tet, IEdge

  VolumeUpdated = .FALSE.
  CircumUpdated = .FALSE.

  !--- build surface edges if not exsiting
  IF(Edge_BD_Tree%numNets==0)THEN
     CALL Surface_Edge()
  ENDIF

  !--- Associate points with elements for subroutine Search_Pole_Surround2
  CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)

  NBold_Tet = NB_Tet
  DO IT = 1, NBold_Tet

     DO  js = 1,6
        ipp(1:2) = IP_Tet(I_Comb_Tet(1:2,js),IT)
        IF(ipp(1)>NB_BD_Point) CYCLE
        IF(ipp(2)>NB_BD_Point) CYCLE

        CALL NodeNetTree_Search(2, ipp, Edge_BD_Tree, IEdge)
        IF(IEdge>0) CYCLE

        !--- an edge connecting boundary nodes (but not boundary edge) found
        !    split it.

        ip1 = ipp(1)
        ip2 = ipp(2)

        !---- search the elements surrounding the edge
        !---- for checking the minimum volume and building the cavity
        CALL Search_Pole_Surround2(ip1,ip2)

        NB_Point = NB_Point + 1
        IF(NB_Point>nallc_point) CALL ReAllocate_Point()
        Posit(:,NB_Point) = (Posit(:,ip2) + Posit(:,ip1))* 0.5d0
        IF(BGSpacing%Model<-1 .OR. BGSpacing%Model>1)THEN
           CALL Get_Point_Mapping(NB_Point)
        ENDIF

        Isucc   = 1
        CALL Insert_Point_SplitEdge(NB_Point,ip1,ip2,Isucc)

        EXIT
     ENDDO
  ENDDO


  !--- release memory
  CALL LinkAssociation_Clear(PointAsso)

  NBold_Tet = NB_Tet
  DO IT = 1, NBold_Tet
     idr = 0
     DO i = 1,4
        IF(Next_Tet(i,it)<=0) idr = idr+1
     ENDDO

     IF(idr<2) CYCLE

     IF(idr==2 .OR. idr==4)THEN
        CALL Error_Stop ('  BoundCell_Break ')
     ENDIF

     DO idr = 1,4
        IF(Next_Tet(idr,it)>0) EXIT
     ENDDO

     ip1 = IP_Tet(ITri_Tet(1,idr),IT)
     ip2 = IP_Tet(ITri_Tet(2,idr),IT)
     ip3 = IP_Tet(ITri_Tet(3,idr),IT)
     NB_Point = NB_Point + 1
     IF(NB_Point>nallc_point) CALL ReAllocate_Point()
     Posit(:,NB_Point) = (Posit(:,ip1) + Posit(:,ip2) + Posit(:,ip3)) / 3.d0
     IF(BGSpacing%Model<-1 .OR. BGSpacing%Model>1)THEN
        CALL Get_Point_Mapping(NB_Point)
     ENDIF

     Isucc = 1
     CALL Insert_Point_SplitFace(NB_Point,IT,idr,Isucc)

  ENDDO

  IF(Debug_Display>3)THEN
     CALL Check_Next(-1)
  ENDIF

  RETURN
END SUBROUTINE BoundCell_Break


