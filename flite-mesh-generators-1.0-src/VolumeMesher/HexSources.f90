!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!**************************************************************************
!>
!!  Build_HexSources
!!  This subroutine builds the volume mesh using hex sources
!!
!<
!**************************************************************************
SUBROUTINE Build_HexSources()
	USE common_Parameters
	USE SpacingStorage
	USE Geometry3DAll
    USE SpacingStorageGen
    USE Source_Type
    USE TetMeshStorage
	
	IMPLICIT NONE
	
	INTEGER :: firstNode, l, noBad, ie
	REAL*8  :: alpha, delta, centre(3)
	
	!Restore the blank hull
	CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
	CALL TetMeshStorage_NodeRestore(NB_Point, Posit, HullBackUp)
	CALL Next_Build()
	CALL Get_Tet_Circum(0)
	CALL Get_Tet_Volume(0)
	CALL Set_Domain_Mapping()
	
	!Just to test
	alpha = 1.5d0
	delta = 10.0d0
	centre(1) = 0.0d0
	centre(2) = 0.0d0
	centre(3) = 0.0d0
	l = 10
	
	firstNode = NB_Point + 1
	
	IF(Debug_Display>0)THEN			
		WRITE(*,*)'Generating test Hex source'
		WRITE(29,*)'Generating test Hex source'
	END IF
	
	CALL HexPoint(alpha,l,delta,centre)
	Mark_Point(1:NB_Point) = 0
	CALL Insert_Points_EasyPeasy(firstNode, NB_Point, 10)
	CALL Remove_Point()
	CALL Get_Tet_Circum(0)
	CALL Get_Tet_Volume(0)
	CALL Set_Domain_Mapping()
	
	
	MeshOut%GridOrder = GridOrder
    MeshOut%NB_Point  = NB_Point
    MeshOut%NB_Tet    = NB_Tet
    ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
    ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
    MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
    MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)
    CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_test',JobNameLength+5, -15, MeshOut)
    DEALLOCATE(MeshOut%IP_Tet)
    DEALLOCATE(MeshOut%Posit)
	
	WRITE(*,*)'---- Optimise  Weights ------'
    CALL MCS_Optim_Weight()
    CALL Get_Tet_SphereCross(0)
	
    CALL Check_Mesh_Geometry2('test')

	noBad = 0
    DO ie=1,NB_Tet
      IF(Mark_Tet(ie)>0)THEN
		noBad = noBad+1
	  ENDIF
	ENDDO  
	MeshOut%GridOrder = GridOrder
    MeshOut%NB_Point  = NB_Point
    MeshOut%NB_Tet    = NB_Tet
    ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
    ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
    MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
    MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)
    CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_bad',JobNameLength+4, -15, MeshOut, Surf,Mark_Tet,noBad)
	DEALLOCATE(MeshOut%IP_Tet)
    DEALLOCATE(MeshOut%Posit)
	
	

END SUBROUTINE Build_HexSources


!**************************************************************************
!>
!!  HexPoint
!!  This subroutine generates points in a hex based point source
!!  (Does not insert)
!!  alpha = expansion factor ratio > 1
!!  l = number of layers to generate
!!  delta1 = initial spacing 
!!  centre(3) = the centre of the source
!!
!<
!**************************************************************************
SUBROUTINE HexPoint(alpha,l,delta,centre)
	USE common_Parameters
	USE Geometry3DAll
	
	IMPLICIT NONE
	
	REAL*8, INTENT(IN) :: alpha, delta, centre(3)
	INTEGER, INTENT(IN) :: l
    INTEGER :: sourceStart
	INTEGER :: lastFirst, thisFirst, il, nPLine
	INTEGER :: lastZFirst, ij, ip, ik
	REAL*8 :: a
	
	!First build the initial cube
	NB_Point = NB_Point + 1
    IF(NB_Point>nallc_point) THEN     
      CALL ReAllocate_Point()            
    END IF	
	Posit(1,NB_Point) = centre(1) - 0.5d0*delta
	Posit(2,NB_Point) = centre(2) + 0.5d0*delta
	Posit(3,NB_Point) = centre(3) - 0.5d0*delta
	
	!This will help us out later
	lastFirst = NB_Point
	
	
	NB_Point = NB_Point + 1
    IF(NB_Point>nallc_point) THEN     
      CALL ReAllocate_Point()            
    END IF	
	Posit(1,NB_Point) = centre(1) + 0.5d0*delta
	Posit(2,NB_Point) = centre(2) + 0.5d0*delta
	Posit(3,NB_Point) = centre(3) - 0.5d0*delta
	
	NB_Point = NB_Point + 1
    IF(NB_Point>nallc_point) THEN     
      CALL ReAllocate_Point()            
    END IF	
	Posit(1,NB_Point) = centre(1) + 0.5d0*delta
	Posit(2,NB_Point) = centre(2) - 0.5d0*delta
	Posit(3,NB_Point) = centre(3) - 0.5d0*delta
	
	NB_Point = NB_Point + 1
    IF(NB_Point>nallc_point) THEN     
      CALL ReAllocate_Point()            
    END IF	
	Posit(1,NB_Point) = centre(1) - 0.5d0*delta
	Posit(2,NB_Point) = centre(2) - 0.5d0*delta
	Posit(3,NB_Point) = centre(3) - 0.5d0*delta
	
	NB_Point = NB_Point + 1
    IF(NB_Point>nallc_point) THEN     
      CALL ReAllocate_Point()            
    END IF	
	Posit(1,NB_Point) = centre(1) - 0.5d0*delta
	Posit(2,NB_Point) = centre(2) + 0.5d0*delta
	Posit(3,NB_Point) = centre(3) + 0.5d0*delta
	
	NB_Point = NB_Point + 1
    IF(NB_Point>nallc_point) THEN     
      CALL ReAllocate_Point()            
    END IF	
	Posit(1,NB_Point) = centre(1) + 0.5d0*delta
	Posit(2,NB_Point) = centre(2) + 0.5d0*delta
	Posit(3,NB_Point) = centre(3) + 0.5d0*delta
	
	NB_Point = NB_Point + 1
    IF(NB_Point>nallc_point) THEN     
      CALL ReAllocate_Point()            
    END IF	
	Posit(1,NB_Point) = centre(1) + 0.5d0*delta
	Posit(2,NB_Point) = centre(2) - 0.5d0*delta
	Posit(3,NB_Point) = centre(3) + 0.5d0*delta
	
	NB_Point = NB_Point + 1
    IF(NB_Point>nallc_point) THEN     
      CALL ReAllocate_Point()            
    END IF	
	Posit(1,NB_Point) = centre(1) - 0.5d0*delta
	Posit(2,NB_Point) = centre(2) - 0.5d0*delta
	Posit(3,NB_Point) = centre(3) + 0.5d0*delta
	
	
	
	DO il = 1,l
	  thisFirst = NB_Point + 1
	  
	  !Calculate the distance to move to the next front to allow a whole number of the
	  !new edges on the next layer at the next spacing
	  WRITE(*,*)'Expansion Factor',alpha**(REAL(il))
	  a = ((1.0d0+2.0d0*REAL(il))*(alpha**(REAL(il))) &
	        - (2.0d0*REAL(il) - 1.0d0)*(alpha**(REAL(il)-1.0d0)))*0.5d0
	
	  !Skipping the last point number of points on each line
	  nPLine = (1+2*il);  
	  
	  NB_Point = NB_Point + 1
	  IF(NB_Point>nallc_point) THEN     
		CALL ReAllocate_Point()            
	  END IF
      Posit(1,NB_Point) = Posit(1,lastFirst) - a*delta
	  Posit(2,NB_Point) = Posit(2,lastFirst) + a*delta
	  Posit(3,NB_Point) = Posit(3,lastFirst) - a*delta	  
	  
	  lastZFirst = NB_Point
	
	  !******************
      !Sort out the first Z cap	
	  DO ij = 1,nPLine
	    DO ip = 1,nPLine
	      NB_Point = NB_Point + 1
	      IF(NB_Point>nallc_point) THEN     
		    CALL ReAllocate_Point()            
	      END IF
          Posit(1,NB_Point) = Posit(1,NB_Point-1) + delta*(alpha**(REAL(il)))
	      Posit(2,NB_Point) = Posit(2,NB_Point-1)
	      Posit(3,NB_Point) = Posit(3,NB_Point-1)	    
	    END DO
		
		NB_Point = NB_Point + 1
	    IF(NB_Point>nallc_point) THEN     
		  CALL ReAllocate_Point()            
	    END IF
        Posit(1,NB_Point) = Posit(1,lastZFirst) 
	    Posit(2,NB_Point) = Posit(2,lastZFirst) - REAL(ij)*delta*(alpha**(REAL(il)))
	    Posit(3,NB_Point) = Posit(3,lastZFirst)	  
				
	  END DO

      DO ip = 1,nPLine
	    NB_Point = NB_Point + 1
	    IF(NB_Point>nallc_point) THEN     
		  CALL ReAllocate_Point()            
	    END IF
        Posit(1,NB_Point) = Posit(1,NB_Point-1) + delta*(alpha**(REAL(il)))
	    Posit(2,NB_Point) = Posit(2,NB_Point-1)
	    Posit(3,NB_Point) = Posit(3,NB_Point-1)	    
	  END DO
      !******************
	  ! Spiral round moving up in Z
	  DO ik = 1,(nPLine-1)
        NB_Point = NB_Point + 1
	    IF(NB_Point>nallc_point) THEN     
		  CALL ReAllocate_Point()            
	    END IF
        Posit(1,NB_Point) = Posit(1,lastZFirst) 
	    Posit(2,NB_Point) = Posit(2,lastZFirst)
	    Posit(3,NB_Point) = Posit(3,lastZFirst)	 + delta*(alpha**(REAL(il)))
		lastZFirst = NB_Point
		
		DO ip = 1,nPLine
	      NB_Point = NB_Point + 1
	      IF(NB_Point>nallc_point) THEN     
		    CALL ReAllocate_Point()            
	      END IF
          Posit(1,NB_Point) = Posit(1,NB_Point-1) + delta*(alpha**(REAL(il)))
	      Posit(2,NB_Point) = Posit(2,NB_Point-1)
	      Posit(3,NB_Point) = Posit(3,NB_Point-1)	    
	    END DO
		
		!Right
		DO ip = 1,nPLine
	      NB_Point = NB_Point + 1
	      IF(NB_Point>nallc_point) THEN     
		    CALL ReAllocate_Point()            
	      END IF
          Posit(1,NB_Point) = Posit(1,NB_Point-1) 
	      Posit(2,NB_Point) = Posit(2,NB_Point-1) - delta*(alpha**(REAL(il)))
	      Posit(3,NB_Point) = Posit(3,NB_Point-1)	    
	    END DO
		
		!Bottom
		DO ip = 1,nPLine
	      NB_Point = NB_Point + 1
	      IF(NB_Point>nallc_point) THEN     
		    CALL ReAllocate_Point()            
	      END IF
          Posit(1,NB_Point) = Posit(1,NB_Point-1) - delta*(alpha**(REAL(il)))
	      Posit(2,NB_Point) = Posit(2,NB_Point-1) 
	      Posit(3,NB_Point) = Posit(3,NB_Point-1)	    
	    END DO
		
		!Left
		DO ip = 1,nPLine
	      NB_Point = NB_Point + 1
	      IF(NB_Point>nallc_point) THEN     
		    CALL ReAllocate_Point()            
	      END IF
          Posit(1,NB_Point) = Posit(1,NB_Point-1) 
	      Posit(2,NB_Point) = Posit(2,NB_Point-1) + delta*(alpha**(REAL(il)))
	      Posit(3,NB_Point) = Posit(3,NB_Point-1)	    
	    END DO
      END DO
	  
	  !Now to sort out the final end
	  NB_Point = NB_Point + 1
	  IF(NB_Point>nallc_point) THEN     
		CALL ReAllocate_Point()            
	  END IF
      Posit(1,NB_Point) = Posit(1,lastZFirst) 
	  Posit(2,NB_Point) = Posit(2,lastZFirst) 
	  Posit(3,NB_Point) = Posit(3,lastZFirst) + delta*(alpha**(REAL(il)))
	  lastZFirst = NB_Point
	  
	  DO ij = 1,nPLine
	    DO ip = 1,nPLine
	      NB_Point = NB_Point + 1
	      IF(NB_Point>nallc_point) THEN     
		    CALL ReAllocate_Point()            
	      END IF
          Posit(1,NB_Point) = Posit(1,NB_Point-1) + delta*(alpha**(REAL(il)))
	      Posit(2,NB_Point) = Posit(2,NB_Point-1) 
	      Posit(3,NB_Point) = Posit(3,NB_Point-1)	
	    END DO
		
		NB_Point = NB_Point + 1
	    IF(NB_Point>nallc_point) THEN     
		  CALL ReAllocate_Point()            
	    END IF
        Posit(1,NB_Point) = Posit(1,lastZFirst) 
	    Posit(2,NB_Point) = Posit(2,lastZFirst) - REAL(ij)*delta*(alpha**(REAL(il)))
	    Posit(3,NB_Point) = Posit(3,lastZFirst)	
		
	  END DO
	  
	  DO ip = 1,nPLine
	    NB_Point = NB_Point + 1
	    IF(NB_Point>nallc_point) THEN     
		  CALL ReAllocate_Point()            
	    END IF
        Posit(1,NB_Point) = Posit(1,NB_Point-1) + delta*(alpha**(REAL(il)))
	    Posit(2,NB_Point) = Posit(2,NB_Point-1) 
	    Posit(3,NB_Point) = Posit(3,NB_Point-1)	
	  
	  END DO

	  lastFirst = thisFirst
	  
    END DO
	
END SUBROUTINE HexPoint





























