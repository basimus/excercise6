!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


program engen_invis_unformatted

     implicit none 
     character*4 istep,fileExtension 
     character*50 groupname 
     character*80 string
     integer ::  i,j,ne,np,nbf,ie,in,ip,it,iz,nstart,nstop,nsample,is,np2
     integer :: ihex,ipri,ipyr,itet,nfaceq,nfacet,icount,itest,tmp,int 
     integer :: nsurf,test,surfno,nbp,isurf,itri
     integer, allocatable :: ielhex(:,:),ielpri(:,:),ielpyr(:,:),trino(:) 
     integer, allocatable :: ieltet(:,:),iface(:,:),iel(:,:),gloloc(:) 
     real*4 :: Mach,ss,T,gamma,uinf,press,R,cp,dens
     real*4, allocatable :: u(:,:),uabs(:,:),coor(:,:),temporary(:,:),uloc(:,:)
     real*8, allocatable :: coor8(:,:),u8(:,:),temp(:,:) 
     parameter (R=287,gamma=1.4)
     logical :: onemesh,unformatted 
write(6,*) '*******************************************************' 
write(6,*) '*********  ENSIGHT82 FILE GENERATOR   *********'
write(6,*) '*******************************************************'
write(6,*) 
write(6,*) 'Enter the filegroup name: '
read(5,'(a)') groupname
write(6,*) 'OK, reading file group: ', groupname(1:len_trim(groupname))
!write(6,*) 'Unformatted input? T/F'
!read(5,*) unformatted
!write(6,*) 'Enter the freestream Mach number:'
!read(5,*) Mach
!write(6,*) 'Enter the freestream temperature (K):'
!read(5,*) T
!write(6,*) 'Enter the freestream pressure (Pa):'
!read(5,*) press
!write(6,*) 'Enter specific heat at constant pressure, cp:'
!read(5,*) cp

 ! if(.not.unformatted) then	!start of format loop
 !   write(6,*) 'Opening the formatted .plt file: ', groupname(1:len_trim(groupname))//'.plt_f'
 !   open(10,file=groupname(1:len_trim(groupname))//'.plt_f',form='formatted',status='old')
 !   write(*,*) 'opened' 
 !   read(10,*) ne,np,nbf
 !   write(*,*) 'read in ne,np,nbf,ihex,ipri,ipyr,itet' 
 !   allocate (iel(8,ne),coor(3,np),iface(5,nbf),u(np,5),uabs(np,5))
 !   do ie=1,ne
 !     read(10) (iel(in,ie),in=1,4) 
 !   enddo
 !   do ip=1,np
 !      read(10,*) (coor8(in,ip),in=1,3)
 !   enddo
 !   do it=1,nbf
 !      read(10,*) (iface(in,it),in=1,5)
 !   enddo
 !   close(10)
 ! else	!unformatted
    write(6,*) 'Opening the unformatted .plt file: ', groupname(1:len_trim(groupname))//'.plt'
    open(10,file=groupname(1:len_trim(groupname))//'.plt',form='unformatted',status='old')
    write(*,*) 'opened' 
    read(10) ne,np,nbf
    write(*,*) 'read in ne,np,nbf' 
    write(*,*) 'ne=',ne
    write(*,*) 'np=',np
    write(*,*) 'nbf=',nbf
    allocate (ieltet(4,ne),coor(3,np),iface(5,nbf),u(np,5),uabs(np,5))
    allocate (coor8(3,np),u8(np,5),trino(nbf))
    read(10) ((ieltet(in,ie),ie=1,ne),in=1,4) 
    read(10) ((coor8(in,ip),ip=1,np),in=1,3)
    read(10) ((iface(in,it),it=1,nbf),in=1,5)
 ! endif !end of format loop
!
! Get the number of surfaces
!
 nsurf = 0 
 do i=1,nbf
   test = iface(5,i)
   if(test.gt.nsurf)nsurf=test
 enddo
 write(*,*) 'There are ', nsurf,'surfaces'
!
!if(.not.unformatted) then	!start of format loop
!  write(6,*) 'Opening the formatted .unk file: ', groupname(1:len_trim(groupname))//'_'//'.unk_f'
!  open(10,file=groupname(1:len_trim(groupname))//'_'//'.unk_f',form='formatted',status='old')
!  write(*,*) 'opened' 
!  read(10,*) np
!  write(*,*) 'np=', np
!  do j=1,4
!    read(10,*) (u8(i,j),i=1,np)
!    ! print *, 'read unknown', j
!  enddo
!  write(*,*) 'read in unknowns' 
!  close(10)
!else !unformatted
!  write(6,*) 'Opening the unformatted .unk file: ', groupname(1:len_trim(groupname))//'.unk'
!  open(10,file=groupname(1:len_trim(groupname))//'.unk',form='unformatted',status='old')
!  write(*,*) 'opened' 
!  read(10) np2
!  write(*,*) 'np in unk=',np2
!  read(10) ((u8(i,j),i=1,np),j=1,5) 
!  write(*,*) 'read in unknowns' 
!  close(10)
!endif !end of format loop
!
! convert from real*8s to real*4s
!
!   u=u8
   coor=coor8
   allocate(temporary(3,np),uloc(10,np))
!
! compute freestream variables
!
!  dens = press/(R*T)
!  write(6,*) 'freestream density =',dens
!  uinf = Mach*sqrt(gamma*R*T)
!  write(6,*) 'freestream speed =',uinf
!
! convert the unknowns from scaled variables to absolute variables
!
  !uabs(:,1) = u(:,1) * dens
  !uabs(:,2:4) = u(:,2:4) * uinf
 ! uabs(:,5) = (uinf**2/cp)*(gamma/u(:,1))*(u(:,5)-0.5*u(:,1)*sum(u(:,2:4)**2))
! uabs(:,5) = (0.4)*(u(:,5)-0.5*u(:,1)*sum(u(:,2:4)**2)/u(:,1))
  
!  open(44, file='P'//groupname(1:len_trim(groupname))//'.res',form='formatted',status='unknown')
!  do j=1,np
!	write(44,'(5E14.4)') u(j,1:5)
!  end do
!  close(44)
  
  open(45, file='M'//groupname(1:len_trim(groupname))//'.mesh',form='formatted',status='unknown')
  do j=1,np
	write(45,'(3E14.4)') coor(1,j),coor(2,j),coor(3,j)
  end do
  write(45,*) "Tets"
  do j=1,ne
	write(45,'(4I10)') ieltet(1,j),ieltet(2,j),ieltet(3,j),ieltet(4,j)
  end do
  write(45,*) "Boundaries"
  do j=1,nbf
	write(45,'(5I10)') iface(1,j),iface(2,j),iface(3,j),iface(4,j),iface(5,j)
  end do
  close(45)
  
end


