!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!                 3D Anisotropic Delaunay Mesh Generator                     \n
!!                 3D Ideal Mesh Generator   
!<
!*******************************************************************************
program main
  !$ USE omp_lib
  USE common_Parameters
  IMPLICIT NONE
  !REAL    :: TimeStart, TimeEnd
  REAL*8 :: ostart,oend

!---  licence check
  call lictest(2032,6,1)
  nallc_increase = 100000

  CALL CPU_TIME(TimeStart)
  !$ ostart = omp_get_wtime()
  !$  write(*,*) 'OpenMP Timing', ostart
  !---- Input job name and control parameters
  CALL Control_Input()

  IF(Frame_Type<=0)THEN
     CALL Job_noFrame()
  ELSE
     CALL Job_withFrame()
  ENDIF

  CALL CPU_TIME(TimeEnd)
  !$ oend = omp_get_wtime()
  WRITE(*, *) ' '
  WRITE(*, *) 'Total cpu_time: ',TimeEnd-TimeStart
  !$  write(*,*) 'OpenMP Walltime elapsed', oend-ostart


  WRITE(29,*) ' '
  WRITE(29,*) 'Total cpu_time: ',TimeEnd-TimeStart
  !$  write(29,*) 'OpenMP Walltime elapsed', oend-ostart
  WRITE(*,*) '---- Successfully ----'
  CALL WellDone_Stop()


END program main


