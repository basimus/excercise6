!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>     
!!    Build array common_Parameters::NEXT_Tet.
!!
!!    @param[in] IsolatedSheet = .true.  : do not treat two elements beside 
!!                                         a sheet triangle as neighbours.    \n
!!                             = .false, : ignore sheet surface.
!<     
!*******************************************************************************
SUBROUTINE Next_Build( )

  USE common_Constants
  USE common_Parameters
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: IFace, IT, Msize, IB
  INTEGER :: i,ipp(4),ip3(3), ii(2)
  INTEGER, DIMENSION(:,:), POINTER :: TetFace  
  TYPE(NodeNetTreeType) :: Face_Tree

  Msize = NB_Tet*4

  ALLOCATE(TetFace(2,Msize))
  CALL NodeNetTree_allocate(3, NB_Point+NB_Extra_Point, Msize, Face_Tree)
  TetFace(:,:)  = 0
  NEXT_Tet(:,:) = 0
  
  IF(IsolatedSheet)THEN
     DO IB = 1, NB_BD_Tri
        IF(IP_BD_Tri(5,IB) <= Surf%NB_Surf - Surf%NB_Sheet) CYCLE
        ip3(1:3) =IP_BD_Tri(1:3,IB)
        CALL NodeNetTree_SearchAdd( 3,ip3,Face_Tree,IFace)
        TetFace(1,IFace) = -1
     ENDDO
  ENDIF
  
  DO IT=1,NB_Tet
     ipp(:) = IP_Tet(:,IT)
     DO i=1,4
        ip3(1:3) = ipp(iTri_Tet(1:3,i))
        !--- in case extra points lead too many triangles.
        where(ip3(:)<=NB_Extra_Point) ip3(:) = ip3(:) + NB_Point

        CALL NodeNetTree_SearchAddRemove( 3,ip3,Face_Tree,IFace)

        ii(:)  = TetFace(:,ABS(IFace))
        IF(IFace>0 .AND. ii(1)==0)THEN
           !--- a new Face
           TetFace(1,IFace) = IT
           TetFace(2,IFace) = i
           NEXT_Tet(i,IT) = -1
        ELSE IF(IFace<0 .AND. ii(1)>0)THEN
           !--- a old Face
           NEXT_Tet(i,IT) = ii(1)
           NEXT_Tet(ii(2),ii(1)) = IT
           TetFace(:,ABS(IFace)) = 0
        ELSE IF(IFace<0 .AND. ii(1)==-1)THEN
           !--- a sheet triangle
           NEXT_Tet(i,IT) = -1
           TetFace(:,ABS(IFace)) = 0
        ELSE
           WRITE(29,*)'Error--- : IFace=',IFace
           WRITE(29,*)' IT=',IT, ' ii=', ii
           WRITE(29,*)' IP3: ',ip3
           CALL Error_Stop ('Next_Build')
        ENDIF
     ENDDO
  ENDDO

  deallocate ( TetFace )
  CALL NodeNetTree_clear ( Face_Tree )

END SUBROUTINE Next_Build

!*******************************************************************************
!>
!!  Find the proper location j and set common_Parameters::NEXT_Tet (j,ITnb) = ITet.
!!  @param[in] ITnb,ITet  the IDs of two neighbouring elements.
!<
!*******************************************************************************
SUBROUTINE Match_Next(ITnb,ITet)

  USE common_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: ITnb,ITet
  INTEGER :: j

  IF(ITnb<=0) RETURN
  DO j=1,4
     IF(  IP_Tet(j,ITnb) /= IP_Tet(1,ITet) .AND.     &
          IP_Tet(j,ITnb) /= IP_Tet(2,ITet) .AND.     &
          IP_Tet(j,ITnb) /= IP_Tet(3,ITet) .AND.     &
          IP_Tet(j,ITnb) /= IP_Tet(4,ITet) )THEN
        NEXT_Tet(j,ITnb) = ITet
        EXIT
     ENDIF
  ENDDO
  IF(j>4)THEN
     WRITE(29,*)' '
     WRITE(29,*)' Error ---- fail to match next'
     WRITE(29,*)'     IT1=',ITnb, ' ip= ',IP_Tet(:,ITnb)
     WRITE(29,*)'     IT2=',ITet, ' ip= ',IP_Tet(:,ITet)
     CALL Error_Stop (' Match_Next')
  ENDIF

  RETURN
END SUBROUTINE Match_Next

!*******************************************************************************
!>
!!  If common_Parameters::NEXT_Tet (j,ITnb) == ITold,
!!     set common_Parameters::NEXT_Tet (j,ITnb) = ITnew
!<     
!*******************************************************************************
SUBROUTINE Match_Next_update(ITnb,ITold,ITnew)

  USE common_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: ITnb, ITold, ITnew
  INTEGER :: j

  IF(ITnb<=0) RETURN
  DO j=1,4
     IF(NEXT_Tet(j,ITnb) == ITold)THEN
       NEXT_Tet(j,ITnb) = ITnew
       RETURN
     ENDIF
  ENDDO

  WRITE(29,*)'Error--- fail to match : ITnb,ITold,ITnew=',ITnb,ITold,ITnew
  CALL Error_Stop ('Match_Next_update')
END SUBROUTINE Match_Next_update


!*******************************************************************************
!>     
!!  Search for the tetrahedra that are adjacent to the edge (ind) of element (IT)                  \n
!!  Return these elements in Pole_Surround::IT_PoleTet
!!         and their extra points in Pole_Surround::IP_PolePt.                \n
!!  @param[in]  IT   : the element.
!!  @param[in]  ind  : the edge number of the cell.
!!                     See CellConnectivity::I_Comb_Tet.
!!  @param[in] common_Parameters::NEXT_Tet  : updated neighbouring system.
!<     
!*******************************************************************************
SUBROUTINE Search_Pole_Surround(IT, ind)

  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: IT, ind
  INTEGER :: ipo1, ipo2, ip1, ip2, ix1, ix2, itnb, i, ndirect

  ipo1 = IP_Tet(I_Comb_Tet(1,ind),IT)
  ipo2 = IP_Tet(I_Comb_Tet(2,ind),IT)
  ix1 = I_Comb_Tet(3,ind)
  ix2 = I_Comb_Tet(4,ind)
  ip1 = IP_Tet(ix1,IT)
  ip2 = IP_Tet(ix2,IT)

  NB_PoleTet    = 1
  IT_PoleTet(1) = IT
  NB_PolePt     = 2
  IP_PolePt(1)  = ip1
  IP_PolePt(2)  = ip2

  ndirect = 0
  DO WHILE(ndirect<=1)

     itnb = NEXT_Tet(ix1, IT)
     DO WHILE(itnb>0)
        NB_PoleTet = NB_PoleTet+1
        IF(NB_PoleTet>=Max_PoleTet)THEN
           WRITE(29,*)'Error--- : NB_Tet=',NB_Tet
           WRITE(29,*)'Reset Max_PoleTet with a larger number please.'
           CALL Check_Next(0)
           CALL Error_Stop ('Search_Pole_Surround')
        ENDIF
        IT_PoleTet(NB_PoleTet) = itnb
        DO i=1,4
           IF(IP_Tet(i,itnb)==ipo1)CYCLE
           IF(IP_Tet(i,itnb)==ipo2)CYCLE
           IF(IP_Tet(i,itnb)==ip2)THEN
              ix1 = i
           ELSE
              ix2 = i
           ENDIF
        ENDDO
        ip2 = IP_Tet(ix2,itnb)

        itnb = NEXT_Tet(ix1, itnb)
        IF(itnb==IT) RETURN

        NB_PolePt            = NB_PolePt+1
        IP_PolePt(NB_PolePt) = ip2
     ENDDO

     IF(itnb<=0)THEN
        ndirect = ndirect+1
        ix1 = I_Comb_Tet(4,ind)
        ix2 = I_Comb_Tet(3,ind)
        ip1 = IP_Tet(ix1,IT)
        ip2 = IP_Tet(ix2,IT)
     ENDIF

  ENDDO

  RETURN

END SUBROUTINE Search_Pole_Surround

!*******************************************************************************
!>     
!!  Find the tetrahedra that are adjacent to the edge IP1-IP2                               \n
!!  Return these elements in Pole_Surround::IT_PoleTet
!!         and their extrta points in Pole_Surround::IP_PolePt.
!!  @param[in]  IP1,IP2  : the two nodes composing the edge.
!!  @param[in] common_Parameters::PointAsso
!!                       :  built by calling LinkAssociation_Build()
!!  @param[in] common_Parameters::NEXT_Tet  : updated neighbouring system.
!<     
!*******************************************************************************
SUBROUTINE Search_Pole_Surround2(IP1,IP2)

  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: IP1,IP2
  INTEGER :: IT, ind, iie

  !--- List all elements surrounding point IP1 in the array ITs_List
  CALL LinkAssociation_List(IP1, List_Length, ITs_List, PointAsso)

  Loop_iie : DO iie = 1 , List_Length
     IT = ITs_List(iie)
     IF(  IP_Tet(1,IT)/=IP2 .AND. IP_Tet(2,IT)/=IP2 .AND.   &
          IP_Tet(3,IT)/=IP2 .AND. IP_Tet(4,IT)/=IP2) CYCLE Loop_iie

     DO ind = 1,12
        IF(  IP_Tet(I_Comb_Tet(1,ind),IT)==IP1 .AND.    &
             IP_Tet(I_Comb_Tet(2,ind),IT)==IP2) EXIT
     ENDDO

     IF(ind>12)THEN
        WRITE(29,*)'Error--- : IP1,IP2,IPs=',IP1,IP2,IP_Tet(:,IT)
        CALL Error_Stop ('Search_Pole_Surround2')
     ENDIF

     EXIT Loop_iie
  ENDDO Loop_iie

  IF(iie>List_Length)THEN
     WRITE(29,*)'Error--- : no edge found for IP1,IP2,=',IP1,IP2
     CALL Error_Stop ('Search_Pole_Surround2')
  ENDIF

  CALL Search_Pole_Surround(IT, ind)

  RETURN
END SUBROUTINE Search_Pole_Surround2

!*******************************************************************************
!>     
!!  Find the tetrahedra that are adjacent to the edge IP1-IP2 
!!         (Added in Sep. 2011. More test in need.)                           \n
!!  Return these elements in Pole_Surround::IT_PoleTet
!!         and their extrta points in Pole_Surround::IP_PolePt.
!!  @param[in]  IP1,IP2  : the two nodes composing the edge.
!!  @param[in] common_Parameters::PointAsso
!!                       :  built by calling LinkAssociation_Build()
!<     
!*******************************************************************************
SUBROUTINE Search_Pole_Surround3(IP1,IP2)

  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: IP1,IP2
  INTEGER :: IT, ind, iie, IP3, i, j

  !--- List all elements surrounding point IP1 in the array ITs_List
  CALL LinkAssociation_List(IP1, List_Length, ITs_List, PointAsso)

  NB_PoleTet = 0
  DO iie = 1 , List_Length
     IT = ITs_List(iie)
     IF(  IP_Tet(1,IT)/=IP2 .AND. IP_Tet(2,IT)/=IP2 .AND.   &
          IP_Tet(3,IT)/=IP2 .AND. IP_Tet(4,IT)/=IP2) CYCLE

     NB_PoleTet  = NB_PoleTet + 1
     IT_PoleTet(NB_PoleTet) = IT
  ENDDO

  IT = IT_PoleTet(1)
  DO ind = 1,12
     IF(  IP_Tet(I_Comb_Tet(1,ind),IT)==IP1 .AND.    &
          IP_Tet(I_Comb_Tet(2,ind),IT)==IP2) EXIT
  ENDDO
  IP3 = IP_Tet(I_Comb_Tet(3,ind),IT)
  NB_PolePt     = 1
  IP_PolePt(1)  = ip3
  IP3 = IP_Tet(I_Comb_Tet(4,ind),IT)

  DO iie = 1, NB_PoleTet-1
     DO j = iie+1, NB_PoleTet
        IT = IT_PoleTet(j)
        IF(which_NodeinTet(IP3, IP_Tet(:,IT))>0) THEN
           IT_PoleTet(j) = IT_PoleTet(iie+1)
           IT_PoleTet(iie+1) = IT
           NB_PolePt     = NB_PolePt +1
           IP_PolePt(NB_PolePt)  = ip3
           DO i = 1,4
              IF(IP_Tet(i,IT)==IP1) CYCLE
              IF(IP_Tet(i,IT)==IP2) CYCLE
              IF(IP_Tet(i,IT)==IP3) CYCLE
              EXIT              
           ENDDO
           IP3 = IP_Tet(i,IT)
           EXIT
        ENDIF
        IF(j==NB_PoleTet)THEN
           WRITE(*, *)' Error---- broken chain for IP1,IP2=',IP1,IP2
           WRITE(29,*)' Error---- broken chain for IP1,IP2=',IP1,IP2
           CALL Error_STOP ('Search_Pole_Surround3:: broken chain 1')
        ENDIF
     ENDDO
  ENDDO
  
  IF(NB_PolePt/=NB_PoleTet .OR. IP3/=IP_PolePt(1))THEN
     WRITE(*, *)' Error---- broken chain for IP1,IP2=',IP1,IP2
     WRITE(29,*)' Error---- broken chain for IP1,IP2=',IP1,IP2
     WRITE(29,*)'   NB_PolePt,NB_PoleTet,...=',NB_PolePt,NB_PoleTet,IP3,IP_PolePt(1)
     CALL Error_STOP ('Search_Pole_Surround3:: broken chain 2')
  ENDIF

  RETURN
END SUBROUTINE Search_Pole_Surround3


!*******************************************************************************
!>
!!    Check the array common_Parameters::NEXT_Tet.
!!    @param[in] nbo = -5    compare each boundary triangle with Surf%IP_Tri. \n
!!                   = -4    compare each boundary triangle with IP_BD_Tri.   \n
!!                   = -3    compare the number of boundary with initial box. \n
!!                   = -2    compare the number of boundary with Surf%NB_Tri. \n
!!                   = -1    compare the number of boundary with NB_BD_Tri.   \n
!!                   =  0    compare the number of boundary with nothing.     \n
!!                   >  0    compare the number of boundary with nbo
!!                           (counting common_Parameters::NEXT_Tet<0 to
!!                            get number of boundary triangles).
!<     
!*******************************************************************************
SUBROUTINE Check_Next(nbo)

  USE common_Constants
  USE common_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: nbo
  INTEGER :: it,i,i1,i2,ii,itnb,ib,Nouts,Iwarn, ip5(5),ip3(3)
  TYPE(NodeNetTreeType) :: Face_Tree

  IF(nbo==-5)THEN
     CALL NodeNetTree_Build (3, Surf%NB_Tri, Surf%IP_Tri(1:3,:), NB_Point,  Face_Tree)
  ELSE IF(nbo==-4)THEN
     CALL NodeNetTree_Build (3, NB_BD_Tri, IP_BD_Tri(1:3,:), NB_Point,  Face_Tree)
  ENDIF

  DO it=1,NB_Tet
     IF(IP_Tet(4,IT)<=0) CYCLE
     DO i=1,4
        IF( NEXT_Tet(i,it)==0 .or. NEXT_Tet(i,it)>NB_Tet .or.    &
            NEXT_Tet(i,it)<=-2 )THEN
           WRITE(29,*)'Error--- : next is out of the range'
           WRITE(29,*)'  IT=',IT,'  IP=',IP_Tet(:,IT)
           WRITE(29,*)'  Next=',NEXT_Tet(:,it), ' NB_Tet=',NB_Tet
           CALL Error_Stop ('Check_Next:: range')
        ENDIF
     ENDDO
  ENDDO


  Nouts = 0
  Iwarn = 0

  DO it=1,NB_Tet
     IF(IP_Tet(4,IT)<=0) CYCLE
     DO i=1,4
        itnb = NEXT_Tet(i,it)
        IF(itnb<=0)THEN
           Nouts = Nouts+1
           IF(nbo==-5 .or. nbo==-4)THEN
              ip3(1:3) = IP_Tet(iTri_Tet(1:3,i),IT)
              CALL NodeNetTree_Search( 3,ip3,Face_Tree,ii)
              IF(ii==0)THEN
                 WRITE(29,*)'Error--- boundary triangle is not real'
                 WRITE(29,*)'it,  ip, i=',it,IP_Tet(:,it), i
                 CALL Error_Stop ('Check_Next:: invalid element')
              ENDIF
           ENDIF
           CYCLE
        ENDIF

        IF(IP_Tet(4,itnb)<=0)THEN
           WRITE(29,*)'Error--- Next to an invalid element'
           WRITE(29,*)'it,  ip,next=',it,IP_Tet(:,it),NEXT_Tet(:,it)
           WRITE(29,*)'itnb,ip =',itnb,IP_Tet(:,itnb)
           CALL Error_Stop ('Check_Next:: invalid element')
        ENDIF

        ii=0
        DO i1=1,4
           DO i2=1,4
              IF(IP_Tet(i1,it)==IP_Tet(i2,itnb))THEN
                 ii=ii+1
                 EXIT
              ENDIF
           ENDDO
        ENDDO

        i2 = 0
        DO i1=1,4
           IF(NEXT_Tet(i1,itnb)==it) i2 = i1
        ENDDO

        IF(ii/=3 .OR. i2==0)THEN
           WRITE(29,*)'Error--- '
           WRITE(29,*)'it  ',it,  ' ip ',IP_Tet(:,it),  ' next ',NEXT_Tet(:,it)
           WRITE(29,*)'itnb',itnb,' ip ',IP_Tet(:,itnb),' next ',NEXT_Tet(:,itnb)
           CALL Error_Stop ('Check_Next')
        ENDIF

        IF(Iwarn==1) CYCLE

        ip5(1:3) = IP_Tet(iTri_Tet(1:3,i),IT)
        ip5(4:5) = ip5(1:2)

        ip3(1:3) = IP_Tet(iTri_Tet(1:3,i2),itnb)
        DO i1=0,2
           IF( ip5(i1+1)==ip3(2) .AND. ip5(i1+2)==ip3(1)    &
                .AND. ip5(i1+3)==ip3(3) )THEN
              i2 = 0
              EXIT
           ENDIF
        ENDDO

        IF(i2>0)THEN
           Iwarn = 1
           WRITE(*, *)'Warning--- Check_Next: NEXT_Tet is not in order'
           WRITE(29,*)'Warning--- Check_Next: NEXT_Tet is not in order'
           WRITE(29,*)'it,  ip,next=',it,IP_Tet(:,it),NEXT_Tet(:,it)
           WRITE(29,*)'itnb,ip,next=',itnb,IP_Tet(:,itnb),NEXT_Tet(:,itnb)
        ENDIF
     ENDDO
  ENDDO

  IF(nbo==-3)then
     ii = 12*Initial_Split**2
  ELSE IF(nbo==-2 .OR. nbo==-5)then
     ii = 0
     DO ib = 1, Surf%NB_Tri
        IF(Surf%IP_Tri(5,ib)>Surf%NB_Surf-Surf%NB_Sheet) ii = ii + 1
     ENDDO
     IF(IsolatedSheet)THEN
        ii = Surf%NB_Tri + ii
     ELSE
        ii = Surf%NB_Tri - ii
     ENDIF
  ELSE IF(nbo==-1 .OR. nbo==-4)then
     ii = 0
     DO ib = 1, Surf%NB_Tri
        IF(IP_BD_Tri(5,ib)>Surf%NB_Surf-Surf%NB_Sheet) ii = ii + 1
     ENDDO
     IF(IsolatedSheet)THEN
        ii = NB_BD_Tri + ii
     ELSE
        ii = NB_BD_Tri - ii
     ENDIF
  ELSE IF(nbo>=0)THEN
     ii = nbo
  ENDIF

  IF(ii>0 .AND. Nouts/=ii)THEN
     WRITE(29,*)'Error--- wrong with boundary triangles counting.'
     WRITE(29,*)'nbo,ii,Nouts=',nbo,ii,Nouts
     CALL Error_Stop ('Check_Next:: boundary counting')
  ELSE IF(ii==0)THEN
     WRITE(*, *)'No. of boundary triangles by counting NEXT_Tet :',Nouts
     WRITE(29,*)'No. of boundary triangles by counting NEXT_Tet :',Nouts
  ENDIF
  
  IF(nbo==-5 .or. nbo==-4)THEN
     CALL NodeNetTree_Clear( Face_Tree)
  ENDIF           

  RETURN

END SUBROUTINE Check_Next
