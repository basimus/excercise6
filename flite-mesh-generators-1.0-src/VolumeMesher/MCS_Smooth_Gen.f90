!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  MCS_Smooth_Gen
!!  
!!  This routine smooths nodes in the mesh to try and match the correct node face distance
!!
!!  
!!
!!  Initial implimentation by S. Walton 26/03/1013
!!
!<
!*******************************************************************************
SUBROUTINE MCS_Smooth_Gen(NB1)
	USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	
	USE Geometry3DAll
	
	INTEGER, INTENT(IN)  :: NB1  !The first point effected by this process
	INTEGER :: K,ie,ip,DD,N,Ibest,I,Loop,Istatus,Iin,Ni,Di,idum,isFinished,numLoops,ic,flag,nbp,improve,NB_old,MaxL,L,j,ii
	
	REAL*8 :: dd2,ftemp,fbest,ranNum,d,FTarget,FBig,p1(3),dtet,psave(3),rsave,vol,ptet(3,4),p2(3),p3(3),p4(3)
	REAL*8 :: pp4(3,4),dum(4),coef,sumObj,lastObj,p1t(3),p2t(3),residual,delta2,dist,sqrt5o4,ptemp(3)
	REAL*8 :: delta,length,pC(3),pProj(3),pTarg(3)
	INTEGER :: ITnb,merged,Isucc,is,j1,j2,j3,j4
	REAL*8 :: dum1,conv,dum2
	REAL*8 :: minl, maxll, templ, aspectRatio
	REAL*8 :: dimi,diFact,targDi, sqrt1o2
	INTEGER :: iFace, iF1, iF2, iF3, iPf
	TYPE(Plane3D) :: aPlane
	OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
      READ(84,*) dum1
      READ(84,*) conv
	  READ(84,*) dum2
      CLOSE(84)
	
	
	sqrt1o2 = 1.0d0/(sqrt(2.0d0))
	
	lastObj = HUGE(0.d0)
	
	sqrt5o4 = sqrt(5.d0)/4.d0
	targDi = 30.0d0
	diFact = 1.d0/(targDi)
	
	improve = 1
	MaxL = 100
	L = 0
	
	!Get link info
	CALL Next_Build()
	!Make sure mapping and geometry is all up to date
	CALL Get_Tet_SphereCross(0)
	CALL Get_Tet_Volume(0)
	CALL Set_Domain_Mapping()
	
	
	DO WHILE((improve.EQ.1).AND.(L<MaxL))
		
		FTarget = (1.D-6)*BGSpacing%MinSize
		FBig = 1.D30
		idum = -1
		sumObj = 0.d0
		
		
		CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
		
		NB_old = NB_Point
		
		!Loop through nodes
		DO ip = NB1, NB_Point

			!Get connected elements
			CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
					
			
			ptemp(1:3) = 0.0d0
					
			DO ic = 1,List_Length
					
				ie = ITs_List(ic)	
				
			
				iFace = 1
				DO j = 1,4
				  IF (ip.EQ.IP_Tet(j,ie)) THEN
					iFace = j
				  ENDIF
				ENDDO
				iF1 = IP_Tet(iTri_Tet(1,iFace),ie)
				iF2 = IP_Tet(iTri_Tet(2,iFace),ie)
				iF3 = IP_Tet(iTri_Tet(3,iFace),ie)
				p1 = Posit(:,ip)
				p2 = Posit(:,iF1)
				p3 = Posit(:,iF2)
				p4 = Posit(:,iF3)
				aPlane = Plane3D_Build(p2,p3,p4)
				!Project to plane
				pProj = Plane3D_ProjectOnPlane(p1,aPlane)
				!Get the *signed* distance
				dist = Plane3D_SignedDistancePointToPlane(p1,aPlane)
				delta2 = Scale_Tet(ie)*BGSpacing%BasicSize*sqrt1o2
				
				!Dist/Dist^3 gives us a d^2 on bottom and correct sign on top
				
				pTarg(1:3) = pProj(1:3) + ((delta2*dist)/(dist*dist*dist))*(p1(1:3)-pProj(1:3))
				
				ptemp(1:3) = ptemp(1:3)+pTarg(1:3)
								
					
			ENDDO
					
			ptemp(1:3) = ptemp(1:3)/REAL(List_Length)
			
			sumObj = sumObj + Geo3D_Distance_SQ(Posit(:,ip),ptemp(:))
			Posit(:,ip) = Posit(:,ip) + 0.1d0*(ptemp(:)-Posit(:,ip))
				
		ENDDO
		
		!Reinsert
		nbp = NB1-1
		
		CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
		CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
		CALL Next_Build()
		CALL Set_Domain_Mapping()
		CALL Get_Tet_SphereCross(0)
		CALL Get_Tet_Volume(0)
		Mark_Point(1:NB_Point) = 0
		CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
		usePower = .FALSE.
		CALL Remove_Point()
		CALL Next_Build()
		CALL Set_Domain_Mapping()
        CALL Get_Tet_SphereCross(0)
        CALL Get_Tet_Volume(0)



		residual = ABS(1.d0 - (lastObj/sumObj))
		IF(Debug_Display>0)THEN
			
			WRITE(*,*)'MCS Optim Objective funtion value:',sumObj,residual
			WRITE(29,*)'MCS Optim Objective funtion value:',sumObj,residual
		END IF
		
		IF (residual>conv) THEN
			lastObj = sumObj
			!deallocate(allPsave)
			L = L+1
		ELSE
		!	nbp = NB1-1
		!	CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
		!	CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
			
		!	Posit(:,1:NB_Old) = allPsave(:,1:NB_Old)
         !               NB_Point = NB_Old

		!	CALL Next_Build()
		!	CALL Set_Domain_Mapping()
		!	CALL Get_Tet_SphereCross(0)
		!	CALL Get_Tet_Volume(0)
		!	Mark_Point(1:NB_Point) = 0
		!	CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
		!	usePower = .FALSE.
		!	CALL Remove_Point()
		!	CALL Next_Build()
         !                CALL Get_Tet_SphereCross(0)
         !       CALL Get_Tet_Volume(0)


			improve = 0
			EXIT
		
		ENDIF

	
	ENDDO


END SUBROUTINE MCS_Smooth_Gen




























