!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!     Read a volume mesh from file *_0.plt
!<       
!*******************************************************************************
SUBROUTINE Read_Volume( )

  USE common_Parameters
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: i, IT, IB, IP, ip1, ip2, ipp(3), nbn, NB1, NB2, ierr
  LOGICAL :: ex
    TYPE(HybridMeshStorageType)  :: aMesh
  

  INQUIRE(file = JobName(1:JobNameLength)//'_0.plt', EXIST=ex)
  IF(.NOT. ex)THEN
     WRITE(29,*) 'Error--- the file '//JobName(1:JobNameLength)//'_0.plt does NOT exist.'
     WRITE(*, *) 'Error--- the file '//JobName(1:JobNameLength)//'_0.plt does NOT exist.'
     CALL Error_Stop( ' Read_Volume ::'  )
  ENDIF
  
  CALL HybridMeshStorage_Input(JobName(1:JobNameLength)//'_0',JobNameLength+2,-15,aMesh,Surf)
  GridOrder = aMesh%GridOrder
  NB_Tet    = aMesh%NB_Tet 
  NB_Point  = aMesh%NB_Point
  NB_BD_Tri =  Surf%NB_Tri

  WRITE(*, *) ' NB_Tet,NB_Point,NB_BD_Tri= ',NB_Tet,NB_Point,NB_BD_Tri
  WRITE(29,*) ' NB_Tet,NB_Point,NB_BD_Tri= ',NB_Tet,NB_Point,NB_BD_Tri

  nallc_Point = NB_Point + nallc_increase
  nallc_Tet   = NB_Tet + nallc_increase
  numTetNodes = Npoint_Tet(GridOrder)

  CALL Allocate_Point( )
  CALL Allocate_Tet( )
  CALL allc_2Dpointer(IP_BD_Tri, 5,NB_BD_Tri,  'IP_BD_Tri')

  IP_Tet(:,1:NB_Tet)       = aMesh%IP_Tet(:,1:NB_Tet)
  Posit(:, 1:NB_Point)     = aMesh%Posit(:, 1:NB_Point)
  IP_BD_Tri(:,1:NB_BD_Tri) =  Surf%IP_Tri(:,1:NB_BD_Tri)


  Surf%NB_Seg   = 0
  Surf%NB_Sheet = 999999
  Surf%NB_Surf  = 0

  INQUIRE(file = JobName(1:JobNameLength)//'_Ini.bmark', EXIST=ex)
  IF(ex)THEN
     OPEN(1, file=JobName(1:JobNameLength)//'_Ini.bmark', form='FORMATTED')
     READ(1,*)nbn
     CALL allc_1Dpointer(Mark_Surf_Point, nbn)
     READ(1,*)nb1, Mark_Surf_Point(1)
     nb1 = 1
     DO WHILE(nb1<nbn)
       READ(1,*,IOSTAT=ierr)nb2, Mark_Surf_Point(nb2)
       IF(ierr<0) nb2 = nbn+1
       Mark_Surf_Point(nb1+1:nb2-1) = Mark_Surf_Point(nb1)
       nb1 = nb2
     ENDDO
     CLOSE (1)
  ENDIF

  !--- resort the points so that
  !    the first NB_BD_Point nodes are the boundary nodes.

  Mark_Point(1:NB_Point) = 0
  DO IB = 1,NB_BD_Tri
     Mark_Point(IP_BD_Tri(1:3,IB)) = -1
     Surf%NB_Surf = MAX(Surf%NB_Surf,IP_BD_Tri(5,IB))
     IF(IP_BD_Tri(4,IB)<0)THEN
        Surf%NB_Sheet = MIN(Surf%NB_Sheet,IP_BD_Tri(5,IB))
     ENDIF
  ENDDO

  IF(Surf%NB_Sheet > Surf%NB_Surf)THEN
     Surf%NB_Sheet = 0
  ELSE
     Surf%NB_Sheet = Surf%NB_Surf - Surf%NB_Sheet + 1
  ENDIF

  NB_Extra_Point = -1
  nbn = -1
  Surf%NB_Point = 0
  do ip = 1,NB_Point
     if(Mark_Point(ip)==-1) Surf%NB_Point = Surf%NB_Point +1
     if(NB_Extra_Point==-1)then
        if(Mark_Point(ip)==0) cycle
        NB_Extra_Point = ip-1
     else if(nbn==-1)then
        if(Mark_Point(ip)==-1) cycle
        nbn = ip-1
     endif
  enddo

  if(nbn==-1) nbn = NB_Point

  IF(Recovery_Time>0 .AND. Recovery_Method>0)THEN
     if(NB_Extra_Point<8 .or. nbn/=Surf%NB_Point + NB_Extra_Point)then
        WRITE(29,*)'Error--- boundary points counting'
        print*,'NB_Extra_Point,nbn,Surf%NB_Point=',NB_Extra_Point,nbn,Surf%NB_Point
        if(NB_Extra_Point==0)THEN
           WRITE(29,*)'Hint: the input mesh is already boundary-recovered,'
           WRITE(29,*)'      so set Recovery_Method=0 before re-read it.'
        ENDIF
        CALL Error_Stop (' Read_Volume  ')
     endif
  ELSE
     CALL Sort_Nodes(NB1,NB2)
     NB_BD_Point = NB1
     NB_Extra_Point = 0
  endif

  IF(NB_Extra_Point/=0)THEN
  DO IB = 1,NB_BD_Tri
     Surf%IP_Tri(1:3,IB) = Surf%IP_Tri(1:3,IB) - NB_Extra_Point
  ENDDO
  ENDIF

  NB_BD_Point = Surf%NB_Point + NB_Extra_Point

  WRITE(29,*)'Read_Volume:: Surf%NB_Point,NB_Point,NB_Extra_Point=',Surf%NB_Point,NB_Point,NB_Extra_Point

  CALL NEXT_Build()

  IF (Element_Break_Method==11) THEN
  ELSE
    IF(Debug_Display>0)THEN
       IF(NB_Extra_Point==0)THEN
          CALL Check_Next(-1)
       ELSE
          CALL Check_Next(0)
       ENDIF
    ENDIF
  END IF


  !---- Measure the domain

  XYZmax(1:3) = Posit(1:3,NB_Extra_Point+1)
  XYZmin(1:3) = Posit(1:3,NB_Extra_Point+1)
  DO IP = NB_Extra_Point+2,NB_Point
     DO i=1,3
        XYZmax(i) = MAX(XYZmax(i),Posit(i,IP))
        XYZmin(i) = MIN(XYZmin(i),Posit(i,IP))
     ENDDO
  ENDDO
  XYZmax(1:3) = XYZmax(1:3) + BGSpacing%BasicSize
  XYZmin(1:3) = XYZmin(1:3) - BGSpacing%BasicSize

  IF(ABS(Background_Model)==1)THEN
     WRITE(*,*)' Error--- why Background_Model===1?'
     CALL Error_Stop (' Read_Volume:: ')
  ENDIF
!  IF(Background_Model==1)  CALL Surface_Scale()
!  IF(Background_Model==-1) CALL Surface_Stretch()

  useStretch = .TRUE.
  CircumUpdated = .FALSE.
  VolumeUpdated = .FALSE.

  RETURN

110 WRITE(29,*) 'Error in openning file ',JobName(1:JobNameLength)//'_0.plt'
   CALL Error_Stop ('Read_Volume  ')
111 WRITE(29,*) 'Error in reading file ',JobName(1:JobNameLength)//'_0.plt'
   CALL Error_Stop (' Read_Volume ')

END SUBROUTINE Read_Volume

!*******************************************************************************
!>
!!     Read a volume mesh from file *_0.plt
!<       
!*******************************************************************************
SUBROUTINE Read_FillingMesh( )

  USE common_Parameters
  IMPLICIT NONE

  INTEGER :: IP
  LOGICAL :: ex

  INQUIRE(file = JobName(1:JobNameLength)//'_1.plt', EXIST=ex)
  IF(.NOT. ex)THEN
     WRITE(29,*) 'Error--- the file '//JobName(1:JobNameLength)//'_1.plt does NOT exist.'
     WRITE(*, *) 'Error--- the file '//JobName(1:JobNameLength)//'_1.plt does NOT exist.'
     CALL Error_Stop( ' Read_FillingMesh ::'  )
  ENDIF

  IF(1==0)THEN
  OPEN(1,file=JobName(1:JobNameLength)//'_1.plt', form='FORMATTED')

  READ(1,*) FillingNodes%NB_Point

  ALLOCATE(FillingNodes%Posit(3,FillingNodes%NB_Point))
  DO IP = 1, FillingNodes%NB_Point
    READ(1,*) FillingNodes%Posit(:,IP)
  ENDDO

  CLOSE(1)
  
  ELSE
  
     CALL TetMeshStorage_Input (JobName(1:JobNameLength)//'_1', JobNameLength+2, FillingNodes)

  ENDIF

  RETURN

END SUBROUTINE Read_FillingMesh

!*******************************************************************************
!>
!!     Output the volume mesh to file *.plt
!<       
!*******************************************************************************
SUBROUTINE Output_Volume()

  USE common_Parameters
  USE Mapping3D
  IMPLICIT NONE

  INTEGER :: i, IT, IB, IP, Isucc, j, numCellNodes
  REAL*8  :: strs, A(3), D(3,3)
  REAL*8, DIMENSION(:,:),ALLOCATABLE  :: str
  REAL*8  :: dir(3,3) = RESHAPE    &
            ( (/1.d0,0.d0,0.d0,  0.d0,1.d0,0.d0,  0.d0,0.d0,1.d0/) , (/3,3/) )
  REAL*8  :: diss(3)

  OPEN(1,file=JobName(1:JobNameLength)//'.plt',form='UNFORMATTED',err=110)
  WRITE(1,err=112) NB_Tet,NB_Point,NB_BD_Tri, 1
  numCellNodes  = 4
  WRITE(*,*)'NB_Tet,NB_Point,NB_BD_Tri='
  WRITE(*,*) NB_Tet,NB_Point,NB_BD_Tri
  WRITE(1,err=112)((IP_Tet(i,IT),IT=1,NB_Tet),i=1,numCellNodes)
  WRITE(1,err=112)((Posit(i,IP),IP=1,NB_Point),i=1,3)
  WRITE(1,err=112)((IP_BD_Tri(i,IB),IB=1,NB_BD_Tri),i=1,5)
  CLOSE(1)

  IF(BGSpacing%Model<-1 .AND. 1==0)THEN
     !--- output max & min gridsizes and 3 those alone x-, y- & z- dreictions
     ALLOCATE(str(6,NB_point))
     DO IP = 1,NB_Point
        DO i=1,3
           diss = Mapping3D_Posit_Transf(dir(:,i), fMap_Point(:,:,IP), 1)
           str(i,IP) = BGSpacing%BasicSize / dsqrt(diss(1)**2 + diss(2)**2 + diss(3)**2)
        ENDDO
        CALL Mapping3D_Decompose(fMap_Point(:,:,IP),A, D)
        str(4,IP) = BGSpacing%BasicSize / min(A(1),A(2),A(3))
        str(5,IP) = BGSpacing%BasicSize / max(A(1),A(2),A(3))
        str(6,IP) = 0
     ENDDO

     OPEN(2,file=JobName(1:JobNameLength)//'.unk', status='unknown',   &
          form='UNFORMATTED',err=110)
     WRITE(2,err=112)NB_Point,6
     WRITE(2,err=112)((str(i,IP),IP=1,NB_Point), i=1,6)
     CLOSE (2)

     DEALLOCATE(str)
  ELSE IF(BGSpacing%Model>1 .AND. 1==0)THEN
     OPEN(2,file=JobName(1:JobNameLength)//'.unk', status='unknown',   &
          form='UNFORMATTED',err=110)
     WRITE(2,err=112)NB_Point,6       !--- xplot needs 6 components?
     WRITE(2,err=112)((Scale_Point(IP),IP=1,NB_Point), i=1,6)
     CLOSE (2)
  ENDIF

 
  RETURN

110 WRITE(29,*) 'Error in openning file ',JobName(1:JobNameLength)//'.plt'
   CALL Error_Stop (' Output_Volume ')
112 WRITE(29,*) 'Error in writing file ',JobName(1:JobNameLength)//'.plt'
   CALL Error_Stop (' Output_Volume ')


END SUBROUTINE Output_Volume


!*******************************************************************************
!>
!!   Read a surface mesh from file *.fro
!!   @param[out]   common_Parameters::Surf     
!<      
!*******************************************************************************
SUBROUTINE Read_Surface( )

  USE common_Parameters
  USE SurfaceCurvatureCADfix
  USE SurfaceMeshManager
  IMPLICIT NONE

  INTEGER :: i
  LOGICAL :: ex

  IF(Curvature_Type/=2)THEN
     INQUIRE(file = JobName(1:JobNameLength)//'.fro', EXIST=ex)
     IF(.NOT. ex)THEN
        WRITE(29,*) 'Error--- the file '//JobName(1:JobNameLength)//'.fro does NOT exist.'
        WRITE(*, *) 'Error--- the file '//JobName(1:JobNameLength)//'.fro does NOT exist.'
        CALL Error_Stop( ' Read_Surface ::'  )
     ENDIF
     CALL SurfaceMeshStorage_Input(JobName,JobNameLength,Surf)
  ELSE IF(Curvature_Type==2) THEN
     CALL SurfaceMeshStorage_from_CADfix(Surf)  !Surface type = 1 so we won't get here - SPW
  ENDIF

  IF(Frame_Type==0)THEN
     CALL Surf_Reverse (Surf, 1) 
  ENDIF

  IF(Surf%GridOrder==1)THEN
     DO i =1,Surf%NB_Tri
        CALL sort_ipp_tri(Surf%IP_Tri(1:3,i))
     ENDDO
  ELSE IF(Surf%GridOrder==3)THEN
     DO i =1,Surf%NB_Tri
        CALL sort_ipph_tri(Surf%IP_Tri(1:3,i), Surf%IPsp_Tri(:,i))
     ENDDO
     Surf%NB_Point = Surf%NB_Point - Surf%NB_Psp
  ENDIF
  


END SUBROUTINE Read_Surface


!*******************************************************************************
!>
!!   Output baoundary triangulation as a surface mesh to file *_out.fro
!!   @param[in]  common_Parameters::NB_BD_Tri
!!   @param[in]  common_Parameters::NB_BD_Point
!!   @param[in]  common_Parameters::IP_BD_Tri
!!   @param[in]  common_Parameters::Posit
!<
!*******************************************************************************
SUBROUTINE Output_Surface( )

  USE common_Parameters
  IMPLICIT NONE

  INTEGER :: i, nas, nbf

  nbf = 0
  DO i =1,NB_BD_Tri
     nbf = MAX(nbf, IP_BD_Tri(5,i))
  ENDDO

  nas = 0
  OPEN(1,file=JobName(1:JobNameLength)//'_out.fro')
  WRITE(1,'(2I9,6(1X,I4))') NB_BD_Tri,NB_BD_Point,nas,nas,   &
                            Surf%NB_Seg,nbf,Surf%NB_Sheet,nas

  DO i = 1,NB_BD_Point
     WRITE(1,'(I9,3(1x,E16.9))') i,Posit(1:3,i)
  ENDDO

  DO i =1,NB_BD_Tri
     WRITE(1,'(5(1x,I9))') i, IP_BD_Tri(1:3,i), IP_BD_Tri(5,i)
  ENDDO

  CLOSE(1)

END SUBROUTINE Output_Surface

!*******************************************************************************
!>
!!   Write a file *_.CC with
!!    common_Parameters::Sphere_Tet and  common_Parameters::RR_Point
!<
!*******************************************************************************
SUBROUTINE Output_CC( )

  USE common_Parameters
  IMPLICIT NONE
  INTEGER :: it

  OPEN(1,file=JobName(1:JobNameLength)//'.CC', status='unknown',   &
       form='UNFORMATTED',err=110)
  WRITE(1,err=111)NB_Tet, NB_Point
  WRITE(1,err=111)(Sphere_Tet(1:3,it),it=1,NB_Tet)
  WRITE(1,err=111)RR_Point(1:NB_Point)
  CLOSE (1)


  RETURN

110 WRITE(29,*) 'Error in openning file ',JobName(1:JobNameLength)//'.CC'
  CALL Error_Stop ('Output_RR')
111 WRITE(29,*) 'Error in writing file ',JobName(1:JobNameLength)//'.CC'
  CALL Error_Stop ('Output_RR')

END SUBROUTINE Output_CC

!*******************************************************************************
!>       
!!   write a cell (position of 4 nodes) through a IO channel
!!        (for gnuplot purpose).
!<
!*******************************************************************************
SUBROUTINE Output_aCell(IT, io)

  USE common_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: IT, io
  CHARACTER*(2) :: sw

  IF(io/=6)THEN
     WRITE(sw,'(I2)') MOD(io,100)
     IF(sw(1:1)==' ') sw(1:1) = '0'
     OPEN(io,file=JobName(1:JobNameLength)//'.cell'//sw, position='append')
  ENDIF

  WRITE(io,*)'# IT=',IT
  WRITE(io,'(3(1X,E12.5),1X,I9)') Posit(:,IP_Tet(1,IT)),IP_Tet(1,IT)
  WRITE(io,'(3(1X,E12.5),1X,I9)') Posit(:,IP_Tet(2,IT)),IP_Tet(2,IT)
  WRITE(io,'(3(1X,E12.5),1X,I9)') Posit(:,IP_Tet(3,IT)),IP_Tet(3,IT)
  WRITE(io,'(3(1X,E12.5),1X,I9)') Posit(:,IP_Tet(1,IT)),IP_Tet(1,IT)
  WRITE(io,'(3(1X,E12.5),1X,I9)') Posit(:,IP_Tet(4,IT)),IP_Tet(4,IT)
  WRITE(io,'(3(1X,E12.5),1X,I9)') Posit(:,IP_Tet(2,IT)),IP_Tet(2,IT)
  WRITE(io,'(3(1X,E12.5),1X,I9)') Posit(:,IP_Tet(4,IT)),IP_Tet(4,IT)
  WRITE(io,'(3(1X,E12.5),1X,I9)') Posit(:,IP_Tet(3,IT)),IP_Tet(3,IT)
  WRITE(io,*)
  WRITE(io,*)

  IF(io/=6)THEN
     CLOSE(io)
  ENDIF

END SUBROUTINE Output_aCell


