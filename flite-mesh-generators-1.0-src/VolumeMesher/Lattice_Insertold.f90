!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!**************************************************************************
!>
!!  Lattice_Insert 
!!  This subroutine inserts points using the unit cell of an ideal tet mesh
!!  A kdtree is used to search for spatial searching to merge close points
!!  Initial implimentation writen by S. Walton 18/10/2013
!!
!<
!**************************************************************************
SUBROUTINE Lattice_Insert()
      USE common_Parameters
      USE array_allocator
      USE SpacingStorage
      USE kdtree2_precision_module
      USE kdtree2_module
      USE Geometry3DAll

      IMPLICIT NONE
        
      INTEGER :: done,allFail,helpFlag,itry,Isucc
      INTEGER :: pStart,pEnd,numIns
      INTEGER :: ip,ic,numAlloc,iRes,itemp
      REAL*8 :: unitCell(3,14),fMap(3,3)
      REAL*8 :: geoD,sqrt34,oneOverSqrt34,p1(3),p2(3),pmid(3)
      REAL*8                         :: b,h,b1,h1,alpha
      REAL*8 :: d,delta,dtet,dmax,dmin,dtemp
      type(kdtree2), pointer :: tree
      INTEGER :: ie,  numLoops,IT,i,j
      INTEGER :: idxin,correltime,nn,nfound,NB_Tet_Old,NumIP,NumResAloc
      character*(4) :: loopString 
      type(kdtree2_result),allocatable :: results(:)
      type(IntQueueType),allocatable :: child(:)
      INTEGER,allocatable :: marker(:)
      REAL*8  :: RAsq,RAstand,RAfac,RAmaxloc,RAminloc,RAvol,RAsph,tolLoc
      
	  
	  tolLoc = 1.0d0
	  
      RAfac   = 2.0d0
      RAstand = BGSpacing%BasicSize / dsqrt(2.d0)
      RAstand = RAstand * RAfac
!
! OH added
      print *,' basic size is:',   BGSpacing%BasicSize

      !The done variable keeps track of wether or not more points need inserting
      done = 0
      allFail = 1
      numLoops = 0
      !This variable means that all points can be considered as neighbours in kdtree
      correltime = -1
      
      !pStart and pEnd tell us the indices of the last set of points created
	  !For testing this is set to 0 0 0
      
!	  NB_Point = NB_Point + 1
!      IF(NB_Point>nallc_point) THEN 
            
!         CALL ReAllocate_Point()
                                                   
!      END IF
!      Posit(1:3,NB_Point) = 0.d0
!	  CALL Get_Point_Mapping(NB_Point)
!	  Isucc = 0
!      CALL Insert_Point_Delaunay(NB_Point,0,Isucc,0)
      pStart = 1
      pEnd = NB_Point
!	  CALL Remove_Tet()
	  
      sqrt34 = (SQRT(3.0d0/4.0d0))
      oneOverSqrt34 = 1/sqrt34
      b1 = oneOverSqrt34/2.d0  !This means we are aiming for the actual spacing
      h1 = oneOverSqrt34/SQRT(2.d0) !This means we are aiming for the actual spacing

     
      NumResAloc = 1000
      allocate(results(NumResAloc))
      numIns = 0

      ie = 1
      !Just to make sure
      !CALL Set_Domain_Mapping()
      WRITE(*,*)'Inserting Points using Lattice Insertion'
      WRITE(29,*)'Inserting Points using Lattice Insertion'
      
      DO WHILE (done .EQ. 0)
                  numLoops = numLoops + 1
            IF(Debug_Display>0)THEN
                  
                  WRITE(*,*)'Lattice Insertion Loop:',numLoops
                  WRITE(29,*)'Lattice Insertion Loop:',numLoops

                  loopString = INT_TO_CHAR(numLoops,4)
            END IF

            !All fail 
            allFail = 1
            numAlloc = 0
                  !Allocate marker and child arrays
                  allocate(marker(NB_Tet))
                  allocate(child(NB_Tet))
                  
                  WRITE(*,*)'NB_Tet',NB_Tet
                  WRITE(*,*)'size marker',SIZE(marker)
                  WRITE(*,*) 'size child',SIZE(child)
                  
                  
                  
                  marker(1:NB_Tet)=0
                  
            CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
            
            IF(Debug_Display>2)THEN           
                   WRITE(*,*)'Lattice Insertion: Generating Points'
                   WRITE(29,*)'Lattice Insertion: Generating Points'
                END IF
            
            Mark_Point(1:NB_Point) = 0
            
            
            
            !alpha = MAXVAL(Scale_Point)*BGSpacing%BasicSize   
            !WRITE(*,*)'Alpha',alpha			
                  
            DO ip = pStart,pEnd
                  
                  
                  !Get the spacing and the lattice points
                  IF(BGSpacing%Model>0)THEN
                        d = Scale_Point(ip)*BGSpacing%BasicSize
                  ELSE IF(BGSpacing%Model<0)THEN
                        d = MINVAL(fMap_Point(:,:,ip))*BGSpacing%BasicSize
                  END IF
                  
                  
                  p1 = Posit(:,ip)
                  
                  b = (d*b1)
                  h = (d*h1)            
                  
                  
                  unitCell(:,1) = (/-1.d0*b,h,0.d0/) + p1
                  unitCell(:,2) = (/0.d0,h,h/) + p1
                  unitCell(:,3) = (/b,h,0.d0/) + p1
                  unitCell(:,4) = (/-1.d0*b,0.d0,h/) + p1
                  unitCell(:,5) = (/b,0.d0,h/) + p1
                  unitCell(:,6) = (/-1.d0*b,-1.d0*h,0.d0/) + p1
                  unitCell(:,7) = (/0.d0,-1.d0*h,h/) + p1
                  unitCell(:,8) = (/b,-1.0d0*h,0.d0/) + p1
                  unitCell(:,9) = (/0.0d0,-1.0d0*h,-1.0d0*h/) + p1
                  unitCell(:,10) = (/-1.d0*b,0.d0,-1.0d0*h/) + p1
                  unitCell(:,11) = (/0.0d0,h,-1.0d0*h/) + p1
                  unitCell(:,12) = (/b,0.d0,-1.0d0*h/) + p1
                  unitCell(:,13) = (/2.d0*b,0.d0,0.d0/) + p1
                  unitCell(:,14) = (/-2.d0*b,0.d0,0.d0/) + p1
                  
                  CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
                  

                  !Now we have the points we need to loop round them and perform the hull check
                  DO ic = 1,14
                              
                        !First check we are in the domain
                        IF( unitCell(1,ic) < XYZmax(1) .AND. unitCell(1,ic) > XYZmin(1) .AND. &
                            unitCell(2,ic) < XYZmax(2) .AND. unitCell(2,ic) > XYZmin(2) .AND. &
                            unitCell(3,ic) < XYZmax(3) .AND. unitCell(3,ic) > XYZmin(3)) THEN
                              IF (ic.EQ.1) THEN
                                    ie = ITs_List(1)
                              ELSE
                                    ie = -1
                              ENDIF
                              p1 = unitCell(:,ic)
                              CALL Locate_Node_Tet(p1,ie,3)
                        
                              IF (ie .GT. 0) THEN
                     
                                    
                                 

                                 IF(BGSpacing%Model>0)THEN
                                     !dtet = Scale_Tet(ie)*BGSpacing%BasicSize
                                     !dtet = MAXVAL(Scale_Point(IP_Tet(:,ie)))*BGSpacing%BasicSize
                                                       
                                     CALL SpacingStorage_GetScale(BGSpacing, p1, dtet)
                                                       
                                 ELSE IF(BGSpacing%Model<0)THEN
                                     !dtet = MAXVAL(fMap_Tet(:,:,ie))*BGSpacing%BasicSize
                                                       
                                      CALL SpacingStorage_GetMapping(BGSpacing, p1, fMap)
                                      dtet = MINVAL(fMap)
                                                       
                                 END IF
                                 
                                 dtet = dtet*BGSpacing%BasicSize
                                 dtet = MIN(dtet,d)
								 
								 
                                 p2 = Posit(:,IP_Tet(1,ie))
                                          
                                 dmax = Geo3D_Distance_SQ(p1,p2)  
								 dmin = dmax
								 
				 DO j = 2,4
				
				    p2 = Posit(:,IP_Tet(j,ie))
				    dtemp = Geo3D_Distance_SQ(p1,p2)
				    dmax = MAX(dmax,dtemp)
				    dmin = MIN(dmin,dtemp)
									
								 
				 END DO
                                 
                                 IF ((dtet*dtet .LE. dmin)) THEN
                                          
                                   
                                      NB_Point = NB_Point + 1
                                      IF(NB_Point>nallc_point) THEN 
                    
                              
                                         numAlloc = numAlloc + 1
                                         nallc_increase = MAX(14,(pEnd-ip)*14)
                                                         
                                         CALL ReAllocate_Point()
                                                  
                                                         
                                      END IF
                                      Posit(:,NB_Point) = unitCell(:,ic)
                                                      
                                      CALL IntQueue_Push(child(ie),NB_Point)
                                      marker(ie) = marker(ie) + 1

                                      CALL Get_Point_Mapping(NB_Point)

                                          
                                      allFail = 0
                                          
                              
                                 END IF
                              END IF            
                        END IF
                  
                  END DO
      
      
            END DO

            
            numIns = NB_Point-pEnd
            IF(Debug_Display>0)THEN
                WRITE(*,*)'Lattice Insertion: number of points to check',numIns
                WRITE(29,*)'Lattice Insertion: number of points to check',numIns
                WRITE(*,*)'Lattice Insertion: number of realocations required',numAlloc
                WRITE(29,*)'Lattice Insertion: number of realocations required',numAlloc
            END IF
            
            IF (allFail .EQ. 1) THEN
                  done = 1
            ELSE
                  
                IF(Debug_Display>0)THEN
                   WRITE(*,*)'Lattice Insertion: creating kdtree'
                   WRITE(29,*)'Lattice Insertion: creating kdtree'
                 END IF
            

                Mark_Point(1:NB_Point)=0
                  
                helpFlag = 0
                  
                 
                tree => kdtree2_create(Posit(1:3,1:NB_Point),sort=.false.,rearrange=.false.)
                          
                IF(Debug_Display>0)THEN
                        WRITE(*,*)'Lattice Insertion: checking proximity to last layer'
                        WRITE(29,*)'Lattice Insertion: checking proximity to last layer'
                END IF
                  
                  
                  
                  DO idxin = 1,pEnd
                        !First get the spacing
                        !Min distance between points is sqrt(3/4) but kdtree is approximate so search wider area
                        IF(BGSpacing%Model>0)THEN
                              d = Scale_Point(idxin)*BGSpacing%BasicSize
                        ELSE IF(BGSpacing%Model<0)THEN
                              d = MINVAL(fMap_Point(:,:,idxin))*BGSpacing%BasicSize
                        END IF
                        
                        !This call searches in a d radius sphere  around the point idxin 
                        nn = kdtree2_r_count_around_point(tree,idxin,correltime,d)
                        IF (nn .GT. 0) THEN
                              
                            IF (nn .GT. NumResAloc) THEN
                                              
                             IF(Debug_Display>0)THEN
                                 WRITE(*,*)'Lattice Insertion: reallocate results'
                                 WRITE(29,*)'Lattice Insertion: reallocate results'
                             END IF
                             deallocate(results)
                             NumResAloc = nn
                             allocate(results(NumResAloc))
                             WRITE(*,*)'NumResAloc',NumResAloc
                             WRITE(*,*)'Size results',SIZE(results)
                                                
                            ENDIF
                                            
                            CALL kdtree2_r_nearest_around_point(tree,idxin,correltime,d,nfound,nn,results)
                              
                            DO ip = 1,nfound
                                    iRes = results(ip)%idx
                                    IF ((ires/=idxin).AND.(Mark_Point(iRes)/=-999).AND.(iRes .GT. pEnd)) THEN
                                          
                                        !Take the spacing at the previously placed point
                                        IF(BGSpacing%Model>0)THEN
                                            !delta = MAX((Scale_Point(iRes)*BGSpacing%BasicSize),d)
                                             delta = d                    
                                        ELSE IF(BGSpacing%Model<0)THEN
                                             !delta = MAX((MINVAL(fMap_Point(:,:,iRes))*BGSpacing%BasicSize),d)
                                             delta = d                     
                                        END IF
                                                                         
                                        delta = 0.5d0*delta  
                                        p1 = Posit(:,iRes)
                                        p2 = Posit(:,idxin)

                                          
                                        geoD = Geo3D_Distance_SQ(p1,p2)        

                                        
                                        IF (geoD.LE.(delta*delta)) THEN
                                            !Then this point needs deleting, help flag indicates at least 
                                            !one point needs removing      
                                            helpFlag = 1
                                            Mark_Point(iRes) = -999
                                            numIns = numIns-1
                                        END IF
                              
                                    END IF
                              END DO
                        
                              
                        END IF
                  END DO
                  
                  
                  !Now check mutual proximity

                  !Difference between this loop and the last is in the last loop we didn't
                  !move points from the last layer, in this loop we need to keep track of
                  !which points have been 'swallowed'

                  IF (done .EQ. 0) THEN
                  
                    IF(Debug_Display>0)THEN
                        WRITE(*,*)'Lattice Insertion: checking mutual proximity'
                        WRITE(29,*)'Lattice Insertion: checking mutual proximity'
                    END IF

                  
                    DO idxin = pEnd+1,NB_Point
                       IF (Mark_Point(idxin).EQ.0) THEN
                          
                          !First get the spacing again min dist is sqrt(3/4) but search wider
                                    
                          IF(BGSpacing%Model>0)THEN
                             d = Scale_Point(idxin)*BGSpacing%BasicSize
                          ELSE IF(BGSpacing%Model<0)THEN
                             d = MINVAL(fMap_Point(:,:,idxin))*BGSpacing%BasicSize
                          END IF
                                    
                          nn = kdtree2_r_count_around_point(tree,idxin,correltime,d)
                          
                
                          IF (nn .GT. 0) THEN
                                          
                             IF (nn .GT. NumResAloc) THEN
                                                              
                                IF(Debug_Display>0)THEN
                                    WRITE(*,*)'Lattice Insertion: reallocate results'
                                    WRITE(29,*)'Lattice Insertion: reallocate results'
                                END IF
                                                              
                                deallocate(results)
                                NumResAloc = nn
                                allocate(results(NumResAloc))
                                WRITE(*,*)'NumResAloc',NumResAloc
                                WRITE(*,*)'Size results',SIZE(results)
                             ENDIF
                                                              
                              CALL kdtree2_r_nearest_around_point(tree,idxin,correltime,d,nfound,nn,results)
                                          
                              DO ip = 1,nfound
                                    iRes = results(ip)%idx
                                IF ((ires/=idxin).AND.(iRes .GT. pEnd).AND.(Mark_Point(iRes)/=-999)) THEN
                                    !Take the minimum spacing of the two points
                                     IF(BGSpacing%Model>0)THEN
                                        !pmid(:) = 0.5d0*(Posit(:,iRes)+Posit(:,idxin))
                                        !CALL SpacingStorage_GetScale(BGSpacing, pmid, delta)      
                                        !delta = delta*BGSpacing%BasicSize
                                        delta = MAX((Scale_Point(iRes)*BGSpacing%BasicSize),d)
                                     ELSE IF(BGSpacing%Model<0)THEN
                                                            
                                        delta = MAX((MINVAL(fMap_Point(:,:,iRes))*BGSpacing%BasicSize),d)
                                     END IF

                                                      
                                     p1 = Posit(:,iRes)
                                     p2 = Posit(:,idxin)
                                     delta = 0.5d0*delta


                                                      
                                     geoD = Geo3D_Distance_SQ(p1,p2)
                                     !write(*,*)'geoD',geoD,'delta',delta*delta
									 !delta = delta*0.5
                                     IF (geoD.LE.((delta*delta))) THEN
                                                            
                                         helpFlag = 1
										 
                                         Mark_Point(iRes) = -999
										
                                         numIns = numIns - 1
                                     END IF

                                

                                END IF
                                                                  
                              END DO
                                    

                                          
                            END IF  
                       END IF
                    END DO
					
					
					
					
                  END IF
                  
              !Tidy everything up
              CALL kdtree2_destroy(tree)
                    
            
            END IF
            
            IF (numIns .GT. 0) THEN
            
                  IF(Debug_Display>0)THEN
                      WRITE(*,*)'Lattice Insertion: number of points to insert',numIns
                      WRITE(29,*)'Lattice Insertion: number of points to inser',numIns
                  END IF
                         WHERE (Mark_Point(pEnd+1:NB_Point)/=-999) Mark_Point(pEnd+1:NB_Point) = 1
                    NB_Tet_Old = NB_Tet
                 !We can insert all the points
                                                       
                 nallc_increase = numIns*5
                 DO itry = 1,5
                           DO ie = 1,NB_Tet_Old
                             IF(marker(ie) > 0 )THEN
                                 NumIP = child(ie)%numNodes
                                 
                               DO i = 1,NumIP
                                 ip = child(ie)%Nodes(i)
                                     IF (Mark_Point(ip).EQ.1) THEN
                                       IF (IP_Tet(4,ie)>0) IT = ie
                                          CALL Locate_Node_Tet(Posit(1:3,ip),IT,3)
                                       IF (IT>0) THEN
                                         !Try to insert the point
										 
										dtet = Scale_Point(ip)*BGSpacing%BasicSize
										p2 = Posit(:,IP_Tet(1,IT))
                                          
                                        dmax = Geo3D_Distance_SQ(p1,p2)  
								        dmin = Geo3D_Distance_SQ(p1,p2)
								 
								        DO j = 2,4
								 
								             p2 = Posit(:,IP_Tet(j,IT))
									         dtemp = Geo3D_Distance_SQ(p1,p2)
									         dmax = MAX(dmax,dtemp)
									         dmin = MIN(dmin,dtemp)
								 
								        END DO
										 
										IF ((dtet*dtet .LE. dmin)) THEN
                                           Isucc = 0
                                           CALL Insert_Point_Delaunay(ip,IT,Isucc,0)
                                           IF (Isucc.EQ.1) THEN
                                               Mark_Point(ip) = 0
                                               marker(ie) = marker(ie)-1
                                             
                                           ENDIF
                                        ENDIF
                                       ENDIF
                         
                         
                                 ENDIF
                               ENDDO
                             ENDIF
                           ENDDO
                           CALL Remove_Tet()
                         ENDDO
                         
                         DO ie=1,NB_Tet_Old
                           CALL IntQueue_Clear(child(ie))
                         ENDDO
                         
                 deallocate(child)
                         deallocate(marker)
                         WHERE (Mark_Point(pEnd+1:NB_Point).EQ.1) Mark_Point(pEnd+1:NB_Point) = -999
                  
                  
                  !Delete failed points
                  CALL Remove_Point()
                  
                  Mark_Point(1:NB_Point)=0      
                  
                  
                 IF(Debug_Display>3)THEN
                     !Output bad elements to plt
                     
                     MeshOut%GridOrder = GridOrder
                     MeshOut%NB_Point  = NB_Point
                     MeshOut%NB_Tet    = NB_Tet
                     ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
                     ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
                     MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
                     MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)



                     !JobNameT = JobName(1:JobNameLength)//'_'//loopString
                     !JobNameLengthT = JobNameLength + 5
                     CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_'//loopString,JobNameLength+5, -15, MeshOut, Surf)

                     DEALLOCATE(MeshOut%IP_Tet)
                     DEALLOCATE(MeshOut%Posit)


                  ENDIF

                  
                  IF ((NB_Point .EQ. pEnd)) THEN
                        done = 1
                  ELSE
                        IF(Debug_Display>0)THEN
                                    WRITE(*,*)'Lattice Insertion: number of points inserted',NB_Point-pEnd
                                    WRITE(29,*)'Lattice Insertion: number of points inserted',NB_Point-pEnd
                        END IF
                  
                  
                        pStart = pEnd+1
                        pEnd = NB_Point
                  
                  END IF
            
            ELSE
                        DO ie=1,NB_Tet
                           CALL IntQueue_Clear(child(ie))
                         ENDDO
                        deallocate(child)
                         deallocate(marker)
                  CALL Remove_Point()
                  done = 1
            END IF
!            
            
            CALL LinkAssociation_Clear(PointAsso)
      END DO
    nallc_increase = 100000
    RR_Point(1:NB_Point) = 0.d0
   ! deallocate(kPosit)
    deallocate(results)
  
      IF(Debug_Display>0)THEN
                  
                  WRITE(*,*)'Lattice Insertion completed in numLoops:',numLoops
                  WRITE(29,*)'Lattice Insertion completed in numLoops:',numLoops
      END IF

END SUBROUTINE Lattice_Insert


!**************************************************************************
!>
!!  unitCell 
!!  This subroutine returns the unit cell around Pt at spacing d
!!
!<
!**************************************************************************
SUBROUTINE unitCellR(Pt,d,xyz)

      REAL*8, INTENT(IN)      :: Pt(3)
      REAL*8, INTENT(IN)       :: d
      REAL*8, INTENT(OUT) :: xyz(3,14)
      REAL*8                         :: b,h,s
      
      b = (d/2.d0)
      h = (d/SQRT(2.d0))

      s = sqrt(3.d0/4.d0)
      
      xyz(:,1) = (/-1.d0*b,h,0.d0/) + Pt(:)
      xyz(:,2) = (/0.d0,h,h/)*s + Pt(:)
      xyz(:,3) = (/b,h,0.d0/) + Pt(:)
      xyz(:,4) = (/-1.d0*b,0,h/) + Pt(:)
      xyz(:,5) = (/b,0.d0,h/) + Pt(:)
      xyz(:,6) = (/-1.d0*b,-1.d0*h,0.d0/) + Pt(:)
      xyz(:,7) = (/0.d0,-1.d0*h,h/)*s + Pt(:)
      xyz(:,8) = (/b,-1.0d0*h,0.d0/) + Pt(:)
      xyz(:,9) = (/0.0d0,-1.0d0*h,-1.0d0*h/)*s + Pt(:)
      xyz(:,10) = (/-1.d0*b,0.d0,-1.0d0*h/) + Pt(:)
      xyz(:,11) = (/0.0d0,h,-1.0d0*h/)*s + Pt(:)
      xyz(:,12) = (/b,0.d0,-1.0d0*h/) + Pt(:)
      xyz(:,13) = (/2.d0*b,0.d0,0.d0/)*s + Pt(:)
      xyz(:,14) = (/-2.d0*b,0.d0,0.d0/)*s + Pt(:)
      


END SUBROUTINE unitCellR
