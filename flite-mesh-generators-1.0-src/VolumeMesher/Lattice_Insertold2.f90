!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!**************************************************************************
!>
!!  Lattice_Insert 
!!  This subroutine inserts points using the unit cell of an ideal tet mesh
!!  A kdtree is used to search for spatial searching to merge close points
!!  Initial implimentation writen by S. Walton 18/10/2013
!!
!<
!**************************************************************************
SUBROUTINE Lattice_Insert()
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE kdtree2_precision_module
	USE kdtree2_module
	USE Geometry3DAll

	INTEGER :: done,allFail,helpFlag
	INTEGER :: pStart,pEnd,NB_Point_Old
	INTEGER :: ip,Isucc
	REAL*8 :: unitCell(3,14)
    REAL*8 :: geoD,sqrt34,oneOverSqrt34
	
    real(kdkind) :: d,delta,dtet
	type(kdtree2), pointer :: tree
	INTEGER :: ie,  numLoops, iSwap
	INTEGER :: idxin,correltime,nn,nfound
	real(kdkind),allocatable :: kPosit(:,:)
	type(kdtree2_result),allocatable :: results(:)
	
	!The done variable keeps track of wether or not more points need inserting
	done = 0
	allFail = 1
	numLoops = 0
	!This variable means that all points can be considered as neighbours in kdtree
	correltime = -1
	
	
	
	sqrt34 = (SQRT(3.0d0/4.0d0))
	oneOverSqrt34 = 1/sqrt34


	allocate(kPosit(3,nallc_point))
	allocate(results(100000))

	ie = 1
	!Just to make sure
	CALL Set_Domain_Mapping()
	
	!pStart and pEnd tell us the indices of the last set of points created
	pStart = 1
	pEnd = NB_Point
	!d = HUGE(0.d0)
	!DO ip=1,NB_Point
	!	IF(BGSpacing%Model>0)THEN
	!				delta = Scale_Point(ip)*BGSpacing%BasicSize
	!				IF(delta<d)THEN
	!					d = delta
	!					pStart = ip
	!				ENDIF
	!	ELSE IF(BGSpacing%Model<0)THEN
	!				delta = MINVAL(fMap_Point(:,:,ip))*BGSpacing%BasicSize
	!				IF(delta<d)THEN
	!					d = delta
	!					pStart = ip
	!				ENDIF
	!	END IF
	!ENDDO
	!pEnd = pStart
	NB_Point_Old = NB_Point
	
	
	WRITE(*,*)'Inserting Points using Lattice Insertion'
	WRITE(29,*)'Inserting Points using Lattice Insertion'
	
	DO WHILE (done .EQ. 0)
		IF(Debug_Display>0)THEN
			numLoops = numLoops + 1
			WRITE(*,*)'Lattice Insertion Loop:',numLoops
			WRITE(29,*)'Lattice Insertion Loop:',numLoops
		END IF

		!All fail 
		allFail = 1
		!Update circum-info and Next info and Link info
                CALL Get_Tet_Volume(0)
		CALL Get_Tet_Circum(0)
		CALL Set_Domain_Mapping()
		CALL Next_Build()
		CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
		
		IF(Debug_Display>2)THEN
                        
             WRITE(*,*)'Lattice Insertion: Generating Points'
             WRITE(29,*)'Lattice Insertion: Generating Points'
        END IF
		
		DO ip = pStart,pEnd
			
			IF(BGSpacing%Model>0)THEN
				d = Scale_Point(ip)*BGSpacing%BasicSize
			ELSE IF(BGSpacing%Model<0)THEN
				d = MINVAL(fMap_Point(:,:,ip))*BGSpacing%BasicSize
			END IF
			
			CALL unitCellR(Posit(:,ip),oneOverSqrt34*d,unitCell)
			
		!	IF((Debug_Display>0 .and. mod(ip,100000)==1) .or.   &
        	!	(Debug_Display>1 .and. mod(ip,10000 )==1) .or.   &
        	!	(Debug_Display>2 .and. mod(ip,1000  )==1) .or. Debug_Display>4)THEN
        	!		WRITE(*,*) ' Generating around  Point ',ip, char(13)
     		!	ENDIF

			!Now we have the points we need to loop round them and perform the hull check
			DO ic = 1,14
					
				!First check we are in the domain
				IF( unitCell(1,ic) < XYZmax(1) .AND. unitCell(1,ic) > XYZmin(1) .AND. &
				    unitCell(2,ic) < XYZmax(2) .AND. unitCell(2,ic) > XYZmin(2) .AND. &
				    unitCell(3,ic) < XYZmax(3) .AND. unitCell(3,ic) > XYZmin(3)) THEN
	
					CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
					ie = ITs_List(1)
					CALL Locate_Node_Tet(unitCell(:,ic),ie,3)
				
					IF (ie .GT. 0) THEN
                        	
		!				CALL Get_Tet_Circum(ie)
						!Then we need to check the circum radius 
						
						
	                                        IF(BGSpacing%Model>0)THEN
                                                    dtet = Scale_Tet(ie)*BGSpacing%BasicSize
                                                ELSE IF(BGSpacing%Model<0)THEN
                                                    dtet = MINVAL(fMap_Tet(:,:,ie))*BGSpacing%BasicSize
                                                END IF




						IF (det .LT. Sphere_Tet(4,ie)) THEN
							
							!Then we can use this point
							
							NB_Point = NB_Point + 1
							IF(NB_Point>nallc_point) THEN 
								CALL ReAllocate_Point()
								deallocate(kPosit)
								allocate(kPosit(3,nallc_point))
							END IF
							Posit(:,NB_Point) = unitCell(:,ic)
							


							CALL Get_Point_Mapping(NB_Point)

							
							allFail = 0
							
					
						END IF
					END IF		
				END IF
			
			END DO
	
	
		END DO
		!CALL Remove_Tet()
		IF(Debug_Display>0)THEN
				WRITE(*,*)'Lattice Insertion: number of points to check',NB_Point-NB_Point_Old
				WRITE(29,*)'Lattice Insertion: number of points to check',NB_Point-NB_Point_Old
		END IF
		
		IF (allFail .EQ. 1) THEN
			done = 1
		ELSE
			
			IF(Debug_Display>0)THEN
				WRITE(*,*)'Lattice Insertion: creating kdtree'
				WRITE(29,*)'Lattice Insertion: creating kdtree'
			END IF
		

			Mark_Point(1:NB_Point)=0
			
			helpFlag = 0
			
			!Then create a tree to query last layer
			
			kPosit(1:3,1:NB_Point) = Posit(1:3,1:NB_Point)
			tree => kdtree2_create(kPosit(1:3,1:NB_Point),sort=.false.,rearrange=.false.)
			
			IF(Debug_Display>0)THEN
				WRITE(*,*)'Lattice Insertion: checking proximity to last layer'
				WRITE(29,*)'Lattice Insertion: checking proximity to last layer'
			END IF
			
			
			
			DO idxin = 1,NB_Point_Old
				!First get the spacing
				!Min distance between points is sqrt(3/4) but kdtree is approximate so search wider area
				IF(BGSpacing%Model>0)THEN
					d = Scale_Point(idxin)*BGSpacing%BasicSize
				ELSE IF(BGSpacing%Model<0)THEN
					d = MINVAL(fMap_Point(:,:,idxin))*BGSpacing%BasicSize
				END IF
				
				nn = kdtree2_r_count_around_point(tree,idxin,correltime,d)
				IF (nn .GT. 0) THEN
					
					CALL kdtree2_r_nearest_around_point(tree,idxin,correltime,d,nfound,nn,results)
					
					DO ip = 1,nfound
						IF (results(ip)%idx .GT. NB_Point_Old) THEN

							
                                                 !Take the minimum spacing of the two points
                                                 IF(BGSpacing%Model>0)THEN
                                                        delta = MIN((Scale_Point(results(ip)%idx)*BGSpacing%BasicSize),d)
                                                 ELSE IF(BGSpacing%Model<0)THEN
                                                        delta = MIN((MINVAL(fMap_Point(:,:,results(ip)%idx))*BGSpacing%BasicSize),d)
                                                 END IF
                            geoD = Geo3D_Distance_SQ(Posit(:,results(ip)%idx),Posit(:,idxin))        



                            IF (geoD.LT.(delta*sqrt34*delta*sqrt34)) THEN
                                                              
                                 helpFlag = 1
                                 Mark_Point(results(ip)%idx) = -999
                            END IF
					
						END IF
					END DO
				
					
				END IF
			END DO
			
			
			IF (helpFlag .EQ. 1) THEN
			
				IF(Debug_Display>0)THEN
					WRITE(*,*)'Lattice Insertion: checking proximity to last layer: removing points'
					WRITE(29,*)'Lattice Insertion: checking proximity to last layer: removing points'
				END IF
			
				!Then some points need deleting
				CALL Remove_Point()
				CALL Next_Build()
				CALL Remove_tet()
				
				Mark_Point(1:NB_Point)=0
				
				!...and we may need to recreate the tree
				IF (NB_Point .GT. NB_Point_Old) THEN
					CALL kdtree2_destroy(tree)
					
					IF(Debug_Display>0)THEN
						WRITE(*,*)'Lattice Insertion: creating kdtree'
						WRITE(29,*)'Lattice Insertion: creating kdtree'
					END IF
					
					
					kPosit(1:3,1:NB_Point) = Posit(1:3,1:NB_Point)
					tree => kdtree2_create(kPosit(1:3,1:NB_Point),sort=.false.,rearrange=.false.)
					IF(Debug_Display>0)THEN
						WRITE(*,*)'Lattice Insertion: number of points to check',NB_Point-NB_Point_Old
						WRITE(29,*)'Lattice Insertion: number of points to check',NB_Point-NB_Point_Old
					END IF
					
					
				ELSE
					done = 1
				END IF
			END IF
			
			helpFlag = 0
			
			!Now check mutual proximity
			IF (done .EQ. 0) THEN
			
				IF(Debug_Display>0)THEN
					WRITE(*,*)'Lattice Insertion: checking mutual proximity'
					WRITE(29,*)'Lattice Insertion: checking mutual proximity'
				END IF

			
				DO idxin = NB_Point_Old+1,NB_Point
					IF (Mark_Point(idxin).EQ.0) THEN
						Mark_Point(idxin) = 1
						!First get the spacing again min dist is sqrt(3/4) but search wider
						
						IF(BGSpacing%Model>0)THEN
							d = Scale_Point(idxin)*BGSpacing%BasicSize
						ELSE IF(BGSpacing%Model<0)THEN
							d = MINVAL(fMap_Point(:,:,idxin))*BGSpacing%BasicSize
						END IF
						
						nn = kdtree2_r_count_around_point(tree,idxin,correltime,d)
							
						IF (nn .GT. 0) THEN
							
							CALL kdtree2_r_nearest_around_point(tree,idxin,correltime,d,nfound,nn,results)
							
							DO ip = 1,nfound
								IF ((results(ip)%idx .GT. NB_Point_Old).AND.(Mark_Point(results(ip)%idx).EQ.0)) THEN
									!Take the minimum spacing of the two points
									IF(BGSpacing%Model>0)THEN
									     
										delta = MIN((Scale_Point(results(ip)%idx)*BGSpacing%BasicSize),d)
									ELSE IF(BGSpacing%Model<0)THEN
										
										delta = MIN((MINVAL(fMap_Point(:,:,results(ip)%idx))*BGSpacing%BasicSize),d)
									END IF

									geoD = Geo3D_Distance_SQ(Posit(:,results(ip)%idx),Posit(:,idxin))
									IF (geoD.LT.(delta*sqrt34*delta*sqrt34)) THEN
										
										helpFlag = 1
										Mark_Point(results(ip)%idx) = -999
									END IF
								END IF
								
								
								
							END DO
						

							
						END IF
						
						
						
					
				
					END IF
				END DO
			END IF
			
			!Tidy everything up
			CALL kdtree2_destroy(tree)
			IF (helpFlag .EQ. 1) THEN
			
				IF(Debug_Display>0)THEN
					WRITE(*,*)'Lattice Insertion: checking mutual proximity: removing points'
					WRITE(29,*)'Lattice Insertion: checking mutual proximity: removing points'
				END IF
			
				!Then some points need deleting
				CALL Remove_Point()
				CALL Next_Build()
				CALL Remove_tet()
				
				
			END IF
			Mark_Point(1:NB_Point)=0
		
		END IF
		
		IF (NB_Point .GT. NB_Point_Old) THEN
		
			IF(Debug_Display>0)THEN
						WRITE(*,*)'Lattice Insertion: number of points to insert',NB_Point-NB_Point_Old
						WRITE(29,*)'Lattice Insertion: number of points to inser',NB_Point-NB_Point_Old
			END IF

			!We can insert all the points
			CALL Insert_Points1(NB_Point_Old+1,NB_Point,-2)
			
			!Delete failed points
			CALL Remove_Point()
			CALL Next_Build()
			Mark_Point(1:NB_Point)=0	
			CALL Remove_tet()
			
			
			
			
			IF (NB_Point .EQ. NB_Point_Old) THEN
				done = 1
			ELSE
				IF(Debug_Display>0)THEN
						WRITE(*,*)'Lattice Insertion: number of points inserted',NB_Point-NB_Point_Old
						WRITE(29,*)'Lattice Insertion: number of points inserted',NB_Point-NB_Point_Old
				END IF
			
			
				pStart = NB_Point_Old+1
				pEnd = NB_Point
				NB_Point_Old = NB_Point
			
			END IF
		
		ELSE
			done = 1
		END IF
!		
		
		CALL LinkAssociation_Clear(PointAsso)
	END DO
	
    RR_Point(1:NB_Point) = 0.d0
 
 
  CALL Next_Build()	
	IF(Debug_Display>0)THEN
			
			WRITE(*,*)'Lattice Insertion completed in numLoops:',numLoops
			WRITE(29,*)'Lattice Insertion completed in numLoops:',numLoops
	END IF

END SUBROUTINE Lattice_Insert


!**************************************************************************
!>
!!  unitCell 
!!  This subroutine returns the unit cell around Pt at spacing d
!!
!<
!**************************************************************************
SUBROUTINE unitCellR(Pt,d,xyz)

	REAL*8, INTENT(IN)	:: Pt(3)
	REAL*8, INTENT(IN) 	:: d
	REAL*8, INTENT(OUT) :: xyz(3,14)
	REAL*8 				:: b,h,s
	
	b = (d/2.d0)
	h = (d/SQRT(2.d0))

	s = sqrt(3.d0/4.d0)
	
	xyz(:,1) = (/-1.d0*b,h,0.d0/) + Pt(:)
	xyz(:,2) = (/0.d0,h,h/)*s + Pt(:)
	xyz(:,3) = (/b,h,0.d0/) + Pt(:)
	xyz(:,4) = (/-1.d0*b,0,h/) + Pt(:)
	xyz(:,5) = (/b,0.d0,h/) + Pt(:)
	xyz(:,6) = (/-1.d0*b,-1.d0*h,0.d0/) + Pt(:)
	xyz(:,7) = (/0.d0,-1.d0*h,h/)*s + Pt(:)
	xyz(:,8) = (/b,-1.0d0*h,0.d0/) + Pt(:)
	xyz(:,9) = (/0.0d0,-1.0d0*h,-1.0d0*h/)*s + Pt(:)
	xyz(:,10) = (/-1.d0*b,0.d0,-1.0d0*h/) + Pt(:)
	xyz(:,11) = (/0.0d0,h,-1.0d0*h/)*s + Pt(:)
	xyz(:,12) = (/b,0.d0,-1.0d0*h/) + Pt(:)
	xyz(:,13) = (/2.d0*b,0.d0,0.d0/)*s + Pt(:)
	xyz(:,14) = (/-2.d0*b,0.d0,0.d0/)*s + Pt(:)
	


END SUBROUTINE unitCellR
