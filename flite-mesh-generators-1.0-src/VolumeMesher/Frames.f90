!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!   Creates structured ideal mesh in a box which can cover a surface,
!!    and find those nodes locating in the enclosed domain by this surface.
!!  @param[in]   common_Parameters::Surf
!!                           : the surface by which the domain enclosed.
!!  @param[out]  common_Parameters::FillingNodes 
!!                           : nodes locating in the domain,
!!                             excluding those points which close to the surface.
!<
!*******************************************************************************
SUBROUTINE Ideal_FillingMesh( )
  USE common_Parameters
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: DomainList(10)

  WRITE(*,*)'---- Set initial box ----'
  CALL Ideal_InitialBox(3)

  WRITE(*,*)'---- find filling nodes of an enclosed area ----'
  DomainList(1) = 2
  CALL Fill_EnclosedDomain(1, DomainList)

END SUBROUTINE Ideal_FillingMesh

!*******************************************************************************
!>
!!   Creates structured ideal mesh in a box which can cover a surface.
!!    Cut a stair-case domain from for the geometry with a gap,
!!    and find a list of nodes which locate in the gap
!!    between the geometry and the stair-case surface.
!!
!!    i.e. Ideal_InitialBox() + Ideal_StairCase().
!!
!!  @param[in]  common_Parameters::Surf
!!  @param[in]  common_Parameters::Ideal_Layer_Expand    
!<
!*******************************************************************************
SUBROUTINE Ideal_FrameMesh( )
  USE common_Parameters
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: LayerExpand, domainIndex


  IF(Ideal_Layer_Expand>0)THEN
     LayerExpand = Ideal_Layer_Expand
     domainIndex = 1
  ELSE
     LayerExpand = 3
     domainIndex = 2
  ENDIF

  WRITE(*,*)'---- Set initial box ----'
  CALL Ideal_InitialBox(LayerExpand)

  WRITE(*,*)'---- Cut StairCase Domain and Surface ----'
  CALL Ideal_StairCase(domainIndex, .TRUE.)

END SUBROUTINE Ideal_FrameMesh


!*******************************************************************************
!>
!!   Creates structured ideal mesh in a box which can cover a surface.
!!   @param[in]  LayerExpand   : the number of grids between
!!                               the box boundary to the surface.
!!   @param[in]  common_Parameters::Surf
!<
!*******************************************************************************
SUBROUTINE Ideal_InitialBox(LayerExpand)

  USE common_Constants
  USE common_Parameters
  USE array_allocator
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: LayerExpand
  INTEGER :: Nx, Ny, Nz, NB1, NB2
  INTEGER :: i, j, k, i1, j1, k1, k2, m, mt, ip, ip4(4), it, ie, ib, im(3)
  LOGICAL :: flag
  REAL*8  :: x,y,z, x0, XSize, Xindent, YZSize

  INTEGER  :: CubeSplitTet(4,6) = RESHAPE   &
       ( (/1,2,3,5,  3,2,7,5,  2,6,7,5,  2,4,3,7,  4,2,6,7,  4,6,8,7/), (/4,6/) )

  !---- Measure the domain


  CALL SurfaceMeasure()

  XSize   = BGSpacing%BasicSize
  Xindent = XSize/2.d0
  YZSize  = XSize/dsqrt(2.d0)

  XYZmax(1:3) = XYZmax(1:3) + LayerExpand * BGSpacing%BasicSize
  XYZmin(1:3) = XYZmin(1:3) - LayerExpand * BGSpacing%BasicSize
  NX = (XYZmax(1) - XYZmin(1)) / XSize  +1
  NY = (XYZmax(2) - XYZmin(2)) / YZSize +1
  NZ = (XYZmax(3) - XYZmin(3)) / YZSize +1

  WRITE(29,*)'LayerExpand=', LayerExpand, ' BGSpacing%BasicSize=', BGSpacing%BasicSize
  WRITE(29,*)'XYZmin=',XYZmin
  WRITE(29,*)'XYZmax=',XYZmax

  NB_Tet = Nx*Ny*Nz*6
  NB_Point = (Nx+1)*(Ny+1)*(Nz+1)
  WRITE(29,*)'Ideal_InitialBox:: NB_Tet, NB_Point=',NB_Tet, NB_Point

  nallc_Point = NB_Point + nallc_increase
  nallc_Tet   = NB_Tet + nallc_increase
  CALL Allocate_Point( )
  CALL Allocate_Tet( )

  !---  Create all the points and mark boundary ones

  ip = 0
  DO k=0,Nz
     z = XYZmin(3) + YZSize*k
     DO j=0,Ny
        y = XYZmin(2) + YZSize*j
        IF (MOD(j,2)==MOD(k,2)) THEN
           x0 = XYZmin(1) + Xindent
        ELSE
           x0 = XYZmin(1)
        ENDIF
        DO i=0,Nx
           ip = ip + 1
           x = x0 + XSize*i
           Posit(1,ip) = x
           Posit(2,ip) = y
           Posit(3,ip) = z
        ENDDO
     ENDDO
  ENDDO

  !----  Create elements

  ie = 0
  DO k=0,Nz-1
     DO j=0,Ny-1
        DO i=-1,Nx
           DO it=1,6 ! tetraheadra
              flag = .TRUE.
              DO m=1,4
                 mt = CubeSplitTet(m,it)
                 k1 = k
                 IF(mt>4) k1 = k1 + 1
                 j1 = j
                 IF(MOD(mt-1,4)>1) j1 = j1 + 1
                 i1 = ipar(i,j,k,mt)
                 IF (i1<0 .OR. i1>Nx) flag=.FALSE.
                 IF (j1<0 .OR. j1>Ny) flag=.FALSE.
                 IF (k1<0 .OR. k1>Nz) flag=.FALSE.
                 IF (i1==Nx .AND. (it==2 .OR. it==5) .AND.    &
                      MOD(j1,2)==MOD(k1,2)) flag=.FALSE.
                 ip = 1 + i1 + (Nx+1)*j1+(Nx+1)*(Ny+1)*k1
                 ip4(m) = ip
              ENDDO

              IF (flag) THEN
                 ie = ie+1
                 IF (ie>NB_Tet) THEN
                    PRINT*,'ie=',ie,' > NB_Tet=',NB_Tet
                    CALL Error_Stop ('  ')
                 ENDIF
                 IP_Tet(1:4,ie) = ip4(1:4)
              ENDIF ! flag
           ENDDO !it=1,6
        ENDDO !i=-1,Nx
     ENDDO !j=0,Ny-1
  ENDDO !k=0,Nz-1


  !--- set boundary triangles
  !---  Re-number points that boundary ones to be first
  CALL Next_Build()
  NB_BD_Tri = 0
  CALL Boundary_Associate(4)
  CALL Forward_BoundaryNodes()
  IP_BD_Tri(5,1:NB_BD_Tri) = 1

  WRITE(29,*)'Ideal_InitialBox:: NB_BD_Tri, NB_BD_Point=',NB_BD_Tri, NB_BD_Point


  IF(NB_BD_Tri /= 4*(Nx*Ny + Nx*Nz + Ny*nz))THEN
     WRITE(29,*)'Error--- : NB_BD_Tri,NB=',NB_BD_Tri, 4*(Nx*Ny + Nx*Nz + Ny*nz)
     CALL Error_Stop ('Ideal_InitialBox')
  ENDIF

  RETURN


CONTAINS

  !>  Private fuction of SUBROUTINE Ideal_InitialBox().
  FUNCTION ipar(i,j,k,mt) RESULT(ipp)
    IMPLICIT NONE
    INTEGER :: i,j,k,mt,ipp
    IF (MOD(j,2)==MOD(k,2)) THEN
       GOTO (30,31,31,32,31,32,31,32), mt
    ELSE
       GOTO (30,31,30,31,30,31,31,32), mt
    ENDIF
30  ipp = i
    RETURN
31  ipp = i+1
    RETURN
32  ipp = i+2
    RETURN
  END FUNCTION ipar

END SUBROUTINE Ideal_InitialBox

!*******************************************************************************
!>
!!  From the present mesh (ideal mesh generated by SUBROUTINE Ideal_InitialBox() )
!!    find a list of nodes which locate in an enclosed domain (defined by surfaces).
!!
!!  @param[in]   common_Parameters::Surf
!!                           : the surface by which the domain enclosed.
!!  @param[in]   numDomain   : the number of separated domains.
!!  @param[in]   DomainList  : the domain IDs.
!!  @param[out]  common_Parameters::FillingNodes 
!!                           : nodes locating in the domain,
!!                             excluding those points which close to the surface.
!<
!*******************************************************************************
SUBROUTINE Fill_EnclosedDomain(numDomain, DomainList)

  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  USE array_allocator
  IMPLICIT NONE

  INTEGER,INTENT(IN) :: numDomain, DomainList(*)
  INTEGER :: ip, ib, IT, i, j, n, layer, nlayer, sizeBD, itnb, ip3(3), np
  LOGICAL :: carryon
  REAL*8  :: rstd, r
  INTEGER, DIMENSION(:),  POINTER :: Mark_BD
  TYPE(Plane3D) :: aPlane

  Mark_Tet(1:NB_Tet) = 0
  Mark_Point(1:NB_BD_Point) = 1
  Mark_Point(NB_BD_Point+1:NB_Point) = 0

  ALLOCATE(Mark_BD(Surf%NB_Point))
  Mark_BD(1:Surf%NB_Point) = 0

  IT = 0
  nlayer = 0
  rstd = BGSpacing%BasicSize
  DO ib = 1, Surf%NB_Tri
     layer = Surf%IP_Tri(5,ib) + 1
     IF(layer<=1) CALL Error_Stop ('  layer<=1')
     nlayer = MAX(nlayer, layer)
     ip3(1:3) = Surf%IP_Tri(1:3,ib)
     aPlane = Plane3D_Build(Surf%Posit(:,ip3(1)),Surf%Posit(:,ip3(2)),Surf%Posit(:,ip3(3)))
     DO i=1,3
        IF(Mark_BD(ip3(i))>0)THEN
           IT = Mark_BD(ip3(i))
        ELSE
           CALL Locate_Node_Tet(Surf%Posit(:,ip3(i)),IT,1)
           Mark_BD(ip3(i)) = IT
        ENDIF
        DO j=1,4
           ip = IP_Tet(j,IT)
           IF(Mark_Point(ip)<0) CYCLE
           r = Plane3D_DistancePointToPlane(Posit(:,ip),aPlane)
           IF(r<rstd)THEN
              Mark_Point(ip) = -layer
           ELSE
              Mark_Point(ip) = layer
           ENDIF
        ENDDO
     ENDDO
  ENDDO

  DEALLOCATE(Mark_BD)

  DO layer = 1,nlayer
     DO IT=1,NB_Tet
        IF(Mark_Tet(IT)/=0) CYCLE
        IF(  Mark_Point(IP_Tet(1,IT))==-layer .OR.    &
             Mark_Point(IP_Tet(2,IT))==-layer .OR.    &
             Mark_Point(IP_Tet(3,IT))==-layer .OR.    &
             Mark_Point(IP_Tet(4,IT))==-layer) THEN
           Mark_Tet(IT) = -1
           WHERE(Mark_Point(IP_Tet(:,IT))==0) Mark_Point(IP_Tet(:,IT)) = layer
        ENDIF
     ENDDO
  ENDDO

  DO ip = 1, NB_Point
     IF(Mark_Point(ip)<0) Mark_Point(ip) = -Mark_Point(ip)
  ENDDO

  DO IT=1,NB_Tet
     IF(  Mark_Point(IP_Tet(1,IT))>1 .AND. Mark_Point(IP_Tet(2,IT))>1 .AND.  &
          Mark_Point(IP_Tet(3,IT))>1 .AND. Mark_Point(IP_Tet(4,IT))>1 )THEN
        Mark_Tet(IT) = -1
     ENDIF
  ENDDO


  DO layer = 1,nlayer
     DO
        carryon = .FALSE.
        DO IT=1,NB_Tet
           IF(Mark_Tet(IT)/=0) CYCLE
           IF(  Mark_Point(IP_Tet(1,IT))==layer .OR.    &
                Mark_Point(IP_Tet(2,IT))==layer .OR.    &
                Mark_Point(IP_Tet(3,IT))==layer .OR.    &
                Mark_Point(IP_Tet(4,IT))==layer) THEN
              Mark_Tet(IT) = layer
           ENDIF
        ENDDO

        DO IT=1,NB_Tet
           IF(Mark_Tet(IT)/=layer) CYCLE
           DO i=1,4
              IF(Mark_Point(IP_Tet(i,IT))==0)THEN
                 Mark_Point(IP_Tet(i,IT)) = layer
                 carryon = .TRUE.
              ENDIF
           ENDDO
        ENDDO

        IF(.NOT. carryon) EXIT
     ENDDO
  ENDDO


  !--- extract nodes
  Mark_Point(1:NB_Point) = 0
  DO n = 1,numDomain
     layer = DomainList(n)
     DO IT = 1,NB_Tet
        IF(Mark_Tet(IT)/=layer) CYCLE
        Mark_Point(IP_Tet(:,IT)) = 1
     ENDDO
  ENDDO
  np = 0
  DO ip=1,NB_Point
     IF(Mark_Point(ip)==1)THEN
       np = np+1
       Posit(:,np) = Posit(:,ip)
     ENDIF
  ENDDO
  WRITE(29,*)'Fill_EnclosedDomain::  NB_Point=', np

  CALL TetMeshStorage_NodeBackup(np, Posit,  FillingNodes)

END SUBROUTINE Fill_EnclosedDomain

!*******************************************************************************
!>
!!  Cut a stair-case domain from the present mesh
!!    (ideal mesh generated by SUBROUTINE Ideal_InitialBox() ) for
!!    the geometry with a gap,
!!    and find a list of nodes which locate in the gap
!!    between the geometry and the stair-case surface.
!!     
!!
!!  @param[in]  common_Parameters::Surf
!!                          : the geometry surface.
!!  @param[in]  domainIndex =1  : the domain is outside the surface.          \n
!!                          =2  : the domain is enclosed by the surface.
!!  @param[in]  isIdeal  = .true.  : the present mesh is an ideal mesh.       \n
!!                       =.false.  : the present mesh is NOT an ideal mesh.
!!  @param[in]  common_Parameters::Ideal_Layer_Gap     
!!  @param[out] common_Parameters::TetFrame   
!!                   :  Ideal mesh after remove the area enclosing the geometry
!!                      with a gap.  (i.e. the area between the stair-case
!!                      surface and out-boundary). The frame  mesh is save as
!!                      TetFrame if common_Parameters::Frame_Stitch==0; otherwise,
!!                      written to file *_f.plt and deleted.
!!  @param[out]  common_Parameters::FillingNodes 
!!                   : nodes locating in th gap between geometry and
!!                     the stair-case surface.    
!!  @param[out]  common_Parameters::IP_BD_Tri
!!                   : boundary triangulation, including the original
!!                     surface and the stair-case surface.
!<
!*******************************************************************************
SUBROUTINE Ideal_StairCase(domainIndex,isIdeal)

  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  USE array_allocator
  IMPLICIT NONE

  INTEGER,INTENT(IN) :: domainIndex
  LOGICAL,INTENT(IN) :: isIdeal
  INTEGER :: ip, ib, IT, i, j, n, sizeBD, itnb, ip3(3), ip4(4), itp(3), iw(3), k, k1, k2
  INTEGER :: NB1, NB2, isp, idom, nbs(100), iLoop
  LOGICAL :: carryon
  REAL*8  :: rstd, r, pp(3), pp3(3,3), fLayer, fLayer2, dd
  INTEGER, DIMENSION(:),  POINTER :: Mark_BD
  REAL*8,  DIMENSION(:),  POINTER :: fMark_Point
  TYPE(Plane3D) :: aPlane
  INTEGER, DIMENSION(10000) :: Type_Wall_Temp

  Mark_Point(1:NB_Point) = 0

  ALLOCATE(Mark_BD(Surf%NB_Point))
  Mark_BD(1:Surf%NB_Point) = 0
  Mark_Tet(1:NB_Tet) = 0

  !--- find boundary-close nodes which are very close to the surface
  !    mark them with -2
  IT = 0
  DO ib = 1, Surf%NB_Tri
     ip3(1:3) = Surf%IP_Tri(1:3,ib)
     pp3(:,:) = Surf%Posit(:,ip3(1:3))
     pp(:) = (pp3(:,1) + pp3(:,2) + pp3(:,3))/3.d0
     CALL SpacingStorage_GetMinGridSize(BGSpacing, pp,rstd)
     aPlane = Plane3D_Build(pp3(:,1), pp3(:,2), pp3(:,3))
     DO i=1,3
        IF(Mark_BD(ip3(i))>0)THEN
           IT = Mark_BD(ip3(i))
        ELSE
           CALL Locate_Node_Tet(pp3(:,i),IT,1)
           Mark_BD(ip3(i)) = IT
        ENDIF
        Mark_Tet(IT) = ib
        DO j=1,4
           ip = IP_Tet(j,IT)
           IF(Mark_Point(ip)==-2) CYCLE
           r = Plane3D_DistancePointToPlane(Posit(:,ip),aPlane)
           IF(r<rstd)THEN
              Mark_Point(ip) = -2
           ELSE
              Mark_Point(ip) = -1
           ENDIF
        ENDDO
     ENDDO

     DO k=1,3
        k1 = k
        k2 = k+1
        IF(k2>3) k2 = 1

        itp(:) = Mark_BD(ip3(1:3))
        pp3(:,:) = Surf%Posit(:,ip3(1:3))
        iw(:) = 1
        DO WHILE(itp(k1)/=itp(k2) .AND.  &
             Next_Tet(1,itp(k1))/=itp(k2) .AND. Next_Tet(2,itp(k1))/=itp(k2) .AND.  &
             Next_Tet(3,itp(k1))/=itp(k2) .AND. Next_Tet(4,itp(k1))/=itp(k2) )
           pp = (iw(1)*pp3(:,1) + iw(2)*pp3(:,2) + iw(3)*pp3(:,3))/(iw(1)+iw(2)+iw(3))
           CALL Locate_Node_Tet(pp,IT,1)
           IF(Mark_Tet(IT)/=ib)THEN
              Mark_Tet(IT) = ib
              DO j=1,4
                 ip = IP_Tet(j,IT)
                 IF(Mark_Point(ip)==-2) CYCLE
                 r = Plane3D_DistancePointToPlane(Posit(:,ip),aPlane)
                 IF(r<rstd)THEN
                    Mark_Point(ip) = -2
                 ELSE
                    Mark_Point(ip) = -1
                 ENDIF
              ENDDO
           ENDIF
           IF(IT==itp(k1) .OR. Next_Tet(1,IT)==itp(k1) .AND. Next_Tet(2,IT)==itp(k1) .AND.  &
                Next_Tet(3,IT)==itp(k1) .AND. Next_Tet(4,IT)==itp(k1) )THEN
              itp(k1) = IT
              pp3(:,k1) = pp
              iw(k2)= iw(k2) + 1
           ELSE IF(IT==itp(k2) .OR. Next_Tet(1,IT)==itp(k2) .AND. Next_Tet(2,IT)==itp(k2) .AND.  &
                Next_Tet(3,IT)==itp(k2) .AND. Next_Tet(4,IT)==itp(k2) )THEN
              itp(k2) = IT
              pp3(:,k2) = pp
              iw(k1)= iw(k1) + 1
           ELSE
              iw(k1)= iw(k1) + 1
           ENDIF
           IF(SUM(iw)>10)THEN
              EXIT
           ENDIF
        ENDDO

     ENDDO
  ENDDO

  DEALLOCATE(Mark_BD)

  Mark_Tet(1:NB_Tet) = 0
  DO IT=1,NB_Tet
     IF(  Mark_Point(IP_Tet(1,IT))==-2 .OR.    &
          Mark_Point(IP_Tet(2,IT))==-2 .OR.    &
          Mark_Point(IP_Tet(3,IT))==-2 .OR.    &
          Mark_Point(IP_Tet(4,IT))==-2) THEN
        WHERE(Mark_Point(IP_Tet(:,IT))==0) Mark_Point(IP_Tet(:,IT)) = -1
     ENDIF
  ENDDO
  DO IT=1,NB_Tet
     IF(  Mark_Point(IP_Tet(1,IT))<0 .AND.    &
          Mark_Point(IP_Tet(2,IT))<0 .AND.    &
          Mark_Point(IP_Tet(3,IT))<0 .AND.    &
          Mark_Point(IP_Tet(4,IT))<0) THEN
        Mark_Tet(IT) = -1
     ENDIF
  ENDDO

  Mark_Point(1:NB_BD_Point) = 1
  DO idom = 1,domainIndex
     CALL Populate_WholeDomain(idom)
     IF(idom<domainIndex)THEN
        DO IT=1,NB_Tet
           IF(Mark_Tet(IT)==idom)THEN
              WHERE(Mark_Point(IP_Tet(:,IT))<0) Mark_Point(IP_Tet(:,IT)) = idom+1
           ENDIF
        ENDDO

        CarryOn = .TRUE.
        DO WHILE(CarryOn)
           CarryOn = .FALSE.
           DO IT = 1, NB_Tet
              IF(Mark_Tet(IT)==-1)THEN
                 DO i=1,4
                    IF(Mark_Point(IP_Tet(i,IT))==idom+1)THEN
                       DO j=1,4
                          IF(Mark_Point(IP_Tet(j,IT))/=idom+1)THEN
                             Mark_Point(IP_Tet(j,IT)) = idom+1
                             CarryOn = .TRUE.
                          ENDIF
                       ENDDO
                    ENDIF
                 ENDDO
              ENDIF
           ENDDO
        ENDDO

     ENDIF
  ENDDO

  IF(Debug_Display>2)THEN
     nbs(:) = 0
     DO it = 1,NB_Tet
        i = Mark_Tet(IT)
        IF(i>0)THEN
           nbs(i) = nbs(i) + 1
        ELSE IF(i==0)THEN
           nbs(domainIndex+1) = nbs(domainIndex+1) +1
        ELSE
           nbs(domainIndex+2) = nbs(domainIndex+2) +1
        ENDIF
     ENDDO
     WRITE(29,*)' number of cells with mark<0, =0, 1,2,.. '
     WRITE(29,*)nbs(domainIndex+2),nbs(domainIndex+1),nbs(1:domainIndex)
  ENDIF

  !--- extract domain and mark boundary points as 1
  Mark_Point(1:NB_Point) = 0
  NB1 = 0
  DO IT=1,NB_Tet
     IF(Mark_Tet(IT)==domainIndex)THEN
        NB1 = NB1 + 1
        IP_Tet(:,NB1) = IP_Tet(:,IT)
     ELSE
        Mark_Point(IP_Tet(:,IT)) = 1
     ENDIF
  ENDDO
  NB_Tet = NB1


  !--- populate Ideal_Layer_Gap layers from boundary-close nodes

  IF(isIdeal)THEN

     Mark_Tet(1:NB_Tet) = 0
     DO isp = 1,Ideal_Layer_Gap
        DO IT=1,NB_Tet
           IF(Mark_Tet(IT)/=0) CYCLE
           IF(  Mark_Point(IP_Tet(1,IT))==isp .OR.    &
                Mark_Point(IP_Tet(2,IT))==isp .OR.    &
                Mark_Point(IP_Tet(3,IT))==isp .OR.    &
                Mark_Point(IP_Tet(4,IT))==isp) THEN
              Mark_Tet(IT) = 1
              WHERE(Mark_Point(IP_Tet(:,IT))==0) Mark_Point(IP_Tet(:,IT)) = isp+1
           ENDIF
        ENDDO
     ENDDO

  ELSE

     fLayer = Ideal_Dist_Gap * BGSpacing%BasicSize
     fLayer2 = 1.001*fLayer

     ALLOCATE(fMark_Point(NB_Point))
     fMark_Point(1:NB_Point) = fLayer2
     WHERE(Mark_Point(:)==1) fMark_Point(:) = 0
     Mark_Point(:) = 0
     Mark_Tet(:) = 0

     DO iLoop = 1,100000
        CarryOn = .FALSE.
        DO IT=1,NB_Tet
           ip4(:) = IP_Tet(:,IT)
           DO i=1,4
              IF(Mark_Point(ip4(i))>=Ideal_Layer_Gap) CYCLE
              IF(fMark_Point(ip4(i))>=fLayer) CYCLE
              Mark_Tet(IT) = 1
              DO j=1,4
                 IF(i==j) CYCLE
                 IF(fMark_Point(ip4(j))<=fMark_Point(ip4(i))) CYCLE
                 dd =   ABS(Posit(1,ip4(j))-Posit(1,ip4(i)))   &
                      + ABS(Posit(2,ip4(j))-Posit(2,ip4(i)))   &
                      + ABS(Posit(3,ip4(j))-Posit(3,ip4(i)))
                 IF(fMark_Point(ip4(j)) > fMark_Point(ip4(i))+dd)THEN
                    fMark_Point(ip4(j)) = fMark_Point(ip4(i))+dd
                    Mark_Point(ip4(j)) = Mark_Point(ip4(i)) + 1
                    CarryOn = .TRUE.
                 ENDIF
              ENDDO
           ENDDO
        ENDDO
        IF(.NOT. CarryOn) EXIT
     ENDDO

     DO ip = 1,NB_Point
        IF(fMark_Point(ip)<=fLayer)THEN
           Mark_Point(ip) = 1
        ELSE
           Mark_Point(ip) = 0
        ENDIF
     ENDDO

     DEALLOCATE(fMark_Point)

  ENDIF

  DO IT=1,NB_Tet
     IF(Mark_Tet(IT)==1) Mark_Point(IP_Tet(:,IT)) = 1
  ENDDO

  DO IT=1,NB_Tet
     IF(Mark_Tet(IT)/=0) CYCLE
     IF(  Mark_Point(IP_Tet(1,IT))/=0 .AND. Mark_Point(IP_Tet(2,IT))/=0 .AND.  &
          Mark_Point(IP_Tet(3,IT))/=0 .AND. Mark_Point(IP_Tet(4,IT))/=0 )THEN
        Mark_Tet(IT) = 1
     ENDIF
  ENDDO

  !--- check if out-boundary being touched
  IF(domainIndex==1)THEN
     DO ip=1,NB_BD_Point
        IF(Mark_Point(ip)/=0)THEN
           WRITE(29,*)'Error--- affected layer touch the out-boundary'
           WRITE(29,*)'ip,mark,po=',ip,Mark_Point(ip),REAL(Posit(:,ip))
           CALL Error_Stop ('Ideal_StairCase')
        ENDIF
     ENDDO
  ENDIF

  !--- remove those elements with Mark_Tet(IT)>0 to generate a gap
  NB1 = 0
  Mark_Point(1:NB_Point) = 0
  DO IT=1,NB_Tet
     IF(Mark_Tet(IT)==0)THEN
        NB1 = NB1 + 1
        IP_Tet(:,NB1) = IP_Tet(:,IT)
        Mark_Point(IP_Tet(:,NB1)) = -1
     ELSE
        WHERE(Mark_Point(IP_Tet(:,IT))==0) Mark_Point(IP_Tet(:,IT)) = 1
     ENDIF
  ENDDO
  NB_Tet = NB1

  !--- save the points left in the gap
  CALL Sort_Nodes(NB1, NB2)
  CALL TetMeshStorage_NodeBackup(NB_Point-NB2+1, Posit(:,NB2:NB_Point),  FillingNodes)
  WRITE(29,*)'No. of points in the gap=',NB_Point-NB2+1

  NB_Point = NB1

  !--- count boundary triangles (far field and staircase boundary)
  NB_BD_Tri = 0
  CALL Next_Build()
  CALL Boundary_Associate(4)

  IF(domainIndex==1)THEN
     !--- do not consider out-boundary of the ideal mesh
     NB1 = 0
     DO ib = 1,NB_BD_Tri
        IF(IP_BD_Tri(1,ib)>NB_BD_Point)THEN
           NB1 = NB1 +1
           IP_BD_Tri(:,NB1) = IP_BD_Tri(:,ib)
        ENDIF
     ENDDO
     NB_BD_Tri = NB1
  ENDIF

  !--- sort boundary points
  CALL Forward_BoundaryNodes()

  WRITE(29,*)'TetFrame : NB_Point, NB_Tet=',NB_Point,NB_Tet
  CALL TetMeshStorage_NodeBackup(NB_Point, Posit,  TetFrame)
  CALL TetMeshStorage_CellBackup(NB_Tet,   IP_Tet, TetFrame)
  IF(Frame_Stitch==0)THEN
     WRITE(*,*)' '
     WRITE(*,*)'---- Output frame mesh----'
     CALL  TetMeshStorage_Output(JobName(1:JobNameLength)//'_f',JobNameLength+2,TetFrame)
     CALL  TetMeshStorage_Clear(TetFrame)
  ENDIF


  !--- make an enclosed surface by merge object surface and staircase surface
  NB_BD_Point = NB_BD_Point + Surf%NB_Point
  NB_Point    = NB_BD_Point             !--- discard non-boundary points
  DO WHILE(NB_Point>nallc_Point)
     CALL ReAllocate_Point()
  ENDDO
  DO IP = NB_Point, Surf%NB_Point+1, -1
     Posit(:,IP) = Posit(:,IP-Surf%NB_Point)
  ENDDO
  DO IP = 1, Surf%NB_Point
     Posit(:,IP) = Surf%Posit(:,IP)
  ENDDO

  NB_BD_Tri   = NB_BD_Tri + Surf%NB_Tri
  CALL allc_2Dpointer(IP_BD_Tri, 5, NB_BD_Tri, 'IP_BD_Tri')
  DO ib = NB_BD_Tri, Surf%NB_Tri+1, -1
     IP_BD_Tri(1,ib) = IP_BD_Tri(1,ib-Surf%NB_Tri) + Surf%NB_Point
     !---change orientation
     IP_BD_Tri(2,ib) = IP_BD_Tri(3,ib-Surf%NB_Tri) + Surf%NB_Point
     IP_BD_Tri(3,ib) = IP_BD_Tri(2,ib-Surf%NB_Tri) + Surf%NB_Point
     IP_BD_Tri(4,ib) = IP_BD_Tri(4,ib-Surf%NB_Tri)
     IP_BD_Tri(5,ib) = 1
  ENDDO

  Type_Wall_Temp(:) = 1
  DO ib = 1, Surf%NB_Tri
     IP_BD_Tri(1:5,ib) = Surf%IP_Tri(1:5,ib)
     IP_BD_Tri(5,ib) = Surf%IP_Tri(5,ib) + 1 
  ENDDO
  DO ib = 1,Surf%NB_Surf
    Type_Wall_Temp(ib+1) = Type_Wall(ib)
  END DO
  DO ib = 1,Surf%NB_Surf+1
    Type_Wall(ib) = Type_Wall_Temp(ib)
  END DO



  !--- fill the gap with Delaunay grids
  CALL SurfaceRestore()
  IF(Debug_Display>3)THEN
     CALL  SurfaceMeshStorage_Output(JobName(1:JobNameLength)//'_temp',JobNameLength+5,Surf)
  ENDIF

END SUBROUTINE Ideal_StairCase


!*******************************************************************************
!>
!!  Build quad-grid mesh in a cubic domain, cut a stair-case domain covery
!!    the geometry with a gap. Set pyramids connecting frame and the gap.   
!!
!!  @param[in]  common_Parameters::Surf       
!!  @param[in]  common_Parameters::Ideal_Layer_Expand       
!!  @param[in]  common_Parameters::Ideal_Layer_Gap
!!                   : the number of grids in the gap between geometry
!!                      and stair-case surface.
!!  @param[out]  common_Parameters::HybFrame    
!!                      the frame hybrid mesh (quads & pyramids) is save as
!!                      HybFrame if common_Parameters::Frame_Stitch==0;
!!                      otherwise, written to file *_hyb.plt and deleted.
!!  @param[out]  common_Parameters::IP_BD_Tri
!!                   : boundary triangulation, including the original
!!                     surface and the stair-case surface.
!<
!*******************************************************************************
SUBROUTINE Hybrid_StairCase( )

  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: NI,NJ,NK, i,j,k, np,nel,neb, ib, in, ilayer, n, m, nm, ip, ie
  INTEGER :: IFace, nallc
  INTEGER :: neighb(1:6), i1(3), i2(3), ip3(3), ip4(4)
  LOGICAL :: carryon
  REAL*8  :: xd, Poo(3)
  INTEGER, DIMENSION(:),  POINTER :: CellMark, NodeMark
  TYPE(NodeNetTreeType) :: Face_Tree
  LOGICAL :: ex
  INTEGER, DIMENSION(10000) :: Type_Wall_Temp

  IF(Ideal_Layer_Expand>0)THEN
     !---- Measure the domain
     CALL SurfaceMeasure()
     XYZmax(1:3) = XYZmax(1:3) + Ideal_Layer_Expand * BGSpacing%BasicSize
     XYZmin(1:3) = XYZmin(1:3) - Ideal_Layer_Expand * BGSpacing%BasicSize
  ELSE
     XYZmin(1:3) = FrameBox(1:3)
     XYZmax(1:3) = FrameBox(4:6)
     IF(XYZmin(1)>=XYZmax(1) .OR. XYZmin(2)>=XYZmax(2) .OR. XYZmin(3)>=XYZmax(3))THEN
        WRITE(29,*)'Error---- not a proper domain:',XYZmin(1),XYZmax(1)
        CALL Error_Stop ('Hybrid_StairCase')
     ENDIF
  ENDIF

  NI = (XYZmax(1) - XYZmin(1)) / BGSpacing%BasicSize +1
  NJ = (XYZmax(2) - XYZmin(2)) / BGSpacing%BasicSize +1
  NK = (XYZmax(3) - XYZmin(3)) / BGSpacing%BasicSize +1

  WRITE(29,*)'XYZmin=',XYZmin
  WRITE(29,*)'XYZmax=',XYZmax

  WRITE(29,*)'Cubic_InitialBox:: NI,NJ,NK=', NI, NJ, NK

  HybFrame%NB_Hex   = NI*NJ*NK
  HybFrame%NB_Point = (NI+1)*(NJ+1)*(NK+1)

  CALL allc_2Dpointer(HybFrame%IP_Hex, 8,   HybFrame%NB_Hex,   'IP_Hex')
  CALL allc_2Dpointer(HybFrame%Posit,  3, 2*HybFrame%NB_Point, 'Posit')

  DO I=1,NI+1
     DO J=1,NJ+1
        DO k=1,NK+1
           np = (K-1)*(NI+1)*(NJ+1) + (J-1)*(NI+1) + I
           HybFrame%Posit(1,np)= XYZmin(1) + (I-1)*BGSpacing%BasicSize
           HybFrame%Posit(2,np)= XYZmin(2) + (J-1)*BGSpacing%BasicSize
           HybFrame%Posit(3,np)= XYZmin(3) + (K-1)*BGSpacing%BasicSize
        ENDDO
     ENDDO
  ENDDO

  DO I=1,NI
     DO J=1,NJ
        DO k=1,NK
           nel= (K-1)*NI*NJ + (J-1)*NI +I
           np = (K-1)*(NI+1)*(NJ+1) + (J-1)*(NI+1) + I
           HybFrame%IP_Hex(1,nel) = np
           HybFrame%IP_Hex(2,nel) = np + 1
           HybFrame%IP_Hex(3,nel) = np + 1 + (NI+1)
           HybFrame%IP_Hex(4,nel) = np     + (NI+1)
           HybFrame%IP_Hex(5,nel) = np              + (NI+1)*(NJ+1)
           HybFrame%IP_Hex(6,nel) = np + 1          + (NI+1)*(NJ+1)
           HybFrame%IP_Hex(7,nel) = np + 1 + (NI+1) + (NI+1)*(NJ+1)
           HybFrame%IP_Hex(8,nel) = np     + (NI+1) + (NI+1)*(NJ+1)
        ENDDO
     ENDDO
  ENDDO

  ALLOCATE(CellMark(HybFrame%NB_Hex))
  CellMark(:) = 0

  !--- find boundary-close nodes which are very close to the surface
  !    mark them with -1
  DO ib = 1, Surf%NB_Tri
     ip3(1:3) = Surf%IP_Tri(1:3,ib)
     i1(:) = (/ NI, NJ, NK /)
     i2(:) = 1
     DO n=1,3
        DO i=1,3
           in = INT(( Surf%Posit(n,ip3(i)) - XYZmin(n) ) / BGSpacing%BasicSize) + 1
           i1(n) = MIN(i1(n),in)
           i2(n) = MAX(i2(n),in)
        ENDDO
     ENDDO

     DO i=i1(1),i2(1)
        DO j=i1(2),i2(2)
           DO k=i1(3),i2(3)
              nel= (K-1)*NI*NJ + (J-1)*NI +I
              CellMark(nel) = -1
           ENDDO
        ENDDO
     ENDDO
  ENDDO

  neighb(1:6) = (/-1,1,-NI,NI,-NI*NJ,NI*NJ/)
  DO i=1, NI
     DO j=1, NJ
        DO k=1, NK
           IF(i<=2 .OR. i>=NI-1 .OR. j<=2 .OR. j>=NJ-1 .OR. k<=2 .OR. k>=NK-1)THEN
              nel= (K-1)*NI*NJ + (J-1)*NI +I
              CellMark(nel) = 1
           ENDIF
        ENDDO
     ENDDO
  ENDDO

  !--- populate Ideal_Layer_Gap layers from boundary-close nodes
  DO ilayer = 1,Ideal_Layer_Gap
     DO i=2, NI-1
        DO j=2, NJ-1
           DO k=2, NK-1
              nel= (K-1)*NI*NJ + (J-1)*NI +I
              IF(CellMark(nel)==-ilayer)THEN
                 DO m=1,6
                    neb = nel + neighb(m)
                    IF(CellMark(neb)==0) CellMark(neb) = -ilayer-1
                 ENDDO
              ENDIF
           ENDDO
        ENDDO
     ENDDO
  ENDDO

  !--- colour the out-domain
  DO ilayer = 1, NI+NJ+NK
     carryon = .FALSE.
     DO i=2, NI-1
        DO j=2, NJ-1
           DO k=2, NK-1
              nel= (K-1)*NI*NJ + (J-1)*NI +I
              IF(CellMark(nel)==ilayer)THEN
                 DO m=1,6
                    neb = nel + neighb(m)
                    IF(CellMark(neb)==0) CellMark(neb) = ilayer+1
                 ENDDO
                 carryon = .TRUE.
              ENDIF
           ENDDO
        ENDDO
     ENDDO
     IF(.NOT. carryon) EXIT
  ENDDO

  !--- set pyramids
  xd = (dsqrt(2.d0)-1.d0)/2.d0 * BGSpacing%BasicSize

  CALL allc_2Dpointer(HybFrame%IP_Pyr, 5, HybFrame%NB_Hex,   'IP_Hex')
  HybFrame%NB_Pyr = 0
  CALL allc_1Dpointer(Mark_Point, 2*HybFrame%NB_Point, 'Mark_Point')
  Mark_Point(1:HybFrame%NB_Point) = 0

  DO i=2, NI-1
     DO j=2, NJ-1
        DO k=2, NK-1
           nel= (K-1)*NI*NJ + (J-1)*NI +I
           IF(CellMark(nel)<=0)THEN
              nm = 0
              DO m=1,6
                 neb = nel + neighb(m)
                 IF(CellMark(neb)>0)THEN
                    HybFrame%NB_Pyr = HybFrame%NB_Pyr + 1
                    HybFrame%IP_Pyr(1:4,HybFrame%NB_Pyr) = HybFrame%IP_Hex(iQuad_Hex(:,m),nel)

                    IF(nm==0)THEN
                       HybFrame%NB_Point = HybFrame%NB_Point + 1
                       Poo(:) = ( HybFrame%Posit(:,HybFrame%IP_Hex(1,nel))    &
                            + HybFrame%Posit(:,HybFrame%IP_Hex(7,nel)) )/2.d0
                       HybFrame%Posit(:,HybFrame%NB_Point) = Poo(:)
                       IF(m==1)THEN
                          HybFrame%Posit(1,HybFrame%NB_Point) = Poo(1) + xd
                       ELSE IF(m==2)THEN
                          HybFrame%Posit(1,HybFrame%NB_Point) = Poo(1) - xd
                       ELSE IF(m==3)THEN
                          HybFrame%Posit(2,HybFrame%NB_Point) = Poo(2) + xd
                       ELSE IF(m==4)THEN
                          HybFrame%Posit(2,HybFrame%NB_Point) = Poo(2) - xd
                       ELSE IF(m==5)THEN
                          HybFrame%Posit(3,HybFrame%NB_Point) = Poo(3) + xd
                       ELSE IF(m==6)THEN
                          HybFrame%Posit(3,HybFrame%NB_Point) = Poo(3) - xd
                       ENDIF
                       Mark_Point(HybFrame%NB_Point) = 3
                    ELSE
                       HybFrame%Posit(:,HybFrame%NB_Point) = Poo(:)
                       Mark_Point(HybFrame%NB_Point) = 2
                    ENDIF
                    HybFrame%IP_Pyr(5,HybFrame%NB_Pyr) = HybFrame%NB_Point
                    nm = nm + 1
                 ENDIF
              ENDDO
           ENDIF
        ENDDO
     ENDDO
  ENDDO

  ALLOCATE(NodeMark(HybFrame%NB_Point))

  !---search far field quad boundary
  NodeMark(:) = 0
  DO I=1,NI+1
     DO J=1,NJ+1
        DO k=1,NK+1
           IF(I==1 .or. I==NI+1 .or. J==1 .or. j==NJ+1 .or. k==1 .or. k==NK+1)THEN
              np = (K-1)*(NI+1)*(NJ+1) + (J-1)*(NI+1) + I
              NodeMark(np) = 2
           ENDIF
        ENDDO
     ENDDO
  ENDDO

  nallc = 100000
  CALL allc_2Dpointer(HybridSurf%IP_Quad, 5, nallc, 'IP_Quad')
  HybridSurf%NB_Quad = 0
  DO ie = 1, HybFrame%NB_Hex
     DO i = 1,6
        ip4(:) = HybFrame%IP_Hex(iQuad_Hex(:,i), ie)
        IF(  NodeMark(ip4(1))/=2 .OR. NodeMark(ip4(2))/=2 .OR.  &
             NodeMark(ip4(3))/=2 .OR. NodeMark(ip4(4))/=2) CYCLE
        HybridSurf%NB_Quad = HybridSurf%NB_Quad + 1
        IF(HybridSurf%NB_Quad>nallc)THEN
           nallc = nallc+100000
           CALL allc_2Dpointer(HybridSurf%IP_Quad, 5, nallc, 'IP_Quad')
        ENDIF
        HybridSurf%IP_Quad(1:4,HybridSurf%NB_Quad) = ip4(:)
        HybridSurf%IP_Quad(5,  HybridSurf%NB_Quad) = 1
     ENDDO
  ENDDO

  !--- recount nodes

  NodeMark(:) = -999

  DO ie = 1, HybFrame%NB_Hex
     IF(CellMark(ie)>0)  NodeMark(HybFrame%IP_Hex(:,ie)) = 1
  ENDDO
  DO ie = 1, HybFrame%NB_Pyr
     NodeMark(HybFrame%IP_Pyr(5,ie)) = 1
     Mark_Point(HybFrame%IP_Pyr(1:4,ie)) = 1
  ENDDO

  np = 0
  DO ip = 1, HybFrame%NB_Point
     IF(NodeMark(ip)==1)THEN
        np = np+1
        NodeMark(ip) = np
        HybFrame%Posit(:,np) = HybFrame%Posit(:,ip)
        Mark_Point(np) = Mark_Point(ip)
     ENDIF
  ENDDO
  HybFrame%NB_Point = np

  !--- recount cells

  nel = 0
  DO ie = 1, HybFrame%NB_Hex
     IF(CellMark(ie)>0)THEN
        nel = nel + 1
        HybFrame%IP_Hex(:,nel) = NodeMark(HybFrame%IP_Hex(:,ie))
     ENDIF
  ENDDO
  HybFrame%NB_Hex = nel

  DO ie = 1, HybFrame%NB_Pyr
     HybFrame%IP_Pyr(:,ie) = NodeMark(HybFrame%IP_Pyr(:,ie))
  ENDDO

  DO ib = 1, HybridSurf%NB_Quad
     ip4(:) = NodeMark(HybridSurf%IP_Quad(1:4,ib))
     IF(  ip4(1)==0 .OR. ip4(2)==0 .OR. ip4(3)==0 .OR. ip4(4)==0)THEN
          CALL Error_Stop (' wrong far-quad')
     ENDIF
     HybridSurf%IP_Quad(1:4,ib) = ip4(:)
  ENDDO

  !--- count triangles

  CALL allc_2Dpointer(Surf%Posit,  3, Surf%NB_Point+4*HybFrame%NB_Pyr, ' ')
  CALL allc_2Dpointer(Surf%IP_Tri, 5, Surf%NB_Tri  +4*HybFrame%NB_Pyr, ' ')
  CALL NodeNetTree_Allocate(3, HybFrame%NB_Point, 4*HybFrame%NB_Pyr, Face_Tree)

  nel = Surf%NB_Tri
  DO ie = 1, HybFrame%NB_Pyr
     DO i=1,4
        ip3(1) = HybFrame%IP_Pyr(iTri_Pyr(1,i),ie)
        ip3(2) = HybFrame%IP_Pyr(iTri_Pyr(3,i),ie)
        ip3(3) = HybFrame%IP_Pyr(iTri_Pyr(2,i),ie)
        CALL NodeNetTree_SearchAdd(3, ip3, Face_Tree, IFace)
        ib = IFace + Surf%NB_Tri
        IF(ib>nel)THEN
           nel = ib
           Surf%IP_Tri(1:3,nel) = ip3
        ELSE
           Surf%IP_Tri(1,ib) = 0
        ENDIF
     ENDDO
  ENDDO

  NodeMark(:) = -999
  DO ib = Surf%NB_Tri+1,nel
     IF(Surf%IP_Tri(1,ib)>0) NodeMark(Surf%IP_Tri(1:3,ib)) = 1
  ENDDO

  np = Surf%NB_Point
  DO ip = 1,  HybFrame%NB_Point
     IF(NodeMark(ip)==1)THEN
        Surf%NB_Point = Surf%NB_Point +1
        NodeMark(ip)  = Surf%NB_Point
        Surf%Posit(:,Surf%NB_Point)  = HybFrame%Posit(:,ip)
     ENDIF
  ENDDO

  CALL allc_1Dpointer(Mark_Surf_Point,  Surf%NB_Point, 'Mark_Surf_Point')
  Mark_Surf_Point(1:Surf%NB_Point) = 0
  DO ip = 1,  HybFrame%NB_Point
     IF(NodeMark(ip)>0)THEN
        Mark_Surf_Point(NodeMark(ip)) = Mark_Point(ip)
     ENDIF
  ENDDO


  IF(Debug_Display>1)THEN
     DO ip = 1,np
        IF(Mark_Surf_Point(ip)/=0) CALL Error_Stop ('Hybrid_StairCase 2')
     ENDDO
     DO ip = np+1,Surf%NB_Point
        IF(Mark_Surf_Point(ip)<=0) CALL Error_Stop ('Hybrid_StairCase 3')
     ENDDO
  ENDIF

  ALLOCATE(HybridSurf%IP_Tri(5, nel-Surf%NB_Tri))
  Surf%NB_Surf = Surf%NB_Surf + 1
  IFace = Surf%NB_Tri
  DO ib = Surf%NB_Tri+1,nel
     IF(Surf%IP_Tri(1,ib)>0)THEN
        IFace = IFace + 1
        ip3 = Surf%IP_Tri(1:3,ib)
        HybridSurf%IP_Tri(1:3,IFace-Surf%NB_Tri) = (/ip3(1),ip3(3),ip3(2)/)
        HybridSurf%IP_Tri(4:5,IFace-Surf%NB_Tri) = (/0, 2/)
        ip3(:) = NodeMark(ip3(:))
        CALL sort_ipp_tri(ip3)
        Surf%IP_Tri(1:3,IFace) = ip3
        Surf%IP_Tri(4:5,IFace) = (/0, 1/)
     ENDIF
  ENDDO

  DO ib = 1,Surf%NB_Tri
    Surf%IP_Tri(5,ib) = Surf%IP_Tri(5,ib) + 1
  END DO
  Type_Wall_Temp(:) = 1
  DO ib = 1,Surf%NB_Surf
    Type_Wall_Temp(ib+1) = Type_Wall(ib)
  END DO
  DO ib = 1,Surf%NB_Surf+1
    Type_Wall(ib) = Type_Wall_Temp(ib)
  END DO

  HybridSurf%NB_Tri = IFace - Surf%NB_Tri
  Surf%NB_Tri = IFace

  
  WRITE(29,*)'Updated Surface: NB_Point, NB_Tri=',  Surf%NB_Point, Surf%NB_Tri
  IF(Debug_Display>3)THEN
     CALL  SurfaceMeshStorage_Output(JobName(1:JobNameLength)//'_temp',JobNameLength+5,Surf)
  ENDIF

  !--- write hybrid-mesh
  WRITE(29,*)'Hybrid Mesh: NB_Point, NB_Hex, NB_Pyr=',    &
       HybFrame%NB_Point, HybFrame%NB_Hex, HybFrame%NB_Pyr
  CALL HybridMeshStorage_Output(TRIM(JobName)//'_hyb',JobNameLength+4,    &
                                -1,HybFrame,HybridSurf)

  CALL NodeNetTree_Clear(Face_Tree)
  DEALLOCATE(CellMark, NodeMark)
  IF(Frame_Stitch==0)THEN
     CALL HybridMeshStorage_Clear(HybFrame)
     CALL SurfaceMeshStorage_Clear(HybridSurf)
  ENDIF



END SUBROUTINE Hybrid_StairCase

!*******************************************************************************
!>
!!  Build quad-grid mesh in a cubic domain, cut a stair-case domain covery
!!    the geometry with a gap. Set wedges connecting frame and the gap.   
!!
!!  @param[in]  common_Parameters::Surf       
!!  @param[in]  common_Parameters::Ideal_Layer_Expand       
!!  @param[in]  common_Parameters::Ideal_Layer_Gap
!!                   : the number of grids in the gap between geometry
!!                      and stair-case surface.
!!  @param[out]  common_Parameters::HybFrame    
!!                      the frame hybrid mesh (quads & wedges) is save as
!!                      HybFrame if common_Parameters::Frame_Stitch==0;
!!                      otherwise, written to file *_hyb.plt and deleted.
!!  @param[out]  common_Parameters::IP_BD_Tri
!!                   : boundary triangulation, including the original
!!                     surface and the stair-case surface.
!<
!*******************************************************************************
SUBROUTINE Wedge_StairCase( )

  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: NI,NJ,NK, i,j,k, np,nel,neb, ib, in, ilayer, n, m, ip, ie
  INTEGER :: nwedge, nallc
  INTEGER :: neighb(1:6), i1(3), i2(3), ip3(3), ip4(4)
  INTEGER :: ijk1(3), ijk2(3)
  LOGICAL :: carryon
  INTEGER, DIMENSION(:),  POINTER :: CellMark, NodeMark
  LOGICAL :: ex
  INTEGER, DIMENSION(10000) :: Type_Wall_Temp


  IF(Ideal_Layer_Expand>0)THEN
     !---- Measure the domain
     CALL SurfaceMeasure()
     XYZmax(1:3) = XYZmax(1:3) + Ideal_Layer_Expand * BGSpacing%BasicSize
     XYZmin(1:3) = XYZmin(1:3) - Ideal_Layer_Expand * BGSpacing%BasicSize
  ELSE
     XYZmin(1:3) = FrameBox(1:3)
     XYZmax(1:3) = FrameBox(4:6)
     IF(XYZmin(1)>=XYZmax(1) .OR. XYZmin(2)>=XYZmax(2) .OR. XYZmin(3)>=XYZmax(3))THEN
        WRITE(29,*)'Error---- not a proper domain:',XYZmin(1),XYZmax(1)
        CALL Error_Stop ('Wedge_StairCase')
     ENDIF
  ENDIF

  NI = (XYZmax(1) - XYZmin(1)) / BGSpacing%BasicSize +1
  NJ = (XYZmax(2) - XYZmin(2)) / BGSpacing%BasicSize +1
  NK = (XYZmax(3) - XYZmin(3)) / BGSpacing%BasicSize +1

  WRITE(29,*)'XYZmin=',XYZmin
  WRITE(29,*)'XYZmax=',XYZmax

  WRITE(29,*)'Cubic_InitialBox:: NI,NJ,NK=', NI, NJ, NK

  HybFrame%NB_Hex   = NI*NJ*NK
  HybFrame%NB_Point = (NI+1)*(NJ+1)*(NK+1)

  CALL allc_2Dpointer(HybFrame%IP_Hex, 8,   HybFrame%NB_Hex,   'IP_Hex')
  CALL allc_2Dpointer(HybFrame%Posit,  3, 2*HybFrame%NB_Point, 'Posit')

  DO I=1,NI+1
     DO J=1,NJ+1
        DO k=1,NK+1
           np = (K-1)*(NI+1)*(NJ+1) + (J-1)*(NI+1) + I
           HybFrame%Posit(1,np)= XYZmin(1) + (I-1)*BGSpacing%BasicSize
           HybFrame%Posit(2,np)= XYZmin(2) + (J-1)*BGSpacing%BasicSize
           HybFrame%Posit(3,np)= XYZmin(3) + (K-1)*BGSpacing%BasicSize
        ENDDO
     ENDDO
  ENDDO

  DO I=1,NI
     DO J=1,NJ
        DO k=1,NK
           nel= (K-1)*NI*NJ + (J-1)*NI +I
           np = (K-1)*(NI+1)*(NJ+1) + (J-1)*(NI+1) + I
           HybFrame%IP_Hex(1,nel) = np
           HybFrame%IP_Hex(2,nel) = np + 1
           HybFrame%IP_Hex(3,nel) = np + 1 + (NI+1)
           HybFrame%IP_Hex(4,nel) = np     + (NI+1)
           HybFrame%IP_Hex(5,nel) = np              + (NI+1)*(NJ+1)
           HybFrame%IP_Hex(6,nel) = np + 1          + (NI+1)*(NJ+1)
           HybFrame%IP_Hex(7,nel) = np + 1 + (NI+1) + (NI+1)*(NJ+1)
           HybFrame%IP_Hex(8,nel) = np     + (NI+1) + (NI+1)*(NJ+1)
        ENDDO
     ENDDO
  ENDDO

  ALLOCATE(CellMark(HybFrame%NB_Hex))
  ALLOCATE(NodeMark(HybFrame%NB_Point))
  CellMark(:) = 0

  !--- find boundary-close nodes which are very close to the surface
  !    mark them with -1
  DO ib = 1, Surf%NB_Tri
     ip3(1:3) = Surf%IP_Tri(1:3,ib)
     i1(:) = (/ NI, NJ, NK /)
     i2(:) = 1
     DO n=1,3
        DO i=1,3
           in = INT(( Surf%Posit(n,ip3(i)) - XYZmin(n) ) / BGSpacing%BasicSize) + 1
           i1(n) = MIN(i1(n),in)
           i2(n) = MAX(i2(n),in)
        ENDDO
     ENDDO

     DO i=i1(1),i2(1)
        DO j=i1(2),i2(2)
           DO k=i1(3),i2(3)
              nel= (K-1)*NI*NJ + (J-1)*NI +I
              CellMark(nel) = -1
           ENDDO
        ENDDO
     ENDDO
  ENDDO

  neighb(1:6) = (/-1,1,-NI,NI,-NI*NJ,NI*NJ/)
  DO i=1, NI
     DO j=1, NJ
        DO k=1, NK
           IF(i<=2 .OR. i>=NI-1 .OR. j<=2 .OR. j>=NJ-1 .OR. k<=2 .OR. k>=NK-1)THEN
              nel= (K-1)*NI*NJ + (J-1)*NI +I
              CellMark(nel) = 1
           ENDIF
        ENDDO
     ENDDO
  ENDDO

  !--- populate Ideal_Layer_Gap layers from boundary-close nodes
  DO ilayer = 1,Ideal_Layer_Gap
     DO i=2, NI-1
        DO j=2, NJ-1
           DO k=2, NK-1
              nel= (K-1)*NI*NJ + (J-1)*NI +I
              IF(CellMark(nel)==-ilayer)THEN
                 DO m=1,6
                    neb = nel + neighb(m)
                    IF(CellMark(neb)==0) CellMark(neb) = -ilayer-1
                 ENDDO
              ENDIF
           ENDDO
        ENDDO
     ENDDO
  ENDDO

  !--- colour the out-domain
  DO ilayer = 1, NI+NJ+NK
     carryon = .FALSE.
     DO i=2, NI-1
        DO j=2, NJ-1
           DO k=2, NK-1
              nel= (K-1)*NI*NJ + (J-1)*NI +I
              IF(CellMark(nel)==ilayer)THEN
                 DO m=1,6
                    neb = nel + neighb(m)
                    IF(CellMark(neb)==0) CellMark(neb) = ilayer+1
                 ENDDO
                 carryon = .TRUE.
              ENDIF
           ENDDO
        ENDDO
     ENDDO
     IF(.NOT. carryon) EXIT
  ENDDO

  !--- mark wedge layer with CellMark(ie)=0

  IF(1==0)THEN
     !--- enlarge tet-domain as a box
     ijk1(:) = (/NI,NJ,NK/)
     ijk2(:) = 1
     DO I=1,NI
        DO J=1,NJ
           DO k=1,NK
              nel= (K-1)*NI*NJ + (J-1)*NI +I
              IF(CellMark(nel) <= 0)THEN
                 ijk1(1) = MIN(ijk1(1),i)
                 ijk1(2) = MIN(ijk1(2),j)
                 ijk1(3) = MIN(ijk1(3),k)
                 ijk2(1) = MAX(ijk2(1),i)
                 ijk2(2) = MAX(ijk2(2),j)
                 ijk2(3) = MAX(ijk2(3),k)
              ENDIF
           ENDDO
        ENDDO
     ENDDO
     DO I=ijk1(1),ijk2(1)
        DO J=ijk1(2),ijk2(2)
           DO k=ijk1(3),ijk2(3)
              nel= (K-1)*NI*NJ + (J-1)*NI +I
              CellMark(nel) = -1
           ENDDO
        ENDDO
     ENDDO
  ENDIF

  WHERE(CellMark(:)<=0) CellMark(:) = -1
  NodeMark(:) = 1
  DO ie = 1, HybFrame%NB_Hex
     IF(CellMark(ie)==-1) NodeMark(HybFrame%IP_Hex(:,ie)) = 0
  ENDDO

  DO ie = 1, HybFrame%NB_Hex
     IF(CellMark(ie)>0)THEN
        DO i=1,8
           IF(NodeMark(HybFrame%IP_Hex(i,ie))==0)THEN
              CellMark(ie) = 0
              EXIT
           ENDIF
        ENDDO
     ENDIF
  ENDDO


  !--- mark nodes

  DO I=1,NI+1
     DO J=1,NJ+1
        DO k=1,NK+1
           np = (K-1)*(NI+1)*(NJ+1) + (J-1)*(NI+1) + I
           IF(MOD(I+J+K,2)==0) NodeMark(np) = 1
        ENDDO
     ENDDO
  ENDDO

  !--- recount cells

  nel = 0
  nwedge = 0
  DO ie = 1, HybFrame%NB_Hex
     IF(CellMark(ie)<0) CYCLE
     IF(CellMark(ie)==0)THEN
        !--- a wedge
        n = 0
        DO i=1,8
           IF(NodeMark(HybFrame%IP_Hex(i,ie))==0)THEN
              HybFrame%IP_Hex(i,ie) = 0
              n = n+1
           ENDIF
        ENDDO
        IF(n>=4) CYCLE
        IF(n>0) nwedge = nwedge + 1
     ENDIF
     nel = nel + 1
     HybFrame%IP_Hex(:,nel) = HybFrame%IP_Hex(:,ie)
  ENDDO
  HybFrame%NB_Hex = nel

  !---search far field quad boundary
  NodeMark(:) = 0
  DO I=1,NI+1
     DO J=1,NJ+1
        DO k=1,NK+1
           IF(I==1 .or. I==NI+1 .or. J==1 .or. j==NJ+1 .or. k==1 .or. k==NK+1)THEN
              np = (K-1)*(NI+1)*(NJ+1) + (J-1)*(NI+1) + I
              NodeMark(np) = 2
           ENDIF
        ENDDO
     ENDDO
  ENDDO

  nallc = 100000
  CALL allc_2Dpointer(HybridSurf%IP_Quad, 5, nallc, 'IP_Quad')
  HybridSurf%NB_Quad = 0
  DO ie = 1, HybFrame%NB_Hex
     DO i = 1,6
        ip4(:) = HybFrame%IP_Hex(iQuad_Hex(:,i), ie)
        IF(  ip4(1)==0 .OR. ip4(2)==0 .OR.  &
             ip4(3)==0 .OR. ip4(4)==0) CYCLE

        IF(  NodeMark(ip4(1))/=2 .OR. NodeMark(ip4(2))/=2 .OR.  &
             NodeMark(ip4(3))/=2 .OR. NodeMark(ip4(4))/=2) CYCLE
        HybridSurf%NB_Quad = HybridSurf%NB_Quad + 1
        IF(HybridSurf%NB_Quad>nallc)THEN
           nallc = nallc+100000
           CALL allc_2Dpointer(HybridSurf%IP_Quad, 5, nallc, 'IP_Quad')
        ENDIF
        HybridSurf%IP_Quad(1:4,HybridSurf%NB_Quad) = ip4(:)
        HybridSurf%IP_Quad(5,  HybridSurf%NB_Quad) = 1
     ENDDO
  ENDDO

  !--- recount nodes
  NodeMark(:) = 0
  DO ie = 1, HybFrame%NB_Hex
     DO i=1,8
        IF(HybFrame%IP_Hex(i,ie)==0)CYCLE
        NodeMark(HybFrame%IP_Hex(i,ie)) = 1
     ENDDO
  ENDDO

  np = 0
  DO ip = 1, HybFrame%NB_Point
     IF(NodeMark(ip)==1)THEN
        np = np+1
        NodeMark(ip) = np
        HybFrame%Posit(:,np) = HybFrame%Posit(:,ip)
     ENDIF
  ENDDO
  HybFrame%NB_Point = np


  DO ie = 1, HybFrame%NB_Hex
     DO i=1,8
        IF(HybFrame%IP_Hex(i,ie)==0)CYCLE
        HybFrame%IP_Hex(i,ie) = NodeMark(HybFrame%IP_Hex(i,ie))
     ENDDO
  ENDDO

  DO ib = 1, HybridSurf%NB_Quad
     ip4(:) = NodeMark(HybridSurf%IP_Quad(1:4,ib))
     IF(  ip4(1)==0 .OR. ip4(2)==0 .OR. ip4(3)==0 .OR. ip4(4)==0)THEN
          CALL Error_Stop (' wrong far-quad')
     ENDIF
     HybridSurf%IP_Quad(1:4,ib) = ip4(:)
  ENDDO

  !--- count triangles

  CALL allc_2Dpointer(Surf%Posit,  3, Surf%NB_Point+7*nwedge, ' ')
  CALL allc_2Dpointer(Surf%IP_Tri, 5, Surf%NB_Tri  +4*nwedge, ' ')
  NodeMark(:) = 0

  nel = Surf%NB_Tri
  DO ie = 1, HybFrame%NB_Hex
     DO i=1,8
        IF(HybFrame%IP_Hex(i,ie)==0)THEN
           ip3(:) = HybFrame%IP_Hex(iCut_Hex(:,i),ie)
           IF(ip3(1)==0 .OR. ip3(2)==0 .OR. ip3(3)==0) CALL Error_Stop ('Wedge_StairCase 3')
           nel = nel + 1
           Surf%IP_Tri(1:3,nel) = ip3
           NodeMark(ip3(:)) = 1
        ENDIF
     ENDDO
  ENDDO

  np = Surf%NB_Point
  DO ip = 1,  HybFrame%NB_Point
     IF(NodeMark(ip)==1)THEN
        Surf%NB_Point = Surf%NB_Point +1
        NodeMark(ip)  = Surf%NB_Point
        Surf%Posit(:,Surf%NB_Point)  = HybFrame%Posit(:,ip)
     ENDIF
  ENDDO

  CALL allc_1Dpointer(Mark_Surf_Point,  Surf%NB_Point, 'Mark_Surf_Point')
  Mark_Surf_Point(1:np) = 0
  Mark_Surf_Point(np+1:Surf%NB_Point) = 1

  ALLOCATE(HybridSurf%IP_Tri(5, nel-Surf%NB_Tri))
  Surf%NB_Surf = Surf%NB_Surf + 1
  DO ib = Surf%NB_Tri+1,nel
     ip3(:) = Surf%IP_Tri(1:3,ib)
     HybridSurf%IP_Tri(1:3,ib-Surf%NB_Tri) = (/ip3(1),ip3(3),ip3(2)/)
     HybridSurf%IP_Tri(4:5,ib-Surf%NB_Tri) = (/0, 2/)
     ip3(:) = NodeMark(ip3(:))
     CALL sort_ipp_tri(ip3)
     Surf%IP_Tri(1:3,ib) = ip3(:)
     Surf%IP_Tri(4:5,ib) = (/0, 1/)
     !Type_Wall(Surf%NB_Surf) = 3 
  ENDDO

  DO ib = 1,Surf%NB_Tri
    Surf%IP_Tri(5,ib) = Surf%IP_Tri(5,ib) + 1
  END DO
  Type_Wall_Temp(:) = 1
  DO ib = 1,Surf%NB_Surf
    Type_Wall_Temp(ib+1) = Type_Wall(ib)
  END DO
  DO ib = 1,Surf%NB_Surf+1
    Type_Wall(ib) = Type_Wall_Temp(ib)
  END DO
  Type_Wall(1) = 3

  HybridSurf%NB_Tri = nel - Surf%NB_Tri
  Surf%NB_Tri = nel


  WRITE(29,*)'Updated Surface: NB_Point, NB_Tri=',  Surf%NB_Point, Surf%NB_Tri
  IF(Debug_Display>3)THEN
     CALL  SurfaceMeshStorage_Output(JobName(1:JobNameLength)//'_temp',JobNameLength+5,Surf)
  ENDIF


  !--- write hybrid-mesh
  WRITE(29,*)'Hybrid Mesh: NB_Point, NB_Hex, NB_Wedge =',    &
       HybFrame%NB_Point, HybFrame%NB_Hex, nWedge
  CALL HybridMeshStorage_Output(TRIM(JobName)//'_hyb',JobNameLength+4,    &
                                -1,HybFrame, HybridSurf)

  DEALLOCATE(CellMark, NodeMark)
  CALL HybridMeshStorage_Clear(HybFrame)
  CALL SurfaceMeshStorage_Clear(HybridSurf)

END SUBROUTINE Wedge_StairCase

!*******************************************************************************
!>
!!  Build octree mesh in a cubic domain, cut a stair-case domain covery
!!    the geometry with a gap.  Split the rest of the octree into hybrid
!!    mesh or tetrahedral mesh. Find a list of nodes which locate in the gap
!!    between the geometry and the stair-case surface.
!!
!!  @param[in]  common_Parameters::Surf       
!!  @param[in]  common_Parameters::Ideal_Layer_Expand       
!!  @param[in]  common_Parameters::Ideal_Layer_Gap
!!                   : the number of grids in the gap between geometry
!!                      and stair-case surface.
!!  @param[in]  common_Parameters::Frame_Type
!!  @param[out]  common_Parameters::HybFrame    
!!                      the frame hybrid mesh (hex. pyr. & tet.) or
!!                      tetrahedral mesh is save as
!!                      HybFrame if common_Parameters::Frame_Stitch==0;
!!                      otherwise, written to file *_hyb.plt and deleted.
!!  @param[out]  common_Parameters::FillingNodes 
!!                   : nodes locating in th gap between geometry and
!!                     the stair-case surface.    
!!  @param[out]  common_Parameters::IP_BD_Tri
!!                   : boundary triangulation, including the original
!!                     surface and the stair-case surface.
!<
!*******************************************************************************
SUBROUTINE Octree_StairCase( )

  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  USE array_allocator
  USE OctreeMeshManager
  IMPLICIT NONE

  INTEGER :: i,j,k, np, IT, ib, ilayer, n, ip, ie, idom, idd(3)
  INTEGER :: i1(3), i2(3), ip3(3), ip4(4), it1, icell, NB1, NB2, level, NBnew
  INTEGER :: nallc, ipHang(18)
  REAL*8  :: dd(3), dmax(3), pp(3), pmin(3), pmax(3)
  LOGICAL :: carryon
  INTEGER, DIMENSION(:),  POINTER :: NodeMark
  INTEGER, DIMENSION(:),  POINTER :: CellMark
  LOGICAL :: ex
  TYPE (OctreeMesh_Type) :: OctMesh
  TYPE (NodeNetTreeType) :: Tri_Tree
  TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
  INTEGER, DIMENSION(10000) :: Type_Wall_Temp


  dd(1) = BGSpacing%BasicSize / dsqrt(BGSpacing%BasicMap(1,1)**2 + BGSpacing%BasicMap(2,1)**2 + BGSpacing%BasicMap(3,1)**2)
  dd(2) = BGSpacing%BasicSize / dsqrt(BGSpacing%BasicMap(1,2)**2 + BGSpacing%BasicMap(2,2)**2 + BGSpacing%BasicMap(3,2)**2)
  dd(3) = BGSpacing%BasicSize / dsqrt(BGSpacing%BasicMap(1,3)**2 + BGSpacing%BasicMap(2,3)**2 + BGSpacing%BasicMap(3,3)**2)

  IF(Ideal_Layer_Expand>0)THEN
     !---- Measure the domain
     CALL SurfaceMeasure()
     XYZmax(1:3) = XYZmax(1:3) + Ideal_Layer_Expand * dd(1:3)
     XYZmin(1:3) = XYZmin(1:3) - Ideal_Layer_Expand * dd(1:3)
  ELSE
     XYZmin(1:3) = FrameBox(1:3)
     XYZmax(1:3) = FrameBox(4:6)
     IF(XYZmin(1)>=XYZmax(1) .OR. XYZmin(2)>=XYZmax(2) .OR. XYZmin(3)>=XYZmax(3))THEN
        WRITE(29,*)'Error---- not a proper domain:',XYZmin(1),XYZmax(1)
        CALL Error_Stop ('Octree_StairCase')
     ENDIF
  ENDIF

  idd(1:3) = (XYZmax(1:3) - XYZmin(1:3)) / dd(1:3) +1
  XYZmax(:) = XYZmin(:) + idd(:)*dd(:)

  WRITE(29,*)'XYZmin=',XYZmin
  WRITE(29,*)'XYZmax=',XYZmax
  WRITE(29,*)'DD    =',dd

  !--- set octree

  CALL OctreeMesh_Set(idd, dd, XYZmin, OctMesh)
  WRITE(29,'(a,i4,i4,i4)') 'OctMesh: imax,jmax,kmax=', &
       OctMesh%imax,OctMesh%jmax,OctMesh%kmax

  !--- refine octree

  CALL OctreeMesh_RefineBySpacing(OctMesh,BGSpacing)
  IF(Debug_Display>3)THEN
     CALL OctreeMesh_Check(OctMesh)
     !  CALL OctreeMesh_CheckNodes(OctMesh)
     CALL OctreeMesh_OutputEnSight('frame_octree',12,OctMesh)
  ENDIF

  !--- find tet mesh domain
  ALLOCATE(CellMark(OctMesh%numCells))
  ALLOCATE(NodeMark(OctMesh%numNodes))

  CALL OctreeMesh_getLeafCells(OctMesh)
  CellMark(1:OctMesh%numCells) = 0
  NodeMark(1:OctMesh%numNodes) = 0

  !--- mark cells containing surface triangles
  DO ib = 1, Surf%NB_Tri
     ip3(1:3) = Surf%IP_Tri(1:3,ib)
     pmax(:) = Surf%Posit(:,ip3(1))
     pmin(:) = Surf%Posit(:,ip3(1))
     DO i=2,3
        DO n=1,3
           pmax(n) = MAX(pmax(n), Surf%Posit(n,ip3(i)))
           pmin(n) = MIN(pmin(n), Surf%Posit(n,ip3(i)))
        ENDDO
     ENDDO
     dmax(:) = pmax(:) - pmin(:)
     level = OctreeMesh_getCoarseLevel(dmax,OctMesh)
     n = 1
     IF(level==0)THEN
        n = MAX(dmax(1)/OctMesh%DX(1),dmax(2)/OctMesh%DY(1),dmax(3)/OctMesh%DZ(1))
        n = n+1
        level = 1
     ENDIF
     DO i = 0,n
        DO j = 0,n
           DO k = 0,n
              pp(1) = pmin(1) + i*dmax(1)/n
              pp(2) = pmin(2) + j*dmax(2)/n
              pp(3) = pmin(3) + k*dmax(3)/n
              pOct_Cell => OctreeMesh_CellSearch(pp,level,OctMesh)
              CALL OctreeMesh_CellMark(pOct_Cell,CellMark,1)
           ENDDO
        ENDDO
     ENDDO
  ENDDO
  DO iCell=1,OctMesh%numCells
     IF(CellMark(iCell)==1)THEN
        pOct_Cell => OctMesh%LeafCells(iCell)%to
        NodeMark(pOct_Cell%Nodes(:)) = 1
        CALL OctreeMesh_getCellHangNodes(pOct_Cell,OctMesh,ipHang)
        WHERE(ipHang(:)>0) NodeMark(ipHang(:)) = 1
     ENDIF
  ENDDO

  !--- colour Octree far-field boundary
  DO iCell=1,OctMesh%numCells
     pOct_Cell => OctMesh%LeafCells(iCell)%to
     DO i=1,6
        p2Oct_Cell => OctreeMesh_NextCell(pOct_Cell,i)
        IF(p2Oct_Cell%level>0) CYCLE

        ip4(:) = pOct_Cell%Nodes(iQuad_Hex(:,i))

        IF(Debug_Display>2)THEN
           DO j= 1,4
              DO n=1,3
                 IF(OctMesh%Posit(n,ip4(j)) - XYZmin(n)<TinySize) EXIT
                 IF(XYZmax(n) - OctMesh%Posit(n,ip4(j))<TinySize) EXIT
              ENDDO
              IF(n>3) CALL Error_Stop ('Octree_StairCase 6')
           ENDDO
        ENDIF

        CellMark(iCell)  = -1
        NodeMark(ip4(:)) = -2
     ENDDO
  ENDDO

  !--- colour the frame-domain
  DO
     CarryOn = .FALSE.
     DO iCell=1,OctMesh%numCells
        IF(CellMark(iCell)==-1)THEN
           pOct_Cell => OctMesh%LeafCells(iCell)%to
           DO i=1,8
              IF(NodeMark(pOct_Cell%Nodes(i))==0)THEN
                 CarryOn = .TRUE.
                 NodeMark(pOct_Cell%Nodes(i)) = -1
              ENDIF
           ENDDO
           CALL OctreeMesh_getCellHangNodes(pOct_Cell,OctMesh,ipHang)
           DO i=1,18
              IF(ipHang(i)==0) CYCLE
              IF(NodeMark(ipHang(i))==0)THEN
                 CarryOn = .TRUE.
                 NodeMark(ipHang(i)) = -1
              ENDIF
           ENDDO
           DO i=1,6
              p2Oct_Cell => OctreeMesh_NextCell(pOct_Cell,i)
              IF(p2Oct_Cell%level<=0) CYCLE
              IF(CellMark(p2Oct_Cell%ID)==0)THEN
                 CarryOn = .TRUE.
                 CellMark(p2Oct_Cell%ID) = -1
              ENDIF
           ENDDO
        ENDIF
     ENDDO
     IF(.NOT. CarryOn) EXIT
  ENDDO

  !--- expand Ideal_Layer_Gap layers for tet-domain towards far-field
  DO ilayer = 1,Ideal_Layer_Gap
     DO iCell=1,OctMesh%numCells
        IF(CellMark(iCell)/=-1) CYCLE
        pOct_Cell => OctMesh%LeafCells(iCell)%to
        DO i=1,8
           IF(NodeMark(pOct_Cell%Nodes(i))==1)THEN
              CellMark(iCell) = 1
              EXIT
           ENDIF
        ENDDO
     ENDDO
     DO iCell=1,OctMesh%numCells
        IF(CellMark(iCell)==1)THEN
           pOct_Cell => OctMesh%LeafCells(iCell)%to
           DO i=1,8
              IF(NodeMark(pOct_Cell%Nodes(i))==-2) CALL Error_Stop (' touch far-boundary 1')
              IF(NodeMark(pOct_Cell%Nodes(i))==-1)  NodeMark(pOct_Cell%Nodes(i)) = 1
           ENDDO
           CALL OctreeMesh_getCellHangNodes(pOct_Cell,OctMesh,ipHang)
           DO i=1,18
              IF(ipHang(i)==0) CYCLE
              IF(NodeMark(ipHang(i))==-2) CALL Error_Stop ('  touch far-boundary 2')
              IF(NodeMark(ipHang(i))==-1) NodeMark(ipHang(i)) = 1
           ENDDO
        ENDIF
     ENDDO
  ENDDO


  !--- mark boundary nodes between frame mesh and tet-zoom with 2
  DO iCell=1,OctMesh%numCells
     IF(CellMark(iCell)==-1 .OR. CellMark(iCell)==1)THEN
        pOct_Cell => OctMesh%LeafCells(iCell)%to
        DO i=1,6
           p2Oct_Cell => OctreeMesh_NextCell(pOct_Cell,i)
           IF(p2Oct_Cell%level<=0) CYCLE
           IF(CellMark(p2Oct_Cell%ID)==0 .AND. CellMark(iCell)==-1)   &
                CALL Error_Stop ('   a hole through layers')
           IF( CellMark(p2Oct_Cell%ID) == -CellMark(iCell) .AND.  &
                p2Oct_Cell%Level <= pOct_Cell%Level )THEN
              ip4(:) = pOct_Cell%Nodes(iQuad_Hex(:,i))
              NodeMark(ip4(:)) = 2
              CALL OctreeMesh_getCellHangNodes(pOct_Cell,OctMesh,ipHang)
              ip4(:) = ipHang(jEdge_Quad_Hex(:,i))
              WHERE(ip4(:)>0) NodeMark(ip4(:)) = 2
           ENDIF
        ENDDO
     ENDIF
  ENDDO


  !--- spliting octree
  WRITE(29,*)' Split Octree--- HybFrame% NB_Hex,NB_Pyr,NB_Tet,NB_Point='
  IF(Frame_Type==4)THEN
     CALL OctreeMesh_getHybridMesh(OctMesh,HybFrame)
  ELSE
     CALL OctreeMesh_getHybridMesh(OctMesh,HybFrame,CellMark)
  ENDIF
  WRITE(29,*) HybFrame%NB_Hex,HybFrame%NB_Pyr,HybFrame%NB_Tet,HybFrame%NB_Point

  !--- reset NodeMark
  CALL allc_1DPointer(Mark_Point, HybFrame%NB_Point, 'Mark_Point')
  CALL allc_1DPointer(NodeMark,   HybFrame%NB_Point, 'NodeMark')
  DO ip = OctMesh%numNodes+1, HybFrame%NB_Point
     pOct_Cell => OctreeMesh_CellSearch(HybFrame%Posit(:,ip),Max_Oct_Level,OctMesh)
     NodeMark(ip) = CellMark(pOct_Cell%ID)
  ENDDO

  !--- get surface between tet-domain and frame mesh
  NBnew = Surf%NB_Tri
  nallc = Surf%NB_Tri
  nb2 = 0
  CALL NodeNetTree_allocate(3, Surf%NB_Point, 4*Surf%NB_Tri, Tri_Tree)

  DO it=1,HybFrame%NB_Tet
     ip4(:) = HybFrame%IP_Tet(:,it)
     pp = ( HybFrame%Posit(:,ip4(1)) + HybFrame%Posit(:,ip4(2))   &
          + HybFrame%Posit(:,ip4(3)) + HybFrame%Posit(:,ip4(4)) ) /4.d0
     pOct_Cell => OctreeMesh_CellSearch(pp,Max_Oct_Level,OctMesh)
     idom = CellMark(pOct_Cell%ID)
     IF(Debug_Display>2)THEN
        IF(idom==1)THEN
           DO i=1,4
              IF(NodeMark(ip4(i))==-1) CALL Error_Stop (' wrong idom 1')
           ENDDO
        ELSE IF(idom==-1)THEN
           DO i=1,4
              IF(NodeMark(ip4(i))==0 .OR. NodeMark(ip4(i))==1) CALL Error_Stop (' wrong idom -1')
           ENDDO
        ELSE IF(idom==0)THEN
           DO i=1,4
              IF(NodeMark(ip4(i))==2 .OR. NodeMark(ip4(i))==-1) CALL Error_Stop (' wrong idom 0')
           ENDDO
        ELSE
           CALL Error_Stop (' wrong idom else')
        ENDIF
     ENDIF

     IF(idom==-1)THEN
        !--- hybrid frame domain
        nb2 = nb2+1
        HybFrame%IP_Tet(:,nb2) = HybFrame%IP_Tet(:,it)
     ELSE IF(idom==1)THEN
        !--- Tet-domain
        DO i = 1,4
           ip3(:) = ip4(iTri_Tet(:,i))
           IF(  NodeMark(ip3(1))==2 .AND. NodeMark(ip3(2))==2 .AND.    &
                NodeMark(ip3(3))==2) THEN
              CALL NodeNetTree_SearchAdd(3,ip3, Tri_Tree, IB)
              IB = IB + Surf%NB_Tri
              IF(IB>NBnew)THEN
                 NBnew = NBnew+1
                 IF(NBnew>nallc)THEN
                    nallc = nallc + 100000
                    CALL allc_2DPointer(Surf%IP_Tri,  5, nallc, 'Surf%Posit')
                 ENDIF
                 Surf%IP_Tri(1:3,NBnew) = ip3(:)
              ELSE
                 !--- triangle pair
                 Surf%IP_Tri(1,IB) = 0
              ENDIF
           ENDIF
        ENDDO
     ELSE
        !--- tet-domain or discarded domain
     ENDIF
  ENDDO

  CALL NodeNetTree_Clear(Tri_Tree)

  IF(Debug_Display>2)THEN
     DO ib = Surf%NB_Tri+1,NBnew
        ip3(:) = Surf%IP_Tri(1:3,IB)
        IF(ip3(1)==0) CYCLE
        IF(NodeMark(ip3(1))/=2 .AND. NodeMark(ip3(1))/=3) CALL Error_Stop ('  not boundary node')
        IF(NodeMark(ip3(2))/=2 .AND. NodeMark(ip3(2))/=3) CALL Error_Stop ('  not boundary node')
        IF(NodeMark(ip3(3))/=2 .AND. NodeMark(ip3(3))/=3) CALL Error_Stop ('  not boundary node')
        NodeMark(ip3(:)) = 3
     ENDDO
     DO ip = 1,OctMesh%numNodes
        IF(NodeMark(ip)==2)THEN
           WRITE(29,*) 'Error--- not all boundary nodes be used'
           WRITE(29,*)' ip=',ip
           CALL Error_Stop ('Octree_StairCase')
        ENDIF
        IF(NodeMark(ip)==3) NodeMark(ip) = 2
     ENDDO
  ENDIF

  nallc  = Surf%NB_Point
  Mark_Point(1:NB_Point) = 0

  DO ip = 1,OctMesh%numNodes
     IF(NodeMark(ip)==2)THEN
        Surf%NB_Point = Surf%NB_Point+1
        IF(Surf%NB_Point>nallc)THEN
           nallc = nallc + 100000
           CALL allc_2DPointer(Surf%Posit,  3, nallc, 'Surf%Posit')
        ENDIF
        Mark_Point(ip) = Surf%NB_Point
        Surf%Posit(:,Surf%NB_Point) = HybFrame%Posit(:,ip)
     ENDIF
  ENDDO

  ALLOCATE(HybridSurf%IP_Tri(5, NBnew-Surf%NB_Tri))
  Surf%NB_Surf = Surf%NB_Surf + 1
  NB1 = Surf%NB_Tri
  DO IB = Surf%NB_Tri+1,NBnew
     IF(Surf%IP_Tri(1,IB)>0)THEN
        NB1 = NB1+1
        ip3(:) = Surf%IP_Tri(1:3,IB)
        HybridSurf%IP_Tri(1:3,NB1-Surf%NB_Tri) = (/ip3(1),ip3(3),ip3(2)/)
        HybridSurf%IP_Tri(4:5,NB1-Surf%NB_Tri) = (/0, 2/)
        ip3(:) = Mark_Point(ip3(:))
        CALL  sort_ipp_tri(ip3)
        Surf%IP_Tri(1:3,NB1) = ip3(:)
        Surf%IP_Tri(4:5,NB1) = (/0, 1/)
     ENDIF
  ENDDO

  Type_Wall_Temp(:) = 1
  DO ib = 1,Surf%NB_Tri
    Surf%IP_Tri(5,ib) = Surf%IP_Tri(5,ib) + 1
  END DO
  Type_Wall_Temp(:) = 1
  DO ib = 1,Surf%NB_Surf
    Type_Wall_Temp(ib+1) = Type_Wall(ib)
  END DO
  DO ib = 1,Surf%NB_Surf+1
    Type_Wall(ib) = Type_Wall_Temp(ib)
  END DO

  HybridSurf%NB_Tri = NB1 - Surf%NB_Tri
  Surf%NB_Tri = NB1

  !--- get nodes in tet-domain
  ALLOCATE(FillingNodes%Posit(3,OctMesh%numNodes))
  np = 0
  DO ip = 1,OctMesh%numNodes
     IF(NodeMark(ip)==1)THEN
        np = np+1
        FillingNodes%Posit(:,np) = OctMesh%Posit(:,ip)
     ENDIF
  ENDDO
  FillingNodes%NB_Point = np


  !--- clean frame mesh

  Mark_Point(:) = 0
  HybFrame%NB_Tet = nb2
  DO it=1,HybFrame%NB_Tet
     Mark_Point(HybFrame%IP_Tet(:,it)) = 1
  ENDDO

  nb1 = 0
  DO it=1,HybFrame%NB_Hex
     DO i=1,8
        ip = HybFrame%IP_Hex(i,it)
        IF(Mark_Point(ip)>0)THEN
           idom = -1
        ELSE
           idom = NodeMark(ip)
           IF(idom==2 .OR. idom==-2) idom = -1
        ENDIF
        IF(idom/=1) EXIT
     ENDDO
     IF(idom==-1)THEN
        nb1= nb1+1
        Mark_Point(HybFrame%IP_Hex(:,it)) = 1
        HybFrame%IP_Hex(:,nb1) = HybFrame%IP_Hex(:,it)
     ELSE IF(idom>=1)THEN
        CALL Error_Stop ('  wrong hex domain')
     ENDIF
  ENDDO
  HybFrame%NB_Hex = nb1

  nb1 = 0
  DO it=1,HybFrame%NB_Pyr
     DO i=1,5
        ip = HybFrame%IP_Pyr(i,it)
        IF(Mark_Point(ip)>0)THEN
           idom = -1
        ELSE
           idom = NodeMark(ip)
           IF(idom==2 .OR. idom==-2) idom = -1
        ENDIF
        IF(idom/=1) EXIT
     ENDDO
     IF(idom==-1)THEN
        nb1= nb1+1
        Mark_Point(HybFrame%IP_Pyr(:,it)) = 1
        HybFrame%IP_Pyr(:,nb1) = HybFrame%IP_Pyr(:,it)
     ELSE IF(idom>=1)THEN
        CALL Error_Stop ('wrong pyr domain')
     ENDIF
  ENDDO
  HybFrame%NB_Pyr = nb1

  np = 0
  DO ip = 1,HybFrame%NB_Point
     IF(Mark_Point(ip)==1)THEN
        np = np + 1
        HybFrame%Posit(:,np) = HybFrame%Posit(:,ip)
        Mark_Point(ip) = np
     ENDIF
  ENDDO
  HybFrame%NB_Point = np

  !---search far field tri boundary
  nallc = HybridSurf%NB_Tri
  DO it = 1, HybFrame%NB_Tet
     DO i=1,4
        ip3(:) = HybFrame%IP_Tet(iTri_Tet(:,i), it)
        IF(  NodeMark(ip3(1))/=-2 .OR. NodeMark(ip3(2))/=-2 .OR.  &
             NodeMark(ip3(3))/=-2) CYCLE
        DO n=1,3
           ilayer = 0
           DO j=1,3
              IF(OctMesh%Posit(n,ip3(j)) - XYZmin(n)<TinySize) ilayer = ilayer-1
              IF(XYZmax(n) - OctMesh%Posit(n,ip4(j))<TinySize) ilayer = ilayer+1
           ENDDO
           if(abs(ilayer)==3) exit
        ENDDO
        IF(n<=3)THEN
           HybridSurf%NB_Tri = HybridSurf%NB_Tri + 1
           IF(HybridSurf%NB_Tri>nallc)THEN
              nallc = nallc+100000
              CALL allc_2Dpointer(HybridSurf%IP_Tri, 5, nallc, 'IP_Tri')
           ENDIF
           HybridSurf%IP_Tri(1:3,HybridSurf%NB_Tri) = ip3(:)
           HybridSurf%IP_Tri(4:5,HybridSurf%NB_Tri) = (/0,1/)
        ENDIF
     ENDDO
  ENDDO

  !---search far field quad boundary
  nallc = 100000
  CALL allc_2Dpointer(HybridSurf%IP_Quad, 5, nallc, 'IP_Quad')
  HybridSurf%NB_Quad = 0
  DO it = 1, HybFrame%NB_Pyr
     ip4(:) = HybFrame%IP_Pyr(1:4, it)
     IF(  NodeMark(ip4(1))/=-2 .OR. NodeMark(ip4(2))/=-2 .OR.  &
          NodeMark(ip4(3))/=-2 .OR. NodeMark(ip4(4))/=-2) CYCLE
     HybridSurf%NB_Quad = HybridSurf%NB_Quad + 1
     IF(HybridSurf%NB_Quad>nallc)THEN
        nallc = nallc+100000
        CALL allc_2Dpointer(HybridSurf%IP_Quad, 5, nallc, 'IP_Quad')
     ENDIF
     HybridSurf%IP_Quad(1:4,HybridSurf%NB_Quad) = ip4(:)
     HybridSurf%IP_Quad(5,  HybridSurf%NB_Quad) = 1
  ENDDO
  DO it = 1, HybFrame%NB_Hex
     DO i = 1,6
        ip4(:) = HybFrame%IP_Hex(iQuad_Hex(:,i), it)
        IF(  NodeMark(ip4(1))/=-2 .OR. NodeMark(ip4(2))/=-2 .OR.  &
             NodeMark(ip4(3))/=-2 .OR. NodeMark(ip4(4))/=-2) CYCLE
        HybridSurf%NB_Quad = HybridSurf%NB_Quad + 1
        IF(HybridSurf%NB_Quad>nallc)THEN
           nallc = nallc+100000
           CALL allc_2Dpointer(HybridSurf%IP_Quad, 5, nallc, 'IP_Quad')
        ENDIF
        HybridSurf%IP_Quad(1:4,HybridSurf%NB_Quad) = ip4(:)
        HybridSurf%IP_Quad(5,  HybridSurf%NB_Quad) = 1
     ENDDO
  ENDDO

  !--- mapping numbering
  DO it = 1, HybFrame%NB_Tet
     HybFrame%IP_Tet(:,it) = Mark_Point(HybFrame%IP_Tet(:,it))
  ENDDO
  DO it = 1, HybFrame%NB_Hex
     HybFrame%IP_Hex(:,it) = Mark_Point(HybFrame%IP_Hex(:,it))
  ENDDO
  DO it = 1, HybFrame%NB_Pyr
     HybFrame%IP_Pyr(:,it) = Mark_Point(HybFrame%IP_Pyr(:,it))
  ENDDO
  DO ib = 1, HybridSurf%NB_Quad
     HybridSurf%IP_Quad(1:4,ib) = Mark_Point(HybridSurf%IP_Quad(1:4,ib))
  ENDDO
  DO ib = 1, HybridSurf%NB_Tri
     HybridSurf%IP_Tri(1:3,ib) = Mark_Point(HybridSurf%IP_Tri(1:3,ib))
  ENDDO



  DEALLOCATE(CellMark)
  DEALLOCATE(NodeMark)
  CALL OctreeMesh_Clear(OctMesh)


  CALL SurfaceMeshStorage_Output(JobName(1:JobNameLength)//'_temp',JobNameLength+5,Surf)
  IF(Frame_Stitch==0)THEN
     CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_hyb',JobNameLength+4,  &
          -1, HybFrame, HybridSurf)
     CALL HybridMeshStorage_Clear(HybFrame)
     CALL SurfaceMeshStorage_Clear(HybridSurf)
  ENDIF

  RETURN

END SUBROUTINE Octree_StairCase




