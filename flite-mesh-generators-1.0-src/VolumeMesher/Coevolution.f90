!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  Coevolution
!!  
!!  This routine drives the co-volume optimisation
!!
!!  
!!
!!  Initial implimentation by S. Walton 18/09/2014
!!  NB1 to NB2 are nodes which can move
!!  
!!
!!
!<
!*******************************************************************************
SUBROUTINE Coevolution(NB1,NB2)
	
	USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	USE HybridMeshStorage
	USE TetMeshStorage
	USE PointAssociation
	IMPLICIT NONE
	
	INTEGER, INTENT(IN) :: NB1,NB2


	REAL*8 :: dummy, numLocLoops, LMax

	INTEGER :: numMeshes
	INTEGER :: m, origin, bestMesh

	TYPE(TetMeshStorageType), ALLOCATABLE :: meshes(:)  !== The collection of meshes
	REAL*8, ALLOCATABLE :: weights(:,:)                 !== The weights for each of the meshes
	REAL*8, ALLOCATABLE :: param(:,:)                   !== The optimisation parameters for each mesh
	REAL*8, ALLOCATABLE :: improvement(:)                !== The improvement yielded by each param set
	REAL*8, ALLOCATABLE :: qualities(:)                 !== The quality of each mesh

	INTEGER :: numParam
	REAL*8  :: para1, para2

	REAL*8 :: Layers(NB_Point)
	INTEGER :: badMask(NB_Point)

	REAL*8 :: quality,minQual,maxQual,bestQuality
	REAL*8, ALLOCATABLE :: locTetQual(:)

	TYPE(IntQueueType), ALLOCATABLE :: buckets(:)
	INTEGER :: numBuckets, b, ib, nb_b
	REAL*8  :: bucketWidth

	REAL*8 :: randn,randn2

	INTEGER :: ie, optElem

	TYPE(PointAssociationArray) :: PointAsso2

	TYPE(IntQueueType) :: effElem

	INTEGER :: i, ip, ic, nbp, Kswap, j, l

   	!=== Optimisation variables
   	INTEGER :: DD
   	INTEGER :: N 
   	INTEGER :: Ibest, Loop, Istatus, Iin, idum, Ni, Di
   	REAL*8  :: ptemp(16), ftemp, fbest, d, dw
   	REAL*8  :: Pt(4,40), Qu(40), optl(4), optu(4)
   	REAL*8  :: psave(4)
   	INTEGER :: MCSFlag, K, p
   	REAL*8  :: p1(3), p2(3), p3(3), p4(3), dimi, invPi, mergeFact
   	REAL*8  :: pC(3), coef, qual, imp

   	!===Convergence tests
   	INTEGER :: globConv
   	REAL*8  :: conv, meanQual, meanQualLast, bestQualityLast
   	REAL*8  :: meanRes, bestRes

   	!===Face variables
   	INTEGER :: i1, i2, i3
   	REAL*8  :: pa(3), pb(3), circum(3), norm(3), optPoint(3), spawn(3), normVec(3)
   	REAL*8  :: meanW

   	!===Weight variables
   	REAL*8 :: o1,o2

   	INTEGER :: loopNum
   	CHARACTER(len=1024) :: filenameStr
   	character(len=1024) :: loopNumStr

   	INTEGER :: numLayers

   	!===Suffling variables
   	INTEGER :: iRan, jRan, tRan 
   	INTEGER, ALLOCATABLE :: order(:)

   	!===Variables to keep track of looping through the element
   	INTEGER :: ip_loc, ie_loc

   	INTEGER :: NB_Point_Orig

   	REAL*8  :: fitInit, fitFinal, locImp, numlocImp, sumlocImp, Swapping_Angle_Deg

   	REAL*8  :: frac

   	REAL*8  :: minVol


   	NB_Point_Orig = NB_Point

   	!conv = 0.1d0/REAL(NB_Tet)
   	conv = epsilon(0d0)

   	DD = 4
   	N = 40
   	invPi = 1.0d0/PI
	
	numLocLoops = 5
	numMeshes = 15

	

	!The parameters
	numParam = 3    !== 1 : number of buckets for quality sort
					!== 2 : stepsize
					!== 3 : discard prob

	Swapping_Angle_Deg = 20.0d0					
	Swapping_Angle          = (Swapping_Angle_Deg*3.141592653589793d0)/180.d0
	Collapse_Angle          = (Swapping_Angle_Deg*3.141592653589793d0)/180.d0
	!We can now allocate everything
	ALLOCATE(param(numParam,numMeshes))
	ALLOCATE(improvement(numMeshes))
	CALL PointAssociation_Allocate(NB_Point, PointAsso2)
	
	
	!....make initial set of parameters
	DO m = 1,numMeshes
		CALL RANDOM_NUMBER(randn)
		param(1,m) = REAL(NB_Tet) !MAX(10.0d0,0.1d0*REAL(NB_Tet)*randn)
     	param(2,m) = 0.01d0
     	param(3,m) = 0.7d0
     	improvement(m) = 0.0d0
	END DO

	!Work out global quality
	Kswap = 2
	CALL Swap3D(Kswap)
	WRITE(*,*)' '
	WRITE(*,*)'---- Collapse Tiny Angles ----'
	CALL Element_Collapse( )
	CALL Next_Build()
	CircumUpdated = .TRUE.
	VolumeUpdated = .TRUE.
	DO ie = 1,NB_Tet
	  CALL Get_Tet_SphereCross(ie)
	  CALL Get_Actual_Tet_Mapping(ie)
	  CALL Get_Tet_Volume(ie)
	END DO
	CircumUpdated = .TRUE.
	VolumeUpdated = .TRUE.
	

	CALL Label_Layers(Layers)
	CALL Get_Global_Quality(1,NB_Point,quality,badMask,Layers)
	
	!Initialise the best mesh
	bestQuality = quality
	bestQualityLast = bestQuality

	!TODO - Outer loop which tests for convergence of the best mesh and the mean quality
	globConv = 0
	loopNum = 0
	DO WHILE (globConv.EQ.0)

		!conv = (1.0d0/REAL(NB_Tet))*(1.0d0/REAL(NB_Tet))
		!Single loop
		DO m = 1, numMeshes


			!CALL Search_and_Split_WellCentered()

			!First job is to select an origin set of parameters
			CALL RANDOM_NUMBER(randn)
			origin = MIN(numMeshes,INT(numMeshes*randn) + 1)



			
			CALL PointAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso2)



			!Get all the parameters
			numBuckets = INT(param(1,origin))
			para1 = param(2,origin)
			para2 = param(3,origin)

			WRITE(*,*)'NumB ', numBuckets, ' p1 ',para1, ' p2 ', para2
			
			!Bucket all the elements in to catogories based on individual quality

			!1) Allocate the buckets
			
			ALLOCATE(buckets(numBuckets),locTetQual(NB_Tet))

			!2) Get Element quality
			
			CALL Get_Sorting_Elem_Qual(Layers,locTetQual,minQual,maxQual)
			loopNum = loopNum+1

			WRITE(*,*)'Mesh ',m
			WRITE(*,*)'maxQual ', maxQual, 'minQual ', minQual
			

			bucketWidth = (maxQual-minQual)/REAL(numBuckets)
			DO ie = 1,NB_Tet
				IF (locTetQual(ie).GT.0.0d0) THEN
				  DO b = 1,numBuckets
				    IF ((locTetQual(ie).LE.(REAL(b)*bucketWidth)).OR.&
				    	(b.EQ.numBuckets)) THEN
				    	  CALL IntQueue_Push(buckets(numBuckets-b+1),ie)
				    	  EXIT
				    ENDIF
				  ENDDO
				ENDIF
			ENDDO
			

			

			!Loop round each element and optimise
			sumlocImp = 0.0d0
			numlocImp = 0.0d0
			Mark_Point(1:NB_Point) = 0
			DO b = 1,numBuckets

			  nb_b = buckets(b)%numNodes
			  IF (nb_b.GT.0) THEN
			    ALLOCATE(order(nb_b))
			    DO iRan = 1,nb_b
			      order(iRan) = iRan
			    ENDDO
			    DO iRan = nb_b,1,-1
			      CALL RANDOM_NUMBER(randn)
			      jRan = iRan*randn + 1
			      tRan = order(iRan)
			      order(iRan) = order(jRan)
			      order(jRan) = tRan
			     ENDDO

			  

			  
			  DO ib = 1,nb_b
			    optElem = buckets(b)%Nodes(order(ib))
				 


				 
				   
				    IF (SUM(Mark_Point(IP_Tet(:,optElem))).LT.4) THEN

				    	DO ie_loc = 1,4
				    		ip_loc = IP_Tet(ie_loc,optElem)
				    		IF (Mark_Point(ip_loc).EQ.0) THEN
						    !We need to collate a list of elements connected the nodes of optElem
						    Mark_Tet(1:NB_Tet) = 0
						   
					    	DO ic = 1,PointAsso2%PointList(ip_loc)%numCells
					    		ie = PointAsso2%PointList(ip_loc)%CellList(ic)
					    		IF (Mark_Tet(ie).EQ.0) THEN
					    			CALL IntQueue_Push(effElem,ie)
					    			Mark_Tet(ie) = 1
					    		END IF

					    	END DO

					    	DO ic = 1,4
					    		ie = Next_Tet(ic,optElem)
					    		IF (ie.GT.0) THEN
						    		IF (Mark_Tet(ie).EQ.0) THEN
						    			CALL IntQueue_Push(effElem,ie)
						    			Mark_Tet(ie) = 1
						    		END IF
						    	ENDIF

					    	ENDDO
						    

						    !Now we have the list of elements we need to set up the optimisation problem
						    !First get the spacing
						    d = HUGE(0.0d0)
						    DO ic = 1,effElem%numNodes
						      d = MIN(d,Scale_Tet( effElem%Nodes(ic) )*BGSpacing%BasicSize)
						    END DO
						    
						    dw = d*d

						    ptemp(1:DD) = 0.0d0
							Ibest = 0
							ftemp = 0.0d0
							I = 0
							Loop = 0
							Istatus = 0
							Iin = 0
							Qu(1:N) = huge(0.0)
							fbest = huge(0.0)

						
							!Upper and lower bounds are going to be 1*d
							optl(1:DD) = -1.0d0
							optu(1:DD) = 1.0d0

							!Generate a set of eggs
							DO Ni = 1,N
							  DO Di = 1,DD
							  	CALL RANDOM_NUMBER(randn)
								Pt(Di,Ni) = optl(Di) + randn*(optu(Di)-optl(Di))
							  END DO
							END DO

							!Here we are going to specify some eggs
							j = 1

							!The first is no change
							Pt(1:DD,j) = 0.0d0

							!DO l = 1,4
								!Now each point could be placed above the reflected circumcentre
								!of the opposite face
								j = j+1
								l = ie_loc

								i1 = IP_Tet(iTri_Tet(1,l),optElem)
								i2 = IP_Tet(iTri_Tet(2,l),optElem)
								i3 = IP_Tet(iTri_Tet(3,l),optElem)

								pa(:) = Posit(:,i1)
								pb(:) = Posit(:,i2)
								pc(:) = Posit(:,i3)

								circum(:) = Geo3D_Sphere_Centre(pa,pb,pc)
								!Reflect centroid by circum
								spawn(:) = 2.0d0*circum(:) - ((pa(:)+pb(:)+pc(:))/3.0d0)
								norm(:) = Geo3D_Cross_Product(pa,pb,pc)

								normVec(:) = (Geo3D_Distance(circum,pa)*norm(:))/Geo3D_Distance(norm)

								optPoint(:) = spawn(:) + (1.1d0*normVec(:))

								!First set everything to zero
								Pt(1:DD,j) = 0.0d0
								

								!Then turn the weight to equal the other face
								Pt(4,j) = (-1.0d0*RR_Point(ip_loc))/dw
								
								!Then set the point to it's optPoint
								
								Pt(1,j) = (optPoint(1) - Posit(1,ip_loc))/d
								Pt(2,j) = (optPoint(2) - Posit(2,ip_loc))/d
								Pt(3,j) = (optPoint(3) - Posit(3,ip_loc))/d

								
							
								

								

							!ENDDO
			

							!Save the original location
							
							psave(1) = Posit(1,ip_loc)
							psave(2) = Posit(2,ip_loc)
							psave(3) = Posit(3,ip_loc)
							psave(4) = RR_Point(ip_loc)
							

							MCSFlag = 0

							Ibest = 0
							ftemp = 0.0d0
							I = 0
							Loop = 0
							Istatus = 0
							Iin = 0
							Qu(1:N) = huge(0.0d0)
							fbest = huge(0.0d0)


							DO WHILE (MCSFlag.EQ.0)

							  CALL Optimising_Cuckoo_Driver(DD,N,ptemp,Ibest,ftemp,Pt,I,Loop, &
									                         Istatus,Iin,Qu,fbest,optu,optl,idum,&
									                         para1,para2)
							  IF ((Loop.EQ.1).AND.(Istatus.EQ.1).AND.(Iin.EQ.0)) THEN
							  	fitInit = fbest
							  ENDIF
							 
							  

							  IF (Iin.GT.0) THEN

							  	!Move stuff
							  	!DO ic = 0,3
							  	  
							  	  	  IF ((ip_loc.GE.NB1)) THEN
								  	  !IF ((ip_loc.GE.NB1).AND.(ip_loc.LE.NB2)) THEN
										 Posit(1,ip_loc) = psave(1) + d*ptemp(1)
										 Posit(2,ip_loc) = psave(2) + d*ptemp(2)
										 Posit(3,ip_loc) = psave(3) + d*ptemp(3)
									  END IF
					                  RR_Point(ip_loc) = psave(4) + dw*ptemp(4)
				                  
								!END DO

				                !Recalculate stuff
				                DO ic = 1,effElem%numNodes
							       CALL Get_Tet_SphereCross(effElem%Nodes(ic))
							       CALL Get_Actual_Tet_Mapping(effElem%Nodes(ic))
							    END DO

							    !Now calculate the objective function
							    ftemp = 0.0d0
							    DO ic = 1,effElem%numNodes
							      ie = effElem%Nodes(ic)

							      p1 = Posit(:,IP_Tet(1,ie))
								  p2 = Posit(:,IP_Tet(2,ie))
								  p3 = Posit(:,IP_Tet(3,ie))
								  p4 = Posit(:,IP_Tet(4,ie))

								  dimi = Geo3D_Tet_Quality(p1,p2,p3,p4,2) * 180.d0 * invPi

								  IF (dimi.LT.(0.5d0*Swapping_Angle_Deg)) THEN
								    ftemp = ftemp + HUGE(0.0d0)
								  ELSE 
				 					CALL isInside(ie,K,minVol)
				 					IF (K.EQ.0) THEN
				 					  CALL isMerged(ie,mergeFact)
				 					  IF (mergeFact.GT.0.0d0) THEN
				 					    pC(:) = (p1+p2+p3+p4) * 0.25d0
				 					    qual = Geo3D_Distance_SQ(pC(:),Sphere_Tet(1:3,ie)) &
				 					    	   / ((Scale_Tet(ie)*BGSpacing%BasicSize)**2.0d0)
				 					   	
				 					   
				 					    coef = (MAXVAL(Layers(IP_Tet(1:4,ie))))
				 					    ftemp = ftemp + qual*coef
				 					  END IF
				 					END IF
								  END IF


							    END DO

							    ftemp = ftemp/REAL(effElem%numNodes)


							  ENDIF
							 
							  IF (Loop.GT.numLocLoops) THEN
							  	
							    !Get the best result
							   
						  	  	  IF ((ip_loc.GE.NB1)) THEN
							  	  !IF ((ip_loc.GE.NB1).AND.(ip_loc.LE.NB2)) THEN
									 Posit(1,ip_loc) = psave(1) + d*Pt(1,Ibest)
									 Posit(2,ip_loc) = psave(2) + d*Pt(2,Ibest)
									 Posit(3,ip_loc) = psave(3) + d*Pt(3,Ibest)
								  END IF
				                  RR_Point(ip_loc) = psave(4) + dw*Pt(4,Ibest)
				                 
								DO ic = 1,effElem%numNodes

							       CALL Get_Tet_SphereCross(effElem%Nodes(ic))
							       CALL Get_Actual_Tet_Mapping(effElem%Nodes(ic))
							    END DO
								MCSFlag = 1
								EXIT
							  END IF
							  
							END DO
							fitFinal = fbest
							!IF (fitFinal.GT.0.0d0) THEN 
							!	locImp = (fitInit-fitFinal)/fitFinal
							!	WRITE(*,*) fitInit, fitFinal, locImp, Ibest
							!	numlocImp = numlocImp + 1.0d0
							!	sumlocImp = sumlocImp + locImp
							!ENDIF
							Mark_Point(ip_loc) = 1
						    CALL IntQueue_Clear(effElem)
						ENDIF
						ENDDO
					 END IF
			    ENDDO
			    DEALLOCATE(order)
			  	
			  ENDIF
			END DO

			IF (1.EQ.0) THEN
			!We need to reinsert
			CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
			CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
			CALL Next_Build()
			CircumUpdated = .TRUE.
			VolumeUpdated = .TRUE.
			DO ie = 1,NB_Tet
				CALL Get_Tet_SphereCross(ie)
				CALL Get_Tet_Volume(ie)			
			END DO
			CircumUpdated = .TRUE.
			VolumeUpdated = .TRUE.
			Mark_Point(1:NB_Point) = 0
			CALL Insert_Points_EasyPeasy(NB_BD_Point+1, NB_Point, 5)
			ENDIF

			Kswap = 2
			CALL Swap3D(Kswap)
			IF (Kswap.EQ.2) THEN
				!It's a valid mesh lets work out the quality
				CALL Next_Build()
				WRITE(*,*)' '
				WRITE(*,*)'---- Collapse Tiny Angles ----'
				CALL Element_Collapse( )
				CALL Next_Build()
				CircumUpdated = .TRUE.
				VolumeUpdated = .TRUE.
				DO ie = 1,NB_Tet
					CALL Get_Tet_SphereCross(ie)
					CALL Get_Tet_Volume(ie)	
					CALL Get_Actual_Tet_Mapping(ie)		
				END DO
				CircumUpdated = .TRUE.
				VolumeUpdated = .TRUE.
				CALL Label_Layers(Layers)
				CALL Get_Global_Quality(1,NB_Point,quality,badMask,Layers)
				WRITE(*,*)'Quality before ', bestQualityLast, ' after ',quality
				

				!First adapt the parameters
				improvement(origin) = (bestQualityLast-quality)/quality

				WRITE(*,*)'Improvement ',improvement(origin)
				IF (improvement(origin).GT.improvement(m)) THEN
					DO p = 1,numParam
					  CALL RANDOM_NUMBER(randn)
					  param(p,m) = param(p,m)+randn*(param(p,origin)-param(p,m))
					END DO
					improvement(m) = 0.0d0
					WRITE(*,*)'Adapting 1'
				ELSE IF (improvement(origin).GT.0.0d0) THEN
					DO p = 1,numParam
					  CALL RANDOM_NUMBER(randn)
					  param(p,origin) = param(p,origin)+randn*(param(p,m)-param(p,origin))
					END DO
					improvement(origin) = 0.0d0
					WRITE(*,*)'Adapting 2'
				ELSE
					CALL RANDOM_NUMBER(randn)
					
					param(1,origin) =  MAX(10.0d0,(REAL(NB_Tet)*randn))

					CALL RANDOM_NUMBER(randn)
					
					param(2,origin) = randn

					CALL RANDOM_NUMBER(randn)
					
					param(3,origin) = randn
					improvement(origin) = 0.0d0
					WRITE(*,*)'Adapting 3'
				END IF

				
			    !Also lets record the quality and time
			    CALL Check_Mesh_Geometry2('Coevolution')
			    

			END IF


			
			DO b = 1,numBuckets
			  CALL IntQueue_Clear(buckets(b))
			ENDDO
			DEALLOCATE(buckets,locTetQual)

		
		!Convergence test
		bestQuality = quality
		bestRes = ABS(bestQualityLast-bestQuality)

		WRITE(*,*)'Residuals: Best ', bestRes, ' stop at ',conv
		WRITE(29,*)'Residuals: Best ', bestRes, ' stop at ',conv
		WRITE(*,*)'Time left ', ((TimeEnd-TimeStart)-(CVT_Time_Limit*60.0d0*60.0d0))

		IF ((bestRes.LT.conv).OR.((TimeEnd-TimeStart)>(CVT_Time_Limit*60.0d0*60.0d0))) THEN
		  globConv = 1
		  EXIT
		ELSE
			
			bestQualityLast = bestQuality
		END IF

		MeshOut%GridOrder = GridOrder
	    MeshOut%NB_Point  = NB_Point
	    MeshOut%NB_Tet    = NB_Tet
	    ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
	    ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
	    MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
	    MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)
		
	    CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_coev',JobNameLength+5, -15, MeshOut)
	    
		DEALLOCATE(MeshOut%IP_Tet)
	    DEALLOCATE(MeshOut%Posit)


	  ENDDO

	  !IF (globConv.NE.1) THEN
	  ! CALL Remove_Packed_Node(NB1,NB_Point)
	  !END IF

	
	  


		

	  
	END DO

	


	DEALLOCATE(param)
	CALL PointAssociation_Clear(PointAsso2)

END SUBROUTINE Coevolution

!This routine calculates the quality of each element for sorting later
SUBROUTINE Get_Sorting_Elem_Qual(Layers,locQual,minQual,maxQual)
	USE common_Constants
	USE common_Parameters
	USE Geometry3DAll

	IMPLICIT NONE

	REAL*8, INTENT(IN) :: Layers(NB_Point)
	REAL*8, INTENT(OUT) :: locQual(NB_Tet)
	REAL*8, INTENT(OUT) :: minQual,maxQual

	INTEGER :: ie,K

	REAL*8  :: mergeFact, weight, dist, qual 

	REAL*8  :: pC(3),minVol

	minQual = HUGE(0.0d0)
	maxQual = 0.0d0

	DO ie = 1,NB_Tet

		!First check to see if we are inside or merged
		CALL isInside(ie,K,minVol)
		IF (K==0) THEN
		  CALL isMerged(ie,mergeFact)							
		  IF(mergeFact.GE.(0.0d0))THEN

		    pC(:) = 0.25d0*( Posit(:,IP_Tet(1,ie)) + Posit(:,IP_Tet(2,ie)) + &
		    	      Posit(:,IP_Tet(3,ie)) +  Posit(:,IP_Tet(4,ie)))

		    dist = Geo3D_Distance_SQ(pC(:),Sphere_Tet(1:3,ie)) / &
		           ((Scale_Tet(ie)*BGSpacing%BasicSize)**2.0d0)
		   
		    
		    qual = dist*((SUM(Layers(IP_Tet(1:4,ie))))*0.25d0)

		    IF (qual.LT.minQual) THEN
		      minQual = qual
		    ENDIF
		    IF (qual.GT.maxQual) THEN
		      maxQual = qual
		    ENDIF

		    locQual(ie) = qual

		  ELSE
		    locQual(ie) = 0.0d0
		  END IF
		ELSE
		  locQual(ie) = 0.0d0
		END IF

	ENDDO

END SUBROUTINE Get_Sorting_Elem_Qual


!This restores the mesh in 'mesh' completly so everything is ready to go
SUBROUTINE Restore_Everything(mesh,RR,Layers)
	USE common_Constants
	USE common_Parameters
	USE HybridMeshStorage
	USE TetMeshStorage
	IMPLICIT NONE

	TYPE(TetMeshStorageType), INTENT(IN) :: mesh
	REAL*8, INTENT(IN) :: RR(mesh%NB_Point)
	REAL*8, INTENT(OUT) :: Layers(mesh%NB_Point)
	INTEGER :: ie

	CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, mesh)
	CALL TetMeshStorage_NodeRestore(NB_Point, Posit, mesh)
	RR_Point(1:NB_Point) = RR(1:NB_Point)
	CALL Next_Build()
	CircumUpdated = .TRUE.
	VolumeUpdated = .TRUE.
	DO ie = 1,NB_Tet
	  CALL Get_Tet_SphereCross(ie)
	  CALL Get_Actual_Tet_Mapping(ie)
	  CALL Get_Tet_Volume(ie)
	END DO
	CircumUpdated = .TRUE.
	VolumeUpdated = .TRUE.
	CALL Label_Layers(Layers)

END SUBROUTINE Restore_Everything



!This subroutine looks for potential splits to help with the well
!centered optimisation
SUBROUTINE Search_and_Split_WellCentered()
  
  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  USE PointAssociation
  IMPLICIT NONE
  
  INTEGER :: outerFlag, split
  INTEGER :: ie, K, valancy(NB_Point)

  REAL*8  :: mergeFact

  INTEGER :: iFace, adjE, ieAdj, ip, ic, ib, count

  INTEGER :: inFlag,  Isucc, NB_Point_Old, ieBreak, i

  REAL*8  :: circum(3), pp(3), elemSize, minVol

  TYPE(PointAssociationArray) :: PointAsso2
 

  !The idea is to split an element based on valancy of it's nodes, first to calculate the valancy
  !Calculate mean edge length of tets

  CALL PointAssociation_Allocate(NB_Point, PointAsso2)
  CALL PointAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso2)

  NB_Point_Old = NB_Point
  DO ip = 1,NB_Point_Old
     Mark_Point(1:NB_Point) = 0
     count = 0
     Mark_Point(ip) = 1
     DO ic = 1,PointAsso2%PointList(ip)%numCells
      ie = PointAsso2%PointList(ip)%CellList(ic)
      DO i = 1,4
        ib = IP_Tet(i,ie)
        IF (Mark_Point(ib).EQ.0) THEN
          count = count + 1
          Mark_Point(ib) = 1
        ENDIF
      ENDDO
     ENDDO
     

     IF (count.LT.7) THEN

     	inFlag = 0
     	elemSize = 0.0d0

     	DO ic = 1,PointAsso2%PointList(ip)%numCells
      		ie = PointAsso2%PointList(ip)%CellList(ic)
      		IF (Scale_Tet(ie).GT.elemSize) THEN
      		  elemSize = Scale_Tet(ie)
      		  ieBreak = ie
      		ENDIF
      	ENDDO

      	!Check if a bad element is connected to this node
      	CALL isInside(ie,K,minVol)
  		IF (K.EQ.0) THEN
  			CALL isMerged(ie,mergeFact)
  			IF (mergeFact.GT.0.0d0) THEN
  				inFlag = 1
  			ENDIF
  		ENDIF

  		IF (inFlag.EQ.1) THEN

  			!Insert the point
  			ie = ieBreak
			pp(:) = 0.25d0*(Posit(:,IP_Tet(1,ie)) + Posit(:,IP_Tet(2,ie)) &
					      + Posit(:,IP_Tet(3,ie)) + Posit(:,IP_Tet(4,ie)))

		  	NB_Point = NB_Point + 1
            IF(NB_Point>nallc_point) THEN 
            	CALL ReAllocate_Point()
            ENDIF
            Posit(:,NB_Point) = pp(:)
            CALL Get_Point_Mapping(NB_Point)
            RR_Point(NB_Point) = 0.0d0

            Isucc   = 0
	        CALL Insert_Point_Delaunay(NB_Point,ie,Isucc,0)
	        IF(Isucc<=0)THEN
	           NB_Point = NB_Point-1
	        ENDIF

	        CALL Remove_Tet()
	        CALL PointAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso2)


  		ENDIF


     ENDIF
  ENDDO


  

  	WRITE(*,*) 'Number of points inserted = ',NB_Point-NB_Point_old


END SUBROUTINE Search_and_Split_WellCentered


!This removes a node
SUBROUTINE Remove_Packed_Node(NB1,NB2)

  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  USE PointAssociation

  IMPLICIT NONE

  INTEGER,INTENT(IN) :: NB1,NB2

  INTEGER :: badMask(NB_Point)

  INTEGER :: ip, finished, ic, ie
  INTEGER :: NB_Point_Old
  INTEGER :: i, i1, i2, i3
  INTEGER :: pointRemoved
  INTEGER :: nbp

  TYPE(PointAssociationArray) :: PointAsso2

  REAL*8 :: quality, Layers(NB_Point)
  REAL*8 :: distIdeal, radius, circum(3), pa(3), pb(3), pc(3)

  REAL*8 :: centroid(3)

  INTEGER :: iDel

  REAL*8  :: worseQual, qual

  !iDel is the point to delete set it to zero and worseQual keeps track of how
  !bad it is
  iDel = 0
  worseQual = 0.0d0

  !First do a prior check on quality
  CALL Check_Mesh_Geometry2('beforeRemoval')
  NB_Point_Old = NB_Point
  finished = 0
  CALL PointAssociation_Allocate(NB_Point, PointAsso2)
  


    CALL Get_Global_Quality(1,NB_Point,quality,badMask,Layers)
    CALL PointAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso2)
   
    !We will be looping around every point checking the volume and such
    Mark_Point(1:NB_Point) = 0
    pointRemoved = 0

    DO ip = NB1,NB2
      IF (badMask(ip).EQ.1) THEN
      	centroid(:) = 0.0d0
        !Loop around the elements surrounding ip
        DO ic = 1,PointAsso2%PointList(ip)%numCells
          ie = PointAsso2%PointList(ip)%CellList(ic)
         

          !Find out which node in ie ip is
          i = which_NodeinTet(ip,IP_Tet(:,ie))

          i1 = IP_Tet(iTri_Tet(1,i),ie)
		  i2 = IP_Tet(iTri_Tet(2,i),ie)
		  i3 = IP_Tet(iTri_Tet(3,i),ie)

		  pa(:) = Posit(:,i1)
		  pb(:) = Posit(:,i2)
		  pc(:) = Posit(:,i3)

		  circum(:) = Geo3D_Sphere_Centre(pa,pb,pc)
		  centroid(:) = centroid(:) + circum
		  
        END DO

        centroid(:) = centroid(:)/REAL(PointAsso2%PointList(ip)%numCells)

        DO ic = 1,PointAsso2%PointList(ip)%numCells
          ie = PointAsso2%PointList(ip)%CellList(ic)
         

          !Find out which node in ie ip is
          i = which_NodeinTet(ip,IP_Tet(:,ie))

          i1 = IP_Tet(iTri_Tet(1,i),ie)
		  i2 = IP_Tet(iTri_Tet(2,i),ie)
		  i3 = IP_Tet(iTri_Tet(3,i),ie)

		  pa(:) = Posit(:,i1)
		  pb(:) = Posit(:,i2)
		  pc(:) = Posit(:,i3)

		  circum(:) = Geo3D_Sphere_Centre(pa,pb,pc)

		  radius = Geo3D_Distance_SQ(circum,pa)
		  distIdeal = Geo3D_Distance_SQ(circum,centroid)

		  IF (radius.GT.distIdeal) THEN
	        
		  	qual = (radius - distIdeal)/distIdeal
		  	IF (qual.GT.worseQual) THEN
		  	  worseQual = qual
		  	  iDel = ip
		  	ENDIF
	        
	      END IF
		  
        END DO

      ENDIF
      
     
    END DO

     IF (iDel.GT.0) THEN
      Mark_Point(iDel) = -999
      CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
	  CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
	  CALL Next_Build()
	  CircumUpdated = .TRUE.
	  VolumeUpdated = .TRUE.
	  DO ie = 1,NB_Tet
		CALL Get_Tet_SphereCross(ie)
		CALL Get_Tet_Volume(ie)			
	  END DO
	  CircumUpdated = .TRUE.
	  VolumeUpdated = .TRUE.
      
      CALL Remove_Point()
 	  CALL Insert_Points_EasyPeasy(NB_BD_Point+1, NB_Point, 5)
 	  CALL Check_Mesh_Geometry2('afterRemoval')
   
    END IF
   
 

  

  WRITE(*,*) 'Number of points removed ', NB_Point_Old - NB_Point

  CALL PointAssociation_Clear(PointAsso2)
END SUBROUTINE Remove_Packed_Node






















