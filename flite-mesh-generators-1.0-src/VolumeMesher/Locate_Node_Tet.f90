!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!*******************************************************************************
!>
!!   Locate a point in a tetrahedron.
!!   Make sure that the neighbouring system (i.e. common_Parameters::NEXT_Tet)
!!      has been built.
!! 
!!   @param[in] Pt  :  the coordinates of the point    
!!   @param[in] IT  :  the initial cell ID of search.
!!                     If input as an invalid ID,
!!                     the ID as the output last time will be used.
!!   @param[in] Isucc =0 : if can not locate Pt in a tetrahedron,
!!                           return the nearest tetrahedron with - sign.     \n
!!                    =1 : if can not locate Pt in a tetrahedron,
!!                            terminate the code.                           \n
!!                    =2 : if can not locate Pt in a tetrahedron
!!                            within one clue, terminate the code.
!!                    =-2 : As above but don't terminate
!!		      =3 : if can not locate Pt in a tetrahedron
!!                            within four clues, return the nearest tet with - sign \n
!!   @param[out] IT  : the number of a tetrahedron in which the point lies.
!!                      if IT<0, then the search is fail and
!!                      abs(IT) it the element nearest to the point.
!!
!!   common_Parameters::Visit_Tet and
!!   Function common_Parameters::GetVisitCount() are applied here.
!<
!*******************************************************************************
SUBROUTINE Locate_Node_Tet(Pt,IT,Isucc)

  USE common_Constants
  USE common_Parameters
  USE Geometry3D
  IMPLICIT NONE

  INTEGER, INTENT(INOUT) :: IT
  INTEGER, INTENT(IN)    :: Isucc
  REAL*8,  INTENT(IN)    :: Pt(3)
  INTEGER, SAVE :: ITsave = 1
  INTEGER :: ThisVisit

  INTEGER :: i, i1, i2, i3, it2, markstart, ind, Itnear, ItPrv, numClue,flag
  REAL*8  :: pp(3,4), p0(3), v6, vmin, d, dmin

  ThisVisit = GetVisitCount()

  numClue = 1

  IF(IT<1 .OR. IT>NB_Tet) IT = ITsave
  IF(IT<1 .OR. IT>NB_Tet) IT = 1       !--- in case ITsave not safe
  markstart = NB_Tet
  dmin = 1.d20
  ItPrv = 0

  LOOP_1 : DO

     DO i=1,4
        pp(:,i) = Posit(:,IP_Tet(i,IT))
     ENDDO

     vmin = HugeValue
     DO i=1,4
        IF(NEXT_Tet(i,IT)==ItPrv) cycle
        i1=iTri_Tet(1,i)
        i2=iTri_Tet(2,i)
        i3=iTri_Tet(3,i)
        v6=Geo3D_Tet_Volume6_High(pp(:,i1),pp(:,i2),pp(:,i3),Pt(:))
        !v6=Geo3D_Tet_Volume6_Pred(pp(:,i1),pp(:,i2),pp(:,i3),Pt(:))
        IF(v6<vmin)THEN
           ind  = i
           vmin = v6
        ENDIF
     ENDDO

     IF(vmin>=0)THEN
        ITsave = IT
        RETURN
     ELSE
        !--- the point is out of element IT, check it's neighbour
        IF(Isucc==0)THEN
           p0(:) = (pp(:,1)+pp(:,2)+pp(:,3)+pp(:,4))/4.d0
           d = Geo3D_Distance_SQ(Pt,p0)
           IF(d<dmin)THEN
              Itnear = IT
              dmin   = d
           ENDIF
        ENDIF
        Visit_Tet(IT) = ThisVisit
        ItPrv         = It
        IT            = NEXT_Tet(ind,IT)
        flag = 0
        IF(IT>0) THEN
          IF( Visit_Tet(IT)==ThisVisit)THEN
              flag = 1
          ENDIF
        ELSE
           flag = 1
        ENDIF
        IF(flag == 1) THEN
           IF(IT>0 .AND. vmin>=-TinyVolume)THEN
              !--- a dead loop, likely the point is on a edge.
              IT     = ItPrv
              ITsave = IT
              RETURN
           ENDIF
           
           IF(ABS(Isucc)==2)THEN
              IF (Isucc<0)THEN
                IT = 0
                RETURN
              ELSE
               WRITE(29,*)'Error--- Fail to search node Pt:',real(Pt),' in one clue, IT=',IT
               CALL Error_Stop ('Locate_Node_Tet')
              ENDIF
           ELSEIF(Isucc==3)THEN
              IF(numClue>3)THEN
               IT = -1
               RETURN
              ELSE
               numClue = numClue+1
              ENDIF
           ENDIF
           !--- the clue breaks, restart a new clue from another element.
           DO it2=markstart,1,-1
              IF(IP_Tet(4,it2)>0 .AND. Visit_Tet(it2)/=ThisVisit) EXIT
           ENDDO
           IT = it2
           ItPrv = 0
           markstart = it2-1
           IF(IT<1)THEN
              !--- fail to search, stop or return a negative value
              IF(Isucc/=0)THEN
                 WRITE(29,*)'Error--- Fail to search node Pt:',real(Pt)
                 CALL Error_Stop ('Locate_Node_Tet')
              ENDIF
              IT = -Itnear
              IF(IT==0)THEN
                 WRITE(29,*)'Error--- Check this point Pt:',real(Pt)
                 CALL Error_Stop ('Locate_Node_Tet')
              ENDIF
              ITsave = ABS(IT)
              RETURN
           ENDIF
        ENDIF
     ENDIF
  ENDDO LOOP_1

  WRITE(29,*)'Error--- should not reach here'
  CALL Error_Stop ('  Locate_Node_Tet :: ')
END SUBROUTINE Locate_Node_Tet


!*******************************************************************************
!>
!!   Locate a point in a tetrahedron, this is done with a cheap check for volume
!!   Make sure that the neighbouring system (i.e. common_Parameters::NEXT_Tet)
!!      has been built.
!! 
!!   @param[in] Pt  :  the coordinates of the point    
!!   @param[in] IT  :  the initial cell ID of search.
!!                     If input as an invalid ID,
!!                     the ID as the output last time will be used.
!!   @param[in] Isucc =0 : if can not locate Pt in a tetrahedron,
!!                           return the nearest tetrahedron with - sign.     \n
!!                    =1 : if can not locate Pt in a tetrahedron,
!!                            terminate the code.                           \n
!!                    =2 : if can not locate Pt in a tetrahedron
!!                            within one clue, terminate the code.
!!                    =-2 : As above but don't terminate
!!		      =3 : if can not locate Pt in a tetrahedron
!!                            within four clues, return the nearest tet with - sign \n
!!   @param[out] IT  : the number of a tetrahedron in which the point lies.
!!                      if IT<0, then the search is fail and
!!                      abs(IT) it the element nearest to the point.
!!
!!   common_Parameters::Visit_Tet and
!!   Function common_Parameters::GetVisitCount() are applied here.
!<
!*******************************************************************************
SUBROUTINE Locate_Node_Tet_Cheap(Pt,IT,Isucc)

  USE common_Constants
  USE common_Parameters
  USE Geometry3D
  IMPLICIT NONE

  INTEGER, INTENT(INOUT) :: IT
  INTEGER, INTENT(IN)    :: Isucc
  REAL*8,  INTENT(IN)    :: Pt(3)
  INTEGER, SAVE :: ITsave = 1
  INTEGER :: ThisVisit

  INTEGER :: i, i1, i2, i3, it2, markstart, ind, Itnear, ItPrv, numClue,flag
  REAL*8  :: pp(3,4), p0(3), v6, vmin, d, dmin

  ThisVisit = GetVisitCount()

  numClue = 1

  IF(IT<1 .OR. IT>NB_Tet) IT = ITsave
  IF(IT<1 .OR. IT>NB_Tet) IT = 1       !--- in case ITsave not safe
  markstart = NB_Tet
  dmin = 1.d20
  ItPrv = 0

  LOOP_1 : DO

     DO i=1,4
        pp(:,i) = Posit(:,IP_Tet(i,IT))
     ENDDO

     vmin = HugeValue
     DO i=1,4
        IF(NEXT_Tet(i,IT)==ItPrv) cycle
        i1=iTri_Tet(1,i)
        i2=iTri_Tet(2,i)
        i3=iTri_Tet(3,i)
        !v6=Geo3D_Tet_Volume6_High(pp(:,i1),pp(:,i2),pp(:,i3),Pt(:))
        v6=Geo3D_Tet_Volume(pp(:,i1),pp(:,i2),pp(:,i3),Pt(:))
        IF(v6<vmin)THEN
           ind  = i
           vmin = v6
        ENDIF
     ENDDO

     IF(vmin>=0)THEN
        ITsave = IT
        RETURN
     ELSE
        !--- the point is out of element IT, check it's neighbour
        IF(Isucc==0)THEN
           p0(:) = (pp(:,1)+pp(:,2)+pp(:,3)+pp(:,4))/4.d0
           d = Geo3D_Distance_SQ(Pt,p0)
           IF(d<dmin)THEN
              Itnear = IT
              dmin   = d
           ENDIF
        ENDIF
        Visit_Tet(IT) = ThisVisit
        ItPrv         = It
        IT            = NEXT_Tet(ind,IT)
        flag = 0
        IF(IT>0) THEN
          IF( Visit_Tet(IT)==ThisVisit)THEN
              flag = 1
          ENDIF
        ELSE
           flag = 1
        ENDIF
        IF(flag == 1) THEN
           IF(IT>0 .AND. vmin>=-TinyVolume)THEN
              !--- a dead loop, likely the point is on a edge.
              IT     = ItPrv
              ITsave = IT
              RETURN
           ENDIF
           
           IF(ABS(Isucc)==2)THEN
              IF (Isucc<0)THEN
                IT = 0
                RETURN
              ELSE
               WRITE(29,*)'Error--- Fail to search node Pt:',real(Pt),' in one clue, IT=',IT
               CALL Error_Stop ('Locate_Node_Tet')
              ENDIF
           ELSEIF(Isucc==3)THEN
              IF(numClue>3)THEN
               IT = -1
               RETURN
              ELSE
               numClue = numClue+1
              ENDIF
           ENDIF
           !--- the clue breaks, restart a new clue from another element.
           DO it2=markstart,1,-1
              IF(IP_Tet(4,it2)>0 .AND. Visit_Tet(it2)/=ThisVisit) EXIT
           ENDDO
           IT = it2
           ItPrv = 0
           markstart = it2-1
           IF(IT<1)THEN
              !--- fail to search, stop or return a negative value
              IF(Isucc/=0)THEN
                 WRITE(29,*)'Error--- Fail to search node Pt:',real(Pt)
                 CALL Error_Stop ('Locate_Node_Tet')
              ENDIF
              IT = -Itnear
              IF(IT==0)THEN
                 WRITE(29,*)'Error--- Check this point Pt:',real(Pt)
                 CALL Error_Stop ('Locate_Node_Tet')
              ENDIF
              ITsave = ABS(IT)
              RETURN
           ENDIF
        ENDIF
     ENDIF
  ENDDO LOOP_1

  WRITE(29,*)'Error--- should not reach here'
  CALL Error_Stop ('  Locate_Node_Tet :: ')
END SUBROUTINE Locate_Node_Tet_Cheap


