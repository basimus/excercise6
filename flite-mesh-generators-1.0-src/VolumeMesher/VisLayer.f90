!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!
! .................................................................
!  This routine generate the viscous layers
! .................................................................

SUBROUTINE VisLayer( ) 

  USE common_Parameters
  USE BoxADTreeModule
  USE SurfaceMeshManager
  USE OptimisingModule
  IMPLICIT NONE

  INTEGER :: nn(4) = (/2,3,1,2/)
  INTEGER :: mn(3),  mm(3), itfr(10000), nrej(20), ierr10
  REAL*8  :: cf1(3), cf2(3), cf3(3), pp1(3), pp2(3), pp3(3)
  REAL*8  :: pa(3), pb(3), pc(3), pd(3), pe(3), dab(3), dp(3)
  REAL*8  :: pMax(3), pMin(3)
  REAL*8  :: xel1(3),  xel2(3), helpb(3), pen(3), citt(3), cit(3), a(3), aa
  REAL*8  :: p2D1(2), p2D2(2), p1D1(1), p1D2(1)

  INTEGER, DIMENSION(:,:), POINTER :: ifrnt
  INTEGER, DIMENSION(:,:), POINTER :: kfrnt
  INTEGER, DIMENSION(:),   POINTER :: layer, lorde, ipOrig, lboun, NextLay, markp
  INTEGER, DIMENSION(:,:), POINTER :: kfrntSave
  REAL*8,  DIMENSION(:,:), POINTER :: snorm

  REAL*8  :: hlay, hlayhf, dotpr, dxm, vl, omega, cosm, v1, cosa, dn, vlm, vlmx, ang, ann, dspac, t
  LOGICAL :: good, ex
  INTEGER :: IPend
  INTEGER :: mxbou, mxpoi, neold, np_lay
  INTEGER :: i, i1, i2, j, j1, it, jt, k, k1, k2, k3, jc, kk, IX, ib, nb
  INTEGER :: iTry, nTry, ie, iw, np, np1
  INTEGER :: IP, IP0, lc, jj, il, jnewp, idir, ndir, iemin1, iemin2, isf
  INTEGER :: ie1, ip1, ip2, if1, if2, if3, nfacl, itr, NI(1000)

  TYPE(BoxADTreeType)       :: BoxADTree
  TYPE(NodeNetTreeType)     :: TriTree
  TYPE(LinkAssociationType) :: ITR_Pt
  TYPE(IntQueueType)        :: nextNodes, closeNodes

  IF(NB_BD_Layers==0) RETURN

  IF(Frame_Stitch==-1)THEN
     WRITE(29,*)'----- Read viscous layers -----'
     INQUIRE(file = JobName(1:JobNameLength)//'_v1.plt',  EXIST=ex)
     IF(.NOT. ex)THEN
        WRITE(*, *)' Error--- miss file *_v1.plt for stitch. Or set Frame_Stitch=0'
        WRITE(29,*)' Error--- miss file *_v1.plt for stitch. Or set Frame_Stitch=0'
        STOP
     ENDIF
     INQUIRE(file = JobName(1:JobNameLength)//'_vis.map', EXIST=ex)
     IF(.NOT. ex)THEN
        WRITE(*, *)' Error--- miss file *_vis.map for stitch. Or set Frame_Stitch=0'
        WRITE(29,*)' Error--- miss file *_vis.map for stitch. Or set Frame_Stitch=0'
        STOP
     ENDIF
     
     CALL HybridMeshStorage_Input  (JobName(1:JobNameLength)//'_v1',JobNameLength+3,-15,VM,SurfOld)
     OPEN(1,file=JobName(1:JobNameLength)//'_vis.map',  form='UNFORMATTED')
     ALLOCATE(Mark_Vis_Point(VM%NB_Point))
     READ(1,err=112)Mark_Vis_Point(1:VM%NB_Point)
     CLOSE(1)
     RETURN
  ENDIF

  IF(Start_Point==2) RETURN
  WRITE(*, *)'----- Generate viscous layers -----'
  WRITE(29,*)'----- Generate viscous layers -----'
  WRITE(29,*)'      NB_BD_Layers = ', NB_BD_Layers

  !--- read viscous layers


  mxbou = 7* Surf%NB_Tri
  mxpoi = (NB_BD_Layers+2) * Surf%NB_Point
  ALLOCATE(kfrnt(5,mxbou))
  ALLOCATE(lboun(mxpoi))
  ALLOCATE(lorde(Surf%NB_Point))
  ALLOCATE(markp(Surf%NB_Point))
  ALLOCATE(snorm(3,Surf%NB_Point))
  ALLOCATE(layer(mxpoi))
  ALLOCATE(ipOrig(mxpoi))
  ALLOCATE(NextLay(mxpoi))
  ALLOCATE(kfrntSave(3,1000))

  VM%NB_Point = Surf%NB_Point
  ALLOCATE(VM%Posit(3,VM%NB_Point))
  DO i = 1, Surf%NB_Point
     VM%Posit(:,i) = Surf%Posit(:,i)
  ENDDO
  DO i = 1, mxpoi
     ipOrig(i)  = i
     NextLay(i) = i
     layer(i)   = 0
  ENDDO

  !--- calculate surface normal

  ALLOCATE( SurfTri(  mxbou  ) )
  ALLOCATE( SurfNode( Surf%NB_Point ) )
  CALL Surf_BuildTriAsso(Surf)

  ! *** mark nodes on symmetry planes
  ! *** lboun() =  0 : from symmetry x symmetry x wall
  ! *** lboun() =  i : from symmetry(i) x wall
  ! *** lboun() = -1 : wall
  ! *** lboun() = -2 : else
  CALL setdt(lboun)

  CALL SurfaceMeshStorage_Measure (Surf, 3, XYZmin, XYZmax)
  CALL BoxADTree_SetTreeDomain (BoxADTree, XYZmin, XYZmax)
  CALL NodeNetTree_Allocate (3, mxpoi, mxbou, TriTree)
  CALL LinkAssociation_Allocate(mxpoi, ITR_Pt)

  DO ib = 1 , Surf%NB_Tri
     IF(Surf%IP_Tri(5,ib)<0) CYCLE
     mn(1:3) = Surf%IP_Tri(1:3,ib)
     CALL NodeNetTree_SearchAdd (3, mn, TriTree, IX)
     DO i = 1,3
        pMin(i) = MIN(Surf%Posit(i,mn(1)),Surf%Posit(i,mn(2)),Surf%Posit(i,mn(3)))
        pMax(i) = MAX(Surf%Posit(i,mn(1)),Surf%Posit(i,mn(2)),Surf%Posit(i,mn(3)))
     ENDDO
     CALL BoxADTree_AddNode(BoxADTree, IX, pMin, pMax)
     CALL LinkAssociation_AddCell(3, IX, mn, ITR_Pt)
     kfrnt(1:5,IX) = Surf%IP_Tri(1:5,ib)
     kfrnt(4,  IX) = 0
     SurfTri(IX)   = SurfTri(ib)
  ENDDO


  ! *** Place in lorde the points on the solid wall in the order to be considered
  !--- sharft order to put trailing nodes to the end
  !                 and put nodes symmetry*wall to the begining

  CALL Surf_clusterSort(Surf, markp)

  IPend = 0
  DO jj = 1, Surf%NB_Point
     IP = markp(jj)
     IF(lboun(IP) == 0)THEN
        ! --- symmetry*symmetry*wall node
        IPend = IPend+1
        lorde(IPend) = IP
        markp(jj) = 0
     ENDIF
  ENDDO
  DO jj = 1, Surf%NB_Point
     IP = markp(jj)
     IF(IP==0) CYCLE
     IF(lboun(IP) >0)THEN
        ! --- symmetry*wall node
        IPend = IPend+1
        lorde(IPend) = IP
        markp(jj) = 0
     ELSE IF(lboun(IP) == -2)THEN
        ! --- not solid wall
        markp(jj) = 0
     ENDIF
  ENDDO
  DO jj = 1, Surf%NB_Point
     IP = markp(jj)
     IF(IP==0) CYCLE
     IF(SurfNode(IP)%onRidge == 0)THEN
        ! --- not trailing node
        IPend = IPend+1
        lorde(IPend) = IP
        markp(jj) = 0
     ENDIF
  ENDDO
  DO jj = 1, Surf%NB_Point
     IP = markp(jj)
     IF(IP==0) CYCLE
     IPend = IPend+1
     lorde(IPend) = IP
  ENDDO
  IF(IPend>Surf%NB_Point) CALL Error_Stop( 'IPend>Surf%NB_Point')

  IF(NorSmooth_Method /= 1) THEN
     ! *** Smooth the normals at solid wall
     omega = 1.
     CALL smnrm( omega,  lboun, snorm, layer )
  ENDIF

  Loop_20 : DO il = 1 , NB_BD_Layers

     nrej(:)  = 0
     ierr10   = 1

     hlay   = Layer_Heights(il)
     hlayhf = hlay / 2.d0
     IF(NorSmooth_Method == 1) THEN
        ! *** Smooth the normals at solid wall
        omega = float(MAX(0,il-1))/float(NB_BD_Layers)
        CALL smnrm( omega, lboun, snorm,  layer)
     ENDIF

     !--- the second loop allow merging
     DO lc = 1,2

        ! *** Consider each point in turn

        Loop_IP : DO jj = 1 , IPend
           IP = lorde(jj)
           IF(IP<=0) CYCLE Loop_IP

           IF(layer(IP) /= il-1)THEN
              IF(layer(IP)<il-1)THEN
                 nrej(8)   = nrej(8) +1
                 lorde(jj) = 0
              ENDIF
              CYCLE Loop_IP
           ENDIF

           IP0 = ipOrig(IP)
           pa(:) = VM%Posit(:,IP)

           ! *** if layer height is close to the point spacing terminate generation
           !     at this point
           CALL SpacingStorage_GetDirectGridSize (BGSpacing, pa, snorm(:,IP0), dspac)
           IF(hlay > 0.9*dspac) THEN
              nrej(9)   = nrej(9) +1
              lorde(jj) = 0
              CYCLE Loop_IP
           ENDIF

           CALL LinkAssociation_List (IP, List_Length, Its_List, ITR_Pt)

           IF(List_Length==0)THEN
              lorde(jj) = 0
              CYCLE Loop_IP
           ENDIF

           IF(lc==1 .OR. lboun(IP)==0)THEN
              nTry = 0
           ELSE
              nTry = List_Length
           ENDIF
           Loop_iTry : DO iTry = 0, nTry

              IF(iTry==0)THEN

                 dp(:) = snorm(:,IP0)
                 dotpr = Geo3D_Dot_Product(SurfNode(IP0)%Anor(:,2), dp)
                 IF(dotpr <= 0.5)     dotpr = 0.5
                 IF(SurfNode(IP0)%onRidge /= 0) dotpr = 0.4
                 pb(:) = pa(:) + hlay/dotpr * dp(:)

                 jnewp = VM%NB_Point + 1
                 IF(jnewp > mxpoi) CALL Error_Stop( 'jnewp > mxpoi' )
                 IF(jnewp > keyLength(VM%Posit))THEN
                    CALL allc_2Dpointer(VM%Posit, 3, jnewp+100000)
                 ENDIF
                 VM%Posit(:,jnewp) = pb(:)

              ELSE

                 ie    = Its_List(iTry)
                 iw    = which_NodeinTri(IP, kfrnt(1:3,ie))
                 jnewp = kfrnt(nn(iw),  ie)
                 IF(Layer(jnewp)/=il)  CYCLE Loop_iTry
                 IF(lboun(IP)>0 .AND. lboun(jnewp)<0) CYCLE Loop_iTry
                 dab(:) = VM%Posit(:,jnewp) - pa(:)
                 vl     = Geo3D_Distance(dab)
                 IF(vl>2*hlay .OR. Geo3D_Dot_Product(dab, snorm(:,IP0))<0.7*vl) CYCLE Loop_iTry
                 pb(:) = VM%Posit(:,jnewp)                 
              ENDIF

              ! *** loop over surrounding elements, list surrounding nodes
              nextNodes%numNodes = 0 
              DO i = 1,List_Length
                 ie = Its_List(i)
                 iw = which_NodeinTri(IP, kfrnt(1:3,ie))
                 IF(iw==0)THEN
                    PRINT*,'IP,ie,kf=',IP,ie,kfrnt(:,ie)
                    CALL Error_Stop( '---- iw = 0')
                 ENDIF
                 DO k = 0,1
                    np  = kfrnt(nn(iw+k),ie)
                    IF(np==jnewp) CYCLE
                    IF(IntQueue_Contain(nextNodes, np)) CYCLE
                    CALL IntQueue_Push(nextNodes, np)   
                    IF(ABS(layer(np )) < il-1 )THEN
                       nrej(7)   = nrej(7) +1
                       lorde(jj) = 0
                       CYCLE Loop_IP
                    ENDIF
                 ENDDO
              ENDDO

              !--- check volume of posible new elements
              dab(:)  = pb(:) - pa(:)
              dab(:)  = dab(:) / Geo3D_Distance(dab)
              ndir    = 0
              vlmx    = 0
              DO i = 1,List_Length
                 ie = Its_List(i)
                 IF(kfrnt(1,ie)==jnewp) CYCLE
                 IF(kfrnt(2,ie)==jnewp) CYCLE
                 IF(kfrnt(3,ie)==jnewp) CYCLE

                 vl = Geo3D_Dot_Product(dab, SurfTri(ie)%anor)

                 IF(vl > 0) CYCLE

                 !--- negative element found
                 IF(iTry==0 .AND. lc==2)THEN                 
                    !--- give chances to adjust the new point
                    IF(ndir==0) nrej(3) = nrej(3) +1
                    IF(lboun(IP)==-1)THEN
                       ndir = 1
                    ELSE IF(lboun(IP)>0)THEN
                       ndir = 2
                    ELSE
                       ndir = 0
                       CYCLE Loop_iTry
                    ENDIF
                    EXIT
                 ELSE                
                    CYCLE Loop_iTry
                 ENDIF
              ENDDO

              !--- if ndir>0, adjust the new point to reach all good element

              IF(ndir>0)THEN
                 IF(ndir==1)THEN
                    !--- using optimising for non-symmetry node
                    OptVariable%IdxFun  = 3
                    OptParameter%Idistb = 500
                    CALL Optimising_Set(2, 200, 1.d-3*hlay)

                    OptPlane%anor(:) = snorm(:,IP0)
                    OptPlane%d       = - Geo3D_Dot_Product(pb, OptPlane%anor(:))
                    CALL Plane3D_buildTangentAxes (OptPlane)
                    p2D1 = Plane3D_project2D (OptPlane, Pb)

                    OptVariable%IdNode  =  IP
                    IP_BD_Tri => kfrnt
                    CALL Optimising_Powell(p2D1, p2D2)
                    NULLIFY(IP_BD_Tri)
                    vlmx = OptParameter%Fend
                    IF(vlmx>0)THEN
                       pb(:) = Plane3D_get3DPosition (OptPlane, P2D2)
                    ENDIF
                 ELSE
                    !--- using optimising for symmetry node
                    OptVariable%IdxFun  = 5
                    OptParameter%Idistb = 10
                    CALL Optimising_Set(1, 100, 1.d-3)

                    DO i = 1,List_Length
                       ie = Its_List(i)
                       iw = which_NodeinTri(IP, kfrnt(1:3,ie))
                       np = kfrnt(nn(iw),  ie)
                       IF(lboun(np)>=0)THEN
                          pc(:) = VM%Posit(:,np) - pa(:)
                          EXIT
                       ENDIF
                    ENDDO
                    pd = Geo3D_Cross_Product(dab,pc)
                    pc = Geo3D_Cross_Product(pd,dab)                        
                    OptDirct1(:) = pc(:) / Geo3D_Distance(pc)
                    OptDirct2(:) = dab(:)
                    P1D1(:) = 1.57d0

                    OptVariable%IdNode  =  IP
                    IP_BD_Tri => kfrnt
                    CALL Optimising_Powell(p1D1, p1D2)
                    NULLIFY(IP_BD_Tri)
                    vlmx = OptParameter%Fend
                    IF(vlmx>0)THEN
                       dab(:) = COS(P1D2(1)) * OptDirct1(:) + SIN(P1D2(1)) * OptDirct2(:)
                       pb(:)  = pa(:) + dab(:) *hlay/dotpr
                    ENDIF
                 ENDIF


                 IF(vlmx>0)THEN
                    VM%Posit(:,jnewp) = pb(:)
                    snorm(:,IP0) = pb(:) - pa(:)
                    snorm(:,IP0) = snorm(:,IP0) / Geo3D_Distance(snorm(:,IP0))
                 ELSE
                    nrej(5) = nrej(5) +1
                    CYCLE Loop_iTry
                 ENDIF
              ENDIF


              ! *** search closing triangles and nodes
              xel1(:) = pb(:) - hlayhf
              xel2(:) = pb(:) + hlayhf
              DO i = 1,nextNodes%numNodes
                 np = nextNodes%Nodes(i)
                 xel1(:) = MIN(xel1(:), VM%Posit(:,np))
                 xel2(:) = MAX(xel2(:), VM%Posit(:,np))
              ENDDO

              nfacl = 10000
              CALL BoxADTree_SearchNode (BoxADTree, nfacl, itfr, xel1, xel2)
              IF(nfacl>=10000) CALL Error_Stop( 'nfacl>=10000')

              closeNodes%numNodes = 0
              DO k = 1 , nfacl
                 itr = itfr(k)
                 mn(1:3) = kfrnt(1:3,itr)
                 IF( lboun(mn(1))>=0 .AND. lboun(mn(2))>=0 .AND. lboun(mn(3))>=0 )THEN
                    itfr(k) = 0
                    CYCLE
                 ENDIF
                 DO i = 1,3
                    IF(mn(i)==IP .OR. mn(i)==jnewp)THEN
                       itfr(k) = 0
                       CYCLE
                    ENDIF
                    IF(IntQueue_Contain(closeNodes, mn(i))) CYCLE
                    CALL IntQueue_Push(closeNodes, mn(i))
                 ENDDO
              ENDDO

              !--- check the new point

              IF(iTry==0)THEN

                 !--- check if it is too close to symmetry plane
                 IF(lboun(IP) < 0) THEN
                    dn = hlayhf
                    IF(  pb(1)+dn > XYZmax(1) .OR. pb(1)-dn < XYZmin(1) .OR.      &
                         pb(2)+dn > XYZmax(2) .OR. pb(2)-dn < XYZmin(2) .OR.      & 
                         pb(3)+dn > XYZmax(3) .OR. pb(3)-dn < XYZmin(3))THEN
                       IF(lc==1) nrej(2) = nrej(2) +1
                       CYCLE Loop_iTry
                    ENDIF
                 ENDIF

                 !--- check if it is too close to an exsiting triangle
                 DO k = 1 , nfacl
                    itr = itfr(k)
                    IF(itr==0) CYCLE
                    mn(1:3) = kfrnt(1:3,itr)
                    pp1(:) = VM%Posit(:,mn(1))
                    pp2(:) = VM%Posit(:,mn(2))
                    pp3(:) = VM%Posit(:,mn(3))
                    dxm = Plane3D_DistancePointToTriangle (pb, pp1,pp2,pp3)
                    IF(dxm < hlayhf) THEN
                       IF(lc==1) nrej(4) = nrej(4) + 1
                       CYCLE Loop_iTry
                    ENDIF
                 ENDDO
              ENDIF

              ! *** check if new edge intersecting with existing triangles

              DO i = 1,nextNodes%numNodes
                 mm(1) = nextNodes%Nodes(i)
                 mm(2) = jnewp
                 cf1(:) = VM%Posit(:,mm(1))
                 cf2(:) = VM%Posit(:,mm(2))

                 xel1(:) = MIN(cf1(:), cf2(:))
                 xel2(:) = MAX(cf1(:), cf2(:))

                 good = .TRUE.
                 DO k = 1 , nfacl
                    itr = itfr(k)
                    IF(itr==0) CYCLE
                    mn(1:3) = kfrnt(1:3,itr)
                    IF(mn(1) == mm(1) .OR. mn(2) == mm(1) .OR. mn(3) == mm(1)) CYCLE

                    pp1(:) = VM%Posit(:,mn(1))
                    pp2(:) = VM%Posit(:,mn(2))
                    pp3(:) = VM%Posit(:,mn(3))

                    IF(xel1(1)>MAX(pp1(1),pp2(1),pp3(1))) CYCLE
                    IF(xel1(2)>MAX(pp1(2),pp2(2),pp3(2))) CYCLE
                    IF(xel1(3)>MAX(pp1(3),pp2(3),pp3(3))) CYCLE
                    IF(xel2(1)<MIN(pp1(1),pp2(1),pp3(1))) CYCLE
                    IF(xel2(2)<MIN(pp1(2),pp2(2),pp3(2))) CYCLE
                    IF(xel2(3)<MIN(pp1(3),pp2(3),pp3(3))) CYCLE

                    CALL inters(pp1,pp2,pp3, cf1,cf2, good)
                    IF(.NOT.good)THEN
                       IF(lc==1)  nrej(1) = nrej(1) + 1
                       CYCLE Loop_iTry
                    ENDIF
                 ENDDO
              ENDDO

              !--- check if new triangles intersecting with edges or close exsiting nodes
              Loop_40 : DO i = 1,List_Length
                 ie = Its_List(i)

                 iw = which_NodeinTri(IP, kfrnt(1:3,ie))
                 np = kfrnt(nn(iw),ie)
                 np1= kfrnt(nn(iw+1),ie)
                 IF(np==jnewp .OR. np1==jnewp) CYCLE Loop_40
                 mm(1:3) = (/ np, np1, jnewp /)

                 cf1(:) = VM%Posit(:,mm(1))
                 cf2(:) = VM%Posit(:,mm(2))
                 cf3(:) = VM%Posit(:,mm(3))

                 xel1(:) = MIN(cf1(:), cf2(:), cf3(:))
                 xel2(:) = MAX(cf1(:), cf2(:), cf3(:))

                 !--- check if new triangles close exsiting nodes
                 pMin(:) = xel1(:) - hlayhf
                 pMax(:) = xel2(:) + hlayhf
                 DO k = 1, closeNodes%numNodes
                    mn(1) = closeNodes%Nodes(k)
                    IF(  mn(1) == mm(1) .OR. mn(1) == mm(2) .OR. mn(1) == mm(3) ) CYCLE
                    pp1(:) = VM%Posit(:,mn(1))
                    IF(pMin(1)>pp1(1)) CYCLE
                    IF(pMin(2)>pp1(2)) CYCLE
                    IF(pMin(3)>pp1(3)) CYCLE
                    IF(pMax(1)<pp1(1)) CYCLE
                    IF(pMax(2)<pp1(2)) CYCLE
                    IF(pMax(3)<pp1(3)) CYCLE
                    dxm = Plane3D_DistancePointToTriangle (pp1, cf1,cf2,cf3)
                    IF(dxm < hlayhf) THEN
                       IF(lc==1)  nrej(1) = nrej(1) + 1
                       CYCLE Loop_iTry
                    ENDIF
                 ENDDO

                 !--- check if new triangles intersecting with edges
                 good = .TRUE.
                 Loop_60 : DO k = 1 , nfacl
                    itr = itfr(k)
                    IF(itr==0) CYCLE
                    mn(1:3) = kfrnt(1:3,itr)

                    pp1(:) = VM%Posit(:,mn(1))
                    pp2(:) = VM%Posit(:,mn(2))
                    pp3(:) = VM%Posit(:,mn(3))

                    IF(xel1(1)>MAX(pp1(1),pp2(1),pp3(1))) CYCLE Loop_60
                    IF(xel1(2)>MAX(pp1(2),pp2(2),pp3(2))) CYCLE Loop_60
                    IF(xel1(3)>MAX(pp1(3),pp2(3),pp3(3))) CYCLE Loop_60
                    IF(xel2(1)<MIN(pp1(1),pp2(1),pp3(1))) CYCLE Loop_60
                    IF(xel2(2)<MIN(pp1(2),pp2(2),pp3(2))) CYCLE Loop_60
                    IF(xel2(3)<MIN(pp1(3),pp2(3),pp3(3))) CYCLE Loop_60

                    IF(  mn(1) /= mm(1) .AND. mn(1) /= mm(2) .AND. mn(1) /= mm(3) .AND.    &
                         mn(2) /= mm(1) .AND. mn(2) /= mm(2) .AND. mn(2) /= mm(3) ) THEN
                       CALL inters(cf1,cf2,cf3, pp1,pp2, good)
                       IF(.NOT.good) EXIT Loop_60
                    ENDIF
                    IF(  mn(2) /= mm(1) .AND. mn(2) /= mm(2) .AND. mn(2) /= mm(3) .AND.    &
                         mn(3) /= mm(1) .AND. mn(3) /= mm(2) .AND. mn(3) /= mm(3) ) THEN
                       CALL inters(cf1,cf2,cf3, pp2,pp3, good)
                       IF(.NOT.good) EXIT Loop_60
                    ENDIF
                    IF(  mn(3) /= mm(1) .AND. mn(3) /= mm(2) .AND. mn(3) /= mm(3) .AND.    &
                         mn(1) /= mm(1) .AND. mn(1) /= mm(2) .AND. mn(1) /= mm(3) ) THEN
                       CALL inters(cf1,cf2,cf3, pp3,pp1, good)
                       IF(.NOT.good) EXIT Loop_60
                    ENDIF
                 ENDDO Loop_60


                 IF(.NOT.good)THEN
                    IF(lc==1)  nrej(1) = nrej(1) + 1
                    CYCLE Loop_iTry
                 ENDIF

              ENDDO Loop_40


              EXIT Loop_iTry

           ENDDO Loop_iTry

           IF(iTry>nTry)THEN
              IF(lc==2)THEN
                 IF(Debug_Display>2 .AND. ierr10>0)THEN
                    WRITE(29,*)' Warning--- fail to develop layer at '
                    WRITE(29,*)' ip,ipOrig,lboun,onRidge=',   &
                         ip,ipOrig(ip),lboun(ip),SurfNode(IP0)%onRidge
                    ierr10 = ierr10 -1
                 ENDIF
                 nrej(10)  = nrej(10) + 1
                 lorde(jj) = 0
              ENDIF
              CYCLE Loop_IP
           ENDIF

           ! *** Ok, all are fine now.
           ! *** store new point

           IF(VM%NB_Point+1 == jnewp) THEN
              VM%NB_Point = VM%NB_Point + 1
              VM%Posit(:,jnewp) = pb(:)              
              lboun(jnewp)      = lboun(IP)
              layer(jnewp)      = il
              lorde(jj)         = jnewp
              ipOrig(jnewp)     = ipOrig(IP)
              NextLay(IP)       = jnewp
              NextLay(jnewp)    = jnewp
           ELSE
              lorde(jj)         = 0
           ENDIF

           ! *** form the elements and triangles

           IF(List_Length > KeyLength(kfrntSave))THEN
              DEALLOCATE(kfrntSave)
              ALLOCATE(kfrntSave(3,List_Length+1000))
           ENDIF
           DO i = 1,List_Length
              ie = Its_List(i)
              kfrntSave(1:3, i) = kfrnt(1:3,ie)
           ENDDO

           Loop_70 : DO i = 1,List_Length
              IF(kfrntSave(1,i)==0) CYCLE Loop_70
              iw = which_NodeinTri(IP, kfrntSave(1:3,i))
              IF(iw==0) CALL Error_Stop( 'iw==0')
              np = kfrntSave(nn(iw  ),i)
              np1= kfrntSave(nn(iw+1),i)
              IF(np==jnewp .OR. np1==jnewp) CYCLE Loop_70

              VM%NB_Tet = VM%NB_Tet + 1
              IF(VM%NB_Tet > keyLength(VM%IP_Tet)) THEN
                 CALL allc_2Dpointer(VM%IP_Tet,4,VM%NB_Tet+100000,' VM%IP_Tet in VisLayer')
              ENDIF
              VM%IP_Tet(1:4,VM%NB_Tet) = (/ IP, np, np1, jnewp /)

              ! *** update the tree and association

              DO kk = 4,1,-1
                 IF(lorde(jj)>0 .AND. lboun(IP)<0)THEN
                    !--- new non-symmetry node
                    IF(kk==3 .OR. kk==2) CYCLE
                 ENDIF
                 mn(1:3) = VM%IP_Tet(iTri_Tet(1:3,kk),VM%NB_Tet)
                 CALL NodeNetTree_SearchAddRemove (3, mn, TriTree, IX)
                 IF(IX>0)THEN
                    IF(IX>mxbou) CALL Error_Stop( 'IX>mxbou')
                    IF(kk==4)THEN
                       WRITE(29,*)'Error---- kk==4 for new'
                       WRITE(29,*)' IP, jnewp=',IP,jnewp, ' jj=',lorde(jj)
                       WRITE(29,*)' mn3=',mn(1:3),' IX=',IX
                       CALL Error_Stop( 'VisLayer')
                    ENDIF
                    pMin(:) = MIN(VM%Posit(:,mn(1)),VM%Posit(:,mn(2)),VM%Posit(:,mn(3)))
                    pMax(:) = MAX(VM%Posit(:,mn(1)),VM%Posit(:,mn(2)),VM%Posit(:,mn(3)))
                    CALL BoxADTree_AddNode(BoxADTree, IX, pMin, pMax)
                    kfrnt(1:4,IX) = (/mn(1), mn(3), mn(2), VM%NB_Tet/)
                    IF(lboun(mn(1))>=0 .AND. lboun(mn(2))>=0 .AND. lboun(mn(3))>=0)THEN
                       !--- a triangle on the sysmetry plane
                       kfrnt(5,IX) = 0
                    ELSE       
                       CALL LinkAssociation_AddCell (3, IX, kfrnt(1:3,IX), ITR_Pt)
                       kfrnt(5,IX) = isf
                    ENDIF
                    a(:) = Geo3D_Cross_Product(VM%Posit(:,mn(1)),VM%Posit(:,mn(2)),VM%Posit(:,mn(3)))
                    aa   = Geo3D_Distance(a)
                    SurfTri(IX)%anor(:) = a(:) / aa
                    SurfTri(IX)%Area    = aa / 2.d0
                 ELSE
                    IX = -IX
                    IF(kk==4) isf = kfrnt(5,IX)
                    CALL BoxADTree_RemoveNode(BoxADTree, IX)
                    CALL LinkAssociation_Remove (3, IX, kfrnt(1:3,IX), ITR_Pt)
                    kfrnt(1:3,IX) = 0
                 ENDIF
              ENDDO

           ENDDO Loop_70

        ENDDO Loop_IP

     ENDDO

     WRITE(29,*)'    '
     WRITE(29,*)'....... layer, VM%NB_Tet, NB_Point: '
     WRITE(29,'(12X,I2,2X,I9,1X,I9)') il, VM%NB_Tet, VM%NB_Point
     WRITE(29,*)'.......  nrej='
     WRITE(29,'(3X, 9(1X,I7))') nrej(1:5)
     WRITE(29,'(3X, 9(1X,I7))') nrej(6:10)
     WRITE(29,*)'    '
     WRITE(*, *)'    '
     WRITE(*, *)'....... layer, VM%NB_Tet, NB_Point: ', il, VM%NB_Tet, VM%NB_Point

     NP_lay = VM%NB_Point
     IF(HighOrder_Model>1)THEN
        CALL VisLayer_ElasticityMesh(lboun, layer, ipOrig, il)
     ENDIF


  ENDDO Loop_20

  NB = TriTree%numNets

  DEALLOCATE( SurfTri )
  DEALLOCATE( SurfNode )
  DEALLOCATE(snorm, lorde, markp)
  CALL NodeNetTree_Clear (TriTree)
  CALL BoxADTree_Clear (BoxADTree)
  CALL LinkAssociation_Clear(ITR_Pt)

  !--- deal with symmetry plane
  WRITE(*, *) '--- deal with symmetry plane'
  WRITE(29,*) '--- deal with symmetry plane'
  CALL geninv(NB, kfrnt, NextLay, ipOrig)

  !--- resort surface triangulation
  WRITE(*, *) '--- resort surface triangulation'
  WRITE(29,*) '--- resort surface triangulation'
  CALL resortface(NB, kfrnt, NP_lay, layer, NextLay)

  IF(Layer_Hybrid>=1)THEN  
     !--- compose prisms &  hexahedra
     WRITE(*, *) '--- compose prisms &  hexahedra '
     WRITE(29,*) '--- compose prisms &  hexahedra '
     CALL form_prism(layer, NextLay)
  ENDIF

  DEALLOCATE(kfrnt, lboun, ipOrig)
  DEALLOCATE(layer, NextLay)

  CALL HybridMeshStorage_Output  (JobName(1:JobNameLength)//'_v1',JobNameLength+3,-15,VM,SurfOld)
  CALL SurfaceMeshStorage_Output (JobName(1:JobNameLength)//'_v2',JobNameLength+3,      Surf)
  OPEN(1,file=JobName(1:JobNameLength)//'_vis.map',  form='UNFORMATTED')
  WRITE(1,err=113)Mark_Vis_Point(1:VM%NB_Point)
  CLOSE(1)
  IF(Frame_Stitch==0)THEN
     CALL HybridMeshStorage_Clear(VM)
     CALL SurfaceMeshStorage_Clear(SurfOld)
     DEALLOCATE(Mark_Vis_Point)
  ENDIF

  Surf%NB_Point  = Surf%NB_Point - Surf%NB_Psp
  Surf%GridOrder = 1


  RETURN
112 WRITE(29,*) 'Error in reading file ',JobName(1:JobNameLength)//'_vis.map'
  CALL Error_Stop (' VisLayer ')
113 WRITE(29,*) 'Error in writing file ',JobName(1:JobNameLength)//'_vis.map'
  CALL Error_Stop (' VisLayer ')

END SUBROUTINE VisLayer

! .................................................................
! *** finds intersection between face and side
!..................................................................

SUBROUTINE inters(cf1,cf2,cf3,ck1,ck2,good)
  USE Plane3DGeom
  IMPLICIT NONE
  REAL*8,  INTENT(IN)  :: cf1(3),cf2(3),cf3(3),ck1(3),ck2(3)
  LOGICAL, INTENT(OUT) :: good
  REAL*8  :: pp2(3,2), pp3(3,3), weight(3), p(3), t
  INTEGER :: ntype

  pp2(:,1) = ck1
  pp2(:,2) = ck2
  pp3(:,1) = cf1
  pp3(:,2) = cf2
  pp3(:,3) = cf3

  ntype = 1
  P = Plane3D_Line_Tri_Cross (pp2, pp3, ntype, weight, t)

  IF(ntype<=0)THEN
     !--- no intersection     
     good = .TRUE.
  ELSE
     !--- with intersection
     good = .FALSE.
  ENDIF

  RETURN
END SUBROUTINE inters

! .................................................................
!   This routine smooth the normal at the solid wall
!..................................................................

SUBROUTINE smnrm( omega,  lboun, snorm, layer)
  USE common_Parameters
  IMPLICIT NONE
  REAL*8,  INTENT(in)    :: omega
  INTEGER, INTENT(in)    :: lboun(*)
  REAL*8,  INTENT(out)   :: snorm(3,*) 
  INTEGER, INTENT(inout) :: layer( * )
  INTEGER :: ip, iLoop, iw1, iw2, ic, ie, ip1, ip2
  REAL*8, DIMENSION(:,:), POINTER :: helpa
  REAL*8 :: al, pp(3)

  ALLOCATE(helpa(3,Surf%NB_Point))

  DO ip = 1 , Surf%NB_Point
     snorm(:,ip) = SurfNode(ip)%Anor(:,1)
  ENDDO

  DO iLoop = 1 , 20

     DO ip = 1,Surf%NB_Point
        helpa(:,ip) = snorm(:,ip) 
     ENDDO

     DO ip=1,Surf%NB_Point
        IF(lboun(ip) <=-2) CYCLE
        IF(lboun(ip) == 0) CYCLE
        pp(:) = 0
        CALL LinkAssociation_List (IP, List_Length, Its_List, Surf%ITR_Pt)
        DO ic = 1,List_Length
           ie = Its_List(ic)
           IF(ip == Surf%IP_Tri(1,ie))THEN
              ip1 = Surf%IP_Tri(2,ie)
              ip2 = Surf%IP_Tri(3,ie)
           ELSE IF(ip == Surf%IP_Tri(2,ie))THEN
              ip1 = Surf%IP_Tri(3,ie)
              ip2 = Surf%IP_Tri(1,ie)
           ELSE IF(ip == Surf%IP_Tri(3,ie))THEN
              ip1 = Surf%IP_Tri(1,ie)
              ip2 = Surf%IP_Tri(2,ie)
           ENDIF
           iw1 = 0
           iw2 = 0
           IF(lboun(ip) == -1) THEN
              iw1 = 1
              iw2 = 1
           ELSE IF(lboun(ip) >0) THEN
              IF(lboun(ip1) >= 0) iw1 = 1
              IF(lboun(ip2) >= 0) iw2 = 1
           ELSE
              PRINT*, 'FLITE > error should not be here'
           ENDIF
           pp(:) = pp(:) + iw1*helpa(:,ip1) + iw2*helpa(:,ip2)

        ENDDO

        al  =  Geo3D_Distance(pp(:))
        IF(al == 0) THEN
           snorm(:,ip) = helpa(:,ip)
        ELSE
           pp(:) = pp(:)/al    
           snorm(:,ip) = (1-omega)*helpa(:,ip) + omega*pp(:) 
        ENDIF

        al  =  Geo3D_Distance(snorm(:,ip))
        IF(al == 0) THEN
           PRINT *, ' al = 0  in smnrm at node: ', ip, layer(ip)
           layer(ip) = layer(ip) - 1
        ELSE
           snorm (:,ip) = snorm (:,ip) / al
        ENDIF
     ENDDO

  ENDDO

  DEALLOCATE(helpa)

  RETURN
END SUBROUTINE smnrm

!..................................................................
!..................................................................

SUBROUTINE setdt( lboun )   
  USE common_Parameters
  USE OptimisingModule
  IMPLICIT NONE

  INTEGER :: norm_type
  INTEGER,  DIMENSION(*), INTENT(OUT) :: lboun
  INTEGER :: ib, ip, jf, i, i1,i2,i3, j, j2, ic, ntrail, is, ip1, ia, ja
  REAL*8  :: dxm, dotpr, a(3), aa
  REAL*8 , PARAMETER  :: PI = 3.141592653589793D0
  REAL*8  :: p2D1(2), p2D2(2)
  REAL*8, DIMENSION(:,:), POINTER :: Pts
  INTEGER,DIMENSION(:,:), POINTER :: ips
  TYPE(Plane3D) :: aPlane

  !--- count the symmetry planes
  NB_SymPlane = 0
  DO jf = 1,Surf%NB_Surf
     IF(Type_Wall(jf)==2)THEN
        NB_SymPlane = NB_SymPlane + 1
        IF(NB_SymPlane>100) CALL Error_Stop('setdt:: too many symmetry planes')
        IS_SymPlane(NB_SymPlane) = jf
     ENDIF
  ENDDO

  DO ib = 1 , Surf%NB_Tri
     IF(Type_Wall(Surf%IP_Tri(5,ib))==2)THEN
        Surf%IP_Tri(5,ib) = -Surf%IP_Tri(5,ib) 
     ENDIF
  ENDDO

  ! *** lboun(*) priority is to symmetry
  ! *** lboun() =  0 : symmetry x symmetry x wall
  ! *** lboun() =  i : symmetry(i) x wall
  ! *** lboun() = -1 : wall
  ! *** lboun() = -2 : else
  ! *** lboun() = -3 : symmetry x symmetry (temporarily)

  lboun(1:Surf%NB_Point) = -2

  DO ib = 1 , Surf%NB_Tri
     jf = Surf%IP_Tri(5,ib)
     IF(jf<0) CYCLE
     IF(Type_Wall(jf)==1)THEN
        lboun(Surf%IP_Tri(1:3,ib)) = -1
     ENDIF
  ENDDO

  DO ib = 1 , Surf%NB_Tri
     jf = Surf%IP_Tri(5,ib)
     IF(jf<0)THEN
        DO i = 1,3
           ip = Surf%IP_Tri(i,ib)
           IF(SurfNode(IP)%PartTri(1)%numNodes>0)THEN
              IF(SurfNode(IP)%PartTri(1)%back==-jf) CYCLE
           ENDIF
           !--- use PartTri to record the plane ID
           CALL IntQueue_Push(SurfNode(IP)%PartTri(1), -jf)

           IF(lboun(ip)==-1)THEN
              lboun(ip) = ABS(jf)
           ELSE IF(lboun(ip)>0 .AND. lboun(ip)/=ABS(jf))THEN
              lboun(ip) = 0
           ELSE IF(lboun(ip)==-2 .AND. SurfNode(IP)%PartTri(1)%Nodes(1)/=-jf)THEN
              lboun(ip) = -3
           ENDIF
        ENDDO
     ENDIF
  ENDDO

  DO is = 1, NB_SymPlane
     jf = IS_SymPlane(is)
     IF(jf==is) CYCLE
     DO ip = 1, Surf%NB_Point
        IF(lboun(ip)==is) CALL Error_Stop (' lboun confussed')
        IF(lboun(ip)==jf) lboun(ip) = is
     ENDDO
  ENDDO

  !--- correct positions of the nodes on the symmetry x symmetry line

  ia = 0
  symLine%region1 = 0
  symLine%region2 = 0
  DO ip = 1 , Surf%NB_Point
     IF(lboun(IP)==-3 .OR. lboun(IP)==0)THEN
        ia = ia+1
        IF(ia==1)THEN
           symLine%region1 = SurfNode(IP)%PartTri(1)%Nodes(1)
           symLine%region2 = SurfNode(IP)%PartTri(1)%Nodes(2)
        ELSE
           IF(  symLine%region1/=SurfNode(IP)%PartTri(1)%Nodes(1) .OR.   &
                symLine%region2/=SurfNode(IP)%PartTri(1)%Nodes(2) ) THEN
              CALL Error_Stop (' Not allow two segments')
           ENDIF
        ENDIF
     ENDIF
  ENDDO

  IF(ia>0)THEN
     !--- fit a straight line
     ALLOCATE(Pts(3,ia))
     ALLOCATE(ips(2,ia))
     ia = 0
     DO ip = 1 , Surf%NB_Point
        IF(lboun(IP)==-3 .OR. lboun(IP)==0)THEN
           ia = ia+1
           Pts(:,ia) = VM%Posit(:,IP)
        ENDIF
     ENDDO

     CALL Geo3D_LineFit(ia,Pts,symLine%Dir,symLine%P0)
     DEALLOCATE(Pts)

     DO ip = 1 , Surf%NB_Point
        IF(lboun(IP)==-3 .OR. lboun(IP)==0)THEN
           a(:) = VM%Posit(:,IP) - symLine%P0
           aa   = Geo3D_Dot_Product(a,symLine%Dir)
           VM%Posit(:,IP) = symLine%P0 + aa*symLine%Dir
        ENDIF
     ENDDO

     !--- search the edges on the line
     ja = 0
     DO  ib = 1 , Surf%NB_Tri
        IF(Surf%IP_Tri(5,ib)/=-symLine%region1) CYCLE
        DO i = 1,3
           ip  = Surf%IP_Tri(i,ib)
           ip1 = Surf%IP_Tri(MOD(i,3)+1,ib)
           IF(  (lboun(IP)==-3  .OR. lboun(IP)==0) .AND.    &
                (lboun(IP1)==-3 .OR. lboun(IP1)==0) ) THEN
              ja = ja+1
              ips(:,ja) = (/ip, ip1/)
           ENDIF
        ENDDO
     ENDDO
     IF(ja>=ia) CALL Error_Stop('ja>=ia')

     !--- pick chains by two ends
     i = 1
     DO WHILE(i<ja)
        j  = i+1
        ia = ja
        DO WHILE(j<=ja)
           IF(ips(2,i)==ips(1,j))THEN
              ips(2,i) = ips(2,j)
              ips(:,j) = ips(:,ja)
              ja = ja -1
           ELSE IF(ips(1,i)==ips(2,j))THEN
              ips(1,i) = ips(1,j)
              ips(:,j) = ips(:,ja)
              ja = ja -1
           ELSE
              j = j+1
           ENDIF
        ENDDO
        IF(ia==ja) i = i+1       !--- start another chain
     ENDDO
     symLine%numPieces = ja
     IF(ja>10) CALL Error_Stop('ja>10')
     symLine%IPends(:,1:ja) = ips(:,1:ja)

     a(:) = VM%Posit(:,symLine%IPends(2,1)) - VM%Posit(:,symLine%IPends(1,1))
     aa   = Geo3D_Dot_Product(a,symLine%Dir)
     IF(aa<0) symLine%Dir = - symLine%Dir
     DEALLOCATE(ips)

     WHERE(lboun(1:Surf%NB_Point)==-3) lboun(1:Surf%NB_Point) = -2

     WRITE(29,*)' Symmetry Surface ',symLine%region1,' and', symLine%region2, ' have a cross:'
     WRITE(29,*)'    Dir: ',  REAL(symLine%Dir)
     WRITE(29,*)'    P  : ',  REAL(symLine%P0)
  ENDIF


  !--- correct positions of the nodes on the symmetry planes
  DO is = 1, NB_SymPlane
     jf = IS_SymPlane(is)

     !---  build surface plane
     a(:) = 0
     aa   = 0
     ia   = 0
     IF(jf==symLine%region1 .OR. jf==symLine%region2)THEN
        DO ib = 1 , Surf%NB_Tri
           IF(Surf%IP_Tri(5,ib)/=-jf) CYCLE
           i1 = Surf%IP_Tri(1,ib)
           i2 = Surf%IP_Tri(2,ib)
           i3 = Surf%IP_Tri(3,ib)
           a(:) = a(:) + VM%Posit(:,i1)+VM%Posit(:,i2)+VM%Posit(:,i3)
           ia   = ia   + 3
        ENDDO
        a(:) = a(:)/ia - symLine%P0(:)
        a(:) = Geo3D_Cross_Product(symLine%Dir, a)
        IF(jf==symLine%region2) a(:) = -a(:)
        SymPlane(is)%anor(:) = a(:) / Geo3D_Distance(a)
        SymPlane(is)%d       = - Geo3D_Dot_Product(SymPlane(is)%anor(:),symLine%P0)
     ELSE
        DO ib = 1 , Surf%NB_Tri
           IF(Surf%IP_Tri(5,ib)/=-jf) CYCLE
           i1 = Surf%IP_Tri(1,ib)
           i2 = Surf%IP_Tri(2,ib)
           i3 = Surf%IP_Tri(3,ib)
           aPlane = Plane3D_Build (VM%Posit(:,i1), VM%Posit(:,i2), VM%Posit(:,i3))   
           a(:) = a(:) + aPlane%anor(:)
           aa   = aa   + aPlane%d
           ia   = ia   + 1
        ENDDO
        SymPlane(is)%d = aa / ia
        aa = Geo3D_Distance(a)
        SymPlane(is)%anor(:) = a(:) / aa
     ENDIF
     CALL Plane3D_buildTangentAxes(SymPlane(is)) 

     WRITE(29,*)' Surface ',jf,' is a symmtry plane:'
     WRITE(29,*)'  ',  REAL(SymPlane(is)%anor),REAL(SymPlane(is)%d)

     DO ip = 1 , Surf%NB_Point
        IF(SurfNode(IP)%PartTri(1)%numNodes==0)  CYCLE
        IF(SurfNode(IP)%PartTri(1)%Nodes(1)/=jf) CYCLE
        IF(lboun(IP)==0) CYCLE
        VM%Posit(:,IP)   = Plane3D_ProjectOnPlane(VM%Posit(:,IP),SymPlane(is))
        Surf%Posit(:,IP) = VM%Posit(:,IP)
     ENDDO
  ENDDO


  !--- calculate normal

  DO IP = 1,Surf%NB_Point
     SurfNode(IP)%anor(:,1) = 0
  ENDDO

  DO  ib = 1, Surf%NB_Tri
     jf = Surf%IP_Tri(5,ib)
     IF(jf<0) CYCLE
     IF(Type_Wall(jf)==1)THEN
        CALL SurfTri_CalGeom(Surf, SurfTri, ib)
        a(:) = SurfTri(ib)%anor(:) / SurfTri(ib)%Area
        DO i = 1,3
           IP = Surf%IP_Tri(i,ib)
           SurfNode(IP)%anor(:,1) = SurfNode(IP)%anor(:,1) + a(:)
        ENDDO
     ENDIF
  ENDDO

  !--- SurfNode(IP)%anor(:,1)  : the direction to develop layers
  !--- SurfNode(IP)%anor(:,2)  : the real normal direction
  DO IP = 1,Surf%NB_Point
     SurfNode(IP)%anor(:,2) = SurfNode(IP)%anor(:,1)
     SurfNode(IP)%numParts  = 0
  ENDDO

  !---- for those node on the cross of double symmetry planes and a wall plane,
  !     set the normal along the cross line
  DO ip = 1 , Surf%NB_Point
     IF(lboun(IP)==0)THEN
        aa   = Geo3D_Dot_Product(SurfNode(IP)%anor(:,1),symLine%Dir)
        IF(aa>0)THEN
           SurfNode(IP)%anor(:,1) = symLine%Dir
        ELSE
           SurfNode(IP)%anor(:,1) = - symLine%Dir
        ENDIF
     ENDIF
  ENDDO

  !---- for those node on the cross of a symmetry plane and a wall plane,
  !     set the normal along the symmetry plane

  DO is = 1, NB_SymPlane
     jf = IS_SymPlane(is)

     DO ib = 1 , Surf%NB_Tri
        IF(Surf%IP_Tri(5,ib)/=-jf) CYCLE

        DO i = 1,3
           ip = Surf%IP_Tri(i,ib)
           IF(lboun(IP)<=0) CYCLE
           IF(SurfNode(IP)%numParts==jf) CYCLE

           a(:) = VM%Posit(:,ip) + SurfNode(IP)%anor(:,1)
           a(:) = Plane3D_ProjectOnPlane(a,SymPlane(is))
           SurfNode(IP)%anor(:,1) = a(:) - VM%Posit(:,ip)
           SurfNode(IP)%numParts = jf           
        ENDDO
     ENDDO
  ENDDO

  !--- unify normal

  DO IP = 1,Surf%NB_Point
     IF(lboun(IP)<=-2) CYCLE
     a(:) = SurfNode(IP)%anor(:,1)
     SurfNode(IP)%anor(:,1) = a(:) / Geo3D_Distance(a)
     a(:) = SurfNode(IP)%anor(:,2)
     SurfNode(IP)%anor(:,2) = a(:) / Geo3D_Distance(a)
  ENDDO

  ! *** Account for trailing edges

  DO IP = 1, Surf%NB_Point
     SurfNode(IP)%onRidge = 0
  ENDDO

  norm_type = 2
  IF(norm_type==2)THEN
     dxm = COS(PI*65.d0/180.0)

     DO IP = 1, Surf%NB_Point
        IF(lboun(IP)<=-2) CYCLE

        CALL LinkAssociation_List (IP, List_Length, Its_List, Surf%ITR_Pt)
        Loop_1 : DO i = 1, List_Length
           ib = Its_List(i)
           IF(Surf%IP_Tri(5,ib)<0) CYCLE
           j   = which_NodeinTri_Next(ip, Surf%IP_Tri(1:3,ib))
           ip1 = Surf%IP_Tri(j,ib)
           DO j = 1, List_Length
              ic = Its_List(j)
              IF(ic==ib) CYCLE
              IF(Surf%IP_Tri(5,ic)<0) CYCLE
              IF(which_NodeinTri(ip1, Surf%IP_Tri(1:3,ic))==0) CYCLE
              dotpr = Geo3D_Dot_Product(SurfTri(ib)%anor, SurfTri(ic)%anor)
              IF(dotpr>dxm) CYCLE
              a(:)  = Geo3D_Cross_Product(SurfTri(ib)%anor, SurfTri(ic)%anor)
              dotpr = Geo3D_Dot_Product(a, VM%Posit(:,ip1)-VM%Posit(:,ip))
              IF(dotpr>0)THEN
                 !--- peak trailing edge
                 SurfNode(IP)%onRidge = 1
              ELSE
                 !--- valley edge
                 SurfNode(IP)%onRidge = -1
              ENDIF
              EXIT Loop_1
           ENDDO
        ENDDO Loop_1
     ENDDO

  ENDIF

  !--- adjust the normal for trailing nodes
  !--- set Optimising parameters
  OptVariable%IdxFun  = 4
  OptParameter%Idistb = 100
  CALL Optimising_Set(2, 200, 1.d-3*Layer_Heights(1))

  DO IP = 1, Surf%NB_Point
     IF(lboun(IP)/=-1) CYCLE
     IF(SurfNode(IP)%onRidge==0) CYCLE

     OptPlane%anor(:) = SurfNode(IP)%anor(:,1)
     a(:) = VM%Posit(:,IP) + OptPlane%anor(:)*Layer_Heights(1)
     OptPlane%d       = - Geo3D_Dot_Product(a, OptPlane%anor(:))
     CALL Plane3D_buildTangentAxes (OptPlane)
     p2D1 = Plane3D_project2D (OptPlane, a)
     OptVariable%IdNode  =  IP
     CALL LinkAssociation_List (IP, List_Length, Its_List, Surf%ITR_Pt)

     CALL Optimising_Powell(p2D1, p2D2)
     IF(OptParameter%Isucc>0)THEN
        a(:) = Plane3D_get3DPosition (OptPlane, P2D2)
        a(:) = a(:) - VM%Posit(:,IP)
        SurfNode(IP)%anor(:,1) = a(:) / Geo3D_Distance(a)
     ENDIF
  ENDDO


  RETURN
END SUBROUTINE setdt


! .................................................................
!   This routine regenerate the symmetry plane if required and 
!   complete the generation of the inviscid region
!..................................................................

SUBROUTINE geninv(NB_kfrnt, kfrnt, NextLay, ipOrig )  
  USE common_Parameters
  USE SurfaceMeshManager2D
  USE SurfaceMeshManager
  USE Geometry3DAll
  IMPLICIT NONE

  INTEGER, INTENT(IN)    ::  NB_kfrnt, NextLay(*), ipOrig(*)
  INTEGER, INTENT(INOUT) ::  kfrnt(5,*)
  INTEGER, DIMENSION(:,:), POINTER :: ibout
  INTEGER, DIMENSION(:  ), POINTER :: newn, nold, ipc, inext, markp
  INTEGER :: is, isf, ic, NB_Tri_Bak, NPF
  INTEGER :: ib, nb, i1, i2, i3, k, kk, nb1, id1, id2, i, j, np, nnp, ip, ip1, ip2, ip3
  REAL*8  :: po(3), co(2)
  REAL*8  :: tor, tl, fmap(3,3), fmetric(3), scalar
  REAL*8  :: RAsq, RAfac, Rstd, Rstd1, RA, pr0(3), pr1(3), p0(3), p1(3), pr(3)
  TYPE(SurfaceMeshStorageType) :: SurfSym
  TYPE(SpacingStorage2DType)   :: Spacing2D

  IF(NB_SymPlane==0) RETURN

  tl  = 1.d-04
  tor = 1.d-03

  IF(symLine%numPieces>0)THEN
     !--- generate nodes on symmetry x symmetry line
     Rafac = (1.5d0 * BGSpacing%BasicSize) **2
     Rstd  = BGSpacing%BasicSize **2
     np    = VM%NB_Point
     DO ic = 1, symLine%numPieces
        !--- split the line according the computational length

        DO i = 1,2
           ip  = NextLay(symLine%IPends(i,ic))
           DO WHILE(ip/=NextLay(ip))
              ip = NextLay(ip)
           ENDDO
           symLine%IPends(i,ic) = ip
        ENDDO
        ip  = symLine%IPends(1,ic)
        ip1 = symLine%IPends(2,ic)
        CALL IntQueue_Push(symLine%Ips(ic), ip)
        pr0 = VM%Posit(:,ip )
        pr1 = VM%Posit(:,ip1)

        DO
           CALL GetEgdeLength_SQ(Pr0, Pr1, RAsq)
           IF(RAsq<Rafac) EXIT
           Rstd1 = MIN(Rstd, RAsq/4.d0)

           p0 = pr0
           p1 = pr1
           DO k=1,20
              pr = (p0+p1) / 2.d0
              CALL GetEgdeLength_SQ(Pr0, Pr, RA)
              IF(ABS(RA-Rstd1)<0.2*Rstd1)THEN
                 EXIT
              ELSE IF(RA>Rstd1)THEN
                 p1 = pr
              ELSE
                 p0 = pr
              ENDIF
           ENDDO

           np = np+1
           IF(np>keyLength(VM%Posit))THEN
              CALL allc_2Dpointer( VM%Posit, 3, np+100000, ' in GENINV')
           ENDIF
           VM%Posit(:,np) = pr
           CALL IntQueue_Push(symLine%Ips(ic), np)
           pr0 = pr
        ENDDO
        CALL IntQueue_Push(symLine%Ips(ic), ip1)

     ENDDO
     VM%NB_Point = np
  ENDIF

  ALLOCATE(ibout(2, Surf%NB_Tri+NB_kfrnt))
  ALLOCATE(SurfSym%Coord(2,VM%NB_Point))
  ALLOCATE(newn (VM%NB_Point))
  ALLOCATE(nold (VM%NB_Point))
  ALLOCATE(ipc  (VM%NB_Point))
  ALLOCATE(inext(VM%NB_Point))
  ALLOCATE(markp(Surf%NB_Point))

  ! *** Loop over surfaces which require regenaration (symmetry)

  NB_Tri_Bak = Surf%NB_Tri

  Loop_isf :  DO is = 1, NB_SymPlane
     isf = IS_SymPlane(is)
     PRINT*,'FLITE > surface number :', isf

     markp(:) = 0
     DO ib = 1, NB_Tri_Bak
        IF(Surf%IP_Tri(5,ib)==-isf)  markp(Surf%IP_Tri(1:3,ib)) = 1
     ENDDO
     DO ib = 1, NB_Tri_Bak
        IF(Surf%IP_Tri(5,ib)/=-isf)THEN
           WHERE(markp(Surf%IP_Tri(1:3,ib))==1) markp(Surf%IP_Tri(1:3,ib)) = 2
        ENDIF
     ENDDO

     ! *** Store in ibout the sides on the interface between the surface
     !     isf and all other surfaces

     DO ib = 1 , NB_kfrnt
        IF(kfrnt(1,ib)==0 ) CYCLE
        IF(kfrnt(5,ib)/=0)  CYCLE
        ip1 = ipOrig(kfrnt(1,ib))
        ip2 = ipOrig(kfrnt(2,ib))
        ip3 = ipOrig(kfrnt(3,ib))
        IF(markp(ip1)==2 .AND. markp(ip2)==2 .AND. markp(ip3)==2)  kfrnt(5,ib) = -isf
     ENDDO

     nb = 0
     DO ib = 1 , NB_kfrnt
        IF(kfrnt(1,ib)==0 )   CYCLE
        IF(kfrnt(5,ib)==-isf) CYCLE
        DO k = 1,3
           i1 = kfrnt(k,ib)
           i2 = kfrnt(MOD(k,3)+1,ib)              
           ip1 = ipOrig(i1)
           ip2 = ipOrig(i2)              
           IF(markp(ip1)/=2   .OR. markp(ip2)/=2  ) CYCLE
           IF(i1==NextLay(i2) .OR. i2==NextLay(i1)) CYCLE   !  avoid symmetry x symmetry line
           nb = nb + 1
           ibout(1,nb) = i2
           ibout(2,nb) = i1
        ENDDO
     ENDDO

     !--- add symmetry x symmetry line if apply
     IF(isf==symLine%Region1)THEN
        DO ic = 1, symLine%numPieces
           DO i = 1,symLine%Ips(ic)%numNodes-1
              nb = nb+1
              ibout(1,nb) = symLine%Ips(ic)%Nodes(i)
              ibout(2,nb) = symLine%Ips(ic)%Nodes(i+1)
           ENDDO
        ENDDO
     ELSEIF(isf==symLine%Region2)THEN
        DO ic = 1, symLine%numPieces
           DO i = symLine%Ips(ic)%numNodes-1, 1, -1
              nb = nb+1
              ibout(1,nb) = symLine%Ips(ic)%Nodes(i+1)
              ibout(2,nb) = symLine%Ips(ic)%Nodes(i)
           ENDDO
        ENDDO
     ENDIF

     IF(nb==0)THEN
        !--- no front edge found, keep the original symmetry surface.
        DO ib = 1 , NB_Tri_Bak
           IF(Surf%IP_Tri(5,ib)==-isf) Surf%IP_Tri(5,ib) = isf
        ENDDO
        CYCLE Loop_isf
     ENDIF

     ! *** Make sure that the sides form a closed domain(s). 

     DO i = 1, nb
        DO j = i+1, nb
           IF((ibout(1,i)==ibout(2,j)) .AND. (ibout(2,i)==ibout(1,j))) THEN
              !--- two indentical front edges found, remove them.
              WRITE(29,*)'Error--- front nodes counting for symmetry plane: ',isf
              WRITE(29,*)' side',i,ibout(:,i),' cover size',j,ibout(:,j)
              CALL Error_Stop('geninv:: 1 ')        
           ENDIF
        ENDDO
     ENDDO

     !  Renumber the points in ibout and map the plane onto an XY plane

     nnp = 0
     nb1 = nb
     nb  = 0
     newn(:) = 0
     nold(:) = 0
     DO  j = 1,nb1
        IF(ibout(1,j)==0) CYCLE
        DO k = 1,2
           ip = ibout(k,j)
           IF(newn(ip)==0)THEN
              nnp = nnp + 1                 
              newn(ip)  = nnp
              nold(nnp) = ip
              SurfSym%Coord(:,nnp) = Plane3D_project2D(SymPlane(is), VM%Posit(:,ip))                 
           ENDIF
        ENDDO

        nb = nb + 1
        ibout(1:2,nb) = newn(ibout(1:2,j))
     ENDDO

     IF(nb/=nnp)THEN
        WRITE(29,*)'Error--- front nodes counting for symmetry plane: ',isf
        WRITE(29,*)'  nb, nnp=',nb, nnp
        CALL Error_Stop('geninv:: 2')
     ENDIF
     SurfSym%NB_Point = nnp

     !--- suppose here only simple connection :: corner number is the node number
     inext(1:nb) = 0
     DO j = 1, nb
        ipc(j)   = j
        DO i = 1, nb
           IF(ibout(1,i)==j)THEN
              inext(j)   = ibout(2,i)
              EXIT
           ENDIF
        ENDDO
        IF(inext(j)==0) CALL Error_Stop('geninv:: inext(j)==0')
     ENDDO

     IF(Debug_Display>2)THEN
        WRITE(78,*)' '
        WRITE(78,*)' '
        WRITE(78,*)'#multi-loop:: isf=', isf, ' nb=', nb
        DO i = 1, nb
           WRITE(78,*)REAL(SurfSym%Coord(:,ipc(i))),ipc(i)
           WRITE(78,*)REAL(SurfSym%Coord(:,ipc(inext(i)))),ipc(inext(i))
           WRITE(78,*)' '
           WRITE(78,*)' '
        ENDDO
     ENDIF


     !---- : extract 2D background

     Spacing2D%Model     = BGSpacing%Model
     Spacing2D%BasicSize = BGSpacing%BasicSize
     Spacing2D%MinSize   = BGSpacing%MinSize
     IF(ABS(BGSpacing%Model)>=2)THEN
        CALL SurfaceMeshStorage_BasicCopy (Surf, Spacing2D%TriMesh)
        IF(Surf%GridOrder>1)THEN
           Spacing2D%TriMesh%GridOrder = 1
           DEALLOCATE(Spacing2D%TriMesh%IPsp_Tri)
        ENDIF
        DO ib = 1, Spacing2D%TriMesh%NB_Tri
           IF(Spacing2D%TriMesh%IP_Tri(5,ib)/=-isf)THEN
              Spacing2D%TriMesh%IP_Tri(1,ib) = 0
           ENDIF
        ENDDO
        CALL Surf_RemoveFake (Spacing2D%TriMesh, markp)
        CALL Surf_BuildNext(Spacing2D%TriMesh)

        np = Spacing2D%TriMesh%NB_Point
        ALLOCATE(Spacing2D%TriMesh%Coord(2, np))
        IF(BGSpacing%Model>0)THEN
           Spacing2D%Model = 2
           ALLOCATE(Spacing2D%Scalar_Point(np))
        ELSE
           Spacing2D%Model = -2
           ALLOCATE(Spacing2D%Metric_Point(3,np))
        ENDIF

        DO ip = 1, Spacing2D%TriMesh%NB_Point
           po = Spacing2D%TriMesh%Posit(:,ip)
           co = Plane3D_project2D(SymPlane(is),po)
           Spacing2D%TriMesh%Coord(:,ip) = co
           IF(BGSpacing%Model>0)THEN
              CALL SpacingStorage_GetScale (BGSpacing, po, Scalar)
              Spacing2D%Scalar_Point(ip) = Scalar
           ELSE
              CALL SpacingStorage_GetMapping (BGSpacing, po, fMap)
              Spacing2D%Metric_Point(:,ip) = Plane3D_projectMapping(SymPlane(is),fMap)
           ENDIF
        ENDDO

     ENDIF


     ! *** Generate the 2D mesh on this plane

     PRINT *,' triangulate symmtry plane:', isf

     CALL Surf2D_BoundaryConnect(SurfSym, NB, ipc, inext)
     CALL Surf2D_DelaunaySwap2(SurfSym, Spacing2D)
     CALL Surf_BuildNext(SurfSym)
     CALL Surf2D_Refine(SurfSym, Spacing2D)

     ! *** high-order
     IF(Surf%GridOrder>1)THEN
        CALL ElasticityMesh_SymmPlane(SurfSym, isf, SymPlane(is), newn, nold)
     ENDIF
     NPF = Surf%cell%numCellNodes     


     ! *** Renumber all the newly generated points after mapping back

     np = VM%NB_Point
     DO  i = 1,SurfSym%NB_Point
        IF(nold(i)==0)THEN
           np = np + 1              
           nold(i) = np
           IF( np > keyLength(VM%Posit) ) THEN
              CALL allc_2Dpointer( VM%Posit, 3, np+100000, ' in GENINV')
           ENDIF
           VM%Posit(:,np)   = Plane3D_get3DPosition(SymPlane(is), SurfSym%Coord(:,i))
        ENDIF
     ENDDO
     VM%NB_Point = np

     ! *** Store the triangles of the generated surface

     nb = Surf%NB_Tri
     DO ib = 1, SurfSym%NB_Tri
        nb = nb + 1
        IF( nb > keyLength(Surf%IP_Tri) ) THEN
           CALL allc_2Dpointer( Surf%IP_Tri, 5, nb+100000, ' in GENINV')
        ENDIF
        Surf%IP_Tri(1:3,nb) = nold(SurfSym%IP_Tri(1:3,ib))
        Surf%IP_Tri(4:5,nb) = (/0,isf/)
        IF(Surf%GridOrder>1)THEN
           IF( nb > keyLength(Surf%IPsp_Tri) ) THEN
              CALL allc_2Dpointer( Surf%IPsp_Tri, NPF, nb+100000, ' in GENINV')
           ENDIF
           Surf%IPsp_Tri(:,nb) = nold(SurfSym%IPsp_Tri(:,ib))
        ENDIF
     ENDDO
     Surf%NB_Tri = nb

     ! *** Go to the next surface

  ENDDO Loop_isf

  PRINT *,' symmetry surface stored'     

  DEALLOCATE(ibout, newn, nold, ipc, inext, markp)
  CALL SurfaceMeshStorage_Clear(SurfSym)

  RETURN
END SUBROUTINE geninv

! .................................................................
!   This routine prepare surface for inviscid mesh
!   Output:
!     Surf    : the boundary surface for inviscid domain
!     SurfOld : the boundary surface for all domain.
!..................................................................

SUBROUTINE resortface(NB_kfrnt, kfrnt, NP_lay, layer, NextLay)  
  USE common_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: NB_kfrnt, NP_lay
  INTEGER, DIMENSION(5,*), INTENT(INOUT) :: kfrnt
  INTEGER, INTENT(INOUT) :: layer(*), NextLay(*)
  INTEGER :: ib, it, ip, np, nb, ipt(5), j, ips(20), NPF, k
  INTEGER, DIMENSION(:),   POINTER :: markp
  INTEGER, DIMENSION(:,:), POINTER :: IPsp_Tri


  !--- discard nodes and triangles on the repalced symmetry plane.
  ALLOCATE(markp(VM%NB_Point))
  markp(1:VM%NB_Point) = 0
  DO ib = 1,Surf%NB_Tri
     IF(Surf%IP_Tri(5,ib)>0)THEN
        markp(Surf%IP_Tri(1:3,ib)) = 1
        IF(Surf%GridOrder>1) markp(Surf%IPsp_Tri(:,ib)) = 1
     ENDIF
  ENDDO
  DO it = 1, VM%NB_Tet
     markp(VM%IP_Tet(:,it)) = 1
  ENDDO

  np = 0
  DO ip = 1, VM%NB_Point
     IF(markp(ip)==0) CYCLE
     np = np+1
     markp(ip) = np
     IF(ip==np) CYCLE
     VM%Posit(:,np) = VM%Posit(:,ip)
     IF(ip>NP_lay) CYCLE
     layer(np)      = layer(ip)
     NextLay(np)    = NextLay(ip)
  ENDDO

  VM%NB_Point = np
  NextLay(1:NP_lay) = markp(NextLay(1:NP_lay))

  DO it = 1, VM%NB_Tet
     VM%IP_Tet(:,it) = markp(VM%IP_Tet(:,it))
  ENDDO
  DO ib = 1, NB_kfrnt
     IF(kfrnt(1,ib)>0) THEN
        kfrnt(1:3,ib) = markp(kfrnt(1:3,ib))
     ENDIF
  ENDDO

  nb=0
  DO ib =1,Surf%NB_Tri
     IF(Surf%IP_Tri(5,ib)>0)THEN
        nb = nb+1
        Surf%IP_Tri(1:3,nb) = markp(Surf%IP_Tri(1:3,ib))
        Surf%IP_Tri(4:5,nb) =       Surf%IP_Tri(4:5,ib)
        IF(Surf%GridOrder>1) Surf%IPsp_Tri(:,nb) = markp(Surf%IPsp_Tri(:,ib))
     ENDIF
  ENDDO

  Surf%NB_Tri   = nb

  IF(HighOrder_Model>1)THEN
     !--- make a copy of IPsp_Tri (the original with old symmetry plane + new symmetry plane)
     NPF = VM%cell%numFaceNodes     
     ALLOCATE(IPsp_Tri(NPF, Surf%NB_Tri))
     IPsp_Tri(1:NPF, 1:Surf%NB_Tri) = Surf%IPsp_Tri(1:NPF, 1:Surf%NB_Tri)
  ENDIF

  write(*,*) ' I am have allocated IPsp '
  DEALLOCATE(markp)
  write(*,*) ' I am have deallocated markp '

  !--- make a copy of the original surface.

  ALLOCATE(SurfOld%IP_Tri(5,2*nb))
  np = 0
  DO ib = 1, nb
     SurfOld%IP_Tri(:,ib) = Surf%IP_Tri(:,ib)
     np = MAX(np, Surf%IP_Tri(1,ib),Surf%IP_Tri(2,ib),Surf%IP_Tri(3,ib))
  ENDDO

  DO ib = 1, NB_kfrnt
     IF(kfrnt(5,ib)<0) THEN
        !--- for triangles on symmtry plane under boundary layers
        !    need change the orientation
        nb = nb +1
        IF(nb>KeyLength(SurfOld%IP_Tri))THEN
           CALL allc_2Dpointer(SurfOld%IP_Tri, 5, nb+100000)
        ENDIF
        SurfOld%IP_Tri(1,nb) = kfrnt(1,ib)
        SurfOld%IP_Tri(2,nb) = kfrnt(3,ib)
        SurfOld%IP_Tri(3,nb) = kfrnt(2,ib)
        SurfOld%IP_Tri(4,nb) = 0
        SurfOld%IP_Tri(5,nb) = ABS(kfrnt(5,ib))
        np = MAX(np, kfrnt(1,ib), kfrnt(2,ib), kfrnt(3,ib))
     ENDIF
  ENDDO

  SurfOld%NB_Tri   = nb
  SurfOld%NB_Point = np
  SurfOld%NB_Surf  = Surf%NB_Surf

  !--- export new surface

  ALLOCATE(Mark_Vis_Point(VM%NB_Point))
  Mark_Vis_Point(:) = 0

  nb = 0
  np = 0
  DO ib = 1, Surf%NB_Tri + NB_kfrnt
     IF(ib<=Surf%NB_Tri)THEN
        !--- get those new triangles on the symmtry planes generated at subroutine geninv().
        ipt(1:5) = Surf%IP_Tri(1:5,ib)
        IF(Type_Wall(ipt(5))/=2) CYCLE
        ipt(4)   = -ib
     ELSE
        !--- get front and far field triangles here
        k = ib-Surf%NB_Tri
        ipt(1:5) = kfrnt(1:5,k)
        IF(ipt(1)==0 ) CYCLE
        IF(ipt(5)<0  ) CYCLE
        IF(Type_Wall(ipt(5))==3)THEN
           !--- farfield
           ipt(4)   = -k
        ELSE IF(kfrnt(4,k)==0)THEN
           !--- triangle not developed
           WRITE(29,*)' Warning--- a triangle with no layer on: ib,k=',ib,k
           WRITE(29,*)'   kf=',kfrnt(1:5,k)
           ipt(4)   = -k
        ENDIF
     ENDIF

     DO j = 1,3
        ip = ipt(j)
        IF(Mark_Vis_Point(ip)==0)THEN
           np = np + 1
           Mark_Vis_Point(ip) = np       
           IF(np>KeyLength(Surf%Posit))THEN
              CALL allc_2Dpointer(Surf%Posit, 3, np+100000)
           ENDIF
           Surf%Posit(:,np) = VM%Posit(:,ip)
        ENDIF
     ENDDO
     nb = nb +1
     IF(nb>KeyLength(Surf%IP_Tri))THEN
        CALL allc_2Dpointer(Surf%IP_Tri, 5, nb+100000)
     ENDIF
     ipt(1:3) = Mark_Vis_Point(ipt(1:3))
     CALL sort_ipp_tri(ipt(1:3))
     Surf%IP_Tri(1:5, nb) = ipt(1:5)
  ENDDO


  Surf%NB_Point = np
  Surf%NB_Tri   = nb

  Surf%GridOrder = HighOrder_Model
  IF(HighOrder_Model>1)THEN
     NPF = VM%cell%numFaceNodes     
     IF(Surf%NB_Tri>keyLength(Surf%IPsp_Tri)) THEN
        DEALLOCATE(Surf%IPsp_Tri)
        ALLOCATE(Surf%IPsp_Tri(NPF,Surf%NB_Tri))
     ENDIF

     DO ib = 1, Surf%NB_Tri
        ipt(1:4) = Surf%IP_Tri(1:4,ib)           
        IF(ipt(4)<0) THEN
           nb = - ipt(4)   
           ips(1:NPF) = IPsp_Tri(1:NPF, nb)
           ips(1:3)   = Mark_Vis_Point(ips(1:3))
           k = which_RotationinTri (ips(1:3), ipt(1:3))
           IF(k<=0) STOP 'fdsfnmewlifj893gur'
           ips(1:NPF) = ips(iRotate_C3Tri(1:NPF,k))
           DO j = 4,NPF
              ip = ips(j)
              IF(ip==0) CYCLE
              IF(Mark_Vis_Point(ip)==0)THEN
                 np = np + 1
                 Mark_Vis_Point(ip) = np       
                 IF(np>KeyLength(Surf%Posit))THEN
                    CALL allc_2Dpointer(Surf%Posit, 3, np+100000)
                 ENDIF
                 Surf%Posit(:,np) = VM%Posit(:,ip)
              ENDIF
              ips(j) = Mark_Vis_Point(ip)
           ENDDO

        ELSE
           IT = ipt(4)
           DO j = 1, 4
              ips(1:NPF) = VM%IP_Tet(VM%cell%FaceNodes(:,j), IT)
              ips(1:3)   = Mark_Vis_Point(ips(1:3))
              k = which_RotationinTri (ips(1:3), ipt(1:3))
              IF(k/=0) EXIT
           ENDDO
           IF(k>=0) STOP 'fsdkljfwef9weufj'     
           ips(1:NPF) = ips(iRotate_C3Tri(1:NPF,k))
           DO j = 4,NPF
              ip = ips(j)
              IF(ip==0) CYCLE
              IF(Mark_Vis_Point(ip)==0)THEN
                 np = np + 1
                 Mark_Vis_Point(ip) = np       
                 IF(np>KeyLength(Surf%Posit))THEN
                    CALL allc_2Dpointer(Surf%Posit, 3, np+100000)
                 ENDIF
                 Surf%Posit(:,np) = VM%Posit(:,ip)
              ENDIF
              ips(j) = Mark_Vis_Point(ip)
           ENDDO
        ENDIF
        Surf%IPsp_Tri(1:NPF, ib) = ips(1:NPF)
     ENDDO

     Surf%NB_Psp   = np - Surf%NB_Point
     Surf%NB_Point = np
     DEALLOCATE(IPsp_Tri)

  ELSE
     Surf%NB_Psp   = 0
  ENDIF

  Surf%IP_Tri(4,1:Surf%NB_Tri) = 0


  RETURN
END SUBROUTINE resortface

! .................................................................
!   This routine compose prism elements.
!..................................................................

SUBROUTINE form_prism(layer, NextLay )  
  USE common_Parameters, ONLY : VM, NB_BD_Layers, Layer_Hybrid
  USE NodeNetTree
  USE CellConnectivity
  USE HybridMeshManager
  IMPLICIT NONE

  INTEGER :: layer(*), NextLay(*)
  INTEGER :: ipp(6), iqq(3)
  INTEGER :: it, ie, i, j, IB, ib1, ib2, ip1, ip2, itnx, ifa, it1, it2, k, ipr
  INTEGER :: NB, Nend
  TYPE(NodeNetTreeType) :: TriTreeHo, TriTreeVe
  INTEGER, DIMENSION(:,:), POINTER :: IP_TriHo, IP_TriVe, iTR_Tet, iTet_Prm, iTet_Pyr
  INTEGER, DIMENSION(:,:), POINTER :: Next_Prm
  INTEGER, DIMENSION(:  ), POINTER :: marke, mark_prm
  LOGICAL :: CarryOn

  !--- build triangles
  !    IP_Tri(4, :)  right tet.
  !    IP_Tri(5, :)  left tet.
  !    IP_Tri(6, :)  the layer (1 to NB_BD_Layers+1).
  ALLOCATE(IP_TriHo(6,  VM%NB_Tet))
  ALLOCATE(IP_TriVe(5,3*VM%NB_Tet))  
  ALLOCATE(iTR_Tet(4, VM%NB_Tet))
  CALL NodeNetTree_Allocate (3, VM%NB_Point,   VM%NB_Tet, TriTreeHo)
  CALL NodeNetTree_Allocate (3, VM%NB_Point, 3*VM%NB_Tet, TriTreeVe)

  IP_TriHo(4:6, :) = 0
  IP_TriVe(4:5, :) = 0
  iTR_Tet(1:4,  :) = 0

  DO it = 1, VM%NB_Tet
     DO i = 1, 4
        iqq(1:3) = VM%IP_Tet(iTri_Tet(:,i),it)
        IF(layer(iqq(1))==layer(iqq(2)) .AND. layer(iqq(1))==layer(iqq(3)))THEN
           !--- a horizontal trianle
           NB = TriTreeHo%numNets
           CALL NodeNetTree_SearchAdd(3,iqq, TriTreeHo, IB)
           IF(IB>NB)THEN
              IP_TriHo(1:3, IB) = iqq(1:3)
              IP_TriHo(4  , IB) = it
              IP_TriHo(6, IB) = layer(iqq(1)) + 1
           ELSE
              IP_TriHo(5  , IB) = it
           ENDIF
        ELSE
           !--- a vertical trianle
           NB = TriTreeVe%numNets
           CALL NodeNetTree_SearchAdd(3,iqq, TriTreeVe, IB)
           IF(IB>NB)THEN
              IP_TriVe(1:3, IB) = iqq(1:3)
              IP_TriVe(4  , IB) = it
           ELSE
              IP_TriVe(5  , IB) = it
           ENDIF
           iTR_Tet(i, it) = IB
        ENDIF
     ENDDO
  ENDDO

  !--- search possible prisms

  NB = VM%NB_Tet / 3
  ALLOCATE(VM%IP_Prm(6, NB), iTet_Prm(3,NB))
  ALLOCATE(marke(VM%NB_Tet))
  marke(:) = 0

  Nend = 0

  Loop_IB : DO IB = 1, TriTreeHo%numNets
     IF(IP_TriHo(6, IB)>NB_BD_Layers) CYCLE Loop_IB

     ipp(1:3) = IP_TriHo(1:3, IB)
     ipp(4:6) = NextLay(ipp(1:3))
     IF(ipp(1)==ipp(4) .OR. ipp(2)==ipp(5) .OR. ipp(3)==ipp(6)) CYCLE Loop_IB

     CALL NodeNetTree_Search(3,ipp(4:6), TriTreeHo, IB2)
     IF(IB2==0)THEN
        WRITE(29,*)'Error--- no top'
        WRITE(29,*)'  ipp=  ',ipp(1:6)
        WRITE(29,*)'  layer ',Layer(ipp(1:6))
        CALL Error_Stop (' IB2==0')
     ENDIF

     Nend = Nend + 1
     VM%IP_Prm(1:6, Nend) = ipp(1:6)

     !--- fit the orientation, let triangle VM%IP_Prm(1:3,it) points inside the prism. 

     it = IP_TriHo(4, IB)
     DO i = 4,6
        IF(which_NodeinTet(ipp(i), VM%IP_Tet(:,it))>0) EXIT
     ENDDO
     IF(i>6)THEN
        it = IP_TriHo(5, IB)
        VM%IP_Prm(1:6, Nend) = (/ipp(1), ipp(3), ipp(2), ipp(4), ipp(6), ipp(5)/)
        DO i = 4,6
           IF(which_NodeinTet(ipp(i), VM%IP_Tet(:,it))>0) EXIT
        ENDDO
        IF(i>6)THEN
           WRITE(29,*)'Error---wrong it: it=',it,' ip4=',VM%IP_Tet(:,it)
           WRITE(29,*)' IB, i=', IB,i
           WRITE(29,*)' ipp=',ipp(1:6)
           CALL Error_Stop ('  ')
        ENDIF
     ENDIF

     !--- find the three tet. forming this prism.

     iTet_Prm(1,Nend) = it
     marke(it) = Nend

     DO i = 1,4
        ib1   = iTR_Tet(i,it)
        IF(ib1==0) CYCLE
        itnx = IP_TriVe(4,ib1)
        IF(itnx==it) itnx = IP_TriVe(5,ib1)
        IF(itnx==0) CYCLE
        j    = which_NodeinTet(ib1, iTR_Tet(:,itnx))
        IF( VM%IP_Tet(j,itnx)==ipp(4) ) EXIT
        IF( VM%IP_Tet(j,itnx)==ipp(5) ) EXIT
        IF( VM%IP_Tet(j,itnx)==ipp(6) ) EXIT
     ENDDO
     IF(i>4) CALL Error_Stop (' ---no itnx')

     iTet_Prm(2,Nend) = itnx
     marke(itnx) = Nend

     it = IP_TriHo(4, IB2)
     DO i = 1,3
        IF(which_NodeinTet(ipp(i), VM%IP_Tet(:,it))>0) EXIT
     ENDDO
     IF(i>3)THEN
        it = IP_TriHo(5, IB2)
        DO i = 1,3
           IF(which_NodeinTet(ipp(i), VM%IP_Tet(:,it))>0) EXIT
        ENDDO
        IF(i>3)THEN
           WRITE(29,*)'Error---wrong it: it=',it,' ip4=',VM%IP_Tet(:,it)
           WRITE(29,*)' IB, i=', IB,i
           WRITE(29,*)' ipp=',ipp(1:6)
           CALL Error_Stop ('  ')
        ENDIF
     ENDIF

     iTet_Prm(3,Nend) = it
     marke(it) = Nend

  ENDDO Loop_IB

  VM%NB_Prm = Nend
  WRITE(29,*)' possible number NB_Prm=',VM%NB_Prm,' from NB_Tet ',VM%NB_Tet

  !--- build the neighbouring of prisms

  ALLOCATE(Next_Prm(3,VM%NB_Prm))
  ALLOCATE(Mark_Prm(  VM%NB_Prm))
  Next_Prm(:,:) = 0
  Mark_Prm(:)   = 0

  DO IB = 1, TriTreeVe%numNets
     it1 = IP_TriVe(4,IB)
     it2 = IP_TriVe(5,IB)
     IF(it2==0) CYCLE
     it1 = marke(it1)
     it2 = marke(it2)
     IF(it1==0 .OR. it2==0) CYCLE
     IF(it1==it2) CYCLE
     DO i=1,3
        IF(Next_Prm(i,it1)==it2) EXIT
        IF(Next_Prm(i,it1)==0)THEN
           Next_Prm(i,it1) = it2
           EXIT
        ENDIF
     ENDDO
     IF(i>3) CALL Error_Stop (' form_prism:: find prism next ')
     DO i=1,3
        IF(Next_Prm(i,it2)==it1) EXIT
        IF(Next_Prm(i,it2)==0)THEN
           Next_Prm(i,it2) = it1
           EXIT
        ENDIF
     ENDDO
     IF(i>3) CALL Error_Stop (' form_prism:: find prism next ')
  ENDDO

  !--- check the neighbouring of prisms

  NB = VM%NB_Tet / 2
  ALLOCATE(VM%IP_Pyr(5, NB))
  VM%NB_Pyr = 0
  !-- iTet_Pyr(1:6,ie) those tet surrounding prism ie forming pyramids
  !   iTet_Pyr(7,  ie) the number of such tet.
  ALLOCATE(iTet_Pyr(7,VM%NB_Prm))
  iTet_Pyr(:,:) = 0

  CarryOn = .TRUE.
  DO WHILE(CarryOn)
     CarryOn = .FALSE.

     DO ie = 1, VM%NB_Prm
        IF(Mark_Prm(ie)/=0) CYCLE

        ipp(1:6) = VM%IP_Prm(1:6, ie)

        DO ifa=1,3
           iqq(1) = ipp(ifa)
           iqq(2) = ipp(MOD(ifa,3)+1)        
           DO i = 1,2
              iqq(3) = NextLay(iqq(i))
              CALL NodeNetTree_Search(3,iqq, TriTreeVe, IB1)
              IF(IB1>0) EXIT
           ENDDO
           IF(i>2) CALL Error_Stop (' IB1==0 i>2')

           iqq(i) = NextLay(iqq(3-i))
           CALL NodeNetTree_Search(3,iqq, TriTreeVe, IB2)
           IF(IB2==0) CALL Error_Stop (' IB2==0')

           it1 = ip_triVe(4,IB1)
           IF(marke(it1) ==ie) it1 = ip_triVe(5,IB1)
           it2 = ip_triVe(4,IB2)
           IF(marke(it2) ==ie) it2 = ip_triVe(5,IB2)

           IF(it1==0 .OR. it2==0) EXIT            !--- symmetry plane

           IF(marke(it1)/=0 .AND. marke(it1)==marke(it2)) CYCLE          !--- another prism or pyramid

           IF(marke(it1)/=0 .OR. marke(it2)/=0) EXIT          !--- it1 it2 can not form a pyramid

           i = which_NodeinTet(IB1, iTR_Tet(:,it1))
           IF(i==0) CALL Error_Stop (' form_prism_1 :: i==0')
           ip1 = VM%IP_Tet(i,it1)
           i = which_NodeinTet(IB2, iTR_Tet(:,it2))
           IF(i==0) CALL Error_Stop (' form_prism_2 :: i==0')
           ip2 = VM%IP_Tet(i,it2)
           IF(ip1/=ip2) EXIT               !--- it1 it2 can not form a pyramid

           !--- it1 it2 form a pyramid
           !--- keep the diagonal IP_Pyr(1,*)--IP_Pyr(3,*) is the original tet. edge,
           !    so that the pyramid is guaranteed valid for Oubay's solvers.
           VM%NB_Pyr = VM%NB_Pyr + 1
           IF(iqq(1)==ipp(ifa) .AND. iqq(2)/=ipp(MOD(ifa,3)+1))THEN
              VM%IP_Pyr(1, VM%NB_Pyr) = ipp(ifa)
              VM%IP_Pyr(2, VM%NB_Pyr) = ipp(MOD(ifa,3)+1)
              VM%IP_Pyr(3, VM%NB_Pyr) = NextLay(ipp(MOD(ifa,3)+1))
              VM%IP_Pyr(4, VM%NB_Pyr) = NextLay(ipp(ifa))
           ELSE IF(iqq(1)/=ipp(ifa) .AND. iqq(2)==ipp(MOD(ifa,3)+1))THEN
              VM%IP_Pyr(1, VM%NB_Pyr) = ipp(MOD(ifa,3)+1)
              VM%IP_Pyr(2, VM%NB_Pyr) = NextLay(ipp(MOD(ifa,3)+1))
              VM%IP_Pyr(3, VM%NB_Pyr) = NextLay(ipp(ifa))
              VM%IP_Pyr(4, VM%NB_Pyr) = ipp(ifa)
           ELSE
              CALL Error_Stop (' form_prism :: form pyramid')
           ENDIF
           VM%IP_Pyr(5, VM%NB_Pyr) = ip1
           marke(it1) = - VM%NB_Pyr
           marke(it2) = - VM%NB_Pyr
           k = iTet_Pyr(7, ie)
           iTet_Pyr(7,   ie) = k+2
           iTet_Pyr(k+1, ie) = it1
           iTet_Pyr(k+2, ie) = it2         
        ENDDO

        IF(ifa>3 .AND. Layer_Hybrid==2)THEN
           !---- check Prism Contribution
           IF(.NOT. Prism_ValidContribution(ipp(1:6), VM%Posit(:,ipp(1:6)), 0.d0))THEN
              ifa = 0
           ENDIF
        ENDIF

        IF(ifa<=3)THEN
           !--- fail to check neighbour (or contribution).
           !    free tet. from this prism and surrounding pyramids.
           marke(iTet_Prm(1:3,ie)) = 0
           DO k = 1, iTet_Pyr(7, ie)
              ipr = - marke(iTet_Pyr(k, ie))
              VM%IP_Pyr(:, ipr) = 0
              marke(iTet_Pyr(k, ie)) = 0
           ENDDO

           iqq(1:3) = Next_Prm(:,ie)
           DO i = 1,3
              IF(iqq(i)>0)THEN
                 WHERE(Next_Prm(:,iqq(i))==ie) Next_Prm(:,iqq(i)) = 0
                 Mark_Prm(iqq(i)) = 0
              ENDIF
           ENDDO

           Mark_Prm(ie) = -1
           CarryOn      = .TRUE.
        ELSE
           Mark_Prm(ie) = 1
        ENDIF
     ENDDO

  ENDDO


  !--- recount tet.
  NB = 0
  DO ie = 1, VM%NB_Tet     
     IF(marke(ie)/=0) CYCLE
     NB = NB + 1
     VM%IP_Tet(:,NB) = VM%IP_Tet(:,ie)
  ENDDO
  ipr = 0
  DO ie = 1, VM%NB_Pyr     
     IF(VM%IP_Pyr(1,ie)==0) CYCLE
     ipr = ipr + 1
     VM%IP_Pyr(:,ipr) = VM%IP_Pyr(:,ie)
  ENDDO
  Nend = 0
  DO ie = 1, VM%NB_Prm
     IF(Mark_Prm(ie)==0) CALL Error_Stop('form_prism:: Mark_Prm(ie)=0')
     IF(Mark_Prm(ie)<0)  CYCLE
     Nend = Nend + 1
     VM%IP_Prm(:,Nend) = VM%IP_Prm(:,ie)
  ENDDO

  IF(NB + 2*ipr + 3*Nend /= VM%NB_Tet) CALL Error_Stop (' form_prism :: NB not right')
  VM%NB_Tet = NB
  VM%NB_Pyr = ipr
  VM%NB_Prm = Nend

  WRITE(29,*)' VM%NB_Tet, Pyr, Prm=',VM%NB_Tet, VM%NB_Pyr, VM%NB_Prm

  DEALLOCATE( IP_TriHo, IP_TriVe, iTR_Tet, iTet_Prm, iTet_Pyr, Next_Prm )
  DEALLOCATE( marke, Mark_Prm )
  CALL NodeNetTree_Clear (TriTreeHo)
  CALL NodeNetTree_Clear (TriTreeVe)

END SUBROUTINE form_prism


