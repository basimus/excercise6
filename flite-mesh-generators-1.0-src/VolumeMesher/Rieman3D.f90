!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!*******************************************************************************
!>
!!   Calculate the metric or the Scalar at a specific node
!!   from the background mesh.
!!   @param[in]  IP  : the node.
!!   @param[in]  common_Parameters::Posit (:,IP)
!!   @param[out] common_Parameters::Scale_Point (IP) for isotropic problems.
!!   @param[out] common_Parameters::fMap_Point  (IP) for anisotropic problems.
!<     
!*******************************************************************************
SUBROUTINE Get_Point_Mapping(IP)

  USE common_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: IP
  REAL*8  :: P(3), fMap(3,3),  Scalar

  P = Posit(:,IP)

  IF(BGSpacing%Model>0)THEN
     CALL SpacingStorage_GetScale(BGSpacing, P, Scalar)
     Scale_Point(IP) = Scalar
  ELSE IF(BGSpacing%Model<0)THEN
     CALL SpacingStorage_GetMapping(BGSpacing, P, fMap)
     fMap_Point(:,:,IP) = fMap
  ELSE
     CALL  Error_Stop (' Get_Point_Mapping')
  ENDIF

  RETURN
END SUBROUTINE Get_Point_Mapping


!*******************************************************************************
!>
!!   Calculate the metric or the Scalar at specific tetrahedral cell
!!   from the background mesh.
!!   @param[in]  IT  : the element.
!!   @param[in]  common_Parameters::IP_Tet (:,IT)
!!   @param[in]  common_Parameters::Mapping_Choose_Model
!!   @param[out] common_Parameters::Scale_Tet (IP) for isotropic problems.
!!   @param[out] common_Parameters::fMap_Tet  (IP) for anisotropic problems.
!<     
!*******************************************************************************
SUBROUTINE Get_Tet_Mapping(IT)

  USE Geometry3DAll
  USE common_Parameters

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: IT
  INTEGER :: ip4(4)
  REAL*8  :: fMap(3,3),fMaps(3,3,4), Str(6), p(3), Scalar, Scalars(4)

  ip4(1:4) = IP_Tet(1:4,IT)

  IF(Mapping_Choose_Model==3)THEN

     p = (  Posit(:,ip4(1)) + Posit(:,ip4(2))   &
          + Posit(:,ip4(3)) + Posit(:,ip4(4)) )/4.d0

     IF(BGSpacing%Model>1)THEN
        CALL SpacingStorage_GetScale(BGSpacing, P, Scalar)
        Scale_Tet(IT) = Scalar
     ELSE IF(BGSpacing%Model<-1)THEN
        CALL SpacingStorage_GetMapping(BGSpacing, P, fMap)
        fMap_Tet(:,:,IT) = fMap(:,:)
     ENDIF

  ELSE

     IF(BGSpacing%Model>1)THEN
        Scalars(1:4)  = Scale_Point(ip4(1:4))
        Scale_Tet(IT) = Scalar_Mean(4,Scalars,Mapping_Interp_Model)
     ELSE IF(BGSpacing%Model<-1)THEN
        fMaps(:,:,1:4) = fMap_Point(:,:,ip4(1:4))
        fMap(:,:) = Mapping3D_Mean(4,fMaps,Mapping_Interp_Model)
        IF(Mapping3D_Error(fMap)>1.e-4)THEN
           str  = Mapping3D_to_Stretch(fMap)
           fMap = Mapping3D_from_Stretch(str)
        ENDIF
        fMap_Tet(:,:,IT) = fMap(:,:)
     ENDIF

  ENDIF

  RETURN
END SUBROUTINE Get_Tet_Mapping

!*******************************************************************************
!>
!!   Set the mapping of points and elements in the domain.
!!   @param[out] common_Parameters::Scale_Point for isotropic problems.
!!   @param[out] common_Parameters::Scale_Tet   for isotropic problems.
!!   @param[out] common_Parameters::fMap_Point  for anisotropic problems.
!!   @param[out] common_Parameters::fMap_Tet    for anisotropic problems.
!<     
!*******************************************************************************
SUBROUTINE Set_Domain_Mapping()

  USE common_Parameters
  USE Geometry3DAll
  IMPLICIT NONE

  INTEGER :: IT, IP, i
  REAL*8  :: Er, Er0, Str(6), strmax(3), strmin(3)

  IF(BGSpacing%Model==-1 .OR. BGSpacing%Model==1) RETURN
  IF(.NOT. useStretch) RETURN


 ! IF (ELEMENT_BREAK_METHOD/=8) THEN
   DO IP=1,NB_Point
      CALL Get_Point_Mapping(IP)
   ENDDO
 ! ELSE
 !  MAPPING_CHOOSE_MODEL = 1
 ! ENDIF

  Er0 = 0
  IF(BGSpacing%Model<-1)THEN
     DO IP=1,NB_Point
        Er = Mapping3D_Error(fMap_Point(:,:,IP))
        IF(Er>1.e-3)THEN
           IF(Debug_Display>1 .AND. Er>Er0)THEN
              Er0 = Er
              WRITE(*, *)' '
              WRITE(*, *)'Warning--- A mapping at a point is not orthogonal: IP,Er=',IP,Er
              WRITE(29,*)' '
              WRITE(29,*)'Warning--- A mapping at a point is not orthogonal'
              WRITE(29,*)'IP,Er=',IP,Er
              WRITE(29,*)'fmap=', fMap_Point(1,:,IP)
              WRITE(29,*)'     ', fMap_Point(2,:,IP)
              WRITE(29,*)'     ', fMap_Point(3,:,IP)
           ENDIF
           str  = Mapping3D_to_Stretch(fMap_Point(:,:,IP))
           fMap_Point(:,:,IP) = Mapping3D_from_Stretch(str)
        ENDIF
     ENDDO

     IF(Debug_Display>2)THEN
        strmax(1:3) = 0
        strmin(1:3) = 1.d20
        DO IP=1,NB_Point
           str  = Mapping3D_to_Stretch(fMap_Point(:,:,IP))
           DO i=1,3
              strmax(i) = MAX(strmax(i),str(i))
              strmin(i) = MIN(strmin(i),str(i))
           ENDDO
        ENDDO
        WRITE(29,*)'Max stretch: ',REAL(strmax)
        WRITE(29,*)'Min stretch: ',REAL(strmin)
     ENDIF
  ENDIF

  DO IT=1,NB_Tet
     CALL Get_Tet_Mapping(IT)
  ENDDO

  IF(BGSpacing%Model<-1)THEN
     DO IT=1,NB_Tet
        Er = Mapping3D_Error(fMap_Tet(:,:,IT))
        IF(Er>1.e-3)THEN
           IF(DEbug_Display>1)THEN
              WRITE(*, *)' '
              WRITE(*, *)'Warning--- A mapping at a point is not orthogonal: IP,Er=',IP,Er
              WRITE(29,*)'Warning--- A mapping at a tet. is not orthogonal'
              WRITE(29,*)'IT,Er=',IT,Er
              WRITE(29,*)'fmap=', fMap_Tet(1,:,IT)
              WRITE(29,*)'     ', fMap_Tet(2,:,IT)
              WRITE(29,*)'     ', fMap_Tet(3,:,IT)
           ENDIF
           str  = Mapping3D_to_Stretch(fMap_Tet(:,:,IT))
           fMap_Tet(:,:,IT) = Mapping3D_from_Stretch(str)
        ENDIF
     ENDDO
  ENDIF

  RETURN

END SUBROUTINE Set_Domain_Mapping

!*******************************************************************************
!>
!!  Calculate the circum-centre and the radius
!!      in the original/mapped domain
!!  @param[in]  IT0   > 0 : calculate on tetrahedron IT0                      \n
!!                    <=0 : calculate all the tetrahedra in the domain
!!  @param[out] common_Parameters::Sphere_Tet (:,IT0).
!<
!*******************************************************************************
SUBROUTINE Get_Tet_Circum(IT0)

  USE Geometry3DAll
  USE common_Parameters

  IMPLICIT NONE

  INTEGER, INTENT(IN)   :: IT0
  INTEGER :: IT, it1, it2
  REAL*8  :: fMap(3,3), P1(3), P2(3), P3(3), P4(3)

  IF(IT0>0)THEN
     !--- calculate one element
     IF(.NOT.CircumUpdated)THEN
        WRITE(29,*)' '
        WRITE(29,*)'Warning--- the geometry for the domain is not updated'
        WRITE(29,*)' while you want to calculate it for element ',IT0, ' only'
     ENDIF
     it1 = IT0
     it2 = IT0
  ELSE
     !--- calculate the whole domain
     CircumUpdated = .TRUE.
     it1 = 1
     it2 = NB_Tet
  ENDIF

  DO IT = it1,it2

     P1=Posit(:,IP_Tet(1,IT))
     P2=Posit(:,IP_Tet(2,IT))
     P3=Posit(:,IP_Tet(3,IT))
     P4=Posit(:,IP_Tet(4,IT))

     IF(useStretch .AND. BGSpacing%Model<0)THEN
        IF(BGSpacing%Model==-1)THEN
           fMap(:,:) = BGSpacing%BasicMap(:,:)
        ELSE
           fMap(:,:) = fMap_Tet(:,:,IT)
        ENDIF
        P1 = Mapping3D_Posit_Transf(P1,fMap,1)
        P2 = Mapping3D_Posit_Transf(P2,fMap,1)
        P3 = Mapping3D_Posit_Transf(P3,fMap,1)
        P4 = Mapping3D_Posit_Transf(P4,fMap,1)
     ENDIF

     Sphere_Tet(1:3,IT) = Geo3D_Sphere_Centre(P1,P2,P3,P4)
     Sphere_Tet(4,  IT) = Geo3D_Distance(P1,Sphere_Tet(1:3,IT))

     IF(useStretch .AND. BGSpacing%Model>1)THEN
        Sphere_Tet(1:4,IT) = Sphere_Tet(1:4,IT) / Scale_Tet(IT)
     ENDIF

  ENDDO

  RETURN
END SUBROUTINE Get_Tet_Circum

!*******************************************************************************
!>
!!  Calculate new centre of the sphere that lies between the barycentre 
!!     on the circumcentre (on physical plane).
!!  @param[in]  IT0   > 0 : calculate on tetrahedron IT0                      \n
!!                    <=0 : calculate all the tetrahedra in the domain
!!  @param[out] common_Parameters::Sphere_Tet (1:3,IT0).
!!  \sa
!!     FUNCTION Geometry3D::Geo3D_Sphere_Cross()
!<
!*******************************************************************************
SUBROUTINE Get_Tet_SphereCross(IT0)

  USE Geometry3DAll
  USE common_Parameters

  IMPLICIT NONE

  INTEGER, INTENT(IN)   :: IT0
  INTEGER :: IT, it1, it2
  REAL*8  :: P1(3), P2(3), P3(3), P4(3), R1, R2, R3, R4

  
  
  
  IF(IT0>0)THEN
     !--- calculate one element
     it1 = IT0
     it2 = IT0
  ELSE
     !--- calculate the whole domain
     CircumUpdated = .TRUE.
     it1 = 1
     it2 = NB_Tet
  ENDIF

  DO IT = it1,it2
     !CALL Get_Tet_Circum(IT)
     P1=Posit(:,IP_Tet(1,IT))
     P2=Posit(:,IP_Tet(2,IT))
     P3=Posit(:,IP_Tet(3,IT))
     P4=Posit(:,IP_Tet(4,IT))
     R1=RR_Point(IP_Tet(1,IT))
     R2=RR_Point(IP_Tet(2,IT))
     R3=RR_Point(IP_Tet(3,IT))
     R4=RR_Point(IP_Tet(4,IT))
     Sphere_Tet(1:3,IT) = Geo3D_Sphere_Cross(P1,P2,P3,P4,R1,R2,R3,R4)
  ENDDO

  RETURN
END SUBROUTINE Get_Tet_SphereCross

!*******************************************************************************
!>
!!   Drag the circum-centre of a tetrahedron to its boundary
!!     if the centre is located outside the cell.
!!  @param[in]  IT0   > 0 : calculate on tetrahedron IT0                      \n
!!                    <=0 : calculate all the tetrahedra in the domain
!!  @param[in,out] common_Parameters::Sphere_Tet (1:3,IT0).
!<
!*******************************************************************************
SUBROUTINE Bound_Tet_SphereCentre(IT0)

  USE Geometry3DAll
  USE common_Constants
  USE common_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(IN)   :: IT0
  INTEGER :: IT, it1, it2, i, j, ntype
  REAL*8  :: fMap(3,3), P1(3), P2(3), P3(3), P4(3), pp3(3,3), pp2(3,2)
  REAL*8  :: weight(3), tt

  IF(.NOT. CircumUpdated)THEN
     WRITE(29,*)'Error--- Geometry is not updated'
     CALL Error_Stop ('Bound_Tet_SphereCentre')
  ENDIF

  IF(IT0>0)THEN
     !--- calculate one element
     it1 = IT0
     it2 = IT0
  ELSE
     !--- calculate the whole domain
     it1 = 1
     it2 = NB_Tet
  ENDIF

  DO IT = it1,it2

     P1=Posit(:,IP_Tet(1,IT))
     P2=Posit(:,IP_Tet(2,IT))
     P3=Posit(:,IP_Tet(3,IT))
     P4=Posit(:,IP_Tet(4,IT))

     IF(useStretch .AND. BGSpacing%Model<0)THEN
        IF(BGSpacing%Model==-1)THEN
           fMap(:,:) = BGSpacing%BasicMap(:,:)
        ELSE
           fMap(:,:) = fMap_Tet(:,:,IT)
        ENDIF
        P1 = Mapping3D_Posit_Transf(P1,fMap,1)
        P2 = Mapping3D_Posit_Transf(P2,fMap,1)
        P3 = Mapping3D_Posit_Transf(P3,fMap,1)
        P4 = Mapping3D_Posit_Transf(P4,fMap,1)
     ENDIF

     pp2(:,1) = (P1+P2+P3+p4)/4.d0
     pp2(:,2) = Sphere_Tet(1:3,IT)

     DO i=1,4
        DO j=1,3
           pp3(:,j) = Posit(:,IP_Tet(iTri_Tet(j,i) ,IT))
        ENDDO
        ntype = 1
        P1 = Plane3D_Line_Tri_Cross(pp2,pp3,ntype,weight,tt)
        IF(tt>0 .AND. tt<1) pp2(:,2) = P1
     ENDDO

     Sphere_Tet(1:3,IT) = pp2(:,2)

     IF(useStretch .AND. BGSpacing%Model>1)THEN
        Sphere_Tet(1:3,IT) = Sphere_Tet(1:3,IT) / Scale_Tet(IT)
     ENDIF

  ENDDO

  RETURN
END SUBROUTINE Bound_Tet_SphereCentre

!*******************************************************************************
!>
!!  Calculate the volume of the tetrahedral mesh in the original/mapped domain.
!!  @param[in]  IT0   > 0 : calculate on tetrahedron IT0                      \n
!!                    <=0 : calculate all the tetrahedra in the domain
!!  @param[out] common_Parameters::Volume_Tet (IT0).
!<
!*******************************************************************************
SUBROUTINE Get_Tet_Volume(IT0)

  USE Geometry3DAll
  USE common_Parameters

  IMPLICIT NONE

  INTEGER, INTENT(IN)   :: IT0
  INTEGER :: IT, it1, it2
  REAL*8  :: fMap(3,3), P1(3), P2(3), P3(3), P4(3)

  IF(IT0>0)THEN
     !--- calculate one element
     IF(.NOT.VolumeUpdated)THEN
        WRITE(29,*)' '
        WRITE(29,*)'Warning--- the volume for the domain is not updated'
        WRITE(29,*)' while you want to calculate it for element ',IT0, ' only'
     ENDIF
     it1 = IT0
     it2 = IT0
  ELSE
     !--- calculate the whole domain
     VolumeUpdated = .TRUE.
     it1 = 1
     it2 = NB_Tet
  ENDIF

  DO IT = it1,it2
     IF(IP_Tet(4,IT)<=0) CYCLE

     P1=Posit(:,IP_Tet(1,IT))
     P2=Posit(:,IP_Tet(2,IT))
     P3=Posit(:,IP_Tet(3,IT))
     P4=Posit(:,IP_Tet(4,IT))

     IF(useStretch .AND. BGSpacing%Model<0)THEN
        IF(BGSpacing%Model==-1)THEN
           fMap(:,:) = BGSpacing%BasicMap(:,:)
        ELSE
           fMap(:,:) = fMap_Tet(:,:,IT)
        ENDIF
        Volume_Tet(IT) = Mapping3D_Tet_Volume(P1,P2,P3,P4,fMap)
     ELSE IF(useStretch .AND. BGSpacing%Model>1)THEN
        Volume_Tet(IT) = Geo3D_Tet_Volume_High(P1,P2,P3,P4) / Scale_Tet(IT)**3
     ELSE
        Volume_Tet(IT) = Geo3D_Tet_Volume_High(P1,P2,P3,P4)
     ENDIF

  ENDDO

  RETURN
END SUBROUTINE Get_Tet_Volume

!*******************************************************************************
!>
!!  Check the geometrical quality of the original/mapped domain.
!!  @param[in]  message   a character string to display.
!!
!!    Screen Output:  max/min of Volume_Tet(:)
!<
!*******************************************************************************
SUBROUTINE Check_Mesh_Geometry(message)

  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  IMPLICIT NONE

  CHARACTER(LEN=*), INTENT(in) :: message
  REAL*8  :: p1(3),p2(3),p3(3),p4(3)
  INTEGER :: xstat (4,10)
  REAL*8  :: QualityInt(4)
  REAL*8  :: di, dimi, al
  INTEGER :: i, IT, ITnb, nne, is, k, j1, j2, j3, j4, Isucc
  INTEGER :: ITnegt
  INTEGER, SAVE :: icheck = 0

  IF(.NOT. VolumeUpdated) CALL Get_Tet_Volume(0)

  icheck = icheck + 1

  nne      = 0
  ITnegt   = 0

  DO  IT = 1,NB_Tet
     IF(IP_Tet(4,IT)<=0) CYCLE
     Quality_Tet(1,IT) = Volume_Tet(IT)
     IF(Quality_Tet(1,IT)>0)THEN
     ELSE IF(Quality_Tet(1,IT)<=0)THEN
        ITnegt = ITnegt+1
     ELSE
        WRITE(*, *)' Error--- NaN value for IT=',IT
        WRITE(29,*)' Error--- NaN value for IT=',IT
        WRITE(29,*)'   ips=',IP_Tet(:,IT)
        WRITE(29,*)'   Pt1=',Posit(1,IP_Tet(:,IT))
        CALL Error_Stop('Check_Mesh_Geometry:: '//message)
     ENDIF

     dimi = 100.
     DO  is = 1, 6
        j1     = IP_Tet(I_Comb_Tet(1,is),IT)
        j2     = IP_Tet(I_Comb_Tet(2,is),IT)
        j3     = IP_Tet(I_Comb_Tet(3,is),IT)
        j4     = IP_Tet(I_Comb_Tet(4,is),IT)
        IF(j1==0 .OR.  j2==0 .OR.  j3==0 .OR.  j4==0) THEN
           CALL  Error_Stop (' error')
        ENDIF
        p1(:) = Posit(:,j1)
        p2(:) = Posit(:,j2)
        p3(:) = Posit(:,j3)
        p4(:) = Posit(:,j4)
        di    = Geo3D_Dihedral_Angle(p1   ,p2   ,p3   ,p4 , Isucc  )
        dimi  = MIN(dimi,di)
     ENDDO
     Quality_Tet(2,IT) = dimi * 180.d0 / PI

     nne = nne + 1
  ENDDO

  IF(icheck==1)THEN
     OPEN(1,file=JobName(1:JobNameLength)//'.chk', status='unknown', POSITION='REWIND')
  ELSE
     OPEN(1,file=JobName(1:JobNameLength)//'.chk', status='old', POSITION='APPEND')
  ENDIF

  WRITE(1,*)'Mesh Geometry quality::  ', message
  WRITE(1,*)'number of nodes: ',NB_Point, ' number of cells: ',NB_Tet
  WRITE(1,*)'useStretch: ',useStretch,' BGSpacing%Model: ',BGSpacing%Model

  QualityMin(1:2) =  1.d36
  QualityMax(1:2) = -1.d36
  DO IT = 1, NB_Tet
     IF(IP_Tet(4,IT)<=0) CYCLE
     DO k=1,2
        QualityMin(k) = MIN(QualityMin(k), Quality_Tet(k,IT))
        QualityMax(k) = MAX(QualityMax(k), Quality_Tet(k,IT))
     ENDDO
  ENDDO
  QualityMax(2) = 70.528779366d0

  QualityInt(:) = (QualityMax(:) - QualityMin(:))/10.d0
  WRITE(1,'(a,E12.5,a,E12.5)') ' min_Vol: ',QualityMin(1),   &
       ' max_Vol: ',QualityMax(1)
  WRITE(1,'(a,F14.8)') ' Minimum dihedral angle: ',QualityMin(2)

  IF(1==0)THEN
  DO i = 1 , 10
     xstat(1:4,i) = 0
     DO IT = 1 , NB_Tet
        IF(IP_Tet(4,IT)>0) THEN
           DO k=1,2
              di = Quality_Tet(k,IT)-QualityMin(k)
              IF( (di>=QualityInt(k)*(i-1) .OR. i==1) .AND.   &
                   (di< QualityInt(k)*i .OR. i==10)  ) THEN
                 xstat(k,i) = xstat(k,i) + 1
              ENDIF
           ENDDO
        ENDIF
     ENDDO
     WRITE(1,'(8e12.4)')(100.d0*xstat(k,i)/nne, k=1,2)
  ENDDO
  ENDIF

  IF(NB_Tet/=nne) WRITE(1,*)'number of fake elements     : ',NB_Tet-nne
  IF(ITnegt>0)    WRITE(1,*)'number of negative elements : ',ITnegt
  WRITE(1,*)' '

  CLOSE (1)

  RETURN
END SUBROUTINE Check_Mesh_Geometry

!*******************************************************************************
!>
!!  Check the geometrical quality and circum-centre of the mesh 
!!     in the original/mapped domain.
!!  @param[in]  message   a character string to display.
!!
!!    Screen Output:  max/min of Sphere_Tet(4,:) , Volume_Tet(:)
!<
!*******************************************************************************
SUBROUTINE Check_Mesh_Geometry2(message)

  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  USE PointAssociation
  IMPLICIT NONE

  CHARACTER(LEN=*), INTENT(in) :: message
  REAL*8  :: p1(3),p2(3),p3(3),p4(3),pp2(3,2),pp3(3,3),weight(3),t,pp(3),pc(3)
  INTEGER :: xstat (4,10),KK,ntype,flag
  REAL*8  :: QualityInt(4),vomid(3),ptet(3,4), vnorm(3)
  REAL*8  :: di, dimi, al, vomin, delta, dist, targDist,normFact,dum(4),minVol
  INTEGER :: i, IT, ITnb, nne, is, k, j1, j2, j3, j4, Isucc, ifa
  INTEGER :: noutside, noutside1, noutside2, ITnegt, numMerge, numSharp
  INTEGER, SAVE :: icheck = 0
  REAL*8 :: qualStore(3,NB_Tet)
  REAL*8 :: qualStoreFace(4,4*NB_Tet),meanDelEdge(NB_Tet)
  REAL*8 :: worseAngle,thisAngle,unitV(3),a,b,c
  INTEGER :: iFace1
  INTEGER, POINTER :: mergeStore(:)
  INTEGER :: merged(NB_Tet)
  INTEGER :: facesMarked(NB_Tet)
  TYPE(Plane3D) :: aPlane
  INTEGER :: noFaces

  INTEGER :: ipp(3),ib

  INTEGER :: ip, ie, ic
  REAL*8  :: numSurround, minSurround, count
  
  TYPE(PointAssociationArray) :: PointAsso2

  noFaces = 0
  
  CALL Get_Tet_SphereCross(0)
  CALL Get_Tet_Volume(0)

 
  ALLOCATE(mergeStore(NB_Tet))
  
  merged(1:NB_Tet) = 0
  qualStore(1:3,1:NB_Tet) = 0.0d0
  facesMarked(1:NB_Tet) = 0
 
  
 
  icheck = icheck + 1

  noutside = 0
  noutside1= 0
  noutside2= 0
  nne      = 0
  ITnegt   = 0
  numMerge = 0
  numSharp = 0
  
  Mark_Tet(1:NB_Tet) = 0
  
  !Calculate mean edge length of tets

  CALL PointAssociation_Allocate(NB_Point, PointAsso2)
  CALL PointAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso2)

  !Just checking data structure
  minSurround = HUGE(0.0d0)
  numSurround = 0.0d0
  DO ip = 1,NB_Point
     Mark_Point(1:NB_Point) = 0
     count = 0
     Mark_Point(ip) = 1
     DO ic = 1,PointAsso2%PointList(ip)%numCells
      ie = PointAsso2%PointList(ip)%CellList(ic)
      DO i = 1,4
        ib = IP_Tet(i,ie)
        IF (Mark_Point(ib).EQ.0) THEN
          count = count + 1
          Mark_Point(ib) = 1
        ENDIF
      ENDDO
     ENDDO
     minSurround = MIN(minSurround, count)
     numSurround = numSurround + count
  ENDDO

  numSurround = REAL(numSurround)/REAL(NB_Point)
 



  DO IT = 1,NB_Tet
  	ptet(:,1) = Posit(:,IP_Tet(1,IT))
  	ptet(:,2) = Posit(:,IP_Tet(2,IT))
  	ptet(:,3) = Posit(:,IP_Tet(3,IT))
  	ptet(:,4) = Posit(:,IP_Tet(4,IT))
  	
  	meanDelEdge(IT) = ( Geo3D_Distance(ptet(:,1),ptet(:,2)) + &
  						Geo3D_Distance(ptet(:,1),ptet(:,3)) + &
  						Geo3D_Distance(ptet(:,1),ptet(:,4)) + &
  						Geo3D_Distance(ptet(:,2),ptet(:,3)) + &
  						Geo3D_Distance(ptet(:,2),ptet(:,4)) + &
  						Geo3D_Distance(ptet(:,3),ptet(:,4)))/6.0d0
    numSurround = 0.0d0
    DO ie = 1,4
      ip = IP_Tet(ie,IT)

     
      numSurround = numSurround + REAL(PointAsso2%PointList(ip)%numCells)


    ENDDO
    qualStore(3,IT) = numSurround*0.25d0

  
  END DO
  CALL PointAssociation_Clear(PointAsso2)
  
  DO  IT = 1,NB_Tet
     IF(IP_Tet(4,IT)<=0) CYCLE
     Quality_Tet(1,IT) = Volume_Tet(IT)
     IF(Quality_Tet(1,IT)<=0.) ITnegt = ITnegt+1

     dimi = 100.
     DO  is = 1, 6
        j1     = IP_Tet(I_Comb_Tet(1,is),IT)
        j2     = IP_Tet(I_Comb_Tet(2,is),IT)
        j3     = IP_Tet(I_Comb_Tet(3,is),IT)
        j4     = IP_Tet(I_Comb_Tet(4,is),IT)
        IF(j1==0 .OR.  j2==0 .OR.  j3==0 .OR.  j4==0) THEN
           CALL  Error_Stop (' error')
        ENDIF
        p1(:) = Posit(:,j1)
        p2(:) = Posit(:,j2)
        p3(:) = Posit(:,j3)
        p4(:) = Posit(:,j4)
        di    = Geo3D_Dihedral_Angle(p1   ,p2   ,p3   ,p4 , Isucc  )
        dimi  = MIN(dimi,di)
     ENDDO
     Quality_Tet(2,IT) = dimi * 180.d0 / PI
	 IF (Quality_Tet(2,IT)<10.0d0) THEN
	   numSharp = numSharp+1
	   Mark_Tet(numSharp) = IT
	 
	 ENDIF
	 !Qual store 2 = min dih angle
	 qualStore(1,IT) = Quality_Tet(2,IT)

     p1(:) = ( Posit(:,IP_Tet(1,IT)) + Posit(:,IP_Tet(2,IT))    &
          + Posit(:,IP_Tet(3,IT)) + Posit(:,IP_Tet(4,IT)) ) / 4.d0
     p4(:) = Sphere_Tet(1:3,IT)
     Quality_Tet(3,IT) = DSQRT((p4(1)-p1(1))**2+(p4(2)-p1(2))**2+(p4(3)-p1(3))**2)
     !Qual 2 is distance between circumcenter and voronoi normalised by half idea voronoi edge
	 normFact = 0.5d0*(meanDelEdge(IT)*0.38d0)
	 qualStore(2,IT) = (Quality_Tet(3,IT)/normFact)
	 
     vomin      = HUGE(0.d0)
	 
	
	 
     DO i = 1, 4
        ITnb = Next_Tet(i,IT)
        IF(ITnb<=0) CYCLE
		
		
        p1(:) = Sphere_Tet(1:3,ITnb)
		!Voronoi edge length
        al    = DSQRT((p4(1)-p1(1))**2+(p4(2)-p1(2))**2+(p4(3)-p1(3))**2)
		
		!Voronoi mid point
		vomid(:) = 0.5d0*(p1(:)+p4(:))
		
		!Ideal voronoi edge length
		dist = 0.38d0*0.5d0*(meanDelEdge(IT)+meanDelEdge(ITnb))
		
		
		IF (al.LT.vomin) THEN
		  vomin = al	  
		   
        ENDIF
		
		
		IF(facesMarked(ITnb).EQ.1) CYCLE
		!Here we are looking at the possiblity of ITnb merging with IT
		!vomid is where the new voronoi point would end up we want to find the
		!worse angle
		worseAngle = 0.0d0
		!Faces in IT
		DO ifa = 1, 4
		  iFace1 = Next_Tet(ifa,IT)
		  IF ((iFace1.NE.ITnb).AND.(iFace1.GT.0)) THEN
		    pp3(:,1) = Posit(:,IP_Tet(iTri_Tet(1,ifa),IT))
			pp3(:,2) = Posit(:,IP_Tet(iTri_Tet(2,ifa),IT))
			pp3(:,3) = Posit(:,IP_Tet(iTri_Tet(3,ifa),IT))
			vnorm(:) = Geo3D_Cross_Product(pp3(:,1),pp3(:,2),pp3(:,3))
			vnorm(:) = vnorm(:)/Geo3D_Distance(vnorm(:))
			unitV(:) = vomid(:)-Sphere_Tet(1:3,iFace1)
			unitV(:) = unitV(:)/Geo3D_Distance(unitV(:))
			a = Geo3D_Distance(Geo3D_Cross_Product(unitV(:),vnorm(:)))
			b = Geo3D_Dot_Product(unitV(:),vnorm)
            thisAngle = ABS(1.0d0-b)
                       
			
			worseAngle = MAX(worseAngle,thisAngle)
		  END IF
		END DO
		!Faces in ITnb
		DO ifa = 1, 4
		  iFace1 = Next_Tet(ifa,ITnb)
		  IF ((iFace1.NE.IT).AND.(iFace1.GT.0)) THEN
		    pp3(:,1) = Posit(:,IP_Tet(iTri_Tet(1,ifa),ITnb))
			pp3(:,2) = Posit(:,IP_Tet(iTri_Tet(2,ifa),ITnb))
			pp3(:,3) = Posit(:,IP_Tet(iTri_Tet(3,ifa),ITnb))
			vnorm(:) = Geo3D_Cross_Product(pp3(:,1),pp3(:,2),pp3(:,3))
			vnorm(:) = vnorm(:)/Geo3D_Distance(vnorm(:))
			unitV(:) = vomid(:)-Sphere_Tet(1:3,iFace1)
			
			a = Geo3D_Distance(Geo3D_Cross_Product(unitV(:),vnorm(:)))
			b = Geo3D_Dot_Product(unitV(:),vnorm)
			thisAngle =ABS(1.0d0-b)
		   
			worseAngle = MAX(worseAngle,thisAngle)
		  END IF
		END DO
		
		noFaces = noFaces + 1
		qualStoreFace(3,noFaces) = worseAngle
		
		!Merge based on distance
		IF (al<(0.1d0*dist)) THEN
		    merged(IT) = 1
			merged(ITnb) = 1			
			flag = 0
		END IF	 
		
		!...or merge based on projection error
		IF (worseAngle<(0.1d0)) THEN
		    merged(IT) = 1
			merged(ITnb) = 1			
			flag = 0
		END IF	 
		
		qualStoreFace(4,noFaces) = al/dist
		
		!Need to think about how merging effects the following stats
		
		pp3(:,1) = Posit(:,IP_Tet(iTri_Tet(1,i),IT))
		pp3(:,2) = Posit(:,IP_Tet(iTri_Tet(2,i),IT))
		pp3(:,3) = Posit(:,IP_Tet(iTri_Tet(3,i),IT))
		aPlane = Plane3D_Build(pp3(:,1),pp3(:,2),pp3(:,3))
		qualStoreFace(1,noFaces) = Plane3D_DistancePointToPlane(vomid,aPlane)/dist
		
		!Does this line cross?
		pp2(:,1) = p1
		pp2(:,2) = p4
		
		pp = Plane3D_Line_Tri_Cross(pp2,pp3,ntype,weight,t)
		pc(:) = (pp3(:,1)+pp3(:,2)+pp3(:,3))/3.0d0
		
		IF (ntype.EQ.7) THEN
			qualStoreFace(2,noFaces) = Geo3D_Distance(pp,pc)/dist
		ELSE
			qualStoreFace(2,noFaces) = -1.0d0*(Geo3D_Distance(pp,pc)/dist)
		ENDIF
		
		
		
		
		
     ENDDO
	 
	 facesMarked(IT) = 1
	 
	 
     Quality_Tet(4,IT) = vomin
	 

   ENDDO

   IF (numSharp>0) THEN
      !Output sharp elements
	  MeshOut%GridOrder = GridOrder
      MeshOut%NB_Point  = NB_Point
      MeshOut%NB_Tet    = NB_Tet
      ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
      ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
      MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
      MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)
      CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_sharp',JobNameLength+6, -15, MeshOut,Surf,Mark_Tet,numSharp)
	  
      DEALLOCATE(MeshOut%IP_Tet)
      DEALLOCATE(MeshOut%Posit)
   
   
   ENDIF


   Mark_Tet(1:NB_Tet) = 0
   

   !Mark the points not on farfield but on boundary
   Mark_Point(1:NB_Point) = 0
   DO IT = 1,Surf%NB_Tri
    IF (  (Type_Wall(Surf%IP_Tri(5,IT)).NE.2).AND. &
          (Type_Wall(Surf%IP_Tri(5,IT)).NE.3) ) THEN

      Mark_Point( Surf%IP_Tri(1:3,IT) ) = 1
    ENDIF

   ENDDO

  
   
   DO  IT = 1,NB_Tet   	

	   
	 
	IF(BGSpacing%Model>0)THEN
		
		delta = Scale_Tet(IT)*BGSpacing%BasicSize
				
	ELSE IF(BGSpacing%Model<0)THEN
		delta = MINVAL(fMap_Tet(:,:,IT))*BGSpacing%BasicSize
				
	END IF
	 
	IF  (merged(IT).EQ.1) THEN
		
		numMerge = numMerge + 1
		mergeStore(numMerge) = IT
	ELSE
		
		p4(:) = Sphere_Tet(1:3,IT)
		
		!Check if we are inside
		!ptet(:,1) = Posit(:,IP_Tet(1,IT))
		!ptet(:,2) = Posit(:,IP_Tet(2,IT))
		!ptet(:,3) = Posit(:,IP_Tet(3,IT))
		!ptet(:,4) = Posit(:,IP_Tet(4,IT))
		KK=0
		!dum =  Geo3D_Tet_Weight(KK,ptet,p4)

    CALL isInside(IT,KK,minVol)
	
		
		IF (KK==1) THEN
		ELSE
		  noutside = noutside + 1
		  Mark_Tet(noutside)=IT
		  DO i = 1,4
		    IF ( (Mark_Point(IP_Tet(iTri_Tet(1,i),IT)).EQ.1).AND. &
				  (Mark_Point(IP_Tet(iTri_Tet(2,i),IT)).EQ.1).AND. &
				  (Mark_Point(IP_Tet(iTri_Tet(3,i),IT)).EQ.1) ) THEN
				!Boundary
				noutside1 = noutside1 + 1
					 
			ENDIF
		  END DO
		  IF(  (Mark_Point(IP_Tet(1,IT)).EQ.1) .OR. (Mark_Point(IP_Tet(2,IT)).EQ.1)  .OR.   &
					(Mark_Point(IP_Tet(3,IT)).EQ.1)  .OR. (Mark_Point(IP_Tet(4,IT)).EQ.1)  )THEN
				  noutside2 = noutside2 + 1
		  ENDIF  
		  
		END IF
		
	ENDIF
     nne = nne + 1

  ENDDO
  
  IF (numMerge>0) THEN
      !Output sharp elements
	  MeshOut%GridOrder = GridOrder
      MeshOut%NB_Point  = NB_Point
      MeshOut%NB_Tet    = NB_Tet
      ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
      ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
      MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
      MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)
      CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_merge',JobNameLength+6, -15, MeshOut,Surf,mergeStore,numMerge)
	  
      DEALLOCATE(MeshOut%IP_Tet)
      DEALLOCATE(MeshOut%Posit)
   
   
   ENDIF
 
 
  OPEN(1985,file=JobName(1:JobNameLength)//TRIM(message)//'.chkelem', status='unknown',POSITION='REWIND')
  OPEN(1986,file=JobName(1:JobNameLength)//TRIM(message)//'.chkface', status='unknown',POSITION='REWIND')
   
  IF(icheck==1)THEN
     OPEN(1,file=JobName(1:JobNameLength)//'.chk', status='unknown', POSITION='REWIND')
     OPEN(1984,file=JobName(1:JobNameLength)//'hist.chk', status='unknown', POSITION='REWIND')
  ELSE
     OPEN(1,file=JobName(1:JobNameLength)//'.chk', status='old', POSITION='APPEND')
     OPEN(1984,file=JobName(1:JobNameLength)//'hist.chk', status='old', POSITION='APPEND')
  ENDIF

  WRITE(1,*)'Mesh Geometry quality::  ', message
  WRITE(1,*)'number of nodes: ',NB_Point, ' number of cells: ',NB_Tet
  WRITE(1,*)'useStretch: ',useStretch,' BGSpacing%Model: ',BGSpacing%Model
  WRITE(1,*)'Mean valancy::  ', numSurround, ' min valancy ', minSurround

  QualityMin(1:4) = Quality_Tet(1:4,1)
  QualityMax(1:4) = Quality_Tet(1:4,1)
  DO IT = 2, NB_Tet
     DO k=1,4
        QualityMin(k) = MIN(QualityMin(k), Quality_Tet(k,IT))
        QualityMax(k) = MAX(QualityMax(k), Quality_Tet(k,IT))
     ENDDO
  ENDDO
  QualityMax(2) = 70.528779366d0
  QualityMin(3) = 0.d0

  QualityInt(:) = (QualityMax(:) - QualityMin(:))/10.d0
  WRITE(1,'(a,E12.5,a,E12.5)') ' min_Vol: ',QualityMin(1),   &
       ' max_Vol: ',QualityMax(1)
  WRITE(1,'(a,F14.8)') ' Minimum dihedral angle: ',QualityMin(2)
  WRITE(1,'(a,E12.5)') ' Max distance (of gridsizes) between circum-centre and centroid: ',   &
       QualityMax(3)/BGSpacing%BasicSize
  WRITE(1,'(a,E12.5)') ' Min distance (of gridsizes) between circum-centres of two cells:',   &
       QualityMin(4)/BGSpacing%BasicSize
  WRITE(1,'(a,E12.5)') ' Max distance (of gridsizes) between circum-centres of two cells:',   &
       QualityMax(4)/BGSpacing%BasicSize

  IF(1==0)THEN
  DO i = 1 , 10
     xstat(1:4,i) = 0
     DO IT = 1 , NB_Tet
        IF(IP_Tet(4,IT)>0) THEN
           DO k=1,4
              di = Quality_Tet(k,IT)-QualityMin(k)
              IF( (di>=QualityInt(k)*(i-1) .OR. i==1) .AND.   &
                   (di< QualityInt(k)*i .OR. i==10)  ) THEN
                 xstat(k,i) = xstat(k,i) + 1
              ENDIF
           ENDDO
        ENDIF
     ENDDO
     WRITE(1,'(8e12.4)')(100.d0*xstat(k,i)/nne, k=1,4)
  ENDDO
  ENDIF

  CALL CPU_TIME(TimeEnd)

  WRITE(1,*)'number of elements with a circum-centre out: ', noutside
  WRITE(1,*)' and out of them ', noutside1, ' on boundary, ',noutside2, 'touch boundary'
  WRITE(1,*)' number merged',numMerge
  IF(NB_Tet/=nne) WRITE(1,*)'number of fake elements     : ',NB_Tet-nne
  IF(ITnegt>0)    WRITE(1,*)'number of negative elements : ',ITnegt
  WRITE(1,*)' '
  WRITE(1984,'(I7,I10,2E20.10,2I8,t1)') icheck-1,NB_Tet, &
             TimeEnd-TimeStart, 100.0d0*(REAL(noutside)/REAL(NB_Tet)), &
             noutside1,noutside2
  CLOSE(1984)
  CLOSE (1)
  
  !Min dihedral angle, distance between circumcenter and voronoi normalised by half idea voronoi edge
 ! WRITE(1985,*) 'Min Dih Angle | Circum-centroid distance | Number of elements touching per node'
  DO IT = 1,NB_Tet
    WRITE(1985,'(3E17.8)') qualStore(1,IT),qualStore(2,IT),qualStore(3,IT)
  END DO
  
  CLOSE(1985)


  !WRITE(1986,*) 'Dist Vor mid to face | Edge intersection to face centroid | Edge face angle if Merge | Edge length'
  DO IT = 1,noFaces
    WRITE(1986,'(4E17.8)') qualStoreFace(1,IT),qualStoreFace(2,IT),qualStoreFace(3,IT),qualStoreFace(4,IT)
  END DO
  
  CLOSE(1986)
  
  
  DEALLOCATE(mergeStore)
  
  RETURN
END SUBROUTINE Check_Mesh_Geometry2



SUBROUTINE isMerged(IT,merged)
  !This subroutine simply checks if an element is merged or not
  !Updates the circumcenters of the elements surrounding IT
  !If merged is less than zero then the element merges
  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  IMPLICIT NONE
  
  INTEGER, INTENT(IN) :: IT
  REAL*8, INTENT(OUT) :: merged
  
  REAL*8  :: p1(3),p4(3),pp3(3,3)
  
  REAL*8  :: vomid(3), vnorm(3)
  
  INTEGER :: i, ITnb, ifa
  REAL*8 :: worseAngle,thisAngle,unitV(3),a,b,c, al
  REAL*8 :: meanDelEdge1, meanDelEdge2,dist,ptet(3,4)
  INTEGER :: iFace1,flag
 

  
  merged = HUGE(0.0d0)
  flag = 0
  
  p4(:) = Sphere_Tet(1:3,IT)
  
  ptet(:,1) = Posit(:,IP_Tet(1,IT))
  ptet(:,2) = Posit(:,IP_Tet(2,IT))
  ptet(:,3) = Posit(:,IP_Tet(3,IT))
  ptet(:,4) = Posit(:,IP_Tet(4,IT))

  meanDelEdge1 =  Scale_Tet(IT)*BGSpacing%BasicSize
  
  DO i = 1, 4
    ITnb = Next_Tet(i,IT)
        
		IF(ITnb<=0) CYCLE
		IF(flag.EQ.1) CYCLE
		
		
		
    p1(:) = Sphere_Tet(1:3,ITnb)
			
		!Voronoi mid point
		vomid(:) = 0.5d0*(p1(:)+p4(:))
		!Voronoi edge length
		al    = DSQRT((p4(1)-p1(1))**2+(p4(2)-p1(2))**2+(p4(3)-p1(3))**2)
		
		
		ptet(:,1) = Posit(:,IP_Tet(1,ITnb))
	  ptet(:,2) = Posit(:,IP_Tet(2,ITnb))
	  ptet(:,3) = Posit(:,IP_Tet(3,ITnb))
	  ptet(:,4) = Posit(:,IP_Tet(4,ITnb))

	  meanDelEdge2 = Scale_Tet(ITnb)*BGSpacing%BasicSize
		
		
		
		!Ideal voronoi edge length
		dist = 0.38d0*0.5d0*(meanDelEdge1+meanDelEdge2)

		!Here we are looking at the possiblity of ITnb merging with IT
		!vomid is where the new voronoi point would end up we want to find the
		!worse angle
		worseAngle = 0.0d0
		!Faces in IT
		DO ifa = 1, 4
		  iFace1 = Next_Tet(ifa,IT)
		  IF ((iFace1.NE.ITnb).AND.(iFace1.GT.0)) THEN
		    pp3(:,1) = Posit(:,IP_Tet(iTri_Tet(1,ifa),IT))
			pp3(:,2) = Posit(:,IP_Tet(iTri_Tet(2,ifa),IT))
			pp3(:,3) = Posit(:,IP_Tet(iTri_Tet(3,ifa),IT))
			vnorm(:) = Geo3D_Cross_Product(pp3(:,1),pp3(:,2),pp3(:,3))
			vnorm(:) = vnorm(:)/Geo3D_Distance(vnorm(:))
			
			unitV(:) = vomid(:)-Sphere_Tet(1:3,iFace1)
			unitV(:) = unitV(:)/Geo3D_Distance(unitV)
			a = Geo3D_Distance(Geo3D_Cross_Product(unitV(:),vnorm(:)))
			b = Geo3D_Dot_Product(unitV(:),vnorm)
            thisAngle = ABS(1.0d0-b)
		
			worseAngle = MAX(worseAngle,thisAngle)
		  END IF
		END DO
		!Faces in ITnb
		DO ifa = 1, 4
		  iFace1 = Next_Tet(ifa,ITnb)
		  IF ((iFace1.NE.IT).AND.(iFace1.GT.0)) THEN
		    pp3(:,1) = Posit(:,IP_Tet(iTri_Tet(1,ifa),ITnb))
			pp3(:,2) = Posit(:,IP_Tet(iTri_Tet(2,ifa),ITnb))
			pp3(:,3) = Posit(:,IP_Tet(iTri_Tet(3,ifa),ITnb))
			vnorm(:) = Geo3D_Cross_Product(pp3(:,1),pp3(:,2),pp3(:,3))
			vnorm(:) = vnorm(:)/Geo3D_Distance(vnorm(:))
			!CALL Get_Tet_SphereCross(iFace1)
			unitV(:) = vomid(:)-Sphere_Tet(1:3,iFace1)
			
			a = Geo3D_Distance(Geo3D_Cross_Product(unitV(:),vnorm(:)))
			b = Geo3D_Dot_Product(unitV(:),vnorm)
			thisAngle =ABS(1.0d0-b)
		   
			worseAngle = MAX(worseAngle,thisAngle)
		  END IF
		END DO
		
		!Merge based on distance
		IF (al<(0.1d0*dist)) THEN
		    flag = 1
		ELSE
		   merged = MIN(merged,(al/dist))
		END IF	 
		
		!..or merge based on projection error
		IF (worseAngle<(0.1d0)) THEN
		    flag =  1
		END IF	 
		
		
		
		
     ENDDO

     IF (flag.EQ.1) THEN
	     merged = -1.0d0
     END IF

END SUBROUTINE isMerged


SUBROUTINE isInside(ie,inside,w)
  !This subroutine checks to see if the element ie contains it's circumcentre
  USE common_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: ie
  INTEGER, INTENT(OUT) :: inside
  REAL*8, INTENT(OUT) :: w
  REAL*8 :: p1t(3),p1(3),p2(3),p3(3),p4(3),vol, eps


  p1(:) = Posit(:,IP_Tet(1,ie))
  p2(:) = Posit(:,IP_Tet(2,ie))
  p3(:) = Posit(:,IP_Tet(3,ie))
  p4(:) = Posit(:,IP_Tet(4,ie))

  p1t = Sphere_Tet(1:3,ie)
  inside = 1

  vol = Geo3D_Tet_Volume6(p1t,p2,p3,p4)

  w = 1.0d0
 
  eps = -1.0d0*epsilon(0d0)

  IF (vol.LT.eps) THEN
    inside = 0
    RETURN
   ! w = MIN(w,ABS(vol))
  END IF

  vol = Geo3D_Tet_Volume6(p1,p1t,p3,p4)
  
  IF (vol.LT.eps) THEN
    inside = 0
    RETURN
   ! w = MIN(w,ABS(vol))
  END IF

  vol = Geo3D_Tet_Volume6(p1,p2,p1t,p4)
 
  IF (vol.LT.eps) THEN
    inside = 0
    RETURN
    ! w = MIN(w,ABS(vol))
  END IF

  vol = Geo3D_Tet_Volume6(p1,p2,p3,p1t)

  IF (vol.LT.eps) THEN
    inside = 0
    RETURN
    ! w = MIN(w,ABS(vol))
  END IF

 ! w = w / Geo3D_Tet_Volume6(p1,p2,p3,p4)




END SUBROUTINE isInside



!*******************************************************************************
!>
!!   This calculates the actual spacing at IT and puts it in Scale_Tet
!!   @param[in]  IT  : the element.
!!   @param[in]  common_Parameters::IP_Tet (:,IT)
!!   @param[in]  common_Parameters::Mapping_Choose_Model
!!   @param[out] common_Parameters::Scale_Tet (IP) for isotropic problems.
!!   @param[out] common_Parameters::fMap_Tet  (IP) for anisotropic problems.
!<     
!*******************************************************************************
SUBROUTINE Get_Actual_Tet_Mapping(IT)

  USE Geometry3DAll
  USE common_Parameters

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: IT
  REAL*8 :: ptet(3,4)

  ptet(:,1) = Posit(:,IP_Tet(1,IT))
  ptet(:,2) = Posit(:,IP_Tet(2,IT))
  ptet(:,3) = Posit(:,IP_Tet(3,IT))
  ptet(:,4) = Posit(:,IP_Tet(4,IT))

  Scale_Tet(IT) = ( Geo3D_Distance(ptet(:,1),ptet(:,2)) + &
            Geo3D_Distance(ptet(:,1),ptet(:,3)) + &
            Geo3D_Distance(ptet(:,1),ptet(:,4)) + &
            Geo3D_Distance(ptet(:,2),ptet(:,3)) + &
            Geo3D_Distance(ptet(:,2),ptet(:,4)) + &
            Geo3D_Distance(ptet(:,3),ptet(:,4)))/6.0d0

  Scale_Tet(IT) = Scale_Tet(IT)/BGSpacing%BasicSize


END SUBROUTINE Get_Actual_Tet_Mapping

SUBROUTINE Get_Actual_Point_Mapping()
  !Works out the mean actual spacing at each point
  USE common_Parameters
  USE PointAssociation
  IMPLICIT NONE

  INTEGER :: IP, ic, ie

  REAL*8  :: scale

  TYPE(PointAssociationArray) :: PointAsso2

  DO ie = 1,NB_Tet
    CALL Get_Actual_Tet_Mapping(ie)
  ENDDO

  CALL PointAssociation_Allocate(NB_Point, PointAsso2)
  CALL PointAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso2)

  DO IP = 1,NB_Point

    scale = 0.0d0

    DO ic = 1,PointAsso2%PointList(IP)%numCells
      ie = PointAsso2%PointList(IP)%CellList(ic)
      scale = scale + Scale_Tet(ie)
    END DO

    Scale_Point(IP) = scale / REAL(PointAsso2%PointList(IP)%numCells)

  ENDDO


END SUBROUTINE Get_Actual_Point_Mapping














