!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  Full CVT
!!  This is full honest old fasioned CVT
!!  Initial implimentation by S. Walton 11/11/1013
!!
!<
!*******************************************************************************
SUBROUTINE Full_CVT(NB1,hull)
	USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE Geometry3DAll
	
	
	
	INTEGER, INTENT(IN)  :: NB1
	INTEGER, INTENT(IN)  :: hull
	
	REAL*8, DIMENSION(:,:), allocatable :: newPosit,oldPosit !To store new positions
	REAL*8, DIMENSION(:), allocatable :: weight !To store the weights
	
	INTEGER :: IT,i,ip,nbp,improve,Old_NB_Point,ic,ie,i1,i2,i3,i4,smtFlag
	REAL*8 :: wTet,obj,lastObj,dmax,dum,conv,residual,dt,dum2
	REAL*8 :: valancy,pC(3)
	REAL*8, DIMENSION(3) :: cTet,p1
	
	
	OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
      READ(84,*) dt
      READ(84,*) conv
	  READ(84,*) dum2
      CLOSE(84)
	
	improve = 1
	smtFlag = 0
	
	conv = epsilon(0d0)
	dt = 0.01d0
	!Make sure mapping and geometry is all up to date
	CALL Get_Tet_SphereCross(0)
	CALL Get_Tet_Volume(0)
	CALL Set_Domain_Mapping()
	CALL Next_Build()
	lastObj = HUGE(0.d0)
	
	
	Old_NB_Point = NB_Point
	IF(Debug_Display>0)THEN
			
		WRITE(*,*)' CVT initial CVT energy:',lastObj
		WRITE(29,*)' CVT initial CVT energy:',lastObj
	END IF
	
	
	ALLOCATE(newPosit(3,NB_Point))
	ALLOCATE(weight(NB_Point))
	ALLOCATE(oldPosit(3,NB_Point))
	improve = 0
	DO WHILE(improve.LT.3)
		
		!Here we want to remove slivers
		IF (hull.NE.1) THEN
		  CALL Swap3D(2)
		  CALL Next_Build()
		  
		
		END IF
		CALL Get_Tet_Circum(0)
		CALL Get_Tet_Volume(0)
	
		
		!Initialise everything
		weight(1:NB_Point) = 0.d0
		newPosit(1:3,1:NB_Point) = 0.d0
		newPosit(:,1:NB1-1) = Posit(:,1:NB1-1)
		!oldPosit(1:3,1:NB_Point) = Posit(1:3,1:NB_Point)
		
		
		
		!Do the CVT and replace Posit
		CALL CVT_Position(newPosit, dmax, NB1,obj,weight)
		
		!IF (obj>2.0d0*lastObj) THEN
		!	improve = 0
		!	EXIT
		!ELSE
		
			!Adam bashford timesteping
			IF (smtFlag.EQ.0) THEN
				DO ip = NB1,NB_Point
					
				   oldPosit(:,ip) = newPosit(:,ip)-Posit(:,ip)
				   Posit(:,ip) = Posit(:,ip) + dt*(oldPosit(:,ip))     
					
				END DO
				smtFlag = 1
			ELSE
				DO ip = NB1,NB_Point
					
				   pC(:) = newPosit(:,ip)-Posit(:,ip)
				   Posit(:,ip) = Posit(:,ip) + (dt/2.0d0)*((3.0d0*pC(:)) - oldPosit(:,ip))
				   oldPosit(:,ip) = pC(:)
						
					
				END DO
			ENDIF
		
			
			
			!Posit(:,NB1:NB_Point) = Posit(:,NB1:NB_Point)+dt*(newPosit(:,NB1:NB_Point)-Posit(:,NB1:NB_Point))
			
			nbp = NB1-1
			IF (hull.EQ.1) THEN
			  CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
			ELSE
			  CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
			  CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
			END IF
			CALL Next_Build()
			CALL Set_Domain_Mapping()
			CALL Get_Tet_SphereCross(0)
			CALL Get_Tet_Volume(0)
			Mark_Point(1:NB_Point) = 0
			CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10) 
			usePower = .FALSE.
			CALL Remove_Point()
			!CALL Next_Build()
			CALL Set_Domain_Mapping()
			!CALL Get_Tet_SphereCross(0)
			!CALL Get_Tet_Volume(0)
			Mark_Point(1:NB_Point) = 0
			
			CALL Check_Mesh_Geometry2('CVT')
			
			residual = ABS(1.d0 - (lastObj/obj))
			
			IF(Debug_Display>0)THEN
				
				WRITE(*,*)' CVT energy and residual:',obj,residual
				WRITE(29,*)'  CVT energy and residual:',obj,residual
			END IF
			
			improve = improve + 1
			
			
			IF (residual>(conv)) THEN
				
				
				
					lastObj = obj
					
				
				
			ELSE
				
				improve = 0
				EXIT
			ENDIF
		
		!END IF
	
	ENDDO

END SUBROUTINE Full_CVT


SUBROUTINE CVT_Source()
	USE common_Parameters
    USE array_allocator
    USE SpacingStorage
    USE kdtree2_precision_module
    USE kdtree2_module
    USE Geometry3DAll
    USE SpacingStorageGen
    USE Source_Type
    USE TetMeshStorage
    USE m_mrgrnk
  
    IMPLICIT NONE

	REAL*8 :: delta,zini,xini,yini
	REAL*8 :: a,b,h,x,y,z,x0
	REAL*8 :: lastObj,residual,dt,conv,nowObj
	REAL*8, ALLOCATABLE :: PositNm1(:,:)
	INTEGER :: Nx,Ny,Nz,i,j,k,firstNode,IT,improve,smtFlag
	
	 OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
      READ(84,*) dt
      READ(84,*) conv
	  CLOSE(84)
	
	!This is just a lazy test routine at the moment
	!Just does the first source
	!Restore the blank mesh
	CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
	CALL TetMeshStorage_NodeRestore(NB_Point, Posit, HullBackUp)
	
	CALL Next_Build()
	CALL Get_Tet_Circum(0)
	CALL Get_Tet_Volume(0)
	CALL Set_Domain_Mapping()
	firstNode = NB_Point + 1
	
	!Create a box of ideal points in the influence of the source
	delta = BGSpacing%Sources%Pt(1)%Space
	
	
	a = delta 
    b = delta/2.0d0 
    h = delta/sqrt(2.0d0)

	Nx = 10
	Ny = 10
	Nz = 10
	
	xini = BGSpacing%Sources%Pt(1)%Posit(1) - 5.0d0*delta
	yini = BGSpacing%Sources%Pt(1)%Posit(2) - 5.0d0*delta
	zini = BGSpacing%Sources%Pt(1)%Posit(3) - 5.0d0*delta
	
	 WRITE(*,*)'Nx:',Nx
	
    do k=0,Nz
	  z = zini + h*k
	  do j=0,Ny
		  y = yini + h*j
		if (mod(j,2).eq.mod(k,2)) then 
		   x0 = xini
		else 
		   x0 = xini-b
		endif
		do i=0,Nx
		
		  NB_Point = NB_Point + 1
		  IF(NB_Point>nallc_point) THEN 
			 CALL ReAllocate_Point()
		  END IF
		
		   
		   x = x0 + delta*i
		   Posit(1,NB_Point) = x
		   Posit(2,NB_Point) = y
		   Posit(3,NB_Point) = z
		   Scale_Point(NB_Point) = delta/BGSpacing%BasicSize
		   
		enddo   !: do i=0,Nx
	 enddo !: do j=0,Ny
    enddo !: do k=0,Nz 

	Mark_Point(1:NB_Point) = 0
	CALL Insert_Points_EasyPeasy(firstNode,NB_Point, 5)
    CALL Remove_Point()
    Mark_Point(1:NB_Point)=0   
	
    Mark_Tet(1:NB_Tet) = 0
    DO IT = 1,NB_Tet
		IF((IP_Tet(1,IT)<firstNode))THEN
			IP_Tet(4,IT) = -99
		END IF
		IF((IP_Tet(2,IT)<firstNode))THEN
			IP_Tet(4,IT) = -99
		END IF
		IF((IP_Tet(3,IT)<firstNode))THEN
			IP_Tet(4,IT) = -99
		END IF
		IF((IP_Tet(4,IT)<firstNode))THEN
			IP_Tet(4,IT) = -99
		END IF
    END DO
    CALL Remove_Tet()
	
	MeshOut%GridOrder = GridOrder
    MeshOut%NB_Point  = NB_Point
    MeshOut%NB_Tet    = NB_Tet
    ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
    ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
    MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
    MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)
    CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_b4',JobNameLength+3, -15, MeshOut)  
    DEALLOCATE(MeshOut%IP_Tet)
    DEALLOCATE(MeshOut%Posit)
	RR_Point(1:NB_Point) = 0.d0
    CALL Get_Tet_SphereCross(0)
    CALL Check_Mesh_Geometry2('b4')
	
	CALL Set_Domain_Mapping()
	lastObj = HUGE(0.d0)                 
	 improve = 1
	 smtFlag = 0
	 allocate(PositNm1(3,NB_Point))
	 
	 OPEN(1998,file='centroidenergy.out',status='unknown')
	 
			 WRITE(*,*)'Time Step:',dt,'Convergence:',conv*dt

	DO WHILE(improve.EQ.1)
	  CALL Centroid_Smooth_Layer(firstNode,firstNode,nowObj,PositNm1,smtFlag,dt)
	  
	  CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
	  CALL Next_Build()
	  CALL Get_Tet_Circum(0)
	  CALL Get_Tet_Volume(0)
	  Mark_Point(1:NB_Point)=0 
	  !CALL Set_Domain_Mapping()
	  CALL Insert_Points_EasyPeasy(firstNode,NB_Point, 5)
	  CALL Remove_Point()
	  Mark_Point(1:NB_Point)=0   

	  DO IT = 1,NB_Tet
		IF((IP_Tet(1,IT)<firstNode))THEN
			IP_Tet(4,IT) = -99
		END IF
		IF((IP_Tet(2,IT)<firstNode))THEN
			IP_Tet(4,IT) = -99
		END IF
		IF((IP_Tet(3,IT)<firstNode))THEN
			IP_Tet(4,IT) = -99
		END IF
		IF((IP_Tet(4,IT)<firstNode))THEN
			IP_Tet(4,IT) = -99
		END IF
		
      END DO
    CALL Remove_Tet()
	  
	  
	  residual = ABS(1.d0 - (lastObj/nowObj))
	  
	  IF(Debug_Display>0)THEN
		WRITE(1998,*) nowObj, residual
		WRITE(*,*)'Centroid smooth energy, residual:',nowObj,residual
		WRITE(29,*)'Centroid smooth energy, residual:',nowObj,residual
	  END IF
	  
	  
	  IF ((residual.LT.(conv*dt)).OR.(nowObj.LT.(conv*dt))) THEN
		improve = 0
	  ELSE
		lastObj = nowObj
	  END IF

	  
	ENDDO   

	
	CLOSE(1998)
	deallocate(PositNm1)
	
	 Mark_Tet(1:NB_Tet) = 0
    DO IT = 1,NB_Tet
		IF((IP_Tet(1,IT)<firstNode))THEN
			IP_Tet(4,IT) = -99
		END IF
		IF((IP_Tet(2,IT)<firstNode))THEN
			IP_Tet(4,IT) = -99
		END IF
		IF((IP_Tet(3,IT)<firstNode))THEN
			IP_Tet(4,IT) = -99
		END IF
		IF((IP_Tet(4,IT)<firstNode))THEN
			IP_Tet(4,IT) = -99
		END IF
    END DO
    CALL Remove_Tet()
	
	MeshOut%GridOrder = GridOrder
    MeshOut%NB_Point  = NB_Point
    MeshOut%NB_Tet    = NB_Tet
    ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
    ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
    MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
    MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)
    CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_af',JobNameLength+3, -15, MeshOut)  
    DEALLOCATE(MeshOut%IP_Tet)
    DEALLOCATE(MeshOut%Posit)
	RR_Point(1:NB_Point) = 0.d0
    CALL Get_Tet_SphereCross(0)
    CALL Check_Mesh_Geometry2('after')
	
END SUBROUTINE














