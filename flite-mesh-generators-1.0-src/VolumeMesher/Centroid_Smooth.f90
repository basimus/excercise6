!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  Centroid Smooth
!!  This routine performs smoothing based on the ideal distance from a centroid from point
!!  This required that the boundary triangulation was stored in CVTMeshInit
!!  NB1 is the first node which effected by the process
!!  Initial implimentation by S. Walton 15/01/2014
!!
!<
!*******************************************************************************
SUBROUTINE Centroid_Smooth(NB1)
	USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE Geometry3DAll
	
	
	
	INTEGER, INTENT(IN)  :: NB1
	
	
	REAL*8, DIMENSION(:,:), allocatable :: newPosit!,oldPosit !To store new positions
	REAL*8, DIMENSION(:), allocatable :: weight !To store the weights
	INTEGER :: IT,i,ip,nbp,improve,Old_NB_Point
	REAL*8 :: wTet,obj,lastObj,sqrt5o4,delta,length,quality,residual,fMap(3,3)
	REAL*8, DIMENSION(3) :: cTet,p1,pC
	REAL*8 :: dt,conv,dum2
	
	
	OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
    READ(84,*) dt
    READ(84,*) conv
	READ(84,*) dum2
    CLOSE(84)
	
	conv = conv*dt
	
	improve = 1
	
	!Make sure mapping and geometry is all up to date
	CALL Get_Tet_SphereCross(0)
	CALL Get_Tet_Volume(0)
	CALL Set_Domain_Mapping()
	CALL Next_Build()
	lastObj = 0
	
	sqrt5o4 = sqrt(5.d0)/4.d0
	
	
	DO IT=1,NB_Tet
	
		p1(:) = ( Posit(:,IP_Tet(1,IT)) + Posit(:,IP_Tet(2,IT))    &
								+ Posit(:,IP_Tet(3,IT)) + Posit(:,IP_Tet(4,IT)) ) / 4.d0
	
		lastObj = lastObj + Geo3D_Distance_SQ(Sphere_Tet(1:3,IT),p1(:))
	
	ENDDO
	
	Old_NB_Point = NB_Point
	IF(Debug_Display>0)THEN
			
		WRITE(*,*)'Centroid Smooth energy:',lastObj
		WRITE(29,*)'Centroid Smooth energy:',lastObj
	END IF
	
	
	ALLOCATE(newPosit(3,NB_Point))
	ALLOCATE(weight(NB_Point))
	
	
	DO WHILE(improve.EQ.1)
		
		!Initialise everything
		weight(1:NB_Point) = 0.d0
		newPosit(1:3,1:NB_Point) = 0.d0
		newPosit(:,1:NB1-1) = Posit(:,1:NB1-1)
		
		
		
		
		!Now loop over elements
		DO IT=1,NB_Tet
			
			pC(:) = ( Posit(:,IP_Tet(1,IT)) + Posit(:,IP_Tet(2,IT))    &
								+ Posit(:,IP_Tet(3,IT)) + Posit(:,IP_Tet(4,IT)) ) / 4.d0
			
			
			
			
								
			
			DO i=1,4
				ip = IP_Tet(i,IT)
				IF (ip>(NB1-1)) THEN
					p1(:) = Posit(:,ip)
					length = Geo3D_Distance(pC,p1)
					
					IF (length>0.d0) THEN
					  delta = Scale_Point(IP_Tet(i,IT))*BGSpacing%BasicSize*sqrt5o4	 
					  quality = (delta-length)*(delta-length)
					
					
					
					  newPosit(:,ip) = newPosit(:,ip) + quality*( pC(:) + &
								     (delta/length)*(Posit(:,ip)-pC(:)))
				
					  weight(ip) = weight(ip) + quality
					ENDIF
		
				END IF
			END DO
		
		END DO
		
		!Now loop over points and calculate weighted centroid
		DO ip = NB1,NB_Point
			IF (weight(ip)>0.d0) THEN
			  p1(:) = (newPosit(:,ip)/weight(ip)) - Posit(:,ip)
			  Posit(:,ip) = Posit(:,ip) + dt*p1(:)
			ENDIF
		END DO
		
		nbp = NB1-1
		CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
		CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
		CALL Next_Build()
		CALL Set_Domain_Mapping()
		CALL Get_Tet_SphereCross(0)
		CALL Get_Tet_Volume(0)
		Mark_Point(1:NB_Point) = 0
		CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10) 
		usePower = .FALSE.
		CALL Remove_Point()
		!CALL Element_Collapse( )
               !CALL Swap3D(2)
		CALL Next_Build()
		CALL Set_Domain_Mapping()
		CALL Get_Tet_SphereCross(0)
		CALL Get_Tet_Volume(0)
		Mark_Point(1:NB_Point) = 0
		
		obj = 0
	
		DO IT=1,NB_Tet
		
			p1(:) = ( Posit(:,IP_Tet(1,IT)) + Posit(:,IP_Tet(2,IT))    &
								+ Posit(:,IP_Tet(3,IT)) + Posit(:,IP_Tet(4,IT)) ) / 4.d0
	
			obj = obj + Geo3D_Distance_SQ(Sphere_Tet(1:3,IT),p1(:))
		
		ENDDO
		
		
		residual = ABS(1.d0 - (lastObj/obj))
		
		IF(Debug_Display>0)THEN
			
			WRITE(*,*)'Centroid smooth energy, residual:',obj,residual
			WRITE(29,*)'Centroid smooth energy, residual:',obj,residual
		END IF
		
		IF (residual<conv) THEN
		
			improve = 0
			
			
		ELSE
		
			IF(Old_NB_Point.EQ.NB_Point)THEN
				lastObj = obj
			!	oldPosit(:,1:NB_Point) = Posit(:,1:NB_Point)
			ELSE
			
				DEALLOCATE(newPosit)
				DEALLOCATE(weight)
				
				Old_NB_Point = NB_Point
				lastObj = obj
			!	oldPosit(:,1:NB_Point) = Posit(:,1:NB_Point)
				ALLOCATE(newPosit(3,NB_Point))
				ALLOCATE(weight(NB_Point))
				
				
			ENDIF
			
		END IF
		
		
		
		
	
	ENDDO

END SUBROUTINE Centroid_Smooth

















