!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!          Function referred by Optimisation Processes.
!!
!!    @param[in]  OptParameter%Ndim       the number of dimensions.
!!    @param[in]  OptVariable%F           the previous solution.
!!    @param[in]  OptParameter%Iter       =0 first call, set OptVariable%F.
!!    @param[in]  P (OptParameter%Ndim)   the position.
!!    @return     F                       the solution respect of P.
!!    @param[out]   OptVariable%getBetter =-1 the P is out of range.      \n
!!                                        = 0 the new solution is NOT better than the previous one.
!!                                        = 1 the new solution is     better than the previous one.
!!                                        = 2 the new solution achieves the target (if set).
!!    @param[out]  OptVariable%F          updated to the new solution if it is better.
!<
!*******************************************************************************
SUBROUTINE Optimising_GetValue (PP, F)

  USE common_Parameters
  USE Geometry3DAll
  USE OptimisingModule
  IMPLICIT NONE

  REAL*8, DIMENSION(OptParameter%Ndim),  INTENT(IN) :: PP
  REAL*8, INTENT(OUT) :: F
  INTEGER :: n, IT, i, IP, ib, ntype, j, K
  REAL*8  :: Fbig, Ftarget, vol, pps(3,4), dd, p1(3)
  REAL*8  :: Psave(3), Rsave
  REAL*8  :: pp2(3,2), pp3(3,3), tt, tts, w(4)
  REAL*8 , PARAMETER :: PI = 3.141592653589793D0
  REAL*8, DIMENSION(OptParameter%Ndim) :: ppt


  IF(OptVariable%IdxFun==1)THEN
     !--- for Recover_CleanFace

     IF(OptParameter%Iter==0) OptVariable%F = 0.d0

     IP = OptVariable%IdNode
     CALL LinkAssociation_List(IP, List_Length, ITs_List, PointAsso)

     F = 1.e30
     DO n = 1,List_Length
        IT  = ITs_List(n)
        pps(:,1:4) = Posit(:,IP_Tet(1:4,IT))
        DO i=1,4
           IF(IP_Tet(i,IT)==IP) THEN
              pps(1:3,i) = pp(1:3)
           ENDIF
        ENDDO
        vol = Geo3D_Tet_volume(pps(:,1), pps(:,2), pps(:,3), pps(:,4))
        F   = MIN (F,vol)
        IF(vol<=OptVariable%F)THEN
           OptVariable%getBetter = 0
           RETURN
        ENDIF
     ENDDO

     OptVariable%getBetter = 1
     OptVariable%F         = F

     ELSE IF(OptVariable%IdxFun==11)THEN
        !--- for Smooth

        IF(OptParameter%Iter==0) OptVariable%F = 0.d0

        IP = OptVariable%IdNode
        Posit(:,IP) = pp(1:3)

        IF(BGSpacing%Model<-1 .OR. BGSpacing%Model>1)THEN
           CALL Get_Point_Mapping(IP)
        ENDIF
        F = 1.e30
        DO n=1,List_Length
           IT       = ITs_List(n)
           IF(BGSpacing%Model<-1 .OR. BGSpacing%Model>1)THEN
              CALL Get_Tet_Mapping(IT)
           ENDIF

           CALL Get_Tet_Volume(IT)
           vol = Volume_Tet(IT)
           IF(vol<=OptVariable%F)THEN
              OptVariable%getBetter = 0
              RETURN
           ENDIF
           F   = MIN (F,vol)
        ENDDO

        OptVariable%getBetter = 1
        OptVariable%F         = F

     ELSE IF(OptVariable%IdxFun==3 .OR. OptVariable%IdxFun==4)THEN
        !--- for VisLayer

        P1 = Plane3D_get3DPosition (OptPlane, PP)
        P1 = P1 - VM%Posit(:,OptVariable%IdNode)
        dd = Geo3D_Distance(P1)
        IF(dd>3.D3 * OptParameter%DTol)THEN
           OptVariable%getBetter = -1
           RETURN
        ENDIF
        P1 = P1 / dd

        F = 1.e30
        DO n = 1,List_Length
           ib  = Its_List(n)
           vol = Geo3D_Dot_Product(SurfTri(ib)%anor, P1)
           F   = MIN (F,vol)
           IF(OptParameter%Iter>0 .AND. vol<=OptVariable%F)THEN
              OptVariable%getBetter = 0
              RETURN
           ENDIF
        ENDDO

        OptVariable%getBetter = 1
        OptVariable%F         = F

     ELSE IF(OptVariable%IdxFun==5)THEN
        !--- for VisLayer

        IF(PP(1)>PI .OR. PP(1)<0)THEN
           OptVariable%getBetter = -1
           RETURN
        ENDIF

        P1 = COS(PP(1)) * OptDirct1(:) + SIN(PP(1)) * OptDirct2(:)

        F = 1.e30
        DO n = 1,List_Length
           ib  = Its_List(n)
           vol = Geo3D_Dot_Product(SurfTri(ib)%anor, P1)
           F   = MIN (F,vol)
           IF(OptParameter%Iter>0 .AND. vol<=OptVariable%F)THEN
              OptVariable%getBetter = 0
              RETURN
           ENDIF
        ENDDO

        OptVariable%getBetter = 1
        OptVariable%F         = F

     ELSE IF(OptVariable%IdxFun==2 .AND. ABS(CVT_METHOD)==1)THEN
        !--- for SmoothVor

        Fbig    = 1.D30
        Ftarget = 1.D-12 * (BGSpacing%MinSize)**4
        IF(OptParameter%Iter==0) OptVariable%F = Fbig
	!R = radius at each point used to calculate Voronoi - SPW
        IP   = OptVariable%IdNode
        Psave(1:3) = Posit(:,IP)
        Rsave      = RR_Point(IP)
        IF(OptParameter%Ndim==1)THEN
           RR_Point(IP) = PP(1) * (10*BGSpacing%BasicSize)
        ELSE IF(OptParameter%Ndim==3)THEN
           Posit(:,IP)  = PP(1:3)
        ELSE IF(OptParameter%Ndim==4)THEN
           Posit(:,IP)  = PP(1:3)
           RR_Point(IP) = PP(4) * (10*BGSpacing%BasicSize)
        ENDIF

        CALL LinkAssociation_List(IP, List_Length, ITs_List, PointAsso)

        OptVariable%getBetter = 1
        F = 0.d0
        DO n=1,List_Length
           IT = ITs_List(n)
           CALL Get_Tet_Volume(IT)
           IF(Volume_Tet(IT)<=0)THEN
              OptVariable%getBetter = -1
              F = Fbig
              EXIT
           ENDIF
           CALL Get_Tet_SphereCross(IT) !Upadates center of IT in Sphere_Tet - SPW
           p1(:) = ( Posit(:,IP_Tet(1,IT)) + Posit(:,IP_Tet(2,IT))    &
                + Posit(:,IP_Tet(3,IT)) + Posit(:,IP_Tet(4,IT)) ) / 4.d0
           dd = Geo3D_Distance_SQ(p1, Sphere_Tet(1:3,IT))
           F = F + dd*dd
           IF(F>OptVariable%F)THEN
              OptVariable%getBetter = 0
              IF(OptParameter%Method/=3) EXIT
           ENDIF
        ENDDO

        IF(OptVariable%getBetter == 1) OptVariable%F = F
        IF(OptVariable%F<=Ftarget) OptVariable%getBetter = 2

        Posit(:,IP)  = Psave(1:3)
        RR_Point(IP) = Rsave   

     ELSE IF(OptVariable%IdxFun==2)THEN
        !--- for SmoothVor
        !--- other thoughts for OptVariable%IdxFun==2

        Fbig    = 1.D30
        Ftarget = 1.D-6
        IF(OptParameter%Iter==0) OptVariable%F = Fbig

        IP   = OptVariable%IdNode
        Psave(1:3) = Posit(:,IP)
        Rsave      = RR_Point(IP)
        IF(OptParameter%Ndim==1)THEN
           RR_Point(IP) = PP(1) * (10*BGSpacing%BasicSize)
        ELSE IF(OptParameter%Ndim==3)THEN
           Posit(:,IP)  = PP(1:3)
        ELSE IF(OptParameter%Ndim==4)THEN
           Posit(:,IP)  = PP(1:3)
           RR_Point(IP) = PP(4) * (10*BGSpacing%BasicSize)
        ENDIF

        CALL LinkAssociation_List(IP, List_Length, ITs_List, PointAsso)

        OptVariable%getBetter = 1
        F = 0.d0
        DO n=1,List_Length
           IT = ITs_List(n)
           CALL Get_Tet_Volume(IT)
           IF(Volume_Tet(IT)<=0)THEN
              OptVariable%getBetter = -1
              F = Fbig
              EXIT
           ENDIF
           CALL Get_Tet_SphereCross(IT)
           pps(:,1:4) = Posit(:,IP_Tet(1:4,IT))
           K = 0
           W = Geo3D_Tet_Weight (K, pps, Sphere_Tet(1:3,IT))
           IF(k==1) CYCLE         !--- p1 inside to the tet.
           dd = 0
           DO i = 1,6
              pp2(:,1:2) = pps(:,I_Comb_Tet(1:2,i))
              dd = dd + Geo3D_Distance_SQ(pp2(:,1), pp2(:,2))
           ENDDO
           p1(:) = (pps(:,1) + pps(:,2) + pps(:,3) + pps(:,4)) / 4.d0
           F = F + Geo3D_Distance_SQ(p1, Sphere_Tet(1:3,IT)) / dd
           IF(F>OptVariable%F)THEN
              OptVariable%getBetter = 0
              IF(OptParameter%Method/=3) EXIT
           ENDIF
        ENDDO

        IF(OptVariable%getBetter == 1) OptVariable%F = F
        IF(OptVariable%F<=Ftarget) OptVariable%getBetter = 2

        Posit(:,IP)  = Psave(1:3)
        RR_Point(IP) = Rsave   

     ELSE IF(OptVariable%IdxFun==-2)THEN
        !--- for SmoothVor
        !--- other thoughts for OptVariable%IdxFun==2

        Fbig    = 1.D30
        Ftarget = 1.D-6
        IF(OptParameter%Iter==0) OptVariable%F = Fbig

        ppt(:) = PP(:)
        tts = SUM(ppt(1:OptParameter%Ndim))
        ppt(:) = ppt(:) + (1.d0-tts)/OptParameter%Ndim     

        DO i = 1, NB_PODva
           ip = Mark_Point(i)
           RR_Point(ip) = 0
           DO n = 1, OptParameter%Ndim
              RR_Point(IP) = RR_Point(IP) + ppt(n)* RRS_POD(IP, n)
           ENDDO
        ENDDO

        OptVariable%getBetter = 1
        F = 0.d0
        DO IT = 1, NB_Tet
           CALL Get_Tet_SphereCross(IT)
           pps(:,1:4) = Posit(:,IP_Tet(1:4,IT))
           K = 0
           W = Geo3D_Tet_Weight (K, pps, Sphere_Tet(1:3,IT))
           IF(k==1) CYCLE         !--- p1 inside to the tet.
           dd = 0
           DO i = 1,6
              pp2(:,1:2) = pps(:,I_Comb_Tet(1:2,i))
              dd = dd + Geo3D_Distance_SQ(pp2(:,1), pp2(:,2))
           ENDDO
           p1(:) = (pps(:,1) + pps(:,2) + pps(:,3) + pps(:,4)) / 4.d0
           F = F + Geo3D_Distance_SQ(p1, Sphere_Tet(1:3,IT)) / dd
           IF(F>OptVariable%F)THEN
              OptVariable%getBetter = 0
              IF(OptParameter%Method/=3) EXIT
           ENDIF
        ENDDO

        IF(OptVariable%getBetter == 1) OptVariable%F = F
        IF(OptVariable%F<=Ftarget) OptVariable%getBetter = 2

     ENDIF


     RETURN
   END SUBROUTINE Optimising_GetValue



   !*******************************************************************************
   !>
   !!          Function referred by Optimisation Processes.
   !!
   !!    @param[in]  OptParameter%Ndim           the number of dimensions.
   !!    @param[in]  P1,P2 (OptParameter%Ndim)   the positions of two particles.
   !!    @return     D                           the distance between them.
   !!                                            It would be referred for convergence judging.
   !<
   !*******************************************************************************
   SUBROUTINE Optimising_Distance (P1, P2, D)

     USE common_Parameters
     USE Geometry3DAll
     USE OptimisingModule
     IMPLICIT NONE

     REAL*8, DIMENSION(OptParameter%Ndim), INTENT(IN) :: P1, P2
     REAL*8, INTENT(OUT)   :: D

     !IF(OptVariable%IdxFun==1)THEN
     !--- for CVT RR
     !--- for Recover_CleanFace
     !--- for VisLayer

     D = SUM(ABS(P1(1:OptParameter%Ndim) - P2(1:OptParameter%Ndim))) 
     !ENDIF

     RETURN
   END SUBROUTINE Optimising_Distance



