c
c     cadparent contains the parent entity of each node
c     This can contain geometry points, curves and faces
c     For a point, the number is the point number + PARENT_POINT
c     For a curve, the number is the curve number + PARENT_CURVE
c     For a surface, the number is the surface number + PARENT_SURFACE
c
      integer PARENT_POINT, PARENT_CURVE, PARENT_SURFACE
      parameter ( PARENT_POINT = 0 )
      parameter ( PARENT_CURVE = 1000000 )
      parameter ( PARENT_SURFACE = 2000000 )
      common/CADfixMode/ clientMode, CFIVersion
      logical clientMode
      real*4   CFIVersion
