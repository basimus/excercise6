!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  MCS_Optim_Gen
!!  
!!  This routine uses MCS to optimise positions and weights of nodes in the mesh
!!
!!  The objective is to match the centroid/node distance to ideal
!!
!!  Initial implimentation by S. Walton 29/10/1013
!!
!<
!*******************************************************************************
SUBROUTINE MCS_Optim_Gen(NB1)
	USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	
	INTEGER, INTENT(IN)  :: NB1  !The first point effected by this process
	INTEGER :: K,ie,ip,DD,N,Ibest,I,Loop,Istatus,Iin,Ni,Di,idum,isFinished,numLoops,ic,flag,nbp,improve,NB_old,MaxL,L,j,ii
	REAL*8, allocatable  :: optx(:), optl(:), optu(:), optg(:) ,ptemp(:),Pt(:,:),Qu(:),vardef(:,:),PtTemp(:,:)
	REAL*8 :: dd2,ftemp,fbest,ranNum,d,FTarget,FBig,p1(3),dtet,psave(3),rsave,vol,ptet(3,4),p2(3),p3(3),p4(3)
	REAL*8 :: pp4(3,4),dum(4),coef,sumObj,lastObj,p1t(3),p2t(3),residual,delta2,dist,sqrt5o4
	REAL*8 :: delta,length,pC(3),pCirc(3)
	INTEGER :: ITnb,merged,Isucc,is,j1,j2,j3,j4, order(NB_Point),iRan,jRan,tRan
	REAL*8 :: dum1,conv,dum2
	REAL*8 :: minl, maxll, templ, aspectRatio, penalty
	REAL*8 :: dimi,diFact,targDi, sqrt1o2, oneOverSqrt34,sqrt2o3,randn,noEdge
	INTEGER :: iFace, iF1, iF2, iF3, iPf
	TYPE(Plane3D) :: aPlane
	OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
      READ(84,*) dum1
      READ(84,*) conv
	  READ(84,*) dum2
      CLOSE(84)
	
	
	sqrt2o3 = SQRT(2.0d0/3.0d0)
	oneOverSqrt34 = 1.0d0/(SQRT(3.0d0/4.0d0))
	sqrt1o2 = 1.0d0/(sqrt(2.0d0))
	
	lastObj = HUGE(0.d0)
	
	sqrt5o4 = sqrt(5.d0)/4.d0
	targDi = 12.0d0
	diFact = 1.d0/(targDi)
	
	improve = 1
	MaxL = 100
	L = 0
	
	!Get link info
		CALL Next_Build()
		!Make sure mapping and geometry is all up to date
		CALL Get_Tet_SphereCross(0)
		CALL Get_Tet_Volume(0)
		CALL Set_Domain_Mapping()
	
	
	DD = 3  !Number of dimensions - one for each space, going to scale by spacing
	N = 30  !Number of eggs
		
	allocate(ptemp(DD),Pt(DD,N),Qu(N),optx(DD), optl(DD), optu(DD), optg(DD),vardef(DD,2),PtTemp(N-1,DD))
	
	DO WHILE((improve.EQ.1).AND.(L<MaxL))
		
		FTarget = (1.D-6)*BGSpacing%MinSize
		FBig = 1.D30
		idum = -1
		sumObj = 0.d0
		
		!Here we want to remove slivers
		CALL Swap3D(2)
		CALL Next_Build()
		CALL Get_Tet_Circum(0)
		CALL Get_Tet_Volume(0)
		
		
		CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
		
		!Set up the optimisation problem
		CALL init_random_seed()
		numLoops = 15 !Number of MCS loops
		
		!allocate(allPsave(3,NB_Point))
		!allPsave(:,1:NB_Point) = Posit(:,1:NB_Point)
		NB_old = NB_Point
		
		
		!An order array to help shuffle
		DO iRan = 1,NB_Point
			order(iRan) = iRan
		ENDDO
		!Shuffle the order
		DO iRan = NB_Point,1,-1
			CALL RANDOM_NUMBER(randn)
		    jRan = iRan * randn + 1
			tRan = order(iRan)
			order(iRan) = order(jRan)
			order(jRan) = tRan
		ENDDO
		
		!First node positions
		DO iRan = 1,NB_Point
		
			IF (order(iRan).GT.(NB1-1)) THEN
			ip = order(iRan)
			
			!Get connected elements
			CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
		
			!Recalculate spacing at these elements and find actual local spacing
			d = 0.0d0
			noEdge = 0.0d0
			DO ic = 1,List_Length
				CALL Get_Tet_Mapping(ITs_List(ic))
				ie = ITs_List(ic)
				DO i = 1,4
					IF (ip.NE.IP_Tet(i,ie)) THEN
						d = d + Geo3D_Distance(Posit(:,ip),Posit(:,IP_Tet(i,ie)))
						noEdge = noEdge + 1.0d0
					END IF
				ENDDO
			ENDDO
			
			d = d/noEdge
			
			ptemp(1:DD) = 0.0d0
			Ibest = 0
			ftemp = 0.0d0
			I = 0
			Loop = 0
			Istatus = 0
			Iin = 0
			Qu(1:N) = huge(0.0)
			fbest = huge(0.0)

		
			!Upper and lower bounds are going to be 0.5*d
			optl(1:DD) = -0.25d0
			optu(1:DD) = 0.25d0
			
			
			!The last egg will be the initial position
			DO Ni = 1,N
				DO Di = 1,DD
					CALL RANDOM_NUMBER(randn)
					Pt(Di,Ni) = optl(Di) + randn*(optu(Di)-optl(Di))
				
				
				ENDDO
				
			ENDDO
			
			Pt(1:DD,1) = 0.0d0
			
		
			isFinished = 0
			
			psave(:) = Posit(:,ip)
		!	rsave = RR_Point(ip)
			idum = -1
			DO WHILE (isFinished==0)
			
				
			
				CALL Optimising_Cuckoo_Driver(DD,N,ptemp,Ibest,ftemp,Pt,I,Loop,Istatus,Iin,Qu,fbest,optu,optl,idum)
				
				IF ((Loop-1).LT.numLoops) THEN
				
					!Calculate fitness at ptemp and put it into ftemp
					Posit(:,ip) = psave(:) + d*ptemp(1:3)
					
					!ftemp = ptemp(4)*ptemp(4)
					ftemp = 0.0d0
					flag = 0
					
					DO ic = 1,List_Length
					
						ie = ITs_List(ic)
                        p1 = Posit(:,IP_Tet(1,ie))
						p2 = Posit(:,IP_Tet(2,ie))
						p3 = Posit(:,IP_Tet(3,ie))
						p4 = Posit(:,IP_Tet(4,ie))
						
						delta2  = (( Geo3D_Distance(p1,p2) + &
									Geo3D_Distance(p1,p3) + &
									Geo3D_Distance(p1,p4) + &
									Geo3D_Distance(p2,p3) + &
									Geo3D_Distance(p2,p4) + &
									Geo3D_Distance(p3,p4))/6.0d0)
						
						delta2 = delta2*sqrt5o4
						
						
						
						!CALL Get_Tet_SphereCross(ie)
						!pCirc(1:3) = Sphere_Tet(1:3,ie)
						pC(:) = (( p1 + p2 + p3 + p4 ) / 4.d0)
						
						
						!Set the coef to the circumcenter centroid distance
						!ftemp = Geo3D_Distance_SQ(pCirc,pC) / (delta2*delta2)
						
						DO ii = 1,4
							IF (IP_Tet(ii,ie)<=NB_BD_Point) THEN
								coef = 10.0d0
							ELSE
								coef = 1.0d0
							END IF
							dum1 = dum1 + coef
							p1(:) = Posit(:,IP_Tet(ii,ie))
							length = Geo3D_Distance(pC,p1)												
							ftemp = ftemp + coef*((((delta2-length)*(delta2-length))/(delta2*delta2)))
							
						ENDDO
						
						
						
						!dimi = Geo3D_Tet_Quality(p1,p2,p3,p4,2)
						!dimi = dimi * 180.d0 / PI
						!IF (dimi.LT.targDi) THEN
						!	penalty = (targDi - dimi)/targDi
						!ELSE
						!	penalty = 0.0d0
						!END IF
						!ftemp = ftemp +  penalty  
							

							
							
					
					
					ENDDO
					
					
					IF (flag.EQ.1) THEN
						ftemp = huge(0.d0)
					ENDIF
					IF (ftemp==0) THEN
						isFinished = 1
						EXIT
					ENDIF
					
				ELSE
				
					!The best result is in Pt(:,Ibest)
					Posit(:,ip) = psave(:) + d*Pt(1:3,Ibest)
					CALL Get_Point_Mapping(ip)
					sumObj = sumObj + fbest
					isFinished = 1
					EXIT
				ENDIF
		
			ENDDO
			
			END IF
		
		ENDDO
		

		
		!Reinsert
		nbp = NB1-1
		
		CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
		CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
		CALL Next_Build()
		CALL Set_Domain_Mapping()
		CALL Get_Tet_SphereCross(0)
		CALL Get_Tet_Volume(0)
		Mark_Point(1:NB_Point) = 0
		CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
		usePower = .FALSE.
		CALL Remove_Point()        
		CALL Next_Build()
		CALL Set_Domain_Mapping()
        CALL Get_Tet_SphereCross(0)
        CALL Get_Tet_Volume(0)



		residual = ABS(1.d0 - (lastObj/sumObj))
		IF(Debug_Display>0)THEN
			
			WRITE(*,*)'MCS Optim Objective funtion value:',sumObj,residual
			WRITE(29,*)'MCS Optim Objective funtion value:',sumObj,residual
		END IF
		
		IF (residual>conv) THEN
			lastObj = sumObj
			!deallocate(allPsave)
			L = L+1
			
		ELSE
		!	nbp = NB1-1
		!	CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
		!	CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
			
		!	Posit(:,1:NB_Old) = allPsave(:,1:NB_Old)
         !               NB_Point = NB_Old

		!	CALL Next_Build()
		!	CALL Set_Domain_Mapping()
		!	CALL Get_Tet_SphereCross(0)
		!	CALL Get_Tet_Volume(0)
		!	Mark_Point(1:NB_Point) = 0
		!	CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
		!	usePower = .FALSE.
		!	CALL Remove_Point()
		!	CALL Next_Build()
         !                CALL Get_Tet_SphereCross(0)
         !       CALL Get_Tet_Volume(0)


			improve = 0
			EXIT
		
		ENDIF

	
	ENDDO

	deallocate(ptemp,Pt,Qu,optx, optl, optu, optg,vardef,PtTemp)

END SUBROUTINE MCS_Optim_Gen




























