!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  CVT using L-BFGS
!!  @param[in]  common_Parameters::IP_Tet  : present mesh.
!!  @param[in]  common_Parameters::Posit   : present mesh.
!!  @param[in]  common_Parameters::CVTMeshInit
!!                       : recovered mesh before inserting any internal nodes.
!<
!*******************************************************************************
SUBROUTINE CVT_LBFGS(NB1)
  ! NB1 is the first point which is effected by this process, I assume this is to
  ! stop the code adjusting boundary nodes - SPW
  USE common_Parameters
  USE array_allocator
  USE SpacingStorage
  USE OptimisingModule
  USE RandomModule
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: NB1
  REAL*8, DIMENSION(:,:),  POINTER :: newPosit  !--(3,:)
  INTEGER :: IP, nbp, kway, ix, NBOld, iy, iz,Ibest,I,Loop,Istatus,Iin,NumLoops,doDumb
  REAL*8  :: dmax, xl, energy, d,optf,ftemp,fbest,dxl,xl2,xl3
  INTEGER, SAVE :: NRR = 0
  !Need to set up some variables for the LBFGS optimiser
  !Since this code has loads of global variables need to be careful with names
  INTEGER :: optn, optm, optiprint,DD,N,Ni,isFinished,idum = -1
  REAL*8 :: optfactr, optpgtol  = 1.0d-5
  CHARACTER(LEN=60) :: opttask, optcsave, CVTctl
  logical                :: optlsave(4)
  integer                :: optisave(44),limFlag,improve
  real*8               :: optdsave(29),tempD(3),dum1,conv,dum2,residual,lastObj,obj
  integer,  allocatable  :: optnbd(:), optiwa(:)
  real*8, allocatable  :: optx(:), optl(:), optu(:), optg(:), optwa(:),ptemp(:),Pt(:,:),Qu(:)
  REAL*8, DIMENSION(:), POINTER ::  vsum

  
  
  OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
  READ(84,*) dum1
  READ(84,*) conv
  READ(84,*) dum2
  CLOSE(84)
  lastObj = HUGE(0.d0)
  improve = 1
  
  DO WHILE (improve.EQ.1)
  
  
  
  IF(.NOT. CircumUpdated) CALL Get_Tet_SphereCross(0)
    
    ALLOCATE(newPosit(3,NB_Point))
    ALLOCATE(vsum(NB_Point))    
     	
    !Number of variables 3 * number of points
    optn = 3*(NB_Point-NB1+1)
    DD = optn
	
	!Number of variables 3 * number of points
    optn = 3*(NB_Point-NB1+1)
    optm = 5
    DD = optn
    NBOld = NB_Point
    optfactr = 1E+7
	
    
    !Need to prepare variables for the LBFGS optimiser
	


	
	
    	!LBFGS
        allocate (optx(optn), optl(optn), optu(optn), optg(optn))
    	allocate ( optnbd(optn))
    	allocate ( optiwa(3*optn) )
    	allocate ( optwa(2*optm*optn + 5*optn + 11*optm*optm + 8*optm) )
	 
	!Define the starting point
	DO ip = NB1, NB_Point
		ix = 3*(ip-NB1+1) - 2
		iy = 3*(ip-NB1+1) - 1
		iz = 3*(ip-NB1+1)
		optx(ix) = Posit(1,ip)
		optx(iy) = Posit(2,ip)
		optx(iz) = Posit(3,ip)

		CALL SpacingStorage_Get3DGridSize(BGSpacing,Posit(:,ip),tempD)
		optl(ix) = optx(ix)-tempD(1)
		optl(iy) = optx(iy)-tempD(2)
		optl(iz) = optx(iz)-tempD(3)
	
		optu(ix) = optx(ix)+tempD(1)
        optu(iy) = optx(iy)+tempD(2)
        optu(iz) = optx(iz)+tempD(3)
		optnbd(ix) = 0
		optnbd(iy) = 0
		optnbd(iz) = 0
	
	ENDDO
	 
	opttask = 'START'
	optiprint = 1
    optfactr = 1E+1

	!First itteration
	call setulb ( optn, optm, optx, optl, optu, optnbd, optf, &
                                                optg, optfactr, optpgtol, &
                                                optwa, optiwa, opttask, optiprint,&
                                                optcsave, optlsave, optisave, optdsave )
	



	CALL CVT_Position(newPosit, dmax, 1,energy,vsum)

    	WRITE(29,*)'CVT_Reinsert:: CVT Energy=',energy
    	optf = energy
    
    	!Compute gradient
    	DO ip = NB1,NBOld
              optg(3*(ip-NB1+1) - 2) = (Posit(1,ip)-newPosit(1,ip))*vsum(ip)*2.0
              optg(3*(ip-NB1+1) - 1) = (Posit(2,ip)-newPosit(2,ip))*vsum(ip)*2.0
              optg(3*(ip-NB1+1)) = (Posit(3,ip)-newPosit(3,ip))*vsum(ip)*2.0
    	END DO




	 
	DO WHILE(opttask(1:2).eq.'FG'.or.opttask.eq.'NEW_X'.or. &
               opttask.eq.'START') 

			   
		!     This is the call to the L-BFGS-B code.
         
         call setulb ( optn, optm, optx, optl, optu, optnbd, optf, &
						optg, optfactr, optpgtol, &
						optwa, optiwa, opttask, optiprint,&
						optcsave, optlsave, optisave, optdsave )
	
		if (opttask(1:2) .eq. 'FG') then
			 IF(NBOld>NB_Point) THEN
                                NB_Point = NBOld
                                CALL ReAllocate_Point()
                        END IF
	   
			!Need to calculate the gradient and energy at optx
			DO ip = NB1, NBOld
				 newPosit(1,ip) = optx(3*(ip-NB1+1) - 2)
				 newPosit(2,ip) = optx(3*(ip-NB1+1) - 1)
				 newPosit(3,ip) = optx(3*(ip-NB1+1))
			ENDDO
			
			DO ip = NB1, NBOld
				Posit(:,ip) = newPosit(:,ip)
			ENDDO
			
			!--- recalculate geometry
			CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
			CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
			CALL Next_Build()
			CALL Set_Domain_Mapping()
			CALL Get_Tet_Circum(0)
			CALL Get_Tet_Volume(0)
			Mark_Point(1:NB_Point) = 0
			CALL Insert_Points_EasyPeasy(NB1, NB_Point, 5)  
			usePower = .FALSE.
			CALL Remove_Point()
			!Here we want to remove slivers
			CALL Swap3D(2)
			CALL Next_Build()
			CALL Get_Tet_Circum(0)
			CALL Get_Tet_Volume(0)
			CALL CVT_Position(newPosit, dmax, 1,energy,vsum)
			
			!WRITE(29,*)'CVT_Reinsert:: CVT Energy=',energy
			optf = energy
			
			!Compute gradient
			DO ip = NB1,NB_Point
				optg(3*(ip-NB1+1) - 2) = (Posit(1,ip)-newPosit(1,ip))*vsum(ip)*2.0
				optg(3*(ip-NB1+1) - 1) = (Posit(2,ip)-newPosit(2,ip))*vsum(ip)*2.0
				optg(3*(ip-NB1+1)) = (Posit(3,ip)-newPosit(3,ip))*vsum(ip)*2.0
			END DO
			
			
		endif
	END DO

	 IF(NBOld>NB_Point) THEN
		 NB_Point = NBOld
					CALL ReAllocate_Point()
			END IF


	DO ip = NB1, NBOld
                 newPosit(1,ip) = optx(3*(ip-NB1+1) - 2)
                 newPosit(2,ip) = optx(3*(ip-NB1+1) - 1)
                 newPosit(3,ip) = optx(3*(ip-NB1+1))
	ENDDO
			 
     
	DO ip = NB1, NBOld
		Posit(:,ip) = newPosit(:,ip)
	ENDDO
	!--- recalculate geometry
	CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
		CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
	CALL Next_Build()
	CALL Set_Domain_Mapping()
	CALL Get_Tet_Circum(0)
	CALL Get_Tet_Volume(0)
	Mark_Point(1:NB_Point) = 0
	CALL Insert_Points_EasyPeasy(NB1, NB_Point, 5)  
	usePower = .FALSE.
	CALL Remove_Point()
	
	
	DEALLOCATE(newPosit)
	DEALLOCATE(vsum)
	DEALLOCATE (optx, optl, optu, optg)
	DEALLOCATE ( optnbd)
	DEALLOCATE ( optiwa )
	DEALLOCATE ( optwa )
	
	obj = optf
	residual = ABS(1.d0 - (lastObj/obj))
	IF(Debug_Display>0)THEN
		
		WRITE(*,*)' CVT LBFGS energy and residual:',obj,residual
		WRITE(29,*)'  CVT LBFGS energy and residual:',obj,residual
	END IF
	IF (residual>conv) THEN
	  lastObj = obj
	ELSE
	  improve = 0
	  EXIT
	ENDIF

   END DO
 
END SUBROUTINE CVT_LBFGS
