!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  Centroid Smooth
!!  This routine performs smoothing based on the ideal distance from a centroid from point
!!  This required that the boundary triangulation was stored in CVTMeshInit
!!  NB1 is the first node which effected by the process
!!  Initial implimentation by S. Walton 15/01/2014
!!
!<
!*******************************************************************************
SUBROUTINE CVT_Quick(NB1,hull)
	USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE Geometry3DAll
	
	
	
	INTEGER, INTENT(IN)  :: NB1
	INTEGER, INTENT(IN)  :: hull
	
	REAL*8, DIMENSION(:,:), allocatable :: newPosit,oldPosit !To store new positions
	REAL*8, DIMENSION(:), allocatable :: weight !To store the weights
	INTEGER :: IT,i,ip,nbp,improve,Old_NB_Point
	REAL*8 :: wTet,obj,lastObj,sqrt5o4,delta,length,dum,conv,residual,dt,dum2
	REAL*8, DIMENSION(3) :: cTet,p1,pC
	
	OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
      READ(84,*) dt
      READ(84,*) conv
	  READ(84,*) dum2
      CLOSE(84)
	
	improve = 1
	
	!Make sure mapping and geometry is all up to date
	CALL Get_Tet_SphereCross(0)
	CALL Get_Tet_Volume(0)
	CALL Set_Domain_Mapping()
	CALL Next_Build()
	lastObj = 0

	
	
	lastObj = HUGE(0.0d0)
	
	Old_NB_Point = NB_Point
	IF(Debug_Display>0)THEN
			
		WRITE(*,*)'Centroid Smooth energy:',lastObj
		WRITE(29,*)'Centroid Smooth energy:',lastObj
	END IF
	
	
	ALLOCATE(newPosit(3,NB_Point))
	ALLOCATE(weight(NB_Point))
	ALLOCATE(oldPosit(3,NB_Point))
	
	DO WHILE(improve.EQ.1)
		
		!Initialise everything
		weight(1:NB_Point) = 0.d0
		newPosit(1:3,1:NB_Point) = 0.d0
		newPosit(:,1:NB1-1) = Posit(:,1:NB1-1)
		oldPosit(1:3,1:NB_Point) = Posit(1:3,1:NB_Point)
		
		obj = 0.0d0
		
		!Now loop over elements
		DO IT=1,NB_Tet
			wTet = Volume_Tet(IT)
		
			cTet(:) = ( Posit(:,IP_Tet(1,IT)) + Posit(:,IP_Tet(2,IT))    &
								+ Posit(:,IP_Tet(3,IT)) + Posit(:,IP_Tet(4,IT)) ) / 4.d0
			pC(:) = Sphere_Tet(1:3,IT)
			
			DO i=1,4
				ip = IP_Tet(i,IT)
				IF (ip>(NB1-1)) THEN
					
					length = sqrt(( Posit(1,ip) - pC(1) )*( Posit(1,ip) - pC(1) ) + &
							 ( Posit(2,ip) - pC(2) )*( Posit(2,ip) - pC(2) ) + &
							 ( Posit(3,ip) - pC(3) )*( Posit(3,ip) - pC(3) ))
							 
					obj = obj + length*wTet
					
					weight(ip) = weight(ip) + wTet
					
					IF (Next_Tet(i,IT)>0) THEN
						
						newPosit(:,ip) = newPosit(:,ip) + cTet*wTet
					ELSE				
						newPosit(:,ip) = newPosit(:,ip) + Sphere_Tet(1:3,IT)*wTet
					ENDIF
		
				END IF
			END DO
		
		END DO
		
		!Now loop over points and calculate weighted centroid
		DO ip = NB1,NB_Point
			
			Posit(:,ip) = Posit(:,ip) + dt*((newPosit(:,ip)/weight(ip))-Posit(:,ip))
			
		END DO
		
		nbp = NB1-1
		IF (hull.EQ.1) THEN
		  CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
		ELSE
		  CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
		  CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
		END IF
		CALL Next_Build()
		CALL Set_Domain_Mapping()
		CALL Get_Tet_SphereCross(0)
		CALL Get_Tet_Volume(0)
		Mark_Point(1:NB_Point) = 0
		CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10) 
		usePower = .FALSE.
		CALL Remove_Point()
		CALL Next_Build()
		CALL Set_Domain_Mapping()
		CALL Get_Tet_SphereCross(0)
		CALL Get_Tet_Volume(0)
		Mark_Point(1:NB_Point) = 0
		
		
		residual = ABS(1.d0 - (lastObj/obj))
		
		IF(Debug_Display>0)THEN
			
			WRITE(*,*)'Quick CVT CVT energy:',obj,residual
			WRITE(29,*)'Quick CVT CVT energy:',obj,residual
		END IF
		
		IF (residual>conv) THEN
			
			
			IF(Old_NB_Point.EQ.NB_Point)THEN
				lastObj = obj
				oldPosit(:,1:NB_Point) = Posit(:,1:NB_Point)
			ELSE
			
				DEALLOCATE(newPosit)
				DEALLOCATE(weight)
				
				Old_NB_Point = NB_Point
				lastObj = obj
				oldPosit(:,1:NB_Point) = Posit(:,1:NB_Point)
				ALLOCATE(newPosit(3,NB_Point))
				ALLOCATE(weight(NB_Point))
				
				
			ENDIF
			
		ELSE
			
			improve = 0
			EXIT
		ENDIF
		
		
	
	ENDDO

END SUBROUTINE CVT_Quick

















