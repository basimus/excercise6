!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!                                                                              !
!*******************************************************************************
SUBROUTINE Job_noFrame()

  USE common_Parameters
  USE Number_Char_Transfer
  IMPLICIT NONE
  INTEGER :: iloop, Isucc,ie,noBad,done
  CHARACTER*(4) :: NL
  
  REAL*8 :: fbest,lastObj,dum1,conv,dum2,residual
  INTEGER, allocatable :: badMask(:)

  !---- Set background Octree mesh

  WRITE(*,*)' '
  WRITE(*,*)'---- Read Background ----'
  CALL Background_Input()
  
  
  ! IF (Element_Break_Method==11) THEN
  !    !---- Read & Insert boundary nodes
  !    CALL Read_Surface( )
  !    CALL Basic_Split() !--Medial axis work
  ! ELSE
  


  IF(Start_Point<=1)THEN
     !---- Read & Insert boundary nodes
     WRITE(*,*)' '
     WRITE(*,*)'---- Read Boundary Points ----'
     CALL Read_Surface( )

     CALL VisLayer()	 
     IF(Element_Break_Method==4)THEN
        !--- generate and save ideal mesh points
        CALL Ideal_FillingMesh( )
     ENDIF
     WRITE(*,*)'---- Insert Boundary Points ----'
     CALL Boundary_Insert()
  ELSE IF(Start_Point==2)THEN
     WRITE(*,*)' '
     WRITE(*,*)'---- Read Volume Mesh ----'
     CALL READ_Volume()
     CALL VisLayer()

     IF (Loop_CVT>0) THEN
      CALL Set_Domain_Mapping()
      RR_Point(1:NB_Point) = 0.d0
      CALL Next_Build()
      CALL Get_Tet_Circum(0)
      CALL Get_Tet_Volume(0)
      CALL Check_Mesh_Geometry2('cvtStats')
     END IF

  ENDIF


  
  
  
  
  IF(Recovery_Time==1 .OR. Recovery_Time==3)THEN
     WRITE(*,*)' '
     WRITE(*,*)'---- Boundary Recovery ----'
     IF(Recovery_Method==1 .OR. Recovery_Method==2)THEN
        CALL Recovery( )
     ELSE IF(Recovery_Method==3 .OR. Recovery_Method==4)THEN
        CALL Recovery3D( )
     ENDIF
  ENDIF

  print *,'  3- BGSpacing%BasicSize is :' ,  BGSpacing%BasicSize

  IF(Background_Model==1)THEN
     WRITE(*,*)' '
     WRITE(*,*)'---- Generate Background by surface meshing----'
     CALL Background_Scale( )
  ELSE IF(Background_Model==-1)THEN
     WRITE(*,*)' '
     WRITE(*,*)'---- Generate Background  by surface meshing----'
     CALL Background_Stretch( )
  ENDIF

  !---- Set the mapping of points and tetrahedra
  !---- Set the centre and radius on the mapped domain

  

  WRITE(*,*)'---- Reset Mapping ----'
  IF(Element_Break_Method==4)THEN
     useStretch = .FALSE.
  ELSE
     useStretch = .TRUE.
  ENDIF
  CALL Set_Domain_Mapping()
  CALL Get_Tet_Circum(0)
  CALL Get_Tet_Volume(0)
  IF(Debug_Display>2 .AND. Recovery_Time==1 .AND. Recovery_Method>0)THEN
     CALL Check_Mesh_Geometry('after Boundary Recovery 1')
  ENDIF


  
  !---- Store boundary triangulation for reinsertion during cosmetics
  

  IF(Loop_CVT>0)THEN
     IF(Start_Point==1)THEN
     	CALL TetMeshStorage_NodeBackup(NB_Point, Posit,  CVTMeshInit)
     	CALL TetMeshStorage_CellBackup(NB_Tet,   IP_Tet, CVTMeshInit)
     ELSE
        !We need to recover the boundary triangulation
        CALL TetMeshStorage_NodeBackup(NB_Point, Posit,  CVTFull)
        CALL TetMeshStorage_CellBackup(NB_Tet,   IP_Tet, CVTFull)

        Recovery_Method=3
        CALL Recovery3D( )
        CALL TetMeshStorage_NodeBackup(NB_Point, Posit,  CVTMeshInit)
        CALL TetMeshStorage_CellBackup(NB_Tet,   IP_Tet, CVTMeshInit)
        !Need to read the full mesh back
        NB_Point = CVTFull%NB_Point
        NB_Tet = CVTFull%NB_Tet
        Posit = CVTFull%Posit
        IP_Tet = CVTFull%IP_Tet
        CALL Next_Build()
     ENDIF

  ENDIF

  !---- Insert new points by breaking large elements

  IF(Element_Break_Method==1 .OR. Element_Break_Method==3)THEN
     WRITE(*,*)' '
     WRITE(*,*)'---- Break Large Elements ----'
     CALL Element_Break()
	 RR_Point(1:NB_Point) = 0.d0
  ENDIF

  !---- Insert new points by breaking long edges

  IF(Element_Break_Method==2 .OR. Element_Break_Method==3)THEN
     WRITE(*,*)' '
     WRITE(*,*)'---- Break Long Edges ----'
     CALL Edge_Break(.TRUE.)
	 RR_Point(1:NB_Point) = 0.d0
  ENDIF

  IF(Element_Break_Method==4)THEN
     !---- Insert new points from the ideal mesh
     WRITE(*,*)' '
     WRITE(*,*)'---- Insert nodes in gap ----'
     CALL Insert_Points2(FillingNodes, 0)
     CALL TetMeshStorage_Clear(FillingNodes)
     WRITE(*,*)'---- Reset Mapping ----'
     useStretch = .TRUE.
     CALL Set_Domain_Mapping()
     WRITE(*,*)'---- Calculate Geometry ----'
     CALL Get_Tet_Circum(0)
     CALL Get_Tet_Volume(0)
	 RR_Point(1:NB_Point) = 0.d0
  ELSE IF(Element_Break_Method==5)THEN
     !--- read points from file and insert them
     CALL Read_FillingMesh( )
     WRITE(*,*)' '
     WRITE(*,*)'---- Insert nodes in gap ----'
     CALL Insert_Points2(FillingNodes, 0)
     CALL TetMeshStorage_Clear(FillingNodes)
	 RR_Point(1:NB_Point) = 0.d0
  ENDIF


  !-- Octree stencil insertion
  IF (Element_Break_Method==6) THEN
  	WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using Octree Stencil ----'
  	CALL Insert_Octree()
    CALL Next_Build()
    CALL Get_Tet_Circum(0)
    CALL Get_Tet_Volume(0)
    CALL Set_Domain_Mapping()
	RR_Point(1:NB_Point) = 0.d0
  END IF

  !-- Lattice growth
  IF (Element_Break_Method==7) THEN
  	WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using Lattice growth ----'
  	CALL Lattice_Insert(-1,NB_Point)
    CALL Next_Build()
    CALL Get_Tet_Circum(0)
    CALL Get_Tet_Volume(0)
    CALL Set_Domain_Mapping()
	RR_Point(1:NB_Point) = 0.d0
  END IF

  !-- Enhanced advancing front techniques

  IF (Element_Break_Method==8) THEN
  	WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using enhanced advancing front ----'
  	CALL Face_Insert()
    CALL Set_Domain_Mapping()
    WRITE(*,*)' '
    WRITE(*,*)'---- Break Large Elements ----'
    CALL Element_Break()
    WRITE(*,*)' '
    WRITE(*,*)'---- Break Long Edges ----'
    CALL Edge_Break(.TRUE.)

    CALL Next_Build()
    CALL Get_Tet_Circum(0)
    CALL Get_Tet_Volume(0)
    CALL Set_Domain_Mapping()
	RR_Point(1:NB_Point) = 0.d0
  END IF

  IF (Element_Break_Method==9) THEN
  	WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using enhanced advancing front ----'
  	CALL Face_Insert()
    CALL Set_Domain_Mapping()
    WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using Octree Stencil ----'
  	CALL Insert_Octree()

    CALL Next_Build()
    CALL Get_Tet_Circum(0)
    CALL Get_Tet_Volume(0)
    CALL Set_Domain_Mapping()
	RR_Point(1:NB_Point) = 0.d0
  END IF


  IF (Element_Break_Method==10) THEN
  	WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using enhanced advancing front ----'
  	CALL Face_Insert()
    CALL Set_Domain_Mapping()
    WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using Lattice growth ----'
  	CALL Lattice_Insert(-1,NB_Point)

    CALL Next_Build()
    CALL Get_Tet_Circum(0)
    CALL Get_Tet_Volume(0)
    CALL Set_Domain_Mapping()
	RR_Point(1:NB_Point) = 0.d0
  END IF

 
  IF(Debug_Display>2 .AND. Element_Break_Method>0)THEN
     CALL Check_Mesh_Geometry('after inserting points')
  ENDIF

  !---- Boundary Recovery

  IF(Start_Point==1)THEN
     IF(Recovery_Time==2 .OR. Recovery_Time==3)THEN
        WRITE(*,*)' '
        WRITE(*,*)'---- Boundary Recovery ----'
        IF(Recovery_Method==1 .OR. Recovery_Method==2)THEN
           CALL Recovery( )
        ELSE IF(Recovery_Method==3 .OR. Recovery_Method==4)THEN
           CALL Recovery3D( )
        ENDIF
        CALL Set_Domain_Mapping()
        CALL Get_Tet_Circum(0)
        CALL Get_Tet_Volume(0)
        IF(Debug_Display>2)THEN
           CALL Check_Mesh_Geometry('after Boundary Recovery 2')
        ENDIF
     ENDIF
  ENDIF


  
  !---- Cosmetic Loop

  !---First check if the user wishes to make co-volume meshes
  IF (Loop_CVT>0) THEN

	! --- Coevolution
    CALL Check_Mesh_Geometry2('before_optimisation')
    Swapping_Angle          = (12.0d0*3.141592653589793d0)/180.d0
    Collapse_Angle          = (12.0d0*3.141592653589793d0)/180.d0
    CALL Coevolution(CVTMeshInit%NB_Point+1,NB_Point)
  ELSE

	  iloop = 1
	  DO WHILE(iloop<=Loop_Cosmetic)
	     WRITE(*,*)' '
	     WRITE(*,*)'---- Cosmetic Loop ----',iloop
	     NL = INT_TO_CHAR(iloop,4)

	     IF(Element_Break_Method==-1)THEN
	        WRITE(*,*)' '
	        WRITE(*,*)'---- Break Long Edges ----'
	        CALL Edge_Break_Directly()
	     ENDIF

	     IF(BoundCell_Split>0 .AND. iloop==1)THEN
	        WRITE(*,*)' '
	        WRITE(*,*)'---- break boundary adhered cells ----'
	        CALL BoundCell_Break()
	     ENDIF

	     WRITE(*,*)' '
	     WRITE(*,*)'---- Smooth Mesh ----'
	     CALL Smooth( )

	     WRITE(*,*)' '
	     WRITE(*,*)'---- Swapping ----'
	    
	     CALL Swap3D(2)
	     IF(Debug_Display>2)THEN
	        WRITE(*,*)'---- Cosmetic Loop ----',iloop
	        CALL Check_Mesh_Geometry('after Swapping')
	     ENDIF

	     WRITE(*,*)' '
	     WRITE(*,*)'---- Collapse Tiny Angles ----'
	     CALL Element_Collapse( )
	     CALL Check_Mesh_Geometry('after Collapse')

	     

	     IF(BoundCell_Split==2)THEN
	        WRITE(*,*)' '
	        WRITE(*,*)'---- break boundary adhered cells ----'
	        CALL Next_Build( )
	        CALL BoundCell_Break()
	     ENDIF

	     IF(Debug_Display>2)THEN
	        CALL Check_Mesh_Geometry('after cosmetic loop'//NL)
	     ENDIF

		 CALL Next_Build( )
		 
	     iloop = iloop + 1

	  ENDDO

	  !RR_Point(1:NB_Point) = 0.d0
	  iloop = 1

  END IF

  IF(Start_Point==0)THEN
  	Curvature_Type = 4
     WRITE(*,*)' '
        WRITE(*,*)'---- break boundary adhered cells ----'
        CALL Next_Build( )
        CALL BoundCell_Break()

    !Output the mesh before collapse
    CALL TetMeshStorage_NodeBackup(NB_Point, Posit,  CVTFull)
    CALL TetMeshStorage_CellBackup(NB_Tet,   IP_Tet, CVTFull)

    WRITE(*,*)'---- Output ----'
     CALL TetMeshStorage_Output('volPre',6, CVTFull, Surf)

    CALL Check_Mesh_Geometry('before NEFEM_Gen')
    CALL NEFEM_Gen()
    CALL Check_Mesh_Geometry('after NEFEM_Gen')
  END IF



  CALL Next_Build()

  
  
  
  
  
  
  
  


  IF(Debug_Display>0)THEN
     CALL Check_Mesh_Geometry('final')
  ENDIF
  
  IF(Loop_CVT>0)THEN
	!Output bad elements to plt
	Mark_Tet(1:NB_Tet) = 0
	CALL Get_Tet_SphereCross(0)
	CALL Get_Tet_Volume(0)
	CALL Check_Mesh_Geometry2('final')
        noBad = 0
        DO ie=1,NB_Tet
          IF(Mark_Tet(ie)>0)THEN
		noBad = noBad+1
	  ENDIF
	ENDDO  
  	CALL Output_CC()
   END IF
  
 
  WRITE(*,*)' '
  WRITE(*,*)'---- Associate Boundary Triangles ----'
  IF (Start_Point==0) THEN
    !CALL Boundary_Associate(4)
  ELSE
    CALL Boundary_Associate(3)
  END IF


  WRITE(*,*)' '
  WRITE(*,*)'---- High Order ----'
  CALL Build_ElasticityMesh()


  Isucc = 0
  IF(Frame_Stitch/=0)THEN
     WRITE(*,*)' '
     WRITE(*,*)'---- Stitch with Boundary Layers ----'
     CALL Mesh3D_StitchVisLayer(Isucc)  !This actually builds the mesh MeshOut for writing
  ENDIF

  IF(Isucc==1)THEN

     IF(Debug_Display>0)THEN
        WRITE(*,*)'---- Output (inviscid part)----'
        JobName = JobName(1:JobNameLength)//'_v2'
        JobNameLength = JobNameLength + 3
        CALL HybridMeshStorage_Output(JobName,JobNameLength, -15, MeshOut, Surf)
        JobNameLength = JobNameLength - 3
     ENDIF

     WRITE(*,*)'---- Output ----'
     CALL HybridMeshStorage_Output(JobName,JobNameLength, -15, VM, Surfold)

  ELSE

     WRITE(*,*)'---- Output ----'
     CALL HybridMeshStorage_Output(JobName,JobNameLength, -15, MeshOut, Surf)

  ENDIF

  
  
  IF(Loop_CVT>0)THEN
	!Output bad elements to plt
	JobName = JobName(1:JobNameLength)//'_bad'
	JobNameLength = JobNameLength + 4
	CALL HybridMeshStorage_Output(JobName,JobNameLength, -3, MeshOut, Surf,Mark_Tet,noBad)
  ENDIF

 

END SUBROUTINE Job_noFrame


