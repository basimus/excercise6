!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


SUBROUTINE Gradient_Based(NB1,hull,NB2)
	!This routine used gradient based methods to move points to match idea element shape
    USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	IMPLICIT NONE
	
	INTEGER, INTENT(IN) :: NB1,NB2
	INTEGER, INTENT(IN)  :: hull
	
	
	REAL*8 :: dum1,conv,dum2
	INTEGER :: improve,L,nbp, ip, improve2,improve3
	REAL*8 :: funVal, lastObj, residual,residual3,funVal3,lastObj3
	REAL*8 :: dt,maxGrad,DEQual,DEQualOld,Layers(NB_Point)
	REAL*8, ALLOCATABLE :: PositM1(:,:), grad(:,:)
	INTEGER, ALLOCATABLE :: badMask(:)
    nbp = NB1-1
	
   
     !First try to correct the density
    ! CALL Correct_Density(NB1,hull)
	
	
	
	RR_Point(1:NB_Point) = 0.0d0
	OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
    READ(84,*) dt
    READ(84,*) conv
	READ(84,*) dum2
    CLOSE(84)
    conv = conv*dt
    L = 0
	improve = 0
	improve3 = 0
	lastObj = HUGE(0.d0)
	lastObj3 = HUGE(0.d0)
	ALLOCATE( PositM1(3,NB_Point), grad(3,NB_Point),  badMask(NB_Point))
	DO WHILE (improve3.EQ.0)
	  CALL Label_Layers(Layers)
	  DO WHILE (improve.EQ.0)
		L = L + 1
	
		!Here we want to remove slivers
		IF (hull.NE.1) THEN
		  CALL Swap3D(2)
		  CALL Next_Build()
		END IF
		
		!Calculate gradients
		CALL Calculate_ObjFun_Grad(grad,funVal,NB1)
		
	
		residual = ABS(1.d0 - (lastObj/funVal))
		
		IF (funVal>lastObj) THEN
			dt = 0.5d0*dt
		END IF
		
		IF(Debug_Display>0)THEN			
			WRITE(*,*)'Function value, and residual:',funVal,residual
			WRITE(29,*)'Function value, and residual:',funVal,residual
		END IF
		
		
			!Move and reinsert

			CALL Get_Global_Quality(NB1,NB2,funVal3,badMask,Layers)
			DO ip = NB1,NB2
			  Posit(:,ip) = Posit(:,ip) - dt*grad(:,ip)*REAL(badMask(ip))
			END DO
			                                                  
			IF (hull.EQ.1) THEN
			  CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
			ELSE
			  CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
			  CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
			END IF
			CALL Next_Build()
			CALL Set_Domain_Mapping()
			CALL Get_Tet_SphereCross(0)
			CALL Get_Tet_Volume(0)
			Mark_Point(1:NB_Point) = 0
			CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
			usePower = .FALSE.
			CALL Remove_Point()        
			CALL Next_Build()
			CALL Set_Domain_Mapping()
			CALL Get_Tet_SphereCross(0)
			CALL Get_Tet_Volume(0)
			
		!	IF (funVal>lastObj) THEN
				
		!		improve2 = 1
				
				
				!Tunnel a node
		!		CALL Get_Global_Quality(DEQualOld,badMask)
				
		!		DO WHILE (improve2.EQ.1) 
		!			CALL Tunnel_Node(NB1,badMask,hull)
		!			IF(Debug_Display>0)THEN			
		!				WRITE(*,*)'Tunnelled Node:',DEQualOld
		!				WRITE(29,*)'Tunnelled Node:',DEQualOld
		!			END IF
					
		!			CALL Get_Global_Quality(DEQual,badMask)
			
		!			IF(Debug_Display>0)THEN			
		!				WRITE(*,*)'Tunnelled Node:',DEQual
		!				WRITE(29,*)'Tunnelled Node:',DEQual
		!			END IF
					
		!			IF (DEQual.GE.DEQualOld) THEN
		!			  improve2 = 0
		!			ELSE
		!			  DEQualOld = DEQual
		!			END IF
				
		!		END DO
				
		!		lastObj = HUGE(0.d0)
			
		!	ELSE
			
			
			  lastObj = funVal
			
		!	END IF
			
		IF (residual<conv) THEN
		
			improve = 0
			EXIT
		END IF


	  END DO
	
	  CALL Label_Layers(Layers)
	  CALL Get_Global_Quality(NB1,NB2,funVal3,badMask,Layers)
	  residual3 = ABS(1.d0 - (lastObj3/funVal3))
	
	  IF(Debug_Display>0)THEN			
			WRITE(*,*)'Grading mesh: last, now, residual',lastObj3, funVal3, residual3
			WRITE(29,*)'Grading mesh:last, now, residual',lastObj3, funVal3, residual3
	  END IF
	
	  IF ((residual3<(0.001d0)).OR.(funVal3.EQ.0.0d0)) THEN
	    improve3 = 1
		EXIT
	  ELSE
	    lastObj3 = funVal3
	    !DEALLOCATE( PositM1, grad,  badMask)
	    !CALL Desperate_Divide(NB1,hull)
	    !ALLOCATE( PositM1(3,NB_Point), grad(3,NB_Point),  badMask(NB_Point))
	  END IF
	
	END DO
	
	DEALLOCATE( PositM1, grad,  badMask)

END SUBROUTINE Gradient_Based


SUBROUTINE Calculate_ObjFun_Grad(grad,funVal,NB1)
    USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	IMPLICIT NONE
	!This routine calculates the gradient of the objective function which
	!aims to move points to match the ideal element shape
	REAL*8, INTENT(OUT) :: grad(3,NB_Point)
	REAL*8, INTENT(OUT) :: funVal
	INTEGER, INTENT(IN) :: NB1
	REAL*8 :: alpha, delta, deltadx(3),pC(3)
	REAL*8 :: length
	INTEGER :: ip,ie,ic,i
	
	
	
	funVal = 0.0d0
	grad(1:3,1:NB_Point) = 0.0d0
	
	!Alpha is the coefficient of the grid size to give the target centroid node distance
	alpha = sqrt(5.d0)/4.d0
	
	
	CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
	
	DO ip = NB1,NB_Point
		!Get connected elements
		CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
		
		DO ic = 1,List_Length
			!First get all the element wide values
			ie = ITs_List(ic)		
			
			
			delta = (( Geo3D_Distance(Posit(:,IP_Tet(1,ie)),Posit(:,IP_Tet(2,ie))) + &
					  Geo3D_Distance(Posit(:,IP_Tet(1,ie)),Posit(:,IP_Tet(3,ie))) + &
					  Geo3D_Distance(Posit(:,IP_Tet(1,ie)),Posit(:,IP_Tet(4,ie))) + &
					  Geo3D_Distance(Posit(:,IP_Tet(2,ie)),Posit(:,IP_Tet(3,ie))) + &
					  Geo3D_Distance(Posit(:,IP_Tet(2,ie)),Posit(:,IP_Tet(4,ie))) + &
					  Geo3D_Distance(Posit(:,IP_Tet(3,ie)),Posit(:,IP_Tet(4,ie))))/6.0d0)
					  
			deltadx(1:3) = 0.0d0
			
			DO i= 1,4
				IF (IP_Tet(i,ie).NE.ip) THEN			
					deltadx(:) = deltadx(:) + (1.0d0/6.0d0)&
					                 *(Posit(:,ip)-Posit(:,IP_Tet(i,ie)))&
						             /(Geo3D_Distance(Posit(:,ip),Posit(:,IP_Tet(i,ie))))
				
				END IF
			END DO
			
			pC(:) = (( Posit(:,IP_Tet(1,ie)) + &
			           Posit(:,IP_Tet(2,ie)) + &
					   Posit(:,IP_Tet(3,ie)) + &
					   Posit(:,IP_Tet(4,ie))) / 4.d0)
					   
					   
			!Calculate the objective function and gradient values
			DO i = 1,4
			  length = Geo3D_Distance(pC,Posit(:,IP_Tet(i,ie)))
			  funVal = funVal + ( length - (alpha*delta) )*( length - (alpha*delta) )	
			  
			  IF (IP_Tet(i,ie).EQ.ip) THEN
			    grad(:,ip) = grad(:,ip) + 2.0d0*( length - (alpha*delta) )*(&
				             ( ((-3.0d0/4.0d0)*(pC(:)-Posit(:,IP_Tet(i,ie))))/length )&
							 - (alpha*deltadx(:)) )
			  
			  ELSE
				grad(:,ip) = grad(:,ip) + 2.0d0*( length - (alpha*delta) )*(&
				             ( ((1.0d0/4.0d0)*(pC(:)-Posit(:,IP_Tet(i,ie))))/length )&
							 - (alpha*deltadx(:)) )
			  
			  END IF
			  
			END DO
			
			
			
			
	
		END DO
		
	END DO
	



END SUBROUTINE Calculate_ObjFun_Grad

SUBROUTINE Variational_Based(NB1,hull)
	
    USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	IMPLICIT NONE
	
	INTEGER, INTENT(IN) :: NB1
	INTEGER, INTENT(IN)  :: hull
	
	
	REAL*8 :: dum1,conv,dum2
	INTEGER :: improve,L,nbp, ip,tunIP, numIns,ie, badMask(NB_Point)
	REAL*8 :: PositM1(3,NB_Point), newPosit(3,NB_Point), funVal, lastObj, residual
	REAL*8 :: dt,maxGrad,DEQual,tunPosit(3)
	REAL*8 :: maxMetric,minMetric
    nbp = NB1-1
	
	RR_Point(1:NB_Point) = 0.0d0
	OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
    READ(84,*) dt
    READ(84,*) conv
	READ(84,*) dum2
    CLOSE(84)
    conv = conv*dt
    L = 0
	improve = 0
	lastObj = HUGE(0.d0)
	
	DO WHILE (improve.EQ.0)
		L = L + 1
	
		!Here we want to remove slivers
		IF (hull.NE.1) THEN
		  CALL Swap3D(2)
		  CALL Next_Build()
		END IF
		
		!Calculate new positions
		CALL Variational_New(newPosit,funVal,NB1,tunPosit,tunIP,maxMetric,minMetric)
		
		
		
		
	
		residual = ABS(1.d0 - (lastObj/funVal))
		
		IF (funVal>lastObj) THEN
			dt = 0.5d0*dt
		END IF
		
		
		IF(Debug_Display>0)THEN			
			WRITE(*,*)'Function value, and residual:',funVal,residual
			WRITE(29,*)'Function value, and residual:',funVal,residual
		END IF
		
		
		!Reinsert
		!Move the points
		!Posit(:,NB1:NB_Point) = Posit(:,NB1:NB_Point) - dt*(newPosit(:,NB1:NB_Point)-Posit(:,NB1:NB_Point))
		Posit(:,NB1:NB_Point) = newPosit(:,NB1:NB_Point)
		
		IF (hull.EQ.1) THEN
		  CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
		ELSE
		  CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
		  CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
		END IF
		CALL Next_Build()
		CALL Set_Domain_Mapping()
		CALL Get_Tet_SphereCross(0)
		CALL Get_Tet_Volume(0)
		Mark_Point(1:NB_Point) = 0
		CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
		usePower = .FALSE.
		CALL Remove_Point()        
		CALL Next_Build()
		CALL Set_Domain_Mapping()
		CALL Get_Tet_SphereCross(0)
		CALL Get_Tet_Volume(0)
		
		
		IF (funVal>lastObj) THEN
			
			!Tunnel a node
		
			Posit(:,tunIP) = tunPosit(:)
		
			IF (hull.EQ.1) THEN
			  CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
			ELSE
			  CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
			  CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
			END IF
			CALL Next_Build()
			CALL Set_Domain_Mapping()
			CALL Get_Tet_SphereCross(0)
			CALL Get_Tet_Volume(0)
			Mark_Point(1:NB_Point) = 0
			CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
			usePower = .FALSE.
			CALL Remove_Point()        
			CALL Next_Build()
			CALL Set_Domain_Mapping()
			CALL Get_Tet_SphereCross(0)
			CALL Get_Tet_Volume(0)
		
			
			IF(Debug_Display>0)THEN			
				WRITE(*,*)'Tunnelled Node'
				WRITE(29,*)'Tunnelled Node'
			END IF
				
		END IF
		
		
		lastObj = funVal
		
		IF (residual<conv) THEN
			improve = 0
			EXIT
		END IF


	END DO

END SUBROUTINE Variational_Based

SUBROUTINE Variational_New(newPosit,energy,NB1,tunPosit,tunIP,maxMetric,minMetric)
    USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	IMPLICIT NONE
	
	REAL*8, INTENT(OUT) :: newPosit(3,NB_Point)
	REAL*8, INTENT(OUT) :: energy
	INTEGER, INTENT(IN) :: NB1
	REAL*8, INTENT(OUT) :: tunPosit(3)
	INTEGER, INTENT(OUT) :: tunIP
	REAL*8, INTENT(OUT) :: maxMetric,minMetric
	REAL*8 :: alpha, delta, deltadx(3),pC(3),pCen(3),numLocTet
	REAL*8 :: length,mui,weight,wip,numTet,locEnergy,metric
	INTEGER :: ip,ie,ic,i
	
	
	
	energy = 0.0d0
	maxMetric = 0.0d0
	minMetric = HUGE(0.0d0)
	
	
	numTet = 0.0d0
	
	
	
	CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
	DO ip = NB1,NB_Point
		!Get connected elements
		CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
	
		newPosit(1:3,ip) = 0.0d0
		weight = 0.0d0
		locEnergy = 0.0d0
		numLocTet = 0.0d0
		DO ic = 1,List_Length
			!First get all the element wide values
			ie = ITs_List(ic)		
			pCen(:) = (Posit(:,IP_Tet(1,ie))+Posit(:,IP_Tet(2,ie)) &
						+Posit(:,IP_Tet(3,ie))+Posit(:,IP_Tet(4,ie)))/4.0d0
			CALL SpacingStorage_GetScale(BGSpacing, pCen, mui)
			mui = mui*BGSpacing%BasicSize
			
			wip = Volume_Tet(ie)/(mui*mui*mui)
			
			metric = (wip-1.0d0)
			
			IF (metric.GT.maxMetric) THEN
			  maxMetric = metric
			  tunPosit(:) = pCen(:)
			  Mark_Tet(ie) = 0
			END IF
			locEnergy = locEnergy + metric
			numLocTet = numLocTet + 1.0d0
			energy = energy + metric*metric
			weight = weight + wip
			numTet = numTet + 1.0d0
			newPosit(:,ip) = newPosit(:,ip) + wip*Sphere_Tet(1:3,ie)
			
		END DO
		
		locEnergy = locEnergy/numLocTet
		
		IF (locEnergy.LT.minMetric) THEN
		  minMetric = locEnergy
		  tunIP = ip
		END IF
		
		newPosit(:,ip) = newPosit(:,ip)/weight
		
	END DO
	
	energy = energy/numTet

END SUBROUTINE Variational_New

SUBROUTINE Correct_Valancy(NB1,hull)
    USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	IMPLICIT NONE
	

	INTEGER, INTENT(IN) :: NB1,hull	
	REAL*8 :: alpha, delta, deltadx(3),pC(3),pCen(3),numLocTet
	REAL*8 :: length,mui,weight,wip,numTet,locEnergy,metric
	INTEGER :: ip,ie,ic,i,targVal,valancy(NB_Point),nbp,NB_PointOld
	INTEGER :: done 
	
	
	done = 0
	
	DO WHILE (done.EQ.0)
	
	!Attempts to correct the valauncy
	IF (hull.NE.1) THEN
	  CALL Swap3D(2)
	  CALL Next_Build()
	END IF
	
	
	targVal = 7
	
	Mark_Tet(1:NB_Tet) = 0
	Mark_Point(1:NB_Point) = 0
	valancy(1:NB_Point) = 0
	
	NB_PointOld = NB_Point
	
	CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
	DO ip = 1,NB_Point
		!Get connected elements
		CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
	
		Mark_Point(1:NB_Point) = 0
		Mark_Point(ip) = 1
	
		DO ic = 1,List_Length
			ie = ITs_List(ic)
			DO i = 1,4
			 IF (Mark_Point(IP_Tet(i,ie)).EQ.0) THEN
			   Mark_Point(IP_Tet(i,ie)) = 1
			   valancy(ip) = valancy(ip) + 1
			 END IF
		    END DO		
		END DO
		
		IF (valancy(ip).LT.targVal) THEN
			DO ic = 1,List_Length
			  ie = ITs_List(ic)
			  Mark_Tet(ie) = Mark_Tet(ie) + 1
			END DO		
		END IF
		
	END DO
	
	
	!Add new points to the list
	DO ie = 1,NB_Tet
		IF (Mark_Tet(ie).GT.1) THEN
			
			NB_Point = NB_Point + 1
			IF(NB_Point>nallc_point) THEN 

				 CALL ReAllocate_Point()
						 								 
			END IF
			Posit(:,NB_Point) = (Posit(:,IP_Tet(1,ie))+Posit(:,IP_Tet(2,ie)) &
						+Posit(:,IP_Tet(3,ie))+Posit(:,IP_Tet(4,ie)))/4.0d0
			
		END IF
	END DO
	
	!Now restore, remove and reinsert
	nbp = NB1-1
	IF (hull.EQ.1) THEN
	  CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
	ELSE
	  CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
	  CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
	END IF
	Mark_Point(1:NB_Point) = 0
	
	
	CALL Next_Build()
	CALL Set_Domain_Mapping()
	CALL Get_Tet_SphereCross(0)
	CALL Get_Tet_Volume(0)
	!Mark_Point(1:NB_Point) = 0
	CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
	usePower = .FALSE.
	IF (hull.NE.1) THEN
		CALL Swap3D(2)
		CALL Next_Build()
	END IF
	CALL Get_Tet_SphereCross(0)
	CALL Get_Tet_Volume(0)
	
	WRITE(*,*)'Correct valancy, points before and after',NB_PointOld,NB_Point
	RR_Point(1:NB_Point) = 0.0d0
	
	  IF (NB_PointOld.EQ.NB_Point) THEN
	    done = 1
      END IF
	
	END DO
END SUBROUTINE Correct_Valancy


SUBROUTINE Correct_Density(NB1,hull)
    USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	IMPLICIT NONE
	

	INTEGER, INTENT(IN) :: NB1,hull	
	REAL*8 :: alpha, delta, deltadx(3),pC(3),pCen(3),numLocTet
	REAL*8 :: length,mui,weight,wip,numTet,locEnergy,metric,locVol,targVol
	REAL*8 :: maxVol,tetVol
	INTEGER :: ip,ie,ic,i,targVal,nbp,NB_PointOld
	INTEGER :: loop
	REAL*8, ALLOCATABLE :: toDel(:)
	
	
	
	DO loop = 1,2
		ALLOCATE(toDel(NB_Point))
	
		!Attempts to correct the density
		IF (hull.NE.1) THEN
		  CALL Swap3D(2)
		  CALL Next_Build()
		END IF
		
		
		CALL Get_Tet_Volume(0)
		CALL Set_Domain_Mapping()
		
		Mark_Tet(1:NB_Tet) = 0
		Mark_Point(1:NB_Point) = 0
		toDel(1:NB_Point) = 0
		
		NB_PointOld = NB_Point
		
		CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
		DO ip = 1,NB_Point
			!Get connected elements
			CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
		
			locVol = 0.0d0
		
			DO ic = 1,List_Length
				ie = ITs_List(ic)
				tetVol = Volume_Tet(ie)
				locVol = locVol + tetVol
				
				pCen(:) = (Posit(:,IP_Tet(1,ie))+Posit(:,IP_Tet(2,ie)) &
						+Posit(:,IP_Tet(3,ie))+Posit(:,IP_Tet(4,ie)))/4.0d0
			    CALL SpacingStorage_GetScale(BGSpacing, pCen, mui)
				
				maxVol = 0.5d0*((mui*BGSpacing%BasicSize)**3.0d0)
				IF (Sphere_Tet(ie,4).GT.(4.0d0*mui*BGSpacing%BasicSize)) THEN
				  Mark_Tet(ie) = 1
				END IF
				
			END DO
			
			targVol = 0.5d0*((Scale_Point(ip)*BGSpacing%BasicSize)**3.0d0)
			
			IF (locVol.LT.targVol) THEN
			  toDel(ip) = 1
			END IF
			
		END DO
		
		IF (loop.EQ.1) THEN
			!Add new points to the list
			DO ie = 1,NB_Tet
				IF (Mark_Tet(ie).EQ.1) THEN
					
					NB_Point = NB_Point + 1
					IF(NB_Point>nallc_point) THEN 

						 CALL ReAllocate_Point()
																 
					END IF
					Posit(:,NB_Point) = (Posit(:,IP_Tet(1,ie))+Posit(:,IP_Tet(2,ie)) &
								+Posit(:,IP_Tet(3,ie))+Posit(:,IP_Tet(4,ie)))/4.0d0
					
				END IF
			END DO
		END IF
		
		!Now restore, remove and reinsert
		nbp = NB1-1
		IF (hull.EQ.1) THEN
		  CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
		ELSE
		  CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
		  CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
		END IF
		Mark_Point(1:NB_Point) = 0
		DO i = NB1,NB_PointOld
			IF (toDel(i).EQ.1) THEN
			  Mark_Point(i) = -999
			END IF
		END DO
		CALL Remove_Point()
		
		CALL Next_Build()
		CALL Set_Domain_Mapping()
		CALL Get_Tet_SphereCross(0)
		CALL Get_Tet_Volume(0)
		!Mark_Point(1:NB_Point) = 0
		CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
		usePower = .FALSE.
		IF (hull.NE.1) THEN
			CALL Swap3D(2)
			CALL Next_Build()
		END IF
		CALL Get_Tet_SphereCross(0)
		CALL Get_Tet_Volume(0)
		
		WRITE(*,*)'Correct density, points before and after',NB_PointOld,NB_Point
		RR_Point(1:NB_Point) = 0.0d0
		DEALLOCATE(toDel)
	END DO
END SUBROUTINE Correct_Density



SUBROUTINE Desperate_Divide(NB1,hull)
    USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	IMPLICIT NONE
	

	INTEGER, INTENT(IN) :: NB1,hull	
	REAL*8 :: alpha, delta, deltadx(3),pCen(3),numLocTet
	REAL*8 :: length,mui,weight,wip,numTet,locEnergy,metric,locVol,targVol
	REAL*8 :: maxVol,tetVol,oppDist,iFace,R,oppDistMax,oppDistMin
	INTEGER :: ip,ie,ic,i,targVal,nbp,NB_PointOld
	INTEGER :: loop,K
	REAL*8 :: pa(3),pb(3),pc(3),pd(3),circum(3),pins(3),vnorm(3)
	REAL*8 :: p1(3),p2(3),p3(3),p4(3),delta2,p1t(3),ptet(3,4),dum(4)
	
	!Attempts to grade the mesh
	IF (hull.NE.1) THEN
	  CALL Swap3D(2)
	  CALL Next_Build()
	END IF
	
	CALL Set_Domain_Mapping()
	CALL Get_Tet_SphereCross(0)
	CALL Get_Tet_Volume(0)
	
	NB_PointOld = NB_Point
	Mark_Tet(1:NB_Tet) = 0
	
	!Add new points to the list
	DO ie = 1,NB_Tet
	
		!Calculate the spacing
		p1 = Posit(:,IP_Tet(1,ie))
		p2 = Posit(:,IP_Tet(2,ie))
		p3 = Posit(:,IP_Tet(3,ie))
		p4 = Posit(:,IP_Tet(4,ie))
		
		
		
		delta2  = (( Geo3D_Distance(p1,p2) + &
					Geo3D_Distance(p1,p3) + &
					Geo3D_Distance(p1,p4) + &
					Geo3D_Distance(p2,p3) + &
					Geo3D_Distance(p2,p4) + &
					Geo3D_Distance(p3,p4))/6.0d0)
					
		!TODO : this condition still needs investigating
	
		IF (delta2.GT.(2.0d0*Scale_Tet(ie)*BGSpacing%BasicSize)) THEN
		
			ptet(:,1) = p1
			ptet(:,2) = p2
			ptet(:,3) = p3
			ptet(:,4) = p4
			p1t = Sphere_Tet(1:3,ie)
			K = 0
			dum =  Geo3D_Tet_Weight(K,ptet,p1t)
			
		    pins(:) = Sphere_Tet(1:3,ie)
		
			IF (K.NE.1) THEN
				
				NB_Point = NB_Point + 1
				IF(NB_Point>nallc_point) THEN 

					 CALL ReAllocate_Point()
															 
				END IF
				Posit(:,NB_Point) = pins(:)
				
			END IF
		END IF
	END DO
	
	WRITE(*,*) 'inserting points'
	
	!Now restore, remove and reinsert
	nbp = NB1-1
	IF (hull.EQ.1) THEN
	  CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
	ELSE
	  CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
	  CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
	END IF
	
	
	CALL Next_Build()
	CALL Set_Domain_Mapping()
	CALL Get_Tet_SphereCross(0)
	CALL Get_Tet_Volume(0)
	Mark_Point(1:NB_Point) = 0
	CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
	usePower = .FALSE.
	IF (hull.NE.1) THEN
		CALL Swap3D(2)
		CALL Next_Build()
	END IF
	CALL Remove_Point()
	CALL Get_Tet_SphereCross(0)
	CALL Get_Tet_Volume(0)
	
	WRITE(*,*)'Desperate_Divide, points before and after',NB_PointOld,NB_Point
	RR_Point(1:NB_Point) = 0.0d0
	
	
END SUBROUTINE Desperate_Divide


