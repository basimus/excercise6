!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  MCS_Optim_Weight
!!  
!!  This routine uses MCS to optimise weights of nodes in the mesh
!!
!!  
!!
!!  Initial implimentation by S. Walton 29/10/1013
!!
!<
!*******************************************************************************
SUBROUTINE MCS_Optim_Weight(NB2,fbest,oneLoop)
	USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	
	INTEGER, INTENT(IN) :: NB2,oneLoop
	INTEGER :: L,LMax,K,ie,ip,DD,N,Ibest,I,Loop,Istatus,Iin,Ni,Di,idum,isFinished,numLoops,ic,flag,nbp,improve,NB_old,j,t1,t2
	INTEGER :: badMask(NB_Point),iep
	REAL*8, allocatable  :: optx(:), optl(:), optu(:), optg(:) ,ptemp(:),Pt(:,:),Qu(:),vardef(:,:),PtTemp(:,:),rsave(:)
	REAL*8, INTENT(OUT) :: fbest
	REAL*8 :: dd1,dd2,ftemp,ranNum,d,FTarget,FBig,p1(3),dtet,psave,vol,ptet(3,4),p2(3),p3(3),p4(3)
	REAL*8 :: pp4(3,4),dum(4),coef,sumObj,lastObj,p1t(3),p2t(3),residual,delta,dist
	INTEGER :: ITnb,merged,iRan,jRan,tRan,numBadLoc,valancy,numEffElem,iFace
	REAL*8 :: dum1,conv,dum2,randn,pp42(3,4),dt,numLoc,dimi
	type(IntQueueType) :: surroundNodes,effElem
	type(Real8QueueType) :: surroundNodesD
	REAL*8, ALLOCATABLE :: faceSpace(:,:)
	INTEGER, ALLOCATABLE :: order(:)
	REAL*8, ALLOCATABLE :: snapStore(:,:),qualStore(:)
	INTEGER, ALLOCATABLE :: maskStore(:,:)
	REAL*8 :: tempRR(NB_Point),RR_Save(NB_Point)
	REAL*8, ALLOCATABLE :: searchParam(:,:)
	
	TYPE(Plane3D) :: aPlane
	
	INTEGER :: j1,j2,j3, bestL,i1,i2,i3,i4
	REAL*8 :: al, vomin, rn, bestLSum, numBound, numBadBound, numBad, totalNum, mergeFact
	
	INTEGER :: DENp,DEd,DEnumLoops,DEloop,DEi,DEj,DEl,DEk,DENoimp,DEimpFlag
	REAL*8 :: F,CR,DEQual
	
	INTEGER :: globFlag, globL,numLocLoops,Lconv, runL
	REAL*8 :: globObj, globObjLast,globRes,globObj2, globObjLast2,globRes2

	REAL*8 :: RR_Marker(NB_Point),o1,o2,o3,o4,w2,w3,w4
	REAL*8 :: Layers(NB_Point)

	REAL*8, ALLOCATABLE :: Fstore(:),CRstore(:)

	REAL*8 :: coefd

	REAL*8 :: localConv
	INTEGER :: isConved


	INTEGER :: nextPoint,worstElem,nextFlag,Mark_Point2(NB_Point)
	REAL*8 :: worstQual
	INTEGER, ALLOCATABLE :: lastPointStore(:)
	
	coefd = 4.0d0/(SQRT(5.0d0))
	
	idum = -1

	CALL IntQueue_Clear(surroundNodes)
	CALL Real8Queue_Clear(surroundNodesD)
	CALL IntQueue_Clear(effElem)
	
	OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
	READ(84,*) dt
	READ(84,*) conv
	READ(84,*) dum2
	READ(84,*) dum2
	READ(84,*) dum2
	READ(84,*) dum2
	READ(84,*) dum2
	READ(84,*) dum2
	READ(84,*) numLocLoops
	READ(84,*) LMax
	READ(84,*) Lconv
	CLOSE(84)
	
	conv = 10.0d0/REAL(NB_Tet)!0.001d0!
	
	WRITE(*,*)'conv',conv
	
	CALL Label_Layers(Layers)
	
	!LMax = 25
		
	!Initial parameters for DE
	CALL RANDOM_NUMBER(CR)
	CALL RANDOM_NUMBER(randn)
	F = 0.1d0 + (randn*0.9d0)
		
		
	!Allocate array to store snapshots for POD optimisation
	ALLOCATE(snapStore(LMax,NB_Point),qualStore(LMax),maskStore(LMax,NB_Point))
	ALLOCATE(Fstore(LMax+1),CRstore(LMax+1),lastPointStore(LMax+1))
	ALLOCATE(searchParam(2,LMax+1))


	

	qualStore(:) = HUGE(0.0d0)
	globFlag = 0
	
	globL = 0

	!Initialise the search parameters
	DO L = 1,LMax+1
		!CALL RANDOM_NUMBER(randn)
		searchParam(1,L) = 0.01d0
		!CALL RANDOM_NUMBER(randn)
		searchParam(2,L) = 0.7d0
		CALL RANDOM_NUMBER(randn)
		nextPoint = NB_Point * randn + 1
		lastPointStore(L) = nextPoint
	END DO
	
	lastObj = HUGE(0.d0)
	bestLSum = HUGE(0.d0)
	bestL = 1
	
	
	CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
	
	
	DO iep = 1,NB_Tet
		
		CALL Get_Actual_Tet_Mapping(iep)
		
	END DO
	

	DO WHILE(globFlag.EQ.0)
		
		ALLOCATE(order(NB_Point))
		!An order array to help shuffle
		DO iRan = 1,NB_Point
			order(iRan) = iRan
		ENDDO
		
		
		improve = 1
		!allocate(allPsave(NB_Point))
		!Get link info
				
		
		
		CircumUpdated = .TRUE.
		VolumeUpdated = .TRUE.
		
		DO iep = 1,NB_Tet
			CALL Get_Tet_SphereCross(iep)
			CALL Get_Tet_Volume(iep)
		END DO
		
		 
      
      
		
		 L = 1
		
		IF (globL .EQ. 0) THEN
		  snapStore(L,1:NB_Point) = RR_Point(1:NB_Point)
		  CALL Get_Global_Quality(1,NB2,bestLSum,badMask,Layers)
		  qualStore(L) = bestLSum
		  globObjLast = bestLSum
		  globObjLast2 = bestLSum
		  lastObj = bestLSum
		  bestL = L
		  maskStore(L,1:NB_Point) = badMask(1:NB_Point)
		ELSE
		  snapStore(L,1:NB_Point) = snapStore(bestL,1:NB_Point)
		  RR_Point(1:NB_Point) = snapStore(L,1:NB_Point)
          CALL Get_Global_Quality(1,NB2,bestLSum,badMask,Layers)
		  qualStore(L) = bestLSum
		  bestL = L
		  
		  maskStore(L,1:NB_Point) = badMask(1:NB_Point)
		END IF
	
		
		DO WHILE(L<LMax)
		
		
			!Select a starting point
			CALL RANDOM_NUMBER(randn)
			IF (randn.GT.(REAL(L)/REAL(LMax))) THEN
			  IF (globL.EQ.0) THEN
				CALL RANDOM_NUMBER(randn)
				jRan = L * randn + 1
				IF (jRan.GT.L) THEN
				  jRan = L
				ELSE IF (jRan.EQ.0) THEN
				  jRan = 1
				END IF
			  ELSE
			    CALL RANDOM_NUMBER(randn)
				jRan = LMax * randn + 1
				IF (jRan.GT.LMax) THEN
				  jRan = LMax
				ELSE IF (jRan.EQ.0) THEN
				  jRan = 1
				END IF
			  END IF
				
			ELSE
				jRan = bestL
			END IF
			
			RR_Point(1:NB_Point) = snapStore(jRan,1:NB_Point)
			runL = jRan
		
			FTarget = (1.D-6)*BGSpacing%MinSize
			FBig = 1.D30
			
			sumObj = 0.d0
			
			
			
			
			
			!Set up the optimisation problem
			!CALL init_random_seed()
			numLoops = numLocLoops !Number of MCS loops
			
			
			!allPsave(1:NB_Point) = RR_Point(1:NB_Point)
			
			
			!Shuffle the order
			!DO iRan = NB_Point,1,-1
			!	CALL RANDOM_NUMBER(randn)
			!   jRan = iRan * randn + 1
			!	tRan = order(iRan)
			!	order(iRan) = order(jRan)
			!	order(jRan) = tRan
			!ENDDO


			Mark_Point(1:NB_Point) = 0

			
			!First node positions
			nextPoint = lastPointStore(runL)
			Mark_Point2(1:NB_Point) = 0
			WRITE(*,*),nextPoint

			!First node positions
			DO WHILE(SUM(Mark_Point2(1:NB_Point)).LT.NB_Point)
				ip = nextPoint
				Mark_Point2(ip) = 1
				nextFlag = 0


				IF ((ip.GT.NB2).OR.(Mark_Point(ip).EQ.1)) THEN
					valancy = 0
				ELSE
				    valancy = 1
					Mark_Point(ip) = 1
					CALL IntQueue_Push(surroundNodes,ip)
				END IF
				
				!Get connected elements
				CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
				
				!Count number of bad elements around this node, and as we are
				!going count the number of points connected to ip
				
				IF (valancy.GT.0) THEN
					numBadLoc = 0
					DO ic = 1,List_Length
						ie = ITs_List(ic)
						
						i1 = IP_Tet(1,ie)
						i2 = IP_Tet(2,ie)
						i3 = IP_Tet(3,ie)
						i4 = IP_Tet(4,ie)
						
					!	IF (Mark_Point(i1).EQ.0) THEN
					!		valancy = valancy + 1
					!		Mark_Point(i1) = 1
					!		CALL IntQueue_Push(surroundNodes,i1)
					!	END IF
					!	IF (Mark_Point(i2).EQ.0) THEN
					!		valancy = valancy + 1
					!		Mark_Point(i2) = 1
					!		CALL IntQueue_Push(surroundNodes,i2)
					!	END IF
					!	IF (Mark_Point(i3).EQ.0) THEN
					!		valancy = valancy + 1
					!		Mark_Point(i3) = 1
					!		CALL IntQueue_Push(surroundNodes,i3)
					!	END IF
					!	IF (Mark_Point(i4).EQ.0) THEN
					!		valancy = valancy + 1
					!		Mark_Point(i4) = 1
					!		CALL IntQueue_Push(surroundNodes,i4)
					!	END IF
						
				
						
						CALL isInside(ie,K,dum2)


						IF (K.EQ.0) THEN
						
						  CALL isMerged(ie,mergeFact)
				          IF (mergeFact>0.0d0) THEN
							numBadLoc = numBadLoc + 1
						  END IF
						END IF
					
					END DO
				END IF
				
				IF ((numBadLoc.GT.0).AND.(valancy.GT.0)) THEN
					
					!IF(BGSpacing%Model>0)THEN
					!		d = Scale_Point(ip)*BGSpacing%BasicSize
					!ELSE IF(BGSpacing%Model<0)THEN
					!		d = MINVAL(fMap_Point(:,:,ip))*BGSpacing%BasicSize
					!END IF
					
					!Need to work out how many elements are effected by this collection of nodes
					Mark_Tet(1:NB_Tet) = 0
					numEffElem = 0
					d = 0.0d0
					DO i1 = 1,valancy
						CALL LinkAssociation_List(surroundNodes%Nodes(i1),List_Length,ITs_List,PointAsso)
						numLoc = 0.0d0
						d = 0.0d0
						DO ic = 1,List_Length
						  ie = ITs_List(ic)
						  IF (Mark_Tet(ie).EQ.0) THEN
							numEffElem = numEffElem + 1
							CALL IntQueue_Push(effElem,ie)
							d = d + Scale_Tet(ie)*BGSpacing%BasicSize
							numLoc = numLoc + 1.0d0
                          END IF							
						END DO
						d = d/numLoc
						
						d = d*d
						CALL Real8Queue_Push(surroundNodesD,d)
					END DO
					
					!d = d/REAL(numEffElem)
					!d = d*d  !because the subroutine wants r*r
					
					DD = valancy  !Number of dimensions - one for each space, going to scale by spacing
					N = MIN(40,10*valancy)  !Number of eggs
						
					allocate(ptemp(DD),Pt(DD,N),PtTemp(N-1,DD),Qu(N),optx(DD), optl(DD), optu(DD), optg(DD),vardef(DD,2),rsave(DD))
						
					
					ptemp(1:DD) = 0.0d0
					Ibest = 0
					ftemp = 0.0d0
					I = 0
					Loop = 0
					Istatus = 0
					Iin = 0
					Qu(1:N) = huge(0.0)
					fbest = huge(0.0)

				
					!Upper and lower bounds are going to be 1*d
					optl(1:DD) = -1.00d0
					optu(1:DD) = 1.00d0
					
				
					!The last egg will be the initial position
					DO Ni = 1,N
						DO Di = 1,DD
							!CALL RANDOM_NUMBER(randn)
							Pt(Di,Ni) = optl(Di) + (REAL(Ni)/REAL(N))*(optu(Di)-optl(Di))
						
						
						ENDDO
						
					ENDDO
					
					Pt(1:DD,N/2) = 0.0d0
				
				
				
					isFinished = 0
					
					DO i1 = 1,valancy
						rsave(i1) = RR_Point(surroundNodes%Nodes(i1))
					END DO
					
					
					DO WHILE (isFinished==0)
					
						
					
						CALL Optimising_Cuckoo_Driver(DD,N,ptemp,Ibest,ftemp,Pt,I,Loop, &
							                      Istatus,Iin,Qu,fbest,optu,optl,idum,&
							                      searchParam(1,runL),searchParam(2,runL))
						

						IF (((Loop-1).LT.numLoops)) THEN
						
							!Calculate fitness at ptemp and put it into ftemp
							DO i1 = 1,valancy
								RR_Point(surroundNodes%Nodes(i1)) = rsave(i1)&
 								                                     + surroundNodesD%V(i1)&
																	 *ptemp(i1)
							END DO
							!ftemp = ptemp(4)*ptemp(4)
							ftemp = 0.0d0
							flag = 0
						
							DO ic = 1,numEffElem
								ie = effElem%Nodes(ic)
								CALL Get_Tet_SphereCross(ie)				
							END DO

							DO ic = 1,numEffElem
							
								ie = effElem%Nodes(ic)
								p1 = Posit(:,IP_Tet(1,ie))
								p2 = Posit(:,IP_Tet(2,ie))
								p3 = Posit(:,IP_Tet(3,ie))
								p4 = Posit(:,IP_Tet(4,ie))
								
									!CALL Get_Tet_SphereCross(ie) 
									!Check if it's inside
									CALL isInside(ie,K,dum2)
									
									
									
									IF (K==0) THEN
									
										!There is a possibility to merge
										
										CALL isMerged(ie,mergeFact)							
										IF(mergeFact.GE.(0.0d0))THEN
										
										
											p1t = Sphere_Tet(1:3,ie)
											

										    p2t = (p1+p2+p3+p4) * 0.25d0
										    coef = d

											dd2 = (Geo3D_Distance_SQ(p2t, p1t)&
											        /(coef*coef))

											coef =  (MAXVAL(Layers(IP_Tet(1:4,ie))))
																	
												
											ftemp = ftemp + coef*dd2
										
										
									
										
										 ENDIF
									ENDIF
										 
										 
									

								
							
							ENDDO
							
							
							IF (flag.EQ.1) THEN
								ftemp = huge(0.d0)
							ENDIF
							IF (ftemp==0) THEN
								isFinished = 1
								EXIT
							ENDIF
							
						ELSE
						
							!The best result is in Pt(:,Ibest)
							
							DO i1 = 1,valancy
								RR_Point(surroundNodes%Nodes(i1)) = rsave(i1)&
 								                                     + surroundNodesD%V(i1)&
																	 *Pt(i1,Ibest)
							END DO
							
							sumObj = sumObj + fbest
							isFinished = 1
							EXIT
						ENDIF
				
					ENDDO

					worstQual = 0.0d0
					worstElem = 0				
					!
					DO ic = 1,numEffElem
						ie = effElem%Nodes(ic)
						  
						CALL Get_Tet_SphereCross(ie)	
                        
                        p1t = Sphere_Tet(1:3,ie)
						p2t = (p1+p2+p3+p4) * 0.25d0	
						coef = Scale_Tet(ie)*BGSpacing%BasicSize
						ftemp = (Geo3D_Distance_SQ(p2t, p1t)&
											        /(coef*coef))
						IF (ftemp.GT.worstQual) THEN
						  worstQual = ftemp
						  worstElem = ie
						END IF

					END DO

					!Figure out where to go next
					
					DO ic = 1,4
					  IF (Mark_Point(IP_Tet(ic,worstElem)).EQ.0) THEN
					    nextPoint = IP_Tet(ic,worstElem)
					    nextFlag = 1
					  ENDIF
					ENDDO		
					
					CALL IntQueue_Clear(surroundNodes)
					CALL Real8Queue_Clear(surroundNodesD)
					CALL IntQueue_Clear(effElem)
					deallocate(ptemp,Pt,PtTemp,Qu,optx, optl, optu, optg,vardef,rsave)
				END IF


				IF ((nextFlag.EQ.0).AND.(SUM(Mark_Point2(1:NB_Point)).LT.NB_Point)) THEN
				  !We need to find a point to go to next
				  nextPoint=minloc(Mark_Point2, 1, mask=Mark_Point2.eq.0)
				  
				END IF



			ENDDO
			
			!CALL Get_Tet_SphereCross(0)
			CALL Get_Global_Quality(1,NB2,sumObj,badMask,Layers)
			residual = ABS(1.d0 - (bestLSum/sumObj))
			
			IF(Debug_Display>0)THEN
					
					WRITE(*,*)'Wei Obj fun val, A, pa',sumObj,searchParam(1,runL),searchParam(2,runL)
					WRITE(29,*)'Wei Obj fun val, A, pa',sumObj,searchParam(1,runL),searchParam(2,runL)
			END IF
			
			
			
			
			L = L+1

			IF (sumObj.LT.qualStore(runL)) THEN
				CALL RANDOM_NUMBER(randn)
				searchParam(1,L) = searchParam(1,L)+randn*(searchParam(1,runL)-searchParam(1,L))
				CALL RANDOM_NUMBER(randn)
				searchParam(2,L) = searchParam(2,L)+randn*(searchParam(2,runL)-searchParam(2,L))
			ELSE
				CALL RANDOM_NUMBER(randn)
				searchParam(1,runL) = randn
				CALL RANDOM_NUMBER(randn)
				searchParam(2,runL) = randn
			END IF
			
			IF ((sumObj.LT.qualStore(L))) THEN
				
				snapStore(L,1:NB_Point) = RR_Point(1:NB_Point)
				qualStore(L) = sumObj
				maskStore(L,1:NB_Point) = badMask(1:NB_Point)
				IF (sumObj<bestLSum) THEN
				  bestL = L
				  bestLSum = sumObj
				END IF
			END IF
			

		
		ENDDO
		
		RR_Point(1:NB_Point) = snapStore(bestL,1:NB_Point)
		CircumUpdated = .TRUE.
		VolumeUpdated = .TRUE.
		
		DO iep = 1,NB_Tet
			CALL Get_Tet_SphereCross(iep)		
		END DO
		
		 CircumUpdated = .TRUE.
		
		
		CALL Check_Mesh_Geometry2('afterGlobalweight')
		
		
	
		globObj = SUM(qualStore)/REAL(LMax)
		globObj2 = qualStore(bestL)
		globL = globL + 1
		
		DEALLOCATE(order)
		
		
		
		globRes = ABS(1.d0 - (globObjLast/globObj))
		globRes2 = ABS(1.d0 - (globObjLast2/globObj2))
		IF(Debug_Display>0)THEN
				
			WRITE(*,*)'DE Global res1, res2:',globRes,globRes2
			WRITE(29,*)'DE Global res1, res2:',globRes,globRes2
		END IF
		IF ((globRes.LT.(10.0d0*conv)).AND.(globRes2.LT.conv).OR.(oneLoop.EQ.1)) THEN
            globFlag = 1
	    ELSE
		   globObjLast = globObj
		   globObjLast2 = globObj2
	    END IF
	END DO
	
		
END SUBROUTINE MCS_Optim_Weight


SUBROUTINE Get_Global_Quality(NB1,NB2,qual,badMask,Layers)
	USE common_Constants
	USE common_Parameters
	USE Geometry3DAll
	
	REAL*8, intent(OUT) :: qual
	INTEGER, intent(OUT) :: badMask(NB_Point)
	REAL*8, intent(IN) :: Layers(NB_Point)
	INTEGER, intent(IN) :: NB1, NB2
	REAL*8 :: numBound,numBadBound,numBad(NB_Tet),totalNum,sumObj,dimi
	INTEGER :: ie,K,merged,j,iep
	REAL*8 :: p1(3),p2(3),p3(3),p4(3),p1t(3),p2t(3),dum(4),ptet(3,4),mergeFact,dd2
	REAL*8 :: avLay,invPi,minVol
	TYPE(Plane3D) :: aPlane
	
	invPi = 1.0d0/PI
					
	numBound = 0.0d0
	numBadBound = 0.0d0
	numBad(1:NB_Tet) = 0.0d0
	totalNum = 0.0d0
	
	badMask(1:NB_Point) = 0.0d0
	sumObj = 0.0d0
	CircumUpdated = .TRUE.
	VolumeUpdated = .TRUE.
	

	

	DO  ie = 1,NB_Tet

		

		IF (IP_Tet(1,ie) >= NB1 .OR. &
		    IP_Tet(2,ie) >= NB1 .OR. &
		    IP_Tet(3,ie) >= NB1 .OR. &
		    IP_Tet(4,ie) >= NB1 .OR. &
			IP_Tet(1,ie) <= NB2 .OR. &
		    IP_Tet(2,ie) <= NB2 .OR. &
		    IP_Tet(3,ie) <= NB2 .OR. &
		    IP_Tet(4,ie) <= NB2 ) THEN

			

			p1 = Posit(:,IP_Tet(1,ie))
			p2 = Posit(:,IP_Tet(2,ie))
			p3 = Posit(:,IP_Tet(3,ie))
			p4 = Posit(:,IP_Tet(4,ie))		

			
			!ptet(:,1) = p1
			!ptet(:,2) = p2
			!ptet(:,3) = p3
			!ptet(:,4) = p4
			!p1t = Sphere_Tet(1:3,ie)
			!K=0
			!dum =  Geo3D_Tet_Weight(K,ptet,p1t)
			 
			CALL isInside(ie,K,minVol)		
				
			IF (K==1) THEN
			!We are inside
			
			
				 
			ELSE
				!There is a possibility to merge
				merged = 0				
				
				CALL isMerged(ie,mergeFact)
				

					
				IF(mergeFact.GE.(0.0d0))THEN
				  
					
					
					
					p1t = Sphere_Tet(1:3,ie)
					p2t = (p1+p2+p3+p4) / 4.d0


					!avLay = 0.0d0
					!DO j=1,4
												  
												    
					   !Get the opposite face
					!   p1(:) = Posit(:,IP_Tet(iTri_Tet(1,j),ie))
					!   p2(:) = Posit(:,IP_Tet(iTri_Tet(2,j),ie))
					!   p3(:) = Posit(:,IP_Tet(iTri_Tet(3,j),ie))
					   
					!   aPlane = Plane3D_Build(p1,p2,p3)
					!   dimi = Plane3D_DistancePointToPlane(p2t,aPlane)
					!   avLay = avLay+dimi
					 
					!ENDDO
					
					avLay = Scale_Tet(ie)*BGSpacing%BasicSize

					dd2 = (Geo3D_Distance_SQ(p2t, p1t)&
							/(avLay*avLay))
					
					
					
					sumObj = sumObj + dd2
				 
					badMask(IP_Tet(1,ie)) = 1
					badMask(IP_Tet(2,ie)) = 1
					badMask(IP_Tet(3,ie)) = 1
					badMask(IP_Tet(4,ie)) = 1

					avLay =  (MAXVAL(Layers(IP_Tet(1:4,ie))))

					numBad(ie) = dd2*avLay

				
				ENDIF
			ENDIF
			
		END IF
	
	
	END DO
	
	
	qual = SUM(numBad)/REAL(NB_Tet)





END SUBROUTINE


!This subrouting loops round nodes and labels nodes based on the distance away from the boundary
SUBROUTINE Label_Layers(Layers)

	USE common_Parameters
	USE common_Constants

	REAL*8, intent(OUT) :: Layers(NB_Point)
	REAL*8 :: dl,label,lastLabel,maxLabel
	INTEGER :: ip,done,flag,ie,ic,i,j,IT

	dl = 1.0d0 !This is how much the weight decreases each layer
	maxLabel = 10.0d0 !The initial value of the label
	label = maxLabel
	!Build the link data
	CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)

	Layers(1:NB_Point) = 1.0d0
	!First label the boundary nodes
	DO IT = 1,Surf%NB_Tri
    IF (  (Type_Wall(Surf%IP_Tri(5,IT)).NE.2).AND. &
          (Type_Wall(Surf%IP_Tri(5,IT)).NE.3) ) THEN

      Layers( Surf%IP_Tri(1:3,IT) ) = label
    ENDIF

   ENDDO


	!Layers(1:NB_BD_Point) = label
	lastLabel = label
	label = label - dl

	done = 0
	DO WHILE (done.EQ.0)
		flag = 1
		DO ip = 1,NB_Point
			IF (Layers(ip).EQ.lastLabel) THEN
				!Get the connected elements
				CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
				DO ic = 1,List_Length
					ie = ITs_List(ic)
 					DO i = 1,4
 						j = IP_Tet(i,ie)
 						IF (label.GT.Layers(j)) THEN
 							Layers(j)=label
 							flag = 0
 						END IF
 					END DO
				END DO
			END IF
		END DO

		lastLabel = label
		label = label - dl
		IF ((label.LE.0.0d0).OR.(flag.EQ.1)) THEN

		  done = 1
		END IF
	END DO

	Layers(:) = Layers(:)/maxLabel

END SUBROUTINE Label_Layers





















