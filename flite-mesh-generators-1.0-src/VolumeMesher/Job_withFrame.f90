!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!                                                                              !
!*******************************************************************************
SUBROUTINE Job_withFrame()

  USE common_Parameters
  USE Number_Char_Transfer
  USE array_allocator
  IMPLICIT NONE
  INTEGER :: iloop
  CHARACTER*(4) :: NL
  INTEGER :: ip, nbp

  !---- Set background Octree mesh
  WRITE(*,*)' '
  WRITE(*,*)'---- Read Background ----'
  CALL Background_Input()


  IF(Frame_Stitch==-1)THEN
     WRITE(*,*)' '
     WRITE(*,*)'---- Read Volume Mesh ----'
     RECOVERY_TIME = 0
     CALL READ_Volume()
     WRITE(*,*)' '
     WRITE(*,*)'---- Read frame mesh----'
     IF(Frame_Type==1)THEN
        CALL TetMeshStorage_Input(JobName(1:JobNameLength)//'_f',JobNameLength+2,TetFrame)
        CALL Mesh3D_Stitch(TetFrame)
     ELSE IF(Frame_Type==2 .OR. Frame_Type==4 .OR. Frame_Type==5)THEN
        CALL HybridMeshStorage_Input(JobName(1:JobNameLength)//'_hyb',JobNameLength+4,  &
             -1, HybFrame, HybridSurf)
        CALL Mesh3D_StitchHybrid(HybFrame, HybridSurf)
        IF(Frame_Type==2 .OR. Frame_Type==5)THEN
           CALL HybridMeshStorage_Output(JobName(1:JobNameLength),JobNameLength,  &
                -1, HybFrame, HybridSurf)
           WRITE(*,*) '---- Successfully Stitching ----'
           STOP
        ENDIF
     ENDIF
     CALL Output_Surface()
     CALL Output_Volume()
     WRITE(*,*) '---- Successfully Stitching ----'
     STOP
  ENDIF

  !---- Read Surface mesh
  WRITE(*,*)' '
  WRITE(*,*)'---- Read Boundary Points ----'
  CALL Read_Surface( )

  !---- Set initial box
  WRITE(*,*)'---- build ideal mesh ----'
  
  IF(Frame_Type==1)THEN
     CALL Ideal_FrameMesh()
  ELSE IF(Frame_Type==2)THEN
     CALL Hybrid_StairCase()
     IF(Element_Break_Method==4)  CALL Ideal_FillingMesh( )
  ELSE IF(Frame_Type==3)THEN
     CALL Wedge_StairCase()
     IF(Element_Break_Method==4)  CALL Ideal_FillingMesh( )
  ELSE IF(Frame_Type==4 .OR. Frame_Type==5)THEN
     CALL Octree_StairCase()
  ENDIF
  


  WRITE(*,*)'---- Insert Boundary Points ----'
  CALL Boundary_Insert()
  WRITE(*,*)' '
  WRITE(*,*)'---- Boundary Recovery ----'
  IF(Recovery_Method==1 .OR. Recovery_Method==2)THEN
     CALL Recovery( )
  ELSE IF(Recovery_Method==3 .OR. Recovery_Method==4)THEN
     CALL Recovery3D( )
  ENDIF

 

  WRITE(*,*)'---- Reset Mapping ----'
  IF(Element_Break_Method==4)THEN
     useStretch = .FALSE.
  ELSE
     useStretch = .TRUE.
  ENDIF
  CALL Set_Domain_Mapping()
  CALL Get_Tet_Circum(0)
  CALL Get_Tet_Volume(0)
  IF(Debug_Display>2)THEN
     CALL Check_Mesh_Geometry('after Boundary Recovery 1')
  ENDIF

  !---- Store information for CVT algorithm

  IF(Loop_CVT>0)THEN
     CALL TetMeshStorage_NodeBackup(NB_Point, Posit,  CVTMeshInit)
     CALL TetMeshStorage_CellBackup(NB_Tet,   IP_Tet, CVTMeshInit)
  ENDIF
  IF(Debug_Display>2 .AND. Start_Point==1)THEN
     IF(CVTMeshInit%NB_Point>0)THEN
        CALL TetMeshStorage_Output(JobName(1:JobNameLength)//'_Ini',  &
                JobNameLength+4, CVTMeshInit, Surf)
        IF(keyLength(Mark_Surf_Point)>=Surf%NB_Point)THEN
           OPEN(1, file=JobName(1:JobNameLength)//'_Ini.bmark', form='FORMATTED')
           WRITE(1,*)Surf%NB_Point
           WRITE(1,*)1,Mark_Surf_Point(1)
           DO ip = 2, Surf%NB_Point
              IF(Mark_Surf_Point(ip)==Mark_Surf_Point(ip-1)) CYCLE
              WRITE(1,*)ip,Mark_Surf_Point(ip)
           ENDDO
           CLOSE(1)
        ENDIF
     ENDIF
     IF(FillingNodes%NB_Point>0)THEN
        CALL TetMeshStorage_Output(JobName(1:JobNameLength)//'_Fill',  &
                 JobNameLength+5, FillingNodes)
     ENDIF
  ENDIF

  !---- Insert new points by breaking large elements

  IF(Element_Break_Method==1 .OR. Element_Break_Method==3)THEN
     WRITE(*,*)' '
     WRITE(*,*)'---- Break Large Elements ----'
     CALL Element_Break()
  ENDIF

  !---- Insert new points by breaking long edges

  IF(Element_Break_Method==2 .OR. Element_Break_Method==3)THEN
     WRITE(*,*)' '
     WRITE(*,*)'---- Break Long Edges ----'
     CALL Edge_Break(.TRUE.)
  ENDIF

  IF(Element_Break_Method==4)THEN
     !---- Insert new points from the ideal mesh
     WRITE(*,*)' '
     WRITE(*,*)'---- Insert nodes in gap ----'
     CALL Insert_Points2(FillingNodes, 0)
     CALL TetMeshStorage_Clear(FillingNodes)
     WRITE(*,*)'---- Reset Mapping ----'
     useStretch = .TRUE.
     CALL Set_Domain_Mapping()
     WRITE(*,*)'---- Calculate Geometry ----'
     CALL Get_Tet_Circum(0)
     CALL Get_Tet_Volume(0)
   RR_Point(1:NB_Point) = 0.d0
  ELSE IF(Element_Break_Method==5)THEN
     !--- read points from file and insert them
     CALL Read_FillingMesh( )
     WRITE(*,*)' '
     WRITE(*,*)'---- Insert nodes in gap ----'
     CALL Insert_Points2(FillingNodes, 0)
     CALL TetMeshStorage_Clear(FillingNodes)
   RR_Point(1:NB_Point) = 0.d0
  ENDIF


  !-- Octree stencil insertion
  IF (Element_Break_Method==6) THEN
    WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using Octree Stencil ----'
    CALL Insert_Octree()
    CALL Next_Build()
    CALL Get_Tet_Circum(0)
    CALL Get_Tet_Volume(0)
    CALL Set_Domain_Mapping()
  RR_Point(1:NB_Point) = 0.d0
  END IF

  !-- Lattice growth
  IF (Element_Break_Method==7) THEN
    WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using Lattice growth ----'
    CALL Lattice_Insert(-1,NB_Point)
    CALL Next_Build()
    CALL Get_Tet_Circum(0)
    CALL Get_Tet_Volume(0)
    CALL Set_Domain_Mapping()
  RR_Point(1:NB_Point) = 0.d0
  END IF

  !-- Enhanced advancing front techniques

  IF (Element_Break_Method==8) THEN
    WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using enhanced advancing front ----'
    CALL Face_Insert()
    CALL Set_Domain_Mapping()
    WRITE(*,*)' '
    WRITE(*,*)'---- Break Large Elements ----'
    CALL Element_Break()
    WRITE(*,*)' '
    WRITE(*,*)'---- Break Long Edges ----'
    CALL Edge_Break(.TRUE.)

    CALL Next_Build()
    CALL Get_Tet_Circum(0)
    CALL Get_Tet_Volume(0)
    CALL Set_Domain_Mapping()
  RR_Point(1:NB_Point) = 0.d0
  END IF

  IF (Element_Break_Method==9) THEN
    WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using enhanced advancing front ----'
    CALL Face_Insert()
    CALL Set_Domain_Mapping()
    WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using Octree Stencil ----'
    CALL Insert_Octree()

    CALL Next_Build()
    CALL Get_Tet_Circum(0)
    CALL Get_Tet_Volume(0)
    CALL Set_Domain_Mapping()
  RR_Point(1:NB_Point) = 0.d0
  END IF


  IF (Element_Break_Method==10) THEN
    WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using enhanced advancing front ----'
    CALL Face_Insert()
    CALL Set_Domain_Mapping()
    WRITE(*,*)' '
    WRITE(*,*)'---- Inserting points using Lattice growth ----'
    CALL Lattice_Insert(-1,NB_Point)

    CALL Next_Build()
    CALL Get_Tet_Circum(0)
    CALL Get_Tet_Volume(0)
    CALL Set_Domain_Mapping()
  RR_Point(1:NB_Point) = 0.d0
  END IF


  IF(Debug_Display>2)THEN
     CALL Check_Mesh_Geometry2('after inserting points')
  ENDIF

  !---- Cosmetic Loop
   !---First check if the user wishes to make co-volume meshes
  IF (Loop_CVT>0) THEN

  ! --- Coevolution
    CALL Check_Mesh_Geometry2('before_optimisation')
    Swapping_Angle          = (12.0d0*3.141592653589793d0)/180.d0
    Collapse_Angle          = (12.0d0*3.141592653589793d0)/180.d0
    CALL Coevolution(CVTMeshInit%NB_Point+1,NB_Point)
  ELSE

    iloop = 1
    DO WHILE(iloop<=Loop_Cosmetic)
       WRITE(*,*)' '
       WRITE(*,*)'---- Cosmetic Loop ----',iloop
       NL = INT_TO_CHAR(iloop,4)

       IF(Frame_Type==4 .OR. Frame_Type==5)THEN
          WRITE(*,*)' '
          WRITE(*,*)'---- Break Long Edges ----'
          CALL Edge_Break_Directly()
       ENDIF

       WRITE(*,*)' '
       WRITE(*,*)'---- Swapping ----'
       CALL Swap3D(2)
       
       IF(Debug_Display>2)THEN
          CALL Check_Mesh_Geometry2('after Swapping')
       ENDIF

       WRITE(*,*)' '
       WRITE(*,*)'---- Collapse Tiny Angles ----'
       CALL Element_Collapse( )

       WRITE(*,*)' '
       WRITE(*,*)'---- Smooth Mesh ----'
       CALL Smooth( )

       CALL Next_Build()

       IF(Debug_Display>2)THEN
          CALL Check_Mesh_Geometry2('after cosmetic loop'//NL)
       ENDIF

       iloop = iloop + 1
    ENDDO
  END IF

  IF(Frame_Type==2 .OR. Frame_Type==3)THEN
     nbp = SIZE(Mark_Surf_Point)
     IF(nbp>NB_Point) CALL Error_Stop ('Job_withFrame')
     IF(nbp<NB_BD_Point)THEN
        CALL allc_1Dpointer(Mark_Surf_Point,  NB_BD_Point, 'Mark_Surf_Point')
        Mark_Surf_Point(nbp+1:NB_BD_Point) = 0
     ENDIF
     RR_Point(1:NB_Point) = 0.d0
  ENDIF
  IF(Frame_Type==2)THEN
     DO ip = 1,nbp
        IF(Mark_Surf_Point(ip)==1)THEN
           RR_Point(ip) = 0.
        ELSE IF(Mark_Surf_Point(ip)==2)THEN
           RR_Point(ip) = -0.25d0 * BGSpacing%BasicSize**2     !---  -0.35d0 * BGSpacing%BasicSize**2
        ELSE IF(Mark_Surf_Point(ip)==3)THEN
           RR_Point(ip) = 0.                        !---  -0.2d0 * BGSpacing%BasicSize**2
        ENDIF
     ENDDO
  ENDIF

  useStretch = .FALSE.
  CALL Set_Domain_Mapping()
  CALL Get_Tet_Circum(0)
  CALL Get_Tet_Volume(0)

  IF(Frame_Stitch==1)THEN
     WRITE(*,*)' '
     WRITE(*,*)'---- Stitch ----'
     IF(Frame_Type==1)THEN
        CALL Mesh3D_Stitch(TetFrame)
     ELSE IF(Frame_Type==2 .OR. Frame_Type==4 .OR. Frame_Type==5)THEN
        CALL Mesh3D_StitchHybrid(HybFrame, HybridSurf)
        IF(Frame_Type==2 .OR. Frame_Type==5)THEN
           CALL HybridMeshStorage_Output(JobName(1:JobNameLength),JobNameLength,  &
                -1, HybFrame, HybridSurf)
           RETURN
        ENDIF
     ENDIF
  ELSE
     JobName = JobName(1:JobNameLength)//'_n'
     JobNameLength = JobNameLength +2
  ENDIF


  WRITE(*,*)' '
  WRITE(*,*)'---- Output ----'
  !-- CALL Output_Surface()
  CALL Output_Volume()
  IF(ABS(Loop_CVT)>0 .AND. Frame_Type<=3)  CALL Output_CC()


END SUBROUTINE Job_withFrame



