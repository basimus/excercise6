!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!     Check if a point is located inside the circumsphere of a tetrahedron
!!           by comparing the distance from the point to the   
!!           circumcentre of the sphere and the radius of the sphere
!!       
!!    @param[in]  IP : the ID of the point.
!!    @param[in]  IT : the ID of the element.
!!    @return     RA <0 : IP is in the circumsphere of IT;                    \n
!!                RA =0 : IP is on the circumsphere of IT;                    \n
!!                RA >0 : IP is out of the circumsphere of IT.
!!
!!    The meaning of return applies only if the cell has a positive volume.   \n     
!!    Reminder: The defination of 'positive volume' from
!!               Geometry3D_module.f90 and used in this code
!!               differs from predicates.c
!!       
!!    USE C function insphere() (code from predicates.c)
!<
!*******************************************************************************
FUNCTION Criterion_in_Sphere(IP,IT) RESULT(RA)

  USE common_Parameters
  USE Mapping3D
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: IP,IT
  REAL*8 :: RA
  REAL*8 :: p0(3), p1(3), p2(3), p3(3), p4(3), Poo(3), fMap(3,3)
  REAL*8 :: RR0, RR1, RR2, RR3, RR4, s1, s2

  p0(:) = Posit(:,IP)
  p1(:) = Posit(:,IP_Tet(1,IT))
  p2(:) = Posit(:,IP_Tet(2,IT))
  p3(:) = Posit(:,IP_Tet(3,IT))
  p4(:) = Posit(:,IP_Tet(4,IT))

  IF(useStretch .AND. BGSpacing%Model<0)THEN
     IF(BGSpacing%Model==-1)THEN
        fMap = BGSpacing%BasicMap(:,:)
     ELSE IF(Mapping_Choose_Model==1 .or. Mapping_Choose_Model==3)THEN
        fMap = fMap_Tet(:,:,IT)
     ELSE IF(Mapping_Choose_Model==2)THEN
        fMap = fMap_Point(:,:,IP)
     ENDIF
     p0 = Mapping3D_Posit_Transf(p0,fMap,1)
     p1 = Mapping3D_Posit_Transf(p1,fMap,1)
     p2 = Mapping3D_Posit_Transf(p2,fMap,1)
     p3 = Mapping3D_Posit_Transf(p3,fMap,1)
     p4 = Mapping3D_Posit_Transf(p4,fMap,1)
  ENDIF

  IF(usePower)THEN

     RR0    = RR_Point(IP)
     RR1    = RR_Point(IP_Tet(1,IT))
     RR2    = RR_Point(IP_Tet(2,IT))
     RR3    = RR_Point(IP_Tet(3,IT))
     RR4    = RR_Point(IP_Tet(4,IT)) 
     Poo(:) = Geo3D_Sphere_Cross (P1, P2, P3, P4, RR1, RR2, RR3, RR4)
     s1     = Geo3D_Distance_SQ(Poo,P1)
     s2     = Geo3D_Distance_SQ(Poo,P0) - RR0 + RR1
     RA     = (s2-s1)/s1
  
  ELSE
  
     call insphere(p1, p2, p3, p4, p0, RA)
  
  ENDIF

  RETURN
END FUNCTION Criterion_in_Sphere

