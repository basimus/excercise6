!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  CVT Loops and optimising smooth.
!!  @param[in]  common_Parameters::IP_Tet  : present mesh.
!!  @param[in]  common_Parameters::Posit   : present mesh.
!!  @param[in]  common_Parameters::CVTMeshInit
!!                       : recovered mesh before inserting any internal nodes.
!<
!*******************************************************************************
SUBROUTINE CVT_Reinsert(NB1)
  ! NB1 is the first point which is effected by this process, I assume this is to
  ! stop the code adjusting boundary nodes - SPW
  USE common_Parameters
  USE array_allocator
  USE SpacingStorage
  USE OptimisingModule
  USE RandomModule
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: NB1
  REAL*8, DIMENSION(:,:),  POINTER :: newPosit  !--(3,:)
  INTEGER :: IP, nbp, kway, ix, NBOld, iy, iz,Ibest,I,Loop,Istatus,Iin,NumLoops,doDumb
  REAL*8  :: dmax, xl, energy, d,optf,ftemp,fbest,dxl,xl2,xl3
  INTEGER, SAVE :: NRR = 0
  !Need to set up some variables for the LBFGS optimiser
  !Since this code has loads of global variables need to be careful with names
  INTEGER :: optn, optm, optiprint,DD,N,Ni,isFinished,idum = -1
  REAL*8 :: optfactr, optpgtol  = 1.0d-5
  CHARACTER(LEN=60) :: opttask, optcsave, CVTctl
  logical                :: optlsave(4)
  integer                :: optisave(44),limFlag
  real*8               :: optdsave(29),tempD(3)
  integer,  allocatable  :: optnbd(:), optiwa(:)
  real*8, allocatable  :: optx(:), optl(:), optu(:), optg(:), optwa(:),ptemp(:),Pt(:,:),Qu(:)
  REAL*8, DIMENSION(:), POINTER ::  vsum

  IF(.NOT. CircumUpdated) CALL Get_Tet_SphereCross(0)
  
  ! CircumUpdated is a global variable which keeps track of wether or not the current
  ! circum centers are valid or not...  
  ! Get_Tet_SphereCross(ie) calculates the voronoi for element ie and updates the global
  ! record, if you set ie=0 it calculates all elements - SPW
  
  !CALL Bound_Tet_SphereCentre(0)
  ! This checks if the vornoi center lies within an element.  If it doesn't it moves it to
  ! be on s surface - arbitarily moves it so the dual mesh is no-longer orthogonal! - SPW

  IF(CVT_Method>=0)THEN
     
     
     !Need to read parameters from the control file
     !CVTctl = 'CVT.ctl '
     
     !OPEN(84,file=TRIM(CVTctl),form='formatted', status='old', BLANK="NULL")
     !read(84,*) optfactr
     !read(84,*) optm
	 !read(84,*) N
	 !read(84,*) NumLoops
     !read(84,*) doDumb
     !CLOSE(84)
     
     

     WRITE(*,*)'Optimisation settings:'
    ! WRITE(*,*)'Factor=',optfactr
    ! WRITE(*,*)'M=',optm  
    ! WRITE(*,*)'Number of eggs=',N
    ! WRITE(*,*)'Number of MCS loops=',NumLoops
    ! WRITE(*,*)'Do the "dumb" CVT=',doDumb

    !These subroutines reload the meshes saved in CVTMeshInit into the Posit and IP_Tet arrays
    !Originally this mesh was stored before cosmetics in Job_noFrame it is now stored after cosmetics
    !before this routine is run - SPW
    !I've changed this back to the original, in the case when the mesh is stored after cosmetics the Next data
    !is not valid??
    !Debugging with this has lead me to believe that between where this used to be and where it is now in
    !Job_noFrame points are added without mesh connectivity being updated? or there is something wrong somewhere
    !else, need to set ELEMENT_BREAK_METHOD=0 to avoid the bug 
    !CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
    !CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
    
    CALL Next_Build() !This is called just incase we have changed any of the local structures
    
    ALLOCATE(newPosit(3,NB_Point))
    ALLOCATE(vsum(NB_Point))    
     	
    !Number of variables 3 * number of points
    optn = 3*(NB_Point-NB1+1)
    DD = optn
	doDumb = 1
    IF (doDumb==1) THEN
    	!Makes sense to start with a normal 'dumb' CVT algorithm to help improve things initially
    	CALL CVT_Position(newPosit, dmax, 1,energy,vsum)
    	!WRITE(29,*)'CVT_Reinsert:: CVT Energy=',energy
    
    	!This next line reduces the motion caused by CVT, technically this is wrong, Lloyds algorithm
    	!does not control the step size 
    	xl = BGSpacing%BasicSize/(dmax+BGSpacing%BasicSize)
    	xl = 1.0
    	DO ip = NB1, NB_Point    
		!Posit(:,ip) = newPosit(:,ip) !This would result in Lloyds algorithm
		Posit(:,ip) = (1.d0-xl)*Posit(:,ip) + xl*newPosit(:,ip)
   	 ENDDO

     

    	CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
    	CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
    	CALL Next_Build()
    	CALL Set_Domain_Mapping()
    	CALL Get_Tet_Circum(0)
    	CALL Get_Tet_Volume(0)
    	Mark_Point(1:NB_Point) = 0
    	CALL Insert_Points1(NB1, NB_Point, 0)  !I'm really not sure about this routine at all!
    	usePower = .FALSE.
    	CALL Remove_Point()
    	WRITE(*, *)' '
    	WRITE(*, *)'CVT_Reinsert:---- Swapping ----'
    	WRITE(29,*)' '
    	WRITE(29,*)'CVT_Reinsert:---- Swapping ----'
    	CALL Swap3D(2)
    	CALL Next_Build()

    END IF
    !Need to recalculate all the geometry
    



   
	!Number of variables 3 * number of points
        optn = 3*(NB_Point-NB1+1)
        optm = 5
        DD = optn
        NBOld = NB_Point
    optfactr = 1E+7
	
    !CALL Check_Mesh_Geometry2('before L-BFGS //NL')
    IF (1.EQ.1) THEN
    	!Need to prepare variables for the LBFGS optimiser
	


	
	
    	!LBFGS
        allocate (optx(optn), optl(optn), optu(optn), optg(optn))
    	allocate ( optnbd(optn))
    	allocate ( optiwa(3*optn) )
    	allocate ( optwa(2*optm*optn + 5*optn + 11*optm*optm + 8*optm) )
	 
	!Define the starting point
	DO ip = NB1, NB_Point
		ix = 3*(ip-NB1+1) - 2
		iy = 3*(ip-NB1+1) - 1
		iz = 3*(ip-NB1+1)
		optx(ix) = Posit(1,ip)
		optx(iy) = Posit(2,ip)
		optx(iz) = Posit(3,ip)

		CALL SpacingStorage_Get3DGridSize(BGSpacing,Posit(:,ip),tempD)
		optl(ix) = optx(ix)-0.5*tempD(1)
		optl(iy) = optx(iy)-0.5*tempD(2)
		optl(iz) = optx(iz)-0.5*tempD(3)
	
		optu(ix) = optx(ix)+0.5*tempD(1)
               optu(iy) = optx(iy)+0.5*tempD(2)
                optu(iz) = optx(iz)+0.5*tempD(3)
		optnbd(ix) = 2
		optnbd(iy) = 2
		optnbd(iz) = 2
	
	ENDDO
	 
	opttask = 'START'
	optiprint = 1
    optfactr = 1E+7

	!First itteration
	call setulb ( optn, optm, optx, optl, optu, optnbd, optf, &
                                                optg, optfactr, optpgtol, &
                                                optwa, optiwa, opttask, optiprint,&
                                                optcsave, optlsave, optisave, optdsave )
	



	CALL CVT_Position(newPosit, dmax, 1,energy,vsum)

    	WRITE(29,*)'CVT_Reinsert:: CVT Energy=',energy
    	optf = energy
    
    	!Compute gradient
    	DO ip = NB1,NBOld
              optg(3*(ip-NB1+1) - 2) = (Posit(1,ip)-newPosit(1,ip))*vsum(ip)*2.0
              optg(3*(ip-NB1+1) - 1) = (Posit(2,ip)-newPosit(2,ip))*vsum(ip)*2.0
              optg(3*(ip-NB1+1)) = (Posit(3,ip)-newPosit(3,ip))*vsum(ip)*2.0
    	END DO




	 
	DO WHILE(opttask(1:2).eq.'FG'.or.opttask.eq.'NEW_X'.or. &
               opttask.eq.'START') 

			   
		!     This is the call to the L-BFGS-B code.
         
         call setulb ( optn, optm, optx, optl, optu, optnbd, optf, &
						optg, optfactr, optpgtol, &
						optwa, optiwa, opttask, optiprint,&
						optcsave, optlsave, optisave, optdsave )
	
		if (opttask(1:2) .eq. 'FG') then
			 IF(NBOld>NB_Point) THEN
                                NB_Point = NBOld
                                CALL ReAllocate_Point()
                        END IF
	   
			!Need to calculate the gradient and energy at optx
			DO ip = NB1, NBOld
				 newPosit(1,ip) = optx(3*(ip-NB1+1) - 2)
				 newPosit(2,ip) = optx(3*(ip-NB1+1) - 1)
				 newPosit(3,ip) = optx(3*(ip-NB1+1))
			ENDDO
			
			DO ip = NB1, NBOld
				Posit(:,ip) = newPosit(:,ip)
			ENDDO
			
                        !--- recalculate geometry
                        CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
                            CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
                        CALL Next_Build()
                        CALL Set_Domain_Mapping()
                        CALL Get_Tet_Circum(0)
                        CALL Get_Tet_Volume(0)
                        Mark_Point(1:NB_Point) = 0
                        CALL Insert_Points1(NB1, NB_Point, 0)  !I'm really not sure about this routine at all!
                        usePower = .FALSE.
                        CALL Remove_Point()
                        WRITE(*, *)' '
                        WRITE(*, *)'CVT_Reinsert:---- Swapping ----'
                        WRITE(29,*)' '
                        WRITE(29,*)'CVT_Reinsert:---- Swapping ----'
                        CALL Swap3D(2)
                        CALL Next_Build()

 
			CALL CVT_Position(newPosit, dmax, 1,energy,vsum)
			
			!WRITE(29,*)'CVT_Reinsert:: CVT Energy=',energy
			optf = energy
			
			!Compute gradient
			DO ip = NB1,NB_Point
				optg(3*(ip-NB1+1) - 2) = (Posit(1,ip)-newPosit(1,ip))*vsum(ip)*2.0
				optg(3*(ip-NB1+1) - 1) = (Posit(2,ip)-newPosit(2,ip))*vsum(ip)*2.0
				optg(3*(ip-NB1+1)) = (Posit(3,ip)-newPosit(3,ip))*vsum(ip)*2.0
			END DO
			
			
		endif
	END DO

	 IF(NBOld>NB_Point) THEN
                     NB_Point = NBOld
                                CALL ReAllocate_Point()
                        END IF


	DO ip = NB1, NBOld
                 newPosit(1,ip) = optx(3*(ip-NB1+1) - 2)
                 newPosit(2,ip) = optx(3*(ip-NB1+1) - 1)
                 newPosit(3,ip) = optx(3*(ip-NB1+1))
        ENDDO
			 
     
	DO ip = NB1, NBOld
		Posit(:,ip) = newPosit(:,ip)
	ENDDO
                        !--- recalculate geometry
                        CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
                            CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
                        CALL Next_Build()
                        CALL Set_Domain_Mapping()
                        CALL Get_Tet_Circum(0)
                        CALL Get_Tet_Volume(0)
                        Mark_Point(1:NB_Point) = 0
                        CALL Insert_Points1(NB1, NB_Point, 0)  !I'm really not sure about this routine at all!
                        usePower = .FALSE.
                        CALL Remove_Point()
                        WRITE(*, *)' '
                        WRITE(*, *)'CVT_Reinsert:---- Swapping ----'
                        WRITE(29,*)' '
                        WRITE(29,*)'CVT_Reinsert:---- Swapping ----'
                        CALL Swap3D(2)
                        CALL Next_Build()
	ENDIF
	DEALLOCATE(newPosit)
	DEALLOCATE(vsum)

     
  ENDIF


 
  
  RETURN

END SUBROUTINE CVT_Reinsert


!*******************************************************************************
!>
!!  Compute barycenters of Voronoi cells responding to internal nodes
!!    NB1 to NB_Point,  stores them in newPosit.
!!  @param[in]  NB1  : the ID of first point considered to move.
!!  @param[out] newPosit : the barycenters of Voronoi cells.
!!  @param[out] dmax     : the maximum distance between the old and new position
!!  @param[out] energy     : the CVT energy
!<
!*******************************************************************************

SUBROUTINE CVT_Position (newPosit, dmax, NB1,energy,vsum)

  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: NB1
  REAL*8, INTENT(OUT)  :: newPosit(3,*), dmax, energy
  REAL*8  :: Pc(3),Q0(3),Q1(3),Q2(3),C1(3),C2(3),Vc(3)
  REAL*8  :: V,d
  REAL*8  :: Rho1,Rho2,Rho3,Rho4,Rho0,RhoQ1,RhoQ2,eps,fc(3)
  INTEGER :: i, ip, IT, ied, ip3(3), ip1, ip2, ITnb,enCount
  REAL*8, INTENT(OUT) ::  vsum(*)

  IF(.NOT. CircumUpdated)THEN
  	CALL Get_Tet_SphereCross(0)
  ENDIF
  !IF(Debug_Display>3)THEN
  !   CALL Check_Next(-1)
  !ENDIF

  eps = epsilon(0d0)
  
  vsum(1:NB_Point)  = 0.d0
  newPosit(1:3, 1:NB_Point) = 0.d0
  energy = 0.d0
  enCount = 0

  DO IT=1,NB_Tet
     !IF(ANY(IP_Tet(:,IT)<NB1)) CYCLE
     C1(:) = Sphere_Tet(1:3,IT)
	 Rho1 = (Scale_Point(IP_Tet(1,IT))+Scale_Point(IP_Tet(2,IT))+&
			Scale_Point(IP_Tet(3,IT))+Scale_Point(IP_Tet(4,IT)))/4.0d0
	 Rho1 = 1.0d0/Rho1
     DO i=1,4
        ITnb = Next_Tet(i,IT)
		ip3(1:3) = IP_Tet(iTri_Tet(1:3,i),IT)
        IF(ITnb<IT) THEN	
			!Need to reflect the circumcenter of IT about the face
			fc(:) = (Posit(:,ip3(1))+Posit(:,ip3(2))+Posit(:,ip3(3)))/3.0d0
			C2(:) = C1(:) + 2.0d0*(fc(:)-C1(:))
			Rho2 = (Scale_Point(ip3(1))+Scale_Point(ip3(2))+Scale_Point(ip3(3)))/3.0d0
			Rho2 = 1.0d0/Rho2
		ELSE
			!IF(ANY(IP_Tet(:,ITnb)<NB1)) CYCLE
			C2 = Sphere_Tet(1:3,ITnb)
			Rho2 = (Scale_Point(IP_Tet(1,ITnb))+Scale_Point(IP_Tet(2,ITnb))+&
					Scale_Point(IP_Tet(3,ITnb))+Scale_Point(IP_Tet(4,ITnb)))/4.0d0
			Rho2 = 1.0d0/Rho2
        END IF

        DO ied=1,3
           ip1 = ip3(ied)
           ip2 = ip3(MOD(ied,3)+1)
           Q0  = (Posit(:,ip1)+Posit(:,ip2))/2.d0
		   Rho0 = (Scale_Point(ip1)+Scale_Point(ip2))/2.d0
		   Rho0 = 1.0d0/Rho0
           IF(ip1>=NB1)THEN
              Q1  = Posit(:,ip1)
			  RhoQ1 = 1.0d0/Scale_Point(ip1)
              Q2  = Q0
			  RhoQ2 = Rho0
              Pc  = (Q1+Q2+C1+C2)/4.d0
              V   =  Geo3D_Tet_Volume6(Q1,Q2,C2,C1)*((Rho2+Rho1+RhoQ1+RhoQ2)/4.0d0)
              !---XIE MODIFICATION
              V   = dabs(V)
              !===END MODIFICATION
              d =    (Posit(1,ip1)-Pc(1))**2   &
                   + (Posit(2,ip1)-Pc(2))**2   &
                   + (Posit(3,ip1)-Pc(3))**2
              enCount = enCount + 1
              energy = energy + V*d
              vsum(ip1) = vsum(ip1) + V
              newPosit(:,ip1) = newPosit(:,ip1) + V * Pc(:)
           ENDIF

           IF(ip2>=NB1)THEN
              Q1  = Q0
			  RhoQ1 = Rho0
              Q2  = Posit(:,ip2)
			  RhoQ2 = 1.0d0/Scale_Point(ip2)
              Pc  = (Q1+Q2+C1+C2)/4.d0
              V   =  Geo3D_Tet_Volume6(Q1,Q2,C2,C1)*((Rho2+Rho1+RhoQ1+RhoQ2)/4.0d0)
              !---XIE MODIFICATION
              V   = dabs(V)
              !===END MODIFICATION
              d =    (Posit(1,ip2)-Pc(1))**2   &
                   + (Posit(2,ip2)-Pc(2))**2   &
                   + (Posit(3,ip2)-Pc(3))**2
              enCount = enCount + 1
              energy = energy + V*d

              vsum(ip2) = vsum(ip2) + V
              newPosit(:,ip2) = newPosit(:,ip2) + V * Pc(:)
           ENDIF
        ENDDO
     ENDDO
  ENDDO

  dmax = 0.d0
  energy = 0.0d0
  DO ip = NB1,NB_Point
     IF (vsum(ip).GT.eps) THEN
		 newPosit(:,ip) = newPosit(:,ip) / vsum(ip)
		 d =    (newPosit(1,ip)-Posit(1,ip))**2   &
			  + (newPosit(2,ip)-Posit(2,ip))**2   &
			  + (newPosit(3,ip)-Posit(3,ip))**2
		 energy = energy + d
		 dmax = MAX(dmax,d)
	  END IF
  ENDDO
  energy = energy/(NB_Point-NB1)

  dmax = DSQRT(dmax)
  energy = energy/NB_Point
   

  RETURN
END SUBROUTINE CVT_Position


!*******************************************************************************
!>
!!   Optimising smooth.
!!   Adjust the position and radius of every point
!!      to make the circum-centre of every cell to move close to its barycenter.
!!   @param[in]  Kway   = 1 : adjust the position only.                       \n
!!                      = 2 : adjust the radius only.                         \n
!!                      else, adjust both.
!!   @param[in] common_Parameters::Mark_Surf_Point 
!!                      for a boundary point IP, if Mark_Surf_Point(IP)>0, 
!!                       do not modify the radius.
!<
!*******************************************************************************
SUBROUTINE SmoothVor (Kway)

  USE common_Constants
  USE common_Parameters
  USE array_allocator
  USE Mapping3D
  USE OptimisingModule
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: Kway
  INTEGER :: ismoo, iter, IP, idmm
  REAL*8  :: dd0, dmov, dd, ppt(4), ppt0(4)

  CircumUpdated = .TRUE.
  VolumeUpdated = .TRUE.
  dd0  = 1.d-4 * BGSpacing%MinSize

  !--- Associate points with elements
  CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)


  !--- smooth out the grid in Loop_Smooth steps
  DO ismoo=1,Loop_Smooth
     dmov = 0

     DO IP=1,NB_Point

        IF(Kway==1)THEN
           IF(IP<=NB_BD_Point) CYCLE
           idmm = 3
        ELSE IF(Kway==2)THEN
           IF(ASSOCIATED(Mark_Surf_Point) .AND. IP<=NB_BD_Point)THEN
              IF(Mark_Surf_Point(IP)>0) CYCLE
           ENDIF
           idmm = 1
        ELSE
           IF(IP>NB_BD_Point)THEN
              idmm = 4
           ELSE
              IF(ASSOCIATED(Mark_Surf_Point))THEN
                 IF(Mark_Surf_Point(IP)>0) CYCLE
              ENDIF
              idmm = 1
           ENDIF
        ENDIF


        !--- set parameters for FUNCTION Optimising_GetValue()
        OptVariable%IdNode = IP
        OptVariable%IdxFun = 2    

        !--- call Optimising
        iter = 30*idmm
        CALL Optimising_Set(idmm, iter, dd0)
        OptParameter%Idistb = 10

        !--- set variable marching function Optimising_GetValue()
        IF(idmm==1)THEN
           ppt0(1)   = RR_Point(IP) / (10*BGSpacing%BasicSize)
        ELSE IF(idmm==3)THEN
           ppt0(1:3) = Posit(:,IP)
        ELSE IF(idmm==4)THEN
           ppt0(1:3) = Posit(:,IP)
           ppt0(4)   = RR_Point(IP) / (10*BGSpacing%BasicSize)
        ENDIF

        CALL Optimising_Powell(ppt0(1:idmm), ppt(1:idmm))

        IF(OptParameter%Isucc==0) CYCLE    !--- no better solution found
        IF(OptParameter%Isucc==4) CYCLE    !--- original solution is the best.

        IF(idmm==1)THEN
           RR_Point(IP) = ppt(1) * (10*BGSpacing%BasicSize)
        ELSE IF(idmm==3)THEN
           Posit(:,IP)  = ppt(1:3)
        ELSE IF(idmm==4)THEN
           Posit(:,IP)  = ppt(1:3)
           RR_Point(IP) = ppt(4) * (10*BGSpacing%BasicSize)
        ENDIF
        CALL Optimising_Distance (ppt0(1:idmm), ppt(1:idmm), dd)
        dmov = MAX(dmov,dd )
     ENDDO

     WRITE(29,'(a,i3,a,E12.3)') 'loop:',ismoo,' max move:',dmov
     IF(dmov<2*dd0) EXIT
  ENDDO

  CircumUpdated = .FALSE.
  VolumeUpdated = .FALSE.

  CALL LinkAssociation_Clear(PointAsso)

  RETURN
END SUBROUTINE SmoothVor


!*******************************************************************************
!>
!!   Optimising smooth by PSO.
!!   Adjust the position and radius of every point
!!      to make the circum-centre of every cell to move close to its barycenter.
!!   @param[in]  Kway   = 1 : adjust the position only.                       \n
!!                      = 2 : adjust the radius only.                         \n
!!                      else, adjust both.
!!   @param[in] common_Parameters::Mark_Surf_Point 
!!                      for a boundary point IP, if Mark_Surf_Point(IP)>0, 
!!                       do not modify the radius.
!<
!*******************************************************************************
SUBROUTINE SmoothVor_PSO (Kway)

  USE common_Constants
  USE common_Parameters
  USE array_allocator
  USE Mapping3D
  USE OptimisingModule
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: Kway
  INTEGER :: ismoo, i, n, IT, IP, ipt, idmm, Isucc, Nsamp, Iter
  REAL*8  :: dd0, dd, dmov
  REAL*8  :: PPs(4,1000), ppt(4)

  CircumUpdated = .TRUE.
  VolumeUpdated = .TRUE.
  dd0  = 1.d-4 * BGSpacing%MinSize

  !--- Associate points with elements
  CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)


  !--- smooth out the grid in Loop_Smooth steps
  DO ismoo=1,Loop_Smooth
     dmov = 0

     Mark_Point(1:NB_Point) = 0

     DO IP=1,NB_Point

        IF(Kway==1)THEN
           IF(IP<=NB_BD_Point) CYCLE
           idmm = 3
        ELSE IF(Kway==2)THEN
           IF(ASSOCIATED(Mark_Surf_Point) .AND. IP<=NB_BD_Point)THEN
              IF(Mark_Surf_Point(IP)>0) CYCLE
           ENDIF
           idmm = 1
        ELSE
           IF(IP>NB_BD_Point)THEN
              idmm = 4
           ELSE
              IF(ASSOCIATED(Mark_Surf_Point))THEN
                 IF(Mark_Surf_Point(IP)>0) CYCLE
              ENDIF
              idmm = 1
           ENDIF
        ENDIF

        !--- set parameters for FUNCTION Optimising_GetValue()
        OptVariable%IdNode = IP
        OptVariable%IdxFun = 2

        !--- call Optimising
        iter = 30*idmm
        CALL Optimising_Set(idmm, iter, dd0)
        OptParameter%Idistb = 0

        Nsamp = 1
        IF(idmm==1)THEN
           PPs(1,  Nsamp) = RR_Point(IP) / (10*BGSpacing%BasicSize)
        ELSEIF(idmm==3)THEN
           PPs(1:3,Nsamp) = Posit(:,IP)
        ELSEIF(idmm==4)THEN
           PPs(1:3,Nsamp) = Posit(:,IP)
           PPs(4,  Nsamp) = RR_Point(IP) / (10*BGSpacing%BasicSize)
        ENDIF
        Mark_Point(IP) = IP

        CALL LinkAssociation_List(IP, List_Length, Its_List, PointAsso)
        DO n = 1,List_Length
           IT = Its_List(n)
           DO i = 1,4
              ipt = IP_Tet(i,IT)
              IF(Mark_Point(ipt)==IP) CYCLE
              Mark_Point(ipt) = IP
              Nsamp = Nsamp + 1

              IF(idmm==1)THEN
                 PPs(1,  Nsamp) = 0.2*RR_Point(ipt) / (10*BGSpacing%BasicSize) + 0.8*PPs(1,  1)
              ELSEIF(idmm==3)THEN
                 PPs(1:3,Nsamp) = 0.2*Posit(:,ipt)  + 0.8*PPs(1:3,1)
              ELSEIF(idmm==4)THEN
                 PPs(1:3,Nsamp) = 0.2*Posit(:,ipt)  + 0.8*PPs(1:3,1)
                 PPs(4,  Nsamp) = 0.2*RR_Point(ipt) / (10*BGSpacing%BasicSize) + 0.8*PPs(4,  1)
              ENDIF
           ENDDO
        ENDDO


        !IF(ABS(CVT_Method)==2)THEN
        !   CALL Optimising_PSO   (Nsamp, PPs(1:idmm,1:Nsamp), ppt(1:idmm))
        !ELSE
           CALL Optimising_Cuckoo(Nsamp, PPs(1:idmm,1:Nsamp), ppt(1:idmm))
        !ENDIF

        IF(OptParameter%Isucc==0) CYCLE

        IF(idmm==1)THEN
           RR_Point(IP) = ppt(1) * (10*BGSpacing%BasicSize)
        ELSE IF(idmm==3)THEN
           Posit(:,IP)  = ppt(1:3)
        ELSE IF(idmm==4)THEN
           Posit(:,IP)  = ppt(1:3)
           RR_Point(IP) = ppt(4) * (10*BGSpacing%BasicSize)
        ENDIF
        CALL Optimising_Distance (PPs(1:idmm, 1), ppt(1:idmm), dd)
        dmov = MAX(dmov,dd )
     ENDDO

     WRITE(29,'(a,i3,a,E12.3)') 'loop:',ismoo,' max move:',dmov
     IF(dmov<2*dd0) EXIT
  ENDDO

  CircumUpdated = .FALSE.
  VolumeUpdated = .FALSE.

  CALL LinkAssociation_Clear(PointAsso)

  RETURN
END SUBROUTINE SmoothVor_PSO

!*******************************************************************************
!>
!<
!*******************************************************************************
SUBROUTINE SmoothRR_PSO ( )

  USE common_Parameters
  USE OptimisingModule
  IMPLICIT NONE

  INTEGER :: i, n, IT, Nsamp, ip
  REAL*8  :: PPs(50,500), ppt(100), dd0, RR11, RR22
  REAL*8, DIMENSION(:), POINTER :: RRd

  IF(NB_POD>50 .OR. NB_POD<20)THEN
     WRITE(29,*)' Error--- not expected NB_POD=',NB_POD
     CALL Error_Stop('SmoothRR_PSO :: NB_POD')
  ENDIF

  CircumUpdated = .TRUE.
  VolumeUpdated = .TRUE.

  !--- choose those activity nodes for Optimising
  ALLOCATE(RRd(NB_Point))
  DO ip = 1, NB_Point
     RRd(ip) =  maxval(RRS_POD(ip, NB_POD-10 : NB_POD))   &
              - minval(RRS_POD(ip, NB_POD-10 : NB_POD))
  ENDDO
  NB_PODva = 0
  dd0 = 1.d-3 * maxval(RRd(1:NB_Point))
  DO ip = 1, NB_Point
     IF(RRd(ip)<dd0)THEN
        Mark_Point(ip) = 0
     ELSE
        NB_PODva = NB_PODva + 1
        Mark_Point(NB_PODva) = ip
     ENDIF
  ENDDO
  DEALLOCATE(RRd)
  WRITE(29,*)' '
  WRITE(29,*)' SmoothRR_PSO:: number of POD modes:',NB_POD
  WRITE(29,*)' SmoothRR_PSO:: number of POD nodes:',NB_PODva

  !--- set samples
  Nsamp = 10*NB_POD
  DO i = 1, Nsamp
     IF(i<=NB_POD)THEN
        PPs(1:NB_POD, i) = 0
        PPs(i, i) = 1
     ELSE
        CALL Random_LevyWalk (PPs(1:NB_POD,i), NB_POD)
     ENDIF
  ENDDO

  !--- call Optimising
  dd0  = 1.d-4 
  OptVariable%IdxFun = -2
  CALL Optimising_Set(NB_POD, 200, dd0)
  CALL Optimising_Cuckoo(Nsamp, PPs(1:NB_POD,1:Nsamp), ppt(1:NB_POD))

  RR22 = SUM(ppt(1:NB_POD))
  ppt(:) = ppt(:) + (1.d0-RR22)/NB_POD     
  DO i = 1, NB_PODva
     ip = Mark_Point(i)
     RR_Point(ip) = 0
     DO n = 1, NB_POD
        RR_Point(IP) = RR_Point(IP) + ppt(n)* RRS_POD(IP, n)
     ENDDO
  ENDDO
  
  WRITE(29,*)'  ppt(:) = '
  WRITE(29,'(6E12.3)')ppt(1:NB_POD)
  WRITE(29,*)' '

  CircumUpdated = .FALSE.
  VolumeUpdated = .FALSE.

  RETURN
END SUBROUTINE SmoothRR_PSO


