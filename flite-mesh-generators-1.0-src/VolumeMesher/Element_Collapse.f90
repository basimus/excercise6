!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!     Remove elements with tiny angles.
!!
!!    Reminder:                                                                       \n
!!      Make sure that the first common_Parameters::NB_BD_Point points
!!           are boundary points.                                                     \n
!!      common_Parameters::NEXT_Tet is not needed and will be ruined in the process.  \n
!!      common_Parameters::PointAsso will be built and kept updated in the process
!!           but destroyed at final.                                                  \n   
!!      Geometry & Mapping of elements will be lost.                                  \n
!!      common_Parameters::NB_Tet & common_Parameters::NB_Point will be updated       
!<      
!*******************************************************************************
SUBROUTINE Element_Collapse( )

  USE common_Constants
  USE common_Parameters
  USE Mapping3D
  IMPLICIT NONE

  REAL*8  :: p1(3),p2(3),p3(3),p4(3), pp(3,4)
  REAL*8  :: fMap(3,3), fMaps(3,3,4)
  REAL*8  :: ang,volmin,angerr
  INTEGER :: IT, IP, IB, IEdge, NB_MovePt, KBB
  INTEGER :: iTarget, ipmove, ipstay
  INTEGER :: i, jj, it1, is, nnp, nnt, Isucc

  CircumUpdated = .FALSE.      !--- geometry of the grids will be lost
  VolumeUpdated = .FALSE.      !--- volume   of the grids will be lost

  angErr = PI
  DO IT = 1,NB_Tet  
        pp(:,1:4) = Posit(:,IP_Tet(1:4,IT))
        IF(BGSpacing%Model<0)THEN
           IF(BGSpacing%Model==-1)THEN
              fMap(:,:) = BGSpacing%BasicMap(:,:)
           ELSE
              fMap(:,:) = fMap_Tet(:,:,IT)
           ENDIF
           DO i=1,4
              pp(:,i) = Mapping3D_Posit_Transf(pp(:,i),fMap,1)
           ENDDO
        ELSE IF(BGSpacing%Model>1)THEN
           !--- no transfer in need since
           !--- a fixed scale will not affect the angle
           !--- pp(:,1:4)   = pp(:,1:4) / Scale_Tet(IT)
        ENDIF

        ang = Geo3D_Tet_Quality(pp(:,1),pp(:,2),pp(:,3),pp(:,4), 2)
        IF(Debug_Display>2 .AND. ang<=0) THEN
              IF(ang<angErr)THEN
                 angErr = ang
                 WRITE(*, *)' '
                 WRITE(*, *)'Warning--- : element with negative angle ',ang
                 WRITE(29,*)' '
                 WRITE(29,*)'Warning--- : element with negative angle ',ang
                 WRITE(29,*)'IT=',IT,' ips=',IP_Tet(1:4,IT)
              ENDIF
        ENDIF
        Quality_Tet(1,IT) = ang
  ENDDO

  NB_MovePt = 1
  Mark_Tet( 1:NB_Tet )     = 0
  DO WHILE(NB_MovePt>0)
     NB_MovePt = 0

     !--- Search all the elements to which a point belongs

     Mark_Point( 1:NB_Point ) = 0

     !--- Associate points with elements
     CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)

     !--- Check element to find out if it has a Tiny Angle

     Loop_IT : DO IT = 1,NB_Tet
        IF(Mark_Tet(IT)==1 .OR. Mark_Tet(IT)==-1) CYCLE Loop_IT
        IF(IP_Tet(4,IT)<=0) CYCLE Loop_IT

        volmin = Quality_Tet(1,IT)
        IF(volmin>=Collapse_Angle)THEN
           !--- a good element
           Mark_Tet(IT) = -1
           CYCLE Loop_IT
        ENDIF


        !--- A Tiny Angle found
        !--- Search the best pair (ipmove->ipstay) to collapse
  !      if(volmin<0.d0) volmin=0.d0
        ipmove = 0
        IF(BoundCell_Split==0)THEN
           KBB = 0
        ELSE IF(BoundCell_Split==2)THEN
           KBB = 1
        ELSE IF(volmin>0.0175)THEN
           KBB = 1
        ELSE
           KBB = 0
        ENDIF
        
        DO i = 1, 4
           ip  = IP_Tet(i,IT)
           IF(ip <= NB_BD_Point) CYCLE  !-- a boundary node
           CALL Collapse_Check(ip,4,IP_Tet(:,IT),2,iTarget,volmin,1,KBB)
           IF(iTarget>0)THEN
              ipmove = ip
              ipstay = IP_Tet(iTarget,IT)
           ENDIF
        ENDDO
        IF(ipmove==0) CYCLE Loop_IT

        !---remove ipmove to ipstay
        NB_MovePt = NB_MovePt + 1

        !--- List all elements surrounding point ipmove in ITs_List
        CALL LinkAssociation_List(ipmove, List_Length, ITs_List, PointAsso)

        DO jj = 1,List_Length
           it1 = ITs_List(jj)
           Mark_Tet(it1) = 1
           Quality_Tet(1,it1) = Quality_Tet(2,it1)
        ENDDO
        Mark_Point(ipmove) = ipstay

        CALL Collapse_PointPair(ipMove,ipStay,0)
        CALL Collapse_identical(ipStay)

     ENDDO Loop_IT

     !---Recount points and elements
     nnt = 0
     DO IT  = 1, NB_Tet
        IF(IP_Tet(4,IT)<=0) CYCLE
        nnt           = nnt + 1
        IP_Tet(:,nnt) = IP_Tet(:,IT)
        Quality_Tet(1,nnt) = Quality_Tet(1,IT)
        Mark_Tet(nnt) = Mark_Tet(IT)
        IF(Mark_Tet(nnt)>0) Mark_Tet(nnt) = 0
        IF(BGSpacing%Model<-1 .OR. BGSpacing%Model>1)THEN
           CALL Get_Tet_Mapping(nnt)
        ENDIF
     ENDDO
     NB_Tet   = nnt

     DO IP = 1, NB_Point
        IF(Mark_Point(IP)/=0) Mark_Point(IP) = -999
     ENDDO
     CALL Remove_Point_inOrder()
     WRITE(29,*) '......  NB_MovePt = ',NB_MovePt,' nelem = ',NB_Tet

     !--- Repeat the procedure if necessary
  ENDDO

     Mark_Point( 1:NB_Point ) = -999
     DO  IT = 1, NB_Tet
        Mark_Point(IP_Tet(1:4,IT)) = 0
     ENDDO
     CALL Remove_Point_inOrder()
     
  RETURN
END SUBROUTINE Element_Collapse



!*******************************************************************************
!>
!!    Find the best target of moving point ipMove.     
!!    @param[in]  ipMove                : the moving node.   
!!    @param[in]  num_ipStay            : the number of possible target nodes.
!!    @param[in]  ipStay(1:num_ipStay)  :  the possible target nodes.
!!    @param[in]  IndxQual :  quality index.                                  \n
!!                          =1   check the volume of the tetrahedron.         \n
!!                          =2   check the minimum Dihedral_Angle.
!!    @param[in]  volmin  :   basic requirement of quality.   
!!    @param[in]  common_Parameters::Mark_Tet
!!    @param[in]  Mark :      the element with   
!!                            common_Parameters::Mark_Tet (:) = Mark
!!                            is an untouchable element.    
!!                         = -999 : no refered.   
!!    @param[in]  KBB  :   =1  avoid to make elements with all four nodes 
!!                             being boundary nodes (<=NB_BD_Point).         \n
!!                         =0  no referred.
!!    @param[out] iTarget :  1<=iTarget<=num_ipStay,                         \n
!!                       ipStay(iTarget) is the best movment for ipMove.     \n
!!                       =0,  no movement is suitable.                       \n
!!                       =-1, a untouchable element get involved.
!!    @param[out]  volmin  :  the quality under the best movement (if existing).
!<       
!*******************************************************************************
SUBROUTINE Collapse_Check(ipMove,num_ipStay,ipStay,IndxQual,iTarget,volmin,Mark,KBB)

  USE Geometry3DAll
  USE common_Parameters
  IMPLICIT NONE
  INTEGER, INTENT(in)  :: ipMove, num_ipStay,  ipStay(*), IndxQual, Mark, KBB
  INTEGER, INTENT(out) :: iTarget
  REAL*8,  INTENT(inout) :: volmin
  INTEGER :: istay, is, jj, IT, ipp(4), i
  REAL*8  :: p1(3),p2(3),p3(3),p4(3)
  REAL*8  :: fMap(3,3), fMaps(3,3,4), Scalars(4), Scalar
  REAL*8  :: did,dim1

  !--- List all elements surrounding point ipMove in the array ITs_List
  CALL LinkAssociation_List(ipMove, List_Length, ITs_List, PointAsso)

  IF(Mark/=-999)THEN
     DO jj = 1,List_Length
        IT = ITs_List(jj)
        volmin = MIN(volmin, Quality_Tet(1,IT))
        IF(Mark_Tet(it)==Mark)THEN
           iTarget=-1
           RETURN
        ENDIF
     ENDDO
  ENDIF 
        
  iTarget = 0

  Loop_istay : DO istay=1, num_ipStay
     is = ipStay(istay)
     IF(is==ipMove) CYCLE Loop_istay

     dim1   = 1.d32

     !--- Check if form a 4-boundary-node element
     IF(KBB==1 .AND. is<=NB_BD_Point)THEN
        DO jj = 1,List_Length
           IT = ITs_List(jj)
           DO i = 1,4
               IF(IP_Tet(i,IT)==ipmove) CYCLE
               IF(IP_Tet(i,IT)>NB_BD_Point) EXIT
           ENDDO
           IF( i>4 ) CYCLE Loop_istay          
        ENDDO
     ENDIF

     !--- Check the quality of those prospective elements
     !--- The best target will be chosen by maximizing the minimum quality
     Loop_jj : DO jj = 1,List_Length
        IT = ITs_List(jj)
        ipp(1:4) = IP_Tet(1:4,IT)
        DO i=1, 4
           IF(ipp(i)==is) CYCLE Loop_jj
           IF(ipp(i)==ipmove) ipp(i) = is
        ENDDO
                
        p1(:) = Posit(:,ipp(1))
        p2(:) = Posit(:,ipp(2))
        p3(:) = Posit(:,ipp(3))
        p4(:) = Posit(:,ipp(4))
        IF(useStretch .AND. BGSpacing%Model<0)THEN
           IF(BGSpacing%Model==-1)THEN
              fMap(:,:) = BGSpacing%BasicMap(:,:)
           ELSE
              fMaps(:,:,1) = fMap_Point(:,:,ipp(1))
              fMaps(:,:,2) = fMap_Point(:,:,ipp(2))
              fMaps(:,:,3) = fMap_Point(:,:,ipp(3))
              fMaps(:,:,4) = fMap_Point(:,:,ipp(4))
              fMap  = Mapping3D_Mean(4,fMaps,Mapping_Interp_Model)
           ENDIF
           p1(:) = Mapping3D_Posit_Transf(p1,fMap,1)
           p2(:) = Mapping3D_Posit_Transf(p2,fMap,1)
           p3(:) = Mapping3D_Posit_Transf(p3,fMap,1)
           p4(:) = Mapping3D_Posit_Transf(p4,fMap,1)
        ENDIF

        did = Geo3D_Tet_Quality(p1,p2,p3,p4,IndxQual)
        IF(IndxQual==1 .AND. useStretch .AND. BGSpacing%Model>1)THEN
            Scalars(1:4)  = Scale_Point(ipp(1:4))
            Scalar = Scalar_Mean(4,Scalars,Mapping_Interp_Model)
            did    =  did / Scalar**3
        ENDIF
        Quality_Tet(3,IT) = did

        IF(did<volmin) CYCLE Loop_istay
        dim1 = MIN(dim1,did)
     ENDDO Loop_jj

     iTarget = istay
     volmin  = dim1
     IF(Mark/=-999)THEN
        DO jj = 1,List_Length
           IT = ITs_List(jj)
           Quality_Tet(2,IT) = Quality_Tet(3,IT)
        ENDDO
     ENDIF 

  ENDDO Loop_istay


  RETURN

END SUBROUTINE Collapse_Check

!*******************************************************************************
!>
!!     Collapse an edge ipMove--ipStay by moving ipMove to ipStay.
!!     @param[in]  ipMove  : the moving node.   
!!     @param[in]  ipStay  : the target node.   
!!     @param[in]  kUpdate :  if =1, update common_Parameters::NEXT_Tet.
!! 
!!     Update:   common_Parameters::IP_Tet,  common_Parameters::PointAsso.    \n                                        \n
!!     Geometry & Mapping of elements will be lost after collapsing
!<       
!*******************************************************************************
SUBROUTINE Collapse_PointPair(ipMove,ipStay,kUpdate)

  USE common_Parameters
  IMPLICIT NONE
  INTEGER, INTENT(in)  :: ipMove, ipStay, kUpdate
  INTEGER :: jj, IT, k, ke, itStay, itMove
  INTEGER :: ITs_Save(ChainTotalLength), List_Length_Save

  !--- List all elements surrounding point ipMove in the array ITs_List
  CALL LinkAssociation_List(ipMove, List_Length, ITs_List, PointAsso)
  !--- copy ITs_List since it will be affected by LinkAssociation_Remove/Add
  List_Length_Save        = List_Length
  ITs_Save(1:List_Length) = ITs_List(1:List_Length)

  IF(kUpdate==1)THEN
     !--- update NEXT_Tet
     DO jj = 1,List_Length
        IT = ITs_List(jj)

        IF(  IP_Tet(1,IT)==ipstay .OR. IP_Tet(2,IT)==ipstay .OR.   &
             IP_Tet(3,IT)==ipstay .OR. IP_Tet(4,IT)==ipstay     ) THEN
           DO k=1,4
              ke = NEXT_Tet(k,IT)
              IF(  IP_Tet(1,ke)/=ipMove .AND. IP_Tet(2,ke)/=ipMove .AND.   &
                   IP_Tet(3,ke)/=ipMove .AND. IP_Tet(4,ke)/=ipMove  ) itStay = ke
              IF(  IP_Tet(1,ke)/=ipstay .AND. IP_Tet(2,ke)/=ipstay .AND.   &
                   IP_Tet(3,ke)/=ipstay .AND. IP_Tet(4,ke)/=ipstay  ) itMove = ke
           ENDDO

           DO k=1,4
              IF(NEXT_Tet(k,itStay)==IT) NEXT_Tet(k,itStay) = itMove
              IF(NEXT_Tet(k,itMove)==IT) NEXT_Tet(k,itMove) = itStay
           ENDDO
        ENDIF

     ENDDO
  ENDIF

  !--- update connectivities and LinkAssociation

  DO jj = 1,List_Length_Save
     IT = ITs_Save(jj)
     CALL LinkAssociation_Remove(4, IT,IP_Tet(:,IT),PointAsso)

     IF(  IP_Tet(1,IT)==ipstay .OR. IP_Tet(2,IT)==ipstay .OR.   &
          IP_Tet(3,IT)==ipstay .OR. IP_Tet(4,IT)==ipstay     )THEN
        IP_Tet(4,IT) = -IP_Tet(4,IT)
     ELSE
        WHERE(IP_Tet(:,IT)==ipMove) IP_Tet(:,IT) = ipStay
        CALL LinkAssociation_AddCell(4, IT,IP_Tet(:,IT),PointAsso)
     ENDIF
  ENDDO

  RETURN
END SUBROUTINE Collapse_PointPair

!*******************************************************************************
!>
!!     Two flat elements with the same connectivities may appear 
!!               after element collapse during boundary recovering.
!!     This routine is for checking and removing such element pairs.
!! 
!!     @param[in]  ipStay  : the target node belonged the two elements.   
!! 
!!     Update:   common_Parameters::IP_Tet,  common_Parameters::PointAsso.    \n                                        \n
!!     Next_Tet, Geometry & Mapping of elements will be lost after collapsing
!<       
!*******************************************************************************
SUBROUTINE Collapse_identical(ipStay)

  USE common_Parameters
  IMPLICIT NONE
  INTEGER, INTENT(in)  :: ipStay
  INTEGER :: ii, jj, it1, it2, i, ip3(3), iq3(3)
  INTEGER :: ITs_Save(ChainTotalLength), List_Length_Save

  !--- List all elements surrounding point ipMove in the array ITs_List
  CALL LinkAssociation_List(ipStay, List_Length, ITs_List, PointAsso)
  !--- copy ITs_List since it will be affected by LinkAssociation_Remove/Add
  List_Length_Save        = List_Length
  ITs_Save(1:List_Length) = ITs_List(1:List_Length)

  !--- update connectivities and LinkAssociation

  DO ii = 1, List_Length_Save
     it1 = ITs_Save(ii)
     IF(IP_Tet(4,it1)<0) CYCLE
     i   = which_NodeinTet(ipStay, IP_Tet(1:4,it1))
     ip3 = IP_Tet(iTri_Tet(1:3,i),it1)
     DO jj = ii+1, List_Length_Save
        it2 = ITs_Save(jj)
        IF(IP_Tet(4,it2)<0) CYCLE
        i   = which_NodeinTet(ipStay, IP_Tet(1:4,it2))
        iq3 = IP_Tet(iTri_Tet(1:3,i),it2)
        if(ip3(1)/=iq3(1) .and. ip3(1)/=iq3(2) .and. ip3(1)/=iq3(3)) cycle
        if(ip3(2)/=iq3(1) .and. ip3(2)/=iq3(2) .and. ip3(2)/=iq3(3)) cycle
        if(ip3(3)/=iq3(1) .and. ip3(3)/=iq3(2) .and. ip3(3)/=iq3(3)) cycle
        
        CALL LinkAssociation_Remove(4, it1,IP_Tet(:,it1),PointAsso)
        CALL LinkAssociation_Remove(4, it2,IP_Tet(:,it2),PointAsso)
        IP_Tet(4,it1) = -IP_Tet(4,it1)
        IP_Tet(4,it2) = -IP_Tet(4,it2)
        EXIT
     ENDDO
  ENDDO

  RETURN
END SUBROUTINE Collapse_identical


!*******************************************************************************
!>
!!    Collapse moving point ipMove by swaping.     
!!         (Added in Sep. 2011. More test in need.)
!!    @param[in]  ipMove                : the moving node.   
!!    @param[in]  num_ipStay            : the number of possible target nodes.
!!    @param[in]  ipStay(1:num_ipStay)  : the possible target nodes.
!!    @param[in]  IndxQual :  quality index.                                  \n
!!                          =1   check the volume of the tetrahedron.         \n
!!                          =2   check the minimum Dihedral_Angle.
!!    @param[in]  volmin  :   basic requirement of quality.
!!    @param[out]  num_ipStay           : the number of possible rest target nodes.
!!                                      =0 ipMove has been removed.
!!    @param[out]  ipStay(1:num_ipStay) : the rest possible target nodes.
!<       
!*******************************************************************************
SUBROUTINE Collapse_withSwap(ipMove,num_ipStay,ipStay,IndxQual,volmin)

  USE Geometry3DAll
  USE common_Parameters
  USE HeapTree
  IMPLICIT NONE
  INTEGER, INTENT(in)  :: ipMove, IndxQual
  INTEGER, INTENT(inout)  :: num_ipStay,  ipStay(*)
  REAL*8,  INTENT(in)  :: volmin
  INTEGER :: istay, is, jj, IT, ipp(4), i, nis, nfail, Isucc
  REAL*8  :: did
  TYPE(HeapTreeType) :: Heap
  INTEGER :: ITs_Save(4)

  !--- build a heap tree for the length of edges from ipmove

  Heap%Ascending = .FALSE.
  CALL HeapTree_Allocate (Heap, num_ipStay)

  DO istay=1, num_ipStay
     is  = ipStay(istay)
     did = Geo3D_Distance_SQ(Posit(:,Ipmove), Posit(:,Is))
     CALL HeapTree_AddValue (Heap, did)
  ENDDO

  !--- swap those edges from ipmove with the longest first
  nis   = 0
  nfail = 0
  DO WHILE(nis+4<num_ipStay .and. nfail<=num_ipStay)

     istay = Heap%toList(1)
     is    = ipStay(istay)
     if(is==0) stop 'dsfdvnufd08igfo'
     CALL Search_Pole_Surround3(ipmove, is)

     Isucc = 0
     CALL Swap3D_1Edge_2(ipmove, is, IndxQual, volmin, Isucc)
     IF(Isucc==1)THEN
        did = -1
        nis = nis + 1
        ipStay(istay) = 0
     ELSE
        did   = Heap%v(istay) / 10
        nfail = nfail + 1
     ENDIF
     CALL HeapTree_ResetNodeValue (Heap, istay, did)
  ENDDO

  CALL HeapTree_Clear (Heap)

  IF(nis+4/=num_ipStay)THEN
     nis = 0
     DO i = 1, num_ipStay
        IF(ipStay(i)==0) CYCLE
        nis = nis + 1
        ipStay(nis) = ipStay(i)
     ENDDO
     num_ipStay = nis
     RETURN
  ENDIF
  
  num_ipStay = 0

  CALL LinkAssociation_List(ipMove, List_Length, ITs_List, PointAsso)
  IF(List_Length/=4)THEN
     WRITE(*,*)'Error---- ipmove=',ipmove, ' number of links:',List_Length
     CALL Error_Stop('Collapse_withSwap')
  ENDIF

  !--- copy ITs_List since it will be affected by LinkAssociation_Remove/Add
  ITs_Save(1:4) = ITs_List(1:4)
  
  !--- update connectivities and LinkAssociation
  DO jj = 1,4
     IT = ITs_Save(jj)
     CALL LinkAssociation_Remove(4, IT,IP_Tet(:,IT),PointAsso)
  ENDDO

  IT = ITs_Save(1)
  DO i = 1,4
     is = IP_Tet(i, ITs_Save(2))
     IF(which_NodeinTet(is, IP_Tet(1:4, IT))>0) CYCLE
     WHERE(IP_Tet(:,IT)==ipMove) IP_Tet(:,IT) = is
     CALL LinkAssociation_AddCell(4, IT,IP_Tet(:,IT),PointAsso)
     EXIT
  ENDDO
  
  DO jj = 2,4
     IT = ITs_Save(jj)
     IP_Tet(4,IT) = -IP_Tet(4,IT)
  ENDDO

  RETURN

END SUBROUTINE Collapse_withSwap
