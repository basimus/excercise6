!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


#ifdef _OldRecov

!*******************************************************************************
!>
!!  Old Recovery code. a library of FORTRAN77 code is in need.
!!  \sa
!!     common_Parameters::Recovery_Method & SUBROUTINE Recovery3D( ).
!<       
!*******************************************************************************
SUBROUTINE Recovery( )
  USE common_Parameters
  USE Mapping3D
  USE array_allocator
  IMPLICIT NONE

  COMMON /dimen/ mxele, maele, mxpoi, mapoi
  COMMON /const/ nne,   np,    nb,    nbndp, iedge, icnt, nb_extra
  COMMON /bou/   nsurf, nsegm, nshet, nwire, nlay
  COMMON /varia/ mpoin, jpold, ngenr
  COMMON /scoor/ scale
  INTEGER :: mxele, maele, mxpoi, mapoi
  INTEGER :: nne,   np,    nb,    nbndp, iedge, icnt, nb_extra
  INTEGER :: nsurf, nsegm, nshet, nwire, nlay
  INTEGER :: mpoin, jpold, ngenr
  REAL*8  :: scale
  INTEGER :: mxedg
  INTEGER, SAVE :: kVisit = 0

  POINTER ( piel   ,  iel   (4,1) )
  POINTER ( pinext ,  inext (4,1) )
  POINTER ( pcoore ,  coore (4,1) )
  POINTER ( piface ,  iface (5,1) )
  POINTER ( pkegfa ,  kegfa (3,1) )
  POINTER ( pmedge ,  medge (2,1) )
  POINTER ( pmface ,  mface (2,1) )
  POINTER ( pcoord ,  coord (3,1) )
  POINTER ( pmodep ,  modep ( 1 ) )
  POINTER ( pdspac ,  dspac ( 1 ) )
  POINTER ( pie_fc ,  ie_fc ( 1 ) )
  POINTER ( plmare ,  lmare ( 1 ) )
  POINTER ( plaste ,  laste ( 1 ) )
  POINTER ( plelem ,  lelem ( 1 ) )
  POINTER ( plsegm ,  lsegm ( 1 ) )
  REAL *8 :: coord , coore
  REAL    :: dspac
  INTEGER :: iel, inext, iface, kegfa, medge, mface, modep, ie_fc
  INTEGER :: lmare, laste, lelem, lsegm
  INTEGER :: irecv, irtrn, itype, nbndpo

  INTEGER :: IB, IP, IT, i, i1, i2, ip1, ip2
  REAL*8  :: dd


  IF(Recovery_Method==1)THEN
    IF(NB_Extra_Point==8)THEN
    ELSE IF(NB_Extra_Point==0)THEN
       NB_Extra_Point = 8
       NB_Point = NB_Point + NB_Extra_Point
       NB_BD_Point = NB_BD_Point + NB_Extra_Point
       IF(NB_Point>nallc_Point) CALL ReAllocate_Point()
       DO IP=NB_Point,NB_Extra_Point+1,-1
         Posit(:,IP) = Posit(:,IP - NB_Extra_Point)
         Scale_Point(IP) = Scale_Point(IP - NB_Extra_Point)
       ENDDO
       DO IT = 1, NB_Tet
         IP_Tet(:,IT) = IP_Tet(:,IT) + NB_Extra_Point
       ENDDO
       DO IB = 1, NB_BD_Tri
         IP_BD_Tri(1:3, IB) = IP_BD_Tri(1:3, IB) + NB_Extra_Point
       ENDDO
    ELSE
       WRITE(29,*)'Error--- NB_Extra_Point=',NB_Extra_Point,    &
                 ' which is not suit for the recovery method'
       CALL Error_Stop ('Recovery')
    ENDIF
  ENDIF

  useStretch    = .FALSE.
  CircumUpdated = .FALSE.      !--- geometry of the grids will be lost
  VolumeUpdated = .FALSE.      !--- volume   of the grids will be lost

  kVisit = kVisit + 1

  !--- build imaginate elements out of the boundary
  CALL Bound_Next()

  irecv = 2
  irtrn = 0
  IF(Recovery_Time==3 .AND. kVisit==1) irtrn = 1    !--- do not clean faces this time
  itype = 1
  nbndpo = Surf%NB_Point

  !--- for /common/
  mxele = nallc_Tet
  maele = nallc_increase
  mxpoi = nallc_Point
  mapoi = nallc_increase
  nne   = NB_Tet
  np    = NB_Point
  nb    = NB_BD_Tri
  nbndp = NB_BD_Point - NB_Extra_Point
  icnt  = 0
  nsurf = Surf%NB_Surf
  nsegm = Surf%NB_Seg
  nshet = Surf%NB_Sheet
  nwire = 0
  nlay  = 0
  mpoin = NB_Point
  scale = 1.d0
  nb_extra = NB_Extra_Point

  dd = max(  XYZmax(1),  XYZmax(2),  XYZmax(3),    &
            -XYZmin(1), -XYZmin(2), -XYZmin(3) )
  do i = 1 , 10
     scale = 0.0001d0 * 10**i
     if((dd/scale).lt.100) exit
  enddo
  if(scale>=1.d5) print *,' warning: coordinates are too big'
  scale = 1.d0 / scale
  print*,'scale=',scale

  mxedg = 2*NB_BD_Tri

  CALL allc( piel  ,  4,  nallc_Tet,    'Recovery' )
  CALL allc( pinext,  4,  nallc_Tet,    'Recovery' )
  CALL allc( pcoore,  4*2,nallc_Tet,    'Recovery' )
  CALL allc( plmare,  1,  nallc_Tet,    'Recovery' )
  CALL allc( plelem,  1,  nallc_Tet,    'Recovery' )
  CALL allc( piface,  5,  NB_BD_Tri,    'Recovery' )
  CALL allc( pkegfa,  3,  NB_BD_Tri,    'Recovery' )
  CALL allc( pie_fc,  1,  NB_BD_Tri,    'Recovery' )
  CALL allc( pmedge,  2,  mxedg,        'Recovery' )
  CALL allc( pmface,  2,  mxedg,        'Recovery' )
  CALL allc( pcoord,  3*2,nallc_Point,  'Recovery' )
  CALL allc( pmodep,  1,  nallc_Point,  'Recovery' )
  CALL allc( pdspac,  1,  nallc_Point,  'Recovery' )
  CALL allc( plaste,  1,  nallc_Point,  'Recovery' )
  CALL allc( plsegm,  1,  Surf%NB_Seg , 'Recovery' )

  Mark_Tet(1:NB_Tet) = 0

  iel  (:, 1:NB_Tet)      = IP_Tet      (:, 1:NB_Tet)
  inext(:, 1:NB_Tet)      = NEXT_Tet    (:, 1:NB_Tet)
  coore(:, 1:NB_Tet)      = Sphere_Tet  (:, 1:NB_Tet) *scale
  lmare(   1:NB_Tet)      = Mark_Tet    (   1:NB_Tet)
  iface(:, 1:NB_BD_Tri)   = Surf%IP_Tri (:, 1:NB_BD_Tri)
  DO IB = 1,NB_BD_Tri
    iface(4,IB) = IB
    IP_BD_Tri(4,IB) = IB
  ENDDO
  coord(:, 1:NB_Point)    = Posit       (:, 1:NB_Point) *scale
  lsegm(   1:Surf%NB_Seg) = IB_Surf_Seg (   1:Surf%NB_Seg)


  lelem(1:NB_Tet) = 0

  dspac(1:NB_Point) = 1.d0
  modep(1:NB_Point) = 0
  DO IP = NB_Extra_Point+1, NB_BD_Point
     !-- dspac(IP) = Scale_Point(IP) * BGSpacing%BasicSize  *scale
  ENDDO
  DO IP = 1, NB_Point
     modep(IP) = IP - NB_Extra_Point
  ENDDO
  WHERE(modep(1:NB_Point)<0) modep(1:NB_Point) = 0

  !-- the subroutine chekd return follows
  !--   iedge = number of surface edge (NB_BD_Edge)
  !--   kegfa(1:3, 1:NB_BD_Tri)   = 3 edges of each triangle
  !--   medge(1:2, 1:NB_BD_Edge)  = 2 nodes of edch edge (IP_BD_Edge)
  !--   mface(1:2, 1:NB_BD_Edge)  = 2 triangles of each edge
  CALL chekd ( iface, medge, kegfa, mface , plsegm,0)

  IF(Recovery_Method==1)THEN

     CALL rec3d_OB(piel,pcoord, iface,pinext, medge,mface,kegfa,   &
          pcoore,pmodep,plsegm,plelem,plmare,pdspac,plaste,   &
          irecv,irtrn,itype,ie_fc)

  ELSE

     CALL rec3d_MH(piel,pinext,pcoore,piface,pkegfa,pmedge,   &
          pmface,pcoord,pmodep,pdspac,pie_fc,plmare,   &
          plaste,plelem,plsegm,irecv,irtrn,nbndpo)
  ENDIF

  WRITE(*, *)'after recovery---- '
  WRITE(*, *)       '   NB_Tet,      nne, NB_Point,       np,    mpoin ='
  WRITE(*, '(8I10)')    NB_Tet,      nne, NB_Point,       np,    mpoin
  WRITE(*, *)       'NB_BD_Point,  nbndp,   nbndpo, NB_BD_Tri,      nb ='
  WRITE(*, '(8I10)') NB_BD_Point,  nbndp,   nbndpo, NB_BD_Tri,      nb
  WRITE(*, *)' '
  WRITE(29,*)'after recovery---- '
  WRITE(29,*)       '   NB_Tet,      nne, NB_Point,       np,    mpoin ='
  WRITE(29,'(8I10)')    NB_Tet,      nne, NB_Point,       np,    mpoin
  WRITE(29,*)       'NB_BD_Point,  nbndp,   nbndpo, NB_BD_Tri,      nb ='
  WRITE(29,'(8I10)') NB_BD_Point,  nbndp,   nbndpo, NB_BD_Tri,      nb
  WRITE(29,*)' '

  IF(irecv/=0)THEN
     !--- recover is NOT completed
     WRITE(29,*)'irecv=', irecv
     IF(kVisit==1 .AND. Recovery_Time/=2)THEN
        WRITE(29,*)'Hint--- : recovery is NOT completed, try later'
        Recovery_Time = 2
     ELSE
        WRITE(29,*)'Error--- : recovery is NOT completed'
        CALL Error_Stop ('Recovery')
     ENDIF
  ENDIF

  IF(mxele>nallc_Tet)THEN
     nallc_Tet   = mxele
     CALL ReAllocate_Tet()
  ENDIF
  IF(mxpoi>nallc_Point)THEN
     nallc_Point = mxpoi
     CALL ReAllocate_Point()
  ENDIF
  NB_Tet   = nne
  NB_Point = np

  IP_Tet(:, 1:NB_Tet)   =  iel  (:, 1:NB_Tet)
  Posit (:, 1:NB_Point) =  coord(:, 1:NB_Point)

  !--- Next_Tet(:,:) has been distroyed by recovery, rebuild it
  CALL Next_Build()
  CALL Surface_Edge_Clear()

  IF(irtrn/=1 .AND. irecv==0)THEN

     DO IP=1,Surf%NB_Point
        Scale_Point(IP) = Scale_Point(IP + NB_Extra_Point)
     ENDDO

     IF(Background_Model==-1)THEN
        DO IP=1,Surf%NB_Point
           fMap_Point(:,:,IP) = fMap_Point(:,:,IP + NB_Extra_Point)
        ENDDO
     ENDIF

     NB_Extra_Point = 0
     NB_BD_Point = Surf%NB_Point
     NB_BD_Tri   = Surf%NB_Tri
     DO IB = 1, Surf%NB_Tri
        IP_BD_Tri(1:5, IB) = Surf%IP_Tri(1:5, IB)
     ENDDO


     IF(Debug_Display>3)THEN
        CALL Boundary_Associate(1)
        IF(nbndp/=Surf%NB_Point .OR. nb/=Surf%NB_Tri)THEN
           WRITE(29,*)'Error--- : Boundary nodes or triangles arent matched'
           WRITE(29,*)'nbndp,Surf%NB_Point,NB_BD_Point=',   &
                nbndp,Surf%NB_Point,NB_BD_Point
           WRITE(29,*)'nb,Surf%NB_Tri,NB_BD_Tri=',nb,Surf%NB_Tri,NB_BD_Tri
           CALL Error_Stop ('Recovery')
        ENDIF
        DO IB = 1,NB_BD_Tri
           CALL Boundary_Tri_Match(iface(1,IB),iface(2,IB),iface(3,IB),I)
           IF( I<=0 .OR. iface(5,IB)/=IP_BD_Tri(5,I) )THEN
              WRITE(29,*)'Error--- : Boundary triangles arent matched'
              WRITE(29,*)'IB,ips=',IB,iface(:,IB)
              IF(i>0) WRITE(29,*)'I,ips=',I,IP_BD_Tri(:,I)
              CALL Error_Stop ('Recovery')
           ENDIF
        ENDDO
     ENDIF

  ENDIF


  CALL deallc( piel  ,   'Recovery' )
  CALL deallc( pinext,   'Recovery' )
  CALL deallc( pcoore,   'Recovery' )
  CALL deallc( piface,   'Recovery' )
  CALL deallc( pkegfa,   'Recovery' )
  CALL deallc( pmedge,   'Recovery' )
  CALL deallc( pmface,   'Recovery' )
  CALL deallc( pcoord,   'Recovery' )
  CALL deallc( pmodep,   'Recovery' )
  CALL deallc( pdspac,   'Recovery' )
  CALL deallc( pie_fc,   'Recovery' )
  CALL deallc( plmare,   'Recovery' )
  CALL deallc( plaste,   'Recovery' )
  CALL deallc( plelem,   'Recovery' )
  CALL deallc( plsegm,   'Recovery' )
  
  RETURN
END SUBROUTINE Recovery


!*******************************************************************************
!>      
!!  To meet the requirement of recovery (old version),
!!     12 imaginary elements are added outside the initial box. 
!!  These elements will be removed after recovery.       
!<      
!*******************************************************************************
SUBROUTINE Bound_Next()

  USE common_Constants
  USE common_Parameters
  IMPLICIT NONE

  INTEGER :: IT, NBB, i, j

  NBB = 0
  DO IT=1,NB_Tet
     DO i=1,4
        IF(NEXT_Tet(i,IT)==-1) NBB = NBB+1
     ENDDO
  ENDDO

  IF(NBB /= 12*Initial_Split**2 .AND. NBB/=NB_BD_Tri)THEN
     WRITE(29,*)'Warning--- wrong number of boundary triangles'
     WRITE(29,*)'NBB=',NBB,12*Initial_Split**2,NB_BD_Tri
  ENDIF

  DO WHILE(NB_Tet+NBB > nallc_Tet)
     CALL ReAllocate_Tet()
  ENDDO

  j = 0
  DO IT=NB_Tet,1,-1
     IP_Tet(:,IT+NBB) = IP_Tet(:,IT)
     Sphere_Tet(:,IT+NBB) = Sphere_Tet(:,IT)

     DO i=1,4
        IF(NEXT_Tet(i,IT)==-1)THEN
           j = j+1
           NEXT_Tet(i,IT+NBB) = j
        ELSE
           NEXT_Tet(i,IT+NBB) = NEXT_Tet(i,IT) +NBB
        ENDIF
     ENDDO
  ENDDO


  DO IT = NBB+1, NB_Tet + NBB
     DO i=1,4
        j = NEXT_Tet(i,IT)
        IF(j<=NBB)THEN
           IP_Tet(1,  j) = -10
           IP_Tet(2:4,j) = IP_Tet(iTri_Tet(1:3,i),IT)
           NEXT_Tet(1,  j) = IT
           NEXT_Tet(2:4,j) = -10
           Sphere_Tet(1:3,j) = 0.d0
           Sphere_Tet(4,  j) = -999.d0
        ENDIF
     ENDDO
  ENDDO


  NB_Tet = NB_Tet + NBB

  RETURN

END SUBROUTINE Bound_Next

#else

SUBROUTINE Recovery( )
  WRITE(29,*)'Error--- For this Recovery Method, you need link the code with'
  WRITE(29,*)'         a proper library--- see your makefile.'
  CALL Error_Stop ('Recovery')
  RETURN
END SUBROUTINE Recovery

#endif


