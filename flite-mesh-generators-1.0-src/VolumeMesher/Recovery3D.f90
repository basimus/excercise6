!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


MODULE common_Recovery3D
  INTEGER, DIMENSION(:  ),  POINTER :: Next_BD_Edge => null()   !<  (NB_BD_Edge)   : next edge of a boundary edge.
  INTEGER, DIMENSION(:  ),  POINTER :: Next_BD_Pt => null()     !<  (NB_Point)     : next point of a boundary edge.
  INTEGER, DIMENSION(:  ),  POINTER :: Prev_BD_Pt => null()     !<  (NB_Point)     : previous point of a boundary edge.
  INTEGER, SAVE :: NP_Origin
  INTEGER, SAVE :: NP_withEdgeBreak
END MODULE common_Recovery3D



!*******************************************************************************
!>
!!  Recovery of the original boundary triangulation from a volume mesh.
!!  @param[in] common_Parameters::NB_Tet
!!  @param[in] common_Parameters::NB_Point
!!  @param[in] common_Parameters::NB_Extra_Point
!!  @param[in] common_Parameters::IP_Tet
!!  @param[in] common_Parameters::NEXT_Tet
!!  @param[in] common_Parameters::Posit
!!  @param[in] common_Parameters::NB_BD_Tri
!!  @param[in] common_Parameters::IP_BD_Tri
!<
!*******************************************************************************
SUBROUTINE Recovery3D( )

  USE Geometry3DAll
  USE common_Constants
  USE common_Parameters
  USE common_Recovery3D
  IMPLICIT NONE

  INTEGER :: nallc_edge,nallc_Tri
  INTEGER :: i, it, ip

  DO it = 1,NB_BD_Tri
     IP_BD_Tri(4,it) = it
  ENDDO

  useStretch    = .FALSE.
  CALL Get_Tet_Volume(0)
  QualityMin(1) = Volume_Tet(1)
  DO  IT = 2,NB_Tet
     QualityMin(1) = MIN(QualityMin(1),Volume_Tet(IT))
  ENDDO
  WRITE(*, *)'Recovery3D:: initial check: min volume=',QualityMin(1)
  WRITE(29,*)'Recovery3D:: initial check: min volume=',QualityMin(1)
  CircumUpdated = .FALSE.
  VolumeUpdated = .FALSE.


  NP_Origin = NB_Point      !--- number of original points before recovery

  !--- build boundary edges
  CALL Boundary_Edge_Treat1(nallc_edge,nallc_Tri)

  !--- recovery boundary edges
  WRITE(*,*)'Recovery3D::  --- recovery boundary edges'
  CALL Recovery_Edge(nallc_edge)
  NP_withEdgeBreak = NB_Point    !---- number of points after recovery edges

  !--- recovery triangles
  WRITE(*,*)'Recovery3D::  --- recovery triangles'
  CALL Recovery_Face(nallc_Tri)

  !--- recomputing edges
  CALL Boundary_Edge_Treat2( )

  CircumUpdated = .FALSE.      !--- geometry of the grids will be lost
  VolumeUpdated = .FALSE.      !--- volume   of the grids will be lost
  
  IF(Recovery_Method==4)THEN
     !--- lift nodes. Improved version
     WRITE(*,*)'Recovery3D::  --- lift those new nodes on boundary faces'
     CALL Recovery_LiftPoint_2( )
     CALL Next_Build( )
  ENDIF

  !--- mark elements: Mark_Tet(it)=1, then it is an element inside  the domain
  !                   Mark_Tet(it)=2, then it is an element outside the domain
  Mark_Tet(1:NB_Tet) = 0
  CALL Boundary_Associate(2)

  !--- a check
  IF(Debug_Display>3)THEN
     DO IT=1,NB_Tet
        DO i=1,4
           ip = IP_Tet(i,IT)
           IF(ip<=NB_Extra_Point)THEN
              IF(Mark_Tet(IT)/=2)THEN
                 WRITE(29,*)'Error---wrong boundary face orientation or population'
                 WRITE(29,*)' IT,Mark,IPs=',IT,Mark_Tet(IT),IP_Tet(:,IT)
                 WRITE(29,*)' Suggestion: check the orientation of the surface mesh.'
                 CALL Error_Stop ('Recovery3D')
              ENDIF
           ENDIF
        ENDDO
     ENDDO
  ENDIF

  IF(Recovery_Method==3)THEN
     !--- lift nodes. original version, not for crossed surfaces.
     WRITE(*,*)'Recovery3D::  --- lift those new nodes on boundary faces'
     CALL Recovery_LiftPoint( )
  ENDIF

  WRITE(*,*)'Recovery3D::  ---  clean face'
  CALL Recover_CleanFace( )
  
  NB_Extra_Point = 0
  IsolatedSheet  = .true.
  CALL Next_Build( )

  !--- a check
  IF(Debug_Display>3)THEN
     CALL Check_Next(-2)
  ENDIF

  DEALLOCATE(Next_BD_Pt,  Prev_BD_Pt)

  RETURN
END SUBROUTINE Recovery3D


!*******************************************************************************
!>
!!   Get the surface edge data structure required for the boundary recovery.
!!
!!   @param[in]  common_Parameters::IP_BD_Tri
!!   @param[out] nallc_edge,nallc_Tri  the sizes used to allocate the array.
!!   @param[out] common_Parameters::NB_BD_Edge  the number of boundary edges.
!!   @param[out] common_Parameters::IP_BD_Edge (2,NB_BD_Edge)
!!                     : 2 nodes of each edge, the nodes with smaller ID takes first.
!!   @param[out] common_Parameters::IEdge_BD_Tri (3,NB_BD_Tri)
!!                     : 3 edges of each boundary triangle.
!!
!!   Also allocate common_Parameters::Next_BD_Edge
!<
!*******************************************************************************

SUBROUTINE Boundary_Edge_Treat1(nallc_edge, nallc_Tri)

  USE common_Parameters
  USE array_allocator
  USE common_Recovery3D
  IMPLICIT NONE

  INTEGER, INTENT(OUT) :: nallc_edge, nallc_Tri
  INTEGER :: IB, i, IEdge,  ipp(2)
  TYPE(NodeNetTreeType) :: BoundEdgeTree

  nallc_edge = 3*NB_BD_Tri
  nallc_Tri  = NB_BD_Tri

     CALL allc_2Dpointer(IEdge_BD_Tri, 3, nallc_Tri,  'IEdge_BD_Tri')
     CALL allc_2Dpointer(IP_BD_Edge,   2, nallc_edge, 'IP_BD_Edge')
     CALL allc_1Dpointer(Next_BD_Edge,    nallc_edge, 'Next_BD_Edge')
     Next_BD_Edge(1:nallc_edge) = 0

     CALL NodeNetTree_allocate(2, NB_BD_Tri, 3*NB_BD_Tri, BoundEdgeTree)

     NB_BD_Edge       = 0
     DO IB = 1,NB_BD_Tri
        DO i = 1,3
           ipp(1) = IP_BD_Tri(i,IB)
           ipp(2) = IP_BD_Tri(MOD(i,3)+1,IB)
           CALL NodeNetTree_SearchAdd(2,ipp,BoundEdgeTree,IEdge)

           IEdge_BD_Tri(i,IB) = IEdge
           IF(IEdge>NB_BD_Edge)THEN
              !-- a new edge
              NB_BD_Edge = IEdge
              IP_BD_Edge(1,IEdge) = ipp(1)
              IP_BD_Edge(2,IEdge) = ipp(2)
           ENDIF
        ENDDO
     ENDDO

     CALL NodeNetTree_clear(BoundEdgeTree)

     WRITE(29,'(a,i10,a,i10)')' Boundary_Edge_Treat1:: no. faces=',   &
                             NB_BD_Tri,' no. edges=',NB_BD_Edge

  RETURN
END SUBROUTINE Boundary_Edge_Treat1

!*******************************************************************************
!>
!!   Form the list of nodes that lie on an original edge.
!!
!!   @param[in]  common_Parameters::IP_BD_Edge (2,NB_BD_Edge)
!!   @param[in]  common_Recovery3D::Next_BD_Edge (NB_BD_Edge)
!!   @param[out] common_Recovery3D::Next_BD_Pt (NB_Point)
!!   @param[out] common_Recovery3D::Prev_BD_Pt (NB_Point)
!!
!!   common_Parameters::IEdge_BD_Tri, common_Parameters::IP_BD_Edge, and
!!   common_Recovery3D::Next_BD_Edge will be deleted.
!<
!*******************************************************************************

SUBROUTINE Boundary_Edge_Treat2( )
  USE array_allocator
  USE common_Parameters
  USE common_Recovery3D
  IMPLICIT NONE

  INTEGER :: i, IEdge

     CALL allc_1Dpointer(Next_BD_Pt,   NB_Point, 'Next_BD_Pt')
     CALL allc_1Dpointer(Prev_BD_Pt,   NB_Point, 'Prev_BD_Pt')
     Next_BD_Pt(:) = 0
     Prev_BD_Pt(:) = 0
     DO IEdge = 1,NB_BD_Edge
        IF(Next_BD_Edge(IEdge)==0) CYCLE
        i = IP_BD_Edge(2,IEdge)
        Prev_BD_Pt(i) = IP_BD_Edge(1,IEdge)
        Next_BD_Pt(i) = IP_BD_Edge(2,Next_BD_Edge(IEdge))
     ENDDO

     DEALLOCATE(IEdge_BD_Tri, IP_BD_Edge, Next_BD_Edge)

  RETURN
END SUBROUTINE Boundary_Edge_Treat2

!*******************************************************************************
!>  Recovery of boundary edges
!*******************************************************************************
SUBROUTINE Recovery_Edge(nallc_edge )

  USE Geometry3DAll
  USE array_allocator
  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  USE common_Recovery3D
  IMPLICIT NONE

  INTEGER, INTENT(INOUT) :: nallc_edge
  INTEGER :: ndold
  INTEGER :: i, ip, ip0, ip1, ip2, ip3, ip4, ipnew, ipp2(2), ipp3(3), ipp4(4)
  INTEGER :: Iedge, ied, ied1, id1, id2, id3, ie, it, ln, jt, itm
  INTEGER :: Idir, ind, ntype, Isucc, iTarget
  INTEGER :: irecv, nclps, nleft, ntype1, ntype2, ntype3
  LOGICAL :: CarryOn
  REAL*8  :: pp2(3,2), pp3(3,3), xsect(3), vmin, volmin, w3(3), t,tm
  TYPE (NodeNetTreeType) :: bdnet

  vmin   = 1.d-6* MAX(QualityMin(1), SmallVolume)

  Mark_Point(1:NB_Point) = 0

  !--- Associate points with elements
  CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)

  !---  loop over all the edges ---

  irecv  = 0
  nclps  = 0
  nleft  = 0
  ntype1 = 0
  ntype2 = 0
  ntype3 = 0
  ndold = NB_BD_Edge
  Loop_Iedge : DO Iedge =  1 ,ndold

     ip2 = IP_BD_Edge(2,Iedge)
     pp2(:,2)   = Posit(:,ip2)
     ied  = Iedge
     ied1 = NB_BD_Edge
     ip0  = 0

     Loop_EdgePiece : DO

        IF(ied-ied1>100)THEN
           WRITE(29,*)'Error--- dead loop'
           WRITE(29,*)'  #Iedge=',Iedge,' ied=',ied
           WRITE(29,*)'  # ip0,ip1,ip2=',IP_BD_Edge(1,Iedge),ip1,ip2
           CALL Error_Stop ('Recovery_Edge')
        ENDIF

        ip1 = IP_BD_Edge(1,ied)
        pp2(:,1)   = Posit(:,ip1)

        !--- List all elements surrounding point ip1 in the array ITs_List
        CALL LinkAssociation_List(ip1, List_Length, ITs_List, PointAsso)

        !---  find if edge exists
        DO ie = 1,List_Length
           it  = ITs_List(ie)
           IF(  IP_Tet(1,it)==ip2 .OR. IP_Tet(2,it)==ip2 .OR.          &
                IP_Tet(3,it)==ip2 .OR. IP_Tet(4,it)==ip2     )THEN
              EXIT Loop_EdgePiece
           ENDIF
        ENDDO

        !---  find which face intersect with edge
        itm = 0
        tm = -1.
        ie = 0
        it = 0
        Loop_ie : DO


           IF(ie>=List_Length)THEN
              IF(itm>0)THEN
                 !--- reached the end, use the uncertant intersection point
                 ie = 0
                 it = itm
              ELSE
                 !--- crossed point lost
                 WRITE(29,*)'Error--- no intersection point surrounding'
                 WRITE(29,*)'  #Iedge=',Iedge,' ied=',ied
                 WRITE(29,*)'  # ip0,ip1,ip2=',IP_BD_Edge(1,Iedge),ip1,ip2
                 CALL Error_Stop ('Recovery_Edge')
              ENDIF
           ELSE
              ie = ie+1
              it = ITs_List(ie)
           ENDIF

           DO Idir = 1 , 4
              !--- ip0: the previous breaking node on this edge
              IF(IP_Tet(Idir,it)==ip0) CYCLE Loop_ie
           ENDDO

           DO Idir = 1 , 4
              IF(IP_Tet(Idir,it)==ip1) EXIT
           ENDDO
           IF(Idir>4) CALL Error_Stop (' Recovery_Edge : error--- wrong with LinkAssociation')

           ipp3( 1:3) = IP_Tet(iTri_Tet(1:3,Idir),it)
           pp3(:,1:3) = Posit(:,ipp3(1:3))
           ntype = 0
           xsect = Plane3D_Line_Tri_Cross(pp2,pp3,ntype,w3,t)

           IF(ntype==0 .AND. ABS(t)<1.d-6)THEN
              ntype = -1
              xsect = Plane3D_Line_Tri_Cross(pp2,pp3,ntype,w3,t)
           ENDIF

           IF(ntype>=4 .AND. ntype<7) THEN
              IF(w3(ntype-3)> 1.d-8) ntype = 7
              IF(w3(ntype-3)<-1.d-8) ntype = -1
           ENDIF
           IF(ntype>0 .AND. t>1.d-6) EXIT
           IF(ie==0) EXIT

           IF(ntype>0 .AND. t<=1.d-6)THEN
              !--- this is a uncertant intercept point, mark it
              IF(ntype>=4 .AND. t>tm)THEN
                 IF(ie==List_Length) EXIT
                 tm  = t
                 itm = it
              ENDIF
           ENDIF

        ENDDO Loop_ie

        !--- crossed point found

        IF(ntype>=4 .AND. ntype<7) THEN
           !---  intersection is on an edge of the face, try swap first

           ip3 = ipp3(MOD(ntype,3)+1)
           ip4 = ipp3(MOD(ntype+1,3)+1)
           DO ind = 1,12
              IF(  IP_Tet(I_Comb_Tet(1,ind),IT)==ip3 .AND.    &
                   IP_Tet(I_Comb_Tet(2,ind),IT)==ip4 ) EXIT
           ENDDO
           IF(ind>12) CALL Error_Stop (' Recovery_Edge : error----here')

           CALL Search_Pole_Surround(IT, ind)

           Isucc = 0
           CALL Swap3D_1Edge(ip3, ip4, 1, vmin, Isucc)
           IF(Isucc==1) CYCLE Loop_EdgePiece
        ENDIF

        !---  update IP_BD_Edge
        NB_BD_Edge = NB_BD_Edge+1
        IF(NB_BD_Edge>nallc_edge) THEN
           nallc_edge = NB_BD_Edge+1000
           CALL allc_2Dpointer(IP_BD_Edge,   2, nallc_edge, 'IP_BD_Edge')
           CALL allc_1Dpointer(Next_BD_Edge,    nallc_edge, 'Next_BD_Edge')
        ENDIF

        IF(ntype>3) THEN
           !--- add a new node
           NB_Point=NB_Point+1
           IF(NB_Point>nallc_point) CALL ReAllocate_Point()
           Posit(1:3,NB_Point)= xsect(1:3)
           IP_BD_Edge(2,ied)          = NB_Point
           IP_BD_Edge(1,NB_BD_Edge)   = NB_Point
        ELSE
           !--- intersect with a node
           IP_BD_Edge(2,ied)          = ipp3(ntype)
           IP_BD_Edge(1,NB_BD_Edge)   = ipp3(ntype)
        ENDIF

        IP_BD_Edge(2,NB_BD_Edge)  = ip2
        Next_BD_Edge(ied)         = NB_BD_Edge
        Next_BD_Edge(NB_BD_Edge)  = 0

        IF (ntype==7) THEN
           !---  intersection inside a face
           !---  insert new point in the center of the face and update the data
           !---  need to update IP_Tet, Next_Tet and LinkAssociation structure

           ntype3 = ntype3+1
           ln = Next_Tet(Idir,it)
           IF(ln<=0) CALL Error_Stop (' Recovery_Edge : intersection on boundary')
           IF(IP_Tet(4,ln)<=0) CALL Error_Stop (' Recovery_Edge : intersection on boundary')
           ipnew = IP_Tet(1,ln) + IP_Tet(2,ln) + IP_Tet(3,ln) + IP_Tet(4,ln)   &
                -ipp3(1)-ipp3(2)-ipp3(3)

           Isucc = 1
           CALL Insert_Point_SplitFace(NB_Point,IT,Idir,Isucc)

           IF(ipnew==ip2) EXIT Loop_EdgePiece

        ELSE IF(ntype>=4) THEN
           !---  intersection on an edge of face, break the edge
           !---  Search_Pole_Surround() has been called before

           ntype2 = ntype2+1
           Isucc  = 1
           CALL Insert_Point_SplitEdge(NB_Point,IP3,IP4,Isucc)

        ELSE
           !--- intercept on a exsiting node
           ntype1 = ntype1+1

        ENDIF


        ied = NB_BD_Edge
        ip0 = ip1

     ENDDO Loop_EdgePiece

     IF(IP_BD_Edge(2,Iedge)==ip2)THEN
        irecv = irecv+1
        CYCLE Loop_Iedge
     ENDIF

     !--- Try to remove the new added points by collapsing the elements

     id1 = Iedge
     CarryOn = .FALSE.
     Loop_Collapse : DO
        id2      = Next_BD_Edge(id1)
        ipp2(1)  = IP_BD_Edge(1,id1)
        ip       = IP_BD_Edge(1,id2)
        ipp2(2)  = IP_BD_Edge(2,id2)
        volmin   = 1.d-6* QualityMin(1)
        CALL Collapse_Check(ip,2,ipp2,1,iTarget,volmin,-999,0)

        IF(iTarget>0) THEN
           !--- can move
           CALL Collapse_PointPair(ip,ipp2(iTarget),1)
           Mark_Point(ip) = -999
           CarryOn        = .TRUE.

           IP_BD_Edge(2,id1) = ipp2(2)
           DO WHILE(id2<NB_BD_Edge)
              id3 = Next_BD_Edge(id2)
              IF(id3==0) CALL Error_Stop (' Recovery_Edge : id3')
              IP_BD_Edge(:,id2) = IP_BD_Edge(:,id3)
              id2 = id3
           ENDDO
           NB_BD_Edge = NB_BD_Edge -1
           Next_BD_Edge(NB_BD_Edge) = 0

           IF(ipp2(2)==ip2)THEN
              Next_BD_Edge(id1) = 0
              IF(ipp2(1)==IP_BD_Edge(1,Iedge))THEN
                 nclps = nclps+1
                 EXIT Loop_Collapse
              ELSE
                 id1 = Iedge
                 CarryOn = .FALSE.
              ENDIF
           ENDIF

        ELSE
           !---can not move
           IF(ipp2(2)/=ip2) THEN
              id1=id2
           ELSE IF(CarryOn) THEN
              id1 = Iedge
              CarryOn = .FALSE.
           ELSE
              nleft = nleft+1
              EXIT Loop_Collapse
           ENDIF
        ENDIF

     ENDDO Loop_Collapse

  ENDDO Loop_Iedge

  !--- build a temp edge searching system for swapping
  CALL LinkAssociation_Clear(PointAsso)
  CALL NodeNetTree_Build(2, NB_BD_Edge, IP_BD_Edge, NB_Point, bdnet)

  !--- cosmestic swapping
  VolumeUpdated = .TRUE.
  IT = 0
  DO WHILE(IT<NB_Tet)
     IT = IT+1
     IF(IP_Tet(4,IT)<=0) CYCLE
     CALL Get_Tet_Volume(IT)
     IF(Volume_Tet(IT)<vmin)THEN
        DO ind = 1,6
           ipp4(1:4) = IP_Tet(I_Comb_Tet(1:4,ind),IT)
           CALL NodeNetTree_search(2,ipp4(1:2),bdnet,ied)
           if(ied>0) cycle     !--- boundary triangle edge, no swap
           Isucc = 0
           t = Geo3D_Dihedral_Angle(Posit(:,ipp4(1)),Posit(:,ipp4(2)),   &
               Posit(:,ipp4(3)),Posit(:,ipp4(4)),Isucc)
           IF(Isucc==0 .OR. ABS(t-PI)>0.001) cycle
           CALL Search_Pole_Surround(IT, ind)
           Isucc = 0
           CALL Swap3D_1Edge(ipp4(1), ipp4(2), 1, vmin, Isucc)
           IF(Isucc==1) exit   !--- swap done
        ENDDO
     ENDIF
  ENDDO

  CALL NodeNetTree_Clear(bdnet)
  VolumeUpdated = .FALSE.

  !--- clear discasted points and cells
  CALL Remove_Tet()
  CALL Remove_Point_inOrder()

  DO ied = 1,NB_BD_Edge
     DO i=1,2
        IP = Mark_Point(IP_BD_Edge(i,ied))
        IF(IP>0)THEN
           IP_BD_Edge(i,ied) = IP
        ELSE
           CALL Error_Stop (' Recovery_Edge : Error---recovery edge')
        ENDIF
     ENDDO
  ENDDO


  PRINT *, '----------after recovering boundary edges----------'
  PRINT *, ' number of elements = ',NB_Tet
  PRINT *, ' number of points   = ',NB_Point
  PRINT *, ' number of edges    = ',NB_BD_Edge
  PRINT *, ' '
  PRINT *, ' edges recovered ',irecv
  PRINT *, ' '
  PRINT *, ' total intersections with nodes  :',ntype1
  PRINT *, ' total intersections with edges  :',ntype2
  PRINT *, ' total intersections with faces  :',ntype3
  PRINT *, ' '
  PRINT *, ' edges with compelete collaps :',nclps
  PRINT *, ' edges left to collaps        :',nleft
  PRINT *, ' number of added edges        :',NB_BD_Edge-ndold
  PRINT *, '---------------------------------------------------'
  PRINT *, ' '


END SUBROUTINE Recovery_Edge


!*******************************************************************************
!>  Recovery of boundary faces
!*******************************************************************************
SUBROUTINE Recovery_Face(nallc_Tri)

  USE Geometry3DAll
  USE array_allocator
  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  USE common_Recovery3D
  IMPLICIT NONE

  INTEGER, INTENT(INOUT) :: nallc_Tri
  INTEGER :: ie, it, ip1, ip2, ipp2(2), ipp3(3), jt
  INTEGER :: Itri, nbn, newb, is1, is2, isr, i1, i2, jek, indk, iTarget
  INTEGER :: nfr, nfro, ifs, kfr, jb, jf
  INTEGER :: ind, ntype, Isucc, ifrec, ifcomp
  REAL*8  :: pp2(3,2), pp3(3,3), xsect(3), xsectk(3), w3(3), t, sfmin, srfm, vmin, volmin
  INTEGER :: ip_edgePiece(2,1000), ip_triPiece(3,1000)

  vmin   = 1.d-6* MAX(QualityMin(1), SmallVolume)

  !--- Re-Associate points with elements.
  CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)

  !---  loop over all the faces ---

  ifrec = 0
  ifcomp = 0
  nbn   = NB_BD_Tri

  LOOP_Itri : DO Itri = 1 , NB_BD_Tri

     ipp3(1:3)  = IP_BD_Tri(1:3,Itri)
     pp3(:,1:3) = Posit(:,ipp3(1:3))

     !--- Check if face is present in any element

     !--- List all elements surrounding point ipp3(1) in the array ITs_List
     CALL LinkAssociation_List(ipp3(1), List_Length, ITs_List, PointAsso)

     DO ie = 1,List_Length
        it = ITs_List(ie)
        IF( (IP_Tet(1,it)==ipp3(2) .OR. IP_Tet(2,it)==ipp3(2) .OR.    &
             IP_Tet(3,it)==ipp3(2) .OR. IP_Tet(4,it)==ipp3(2)     ) .AND.    &
             (IP_Tet(1,it)==ipp3(3) .OR. IP_Tet(2,it)==ipp3(3) .OR.    &
             IP_Tet(3,it)==ipp3(3) .OR. IP_Tet(4,it)==ipp3(3)     )    ) THEN
           ifrec = ifrec +1
           CYCLE Loop_Itri
        ENDIF
     ENDDO

     newb = 0
     nfr  = 0
     DO jb = 1 , 3
        ifs = IEdge_BD_Tri(jb,Itri)
        ip1 = IP_BD_Edge(1,ifs)
        nfro= nfr
        DO WHILE(ifs>0)
           nfr = nfr + 1
           ip_edgePiece(:,nfr) = IP_BD_Edge(:,ifs)
           IF(nfr==1000) CALL Error_Stop (' Recovery_Face : nfr>1000')
           ifs = Next_BD_Edge(ifs)
        ENDDO

        !---   order the rotation of the edges 1-->2-->3-->1
        ip2 = ip_edgePiece(2,nfr)
        IF(  (ipp3(2)==ip1 .AND. ipp3(1)==ip2) .OR.    &
             (ipp3(3)==ip1 .AND. ipp3(2)==ip2) .OR.    &
             (ipp3(1)==ip1 .AND. ipp3(3)==ip2)   ) THEN
           DO jf = nfro+1,nfr
              jt = ip_edgePiece(1,jf)
              ip_edgePiece(1,jf) = ip_edgePiece(2,jf)
              ip_edgePiece(2,jf) = jt
           ENDDO
        ENDIF
     ENDDO

     ifs = nfr

     Loop_ifs : DO WHILE(ifs/=0)

        is1 = ip_edgePiece(1,ifs)
        is2 = ip_edgePiece(2,ifs)

        CALL Search_Pole_Surround2(is1, is2)

        !--- find if an existing triangle makes a part of the triangle Itri
        Loop_ie_1 : DO ie=1,NB_PoleTet
           it = IT_PoleTet(ie)

           IF (IP_Tet(4,it)<=0) THEN
              WRITE(29,*)'Error--- not expect this element, it=',it
              WRITE(29,*)'is1,is2=',is1,is2,' ips=',IP_Tet(:,it)
              CALL Error_Stop (' --- Fial at Recovery_Face')
           ENDIF

           DO ind = 1,12
              IF(  IP_Tet(I_Comb_Tet(3,ind),it)==is1  .AND.    &
                   IP_Tet(I_Comb_Tet(4,ind),it)==is2      ) EXIT
           ENDDO
           IF(ind>12) CALL Error_Stop (' Recovery_Face : error of pole---')

           i1 = IP_Tet(I_Comb_Tet(1,ind),it)
           i2 = IP_Tet(I_Comb_Tet(2,ind),it)

           isr = 0
           DO kfr = 1, nfr
              IF(i1==ip_edgePiece(1,kfr)) THEN
                 IF(isr==0) THEN
                    isr = i1
                 ELSE IF(isr/=i1)THEN
                    isr = -1
                 ENDIF
              ENDIF
              IF(i2==ip_edgePiece(1,kfr)) THEN
                 IF(isr==0) THEN
                    isr = i2
                 ELSE IF(isr/=i2)THEN
                    isr = -1
                 ENDIF
              ENDIF
           ENDDO

           IF(isr==0) CYCLE Loop_ie_1
           IF(isr==-1)THEN
              WRITE(*, *)' '
              WRITE(*, *)' Warning--- Oubay thinks that you have a flat element: ',it
              WRITE(29,*)' '
              WRITE(29,*)' Warning--- Oubay thinks that you have a flat element: ',it
              CYCLE Loop_ie_1
           ENDIF

           !--- element has 3 node on triangle Itri

           DO jb =  1 , newb
              IF( (is1==ip_triPiece(1,jb) .OR. is1==ip_triPiece(2,jb) .OR.    &
                   is1==ip_triPiece(3,jb))   .AND.  &
                  (is2==ip_triPiece(1,jb) .OR. is2==ip_triPiece(2,jb) .OR.    &
                   is2==ip_triPiece(3,jb))   .AND.  &
                  (isr==ip_triPiece(1,jb) .OR. isr==ip_triPiece(2,jb) .OR.    &
                   isr==ip_triPiece(3,jb))  ) CYCLE Loop_ie_1
           ENDDO

           newb = newb + 1
           ip_triPiece(1:3,newb) = (/is1, is2, isr/)
           ip_edgePiece(:,ifs)   = ip_edgePiece(:,nfr)
           nfr = nfr - 1

           DO kfr = 1 , nfr
              IF(is1==(ip_edgePiece(2,kfr)) .AND. isr==(ip_edgePiece(1,kfr))) EXIT
           ENDDO
           IF(kfr<=nfr)THEN
              !--- found a corner
              ip_edgePiece(:,kfr) = ip_edgePiece(:,nfr)
              nfr = nfr - 1
           ELSE
              nfr = nfr + 1
              ifs = nfr + 1
              ip_edgePiece(1,nfr) = is1
              ip_edgePiece(2,nfr) = isr
           ENDIF

           DO kfr = 1 , nfr
              IF(is2==(ip_edgePiece(1,kfr)) .AND. isr==(ip_edgePiece(2,kfr))) EXIT
           ENDDO
           IF(kfr<=nfr)THEN
              !--- found a corner
              ip_edgePiece(:,kfr) = ip_edgePiece(:,nfr)
              nfr = nfr - 1
           ELSE
              nfr = nfr + 1
              ifs = nfr + 1
              ip_edgePiece(1,nfr) = isr
              ip_edgePiece(2,nfr) = is2
           ENDIF

           IF(ifs>nfr) THEN
              ifs = nfr
           ELSE
              ifs = ifs - 1
           ENDIF

           CYCLE Loop_ifs

        ENDDO Loop_ie_1

        sfmin = -1.0E+06
        jek   = 0
        indk  = 0

        !--- find which edge intersect with the face Itri

        Loop_ie_2 : DO ie=1,NB_PoleTet
           it = IT_PoleTet(ie)

           DO ind = 1,12
              IF(  IP_Tet(I_Comb_Tet(3,ind),it)==is1  .AND.    &
                   IP_Tet(I_Comb_Tet(4,ind),it)==is2      ) EXIT
           ENDDO
           IF(ind>12) CALL Error_Stop (' Recovery_Face : error of pole---')

           i1 = IP_Tet(I_Comb_Tet(1,ind),it)
           i2 = IP_Tet(I_Comb_Tet(2,ind),it)

           DO jb =  1 , newb
              IF( (is1==ip_triPiece(1,jb) .OR. is1==ip_triPiece(2,jb) .OR.         &
                   is1==ip_triPiece(3,jb))                                  .AND.  &
                  (is2==ip_triPiece(1,jb) .OR. is2==ip_triPiece(2,jb) .OR.         &
                   is2==ip_triPiece(3,jb))                                  .AND.  &
                  (i1 ==ip_triPiece(1,jb) .OR. i2 ==ip_triPiece(1,jb) .OR.         &
                   i1 ==ip_triPiece(2,jb) .OR. i2 ==ip_triPiece(2,jb) .OR.         &
                   i1 ==ip_triPiece(3,jb) .OR. i2 ==ip_triPiece(3,jb))  ) CYCLE Loop_ie_2
           ENDDO

           pp2(:,1) = Posit(:,i1)
           pp2(:,2) = Posit(:,i2)
           ntype = 0
           xsect = Plane3D_Line_Tri_Cross(pp2,pp3,ntype,w3,t)

           IF (ntype==7) THEN
              !--- an intersection point found inside a triangle
              !--- compare the minimum weight
              srfm = MIN(w3(1),w3(2),w3(3))
              IF(srfm<=0) CALL Error_Stop (' Recovery_Face : error with Line_Tri_Cross')
              IF(srfm>sfmin) THEN
                 IF(jek/=0) THEN
                    WRITE(29,*) ' found better intersection'
                    WRITE(29,*) Itri,ipp3,is1,is2
                    WRITE(29,*) sfmin,srfm,jek,it
                 ENDIF
                 jek = it
                 indk = ind
                 sfmin = srfm
                 xsectk(:) = xsect(:)
                 IF(srfm>1.e-5) EXIT Loop_ie_2
              ENDIF
           ELSE IF(ntype>0)THEN
              WRITE(29,*)' Error--- intersection through line or point, ntype=',ntype
              WRITE(29,*)' Itri=',Itri,' ipp3=',IP_BD_Tri(1:3,Itri)
              WRITE(29,*)' i1,i2=',i1,i2,' w3=',REAL(w3), ' t=', REAL(t)
              CALL Error_Stop ('Recovery_Face')
           ENDIF

        ENDDO Loop_ie_2

        IF(jek==0)THEN
           WRITE(29,*)'Error---  : can not find jek'
           WRITE(29,*)' Itri=',Itri,' is1, is2=',is1,is2
           CALL Error_Stop ('Recovery_Face')
        ENDIF
        
        !--- the edge intersecting with the triangle is found: i1--i2
        it = jek
        i1 = IP_Tet(I_Comb_Tet(1,indk),it)
        i2 = IP_Tet(I_Comb_Tet(2,indk),it)
        CALL Search_Pole_Surround(IT, indk)

        !--- try to swap this edge at first;
        Isucc = 0
        CALL Swap3D_1Edge(i1, i2, 1, vmin, Isucc)
        IF(Isucc==1) CYCLE

        !---  fail to swap, then break it.
        !---  create a new point
        NB_Point = NB_Point+1
        IF(NB_Point>nallc_point) CALL ReAllocate_Point()
        Posit(:,NB_Point)= xsectk(:)

        !---  split edge by inserting the new point.
        Isucc = 1
        CALL Insert_Point_SplitEdge(NB_Point,i1,i2,Isucc)

        IF(1==0)THEN
           !--- following collapse seems not very helpful
           ipp2(1:2) = (/is1,is2/)
           volmin    = vMin
           CALL Collapse_Check(NB_Point,2,ipp2,1,iTarget,volmin,-999,0)
           IF(iTarget>0) THEN
              !--- can move
              CALL Collapse_PointPair(NB_Point,ipp2(iTarget),1)
              NB_Point = NB_Point-1
              CYCLE
           ENDIF
        ENDIF

        !---  Update the front
        newb = newb + 1
        ip_triPiece(1,newb) = is1
        ip_triPiece(2,newb) = is2
        ip_triPiece(3,newb) = NB_Point

        ip_edgePiece(:,ifs) = ip_edgePiece(:,nfr)
        ip_edgePiece(1,nfr) = is1
        ip_edgePiece(2,nfr) = NB_Point
        nfr = nfr + 1
        ip_edgePiece(1,nfr) = NB_Point
        ip_edgePiece(2,nfr) = is2
        ifs = nfr

     ENDDO Loop_ifs

     IF(nfr<3 .AND. nfr/=0) WRITE(29,*)'Error:',Itri,nfr,newb
     IF(nfr>=3) WRITE(29,*)  'not recovered:',Itri,nfr,newb
     IF(nfr==0) ifcomp = ifcomp + 1

     IF(newb/=0) THEN
        CALL sort_ipp_tri(ip_triPiece(1:3,1))
        IP_BD_Tri(1:3,Itri) = ip_triPiece(1:3,1)
        DO jb = 2,newb
           nbn = nbn + 1
           IF(nbn>nallc_Tri) THEN
              nallc_Tri  = nbn+1000
              CALL allc_2Dpointer(IP_BD_Tri,    5, nallc_Tri,  'IP_BD_Tri')
              CALL allc_2Dpointer(IEdge_BD_Tri, 3, nallc_Tri,  'IEdge_BD_Tri')
           ENDIF
           CALL sort_ipp_tri(ip_triPiece(1:3,jb))
           IP_BD_Tri(1:3,nbn) = ip_triPiece(1:3,jb)
           IP_BD_Tri(4,nbn) = Itri
           IP_BD_Tri(5,nbn) = IP_BD_Tri(5,Itri)
        ENDDO
     ENDIF

  ENDDO Loop_Itri

  PRINT *, ' no. faces recovered   : ', ifrec
  PRINT *, ' no. faces compositted : ', ifcomp
  PRINT *, ' no. faces & pieces    : ', NB_BD_Tri,nbn

  CALL Remove_Tet()

  NB_BD_Tri = nbn
  NB_BD_Point = NB_BD_Point + (NB_Point - NP_Origin)

END SUBROUTINE Recovery_Face


!*******************************************************************************
!>
!!  Lift the additional points from the surface by collapsing then to an internal point or moving them to the internal domain.
!!  This subroutine can only be used in the case edges surrounded by two elemebts only.
!!  @param[in] common_Recovery3D::NP_Origin  :  the number of points before recovery.
!!  @param[in] common_Recovery3D::NP_withEdgeBreak  :  the number of points after recovery edges.
!!  @param[in] common_Parameters::NB_Point  :  the number of points at present
!!                                             (after recovery faces).
!!  @param[in] common_Parameters::Surf  :  the original surface triangulation.
!!  @param[in] common_Parameters::IP_BD_Tri
!!                          :  the surface triangulation at present.
!!                          (including extra nodes and fractured triangles).
!!  @param[out] common_Parameters::IP_BD_Tri
!!                          :  the surface triangulation after lifting points.
!!  @param[out] common_Parameters::IP_Tet
!!                          :  the updated mesh after lifting points.
!<
!*******************************************************************************
SUBROUTINE Recovery_LiftPoint( )

  USE Geometry3DAll
  USE array_allocator
  USE common_Constants
  USE common_Parameters
  USE common_Recovery3D
  IMPLICIT NONE

  INTEGER :: ifrnt(2,1000), newtr(3,1000), idFadeTri(1000), Loops(1000,2)
  INTEGER :: nFadeTri, nonf, ntri
  INTEGER :: nbndpo, nbold, ic1, ic2, n, i, ii, is, loop, k
  INTEGER :: ipp3(3), ip, ie, ib, j1, j2, jj, it, ke, kke
  INTEGER :: k1,k2,k3,k4, l1,l2,l3, i1,i2, m, jb
  REAL*8  :: normal(3), dirt(3)
  INTEGER :: Its_List_Save(ChainTotalLength)
  INTEGER :: List_Length_Save

  INTEGER, DIMENSION(:),  POINTER :: ipMark
  TYPE (LinkAssociationType) :: PointAssoTri


  !--- Re-Associate points with elements.
  CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)

  !--- Associate points with surface triangles
  CALL LinkAssociation_Build(3, NB_BD_Tri, IP_BD_Tri(1:3,1:NB_BD_Tri), NB_Point, PointAssoTri)

  ALLOCATE( ipMark(nallc_Point) )
  Mark_Point(1:NB_Point) = 0

  !--- mark original boundary nodes with 1
  nbndpo = NB_Extra_Point+Surf%NB_Point
  Mark_Point(1:nbndpo)  = 1

  !--- mark added nodes on edges of the original triangle with 2
  Mark_Point(NP_Origin+1 : NP_withEdgeBreak)  =  2

  !--- mark added nodes inside the original triangle with 3
  Mark_Point(NP_withEdgeBreak+1 : NB_Point)  =  3

  WRITE(29,*)'nbndpo,NP_Origin,NP_withEdgeBreak,nb_point='
  WRITE(29,*)nbndpo,NP_Origin,NP_withEdgeBreak,nb_point


  !--- loop over all added points
  Loop_IP : DO ip  =  NB_Point, NP_Origin + 1 , -1

     !--- List all triangles surrounding point ip in the array ITs_List
     CALL LinkAssociation_List(ip, List_Length, ITs_List, PointAssoTri)
     IF(List_Length==0) CALL Error_Stop (' Recovery_LiftPoint---List_Length')
     !---  find the boundary edges surrounding the added node

     nFadeTri  = 0
     nonf      = 0

     DO n = 1,List_Length
        ke  = ITs_List(n)

        ipp3(1:3)   =  IP_BD_Tri(1:3,ke)

        DO i = 1, 3
           IF(ipp3(i)==ip) THEN
              nonf  =  nonf + 1
              ifrnt(1,nonf)  =  ipp3(MOD(i,3)+1)
              ifrnt(2,nonf)  =  ipp3(MOD(i+1,3)+1)
              ipMark(ifrnt(1:2,nonf)) = IP_BD_Tri(4,ke)
           ENDIF
        ENDDO

        nFadeTri  =  nFadeTri + 1
        idFadeTri(nFadeTri)  =  ke

     ENDDO

     !---  If added point is on an edge
     !        then update the number of surounding edges
     ic1    =  0
     ic2    =  0
     IF(Mark_Point(ip)==2) THEN
        !--- ip is on centre of original boundary edge
        !    find two end of this edge ic1, ic2
        ic1 = Prev_BD_Pt(ip)
        ic2 = Next_BD_Pt(ip)
        IF(ic1==0 .OR. ic2==0) CALL Error_Stop (' Recovery_LiftPoint :  Error by Prev/Next_BD_Pt')
        Loops(1:2,1) = (/ic1,ic2/)
        Loops(1:2,2) = (/ic2,ic1/)
        ipMark(ic1) = 0
        ipMark(ic2) = 0
        dirt(:) = Posit(:,ic2) - Posit(:,ic1)
     ELSE
        Loops(1:2,1) = ifrnt(1:2,nonf)
        Loops(1:2,2) = 0
        nonf = nonf-1
     ENDIF

     !---  order the edges into one or 2 loops ( O or OO )
     ntri = 0
     DO loop=1,2
        IF(Loops(1,loop)==0) EXIT
        is = 2
        DO WHILE(Loops(is,loop)/=Loops(1,loop))
           DO jj = 1,nonf
              IF(ifrnt(1,jj)==Loops(is,loop))THEN
                 is = is+1
                 Loops(is,loop) = ifrnt(2,jj)
                 IF(Debug_Display>3)THEN
                    IF(  ipMark(ifrnt(1,jj))>0 .AND. ipMark(ifrnt(2,jj))>0 .AND.   &
                         ipMark(ifrnt(1,jj))/=ipMark(ifrnt(2,jj)) )THEN
                       CALL Error_Stop ('Recovery_LiftPoint : ipmark')
                    ENDIF
                 ENDIF
                 EXIT
              ENDIF
           ENDDO
           IF(jj>nonf) CALL Error_Stop ('Recovery_LiftPoint : broken loop')
           IF(jj<nonf) ifrnt(:,jj) = ifrnt(:,nonf)
           nonf = nonf-1
        ENDDO
        is = is-1     !--- do need repeat two ends for PlaneTriangulator
        IF(is<3)THEN
           WRITE(29,*)'Error--- not a close loop'
           WRITE(29,*)' IP=',IP,' is=',is,' loop=',loops(1:is,loop)
           CALL Error_Stop ('Recovery_LiftPoint')
        ENDIF

        !---  triangulate the edges stating from the smallest angle
        normal(:) = Geo3D_Cross_Product(Posit(:,Loops(3,loop)),   &
             Posit(:,Loops(2,loop)),Posit(:,ip))
        CALL PlaneTriangulator(is, Loops(:,loop), Posit, normal, ntri, newtr)

        IF(nonf==0) EXIT
     ENDDO

     IF(nonf/=0)THEN
        WRITE(29,*)' Error---  wrong Loop. ip,nonf=',ip,nonf
        WRITE(29,*)' Please try with setting RECOVERY_METHOD=4.'
        CALL Error_Stop ('Recovery_LiftPoint: wrong loop')
     ENDIF


     !--- List all elements surrounding point ip in the array ITs_List
     CALL LinkAssociation_List(ip, List_Length, ITs_List, PointAsso)
     List_Length_Save = List_Length
     ITs_List_Save(1:List_Length_Save) = ITs_List(1:List_Length)


     DO jj  =  1 , ntri

        !---  creat two elements with zero volume for each new triangle

        IF(jj==1)THEN
           !---  add new point to the outside
           NB_Point   =  NB_Point  + 1
           IF(NB_Point>nallc_point) CALL ReAllocate_Point()
           Posit(:,NB_Point)  =  Posit(:,ip)
        ENDIF

        IF(NB_Tet+2>nallc_Tet) CALL ReAllocate_Tet()

        NB_Tet             =  NB_Tet + 1
        IP_Tet(1:3,NB_Tet)    =  newtr(1:3,jj)
        IP_Tet(4,NB_Tet)      =  ip
        Mark_Tet(NB_Tet)      =  1   !--- an element inside the boundary surrounding domain
        CALL LinkAssociation_AddCell(4, NB_Tet,IP_Tet(:,NB_Tet),PointAsso)

        NB_Tet             =  NB_Tet + 1
        IP_Tet(1,NB_Tet)      =  newtr(1,jj)
        IP_Tet(2,NB_Tet)      =  newtr(3,jj)
        IP_Tet(3,NB_Tet)      =  newtr(2,jj)
        IP_Tet(4,NB_Tet)      =  NB_Point
        Mark_Tet(NB_Tet)      =  2   !--- an element outside the boundary surrounding domain
        CALL LinkAssociation_AddCell(4, NB_Tet,IP_Tet(:,NB_Tet),PointAsso)

     ENDDO


     !--- update Next/Prev_BD_Pt
     IF(Mark_Point(ip)==2)THEN
        IF(Next_BD_Pt(ic1)==ip) Next_BD_Pt(ic1) = ic2
        IF(Prev_BD_Pt(ic2)==ip) Prev_BD_Pt(ic2) = ic1
     ENDIF

     !---  break all external elements connection to ip and connenct them to NB_Point
     DO n  =  1 , List_Length_Save
        ie  =  ITs_List_Save(n)
        IF(Mark_Tet(ie)==2) THEN
           CALL LinkAssociation_Remove(4, ie,IP_Tet(:,ie),PointAsso)
           WHERE(IP_Tet(1:4,ie)==ip) IP_Tet(1:4,ie)  =  NB_Point
           CALL LinkAssociation_AddCell(4, ie,IP_Tet(:,ie),PointAsso)
        ENDIF
     ENDDO

     !---  update the data structure of the boundary faces

     DO i=1,nFadeTri
        jb = idFadeTri(i)
        CALL LinkAssociation_Remove(3, jb,IP_BD_Tri(1:3,jb),PointAssoTri)
     ENDDO

     DO i  =  1 , ntri     !---ntri < nFadeTri
        jb = idFadeTri(i)

        IP_BD_Tri(1:3,jb)  =  newtr(1:3,i)
        CALL LinkAssociation_AddCell(3, jb,IP_BD_Tri(1:3,jb),PointAssoTri)
        IP_BD_Tri(4,jb) = 0
        DO ii=1,3
           IF(ipMark(newtr(ii,i))>0)THEN
              IF(IP_BD_Tri(4,jb)==0)THEN
                 IP_BD_Tri(4,jb) = ipMark(newtr(ii,i))
              ELSE IF(IP_BD_Tri(4,jb)/=ipMark(newtr(ii,i)))THEN
                 PRINT*,'ip,jb,newtr,ipmark=',ip,jb,newtr(:,i),ipmark(newtr(:,i))
                 CALL Error_Stop (' Recovery_LiftPoint :  wrong ipMark 1')
              ENDIF
           ENDIF
        ENDDO
        IF(IP_BD_Tri(4,jb)==0) CALL Error_Stop (' Recovery_LiftPoint :  wrong ipMark 2')

     ENDDO

     !--- mark triangles to be deleted

     DO i  = ntri+1,  nFadeTri   !---ntri < nFadeTri
        jb = idFadeTri(i)
        IP_BD_Tri(4,jb) = -1
     ENDDO

     !---  try to remove the point

     NB_BD_Point  =  NB_BD_Point - 1
  ENDDO Loop_IP

  DEALLOCATE( ipMark )

  IF(Debug_Display>3)THEN
     DO ib = 1, NB_BD_Tri
        jb = IP_BD_Tri(4,ib)
        IF(jb==-1) CYCLE
        IF(jb<=0 .OR. jb>Surf%NB_Tri) CALL Error_Stop (' check ib, jb')
        CALL sort_ipp_tri(IP_BD_Tri(1:3,ib))
        DO i=1,3
           IF(Surf%IP_Tri(i,jb) /= IP_BD_Tri(i,ib)-NB_Extra_Point)THEN
              WRITE(29,*) 'Error--- surface triangles does not return'
              WRITE(29,*)'ib,IP_BD_Tri=',ib,IP_BD_Tri(:,ib)
              WRITE(29,*)'jb,Surf%IP_Tri=',jb,Surf%IP_Tri(:,jb)
              CALL Error_Stop ('Recovery_LiftPoint')
           ENDIF
        ENDDO
     ENDDO
  ENDIF

  NB_BD_Tri = Surf%NB_Tri

  DO ib = 1,Surf%NB_Tri
     IP_BD_Tri(1:3,ib) = Surf%IP_Tri(1:3,ib) + NB_Extra_Point
     IP_BD_Tri(4:5,ib) = Surf%IP_Tri(4:5,ib)
  ENDDO

  WRITE(*, *)' lifted all points with...'
  WRITE(*, *)' NB_BD_Point, NB_BD_Tri, NB_Point=', NB_BD_Point, NB_BD_Tri, NB_Point
  WRITE(29,*)' lifted all points with...'
  WRITE(29,*)' NB_BD_Point, NB_BD_Tri, NB_Point=', NB_BD_Point, NB_BD_Tri, NB_Point


  RETURN
END SUBROUTINE Recovery_LiftPoint

!*******************************************************************************
!>
!!  Lift the additional points from the surface by collapsing then to 
!!       an internal point or moving them to the internal domain.
!!  This subroutine is used in the case edges are surrounded by more than 2 elements.
!!  @param[in] common_Recovery3D::NP_Origin    :  the number of points before recovery.
!!  @param[in] common_Recovery3D::NP_withEdgeBreak  :  the number of points after recovery edges.
!!  @param[in] common_Parameters::NB_Point  :  the number of points at present
!!                                             (after recovery faces).
!!  @param[in] common_Parameters::Surf  :  the original surface triangulation.
!!  @param[in] common_Parameters::IP_BD_Tri
!!                          :  the surface triangulation at present.
!!                          (including extra nodes and fractured triangles).
!!  @param[out] common_Parameters::IP_BD_Tri
!!                          :  the surface triangulation after lifting points.
!!  @param[out] common_Parameters::IP_Tet
!!                          :  the updated mesh after lifting points.
!<
!*******************************************************************************
SUBROUTINE Recovery_LiftPoint_2( )

  USE Geometry3DAll
  USE array_allocator
  USE common_Constants
  USE common_Parameters
  USE common_Recovery3D
  IMPLICIT NONE

  INTEGER :: ifrnt(2,1000), newtr(3,1000), idFadeTri(1000), Loops(1000,5)
  INTEGER :: loopMark(4), idtri(4,3000), idtet(2,3000)
  INTEGER :: npairs, ipair(2,4), ipa, ipn
  INTEGER :: nFadeTri, nonf, ntri, ntrii(0:4)
  INTEGER :: nbndpo, nbold, ic1, ic2, n, i, ii, is, loop, nloop, k
  INTEGER :: ipp3(3), ip, ie, ib, j1, j2, jj, it, ke
  INTEGER :: k1,k2,k3,k4, i1,i2, m, jb
  REAL*8  :: normal(3)
  INTEGER :: ITs_List_Tet(ChainTotalLength), ITs_List_Tri(ChainTotalLength)
  INTEGER :: List_Length_Tet, List_Length_Tri
  LOGICAL :: CarryOn

  INTEGER, DIMENSION(:),  POINTER :: ipMark
  TYPE (LinkAssociationType) :: PointAssoTri
  TYPE (NodeNetTreeType) :: TriTree


  !--- Re-Associate points with elements.
  CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)

  !--- Associate points with surface triangles
  CALL LinkAssociation_Build(3, NB_BD_Tri, IP_BD_Tri(1:3,1:NB_BD_Tri), NB_Point, PointAssoTri)

  CALL NodeNetTree_Allocate(3, NB_Point, 3000, TriTree)

  ALLOCATE( ipMark(nallc_Point) )
  Mark_Point(1:NB_Point) = 0

  !--- mark original boundary nodes with 1
  nbndpo = NB_Extra_Point+Surf%NB_Point
  Mark_Point(1:nbndpo)  = 1

  !--- mark added nodes on the edges of the original triangle with 2
  Mark_Point(NP_Origin+1 : NP_withEdgeBreak)  =  2

  !--- mark added nodes on the inside of the original triangle with 3
  Mark_Point(NP_withEdgeBreak+1 : NB_Point)  =  3

  WRITE(29,*)'nbndpo,NP_Origin,NP_withEdgeBreak,nb_point='
  WRITE(29,*)nbndpo,NP_Origin,NP_withEdgeBreak,nb_point


  !--- loop over all added points
  Loop_IP : DO ip  =  NB_Point, NP_Origin + 1 , -1

     CALL LinkAssociation_List(ip, List_Length, ITs_List, PointAsso)
     List_Length_Tet = List_Length
     ITs_List_Tet(1:List_Length_Tet) = ITs_List(1:List_Length)

     CALL LinkAssociation_List(ip, List_Length, ITs_List, PointAssoTri)
     List_Length_Tri = List_Length
     ITs_List_Tri(1:List_Length_Tri) = ITs_List(1:List_Length)

     !---  find the boundary edges surrounding the added node

     nFadeTri  = 0
     nonf      = 0

     DO n = 1,List_Length_Tri
        ke  = ITs_List_Tri(n)
        ipp3(1:3)   =  IP_BD_Tri(1:3,ke)
        DO i = 1, 3
           IF(ipp3(i)==ip) THEN
              nonf  =  nonf + 1
              ifrnt(1,nonf)  =  ipp3(MOD(i,3)+1)
              ifrnt(2,nonf)  =  ipp3(MOD(i+1,3)+1)
              ipMark(ifrnt(1:2,nonf)) = IP_BD_Tri(4,ke)
           ENDIF
        ENDDO
        nFadeTri  =  nFadeTri + 1
        idFadeTri(nFadeTri)  =  ke
     ENDDO

     !---  If added point is on an edge
     !        then add an edge to make double loop
     ic1    =  0
     ic2    =  0
     IF(Mark_Point(ip)==2) THEN
        !--- ip is on centre of original boundary edge
        !    find two end of this edge ic1, ic2
        ic1 = Prev_BD_Pt(ip)
        ic2 = Next_BD_Pt(ip)
        IF(ic1==0 .OR. ic2==0) CALL Error_Stop ('Recovery_LiftPoint_2:  Error by Prev/Next_BD_Pt')
        Loops(1:2,1) = (/ic1,ic2/)
        Loops(1:2,2) = (/ic2,ic1/)
        Loops(1:2,3) = (/ic1,ic2/)
        Loops(1:2,4) = (/ic2,ic1/)
        ipMark(ic1) = 0
        ipMark(ic2) = 0
     ELSE
        Loops(1:2,1) = ifrnt(1:2,nonf)
        Loops(1:2,2) = 0
        Loops(1:2,3) = 0
        Loops(1:2,4) = 0
        nonf = nonf-1
     ENDIF

     !---  order the edges into a single or double loop ( O or OO )
     !     triangulate each ring
     ntri     = 0
     ntrii(0) = 0
     loopMark(:) = 0
     DO loop=1,4
        IF(Loops(1,loop)==0) EXIT
        is = 2
        DO WHILE(Loops(is,loop)/=Loops(1,loop))
           DO jj = 1,nonf
              IF(ifrnt(1,jj)==Loops(is,loop))THEN
                 is = is+1
                 Loops(is,loop) = ifrnt(2,jj)
                 IF(loopMark(loop)==0)THEN
                    loopMark(loop) = ipMark(Loops(is,loop))
                 ELSE IF(ipMark(Loops(is,loop))>0)THEN
                    IF(loopMark(loop)/=ipMark(Loops(is,loop)))THEN
                       CALL Error_Stop ('Recovery_LiftPoint_2: loopMark')
                    ENDIF
                 ENDIF
                 EXIT
              ENDIF
           ENDDO
           IF(jj>nonf) CALL Error_Stop (' Recovery_LiftPoint_2 : broken loop')
           IF(jj<nonf) ifrnt(:,jj) = ifrnt(:,nonf)
           nonf = nonf-1
        ENDDO
        is = is-1     !--- do not need repeat two ends for PlaneTriangulator
        IF(is<3)THEN
           IF(loop<=2)THEN
              WRITE(29,*)'Error--- not a close loop'
              WRITE(29,*)' IP=',IP,' is=',is,' loop=',loops(1:is,loop)
              CALL Error_Stop ('Recovery_LiftPoint_2')
           ELSE
              is = 0
           ENDIF
        ENDIF

        !---  triangulate each loop
        IF(is==0)THEN
           ntrii(loop) = ntrii(loop-1)
        ELSE
           normal(:) = Geo3D_Cross_Product(Posit(:,Loops(3,loop)),   &
                Posit(:,Loops(2,loop)),Posit(:,ip))
           CALL PlaneTriangulator(is, Loops(:,loop), Posit, normal, ntri, newtr)
           ntrii(loop) = ntri
        ENDIF

        IF(nonf==0) EXIT
     ENDDO
     nloop = loop

     IF(nonf/=0)THEN
        WRITE(29,*)' Error---  wrong Loop. ip,nonf=',ip,nonf
        CALL Error_Stop ('Recovery_LiftPoint_2: wrong loop')
     ENDIF

     !--- mark boundary trinagle based on their loop

     DO n = 1,List_Length_Tri
        ke = ITs_List_Tri(n)
        ipp3(1:3) = IP_BD_Tri(1:3,ke)
        CALL sort_ipp_tri(ipp3(1:3))
        CALL NodeNetTree_SearchAdd(3,ipp3,TriTree,IB)
        idtri(1:3,IB) = ipp3(1:3)
        IF(nloop<=2)THEN
           idtri(4, IB) = 1
        ELSE
           DO loop = 1,4
              IF(IP_BD_Tri(4,ke)==loopMark(loop))THEN
                 idtri(4, IB) = loop
                 EXIT
              ENDIF
           ENDDO
        ENDIF
     ENDDO

     IF(nloop<=2)THEN
        !--- simple connection: 1 or 2 loops
        npairs = 2
        ipair(:,1) = (/ 1, 2/)
        ipair(:,2) = (/-1,-2/)

        !--- mark element on each side
        CarryOn = .TRUE.
        idtet(1,1:List_Length_Tet) = 0
        DO WHILE(CarryOn)
           CarryOn = .FALSE.
           DO n = 1,List_Length_Tet
              IF(idtet(1,n) /= 0) CYCLE
              IT = ITs_List_Tet(n)
              k1 = 0
              k2 = 0
              k3 = 0
              DO i=1,4
                 ipp3(1:3) = IP_Tet(iTri_Tet(1:3,i), IT)
                 IF(ipp3(1)/=ip .AND. ipp3(2)/=ip .AND. ipp3(3)/=ip) CYCLE
                 CALL sort_ipp_tri(ipp3(1:3))
                 CALL NodeNetTree_Search(3,ipp3,TriTree,IB)
                 IF(ib==0)THEN
                    IF(k1==0)THEN
                       k1 = i
                       k2 = i
                    ELSE
                       k2 = i
                    ENDIF
                 ELSE
                    IF(ipp3(2)==idtri(2,IB))THEN
                       k3 = idtri(4,IB)
                    ELSE
                       k3 = 3-idtri(4,IB)
                    ENDIF
                    IF(idtet(1,n)==0)THEN
                       idtet(1,n) = k3
                    ELSE IF(idtet(1,n) /= k3)THEN
                       CALL Error_Stop ('Recovery_LiftPoint_2: orientation')
                    ENDIF
                 ENDIF
              ENDDO

              IF(k1>0 .AND. k3/=0)THEN
                 k4 = MAX(1, k2-k1)
                 DO i= k1, k2, k4
                    ipp3(1:3) = IP_Tet(iTri_Tet(1:3,i), IT)
                    CALL sort_ipp_tri(ipp3(1:3))
                    CALL NodeNetTree_SearchAdd(3,ipp3,TriTree,IB)
                    idtri(1:4,IB) = (/ipp3(1), ipp3(3), ipp3(2), k3/)
                 ENDDO
              ELSE IF(k3==0)THEN
                 CarryOn = .TRUE.
              ENDIF

           ENDDO
        ENDDO

     ELSE
        !--- Edges surrounded by more than 2 elements----

        !--- mark elements by adjacent triangles

        CarryOn = .TRUE.
        idtet(:,1:List_Length_Tet) = 0
        DO WHILE(CarryOn)
           CarryOn = .FALSE.
           DO n = 1,List_Length_Tet
              IF(idtet(1,n) /= 0) CYCLE
              IT = ITs_List_Tet(n)
              k1 = 0
              k2 = 0
              k3 = 0
              DO i=1,4
                 ipp3(1:3) = IP_Tet(iTri_Tet(1:3,i), IT)
                 IF(ipp3(1)/=ip .AND. ipp3(2)/=ip .AND. ipp3(3)/=ip) CYCLE
                 CALL sort_ipp_tri(ipp3(1:3))
                 CALL NodeNetTree_Search(3,ipp3,TriTree,IB)
                 IF(ib==0)THEN
                    IF(k1==0)THEN
                       k1 = i
                       k2 = i
                    ELSE
                       k2 = i
                    ENDIF
                 ELSE
                    IF(ipp3(2)==idtri(2,IB))THEN
                       k3 = idtri(4,IB)
                    ELSE
                       k3 =  -idtri(4,IB)
                    ENDIF
                    IF(idtet(1,n)==0)THEN
                       idtet(1,n) = k3
                    ELSE IF(idtet(1,n)/=k3)THEN
                       IF(idtet(2,n)==0)THEN
                          idtet(2,n) = k3
                       ELSE IF(idtet(2,n)/=k3)THEN
                          CALL Error_Stop ('Recovery_LiftPoint_2: orientation 2')
                       ENDIF
                    ENDIF
                 ENDIF
              ENDDO

              IF(k1>0 .AND. k3/=0)THEN
                 k4 = MAX(1, k2-k1)
                 DO i= k1, k2, k4
                    ipp3(1:3) = IP_Tet(iTri_Tet(1:3,i), IT)
                    CALL sort_ipp_tri(ipp3(1:3))
                    CALL NodeNetTree_SearchAdd(3,ipp3,TriTree,IB)
                    idtri(1:4,IB) = (/ipp3(1), ipp3(3), ipp3(2), k3/)
                 ENDDO
              ELSE IF(k3==0)THEN
                 CarryOn = .TRUE.
              ENDIF
           ENDDO
        ENDDO

        !--- match pairs
        npairs = 0
        DO n = 1,List_Length_Tet
           k1 = idtet(1,n)
           k2 = idtet(2,n)
           IF(k2/=0)THEN
              DO ipa=1,npairs
                 IF(k1==ipair(1,ipa) .OR. k1==ipair(2,ipa))THEN
                    IF(k2/=ipair(1,ipa) .AND. k2/=ipair(2,ipa))THEN
                       CALL Error_Stop ('Recovery_LiftPoint_2: k1,k2')
                    ENDIF
                    EXIT
                 ENDIF
              ENDDO
              IF(ipa>npairs)THEN
                 !--- a new pair
                 npairs = npairs+1
                 ipair(1:2,ipa) = (/k1,k2/)
              ENDIF
           ENDIF
        ENDDO

        !--- locate cell
        DO n = 1,List_Length_Tet
           DO ipa=1,npairs
              IF(idtet(1,n)==ipair(1,ipa) .OR. idtet(1,n)==ipair(2,ipa))THEN
                 idtet(1,n) = ipa
                 EXIT
              ENDIF
           ENDDO
        ENDDO

     ENDIF


     !--- create / change elements

     DO ipa = 1, npairs
        IF(ipa==1)THEN
           ipn = ip
        ELSE
           !---  add new point to the outside
           NB_Point  =  NB_Point  + 1
           IF(NB_Point>nallc_point) CALL ReAllocate_Point()
           Posit(:,NB_Point)  =  Posit(:,ip)
           ipn = NB_Point
        ENDIF

        !---  create two elements with zero volume for each new triangle
        DO k = 1,2
           k1 = ntrii(ABS(ipair(k,ipa))-1) +1
           k2 = ntrii(ABS(ipair(k,ipa)))
           DO jj  =  k1, k2


              NB_Tet  =  NB_Tet + 1
              IF(NB_Tet>nallc_Tet) CALL ReAllocate_Tet()
              IF(ipair(k,ipa)>0)THEN
                 IP_Tet(1:3,NB_Tet) =     newtr(1:3,jj)
              ELSE
                 IP_Tet(1:3,NB_Tet) =  (/ newtr(1,jj), newtr(3,jj), newtr(2,jj) /)
              ENDIF
              IP_Tet(4,NB_Tet)   =  ipn
              CALL LinkAssociation_AddCell(4, NB_Tet,IP_Tet(:,NB_Tet),PointAsso)

           ENDDO
        ENDDO

        IF(ipa==1) CYCLE

        !---  break all external elements connecting to ip and connenct them to  NB_Point
        DO n  =  1 , List_Length_Tet
           IF(idtet(1,n)/=ipa) CYCLE
           ie  =  ITs_List_Tet(n)
           CALL LinkAssociation_Remove(4, ie,IP_Tet(:,ie),PointAsso)
           WHERE(IP_Tet(1:4,ie)==ip) IP_Tet(1:4,ie)  =  ipn
           CALL LinkAssociation_AddCell(4, ie,IP_Tet(:,ie),PointAsso)
        ENDDO
     ENDDO

     !--- update Next/Prev_BD_Pt

     IF(Mark_Point(ip)==2)THEN
        IF(Next_BD_Pt(ic1)==ip) Next_BD_Pt(ic1) = ic2
        IF(Prev_BD_Pt(ic2)==ip) Prev_BD_Pt(ic2) = ic1
     ENDIF

     !---  update the data structure of the boundary faces
     DO i=1,nFadeTri
        jb = idFadeTri(i)
        CALL LinkAssociation_Remove(3, jb,IP_BD_Tri(1:3,jb),PointAssoTri)
     ENDDO

     DO i  =  1 , ntri     !---ntri < nFadeTri
        jb = idFadeTri(i)

        IP_BD_Tri(1:3,jb)  =  newtr(1:3,i)
        CALL LinkAssociation_AddCell(3, jb,IP_BD_Tri(1:3,jb),PointAssoTri)
        IP_BD_Tri(4,jb) = 0
        DO ii=1,3
           IF(ipMark(newtr(ii,i))>0)THEN
              IF(IP_BD_Tri(4,jb)==0)THEN
                 IP_BD_Tri(4,jb) = ipMark(newtr(ii,i))
              ELSE IF(IP_BD_Tri(4,jb)/=ipMark(newtr(ii,i)))THEN
                 PRINT*,'ip,jb,newtr,ipmark=',ip,jb,newtr(:,i),ipmark(newtr(:,i))
                 CALL Error_Stop (' Recovery_LiftPoint_2 :  wrong ipMark 1')
              ENDIF
           ENDIF
        ENDDO
        IF(IP_BD_Tri(4,jb)==0) CALL Error_Stop (' Recovery_LiftPoint_2 :  wrong ipMark 2')

     ENDDO

     !--- mark triangle to be deleted
     DO i  = ntri+1,  nFadeTri   !---ntri < nFadeTri
        jb = idFadeTri(i)
        IP_BD_Tri(4,jb) = -1
     ENDDO

     CALL NodeNetTree_Zerolise(TriTree)

     !---  try to remove the point
     NB_BD_Point  =  NB_BD_Point - 1

  ENDDO Loop_IP

  DEALLOCATE( ipMark )
  CALL NodeNetTree_Clear(TriTree)

  IF(Debug_Display>3)THEN
     DO ib = 1, NB_BD_Tri
        jb = IP_BD_Tri(4,ib)
        IF(jb==-1) CYCLE
        IF(jb<=0 .OR. jb>Surf%NB_Tri) CALL Error_Stop (' check ib, jb')
        CALL sort_ipp_tri(IP_BD_Tri(1:3,ib))
        DO i=1,3
           IF(Surf%IP_Tri(i,jb) /= IP_BD_Tri(i,ib)-NB_Extra_Point)THEN
              WRITE(29,*) 'Error--- surface triangles does not return'
              WRITE(29,*)'ib,IP_BD_Tri=',ib,IP_BD_Tri(:,ib)
              WRITE(29,*)'jb,Surf%IP_Tri=',jb,Surf%IP_Tri(:,jb)
              CALL Error_Stop ('Recovery_LiftPoint_2')
           ENDIF
        ENDDO
     ENDDO
  ENDIF

  NB_BD_Tri = Surf%NB_Tri

  DO ib = 1,Surf%NB_Tri
     IP_BD_Tri(1:3,ib) = Surf%IP_Tri(1:3,ib) + NB_Extra_Point
     IP_BD_Tri(4:5,ib) = Surf%IP_Tri(4:5,ib)
  ENDDO

  WRITE(*, *)' lifted all points with...'
  WRITE(*, *)' NB_BD_Point, NB_BD_Tri, NB_Point=', NB_BD_Point, NB_BD_Tri, NB_Point
  WRITE(29,*)' lifted all points with...'
  WRITE(29,*)' NB_BD_Point, NB_BD_Tri, NB_Point=', NB_BD_Point, NB_BD_Tri, NB_Point


  RETURN
END SUBROUTINE Recovery_LiftPoint_2


!*******************************************************************************
!>
!!  Delete those elements that are outside the computational domain.
!!  @param[in] common_Recovery3D::NP_Origin :  the number of points before recovery.
!!  @param[in] common_Parameters::NB_Point  :  the number of points at present
!!                                             (after lift points).
!!  @param[in] common_Parameters::Mark_Tet (it) = 2 element is out.           \n
!!                                         else, element is in.
!<
!*******************************************************************************
SUBROUTINE Recover_CleanFace( )

  USE Geometry3DAll
  USE common_Parameters
  USE OptimisingModule
  USE common_Recovery3D
  IMPLICIT NONE

  INTEGER :: nmovt, nmov, nfail, ndelt, ndel, Ntry, nnn
  INTEGER :: IT, jj, IP, ips, ib, num_ipStay, ipStay(1000), iTarget, i, n, nf
  REAL*8  :: volmin, pp(3), pps(3,4), dp(3), ppo(3), ppt(3), dd0
  INTEGER, DIMENSION(:),  POINTER :: ipMark

  !--- mark added boundary nodes which inside the domain with 1

  Mark_Point(1:NP_Origin) = 0
  Mark_Point(NP_Origin+1:NB_Point) = 1
  DO IT = 1, NB_Tet
     IF(IP_Tet(4,IT)>0 .AND. Mark_Tet(IT)==2) Mark_Point(IP_Tet(:,IT)) = -2
  ENDDO

  ALLOCATE(ipMark(NB_Point))

  nmovt = 0
  ndelt = 0
  Loop_Ntry : DO Ntry = 1,7
     ndel = 0
     nmov = 0
     nfail = 0
     ipMark(1:NB_Point) = 0

     Loop_IP : DO IP = NB_Point , NP_Origin+1, -1
        IF(Mark_Point(IP)/=1) CYCLE

        !--- List all elements surrounding point ipMove in the array ITs_List
        CALL LinkAssociation_List(IP, List_Length, ITs_List, PointAsso)
        num_ipStay = 0
        DO n = 1,List_Length
           IT = ITs_List(n)
           DO i=1,4
              ips = IP_Tet(i,IT)
              IF(ips==IP) CYCLE
              IF(ipMark(ips)/=IP)THEN
                 ipMark(ips) = IP
                 num_ipStay = num_ipStay+1
                 ipStay(num_ipStay) = ips
              ENDIF
           ENDDO
        ENDDO

        IF(1==1)THEN
           !--- XIE: new option added in Sep. 2011. More test in need
           volmin  = MAX(1.d-6* QualityMin(1), TinyVolume)
           nnn = NB_Tet
           CALL Collapse_withSwap(IP,num_ipStay,ipStay,1,volmin)
           IF(NB_Tet>nnn)THEN
              Mark_Tet(nnn+1:NB_Tet) = 1
           ENDIF
           IF(num_ipStay==0) THEN
              !--- succeed
              Mark_Point(IP) = -999
              ndel = ndel  + 1
              ndelt= ndelt + 1
              CYCLE Loop_IP
           ENDIF
        ENDIF
        
        volmin  = 1.d-6* QualityMin(1)
        
        CALL Collapse_Check(IP,num_ipStay,ipStay,1,iTarget,volmin,-999,0)

        !---  if min volume is possitive then move the point
        IF(iTarget>0 .AND. volmin>0) THEN
           Mark_Point(IP) = -999
           CALL Collapse_PointPair(IP,ipStay(iTarget),0)
           CALL Collapse_identical(ipStay(iTarget))
           ndel = ndel  + 1
           ndelt= ndelt + 1
           CYCLE Loop_IP
        ENDIF

        !---  can not collapse, try to move the point
        !---  search for the best possible location

        dd0 = 0
        DO jj = 1,num_ipStay
           ips = ipStay(jj)
           dd0 = MAX(dd0,ABS(Posit(1,ips)-Posit(1,IP)))
           dd0 = MAX(dd0,ABS(Posit(2,ips)-Posit(2,IP)))
           dd0 = MAX(dd0,ABS(Posit(3,ips)-Posit(3,IP)))
        ENDDO
        dd0 = 1.e-4 *dd0

        !--- set parameters for FUNCTION Optimising_GetValue()
        OptVariable%IdNode  = IP
        OptVariable%IdxFun  = 1    
        
        !--- call Optimising
        CALL Optimising_Set(3, 400, dd0)
        OptParameter%Idistb = 20
        CALL Optimising_Powell(Posit(:,IP), ppt)

        IF(OptParameter%Isucc>0)THEN
           IF(OptParameter%Fend <=0) CALL Error_Stop ('Recover_CleanFace:: Fend <=0')
           Mark_Point(IP) = -1
           nmov  = nmov + 1
           nmovt = nmovt+ 1
           Posit(:,IP) = ppt(:)
        ELSE
           nfail = nfail + 1
        ENDIF

     ENDDO Loop_IP

     WRITE(*, *) 'Recovery_CleanFace:: Ntry,ndel,ndelt,nmov,nmovt,nfail'
     WRITE(*, *) Ntry,ndel,ndelt,nmov,nmovt,nfail
     WRITE(29,*) 'Recovery_CleanFace:: Ntry,ndel,ndelt,nmov,nmovt,nfail'
     WRITE(29,*) Ntry,ndel,ndelt,nmov,nmovt,nfail
     IF(nfail==0 .OR. (nmov==0 .AND. ndel==0)) EXIT
  ENDDO Loop_Ntry

  !---   renumber
  Mark_Point(1:NB_Point) = -999
  nnn = 0
  DO  IT  = 1 , NB_Tet
     IF(Mark_Tet(IT)==2) CYCLE
     IF(IP_Tet(4,IT)<0) CYCLE
     nnn  = nnn + 1
     IP_Tet(1:4,nnn) = IP_Tet(1:4,IT)
     Mark_Point(IP_Tet(1:4,nnn)) = 0
  ENDDO
  NB_Tet = nnn

  CALL Remove_Point_inOrder()
  NB_BD_Point = Surf%NB_Point

  WRITE(*, *) 'Recovery_CleanFace:: NB_BD_Point,NB_BD_Tri,NB_Point,NB_Tet'
  WRITE(*, *) NB_BD_Point,NB_BD_Tri,NB_Point,NB_Tet
  WRITE(29,*) 'Recovery_CleanFace:: NB_BD_Point,NB_BD_Tri,NB_Point,NB_Tet'
  WRITE(29,*) NB_BD_Point,NB_BD_Tri,NB_Point,NB_Tet

  DEALLOCATE( ipMark )

  RETURN
END SUBROUTINE Recover_CleanFace


