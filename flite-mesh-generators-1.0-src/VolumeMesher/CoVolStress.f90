!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  CoVolStress
!!  
!!  Contains all the subroutines needed for the stress based mesh optimisation
!<
!*******************************************************************************


SUBROUTINE CoVolStress()
	USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	USE HybridMeshStorage
	USE TetMeshStorage
	USE PointAssociation
	USE Queue
	IMPLICIT NONE

	REAL*8 :: IP_Disp(3,NB_Point), dw, dwdw, dwdwdw
	INTEGER :: IP_BoundCond(NB_Point)	
	INTEGER :: ip, ie, i, j, k, ip1, ip2, ip3, ip4, nbp, ic
	REAL*8 :: p1(3), p2(3), p3(3), p4(3), pC(3), wdum
	INTEGER :: Kswap,inside


	!Optimisation variables
	INTEGER :: D  !Number of dimensions
	INTEGER :: N !Number of eggs
	REAL*8, ALLOCATABLE :: ptemp(:), Pt(:,:), psave(:) !Temp coordinates, coordinates
	REAL*8 :: ftemp, fbest !Temp fitness
	INTEGER :: Ibest, Loop, Istatus, Iin, II,  idum, MCSFlag
	REAL*8, ALLOCATABLE :: u(:),l(:),Qu(:) ! upper and lower bounds
	REAL*8 :: A, pa, randn, vtar, vcons
	REAL*8, ALLOCATABLE :: elemQual(:)
	REAL*8, ALLOCATABLE :: IP_k(:)
	REAL*8 :: d12, d13, d14, d23, d24, d34, dMax, dMin, dMean

	LOGICAL :: useStress
	INTEGER :: globLoop, numGlobLoops

	TYPE(IntQueueType), ALLOCATABLE :: buckets(:)
	INTEGER :: numBuckets, b, ib, nb_b
	REAL*8  :: bucketWidth, maxQual, minQual

	INTEGER :: springType  !1== inverse dist, 2=equal, 3=quality based
	LOGICAL :: blockNodes  !True if nodes remain part of boundary condition in stress solver
	INTEGER :: penalty !0== no penalty, 1== distance, 2== volume, 3== both
	INTEGER :: objFun !1== circumcentre, 2==equalatoral


	!===Suffling variables
   	INTEGER :: iRan, jRan, tRan, ibb
   	INTEGER, ALLOCATABLE :: order(:)

   	INTEGER :: useStressIn, blockNodesIn

   	TYPE(PointAssociationArray) :: PointAsso2

   	INTEGER :: Isucc

   	vcons = (1.0d0/12.0d0)*SQRT(2.0d0)

   	OPEN(84,file='coStress.ctl',status='old',BLANK='NULL')
   	READ(84,*) useStressIn
   	READ(84,*) blockNodesIn
   	READ(84,*) springType
   	READ(84,*) penalty
   	READ(84,*) objFun
   	CLOSE(84)


   	!Need to write a file to read these settings in
   	IF (useStressIn.EQ.1) THEN
		useStress = .TRUE.
	ELSE 
		useStress = .FALSE.
	END IF
	IF (blockNodesIn.EQ.1) THEN
		blockNodes = .TRUE.
	ELSE
		blockNodes = .FALSE.
	END IF


	numGlobLoops = 50
	

	DO globLoop = 1,numGlobLoops

		WRITE(*,*)'Optimisation loop ',globLoop,' of ',numGlobLoops
		CALL Set_Domain_Mapping()
		IP_BoundCond(:) = 0

		!Need to sort the elements 
		!WRITE(*,*)'Allocating NB_Tet',NB_Tet
		ALLOCATE(elemQual(NB_Tet))

		maxQual = 0.0d0
		minQual = huge(0.0d0)

		DO ie = 1,NB_Tet
			CALL Get_Tet_SphereCross(ie)
			ip1 = IP_Tet(1,ie)
			ip2 = IP_Tet(2,ie)
			ip3 = IP_Tet(3,ie)
			ip4 = IP_Tet(4,ie)

			dw = Scale_Tet(ie)*BGSpacing%BasicSize
			dwdw = dw*dw

			p1 = Posit(:,ip1)
			p2 = Posit(:,ip2)
			p3 = Posit(:,ip3)
			p4 = Posit(:,ip4)
			pC = (p1+p2+p3+p4) * 0.25d0

			elemQual(ie) = Geo3D_Distance_SQ(Sphere_Tet(:,ie),PC)/dwdw

			maxQual = MAX(maxQual,elemQual(ie))
			minQual = MIN(minQual,elemQual(ie))

		END DO


		numBuckets = NB_Tet/25 
		!WRITE(*,*)'Allocating numBuckets',numBuckets
		ALLOCATE(buckets(numBuckets))
		bucketWidth = (maxQual-minQual)/REAL(numBuckets)
		DO ie = 1,NB_Tet
			DO b = 1,numBuckets
				IF ((  elemQual(ie).LE. (REAL(b)*bucketWidth)).OR.&
				    	(b.EQ.numBuckets)) THEN
					CALL IntQueue_Push(buckets(numBuckets-b+1),ie)
				   	EXIT
				END IF
			END DO
		END DO

		!WRITE(*,*)'Allocating NB_Point',NB_Point
		ALLOCATE(IP_k(NB_Point))
		IF (springType.EQ.2) THEN
			IP_k(:) = 1.0d0
		ELSE IF (springType.EQ.3) THEN
			!Build node to element data structure
			CALL PointAssociation_Clear(PointAsso2)
			CALL PointAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso2)
			DO ip = 1,NB_Point
				IP_k(ip) = 0.0d0
				DO ic = 1,PointAsso2%PointList(ip)%numCells
					ie = PointAsso2%PointList(ip)%CellList(ic)
					!If the quality is zero we want the stiffness to be huge
					IP_k(ip) = IP_k(ip) + 1.0d0/(TinySize+elemQual(ie))
				END DO
				IP_k(ip) = IP_k(ip)/REAL(PointAsso2%PointList(ip)%numCells)
			END DO
		END IF

		DO b = 1,numBuckets
			!Loop round elements and optimise each

			nb_b = buckets(b)%numNodes
			!Need to randomise the order
			IF (nb_b.GT.0) THEN
				!WRITE(*,*)'Allocating nb_b',nb_b
				ALLOCATE(order(nb_b))
			    DO iRan = 1,nb_b
			      order(iRan) = iRan
			    ENDDO
			    DO iRan = nb_b,1,-1
			      CALL RANDOM_NUMBER(randn)
			      jRan = iRan*randn + 1
			      tRan = order(iRan)
			      order(iRan) = order(jRan)
			      order(jRan) = tRan
			     ENDDO
			END IF

			DO ibb = 1,nb_b

				ib = order(ibb)
				ie = buckets(b)%Nodes(ib)

				!Check to see if this element contains it's circumcentre
				CALL Get_Tet_SphereCross(ie)
				CALL isInside(ie,inside,wdum)

				IF (inside.EQ.0) THEN

					ip1 = IP_Tet(1,ie)
					ip2 = IP_Tet(2,ie)
					ip3 = IP_Tet(3,ie)
					ip4 = IP_Tet(4,ie)

					!Set up optimiser
					D = 12
					N = 36

					ALLOCATE(ptemp(D), Pt(D,N), u(D), l(D), Qu(N), psave(D))

					!The optimisation is going to happen in normalised space
					!need to store the conversion factor
					dw = Scale_Tet(ie)*BGSpacing%BasicSize
					dwdw = dw*dw
					dwdwdw = dw*dwdw

					ptemp(:) = 0.0d0
					Ibest = 0
					ftemp = 0.0d0
					II = 0
					Loop = 0
					Istatus = 0
					Iin = 0
					Qu(:) = huge(0.0d0)
					fbest = huge(0.0)
					l(:) = -1.0d0
					u(:) = 1.0d0
					A = 0.01d0
					pa = 0.7d0

					!Generate eggs
					DO i = 1,N
						DO j = 1,D
							CALL RANDOM_NUMBER(randn)
							Pt(j,i) = l(j) + randn*(u(j)-l(j))
						END DO
					END DO

					Pt(:,1) = 0.0d0 !Set the first one to zero

					!Save original position
					psave(1) = Posit(1,ip1)
					psave(2) = Posit(2,ip1)
					psave(3) = Posit(3,ip1)
					psave(4) = Posit(1,ip2)
					psave(5) = Posit(2,ip2)
					psave(6) = Posit(3,ip2)
					psave(7) = Posit(1,ip3)
					psave(8) = Posit(2,ip3)
					psave(9) = Posit(3,ip3)
					psave(10) = Posit(1,ip4)
					psave(11) = Posit(2,ip4)
					psave(12) = Posit(3,ip4)

					MCSFlag = 0

					DO WHILE (MCSFlag.EQ.0)

						CALL Optimising_Cuckoo_Driver(D,N,ptemp,Ibest,ftemp,Pt,II,Loop,&
							                          Istatus, Iin, Qu, fbest, u, l, idum, &
							                          A, pa)

						IF (Loop.GT.10) THEN
							!Get the best result and apply spring solver
							IP_Disp(:,:) = 0.0d0

							IF (.NOT.blockNodes) THEN
								IP_BoundCond(:) = 0
							END IF
							IP_BoundCond(ip1) = 1
							IP_BoundCond(ip2) = 1
							IP_BoundCond(ip3) = 1
							IP_BoundCond(ip4) = 1
							IP_BoundCond(1:NB_BD_Point) = 1



							IP_Disp(1,ip1) = dw*Pt(1,Ibest)
							IP_Disp(2,ip1) = dw*Pt(2,Ibest)
							IP_Disp(3,ip1) = dw*Pt(3,Ibest)
							IP_Disp(1,ip2) = dw*Pt(4,Ibest)
							IP_Disp(2,ip2) = dw*Pt(5,Ibest)
							IP_Disp(3,ip2) = dw*Pt(6,Ibest)
							IP_Disp(1,ip3) = dw*Pt(7,Ibest)
							IP_Disp(2,ip3) = dw*Pt(8,Ibest)
							IP_Disp(3,ip3) = dw*Pt(9,Ibest)
							IP_Disp(1,ip4) = dw*Pt(10,Ibest)
							IP_Disp(2,ip4) = dw*Pt(11,Ibest)
							IP_Disp(3,ip4) = dw*Pt(12,Ibest)

							!Lock boundary
							IP_Disp(:,1:NB_BD_Point) = 0.0d0

							IF (useStress) THEN
								
								CALL CoVolSpringSolver(IP_Disp,IP_BoundCond,IP_k,springType)
								
							END IF

							DO ip = NB_BD_Point+1,NB_Point
							 	Posit(:,ip) = Posit(:,ip) + IP_Disp(:,ip)
							END DO


							MCSFlag = 1

						ELSEIF (Iin.GT.0) THEN
							!Move and calculate fitness
							IF (ip1.GT.NB_BD_Point) THEN
								Posit(1,ip1) = psave(1)  + dw*ptemp(1)
								Posit(2,ip1) = psave(2)  + dw*ptemp(2)
								Posit(3,ip1) = psave(3)  + dw*ptemp(3)
							END IF
							IF (ip2.GT.NB_BD_Point) THEN
								Posit(1,ip2) = psave(4)  + dw*ptemp(4)
								Posit(2,ip2) = psave(5)  + dw*ptemp(5)
								Posit(3,ip2) = psave(6)  + dw*ptemp(6)
							END IF
							IF (ip3.GT.NB_BD_Point) THEN
								Posit(1,ip3) = psave(7)  + dw*ptemp(7)
								Posit(2,ip3) = psave(8)  + dw*ptemp(8)
								Posit(3,ip3) = psave(9)  + dw*ptemp(9)
							END IF
							IF (ip4.GT.NB_BD_Point) THEN
								Posit(1,ip4) = psave(10) + dw*ptemp(10)
								Posit(2,ip4) = psave(11) + dw*ptemp(11)
								Posit(3,ip4) = psave(12) + dw*ptemp(12)
							END IF


							

							p1 = Posit(:,ip1)
							p2 = Posit(:,ip2)
							p3 = Posit(:,ip3)
							p4 = Posit(:,ip4)

							ftemp = 0.0d0

							IF (objFun.EQ.1) THEN
								pC = (p1+p2+p3+p4) * 0.25d0		
								CALL Get_Tet_SphereCross(ie)
								ftemp = ftemp + Geo3D_Distance_SQ(Sphere_Tet(:,ie),pC)/dwdw

							END IF
							IF (objFun.EQ.2) THEN
							 	d12 = Geo3D_Distance_SQ(p1,p2)
							 	d13 = Geo3D_Distance_SQ(p1,p3)
							 	d14 = Geo3D_Distance_SQ(p1,p4)
							 	d23 = Geo3D_Distance_SQ(p2,p3)
							 	d24 = Geo3D_Distance_SQ(p2,p4)
							 	d34 = Geo3D_Distance_SQ(p3,p4)

							 	dMax = MAX(d12, d13, d14, d23, d24, d34)
							 	dMin = MAX(d12, d13, d14, d23, d24, d34)
							 	!dMean = (d12 + d13 + d14 + d23 + d24 + d34)/6.0d0

							 	ftemp = ftemp + ABS(dMax-dMin)/dwdw

							 END IF

							IF ((penalty.EQ.1).OR.(penalty.EQ.3)) THEN
								!Penalty based on distance moved
								ftemp = ftemp + (Geo3D_Distance_SQ(ptemp(1:3))+&
								 					Geo3D_Distance_SQ(ptemp(4:6))+&
								 					Geo3D_Distance_SQ(ptemp(7:9))+&
								 					Geo3D_Distance_SQ(ptemp(10:12)))/(16.0d0*dwdw)
							END IF
							IF ((penalty.EQ.2).OR.(penalty.EQ.3)) THEN
								!Penalty based on volume
								vtar = vcons*dwdwdw
								ftemp = ftemp + ((vtar-Geo3D_Tet_Volume_High(p1,p2,p3,p4))/vtar)
							END IF
						END IF

					END DO

					DEALLOCATE(ptemp, Pt, u, l, Qu, psave)
				END IF
			
			END DO

			IF (nb_b.GT.0) THEN

				DEALLOCATE(order)
			END IF

		END DO

		DO b = 1,numBuckets
			CALL IntQueue_Clear(buckets(b))
		END DO
		DEALLOCATE(elemQual,buckets,IP_k)

		!Reinsert
		CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
		CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
		CALL Next_Build()
		CircumUpdated = .TRUE.
		VolumeUpdated = .TRUE.
		DO ie = 1,NB_Tet
			CALL Get_Tet_SphereCross(ie)
			CALL Get_Tet_Volume(ie)			
		END DO
		CircumUpdated = .TRUE.
		VolumeUpdated = .TRUE.
		Mark_Point(1:NB_Point) = 0
		
		CALL Insert_Points_EasyPeasy(NB_BD_Point+1, NB_Point, 5)
		CALL Remove_Point()
		CALL Next_Build()

	END DO

END SUBROUTINE


SUBROUTINE CoVolSpringSolver(IP_Disp,IP_BoundCond,IP_k,springType)

	!This solves the spring system given initial displacement IP_Disp and 
	!flags for boundary conditions IP_BoundCond - spring constant is calculated internally
	!in this subroutine
	
	USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	USE HybridMeshStorage
	USE TetMeshStorage
	USE PointAssociation
	USE Queue
	IMPLICIT NONE

	REAL*8, INTENT(INOUT) :: IP_Disp(3,NB_Point)
	INTEGER, INTENT(IN) :: IP_BoundCond(NB_Point)
	REAL*8, INTENT(IN) :: IP_k(NB_Point)  !An optional spring constant per node
	INTEGER, INTENT(IN) :: springType

	REAL*8 :: IP_Disp_P1(3,NB_Point), ksum, k
	REAL*8 :: maxDif

	TYPE(PointAssociationArray) :: PointAsso2
	TYPE(IntQueueType) :: surrNodes(NB_Point)

	INTEGER :: ip, conv, i, ip2, numLoop


	!Build node to element data structure
	CALL PointAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso2)

	!Loop round nodes and build node to node structure
	DO ip = 1,NB_Point
		surrNodes(ip) = getSurroundNodes(ip)
	END DO

	conv = 0
	numLoop = 0

	DO WHILE (conv.EQ.0)

		numLoop = numLoop + 1

		!First reset next displacement
		IP_Disp_P1(:,:) = 0.0d0
		maxDif = 0.0d0

		!Loop round nodes
		DO ip = 1,NB_Point
			IF (IP_BoundCond(ip).EQ.0) THEN
				ksum = 0.0d0

				DO i = 1,surrNodes(ip)%numNodes
					!Calculate spring constant
					ip2 = surrNodes(ip)%nodes(i)
					IF (springType.GT.1) THEN
						k = 0.5d0*(IP_k(ip)+IP_k(ip2))				
					ELSE
						k = Geo3D_Distance_SQ(Posit(:,ip),Posit(:,ip2))
						k = 1.0d0/k
					END IF

					ksum = ksum + k
					IP_Disp_P1(:,ip) = IP_Disp_P1(:,ip) + k*IP_Disp(:,ip2)
				END DO

				IP_Disp_P1(:,ip) = IP_Disp_P1(:,ip)/ksum

				maxDif = MAX(maxDif,Geo3D_Distance(IP_Disp_P1(:,ip),IP_Disp(:,ip)))
			ELSE
				IP_Disp_P1(:,ip) = IP_Disp(:,ip)
			END IF
		END DO

		IP_Disp(:,:) = IP_Disp_P1(:,:)

		IF ((maxDif.LT.TinySize).OR.(numLoop.GT.1000)) THEN
			conv = 1
		END IF

	END DO

	DO ip = 1,NB_Point
		CALL IntQueue_Clear(surrNodes(ip))
	END DO

	CALL PointAssociation_Clear(PointAsso2)
	
	CONTAINS

	FUNCTION getSurroundNodes(ipS) RESULT(nodesS)
		IMPLICIT NONE

		INTEGER, INTENT(IN) :: ipS
		TYPE(IntQueueType) :: nodesS
		INTEGER :: icS, ieS, jS, kS

		CALL IntQueue_Clear(nodesS)
		DO icS = 1,PointAsso2%PointList(ipS)%numCells
			ieS = PointAsso2%PointList(ipS)%CellList(icS)
			DO jS = 1,4
				kS = IP_Tet(jS,ieS)
				IF (kS.NE.ipS) THEN
					IF (.NOT.IntQueue_Contain(nodesS,kS)) THEN
						CALL IntQueue_Push(nodesS,kS)
					END IF
				END IF
			END DO
		END DO

	END FUNCTION getSurroundNodes



END SUBROUTINE CoVolSpringSolver