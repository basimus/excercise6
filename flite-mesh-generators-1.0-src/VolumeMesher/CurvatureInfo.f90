!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!*******************************************************************************
!>
!!  curvature data and function
!<
!*******************************************************************************



!>
!!
SUBROUTINE GetRegionUVBox(RegionID, UBOT, VBOT, UTOP, VTOP)
  USE occt_fortran  !SPW
  USE common_Parameters
   
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: RegionID
  REAL*8,  INTENT(OUT) :: UBOT, VBOT, UTOP, VTOP
  IF(Curvature_Type==1)THEN
     UBOT = 0.d0
     VBOT = 0.d0
     UTOP = Curvt%Regions(RegionID)%numNodeU - 1
     VTOP = Curvt%Regions(RegionID)%numNodeV - 1
  ELSE IF(Curvature_Type==4) THEN
     CALL OCCT_GetSurfaceUVBox( RegionID, UBOT, VBOT, UTOP, VTOP )
  ELSE
	 CALL CADFIX_GetSurfaceUVBox( RegionID, UBOT, VBOT, UTOP, VTOP )
  ENDIF
END SUBROUTINE GetRegionUVBox

!>
!!
SUBROUTINE GetRegionUVmax(RegionID, IUTOP, IVTOP)
  USE occt_fortran  !SPW
  USE common_Parameters
   
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: RegionID
  INTEGER, INTENT(OUT) :: IUTOP, IVTOP
  REAL*8 :: UBOT, VBOT, UTOP, VTOP
  IF(Curvature_Type==1)THEN
     IUTOP = Curvt%Regions(RegionID)%numNodeU
     IVTOP = Curvt%Regions(RegionID)%numNodeV
  ELSE IF(Curvature_Type==4) THEN
     CALL OCCT_GetSurfaceUVBox( RegionID, UBOT, VBOT, UTOP, VTOP )
     IUTOP = UTOP - UBOT + 1.5  
     IVTOP = VTOP - VBOT + 1.5
  ELSE
	  CALL CADFIX_GetSurfaceUVBox( RegionID, UBOT, VBOT, UTOP, VTOP )
     IUTOP = UTOP - UBOT + 1.5
	  IVTOP = VTOP - VBOT + 1.5
  ENDIF
END SUBROUTINE GetRegionUVmax

!>
!!
SUBROUTINE GetRegionName(RegionID, entityName)
  USE common_Parameters
   
  USE occt_fortran  !SPW

  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: RegionID
  CHARACTER(LEN=*), INTENT(OUT)  :: entityName
  IF(Curvature_Type==1)THEN
     entityName = Curvt%Regions(RegionID)%theName
  ELSE IF(Curvature_Type==4) THEN 
     CALL OCCT_GetSurfaceName( RegionID, entityName )
  ELSE
     CALL CADFIX_GetSurfaceName( RegionID, entityName )
  ENDIF
  IF(LEN_TRIM(entityName)==0) entityName = INT_to_CHAR (RegionID, 4)
  WRITE(103,*) RegionID, entityName
END SUBROUTINE GetRegionName


!>
!!
SUBROUTINE GetRegionGeoType(RegionID, GeoType)
  USE common_Parameters
   
  USE occt_fortran  !SPW

  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: RegionID
  INTEGER, INTENT(OUT) :: GeoType
  IF(Curvature_Type==1)THEN
     GeoType = Curvt%Regions(RegionID)%GeoType
  ELSE IF(Curvature_Type==4) THEN 
     GeoType = 1
     
  ELSE
       GeoType = 1
       CALL CADFIX_GetSurfaceTopoType( RegionID, GeoType)
       IF(GeoType/=-1) GeoType = 1
  ENDIF
END SUBROUTINE GetRegionGeoType


!>
!!
SUBROUTINE GetRegionNumCurves(RegionID, nc)
  USE common_Parameters
  USE occt_fortran  !SPW
   
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: RegionID
  INTEGER, INTENT(OUT) :: nc
  IF(Curvature_Type==1)THEN
     nc = Curvt%Regions(RegionID)%numCurve
  ELSE IF(Curvature_Type==4) THEN
     CALL OCCT_GetSurfaceNumCurves(RegionID, nc)
  ELSE
     CALL CADFIX_GetSurfaceNumCurves(RegionID, nc)
  ENDIF
END SUBROUTINE GetRegionNumCurves

!>
!!
SUBROUTINE GetRegionCurveList(RegionID, IC)
  USE occt_fortran  !SPW
  USE common_Parameters
   
  USE Queue
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: RegionID
  TYPE(IntQueueType), INTENT(INOUT) :: IC
  INTEGER :: nc, List(10000)
  IF(Curvature_Type==1)THEN
     CALL IntQueue_Set(IC, Curvt%Regions(RegionID)%numCurve, Curvt%Regions(RegionID)%IC)
  ELSE IF(Curvature_Type == 4) THEN
     CALL OCCT_GetSurfaceCurves(RegionID, nc, List)
     IF(nc>10000) CALL Error_Stop (' GetRegionCurveList: nc>10000 ')
     CALL IntQueue_Set(IC, nc, List)
  ELSE
    CALL CADFIX_GetSurfaceCurves(RegionID, nc, List)
	IF(nc>10000) CALL Error_Stop (' GetRegionCurveList: nc>10000 ')
      CALL IntQueue_Set(IC, nc, List)
  ENDIF
END SUBROUTINE GetRegionCurveList


!>
!!
SUBROUTINE GetUVPointInfo0(RegionID, u, v, P)
  USE occt_fortran  !SPW
  USE common_Parameters
   
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: RegionID
  REAL*8,  INTENT(IN)  :: u, v
  REAL*8,  INTENT(OUT) :: P(3)
  REAL*8 :: uv(2), Pu(3), Pv(3), Puv(3),Puu(3),Pvv(3)
  IF(Curvature_Type==1)THEN
     CALL RegionType_Interpolate(Curvt%Regions(RegionID), 0, u, v, P)
  ELSE IF(Curvature_Type==4) THEN
    CALL OCCT_GetUVPointInfoAll(RegionID,u,v,P,Pu,Pv,Puv,Puu,Pvv)
  ELSE
    uv(:) = (/u,v/)
    CALL CADFIX_GetXYZFromUV1( RegionID, uv, P )
  ENDIF
END SUBROUTINE GetUVPointInfo0

!>
!!
SUBROUTINE GetUVPointInfo1(RegionID, u, v, P, Pu, Pv, Puv)
  USE occt_fortran  !SPW
  USE common_Parameters
   
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: RegionID
  REAL*8,  INTENT(IN)  :: u, v
  REAL*8,  INTENT(OUT) :: P(3), Pu(3), Pv(3), Puv(3)
  REAL*8 :: info(18),Puu(3),Pvv(3)
  IF(Curvature_Type==1)THEN
     CALL RegionType_Interpolate(Curvt%Regions(RegionID), 1, u, v, P, Pu, Pv, Puv)
  ELSE IF(Curvature_Type==4) THEN
     CALL OCCT_GetUVPointInfoAll(RegionID,u,v,P,Pu,Pv,Puv,Puu,Pvv)
    
  ELSE
	 CALL CADFIX_GetUVPointInfo( RegionID, u, v, info )
     P(1:3)   = info(1:3)
     Pu(1:3)  = info(4:6)
     Pv(1:3)  = info(7:9)
     Puv(1:3) = info(10:12)
  ENDIF
END SUBROUTINE GetUVPointInfo1


!>
!!
SUBROUTINE GetUVFromXYZ(RegionID, npl, xyz, uv, TOLG)
  USE occt_fortran  !SPW
  USE common_Parameters
   
  USE SurfaceCurvatureManager
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: RegionID, npl
  REAL*8,  INTENT(IN)  :: xyz(3,*), TOLG
  REAL*8,  INTENT(OUT) :: uv(2,*)
  REAL*8 :: drm_max
  IF(Curvature_Type==1)THEN
     CALL RegionType_GetUVFromXYZ(Curvt%Regions(RegionID), npl, xyz, uv, TOLG, drm_max)
     IF( drm_max > TOLG ) THEN 
        WRITE(29,'(/,a,    /)') ' LOCUV >        !!! *** WARNING *** !!!           '
        WRITE(29,'(  a,e12.5)') '        The maximum distance between the target is: ', drm_max
        WRITE(29,'(  a      )') '        This is BIGGER than the tolerance used by '
        WRITE(29,'(  a,e12.5)') '        SURFACE for coincident points which is:   ', TOLG
        WRITE(29,'(  a      )') '        The run continues but you are advised to  '
        WRITE(29,'(  a      )') '        check your input geometry data for errors '
     ENDIF
  ELSE IF(Curvature_Type==4) THEN
     CALL OCCT_GetUVFromXYZ( RegionID, npl, xyz, uv, TOLG )
  ELSE
     CALL CADFIX_GetUVFromXYZ( RegionID, npl, xyz, uv )
  ENDIF
END SUBROUTINE GetUVFromXYZ

!>
!!
SUBROUTINE ProjectToRegionFromXYZ(RegionID, xyz, uv, TOLG, onSurf, drm, Pt)
  USE occt_fortran  !SPW
  USE common_Parameters
   
  USE SurfaceCurvatureManager
  IMPLICIT NONE
  INTEGER, INTENT(IN)    :: RegionID
  REAL*8,  INTENT(INOUT) :: uv(2)
  REAL*8,  INTENT(IN)    :: TOLG,xyz(3)
  INTEGER, INTENT(in)    :: onSurf
  REAL*8,  INTENT(OUT)   :: drm  
  REAL*8, INTENT(OUT) :: Pt(3)
  REAL*8 :: uv1(2,1)
  INTEGER :: mesh_level
  mesh_level = 1
  IF(Curvature_Type==1)THEN
     CALL RegionType_fguess( Curvt%Regions(RegionID), xyz, uv ,drm, mesh_level)
     CALL RegionType_GetUVFromXYZ1(Curvt%Regions(RegionID), xyz, uv, TOLG, onSurf, drm, Pt)
  ELSE IF(Curvature_Type==4) THEN
     CALL OCCT_GetUVFromXYZ(  RegionID, 1, xyz, uv1, TOLG )
     uv(1) = uv1(1,1)
     uv(2) = uv1(2,1)
     CALL GetUVPointInfo0( RegionID, uv(1),uv(2), Pt )
     drm = Geo3D_Distance(xyz, Pt)
  ELSE
     CALL CADFIX_GetUVFromXYZ(  RegionID, 1, xyz, uv )
     CALL CADFIX_GetXYZFromUV1( RegionID, uv, Pt )
	 drm = Geo3D_Distance(xyz, Pt)
  ENDIF
  !xyz(:) = Pt(:)
END SUBROUTINE ProjectToRegionFromXYZ

