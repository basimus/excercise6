!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!**************************************************************************
!>
!!  Face_Insert 
!!  This subroutine inserts points by moving from each edge in the direction of the face normal
!!  A kdtree is used to search for spatial searching to merge close points
!!  Initial implimentation writen by S. Walton 18/10/2013
!!
!<
!**************************************************************************
SUBROUTINE Face_Insert()
      USE common_Constants
      USE common_Parameters
      USE array_allocator
      USE SpacingStorage
      USE Geometry3DAll
	  USE m_mrgrnk

      IMPLICIT NONE
        
      INTEGER :: done,allFail,helpFlag,itry,Isucc,subdone
      INTEGER :: pStart,pEnd,numIns
      INTEGER :: ip,ic,numAlloc,iRes,itemp
      REAL*8 :: unitCell(3,14),fMap(3,3),pa(3),pb(3),pc(3),vunorm(3),vnorm(3),face(3,12),faced(12)
      REAL*8 :: geoD,sqrt34,oneOverSqrt34,p1(3),p2(3),pmid(3),oneOverSqrt2,p3(3),p4(3)
      REAL*8                         :: b,h,b1,h1,alpha
      REAL*8 :: d,delta,dtet,dmax,dmin,dtemp,maxS,dmid,da,db,dc,d2,ptet(3,4),dum(4)
      REAL*8 :: pedge(3,6)
      INTEGER :: ie,  numLoops,IT,i,j, numInsSave,iFace,ia,ib,icc,icT,ii
      INTEGER :: idxin,correltime,nn,nfound,NB_Tet_Old,NumIP,NumResAloc,idxinI
      character*(4) :: loopString 
     
      type(IntQueueType),allocatable :: child(:)
      INTEGER,allocatable :: marker(:)
      REAL*8  :: RAsq,RAstand,RAfac,RAmaxloc,RAminloc,RAvol,RAsph,tolLoc
	  REAL*8  :: circum(3),circRad,sqrt1o2,tol,sqrt2o3
      INTEGER :: distFlag,KK
	  INTEGER, allocatable :: Isort(:)
	  REAL*8, allocatable :: TempR(:)
	  
	  REAL*8 :: nott,needed,gamma,oneOverGammam1,spawn(3)
	  INTEGER :: numLay,insFlag, numBadDirect,numFacesConsidered,numChoice
	  
	  INTEGER :: surfMark(Surf%NB_Tri)
	  INTEGER, allocatable ::  edgeList(:)
	  INTEGER :: it1, it2,numEdgeUnique,ip1,ip2,it3,iedge
	  
	  INTEGER, allocatable :: adjacency(:) !This will be the graph of the triangles
	                                         ! -1 is a not matched edge, 0 is no edge, and 1 is matched
	  
	  
	  INTEGER, allocatable :: label(:,:) ! Column 1 gives the even (1) odd (-1) unmet (0)
	                                  ! Column 2 gives matched (1) or unmatched (0)
									  ! Column 3-5 gives explored (1) or not explored(0) for triangle edges
	  INTEGER, allocatable :: path(:)  !The current path and it's length
	  type(IntQueueType) :: unmatchedQ !The queue of unmatched nodes 
	  type(IntQueueType) :: choiceList !List of nodes (in terms of path length) where there was a choice
	  INTEGER :: done2, q1, done3,oldPathLength, pathLength, numDirectElem
	  
	  !Can I keep track of elements and avoid placing points which create direct elements which 
	  !intersect?
	  INTEGER,allocatable :: directElem(:,:)
	  LOGICAL :: good

	  INTEGER :: ib1, ib2, lab1, lab2, itb
	  
	  
	  numDirectElem = 0
	  
	  !First build the list of edges which have unique triangles
	  CALL Surf_BuildNext(Surf)
	  ALLOCATE(edgeList(Surf%NB_Edge))
	  ALLOCATE(directElem(4,2*Surf%NB_Tri))
	  ALLOCATE(adjacency(Surf%NB_Edge))
	  ALLOCATE(label(5,Surf%NB_Tri))
	  ALLOCATE(path(Surf%NB_Tri))
	 
	  adjacency(1:Surf%NB_Edge) = 0
	 
	  label(1:5,1:Surf%NB_Tri) = 0
	  
	  surfMark(1:Surf%NB_Tri) = 0
	  edgeList(1:Surf%NB_Edge) = -1
	  numEdgeUnique = 0
	  DO ie = 1,Surf%NB_Edge
	    !The triangles connected to this edge
		it1 = Surf%ITR_Edge(1,ie)
		it2 = Surf%ITR_Edge(2,ie)

		!Find the region number of these triangles
		ib1 = Surf%IP_Tri(5,it1)
		ib2 = Surf%IP_Tri(5,it2)

		!Then the label
		lab1 = Type_Wall(ib1)
		lab2 = Type_Wall(ib2)
		
		IF (  (lab1.NE.2).AND. (lab1.NE.3) .AND. &
		      (lab2.NE.2).AND. (lab2.NE.3)  ) THEN

			!Now we need to check the dihedral angle 
			p1(:) = Posit(:,Surf%IP_Tri(1,it1))
			p2(:) = Posit(:,Surf%IP_Tri(2,it1))
			p3(:) = Posit(:,Surf%IP_Tri(3,it1))
			ic = ABS(which_EdgeinTri(Surf%IP_Edge(:,ie), Surf%IP_Tri(:,it2)))
			p4(:) = Posit(:,Surf%IP_Tri(ic,it2))
			alpha = Geo3D_Dihedral_Angle(p1,p2,p3,p4,Isucc)
			
			IF (alpha.GT.((-2.0d0/3.0d0)*PI)) THEN
			
			  !What would the quality be if we used this edge
			  numBadDirect = 0
			  icT = 0
			  ip1 = Surf%IP_Edge(1,ie)
			  ip2 = Surf%IP_Edge(2,ie)
			
			  !First calculate the face normal
			  ia = Surf%IP_Tri(1,it1)
			  ib = Surf%IP_Tri(2,it1)
			  icc = Surf%IP_Tri(3,it1)
			  pa(:) = Posit(:,ia)
			  pb(:) = Posit(:,ib)
			  pc(:) = Posit(:,icc)
			  
			  circum = Geo3D_Sphere_Centre(pa,pb,pc)
			  delta = Geo3D_Distance(circum,pa)
			  spawn(:) = 0.0d0
			  spawn(:) = 2.0d0*circum(:) - ((pa(:)+pb(:)+pc(:))/3.0d0)
		 
			  vnorm(:) = Geo3D_Cross_Product(pa,pb,pc)
			  vunorm(1:3) = 0.0d0
			  vunorm(:) = vnorm(:) / Geo3D_Distance(vnorm)
				
			  ia = Surf%IP_Tri(1,it2)
			  ib = Surf%IP_Tri(2,it2)
			  icc = Surf%IP_Tri(3,it2)
			  pa(:) = Posit(:,ia)
			  pb(:) = Posit(:,ib)
			  pc(:) = Posit(:,icc)
				
			  circum = Geo3D_Sphere_Centre(pa,pb,pc)
			  delta = 1.1d0*MAX(delta,Geo3D_Distance(circum,pa))
			  spawn(:) = 0.5d0*(spawn(:)+(2.0d0*circum(:) - ((pa(:)+pb(:)+pc(:))/3.0d0)))
					 
			  vnorm(:) = Geo3D_Cross_Product(pa,pb,pc)
				
			  vunorm(:) = 0.5d0*(vunorm(:) + (vnorm(:) / Geo3D_Distance(vnorm)))
				
				
				
			  vnorm(:) = delta*vunorm(:)
			  icT = icT+1
				
			  face(:,icT) = spawn(:) - vnorm(:)
			  
			  !Now check to see what would happen if we connected this directly
			
			  ia = Surf%IP_Tri(1,it1)
		      ib = Surf%IP_Tri(2,it1)
			  icc = Surf%IP_Tri(3,it1)
			  pa(:) = Posit(:,ia)
			  pb(:) = Posit(:,ib)
			  pc(:) = Posit(:,icc)		
			  p4(:) = face(:,icT)
			
			  circum(:) = Geo3D_Sphere_Centre(pa,pb,pc,p4)
			
			  ptet(:,1) = pa
			  ptet(:,2) = pb
			  ptet(:,3) = pc
			  ptet(:,4) = p4
			  KK=0
			  dum =  Geo3D_Tet_Weight(KK,ptet,circum)
			  IF (KK==1) THEN
			  ELSE
			    numBadDirect = numBadDirect + 1
			  END IF
			 
			  ia = Surf%IP_Tri(1,it2)
			  ib = Surf%IP_Tri(2,it2)
			  icc = Surf%IP_Tri(3,it2)
			  pa(:) = Posit(:,ia)
			  pb(:) = Posit(:,ib)
			  pc(:) = Posit(:,icc)		
			  p4(:) = face(:,icT)
			
			  circum(:) = Geo3D_Sphere_Centre(pa,pb,pc,p4)
			
			  ptet(:,1) = pa
			  ptet(:,2) = pb
			  ptet(:,3) = pc
			  ptet(:,4) = p4
			  KK=0
			  dum =  Geo3D_Tet_Weight(KK,ptet,circum)
			  IF (KK==1) THEN
			  ELSE
			    numBadDirect = numBadDirect + 1
			  END IF
			 
			  
			  IF (numBadDirect.EQ.0) THEN
			  
			    
			    CALL Find_Edge_Number(it1,it2,iedge)
			    adjacency(iedge) = -1
				
				
				
		        IF ((surfMark(it1)+surfMark(it2)).EQ.0) THEN
			
			
			
			 
				    numEdgeUnique = numEdgeUnique + 1
				    CALL Find_Edge_Number(it1,it2,iedge)
			        adjacency(iedge) = 1
				  
				    surfMark(it1) = 1
				    surfMark(it2) = 1
				    label(2,it1) = 1
				    label(2,it2) = 1
			 
			  
			    END IF
			  END IF
			END IF
		END IF
	  END DO
	  PRINT *, 'Number of unique edges before blossom ',numEdgeUnique
	  
	  !The above greedy algorithm gives us a maximal matching but not the maximum matching
	  
	  done = 0
	  
	  DO WHILE (done.EQ.0)
	  
	   !First build the queue of unmatched nodes
	   !PRINT *, 'Starting new global search'
	   CALL IntQueue_Clear(unmatchedQ)
	   DO ie = 1,Surf%NB_Tri     
         IF (label(2,ie).EQ.0) THEN		 
		   CALL IntQueue_Push(unmatchedQ, ie)
	     END IF
	   END DO
	   
	   !Now we loop through the queue trying to find augmented paths
	   IF (unmatchedQ%numNodes.GT.0) THEN
	     done2 = 0
	   ELSE
	     done2 = 1
		 done = 1
	   END IF
	   DO WHILE (done2.EQ.0)
	     !Get last in queue
		 q1 = unmatchedQ%Nodes(unmatchedQ%numNodes)
		 !PRINT *, 'Trying new path ',q1
	     label(1,q1) = 1  !First node is even
		 pathLength = 1
		 path(pathLength) = q1   
		 CALL IntQueue_Clear(choiceList)
		 
	     done3 = 0
		 DO WHILE (done3.EQ.0)
	       !Try to extend the path
		   q1 = path(pathLength)
		   
		   !Mark the points in the path where there was a possible choice
		   !then when a dead end happens set pathlength to that choice, if choice = 1 then end
		   !the choice will always be out of 3, it1, it2, or it3 record this in another variable 
		   !i.e. which one to try next... need to uncolour on the way back though  
		   !i.e. label(choice+1:pathlength) = 0; pathlength = choice
		   !It will need to be a list of 'choices' going back along the tree
		   
		   IF (label(1,q1).EQ.1) THEN !even
		     !1) See if there is a possibility to end
		     DO i = 1,3
			   ie = Surf%Next_Tri(i,q1)
			   CALL Find_Edge_Number(ie,q1,iedge)
		           IF (iedge.GT.0) THEN 
			   IF ((ie.GT.0).AND.(adjacency(iedge).NE.0)) THEN
			     
			     IF ((label(2,ie).EQ.0).AND.(label(1,ie).EQ.0)) THEN
			      !Augmented path found
		    	!	PRINT *, 'Augmented path found ', path(1),'-',ie
			   	 !Add it and augment
				  pathLength = pathLength + 1
				  path(pathLength) = ie
				
				  DO ip1 = 1,pathLength-1
				
				    it1 = path(ip1)
				    it2 = path(ip1+1)
				    CALL Find_Edge_Number(it1,it2,iedge)
		            adjacency(iedge) = -1*adjacency(iedge)
				    
				  
				    label(2,it1) = 1
			        label(2,it2) = 1
				
				  END DO
				
				  label(1,1:Surf%NB_Tri) = 0
				  label(3:5,1:Surf%NB_Tri) = 0
			  
			      done2 = 1
				  done3 = 1
				  EXIT
			     END IF
			   END IF
                           END IF
		     END DO
			 
		   END IF
		   
		   
	       DO i = 1,3
		    
		    IF (label(i+2,q1).EQ.0) THEN
		      !Mark this route as explored
			  label(i+2,q1)=1
			  ie = Surf%Next_Tri(i,q1)
			  CALL Find_Edge_Number(ie,q1,iedge)
                          IF (iedge.GT.0) THEN
			  IF ((ie.GT.0).AND.(adjacency(iedge).NE.0)) THEN
				  IF (label(1,ie).EQ.0) THEN
					!This means ie is not in the path
					IF (adjacency(iedge).EQ.(-1*label(1,q1))) THEN
					  
					  !We can go down this path
					  
					  IF (i.LT.3) THEN
						!Record the choice
						CALL IntQueue_Push(choiceList, pathLength)
						
					  END IF
					  
					  pathLength = pathLength + 1
					  path(pathLength) = ie
					  label(1,ie) = -1*label(1,q1)
					  
					  
					  EXIT
					  
					END IF
				  END IF
			  END IF
                          END IF
			
			END IF
		   END DO
		   
		   !Now we need to check if we failed to advance
		   IF (path(pathLength).EQ.q1) THEN
		     !We failed so go back
			 IF (choiceList%numNodes.GT.0) THEN
			   
			   oldPathLength = choiceList%Nodes(choiceList%numNodes)
			   CALL IntQueue_RemoveLast(choiceList)
			 
			  ! PRINT *, 'Retrace to ',path(oldPathLength)
			   DO i = oldPathLength+1,pathLength
			    label(1,path(i)) = 0
			    !label(3:5,path(i)) = 0
			   END DO
			   pathLength = oldPathLength
			 ELSE
			   !Failed to find path
			   !PRINT *, 'Failed to find path'
			   done3 = 1
			 END IF
		   END IF
		   
		   
		   
		   
		   
	     END DO
	     CALL IntQueue_RemoveLast(unmatchedQ)
		 IF (unmatchedQ%numNodes.EQ.0) THEN
		   !Couldn't find any agmented paths
		   done = 1
		   done2 = 1
		 END IF
	   END DO
	  
	  END DO
	  
	  surfMark(1:Surf%NB_Tri) = 0
	  
	  !Now we have a maximum matching to build
	  numEdgeUnique = 0
	  DO ie = 1,Surf%NB_Edge
	    !The triangles connected to this edge
		it1 = Surf%ITR_Edge(1,ie)
		it2 = Surf%ITR_Edge(2,ie)
		CALL Find_Edge_Number(it1,it2,iedge)
             IF (iedge.GT.0) THEN
		IF (adjacency(iedge).EQ.1) THEN
		   surfMark(it1) = 1
		   surfMark(it2) = 1
		   numEdgeUnique = numEdgeUnique + 1
		   edgeList(numEdgeUnique) = ie
		   	
		END IF
             END IF
	  END DO
	  PRINT *, 'Number of unique edges after blossom ',numEdgeUnique
	  
	  
	  
	  
	  
	  
      pStart = NB_Point+1
      
	  CALL Remove_Tet()
	  
     
      WRITE(*,*)'Inserting Points using face Insertion'
      WRITE(29,*)'Inserting Points using face Insertion'
      
     
	numBadDirect = 0
    numFacesConsidered = 0
		  
	DO ip = 1,numEdgeUnique
		  
		
		icT = 0
		 
		!Lets find the node and face numbers for this edge
		it1 = Surf%ITR_Edge(1,edgeList(ip))
		it2 = Surf%ITR_Edge(2,edgeList(ip))
		ip1 = Surf%IP_Edge(1,edgeList(ip))
		ip2 = Surf%IP_Edge(2,edgeList(ip))
		
				
		!First calculate the face normal
		ia = Surf%IP_Tri(1,it1)
		ib = Surf%IP_Tri(2,it1)
		icc = Surf%IP_Tri(3,it1)

		

		pa(:) = Posit(:,ia)
		pb(:) = Posit(:,ib)
		pc(:) = Posit(:,icc)
		
		pedge(:,1) = Posit(:,ia)
		pedge(:,2) = Posit(:,ib)
		pedge(:,3) = Posit(:,icc)
		
		circum = Geo3D_Sphere_Centre(pa,pb,pc)
		delta = Geo3D_Distance(circum,pa)
		spawn(:) = 0.0d0
		spawn(:) = 2.0d0*circum(:) - ((pa(:)+pb(:)+pc(:))/3.0d0)
 
		vnorm(:) = Geo3D_Cross_Product(pa,pb,pc)
		vunorm(1:3) = 0.0d0
		vunorm(:) = vnorm(:) / Geo3D_Distance(vnorm)
		
		ia = Surf%IP_Tri(1,it2)
		ib = Surf%IP_Tri(2,it2)
		icc = Surf%IP_Tri(3,it2)
		pa(:) = Posit(:,ia)
		pb(:) = Posit(:,ib)
		pc(:) = Posit(:,icc)
		pedge(:,4) = Posit(:,ia)
		pedge(:,5) = Posit(:,ib)
		pedge(:,6) = Posit(:,icc)
		
		circum = Geo3D_Sphere_Centre(pa,pb,pc)
		delta = 1.1d0*MAX(delta,Geo3D_Distance(circum,pa))
		spawn(:) = 0.5d0*(spawn(:)+(2.0d0*circum(:) - ((pa(:)+pb(:)+pc(:))/3.0d0)))
			 
		vnorm(:) = Geo3D_Cross_Product(pa,pb,pc)
		
		vunorm(:) = 0.5d0*(vunorm(:) + (vnorm(:) / Geo3D_Distance(vnorm)))
		
		
		
		vnorm(:) = delta*vunorm(:)
		icT = icT+1
		
		face(:,icT) = spawn(:) - vnorm(:)
		
		!Need to check for intersection with other elements
		good = .TRUE.
	
		NB_Point = NB_Point + 1
		IF(NB_Point>nallc_point) THEN     
			CALL ReAllocate_Point()
		END IF
		Posit(:,NB_Point) = face(:,icT)
		
		CALL Get_Point_Mapping(NB_Point)
		
		!Now check to see what would happen if we connected this directly
		
		ia = Surf%IP_Tri(1,it1)
		ib = Surf%IP_Tri(2,it1)
		icc = Surf%IP_Tri(3,it1)
		pa(:) = Posit(:,ia)
		pb(:) = Posit(:,ib)
		pc(:) = Posit(:,icc)		
		p4(:) = Posit(:,NB_Point)
		numDirectElem = numDirectElem + 1
		directElem(1,numDirectElem) = ia
		directElem(2,numDirectElem) = ib
		directElem(3,numDirectElem) = icc
		directElem(4,numDirectElem) = NB_Point
		circum(:) = Geo3D_Sphere_Centre(pa,pb,pc,p4)
		
		ptet(:,1) = pa
		ptet(:,2) = pb
		ptet(:,3) = pc
		ptet(:,4) = p4
		KK=0
		dum =  Geo3D_Tet_Weight(KK,ptet,circum)
		IF (KK==1) THEN
		  IF (.NOT.good) THEN
			numBadDirect = numBadDirect + 1
		  END IF
		ELSE
		  numBadDirect = numBadDirect + 1
		END IF
		numFacesConsidered = numFacesConsidered + 1
		
		ia = Surf%IP_Tri(1,it2)
		ib = Surf%IP_Tri(2,it2)
		icc = Surf%IP_Tri(3,it2)
		pa(:) = Posit(:,ia)
		pb(:) = Posit(:,ib)
		pc(:) = Posit(:,icc)		
		p4(:) = Posit(:,NB_Point)
		
		numDirectElem = numDirectElem + 1
		directElem(1,numDirectElem) = ia
		directElem(2,numDirectElem) = ib
		directElem(3,numDirectElem) = icc
		directElem(4,numDirectElem) = NB_Point
		
		circum(:) = Geo3D_Sphere_Centre(pa,pb,pc,p4)
		
		ptet(:,1) = pa
		ptet(:,2) = pb
		ptet(:,3) = pc
		ptet(:,4) = p4
		KK=0
		dum =  Geo3D_Tet_Weight(KK,ptet,circum)
		IF (KK==1) THEN
		  IF (.NOT.good) THEN
			numBadDirect = numBadDirect + 1
		  END IF
		ELSE
		  numBadDirect = numBadDirect + 1
		END IF
		numFacesConsidered = numFacesConsidered + 1


		IF (IP_BD_Tri(5,it1) > Surf%NB_Surf - Surf%NB_Sheet) THEN
			!This is a sheet so we need to advance in the other direction as well
			icT = 0
		 
			!Lets find the node and face numbers for this edge
			it1 = Surf%ITR_Edge(1,edgeList(ip))
			it2 = Surf%ITR_Edge(2,edgeList(ip))
			ip1 = Surf%IP_Edge(1,edgeList(ip))
			ip2 = Surf%IP_Edge(2,edgeList(ip))
			
					
			!First calculate the face normal
			ia = Surf%IP_Tri(3,it1)
			ib = Surf%IP_Tri(2,it1)
			icc = Surf%IP_Tri(1,it1)

			pa(:) = Posit(:,ia)
			pb(:) = Posit(:,ib)
			pc(:) = Posit(:,icc)
			
			pedge(:,1) = Posit(:,ia)
			pedge(:,2) = Posit(:,ib)
			pedge(:,3) = Posit(:,icc)
			
			circum = Geo3D_Sphere_Centre(pa,pb,pc)
			delta = Geo3D_Distance(circum,pa)
			spawn(:) = 0.0d0
			spawn(:) = 2.0d0*circum(:) - ((pa(:)+pb(:)+pc(:))/3.0d0)
	 
			vnorm(:) = Geo3D_Cross_Product(pa,pb,pc)
			vunorm(1:3) = 0.0d0
			vunorm(:) = vnorm(:) / Geo3D_Distance(vnorm)
			
			ia = Surf%IP_Tri(3,it2)
			ib = Surf%IP_Tri(2,it2)
			icc = Surf%IP_Tri(1,it2)
			pa(:) = Posit(:,ia)
			pb(:) = Posit(:,ib)
			pc(:) = Posit(:,icc)
			pedge(:,4) = Posit(:,ia)
			pedge(:,5) = Posit(:,ib)
			pedge(:,6) = Posit(:,icc)
			
			circum = Geo3D_Sphere_Centre(pa,pb,pc)
			delta = 1.1d0*MAX(delta,Geo3D_Distance(circum,pa))
			spawn(:) = 0.5d0*(spawn(:)+(2.0d0*circum(:) - ((pa(:)+pb(:)+pc(:))/3.0d0)))
				 
			vnorm(:) = Geo3D_Cross_Product(pa,pb,pc)
			
			vunorm(:) = 0.5d0*(vunorm(:) + (vnorm(:) / Geo3D_Distance(vnorm)))
			
			
			
			vnorm(:) = delta*vunorm(:)
			icT = icT+1
			
			face(:,icT) = spawn(:) - vnorm(:)
			
			!Need to check for intersection with other elements
			good = .TRUE.
		
			NB_Point = NB_Point + 1
			IF(NB_Point>nallc_point) THEN     
				CALL ReAllocate_Point()
			END IF
			Posit(:,NB_Point) = face(:,icT)
			
			CALL Get_Point_Mapping(NB_Point)
			
			!Now check to see what would happen if we connected this directly
			
			ia = Surf%IP_Tri(1,it1)
			ib = Surf%IP_Tri(2,it1)
			icc = Surf%IP_Tri(3,it1)
			pa(:) = Posit(:,ia)
			pb(:) = Posit(:,ib)
			pc(:) = Posit(:,icc)		
			p4(:) = Posit(:,NB_Point)
			numDirectElem = numDirectElem + 1
			directElem(1,numDirectElem) = ia
			directElem(2,numDirectElem) = ib
			directElem(3,numDirectElem) = icc
			directElem(4,numDirectElem) = NB_Point
			circum(:) = Geo3D_Sphere_Centre(pa,pb,pc,p4)
			
			ptet(:,1) = pa
			ptet(:,2) = pb
			ptet(:,3) = pc
			ptet(:,4) = p4
			KK=0
			dum =  Geo3D_Tet_Weight(KK,ptet,circum)
			IF (KK==1) THEN
			  IF (.NOT.good) THEN
				numBadDirect = numBadDirect + 1
			  END IF
			ELSE
			  numBadDirect = numBadDirect + 1
			END IF
			numFacesConsidered = numFacesConsidered + 1
			
			ia = Surf%IP_Tri(1,it2)
			ib = Surf%IP_Tri(2,it2)
			icc = Surf%IP_Tri(3,it2)
			pa(:) = Posit(:,ia)
			pb(:) = Posit(:,ib)
			pc(:) = Posit(:,icc)		
			p4(:) = Posit(:,NB_Point)
			
			numDirectElem = numDirectElem + 1
			directElem(1,numDirectElem) = ia
			directElem(2,numDirectElem) = ib
			directElem(3,numDirectElem) = icc
			directElem(4,numDirectElem) = NB_Point
			
			circum(:) = Geo3D_Sphere_Centre(pa,pb,pc,p4)
			
			ptet(:,1) = pa
			ptet(:,2) = pb
			ptet(:,3) = pc
			ptet(:,4) = p4
			KK=0
			dum =  Geo3D_Tet_Weight(KK,ptet,circum)
			IF (KK==1) THEN
			  IF (.NOT.good) THEN
				numBadDirect = numBadDirect + 1
			  END IF
			ELSE
			  numBadDirect = numBadDirect + 1
			END IF
			numFacesConsidered = numFacesConsidered + 1



		END IF

		   
	END DO	  
	
	WRITE(*,*)'Number faces considered not lonely= ',numFacesConsidered
    WRITE(29,*)'Number faces considered not lonely= ',numFacesConsidered
	
	IF (1.EQ.1) THEN !This needs rewriting
		!Now to put points above lonely faces
		DO ip = 1,Surf%NB_Tri
		  !Check to see if this triangle has been considered before
		  ib1 = Surf%IP_Tri(5,ip)
		  lab1 = Type_Wall(ib1)
		  IF ((surfMark(ip).EQ.0).AND. (lab1.NE.2).AND. (lab1.NE.3)) THEN
			it1 = ip
			!First calculate the face normal
			ia = Surf%IP_Tri(1,it1)
			ib = Surf%IP_Tri(2,it1)
			icc = Surf%IP_Tri(3,it1)
			pa(:) = Posit(:,ia)
			pb(:) = Posit(:,ib)
			pc(:) = Posit(:,icc)
			pedge(:,1) = Posit(:,ia)
			pedge(:,2) = Posit(:,ib)
			pedge(:,3) = Posit(:,icc)
		
			circum = Geo3D_Sphere_Centre(pa,pb,pc)
			delta = 1.1d0*Geo3D_Distance(circum,pa)
			
			spawn(:) = 2.0d0*circum(:) - ((pa(:)+pb(:)+pc(:))/3.0d0)
			
			vnorm(:) = Geo3D_Cross_Product(pa,pb,pc)
			vunorm(:) = vnorm(:) / Geo3D_Distance(vnorm)
		
			vnorm(:) = delta*vunorm(:)
			
			icT = 1
			face(:,icT) = spawn(:) - vnorm(:)
			
			!Need to check for intersection with other elements
			good = .TRUE.
			!Loop_361 : DO ie = 1,numDirectElem
			!	DO iFace = 1,4
			!	  DO ii = 1,3
			!		CALL inters( Posit(:,directElem(iTri_Tet(1,iFace),ie)), &
			!					 Posit(:,directElem(iTri_Tet(2,iFace),ie)), &
			!					 Posit(:,directElem(iTri_Tet(3,iFace),ie)), &
			!					 face(:,icT),&
			!					 pedge(:,ii), &
			!					 good)
			!		IF(.NOT.good) EXIT Loop_361
			!	  END DO
			!	  
			!	END DO
			!
			!END DO Loop_361
			
			!IF (good) THEN
				NB_Point = NB_Point + 1
				IF(NB_Point>nallc_point) THEN     
					CALL ReAllocate_Point()
				END IF
				Posit(:,NB_Point) = face(:,icT)
				
				CALL Get_Point_Mapping(NB_Point)
				
				!Now check to see what would happen if we connected this directly
				numDirectElem = numDirectElem + 1
				directElem(1,numDirectElem) = ia
				directElem(2,numDirectElem) = ib
				directElem(3,numDirectElem) = icc
				directElem(4,numDirectElem) = NB_Point
					
				p4(:) = Posit(:,NB_Point)
				
				circum(:) = Geo3D_Sphere_Centre(pa,pb,pc,p4)
				
				ptet(:,1) = pa
				ptet(:,2) = pb
				ptet(:,3) = pc
				ptet(:,4) = p4
				KK=0
				dum =  Geo3D_Tet_Weight(KK,ptet,circum)
				IF (KK==1) THEN
				  IF (.NOT.good) THEN
				    numBadDirect = numBadDirect + 1
				  END IF
				ELSE
				  numBadDirect = numBadDirect + 1
				END IF
				numFacesConsidered = numFacesConsidered + 1
			!END IF
		  END IF
		END DO
	END IF
	
	WRITE(*,*)'Number faces= ',Surf%NB_Tri
    WRITE(29,*)'Number faces= ',Surf%NB_Tri
	WRITE(*,*)'Number faces considered= ',numFacesConsidered
    WRITE(29,*)'Number faces considered= ',numFacesConsidered
	WRITE(*,*)'Number of bad elements connected to faces using direct= ',numBadDirect
    WRITE(29,*)'Number of bad elements connected to faces using direct= ',numBadDirect
	
		
	Mark_Point(1:NB_Point) = 0
	CALL Insert_Points_EasyPeasy(pStart, NB_Point, 10)
	usePower = .FALSE.
	CALL Remove_Point()
	
			
    DEALLOCATE(adjacency,label,path,edgeList,directElem)
END SUBROUTINE Face_Insert


SUBROUTINE Find_Edge_Number(it1,it2,ie)
    USE common_Parameters
	IMPLICIT NONE

	INTEGER, INTENT(IN) :: it1, it2
	INTEGER, INTENT(OUT) :: ie
	
	INTEGER :: i,j,IEdge
	
	ie = -1
	
	DO i = 1,3
	  IEdge = Surf%IED_Tri(i,it1)
	  DO j=1,2
	   IF (Surf%ITR_Edge(j,IEdge).EQ.it2) THEN
        ie = IEdge
        RETURN
       END IF		
	  END DO
	END DO



END SUBROUTINE Find_Edge_Number







