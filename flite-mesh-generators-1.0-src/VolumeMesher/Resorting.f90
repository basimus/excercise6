!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>       
!!     Remove those tetrahedra with negative marker::IP_Tet (4,IT)<0
!!        and renumber the rest.                                                \n       
!!     Reset :   common_Parameters::Mark_Tet,  common_Parameters::IP_Tet,
!!               common_Parameters::NB_Tet
!<       
!*******************************************************************************
SUBROUTINE Remove_Tet()

  USE common_Parameters
  IMPLICIT NONE

  INTEGER :: IT, NB_new, itnew, itnb, i


  Mark_Tet(1:NB_Tet) = 0
  NB_new = 0
  DO IT = 1,NB_Tet
     IF(IP_Tet(4,IT)<=0) CYCLE
     NB_new = NB_new+1
     Mark_Tet(IT) = NB_new
  ENDDO

  IF(NB_new==NB_Tet) RETURN
  WRITE(29,*)'Remove_Tet: NB_Tet (before and after removing) =',NB_Tet, NB_new

  !--- reset ADTree
  IF(ADTree_Search)THEN
     CALL ADTree_MappingCells(ADTreeForMesh,Mark_Tet,NB_Tet)
  ENDIF

  DO IT = 1,NB_Tet
     itnew = Mark_tet(IT)
     IF(itnew==0) CYCLE
     IP_Tet(:,itnew) = IP_Tet(:,IT)
     Sphere_Tet(:,itnew) = Sphere_Tet(:,IT)
     Volume_Tet(  itnew) = Volume_Tet(  IT)
     IF(BGSpacing%Model<-1)THEN
        fMap_Tet(:,:,itnew) = fMap_Tet(:,:,IT)
     ELSE IF(BGSpacing%Model>1)THEN
        Scale_Tet(itnew) = Scale_Tet(IT)
     ENDIF
     DO i=1,4
        itnb = Next_Tet(i,IT)
        IF(itnb>0)THEN
           IF (Mark_Tet(itnb)==0) THEN
             Next_Tet(i,itnew) = -1
           ELSE
            Next_Tet(i,itnew) = Mark_Tet(itnb)
            IF(Debug_Display>3)THEN
               IF(Next_Tet(i,itnew)==0)THEN
                 
                  WRITE(29,*)'Error--- : wrong Next'
                  WRITE(29,*)'itnew=',itnew, ' IT=',IT, ' IP=',IP_Tet(:,itnew)
                  WRITE(29,*)'itnb =',itnb,  ' IP=',IP_Tet(:,itnb),   &
                       ' R=',Sphere_Tet(4,itnb)
                  CALL Error_Stop ('Remove_Tet')
               ENDIF
            ENDIF
           ENDIF
        ELSE
           Next_Tet(i,itnew) = -1
        ENDIF
     ENDDO
  ENDDO
  NB_Tet = NB_new

END SUBROUTINE Remove_Tet

!*******************************************************************************
!>      
!!     Remove those points with a negative marker::Mark_Point (IP)=-999.      \n
!!     Reset :   common_Parameters::NB_Point, common_Parameters::IP_Tet,
!!               common_Parameters::IP_BD_Tri (1:3,:).
!!     @param[out]  common_Parameters::Mark_Point (old_ID) = IP               \n
!!                  IP>0 then newID = IP;                                     \n
!!                  IP=0 then newID = oldID;                                  \n
!!                  IP<0 then oldID is removed.
!<      
!*******************************************************************************
SUBROUTINE Remove_Point()

  USE common_Parameters
  IMPLICIT NONE

  INTEGER :: IT, IP, IPend, i, IB

  WHERE(Mark_Point(1:NB_Point)/=-999) Mark_Point(1:NB_Point) = 0
  IPend = NB_Point
  DO IP = 1, NB_Point
     IF(Mark_Point(IP)/=-999) CYCLE
     DO WHILE(Mark_Point(IPend)==-999)
        IPend = IPend-1
     ENDDO
     IF(IP>IPend) EXIT

     Mark_Point(IPend) = IP
     Mark_Point(IP) = -1
     Posit(:,IP)  = Posit(:,IPend)
     RR_Point(IP) = RR_Point(IPend)
     IF(BGSpacing%Model<-1)THEN
        fMap_Point(:,:,IP) = fMap_Point(:,:,IPend)
     ELSE IF(BGSpacing%Model>1)THEN
        Scale_Point(IP) = Scale_Point(IPend)
     ENDIF

     IPend = IPend-1
  ENDDO

  IF(NB_Point==IPend) RETURN
  WRITE(29,*)'Remove_Point: NB_Point (before and after removing) =',NB_Point, IPend

  NB_Point = IPend

  DO IT = 1,NB_Tet
     DO i=1,4
        IP = Mark_Point(IP_Tet(i,IT))
        IF(IP<0) THEN
         WRITE(*,*) IP_Tet(1,IT),IP_Tet(2,IT),IP_Tet(3,IT),IP_Tet(4,IT) 
         CALL  Error_Stop (' Remove_Point: A tet with Mark_Point<0 exists')
        END IF
        IF(IP>0) IP_Tet(i,IT) = IP
     ENDDO
  ENDDO

  DO IB = 1,NB_BD_Tri
     DO i=1,3
        IP = Mark_Point(IP_BD_Tri(i,IB))
        IF(IP<0) CALL  Error_Stop (' Remove_Point: Tried to remove boundary point')
        IF(IP>0) IP_BD_Tri(i,IB) = IP
     ENDDO
  ENDDO


END SUBROUTINE Remove_Point

!*******************************************************************************
!>     
!!     Remove those points with negative marker::Mark_Point (IP)=-999.      \n
!!     Differing from subroutine Remove_Point(): here the order of points is kept.  \n
!!     Reset :   common_Parameters::NB_Point, common_Parameters::IP_Tet,
!!               common_Parameters::IP_BD_Tri (1:3,:).
!!     @param[out]  common_Parameters::Mark_Point (old_ID) = IP               \n
!!                  IP>0 then newID = IP;                                     \n
!!                  IP=-999 then oldID is removed.
!<      
!*******************************************************************************
SUBROUTINE Remove_Point_inOrder()

  USE common_Parameters
  IMPLICIT NONE

  INTEGER :: IT, IP, IPend, i, IB

  IPend = 0
  DO IP = 1, NB_Point
     IF(Mark_Point(IP)==-999) CYCLE
     IPend = IPend+1
     Mark_Point(IP)  = IPend
     Posit(:,IPend)  = Posit(:,IP)
     RR_Point(IPend) = RR_Point(IP)
     IF(BGSpacing%Model<-1)THEN
        fMap_Point(:,:,IPend) = fMap_Point(:,:,IP)
     ELSE IF(BGSpacing%Model>1)THEN
        Scale_Point(IPend) = Scale_Point(IP)
     ENDIF
  ENDDO

  IF(NB_Point==IPend) RETURN
  IF(Debug_Display>2)THEN
     WRITE(29,*)'Remove_Point: NB_Point (before and after removing) =',NB_Point, IPend
  ENDIF

  NB_Point = IPend

  DO IT = 1,NB_Tet
     DO i=1,4
        IP = Mark_Point(IP_Tet(i,IT))
        IF(IP<=0)THEN
           WRITE(29,*)'Error--- Element points dispear'
           CALL Error_Stop ('Remove_Point_inOrder')
        ENDIF
        IP_Tet(i,IT) = IP
     ENDDO
  ENDDO

  DO IB = 1,NB_BD_Tri
     DO i=1,3
        IP = Mark_Point(IP_BD_Tri(i,IB))
        IF(IP<=0)THEN
           WRITE(29,*)'Error--- Boundary points disappear, IB=',IB
           WRITE(29,*)' IP_BD_Tri(:,IB)=',IP_BD_Tri(:,IB)
           WRITE(29,*)' Mark_Point=',Mark_Point(IP_BD_Tri(1:3,IB))
           CALL Error_Stop ('Remove_Point_inOrder')
        ENDIF
        IP_BD_Tri(i,IB) = IP
     ENDDO
  ENDDO


END SUBROUTINE Remove_Point_inOrder

!*******************************************************************************
!>       
!!     sort boundary nodes according to common_Parameters::Mark_Point.    
!!     @param[in]      
!!       common_Parameters::Mark_Point (ip)  <0 : node put first;             \n
!!                                     (ip)  >0 : node put last;              \n
!!                                     (ip)  =0 : node put in middle.
!!     @param[out]   IPfirstEnd   :  the End node in the first part
!!     @param[out]   IPlastStart  :  the Start node in the last part
!!     @param[out]   common_Parameters::Mark_Point (old_ID) = new_ID
!!                                      mapping of points.
!!
!!     The order of node in each part is kept.  e.g. if by input ip1<ip2
!!         and sign(Mark_Point(ip1))=sign(Mark_Point(ip2))
!!         then by output we have Mark_Point(ip1)<Mark_Point(ip2).
!!
!!     update:  common_Parameters::Posit, common_Parameters::IP_BD_Tri,
!!              common_Parameters::IP_Tet.
!<       
!*******************************************************************************
SUBROUTINE Sort_Nodes(IPfirstEnd, IPlastStart)

  USE common_Parameters
  USE array_allocator
  IMPLICIT NONE

  INTEGER, INTENT(OUT) :: IPfirstEnd, IPlastStart
  INTEGER :: ip, it, ib, ipp(3)
  REAL*8, DIMENSION(:,:),  POINTER :: coorpold
  REAL*8, DIMENSION(  :),  POINTER :: rrold

  CALL SignSort(NB_Point, Mark_Point, IPfirstEnd, IPlastStart)

  !--- resorting Posit
  CALL allc_2Dpointer(coorpold, 3,NB_Point,   'coorpold')
  CALL allc_1Dpointer(rrold,      NB_Point,   'rrold')
  DO ip=1,NB_Point
     coorpold(:,ip) = Posit(:,ip)
     rrold(ip)      = RR_Point(ip)
  ENDDO

  DO ip=1,NB_Point
     Posit(:,Mark_Point(ip))  = coorpold(:,ip)
     RR_Point(Mark_Point(ip)) = rrold(ip)
  ENDDO
  DEALLOCATE(coorpold, rrold)

  DO it=1,NB_Tet
     IP_Tet(:,it) = Mark_Point(IP_Tet(:,it))
  ENDDO

  DO ib = 1,NB_BD_Tri
     ipp(1:3) = Mark_Point(IP_BD_Tri(1:3,IB))
     CALL sort_ipp_tri(ipp)
     IP_BD_Tri(1:3,IB) = ipp(1:3)
  ENDDO

  RETURN
END SUBROUTINE Sort_Nodes


!*******************************************************************************
!>
!!   Populate mark value from those points with none-zero mark values
!!            by several layers  
!!   @param[in]  nlayer    : the number of populating layers.
!!                         Choose a big number or -1 to populate the whole domain
!!   @param[in]  common_Parameters::Mark_Point
!!                         : the marking map of all points.
!!                         The populating only go through those points with
!!                         Mark_Point(ip) = 0   
!!   @param[out] common_Parameters::Mark_Point
!!                         : updated marking map. 
!!   @param[out] common_Parameters::Mark_Tet
!!                         : marking map of cells after populating.           \n
!!                         = 0  : an cell not be affected.                    \n
!!                         else : an cell be populated.   
!<       
!*******************************************************************************
SUBROUTINE Populate_Layers(nlayer)

  USE common_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: nlayer
  INTEGER :: ilayer, nlayer1
  INTEGER :: i, ip, IT, ip4(4)
  LOGICAL :: CarryOn
  INTEGER, DIMENSION(:),  POINTER :: Check_Tet

  ALLOCATE(Check_Tet(NB_Tet))
  Check_Tet(1:NB_Tet) = 0
  Mark_Tet (1:NB_Tet) = 0
  nlayer1 = nlayer
  if(nlayer1<0) nlayer1 = NB_Point

  DO ilayer = 1,nlayer1
     CarryOn = .FALSE.
     DO IT = 1, NB_Tet
        IF(Check_Tet(IT)==0)THEN
           DO i=1,4
              ip = IP_Tet(i,IT)
              IF(Mark_Point(ip)/=0)THEN
                 Mark_Tet(IT)  = Mark_Point(ip)
                 Check_Tet(IT) = ilayer
                 CarryOn = .TRUE.
                 EXIT
              ENDIF
           ENDDO
        ENDIF
     ENDDO

     IF(.NOT. CarryOn) EXIT

     DO IT = 1, NB_Tet
        IF(Check_Tet(IT)==ilayer)THEN
           ip4(:) = IP_Tet(:,IT)
           WHERE(Mark_Point(ip4(:))==0) Mark_Point(ip4(:)) = Mark_Tet(IT)
        ENDIF
     ENDDO
  ENDDO

  DEALLOCATE(Check_Tet)

END SUBROUTINE Populate_Layers

!*******************************************************************************
!>
!!   Populate mark value from those points with a given mark value.
!!   @param[in]  MarkGiven : The given mark value of source points.
!!   @param[in]  common_Parameters::Mark_Point
!!                         : the marking map of all points.
!!                         The populating only go through those points with
!!                         Mark_Point(ip) = 0   
!!   @param[in]  common_Parameters::Mark_Tet
!!                         : the marking map of all elements.
!!                         The populating only go through those elements with
!!                         Mark_Tet(ip) = 0   
!!   @param[out] common_Parameters::Mark_Point
!!                         : updated marking map. 
!!   @param[out] common_Parameters::Mark_Tet
!!                         : marking map of cells after populating.           \n
!!                         = 0  : an cell not be affected.                    \n
!!                         else : an cell be populated.   
!<       
!*******************************************************************************
SUBROUTINE Populate_WholeDomain(MarkGiven)

  USE common_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: MarkGiven
  INTEGER :: i, j, IT, ip4(4)
  LOGICAL :: CarryOn

  IF(MarkGiven==0) RETURN

  DO
     CarryOn = .FALSE.
     DO IT = 1, NB_Tet
        IF(Mark_Tet(IT)==0)THEN
           DO i=1,4
              IF(Mark_Point(IP_Tet(i,IT))==MarkGiven)THEN
                 CarryOn = .TRUE.
                 Mark_Tet(IT) = MarkGiven
                 ip4(:) = IP_Tet(:,IT)
                 WHERE(Mark_Point(ip4(:))==0) Mark_Point(ip4(:)) = MarkGiven
                 EXIT
              ENDIF
           ENDDO
        ENDIF
     ENDDO

     IF(.NOT. CarryOn) EXIT
  ENDDO

END SUBROUTINE Populate_WholeDomain


!*******************************************************************************
!>
!!     sort triangle connectivity by putting the minimum node number first
!!          but keeping the orientation.  
!<       
!*******************************************************************************
SUBROUTINE sort_ipp_tri(ipp)

  IMPLICIT NONE

  INTEGER, INTENT(INOUT)  :: ipp(3)
  INTEGER :: j1

  j1 = MIN(ipp(1),ipp(2),ipp(3))
  IF(ipp(1)==j1) THEN
  ELSE IF(ipp(2)==j1) THEN
     ipp(2) = ipp(3)
     ipp(3) = ipp(1)
     ipp(1) = j1
  ELSE
     ipp(3) = ipp(2)
     ipp(2) = ipp(1)
     ipp(1) = j1
  ENDIF

  RETURN

END SUBROUTINE sort_ipp_tri

!*******************************************************************************
!>
!!     sort triangle connectivity by putting the minimum node number first
!!          but keeping the orientation.  
!<       
!*******************************************************************************
SUBROUTINE sort_ipph_tri(ipp, ipsp)
  USE CellConnectivity
  IMPLICIT NONE

  INTEGER, INTENT(INOUT)  :: ipp(3), ipsp(10)
  INTEGER :: j1

  j1 = MIN(ipp(1),ipp(2),ipp(3))
  IF(ipp(1)==j1) THEN
  ELSE IF(ipp(2)==j1) THEN
     ipp(2) = ipp(3)
     ipp(3) = ipp(1)
     ipp(1) = j1
     ipsp   = ipsp(iRotate_C3Tri(:,2))
  ELSE
     ipp(3) = ipp(2)
     ipp(2) = ipp(1)
     ipp(1) = j1
     ipsp   = ipsp(iRotate_C3Tri(:,3))
  ENDIF

  RETURN

END SUBROUTINE sort_ipph_tri


