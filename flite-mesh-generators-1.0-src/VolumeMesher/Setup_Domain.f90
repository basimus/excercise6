!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!    Setup an initial domain and build a tetrahedral mesh under a convex-hull (box).
!!    @param[in]   common_Parameters::Surf
!!    @param[in]   common_Parameters::Initial_Split
!!    @param[out]  common_Parameters::XYZmin
!!    @param[out]  common_Parameters::XYZmax
!!    @param[out]  common_Parameters::NB_Point
!!    @param[out]  common_Parameters::NB_Extra_Point
!!    @param[out]  common_Parameters::Posit
!!    @param[out]  common_Parameters::NB_Tet
!!    @param[out]  common_Parameters::IP_Tet
!!    @param[out]  common_Parameters::NEXT_Tet
!!    @param[out]  common_Parameters::ADTreeForMesh
!!                                   if common_Parameters::ADTree_Search==.true.
!!
!<       
!*******************************************************************************
SUBROUTINE Setup_Domain()

  USE common_Parameters
  USE Mapping3D
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: IT, IP, IB, i, j, k, n, ipweb(3), IPn, IP1
  REAL*8  :: cent(3), rad(3), xyz0(3), r, rmin, pweb(3), cweb(3)
  INTEGER :: iHex(8), ITet(4,6)
  INTEGER, DIMENSION(:), ALLOCATABLE :: Mark_Frame, IPnear
  REAL*8,  DIMENSION(:), ALLOCATABLE :: Dist_Frame
  REAL*8  :: minXYZ,maxXYZ

  !---- Measure the domain

  CALL SurfaceMeasure()
  XYZmax(1:3) = XYZmax(1:3) +  BGSpacing%BasicSize
  XYZmin(1:3) = XYZmin(1:3) -  BGSpacing%BasicSize

  !minXYZ = MINVAL(XYZmin)
  !XYZmin(1:3) = minXYZ

  !maxXYZ = MAXVAL(XYZmax)
  !XYZmax(1:3) = maxXYZ

  !XYZmin(1:3) = -1.0d0*MAX(maxXYZ,ABS(minXYZ))
  !XYZmax(1:3) = -1.0d0*XYZmin(1:3)

  


  !---- Put NB_Extra_Point points to form a cubic convex full

  cent(:) = (XYZmax(:) + XYZmin(:))/2.d0
  rad(:)  = (XYZmax(:) - XYZmin(:))/1.6d0
  xyz0(:) = cent(:) - rad(:)
  rad(:)  = 2.d0 * rad(:) / Initial_Split

  NB_Extra_Point = (Initial_Split+1)**3
  ALLOCATE(Mark_Frame(NB_Extra_Point),IPnear(NB_Extra_Point))
  ALLOCATE(Dist_Frame(NB_Extra_Point))
  Mark_Frame(1:NB_Extra_Point) = 0
  IPnear    (1:NB_Extra_Point) = 0
  Dist_Frame(1:NB_Extra_Point) = 1.d20


  !---- mark and replace those extra points if it is too close to a boundary point

  DO IP = 1,Surf%NB_Point
     pweb(:)  = (Posit(:,IP) - xyz0(:))/rad(:)
     ipweb(:) = INT(pweb)
     cweb(:)  = pweb(:) - ipweb(:)
     WHERE(cweb(:)>0.5d0)
        ipweb(:) = ipweb(:) + 1
        cweb(:)  = 1.d0 - cweb(:)
     END WHERE
     IF(  MIN(ipweb(1),ipweb(2),ipweb(3))==0 .OR.    &
          MAX(ipweb(1),ipweb(2),ipweb(3))==Initial_Split ) CYCLE
     IF(  MAX(cweb(1), cweb(2), cweb(3))>0.2  ) CYCLE

     cweb(:)  = cweb(:)*rad(:)
     r = cweb(1)*cweb(1) + cweb(2)*cweb(2) + cweb(3)*cweb(3)
     r = r / (Scale_Point(IP)*BGSpacing%BasicSize)**2
     n = 1 + ipweb(1) + ipweb(2)*(Initial_Split+1)    &
          +  ipweb(3)*(Initial_Split+1)**2
     IF(r<Dist_Frame(n))THEN
        Dist_Frame(n) = r
        IPnear(n) = IP
        IF(r<0.5)THEN
           IF(Mark_Frame(n)==0) NB_Extra_Point = NB_Extra_Point -1
           Mark_Frame(n) = IP
        ENDIF
     ENDIF
  ENDDO

  WRITE(*, *)'Initial_Split,NB_Extra_Point = ',Initial_Split,NB_Extra_Point
  WRITE(29,*)'Initial_Split,NB_Extra_Point = ',Initial_Split,NB_Extra_Point

  NB_Point = Surf%NB_Point + NB_Extra_Point

  DO WHILE(NB_Point>nallc_Point)
     CALL ReAllocate_Point( )
  ENDDO

  DO WHILE(6*Initial_Split**3>nallc_Tet)
     CALL ReAllocate_Tet( )
  ENDDO
  CALL allc_2Dpointer(IP_BD_Tri, 5,    &
       Surf%NB_Tri+12*Initial_Split**2, 'IP_BD_Tri')
  CALL allc_1Dpointer(Mark_BD_Point,     nallc_Point,  'Mark_BD_Point')

  Mark_BD_Point(1:NB_Point) = 0

  DO IP=NB_Point,NB_Extra_Point+1,-1
     Posit(:,IP) = Posit(:,IP - NB_Extra_Point)
     Scale_Point(IP) = Scale_Point(IP - NB_Extra_Point)
  ENDDO

  IF(Background_Model==-1)THEN
     DO IP=NB_Point,NB_Extra_Point+1,-1
        fMap_Point(:,:,IP) = fMap_Point(:,:,IP - NB_Extra_Point)
     ENDDO
  ENDIF

  IP = 0
  DO k=1,Initial_Split+1
     DO j=1,Initial_Split+1
        DO i=1,Initial_Split+1
           n = 1 + (i-1) + (j-1)*(Initial_Split+1)    &
                + (k-1)*(Initial_Split+1)**2
           IF(Mark_Frame(n)==0)THEN
              IP = IP+1
              Mark_Frame(n) = IP
              Posit(1,IP) = xyz0(1) + (i-1)*rad(1)
              Posit(2,IP) = xyz0(2) + (j-1)*rad(2)
              Posit(3,IP) = xyz0(3) + (k-1)*rad(3)
              IF(IPnear(n)>0)  IPnear(n) = IPnear(n) + NB_Extra_Point
           ELSE
              IF(IPnear(n)/=Mark_Frame(n))THEN
                 WRITE(29,*)'Error---'
                 CALL Error_Stop ('Setup_Domain')
              ENDIF
              Mark_Frame(n) = Mark_Frame(n) + NB_Extra_Point
              Mark_BD_Point(Mark_Frame(n)) = -1
              IPnear(n)     = Mark_Frame(n)
           ENDIF
        ENDDO
     ENDDO
  ENDDO

  IF(IP/=NB_Extra_Point)THEN
     WRITE(29,*)'Error--- initial points counting'
     CALL Error_Stop ('Setup_Domain')
  ENDIF

  IF(Initial_Split==1 .AND. 1==0)THEN
     !--- this option is discaded
     IP_Tet(1:4,1)  = (/1,2,4,6/)
     IP_Tet(1:4,2)  = (/4,6,8,7/)
     IP_Tet(1:4,3)  = (/1,5,6,7/)
     IP_Tet(1:4,4)  = (/1,4,3,7/)
     IP_Tet(1:4,5)  = (/1,6,4,7/)
     DO IT=1,5
        IP_Tet(:,IT) = Mark_Frame(IP_Tet(:,IT))
     ENDDO
     NB_Tet = 5
  ELSE

     iHex(1:2)    = (/1,2/)
     iHex(3:4)    = iHex(1:2) + (Initial_Split+1)
     iHex(5:8)    = iHex(1:4) + (Initial_Split+1)**2
     ITet(1:4,1)  = (/ iHex(1), iHex(2), iHex(3), iHex(5) /)
     ITet(1:4,2)  = (/ iHex(5), iHex(7), iHex(6), iHex(3) /)
     ITet(1:4,3)  = (/ iHex(2), iHex(6), iHex(3), iHex(5) /)
     ITet(1:4,4)  = (/ iHex(2), iHex(4), iHex(3), iHex(6) /)
     ITet(1:4,5)  = (/ iHex(6), iHex(7), iHex(8), iHex(4) /)
     ITet(1:4,6)  = (/ iHex(4), iHex(7), iHex(3), iHex(6) /)

     IP = 0
     IT = 0
     DO k=1,Initial_Split
        DO j=1,Initial_Split
           DO i=1,Initial_Split
              IP =  (i-1) + (j-1)*(Initial_Split+1)    &
                   + (k-1)*(Initial_Split+1)**2
              DO n=1,6
                 IP_Tet(:,IT+n) = Mark_Frame(IP + ITet(:,n))
              ENDDO
              IT = IT+6
           ENDDO
        ENDDO
     ENDDO

     NB_Tet = IT
  ENDIF

  CALL Next_Build()

  !--- set ADTree for searching
  IF(ADTree_Search)THEN
     call ADTree_SetName(ADTreeForMesh, 'mesh_ADTree')
     call ADTree_SetTree(ADTreeForMesh, Posit, IP_Tet, NB_Tet, NB_Point)
  ENDIF

  !---- Set stretch mapping

  IF(useStretch)THEN
     IF(ABS(Background_Model)>=2 .AND. (BGSpacing%Model<0 .OR. BGSpacing%Model>1))THEN
        !--- we already have a background,
        !--- so we interpolate surface points and frame points on it.
        DO IP=1,NB_Point
           CALL Get_Point_Mapping(IP)
        ENDDO
     ELSE IF(ABS(Background_Model)==1)THEN
        !--- we do not have a background,
        !--- but we have already calculated stretch values for surface point,
        !--- and we set that for frame points by its nearest surface point.
        DO k=1,Initial_Split+1
           DO j=1,Initial_Split+1
              DO i=1,Initial_Split+1
                 n = 1 + (i-1) + (j-1)*(Initial_Split+1)    &
                      + (k-1)*(Initial_Split+1)**2
                 IPn = Mark_Frame(n)
                 IF(IPn>NB_Extra_Point)CYCLE
                 IP = IPnear(n)
                 IF(IP==0)THEN
                    rmin = HugeValue
                    DO IP1=NB_Extra_Point+1,NB_Point
                       r = Geo3D_Distance_SQ(Posit(:,IP1),Posit(:,IPn))
                       IF(r<rmin)THEN
                          IP = IP1
                          rmin = r
                       ENDIF
                    ENDDO
                 ENDIF
                 Scale_Point(IPn) = Scale_Point(IP)
                 IF(Background_Model==-1)THEN
                    fMap_Point(:,:,IPn) = fMap_Point(:,:,IP)
                 ENDIF
              ENDDO
           ENDDO
        ENDDO
     ENDIF

     IF(ABS(Background_Model)==1 .OR. BGSpacing%Model<0)THEN
        DO IT=1,NB_Tet
           CALL Get_Tet_Mapping(IT)
        ENDDO
     ENDIF
  ENDIF

  !---- Calculate the centre and radius of the Circumscribed sphere

  CircumUpdated = .FALSE.
  CALL Get_Tet_Circum(0)
  CALL Get_Tet_Volume(0)
  IF(Debug_Display>3 .AND. Initial_Split>1)THEN
     WRITE(*,*)'-- check initial frame'
     CALL check_next(-3)
  ENDIF
  IF(Debug_Display>2 .AND. Initial_Split>1)THEN
     CALL Check_Mesh_Geometry('Setup_Domain')
  ENDIF

  DEALLOCATE(Mark_Frame, IPnear, Dist_Frame)



  RETURN

END SUBROUTINE Setup_Domain
