!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!     Break large edges 
!!     @param[in]  byDelaunay = .true.  :  generate new elements by Delaunay method.  \n
!!                            =.flase.  :  generate new elements by splitting directly.
!<
!*******************************************************************************
SUBROUTINE Edge_Break(byDelaunay)

  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  USE Mapping3D
  IMPLICIT NONE

  LOGICAL, INTENT(IN) :: byDelaunay
  INTEGER :: IP, IT, i, i1, i2, itpole, ip1, ip2, ipp(4), IB
  INTEGER :: NBold_Tet, NBold_Pt, NBcount_Pt, Idou, ILoop, indmax, Isucc
  REAL*8  :: p1(3),p2(3),p3(3),p4(3),p0(3), pp(3,4)
  REAL*8  :: vol, xl, fMap(3,3)
  REAL*8  :: RAsq,RAstand,RAfac,RAmaxloc,RAvol,RAsph

  IF(.NOT. CircumUpdated) CALL Get_Tet_Circum(0)
  IF(.NOT. VolumeUpdated) CALL Get_Tet_Volume(0)

  Idou    = 1
  IF(Element_Break_Method==3) Idou = 0
  RAfac   = 2.0d0
  RAstand = dsqrt(2.d0) * BGSpacing%BasicSize
  RAvol   = IdealVolume / 10.d0
  RAsph   = RAstand / 2.d0

  DO i=1,Idou
     RAstand = RAstand * RAfac
     RAsph   = RAsph   * RAfac
  ENDDO

  IF(.NOT.NodeNetTree_isBuilt(Edge_BD_Tree)) CALL Surface_Edge()    !--- for Boundary_Edg_Match

  NBcount_Pt = 0
  ILoop = 0
  Loop_Idou : DO WHILE(Idou>=0)

     NBold_Tet = NB_Tet
     NBold_Pt  = NB_Point

     Loop_IT : DO IT = 1,NBold_Tet

        IF(IP_Tet(4,IT) <=0)           CYCLE Loop_IT  !--- a false element
        IF(IP_Tet(4,IT) <= NBcount_Pt) CYCLE Loop_IT  !--- a checked element
        IF(IP_Tet(4,IT) >  NBold_Pt)   CYCLE Loop_IT  !--- a new generated element in this loop
        IF(Sphere_Tet(4,IT)<=RAsph)    CYCLE Loop_IT  !--- a small element
        IF(Volume_Tet(IT)  < RAvol)    CYCLE Loop_IT  !--- a small element

        !---- find the longest edge

        ipp(1:4)  = IP_Tet(1:4,IT)
        pp(:,1:4) = Posit(:,ipp(1:4))
        IF(BGSpacing%Model<0)THEN
           IF(BGSpacing%Model==-1)THEN
              fMap(:,:) = BGSpacing%BasicMap(:,:)
           ELSE
              fMap(:,:) = fMap_Tet(:,:,IT)
           ENDIF
           DO i=1,4
              pp(:,i) = Mapping3D_Posit_Transf(pp(:,i),fMap,1)
           ENDDO
        ELSE IF(BGSpacing%Model>1)THEN
           DO i=1,4
              pp(:,i) = pp(:,i) / Scale_Tet(IT)
           ENDDO
        ENDIF

        indmax = 0
        RAmaxloc = 0.d0
        DO i=1,6
           i1  = I_Comb_Tet(1,i)
           i2  = I_Comb_Tet(2,i)
           ip1 = ipp(i1)
           ip2 = ipp(i2)

           !--- skip a boundary edge
           IF(Recovery_Time<=1)THEN
              IF(ip1<=NB_BD_Point .AND. ip2<=NB_BD_Point)THEN
                 CALL Boundary_Edg_Match(IP1,IP2,IB)
                 IF(IB>0) CYCLE    !---- a boundary edge
              ENDIF
           ELSE
              IF(ip1<=8 .AND. ip2<=8)THEN
                 IF(ip1==1 .AND. ip2/=7) CYCLE
                 IF(ip1==2 .AND. ip2/=8) CYCLE
                 IF(ip1==3 .AND. ip2/=5) CYCLE
                 IF(ip1==4 .AND. ip2/=6) CYCLE
                 IF(ip1==5 .AND. ip2/=3) CYCLE
                 IF(ip1==6 .AND. ip2/=4) CYCLE
                 IF(ip1==7 .AND. ip2/=1) CYCLE
                 IF(ip1==8 .AND. ip2/=2) CYCLE
              ENDIF
           ENDIF


           p1(:) = pp(:,i1)
           p2(:) = pp(:,i2)
           RAsq  = Geo3D_Distance(p1,p2)
           IF(RAsq>RAmaxloc)THEN
              RAmaxloc = RAsq
              IF(RAsq>=RAstand) indmax = i
           ENDIF
        ENDDO

        IF(indmax==0) CYCLE Loop_IT
        RAsq  = RAmaxloc

        !---- search the elements surrounding the edge
        !---- for checking the minimum volume and building the cavity
        CALL Search_Pole_Surround(IT, indmax)

        !---- check minimum volume
        IF(NB_PolePt>NB_PoleTet)THEN
           WRITE(29,*)'Error--- : NB_PolePt>NB_PoleTet, boundary egde again?',NB_PolePt,NB_PoleTet
           WRITE(29,*)'IT=',IT,' ind=',indmax,' ips=',ipp(I_Comb_Tet(1:2,indmax))
           CALL Error_Stop ('Edge_Break')
        ENDIF

        DO i=1,NB_PoleTet
           itpole = IT_PoleTet(i)
           IF(IP_Tet(4,itpole) >  NBold_Pt)   CYCLE Loop_IT  !--- a new generated element in this loop
           IF(IP_Tet(4,itpole) <=0        )   CYCLE Loop_IT  !--- a false element
           IF(Sphere_Tet(4,itpole)<=RAsph)    CYCLE Loop_IT  !--- a small element
           IF(Volume_Tet(itpole)  < RAvol)    CYCLE Loop_IT  !--- a small element
        ENDDO

        NB_Point = NB_Point + 1
        IF(NB_Point>nallc_point) CALL ReAllocate_Point()
        xl = RAsq/BGSpacing%BasicSize
        IF(xl>2.1 .AND. xl<3.3)THEN
           xl = 0.33333d0
        ELSE
           xl = 0.5d0
        ENDIF

        ip1 = ipp(I_Comb_Tet(1,indmax))
        ip2 = ipp(I_Comb_Tet(2,indmax))
        p0 = Posit(:,ip1) + (Posit(:,ip2)-Posit(:,ip1))*xl
        Posit(:,NB_Point) = p0(:)
        IF(BGSpacing%Model<-1 .OR. BGSpacing%Model>1)THEN
           CALL Get_Point_Mapping(NB_Point)
        ENDIF

        Isucc   = 0
        IF(byDelaunay) THEN
           CALL Insert_Point_Delaunay(NB_Point,-1,Isucc,0)
        ELSE
           CALL Insert_Point_SplitEdge(NB_Point,ip1,ip2,Isucc)
        ENDIF
        IF(Isucc<=0)THEN
           !--- fail to insert
           NB_Point = NB_Point -1
        ENDIF

        IF(Debug_Display>2 .AND. MOD(NB_Point,100000)==0)THEN
           WRITE(*,'(2(a,I3),2(a,i8),a)') ' Idou=',Idou, ' ILoop=',ILoop,     &
                ' Total elements.:', NB_Tet,    &
                ' Total points.: ',NB_Point,CHAR(13)
           CALL Check_Mesh_Geometry('Edge_Break')
        ENDIF

     ENDDO Loop_IT

     IF(byDelaunay) CALL Remove_tet()

     IF(NBold_Pt == NB_Point .OR. ILoop==15)THEN
        IF(ILoop==15) WRITE(29,'(/,a)')'WARNING--- Edge_Break: too many loops for breaking'
        Idou=Idou-1
        RAstand = RAstand / RAfac
        RAsph   = RAsph   / RAfac
        NBcount_Pt = 0
        ILoop = 0
        WRITE(*,*)
     ELSE
        NBcount_Pt =  NBold_Pt
        ILoop     = ILoop + 1
        WRITE(*,'(2(a,I3),2(a,i8),a)') ' Idou=',Idou, ' ILoop=',ILoop,     &
             ' Total elements.:', NB_Tet,    &
             ' Total points.: ',NB_Point,CHAR(13)
     ENDIF

  ENDDO Loop_Idou
  WRITE(*,*)


  IF(Debug_Display>3)THEN
     Mark_Point(1:NB_Point) = 0
     DO IT=1,NB_Tet
        ipp(1:4) = IP_Tet(1:4,IT)
        Mark_Point(ipp(1:4)) = 1
     ENDDO
     DO IP=1,NB_Point
        IF(Mark_Point(IP)==0)THEN
           WRITE(29,*)'Error--- : a node lost: ',IP
           CALL Error_Stop ('Edge_Break')
        ENDIF
     ENDDO

     CALL Check_Next(-1)
  ENDIF

  RETURN
END SUBROUTINE Edge_Break

!*******************************************************************************
!>
!!     Break large edges by splitting directly (not by Delaunay).             \n
!!     Reminder: This subroutine deffers from Edge_Break() by that
!!             it build edge system and test each edge directly,
!!             rather than investigating edges through each element.
!<
!*******************************************************************************
SUBROUTINE Edge_Break_Directly()

  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  USE Mapping3D
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: IP, IT, i, i1, i2, ip1, ip2, ipp(4)
  INTEGER :: ILoop, iis, Isucc
  INTEGER :: Mside, NB_Edge, FirstEdge, LastEdge
  REAL*8  :: vol, xl, fMap(3,3), Scalar
  REAL*8  :: RAsq,RAstand,RAvol,RAsph
  INTEGER, DIMENSION(:,:), POINTER :: IP_Edge   !--- (2,:)

  VolumeUpdated = .FALSE.
  CircumUpdated = .FALSE.

  RAstand = dsqrt(2.d0) * BGSpacing%BasicSize
  RAvol   = IdealVolume / 10.d0
  RAsph   = RAstand / 2.d0

  !--- compute edge data structure

  Mside = NB_Tet+2*(NB_Point+NB_BD_Tri)
  CALL allc_2Dpointer ( IP_Edge, 2, Mside, 'IP_Edge')
  CALL Build_Edge(IP_Edge,NB_Edge,Mside)

  !--- Associate points with elements for subroutine Search_Pole_Surround2
  CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)


  FirstEdge = 1
  IF(Recovery_Time<=1)  FirstEdge = NB_BD_Edge+1
  LastEdge  = NB_Edge

  Loop_ILoop : DO ILoop=1,1

     Loop_iis : DO iis = FirstEdge, LastEdge

        ip1 = IP_Edge(1,iis)
        ip2 = IP_Edge(2,iis)
        IF(ILoop==1 .AND. Recovery_Time>1)THEN
           !--- before recovery, skip the frame edges
           IF(ip1<=8 .AND. ip2<=8)THEN
              IF(ip1==1 .AND. ip2/=7) CYCLE Loop_iis
              IF(ip1==2 .AND. ip2/=8) CYCLE Loop_iis
              IF(ip1==3 .AND. ip2/=5) CYCLE Loop_iis
              IF(ip1==4 .AND. ip2/=6) CYCLE Loop_iis
              IF(ip1==5 .AND. ip2/=3) CYCLE Loop_iis
              IF(ip1==6 .AND. ip2/=4) CYCLE Loop_iis
              IF(ip1==7 .AND. ip2/=1) CYCLE Loop_iis
              IF(ip1==8 .AND. ip2/=2) CYCLE Loop_iis
           ENDIF
        ENDIF

        IF(BGSpacing%Model<0)THEN
           IF(BGSpacing%Model==-1)THEN
              fMap(:,:) = BGSpacing%BasicMap(:,:)
           ELSE
              fMap(:,:) = Mapping3D_Interpolate(fMap_Point(:,:,ip1), fMap_Point(:,:,ip2),  &
                   0.5d0, Mapping_Interp_Model)
           ENDIF
           RAsq  = Mapping3D_Distance(Posit(:,ip1),Posit(:,ip2),fMap)
        ELSE IF(BGSpacing%Model>1)THEN
           Scalar  = (Scale_Point(ip1) + Scale_Point(ip1))/2.0
           RAsq  = Geo3D_Distance(Posit(:,ip1),Posit(:,ip2)) / Scalar
        ELSE
           RAsq  = Geo3D_Distance(Posit(:,ip1),Posit(:,ip2))
        ENDIF

        IF(RAsq<=RAstand) CYCLE Loop_iis

        !---- search the elements surrounding the edge
        !---- for checking the minimum volume and building the cavity
        CALL Search_Pole_Surround2(ip1,ip2)

        !---- check minimum volume
        IF(NB_PolePt>NB_PoleTet)THEN
           WRITE(29,*)'Error--- : NB_PolePt>NB_PoleTet, boundary Edge? iis=',iis
           WRITE(29,*)'ip1,ip2=',IP1,IP2, ' NB_PolePt>NB_PoleTet=',NB_PolePt,NB_PoleTet
           CALL Error_Stop ('Edge_Break')
        ENDIF

        DO i=1,NB_PoleTet
           IT = IT_PoleTet(i)
           IF(IP_Tet(4,IT)    <=0)        CYCLE Loop_iis  !--- a false element
           IF(Sphere_Tet(4,IT)<=RAsph)    CYCLE Loop_iis  !--- a small element
           IF(Volume_Tet(IT)  < RAvol)    CYCLE Loop_iis  !--- a small element
        ENDDO


        NB_Point = NB_Point + 1
        IF(NB_Point>nallc_point) CALL ReAllocate_Point()
        xl = 0.5d0
        Posit(:,NB_Point) = Posit(:,ip1) + (Posit(:,ip2)-Posit(:,ip1))*xl
        IF(BGSpacing%Model<-1 .OR. BGSpacing%Model>1)THEN
           CALL Get_Point_Mapping(NB_Point)
        ENDIF

        Isucc   = 0
        CALL Insert_Point_SplitEdge(NB_Point,ip1,ip2,Isucc)
        IF(Isucc==-1)THEN
           !--- fail to insert
           NB_Point = NB_Point -1
           CYCLE Loop_iis
        ENDIF


        !--- add new edges
        DO i=1,NB_PoleTet
           NB_Edge = NB_Edge + 1
           IF(NB_Edge>Mside) THEN
              Mside = Mside+10000
              CALL allc_2Dpointer ( IP_Edge, 2, Mside, 'IP_Edge')
           ENDIF
           IP_Edge(1,NB_Edge) = IP_PolePt(i)
           IP_Edge(2,NB_Edge) = NB_Point
        ENDDO

        IF(Debug_Display>2 .AND. MOD(NB_Point,100000)==0)THEN
           WRITE(*,'(a,I3,2(a,i8),a)')  ' ILoop=',ILoop,     &
                ' Total elements.:', NB_Tet,    &
                ' Total points.: ',NB_Point,CHAR(13)
           CALL Check_Mesh_Geometry('Edge_Break_Directly')
        ENDIF

     ENDDO Loop_iis


     FirstEdge = LastEdge+1
     LastEdge  = NB_Edge
     WRITE(*,'(a,I3,2(a,i8),a)')  ' ILoop=',ILoop,     &
          ' Total elements.:', NB_Tet,    &
          ' Total points.: ',NB_Point,CHAR(13)
     IF(FirstEdge>LastEdge) EXIT Loop_ILoop

  ENDDO Loop_ILoop


  WRITE(*,*)

  !--- release memory
  CALL LinkAssociation_Clear(PointAsso)
  DEALLOCATE(IP_Edge)

  IF(Debug_Display>3)THEN
     Mark_Point(1:NB_Point) = 0
     DO IT=1,NB_Tet
        ipp(1:4) = IP_Tet(1:4,IT)
        Mark_Point(ipp(1:4)) = 1
     ENDDO
     DO IP=1,NB_Point
        IF(Mark_Point(IP)==0)THEN
           WRITE(29,*)'Error--- : a node lost: ',IP
           CALL Error_Stop ('Edge_Break')
        ENDIF
     ENDDO

     CALL Check_Next(-1)
  ENDIF

  RETURN
END SUBROUTINE Edge_Break_Directly

!>
!!  Compute the SQUARE of the distance of two points under computaional domain.
!!  Get background information from module Spacing_Parameters.
!<
SUBROUTINE GetEgdeLength_SQ(P1,P2, RAsq)
  USE common_Parameters
  USE Mapping3D
  IMPLICIT NONE
  REAL*8,  INTENT(in)  :: p1(3), p2(3)
  REAL*8,  INTENT(out) :: RAsq
  REAL*8  :: p0(3), Scalar, fMap(3,3)

  IF(BGSpacing%Model<0)THEN
     p0(:) = (p1(:) + p2(:)) * 0.5d0
     CALL SpacingStorage_GetMapping(BGSpacing, P0, fMap)
     RAsq = Mapping3D_Distance_SQ(p1, p2, fMap)
  ELSE IF(BGSpacing%Model>1)THEN
     p0(:) = (p1(:) + p2(:)) * 0.5d0
     CALL SpacingStorage_GetScale(BGSpacing, P0, Scalar)
     RAsq = Geo3D_Distance_SQ(p1, p2) / (Scalar*Scalar)
  ELSE
     RAsq = Geo3D_Distance_SQ(p1, p2)
  ENDIF

  RETURN
END SUBROUTINE GetEgdeLength_SQ


