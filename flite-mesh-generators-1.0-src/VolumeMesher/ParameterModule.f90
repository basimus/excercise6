!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!*************************** MODULE OF GLOBAL DATA ****************************!
!*******************************************************************************
!     ABBREVIATIONS:
!
!     NB_    : the NumBer of ...
!     IP_    : the Id of the Point of ...
!     IT_    : the Id of the Tetrahedron of ...
!     Mark_  : the flag of ...
!
!     _BD_   : Boundary
!
!     _Tet   : of the Tetrahedral elements
!     _Tri   : of the Triangular elements
!     _Edge  : of the edges
!     _Point : of the vertex
!
!*******************************************************************************

!*******************************************************************************
!>
!!  Constant parameters.
!!
!!  Include module:
!!    CellConnectivity
!<
!*******************************************************************************
MODULE common_Constants
  USE CellConnectivity
  IMPLICIT NONE

  REAL*8 , PARAMETER :: PI = 3.141592653589793D0

END MODULE common_Constants


!*******************************************************************************
!>
!!  Control parameters and global data.
!!
!!  Include modules:
!!    NodeNetTree,            
!!    ADTreeModule,      
!!    LinkAssociation,        
!!    TetMeshStorage,         
!!    SurfaceMeshStorage,    
!!    HybridMeshStorage.  
!!    SurfaceCurvature
!<
!*******************************************************************************

MODULE common_Parameters
  USE NodeNetTree
  USE ADTreeModule
  USE LinkAssociation
  USE TetMeshStorage
  USE SurfaceMeshStorage
  USE HybridMeshStorage
  USE SurfaceCurvature
  USE SpacingStorage
  USE SurfaceMeshGeometry
  USE Elasticity
  IMPLICIT NONE

  !================================================  Title

  INTEGER, PARAMETER :: MaxFileNameLength = 256
  CHARACTER ( LEN = MaxFileNameLength ) :: JobName   !<  Prefix of the input files.
  INTEGER, SAVE      :: JobNameLength                !<  Length of JobName.


  REAL, SAVE    :: TimeStart, TimeEnd

  !================================================  NAMELIST input parameters
  !--- NAMELIST /ControlParameters/
  !--- NAMELIST /FrameParameters/
  !--- NAMELIST /BackgroundParameters/
  !--- NAMELIST /CosmeticParameters/
  !
  !    Above control parameters are set by default in the code and modified
  !    by namelist reading file "Mesh3D_v41.ctl" or command flags.
  !    Please refer "README.TXT" for detail.
  !

  !>  The level of debug and display.
  !!   A bigger number indicates more checking in the code
  !!        and more output on the screen.
  INTEGER, SAVE :: Debug_Display
  !>
  !!  @param Start_Point  =1,  read surface geometry from "*.fro" file.    \n
  !!                      =2,  read volume mesh and surface geometry from "*_0.plt" file.
  INTEGER, SAVE :: Start_Point
  !>
  !!  @param Curvature_Type =1,  Swansea curvature type;  Read "*.fro" file.   \n
  !!                        =2,  CADfix curvature type;   Read "*_backup.fbm" file.   \n
  !!                        =3,  Read CADfix curvature ("*.fbm" file),
  !!                             and convert it to Swansea curvature type.
  !!                             It will be reset as 1 after converting.
  INTEGER, SAVE :: Curvature_Type
  !>  The number of spliting of the initial hull box in each direction.   \n
  !!  Reminder: not fully checked for Initial_Split > 1 cases.
  INTEGER, SAVE :: Initial_Split
  !>
  !!  @param  ADTree_Search =.false., do not use AD_Tree for element search
  !!                                  when inserting boundary nodes.      \n
  !!                        =.true.,  use AD_Tree for element search
  !!                                  when inserting boundary nodes.
  LOGICAL, SAVE :: ADTree_Search
  !>  Options for inserting boundary points.
  !!  @param Bound_Insert_Swap(0)
  !!        =3,  do swapping before recovery based on number of boundary edges.    \n
  !!        =2,  do swapping before recovery based on dihedral.                    \n
  !!        =1,  do swapping before recovery based on volume.                      \n
  !!        =0,  no swapping before recovery.                                      \n
  INTEGER, SAVE :: Bound_Insert_Swap
  !>  @param Bound_Insert_Soft(0)
  !!        =1,  allow some points to fail to insert at first,
  !!                then try to insert them after others.                          \n
  !!        =0,  not allow any points to fail to insert.                           \n
  INTEGER, SAVE :: Bound_Insert_Soft
  !>  @param Bound_Insert_Order(0)
  !!        =1,  insert those surface nodes with a larger surface spacing first.   \n
  !!        =0,  insert surface nodes by its original order.
  !!
  !!   Reminder: under any circumstance, the numbering order of surface nodes
  !!             is NOT changed.
  INTEGER, SAVE :: Bound_Insert_Order
  !>
  !!  @param Recovery_Time
  !!         =0,  no recovery.                            \n
  !!         =1,  do recovery before refining the mesh.   \n
  !!         =2,  do recovery after refining the mesh.    \n
  !!         =3,  do recovery before refining the mesh without lift-point or
  !!              clean-face, then complete recovery after refining the mesh.
  INTEGER, SAVE :: Recovery_Time
  !>
  !!  @param Recovery_Method
  !!         =0,  no recovery.                                           \n
  !!         =1,  recovery by swapping edges. (old version)              \n
  !!         =2,  recovery by splitting edges. (old version)             \n
  !!         =3,  recovery by splitting edges, simple connection only.   \n
  !!         =4,  recovery by splitting edges, improved version.
  INTEGER, SAVE :: Recovery_Method
  !>
  !!  @param Element_Break_Method
  !!         =0,  no break.                                      \n
  !!         =1,  break large elements by inserting points in.   \n
  !!         =2,  break long edges by inserting points in.       \n
  !!         =3,  (=1+2)                                         \n
  !!         =4,  generate ideal mesh nodes and insert them.     \n
  !!         =5,  read mesh nodes from "*_1.plt" file and insert them.    \n
  !!         =-1, split long edges directly in cosmetic loop.
  INTEGER, SAVE :: Element_Break_Method

  !>  The type of frame mesh.
  !!  @param  Frame_Type
  !!          = 0,  no frame mesh. The domain enclosed by surface.         \n
  !!          = 1,  tetrahedral ideal mesh.                                \n
  !!          = 2,  cubic mesh, connecting tetrahedral with pyramids.      \n
  !!          = 3,  cubic mesh, connecting tetrahedral with wedges.        \n
  !!          = 4,  octree-tet mesh.                                       \n
  !!          = 5,  octree-hybrid mesh. 
  INTEGER, SAVE :: Frame_Type
  !>
  !!  The option to stitch the frame or boundary layers.
  !!  @param  Frame_Stitch  =0,  do NOT stitch the frame. Also apply to boundary layers.   \n
  !!                        =1,  stitch the frame.                                         \n
  !!                        =-1, read frame mesh ("*_f.plt" or "*_hyb.plt"), and
  !!                             innner mesh ("*_0.plt"), and stitch them.
  INTEGER, SAVE :: Frame_Stitch
  !>  The distance (number of gridsizes) between geometry and ideal mesh
  !!        (not refered at the moment).
  REAL*8,  SAVE :: Ideal_Dist_Gap
  !>  The number of layers connecting geometry and ideal mesh.
  INTEGER, SAVE :: Ideal_Layer_Gap
  !>  The number of layers ideal mesh expanding from geomatry.
  !!  @param Ideal_Layer_Expand  > 0,  indicating open domain.   \n
  !!                             <=0,  indicating a closed domain / specified domain.
  INTEGER, SAVE :: Ideal_Layer_Expand
  
  !>  The index of the background type.
  !!  @param Background_Model
  !!         = 1 or -1,  Tet. background (read "*.bac" & "*.fro" (layout) ).        \n
  !!         = 2 or -2,  Tet. background (read "*.bac").                            \n
  !!         = 3 or -3,  Oct. background (read "*.bac" & "*.fro" (layout) ).        \n
  !!         = 4 or -4,  Oct. background (read "*.bac").                            \n
  !!         = 5 or -5,  Oct. background (read "*.bac" & "*.dat").                  \n
  !!         = 6 or -6,  Oct. background (read "*.bac" & "*.fro" (curvature)).      \n
  !!         = 7 or -7,  Oct. background (read "*.Obac").                           \n
  !!         >0          for   isotropic problems.                                  \n
  !!         <0          for anisotropic problems.                                  \n
  !<
  INTEGER, SAVE :: Background_Model
  !>  The globe grid size. 
  !!  This is only be refered if the *.bac file is needed but not existing.
  REAL*8, SAVE  :: Globe_GridSize
  !>  The maximum value of stretch (>2.0).
  !!      Only be refered for anisotropic problems.
  REAL*8,  SAVE :: Stretch_Limit
  !>
  !!  @param Stretched_Bound_Insert
  !!          =.true.,  insert surface nodes with a stretching background.  \n
  !!          =.false., insert surface nodes with no background.
  LOGICAL, SAVE :: Stretched_Bound_Insert
  !>      The method to interpolate two mappings or surface scale.
  !!
  !!      For anisotropic problem, interpolate a mapping by:
  !!  @param Mapping_Interp_Model
  !!         =1,  by using simultaneous matrix reduction.                   \n
  !!         =2,  by using a matrix power -1, so large grid sizes dominate. \n
  !!         =3,  by using a matrix power  1, so small grid sizes dominate.
  !!
  !!      For isotropic problem, the interpolate scale by:
  !!  @param Mapping_Interp_Model
  !!         =1,  by using geometric  mean.                                 \n
  !!         =2,  by using harmonic   mean, so large grid sizes dominate.   \n
  !!         =3,  by using arithmetic mean, so small grid sizes dominate.
  INTEGER, SAVE :: Mapping_Interp_Model
  !>  The method to choose mapping when calculating a mapping for
  !!  a tetrahedron and checking whether a point located in the circumball of a tet.
  !!  @param Mapping_Choose_Model
  !!         =1,  get a mapping for an element by the mean of the 4 nodes,
  !!              and using the mapping of the element for in-sphere checking.   \n
  !!         =2,  get a mapping for an element by the mean of the 4 nodes,
  !!              and using the mapping of the node for in-sphere checking.      \n
  !!         =3,  get a mapping for an element by its centre position,
  !!              and using the mapping of the element for in-sphere checking.
  INTEGER, SAVE :: Mapping_Choose_Model
  !>
  !!  @param Interpolate_Oct_Mapping
  !!         =.FALSE., set the value of a point by the value of the Cube_grid
  !!                   in which the point lies.                                  \n
  !!         =.TRUE.,  set the value of a point by interpolating the Cube_grid
  !!                   in which the point lies. This is a more precise but slower way.
  LOGICAL, SAVE :: Interpolate_Oct_Mapping
  !>  Factor of background mesh gradation (0.05~3.0).
  !!      A small value results in a strong gradting and a smoother mesh.
  !!      If >=3.0, no gradation.
  REAL*8,  SAVE :: Gradation_Factor
  !>  Factors of curvature affect spacing.
  !!    @param Curvature_Factors (1)  The cofficient. if  = 0.2, then the grid size for 
  !!                                  a sphere surface with a radius of 1 is 0.2.    \n
  !!                             (2)  The maximum Sagitta (with dimension).          \n
  !!                             (3)  The minimum grid size (with dimension).        \n
  !!                             (4)  The maximum grid size (with dimension).        \n
  !!                             (5)  The least trailing angle (in degree, 0~180).   \n
  !!                             (6)  The full trailing angle (in degree, 0~180).    \n
  !!                             (7)  The minimum grid size (with dimension) for trailing.
  REAL*8,  SAVE :: Curvature_Factors(7)
  !>  The adjustor for the grid size (0.01~100).
  REAL*8,  SAVE :: GridSize_Adjustor

  !>  The number of CVT reinsert loops.
  INTEGER, SAVE :: Loop_CVT

  REAL*8, SAVE :: CVT_Time_Limit

  !>  The way of CVT cosmetic
  !!  @param CVT_Method   = 0   cvt reinsert without Voronoi centre smooth.                    \n
  !!                      = 1   cvt reinsert with smooth Voronoi centre by Powell Optimising.  \n
  !!                      = 2   cvt reinsert with smooth Voronoi centre by PSO Optimising.     \n
  !!                      = 3   cvt reinsert with smooth Voronoi centre by Cuckoo Optimising.  \n
  !!                      =-1   smooth Voronoi centre by Powell Optimising, no reinsert.       \n
  !!                      =-2   smooth Voronoi centre by PSO Optimising, no reinsert.          \n
  !!                      =-3   smooth Voronoi centre by Cuckoo Optimising, no reinsert.
  INTEGER, SAVE :: CVT_Method
  !>  The number of cosmetic loops.
  INTEGER, SAVE :: Loop_Cosmetic
  !>  The number of cosmetic smooth iteration.
  INTEGER, SAVE :: Loop_Smooth
  !>  The way choosing the direction of a smoothing node.
  !!  @param Smooth_Method
  !!         =1,  the mean of surrounding nodes.                            \n
  !!         =2,  the mean of the imagine nodes that make each element
  !!              has the same voulme.                                      \n
  !!         =3,  the mean of surrounding nodes with weights.               \n
  !!         =4,  using Powell Optimisation.
  INTEGER, SAVE :: Smooth_Method
  !>  
  !!  @param BoundCell_Split =0, do not care all-boundary-node cells.        \n
  !!                         =1, split all-boundary-node cells if posible.   \n
  !!                         =2, always split all-boundary-node cells.
  INTEGER, SAVE :: BoundCell_Split
  !>  The maximum collapse angle. Input in degree.
  REAL*8,  SAVE :: Collapse_Angle
  !>  The maximum swapping angle, Input in degree.
  REAL*8,  SAVE :: Swapping_Angle
  
  !>  The High-Order mesh generation model.
  !!  @param  HighOrder_Model   =1,  linear model, no high order treatment.  \n
  !!                            =3,  cubic model.
  INTEGER, SAVE :: HighOrder_Model

  !>  The number of boundary layers (<=100).                         \n
  !!   @param NB_BD_Layers =0,  no boundary layer developed.         \n
  !!                       <0,  read boundary from "*_v1.plt".  
  INTEGER, SAVE :: NB_BD_Layers
  !>  The thichkness of each boundary layers.
  REAL*8, DIMENSION(100), SAVE :: Layer_Heights
  !>  The method to smooth the normal directions.
  !!  @param NorSmooth_Method = 1   smooth the normal every layer.   \n
  !!                          else  smooth the normal in one go.
  INTEGER, SAVE :: NorSmooth_Method
  !>  @param Layer_Hybrid = 0   Tet. elements only in the layers.   \n
  !!                      = 1   hybrid mesh in the layers. Allow negative contribution for prisms.  \n
  !!                      = 2   hybrid mesh in the layers. NOT allow negative contribution for prisms.
  INTEGER, SAVE :: Layer_Hybrid
  
  NAMELIST /ControlParameters/        &
       Debug_Display,                 &
       Start_Point,                   &
       Curvature_Type,                &
       ADTree_Search,                 &
       Bound_Insert_Swap,             &
       Bound_Insert_Soft,             &
       Bound_Insert_Order,            &
       Recovery_Time,                 &
       Element_Break_Method
  NAMELIST /FrameParameters/          &
       Frame_Type,                    &
       Frame_Stitch,                  &
       Ideal_Layer_Gap,               &
       Ideal_Layer_Expand
  NAMELIST /BackgroundParameters/     &
       Background_Model,              &
       Stretch_Limit,                 &
       Stretched_Bound_Insert,        &
       Interpolate_Oct_Mapping,       &
       Gradation_Factor
  NAMELIST /CosmeticParameters/       &
       Loop_CVT,                      &
  !     CVT_Method,                    &
       CVT_Time_Limit,                &
       Loop_Cosmetic,                 &
       Loop_Smooth,                   &
       Smooth_Method,                 &
       BoundCell_Split,               &
       Collapse_Angle,                &
       Swapping_Angle,                &
       HighOrder_Model
  NAMELIST /BDLayerParameters/        &
       NorSmooth_Method,              &
       Layer_Hybrid
       
  !================================================  other reading parameters
       
  CHARACTER(LEN=512)  :: NameUc, NameUs
  TYPE (IntQueueType), SAVE :: ListUc, ListUs
  !>   FrameBox(1:3)   the minimum corner of frame.
  !!   FrameBox(4:6)   the maximum corner of frame.
  REAL*8 , SAVE :: FrameBox(6)

  !================================================  background parameters

  REAL*8 , SAVE :: XYZmin(3)       !<   The minimum corner of the domain.
  REAL*8 , SAVE :: XYZmax(3)       !<   The maximum corner of the domain.
  REAL*8 , SAVE :: QualityMin(4)
  REAL*8 , SAVE :: QualityMax(4)


  !================================================  error standard

  REAL*8 , SAVE :: TinySize
  REAL*8 , SAVE :: HugeValue
  REAL*8 , SAVE :: SmallVolume
  REAL*8 , SAVE :: IdealVolume
  REAL*8 , SAVE :: TinyVolume

  !================================================  the size of problem

  INTEGER, SAVE :: GridOrder   = 1          !<    the grid-order of the mesh.
  INTEGER, SAVE :: numTetNodes = 4          !<    the number of nodes in a tetrahedron
  INTEGER, SAVE :: nallc_Point              !<    Lenth of allocated array of Point.
  INTEGER, SAVE :: nallc_Tet                !<    Lenth of allocated array of tetrahedra.
  INTEGER, SAVE :: nallc_increase           !<    Lenth of increasement of each call of reAllocate.
  INTEGER, SAVE :: NB_Point                 !<    The number of points.
  INTEGER, SAVE :: NB_Tet                   !<    The number of tettrahedra.

  !>    The number of extra points due to initial hull setting.
  !!               These points are put in front of the boundary nodes
  !!               at first. After completing recovery, these points will be
  !!               removed or moved to after boundary nodes, so NB_Extra_Point
  !!               will be down to zero.
  INTEGER, SAVE :: NB_Extra_Point
  INTEGER, SAVE :: NB_BD_Point      !<    The number of points on boundary.
  INTEGER, SAVE :: NB_BD_Tri        !<    The number of triangles on boundary.
  INTEGER, SAVE :: NB_BD_Edge       !<    The number of edges on the boundary.

  !================================================  connectivties

  !>   IP_Tet(4,NB_Tet) :      connectivities of the tetrahedral elements.
  !!                           If IP_Tet(4,it)<=0, then 'it' is a fake element.
  INTEGER, DIMENSION(:,:),  POINTER :: IP_Tet => null()
  !>   NEXT_Tet(4,NB_Tet)  :   four neighbours elements of a tetrahedron.
  !!                           Be in mind that neighbour NEXT_Tri(i,it) does NOT
  !!                           contain vertex IP_Tet(i,it) for i=1,2,3,4.
  !!                           If NEXT_Tet(i,IT)=-1 then the i-th triangle
  !!                           of element IT is on the boundary.
  INTEGER, DIMENSION(:,:),  POINTER :: NEXT_Tet => null()
  !>   IP_BD_Tri(5,NB_BD_Tri) :    \n
  !!      1st to 3rd values: connectivities of the boundary triangles.
  !!                         The code sorts the order to make the
  !!                         1st value is the smallest one.   \n
  !!      the 4th value:     the element to which the triangle belongs
  !!                         with orientating inside (when output).   \n
  !!      the 5th value:     the surface number (input).
  !!                         If =0, then it is the initail box hull (set).
  INTEGER, DIMENSION(:,:),  POINTER :: IP_BD_Tri => null()
  INTEGER, DIMENSION(:,:),  POINTER :: IP_BD_Edge => null()     !<  (2,NB_BD_Edge) : 2 nodes of a surface edge.
  INTEGER, DIMENSION(:,:),  POINTER :: ITri_BD_Edge => null()   !<  (2,NB_BD_Edge) : 2 adjacent triangles of a surface edge.
  INTEGER, DIMENSION(:,:),  POINTER :: IEdge_BD_Tri => null()   !<  (3,NB_BD_Tri)  : 3 edges of a surface triangle.
  INTEGER, DIMENSION(:  ),  POINTER :: IB_Surf_Seg => null()    !<  (*)            : Surface segment array
  !>  @param  IsolatedSheet  = .true.  : do not treat two elements beside a sheet triangle as neighbours.    \n
!!                           = .false, : ignore sheet surface.
  LOGICAL, SAVE ::  IsolatedSheet  = .FALSE.

  !================================================  tree & association

  !>   Storage of boundary triangle connectivity with search engine.
  TYPE(NodeNetTreeType), SAVE :: Tri_BD_Tree
  !>   Storage of boundary edge connectivity with search engine.
  TYPE(NodeNetTreeType), SAVE :: Edge_BD_Tree

  !>   Association between points and elements,
  !!              used for searching elements adjoining a point.
  TYPE (LinkAssociationType), SAVE :: PointAsso
  !>   Array pointer to point the array of elements adjoining a specified point.
  !!         Obtained by subroutine LinkAssociation_List(IP, List_Length, CellList, PointAsso)
  INTEGER, DIMENSION(:), POINTER :: Its_List => null()
  !>   Effective length of array Its_List.
  !!         Obtained by subroutine LinkAssociation_List(IP, List_Length, CellList, PointAsso)
  INTEGER   ::  List_Length
  !>   Max of List_Length
  INTEGER, PARAMETER :: ChainTotalLength = 10000

  !================================================  flags

  INTEGER, DIMENSION(:),  POINTER :: Mark_Tet => null()          !<   (NB_Tet)
  INTEGER, DIMENSION(:),  POINTER :: Mark_Point => null()        !<   (NB_Point)
  INTEGER, DIMENSION(:),  POINTER :: Mark_BD_Point => null()     !<   (NB_BD_Point)
  INTEGER, DIMENSION(:),  POINTER :: Mark_Surf_Point => null()   !<   (NB_BD_Point)
  INTEGER, DIMENSION(:),  POINTER :: Mark_Vis_Point => null()    !<   (VM%NB_Point)

  !>   Visit_Tet(NB_Tet)  :  used with common_Parameters::VisitCount
  !!                         to identify a element not being or just being visited.
  !!                         Make sure that abs(Visit_Tet(:))<VisitCount for every element
  !!                         when VisitCount is update be function GetVisitCount().
  !!  @param Visit_Tet |Visit_Tet(IT)| < VisitCount : cell IT has not been visited.  \n
  !!                   |Visit_Tet(IT)| = VisitCount : cell IT has been visited.      \n
  !!                   |Visit_Tet(IT)| > VisitCount : error.
  INTEGER, DIMENSION(:),  POINTER :: Visit_Tet => null()
  !>   Visiting counter, increased by function GetVisitCount().
  INTEGER, SAVE :: VisitCount = 0

  !================================================  geometries

  !>   Posit(3,NB_Point)  :      coordinates of each point.
  REAL*8, DIMENSION(:,:),  POINTER :: Posit => null()
  !>   RR_Point(NB_Point) :    imaginal radius of each point,
  !!                           used for calculate element circum-centres.
  REAL*8, DIMENSION(:  ),  POINTER :: RR_Point => null()
  !>   Sphere_Tet(4,NB_Tet) :   the centre (x,y,z) and radius of
  !!                           the circumscribed sphere of each tetrahedron.  \n
  !!         Reminder:  for an anisotropic problem, the centre and the raduis
  !!                    of the circumball are the values in the mapped plane
  !!                    based on the mean stretching of the tetrahedron,
  !!                    rather than the original physical plane.
  REAL*8, DIMENSION(:,:),  POINTER :: Sphere_Tet => null()
  !>   Volume_Tet(NB_Tet)  :    the volume of each tetrahedron.
  REAL*8, DIMENSION(:  ),  POINTER :: Volume_Tet => null()
  !>   Quality_Tet(4, NB_Tet)  :    the quality of each tetrahedron.
  REAL*8,  DIMENSION(:,:), POINTER :: Quality_Tet => null()
  !>  @param CircumUpdated   = .true.  : the circum-sphere for all element has been updated.   \n
  !!                         = .false. : the circum-sphere for all element has NOT been updated.
  LOGICAL, SAVE ::  CircumUpdated  = .FALSE.
  !>  @param  VolumeUpdated  = .true.  : the volume for all element has been updated.      \n
  !!                         = .false. : the volume for all element has NOT been updated.
  LOGICAL, SAVE ::  VolumeUpdated  = .FALSE.

  !================================================  background

  TYPE(SpacingStorageType), SAVE  :: BGSpacing     !<   background mesh and spacing.
  
  REAL*8, DIMENSION(:    ),  POINTER :: Scale_Point => null()   !< (NB_Point)     : grid-size scale on each point.
  REAL*8, DIMENSION(:    ),  POINTER :: Scale_Tet => null()     !< (NB_Tet)       : grid-size scale on each tetrahedron.
  REAL*8, DIMENSION(:,:,:),  POINTER :: fMap_Point => null()    !< (3,3,NB_Point) : stretch mapping on each point.
  REAL*8, DIMENSION(:,:,:),  POINTER :: fMap_Tet => null()      !< (3,3,NB_Tet)   : stretch mapping on each tetrahedron.
  
  !>   @param useStretch  = .true., then the volume and other geometries are
  !!                                calculated in the stretching domain if applied.  \n
  !!                      = .false., then the volume and other geometries are
  !!                                calculated in the free domain.
  LOGICAL, SAVE :: useStretch = .TRUE.

  !>   @param usePower    = .true.,  then apply Delaunay in circle criterion in power diagram.  \n
  !!                      = .false., then apply Delaunay in circle criterion in normal domain.
  LOGICAL, SAVE :: usePower = .FALSE.

  !================================================  ADTree

  TYPE (ADTreeType), SAVE :: ADTreeForMesh

  !================================================  Mesh buckup

  TYPE(HybridMeshStorageType), SAVE  :: VM             !<   Hybrid mesh for viscous layers.
  TYPE(HybridMeshStorageType), SAVE  :: VMHO           !<   High-Order mesh for viscous layers.
  TYPE(TetMeshStorageType), SAVE     :: CVTMeshInit    !<   Initial mesh for CVT loop.
  TYPE(TetMeshStorageType), SAVE     :: CVTFull
  TYPE(TetMeshStorageType), SAVE     :: HullBackUp     !<   The hull with no points
  TYPE(TetMeshStorageType), SAVE     :: TetFrame       !<   Tetrahedral mesh as a frame.
  TYPE(TetMeshStorageType), SAVE     :: FillingNodes   !<   Nodes (with position) used for element-breaking.
  TYPE(HybridMeshStorageType), SAVE  :: HybFrame       !<   Hybrid mesh as a frame.
  TYPE(HybridMeshStorageType), SAVE  :: MeshOut        !<   Mesh for output

  !>   Input surface mesh.   \n
  !!        remind:    variables under Surf% are kept unchange throughout.
  !!                   variables with _BD_ might changed before or after recovery
  !!                   There may has a difference of NB_Extra_Point.
  TYPE(SurfaceMeshStorageType), SAVE :: Surf
  TYPE(SurfaceCurvatureType), SAVE   :: Curvt          !<   Surface curvature.
  INTEGER, SAVE :: NumRegions                     !<   Number of regions.
  INTEGER, SAVE :: NumCurves                      !<   Number of curves.

  !================================================  boundary layer

  !>   Use for viscous layer
  TYPE(SurfaceMeshStorageType), SAVE :: SurfOld
  

  !>   surface piece character
     INTEGER, DIMENSION(  Surf_MaxNB_Surf) :: surfsegm = 0
     INTEGER, DIMENSION(2,Surf_MaxNB_Surf) :: surfmase = 0
  !>  The types of each surface region,
  !!  @param Type_Wall (i) = 1, the surface i is a wall to develop boundary layers;   \n
  !!                   (i) = 2, the surface i is a symmetry plane.    \n
  !!                   (i) = 3, the surface i is far field;    \n
  INTEGER, DIMENSION(10000), SAVE :: Type_Wall
  !>  The number of symmtry planes
  INTEGER, SAVE :: NB_SymPlane
  !>  The surface number of each symmtry plane.
  INTEGER, DIMENSION(100), SAVE :: IS_SymPlane
  !>  The plane of each (symmetry) surface
  TYPE(Plane3D), DIMENSION(100) :: SymPlane
  !>
  !!
  TYPE :: SurfCurve
     REAL*8 :: P0(3)
     REAL*8 :: Dir(3)
     INTEGER :: region1, region2
     INTEGER :: numPieces
     INTEGER :: IPends(2,10)
     TYPE(IntQueueType), DIMENSION(10) :: Ips
  END TYPE SurfCurve
  TYPE(SurfCurve), SAVE :: symLine
  
  !>   for boundary layer
  TYPE(Plane3D) :: OptPlane
  REAL*8 :: OptDirct1(3), OptDirct2(3)
  TYPE(SurfaceNodeType),     DIMENSION(:), POINTER :: SurfNode => null()
  TYPE(SurfaceTriangleType), DIMENSION(:), POINTER :: SurfTri => null()
  !
  TYPE(SurfaceMeshStorageType), SAVE :: HybridSurf

  REAL*8, DIMENSION(:,:), POINTER :: RRS_POD => null()
  INTEGER, SAVE :: NB_POD   = 0
  INTEGER, SAVE :: NB_PODva = 0

  !================================================  elasticity modify 
  TYPE(MaterialPar), SAVE :: Material

  !====================Medial Axis Parameters
  INTEGER, SAVE :: medSurf1, medSurf2 
  INTEGER, SAVE :: medNumSamp, medOrder
  REAL*8, SAVE  :: medError,medSpace
  
  CHARACTER(LEN=1000) :: ErrorMessage
  

CONTAINS

  !>
  !!  Increasing common_Parameters::VisitCount by 1 every time
  !!    and return the value.
  !<
  FUNCTION GetVisitCount() RESULT (ThisVisit)
    IMPLICIT NONE
    INTEGER :: ThisVisit
    VisitCount = VisitCount + 1
    IF(VisitCount > 10000000) VisitCount = 1
    IF(VisitCount==1) Visit_Tet(1:nallc_Tet) = 0
    ThisVisit = VisitCount
  END FUNCTION GetVisitCount


END MODULE common_Parameters

!*******************************************************************************
!
!*******************************************************************************

MODULE Pole_Surround
  IMPLICIT NONE

  INTEGER, PARAMETER :: Max_PoleTet = 100000
  INTEGER, SAVE :: NB_PoleTet, NB_PolePt
  INTEGER, SAVE :: IT_PoleTet(Max_PoleTet), IP_PolePt(Max_PoleTet+1)

END MODULE Pole_Surround


