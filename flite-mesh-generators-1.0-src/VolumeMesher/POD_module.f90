!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!------------------------------------
!------------------------------------

! Module definitions file for POD implimentation in fortran.
! Use dsvdc.f90 for SVD
! Written by S.Walton 2011
! Version 1.0 - 29th June 2011 Initial implimentation

!-----------------------------------
!-----------------------------------

MODULE pod_module

	!Defines data structure to be used for POD and subroutines to be used
	
	use SVD
	
	type PodData
		integer :: n !Number of dimensions of the full order data
		integer :: p !Number of snapshots
		double precision, pointer :: x(:,:) !Snapshot matrix (n,p)
		double precision, pointer :: u(:,:) !POD Modes (n,p)
		double precision, pointer :: c(:,:) !Coefficients (p,p)
		double precision, pointer :: s(:) !Singular values (min(n+1,p))
	end type PodData
	
	contains
	
	!---------------------------------
	subroutine allocatePodData(pod)
		!Allocates x,u,c given n and p
		IMPLICIT NONE
		
		type(PodData) :: pod
		
		integer :: allocateStatus, MM
		
		allocate(pod%x(pod%n,pod%p),stat=allocateStatus)
   		if(allocateStatus/=0) STOP "ERROR: Not enough memory in allocatePodData, allocating x"
   		
   		allocate(pod%u(pod%n,pod%p),stat=allocateStatus)
   		if(allocateStatus/=0) STOP "ERROR: Not enough memory in allocatePodData, allocating u"
   		
   		allocate(pod%c(pod%n,pod%p),stat=allocateStatus)
   		if(allocateStatus/=0) STOP "ERROR: Not enough memory in allocatePodData, allocating c"
   		
   		MM = min(pod%n+1,pod%p)
   		allocate(pod%s(MM),stat=allocateStatus)
   		if(allocateStatus/=0) STOP "ERROR: Not enough memory in allocatePodData, allocating s"
   		
   	end subroutine allocatePodData
	!----------------------------------
	subroutine build_POD(pod)
		!Performs SVD then calls routine to calculate coefficients for each snapshot
		IMPLICIT NONE
		
		type(PodData) :: pod
		
		integer :: allocateStatus,job=21,info
		double precision :: e(pod%p),v(pod%n,pod%p), xx(pod%n,pod%p) !Redundant information for dsvdc
		
		xx = pod%x !Needs to be done because dsvdc will destroy xx
		
		! Perform svd
		call dsvdc(xx, pod%n, pod%p, pod%s, e, pod%u, v, job, info)
		
		!Calculate coefficients
		call calc_coef(pod)
		
		
	end subroutine build_POD
	!-----------------------------------
	subroutine calc_coef(pod)
		!Calculates coefficient matrix
		IMPLICIT NONE
		
		type(PodData) :: pod
		
		integer :: i,j

		!Loop over each snapshot
		DO i=1,pod%p
			!Loop over each mode
			DO j=1,pod%p
				pod%c(i,j) = DOT_PRODUCT(pod%u(1:pod%n,j),pod%x(1:pod%n,i))
			END DO
		END DO
	end subroutine calc_coef
	!------------------------------------
	subroutine reconstruct(pod,newx,coef)
		!Reconstructs solution from pod and coefficients, outputs to coef
		IMPLICIT NONE
		
		type(PodData) :: pod
		double precision :: coef(pod%p), newx(pod%n)
		double precision :: helpArray(pod%p)
		
		integer :: i
		
		DO i = 1,pod%n
			helpArray(1:pod%p) = coef(1:pod%p)*pod%u(i,1:pod%p)	
			newx(i) = sum(helpArray)
		END DO
	end subroutine reconstruct
	!-----------------------------------
	subroutine deallocatePodData(pod)
		!Allocates x,u,c given n and p
		IMPLICIT NONE
		
		type(PodData) :: pod
		
		
		
		deallocate(pod%x,pod%u,pod%c,pod%s)
   		
   	end subroutine deallocatePodData
	
	
END MODULE pod_module

!-----------------------------------
!-----------------------------------


