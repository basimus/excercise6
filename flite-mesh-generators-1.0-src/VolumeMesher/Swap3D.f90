!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!   Sellection of possible triangulations for polygons.
!!   Used while swapping a edge.
!<
MODULE SwapConnect
  IMPLICIT NONE

  !>  npos(n) : the number of possible different triangles connecting
  !!            the nodes of a (n+2)-side polygon.
  INTEGER, PARAMETER :: npos(4) = (/1, 4, 10, 20/)

  !>  nce(:,i,n) : the 3 nodes of the i-th triangle out of
  !!               the total SwapConnect::npos (n) triangles
  !!               for a (n+2)-side polygon.
  INTEGER, PARAMETER :: nce(3,20,4) = reshape( (/                    &
         1,2,3  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,        &
         0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,        &
         0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,        &
         0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0       ,   &
         2,4,1  ,   2,3,4  ,   3,1,2  ,   1,3,4  ,   0,0,0  ,        &
         0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,        &
         0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,        &
         0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0       ,   &
         3,1,2  ,   4,1,3  ,   1,4,5  ,   2,5,1  ,   2,4,5  ,        &
         2,3,4  ,   3,5,1  ,   5,3,4  ,   2,4,1  ,   5,2,3  ,        &
         0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,        &
         0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0  ,   0,0,0       ,   &
         3,1,2  ,   4,1,3  ,   5,1,4  ,   1,5,6  ,   2,6,1  ,        &
         2,5,6  ,   2,4,5  ,   4,2,3  ,   3,6,1  ,   3,5,6  ,        &
         5,3,4  ,   6,4,5  ,   1,4,6  ,   4,1,2  ,   2,5,1  ,        &
         3,5,2  ,   3,6,2  ,   4,6,3  ,   1,3,5  ,   2,4,6      /),  &
         (/3,20,4/) )

  !>  mpos(n) : the number of possible triangulations by spliting
  !!            a (n+2)-side polygon.
  INTEGER, PARAMETER :: mpos(4) = (/1, 2, 5,  14/)

  !>  ncp(:,j,n) : the triangles composing the j-th triangulation.
  !!               (j<= SwapConnect::mpos (n) for a (n+2)-side polygon.)
  INTEGER, PARAMETER :: ncp(4,14,4) = reshape( (/                    &
         1,0,0,0  ,   0,0,0,0  ,   0,0,0,0  ,   0,0,0,0  ,           &
         0,0,0,0  ,   0,0,0,0  ,   0,0,0,0  ,   0,0,0,0  ,           &
         0,0,0,0  ,   0,0,0,0  ,   0,0,0,0  ,   0,0,0,0  ,           &
         0,0,0,0  ,   0,0,0,0        ,                               &
         1,2,0,0  ,   3,4,0,0  ,   0,0,0,0  ,   0,0,0,0  ,           &
         0,0,0,0  ,   0,0,0,0  ,   0,0,0,0  ,   0,0,0,0  ,           &
         0,0,0,0  ,   0,0,0,0  ,   0,0,0,0  ,   0,0,0,0  ,           &
         0,0,0,0  ,   0,0,0,0        ,                               &
         1,2,3,0  ,   4,5,6,0  ,   1,7,8,0  ,   3,9,6,0  ,           &
         4,8,10,0  ,   0,0,0,0  ,   0,0,0,0  ,   0,0,0,0  ,          &
         0,0,0,0  ,   0,0,0,0  ,   0,0,0,0  ,   0,0,0,0  ,           &
         0,0,0,0  ,   0,0,0,0        ,                               &
         1,2,3,4  ,   5,6,7,8  ,   1,9,10,11  ,   8,12,13,14  ,      &
         4,15,16,11  ,   5,17,18,12  ,   1,2,12,13  ,   4,15,7,8  ,  &
         5,17,10,11  ,   1,4,11,19  ,   5,12,8,20  ,   4,8,14,3  ,   &
         5,6,16,11  ,   1,9,18,12                               /),  &
        (/4,14,4/) )

END MODULE SwapConnect

!*******************************************************************************
!>
!!   Swap edges to get tetrahedra with better quality   
!!       (in stretch / non-stretch domain whichever applied).
!!   @param[in]  Kswap  = 1,   use volume as quality standard.                \n
!!                      = 2,   use the minimum dihedral angle.
!!   @param[in]  common_Parameters::Swapping_Angle  refered when Kswap=2.
!!
!!   Reminder:                                                                \n
!!     common_Parameters::NEXT_Tet is not in need in this subroutine
!!               and will be ruined aftermath.                                \n
!!     common_Parameters::PointAsso will be built and kept updated in
!!               the process but deleted at final due to elements recounting.
!<
!*******************************************************************************
SUBROUTINE Swap3D(Kswap)

  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  USE array_allocator
  USE SwapConnect
  IMPLICIT NONE

  INTEGER, INTENT(INOUT) :: Kswap
  INTEGER :: IP_Chain(2,6), IT_Cavity(6)
  REAL*8  :: dihn(2,1000)
  INTEGER, DIMENSION(:  ), POINTER :: led
  INTEGER, DIMENSION(:  ), POINTER :: lsde
  INTEGER, DIMENSION(:  ), POINTER :: lmp
  INTEGER, DIMENSION(:  ), POINTER :: lms
  INTEGER, DIMENSION(:,:), POINTER :: IP_Edge   !--- (2,:)

  INTEGER :: Mside, mled, mlsd, madd
  INTEGER :: ipp(4), jpp(2), ip1, ip2, iqq(3)
  INTEGER :: IT, IP, IB, IS, itri, iis, istore, NchainSeg
  INTEGER :: ndiv, idiv, inp, jnp, knp, ILoop, kUnCheck
  INTEGER :: i, iside,  ichain, jside
  INTEGER :: iie, ntry, ir, Isucc
  INTEGER :: NB_Edge, NB_Edge_update, NB_Edge_Swap, NB_Edge_deleted
  INTEGER :: NB_Tet_update, NB_Tet_deleted

  REAL *8 :: p1(3),p2(3),p3(3),p4(3)
  REAL *8 :: fMap(3,3), fMaps(3,3,4), Scalars(4)
  REAL *8 :: swapping_angle_idv, did, dihmin, dihmax, Scalar
  LOGICAL :: negExist

  Mside = NB_Tet+2*(NB_Point+NB_BD_Tri)
  mled  = 1000
  mlsd  = 1000
  madd  = 1000

  ALLOCATE ( lmp(NB_Point) )          !--- mark of points
  ALLOCATE ( lms(Mside) )             !--- mark of edges
  ALLOCATE ( led(mled) )              !--- store deleted elements
  ALLOCATE ( lsde(mlsd) )             !--- store deleted edges
  ALLOCATE ( IP_Edge(2, Mside) )      !--- edge connectivity

  !--- construct boundary data structure

  CALL Boundary_Associate(1)

  !--- compute dihedral angles for each element

  IF(Kswap==1)THEN
     IF(.NOT. VolumeUpdated) CALL Get_Tet_Volume(0)
     Quality_Tet(1,1:NB_Tet) = Volume_Tet(1:NB_Tet)
  ELSE
     DO IT = 1, NB_Tet
        p1(:)   = Posit(:,IP_Tet(1,IT))
        p2(:)   = Posit(:,IP_Tet(2,IT))
        p3(:)   = Posit(:,IP_Tet(3,IT))
        p4(:)   = Posit(:,IP_Tet(4,IT))
        IF(useStretch .AND. BGSpacing%Model<0)THEN
           IF(BGSpacing%Model==-1)THEN
              fMap(:,:) = BGSpacing%BasicMap(:,:)
           ELSE
              fMap(:,:) = fMap_Tet(:,:,IT)
           ENDIF
           p1(:)   = Mapping3D_Posit_Transf(p1,fMap,1)
           p2(:)   = Mapping3D_Posit_Transf(p2,fMap,1)
           p3(:)   = Mapping3D_Posit_Transf(p3,fMap,1)
           p4(:)   = Mapping3D_Posit_Transf(p4,fMap,1)
        ELSE IF(useStretch .AND. BGSpacing%Model>1)THEN
           p1(:)   = p1 / Scale_Tet(IT)
           p2(:)   = p2 / Scale_Tet(IT)
           p3(:)   = p3 / Scale_Tet(IT)
           p4(:)   = p4 / Scale_Tet(IT)
        ENDIF

        Quality_Tet(1,IT) = Geo3D_Tet_Quality(p1,p2,p3,p4,2)
        IF(Quality_Tet(1,IT)<0.d0) Quality_Tet(1,IT) = 0.d0
     ENDDO
  ENDIF

  negExist = .FALSE.
  DO IT = 1, NB_Tet
     IF(Quality_Tet(1,IT)<=0.d0)THEN
        negExist = .TRUE.
        EXIT
     ENDIF
  ENDDO

  CircumUpdated = .FALSE.      !--- geometry of the grids will be lost
  VolumeUpdated = .FALSE.      !--- volume   of the grids will be lost

  !--- compute edge data structure

  CALL Build_Edge(IP_Edge,NB_Edge,Mside)
  NB_Edge_update = NB_Edge

  !--- Associate points with elements
  CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)

  NB_Tet_deleted  = 0
  NB_Tet_update   = NB_Tet
  NB_Edge_deleted = 0
  IF(Kswap==1)THEN
     ndiv = 1
  ELSE
     Swapping_Angle_idv = 14.d0*PI/180.d0
     ndiv = MAX( 1, INT(Swapping_Angle / Swapping_Angle_idv) )
  ENDIF

  !---- loop over number of divisions

  Loop_idv : DO idiv = 1 , ndiv

     IF(Kswap==1)THEN
        Swapping_Angle_idv = HugeValue
     ELSE
        Swapping_Angle_idv = Swapping_Angle / ndiv * idiv
     ENDIF
     lms(1:NB_Edge_update) = 0
     lmp(1:NB_Point)       = 0
     Mark_Tet(1:NB_Tet_update)  = 1

     Loop_ILoop : DO ILoop = 1,7

        DO IS = 1, NB_Edge_update
           ip1 = IP_Edge(1,IS)
           ip2 = IP_Edge(2,IS)
           IF(ip1/=0 .AND. ip2/=0) THEN
              IF(lmp(ip1)<0 .OR. lmp(ip2)<0)  lms(IS) = 0
           ENDIF
        ENDDO
        DO IP = 1 , NB_Point
           IF(lmp(IP)<0) lmp(IP) = 0
        ENDDO

        !--- loop over the edges, skip the first NB_BD_Edge boundary edge

        NB_Edge      = NB_Edge_update
        ntry         = 0
        NB_Edge_Swap = 0
        Loop_iis : DO iis = NB_BD_Edge+1, NB_Edge

           !--- check that edge still exists and not boundary edge

           ip1 = IP_Edge(1,iis)
           ip2 = IP_Edge(2,iis)
           IF(ip1==0 .AND. ip2==0) CYCLE Loop_iis
           IF(lms(iis)==1 .AND. lmp(ip1)==1 .AND. lmp(ip2)==1)CYCLE Loop_iis
           IF(ip1<=NB_Extra_Point)THEN
              !--- the extra points hace a large number of links, 
              !    so don't use them to search the change.
              ip1 = IP_Edge(2,iis)
              ip2 = IP_Edge(1,iis)
           ENDIF

           IF(Debug_Display>3)THEN
              IF(ip1<=NB_BD_Point .AND. ip2<=NB_BD_Point) THEN
                 !-- check the edge not a boundary edge
                 CALL Boundary_Edg_Match(ip1,ip2,IS)
                 IF(IS>0)THEN
                    WRITE(29,*)'Error---: a boundary edge found'
                    WRITE(29,*)'iis,NB_BD_Edge,is=',iis,NB_BD_Edge, IS
                    WRITE(29,*)'ip1,ip2=', ip1,ip2
                    
                    !CALL Error_Stop ('Swap3D:: boundary edge')
                     Kswap = -1
                        RETURN
                 ENDIF
              ENDIF
           ENDIF

           !--- find number of edges surrounding it

           did       = HugeValue   !-- the minimum quality
           NchainSeg = 0           !-- no. of chain segments surrounding axis ip1-ip2
           kUncheck  = 0           !-- if =0, then all surrounding elements checked

           !--- List all elements surrounding point ip1 in the array ITs_List
           CALL LinkAssociation_List(ip1, List_Length, ITs_List, PointAsso)

           Loop_iie : DO iie = 1 , List_Length
              IT = ITs_List(iie)

              IF(  IP_Tet(1,IT)/=ip2 .AND. IP_Tet(2,IT)/=ip2 .AND.   &
                   IP_Tet(3,IT)/=ip2 .AND. IP_Tet(4,IT)/=ip2) CYCLE Loop_iie

              !--- if some elements have been distroyed then try latter

              did  = MIN(did,Quality_Tet(1,IT))
              NchainSeg  = NchainSeg + 1

              !--- Do not swap if more than 5 segments found
              IF(NchainSeg>5) THEN
                 lms(iis) = 1
                 IF(lmp(ip1)==0) lmp(ip1) = 1
                 IF(lmp(ip2)==0) lmp(ip2) = 1
                 CYCLE Loop_iis
              ENDIF

              IT_Cavity(NchainSeg)  = IT
              IF(Mark_Tet(IT)>=ILoop) kUnCheck = 1
              DO iside = 1 , 12
                 IF(  IP_Tet(I_Comb_Tet(1,iside),IT)==ip1 .AND.   &
                      IP_Tet(I_Comb_Tet(2,iside),IT)==ip2  ) THEN
                    IP_Chain(1,NchainSeg) = IP_Tet(I_Comb_Tet(3,iside),IT)
                    IP_Chain(2,NchainSeg) = IP_Tet(I_Comb_Tet(4,iside),IT)
                    CYCLE Loop_iie
                 ENDIF
              ENDDO

              !WRITE(29,*)'Error---: chain missed--'
              !CALL Error_Stop ('Swap3D:: chain missed')
               Kswap = -1
                        RETURN
           ENDDO Loop_iie

           IF(NchainSeg<=2)THEN
              !--- an edge only belongs two elements.
              IF(NchainSeg==1) THEN
                Kswap = -1
                        RETURN
              END IF
              IF((.NOT.negExist) .AND. NchainSeg==0) THEN
                Kswap = -1
                        RETURN
              END IF
              IF(NchainSeg==2)THEN
                 IF(Debug_Display>1)THEN
                    WRITE(*, *)'Warning--- Swap3D:: flat or negative element?'
                    WRITE(29,*)'Warning--- Swap3D:: flat or negative element?'
                    WRITE(29,*)'idiv,iLoop=',idiv,iLoop
                    WRITE(29,*)'iis=',iis,'ip1,ip2=',ip1,ip2
                    WRITE(29,*)'NchainSeg=',NchainSeg, ' ITs=',IT_Cavity(1:NchainSeg)
                    WRITE(29,*)'chain=',IP_Chain(:,1), IP_Chain(:,2),' did=', did
                 ENDIF
                 IF(IP_Chain(2,1)/=IP_Chain(1,2) .OR. IP_Chain(2,2)/=IP_Chain(1,1))THEN
                   ! WRITE(29,*)'Error--- : Broken flat chain: iis=',iis,'ip1,ip2=',ip1,ip2
                   ! CALL Error_Stop ('Swap3D:: Broken flat chain')
                   Kswap = -1
                        RETURN
                        
                 ENDIF
              ENDIF

              !--- remove two elements.
              DO ichain = 1, NchainSeg
                 IT      = IT_Cavity(ichain)
                 NB_Tet_deleted = NB_Tet_deleted + 1
                 IF(NB_Tet_deleted>mled) THEN
                    mled = mled + madd
                    CALL allc_1Dpointer ( led,  mled,     'led' )
                 ENDIF
                 led(NB_Tet_deleted)  = IT
                 CALL LinkAssociation_Remove(4, IT,IP_Tet(:,IT),PointAsso)
              ENDDO

              !---  remove edge
              NB_Edge_deleted = NB_Edge_deleted + 1
              IF(NB_Edge_deleted>mlsd) THEN
                 mlsd = mlsd + madd
                 CALL allc_1Dpointer ( lsde, mlsd,     'lsde')
              ENDIF
              lsde(NB_Edge_deleted) = iis
              IP_Edge(1:2,iis)      = 0
              lmp(ip1) = -1
              lmp(ip2) = -1
              NB_Edge_Swap = NB_Edge_Swap + 1

              CYCLE Loop_iis
           ENDIF

           !--- Do not swap if the minimum quality is large
           !--- or all has been checked.
           IF(kUnCheck==0 .OR. did>=Swapping_Angle_idv) THEN
              lms(iis) = 1
              IF(lmp(ip1)==0) lmp(ip1) = 1
              IF(lmp(ip2)==0) lmp(ip2) = 1
              CYCLE Loop_iis
           ENDIF

           !--- start a new try to swap
           ntry   = ntry + 1

           !--- sort IP_Chain to a chain, ie. IP_Chain(1,is+1)=IP_Chain(2,is)

           ipp(1:2)     = IP_Chain(1:2,1)
           Loop_ichain : DO ichain = 2,NchainSeg-1
              DO iside = ichain , NchainSeg
                 IF(IP_Chain(1,iside)==ipp(2)) THEN
                    jpp(1:2)         = IP_Chain(1:2,ichain)
                    IP_Chain(1:2,ichain) = IP_Chain(1:2,iside)
                    IP_Chain(1:2,iside)  = jpp(1:2)
                    ipp(2)           = IP_Chain(2,ichain)
                    CYCLE Loop_ichain
                 ENDIF
              ENDDO
              EXIT  Loop_ichain
           ENDDO Loop_ichain

           IF(ichain/=NchainSeg .OR. IP_Chain(2,NchainSeg)/=IP_Chain(1,1))THEN
              WRITE(*, *)'Error--- : Broken chain: iis=',iis,'ip1,ip2=',ip1,ip2
              WRITE(29,*)'Error--- : Broken chain: iis=',iis,'ip1,ip2=',ip1,ip2
              WRITE(29,*)'idiv,iLoop=',idiv,iLoop
              WRITE(29,*)'ichain, iside, Nsur=',ichain,iside,NchainSeg
              WRITE(29,*)'chain=',IP_Chain(:,1:NchainSeg)
              !CALL Error_Stop ('Swap3D::  Broken chain')
              Kswap = -1
              RETURN
                         
           ENDIF

           !--- Check qulity for all possible new elements

           DO itri = 1 , npos(NchainSeg-2)
              DO istore = 1, 2

                 !--- istore=1 : store the top element
                 !--- istore=2 : store the bottom element

                 ipp(1) = IP_Chain(1,nce(1,itri,NchainSeg-2))
                 IF(istore==1)THEN
                    ipp(2) = IP_Chain(1,nce(2,itri,NchainSeg-2))
                    ipp(3) = IP_Chain(1,nce(3,itri,NchainSeg-2))
                    ipp(4) = ip2
                 ELSE
                    ipp(2) = IP_Chain(1,nce(3,itri,NchainSeg-2))
                    ipp(3) = IP_Chain(1,nce(2,itri,NchainSeg-2))
                    ipp(4) = ip1
                 ENDIF

				 
                 p1(:) = Posit(:,ipp(1))
                 p2(:) = Posit(:,ipp(2))
                 p3(:) = Posit(:,ipp(3))
                 p4(:) = Posit(:,ipp(4))
                 IF(useStretch .AND. BGSpacing%Model<0)THEN
                    IF(BGSpacing%Model==-1)THEN
                       fMap(:,:) = BGSpacing%BasicMap(:,:)
                    ELSE
                       fMaps(:,:,1) = fMap_Point(:,:,ipp(1))
                       fMaps(:,:,2) = fMap_Point(:,:,ipp(2))
                       fMaps(:,:,3) = fMap_Point(:,:,ipp(3))
                       fMaps(:,:,4) = fMap_Point(:,:,ipp(4))
                       fMap  = Mapping3D_Mean(4,fMaps,Mapping_Interp_Model)
                    ENDIF
                    p1(:) = Mapping3D_Posit_Transf(p1,fMap,1)
                    p2(:) = Mapping3D_Posit_Transf(p2,fMap,1)
                    p3(:) = Mapping3D_Posit_Transf(p3,fMap,1)
                    p4(:) = Mapping3D_Posit_Transf(p4,fMap,1)
                 ELSE IF(useStretch .AND. BGSpacing%Model>1)THEN
                    Scalars(1:4) = Scale_Point(ipp(1:4))
                    Scalar = Scalar_Mean(4,Scalars,Mapping_Interp_Model)
                    p1(:)  = p1 / Scalar
                    p2(:)  = p2 / Scalar
                    p3(:)  = p3 / Scalar
                    p4(:)  = p4 / Scalar
                 ENDIF

                 dihn(istore,itri) = Geo3D_Tet_Quality(p1,p2,p3,p4,Kswap)

                 !-- if the face is a boundary face then pass
                 IF(dihn(istore,itri)>0) THEN
                    !-- test if the triangle is a boundary face
                    IF(  MAX(ipp(1),ipp(2),ipp(3))<=NB_BD_Point  ) THEN
                       CALL Boundary_Tri_Match(ipp(1),ipp(2),ipp(3),IB)
                       IF(IB>0) dihn(istore,itri) = 0   !--- boundary tri.
                    ENDIF
                 ELSE
                    dihn(istore,itri) = 0
                 ENDIF

              ENDDO
           ENDDO     !--- loop itri

           !--- find the best configuration
           !--- dihmax : the minimum angle from the best configuration

           dihmax = -HugeValue
           DO inp = 1 , mpos(NchainSeg-2)
              dihmin = HugeValue
              DO jnp =  1 , NchainSeg-2
                 itri   = ncp(jnp,inp,NchainSeg-2)
                 dihmin = MIN(dihmin,dihn(1,itri),dihn(2,itri))
              ENDDO
              IF(dihmin>dihmax) THEN
                 knp    = inp
                 dihmax = dihmin
              ENDIF
           ENDDO

           !--- skip if the best configuration worse than the original

           IF( dihmax<=did ) THEN
              lms(iis) = 1
              IF(lmp(ip1)==0) lmp(ip1) = 1
              IF(lmp(ip2)==0) lmp(ip2) = 1
              CYCLE Loop_iis
           ENDIF

           !--- Do swapping now
           !--- NB_Tet_deleted  : the no. of deleted elements to be removed
           !--- led(:)          : the list of deleted elements to be removed

           DO ichain = 1, NchainSeg
              IT      = IT_Cavity(ichain)
              NB_Tet_deleted = NB_Tet_deleted + 1
              IF(NB_Tet_deleted>mled) THEN
                 mled = mled + madd
                 CALL allc_1Dpointer ( led,  mled,     'led' )
              ENDIF
              led(NB_Tet_deleted)  = IT
              CALL LinkAssociation_Remove(4, IT,IP_Tet(:,IT),PointAsso)
           ENDDO

           !--- store new elements

           DO ichain = 1 , NchainSeg-2
              itri = ncp(ichain,knp,NchainSeg-2)

              Loop_istore : DO istore = 1, 2

                 ipp(1) = IP_Chain(1,nce(1,itri,NchainSeg-2))
                 IF(istore==1)THEN
                    ipp(2) = IP_Chain(1,nce(2,itri,NchainSeg-2))
                    ipp(3) = IP_Chain(1,nce(3,itri,NchainSeg-2))
                    ipp(4) = ip2
                 ELSE
                    ipp(2) = IP_Chain(1,nce(3,itri,NchainSeg-2))
                    ipp(3) = IP_Chain(1,nce(2,itri,NchainSeg-2))
                    ipp(4) = ip1
                 ENDIF

                 IF(negExist)THEN
                    !--- the new element may existed as a negative-volume element.
                    CALL LinkAssociation_List(ipp(4), List_Length, ITs_List, PointAsso)
                    DO iie = 1 , List_Length
                       IT = ITs_List(iie)
                       inp = which_nodeinTet(ipp(4), IP_Tet(:,IT))
                       iqq(1:3) = IP_Tet(iTri_Tet(1:3,inp), IT)
                       inp = which_RotationinTri(ipp(1:3), iqq(1:3))
                       IF(inp==0) CYCLE
                       IF(inp>0) THEN
                        Kswap = -1
                        RETURN
                       ENDIF                 

                       !--- remove two elements.
                       NB_Tet_deleted = NB_Tet_deleted + 1
                       IF(NB_Tet_deleted>mled) THEN
                          mled = mled + madd
                          CALL allc_1Dpointer ( led,  mled,     'led' )
                       ENDIF
                       led(NB_Tet_deleted)  = IT
                       CALL LinkAssociation_Remove(4, IT,IP_Tet(:,IT),PointAsso)

                       CYCLE Loop_istore
                    ENDDO
                 ENDIF

                 IF(NB_Tet_deleted>0) THEN
                    IT = led(NB_Tet_deleted)
                    NB_Tet_deleted = NB_Tet_deleted - 1
                 ELSE
                    NB_Tet_update   = NB_Tet_update + 1
                    IF(NB_Tet_update>nallc_Tet) THEN
                       CALL ReAllocate_Tet( )
                    ENDIF
                    IT = NB_Tet_update
                 ENDIF
                 IP_Tet(1:4,IT) = ipp(1:4)
                 Mark_Tet( IT ) = ILoop + 1
                 Quality_Tet(1, IT ) = dihn( istore,itri )
                 IF(useStretch .AND. ABS(BGSpacing%Model)>1)THEN
                    CALL Get_Tet_Mapping(IT)
                 ENDIF

                 !---  update data structure
                 CALL LinkAssociation_AddCell(4, IT,IP_Tet(:,IT),PointAsso)

              ENDDO Loop_istore
           ENDDO

           !---  update side data structure, remove the old one
           NB_Edge_deleted = NB_Edge_deleted + 1
           IF(NB_Edge_deleted>mlsd) THEN
              mlsd = mlsd + madd
              CALL allc_1Dpointer ( lsde, mlsd,     'lsde')
           ENDIF
           lsde(NB_Edge_deleted) = iis
           IP_Edge(1:2,iis)      = 0
           lmp(ip1) = -1
           lmp(ip2) = -1
           NB_Edge_Swap = NB_Edge_Swap + 1

           !---  update side data structure, add new edges
           DO iside = 1, NchainSeg-3
              itri = ncp(iside,knp,NchainSeg-2)
              jpp(1:2) = IP_Chain(1,nce(1:2,itri,NchainSeg-2))
              lmp(jpp(1:2)) = -1

              IF(NB_Edge_deleted==0) THEN
                 !-- create an edge
                 NB_Edge_update = NB_Edge_update + 1
                 IF(NB_Edge_update>Mside) THEN
                    Mside = Mside + madd
                    CALL allc_1Dpointer ( lms,  Mside,    'lms' )
                    CALL allc_2Dpointer ( IP_Edge, 2, Mside, 'IP_Edge')
                 ENDIF
                 jside   = NB_Edge_update
              ELSE
                 !-- replace an edge
                 jside           = lsde(NB_Edge_deleted)
                 NB_Edge_deleted = NB_Edge_deleted - 1
              ENDIF
              lms(jside) = 0
              IP_Edge(1:2,jside) = jpp(1:2)

              IF(MAX(jpp(1),jpp(2))<=NB_BD_Point+NB_Extra_Point) THEN
                 !--- check if a new boundary edge appear
                 !    this is possible when the subrutine is called from Boundary_Insert
                 CALL Boundary_Edg_Match(jpp(1),jpp(2),IS)
                 IF(IS>0)  IP_Edge(1:2,jside) = 0
              ENDIF
           ENDDO

        ENDDO Loop_iis

        WRITE(29,'(a,i2,3(a,i8))') ' Loop: ',iLoop,'  NB_Edge_Swap = ',NB_Edge_Swap,   &
             ' nfail =',ntry-NB_Edge_Swap,   ' nelem =',NB_Tet_update-NB_Tet_deleted

        IF(NB_Edge_Swap<=1) EXIT Loop_ILoop
     ENDDO Loop_ILoop
  ENDDO Loop_idv

  !---  renumber the elements

  NB_Tet = NB_Tet_update
  DO iie = 1, NB_Tet_deleted
     IP_Tet(4,led(iie)) = 0
  ENDDO
  DO iie = 1, NB_Tet_deleted
     IT  = led(iie)
     DO WHILE(IP_Tet(4,NB_Tet)==0)
        NB_Tet = NB_Tet - 1
     ENDDO
     IF(IT<NB_Tet) THEN
        IP_Tet(1,IT) = IP_Tet(1,NB_Tet)
        IP_Tet(2,IT) = IP_Tet(2,NB_Tet)
        IP_Tet(3,IT) = IP_Tet(3,NB_Tet)
        IP_Tet(4,IT) = IP_Tet(4,NB_Tet)
        IF(BGSpacing%Model<-1)THEN
           fMap_Tet(:,:,IT) = fMap_Tet(:,:,NB_Tet)
        ELSE IF(BGSpacing%Model>1)THEN
           Scale_Tet(IT) = Scale_Tet(NB_Tet)
        ENDIF
        NB_Tet = NB_Tet - 1
     ENDIF
  ENDDO

  DEALLOCATE ( lmp, lms, IP_Edge, led, lsde )
  CALL LinkAssociation_Clear(PointAsso)


  RETURN
END SUBROUTINE Swap3d

!*******************************************************************************
!>
!!   Swap edges to increase the number of boundary edges.
!!   Only apllied before recovery.                                            \n
!!   Reminder:                                                                \n
!!     common_Parameters::NEXT_Tet is not in need in this subroutine
!!               and will be ruined aftermath.                                \n
!!     common_Parameters::PointAsso will be built and kept updated in
!!               the process but deleted at final due to elements recounting.
!<
!*******************************************************************************
SUBROUTINE Swap3D_BD( )

  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  USE array_allocator
  USE SwapConnect
  IMPLICIT NONE

  INTEGER :: IP_Chain(2,6), IT_Cavity(6)
  REAL*8  :: dihn(2,1000)
  INTEGER, DIMENSION(:  ), POINTER :: led
  INTEGER, DIMENSION(:  ), POINTER :: lsde
  INTEGER, DIMENSION(:  ), POINTER :: lmp
  INTEGER, DIMENSION(:  ), POINTER :: lms
  INTEGER, DIMENSION(:,:), POINTER :: IP_Edge   !--- (2,:)

  INTEGER :: Mside, mled, mlsd, madd
  INTEGER :: ipp(4), jpp(2), ip1, ip2
  INTEGER :: IT, IP, IB, IS, itri, iis, istore, NchainSeg
  INTEGER :: inp, jnp, knp, ILoop, kUnCheck
  INTEGER :: i, iside,  ichain, jside
  INTEGER :: iie, ntry, ir, Isucc
  INTEGER :: NB_Edge, NB_Edge_update, NB_Edge_Swap, NB_Edge_deleted
  INTEGER :: NB_Tet_update, NB_Tet_deleted, NB_newBD, NB_newMax

  REAL *8 :: p1(3),p2(3),p3(3),p4(3)
  REAL *8 :: did, dihmin, dihmax

  Mside = NB_Tet+2*(NB_Point+NB_BD_Tri)
  mled  = 1000
  mlsd  = 1000
  madd  = 1000

  ALLOCATE ( lmp(NB_Point) )          !--- mark of points
  ALLOCATE ( lms(Mside) )             !--- mark of edges
  ALLOCATE ( led(mled) )              !--- store deleted elements
  ALLOCATE ( lsde(mlsd) )             !--- store deleted edges
  ALLOCATE ( IP_Edge(2, Mside) )      !--- edge connectivity

  !--- construct boundary data structure
  CALL Boundary_Associate(1)

  IF(.NOT. VolumeUpdated) CALL Get_Tet_Volume(0)
  Quality_Tet(1,1:NB_Tet) = Volume_Tet(1:NB_Tet)

  CircumUpdated = .FALSE.      !--- geometry of the grids will be lost
  VolumeUpdated = .FALSE.      !--- volume   of the grids will be lost


  !--- compute edge data structure

  CALL Build_Edge(IP_Edge,NB_Edge,Mside)
  NB_Edge_update = NB_Edge

  !--- Associate points with elements
  CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)

  NB_Tet_deleted  = 0
  NB_Tet_update   = NB_Tet
  NB_Edge_deleted = 0

  !---- loop over number of divisions


  lms(1:NB_Edge_update) = 0
  lmp(1:NB_Point)       = 0
  Mark_Tet(1:NB_Tet_update)  = 1

  Loop_ILoop : DO ILoop = 1,7

     DO IS = 1, NB_Edge_update
        ip1 = IP_Edge(1,IS)
        ip2 = IP_Edge(2,IS)
        IF(ip1/=0 .AND. ip2/=0) THEN
           IF(lmp(ip1)<0 .OR. lmp(ip2)<0)  lms(IS) = 0
        ENDIF
     ENDDO
     DO IP = 1 , NB_Point
        IF(lmp(IP)<0) lmp(IP) = 0
     ENDDO


     !--- loop over the edges, skip the first NB_BD_Edge boundary edge

     NB_Edge      = NB_Edge_update
     ntry         = 0
     NB_Edge_Swap = 0
     Loop_iis : DO iis = NB_BD_Edge+1,NB_Edge

        !--- check that edge still exists and not boundary edge

        ip1 = IP_Edge(1,iis)
        ip2 = IP_Edge(2,iis)
        IF(ip1==0 .AND. ip2==0) CYCLE Loop_iis
        IF(lms(iis)==1 .AND. lmp(ip1)==1 .AND. lmp(ip2)==1)CYCLE Loop_iis
        IF(ip1<=NB_Extra_Point)THEN
           !--- the extra points hace a large number of links, 
           !    so don't use them to search the change.
           ip1 = IP_Edge(2,iis)
           ip2 = IP_Edge(1,iis)
        ENDIF

        IF(Debug_Display>3)THEN
           IF(ip1<=NB_BD_Point .AND. ip2<=NB_BD_Point) THEN
              !-- check the edge not a boundary edge
              CALL Boundary_Edg_Match(ip1,ip2,IS)
              IF(IS>0)THEN
                 WRITE(29,*)'Error---: a boundary edge found'
                 WRITE(29,*)'iis,NB_BD_Edge,is=',iis,NB_BD_Edge, IS
                 WRITE(29,*)'ip1,ip2=', ip1,ip2
                 CALL Error_Stop ('Swap3D_BD:: boundary edge found')
              ENDIF
           ENDIF
        ENDIF

        !--- find number of edges surrounding it

        did       = SmallVolume !-- the minimum quality
        NchainSeg = 0           !-- no. of chain segments surrounding axis ip1-ip2
        kUncheck  = 0           !-- if =0, then all surrounding elements checked

        !--- List all elements surrounding point ip1 in the array ITs_List
        CALL LinkAssociation_List(ip1, List_Length, ITs_List, PointAsso)

        Loop_iie : DO iie = 1 , List_Length
           IT = ITs_List(iie)

           IF(  IP_Tet(1,IT)/=ip2 .AND. IP_Tet(2,IT)/=ip2 .AND.   &
                IP_Tet(3,IT)/=ip2 .AND. IP_Tet(4,IT)/=ip2) CYCLE Loop_iie

           !--- if some elements have been distroyed then try latter

           did  = MIN(did,Quality_Tet(1,IT))
           NchainSeg  = NchainSeg + 1

           !--- Do not swap if more than 6 segments found
           IF(NchainSeg>6) THEN
              lms(iis) = 1
              IF(lmp(ip1)==0) lmp(ip1) = 1
              IF(lmp(ip2)==0) lmp(ip2) = 1
              CYCLE Loop_iis
           ENDIF

           IT_Cavity(NchainSeg)  = IT
           IF(Mark_Tet(IT)>=ILoop) kUnCheck = 1
           DO iside = 1 , 12
              IF(  IP_Tet(I_Comb_Tet(1,iside),IT)==ip1 .AND.   &
                   IP_Tet(I_Comb_Tet(2,iside),IT)==ip2  ) THEN
                 IP_Chain(1,NchainSeg) = IP_Tet(I_Comb_Tet(3,iside),IT)
                 IP_Chain(2,NchainSeg) = IP_Tet(I_Comb_Tet(4,iside),IT)
                 CYCLE Loop_iie
              ENDIF
           ENDDO

           WRITE(29,*)'Error---: chain missed--'
           CALL Error_Stop ('Swap3D_BD:: chain missed ')
        ENDDO Loop_iie

        IF(NchainSeg<=2)THEN
           IF(NchainSeg==1) CALL Error_Stop ('Swap3D_BD:: NchainSeg==1')
           IF(Debug_Display>1)THEN
              WRITE(*, *)'Warning--- Swap3D_BD:: flat or negative element?'
              WRITE(29,*)'Warning--- Swap3D_BD:: flat or negative element?'
              WRITE(29,*)'iLoop=',iLoop
              WRITE(29,*)'iis=',iis,'ip1,ip2=',ip1,ip2
              WRITE(29,*)'NchainSeg=',NchainSeg, ' ITs=',IT_Cavity(1:NchainSeg)
              WRITE(29,*)'chain=',IP_Chain(:,1:NchainSeg), ' did=', did
           ENDIF
           IF(IP_Chain(2,1)/=IP_Chain(1,2) .OR. IP_Chain(2,2)/=IP_Chain(1,1))THEN
              WRITE(29,*)'Error--- : Broken flat chain: iis=',iis,'ip1,ip2=',ip1,ip2
              CALL Error_Stop ('Swap3D_BD:: Broken flat chain')
           ENDIF

           !--- remove two elements.
           DO ichain = 1, NchainSeg
              IT      = IT_Cavity(ichain)
              NB_Tet_deleted = NB_Tet_deleted + 1
              IF(NB_Tet_deleted>mled) THEN
                 mled = mled + madd
                 CALL allc_1Dpointer ( led,  mled,     'led' )
              ENDIF
              led(NB_Tet_deleted)  = IT
              CALL LinkAssociation_Remove(4, IT,IP_Tet(:,IT),PointAsso)
           ENDDO

           !---  remove edge
           NB_Edge_deleted = NB_Edge_deleted + 1
           IF(NB_Edge_deleted>mlsd) THEN
              mlsd = mlsd + madd
              CALL allc_1Dpointer ( lsde, mlsd,     'lsde')
           ENDIF
           lsde(NB_Edge_deleted) = iis
           IP_Edge(1:2,iis)      = 0
           lmp(ip1) = -1
           lmp(ip2) = -1
           NB_Edge_Swap = NB_Edge_Swap + 1

           CYCLE Loop_iis
        ENDIF

        !--- Do not swap if all has been checked.
        IF(kUnCheck==0) THEN
           lms(iis) = 1
           IF(lmp(ip1)==0) lmp(ip1) = 1
           IF(lmp(ip2)==0) lmp(ip2) = 1
           CYCLE Loop_iis
        ENDIF

        !--- start a new try to swap
        ntry   = ntry + 1

        !--- sort IP_Chain to a chain, ie. IP_Chain(1,is+1)=IP_Chain(2,is)

        ipp(1:2)     = IP_Chain(1:2,1)
        Loop_ichain : DO ichain = 2,NchainSeg-1
           DO iside = ichain , NchainSeg
              IF(IP_Chain(1,iside)==ipp(2)) THEN
                 jpp(1:2)         = IP_Chain(1:2,ichain)
                 IP_Chain(1:2,ichain) = IP_Chain(1:2,iside)
                 IP_Chain(1:2,iside)  = jpp(1:2)
                 ipp(2)           = IP_Chain(2,ichain)
                 CYCLE Loop_ichain
              ENDIF
           ENDDO
           EXIT  Loop_ichain
        ENDDO Loop_ichain

        IF(ichain/=NchainSeg .OR. IP_Chain(2,NchainSeg)/=IP_Chain(1,1))THEN
           WRITE(*, *)'Error--- : Broken chain: iis=',iis,'ip1,ip2=',ip1,ip2
           WRITE(29,*)'Error--- : Broken chain: iis=',iis,'ip1,ip2=',ip1,ip2
           WRITE(29,*)'iLoop=',iLoop
           WRITE(29,*)'ichain, iside, Nsur=',ichain,iside,NchainSeg
           WRITE(29,*)'chain=',IP_Chain(:,1:NchainSeg)
           CALL Error_Stop ('Swap3D_BD:: Broken chain')
        ENDIF


        !--- Check qulity for all possible new elements

        DO itri = 1 , npos(NchainSeg-2)
           DO istore = 1, 2

              !--- istore=1 : store the top element
              !--- istore=2 : store the bottom element

              ipp(1) = IP_Chain(1,nce(1,itri,NchainSeg-2))
              IF(istore==1)THEN
                 ipp(2) = IP_Chain(1,nce(2,itri,NchainSeg-2))
                 ipp(3) = IP_Chain(1,nce(3,itri,NchainSeg-2))
                 ipp(4) = ip2
              ELSE
                 ipp(2) = IP_Chain(1,nce(3,itri,NchainSeg-2))
                 ipp(3) = IP_Chain(1,nce(2,itri,NchainSeg-2))
                 ipp(4) = ip1
              ENDIF

              p1(:) = Posit(:,ipp(1))
              p2(:) = Posit(:,ipp(2))
              p3(:) = Posit(:,ipp(3))
              p4(:) = Posit(:,ipp(4))

              dihn(istore,itri) = Geo3D_Tet_Quality(p1,p2,p3,p4,1)

           ENDDO


        ENDDO     !--- loop itri

        !--- find the best configuration
        !--- dihmax : the minimum angle from the best configuration

        dihmax = -HugeValue
        NB_newMax = 0
        knp = 0
        DO inp = 1 , mpos(NchainSeg-2)

           dihmin = HugeValue
           DO jnp =  1 , NchainSeg-2
              itri   = ncp(jnp,inp,NchainSeg-2)
              dihmin = MIN(dihmin,dihn(1,itri),dihn(2,itri))
           ENDDO

           IF(dihmin<=did) CYCLE
           NB_newBD  = 0
           DO iside = 1, NchainSeg-3
              itri = ncp(iside,inp,NchainSeg-2)
              jpp(1:2) = IP_Chain(1,nce(1:2,itri,NchainSeg-2))

              IF(MAX(jpp(1),jpp(2))<=NB_BD_Point+NB_Extra_Point) THEN
                 !--- check if a new boundary edge appear
                 !    this is possible when the subrutine is called from Boundary_Insert
                 CALL Boundary_Edg_Match(jpp(1),jpp(2),IS)
                 IF(IS>0)  NB_newBD = NB_newBD+1
              ENDIF
           ENDDO

           IF(NB_newBD>NB_newMax .OR. (NB_newBD==NB_newMax .AND. dihmin>dihmax)) THEN
              knp    = inp
              dihmax = dihmin
              NB_newMax = NB_newBD
           ENDIF
        ENDDO

        !--- skip if the best configuration worse than the original

        IF( knp==0 ) THEN
           lms(iis) = 1
           IF(lmp(ip1)==0) lmp(ip1) = 1
           IF(lmp(ip2)==0) lmp(ip2) = 1
           CYCLE Loop_iis
        ENDIF

        !--- Do swapping now
        !--- NB_Tet_deleted  : the no. of deleted elements to be removed
        !--- led(:)          : the list of deleted elements to be removed

        DO ichain = 1, NchainSeg
           IT      = IT_Cavity(ichain)
           NB_Tet_deleted = NB_Tet_deleted + 1
           IF(NB_Tet_deleted>mled) THEN
              mled = mled + madd
              CALL allc_1Dpointer ( led,  mled,     'led' )
           ENDIF
           led(NB_Tet_deleted)  = IT

           CALL LinkAssociation_Remove(4, IT,IP_Tet(:,IT),PointAsso)

        ENDDO

        !--- store new elements

        DO ichain = 1 , NchainSeg-2
           itri = ncp(ichain,knp,NchainSeg-2)

           DO istore = 1, 2

              IF(NB_Tet_deleted>0) THEN
                 IT = led(NB_Tet_deleted)
                 NB_Tet_deleted = NB_Tet_deleted - 1
              ELSE
                 NB_Tet_update   = NB_Tet_update + 1
                 IF(NB_Tet_update>nallc_Tet) THEN
                    CALL ReAllocate_Tet( )
                 ENDIF
                 IT = NB_Tet_update
              ENDIF
              IP_Tet(1,IT) = IP_Chain(1,nce(1,itri,NchainSeg-2))
              IF(istore==1)THEN
                 IP_Tet(2,IT) = IP_Chain(1,nce(2,itri,NchainSeg-2))
                 IP_Tet(3,IT) = IP_Chain(1,nce(3,itri,NchainSeg-2))
                 IP_Tet(4,IT) = ip2
              ELSE
                 IP_Tet(2,IT) = IP_Chain(1,nce(3,itri,NchainSeg-2))
                 IP_Tet(3,IT) = IP_Chain(1,nce(2,itri,NchainSeg-2))
                 IP_Tet(4,IT) = ip1
              ENDIF
              Mark_Tet( IT ) = ILoop + 1
              Quality_Tet(1, IT ) = dihn( istore,itri )
              IF(useStretch .AND. ABS(BGSpacing%Model)>1)THEN
                 CALL Get_Tet_Mapping(IT)
              ENDIF

              !---  update data structure
              CALL LinkAssociation_AddCell(4, IT,IP_Tet(:,IT),PointAsso)

           ENDDO
        ENDDO

        !---  update side data structure, remove the old one
        NB_Edge_deleted = NB_Edge_deleted + 1
        IF(NB_Edge_deleted>mlsd) THEN
           mlsd = mlsd + madd
           CALL allc_1Dpointer ( lsde, mlsd,     'lsde')
        ENDIF
        lsde(NB_Edge_deleted) = iis
        IP_Edge(1:2,iis)      = 0
        lmp(ip1) = -1
        lmp(ip2) = -1
        NB_Edge_Swap = NB_Edge_Swap + 1

        !---  update side data structure, add new edges
        DO iside = 1, NchainSeg-3
           itri = ncp(iside,knp,NchainSeg-2)
           jpp(1:2) = IP_Chain(1,nce(1:2,itri,NchainSeg-2))
           lmp(jpp(1:2)) = -1

           IF(NB_Edge_deleted==0) THEN
              !-- create an edge
              NB_Edge_update = NB_Edge_update + 1
              IF(NB_Edge_update>Mside) THEN
                 Mside = Mside + madd
                 CALL allc_1Dpointer ( lms,  Mside,    'lms' )
                 CALL allc_2Dpointer ( IP_Edge, 2, Mside, 'IP_Edge')
              ENDIF
              jside   = NB_Edge_update
           ELSE
              !-- replace an edge
              jside           = lsde(NB_Edge_deleted)
              NB_Edge_deleted = NB_Edge_deleted - 1
           ENDIF
           lms(jside) = 0
           IP_Edge(1:2,jside) = jpp(1:2)

           IF(MAX(jpp(1),jpp(2))<=NB_BD_Point+NB_Extra_Point) THEN
              !--- check if a new boundary edge appear
              !    this is possible when the subrutine is called from Boundary_Insert
              CALL Boundary_Edg_Match(jpp(1),jpp(2),IS)
              IF(IS>0)  IP_Edge(1:2,jside) = 0
           ENDIF
        ENDDO

     ENDDO Loop_iis

     WRITE(29,'(a,i2,3(a,i8))') ' Loop: ',iLoop,'  NB_Edge_Swap = ',NB_Edge_Swap,   &
          ' nfail =',ntry-NB_Edge_Swap,   ' nelem =',NB_Tet_update-NB_Tet_deleted

     IF(NB_Edge_Swap<=1) EXIT Loop_ILoop
  ENDDO Loop_ILoop

  !---  renumber the elements

  NB_Tet = NB_Tet_update
  DO iie = 1, NB_Tet_deleted
     IP_Tet(4,led(iie)) = 0
  ENDDO
  DO iie = 1, NB_Tet_deleted
     IT  = led(iie)
     DO WHILE(IP_Tet(4,NB_Tet)==0)
        NB_Tet = NB_Tet - 1
     ENDDO
     IF(IT<NB_Tet) THEN
        IP_Tet(1,IT) = IP_Tet(1,NB_Tet)
        IP_Tet(2,IT) = IP_Tet(2,NB_Tet)
        IP_Tet(3,IT) = IP_Tet(3,NB_Tet)
        IP_Tet(4,IT) = IP_Tet(4,NB_Tet)
        IF(BGSpacing%Model<-1)THEN
           fMap_Tet(:,:,IT) = fMap_Tet(:,:,NB_Tet)
        ELSE IF(BGSpacing%Model>1)THEN
           Scale_Tet(IT) = Scale_Tet(NB_Tet)
        ENDIF
        NB_Tet = NB_Tet - 1
     ENDIF
  ENDDO

  DEALLOCATE ( lmp, lms, IP_Edge, led, lsde )
  CALL LinkAssociation_Clear(PointAsso)

  RETURN
END SUBROUTINE Swap3D_BD


!*******************************************************************************
!>
!!   Swap one edge if no negative element appears.
!!   @param[in]  Kswap  = 1,   use volume as quality standard.                \n
!!                      = 2,   use the minimum dihedral angle.
!!   @param[in]  ip1,ip2   the two points of the edge being splitted.
!!   @param[in]  QultMin   the lease quality to carry swapping.
!!   @param[in]  Pole_Surround::NB_PoleTet
!!                          Obtained by calling Search_Pole_Surround( )
!!   @param[in]  Pole_Surround::IT_PoleTet (1:NB_PoleTet)      
!!   @param[in]  Pole_Surround::NB_PolePt
!!                          Obtained by calling Search_Pole_Surround( )
!!   @param[in]  Pole_Surround::IP_PolePt (1:NB_PolePt)
!!       
!!   @param[out]  Isucc = 1,   swap successfully.                             \n
!!                      < 0,   no swap due to                                 \n
!!                      =-1,      too many nodes on the chain.                \n
!!                      =-2,      is a boundary edge.                         \n
!!                      =-3,      original mesh is good enought.              \n
!!                      =-5,      no better solution for swapping.            \n
!!                      =-6,      too few nodes on the chain (flat cells?)
!!
!!     Modify:  common_Parameters::NEXT_Tet and
!!              common_Parameters::PointAsso (if built).
!<
!*******************************************************************************
SUBROUTINE Swap3D_1Edge(ip1, ip2, Kswap, QultMin, Isucc)

  USE common_Parameters
  USE Pole_Surround
  USE Geometry3DAll
  USE SwapConnect
  IMPLICIT NONE

  INTEGER, INTENT(IN)    :: ip1, ip2, Kswap
  INTEGER, INTENT(INOUT) :: Isucc
  REAL*8, INTENT(IN)     :: QultMin
  INTEGER :: IP_Chain(6), IT_Cavity(12), IT_Outs(2,6), IP_Chord(3,6)
  REAL*8  :: dihn(2,1000)

  INTEGER :: ipp(4), jpp(2)
  INTEGER :: IT, IP, itri, istore, NchainSeg, NTnew
  INTEGER :: inp, jnp, knp, iside,  ichain, it2, i, i1, i2, iout, j
  REAL *8 :: p1(3),p2(3),p3(3),p4(3)
  REAL *8 :: dihmin, dihmax


  IF(NB_PoleTet/=NB_PolePt)THEN
     Isucc = -2
     RETURN
  ENDIF
  NchainSeg  = NB_PoleTet
  IF(NchainSeg>6)THEN
     Isucc = -1
     RETURN
  ENDIF
  IT_Cavity(1:NB_PoleTet) = IT_PoleTet(1:NB_PoleTet)
  IP_Chain (1:NB_PolePt)  = IP_PolePt (1:NB_PolePt)

  IF(NchainSeg<=2)THEN
     IF(NchainSeg<=1) CALL  Error_Stop ('Swap3D_1Edge:: NchainSeg<=1')

     IF(Debug_Display>1)THEN
        WRITE(*, *)'Warning--- Swap3D_1Edge:: flat or negative element?'
        WRITE(29,*)'Warning--- Swap3D_1Edge:: flat or negative element?'
        WRITE(29,*)'ip1,ip2=',ip1,ip2
        WRITE(29,*)'NchainSeg=',NchainSeg, ' ITs=',IT_Cavity(1:NchainSeg)
        WRITE(29,*)'chain=',IP_Chain(1:NchainSeg)
     ENDIF

     !--- remove two elements.
     DO ichain = 1, NchainSeg
        IT = IT_Cavity(ichain)
        DO i=1,4
           IF(IP_Tet(i,IT)==ip1) IT_Outs(1,ichain) = NEXT_Tet(i,IT)
           IF(IP_Tet(i,IT)==ip2) IT_Outs(2,ichain) = NEXT_Tet(i,IT)
        ENDDO
        IF(LinkAssociation_Exist(PointAsso))THEN
           CALL LinkAssociation_Remove(4, IT,IP_Tet(:,IT),PointAsso)
        ENDIF
        IP_Tet(4,IT) = -999
     ENDDO
     !--- rebuild next_tet
     CALL Match_Next_update(IT_Outs(1,1), IT_Cavity(1), IT_Outs(1,2))
     CALL Match_Next_update(IT_Outs(1,2), IT_Cavity(2), IT_Outs(1,1))
     CALL Match_Next_update(IT_Outs(2,1), IT_Cavity(1), IT_Outs(2,2))
     CALL Match_Next_update(IT_Outs(2,2), IT_Cavity(2), IT_Outs(2,1))

     Isucc = 1
     RETURN
  ENDIF

  !--- calculate qulity for all possible new elements
  DO itri = 1 , npos(NchainSeg-2)
     ipp(1:3) = IP_Chain(nce(1:3,itri,NchainSeg-2))
     p1(:) = Posit(:,ipp(1))
     p2(:) = Posit(:,ipp(2))
     p3(:) = Posit(:,ipp(3))
     !--- istore=1 : store the top element
     p4(:) = Posit(:,ip2)
     dihn(1,itri) = Geo3D_Tet_Quality(p1,p2,p3,p4,Kswap)
     !--- istore=2 : store the bottom element
     p4(:) = Posit(:,ip1)
     dihn(2,itri) = Geo3D_Tet_Quality(p1,p3,p2,p4,Kswap)   !--- mind the order
  ENDDO

  !--- find the best configuration
  !--- dihmax : the minimum angle from the best configuration
  dihmax = -HugeValue
  DO inp = 1 , mpos(NchainSeg-2)
     dihmin = HugeValue
     DO jnp =  1 , NchainSeg-2
        itri   = ncp(jnp,inp,NchainSeg-2)
        dihmin = MIN(dihmin,dihn(1,itri),dihn(2,itri))
     ENDDO
     IF(dihmin>dihmax) THEN
        knp    = inp
        dihmax = dihmin
     ENDIF
  ENDDO

  !--- skip if the best configuration worse than the original
  IF(dihmax<=QultMin)THEN
     Isucc = -5
     RETURN
  ENDIF

  !--- Do swapping now

  !--- remove old cells and mark the neighbours
  DO ichain = 1, NchainSeg
     IT = IT_Cavity(ichain)
     DO i=1,4
        IF(IP_Tet(i,IT)==ip1) IT_Outs(1,ichain) = NEXT_Tet(i,IT)
        IF(IP_Tet(i,IT)==ip2) IT_Outs(2,ichain) = NEXT_Tet(i,IT)
     ENDDO

     IF(LinkAssociation_Exist(PointAsso))THEN
        CALL LinkAssociation_Remove(4, IT,IP_Tet(:,IT),PointAsso)
     ENDIF
     IP_Tet(4,IT) = -999
  ENDDO


  !--- store new elements
  NTnew = 0
  DO ichain = 1 , NchainSeg-2
     DO istore = 1, 2
        NTnew = NTnew+1
        IF(NTnew>NchainSeg)THEN
           NB_Tet = NB_Tet+1
           IF(NB_Tet>nallc_Tet)   CALL ReAllocate_Tet( )
           IT_Cavity(NTnew) = NB_Tet
        ENDIF
     ENDDO
  ENDDO


  NTnew = 0
  DO ichain = 1 , NchainSeg-2
     itri = ncp(ichain,knp,NchainSeg-2)
     ipp(1:3) = IP_Chain(nce(1:3,itri,NchainSeg-2))

     DO istore = 1, 2
        NTnew = NTnew+1
        IT = IT_Cavity(NTnew)

        IP_Tet(1,IT) = ipp(1)
        IF(istore==1)THEN
           IP_Tet(2,IT) = ipp(2)
           IP_Tet(3,IT) = ipp(3)
           IP_Tet(4,IT) = ip2
           IT2 = IT_Cavity(NTnew+1)
        ELSE
           IP_Tet(2,IT) = ipp(3)
           IP_Tet(3,IT) = ipp(2)
           IP_Tet(4,IT) = ip1
           IT2 = IT_Cavity(NTnew-1)
        ENDIF

        NEXT_Tet(:,IT) = -1
        NEXT_Tet(4,IT) = IT2

        IF(LinkAssociation_Exist(PointAsso))THEN
           CALL LinkAssociation_AddCell(4, IT,IP_Tet(:,IT),PointAsso)
        ENDIF
     ENDDO

  ENDDO

  !---  update next_tet
  DO iside = 1, NchainSeg-3
     itri = ncp(iside,knp,NchainSeg-2)
     ip_chord(1:2,iside) = IP_Chain(nce(1:2,itri,NchainSeg-2))
     ip_chord(3,  iside) = 0
  ENDDO

  DO ichain = 1 , NchainSeg-2
     i1       = 2*ichain
     ipp(1:3) = IP_Tet(1:3,IT_Cavity(i1))
     ipp(4)   = ipp(1)
     Loop_i : DO i=1,3
        jpp(1:2) = ipp(i:i+1)
        DO iside = 1, NchainSeg-3
           IF(ip_chord(3,iside)<0) CYCLE
           IF(  (jpp(1)==ip_chord(1,iside) .AND. jpp(2)==ip_chord(2,iside)) .OR.  &
                (jpp(1)==ip_chord(2,iside) .AND. jpp(2)==ip_chord(1,iside)) ) THEN
              IF(ip_chord(3,iside)==0)THEN
                 ip_chord(3,iside)=ichain
              ELSE
                 i2 = 2*ip_chord(3,iside)
                 CALL Match_Next(IT_Cavity(i1),   IT_Cavity(i2))
                 CALL Match_Next(IT_Cavity(i2),   IT_Cavity(i1))
                 CALL Match_Next(IT_Cavity(i1-1), IT_Cavity(i2-1))
                 CALL Match_Next(IT_Cavity(i2-1), IT_Cavity(i1-1))
                 ip_chord(3,iside)=-1
              ENDIF
              CYCLE Loop_i
           ENDIF
        ENDDO

        DO istore = 1,2
           IF(istore==2) IT = IT_Cavity(i1)
           IF(istore==1) IT = IT_Cavity(i1-1)
           DO iout = 1,NchainSeg
              it2 = IT_Outs(istore,iout)
              IF(it2<=0) CYCLE
              i2 = 0
              DO j=1,4
                 IF(IP_Tet(j,it2)==jpp(1)) i2 = i2+1
                 IF(IP_Tet(j,it2)==jpp(2)) i2 = i2+1
              ENDDO

              IF(i2<2)CYCLE
              CALL Match_Next(IT,  it2)
              CALL Match_Next(it2, IT)
              IT_Outs(istore,iout) = 0
              EXIT
           ENDDO
        ENDDO

     ENDDO Loop_i
  ENDDO

  IF(Debug_Display>2)THEN
     DO iside = 1, NchainSeg-3
        IF(ip_chord(3,iside)/=-1) CALL Error_Stop ('  Swap3d_1Edge:: ip_chord')
     ENDDO
     DO iout = 1,NchainSeg
        IF(IT_Outs(1,iout)>0 .OR. IT_Outs(2,iout) >0)THEN
           WRITE(29,*)' Error--- IT_Outs(:,iout)=',IT_Outs(:,iout)
           WRITE(29,*)'ip1,ip2=',ip1,ip2
           CALL  Error_Stop (' Swap3d_1Edge:: IT_Outs')
        ENDIF
     ENDDO
  ENDIF

  Isucc = 1

  RETURN
END SUBROUTINE Swap3d_1Edge

!*******************************************************************************
!>
!!   Swap one edge if no negative element appears.                            \n
!!   Differing from subroutine Swap3D_1Edge(), 
!!        this subroutine does not need/update common_Parameters::NEXT_Tet
!!         (Added in Sep. 2011. More test in need.)
!!
!!   @param[in]  Kswap  = 1,   use volume as quality standard.                \n
!!                      = 2,   use the minimum dihedral angle.
!!   @param[in]  ip1,ip2   the two points of the edge being splitted.
!!   @param[in]  QultMin   the lease quality to carry swapping.
!!   @param[in]  Pole_Surround::NB_PoleTet
!!                          Obtained by calling Search_Pole_Surround( )
!!   @param[in]  Pole_Surround::IT_PoleTet (1:NB_PoleTet)      
!!   @param[in]  Pole_Surround::NB_PolePt
!!                          Obtained by calling Search_Pole_Surround( )
!!   @param[in]  Pole_Surround::IP_PolePt (1:NB_PolePt)
!!       
!!   @param[out]  Isucc = 1,   swap successfully.                             \n
!!                      < 0,   no swap due to                                 \n
!!                      =-1,      too many nodes on the chain.                \n
!!                      =-2,      is a boundary edge.                         \n
!!                      =-3,      original mesh is good enought.              \n
!!                      =-5,      no better solution for swapping.            \n
!!                      =-6,      too few nodes on the chain (flat cells?)
!!
!!     common_Parameters::NEXT_Tet is not in need in this subroutine
!!               and will be ruined aftermath.                                \n
!!     Modify: common_Parameters::PointAsso (if built).
!<
!*******************************************************************************
SUBROUTINE Swap3D_1Edge_2(ip1, ip2, Kswap, QultMin, Isucc)

  USE common_Parameters
  USE Pole_Surround
  USE Geometry3DAll
  USE SwapConnect
  IMPLICIT NONE

  INTEGER, INTENT(IN)    :: ip1, ip2, Kswap
  INTEGER, INTENT(INOUT) :: Isucc
  REAL*8, INTENT(IN)     :: QultMin
  INTEGER :: IP_Chain(6), IT_Cavity(12)
  REAL*8  :: dihn(2,1000)

  INTEGER :: ipp(4), jpp(2)
  INTEGER :: IT, IP, itri, istore, NchainSeg, NTnew
  INTEGER :: inp, jnp, knp, iside,  ichain, i, i1, i2, iout, j
  REAL *8 :: p1(3),p2(3),p3(3),p4(3)
  REAL *8 :: dihmin, dihmax


  IF(NB_PoleTet/=NB_PolePt)THEN
     Isucc = -2
     RETURN
  ENDIF
  NchainSeg  = NB_PoleTet
  IF(NchainSeg>6)THEN
     Isucc = -1
     RETURN
  ENDIF
  IT_Cavity(1:NB_PoleTet) = IT_PoleTet(1:NB_PoleTet)
  IP_Chain (1:NB_PolePt)  = IP_PolePt (1:NB_PolePt)

  IF(NchainSeg<=2)THEN
     IF(NchainSeg<=1) CALL  Error_Stop ('Swap3D_1Edge_2:: NchainSeg<=1')

     IF(Debug_Display>1)THEN
        WRITE(*, *)'Warning--- Swap3D_1Edge_2:: flat or negative element?'
        WRITE(29,*)'Warning--- Swap3D_1Edge_2:: flat or negative element?'
        WRITE(29,*)'ip1,ip2=',ip1,ip2
        WRITE(29,*)'NchainSeg=',NchainSeg, ' ITs=',IT_Cavity(1:NchainSeg)
        WRITE(29,*)'chain=',IP_Chain(1:NchainSeg)
     ENDIF

     !--- remove two elements.
     DO ichain = 1, NchainSeg
        IT = IT_Cavity(ichain)
        IF(LinkAssociation_Exist(PointAsso))THEN
           CALL LinkAssociation_Remove(4, IT,IP_Tet(:,IT),PointAsso)
        ENDIF
        IP_Tet(4,IT) = -999
     ENDDO

     Isucc = 1
     RETURN
  ENDIF

  !--- calculate qulity for all possible new elements
  DO itri = 1 , npos(NchainSeg-2)
     ipp(1:3) = IP_Chain(nce(1:3,itri,NchainSeg-2))
     p1(:) = Posit(:,ipp(1))
     p2(:) = Posit(:,ipp(2))
     p3(:) = Posit(:,ipp(3))
     !--- istore=1 : store the top element
     p4(:) = Posit(:,ip2)
     dihn(1,itri) = Geo3D_Tet_Quality(p1,p2,p3,p4,Kswap)
     !--- istore=2 : store the bottom element
     p4(:) = Posit(:,ip1)
     dihn(2,itri) = Geo3D_Tet_Quality(p1,p3,p2,p4,Kswap)   !--- mind the order
  ENDDO

  !--- find the best configuration
  !--- dihmax : the minimum angle from the best configuration
  dihmax = -HugeValue
  DO inp = 1 , mpos(NchainSeg-2)
     dihmin = HugeValue
     DO jnp =  1 , NchainSeg-2
        itri   = ncp(jnp,inp,NchainSeg-2)
        dihmin = MIN(dihmin,dihn(1,itri),dihn(2,itri))
     ENDDO
     IF(dihmin>dihmax) THEN
        knp    = inp
        dihmax = dihmin
     ENDIF
  ENDDO

  !--- skip if the best configuration worse than the original
  IF(dihmax<=QultMin)THEN
     Isucc = -5
     RETURN
  ENDIF

  !--- Do swapping now

  !--- remove old cells and mark the neighbours
  DO ichain = 1, NchainSeg
     IT = IT_Cavity(ichain)

     IF(LinkAssociation_Exist(PointAsso))THEN
        CALL LinkAssociation_Remove(4, IT,IP_Tet(:,IT),PointAsso)
     ENDIF
     IP_Tet(4,IT) = -999
  ENDDO


  !--- store new elements
  NTnew = 0
  DO ichain = 1 , NchainSeg-2
     DO istore = 1, 2
        NTnew = NTnew+1
        IF(NTnew>NchainSeg)THEN
           NB_Tet = NB_Tet+1
           IF(NB_Tet>nallc_Tet)   CALL ReAllocate_Tet( )
           IT_Cavity(NTnew) = NB_Tet
        ENDIF
     ENDDO
  ENDDO


  NTnew = 0
  DO ichain = 1 , NchainSeg-2
     itri = ncp(ichain,knp,NchainSeg-2)
     ipp(1:3) = IP_Chain(nce(1:3,itri,NchainSeg-2))

     DO istore = 1, 2
        NTnew = NTnew+1
        IT = IT_Cavity(NTnew)

        IP_Tet(1,IT) = ipp(1)
        IF(istore==1)THEN
           IP_Tet(2,IT) = ipp(2)
           IP_Tet(3,IT) = ipp(3)
           IP_Tet(4,IT) = ip2
        ELSE
           IP_Tet(2,IT) = ipp(3)
           IP_Tet(3,IT) = ipp(2)
           IP_Tet(4,IT) = ip1
        ENDIF

        IF(LinkAssociation_Exist(PointAsso))THEN
           CALL LinkAssociation_AddCell(4, IT,IP_Tet(:,IT),PointAsso)
        ENDIF
     ENDDO

  ENDDO


  Isucc = 1

  RETURN
END SUBROUTINE Swap3d_1Edge_2


