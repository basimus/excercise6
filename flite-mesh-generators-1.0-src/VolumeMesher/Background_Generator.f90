!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!     Build an anisotropic background data structure from the generated volume mesh of a surface triangulation.
!<       
!*******************************************************************************
SUBROUTINE Background_Stretch( )

  USE common_Parameters
  USE Geometry3DAll
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: IT, ip, ipt, i,j
  REAL*8  :: fm1(3,3), fm2(3,3), xl, dd
  INTEGER, DIMENSION(:  ), ALLOCATABLE :: idx
  REAL*8,  DIMENSION(:  ), ALLOCATABLE :: xdx
  LOGICAL :: CarryOn

  CALL allc_3Dpointer(BGSpacing%fMap_Point,3,3,NB_Point, 'fMap_Point_B')

  DO ip = NB_Extra_Point+1, NB_Extra_Point+Surf%NB_Point
     BGSpacing%fMap_Point(1:3,1:3,ip) = fMap_Point(1:3,1:3,ip)
  ENDDO

  !--- set AdTree for the background mesh
  call ADTree_SetName(BGSpacing%ADTree, 'Background_ADTree')
  call ADTree_SetTree(BGSpacing%ADTree, Posit, IP_Tet, NB_Tet, NB_Point)

  IF(NB_Point==Surf%NB_Point) RETURN
  
  !--- Interpolation for those node which are not original surface nodes

  ALLOCATE(idx(NB_Point), xdx(NB_Point))
  idx(1:NB_Point) = 0
  idx(NB_Extra_Point+1 : NB_Extra_Point+Surf%NB_Point) = -1
  xdx(1:NB_Point) = 0

  CarryOn = .TRUE.
  DO WHILE(CarryOn)
     DO IT = 1,NB_Tet
        DO i=1,4
           ip = IP_Tet(i,IT)                  !--- ip, a node unsetted
           IF(idx(ip)==-1) CYCLE
           DO j=1,4
              ipt = IP_Tet(j,IT)              !--- ipt, a node setted
              IF(idx(ipt)/=-1) CYCLE
              
              dd = 1.d0 / Geo3D_Distance(Posit(:,ip), Posit(:,ipt))
              IF(idx(ip)==0)THEN
                 idx(ip) = 1
                 BGSpacing%fMap_Point(:,:,ip) = BGSpacing%fMap_Point(:,:,ipt)
                 xdx(iP) = dd                 
              ELSE
                 fm1 = BGSpacing%fMap_Point(:,:,ip)
                 fm2 = BGSpacing%fMap_Point(:,:,ipt)
                 xl  = dd / (xdx(ip)+dd)
                 fm1 = Mapping3D_Interpolate(fm1,fm2,xl,Mapping_Interp_Model)
                 BGSpacing%fMap_Point(:,:,ip) = fm1
                 xdx(ip) = xdx(ip)+dd
              ENDIF
           ENDDO
        ENDDO
     ENDDO

     CarryOn = .FALSE.
     DO ip=1,NB_Point
        IF(idx(ip)==0)THEN
           CarryOn = .TRUE.
        ELSE
           idx(ip) = -1
        ENDIF
     ENDDO

  ENDDO

  DEALLOCATE(idx, xdx)

  RETURN

END SUBROUTINE Background_Stretch

!*******************************************************************************
!>
!!   Calculate the metric at each surface point from the triangulation itself.
!!   @param[in]  common_Parameters::Surf      
!!   @param[in]  common_Parameters::IP_BD_Tri
!!   @param[out] common_Parameters::fMap_Point : mapping for surface nodes.
!<       
!*******************************************************************************
SUBROUTINE Surface_Stretch( )

  USE common_Constants
  USE common_Parameters
  USE Geometry3DAll
  USE array_allocator
  USE SurfaceMeshManager
  IMPLICIT NONE

  INTEGER :: ib, ip, i,j, ip3(3), ipt
  REAL*8  :: ff(3), fm1(3,3), fm2(3,3)
  REAL*8,  DIMENSION(:,:), ALLOCATABLE :: Stretch   !--- (6,:)

  REAL*8  :: xl, xlsq, dx, dy, dz, dx2, dy2, city, citz, xlmin
  INTEGER :: ibmin,ip1i,ip2i


  ALLOCATE(Stretch(6,Surf%NB_Point))
  
  CALL Surf_getStretch(Surf,  Stretch)

  DO ip=1,Surf%NB_Point
     stretch(1:3, ip) = stretch(1:3, ip) / BGSpacing%BasicSize
     fMap_Point(1:3,1:3,ip) = Mapping3D_from_Stretch(Stretch(:,ip))
  ENDDO

  IF(Debug_Display>2)THEN
     ff(1:3)  = stretch(1:3,1)
     ip3(1:3) = 1
     DO ip=2,Surf%NB_Point
        DO i=1,3
           IF(ff(i)<stretch(i,ip))THEN
              ip3(i) = ip
              ff(i)  = stretch(i,ip)
           ENDIF
        ENDDO
     ENDDO
     WRITE(29,'(a,3E13.5)')'Max stretch:     ',ff(:)
     WRITE(29,'(a,3I13)')  ' at surface nodes',ip3(:)

     ff(1:3)  = stretch(1:3,1)
     ip3(1:3) = 1
     DO ip=2,Surf%NB_Point
        DO i=1,3
           IF(ff(i)>stretch(i,ip))THEN
              ip3(i) = ip
              ff(i)  = stretch(i,ip)
           ENDIF
        ENDDO
     ENDDO
     WRITE(29,'(a,3E13.5)')'Min stretch:     ',ff(:)
     WRITE(29,'(a,3I13)')  ' at surface nodes',ip3(:)

     ff(1)  = stretch(2,1)/stretch(1,1)
     ip3(1) = 1
     DO ip=2,Surf%NB_Point
        ff(2) = stretch(2,ip)/stretch(1,ip)
        IF(ff(1)>ff(2))THEN
           ip3(1) = ip
           ff(1)  = ff(2)
        ENDIF
     ENDDO
     WRITE(29,'(a,3E13.5)')'Min(stretch(2,:)/stretch(1,:))=',ff(1)
     WRITE(29,'(a,3I13)')  ' at surface nodes',ip3(1)

     xlmin = 1.d36
     DO ib = 1,Surf%NB_Tri
        DO i=1,3
           ip = IP_BD_Tri(i,IB)
           fm1 = fMap_Point(:,:,ip)
           DO j=1,3
              IF(i==j) CYCLE
              ipt = IP_BD_Tri(j,IB)
              xl = Mapping3D_Distance(Posit(:,ip),Posit(:,ipt),fm1)
              IF(xlmin>xl)THEN
                 xlmin = xl
                 ip1i  = ip
                 ip2i  = ipt
                 ibmin = ib
              ENDIF
           ENDDO
        ENDDO
     ENDDO
     xlmin = xlmin/BGSpacing%BasicSize
     WRITE(29,'(a,3E13.5)')'Min distance at mapped domain:',xlmin
     WRITE(29,*)'  at triangle',ibmin,' between nodes',ip1i,ip2i
     WRITE(29,'(a,6E11.3)')'  with stretch',Stretch(1:6,ip1i)
     WRITE(29,'(a,6E11.3)')'  vs.  stretch',Stretch(1:6,ip2i)

  ENDIF

  DEALLOCATE(Stretch)
  CALL Background_Adjust()

  RETURN

END SUBROUTINE Surface_Stretch

!*******************************************************************************
!>
!!     Build an isotropic background data structure for the generated volume mesh.
!<       
!*******************************************************************************
SUBROUTINE Background_Scale( )

  USE common_Parameters
  USE Geometry3DAll
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: IT, ip, ipt, i,j
  REAL*8  :: fm1, fm2, xl, dd
  INTEGER, DIMENSION(:  ), ALLOCATABLE :: idx
  REAL*8,  DIMENSION(:  ), ALLOCATABLE :: xdx
  LOGICAL :: CarryOn

  CALL allc_1Dpointer(BGSpacing%Scalar_Point,   NB_Point, 'Scale_Point_B')

  DO ip = NB_Extra_Point+1, NB_Extra_Point+Surf%NB_Point
     BGSpacing%Scalar_Point(ip) = Scale_Point(ip)
  ENDDO

  !--- set AdTree for the background mesh
  call ADTree_SetName(BGSpacing%ADTree, 'Background_ADTree')
  call ADTree_SetTree(BGSpacing%ADTree, Posit, IP_Tet, NB_Tet, NB_Point)

  IF(NB_Point==Surf%NB_Point) RETURN

  !--- Interpolation for those node which are not original surface nodes

  ALLOCATE(idx(NB_Point), xdx(NB_Point))
  idx(1:NB_Point) = 0
  idx(NB_Extra_Point+1 : NB_Extra_Point+Surf%NB_Point) = -1
  xdx(1:NB_Point) = 0

  CarryOn = .TRUE.
  DO WHILE(CarryOn)
     DO IT = 1,NB_Tet
        DO i=1,4
           ip = IP_Tet(i,IT)                  !--- ip, a node unsetted
           IF(idx(ip)==-1) CYCLE
           DO j=1,4
              ipt = IP_Tet(j,IT)              !--- ipt, a node setted
              IF(idx(ipt)/=-1) CYCLE
              
              dd = 1.d0 / Geo3D_Distance(Posit(:,ip), Posit(:,ipt))
              IF(idx(ip)==0)THEN
                 idx(ip) = 1
                 BGSpacing%Scalar_Point(ip) = BGSpacing%Scalar_Point(ipt)
                 xdx(iP) = dd                 
              ELSE
                 fm1 = BGSpacing%Scalar_Point(ip)
                 fm2 = BGSpacing%Scalar_Point(ipt)
                 xl  = dd / (xdx(ip)+dd)
                 fm1 = Scalar_Interpolate(fm1,fm2,xl,Mapping_Interp_Model)
                 BGSpacing%Scalar_Point(ip) = fm1
                 xdx(ip) = xdx(ip)+dd
              ENDIF
           ENDDO
        ENDDO
     ENDDO

     CarryOn = .FALSE.
     DO ip=1,NB_Point
        IF(idx(ip)==0)THEN
           CarryOn = .TRUE.
        ELSE
           idx(ip) = -1
        ENDIF
     ENDDO
     
  ENDDO

  DEALLOCATE(idx, xdx)

  RETURN

END SUBROUTINE Background_Scale

!*******************************************************************************
!>
!!   Calculate the grid size from the surface.
!!   @param[in]  common_Parameters::Surf      
!!   @param[in]  common_Parameters::IP_BD_Tri
!!   @param[out] common_Parameters::Scale_Point : scale for surface nodes.
!<       
!*******************************************************************************
SUBROUTINE Surface_Scale( )

  USE common_Parameters
  USE Geometry3DAll
  USE SurfaceMeshManager
  IMPLICIT NONE

  INTEGER :: IP, ip1, ip2
  REAL*8  :: f1, f2

  CALL Surf_getScalar(Surf,  Scale_Point, 5)  
  
  IF(ABS(Background_Model)==1)THEN
     f1  = Scale_Point(1)
     f2  = Scale_Point(1)
     ip1 = 1
     ip2 = 1
     DO ip=2,Surf%NB_Point
        IF(f1<Scale_Point(ip))THEN
           ip1 = ip
           f1  = Scale_Point(ip)
        ELSE IF(f2>Scale_Point(ip))THEN
           ip2 = ip
           f2  = Scale_Point(ip)
        ENDIF
     ENDDO
     WRITE(29,'(a,E13.5,a,I13)')'Max Scale:',f1,'  at point',ip1
     WRITE(29,'(a,E13.5,a,I13)')'Min Scale:',f2,'  at point',ip2
     IF((f1-f2)/f2<0.01)THEN
        WRITE(29,*)' Suggestion---- The spacing of the surface mesh is quite unified,'
        WRITE(29,*)'                so it it better set Background_Model as 2.'
     ENDIF
     
     BGSpacing%BasicSize = f1
     BGSpacing%MinSize   = f2
  ENDIF
  
  Scale_Point(1:Surf%NB_Point) = Scale_Point(1:Surf%NB_Point)  / BGSpacing%BasicSize

  IF(Background_Model==1) CALL Background_Adjust()

  RETURN

END SUBROUTINE Surface_Scale

