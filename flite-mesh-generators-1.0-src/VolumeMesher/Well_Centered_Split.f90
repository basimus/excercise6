!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


SUBROUTINE Well_Centered_Split(NB1)
	!This routine attempts to remove bad elements by splitting
	
    USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	IMPLICIT NONE
	
	INTEGER, INTENT(IN) :: NB1
	INTEGER :: IT,i
	INTEGER :: NP_StartLoop, numBad
	INTEGER :: globLoop, finished
	
	INTEGER :: KK
	
	REAL*8  :: ptet(3,4), pc(3), dum(4)
	REAL*8  :: pa(3),pb(3),pd(3), p(3)
	
	!globLoops keeps track of the number of times we have looped round the mesh
	!and finished tells us when to exit the global loop
	globLoop = 0
	finished = 0
	
	DO WHILE(finished.EQ.0)
	  !Smooth the mesh first
	  !CALL MCS_Optim_Gen(NB1)
	  !CALL Swap3D(2)
	  !CALL Next_Build()
	  
	  globLoop = globLoop + 1
	  !This will help us later when we are inserting the generated points
	  NP_StartLoop = NB_Point
	  !Keep track of how many elements in the mesh are still bad
	  numBad = 0
	  
	  !Now loop round the mesh
	  DO IT = 1,NB_Tet
	    
        !First check to see if this element is bad
        pc(:) = Sphere_Tet(1:3,IT)
		ptet(:,1) = Posit(:,IP_Tet(1,IT))
		ptet(:,2) = Posit(:,IP_Tet(2,IT))
		ptet(:,3) = Posit(:,IP_Tet(3,IT))
		ptet(:,4) = Posit(:,IP_Tet(4,IT))
		KK = 0
	    dum =  Geo3D_Tet_Weight(KK,ptet,pc)
		
		IF (KK.NE.1) THEN
		  numBad = numBad + 1
          		 

		 
		  !Now loop around the faces and check which are too large
		!  DO i=1,4
		!    IF ( IP_Tet(iTri_Tet(1,i),IT)>NB_BD_Point.AND. &
		!		  IP_Tet(iTri_Tet(2,i),IT)>NB_BD_Point.AND. &
		!		  IP_Tet(iTri_Tet(3,i),IT)>NB_BD_Point ) THEN
				  
		!	  pa(:) = Posit(:,IP_Tet(iTri_Tet(1,i),IT))
		!      pb(:) = Posit(:,IP_Tet(iTri_Tet(2,i),IT))
		!      pd(:) = Posit(:,IP_Tet(iTri_Tet(3,i),IT))
			
		!	  p(:) = Posit(:,IP_Tet(i,IT))
		
		      !pface needs splitting if p lies within the circumsphere of pface
			
		!	  pc = Geo3D_Sphere_Centre(pa(:),pb(:),pd(:))
	        
        !      IF (Geo3D_Distance_SQ(pc,pa) .GT. Geo3D_Distance_SQ(pc,p)) THEN			    
			    !We need to split
			    NB_Point = NB_Point + 1
			    IF (NB_Point>nallc_point) THEN
			      CALL ReAllocate_Point()
			    END IF
			    
		!	    Posit(:,NB_Point) = ( pa(:)+pb(:)+pd(:) ) / 3.0d0
				Posit(:,NB_Point) = Sphere_Tet(1:3,IT)
			    CALL Get_Point_Mapping(NB_Point)
				
				
			
	    !      END IF
		!	END IF
	    !  END DO
	    END IF
	  END DO
	  
	  !Now we need to actually insert the points
	  IF (Debug_Display>0) THEN
	    WRITE(*,*) 'Well centered split, Loop: ',globLoop
		WRITE(*,*) 'Number bad: ',numBad*100.0d0/NB_Tet,', Number to insert: ',NB_Point-NP_StartLoop
	    WRITE(29,*) 'Well centered split, Loop: ',globLoop
		WRITE(29,*) 'Number bad: ',numBad*100.0d0/NB_Tet,', Number to insert: ',NB_Point-NP_StartLoop
	  END IF
	  
	  IF ((NB_Point-NP_StartLoop).GT.0) THEN
	    CALL Get_Tet_SphereCross(0)
		CALL Get_Tet_Volume(0)
		Mark_Point(1:NB_Point) = 0
	    CALL Insert_Points_EasyPeasy(NP_StartLoop+1, NB_Point, 10)
	    usePower = .FALSE.
		CALL Remove_Point()     
        Mark_Point(1:NB_Point) = 0		
		CALL Next_Build()
		CALL Set_Domain_Mapping()
	  !ELSE
	    finished = 1
	  END IF
      	  
	END DO
	
END SUBROUTINE Well_Centered_Split