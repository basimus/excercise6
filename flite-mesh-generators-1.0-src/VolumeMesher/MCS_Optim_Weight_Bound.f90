!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  MCS_Optim_Weight_Bound
!!  
!!  This routine uses MCS to optimise weights of nodes in boundary elements the mesh
!!
!!  
!!
!!  Initial implimentation by S. Walton 29/10/1013
!!
!<
!*******************************************************************************
SUBROUTINE MCS_Optim_Weight_Bound()
	USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	
	INTEGER :: L,LMax,K,ie,ip,DD,N,Ibest,I,Loop,Istatus,Iin,Ni,Di,idum,isFinished,numLoops,ic,flag,nbp,improve,NB_old
	REAL*8, allocatable  :: optx(:), optl(:), optu(:), optg(:) ,ptemp(:),Pt(:,:),Qu(:),vardef(:,:),allPsave(:)
	REAL*8 :: dd2,ftemp,fbest,ranNum,d,FTarget,FBig,p1(3),dtet,psave,rsave,vol,ptet(3,4),p2(3),p3(3),p4(3)
	REAL*8 :: pp4(3,4),dum(4),coef,sumObj,lastObj,p1t(3),p2t(3)
	
	lastObj = HUGE(0.d0)
	
	improve = 1
	allocate(allPsave(NB_Point))
	!Get link info
	CALL Next_Build()
	CALL Get_Tet_Volume(0)
	CALL Set_Domain_Mapping()
	CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
	!Make sure mapping and geometry is all up to date
	CALL Get_Tet_SphereCross(0)
	
	LMax = 500
	L = 0
	DO WHILE((improve.EQ.1).AND.(L<LMax))
	
		FTarget = (1.D-6)*BGSpacing%MinSize
		FBig = 1.D30
		idum = -1
		sumObj = 0.d0
		
		
		
		
		
		!Set up the optimisation problem
		CALL init_random_seed()
		numLoops = 5 !Number of MCS loops
		DD = 1  !Number of dimensions - one for each space, going to scale by spacing
		N = 10  !Number of eggs
		
		allocate(ptemp(DD),Pt(DD,N),Qu(N),optx(DD), optl(DD), optu(DD), optg(DD),vardef(DD,2))
		
		allPsave(1:NB_Point) = RR_Point(1:NB_Point)
		
		
		!First node positions
		DO ip = 1, NB_Point
		

		
		
			!Get delta
			IF(BGSpacing%Model>0)THEN
					d = Scale_Point(ip)*BGSpacing%BasicSize
			ELSE IF(BGSpacing%Model<0)THEN
					d = MINVAL(fMap_Point(:,:,ip))*BGSpacing%BasicSize
			END IF
			
			!Get connected elements
			CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
		
			
			
			ptemp(1:DD) = 0.0d0
			Ibest = 0
			ftemp = 0.0d0
			I = 0
			Loop = 0
			Istatus = 0
			Iin = 0
			Qu(1:N) = huge(0.0)
			fbest = huge(0.0)

		
			!Upper and lower bounds are going to be 1*d
			optl(1:DD) = 1.0d0
			optu(1:DD) = -1.0d0
			
		
			!The last egg will be the initial position
			DO Ni = 1,N
				DO Di = 1,DD
				
					Pt(Di,Ni) = optl(Di) + Random_Gauss(idum)*(optu(Di)-optl(Di))
				
				
				ENDDO
				
			ENDDO
			
			Pt(1:DD,N) = 0.0d0
		
			isFinished = 0
			
			
			rsave = RR_Point(ip)
		
			DO WHILE (isFinished==0)
			
				
			
				CALL Optimising_Cuckoo_Driver(DD,N,ptemp,Ibest,ftemp,Pt,I,Loop,Istatus,Iin,Qu,fbest,optu,optl,idum)
				
				IF ((Loop-1).LT.numLoops) THEN
				
					!Calculate fitness at ptemp and put it into ftemp
					RR_Point(ip) = rsave + d*ptemp(1)
					!ftemp = ptemp(4)*ptemp(4)
					ftemp = 0
					flag = 0
					
					DO ic = 1,List_Length
					
						ie = ITs_List(ic)
						IF (Next_Tet(ip,ie)<0)THEN
							p1 = Posit(:,IP_Tet(1,ie))
							p2 = Posit(:,IP_Tet(2,ie))
							p3 = Posit(:,IP_Tet(3,ie))
							p4 = Posit(:,IP_Tet(4,ie))
							vol = Geo3D_Tet_Volume(p1,p2,p3,p4)
							IF(vol.LE.0.d0)THEN
								flag = 1
								ftemp = huge(0.d0)
								EXIT
							ELSE
								CALL Get_Tet_SphereCross(ie) 
								!Check if it's inside
								
								ptet(:,1) = p1
								ptet(:,2) = p2
								ptet(:,3) = p3
								ptet(:,4) = p4
								p1t = Sphere_Tet(:,ie)
								K=0
								dum =  Geo3D_Tet_Weight(K,ptet,p1t)
								
								IF (K==1) THEN
								!We are inside
									
								ELSE
									
									 
									 p2t = p1+p2+p3+p4 / 4.d0
									dd2 = Geo3D_Distance_SQ(p2t, p1t)
							
								
							
								
								
									ftemp = ftemp + dd2
									 
									 
								ENDIF

								
							ENDIF
						ENDIF
					
					ENDDO
					
					
					IF (flag.EQ.1) THEN
						ftemp = huge(0.d0)
					ENDIF
					IF (ftemp==0) THEN
						isFinished = 1
						EXIT
					ENDIF
					
				ELSE
				
					!The best result is in Pt(:,Ibest)
					RR_Point(ip) = rsave + d*Pt(1,Ibest)
					
					sumObj = sumObj + fbest
					isFinished = 1
					EXIT
				ENDIF
		
			ENDDO
			
			
		
		ENDDO
		deallocate(ptemp,Pt,Qu,optx, optl, optu, optg,vardef)
		CALL Get_Tet_SphereCross(0)
		
		
		IF(Debug_Display>0)THEN
			
			WRITE(*,*)'MCS Optim Objective funtion value:',sumObj
			WRITE(29,*)'MCS Optim Objective funtion value:',sumObj
		END IF
		
		IF (sumObj<lastObj) THEN
			lastObj = sumObj
			L = L+1
		
		ELSE
			allPsave(1:NB_Point) = RR_Point(1:NB_Point)
			
			CALL Get_Tet_SphereCross(0)
			
			improve = 0
			EXIT
		
		ENDIF

	
	ENDDO


END SUBROUTINE MCS_Optim_Weight_Bound




























