!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!   Allocate element-related arrays (under module common_Parameters).        \n
!!   @param[in] common_Parameters::nallc_Tet : the size.
!<
!*******************************************************************************
SUBROUTINE Allocate_Tet( )

  USE common_Parameters
  USE array_allocator
  IMPLICIT NONE

  CALL DeAllocate_Tet()
  CALL allc_2Dpointer(IP_Tet,     numTetNodes, nallc_Tet,   'IP_Tet')
  CALL allc_2Dpointer(NEXT_Tet,   4,nallc_Tet,   'NEXT_Tet')
  CALL allc_2Dpointer(Sphere_Tet, 4,nallc_Tet,   'Sphere_Tet')
  CALL allc_1Dpointer(Volume_Tet,   nallc_Tet,   'Volume_Tet')
  CALL allc_2Dpointer(Quality_Tet,4,nallc_Tet,   'Quality_Tet')
  CALL allc_1Dpointer(Mark_Tet,     nallc_Tet,   'Mark_Tet')
  CALL allc_1Dpointer(Visit_Tet,    nallc_Tet,   'Visit_Tet')
  CALL allc_1Dpointer(Scale_Tet,    nallc_Tet,   'Scale_Tet')
  IF(BGSpacing%Model<-1) CALL allc_3Dpointer(fMap_Tet, 3,3,nallc_Tet, 'fMap_Tet')

  IP_Tet(:,       1:nallc_Tet) = 0
  NEXT_Tet(:,     1:nallc_Tet) = 0
  Sphere_Tet(1:3, 1:nallc_Tet) = 0.D0
  Sphere_Tet(4,   1:nallc_Tet) = 1.D0
  Volume_Tet(     1:nallc_Tet) = 0.D0
  Mark_Tet(       1:nallc_Tet) = 0
  Visit_Tet(      1:nallc_Tet) = 0
  Scale_Tet(      1:nallc_Tet) = 1.D0
  IF(BGSpacing%Model<-1)THEN
     fMap_Tet(:,:,1:nallc_Tet) = 0.D0
  ENDIF



  RETURN

END SUBROUTINE Allocate_Tet

!*******************************************************************************
!>
!!   ReAllocate element-related arrays (under module common_Parameters).
!!   The size of arraies, common_Parameters::nallc_Tet, will be increased
!!      by a number of common_Parameters::nallc_increase in this subroutine.
!<
!*******************************************************************************
SUBROUTINE ReAllocate_Tet( )

  USE common_Parameters
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: n1

  n1 = nallc_Tet + 1
  nallc_Tet = nallc_Tet + nallc_increase
  CALL allc_2Dpointer(IP_Tet,     numTetNodes, nallc_Tet,   'IP_Tet')
  CALL allc_2Dpointer(NEXT_Tet,   4,nallc_Tet,   'NEXT_Tet')
  CALL allc_2Dpointer(Sphere_Tet, 4,nallc_Tet,   'Sphere_Tet')
  CALL allc_1Dpointer(Volume_Tet,   nallc_Tet,   'Volume_Tet')
  CALL allc_2Dpointer(Quality_Tet,4,nallc_Tet,   'Quality_Tet')
  CALL allc_1Dpointer(Mark_Tet,     nallc_Tet,   'Mark_Tet')
  CALL allc_1Dpointer(Visit_Tet,    nallc_Tet,   'Visit_Tet')
  CALL allc_1Dpointer(Scale_Tet,    nallc_Tet,   'Scale_Tet')
  IF(BGSpacing%Model<-1) CALL allc_3Dpointer(fMap_Tet, 3,3,nallc_Tet, 'fMap_Tet')

  IP_Tet(:,       n1:nallc_Tet) = 0
  NEXT_Tet(:,     n1:nallc_Tet) = 0
  Sphere_Tet(1:3, n1:nallc_Tet) = 0.D0
  Sphere_Tet(4,   n1:nallc_Tet) = 1.D0
  Volume_Tet(     n1:nallc_Tet) = 0.D0
  Mark_Tet(       n1:nallc_Tet) = 0
  Visit_Tet(      n1:nallc_Tet) = 0
  Scale_Tet(      n1:nallc_Tet) = 1.D0
  IF(BGSpacing%Model<-1)THEN
     fMap_Tet(:,:,n1:nallc_Tet) = 0.D0
  ENDIF



  RETURN

END SUBROUTINE ReAllocate_Tet
!*******************************************************************************
!>
!!   DeAllocate element-related arrays (under module common_Parameters).
!<
!*******************************************************************************
SUBROUTINE DeAllocate_Tet( )

  USE common_Parameters
  IMPLICIT NONE

  IF(ASSOCIATED(IP_Tet))     deallocate(IP_Tet)
  IF(ASSOCIATED(NEXT_Tet))   deallocate(NEXT_Tet)
  IF(ASSOCIATED(Sphere_Tet)) deallocate(Sphere_Tet)
  IF(ASSOCIATED(Volume_Tet)) deallocate(Volume_Tet)
  IF(ASSOCIATED(Quality_Tet))deallocate(Quality_Tet)
  IF(ASSOCIATED(Mark_Tet))   deallocate(Mark_Tet)
  IF(ASSOCIATED(Visit_Tet))  deallocate(Visit_Tet)
  IF(ASSOCIATED(Scale_Tet))  deallocate(Scale_Tet)
  IF(ASSOCIATED(fMap_Tet))   deallocate(fMap_Tet)



  RETURN

END SUBROUTINE DeAllocate_Tet

!*******************************************************************************
!>
!!   Allocate point-related arrays (under module common_Parameters).          \n
!!   @param[in] common_Parameters::nallc_Point : the size.
!<
!*******************************************************************************
SUBROUTINE Allocate_Point( )

  USE common_Parameters
  USE array_allocator
  IMPLICIT NONE

  CALL DeAllocate_Point()
  CALL allc_2Dpointer(Posit,       3, nallc_Point,   'Posit')
  CALL allc_1Dpointer(RR_Point,       nallc_Point,   'RR_Point')
  CALL allc_1Dpointer(Mark_Point,     nallc_Point,   'Mark_Point')
  CALL allc_1Dpointer(Scale_Point,    nallc_Point,   'Scale_Point')
  IF(BGSpacing%Model<-1) CALL allc_3Dpointer(fMap_Point, 3,3,nallc_Point, 'fMap_Point')

  Posit(:,      1:nallc_Point) = 0.D0
  RR_Point(     1:nallc_Point) = 0.D0
  Mark_Point(   1:nallc_Point) = 0
  Scale_Point(  1:nallc_Point) = 1.D0

  

  IF(BGSpacing%Model<-1)THEN
     fMap_Point(:,:,1:nallc_Point) = 0.D0
  ENDIF




  RETURN

END SUBROUTINE Allocate_Point

!*******************************************************************************
!>
!!   ReAllocate point-related arrays (under module common_Parameters).       
!!   The size of arraies, common_Parameters::nallc_Point, will be increased
!!      by a number of common_Parameters::nallc_increase in this subroutine.
!<
!*******************************************************************************
SUBROUTINE ReAllocate_Point( )

  USE common_Parameters
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: n1

  n1 = nallc_Point + 1
  nallc_Point = nallc_Point + nallc_increase
  CALL allc_2Dpointer(Posit,       3, nallc_Point,   'Posit')
  CALL allc_1Dpointer(RR_Point,       nallc_Point,   'RR_Point')
  CALL allc_1Dpointer(Mark_Point,     nallc_Point,   'Mark_Point')
  CALL allc_1Dpointer(Scale_Point,    nallc_Point,   'Scale_Point')
  IF(BGSpacing%Model<-1) CALL allc_3Dpointer(fMap_Point, 3,3,nallc_Point, 'fMap_Point')

  Posit(:,      n1:nallc_Point) = 0.D0
  RR_Point(     n1:nallc_Point) = 0.D0
  Mark_Point(   n1:nallc_Point) = 0
  Scale_Point(  n1:nallc_Point) = 1.D0
  IF(BGSpacing%Model<-1)THEN
     fMap_Point(:,:,n1:nallc_Point) = 0.D0
  ENDIF




  RETURN

END SUBROUTINE ReAllocate_Point
!*******************************************************************************
!>
!!   DeAllocate point-related arrays (under module common_Parameters)
!<
!*******************************************************************************
SUBROUTINE DeAllocate_Point( )

  USE common_Parameters
  IMPLICIT NONE

  IF(ASSOCIATED(Posit))        deallocate(Posit)
  IF(ASSOCIATED(RR_Point))     deallocate(RR_Point)
  IF(ASSOCIATED(Mark_Point))   deallocate(Mark_Point)
  IF(ASSOCIATED(Scale_Point))  deallocate(Scale_Point)
  IF(ASSOCIATED(fMap_Point))   deallocate(fMap_Point)



  RETURN

END SUBROUTINE DeAllocate_Point
