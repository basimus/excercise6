!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!    Insert a number of points to the present mesh by Delaunay method.
!!	  This routine does nothing clever, no IFs, just trys to insert the points
!!    NB1 to NB2.
!!	  Mark_Point will be used in this routine, any points marked -999 will be ignored
!!
!!    @param[in]  NB1,NB2   : The points to insert, i.e NB1<=IP<=NB2.
!!                            The coordinates of those points are given by
!!                            common_Parameters::Posit (:,NB1:NB2).
!!    @param[in] IT1    : List of original elements the points lie in
!!    @param[in] NoTry : Number of tries each point is given to enter the mesh
!!
!!    common_Parameters::NEXT_Tet is in need and updated.   
!<
!*******************************************************************************
SUBROUTINE Insert_Points_EasyPeasy(NB1,NB2,NoTry)
	USE common_Constants
	USE common_Parameters
	IMPLICIT NONE
	
	INTEGER, INTENT(IN) :: NB1, NB2, NoTry
	type(IntQueueType) :: IT1
	INTEGER :: ip,itry,Isucc,IT,listStart,Isucc1,ipp(4)
  REAL*8 :: w4(4)

	
	!Mark any point which not inserted with a 1
	WHERE (Mark_Point(NB1:NB2)/=-999) Mark_Point(NB1:NB2) = 1
	
	
	IT = -1

  ADTree_Search = .TRUE.
  CALL ADTree_SetName(ADTreeForMesh,'insEasy')
  CALL ADTree_SetTree(ADTreeForMesh, Posit, IP_Tet, NB_Tet, NB_Point)
	
	DO itry = 1,NoTry
	  DO ip = NB1,NB2	   
	     IF (Mark_Point(ip).EQ.1) THEN
	      

		  

      Isucc1 = -1
      CALL ADTree_SearchNode(ADTreeForMesh,Posit(1:3,ip),IT,ipp,w4, Isucc1)

		   ! IF (Isucc1.NE.0) THEN

          CALL Locate_Node_Tet(Posit(1:3,ip),IT,3)
          IF (IT.GT.0) THEN
  		      !Try to insert the point
  		      Isucc = 0
  		      CALL Insert_Point_Delaunay(ip,IT,Isucc,0)
  		      IF (Isucc.EQ.1) THEN
  		        Mark_Point(ip) = 0
  	          ENDIF
			     ENDIF
		    ENDIF
		  
	    ! ENDIF
	  ENDDO
      CALL Remove_Tet()
    ENDDO
	
	!Any left over points are failures 
	WHERE (Mark_Point(NB1:NB2).EQ.1) Mark_Point(NB1:NB2) = -999

	ADTree_Search = .FALSE.
  CALL ADTree_Clear(ADTreeForMesh)
	
	
END SUBROUTINE Insert_Points_EasyPeasy

!*******************************************************************************
!>
!!    Insert a number of points to the present mesh by Delaunay method.
!!	  This routine does nothing clever, no IFs, just trys to insert the points
!!    NB1 to NB2.
!!	  Mark_Point will be used in this routine, any points marked -999 will be ignored
!!
!!    @param[in]  NB1,NB2   : The points to insert, i.e NB1<=IP<=NB2.
!!                            The coordinates of those points are given by
!!                            common_Parameters::Posit (:,NB1:NB2).
!!    @param[in] IT1    : List of original elements the points lie in
!!    @param[in] NoTry : Number of tries each point is given to enter the mesh
!!
!!    common_Parameters::NEXT_Tet is in need and updated.   
!<
!*******************************************************************************
SUBROUTINE Insert_Points_Easy(NB1,NB2,NoTry,IT1)
	USE common_Constants
	USE common_Parameters
	IMPLICIT NONE
	
	INTEGER, INTENT(IN) :: NB1, NB2, NoTry
	type(IntQueueType) :: IT1
	INTEGER :: ip,itry,Isucc,IT,ITt,listStart
	
	!Mark any point which not inserted with a 1
	WHERE (Mark_Point(NB1:NB2)/=-999) Mark_Point(NB1:NB2) = 1
	
	
	IT = -1
	listStart = 1-NB1
	DO ITt=1,NB_Tet
		Mark_Tet(ITt) = ITt
	ENDDO
	DO itry = 1,NoTry
	  DO ip = NB1,NB2	   
	     IF (Mark_Point(ip).EQ.1) THEN
	      
		  !First the search
		  ITt = IT1%nodes(listStart+ip)
		  IF (Mark_Tet(ITt)/=0) THEN
		   IF (IP_Tet(4,Mark_Tet(ITt))>0) IT = Mark_Tet(ITt)
		  ENDIF
		  CALL Locate_Node_Tet_Cheap(Posit(1:3,ip),IT,3)
		    IF (IT>0) THEN
		      !Try to insert the point
		      Isucc = 0
		      CALL Insert_Point_Delaunay(ip,IT,Isucc,0)
		      IF (Isucc.EQ.1) THEN
		        Mark_Point(ip) = 0
	          ENDIF
			
		    ENDIF
		  
	     ENDIF
	  ENDDO
      
    ENDDO
	CALL Remove_Tet()
	
	!Any left over points are failures 
	WHERE (Mark_Point(NB1:NB2).EQ.1) Mark_Point(NB1:NB2) = -999

	
	
	
END SUBROUTINE Insert_Points_Easy

!*******************************************************************************
!>
!!    Insert a number of points to the present mesh by Delaunay method.
!!    @param[in]  NB1,NB2   : the insected points, i.e NB1<=IP<=NB2.
!!                            The coordinates of those points are given by
!!                            common_Parameters::Posit (:,NB1:NB2).
!!    @param[in]  Isucc = 1  : program stops if fail to insert a point.        \n
!!                      = 0  : mark those points failing to insert with -999. \n
!!                      =-1  : allow some points to fail to insert at first,
!!                          then try to insert them after others.
!!                      =-2  : -1 and 0 combined
!!
!!    common_Parameters::NEXT_Tet is in need and updated.   
!<
!*******************************************************************************
SUBROUTINE Insert_Points1(NB1,NB2,Isucc)
  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  USE Geometry3DAll
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: NB1, NB2, Isucc
  INTEGER :: i, IP, IT, Isucc1, IPfail, Nfail, Itry, NBD, jp, jpm
  REAL*8  :: d, dm
  INTEGER, DIMENSION(:), POINTER :: List_Fail

  IF(.NOT. CircumUpdated) CALL Get_Tet_Circum(0)
  IF(.NOT. VolumeUpdated) CALL Get_Tet_Volume(0)

  ALLOCATE(List_Fail(NB_Point))
  List_Fail(1:NB_Point) = 0

  WRITE(*,*)' '
  Mark_Point(NB1:NB2)  = 0
  IPfail = 0
  NBD = MAX(2,(NB2-NB1)/200)
  DO ip=NB1,NB2
     Isucc1 = 1
     IF(Isucc<=0) Isucc1 = 0
     IF( (Debug_Display>0 .AND. MOD(ip-NB1,50*NBD)==0) .OR.   &
          (Debug_Display>1 .AND. MOD(ip-NB1,10*NBD)==0) .OR.   &
          (Debug_Display>2 .AND. MOD(ip-NB1,   NBD)==0) .OR.   &
          Debug_Display>4 )THEN
        WRITE(*,'(a,I6,a,I6,a,I6,a$)') '  Insert point ',IP,   &
             ' out of points from ',NB1,' to ',NB2, CHAR(13)
     ENDIF
     IT = -1
     CALL Locate_Node_Tet(Posit(:,IP),IT,0)

     IF(IT<0)THEN
        IF((Isucc==0).or.(Isucc==-2))THEN
           Mark_Point(ip) = -999
           CYCLE
        ELSE
           WRITE(29,*)' '
           WRITE(29,*)'Error--- Fail to locate node IP:',IP,IT
           CALL Error_Stop ('Insert_Points1')
        ENDIF
     ENDIF
     CALL Insert_Point_Delaunay(IP,IT,Isucc1,0)

     IF(Isucc1<=0)THEN
        IF(Isucc==-1)THEN
           IF(Isucc1==0)THEN
             IF(Isucc==-2)THEN
                 Mark_Point(IP) = -999
             ELSE
              !--- fail to seraching a element to contains IP
              WRITE(29,*)'Error--- Fail to search node again IP:',IP
              CALL Error_Stop ('Insert_Points1  ')
             ENDIF
           ELSE IF(Isucc1==-1)THEN
              !--- fail to insert
              IPfail = IPfail + 1
              List_Fail(IPfail) = IP
           ELSE IF(Isucc1==-2)THEN
              !---  IP is too close to an old node
              DO i=1,4
                 jp = IP_Tet(i,IT)
                 d =  (Posit(1,IP)-Posit(1,jp))**2 + (Posit(2,IP)-Posit(2,jp))**2    &
                      + (Posit(3,IP)-Posit(3,jp))**2
                 IF(i==1 .OR. d<dm)THEN
                    jpm = jp
                    dm  = d
                 ENDIF
              ENDDO
              Mark_Point(ip) = jp
              IF(Debug_Display>2)THEN
                 WRITE(29,*)'Reminder: close points: ip,jp=',ip,jp,SQRT(dm)
              ENDIF
           ELSE IF(Isucc1==-3)THEN
            IF(Isucc==-2)THEN
              Mark_Point(IP) = -999
            ELSE     
              !--- IP is too close to boundary
              WRITE(29,*)'Error--- Inserting node is too close boundary. IP:',IP
              CALL Error_Stop ('Insert_Points1  ')
            ENDIF
           ENDIF
        ELSE IF(Isucc==0)THEN
           Mark_Point(ip) = -999
        ELSE
            IF(Isucc==-2)THEN
              Mark_Point(IP) = -999
            ELSE

           WRITE(29,*)'Error--- Fail about Isucc=',Isucc
           CALL Error_Stop ('Insert_Points1')
           ENDIF
        ENDIF
     ENDIF
  ENDDO
  WRITE(*,*)' '

  !--- Remove those false tetrahedra
  CALL Remove_Tet()

  !--- insert those nodes failed to insert before
  IF(IPfail>0)THEN
     DO Itry = 1,3
        WRITE(29,*)'Unhappy--- ',IPfail,' points fail to insert, try again'
        Nfail  = IPfail
        IPfail = 0
        DO i=1,Nfail
           Isucc1 = 0
           IF(Itry==3) Isucc1 = 1
           IP = List_Fail(i)
           CALL Locate_Node_Tet(Posit(:,IP),IT,1)
           CALL Insert_Point_Delaunay(IP,IT,Isucc1,0)
           IF(Isucc1<=0)THEN
              IPfail = IPfail + 1
              List_Fail(IPfail) = IP
           ENDIF
        ENDDO
        CALL Remove_Tet()
        IF(IPfail==0) EXIT
     ENDDO
  ENDIF

  IF((IPfail>0).and.(Isucc==-1))THEN
     WRITE(29,*)'Error--- : some points still can not be inserted'
     CALL Error_Stop ('Insert_Points1')
  
  ELSEIF(IPfail>0)THEN

     DO i=1,IPfail

       Mark_Point(List_Fail(i)) = -999
     ENDDO

  ENDIF

  

  DEALLOCATE(List_Fail)

  RETURN
END SUBROUTINE Insert_Points1

!*******************************************************************************
!>
!!    Insert the points from a tetrahedral mesh
!!         to the present mesh by Delaunay method.
!!    @param[in]  TetMesh    :   TetMeshStorageType.
!!    @param[in]  Isucc = 1  : program stops if fail to insert a point.        \n
!!                      = 0  : discard those points failing to insert.
!!
!!    common_Parameters::NEXT_Tet is in need and updated.   
!<
!*******************************************************************************
SUBROUTINE Insert_Points2(TetMesh, Isucc)
  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  USE Geometry3DAll
  IMPLICIT NONE

  INTEGER, INTENT(IN) ::  Isucc
  TYPE(TetMeshStorageType),  INTENT(IN) :: TetMesh
  INTEGER :: NB1, NB2, NBD, IP, IT, ipp(4), Isucc1, ip3(3), IB, i
  REAL*8  :: P0(3), w4(4)
  INTEGER, DIMENSION(:), POINTER :: ipdone
  INTEGER :: i0, i1, i2, i3, i4, i5

  !  IF(useStretch .AND. BGSpacing%Model/=1)THEN
  !     WRITE(29,*)' '
  !     WRITE(29,*)'Error--- no stretch only for this insert'
  !     CALL Error_Stop ('Insert_Points2')
  !  ENDIF

  IF(.NOT. CircumUpdated) CALL Get_Tet_Circum(0)
  IF(.NOT. VolumeUpdated) CALL Get_Tet_Volume(0)

  ADTree_Search = .TRUE.
  CALL ADTree_SetName(ADTreeForMesh, 'mesh_Insert_Points2')
  CALL ADTree_SetTree(ADTreeForMesh, Posit, IP_Tet, NB_Tet, NB_Point)
  CALL Surface_Edge()

  ALLOCATE(ipdone(TetMesh%NB_Point))
  ipdone(:) = 0

  NB1 = 1
  NB2 = TetMesh%NB_Point
  NBD = MAX(2,(NB2-NB1)/200)
  IT = 0
  Loop_IP : DO ip = NB1, NB2
     IF(  (Debug_Display>0 .AND. MOD(ip-NB1,50*NBD)==0) .OR.   &
          (Debug_Display>1 .AND. MOD(ip-NB1,10*NBD)==0) .OR.   &
          (Debug_Display>2 .AND. MOD(ip-NB1,   NBD)==0) .OR.   &
          (Debug_Display>4) )THEN
        WRITE(*,'(a,I7,a,I7,a,I7,a$)') ' Insert point ',IP, ' out of ',  &
             NB2, ' points with NB_Point= ',NB_Point, CHAR(13)
     ENDIF

     P0 = TetMesh%Posit(:,ip)

     !--- check if P0 locating in the original domain
     Isucc1 = -1
     CALL ADTree_SearchNode(ADTreeForMesh,P0,IT,ipp,w4, Isucc1)
     IF(Isucc1==0)THEN
        !--- fail to locate
        ipdone(IP) = -1
        CYCLE  Loop_IP
     ENDIF
     IF(MAX(w4(1),w4(2),w4(3),w4(4))>0.999)THEN
        !--- close to a nodes
        ipdone(IP) = -2
        CYCLE  Loop_IP
     ENDIF

     IF(MIN(w4(1),w4(2),w4(3),w4(4))<0.001)THEN
        DO i = 1, 4
           IF(w4(i)<0.001)THEN
              IF(Next_Tet(i,IT)<0)THEN
                 !---close to a boundary triangle
                 ipdone(IP) = -3
                 CYCLE Loop_IP
              ENDIF
           ENDIF
        ENDDO
        DO i = 1, 6
           IF(w4(I_Comb_Tet(1,i))<0.001 .AND. w4(I_Comb_Tet(2,i))<0.001)THEN
              ip3(1:2) = ipp(I_Comb_Tet(3:4,i))
              CALL Boundary_Edg_Match(ip3(1),ip3(2),ib)
              IF(ib>0)THEN
                 !---close to a boundary edge
                 ipdone(IP) = -4
                 CYCLE Loop_IP
              ENDIF
           ENDIF
        ENDDO
     ENDIF


     NB_Point = NB_Point + 1
     IF(NB_Point> nallc_Point) CALL ReAllocate_Point( )
     Posit(:,NB_Point) = P0

     IF(useStretch .AND. (BGSpacing%Model<-1 .OR. BGSpacing%Model>1))THEN
        CALL Get_Point_Mapping(NB_Point)
     ENDIF

     Isucc1 = Isucc
     CALL Insert_Point_Delaunay(NB_Point,IT,Isucc1,0)
     IF(Isucc1<=0)THEN
        !--- fail to insert this node
        ipdone(IP) = -5
        NB_Point = NB_Point -1
        CYCLE Loop_IP
     ENDIF

     ipdone(IP) = 1
  ENDDO Loop_IP
  WRITE(*,*)' '

  IF(Debug_Display>1)THEN
     i0 = 0
     i1 = 0
     i2 = 0
     i3 = 0
     i4 = 0
     i5 = 0
     DO IP = NB1, NB2
        IF(ipdone(IP)== 1) i0 = i0+1
        IF(ipdone(IP)==-1) i1 = i1+1
        IF(ipdone(IP)==-2) i2 = i2+1
        IF(ipdone(IP)==-3) i3 = i3+1
        IF(ipdone(IP)==-4) i4 = i4+1
        IF(ipdone(IP)==-5) i5 = i5+1
     ENDDO
     IF(i0+i1+i2+i3+i4+i5/=NB2-NB1+1) CALL Error_Stop ('  Insert_Points2: i-count')
     IF(i1>0) WRITE(29,*)'No. nodes failling to locate:          ', i1
     IF(i2>0) WRITE(29,*)'No. nodes too close to other nodes:    ', i2
     IF(i3>0) WRITE(29,*)'No. nodes too close to boundary faces: ', i3
     IF(i4>0) WRITE(29,*)'No. nodes too close to boundary edges: ', i4
     IF(i5>0) WRITE(29,*)'No. nodes failling to insert:          ', i5
     IF(i0>0) WRITE(29,*)'No. nodes inserted:                    ', i0
  ENDIF

  ADTree_Search = .FALSE.
  CALL ADTree_Clear(ADTreeForMesh)

  !--- Remove those false tetrahedra
  CALL Remove_Tet()

  DEALLOCATE(ipdone)

  RETURN
END SUBROUTINE Insert_Points2


!*******************************************************************************
!>
!!    Insert a number of points to the present mesh by Delaunay method using
!!    ADTree
!!    @param[in]  NB1,NB2   : the insected points, i.e NB1<=IP<=NB2.
!!                            The coordinates of those points are given by
!!                            common_Parameters::Posit (:,NB1:NB2).
!!    @param[in]  Isucc = 1  : program stops if fail to insert a point.        \n
!!                      = 0  : mark those points failing to insert with -999. \n
!!                      =-1  : allow some points to fail to insert at first,
!!                          then try to insert them after others.
!!                      =-2  : -1 and 0 combined
!!
!!    common_Parameters::NEXT_Tet is in need and updated.   
!<
!*******************************************************************************
SUBROUTINE Insert_Points3(NB1,NB2,Isucc)
  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  USE Geometry3DAll
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: NB1, NB2, Isucc
  INTEGER :: i, IP, IT, Isucc1, IPfail, Nfail, Itry, NBD, jp, jpm,ipp(4)
  REAL*8  :: d, dm, w4(4),P0(3)
  INTEGER, DIMENSION(:), POINTER :: List_Fail

  IF(.NOT. CircumUpdated) CALL Get_Tet_Circum(0)
  IF(.NOT. VolumeUpdated) CALL Get_Tet_Volume(0)

  ADTree_Search = .TRUE.
  CALL ADTree_SetName(ADTreeForMesh, 'mesh_Insert_Points3')
  CALL ADTree_SetTree(ADTreeForMesh, Posit, IP_Tet, NB_Tet, NB_Point)
  CALL Surface_Edge()
  
  
  ALLOCATE(List_Fail(NB_Point))
  List_Fail(1:NB_Point) = 0

  WRITE(*,*)' '
  Mark_Point(NB1:NB2)  = 0
  IPfail = 0
  NBD = MAX(2,(NB2-NB1)/200)
  DO ip=NB1,NB2
     Isucc1 = 1
     IF(Isucc<=0) Isucc1 = 0
    
     IT = -1
       P0 = Posit(:,ip)
       Isucc1 = -1
       CALL ADTree_SearchNode(ADTreeForMesh,P0,IT,ipp,w4, Isucc1)
       
     !CALL Locate_Node_Tet(Posit(:,IP),IT,0)

     IF(IT<0)THEN
        IF((Isucc1==0))THEN
           Mark_Point(ip) = -999
           CYCLE
        ELSE
           WRITE(29,*)' '
           WRITE(29,*)'Error--- Fail to locate node IP:',IP,IT
           CALL Error_Stop ('Insert_Points1')
        ENDIF
     ENDIF
     Isucc1=0
     CALL Insert_Point_Delaunay(IP,IT,Isucc1,0)

     IF(Isucc1<=0)THEN
        IF(Isucc==-1)THEN
           IF(Isucc1==0)THEN
             IF(Isucc==-2)THEN
                 Mark_Point(IP) = -999
             ELSE
              !--- fail to seraching a element to contains IP
              WRITE(29,*)'Error--- Fail to search node again IP:',IP
              CALL Error_Stop ('Insert_Points1  ')
             ENDIF
           ELSE IF(Isucc1==-1)THEN
              !--- fail to insert
              IPfail = IPfail + 1
              List_Fail(IPfail) = IP
           ELSE IF(Isucc1==-2)THEN
              !---  IP is too close to an old node
              DO i=1,4
                 jp = IP_Tet(i,IT)
                 d =  (Posit(1,IP)-Posit(1,jp))**2 + (Posit(2,IP)-Posit(2,jp))**2    &
                      + (Posit(3,IP)-Posit(3,jp))**2
                 IF(i==1 .OR. d<dm)THEN
                    jpm = jp
                    dm  = d
                 ENDIF
              ENDDO
              Mark_Point(ip) = jp
              IF(Debug_Display>2)THEN
                 WRITE(29,*)'Reminder: close points: ip,jp=',ip,jp,SQRT(dm)
              ENDIF
           ELSE IF(Isucc1==-3)THEN
            IF(Isucc==-2)THEN
              Mark_Point(IP) = -999
            ELSE     
              !--- IP is too close to boundary
              WRITE(29,*)'Error--- Inserting node is too close boundary. IP:',IP
              CALL Error_Stop ('Insert_Points1  ')
            ENDIF
           ENDIF
        ELSE IF(Isucc==0)THEN
           Mark_Point(ip) = -999
        ELSE
            IF(Isucc==-2)THEN
              Mark_Point(IP) = -999
            ELSE

           WRITE(29,*)'Error--- Fail about Isucc=',Isucc
           CALL Error_Stop ('Insert_Points1')
           ENDIF
        ENDIF
     ENDIF
  ENDDO
  WRITE(*,*)' '

  !--- Remove those false tetrahedra
  ADTree_Search = .FALSE.
  CALL ADTree_Clear(ADTreeForMesh)
  CALL Remove_Tet()

  !--- insert those nodes failed to insert before
  IF(IPfail>0)THEN
     DO Itry = 1,3
        WRITE(29,*)'Unhappy--- ',IPfail,' points fail to insert, try again'
        Nfail  = IPfail
        IPfail = 0
        DO i=1,Nfail
           Isucc1 = 0
           IF(Itry==3) Isucc1 = 1
           IP = List_Fail(i)
           CALL Locate_Node_Tet(Posit(:,IP),IT,1)
           CALL Insert_Point_Delaunay(IP,IT,Isucc1,0)
           IF(Isucc1<=0)THEN
              IPfail = IPfail + 1
              List_Fail(IPfail) = IP
           ENDIF
        ENDDO
        CALL Remove_Tet()
        IF(IPfail==0) EXIT
     ENDDO
  ENDIF

  IF((IPfail>0).and.(Isucc==-1))THEN
     WRITE(29,*)'Error--- : some points still can not be inserted'
     CALL Error_Stop ('Insert_Points1')
  
  ELSEIF(IPfail>0)THEN

     DO i=1,IPfail

       Mark_Point(List_Fail(i)) = -999
     ENDDO

  ENDIF

  

  DEALLOCATE(List_Fail)

  RETURN
END SUBROUTINE Insert_Points3

!*******************************************************************************
!>
!!   Insert a single point, IP, to the present mesh by Delaunay method.
!!   (Search a cavity of a new point and split the cavity forming balls.)
!!
!!   @param[in]  IP  :  the insected point.
!!                           The coordinates of IP are given by
!!                           common_Parameters::Posit (:,IP).
!!   @param[in]  IT0 > 0 : the initial tetrahedron in which point IP lies.     \n
!!                   = 0 : program will do search for the initial tetrahedron. \n
!!                   =-1 : point IP locates on an edge, and all the tetrahedra
!!                         surrounding have been given by module Pole_Surround.
!!   @param[in]  Isucc = 1  : program stops if fail to insert this point.       \n
!!                     = 0  : subroutine return if fail to insert this point.
!!                            Higher quality standard applied for these case.
!!   @param[in]  Key   = 3  : save the new elements by Pole_Surround::NB_PoleTet \n
!!                            and Pole_Surround::IT_PoleTet.
!!   @param[out] Isucc = 1  : successful to insert this point.                 \n
!!                     = 0  : fail to insert at searching element.             \n
!!                     =-1  : fail to insert due to bringing up flat elements. \n
!!                     =-2  : fail to insert duo to IP close an old point.     \n
!!                     =-3  : fail to insert duo to IP close boundary.
!!
!!   common_Parameters::NEXT_Tet is in need and updated.                       \n
!!   Function common_Parameters::GetVisitCount() are applied here.
!<
!*******************************************************************************
SUBROUTINE Insert_Point_Delaunay(IP,IT0,Isucc,Key)

  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  USE Geometry3DAll
  USE array_allocator
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: IP, IT0, Key
  INTEGER, INTENT(INOUT) :: Isucc
  INTEGER, PARAMETER :: Max_Ball   = 80 * Max_PoleTet
  INTEGER, PARAMETER :: Limit_Side = 40 * Max_PoleTet
  INTEGER :: IT_Cavity(Max_Ball), Nins, Isucc1
  INTEGER :: IP_Hull(3, Max_Ball), Nside, Nside0
  INTEGER :: ID_Hull(2, Max_Ball)
  INTEGER :: IP_Edge(4, 2*Max_Ball), Nedge
  REAL*8  :: Vol_Hull(Max_Ball)
  REAL*8  :: fMap(3,3), weights(4)
  INTEGER :: i, j, it, it1, it2, i1, i2, i3, i4, ip1, ip2, ip3, ip4, ind, ipp(4)
  INTEGER :: isd, Ns1, Ns2
  REAL*8  :: P0(3), p1(3), p2(3), p3(3)
  REAL*8  :: RAfac,  RAsq, Criterion_in_sphere, v, SmallV, Scalar
  LOGICAL :: completed
  INTEGER, DIMENSION(:,:),  POINTER :: IP_Tet_old
  INTEGER :: ThisVisit, iV

  IF(.NOT. CircumUpdated) CALL Error_Stop ('Insert_Point_Delaunay: .NOT. CircumUpdated')
  IF(.NOT. VolumeUpdated) CALL Error_Stop ('Insert_Point_Delaunay: .NOT. VolumeUpdated')

  ThisVisit = GetVisitCount()

  IP_Tet_old => null()
  !--------------------------------------------
  !     Find the cavity of a new point  
  !     Output:  IT_Cavity(*) : a serial of tetrahedra which compose a cavity
  !              Nins         : the length of IT_Cavity 
  !              IP_Hull(3,*) : three nodes of each face of the hull
  !              Nside        : the length of IP_Hull, i.e. number of faces
  !              ID_Hull(2,*) : original tetrahedra of each face in cavity,
  !                             and the direction
  !--------------------------------------------

  !---- Criterion of splitting

  RAfac  = 0.d0

  !---- Initialise a hull

  P0(:) = Posit(:,IP)

  IF(IT0>0)THEN
     !--- insert a node in a specified element
     it1 = IT0
  ELSE IF(IT0==0)THEN
     !--- search an element & insert a node in it
     IF(ADTree_Search)THEN
        Isucc1 = -1
        CALL ADTree_SearchNode(ADTreeForMesh,P0,it1,ipp,weights, Isucc1)
        IF(Isucc1==0) THEN
		  !Do this the old fashsioned way
		  CALL Locate_Node_Tet(P0,it1,Isucc)
		  
		
		ELSE
          IF((Debug_Display>3).AND.(it1.GT.0))THEN
             IF(  IP_Tet(1,it1)/=ipp(1) .OR. IP_Tet(2,it1)/=ipp(2) .OR.  &
                  IP_Tet(3,it1)/=ipp(3) .OR. IP_Tet(4,it1)/=ipp(4) )THEN
                WRITE(29,*)'Error--- it1,ipp,ips=',it1,ipp,IP_Tet(:,it1)
                CALL Error_Stop ('Insert_Point_Delaunay')
             ENDIF
          ENDIF
          IF(Debug_Display>2 .AND. Isucc1==0)THEN
             WRITE(29,*)' '
             WRITE(29,*)'Warning---  IP ',IP, ' found out all cell ', it1
             WRITE(29,*)'       weights=',weights
          ENDIF
		ENDIF
     ELSE
        it1 = 0
        Isucc1 = Isucc
        CALL Locate_Node_Tet(P0,it1,Isucc1)
     ENDIF

     IF(it1<=0)THEN
        IF(Isucc==0)THEN
           RETURN
        ELSE
           WRITE(29,*)' Error--- fail to locate a point, ip=',ip,' it1=',it1
           CALL Error_Stop (' Insert_Point_Delaunay ')
        ENDIF
     ENDIF
  ELSE
     !--- insert a node on an edge definated by IT_PoleTet(:)
     it1 = IT_PoleTet(1)
  ENDIF

  IF(IT0==0)THEN
     !--- called from Boundary_Treat to insert boundary nodes
     SmallV = 0.d0
  ELSE
     !--- called from Element_Break / Edge_Break
     SmallV = MIN( 1.D-3 * Volume_Tet(it1),  SmallVolume )
  ENDIF

  IF(IT0>=0)THEN
     NB_PoleTet    = 1
     IT_PoleTet(1) = it1

     DO i=1,4
        ip1 = IP_Tet(iTri_Tet(1,i),it1)
        ip2 = IP_Tet(iTri_Tet(2,i),it1)
        ip3 = IP_Tet(iTri_Tet(3,i),it1)
        p1(:) = Posit(:,ip1)
        p2(:) = Posit(:,ip2)
        p3(:) = Posit(:,ip3)

        IF(BGSpacing%Model==1 .OR. (.NOT. useStretch))THEN
           v = Geo3D_Tet_Volume_Pred(p1,p2,p3,p0)
        ELSE IF(BGSpacing%Model<0)THEN
           IF(BGSpacing%Model==-1)THEN
              fMap(:,:) = BGSpacing%BasicMap(:,:)
           ELSE
              fMap(:,:) = fMap_Tet(:,:,it1)
           ENDIF
           v = Mapping3D_Tet_Volume_Pred(p1,p2,p3,p0,fMap)
        ELSE IF(BGSpacing%Model>1)THEN
           Scalar   = Scale_Tet(it1)
           v = Geo3D_Tet_Volume_Pred(p1,p2,p3,p0) / Scalar**3
        ENDIF

        IF(v<=MIN(TinySize,SmallV))THEN
           it2 = NEXT_Tet(i,it1)
           IF(it2<=0)THEN
              IF(Isucc==0)THEN
                 Isucc = -3
                 RETURN
              ELSE
                 WRITE(29,*)' '
                 WRITE(29,*)'Error--- : A point on boundary, IP,IT0,it1=',IP,IT0,it1
                 WRITE(29,*)' v, TinySize,SmallV=',REAL(v),REAL(TinySize),REAL(SmallV)
                 WRITE(29,*)'  Pt=', real(Posit(:,IP))
                 WRITE(29,*)'  IP_Tet=',IP_Tet(:,it1),i
                 CALL Error_Stop ('Insert_Point_Delaunay')
              ENDIF
           ELSE
              NB_PoleTet = NB_PoleTet + 1
              IT_PoleTet(NB_PoleTet) = it2
           ENDIF
        ENDIF
     ENDDO

     IF(NB_PoleTet>=4)THEN
        IF(Isucc==0)THEN
           Isucc = -2
           RETURN
        ELSE
           WRITE(29,*)'Error--- : the new point too close an old point'
           WRITE(29,*)'IP=',IP, ' IT=',it1, ' ips=',ip_tet(:,it1)
           WRITE(29,*)'p0=',P0
           WRITE(29,*)'p1=',posit(:,ip_tet(1,it1))
           WRITE(29,*)'p2=',posit(:,ip_tet(2,it1))
           WRITE(29,*)'p3=',posit(:,ip_tet(3,it1))
           WRITE(29,*)'p4=',posit(:,ip_tet(4,it1))
           CALL Error_Stop ('Insert_Point_Delaunay')
        ENDIF
     ELSE IF(NB_PoleTet==3)THEN
        DO ind=1,6
           ip1 = IP_Tet(I_Comb_Tet(1,ind),it1)
           ip2 = IP_Tet(I_Comb_Tet(2,ind),it1)
           Ns1 = 0
           DO Nins = 2,3
              it2 = IT_PoleTet(Nins)
              DO i=1,4
                 IF(IP_Tet(i,it2)==ip1) Ns1 = Ns1+1
                 IF(IP_Tet(i,it2)==ip2) Ns1 = Ns1+1
              ENDDO
           ENDDO
           IF(Ns1 == 4)THEN
              CALL Search_Pole_Surround(it1, ind)
              IF(NB_PolePt>NB_PoleTet)THEN
                 IF(Isucc==0)THEN
                    Isucc = -3
                    RETURN
                 ELSE           
                    WRITE(29,*)'Error--- : boundary egde NB_PolePt>NB_PoleTet:',NB_PolePt,NB_PoleTet
                    WRITE(29,*)'IP=',IP, 'ip1=',ip1, 'ip2=',ip2
                    WRITE(29,*)'p0=',posit(:,IP)
                    WRITE(29,*)'p1=',posit(:,ip1)
                    WRITE(29,*)'p2=',posit(:,ip2)
                    WRITE(29,*)'p3=',posit(:,IP_Tet(I_Comb_Tet(3,ind),it1))
                    WRITE(29,*)'p4=',posit(:,IP_Tet(I_Comb_Tet(4,ind),it1))
                    CALL Error_Stop ('Insert_Point_Delaunay')
                 ENDIF
              ENDIF
              EXIT
           ENDIF
        ENDDO
     ENDIF

  ENDIF

  DO Nins = 1,NB_PoleTet
     it1 = IT_PoleTet(Nins)   !--- a cavity element
     IT_Cavity(Nins) = it1    !--- cavity elements series
     Visit_Tet(it1)  = ThisVisit      !--- mark a cavity element
  ENDDO

  Nside = 0
  DO Nins = 1,NB_PoleTet
     it1 = IT_Cavity(Nins)
     DO i=1,4
        it2 = Next_Tet(i, it1)
        IF(it2>0)THEN
           IF(Visit_Tet(it2)==ThisVisit) CYCLE
        ENDIF
        
        Nside= Nside+1
        !--- connectivety of a hull triangle
        IP_Hull(1:3,Nside) = IP_Tet(iTri_Tet(1:3,i),it1)
        !--- a cavity elements to which a hull triangle belong.
        ID_Hull(1:2,Nside) = (/it1, i/)
     ENDDO
  ENDDO

  Nins   = NB_PoleTet
  Nside0 = Nside

  !---- Expand the hull if possible

  Ns1 = 0

  DO WHILE(Nside>Ns1)

     Ns2 = Nside

     DO isd=Ns1+1,Ns2

        !--- check the new hull triangles only
        it1 = ID_Hull(1,isd)
        i1  = ID_Hull(2,isd)
        it2 = NEXT_Tet(i1,it1)
        IF(it2<=0) CYCLE
        IF(ABS(Visit_Tet(it2))==ThisVisit) CYCLE     !--- a checked element

        IF(isd>Limit_Side)THEN
           !--- mark a outside element of the over-full hull
           Visit_Tet(it2) = -ThisVisit
           CYCLE
        ENDIF

        RAsq = Criterion_in_Sphere(IP,it2)

        IF(RAsq < RAfac)THEN
           !---- Add a tetrahedron into the hull and expand the hull surface
           Nins = Nins+1
           Visit_Tet(it2)  = ThisVisit       !-- mark a cavity element
           IT_Cavity(Nins) = it2             !-- add a cavity element in the series

           DO i=1,4
              Nside = Nside+1
              IP_Hull(1:3,Nside) = IP_Tet(iTri_Tet(1:3,i),it2)
              ID_Hull(1:2,Nside) = (/it2, i/)
           ENDDO
        ELSE
           Visit_Tet(it2) = -ThisVisit   !--- mark a outside element of the hull
        ENDIF

        IF( MAX(Nins, Nside) > Max_Ball-4 )THEN
           WRITE(29,*)'Error--- : reset Max_Ball please'
           WRITE(29,*)'IP,Nins,Nside=',IP,Nins,Nside
           CALL Error_Stop ('Insert_Point_Delaunay')
        ENDIF

     ENDDO
     Ns1 = Ns2

  ENDDO

  !--- remove those triangles resulting in small elements.

  completed = .FALSE.
  DO WHILE(.NOT. completed)
     completed = .TRUE.
     Ns2 = Nside

     DO isd=Ns2, 1, -1
        it1 = ID_Hull(1,isd)

        IF(Visit_Tet(it1)/=ThisVisit)THEN
           !--- a removed elements, discard its triangles from the hull surface.
           completed = .FALSE.
           IF(isd/=Nside)THEN
              ID_Hull(:,isd) = ID_Hull(:,Nside)
              IP_Hull(:,isd) = IP_Hull(:,Nside)
              Vol_Hull( isd) = Vol_Hull( Nside)
           ENDIF
           Nside = Nside-1
           CYCLE
        ENDIF

        IF(ID_Hull(2,isd)<0) CYCLE    !--- skip a checked triangle

        i   = ID_Hull(2,isd)
        it2 = Next_Tet(i,it1)
        IF(it2>0)THEN
           IF(Visit_Tet(it2)==ThisVisit) CYCLE    !--- skip a insided triangle
        ENDIF

        !--- calculate the volume of a possibly ball
        ipp(1:3) = IP_Hull(1:3,isd)
        ipp(4)   = IP
        Vol_Hull(isd) = LocalVol()

        IF(isd>Nside0 .AND. Vol_Hull(isd)<=MIN(SmallV,Volume_Tet(it1)) )THEN
           !--- remove this element from cavity
           Visit_Tet(it1) = -ThisVisit
           IF(isd/=Nside)THEN
              ID_Hull(:,isd) = ID_Hull(:,Nside)
              IP_Hull(:,isd) = IP_Hull(:,Nside)
              Vol_Hull( isd) = Vol_Hull( Nside)
           ENDIF
           Nside = Nside-1
           completed = .FALSE.
        ELSE
           ID_Hull(2,isd) = - ID_Hull(2,isd)  !--- mark triangle checked
        ENDIF

     ENDDO

  ENDDO

  !--- recount the boundary triangles and the cavity of the hull

  Ns1=0
  DO isd =1,Nside
     it1 = ID_Hull(1,isd)
     IF(Visit_Tet(it1)/=ThisVisit)THEN
        WRITE(29,*)'Error--- : wrong here--1'
        WRITE(29,*)'IP=',IP,' it1=',it1,' mark=',Visit_Tet(it1)
        CALL Error_Stop ('Insert_Point_Delaunay')
     ENDIF
     i = ABS(ID_Hull(2,isd))
     it2 = Next_Tet(i,it1)
     iV = 0
     IF(it2>0) iV = Visit_Tet(it2)
     IF(iV/=ThisVisit)THEN
        IF(ID_Hull(2,isd)>0)THEN
           WRITE(29,*)'Error--- : wrong here--2'
           WRITE(29,*)'IP=',IP,' it1=',it1,' it2=',it2,' isd=',isd,' ID2=',ID_Hull(2,isd)
           CALL Error_Stop ('Insert_Point_Delaunay')
        ENDIF
        !--- add a hull triangl
        Ns1=Ns1+1
        IP_Hull(:,Ns1) = IP_Hull(:,isd)
        ID_Hull(:,Ns1) = ABS(ID_Hull(:,isd))
        Vol_Hull( Ns1) = Vol_Hull(isd)

        IF(Isucc==1)THEN
           !--- success only
           IF(Vol_Hull(Ns1)<0.d0)THEN
              !--- Tiny or negative value
              WRITE(29,*)' '
              WRITE(29,*)'Error--- : An ill element appears:                             '
              WRITE(29,*)'IP=',IP,'isd=',isd,'ip_hull=',ip_hull(:,isd)
              WRITE(29,*)'Vol=',Vol_Hull(Ns1)
              CALL Error_Stop ('Insert_Point_Delaunay')
           ENDIF
        ELSE
           !--- if not success, give it up and try later
           IF(Vol_Hull(Ns1)<SmallV)THEN
              Isucc = -1
              RETURN
           ENDIF
        ENDIF

     ELSE IF(Visit_Tet(it2)==ThisVisit)THEN
        !--- a triangle between two cavity element, skip
     ELSE
        WRITE(29,*)'Error--- : wrong here--2'
        WRITE(29,*)'IP=',IP,' it1,it2=',it1,it2,' mark=',Visit_Tet(it2)
        CALL Error_Stop ('Insert_Point_Delaunay')
     ENDIF

  ENDDO
  Nside = Ns1

  NS1 = 0
  DO isd=1,Nins
     it1=IT_Cavity(isd)
     IF(Visit_Tet(it1)==ThisVisit)THEN
        Ns1=Ns1+1
        IT_Cavity(Ns1)=it1
     ENDIF
  ENDDO
  Nins = Ns1


  !--------------------------------------------
  !     Split a cavity into tetrahedra   
  !     Input:  IP  :  the central point of the cavity  
  !             IT_Cavity(*) : a serial of tetrahedra which compose a cavity
  !                            This array will be modified
  !             Nins         : the length of IT_Cavity  
  !             IP_Hull(3,*) : three nodes of each face of the hull
  !             Nside        : the length of IP_Hull, i.e. number of edges
  !             ID_Hull(2,*) : original tetrahedra of each face in cavity,
  !                             and the direction
  !--------------------------------------------

  !--- prepare for ADTree resetting
  IF(ADTree_Search)THEN
     CALL allc_2Dpointer( IP_Tet_old, 4, Nins, 'IP_Tet_old')
     DO isd = 1,Nins
        IT = IT_Cavity(isd)
        IP_Tet_old(:,isd) = IP_Tet(:,IT)
     ENDDO
  ENDIF

  !---- Split the cavity

  DO isd = 1,Nside
     IF(isd<=Nins)THEN
        IT = IT_Cavity(isd)
		!There is no change
     ELSE
	    
        NB_Tet = NB_Tet+1
        IF(NB_Tet>=nallc_Tet) CALL ReAllocate_Tet()
		
        IT = NB_Tet
        IT_Cavity(isd) = IT
     ENDIF

     !--- set new cell, put the new point at the 4th node of IP_Tet(:,IT),
     !--- this is used in some subroutines like Element_Break & Edge_Break
     IP_Tet(1:3,IT)=IP_Hull(1:3,isd)
     IP_Tet(4,  IT)=IP

     !--- set new neighbouring pairs in ID_Hull
     ID_Hull(2,isd) = Next_Tet(ID_Hull(2,isd), ID_Hull(1,isd))

     IF(useStretch .AND. ABS(BGSpacing%Model)>1)THEN
        CALL Get_Tet_Mapping(IT)
     ENDIF
     CALL Get_Tet_Circum(IT)
     CALL Get_Tet_Volume(IT)

  ENDDO

  IF(IT0<0)THEN
     !--- return the points on the hull to subroutine Edge_Break() through
     !    module /Pole_Surround/ for new edges counting
     NB_PolePt = 0
     DO isd = 1,Nside
        Loop_i : DO i=1,3
           ip1 = IP_Hull(i,isd)
           DO j=1, NB_PolePt
              IF(ip1==IP_PolePt(j)) CYCLE Loop_i
           ENDDO
           NB_PolePt = NB_PolePt + 1
           IF(NB_PolePt>Max_PoleTet) CALL Error_Stop ('Insert_Point_Delaunay: NB_PolePt>Max_PoleTet')
           IP_PolePt(NB_PolePt) = ip1
        ENDDO Loop_i
     ENDDO

  ENDIF

  IF(Key==3)THEN
     !--- return the new elements of the ball through module /Pole_Surround/ 
     NB_PoleTet = Nside
     IF(NB_PoleTet>Max_PoleTet) CALL Error_Stop ('Insert_Point_Delaunay: NB_PoleTet>Max_PoleTet')
     IT_PoleTet(1:Nside) = IT_Cavity(1:Nside)
  ENDIF

  !---- Some tetrahedra might disappear if Nside < Nins
  DO isd=Nside+1, Nins
     IT = IT_Cavity(isd)
     IP_Tet(4,IT) = -1
  ENDDO

  !--- reset ADTree
  IF(ADTree_Search)THEN
     DO isd=1, Nins
        ipp = IP_Tet_old(:,isd)
        CALL ADTree_RemoveNode(ADTreeForMesh, ipp)
     ENDDO
     DEALLOCATE( IP_Tet_old )
     CALL ADTree_SetPoint(ADTreeForMesh,IP,Posit(:,IP))
     DO isd=1, Nside
        IT = IT_Cavity(isd)
        ipp = IP_Tet(:,IT)
        CALL ADTree_AddNode(ADTreeForMesh, it,ipp)
     ENDDO
  ENDIF

  !----Sort out mark tet
  DO isd=1,Nins
  
  
  
  ENDDO
  
  !---- Rebuild next system

  Nedge = 0
  DO isd = 1,Nside
     IT  = IT_Cavity(isd)
     it2 = ID_Hull(2,isd)
     NEXT_Tet(1:3,IT) = -1
     NEXT_Tet(4,  IT) = it2
     CALL Match_Next(it2,IT)

     DO ind=1,3
        ip1 = IP_Tet(MOD(ind,  3)+1,IT)
        ip2 = IP_Tet(MOD(ind+1,3)+1,IT)
        DO i=1,Nedge
           IF(ip1==IP_edge(2,i) .AND. ip2==IP_edge(1,i))THEN
              it2 = IP_edge(3,i)
              Next_Tet(ind,IT) = it2
              Next_Tet(IP_edge(4,i),it2) = IT
              EXIT
           ENDIF
        ENDDO
        IF(i<=Nedge)THEN
           IP_edge(:,i) = IP_edge(:,Nedge)
           Nedge = Nedge-1
        ELSE
           Nedge = Nedge+1
           IP_edge(:,Nedge) = (/ip1,ip2,IT,ind/)
        ENDIF
     ENDDO
  ENDDO

  IF(Nedge/=0) CALL Error_Stop (' Insert_Point_Delaunay:: Nedge')

  Isucc = 1


CONTAINS

  FUNCTION localVol() RESULT (vol)
    IMPLICIT NONE
    REAL*8 :: vol
    REAL*8 :: p1(3), p2(3), p3(3), p4(3), pR0(3), fMaps(3,3,4), Scalars(4)

    p1(:) = Posit(:,ipp(1))
    p2(:) = Posit(:,ipp(2))
    p3(:) = Posit(:,ipp(3))
    p4(:) = Posit(:,ipp(4))              

    IF(BGSpacing%Model==1 .OR. (.NOT. useStretch))THEN
       vol = Geo3D_Tet_Volume_Pred(p1,p2,p3,p4)
    ELSE IF(BGSpacing%Model<0)THEN
       IF(BGSpacing%Model==-1)THEN
          fMap(:,:) = BGSpacing%BasicMap(:,:)
       ELSE IF(Mapping_Choose_Model==3)THEN
          pR0 = (p1+p2+p3+p4)/4.d0
          CALL SpacingStorage_GetMapping(BGSpacing, pR0, fMap)
       ELSE
          fMaps(:,:,1:4) = fMap_Point(:,:,ipp(1:4))
          fMap(:,:)      = Mapping3D_Mean(4,fMaps,Mapping_Interp_Model)
       ENDIF
       vol = Mapping3D_Tet_Volume_Pred(p1,p2,p3,p4,fMap)
    ELSE IF(BGSpacing%Model>1)THEN
       IF(Mapping_Choose_Model==3)THEN
          pR0 = (p1+p2+p3+p4)/4.d0
          CALL SpacingStorage_GetScale(BGSpacing, pR0, Scalar)
       ELSE
          Scalars(1:4) = Scale_Point(ipp(1:4))
          Scalar = Scalar_Mean(4,Scalars,Mapping_Interp_Model)
       ENDIF
       vol = Geo3D_Tet_Volume_Pred(p1,p2,p3,p4) / Scalar**3
    ENDIF

  END  FUNCTION localVol

END SUBROUTINE Insert_Point_Delaunay

!*******************************************************************************
!>
!!     Split an egde.                                                         \n
!!     All the elements surrounding the edge given by module Pole_Surround .
!!     @param[in]  IP       : the insected point. 
!!     @param[in]  IP1,IP2  : the two points of the edge being splitted.
!!     @param[in]  Isucc    = 1  : program stops if fail to insert a point.    \n
!!                          = 0  : subroutine return with failure to insert a point.
!!                                 Higher quality standard applied for these case.
!!     @param[out] Isucc    = 1  : Successful to insert a point.              \n
!!                          =-1  : Failure to insert a point. 
!!
!!    Reminder: Pole_Surround::NB_PoleTet & Pole_Surround::IT_PoleTet,
!!              (i.e. the ring-pole of IP1, IP2) must be built in advance
!!              (by calling Search_Pole_Surround() ).                         \n
!!    Modify:  common_Parameters::IP_Tet, common_Parameters::NEXT_Tet and
!!             common_Parameters::PointAsso (if built).
!<
!*******************************************************************************
SUBROUTINE Insert_Point_SplitEdge(IP,IP1,IP2,Isucc)

  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  USE Geometry3DAll
  USE array_allocator
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: IP, IP1, IP2
  INTEGER, INTENT(INOUT) :: Isucc
  INTEGER, PARAMETER :: Max_Ball = 2*Max_PoleTet
  INTEGER :: IT_Cavity(Max_Ball)
  INTEGER :: isd, Nins, NTnew
  INTEGER :: i, it, it2, ipp(4)
  INTEGER, DIMENSION(:,:),  POINTER :: IP_Tet_old


  IT_Cavity(1:NB_PoleTet) = IT_PoleTet(1:NB_PoleTet)
  Nins  = NB_PoleTet
  NTnew = 2*NB_PoleTet
  DO isd = Nins+1,NTnew
     NB_Tet = NB_Tet+1
     IF(NB_Tet>nallc_Tet) CALL ReAllocate_Tet()
     IT_Cavity(isd) = NB_Tet
     IT  = IT_Cavity(isd-Nins)
     IP_Tet(:,NB_Tet) = IP_Tet(:,IT)
     NEXT_Tet(:,NB_Tet) = NEXT_Tet(:,IT)
  ENDDO

  IF(LinkAssociation_Exist(PointAsso))THEN
     DO isd=1,Nins
        IT = IT_Cavity(isd)
        CALL LinkAssociation_Remove(4, IT,IP_Tet(:,IT),PointAsso)
     ENDDO
  ENDIF

  !--- prepare for ADTree resetting
  IF(ADTree_Search)THEN
     CALL allc_2Dpointer( IP_Tet_old, 4, Nins, 'IP_Tet_old')
     DO isd = 1,Nins
        IT = IT_Cavity(isd)
        IP_Tet_old(:,isd) = IP_Tet(:,IT)
     ENDDO
  ENDIF

  !---- Split the cavity

  DO isd = 1,Nins
     IT = IT_Cavity(isd)
     DO i=1,4
        IF(  IP_Tet(i,IT)==IP1 )  EXIT
     ENDDO
     ipp(1:3) = IP_Tet(iTri_Tet(1:3,i),IT)
     IP_Tet(1:3,IT) = ipp(1:3)
     IP_Tet(4  ,IT) = IP
     IT2 = NEXT_Tet(i,IT)
     NEXT_Tet(4,IT) = IT2
     DO i=1,3
        IF(  IP_Tet(i,IT)==IP2 )  EXIT
     ENDDO
     NEXT_Tet(i,IT) = IT_Cavity(isd+Nins)
  ENDDO

  DO isd = Nins+1,NTnew
     IT = IT_Cavity(isd)
     DO i=1,4
        IF(  IP_Tet(i,IT)==IP2 )  EXIT
     ENDDO
     ipp(1:3) = IP_Tet(iTri_Tet(1:3,i),IT)
     IP_Tet(1:3,IT) = ipp(1:3)
     IP_Tet(4  ,IT) = IP
     IT2 = NEXT_Tet(i,IT)
     NEXT_Tet(4,IT) = IT2
     CALL Match_Next(IT2, IT)
     DO i=1,3
        IF(  IP_Tet(i,IT)==IP1 )  EXIT
     ENDDO
     NEXT_Tet(i,IT) = IT_Cavity(isd-Nins)
  ENDDO

  DO isd = 1,NTnew
     IT  = IT_Cavity(isd)
     IF(isd == Nins)THEN
        IT2 = IT_Cavity(1)
     ELSE IF(isd == NTnew)THEN
        IT2 = IT_Cavity(Nins+1)
     ELSE
        IT2 = IT_Cavity(isd+1)
     ENDIF
     CALL Match_Next(IT, IT2)
     CALL Match_Next(IT2, IT)
  ENDDO

  !---- update the geometry
  DO isd=1,NTnew
     IT = IT_Cavity(isd)
     IF(useStretch .AND. ABS(BGSpacing%Model)>1)THEN
        CALL Get_Tet_Mapping(IT)
     ENDIF
     IF(CircumUpdated) CALL Get_Tet_Circum(IT)
     IF(VolumeUpdated) CALL Get_Tet_Volume(IT)
  ENDDO

  !--- update association between points and elements
  IF(LinkAssociation_Exist(PointAsso))THEN
     DO isd=1,NTnew
        IT = IT_Cavity(isd)
        CALL LinkAssociation_AddCell(4, IT,IP_Tet(:,IT),PointAsso)
     ENDDO
  ENDIF

  !--- reset ADTree
  IF(ADTree_Search)THEN
     DO isd=1, Nins
        ipp = IP_Tet_old(:,isd)
        CALL ADTree_RemoveNode(ADTreeForMesh, ipp)
     ENDDO
     DEALLOCATE( IP_Tet_old )
     CALL ADTree_SetPoint(ADTreeForMesh,IP,Posit(:,IP))
     DO isd=1, NTnew
        IT = IT_Cavity(isd)
        ipp = IP_Tet(:,IT)
        CALL ADTree_AddNode(ADTreeForMesh, it,ipp)
     ENDDO
  ENDIF


  Isucc = 1


END SUBROUTINE Insert_Point_SplitEdge

!*******************************************************************************
!>
!!    Split a triangle face.    
!!    @param[in]  IP   :  the intersection point.
!!    @param[in]  IT0  :  the element which the face is belong to.
!!    @param[in]  Idir :  the direction of the face (1,2,3 or 4).
!!    @param[in]  Isucc = 1  : program stops if fail to insert a point.        \n
!!                      = 0  : subroutine return with failure to insert a point.
!!                             Higher qulity standard applied for these case.
!!    @param[out]  Isucc = 1 : Successful to insert a point.                  \n
!!                       =-1 : Failure to insert a point.
!!
!!    Modify:  common_Parameters::IP_Tet, common_Parameters::NEXT_Tet.
!<
!*******************************************************************************
SUBROUTINE Insert_Point_SplitFace(IP,IT0,Idir,Isucc)

  USE common_Constants
  USE common_Parameters
  USE Pole_Surround
  USE Geometry3DAll
  USE array_allocator
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: IP, IT0, Idir
  INTEGER, INTENT(INOUT) :: Isucc
  INTEGER :: IT_Cavity(6), IP_Tet_old(4,2)
  INTEGER :: i, i2, j, k, it, it2, itold, ipp(3), ipp2(3)
  INTEGER :: Idir2, Id


  IT_Cavity(1) = IT0
  IT_Cavity(4) = NEXT_Tet(Idir,IT0)
  IF(IT_Cavity(4)<=0)THEN
     IF(Isucc==1)THEN
        WRITE(29,*)'Error---try to split a boundary face'
        WRITE(29,*)'IP=',IP,' IR,Idir=',IT,Idir
        CALL Error_Stop ('Insert_Point_SplitFace')
     ELSE
        Isucc = -1
        RETURN
     ENDIF
  ENDIF

  IF(IP_Tet(4,IT_Cavity(4))<=0) CALL Error_Stop ('Insert_Point_SplitFace: split boundary face')

  !---  update connectivity for tetrahedra IP_Tet
  IF(LinkAssociation_Exist(PointAsso))THEN
     CALL LinkAssociation_Remove(4, IT_Cavity(1),IP_Tet(:,IT_Cavity(1)),PointAsso)
     CALL LinkAssociation_Remove(4, IT_Cavity(4),IP_Tet(:,IT_Cavity(4)),PointAsso)
  ENDIF


  DO k = 0,3,3
     DO i = 2,3
        NB_Tet = NB_Tet+1
        IF(NB_Tet>nallc_Tet) CALL ReAllocate_Tet()
        IT_Cavity(k+i) = NB_Tet
        IP_Tet(:,NB_Tet) = IP_Tet(:,IT_Cavity(k+1))
        Next_Tet(:,NB_Tet) = Next_Tet(:,IT_Cavity(k+1))
     ENDDO
  ENDDO


  !--- save old connectivities
  ipp(1:3) = IP_Tet(iTri_Tet(1:3,Idir),IT0)
  DO i=1,4
     IF(IP_Tet(i,IT_Cavity(4))==ipp(1)) CYCLE
     IF(IP_Tet(i,IT_Cavity(4))==ipp(2)) CYCLE
     IF(IP_Tet(i,IT_Cavity(4))==ipp(3)) CYCLE
     Idir2 = i
     EXIT
  ENDDO
  IP_Tet_old(:,1) = IP_Tet(:,IT0)
  IP_Tet_old(:,2) = IP_Tet(:,IT_Cavity(4))
  ipp2(1:3) = IP_Tet_old(iTri_Tet(1:3,Idir2),2)


  !---- Split the cavity
  DO k = 0,3,3
     Id = Idir
     IF(k==3) Id = Idir2
     DO i = 1,3
        IT = IT_Cavity(k+i)
        IP_Tet(iTri_Tet(i,Id),IT) = IP
     ENDDO
  ENDDO

  !---- update the geometry
  DO i=1,6
     IT = IT_Cavity(i)
     IF(useStretch .AND. ABS(BGSpacing%Model)>1)THEN
        CALL Get_Tet_Mapping(IT)
     ENDIF
     IF(CircumUpdated) CALL Get_Tet_Circum(IT)
     IF(VolumeUpdated) CALL Get_Tet_Volume(IT)
  ENDDO


  !--- update the association between points and elements
  IF(LinkAssociation_Exist(PointAsso))THEN
     DO i=1,6
        IT = IT_Cavity(i)
        CALL LinkAssociation_AddCell(4, IT,IP_Tet(:,IT),PointAsso)
     ENDDO
  ENDIF

  !--- reset ADTree
  IF(ADTree_Search)THEN
     CALL ADTree_RemoveNode(ADTreeForMesh, IP_Tet_old(:,1))
     CALL ADTree_RemoveNode(ADTreeForMesh, IP_Tet_old(:,2))
     CALL ADTree_SetPoint(ADTreeForMesh, IP, Posit(:,IP))
     DO i=1, 6
        IT = IT_Cavity(i)
        CALL ADTree_AddNode(ADTreeForMesh, IT, IP_Tet(:,IT))
     ENDDO
  ENDIF


  !---- Rebuild next system
  DO k = 0,3,3
     Id = Idir
     IF(k==3) Id = Idir2
     ITold = IT_Cavity(k+1)
     DO i = 1,3
        IT  = IT_Cavity(k+i)
        IT2 = Next_Tet(iTri_Tet(i,Id),IT)
        CALL Match_Next_update(IT2,ITold,IT)

        i2 = MOD(i,3)+1
        NEXT_Tet(iTri_Tet(i2,Id),IT) = IT_Cavity(k+i2)
        i2 = MOD(i2,3)+1
        NEXT_Tet(iTri_Tet(i2,Id),IT) = IT_Cavity(k+i2)
     ENDDO
  ENDDO

  DO i=1,3
     DO j=1,3
        IF(ipp(i)==ipp2(j))THEN
           NEXT_Tet(Idir, IT_Cavity(i))   = IT_Cavity(j+3)
           NEXT_Tet(Idir2,IT_Cavity(j+3)) = IT_Cavity(i)
           EXIT
        ENDIF
     ENDDO
  ENDDO


  Isucc = 1


END SUBROUTINE Insert_Point_SplitFace

