!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!   Improve the grids quality by moving the position of nodes.
!!   @param[in] common_Parameters::Smooth_Method
!<
!*******************************************************************************
SUBROUTINE Smooth ( )

  USE common_Constants
  USE common_Parameters
  USE Mapping3D
  USE OptimisingModule
  IMPLICIT NONE

  INTEGER, DIMENSION(:), POINTER :: leb

  INTEGER :: Nadjust, ipp(4)
  INTEGER :: itry, isr, i, ncone, ismoo, Isucc
  INTEGER :: IT, IP, ib
  REAL*8  :: ar, arm, art, ars, av(3), avs
  REAL*8  :: ppold(3), ppnew(3), P1(3), P2(3), p3(3), dd0, dd(3)
  REAL*8  :: fMap_Ptold(3,3), fMap_Tetold(3,3,ChainTotalLength), fmap(3,3)
  REAL*8  :: vol_Tetold(ChainTotalLength)

  !--- get volume for all elements
  CALL Get_Tet_Volume(0)
  CircumUpdated = .FALSE.      !--- geometry of the grids will be lost

  ALLOCATE( leb(NB_Point) )
  leb(1:NB_Point)=0
  DO ib = 1 , NB_BD_Tri
     leb(IP_BD_Tri(1:3,ib)) = 1
  ENDDO

  !--- Associate points with elements
  CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)

  Loop_ismoo : DO ismoo=1,Loop_Smooth

     !--- smooth out the grid in Loop_Smooth steps

     Nadjust = 0
     Mark_Point(1:NB_Point)=0

     Loop_IP : DO IP=1,NB_Point

        IF(leb(IP)==1) CYCLE Loop_IP
        ppold(:) = Posit(: ,IP)
        IF(BGSpacing%Model<-1)THEN
           fMap_Ptold(:,:) = fMap_Point(:,:,IP)
        ELSE IF(BGSpacing%Model>1)THEN
           fMap_Ptold(1,1) = Scale_Point(IP)
        ENDIF

        !--- List all elements surrounding point IP in the array ITs_List
        CALL LinkAssociation_List(IP, List_Length, ITs_List, PointAsso)
        IF(List_Length>ChainTotalLength)THEN
           WRITE(29,*)'Warning--- List_Length>ChainTotalLength:',List_Length,ChainTotalLength
           CYCLE Loop_IP
        ENDIF

        DO ncone=1,List_Length
           IT       = ITs_List(ncone)
           ipp(1:4) = IP_Tet(1:4,IT)

           IF(Debug_Display>3)THEN
              IF(  ipp(1)/=IP .AND. ipp(2)/=IP .AND.    &
                   ipp(3)/=IP .AND. ipp(4)/=IP )THEN
                 WRITE(29,*)'Error--- : data structure'
                 WRITE(29,*)'IP,IT,ipp=',IP,IT,ipp(1:4)
                 CALL Error_Stop ('Smooth')
              ENDIF
           ENDIF

           vol_tetold(ncone) = Volume_Tet(IT)
           IF(BGSpacing%Model<-1)THEN
              fMap_Tetold(:,:,ncone) = fMap_Tet(:,:,IT)
           ELSE IF(BGSpacing%Model>1)THEN
              fMap_Tetold(1,1,ncone) = Scale_Tet(IT)
           ENDIF
        ENDDO

        ppnew(:) = 0.d0
        arm = 1.e+10

        IF(Smooth_Method==1)THEN

           isr = 0
           DO ncone=1,List_Length
              IT       = ITs_List(ncone)
              ipp(1:4) = IP_Tet(1:4,IT)
              ar       = Volume_Tet(IT)
              arm      = MIN(arm,ar)
              DO i = 1 , 4
                 IF(ipp(i)==IP) CYCLE
                 IF(Mark_Point(ipp(i))==IP) CYCLE
                 ppnew(1:3) = ppnew(1:3) + Posit(1:3,ipp(i))
                 Mark_Point(ipp(i)) = IP
                 isr = isr + 1
              ENDDO
           ENDDO
           Posit(1:3,IP) = ppnew(1:3) / isr

        ELSE IF(Smooth_Method==2)THEN

           ars = 0.d0
           DO ncone=1,List_Length
              IT  = ITs_List(ncone)
              ar  = Volume_Tet(IT)
              arm = MIN(arm,ar)
              ars = ars + ar
           ENDDO
           ars = ars / List_Length

           DO ncone=1,List_Length
              IT = ITs_List(ncone)
              IF(Volume_Tet(IT)<1.d-24) CYCLE
              ar = (ars - Volume_Tet(IT))/ABS(Volume_Tet(IT))
              IF(ar>10.d0) ar = 10.d0

              ipp(1:4) = IP_Tet(1:4,IT)
              DO i = 1 , 4
                 IF(ipp(i)==IP) EXIT
              ENDDO

              P1(:) = Posit(:,ipp(iTri_Tet(1,i)))
              P2(:) = Posit(:,ipp(iTri_Tet(2,i)))
              P3(:) = Posit(:,ipp(iTri_Tet(3,i)))
              av(:) = Geo3D_Cross_Product(P3,P2,P1)
              avs   = dsqrt(av(1)*av(1) + av(2)*av(2) + av(3)*av(3))
              av(:) = av(:)/avs
              P2(:) = Posit(:,IP) - P2(:)
              avs   = av(1)*P2(1) + av(2)*P2(2) + av(3)*P2(3)
              ppnew(:) = ppnew(:) + avs*ar*av(:)
           ENDDO

           Posit(1:3,IP) = Posit(1:3,IP) + ppnew(1:3) / List_Length

        ELSE IF(Smooth_Method==3)THEN

           ars = 0.d0
           DO ncone=1,List_Length
              IT       = ITs_List(ncone)
              ipp(1:4) = IP_Tet(1:4,IT)
              ar       = Volume_Tet(IT)
              arm      = MIN(arm,ar)
              ars      = ars + ar
              DO i = 1 , 4
                 IF(ipp(i)==IP) CYCLE
                 ppnew(1:3) = ppnew(1:3) + Posit(1:3,ipp(i)) * ar
              ENDDO
           ENDDO
           Posit(1:3,IP) = ppnew(1:3) / (ars*3.d0)

        ENDIF


        IF(Smooth_Method<=3)THEN
           Isucc = 1
           DO itry=1,5

              IF(itry>1)THEN
                 Posit(:,IP) = (Posit(:,IP) + ppold(:)) /2.D0
              ENDIF

              IF(BGSpacing%Model<-1 .OR. BGSpacing%Model>1)THEN
                 CALL Get_Point_Mapping(IP)
              ENDIF

              isr = 0
              art = 1.e+10
              DO ncone=1,List_Length
                 IT       = ITs_List(ncone)
                 IF(BGSpacing%Model<-1 .OR. BGSpacing%Model>1)THEN
                    CALL Get_Tet_Mapping(IT)
                 ENDIF

                 CALL Get_Tet_Volume(IT)
                 ar = Volume_Tet(IT)
                 IF(ar<=arm)THEN
                    isr = 1
                    EXIT
                 ENDIF
                 art = MIN(art,ar)
              ENDDO

              IF(isr==0) EXIT
              IF(itry==5) Isucc = 0    !--- fail to move
           ENDDO

        ELSE IF(Smooth_Method==4)THEN
           !---  search for the best possible location

           IF(BGSpacing%Model<-1)THEN
              fMap = fMap_Point(:,:,IP)
              dd(1:3) = BGSpacing%BasicSize / dsqrt( fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2 )
              dd0 = 1.d-3 * MIN(dd(1),dd(2),dd(3))
           ELSE IF(BGSpacing%Model>1)THEN
              dd0 = 1.d-3 * Scale_Point(IP) * BGSpacing%BasicSize
           ELSE
              dd0 = 1.d-3 * BGSpacing%BasicSize
           ENDIF

           !--- set parameters for FUNCTION Optimising_GetValue()
           OptVariable%IdNode  = IP
           OptVariable%IdxFun  = 11 

           !--- call Optimising
           CALL Optimising_Set(3, 400, dd0)
           OptParameter%Idistb = 100
           CALL Optimising_Powell(Posit(:,IP), ppnew)

           IF(OptParameter%Isucc>0)THEN
              Posit(:,IP) = ppnew(:)
              DO ncone=1,List_Length
                 IT       = ITs_List(ncone)
                 IF(BGSpacing%Model<-1 .OR. BGSpacing%Model>1)THEN
                    CALL Get_Tet_Mapping(IT)
                 ENDIF

                 CALL Get_Tet_Volume(IT)
                 ar = Volume_Tet(IT)
                 IF(ar<=arm)THEN
                    isr = 1
                    EXIT
                 ENDIF
                 art = MIN(art,ar)
              ENDDO
              Isucc = 1
           ELSE
              Isucc = 0
           ENDIF

        ENDIF


        IF(Isucc==0)THEN
           !--- fail to move, return the old values
           Posit(:,IP) = ppold(:)
           DO ncone=1,List_Length
              IT  = ITs_List(ncone)
              Volume_Tet(IT) = vol_Tetold(ncone)
           ENDDO
           IF(BGSpacing%Model<-1)THEN
              fMap_Point(:,:,IP) = fMap_Ptold(:,:)
              DO ncone=1,List_Length
                 IT  = ITs_List(ncone)
                 fMap_Tet(:,:,IT) = fMap_Tetold(:,:,ncone)
              ENDDO
           ELSE IF(BGSpacing%Model>1)THEN
              Scale_Point(IP) = fMap_Ptold(1,1)
              DO ncone=1,List_Length
                 IT  = ITs_List(ncone)
                 Scale_Tet(IT) = fMap_Tetold(1,1,ncone)
              ENDDO
           ENDIF
        ELSE 
           Nadjust = Nadjust + 1
        ENDIF

     ENDDO Loop_IP

     IF(Nadjust==0) EXIT
     WRITE(*,'(a,i3,a,i8,a,E12.3)') 'loop:',ismoo,' moved points:',Nadjust

  ENDDO Loop_ismoo


  DEALLOCATE ( leb )

  RETURN
END SUBROUTINE Smooth
