!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  Set default control parameters, and modify them by reading NAMELIST
!!    or by command line arguments.
!<       
!*******************************************************************************
SUBROUTINE Control_Input( )

  USE common_Constants
  USE common_Parameters
  USE Number_Char_Transfer
  
  IMPLICIT NONE

  INTEGER :: iargc, i, Isucc, ib, nb
  CHARACTER(LEN=256) :: sw, path, controlFile
  LOGICAL :: ex_ctl, ex_name, ex_path, output_ctl, allRight, TF, ex
  CHARACTER(LEN=512)  :: NameSs, NameFs
  TYPE (IntQueueType) :: ListSs, ListFs,ListQs

  !---- Default setting

  ex_ctl = .FALSE.
  TF = .FALSE.

  controlFile  ='Mesh3D_v50.ctl '
  JobName      = ' '

  !--- NAMELIST /ControlParameters/
  Debug_Display           = 1
  Start_Point             = 1
  Curvature_Type          = 4
  Initial_Split           = 1
  ADTree_Search           = .FALSE.
  Bound_Insert_Swap       = 0
  Bound_Insert_Soft       = 0
  Bound_Insert_Order      = 0
  Recovery_Time           = 1
  Recovery_Method         = 4
  Element_Break_Method    = 8
  !--- NAMELIST /FrameParameters/
  Frame_Type              = 0
  Frame_Stitch            = 1
  Ideal_Dist_Gap          = 1.0
  Ideal_Layer_Gap         = 2
  Ideal_Layer_Expand      = 16
  !--- NAMELIST /BackgroundParameters/
  Background_Model        = 7  
  Globe_GridSize          = -1.d0
  Stretch_Limit           = 10.d0
  Stretched_Bound_Insert  = .FALSE.
  Mapping_Interp_Model    = 1
  Mapping_Choose_Model    = 1
  Interpolate_Oct_Mapping = .FALSE.
  Gradation_Factor        = 0.5d0
  Curvature_Factors(1:7)  = (/ 0.2d0, 0.1d0, 1.d0, 10.d0, 150.d0, 120.d0, 1.d0 /)
  GridSize_Adjustor       = 1.d0
  !--- NAMELIST /CosmeticParameters/
  Loop_CVT                = 0
  !CVT_Method              = -3
  CVT_Time_Limit          = 0.5d0
  Loop_Cosmetic           = 1
  Loop_Smooth             = 5
  Smooth_Method           = 2
  BoundCell_Split         = 0
  Collapse_Angle          = 12.5d0
  Swapping_Angle          = 30.0d0
  HighOrder_Model         = 1
  !--- NAMELIST /BDLayerParameters/
  NB_BD_Layers            = 0
  Layer_Heights(:)        = 0
  NorSmooth_Method        = 1
  Layer_Hybrid            = 0
  
  NameUc = ' '
  NameUs = ' '
  NameSs = ' '
  NameFs = ' '
  FrameBox(1:3) =  1
  FrameBox(4:6) = -1
  path   = ' '
  JobName= ' '
  
  ex_name      = .FALSE.
  ex_path      = .FALSE.
  output_ctl   = .FALSE.

  i = 0
  DO WHILE(i<iargc())
     i = i+1
     CALL GETARG(i,sw)
     IF(sw(1:1)=='-')THEN
        IF(sw(2:3)=='h ' .OR. sw(2:3)=='H ')THEN
           CALL Help_Message(2)
           STOP
        ELSE IF(sw(2:4)=='hh ' .OR. sw(2:4)=='HH ')THEN
           CALL Help_Message(3)
           STOP
        ELSE IF(sw(2:3)=='o ' .OR. sw(2:3)=='O ')THEN
           output_ctl = .TRUE.
        ELSE IF(sw(2:6)=='path ' .OR. sw(2:6)=='PATH ')THEN
           i = i+1
           CALL GETARG(i,sw)
           path = sw
           ex_path = .TRUE.
        ELSE
           i = i+1
        ENDIF
     ELSE IF(.NOT. ex_name)THEN
        JobName = sw
        ex_name = .TRUE.
     ELSE
        WRITE(*,*)'Error--- more than one job names'
        CALL Help_Message(2)
        STOP
     ENDIF
  ENDDO

     !---- Input Job Name if not given on the command line
     IF(JobName==' ' .and. (.not.output_ctl))THEN
        WRITE(*,*) 'Input Problem Name : '
        READ (*,*) JobName
     ENDIF

     JobNameLength = LEN_TRIM( JobName )
     IF( JobNameLength == 0  .and. (.not.output_ctl)) THEN
        WRITE(*,*)'Error--- : file name has zero JobNameLength'
        CALL Error_Stop ('Control_Input')
     ENDIF

     IF(ex_path)THEN
        JobName = TRIM(path) // '/' // JobName
        JobNameLength = LEN_TRIM( JobName )
     ENDIF
     
          
  !---- Input control parameters by namelist
  ex_path = CHAR_FilenamePath(JobName,path)
  IF(ex_path) controlFile = TRIM(path) // controlFile
  ex_ctl = .FALSE.
  controlFile = TRIM(controlFile)
  INQUIRE(file = controlFile, EXIST=ex_ctl)
  IF(ex_ctl)THEN
     OPEN(1,file=TRIM(controlFile), status='old')
     READ(1,ControlParameters,end=101)
101  CLOSE (1)
     OPEN(1,file=TRIM(controlFile), status='old')
     READ(1,FrameParameters,end=102)
102  CLOSE (1)
     OPEN(1,file=TRIM(controlFile), status='old')
     READ(1,BackgroundParameters,end=103)
103  CLOSE (1)
     OPEN(1,file=TRIM(controlFile), status='old')
     READ(1,CosmeticParameters,end=104)
104  CLOSE (1)
     OPEN(1,file=TRIM(controlFile), status='old')
     READ(1,BDLayerParameters,end=105)
105  CLOSE (1)
  ENDIF

  !---- get job-name
  IF(Curvature_Type==2)THEN
     CALL CADFIX_Check_Availability( TF )
     IF(TF)THEN
        CALL CADFIX_Init()
        CALL CADFIX_IsClientMode( TF )
        IF(TF)THEN
           JobName = 'CADfixMode'
           ex_name = .TRUE.
        ENDIF
     ENDIF
  ENDIF
  

  IF(.NOT. output_ctl)THEN

     !--- read other parameters from .bpp
     ex_ctl = .FALSE.
     INQUIRE(file = JobName(1:JobNameLength)//'.bpp', EXIST=ex_ctl)
     IF(ex_ctl)THEN
        OPEN(1,file=JobName(1:JobNameLength)//'.bpp', status='old')
        DO 
           READ(1,*,IOSTAT=Isucc)sw
           IF(Isucc<0)THEN
              EXIT
           ELSE IF(CHAR_isEqual (sw, '#end', .FALSE.) ) THEN
              EXIT
           ELSE IF(CHAR_isEqual (sw, '#Globe_GridSize', .FALSE.) ) THEN
              READ(1,*) Globe_GridSize
           ELSE IF(CHAR_isEqual (sw, '#Background_Model', .FALSE.) ) THEN
              READ(1,*) Background_Model
           ELSE IF(CHAR_isEqual (sw, '#Curvature_Factors', .FALSE.) ) THEN
              READ(1,*) Curvature_Factors(1:7)
           ELSE IF(CHAR_isEqual (sw, '#unchecking_curve_ids', .FALSE.) ) THEN
              CALL IntQueue_Read(ListUc, 1)
           ELSE IF(CHAR_isEqual (sw, '#unchecking_surf_ids', .FALSE.) ) THEN
              CALL IntQueue_Read(ListUs, 1)
           ELSE IF(CHAR_isEqual (sw, '#Highorder_surf_ids', .FALSE.) ) THEN
           ELSE IF(CHAR_isEqual (sw, '#Generating_surf_ids', .FALSE.) ) THEN
           ELSE IF(CHAR_isEqual (sw, '#unchecking_curve_names', .FALSE.) ) THEN
              READ(1,'(a)') NameUc
           ELSE IF(CHAR_isEqual (sw, '#unchecking_surf_names', .FALSE.) ) THEN
              READ(1,'(a)') NameUs
           ELSE IF(CHAR_isEqual (sw, '#Generating_surf_names', .FALSE.) ) THEN
           ELSE IF(CHAR_isEqual (sw, '#SuperPatch_Groups', .FALSE.) ) THEN
           ELSE IF(CHAR_isEqual (sw, '#SuperCurve_RidgeAngle', .FALSE.) ) THEN
           ELSE IF(CHAR_isEqual (sw, '#Layer_Heights', .FALSE.) ) THEN
              READ(1,*) NB_BD_Layers, Layer_Heights(1:NB_BD_Layers)               
           ELSE IF(CHAR_isEqual (sw, '#symmetry_surf_ids', .FALSE.) ) THEN
              CALL IntQueue_Read(ListSs, 1)
           ELSE IF(CHAR_isEqual (sw, '#quad_surf_ids', .FALSE.) ) THEN
              CALL IntQueue_Read(ListQs, 1)
           ELSE IF(CHAR_isEqual (sw, '#farfield_surf_ids', .FALSE.) ) THEN
              CALL IntQueue_Read(ListFs, 1)             
           ELSE IF(CHAR_isEqual (sw, '#symmetry_surf_names', .FALSE.) ) THEN
              READ(1,'(a)') NameSs
           ELSE IF(CHAR_isEqual (sw, '#farfield_surf_names', .FALSE.) ) THEN
              READ(1,'(a)') NameFs
           ELSE IF(CHAR_isEqual (sw, '#Frame_Domain', .FALSE.) ) THEN
              READ(1,*) FrameBox(1:6)
           ELSE IF(CHAR_isEqual (sw, '#Medial_Axis_First_Surf', .FALSE.) ) THEN
              READ(1,*) medSurf1
           ELSE IF(CHAR_isEqual (sw, '#Medial_Axis_Last_Surf', .FALSE.) ) THEN
              READ(1,*) medSurf2
           ELSE IF(CHAR_isEqual (sw, '#Medial_Axis_Number_of_Slices', .FALSE.) ) THEN
              READ(1,*) medNumSamp
           ELSE IF(CHAR_isEqual (sw, '#Medial_Axis_Poly_Order', .FALSE.) ) THEN
              READ(1,*) medOrder
           ELSE IF(CHAR_isEqual (sw, '#Medial_Axis_Error_Aim', .FALSE.) ) THEN
              READ(1,*) medError
           ELSE IF(CHAR_isEqual (sw, '#Medial_Axis_Spacing', .FALSE.) ) THEN
              READ(1,*) medSpace

           ELSE
              sw = ADJUSTL(sw)
              IF(sw(1:1)=='#') THEN
                 WRITE(*,*)' Warning---- unkonwn title:', sw
              ENDIF
           ENDIF
        ENDDO
     ENDIF
  ENDIF

  !---- Command Options Input
  i     = 0
  Isucc = 1
  DO WHILE(i<iargc())
     i = i+1
     CALL GETARG(i,sw)
     IF(sw(1:3)=='-h ' .OR. sw(1:3)=='-H ' .OR. sw(1:4)=='-hh ' .OR. sw(1:4)=='-HH ')THEN
     ELSE IF(sw(1:3)=='-o ' .OR. sw(1:3)=='-O ')THEN
     ELSE IF(i<iargc() .AND. (sw(1:6)=='-path ' .OR. sw(1:6)=='-PATH '))THEN
        i = i+1
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-dd ' .OR. sw(1:4)=='-DD '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Debug_Display = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-bis ' .OR. sw(1:5)=='-BIS '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Bound_Insert_Swap = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-bif ' .OR. sw(1:5)=='-BIF '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Bound_Insert_Soft = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-bio ' .OR. sw(1:5)=='-BIO '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Bound_Insert_Order = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-rm ' .OR. sw(1:4)=='-RM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Recovery_Method = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-sp ' .OR. sw(1:4)=='-SP '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Start_Point = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-rt ' .OR. sw(1:4)=='-RT '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Recovery_Time = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-ebm ' .OR. sw(1:5)=='-EBM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Element_Break_Method = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-ft ' .OR. sw(1:4)=='-FT '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Frame_Type = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-ile ' .OR. sw(1:5)=='-ILE '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Ideal_Layer_Expand = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-ilg ' .OR. sw(1:5)=='-ILG '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Ideal_Layer_Gap = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-fs ' .OR. sw(1:4)=='-FS '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Frame_Stitch = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-bm ' .OR. sw(1:4)=='-BM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Background_Model = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-ggs ' .OR. sw(1:4)=='-GGS '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Globe_GridSize = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-sl ' .OR. sw(1:4)=='-SL '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Stretch_Limit = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-sbi ' .OR. sw(1:5)=='-SBI '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Isucc = 1
        IF(sw(1:1)=='t' .OR. sw(1:1)=='T')THEN
           Stretched_Bound_Insert = .TRUE.
        ELSE IF(sw(1:1)=='f' .OR. sw(1:1)=='F')THEN
           Stretched_Bound_Insert = .FALSE.
        ELSE
           Isucc = 0
        ENDIF
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf1 ' .OR. sw(1:5)=='-CF1 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(1) = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf2 ' .OR. sw(1:5)=='-CF2 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(2) = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf3 ' .OR. sw(1:5)=='-CF3 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(3) = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf4 ' .OR. sw(1:5)=='-CF4 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(4) = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf5 ' .OR. sw(1:5)=='-CF5 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(5) = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf6 ' .OR. sw(1:5)=='-CF6 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(6) = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf7 ' .OR. sw(1:5)=='-CF7 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(7) = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-mim ' .OR. sw(1:5)=='-MIM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Mapping_Interp_Model = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-mcm ' .OR. sw(1:5)=='-MCM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Mapping_Choose_Model = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-iom ' .OR. sw(1:5)=='-IOM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Isucc = 1
        IF(sw(1:1)=='t' .OR. sw(1:1)=='T')THEN
           Interpolate_Oct_Mapping = .TRUE.
        ELSE IF(sw(1:1)=='f' .OR. sw(1:1)=='F')THEN
           Interpolate_Oct_Mapping = .FALSE.
        ELSE
           Isucc = 0
        ENDIF
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-ga ' .OR. sw(1:4)=='-GA '))THEN
        i = i+1
        CALL GETARG(i,sw)
        GridSize_Adjustor = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-gf ' .OR. sw(1:4)=='-GF '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Gradation_Factor = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-lc ' .OR. sw(1:4)=='-LC '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Loop_Cosmetic = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:6)=='-lcvt ' .OR. sw(1:6)=='-LCVT '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Loop_CVT = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-ls ' .OR. sw(1:4)=='-LS '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Loop_Smooth = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-ca ' .OR. sw(1:4)=='-CA '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Collapse_Angle = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-sa ' .OR. sw(1:4)=='-SA '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Swapping_Angle = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-hm ' .OR. sw(1:4)=='-HM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        HighOrder_Model = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-nsm ' .OR. sw(1:5)=='-NSM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        NorSmooth_Method = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-lh ' .OR. sw(1:4)=='-LH '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Layer_Hybrid = CHAR_to_INT(sw,Isucc)
     ELSE IF(sw(1:1)=='-')THEN
        Isucc = 0
     ENDIF

     IF(Isucc/=1)THEN
        IF(Isucc==0)THEN
           WRITE(*, *)'Error--- Control_Input: Illegal flags'
        ENDIF
        CALL Help_Message(Isucc)
        STOP
     ENDIF

  ENDDO

  IF(output_ctl)THEN
     OPEN(1,file=TRIM(controlFile))
     WRITE(1,'(a)')'!--- please refer README for the control parameters reading...'
     WRITE(1,'(a)')' '
     WRITE(1,ControlParameters)
     WRITE(1,FrameParameters)
     WRITE(1,BackgroundParameters)
     WRITE(1,CosmeticParameters)
     WRITE(1,BDLayerParameters)
     CLOSE(1)
     WRITE(*,*)'---- Output a new file for control parameters ----'
     STOP
  ENDIF

  IF(Curvature_Type==2)THEN
     CALL CADFIX_IsClientMode( TF )
     IF(.NOT. TF)THEN
        sw = JobName(1:JobNameLength)//'_backup' 
        INQUIRE(file = sw(1:JobNameLength+7)//'.fbm', EXIST=ex)
        IF(.NOT. ex)THEN
           WRITE(29,*) 'Error--- the file '//sw(1:JobNameLength+7)//'.fbm does NOT exist.'
           WRITE(*, *) 'Error--- the file '//sw(1:JobNameLength+7)//'.fbm does NOT exist.'
           CALL Error_Stop( ' Control_Input ::'  )
        ENDIF
        CALL CADFIX_LoadGeom( sw )
        CALL CADFIX_AnalyseModel()
     ENDIF
  ENDIF

  WRITE(*,*) '      Program Name : ',TRIM( JobName )
  WRITE(*,*)' '
  OPEN(29, file=JobName(1:JobNameLength)//'_VT.log', status='unknown')
  CALL Happy_Start(JobName)

  !---- Check control parameters

  AllRight = .TRUE.

  IF(Initial_Split<1)THEN
     WRITE(29,*)'Error--- Invalid input: Initial_Split=',Initial_Split
     AllRight = .FALSE.
  ENDIF
  IF(Start_Point<0 .OR. Start_Point>2)THEN
     WRITE(29,*)'Error--- Invalid input: Start_Point=',Start_Point
     AllRight = .FALSE.
  ENDIF
  IF(Start_Point==2 .AND. ADTree_Search)THEN
     WRITE(29,*)'Error--- Conflict input: ADTree_Search vs. Start_Point'
     AllRight = .FALSE.
  ENDIF

  IF(Curvature_Type<1 .OR. Curvature_Type>4)THEN
     WRITE(29,*)'Error--- Invalid input: Curvature_Type=',Curvature_Type
     AllRight = .FALSE.
  ENDIF

  IF(Curvature_Type==2)THEN
     CALL CADFIX_Check_Availability( TF )
     IF(.NOT. TF)THEN
        WRITE(29,*)'Error--- Invalid input: Curvature_Type=',Curvature_Type
        WRITE(29,*)'         No Cadfix library being linked.'
        WRITE(29,*)'         Either reset Curvature_Type=1, '
        WRITE(29,*)'         or re-compile the code with CADfix library. '
        AllRight = .FALSE.
     ENDIF
  ENDIF

  IF(ABS(Background_Model)>7 .OR. Background_Model==0 )THEN
     WRITE(29,*)'Error--- Invalid input: Background_Model=',Background_Model
     AllRight = .FALSE.
  ENDIF
  IF(ABS(Background_Model)==1 .AND. Start_Point==2)THEN
     WRITE(29,*)'Error--- Conflict input: Background_Model vs. Start_Point'
     AllRight = .FALSE.
  ENDIF
  IF(ABS(Background_Model)==3 .AND. Start_Point==2)THEN
     WRITE(29,*)'Error--- Conflict input: Background_Model vs. Start_Point'
     AllRight = .FALSE.
  ENDIF
  IF(Stretch_Limit<2.0)THEN
     WRITE(29,*)'Error--- Invalid input: Stretch_Limit=',Stretch_Limit
     AllRight = .FALSE.
  ENDIF
  IF( Bound_Insert_Swap<0 .OR. Bound_Insert_Swap>3 )THEN
     WRITE(29,*)'Error--- Invalid input: Bound_Insert_Swap=',Bound_Insert_Swap
     AllRight = .FALSE.
  ENDIF
  IF( Bound_Insert_Soft<0 .OR. Bound_Insert_Soft>1 )THEN
     WRITE(29,*)'Error--- Invalid input: Bound_Insert_Soft=',Bound_Insert_Soft
     AllRight = .FALSE.
  ENDIF
  IF( Bound_Insert_Order<0 .OR. Bound_Insert_Order>1 )THEN
     WRITE(29,*)'Error--- Invalid input: Bound_Insert_Order=',Bound_Insert_Order
     AllRight = .FALSE.
  ENDIF
  !IF(Loop_CVT<0)THEN
  !   WRITE(29,*)'Error--- Invalid input: Loop_CVT=',Loop_CVT
  !   AllRight = .FALSE.
  !ENDIF
  IF(Loop_CVT>0 .AND. Start_Point==1 .AND. Recovery_Time/=1)THEN
     WRITE(29,*)'Error--- Conflict input: Loop_CVT vs. Recovery_Time'
     AllRight = .FALSE.
  ENDIF
  IF(Loop_Cosmetic<0)THEN
     WRITE(29,*)'Error--- Invalid input: Loop_Cosmetic=',Loop_Cosmetic
     AllRight = .FALSE.
  ENDIF
  !IF(Loop_Smooth<0)THEN
  !   WRITE(29,*)'Error--- Invalid input: Loop_Smooth=',Loop_Smooth
  !   AllRight = .FALSE.
  !ENDIF
  IF(Smooth_Method<1 .OR. Smooth_Method>4)THEN
     WRITE(29,*)'Error--- Invalid input: Smooth_Method=',Smooth_Method
     AllRight = .FALSE.
  ENDIF
  IF(Mapping_Interp_Model<1 .OR. Mapping_Interp_Model>3)THEN
     WRITE(29,*)'Error--- Invalid input: Mapping_Interp_Model=',Mapping_Interp_Model
     AllRight = .FALSE.
  ENDIF
  IF(Mapping_Choose_Model<1 .OR. Mapping_Choose_Model>3)THEN
     WRITE(29,*)'Error--- Invalid input: Mapping_Choose_Model=',Mapping_Choose_Model
     AllRight = .FALSE.
  ENDIF
  IF(Interpolate_Oct_Mapping .AND. Background_Model<0)THEN
     WRITE(29,*)'Warning--- Control_Input: Interpolate_Oct_Mapping is .true.'
     WRITE(29,*)'           So it may take a longer time for an anisotropic case.'
     WRITE(29,*)' '
  ENDIF
  IF(Recovery_Method<0 .OR. Recovery_Method>4)THEN
     WRITE(29,*)'Error--- Invalid input: Recovery_Method=',Recovery_Method
     AllRight = .FALSE.
  ENDIF
#ifndef _OldRecov
  IF(Recovery_Method==1 .OR. Recovery_Method==2)THEN
     WRITE(29,*)'Error--- Invalid input: Recovery_Method=',Recovery_Method
     WRITE(29,*)'         For this Recovery_Method, you need link the code with'
     WRITE(29,*)'         a proper library--- see your makefile.'
     AllRight = .FALSE.
  ENDIF
#endif
  IF(Element_Break_Method<-1 .OR. Element_Break_Method>12)THEN
     WRITE(29,*)'Error--- Invalid input: Element_Break_Method=',Element_Break_Method
     AllRight = .FALSE.
  ENDIF
  IF(Element_Break_Method==4 .AND. Start_Point==2)THEN
     WRITE(29,*)'Error--- Conflict input: Element_Break_Method vs. Start_Point'
     WRITE(29,*)'   Set Element_Break_Method=5 or Start_Point=1'
     AllRight = .FALSE.
  ENDIF
  IF(Collapse_Angle<0 .OR. Collapse_Angle>15.0)THEN
     WRITE(29,*)'Error--- Invalid input: Collapse_Angle=',Collapse_Angle
     AllRight = .FALSE.
  ENDIF
  IF(Swapping_Angle<15.0 .OR. Swapping_Angle>60.0)THEN
     WRITE(29,*)'Error--- Invalid input: Swapping_Angle=',Swapping_Angle
     AllRight = .FALSE.
  ENDIF
  IF(Gradation_Factor<0.05)THEN
     WRITE(29,*)'Error--- Invalid input: Gradation_Factor=',Gradation_Factor
     AllRight = .FALSE.
  ENDIF
  IF(Recovery_Time<0 .OR. Recovery_Time>3)THEN
     WRITE(29,*)'Error--- Invalid input: Recovery_Time=',Recovery_Time
     AllRight = .FALSE.
  ENDIF
  IF(GridSize_Adjustor<0.01 .OR. GridSize_Adjustor>100)THEN
     WRITE(29,*)'Error--- Invalid input: GridSize_Adjustor=',GridSize_Adjustor
     AllRight = .FALSE.
  ENDIF
  IF(Frame_Type==-1)THEN
     WRITE(29,*)'Warning--- Frame_Type has been reset as 0'
     Frame_Type = 0
  ENDIF
  IF(Frame_Type<0 .OR. Frame_Type>5)THEN
     WRITE(29,*)'Error--- Invalid input: Frame_Type=',Frame_Type
     AllRight = .FALSE.
  ENDIF
  IF(Frame_Stitch>0 .AND. Frame_Type/=0 .AND. Start_Point==2)THEN
     WRITE(29,*)'Error--- Conflict input:  Frame_Stitch vs. Frame_Type vs. Start_Point'
     WRITE(29,*)'   Set Frame_Stitch=-1 or 0,  or Start_Point=1'
     AllRight = .FALSE.
  ENDIF
  IF(NB_BD_Layers<0)THEN
     WRITE(29,*)'Error--- Invalid input:  NB_BD_Layers=', NB_BD_Layers
     AllRight = .FALSE.
  ENDIF
  IF(BoundCell_Split<0 .OR. BoundCell_Split>2)THEN
     WRITE(29,*)'Error--- Invalid input: BoundCell_Split=',BoundCell_Split
     AllRight = .FALSE.
  ENDIF
  IF(BoundCell_Split>0 .AND. HighOrder_Model==1 .AND. NB_BD_Layers/=0)THEN
     WRITE(29,*)'Warning--- Control_Input: BoundCell_Split>0'
     WRITE(29,*)'           This is not necessary for a boundary layer problem.'
     WRITE(29,*)' '
  ENDIF
  IF(BoundCell_Split==0 .AND. HighOrder_Model>1)THEN
     WRITE(29,*)'Warning--- Control_Input: BoundCell_Split=0'
     WRITE(29,*)'           It is better to set BoundCell_Split>0 HighOrder problem.'
     WRITE(29,*)' '
  ENDIF
  IF(HighOrder_Model/=1 .AND. HighOrder_Model/=3)THEN
     WRITE(29,*)'Error--- Invalid input: HighOrder_Model=',HighOrder_Model
     AllRight = .FALSE.
  ENDIF
  IF(HighOrder_Model>1 .AND. FRAME_TYPE==0 .AND. NB_BD_Layers>0 .AND. LAYER_HYBRID>0)THEN
     WRITE(29,*)'Error--- Conflict input:  HighOrder_Model vs. LAYER_HYBRID'
     WRITE(29,*)'   Set LAYER_HYBRID=0'
     AllRight = .FALSE.
  ENDIF

  IF(.NOT. AllRight)THEN
     WRITE(29,*)'--- Use flag -h for more information.'
     CALL Error_Stop ('Control_Input')
  ENDIF

  !--- Angle unit transfer

  Collapse_Angle = Collapse_Angle*PI/180.d0
  Swapping_Angle = Swapping_Angle*PI/180.d0

  !--- surface conditions
  
  Type_Wall(:) = 1
  IF(Curvature_Type/=2)THEN
     !--- symmetry surfaces
     DO i = 1, ListSs%numNodes
        ib = ListSs%Nodes(i)
        IF(ib>0 .AND. ib<=10000) Type_Wall(ib) = 2
     ENDDO
     !---- farfield surface
     DO i = 1, ListFs%numNodes
        ib = ListFs%Nodes(i)
        IF(ib>0 .AND. ib<=10000) Type_Wall(ib) = 3
     ENDDO
     !---- quad surface
     DO i = 1, ListQs%numNodes
        ib = ListQs%Nodes(i)
        IF(ib>0 .AND. ib<=10000) Type_Wall(ib) = 10
     ENDDO
  ELSE    !We should set curvature type = 1, naming surface conditions needs to be addressed in surface generator - SPW
     CALL CADFIX_GetNumSurfaces( nb )     
     DO ib = 1, nb
        CALL CADFIX_GetSurfaceName( ib, sw )
        IF(CHAR_Contains(NameSs,sw))  Type_Wall(ib) = 2
        IF(CHAR_Contains(NameFs,sw))  Type_Wall(ib) = 3
     ENDDO
  ENDIF


  !--- elasticity material for high order mesh
  
  Material%E  = 1.d1
  Material%Nu = 0.1d0   
  CALL MaterialPar_set(Material)

  !--- Initialize the predicates code used in Criterion_in_Sphere etc.

  CALL exactinit()


END SUBROUTINE Control_Input


!*******************************************************************************
!>
!!   Screen output help message.
!<       
!*******************************************************************************
SUBROUTINE Help_Message(Isucc)

  USE common_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: Isucc
  CHARACTER(LEN=256)  :: sw

  CALL GETARG(0,sw)

  WRITE(*,'(a)')' SYNOPSIS'
  WRITE(*,'(a)')'     '//TRIM(sw)//' [-h] [-o] [jobname] [-path xxx] [setting]'
  WRITE(*,'(a)')'  '
  WRITE(*,'(a)')' DESCRIPTION'
  WRITE(*,'(a)')'     All flags, except the jobname and path, are NOT case sensitive.'
  WRITE(*,'(a)')'     The flags can be used with NO particular order.'
  WRITE(*,'(a)')'  '
  WRITE(*,'(a)')'     -h        :  For this help message and defination of parameters.'
  WRITE(*,'(a)')'     -o        :  Save present control parameters and write them in "Mesh3D_v50.ctl".'
  WRITE(*,'(a)')'                  If a "Mesh3D_v50.ctl" already exsits, it will be replaced.'
  WRITE(*,'(a)')'  '
  WRITE(*,'(a)')'     jobname   :  program name (prefix of the files involved);'
  WRITE(*,'(a)')'                  If no jobname given here, you will be asked to input it '
  WRITE(*,'(a)')'                  after a screen prompt.'
  WRITE(*,'(a)')'     -path xxx :  Give the path of the directory where the input/output files locate.'
  WRITE(*,'(a)')'                  Normally we need not to give a path if the input/output files '
  WRITE(*,'(a)')'                  locate in the current working directory.'
  WRITE(*,'(a)')'                  However, it is always a must for submitting a job by "qsub".'
  WRITE(*,'(a)')'  '
  WRITE(*,'(a)')'     All control parameters have their default values, and can be reset as Namelist '
  WRITE(*,'(a)')'     variables read from file "Mesh3D_v50.ctl".'
  WRITE(*,'(a)')'     You can also correct some of them further by using following flags:'
  WRITE(*,'(a)')'  '
  WRITE(*,'(a)')'  |  -DD   |  0,1,2,3,4        | Debug_Display            |'
  IF(Isucc==3) &
  WRITE(*,'(a)')'  |  -SP   |  0,1,2              | Start_Point              |'
  IF(Isucc==3) &
  WRITE(*,'(a)')'  |  -BIS  |  0,1,2,3          | Bound_Insert_Swap        |'
  IF(Isucc==3) &
  WRITE(*,'(a)')'  |  -BIF  |  0,1              | Bound_Insert_Soft        |'
  WRITE(*,'(a)')'  |  -BIO  |  0,1              | Bound_Insert_Order       |'
  IF(Isucc==3) &
  WRITE(*,'(a)')'  |  -RT   |  0,1,2,3          | Recovery_Time            |'
  IF(Isucc==3) &
  WRITE(*,'(a)')'  |  -RM   |  0,1,2,3,4        | Recovery_Method          |'
  WRITE(*,'(a)')'  |  -EBM  |  -1,0,1,2,3,4,5,6,7,8    | Element_Break_Method     |'
  WRITE(*,'(a)')'  |        |                   |                          |'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  |  -FT   |  0,1,2,3,4,5      | Frame_Type               |'
  WRITE(*,'(a)')'  |  -FS   |  -1, 0, or 1      | Frame_Stitch             |'
  WRITE(*,'(a)')'  |  -ILE  |                   | Ideal_Layer_Expand       |'
  WRITE(*,'(a)')'  |  -ILG  |                   | Ideal_Layer_Gap          |'
  WRITE(*,'(a)')'  |  -FS   |  -1,0,1           | Frame_Stitch             |'
  WRITE(*,'(a)')'  |        |                   |                          |'
  ENDIF
  WRITE(*,'(a)')'  |  -BM   |  (±) 1,2,...,7    | Background_Model         |'
  WRITE(*,'(a)')'  |  -GGS  |                   | Globe_GridSize           |'
  WRITE(*,'(a)')'  |  -CF1, -CF2, ..., -CF7     | Curvature_Factors(1:7)   |'
  IF(Isucc==3) &
  WRITE(*,'(a)')'  |  -SL   |  >2.0             | Stretch_Limit            |'
  WRITE(*,'(a)')'  |  -SBI  |  T,F              | Stretched_Bound_Insert   |'
  WRITE(*,'(a)')'  |  -MIM  |  1,2,3            | Mapping_Interp_Model     |'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  |  -MCM  |  1,2,3            | Mapping_Choose_Model     |'
  WRITE(*,'(a)')'  |  -IOM  |  T,F              | Interpolate_Oct_Mapping  |'
  WRITE(*,'(a)')'  |  -GA   |  0.01<=V<=100.0   | GridSize_Adjustor        |'
  ENDIF
  WRITE(*,'(a)')'  |  -GF   |  >0.05            | Gradation_Factor         |'
  WRITE(*,'(a)')'  |        |                   |                          |'
  WRITE(*,'(a)')'  |  -LC   |  0,1,2,...        | Loop_Cosmetic            |'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  |  -LCVT |  >=0              | Loop_CVT                 |'
  WRITE(*,'(a)')'  |  -LS   |  0,1,2,...        | Loop_Smooth              |'
  WRITE(*,'(a)')'  |  -CA   |                   | Collapse_Angle           |'
  WRITE(*,'(a)')'  |  -SA   |                   | Swapping_Angle           |'
  WRITE(*,'(a)')'  |  -HM   |  1,3              | HighOrder_Model          |'
  WRITE(*,'(a)')'  |        |                   |                          |'
  WRITE(*,'(a)')'  |  -NSM  |  0,1              | NorSmooth_Method         |'
  WRITE(*,'(a)')'  |  -LH   |  0,1,2            | Layer_Hybrid             |'
  ENDIF
  WRITE(*,'(a)')'  '
  IF(Isucc==0)WRITE(*,'(a)')'      Use flag -h to get more detail of the parameters.'

  IF(Isucc==0)RETURN

  WRITE(*,'(a)')' EXAMPLE:'
  WRITE(*,'(a)')'     '//TRIM(sw)//' s_2l -path /home/work/sphere/ -DD 3 -BIS 3 -o'
  WRITE(*,'(a)')' '
  WRITE(*,'(a)')' '
  WRITE(*,'(a)')' The defination of the parameters (default):'
  WRITE(*,'(a)')' '
  WRITE(*,'(a)')'  Debug_Display(1)'
  WRITE(*,'(a)')'        How verbose the mesh generator is'
  WRITE(*,'(a)')'        A bigger number indicates more checking in the code '
  WRITE(*,'(a)')'          and more output on the screen.'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  Start_Point(1)'
  WRITE(*,'(a)')'          =1,  read surface geometry from "*.fro" file and generate volume mesh.'
  WRITE(*,'(a)')'          =2,  read existing volume mesh from "*_0.plt" file for cosmetic treatment.'
  WRITE(*,'(a)')'  Curvature_Type(4)'
  WRITE(*,'(a)')'          =1,  Swansea curvature type; Read "*.dat" file.'
  WRITE(*,'(a)')'          =2,  CADfix curvature type;  Read "*.fbm" file.'
  WRITE(*,'(a)')'          =3,  Read CADfix curvature ("*.fbm" file),'
  WRITE(*,'(a)')'               and convert it to Swansea curvature type.'
  WRITE(*,'(a)')'               It will be reset as 1 after converting.'
  WRITE(*,'(a)')'          =4,  IGES or STEP input (.igs, .stp, .iges or .step) '
  WRITE(*,'(a)')'  ADTree_Search(F)'
  WRITE(*,'(a)')'          If .false., do not use AD_Tree for element search.'
  WRITE(*,'(a)')'          If .true.,  use AD_Tree for element search.'
  ENDIF
  WRITE(*,'(a)')'  Bound_Insert_Swap(3)'
  WRITE(*,'(a)')'          =3,  do swapping before recovery based on number of boundary edges.'
  WRITE(*,'(a)')'          =2,  do swapping before recovery based on dihedral.'
  WRITE(*,'(a)')'          =1,  do swapping before recovery based on volume.'
  WRITE(*,'(a)')'          =0,  no swapping before recovery.'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  Bound_Insert_Soft(0)'
  WRITE(*,'(a)')'          =1,  allow some points to fail to insert at first,'
  WRITE(*,'(a)')'                then try to insert them after others.'
  WRITE(*,'(a)')'          =0,  not allow.'
  ENDIF
  WRITE(*,'(a)')'  Bound_Insert_Order(0)'
  WRITE(*,'(a)')'          =1,  insert those surface nodes with a larger surface spacing first.'
  WRITE(*,'(a)')'          =0,  insert surface nodes by its original order.     '
  WRITE(*,'(a)')'          For both case, the numbering order of surface nodes is NOT changed.'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  Recovery_Time(1)'
  WRITE(*,'(a)')'          =0,  no recovery.'
  WRITE(*,'(a)')'          =1,  do recovery before refining the mesh.'
  WRITE(*,'(a)')'          =2,  do recovery after refining the mesh.'
  WRITE(*,'(a)')'          =3,  do recovery before refining the mesh without lift-point or clean-face,'
  WRITE(*,'(a)')'               then complete recovery after refining the mesh.'
  ENDIF
  WRITE(*,'(a)')'  Element_Break_Method(8)'
  WRITE(*,'(a)')'          =1,  break large elements by inserting points at the element centroid.'
  WRITE(*,'(a)')'          =2,  break long edges by inserting points at the mid edge.'
  WRITE(*,'(a)')'          =3,  (=1+2)'
  WRITE(*,'(a)')'          =4,  generate ideal mesh nodes and insert them.'
  WRITE(*,'(a)')'          =5,  read mesh nodes from "*_1.plt" file and insert them.'
  WRITE(*,'(a)')'          =6,  insert nodes using an octree stencil'
  WRITE(*,'(a)')'          =7,  insert nodes using ideal lattice growth'
  WRITE(*,'(a)')'          =8,  enhanced tetrahedral advancing front then (=1+2)'
  WRITE(*,'(a)')'          =9,  enhanced tetrahedral advancing front then 6'
  WRITE(*,'(a)')'          =10,  enhanced tetrahedral advancing front then 7'
  WRITE(*,'(a)')'          =0,  no element refinement.'
  WRITE(*,'(a)')'          =-1, split long edges directly in cosmetic loop.'
  WRITE(*,'(a)')' '
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  Frame_Type(0)'
  WRITE(*,'(a)')'        The type of frame mesh.'
  WRITE(*,'(a)')'          =0,  a closed surface domain is defined no special mesh is needed.'
  WRITE(*,'(a)')'          =1,  fill the domain with tetrahedral mesh made from ideal mesh. '
  WRITE(*,'(a)')'          =2,  fill the domain with hexahedral mesh and '
  WRITE(*,'(a)')'               use tetrahedral and pyramids as a transition zone.'
  WRITE(*,'(a)')'          =3,  fill the domain with hexahedral mesh and '
  WRITE(*,'(a)')'               use tetrahedral and wedges as a transition zone.'
  WRITE(*,'(a)')'          =4,  fill the domain with an octree generated tetrahedral mesh.'
  WRITE(*,'(a)')'          =5,  fill the domain with an octree generated hybrid mesh.'
  WRITE(*,'(a)')'  Frame_Stitch(1)'
  WRITE(*,'(a)')'        The option to stitch the frame or boundary layers.'
  WRITE(*,'(a)')'          =0,  do NOT stitch the frame. Also apply to boundary layers.'
  WRITE(*,'(a)')'          =1,  stitch the frame.'
  WRITE(*,'(a)')'          =-1, read frame mesh ("*_f.plt" or "*_hyb.plt" or "*_v1.plt"), and '
  WRITE(*,'(a)')'               innner mesh ("*_0.plt"), and stitch them.'
  WRITE(*,'(a)')'  Ideal_Layer_Expand(16)'
  WRITE(*,'(a)')'        The number of layers ideal mesh expanding from geomatry.'
  WRITE(*,'(a)')'          > 0,  indicating open domain.'
  WRITE(*,'(a)')'          <=0,  indicating a closed domain /specified domain.'
  WRITE(*,'(a)')'  Ideal_Layer_Gap(2)'
  WRITE(*,'(a)')'          The number of layers connecting geometry and ideal mesh.'
  WRITE(*,'(a)')' '
  ENDIF
  WRITE(*,'(a)')'  Background_Model(4)'
  WRITE(*,'(a)')'          = ±1,  Tet. background (read "*.fro" (layout) ).'
  WRITE(*,'(a)')'          = ±2,  Tet. background (read "*.bac").'
  WRITE(*,'(a)')'          = ±3,  input "*.bac" & "*.fro" files, '
  WRITE(*,'(a)')'                 build a surface-layout-matched octree mesh as the background.'
  WRITE(*,'(a)')'          = ±4,  input "*.bac" file, build an octree mesh as the background.'
  WRITE(*,'(a)')'          = ±5,  input "*.bac" & "*.dat" files, '
  WRITE(*,'(a)')'                 build a curvature-controlled octree mesh as the background.'
  WRITE(*,'(a)')'          = ±6,  input "*.bac" & "*.fro" files, '
  WRITE(*,'(a)')'                 build a curvature-controlled octree mesh as the background.'
  WRITE(*,'(a)')'          = ±7,  Oct. background (read "*.Obac").'
  WRITE(*,'(a)')'          >0          for   isotropic problems.'
  WRITE(*,'(a)')'          <0          for anisotropic problems.'
  WRITE(*,'(a)')'  Globe_GridSize(1.0)'
  WRITE(*,'(a)')'          The globe grid size. '
  WRITE(*,'(a)')'          This is only be refered if the *.bac file is needed but not existing.'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  Stretch_Limit(10.0)'
  WRITE(*,'(a)')'        The maximum value of allowed stretching (>2.0). '
  WRITE(*,'(a)')'        Only be refered for anisotropic problems. '
  WRITE(*,'(a)')'  Stretched_Bound_Insert(F)'
  WRITE(*,'(a)')'          If .true.,  insert surface nodes utilizing the anisotropic Delaunay kernel.'
  WRITE(*,'(a)')'          If .false., insert surface nodes utilizing the isotropic Delaunay kernel.'
  ENDIF
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  Interpolate_Oct_Mapping(F)'
  WRITE(*,'(a)')'          If .FALSE., metric is stored at cell level.'
  WRITE(*,'(a)')'          If .TRUE., metric is stored at node level. '
  WRITE(*,'(a)')'                     This is a more precise but slower method.'
  ENDIF
  WRITE(*,'(a)')'  Gradation_Factor(0.5)'
  WRITE(*,'(a)')'        Gradation factor of background mesh (0.05~3.0).'
  WRITE(*,'(a)')'        A small value results in a strong gradting and a smoother mesh.'
  WRITE(*,'(a)')'          If >=3.0, no gradation.'
  WRITE(*,'(a)')'  Curvature_Factors(:)'
  WRITE(*,'(a)')'        Factors of curvature affect spacing.'
  WRITE(*,'(a)')'        *(1)  contains the minimum spacing as a ratio of the radius of curvature. '
  WRITE(*,'(a)')'        *(2)  contains the maximum distance allowed between the generated '
  WRITE(*,'(a)')'                       surface element and the true geometry (with dimension).'
  WRITE(*,'(a)')'        *(3)  contains the minimum spacing allowed when element sizes are computed '
  WRITE(*,'(a)')'                       according to the underlying curvature (with dimension).'
  WRITE(*,'(a)')'        *(4)  maximum spacing allowed when element sizes are computed according to '
  WRITE(*,'(a)')'                       the underlying curvature (with dimension).'
  WRITE(*,'(a)')'        *(5)  contains the angle dihedral below which ridges are assumed (in degree, 0~180).'
  WRITE(*,'(a)')'        *(6)  contains the angle dihedral below which minimum spacing will be used at the '
  WRITE(*,'(a)')'                       ridges (in degree, 0~180).'
  WRITE(*,'(a)')'        *(7)  The minimum grid size (with dimension) for trailing.'
  WRITE(*,'(a)')' '
  WRITE(*,'(a)')'  Loop_Cosmetic(1)'
  WRITE(*,'(a)')'        The number of cosmetic loops.'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  Loop_CVT(0)'
  WRITE(*,'(a)')'       Set this greater than 1 if you want to optimise a mesh for co-volume methods'
  WRITE(*,'(a)')'  CVT_Time_Limit(0.5)'
  WRITE(*,'(a)')'       Time limit in hours for running the co-volume mesh optimisation'
  ! WRITE(*,'(a)')'  CVT_Method(1)'
  ! WRITE(*,'(a)')'           = 0   cvt reinsert without Voronoi centre smooth.'
  ! WRITE(*,'(a)')'           = 1   cvt reinsert with smooth Voronoi centre by Powell Optimising.'
  ! WRITE(*,'(a)')'           = 2   cvt reinsert with smooth Voronoi centre by PSO Optimising.'
  ! WRITE(*,'(a)')'           = 3   cvt reinsert with smooth Voronoi centre by Cuckoo Optimising.'
  ! WRITE(*,'(a)')'           =-1   smooth Voronoi centre by Powell Optimising, no reinsert.'
  ! WRITE(*,'(a)')'           =-2   smooth Voronoi centre by PSO Optimising, no reinsert.'
  ! WRITE(*,'(a)')'           =-3   smooth Voronoi centre by Cuckoo Optimising, no reinsert.'
  WRITE(*,'(a)')'  Loop_Smooth(5)'
  WRITE(*,'(a)')'        The number of cosmetic smooth iteration.'
  WRITE(*,'(a)')'  Smooth_Method(2)'
  WRITE(*,'(a)')'        The way choosing the direction of a smoothing node.'
  WRITE(*,'(a)')'          =1,  the mean of surrounding nodes.'
  WRITE(*,'(a)')'          =2,  the mean of the imagine nodes that make each element '
  WRITE(*,'(a)')'               has the same voulme.'
  WRITE(*,'(a)')'          =3,  the mean of surrounding nodes with weights.'
  WRITE(*,'(a)')'          =4,  using Powell Optimisation.'
  WRITE(*,'(a)')'  BoundCell_Split(0)'
  WRITE(*,'(a)')'          =0,  do not care all-boundary-node cells.'
  WRITE(*,'(a)')'          =1,  split all-boundary-node cells if posible.'
  WRITE(*,'(a)')'          =2,  always split all-boundary-node cells.'
  WRITE(*,'(a)')'  Collapse_Angle(12.5)'
  WRITE(*,'(a)')'       the maximum dihedral angle (in degree) '
  WRITE(*,'(a)')'           below which element collapse will be attempted.'
  WRITE(*,'(a)')'  Swapping_Angle(30.0)'
  WRITE(*,'(a)')'       the maximum dihedral angle below which edge swapping will be attempted. '
  WRITE(*,'(a)')'  HighOrder_Model(1)'
  WRITE(*,'(a)')'       the High-Order mesh generation model.'
  WRITE(*,'(a)')'          =1,  linear model, no high order treatment.'
  WRITE(*,'(a)')'          =3,  cubic model.'
  WRITE(*,'(a)')' '
  WRITE(*,'(a)')'  NB_BD_Layers(0)'
  WRITE(*,'(a)')'       the number of boundary layers to be generated. (<=100).'
  WRITE(*,'(a)')'          =0,  no boundary layer developed.'
  WRITE(*,'(a)')'  Layer_Heights(0)'
  WRITE(*,'(a)')'       An array (maximum=100) that indicates the thickness '
  WRITE(*,'(a)')'           (in real dimension) of each layer.'
  WRITE(*,'(a)')'  NorSmooth_Method(1)'
  WRITE(*,'(a)')'       The method to smooth the normal directions.'
  WRITE(*,'(a)')'           = 1   smooth the normal gradually every layer.'
  WRITE(*,'(a)')'           else  smooth the normal once at before the layer generation.'
  WRITE(*,'(a)')'  Layer_Hybrid(0)'
  WRITE(*,'(a)')'           = 0   Tet. elements only in the layers.'
  WRITE(*,'(a)')'           = 1   hybrid mesh in the layers. Allow negative contribution for prisms.'
  WRITE(*,'(a)')'           = 2   hybrid mesh in the layers. NOT allow negative contribution for prisms.'
  ENDIF
  WRITE(*,'(a)')' '
  WRITE(*,'(a)')'  Please visit "http://git.swansim.org/swansim/flite-mesh-generators/wikis/home" for more information.'

  RETURN
END SUBROUTINE Help_Message

