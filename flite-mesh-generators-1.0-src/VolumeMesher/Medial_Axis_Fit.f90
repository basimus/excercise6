!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!*******************************************************************************
!>
!!  Basic Split
!!  
!!  Tries to decompose domain using corner points
!<
!*******************************************************************************
SUBROUTINE Basic_Split()
  USE common_Constants
  USE common_Parameters
  USE CellConnectivity
  USE Queue
  USE SurfaceMeshManager
  USE SurfaceMeshStorage
  USE HybridMeshStorage
  USE HybridMeshManager
  USE Plane3DGeom
  USE SurfaceCurvatureManager
  USE TetMeshStorage
  USE Geometry2D
  IMPLICIT NONE

  INTEGER :: noOfRegions, k, s, j, ip, numSurf,ns1,ns2, nc, ip1, ip2, l, i, nextNode,ip3,ip12,ip22
  INTEGER :: iStart,minc,maxc, curFwd, curBac
  REAL*8 :: minNodes,maxNodes,dist, angFwd, angBac
  INTEGER :: s1,s2,s3,s4,s11,s12,s13,s14,a,b,flag,e, e1,c,ip1t,ip2t,o,ie,it1,it2,numCutsTB,numCutsLR
  TYPE(Plane3D) :: aPlane
  TYPE(Plane3D), ALLOCATABLE :: cuttingPlanes(:)
  TYPE(IntQueueType), ALLOCATABLE :: lreg(:),surfToCurve(:), curveToEdge(:)
  INTEGER, ALLOCATABLE :: nsreg(:), opreg(:,:)
  TYPE(IntQueueType) :: nodesToSurf(Surf%NB_Point),nodesToCurve(Surf%NB_Point),singCurvOrd
  TYPE(IntQueueType) :: nodesToElem(Surf%NB_Point),nodesToEdge(Surf%NB_Point),nodesToNode(Surf%NB_Point)
  TYPE(IntQueueType),allocatable :: surfCornerNodes(:),surfOutsideOrdered(:)
  TYPE(IntQueueType),ALLOCATABLE :: boundingCurves(:),surfToEdge(:), curvesOrdered(:),curveLoops(:)
  TYPE(Point3dQueueType), ALLOCATABLE :: curveSampled(:)
  REAL*8, ALLOCATABLE :: top(:,:),bottom(:,:),left(:,:),right(:,:)
  TYPE(Point3dQueueType) :: allSampled,cutRaw
  REAL*8 :: Pt(3), centroid(3),normal(3),minDiff, cross(3), diff, P1(3), P2(3),P12(2),P22(2),P3(3),P(2)
  REAL*8 ::  pPrev(2), pCur(2), pNext(2)
  REAL*8, ALLOCATABLE :: angles(:), distToPlane(:), P2D(:,:)
  REAL*8 :: dot, det,curAngle, width, d1,d2,gap,t,u,v
  INTEGER :: numCount, count,d, globLoop, totEdges,countNode(Surf%NB_Point)
  INTEGER, ALLOCATABLE :: considered(:), edgeInfo(:,:), curves(:,:),edgeToCurve(:),numSampCurve(:)
  INTEGER, ALLOCATABLE :: curvRegToSurf(:), globCurveSamp(:), locCurveToGlob(:)

  INTEGER :: ned,numCurve,sorted(4),isCorner(Surf%NB_Point),smoothLoop,si,noEnd

  INTEGER, ALLOCATABLE :: triangles(:,:),inTri(:),hasCorner(:),oldSurfNum(:)
  INTEGER :: numTri, numCorn,dum1,dum2,nsym,flag2,finalCurve, numLoops,ss,finalSurf
  INTEGER :: offSet

  TYPE(SurfaceCurvatureType) :: Curv,Curv2
  

  REAL*8 :: surfNorm(3),x21,x31,y21,y31,area2,W(3),V1(3),V2(3)

  REAL*8,ALLOCATABLE :: curveSampledOrder(:,:,:)

  INTEGER :: numCuts,numSubCurves,sc,cc

  REAL*8, ALLOCATABLE :: polyx(:),polyy(:),polyz(:),xi(:),yi(:),zi(:),ti(:)
  CHARACTER*80 :: duMtext
  
  
  CALL Surf_BuildNext(Surf)

  !Read in the reg file

  OPEN(922,file = JobName(1:JobNameLength)//'.reg',form='formatted')
  numSurf = 0
  READ(922,'(I10)') noOfRegions
  ALLOCATE(nsreg(noOfRegions),lreg(noOfRegions),opreg(2,noOfRegions))
  DO k = 1,noOfRegions
    READ(922,'(I10)') nsreg(k)
    DO j = 1,nsreg(k)
      READ(922,'(I10)') s
      numSurf = MAX(s,numSurf)
      CALL IntQueue_Push(lreg(k), s)
    END DO
  END DO
  CLOSE(922)

  OPEN(9222,file = JobName(1:JobNameLength)//'.bco',form='formatted')
  
  READ(9222,*) duMtext
  READ(9222,*) dum1 ,dum2 , nsym
  CLOSE(9222)

 

  ALLOCATE(surfOutsideOrdered(2*numSurf),surfCornerNodes(numSurf),oldSurfNum(2*numSurf))

  !Find connectivity information
  CALL Update_Topology(nodesToSurf,nodesToElem,Surf,nodesToNode)
  isCorner(:) = 0
  DO ip = 1,Surf%NB_Point
    IF (nodesToSurf(ip)%numNodes.GT.3) THEN
      !write(24,*) Surf%Posit(1,ip),Surf%Posit(2,ip),Surf%Posit(3,ip)
      isCorner(ip) = 1
      !WRITE(24,*) Surf%Posit(1,ip),Surf%Posit(2,ip),Surf%Posit(3,ip)
      ! DO i = 1,nodesToSurf(ip)%numNodes
      !   s = nodesToSurf(ip)%Nodes(i)
      !   !CALL IntQueue_Push(surfCornerNodes(s),ip)
      ! END DO
    END IF
  END DO


  OPEN(923,file='fort.97',form='formatted')
  READ(923,'(I10)') ned
  WRITE(*,*) ned
  ALLOCATE(edgeInfo(11,ned))
  DO i = 1,ned
    READ(923,'(11I10)') edgeInfo(1:11,i)
    ip = edgeInfo(2,i)
   ! WRITE(24,*) Surf%Posit(1,ip),Surf%Posit(2,ip),Surf%Posit(3,ip)
    ip = edgeInfo(3,i)
  !  WRITE(24,*) Surf%Posit(1,ip),Surf%Posit(2,ip),Surf%Posit(3,ip)
  END DO
  CLOSE(923)


  ! DO s = 1,numSurf
    
  !   DO i = 1,ned
  !     IF ((edgeInfo(8,i).EQ.s).OR.(edgeInfo(9,i).EQ.s).OR.(edgeInfo(10,i).EQ.s).OR.(edgeInfo(11,i).EQ.s)) THEN

  !       ip = edgeInfo(2,i)
  !       WRITE(24+s,*) Surf%Posit(1,ip),Surf%Posit(2,ip),Surf%Posit(3,ip)
  !       ip = edgeInfo(3,i)
  !       WRITE(24+s,*) Surf%Posit(1,ip),Surf%Posit(2,ip),Surf%Posit(3,ip)

  !     END IF

  !   END DO  
    
  ! END DO


  !Need to count the number of intersection curves and relate these back to the surfaces
  ALLOCATE(curves(4,ned),surfToCurve(2*numSurf),curveToEdge(ned),considered(ned),edgeToCurve(ned),surfToEdge(numSurf))
  numCurve = 0
  
  DO i = 1,ned

    sorted(:) = sort4values(edgeInfo(8,i),edgeInfo(9,i),edgeInfo(10,i),edgeInfo(11,i))
    s11 = sorted(1)
    s12 = sorted(2)
    s13 = sorted(3)
    s14 = sorted(4)
    CALL IntQueue_Push(nodesToEdge(edgeInfo(2,i)),i)
    CALL IntQueue_Push(nodesToEdge(edgeInfo(3,i)),i)
    IF (s11.GT.0) THEN
    	CALL IntQueue_Push(surfToEdge(s11),i)
    END IF
    IF (s12.GT.0) THEN
    	CALL IntQueue_Push(surfToEdge(s12),i)
    END IF
    IF (s13.GT.0) THEN
    	CALL IntQueue_Push(surfToEdge(s13),i)
    END IF
    IF (s14.GT.0) THEN
    	CALL IntQueue_Push(surfToEdge(s14),i)
    END IF

   
    

    IF (numCurve.EQ.0) THEN
      s1 = s11
      s2 = s12
      s3 = s13
      s4 = s14
      numCurve = numCurve + 1
      curves(1,numCurve) = s1
      curves(2,numCurve) = s2
      curves(3,numCurve) = s3
      curves(4,numCurve) = s4
      IF (s1.GT.0) THEN
        CALL IntQueue_Push(surfToCurve(s1),numCurve)
      END IF
      IF (s2.GT.0) THEN
        CALL IntQueue_Push(surfToCurve(s2),numCurve)
      END IF
      IF (s3.GT.0) THEN
        CALL IntQueue_Push(surfToCurve(s3),numCurve)
      END IF
      IF (s4.GT.0) THEN
        CALL IntQueue_Push(surfToCurve(s4),numCurve)
      END IF
      CALL IntQueue_Push(curveToEdge(numCurve),i)
      edgeToCurve(i) = numCurve
      IF (.NOT.IntQueue_Contain(nodesToCurve(edgeInfo(2,i)), numCurve)) THEN
        CALL IntQueue_Push(nodesToCurve(edgeInfo(2,i)),numCurve)
      END IF
      IF (.NOT.IntQueue_Contain(nodesToCurve(edgeInfo(3,i)), numCurve)) THEN
        CALL IntQueue_Push(nodesToCurve(edgeInfo(3,i)),numCurve)
      END IF
    ELSE
      flag = 0
      DO k = 1,numCurve

        sorted(:) = sort4values(curves(1,k),curves(2,k),curves(3,k),curves(4,k))
        s1 = sorted(1)
        s2 = sorted(2)
        s3 = sorted(3)
        s4 = sorted(4)
          
       IF ((s11.EQ.s1).AND.(s12.EQ.s2).AND.(s13.EQ.s3).AND.(s14.EQ.s4)) THEN

        CALL IntQueue_Push(curveToEdge(k),i)
        edgeToCurve(i) = k
        IF (.NOT.IntQueue_Contain(nodesToCurve(edgeInfo(2,i)), k)) THEN
        CALL IntQueue_Push(nodesToCurve(edgeInfo(2,i)),k)
      END IF
      IF (.NOT.IntQueue_Contain(nodesToCurve(edgeInfo(3,i)), k)) THEN
        CALL IntQueue_Push(nodesToCurve(edgeInfo(3,i)),k)
      END IF
        flag = 1
   
       END IF

      END DO

      IF (flag.EQ.0) THEN
        numCurve = numCurve + 1
        curves(1,numCurve) = s11
        curves(2,numCurve) = s12
        curves(3,numCurve) = s13
        curves(4,numCurve) = s14
        IF (s11.GT.0) THEN
          CALL IntQueue_Push(surfToCurve(s11),numCurve)
        END IF
        IF (s12.GT.0) THEN
          CALL IntQueue_Push(surfToCurve(s12),numCurve)
        END IF
        IF (s13.GT.0) THEN
          CALL IntQueue_Push(surfToCurve(s13),numCurve)
        END IF
        IF (s14.GT.0) THEN
          CALL IntQueue_Push(surfToCurve(s14),numCurve)
        END IF
        CALL IntQueue_Push(curveToEdge(numCurve),i)
        edgeToCurve(i) = numCurve
        IF (.NOT.IntQueue_Contain(nodesToCurve(edgeInfo(2,i)), numCurve)) THEN
        CALL IntQueue_Push(nodesToCurve(edgeInfo(2,i)),numCurve)
      END IF
      IF (.NOT.IntQueue_Contain(nodesToCurve(edgeInfo(3,i)), numCurve)) THEN
        CALL IntQueue_Push(nodesToCurve(edgeInfo(3,i)),numCurve)
      END IF
      END IF
    END IF

  END DO

  ALLOCATE(hasCorner(ned))

  !Now to loop round and find with curves have corners or not
 

  Curv%NB_Curve = 0
  DO i = 1,numCurve
   WRITE(*,*) 'curve ', i,' has ',curveToEdge(i)%numNodes,' edges' 
  END DO

  DO i = 1,numSurf
    WRITE(*,*) 'surface ',i,' bounded by ',surfToCurve(i)%numNodes,' curves'
  END DO

  ALLOCATE(globCurveSamp(ned))

  ALLOCATE(curvesOrdered(ned))

  !Record the current number of curves
  finalCurve = numCurve
  hasCorner(:) = 1
  isCorner(:) = 0

  !Order the curves which have corners
  DO c = 1,numCurve
   
    WRITE(*,*) 'Ordering curve ',c,'\',numCurve, ' which has ',curveToEdge(c)%numNodes, ' edges'
    countNode(:) = 0

  	!Find the first node
  	DO j = 1,curveToEdge(c)%numNodes
  		  e = curveToEdge(c)%nodes(j)
        ip1t = edgeInfo(2,e)
        ip2t = edgeInfo(3,e)
        countNode(ip1t) = countNode(ip1t)+1
        countNode(ip2t) = countNode(ip2t)+1
    END DO

    DO j = 1,curveToEdge(c)%numNodes
        e = curveToEdge(c)%nodes(j)
        ip1t = edgeInfo(2,e)
        ip2t = edgeInfo(3,e)
        !WRITE(*,*) ip1t,ip2t,countNode(ip1t) , countNode(ip2t)
          
    END DO

    !Now we need to count how many end nodes there are in this curve
    noEnd = 0
    DO ip = 1,Surf%NB_Point
      IF (countNode(ip).EQ.1) THEN
        isCorner(ip) = 1
        noEnd = noEnd + 1
      END IF
    END DO

    IF (noEnd.EQ.0) THEN
      hasCorner(c) = 0 !This curve loops around
      e1 = curveToEdge(c)%Nodes(1)
      ip1 = edgeInfo(2,e1)
      ip2 = edgeInfo(3,e1)

      CALL IntQueue_Push(curvesOrdered(c),ip1)
      !WRITE(24+c,*) Surf%Posit(1,ip1),Surf%Posit(2,ip1),Surf%Posit(3,ip1)
      CALL IntQueue_Push(curvesOrdered(c),ip2)
      !WRITE(24+c,*) Surf%Posit(1,ip2),Surf%Posit(2,ip2),Surf%Posit(3,ip2)


      flag = 0
      considered(:) = 0
      considered(e1) = 1

      DO WHILE (flag.EQ.0)
        DO j = 1,curveToEdge(c)%numNodes
          e = curveToEdge(c)%nodes(j)
          
          IF (considered(e).EQ.0) THEN

            IF ((edgeInfo(2,e).EQ.ip2).OR.(edgeInfo(3,e).EQ.ip2)) THEN
                  IF (edgeInfo(2,e).EQ.ip2) THEN
                    ip2t = edgeInfo(3,e)
                    ip1t = edgeInfo(2,e)
                  ELSE
                    ip2t = edgeInfo(2,e)
                    ip1t = edgeInfo(3,e)
                  END IF
                  !This could be the next edge
     
                  IF (ip2t.EQ.curvesOrdered(c)%Nodes(1)) THEN
                    !Finished
                    ip1t = edgeInfo(2,e)
                    ip2t = edgeInfo(3,e)
                    CALL IntQueue_Push(curvesOrdered(c),ip2t)
                !WRITE(*,*)ip1t,ip2t,countNode(ip1t),countNode(ip2t)
                    considered(e) = 1
                    flag = 1
                  ELSE
                    IF (.NOT.IntQueue_Contain(curvesOrdered(c),ip2t)) THEN
                      ip1 = ip1t
                      ip2 = ip2t
                      considered(e) = 1
                      CALL IntQueue_Push(curvesOrdered(c),ip2)
                      !WRITE(*,*) edgeInfo(2,e),edgeInfo(3,e),countNode(edgeInfo(2,e)),countNode(edgeInfo(3,e))
                      !WRITE(24+c,*) Surf%Posit(1,ip2),Surf%Posit(2,ip2),Surf%Posit(3,ip2)      
                      EXIT
                    
                    END IF
                  END IF
              END IF
          END IF
        END DO
      END DO


    ELSE
      considered(:) = 0
      numSubCurves = noEnd/2
      WRITE(*,*) 'Curve ',c,'\',numCurve, ' has ',numSubCurves, ' sub curves'

      DO sc = 1,numSubCurves
        WRITE(*,*) 'Sub curve ',sc,'\',numSubCurves
        IF (sc.EQ.1) THEN
          cc = c
        ELSE
          cc = finalCurve+1  !This is a new curve
          finalCurve = cc

          !Update some data structure
          s1 = curves(1,c)
          s2 = curves(2,c)
          s3 = curves(3,c)
          s4 = curves(4,c)

          curves(:,cc) = curves(:,c)

          IF (s1.GT.0) THEN
            CALL IntQueue_Push(surfToCurve(s1),cc)
          END IF
          IF (s2.GT.0) THEN
            CALL IntQueue_Push(surfToCurve(s2),cc)
          END IF
          IF (s3.GT.0) THEN
            CALL IntQueue_Push(surfToCurve(s3),cc)
          END IF
          IF (s4.GT.0) THEN
            CALL IntQueue_Push(surfToCurve(s4),cc)
          END IF

        END IF

        e1 = -1

        DO j = 1,curveToEdge(c)%numNodes
      	   	e = curveToEdge(c)%nodes(j)
            IF (considered(e).EQ.0) THEN
              ip1t = edgeInfo(2,e)
              ip2t = edgeInfo(3,e)
              !WRITE(*,*)ip1t,ip2t,countNode(ip1t),countNode(ip2t),edgeInfo(8,e),edgeInfo(9,e),edgeInfo(10,e),edgeInfo(11,e)
               IF ((countNode(ip1t).EQ.1)) THEN
              	   ip1 = ip1t
                  e1 = e
              END IF
            END IF
        END DO

        IF (e1.EQ.-1) THEN
        WRITE(*,*) 'shouldnt be here ask Sean for help'
        ! !Find the first node
        ! DO c = 1,
        ! c = surfToCurve(s)%Nodes(1)
        ! e1 = curveToEdge(c)%Nodes(1)
      END IF

       !WRITE(*,*)ip1,ip2,countNode(ip1),countNode(ip2),considered(e1)

        ip1 = edgeInfo(2,e1)
        ip2 = edgeInfo(3,e1)

        CALL IntQueue_Push(curvesOrdered(cc),ip1)
        !WRITE(24+cc,*) Surf%Posit(1,ip1),Surf%Posit(2,ip1),Surf%Posit(3,ip1)
        CALL IntQueue_Push(curvesOrdered(cc),ip2)
        !WRITE(24+cc,*) Surf%Posit(1,ip2),Surf%Posit(2,ip2),Surf%Posit(3,ip2)
        considered(e1) = 1


        

        IF (countNode(ip2).NE.1) THEN  !This checks for single edge

          flag = 0
         
          DO WHILE (flag.EQ.0)
          	DO j = 1,curveToEdge(c)%numNodes
          		e = curveToEdge(c)%nodes(j)
          		
          		IF (considered(e).EQ.0) THEN

          			IF ((edgeInfo(2,e).EQ.ip2).OR.(edgeInfo(3,e).EQ.ip2)) THEN
      		            IF (edgeInfo(2,e).EQ.ip2) THEN
      		              ip2t = edgeInfo(3,e)
      		              ip1t = edgeInfo(2,e)
      		            ELSE
      		              ip2t = edgeInfo(2,e)
      		              ip1t = edgeInfo(3,e)
      		            END IF

      	              !This could be the next edge
      	 
      	              IF (countNode(ip2t).EQ.1) THEN
      	                !Finished
      	                ip1t = edgeInfo(2,e)
      			            ip2t = edgeInfo(3,e)

      			           !WRITE(*,*)ip1t,ip2t,countNode(ip1t),countNode(ip2t)
                       CALL IntQueue_Push(curvesOrdered(cc),ip2t)
      	                considered(e) = 1
      	                flag = 1
      	              ELSE
      	              	!IF (.NOT.IntQueue_Contain(curvesOrdered(cc),ip2t)) THEN
      	              		ip1 = ip1t
      	              		ip2 = ip2t
      	              		considered(e) = 1
      	                	CALL IntQueue_Push(curvesOrdered(cc),ip2)
      	                	!WRITE(*,*) edgeInfo(2,e),edgeInfo(3,e),countNode(edgeInfo(2,e)),countNode(edgeInfo(3,e))
          					     !WRITE(24+cc,*) Surf%Posit(1,ip2),Surf%Posit(2,ip2),Surf%Posit(3,ip2)
      	                	
      	                	EXIT
      	                
      	                !END IF

      	              
      	              END IF
      		          

      		        END IF


          		END IF

          	END DO
          END DO

        END IF
      END DO

    END IF
    WRITE(*,*) SUM(considered), ' edges found'

   
  END DO

  ALLOCATE(curveLoops(ned))

  finalSurf = numSurf
  oldSurfNum(:) = 0

  DO s =  noOfRegions+1-nsym,numSurf
    WRITE(*,*)'Surface ',s
    !Need to find loops of curves
    considered(:) = 0
    numLoops = 0

    !First find curves which themselves are loops
    DO i = 1,surfToCurve(s)%numNodes
      c = surfToCurve(s)%nodes(i)
      IF (curvesOrdered(c)%nodes(1).EQ.curvesOrdered(c)%nodes(curvesOrdered(c)%numNodes)) THEN
        numLoops = numLoops + 1
        CALL IntQueue_Clear(curveLoops(numLoops))
        CALL IntQueue_Push(curveLoops(numLoops),c)
        considered(c) = 1
      END IF
    END DO

    DO WHILE (SUM(considered).NE.surfToCurve(s)%numNodes) 
      !Now we need to search for the remaining loops
      flag = 0
      numLoops = numLoops + 1
      CALL IntQueue_Clear(curveLoops(numLoops))
      cc = -1
      DO i = 1,surfToCurve(s)%numNodes
          c = surfToCurve(s)%nodes(i)
          IF (considered(c).EQ.0) THEN
            ip1 = curvesOrdered(c)%nodes(1)
            ip2 = curvesOrdered(c)%nodes(curvesOrdered(c)%numNodes)
            cc = c
          END IF
      END DO
      considered(cc) = 1
      CALL IntQueue_Push(curveLoops(numLoops),cc)
      WRITE(*,*) numLoops,ip1,ip2

      DO WHILE (flag.EQ.0)
        DO i = 1,surfToCurve(s)%numNodes
          c = surfToCurve(s)%nodes(i)
          IF (considered(c).EQ.0) THEN
            IF ((curvesOrdered(c)%nodes(1).EQ.ip2).OR.(curvesOrdered(c)%nodes(curvesOrdered(c)%numNodes).EQ.ip2)) THEN
              !This is the next curve
              CALL IntQueue_Push(curveLoops(numLoops),c)
              considered(c) = 1
              IF (curvesOrdered(c)%nodes(1).EQ.ip2) THEN
                ip1 = curvesOrdered(c)%nodes(1)
                ip2 = curvesOrdered(c)%nodes(curvesOrdered(c)%numNodes)
              ELSE
                ip2 = curvesOrdered(c)%nodes(1)
                ip1 = curvesOrdered(c)%nodes(curvesOrdered(c)%numNodes)
              END IF
              WRITE(*,*) numLoops,ip1,ip2
              IF (ip2.EQ.curvesOrdered(cc)%nodes(1)) THEN
                flag = 1
              ELSE
                EXIT
              END IF


            END IF
          END IF
        END DO
      END DO

    END DO

    WRITE(*,*)'Surface ',s,' has ', numLoops, ' loops'

    DO k = 1,numLoops
      !Order nodes in each loop
      IF (k.EQ.1) THEN
        ss = s
      ELSE
        ss = finalSurf + 1
        finalSurf = ss
        DO i = 1,curveLoops(k)%numNodes
          c = curveLoops(k)%nodes(i)
          CALL IntQueue_Remove(surfToCurve(s),c)
          CALL IntQueue_Push(surfToCurve(ss),c)
        END DO


      END IF

      oldSurfNum(ss) = s

      c = curveLoops(k)%nodes(1)
      ip1 = curvesOrdered(c)%nodes(1)
      ip2 = curvesOrdered(c)%nodes(curvesOrdered(c)%numNodes)
      
      DO j = 1,curvesOrdered(c)%numNodes
        ip = curvesOrdered(c)%nodes(j)
        CALL IntQueue_Push(surfOutsideOrdered(ss),ip)
      END DO

      IF (curveLoops(k)%numNodes.GT.1) THEN      

        DO i = 2,curveLoops(k)%numNodes
          c = curveLoops(k)%nodes(i)
          ip1t = curvesOrdered(c)%nodes(1)
          ip2t = curvesOrdered(c)%nodes(curvesOrdered(c)%numNodes)
          
          IF (ip1t.EQ.ip2) THEN
            !The curve is already in the correct order
            DO j = 2,curvesOrdered(c)%numNodes
              ip = curvesOrdered(c)%nodes(j)
              CALL IntQueue_Push(surfOutsideOrdered(ss),ip)
            END DO    
            ip1 = ip1t
            ip2 = ip2t
          ELSE
            !The curve is not in the correct order
            DO j = curvesOrdered(c)%numNodes-1,1,-1
              ip = curvesOrdered(c)%nodes(j)
              CALL IntQueue_Push(surfOutsideOrdered(ss),ip)
            END DO    
            ip2 = ip1t
            ip1 = ip2t

          END IF
        END DO

        !Remove last node
        CALL IntQueue_RemoveLast(surfOutsideOrdered(ss))

      END IF




      DO i = 1,surfOutsideOrdered(ss)%numNodes
       ip = surfOutsideOrdered(ss)%Nodes(i)
       WRITE(24+ss,*) Surf%Posit(1,ip),Surf%Posit(2,ip),Surf%Posit(3,ip)

      END DO

    END DO

    

   

   

  END DO

  numCurve = finalCurve



  

    !Now to create the dat file
    Curv%NB_Region = numSurf - noOfRegions
    ALLOCATE(Curv%Regions(2*Curv%NB_Region))  !Allocate for twice the possible number of regions incase need split
    ALLOCATE(curvRegToSurf(finalSurf))
    curvRegToSurf(:) = 0
    Curv%NB_Region = 0

    numCuts = 20
    ALLOCATE(curveSampledOrder(3,numCuts*2,numCurve))
    
    globCurveSamp(:) = -1


    DO s = noOfRegions+1-nsym,finalSurf
      ss = oldSurfNum(s)
      WRITE(*,*)'Building surface ',s,' part of surface ',ss

      !Start building the curvature
      Curv%NB_Region = Curv%NB_Region + 1
      Curv%Regions(Curv%NB_Region)%ID       =  Curv%NB_Region
      curvRegToSurf(S) = Curv%NB_Region
      Curv%Regions(Curv%NB_Region)%TopoType = 1
      Curv%Regions(Curv%NB_Region)%numNodeU = numCuts
      Curv%Regions(Curv%NB_Region)%numNodeV = numCuts
      
      ALLOCATE(Curv%Regions(Curv%NB_Region)%Posit(3,numCuts,numCuts))


      !First calculate the average normal and centroid of this surface

      centroid(:) = 0.0d0
      count = 0
      DO i = 1,surfOutsideOrdered(s)%numNodes
        ip = surfOutsideOrdered(s)%nodes(i)
        
        centroid(:) = centroid(:) + Surf%Posit(:,ip)
        count = count + 1
        
      END DO

      centroid(:) = centroid(:)/REAL(count)

      surfNorm(:) = 0.0d0
      count = 0
      DO i = 1,surfOutsideOrdered(s)%numNodes-1
        P1(:) = Surf%Posit(:,surfOutsideOrdered(s)%Nodes(i))
        P2(:) = Surf%Posit(:,surfOutsideOrdered(s)%Nodes(i+1))
        normal(:) = Geo3D_Cross_Product(P1,centroid,P2)
        surfNorm(:) = surfNorm(:) + normal(:)/Geo3D_Distance(normal)
        count = count + 1
      END DO

      surfNorm(:) = surfNorm(:)/count
      surfNorm(:) = surfNorm(:)/Geo3D_Distance(surfNorm)

      !We need to have surfaces normal to this
     



      !We now need to fit a function to each intersection curve
      nC = surfToCurve(s)%numNodes

      
      
      ip1 = -1
      !First find the first corner
      DO i = 1,surfOutsideOrdered(s)%numNodes
        ip = surfOutsideOrdered(s)%Nodes(i)
        IF (isCorner(ip).EQ.1) THEN
          ip1 = i
          EXIT
        END IF
      END DO
      IF (ip1.LT.0) THEN
        !This means the loop is a loop with one curve
        WRITE(*,*) 'Skipping surface ', s
        curvRegToSurf(s) = -1
        CYCLE

      END IF

      ALLOCATE(boundingCurves(nC))
      ALLOCATE(locCurveToGlob(nC))
      ALLOCATE(curveSampled(nC))
      flag = 0
      c = 1
      iStart = ip1
      i = iStart
      DO WHILE (flag.EQ.0)
        ip = surfOutsideOrdered(s)%Nodes(i)
        IF ((isCorner(ip).EQ.1).AND.(i.NE.iStart)) THEN
          !This needs to go into the current and next curve
          CALL IntQueue_Push(boundingCurves(c),ip)
          IF (c.EQ.nC) THEN
            CALL IntQueue_Push(boundingCurves(1),ip)
            c = 1
          ELSE
            CALL IntQueue_Push(boundingCurves(c+1),ip)
            c = c+1
          END IF
        ELSE
          CALL IntQueue_Push(boundingCurves(c),ip)
        END IF

        IF (i.EQ.surfOutsideOrdered(s)%numNodes) THEN
          i = 1
        ELSE
          i = i + 1
        END IF

        IF (i.EQ.iStart) THEN
          ip = surfOutsideOrdered(s)%Nodes(i)
          CALL IntQueue_Push(boundingCurves(c),ip)
          flag = 1
        END IF

      END DO

      !Now we need to identify a top,bottom,left and right for this set of curves
      !First count the number of curves with nodes
      
      k = 0
      minNodes = HUGE(0.0d0)
      maxNodes = 0.0d0
      DO c = 1,nC

        IF (boundingCurves(c)%numNodes.GT.0) THEN
          k = k + 1
          ip1 = boundingCurves(c)%nodes(1)
          ip2 = boundingCurves(c)%nodes(boundingCurves(c)%numNodes)
          diff = Geo3D_Distance_SQ(Surf%Posit(:,ip1),Surf%Posit(:,ip2))

          IF (diff.GT.maxNodes) THEN
            maxc = c
            maxNodes = diff
          END iF
          IF (diff.LT.minNodes) THEN
            minc = c
            minNodes = diff
          END iF
        END IF
      END DO

      ALLOCATE(numSampCurve(nC))

      DO c = 1,nC
       IF (boundingCurves(c)%numNodes.GT.0) THEN
        ip1 = boundingCurves(c)%nodes(1)
        ip2 = boundingCurves(c)%nodes(boundingCurves(c)%numNodes)
        DO i = 1,surfToCurve(s)%numNodes
          s1 = surfToCurve(s)%nodes(i)
          IF (IntQueue_Contain(curvesOrdered(s1),ip1).AND.IntQueue_Contain(curvesOrdered(s1),ip2)) THEN
            locCurveToGlob(c) = s1
          END IF
        END DO
       END IF
      END DO

      Curv%Regions(Curv%NB_Region)%numCurve = k
      ALLOCATE(Curv%Regions(Curv%NB_Region)%IC(k))
      k = 0
      DO c = 1,nC
       IF (boundingCurves(c)%numNodes.GT.0) THEN
          k = k + 1
      	  Curv%Regions(Curv%NB_Region)%IC(k) = locCurveToGlob(c)
       END IF
      END DO
      

      !Need to workout how many samples to do per curve
      numSampCurve(:) = numCuts  
      

      IF (SUM(numSampCurve).NE.4*numCuts) THEN

        numSampCurve(:) = numCuts

       

        IF (k.EQ.2) THEN
          numSampCurve(1) = 2*numCuts
          numSampCurve(2) = 2*numCuts
        END IF

        IF (k.EQ.3) THEN
          numSampCurve(maxc) = 2*numCuts
        END IF

        IF (k.GE.5) THEN
          numSampCurve(minc) = 2

          !Now we want to find the smallest angle to the next curve
          ip1 = boundingCurves(minc)%nodes(1)
          ip2 = boundingCurves(minc)%nodes(boundingCurves(minc)%numNodes)

          !Look foward
          IF (minc.EQ.nC) THEN
            curFwd = 1
          ELSE
            curFwd = minc+1
          END IF

          !Look back
          IF (minc.EQ.1) THEN
            curBac = nC
          ELSE
            curBac = nc-1
          END iF

          ip12 = boundingCurves(curFwd)%nodes(1)
          ip22 = boundingCurves(curFwd)%nodes(boundingCurves(curFwd)%numNodes)
          angFwd = Geo3D_Included_Angle(Surf%Posit(:,ip1),Surf%Posit(:,ip2),Surf%Posit(:,ip22))

          ip12 = boundingCurves(curBac)%nodes(1)
          ip22 = boundingCurves(curBac)%nodes(boundingCurves(curBac)%numNodes)
          angBac = Geo3D_Included_Angle(Surf%Posit(:,ip2),Surf%Posit(:,ip1),Surf%Posit(:,ip12))

          IF (angFwd.GT.angBac) THEN
            

            numSampCurve(curBac) = numCuts - numSampCurve(minc)
            s2 = curBac
          ELSE
            
            numSampCurve(curFwd) = numCuts - numSampCurve(minc)
            s2 = curFwd
          END IF

         
        END IF

        IF (k.GE.6) THEN
          s1 = minc
          minNodes = HUGE(0.0d0)
          DO c = 1,nC
            IF ((boundingCurves(c)%numNodes.GT.0).AND.(c.NE.s1).AND.(c.NE.s2)) THEN
              ip1 = boundingCurves(c)%nodes(1)
              ip2 = boundingCurves(c)%nodes(boundingCurves(c)%numNodes)
              diff = Geo3D_Distance_SQ(Surf%Posit(:,ip1),Surf%Posit(:,ip2))
              IF (diff.LT.minNodes) THEN
                minc = c
                minNodes = diff
              END iF
            END IF
          END DO

          numSampCurve(minc) = 2!MAX(2,numCuts*INT(minNodes/maxNodes))

           !Now we want to find the smallest angle to the next curve
          ip1 = boundingCurves(minc)%nodes(1)
          ip2 = boundingCurves(minc)%nodes(boundingCurves(minc)%numNodes)

          !Look foward
          IF (minc.EQ.nC) THEN
            curFwd = 1
          ELSE
            curFwd = minc+1
          END IF

          !Look back
          IF (minc.EQ.1) THEN
            curBac = nC
          ELSE
            curBac = nc-1
          END iF

          ip12 = boundingCurves(curFwd)%nodes(1)
          ip22 = boundingCurves(curFwd)%nodes(boundingCurves(curFwd)%numNodes)
          angFwd = Geo3D_Included_Angle(Surf%Posit(:,ip1),Surf%Posit(:,ip2),Surf%Posit(:,ip22))

          ip12 = boundingCurves(curBac)%nodes(1)
          ip22 = boundingCurves(curBac)%nodes(boundingCurves(curBac)%numNodes)
          angBac = Geo3D_Included_Angle(Surf%Posit(:,ip2),Surf%Posit(:,ip1),Surf%Posit(:,ip12))


          IF (((angFwd.GT.angBac).AND.(curBac.NE.s2)).OR.(curFwd.EQ.s2)) THEN
            numSampCurve(curBac) = numCuts - numSampCurve(minc)
            s2 = curBac
          ELSE
            numSampCurve(curFwd) = numCuts - numSampCurve(minc)
            s2 = curFwd
          END IF
        END IF

        IF (k.GT.6) THEN
          DO WHILE (SUM(numSampCurve).GT.(4*numCuts))
            DO c = 1,nC
              IF (numSampCurve(c).GT.3) THEN
                numSampCurve(c) = numSampCurve(c)-1
                IF (SUM(numSampCurve).EQ.(4*numCuts)) THEN
                  EXIT
                END IF

              END IF
            END DO
          END DO

        END IF



      END IF

      DO c = 1,nC
        IF (boundingCurves(c)%numNodes.GT.0) THEN
          globCurveSamp(locCurveToGlob(c)) = numSampCurve(c)
        END IF
      END DO

      WRITE(*,*)k,SUM(numSampCurve)

      !Now to fit functions to these curves and resample
      DO c = 1,nC
      	IF (boundingCurves(c)%numNodes.GT.0) THEN
      	!First samples for the global array
      	d = MIN(2,boundingCurves(c)%numNodes-1)
     	d = MAX(1,d)
      
     	ALLOCATE(polyx(d+1),polyy(d+1),polyz(d+1))

      	ALLOCATE(xi(boundingCurves(c)%numNodes))
      	ALLOCATE(yi(boundingCurves(c)%numNodes))
      	ALLOCATE(zi(boundingCurves(c)%numNodes))
     	 ALLOCATE(ti(boundingCurves(c)%numNodes))

      	P1(:) = Surf%Posit(:,boundingCurves(c)%Nodes(1))
      	P2(:) = Surf%Posit(:,boundingCurves(c)%Nodes(boundingCurves(c)%numNodes))

      	DO i = 1,boundingCurves(c)%numNodes
      	  ip = boundingCurves(c)%Nodes(i)
      	  xi(i) = Surf%Posit(1,ip)
      	  yi(i) = Surf%Posit(2,ip)
      	  zi(i) = Surf%Posit(3,ip)
      	  !ti(i) = REAL(i)
      	  P3(:) = Geo3D_ProjectOnLine(P1,P2,Surf%Posit(:,ip),ti(i)) 
      	END DO
	
     	 polyx = polyfit(ti,xi,d)
     	 polyy = polyfit(ti,yi,d)
     	 polyz = polyfit(ti,zi,d)

     	 Pt(1) = xi(1)
     	 Pt(2) = yi(1)
     	 Pt(3) = zi(1)

     	 curveSampledOrder(:,1,locCurveToGlob(c)) = Pt(:)

     	 DO i = 1,(2*numCuts)-2

          t = (REAL(i)*(ti(boundingCurves(c)%numNodes)-ti(1))/REAL(2*numCuts)) + ti(1)
          Pt(:) = 0.0d0
          DO o = 1,d+1
            Pt(1) = Pt(1) + polyx(o)*(t**REAL(o-1))
            Pt(2) = Pt(2) + polyy(o)*(t**REAL(o-1))
            Pt(3) = Pt(3) + polyz(o)*(t**REAL(o-1))
          END DO

          curveSampledOrder(:,i+1,locCurveToGlob(c)) = Pt(:)

        END DO

        Pt(1) = xi(boundingCurves(c)%numNodes)
        Pt(2) = yi(boundingCurves(c)%numNodes)
        Pt(3) = zi(boundingCurves(c)%numNodes)

        curveSampledOrder(:,2*numCuts,locCurveToGlob(c)) = Pt(:)

        DEALLOCATE(xi,yi,zi,ti,polyx,polyy,polyz)

        
          d = MIN(2,boundingCurves(c)%numNodes-1)
          d = MAX(1,d)
          
          ALLOCATE(polyx(d+1),polyy(d+1),polyz(d+1))

          ALLOCATE(xi(boundingCurves(c)%numNodes))
          ALLOCATE(yi(boundingCurves(c)%numNodes))
          ALLOCATE(zi(boundingCurves(c)%numNodes))
          ALLOCATE(ti(boundingCurves(c)%numNodes))

          P1(:) = Surf%Posit(:,boundingCurves(c)%Nodes(1))
          P2(:) = Surf%Posit(:,boundingCurves(c)%Nodes(boundingCurves(c)%numNodes))

          DO i = 1,boundingCurves(c)%numNodes
            ip = boundingCurves(c)%Nodes(i)
            xi(i) = Surf%Posit(1,ip)
            yi(i) = Surf%Posit(2,ip)
            zi(i) = Surf%Posit(3,ip)
            !ti(i) = REAL(i)
            P3(:) = Geo3D_ProjectOnLine(P1,P2,Surf%Posit(:,ip),ti(i)) 
          END DO

          polyx = polyfit(ti,xi,d)
          polyy = polyfit(ti,yi,d)
          polyz = polyfit(ti,zi,d)

          Pt(1) = xi(1)
          Pt(2) = yi(1)
          Pt(3) = zi(1)

          CALL Point3dQueue_Push(curveSampled(c),Pt)

          IF (numSampCurve(c).EQ.2*numCuts) THEN

            DO i = 2,numCuts

              t = (REAL(i)*(ti(boundingCurves(c)%numNodes)-ti(1))/REAL(numSampCurve(c))) + ti(1)
              
              Pt(:) = 0.0d0
              DO o = 1,d+1
                Pt(1) = Pt(1) + polyx(o)*(t**REAL(o-1))
                Pt(2) = Pt(2) + polyy(o)*(t**REAL(o-1))
                Pt(3) = Pt(3) + polyz(o)*(t**REAL(o-1))
              END DO

              CALL Point3dQueue_Push(curveSampled(c),Pt)

            END DO

            DO i = numCuts,numSampCurve(c)-2

              t = (REAL(i)*(ti(boundingCurves(c)%numNodes)-ti(1))/REAL(numSampCurve(c))) + ti(1)
              
              Pt(:) = 0.0d0
              DO o = 1,d+1
                Pt(1) = Pt(1) + polyx(o)*(t**REAL(o-1))
                Pt(2) = Pt(2) + polyy(o)*(t**REAL(o-1))
                Pt(3) = Pt(3) + polyz(o)*(t**REAL(o-1))
              END DO

              CALL Point3dQueue_Push(curveSampled(c),Pt)

            END DO



          ELSE

            DO i = 1,numSampCurve(c)-2

              t = (REAL(i)*(ti(boundingCurves(c)%numNodes)-ti(1))/REAL(numSampCurve(c))) + ti(1)
              Pt(:) = 0.0d0
              DO o = 1,d+1
                Pt(1) = Pt(1) + polyx(o)*(t**REAL(o-1))
                Pt(2) = Pt(2) + polyy(o)*(t**REAL(o-1))
                Pt(3) = Pt(3) + polyz(o)*(t**REAL(o-1))
              END DO

              CALL Point3dQueue_Push(curveSampled(c),Pt)

            END DO

          END IF

          Pt(1) = xi(boundingCurves(c)%numNodes)
          Pt(2) = yi(boundingCurves(c)%numNodes)
          Pt(3) = zi(boundingCurves(c)%numNodes)

          CALL Point3dQueue_Push(curveSampled(c),Pt)


          DEALLOCATE(xi,yi,zi,ti,polyx,polyy,polyz)
        END IF

      END DO
     
      CALL Point3dQueue_Clear(allSampled)
      !Now to sort these out into top,bottom,left and right
      DO c = 1,nC   
        
        DO i = 1,curveSampled(c)%numNodes
          
          Pt(:) = curveSampled(c)%Pt(:,i)
         
          CALL Point3dQueue_Push(allSampled,Pt)
         
        END DO
      END DO

      ALLOCATE(top(3,numCuts),bottom(3,numCuts),left(3,numCuts),right(3,numCuts))
      
      DO i = 1,numCuts
        top(:,i) = allSampled%Pt(:,i)
        right(:,i) = allSampled%Pt(:,i+numCuts)
        bottom(:,i) = allSampled%Pt(:,i+2*numCuts)
        left(:,i) = allSampled%Pt(:,i+3*numCuts)
      END DO


      ! DO i = 1,numCuts
      !   WRITE(24+s,*) top(1,i),top(2,i),top(3,i)
      ! END DO
      ! DO i = 1,numCuts
      !   WRITE(24+s,*) right(1,i),right(2,i),right(3,i)
      ! END DO
      ! DO i = 1,numCuts
      !   WRITE(24+s,*) bottom(1,i),bottom(2,i),bottom(3,i)
      ! END DO
      ! DO i = 1,numCuts
      !   WRITE(24+s,*) left(1,i),left(2,i),left(3,i)
      ! END DO




      DO i = 1,numCuts
        Curv%Regions(Curv%NB_Region)%Posit(:,i,numCuts) = top(:,i)
      END DO

      DO i = 1,numCuts
        Curv%Regions(Curv%NB_Region)%Posit(:,numCuts,numCuts-i+1) = right(:,i)
      END DO

      DO i = 1,numCuts
        Curv%Regions(Curv%NB_Region)%Posit(:,numCuts-i+1,1) = bottom(:,i)
      END DO

      DO i = 1,numCuts
        Curv%Regions(Curv%NB_Region)%Posit(:,1,i) = left(:,i)
      END DO
      

      !Need to figure out which direction is best for cutting
      numCutsTB = 0
      DO i = 1,numCuts-2

        !Using the top and bottom nodes as the line where the plane will cut
        P1(:) = Curv%Regions(Curv%NB_Region)%Posit(:,i+1,1)
        P2(:) = Curv%Regions(Curv%NB_Region)%Posit(:,i+1,numCuts)
        P3(:) = P1(:) + surfNorm(:)

        normal(:) = Geo3D_Cross_Product(P1(:),P2(:),P3(:))
        aPlane = Plane3D_Build2(P1,normal(:))

        CALL Point3dQueue_Clear(cutRaw)
        DO ie = 1,numCuts-1
            
            P1(:) = left(:,ie)
            P2(:) = left(:,ie+1)

            P3(:) = Plane3D_Line_Plane_Cross(P1,P2,aPlane,t)

            IF ((t.LE.1).AND.(t.GE.0)) THEN

                numCutsTB = numCutsTB + 1
                
            END IF

         END DO

         DO ie = 1,numCuts-1
            
            P1(:) = right(:,ie)
            P2(:) = right(:,ie+1)

            P3(:) = Plane3D_Line_Plane_Cross(P1,P2,aPlane,t)

            IF ((t.LE.1).AND.(t.GE.0)) THEN

                numCutsTB = numCutsTB + 1
                
            END IF

         END DO
      END DO
      numCutsLR = 0
      DO i = 1,numCuts-2

        !Using the top and bottom nodes as the line where the plane will cut
        P1(:) = Curv%Regions(Curv%NB_Region)%Posit(:,1,i+1)
        P2(:) = Curv%Regions(Curv%NB_Region)%Posit(:,numCuts,i+1)
        P3(:) = P1(:) + surfNorm(:)

        normal(:) = Geo3D_Cross_Product(P1(:),P2(:),P3(:))
        aPlane = Plane3D_Build2(P1,normal(:))

        CALL Point3dQueue_Clear(cutRaw)
        DO ie = 1,numCuts-1
            
            P1(:) = top(:,ie)
            P2(:) = top(:,ie+1)

            P3(:) = Plane3D_Line_Plane_Cross(P1,P2,aPlane,t)

            IF ((t.LE.1).AND.(t.GE.0)) THEN

                numCutsLR = numCutsLR + 1
                
            END IF

         END DO

         DO ie = 1,numCuts-1
            
            P1(:) = bottom(:,ie)
            P2(:) = bottom(:,ie+1)

            P3(:) = Plane3D_Line_Plane_Cross(P1,P2,aPlane,t)

            IF ((t.LE.1).AND.(t.GE.0)) THEN

                numCutsLR = numCutsLR + 1
                
            END IF

         END DO
      END DO

      !Now the outside is build to add the inside
      DO i = 1,numCuts-2

        IF (numCutsLR.GT.numCutsTB) THEN
          P1(:) = Curv%Regions(Curv%NB_Region)%Posit(:,i+1,1)
          P2(:) = Curv%Regions(Curv%NB_Region)%Posit(:,i+1,numCuts)
        ELSE
          P1(:) = Curv%Regions(Curv%NB_Region)%Posit(:,1,i+1)
          P2(:) = Curv%Regions(Curv%NB_Region)%Posit(:,numCuts,i+1)
        END IF
        P3(:) = P1(:) + surfNorm(:)

        CALL Point3dQueue_Push(cutRaw, P1)


        normal(:) = Geo3D_Cross_Product(P1(:),P2(:),P3(:))
        aPlane = Plane3D_Build2(P1,normal(:))

        CALL Point3dQueue_Clear(cutRaw)
        DO ie = 1,Surf%NB_Edge

              !Triangles connected
              it1 = Surf%ITR_Edge(1,ie)
              it2 = Surf%ITR_Edge(2,ie)

              IF ( (Surf%IP_Tri(5,it1).EQ.ss).AND.(Surf%IP_Tri(5,it2).EQ.ss) ) THEN

                  !Now check to see if intersection occurs 
                  
                  ip1 = Surf%IP_Edge(1,ie)
                  ip2 = Surf%IP_Edge(2,ie)

                  P1(:) = Surf%Posit(:,ip1)
                  P2(:) = Surf%Posit(:,ip2)

                  P3(:) = Plane3D_Line_Plane_Cross(P1,P2,aPlane,t)

                  IF ((t.LE.1).AND.(t.GE.0)) THEN

                      CALL Point3dQueue_Push(cutRaw, P3)
                      

                  END IF

              END IF


          END DO



          IF (numCutsLR.GT.numCutsTB) THEN
            P1(:) = Curv%Regions(Curv%NB_Region)%Posit(:,i+1,1)
            P2(:) = Curv%Regions(Curv%NB_Region)%Posit(:,i+1,numCuts)
          ELSE
            P1(:) = Curv%Regions(Curv%NB_Region)%Posit(:,1,i+1)
            P2(:) = Curv%Regions(Curv%NB_Region)%Posit(:,numCuts,i+1)
          END IF

          CALL Point3dQueue_Push(cutRaw, P2)

          !First allocate arrays
          ALLOCATE(xi(cutRaw%numNodes))
          ALLOCATE(yi(cutRaw%numNodes))
          ALLOCATE(zi(cutRaw%numNodes))
          ALLOCATE(ti(cutRaw%numNodes))

          DO k = 1,cutRaw%numNodes
           ! WRITE(24+s,*)cutRaw%Pt(1,k),cutRaw%Pt(2,k),cutRaw%Pt(3,k)
            xi(k) = cutRaw%Pt(1,k)
            yi(k) = cutRaw%Pt(2,k)
            zi(k) = cutRaw%Pt(3,k)
            P3(:) = Geo3D_ProjectOnLine(P1,P2,cutRaw%Pt(:,k),ti(k))

          END DO

          d = MIN(2,cutRaw%numNodes-1)
          d = MAX(1,d)
          ALLOCATE(polyx(d+1),polyy(d+1),polyz(d+1))

          polyx = polyfit(ti,xi,d)
          polyy = polyfit(ti,yi,d)
          polyz = polyfit(ti,zi,d)

          DO k = 1,numCuts-2
            t = REAL(k)/REAL(numCuts)!(REAL(k)*(ti(cutRaw%numNodes)-ti(1))/REAL(numCuts)) + ti(1)
            Pt(:) = 0.0d0
            DO o = 1,d+1
              Pt(1) = Pt(1) + polyx(o)*(t**REAL(o-1))
              Pt(2) = Pt(2) + polyy(o)*(t**REAL(o-1))
              Pt(3) = Pt(3) + polyz(o)*(t**REAL(o-1))
            END DO
            

           ! WRITE(24+s,*)Pt(1),Pt(2),Pt(3)

            IF (numCutsLR.GT.numCutsTB) THEN
              Curv%Regions(Curv%NB_Region)%Posit(:,i+1,k+1) = Pt(:)
              
            ELSE
              Curv%Regions(Curv%NB_Region)%Posit(:,k+1,i+1) = Pt(:)
              
            END IF

          END DO
          


          DEALLOCATE(xi,yi,zi,ti,polyx,polyy,polyz)

      END DO


      DEALLOCATE(boundingCurves,curveSampled,locCurveToGlob)
      DEALLOCATE(numSampCurve,top,bottom,left,right)
     
      CALL RegionType_BuildTangent(Curv%Regions(Curv%NB_Region))
      


    END DO
    
    !Need to add intersection curves now
  Curv%NB_Curve = numCurve
  ALLOCATE(Curv%Curves (Curv%NB_Curve) )

  

  DO i = 1,numCurve
    IF (hasCorner(i).EQ.0) THEN
	      !Need to use raw values
	      CALL IntQueue_Clear(singCurvOrd)

	      e1 = curveToEdge(i)%Nodes(1)
	      ip1 = edgeInfo(2,e1)
	      ip2 = edgeInfo(3,e1)

	      CALL IntQueue_Push(singCurvOrd,ip1)
	      CALL IntQueue_Push(singCurvOrd,ip2)

	      flag = 0
	      considered(:) = 0
	      considered(e1) = 1

	      WRITE(*,*) 'Ordering curve ',i

	      DO WHILE (flag.EQ.0)

	        DO e = 1,ned
	          
	          IF ((considered(e).EQ.0)) THEN
	            

	            IF ((edgeInfo(2,e).EQ.ip2).OR.(edgeInfo(3,e).EQ.ip2)) THEN
	              IF (edgeInfo(2,e).EQ.ip2) THEN
	                ip2t = edgeInfo(3,e)
	                ip1t = edgeInfo(2,e)
	              ELSE
	                ip2t = edgeInfo(2,e)
	                ip1t = edgeInfo(3,e)
	              END IF

	              !WRITE(*,*)ip1t,ip2t

	              IF (IntQueue_Contain(curveToEdge(i),e)) THEN

	                !This is the next edge
	                ip1 = ip1t
	                ip2 = ip2t
	                
	                considered(e) = 1

	                IF (ip2.EQ.singCurvOrd%Nodes(1)) THEN
	                  !Finished
	                  CALL IntQueue_Push(singCurvOrd,ip2)
	                  flag = 1
	                ELSE
	                  CALL IntQueue_Push(singCurvOrd,ip2)
	                  EXIT
	                END IF
	              END IF

	            END IF

	            
	          END IF

	        END DO

	      END DO

	    Curv%Curves(i)%ID       = i
	    Curv%Curves(i)%TopoType = 1
	    Curv%Curves(i)%numNodes = singCurvOrd%numNodes

	    ALLOCATE(Curv%Curves(i)%Posit(3,singCurvOrd%numNodes))
	    DO j = 1,singCurvOrd%numNodes
	      Curv%Curves(i)%Posit(:,j) = Surf%Posit(:,singCurvOrd%nodes(j))
	    END DO

    ELSE
      !Use sampled data
      Curv%Curves(i)%ID       = i
	  Curv%Curves(i)%TopoType = 1
	  Curv%Curves(i)%numNodes = 2*numCuts
	  ALLOCATE(Curv%Curves(i)%Posit(3,Curv%Curves(i)%numNodes))
	  DO j = 1,2*numCuts
	  	Curv%Curves(i)%Posit(:,j) = curveSampledOrder(:,j,i)
	  END DO


    END IF
  END DO

    



    CALL SurfaceCurvature_Output('medial',6,Curv)
    CALL SurfaceCurvature_OutputEnSight('medial',6,Curv)
    CALL SurfaceMeshStorage_Output('smooth',6,Surf)


    ! DO s = 1,Curv%NB_Region
    !   DO i = 1,numCuts
    !     DO j = 1,numCuts
    !       u = REAL(i-1)*REAL(Curv%Regions(s)%numNodeU)/REAL(numCuts)
    !       v = REAL(j-1)*REAL(Curv%Regions(s)%numNodeV)/REAL(numCuts)
    !       CALL RegionType_Interpolate(Curv%Regions(s), 0, u, v, P1)
    !       WRITE(1984,*) P1(1),P1(2),P1(3)
    !     END DO
    !   END DO
    ! END DO



   


 


  contains

  function polyfit(vx, vy, d)
    implicit none
    integer, intent(in)                   :: d
    integer, parameter                    :: dp = selected_real_kind(15, 307)
    real(dp), dimension(d+1)              :: polyfit
    real(dp), dimension(:), intent(in)    :: vx, vy
 
    real(dp), dimension(:,:), allocatable :: X
    real(dp), dimension(:,:), allocatable :: XT
    real(dp), dimension(:,:), allocatable :: XTX
 
    integer :: i, j
 
    integer     :: n, lda, lwork
    integer :: info
    integer, dimension(:), allocatable :: ipiv
    real(dp), dimension(:), allocatable :: work
 
    n = d+1
    lda = n
    lwork = n
 
    allocate(ipiv(n))
    allocate(work(lwork))
    allocate(XT(n, size(vx)))
    allocate(X(size(vx), n))
    allocate(XTX(n, n))
 
    ! prepare the matrix
    do i = 0, d
       do j = 1, size(vx)
          X(j, i+1) = vx(j)**i
       end do
    end do
 
    XT  = transpose(X)
    XTX = matmul(XT, X)
 
    ! calls to LAPACK subs DGETRF and DGETRI
    call DGETRF(n, n, XTX, lda, ipiv, info)
    if ( info /= 0 ) then
       print *, "problem"
       return
    end if
    call DGETRI(n, XTX, lda, ipiv, work, lwork, info)
    if ( info /= 0 ) then
       print *, "problem"
       return
    end if
 
    polyfit = matmul( matmul(XTX, XT), vy)
 
    deallocate(ipiv)
    deallocate(work)
    deallocate(X)
    deallocate(XT)
    deallocate(XTX)
 
end function

 

  FUNCTION sort4values(a,b,c,d)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: a,b,c,d
    INTEGER, dimension(4) :: sort4values

    INTEGER :: low1,low2,high1,high2,middle1,middle2,lowest,highest

    


    if (a < b) then
        low1 = a
        high1 = b
    else 
        low1 = b
        high1 = a
    end if

    if (c < d) then
        low2 = c
        high2 = d
    else
        low2 = d
        high2 = c
    end iF

    if (low1 < low2) then
        lowest = low1
        middle1 = low2
    else
        lowest = low2
        middle1 = low1
    end iF

    if (high1 > high2) then
        highest = high1
        middle2 = high2
    else
        highest = high2
        middle2 = high1
    end if
    if (middle1 < middle2) then
        sort4values(1) = lowest
        sort4values(2) = middle1
        sort4values(3) = middle2
        sort4values(4) = highest

        
    else
        sort4values(1) = lowest
        sort4values(2) = middle2
        sort4values(3) = middle1
        sort4values(4) = highest
    end if

  END FUNCTION sort4values
 
  SUBROUTINE Update_Topology(nodesToSurf,nodesToElem, SurfIn,nodesToNode)
      USE common_Constants
      USE common_Parameters
      USE Queue
      IMPLICIT NONE
      TYPE(SurfaceMeshStorageType), INTENT(IN) :: SurfIn
      TYPE(IntQueueType), INTENT(OUT) :: nodesToSurf(SurfIn%NB_Point)
      TYPE(IntQueueType),INTENT(OUT) :: nodesToNode(SurfIn%NB_Point)
      TYPE(IntQueueType), INTENT(OUT) :: nodesToElem(SurfIn%NB_Point)

      REAL*8 :: normals(3,SurfIn%NB_Point)
      REAL*8 :: triNorm(3,SurfIn%NB_Tri)
      INTEGER :: ie,i,ip,ne, si,j,jp

      !First the triangles
      DO ie = 1,SurfIn%NB_Tri
        DO i = 1,3
          ip = SurfIn%IP_Tri(i,ie)
          IF (ip.GT.0) THEN
            IF (.NOT.IntQueue_Contain(nodesToElem(ip),ie)) THEN
              CALL IntQueue_Push(nodesToElem(ip),ie)
          END IF
        END IF
      END DO
      
    END DO

    normals(:,:) = 0.0d0

    !...and the normals
    DO ip = 1,SurfIn%NB_Point
      ne = nodesToElem(ip)%numNodes
      IF (ne.GT.0) THEN
        DO i = 1,ne
          
          DO j = 1,3

            jp = SurfIn%IP_Tri(j,nodesToElem(ip)%nodes(i))
            IF (.NOT.IntQueue_Contain(nodesToNode(ip),jp)) THEN
                CALL IntQueue_Push(nodesToNode(ip),jp)
            END IF

          END DO


          si = SurfIn%IP_Tri(5,nodesToElem(ip)%nodes(i))
          IF (.NOT.IntQueue_Contain(nodesToSurf(ip),si)) THEN
              CALL IntQueue_Push(nodesToSurf(ip),si)
            END IF
        END DO
        

      END IF
    END DO


  END SUBROUTINE


END SUBROUTINE




!*******************************************************************************
!>
!!  Hex Direct Projection
!!  
!!  Generate hex mesh by projecting each surface onto it's medial axis
!<
!*******************************************************************************
SUBROUTINE Hex_Direct()
    
    USE common_Constants
    USE common_Parameters
    USE CellConnectivity
    USE Queue
    USE SurfaceMeshManager
    USE SurfaceMeshStorage
    USE HybridMeshStorage
    USE HybridMeshManager
    USE Plane3DGeom
    USE SurfaceCurvatureManager
    USE TetMeshStorage
    IMPLICIT NONE

    INTEGER :: noOfRegions
    INTEGER, ALLOCATABLE :: nsreg(:), opreg(:,:), Mark_Point_Surf(:)
    REAL*8, ALLOCATABLE  :: regMax(:,:), regMin(:,:)
    TYPE(IntQueueType), ALLOCATABLE :: lreg(:)
    TYPE(IntQueueType) :: surfToReg,lockCheck

    TYPE(SurfaceMeshStorageType) :: QuadMesh, GapSurf

    INTEGER :: i,j,s,k,l,m,ip1,ip2,ip3,ip4,si,sj,sk,np,nel,itri,ieHex,iePyr
    INTEGER :: ip1t,ip2t,ip3t,ip4t
    INTEGER :: IT, IB, NB, ipp(3), ip,ie, NI,NJ,NK,re
    INTEGER :: flag, added, flag2,nsurf,nseg
    TYPE(IntQueueType), ALLOCATABLE :: nodesToSurf(:)
    TYPE(IntQueueType), ALLOCATABLE :: nodesToElem(:)
    REAL*8, ALLOCATABLE :: surfNorms(:,:), nodeNorms(:,:),nodeNormHelp(:,:)
    TYPE(SurfaceMeshStorageType), ALLOCATABLE :: surfLayers(:)

    REAL*8 :: P1(3), P2(3), P3(3), P(2), dist, minDist, norm(3),dir(3),dirTest,P4(3), P5(3)
    REAL*8 :: pp2(3,2), pp3(3,3),weight(3),t, r, sumDist, meanDist, numCount, Scalar,area
    INTEGER :: ntype, layNum, oldlayNum, finished,numLayers,curNode,iLay
    CHARACTER(len=5) :: filename1
    CHARACTER(len=50) :: text

    TYPE(HybridMeshStorageType) :: HybMesh,HexFrame
    TYPE(TetMeshStorageType) :: GapVol
    TYPE(SurfaceCurvatureType) :: Curv

    INTEGER, ALLOCATABLE :: lockedNode(:), ridgeNode(:)
    INTEGER :: maxi,maxj

    REAL*8 :: gridSpacing,d1,d2,d3,d4

    TYPE(Plane3D) :: aPlane


    INTEGER, ALLOCATABLE :: links(:,:), linked(:,:)

    CHARACTER( LEN = MaxFileNameLength ) :: JobNameBackup
    INTEGER :: OldJobNameLength


    !First to smooth the medial surface

    
    !TODO - renable
    !   DO i = 1,5
    !       CALL Surf_To_Dat(Surf,Curv)
    !   END DO

    CALL SurfaceMeshStorage_Input(JobName(1:JobNameLength)//'Q',JobNameLength+1,QuadMesh)

    !First we need to associate surfaces with regions
    !Read in the reg file
    OPEN(922,file = JobName(1:JobNameLength)//'.reg',form='formatted')
    READ(922,'(I10)') noOfRegions
    ALLOCATE(nsreg(noOfRegions),lreg(noOfRegions),opreg(2,noOfRegions))
    ALLOCATE(regMax(3,noOfRegions), regMin(3,noOfRegions))
    DO k = 1,noOfRegions
      READ(922,'(I10)') nsreg(k)
      DO j = 1,nsreg(k)
        READ(922,'(I10)') s
        CALL IntQueue_Push(lreg(k), s)
      END DO
    END DO
    CLOSE(922)

    !Also we need to read the .bco file to assosiate surfaces with regions in the quad mesh
    OPEN(923,file = JobName(1:JobNameLength)//'.bco',form='formatted')
    read(923,'(a)') text
    read(923,*) nsurf ,nseg
    read(923,'(a)') text
    DO i = 1,nsurf
    	READ(923,*) j, s
        CALL IntQueue_Push(surfToReg, s)
        WRITE(*,*) surfToReg%nodes(i)
    END DO
    CLOSE(923)

     !Now project
    !Now to allocate the surfaces
    ALLOCATE(surfLayers(nsurf))
    DO s = 1,nsurf
      
        surfLayers(s)%NB_Surf = 1
        surfLayers(s)%NB_Point = QuadMesh%NB_Point
        CALL allc_Surf(surfLayers(s),QuadMesh%NB_Tri,QuadMesh%NB_Point,QuadMesh%NB_Tri)
        surfLayers(s)%Posit(:,1:QuadMesh%NB_Point) = QuadMesh%Posit(:,1:QuadMesh%NB_Point)
        surfLayers(s)%NB_Tri = 0
        DO it = 1,QuadMesh%NB_Tri
          IF (QuadMesh%IP_Tri(5,it).EQ.s) THEN
            surfLayers(s)%NB_Tri = surfLayers(s)%NB_Tri + 1
            surfLayers(s)%IP_Tri(:,surfLayers(s)%NB_Tri) = QuadMesh%IP_Tri(:,it)
            surfLayers(s)%IP_Tri(5,surfLayers(s)%NB_Tri) = 1
          END IF
        END DO
      
    END DO

    !Surf contains medial axis and Quad is the mesh we are going to project
    


    !Calculate and smooth normals
    ALLOCATE(nodesToElem(QuadMesh%NB_Point),nodesToSurf(QuadMesh%NB_Point))
    CALL Update_Topology(nodesToSurf,nodesToElem,QuadMesh)
    !First we need normals
    ALLOCATE(surfNorms(3,QuadMesh%NB_Tri))
    

    ALLOCATE(nodeNorms(3,QuadMesh%NB_Point),nodeNormHelp(3,QuadMesh%NB_Point), &
             lockedNode(QuadMesh%NB_Point),ridgeNode(QuadMesh%NB_Point))

    !Reset normals
	  DO it = 1,QuadMesh%NB_Tri
		  surfNorms(:,it) = Geo3D_Cross_Product(QuadMesh%Posit(:,QuadMesh%IP_Tri(1,it)), &
		                      QuadMesh%Posit(:,QuadMesh%IP_Tri(2,it)), &
		                      QuadMesh%Posit(:,QuadMesh%IP_Tri(3,it)))
		  surfNorms(:,it) = surfNorms(:,it)/Geo3D_Distance(surfNorms(:,it))
	  END DO

    lockedNode(:) = 0
    ridgeNode(:) = 0
    DO ip = 1,QuadMesh%NB_Point
      
      CALL IntQueue_Clear(lockCheck)
      DO si = 1,nodesToSurf(ip)%numNodes
        Sk = surfToReg%nodes(nodesToSurf(ip)%nodes(si)) 
        IF (.NOT.IntQueue_Contain(lockCheck,Sk)) THEN
            CALL IntQueue_Push(lockCheck,Sk)
        END IF
       	
        
      END DO
      IF (lockCheck%numNodes.GT.1) THEN
        lockedNode(ip) = 1
      END IF
      

      !Now test the largest change in normal
      DO i = 1,nodesToElem(ip)%numNodes
      	it = nodesToElem(ip)%Nodes(i)
      	DO j = 1,nodesToElem(ip)%numNodes
      		itri = nodesToElem(ip)%Nodes(j)
      		dirTest = surfNorms(1,it)*surfNorms(1,itri) + surfNorms(2,it)*surfNorms(2,itri) &
      					+ surfNorms(3,it)*surfNorms(3,itri)
      		IF (dirTest.LT.0.5d0) THEN
      			ridgeNode(ip) = 1
      		END IF

      	END DO

      END DO

    END DO

    nodeNorms(:,:) = 0.0d0

   DO ip = 1,QuadMesh%NB_Point
       
      !First find the normal
      norm(:) = nodeNorms(:,ip)
      DO i = 1,nodesToElem(ip)%numNodes
        it = nodesToElem(ip)%Nodes(i)
      s = QuadMesh%IP_Tri(5,it)
      IF ((Type_Wall(s).NE.2).AND.(Type_Wall(s).NE.3)) THEN
        norm(:) = norm(:) + (surfNorms(:,it))   
      END IF
        
      END DO
      !norm(:) = norm(:)/REAL(nodesToElem(ip)%numNodes)
      nodeNorms(:,ip) = norm(:)/Geo3D_Distance(norm)
        
      
    END DO

  !Smooth normals
  

 
   
   m = 0
    DO WHILE (m.LT.1000)
      m = m + 1

      nodeNormHelp(:,:) = nodeNorms(:,:)

       
      DO ip = 1,QuadMesh%NB_Point
        !IF  (ridgeNode(ip).EQ.0) THEN
          norm(:) = 0.0d0
          !Now test the largest change in normal
          DO i = 1,nodesToElem(ip)%numNodes
            ie = nodesToElem(ip)%Nodes(i)
             IF(ip == QuadMesh%IP_Tri(1,ie))THEN
              ip1 = QuadMesh%IP_Tri(2,ie)
              ip2 = QuadMesh%IP_Tri(3,ie)
             ELSE IF(ip == QuadMesh%IP_Tri(2,ie))THEN
              ip1 = QuadMesh%IP_Tri(3,ie)
              ip2 = QuadMesh%IP_Tri(1,ie)
             ELSE IF(ip == QuadMesh%IP_Tri(3,ie))THEN
              ip1 = QuadMesh%IP_Tri(1,ie)
              ip2 = QuadMesh%IP_Tri(2,ie)
             ENDIF

             norm(:) = norm(:) + nodeNormHelp(:,ip1) + nodeNormHelp(:,ip2)

          END DO
          IF (Geo3D_Distance(norm).GT.0.0d0) THEN
            norm(:) = norm(:)/Geo3D_Distance(norm)
            nodeNorms(:,ip) = 0.5d0*(nodeNormHelp(:,ip) + norm(:))
            nodeNorms(:,ip) =nodeNorms(:,ip)/Geo3D_Distance(nodeNorms(:,ip))
          END IF
        !END IF
      END DO


     
    END DO

  gridSpacing = 0.0d0
  nel = 0

  DO s = 1,nsurf

 
      WRITE(*,*) 'Surface ',s
      DO ip = 1,surfLayers(s)%NB_Point

        !First check this is a point we care about
        flag = 0
        DO si = 1,nodesToSurf(ip)%numNodes
          sj = nodesToSurf(ip)%nodes(si)
          IF (sj.EQ. s)THEN
            flag = 1
          END IF
        END DO

        IF ((flag.EQ.1).AND.(Type_Wall(s).NE.2).AND.(Type_Wall(s).NE.3)) THEN

          norm(:) = nodeNorms(:,ip)
          !Now draw the line for intersection
          P1(:) = QuadMesh%Posit(:,ip)
          P2(:) = P1(:) - 1000.0d0*norm(:)
          pp2(:,1) = P1(:)
          pp2(:,2) = P2(:)
          !Loop over each surface connected to this point find the closest intersection
          minDist = HUGE(0.0d0)
          P3(:) = P1(:)

          

          DO si = 1,nodesToSurf(ip)%numNodes
            sj = nodesToSurf(ip)%nodes(si)
            IF (sj .EQ. s)THEN
              DO i = 1,Surf%NB_Tri

                sk = Surf%IP_Tri(5,i)

                flag2 = 0
                !Use reg information to find associated surfaces
                DO m = 1,nsreg(surfToReg%nodes(s))
                  IF (sk.EQ.lreg(surfToReg%nodes(s))%Nodes(m)) THEN
                    flag2 = 1
                  END IF
                END DO

                IF (sk.EQ.surfToReg%nodes(s)) THEN 
                  flag2 = 0 !We don't want to self intersect
                END IF


                IF (flag2.EQ.1) THEN
                  pp3(:,1) = Surf%Posit(:,Surf%IP_Tri(1,i))
                  pp3(:,2) = Surf%Posit(:,Surf%IP_Tri(2,i))
                  pp3(:,3) = Surf%Posit(:,Surf%IP_Tri(3,i))
                  ntype = -1000
                  P2(:) = Plane3D_Line_Tri_Cross(pp2,pp3,ntype,weight,t)
                  !Check direction and position
                  dir(:) = P1(:) - P2(:)
                  dirTest = norm(1)*dir(1) + norm(2)*dir(2) + norm(3)*dir(3)
                  IF ((ntype.GT.0).AND.(dirTest.GE.0)) THEN
                    dist = Geo3D_Distance_SQ(P2(:),QuadMesh%Posit(:,ip))
                    IF (dist.LT.minDist) THEN
                      minDist = dist
                      P3(:) = P2(:)
                    END IF
                  END IF

                END IF
              END DO
            END IF
          END DO
          DO si = l,layNum

          	CALL SpacingStorage_GetScale(BGSpacing, QuadMesh%Posit(:,ip), Scalar)
          	Scalar = Scalar*BGSpacing%BasicSize
          	gridSpacing = gridSpacing + Scalar
          	nel = nel+1
          	dir(:) = P3(:) - QuadMesh%Posit(:,ip)
          	IF (Geo3D_Distance_SQ(dir).LT.(Scalar*Scalar)) THEN
          		surfLayers(s)%Posit(:,ip) = QuadMesh%Posit(:,ip) + (Scalar/Geo3D_Distance(dir))*(dir(:))
          	ELSE
            	surfLayers(s)%Posit(:,ip) = P3(:)
            END IF
            
          END DO
        END IF

      END DO

      !At the end put the current points in the next surface
      WRITE(filename1,'(I5.5)') s
      
      CALL SurfaceMeshStorage_Output_STL(filename1,5,surfLayers(s))
    END DO


    

    !Now we want to snap these surfaces to our background hex grid
    

    ! DO s = 1,nsurf
    !   IF ((Type_Wall(s).NE.2).AND.(Type_Wall(s).NE.3)) THEN
    !     DO it = 1,surfLayers(s)%NB_Tri
    !       P1(:) = surfLayers(s)%Posit(:,surfLayers(s)%IP_Tri(1,it))
    !       P2(:) = surfLayers(s)%Posit(:,surfLayers(s)%IP_Tri(2,it))
    !       P3(:) = surfLayers(s)%Posit(:,surfLayers(s)%IP_Tri(3,it))

    !       gridSpacing = (gridSpacing+Geo3D_Distance_SQ(P1,P2))
    !       gridSpacing = (gridSpacing+Geo3D_Distance_SQ(P1,P3))
    !       gridSpacing = (gridSpacing+Geo3D_Distance_SQ(P3,P2))
    !       nel = nel+3

    !     END DO
    !    END IF
    ! END DO

    gridSpacing = SQRT(gridSpacing/(REAL(nel)))

    WRITE(*,*) 'Grid spacing = ',gridSpacing


   

    numCount = 0.0d0
    sumDist = 0.0d0


    DO s = 1,nsurf
      IF ((Type_Wall(s).NE.2).AND.(Type_Wall(s).NE.3)) THEN
        DO ip = 1,surfLayers(s)%NB_Point

          !Move back a couple of gridSpaces
          P1(:) = QuadMesh%Posit(:,ip)
          P2(:) = surfLayers(s)%Posit(:,ip) - P1(:)

          dist = Geo3D_Distance(P2)
          IF (dist.GT.0.0d0) THEN
          	numCount = numCount+1.0d0
          	sumDist = sumDist + dist
          END IF

          IF(dist.LT.gridSpacing)THEN
              surfLayers(s)%Posit(:,ip) = P1(:) + (0.5d0)*(P2(:))
          ELSE
              surfLayers(s)%Posit(:,ip) = P1(:) + ((dist-gridSpacing)/dist)*P2(:)
          END IF
          

          
          


          ! !Snap to the uniform hex grid
          ! minDist = HUGE(0.0d0)
          ! DO i = 1,HexFrame%NB_Point
          !   dist = Geo3D_Distance_SQ(surfLayers(s)%Posit(:,ip),HexFrame%Posit(:,i))
          !   IF (dist.LT.minDist) THEN
          !       k = i
          !       minDist = dist
          !   END IF
          ! END DO

          !surfLayers(s)%Posit(:,ip) = HexFrame%Posit(:,k)

        END DO
      END IF
    END DO

    meanDist = sumDist/numCount
    WRITE(*,*) meanDist
    WRITE(*,*) meanDist/gridSpacing
    numLayers = NINT(meanDist/gridSpacing)
    

  
   !Attempt to combine surface 1 and surfLayers(1) into prisms and output the free surface
   HybMesh%NB_Point = QuadMesh%NB_Point*(1+nsurf)*numLayers

   HybMesh%NB_Prm = 0
   HybMesh%NB_Hex = 0 
   HybMesh%NB_Pyr = 0

   GapSurf%NB_Point = HybMesh%NB_Point
   GapSurf%NB_Tri = 0
   GapSurf%NB_Surf = nsurf

   DO s = 1,nsurf
    IF (Type_Wall(s).EQ.10) THEN
      HybMesh%NB_Hex = HybMesh%NB_Hex + surfLayers(s)%NB_Tri
      HybMesh%NB_Point = HybMesh%NB_Point + surfLayers(s)%NB_Tri
      HybMesh%NB_Pyr = HybMesh%NB_Pyr + surfLayers(s)%NB_Tri
    ELSE
      HybMesh%NB_Prm = HybMesh%NB_Prm + surfLayers(s)%NB_Tri !<  (6,*) connectivities of each prism.
    END IF
   END DO
   GapSurf%NB_Tri = HybMesh%NB_Prm+HybMesh%NB_Hex
   HybMesh%NB_Prm = HybMesh%NB_Prm*numLayers
   HybMesh%NB_Hex = HybMesh%NB_Hex*numLayers

   CALL allc_Surf(GapSurf,GapSurf%NB_Tri,GapSurf%NB_Point,GapSurf%NB_Tri)
   
   CALL allc_2Dpointer(HybMesh%IP_Prm,   6, HybMesh%NB_Prm)
   CALL allc_2Dpointer(HybMesh%IP_Hex,   8, HybMesh%NB_Hex)
   CALL allc_2Dpointer(HybMesh%IP_Pyr,   5, HybMesh%NB_Pyr)
   
   CALL allc_HybridMesh_Posit(HybMesh, HybMesh%NB_Point)

   HybMesh%Posit(:,1:QuadMesh%NB_Point) = QuadMesh%Posit(:,1:QuadMesh%NB_Point)
   DO s = 1,nsurf
    HybMesh%Posit(:,((s)*QuadMesh%NB_Point)+1:((s)*QuadMesh%NB_Point)+1+QuadMesh%NB_Point) = &
          surfLayers(s)%Posit(:,1:QuadMesh%NB_Point)
   END DO
  

  ! 
  ! HybMesh%Posit(:,Surf%NB_Point+1:HybMesh%NB_Point) = surfLayers(1)%Posit(:,1:Surf%NB_Point)

  ie = 0
  ieHex = 0
  itri = 0
  iePyr = 0
  GapSurf%NB_Tri = 0
  curNode = QuadMesh%NB_Point
   DO s = 1,nsurf
    IF ((Type_Wall(s).NE.2).AND.(Type_Wall(s).NE.3).AND.(Type_Wall(s).NE.10)) THEN
     DO it = 1,surfLayers(s)%NB_Tri

       !Find the target positions
       IF (lockedNode(surfLayers(s)%IP_Tri(1,it)).EQ.0) THEN
       	P1(:) = surfLayers(s)%Posit(:,surfLayers(s)%IP_Tri(1,it))
       ELSE
       	P1(:) = HybMesh%Posit(:,surfLayers(s)%IP_Tri(1,it))
       END IF

       IF (lockedNode(surfLayers(s)%IP_Tri(2,it)).EQ.0) THEN
       	P2(:) = surfLayers(s)%Posit(:,surfLayers(s)%IP_Tri(2,it))
       ELSE
       	P2(:) = HybMesh%Posit(:,surfLayers(s)%IP_Tri(2,it))
       END IF

       IF (lockedNode(surfLayers(s)%IP_Tri(3,it)).EQ.0) THEN
       	P3(:) = surfLayers(s)%Posit(:,surfLayers(s)%IP_Tri(3,it))
       ELSE
       	P3(:) = HybMesh%Posit(:,surfLayers(s)%IP_Tri(3,it))
       END IF
       
       d1 = Geo3D_Distance(P1,HybMesh%Posit(:,surfLayers(s)%IP_Tri(1,it)))/REAL(numLayers)
       d2 = Geo3D_Distance(P2,HybMesh%Posit(:,surfLayers(s)%IP_Tri(2,it)))/REAL(numLayers)
       d3 = Geo3D_Distance(P3,HybMesh%Posit(:,surfLayers(s)%IP_Tri(3,it)))/REAL(numLayers)

       DO iLay = 1,numLayers
        ie = ie + 1
        IF (iLay.EQ.1) THEN
            HybMesh%IP_Prm(1:3,ie) = surfLayers(s)%IP_Tri(1:3,it)
        ELSE
            HybMesh%IP_Prm(1:3,ie) = HybMesh%IP_Prm(4:6,ie-1)
        END IF

        curNode = curNode + 1
        P4(:) = HybMesh%Posit(:,HybMesh%IP_Prm(1,ie))
        IF (d1.GT.0.0d0) THEN
        	HybMesh%Posit(:,curNode) = P4(:) + (d1/(Geo3D_Distance(P4,P1)))*(P1(:)-P4(:))
        ELSE
        	HybMesh%Posit(:,curNode) = P4(:)
        END IF
        HybMesh%IP_Prm(4,ie) = curNode

        curNode = curNode + 1
        P4(:) = HybMesh%Posit(:,HybMesh%IP_Prm(2,ie))
        IF (d2.GT.0.0d0) THEN
        	HybMesh%Posit(:,curNode) = P4(:) + (d2/(Geo3D_Distance(P4,P2)))*(P2(:)-P4(:))
        ELSE
        	HybMesh%Posit(:,curNode) = P4(:)
        END IF
        HybMesh%IP_Prm(5,ie) = curNode

        curNode = curNode + 1
        P4(:) = HybMesh%Posit(:,HybMesh%IP_Prm(3,ie))
        IF (d3.GT.0.0d0) THEN
        	HybMesh%Posit(:,curNode) = P4(:) + (d3/(Geo3D_Distance(P4,P3)))*(P3(:)-P4(:))
        ELSE
        	HybMesh%Posit(:,curNode) = P4(:)
        END IF
        HybMesh%IP_Prm(6,ie) = curNode

        

       END DO

      ! ie = ie + 1
      ! HybMesh%IP_Prm(1:3,ie) = surfLayers(s)%IP_Tri(1:3,it)
      ! DO i = 1,3
      !   IF (lockedNode(surfLayers(s)%IP_Tri(i,it)).EQ.0) THEN
      !       HybMesh%IP_Prm(i+3,ie) = surfLayers(s)%IP_Tri(i,it) + s*QuadMesh%NB_Point
      !   ELSE
      !       HybMesh%IP_Prm(i+3,ie) = surfLayers(s)%IP_Tri(i,it)
      !   END IF
      ! END DO
      itri = itri + 1
      GapSurf%IP_Tri(1:3,itri) = HybMesh%IP_Prm(4:6,ie)
      GapSurf%IP_Tri(5,itri) = s

     END DO

    ELSE IF ((Type_Wall(s).NE.2).AND.(Type_Wall(s).NE.3).AND.(Type_Wall(s).EQ.10)) THEN
    	!This is a quad surface

        CALL Surf_BuildNext(surfLayers(s))

        DO it = 1,surfLayers(s)%NB_Tri-1,2
            !Loop in twos
            !Find the target positions

            ip1t = surfLayers(s)%IP_Tri(1,it)
            ip2t = surfLayers(s)%IP_Tri(2,it)
            ip3t = surfLayers(s)%IP_Tri(3,it)

            DO i = 1,3
             k = surfLayers(s)%IP_Tri(i,it+1)
             IF ((k.NE.ip1t).AND.(k.NE.ip2t).AND.(k.NE.ip3t)) THEN
              ip4t = k
              DO j = 1,3

                IF (surfLayers(s)%Next_Tri(j,it).EQ.(it+1)) THEN
                  IF (j.EQ.1) THEN
                    ip1 = ip4t
                    ip2 = ip3t
                    ip3 = ip1t
                    ip4 = ip2t
                  ELSE IF (j.EQ.2) THEN
                    ip1 = ip4t
                    ip2 = ip1t
                    ip3 = ip2t
                    ip4 = ip3t
                  ELSE
                    ip1 = ip4t
                    ip2 = ip2t
                    ip3 = ip3t
                    ip4 = ip1t
                  END IF
                END IF
              END DO
             END iF
            END DO

           

             IF (lockedNode(ip1).EQ.0) THEN
              P1(:) = surfLayers(s)%Posit(:,ip1)
             ELSE
              P1(:) = HybMesh%Posit(:,ip1)
             END IF

             IF (lockedNode(ip2).EQ.0) THEN
              P2(:) = surfLayers(s)%Posit(:,ip2)
             ELSE
              P2(:) = HybMesh%Posit(:,ip2)
             END IF

             IF (lockedNode(ip3).EQ.0) THEN
              P3(:) = surfLayers(s)%Posit(:,ip3)
             ELSE
              P3(:) = HybMesh%Posit(:,ip3)
             END IF

             IF (lockedNode(ip4).EQ.0) THEN
              P4(:) = surfLayers(s)%Posit(:,ip4)
             ELSE
              P4(:) = HybMesh%Posit(:,ip4)
             END IF
             
             d1 = Geo3D_Distance(P1,HybMesh%Posit(:,ip1))/REAL(numLayers)
             d2 = Geo3D_Distance(P2,HybMesh%Posit(:,ip2))/REAL(numLayers)
             d3 = Geo3D_Distance(P3,HybMesh%Posit(:,ip3))/REAL(numLayers)
             d4 = Geo3D_Distance(P4,HybMesh%Posit(:,ip4))/REAL(numLayers)

             DO iLay = 1,numLayers

                ieHex = ieHex + 1
                IF (iLay.EQ.1) THEN
                    HybMesh%IP_Hex(1,ieHex) = ip1
                    HybMesh%IP_Hex(2,ieHex) = ip2
                    HybMesh%IP_Hex(3,ieHex) = ip3
                    HybMesh%IP_Hex(4,ieHex) = ip4
                ELSE
                    HybMesh%IP_Hex(1:4,ieHex) = HybMesh%IP_Hex(5:8,ieHex-1)
                END IF

                curNode = curNode + 1
                P5(:) = HybMesh%Posit(:,HybMesh%IP_Hex(1,ieHex))
                IF (d1.GT.0.0d0) THEN
                  HybMesh%Posit(:,curNode) = P5(:) + (d1/(Geo3D_Distance(P5,P1)))*(P1(:)-P5(:))
                ELSE
                  HybMesh%Posit(:,curNode) = P5(:)
                END IF
                HybMesh%IP_Hex(5,ieHex) = curNode

                curNode = curNode + 1
                P5(:) = HybMesh%Posit(:,HybMesh%IP_Hex(2,ieHex))
                IF (d2.GT.0.0d0) THEN
                  HybMesh%Posit(:,curNode) = P5(:) + (d2/(Geo3D_Distance(P5,P2)))*(P2(:)-P5(:))
                ELSE
                  HybMesh%Posit(:,curNode) = P5(:)
                END IF
                HybMesh%IP_Hex(6,ieHex) = curNode

                curNode = curNode + 1
                P5(:) = HybMesh%Posit(:,HybMesh%IP_Hex(3,ieHex))
                IF (d3.GT.0.0d0) THEN
                  HybMesh%Posit(:,curNode) = P5(:) + (d3/(Geo3D_Distance(P5,P3)))*(P3(:)-P5(:))
                ELSE
                  HybMesh%Posit(:,curNode) = P5(:)
                END IF
                HybMesh%IP_Hex(7,ieHex) = curNode

                curNode = curNode + 1
                P5(:) = HybMesh%Posit(:,HybMesh%IP_Hex(4,ieHex))
                IF (d4.GT.0.0d0) THEN
                  HybMesh%Posit(:,curNode) = P5(:) + (d4/(Geo3D_Distance(P5,P4)))*(P4(:)-P5(:))
                ELSE
                  HybMesh%Posit(:,curNode) = P5(:)
                END IF
                HybMesh%IP_Hex(8,ieHex) = curNode


             END DO

               !...place a pyramid on top
          curNode = curNode + 1
          ip1 = HybMesh%IP_Hex(5,ieHex)
          ip2 = HybMesh%IP_Hex(6,ieHex)
          ip3 = HybMesh%IP_Hex(7,ieHex)
          ip4 = HybMesh%IP_Hex(8,ieHex)

          P5(:) = 0.25d0*(HybMesh%Posit(:,ip1)+HybMesh%Posit(:,ip2)+HybMesh%Posit(:,ip3)+HybMesh%Posit(:,ip4))
          !Find the normal
          norm(:) = 0.25d0*(nodeNorms(:,ip1t)+nodeNorms(:,ip2t)+nodeNorms(:,ip3t)+nodeNorms(:,ip4t))
          norm(:) = norm(:)/Geo3D_Distance(norm)
          P5(:) = P5(:) - 0.25d0*gridSpacing*norm(:)
          curNode = curNode + 1
          HybMesh%Posit(:,curNode) = P5(:)

          iePyr = iePyr + 1
          HybMesh%IP_Pyr(1,iePyr) = ip1
          HybMesh%IP_Pyr(2,iePyr) = ip2
          HybMesh%IP_Pyr(3,iePyr) = ip3
          HybMesh%IP_Pyr(4,iePyr) = ip4
          HybMesh%IP_Pyr(5,iePyr) = curNode



          itri = itri + 1
          GapSurf%IP_Tri(1,itri) = ip1
          GapSurf%IP_Tri(2,itri) = ip2
          GapSurf%IP_Tri(3,itri) = curNode
          GapSurf%IP_Tri(5,itri) = s

          itri = itri + 1
          GapSurf%IP_Tri(1,itri) = ip2
          GapSurf%IP_Tri(2,itri) = ip3
          GapSurf%IP_Tri(3,itri) = curNode
          GapSurf%IP_Tri(5,itri) = s

          itri = itri + 1
          GapSurf%IP_Tri(1,itri) = ip3
          GapSurf%IP_Tri(2,itri) = ip4
          GapSurf%IP_Tri(3,itri) = curNode
          GapSurf%IP_Tri(5,itri) = s

          itri = itri + 1
          GapSurf%IP_Tri(1,itri) = ip4
          GapSurf%IP_Tri(2,itri) = ip1
          GapSurf%IP_Tri(3,itri) = curNode
          GapSurf%IP_Tri(5,itri) = s

             
        END DO

        


       


    ELSE
      DO it = 1,surfLayers(s)%NB_Tri
        itri = itri + 1
        GapSurf%IP_Tri(1:3,itri) = surfLayers(s)%IP_Tri(1:3,it)
        GapSurf%IP_Tri(5,itri) = s
      END DO
    END IF
   END DO

   

   HybMesh%NB_Prm = ie
   HybMesh%NB_Pyr = iePyr
   HybMesh%NB_Hex = ieHex
   GapSurf%NB_Tri = itri

  

   CALL HybridMeshStorage_Output('prism',5,-5,HybMesh)

   GapSurf%Posit(:,1:GapSurf%NB_Point) = HybMesh%Posit(:,1:GapSurf%NB_Point)

   ALLOCATE(Mark_Point_Surf(GapSurf%NB_Point))
   Mark_Point_Surf(:) = -999
   DO IT = 1,GapSurf%NB_Tri
        DO i = 1,3  
            Mark_Point_Surf(GapSurf%IP_Tri(i,IT)) = 10
        END DO
    END DO 
    CALL Remove_Point_Surf(GapSurf,Mark_Point_Surf)

   !We need to remove duplicate points from GapSurf
   DO ip1 = 1,GapSurf%NB_Point
    DO ip2 = 1,GapSurf%NB_Point
        IF (ip1.NE.ip2) THEN
            dist = Geo3D_Distance_SQ(GapSurf%Posit(:,ip1),GapSurf%Posit(:,ip2))
            IF (dist.EQ.0.0d0) THEN
                DO IT = 1,GapSurf%NB_Tri
                    DO i = 1,3  
                        IF (GapSurf%IP_Tri(i,IT).EQ.ip2) THEN
                            GapSurf%IP_Tri(i,IT) = ip1
                        END IF
                    END DO
                END DO
            END IF
        END IF
    END DO
   END DO
   Mark_Point_Surf(:) = -999
   DO IT = 1,GapSurf%NB_Tri

        DO i = 1,3  
            Mark_Point_Surf(GapSurf%IP_Tri(i,IT)) = 10
        END DO
    END DO 
    CALL Remove_Point_Surf(GapSurf,Mark_Point_Surf)
   


   CALL SurfaceMeshStorage_Output('GapSurf',7,GapSurf)

   !GapSurf is now our surface which we want to mesh inside

   OldJobNameLength = JobNameLength
   JobNameBackup = JobName

   JobNameLength = 7
   JobName = 'GapSurf'

   
   CALL SurfaceMeshStorage_Clear(Surf)
   BOUND_INSERT_SWAP = 0
   
   !CALL Job_noFrame()

  
   !BGSpacing%BasicSize = gridSpacing
   
   CALL Read_Surface()
   JobName = JobNameBackup
   JobNameLength = OldJobNameLength

   CALL VisLayer()  
   CALL Boundary_Insert()
   WRITE(*,*)' '
   WRITE(*,*)'---- Recover boundary ----'
   CALL Recovery3D()
   useStretch = .TRUE.
   CALL Set_Domain_Mapping()
   CALL Get_Tet_Circum(0)
   CALL Get_Tet_Volume(0)
   WRITE(*,*)' '
   WRITE(*,*)'---- Break Large Elements ----'
   
   CALL Element_Break()
   

   CALL Swap3D(2)
   CALL Next_Build( )
   CALL Element_Collapse( )
   CALL Next_Build( )
   CALL Smooth( )
   CALL Next_Build( )

   CALL TetMeshStorage_NodeBackup(NB_Point, Posit,  GapVol)
   CALL TetMeshStorage_CellBackup(NB_Tet,   IP_Tet, GapVol)
   CALL TetMeshStorage_Output('GapVol',6,GapVol)

    contains

        !*******************************************************************************
    !>      
    !!     Remove those points with a negative marker::Mark_Point_Surf (IP)=-999.      \n
    !!     Reset :   common_Parameters::NB_Point, common_Parameters::IP_Tet,
    !!               common_Parameters::IP_BD_Tri (1:3,:).
    !!     @param[out]  common_Parameters::Mark_Point_Surf (old_ID) = IP               \n
    !!                  IP>0 then newID = IP;                                     \n
    !!                  IP=0 then newID = oldID;                                  \n
    !!                  IP<0 then oldID is removed.
    !<      
    !*******************************************************************************
    SUBROUTINE Remove_Point_Surf(SurfIn,Mark_Point_Surf)

      USE common_Parameters
      
      IMPLICIT NONE
      TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: SurfIn
      INTEGER, INTENT(INOUT) :: Mark_Point_Surf(SurfIn%NB_Point)

      INTEGER :: IT, IP, IPend, i, IB

      WHERE(Mark_Point_Surf(1:SurfIn%NB_Point)/=-999) Mark_Point_Surf(1:SurfIn%NB_Point) = 0
      IPend = SurfIn%NB_Point
      DO IP = 1, SurfIn%NB_Point
         IF(Mark_Point_Surf(IP)/=-999) CYCLE
         DO WHILE(Mark_Point_Surf(IPend)==-999)
            IPend = IPend-1
         ENDDO
         IF(IP>IPend) EXIT

         Mark_Point_Surf(IPend) = IP
         Mark_Point_Surf(IP) = -1
         SurfIn%Posit(:,IP)  = SurfIn%Posit(:,IPend)
         
         

         IPend = IPend-1
      ENDDO

      IF(SurfIn%NB_Point==IPend) RETURN
      WRITE(29,*)'Remove_Point_Surf: SurfIn%NB_Point (before and after removing) =',SurfIn%NB_Point, IPend

      SurfIn%NB_Point = IPend

      DO IT = 1,SurfIn%NB_Tri
         DO i=1,3
            IP = Mark_Point_Surf(SurfIn%IP_Tri(i,IT))
            IF(IP<0) THEN
             WRITE(*,*) SurfIn%IP_Tri(1,IT),SurfIn%IP_Tri(2,IT),SurfIn%IP_Tri(3,IT) 
             CALL  Error_Stop (' Remove_Point_Surf: A tet with Mark_Point_Surf<0 exists')
            END IF
            IF(IP>0) SurfIn%IP_Tri(i,IT) = IP
         ENDDO
      ENDDO

      


    END SUBROUTINE Remove_Point_Surf



    SUBROUTINE Update_Topology(nodesToSurf,nodesToElem, SurfIn)
      USE common_Constants
      USE common_Parameters
      USE Queue
      IMPLICIT NONE
      TYPE(SurfaceMeshStorageType), INTENT(IN) :: SurfIn
      TYPE(IntQueueType), INTENT(OUT) :: nodesToSurf(SurfIn%NB_Point)
      TYPE(IntQueueType) :: nodesToNode(SurfIn%NB_Point)
      TYPE(IntQueueType), INTENT(OUT) :: nodesToElem(SurfIn%NB_Point)
      REAL*8 :: normals(3,SurfIn%NB_Point)
      REAL*8 :: triNorm(3,SurfIn%NB_Tri)
      INTEGER :: ie,i,ip,ne, si,j,jp

      !First the triangles
      DO ie = 1,SurfIn%NB_Tri
        DO i = 1,3
          ip = SurfIn%IP_Tri(i,ie)
          IF (ip.GT.0) THEN
            IF (.NOT.IntQueue_Contain(nodesToElem(ip),ie)) THEN
              CALL IntQueue_Push(nodesToElem(ip),ie)
          END IF
        END IF
      END DO
      
    END DO

    normals(:,:) = 0.0d0

    !...and the normals
    DO ip = 1,SurfIn%NB_Point
      ne = nodesToElem(ip)%numNodes
      IF (ne.GT.0) THEN
        DO i = 1,ne
          
          DO j = 1,3

            jp = SurfIn%IP_Tri(j,nodesToElem(ip)%nodes(i))
            IF (.NOT.IntQueue_Contain(nodesToNode(ip),jp)) THEN
                CALL IntQueue_Push(nodesToNode(ip),jp)
            END IF

          END DO


          si = SurfIn%IP_Tri(5,nodesToElem(ip)%nodes(i))
          IF (.NOT.IntQueue_Contain(nodesToSurf(ip),si)) THEN
              CALL IntQueue_Push(nodesToSurf(ip),si)
            END IF
        END DO
        

      END IF
    END DO


  END SUBROUTINE





END SUBROUTINE



!*******************************************************************************
!>
!!  Hex Layering
!!  
!!  Generate Hex mesh layer by layer
!<
!*******************************************************************************
SUBROUTINE Hex_Layering()
    
    USE common_Constants
    USE common_Parameters
    USE CellConnectivity
    USE Queue
    USE SurfaceMeshManager
    USE SurfaceMeshStorage
    USE HybridMeshStorage
    USE HybridMeshManager
    USE Plane3DGeom
    USE SurfaceCurvatureManager
    IMPLICIT NONE

    TYPE(HybridMeshStorageType) :: TetMesh
    TYPE(SurfaceMeshStorageType) :: QuadMesh
    INTEGER :: i,j,s,k,l,m,ip1,ip2,ip3,si,sj

    INTEGER :: noOfRegions
    INTEGER, ALLOCATABLE :: nsreg(:), opreg(:,:)
    REAL*8, ALLOCATABLE  :: regMax(:,:), regMin(:,:)
    TYPE(IntQueueType), ALLOCATABLE :: lreg(:)

    TYPE(NodeNetTreeType) :: TriTree
    INTEGER :: IT, IB, NB, ipp(3), ip
    INTEGER :: NB_PyrAT, NB_PrmAT,numMarked
    INTEGER :: flag, added

    INTEGER,POINTER :: layer(:,:),layerNode(:,:),markHybTet(:)
    TYPE(SurfaceMeshStorageType), ALLOCATABLE :: contours(:)
    TYPE(SurfaceMeshStorageType), ALLOCATABLE :: surfLayers(:,:)
    TYPE(SurfaceCurvatureType):: Curv

    TYPE(IntQueueType), ALLOCATABLE :: nodesToSurf(:)
    TYPE(IntQueueType), ALLOCATABLE :: nodesToElem(:)
    REAL*8, ALLOCATABLE :: surfNorms(:,:), nodeNorms(:,:)

    REAL*8 :: P1(3), P2(3), P3(3), P(2), dist, minDist, norm(3),dir(3),dirTest
    REAL*8 :: pp2(3,2), pp3(3,3),weight(3),t, r
    INTEGER :: ntype, layNum, oldlayNum, finished

    CHARACTER(len=5) :: filename1, filename2


    !Read region information
    !Read in the reg file
    OPEN(922,file = JobName(1:JobNameLength)//'.reg',form='formatted')
    READ(922,'(I10)') noOfRegions
    ALLOCATE(nsreg(noOfRegions),lreg(noOfRegions),opreg(2,noOfRegions))
    ALLOCATE(regMax(3,noOfRegions), regMin(3,noOfRegions))
    DO k = 1,noOfRegions
        READ(922,'(I10)') nsreg(k)
        DO j = 1,nsreg(k)
            READ(922,'(I10)') s
            CALL IntQueue_Push(lreg(k), s)
        END DO
    END DO
    CLOSE(922)

    !Read mesh
    CALL HybridMeshStorage_Input(JobName,JobNameLength,-3,TetMesh,Surf)
    !Need to make this the 'main' mesh
    

    WRITE(*, *) ' NB_Tet,NB_Point,NB_BD_Tri= ',TetMesh%NB_Tet,TetMesh%NB_Point,Surf%NB_Tri
    WRITE(29,*) ' NB_Tet,NB_Point,NB_BD_Tri= ',TetMesh%NB_Tet,TetMesh%NB_Point,Surf%NB_Tri

    
    
    CALL HybridMesh_BuildNext(TetMesh) !Build next information
    
    !We need to associate the surface triangles with elements

     !--- build a triangulation system

    CALL NodeNetTree_allocate(3, TetMesh%NB_Point, Surf%NB_Tri, TriTree)
    Surf%IP_Tri(4,:) = 0

    Surf%NB_Point = 0
    DO IB = 1,Surf%NB_Tri
       ipp(1:3) = Surf%IP_Tri(1:3,IB)
       CALL NodeNetTree_SearchAdd(3,ipp, TriTree, NB)
       Surf%NB_Point = MAX(Surf%NB_Point, ipp(1), ipp(2), ipp(3))
       !--- mark interface triangle
       IF(Surf%IP_Tri(5,IB) .LE. noOfRegions) Surf%IP_Tri(4,IB) = -1
    ENDDO
    IF(NB/=Surf%NB_Tri)THEN
       WRITE(*,*)' Error--- NB/=Surf%NB_Tri :', NB, Surf%NB_Tri
       CALL Error_Stop ('HybridMesh_AssoBoundSurface :: NB/=Surf%NB_Tri ')
    ENDIF

    !--- Search tet. element for boundary triangles

    DO IT = 1,TetMesh%NB_Tet
       DO i=1,4
          ipp(1:3) = TetMesh%IP_Tet(iTri_Tet(1:3,i),IT)
          IF(MAXVAL(ipp)>Surf%NB_Point) CYCLE

          CALL NodeNetTree_search(3,ipp,TriTree,IB)
          IF(IB<=0) CYCLE           

          IF(Surf%IP_Tri(4,IB)<=0)THEN
             !--- point into the tetrahedron
             Surf%IP_Tri(4,IB) = IT
          ENDIF
       ENDDO
    ENDDO

   CALL NodeNetTree_Clear(TriTree)

   !Now in the actual surfaces IP_Tri(4,ie) is the tet number for the first layer
   !noOfRegions = 1 !TODO: This shouldn't be hardcoded, it tells us how many surfaces to advance from

   ALLOCATE(layer(noOfRegions,TetMesh%NB_Tet),layerNode(noOfRegions,TetMesh%NB_Point),&
            markHybTet(TetMesh%NB_Tet))
   layer(:,:) = 0
   layerNode(:,:) = 0
   markHybTet(:) = 0
   DO IB = 1,Surf%NB_Tri
    IF (Surf%IP_Tri(5,IB) .LE. noOfRegions) THEN
        
            markHybTet(Surf%IP_Tri(4,IB)) = 1
            layer(Surf%IP_Tri(5,IB),Surf%IP_Tri(4,IB)) = 1
            layerNode(Surf%IP_Tri(5,IB),Surf%IP_Tri(1,IB)) = 1 !Unless the tet already marked add 
            layerNode(Surf%IP_Tri(5,IB),Surf%IP_Tri(2,IB)) = 1 !to layer tets which have nodes marked
            layerNode(Surf%IP_Tri(5,IB),Surf%IP_Tri(3,IB)) = 1
        
    END IF
   END DO

  
  
   DO IT = 1,TetMesh%NB_Tet
    IF (markHybTet(IT).EQ.0) THEN
        
        DO j = 1,noOfRegions
            
            DO i = 1,4
                ip = TetMesh%IP_Tet(i,IT)
                IF (layerNode(j,ip).EQ.1) THEN
                    !Check to see any other nodes of this tet are already marked
                    flag = 0
                    DO k = 1,4
                        l = TetMesh%IP_Tet(k,IT)
                        DO m = 1,noOfRegions
                            IF (m.NE.j) THEN
                                flag = flag + layerNode(m,l)
                            END IF
                        END DO
                    END DO
                    IF (flag.LT.5) THEN
                        IF (markHybTet(IT).EQ.0) THEN
                            markHybTet(IT) = 1
                            layer(j,IT) = 1
                        END IF
                    END IF
                END IF
            END DO

        END DO

        
    END IF  
   END DO

   !markHybTet(:) = 0
   !numMarked = 0
   !DO IT = 1,TetMesh%NB_Tet
   !    IF (layer(3,IT).EQ.1) THEN
   !        numMarked = numMarked + 1
   !        markHybTet(numMarked) = IT
   !    END IF
   !END DO
   !CALL HybridMeshStorage_Output('firstLay',8,-3,TetMesh,Surf,markHybTet,numMarked)

   layNum = 1
   finished = 0
   ALLOCATE(contours(noOfRegions))

   DO s = 1,noOfRegions
    contours(s)%NB_Surf = 0
    contours(s)%NB_Point = TetMesh%NB_Point
    CALL allc_Surf(contours(s),TetMesh%NB_Tet,TetMesh%NB_Point,TetMesh%NB_Tet)
    contours(s)%Posit(:,1:TetMesh%NB_Point) = TetMesh%Posit(:,1:TetMesh%NB_Point)
    contours(s)%NB_Tri = 0
   END DO

   DO WHILE (finished.EQ.0)

        WRITE(*,*)'Building layer ',layNum
        oldlayNum = layNum

       DO s = 1,noOfRegions
             
           DO IT = 1,TetMesh%NB_Tet
            IF (markHybTet(IT).EQ.0) THEN       
                numMarked = 0
                DO i = 1,4
                    IB = TetMesh%Next_Tet(i,IT)
                    IF (layer(s,IB).EQ.layNum) THEN
                        !k is the index to the next tet
                        ip1 = TetMesh%IP_Tet(iTri_Tet(1,i),IT)
                        ip2 = TetMesh%IP_Tet(iTri_Tet(2,i),IT)
                        ip3 = TetMesh%IP_Tet(iTri_Tet(3,i),IT)
                        contours(s)%NB_Tri = contours(s)%NB_Tri + 1
                        contours(s)%IP_Tri(1,contours(s)%NB_Tri) = ip1
                        contours(s)%IP_Tri(2,contours(s)%NB_Tri) = ip2
                        contours(s)%IP_Tri(3,contours(s)%NB_Tri) = ip3
                        contours(s)%IP_Tri(5,contours(s)%NB_Tri) = layNum
                        contours(s)%NB_Surf = layNum

                        !Fill in the layer information for the next layer
                        IF (markHybTet(IT).EQ.0) THEN
                          markHybTet(IT) = layer(s,IB) + 1
                          
                          layer(s,IT) = markHybTet(IT)
                          layerNode(s,ip1) = markHybTet(IT)  
                          layerNode(s,ip2) = markHybTet(IT) 
                          layerNode(s,ip3) = markHybTet(IT)
                        END IF
                    END IF
                END DO
                
            END IF  
           END DO
       END DO

       layNum = MAXVAL(markHybTet)

       !Mark tets in the gaps
       DO IT = 1,TetMesh%NB_Tet
        IF (markHybTet(IT).EQ.0) THEN
            
            DO j = 1,noOfRegions
                
                DO i = 1,4
                    ip = TetMesh%IP_Tet(i,IT)
                    IF (layerNode(j,ip).EQ.layNum) THEN
                        !Check to see any other nodes of this tet are already marked
                        flag = 0
                        DO k = 1,4
                            l = TetMesh%IP_Tet(k,IT)
                            DO m = 1,noOfRegions
                                IF (m.NE.j) THEN
                                    flag = flag + layerNode(m,l)
                                END IF
                            END DO
                        END DO
                        IF (flag.LT.5) THEN
                            IF (markHybTet(IT).EQ.0) THEN
                                markHybTet(IT) = layNum
                                layer(j,IT) = layNum
                            END IF
                        END IF
                    END IF
                END DO

            END DO          
        END IF  
       END DO

       IF (layNum.EQ.oldlayNum) THEN
        !Nothing was added
        finished = 1
       END IF

   END DO



   !markHybTet(:) = 0
   !numMarked = 0
   !DO IT = 1,TetMesh%NB_Tet
   !    IF (layer(3,IT).EQ.MAXVAL(layer(3,:))) THEN
   !        numMarked = numMarked + 1
   !        markHybTet(numMarked) = IT
   !    END IF
   !END DO
   !CALL HybridMeshStorage_Output('secondLay',8,-3,TetMesh,Surf,markHybTet,numMarked)

   DO m = 1,noOfRegions
    WRITE(filename1,'(I5.5)') m
    CALL SurfaceMeshStorage_Output('cont'//filename1,9,contours(m))
    !CALL Surf_To_Dat(contours(m),Curv)
    !CALL SurfaceMeshStorage_Output('contsmo'//filename1,12,contours(m))
    !CALL SurfaceCurvature_Output('medial'//filename1,11,Curv)
   END DO

   !Contours is now a surface object with the first layer of each region
   !CALL Surf_To_Dat(contours,Curv)

   CALL SurfaceMeshStorage_Input(JobName,JobNameLength,QuadMesh)
   ALLOCATE(nodesToSurf(QuadMesh%NB_Point),nodesToElem(QuadMesh%NB_Point))
   
   CALL Update_Topology(nodesToSurf,nodesToElem,QuadMesh)

   !This projects to the new curvature stuff
   !DO i = 1,QuadMesh%NB_Point
   !        P2(:) = 0.0d0
   !        flag = 0
   !        minDist = HUGE(0.0d0)
   !        DO si = 1,nodesToSurf(i)%numNodes
    !       S = nodesToSurf(i)%nodes(si)
    !       IF (S.LE. noOfRegions)THEN
    !           P(1) = 0.5*REAL(medNumSamp)
    !           P(2) = 0.5*REAL(medNumSamp)
    !           CALL RegionType_GetUVFromXYZ1(Curv%Regions(S), QuadMesh%Posit(:,i), P, 0.001d0, 0, dist, P3)
    !           IF (dist.LT.minDist) THEN
    !               P2(:) = P3(:)
    !               dist = minDist
    !           END IF
    !           flag = 1
    !       END IF
    !   END DO
    !   IF (flag .EQ. 1) THEN
    !       P3(:) = P2(:)
    !       QuadMesh%Posit(:,i) = P3(:)
    !   END IF
!
!
  ! END DO


  !Project to the triangulation rather than the new surface
  !First we need normals
  ALLOCATE(surfNorms(3,QuadMesh%NB_Tri))
  DO it = 1,QuadMesh%NB_Tri
    surfNorms(:,it) = Geo3D_Cross_Product(QuadMesh%Posit(:,QuadMesh%IP_Tri(1,it)), &
                                                QuadMesh%Posit(:,QuadMesh%IP_Tri(2,it)), &
                                                QuadMesh%Posit(:,QuadMesh%IP_Tri(3,it)))
    surfNorms(:,it) = surfNorms(:,it)/Geo3D_Distance(surfNorms(:,it))
  END DO

  ALLOCATE(nodeNorms(3,QuadMesh%NB_Point))
  DO ip = 1,QuadMesh%NB_Point
    !First check this is a point we care about
    flag = 0
    DO si = 1,nodesToSurf(ip)%numNodes
        S = nodesToSurf(ip)%nodes(si)
        IF (S.LE. noOfRegions)THEN
            flag = 1
        END IF
    END DO

    IF (flag .EQ. 1) THEN
        !First find the normal
        norm(:) = 0.0d0
        DO i = 1,nodesToElem(ip)%numNodes
        it = nodesToElem(ip)%Nodes(i)
        IF (QuadMesh%IP_Tri(5,it).LE.noOfRegions) THEN
             norm(:) = norm(:) + surfNorms(:,it)
        END IF
        END DO
        !norm(:) = norm(:)/REAL(nodesToElem(ip)%numNodes)
        nodeNorms(:,ip) = norm(:)/Geo3D_Distance(norm)
    END IF
  END DO

  m = 0
  DO WHILE (m.LT.100)
    m = m + 1
    WRITE(*,*) 'Smoothing normals ',m
    !Now to smooth the surface normals
    DO it = 1,QuadMesh%NB_Tri
      surfNorms(:,it) = (nodeNorms(:,QuadMesh%IP_Tri(1,it)) + nodeNorms(:,QuadMesh%IP_Tri(2,it)) &
                        + nodeNorms(:,QuadMesh%IP_Tri(3,it)))
      surfNorms(:,it) = surfNorms(:,it)/Geo3D_Distance(surfNorms(:,it))
    END DO
    DO ip = 1,QuadMesh%NB_Point
      !First check this is a point we care about
      flag = 0
      DO si = 1,nodesToSurf(ip)%numNodes
        S = nodesToSurf(ip)%nodes(si)
        IF (S.LE. noOfRegions)THEN
          flag = flag + 1
        END IF
      END DO

      IF (flag .EQ. 1) THEN
        !First find the normal
        norm(:) = 0.0d0
        DO i = 1,nodesToElem(ip)%numNodes
          it = nodesToElem(ip)%Nodes(i)
          IF (QuadMesh%IP_Tri(5,it).LE.noOfRegions) THEN
           norm(:) = norm(:) + surfNorms(:,it)
          END IF
        END DO
        !norm(:) = norm(:)/REAL(nodesToElem(ip)%numNodes)
        nodeNorms(:,ip) = norm(:)/Geo3D_Distance(norm)
      END IF
    END DO
  END DO




  !Now to allocate the surfaces
  ALLOCATE(surfLayers(noOfRegions,layNum))
  DO s = 1,noOfRegions
    DO l = 1,layNum
        surfLayers(s,l)%NB_Surf = 1
        surfLayers(s,l)%NB_Point = QuadMesh%NB_Point
        CALL allc_Surf(surfLayers(s,l),QuadMesh%NB_Tri,QuadMesh%NB_Point,QuadMesh%NB_Tri)
        surfLayers(s,l)%Posit(:,1:QuadMesh%NB_Point) = QuadMesh%Posit(:,1:QuadMesh%NB_Point)
        surfLayers(s,l)%NB_Tri = 0
        DO it = 1,QuadMesh%NB_Tri
            IF (QuadMesh%IP_Tri(5,it).EQ.s) THEN
                surfLayers(s,l)%NB_Tri = surfLayers(s,l)%NB_Tri + 1
                surfLayers(s,l)%IP_Tri(:,surfLayers(s,l)%NB_Tri) = QuadMesh%IP_Tri(:,it)
                surfLayers(s,l)%IP_Tri(5,surfLayers(s,l)%NB_Tri) = 1
            END IF
        END DO
    END DO
  END DO
  
  DO l = 1,layNum
    DO s = 1,noOfRegions
        WRITE(*,*) 'Region ',s,' layer ',l
        DO ip = 1,QuadMesh%NB_Point

            !First check this is a point we care about
            flag = 0
            DO si = 1,nodesToSurf(ip)%numNodes
                sj = nodesToSurf(ip)%nodes(si)
                IF (sj.EQ. s)THEN
                    flag = 1
                END IF
            END DO

            IF (flag.EQ.1) THEN

                norm(:) = nodeNorms(:,ip)
                !Now draw the line for intersection
                P1(:) = surfLayers(s,l)%Posit(:,ip)
                P2(:) = P1(:) - 1000.0d0*norm(:)
                pp2(:,1) = P1(:)
                pp2(:,2) = P2(:)
                !Loop over each surface connected to this point find the closest intersection
                minDist = HUGE(0.0d0)
                P3(:) = P1(:)
                DO si = 1,nodesToSurf(ip)%numNodes
                    sj = nodesToSurf(ip)%nodes(si)
                    IF (sj .EQ. s)THEN
                        DO i = 1,contours(s)%NB_Tri
                            IF (contours(s)%IP_Tri(5,i).EQ.l) THEN
                                pp3(:,1) = contours(s)%Posit(:,contours(s)%IP_Tri(1,i))
                                pp3(:,2) = contours(s)%Posit(:,contours(s)%IP_Tri(2,i))
                                pp3(:,3) = contours(s)%Posit(:,contours(s)%IP_Tri(3,i))
                                ntype = -1000
                                P2(:) = Plane3D_Line_Tri_Cross(pp2,pp3,ntype,weight,t)
                                !Check direction and position
                                dir(:) = P1(:) - P2(:)
                                dirTest = norm(1)*dir(1) + norm(2)*dir(2) + norm(3)*dir(3)
                                IF ((ntype.GT.0).AND.(dirTest.GE.0)) THEN
                                    dist = Geo3D_Distance_SQ(P2(:),QuadMesh%Posit(:,ip))
                                    IF (dist.LT.minDist) THEN
                                        minDist = dist
                                        P3(:) = P2(:)
                                    END IF
                                END IF

                            END IF
                        END DO
                    END IF
                END DO
                DO si = l,layNum
                    surfLayers(s,si)%Posit(:,ip) = P3(:)
                END DO
            END IF

        END DO

        !At the end put the current points in the next surface
        WRITE(filename1,'(I5.5)') s
        WRITE(filename2,'(I5.5)') l
        CALL SurfaceMeshStorage_Output_STL(filename1//'_'//filename2,11,surfLayers(s,l))
    END DO
  END DO

  !DO l = 1,layNum
  !  DO s = 1,noOfRegions

    !   WRITE(filename1,'(I5.5)') s
    !   WRITE(filename2,'(I5.5)') l
    !   CALL SurfaceMeshStorage_Output(filename1//'_'//filename2,11,surfLayers(s,l))

  !  END DO
  !END DO

  

   
contains

SUBROUTINE Update_Topology(nodesToSurf,nodesToElem, SurfIn)
    USE common_Constants
    USE common_Parameters
    USE Queue
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: SurfIn
    TYPE(IntQueueType), INTENT(OUT) :: nodesToSurf(SurfIn%NB_Point)
    TYPE(IntQueueType) :: nodesToNode(SurfIn%NB_Point)
    TYPE(IntQueueType), INTENT(OUT) :: nodesToElem(SurfIn%NB_Point)
    REAL*8 :: normals(3,SurfIn%NB_Point)
    REAL*8 :: triNorm(3,SurfIn%NB_Tri)
    INTEGER :: ie,i,ip,ne, si,j,jp

    !First the triangles
    DO ie = 1,SurfIn%NB_Tri
        DO i = 1,3
            ip = SurfIn%IP_Tri(i,ie)
            IF (ip.GT.0) THEN
                IF (.NOT.IntQueue_Contain(nodesToElem(ip),ie)) THEN
                    CALL IntQueue_Push(nodesToElem(ip),ie)
                END IF
            END IF
        END DO
        
    END DO

    normals(:,:) = 0.0d0

    !...and the normals
    DO ip = 1,SurfIn%NB_Point
        ne = nodesToElem(ip)%numNodes
        IF (ne.GT.0) THEN
            DO i = 1,ne
                
                DO j = 1,3

                    jp = SurfIn%IP_Tri(j,nodesToElem(ip)%nodes(i))
                    IF (.NOT.IntQueue_Contain(nodesToNode(ip),jp)) THEN
                        CALL IntQueue_Push(nodesToNode(ip),jp)
                    END IF

                END DO


                si = SurfIn%IP_Tri(5,nodesToElem(ip)%nodes(i))
                IF (.NOT.IntQueue_Contain(nodesToSurf(ip),si)) THEN
                    CALL IntQueue_Push(nodesToSurf(ip),si)
                END IF
            END DO
            

        END IF
    END DO


END SUBROUTINE




END SUBROUTINE

!*******************************************************************************
!>
!!  Surf to dat
!!  Converts a triangulation to a dat file
!!  
!!  
!<
!*******************************************************************************




SUBROUTINE Surf_To_Dat(SurfIn,Curv)

 
  USE common_Constants
  USE common_Parameters
  USE Queue
  USE Plane3DGeom
  USE SurfaceCurvatureManager
  IMPLICIT NONE

  TYPE(SurfaceCurvatureType), INTENT(OUT) :: Curv
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: SurfIn

  INTEGER :: noOfRegions
  INTEGER, ALLOCATABLE :: nsreg(:), opreg(:,:)
  REAL*8, ALLOCATABLE  :: regMax(:,:), regMin(:,:)
  TYPE(IntQueueType), ALLOCATABLE :: lreg(:)

  INTEGER :: S, ie, je, ke, numCuts, k
  REAL*8  :: zmin(3,SurfIn%NB_Surf + SurfIn%NB_Sheet), zmax(3,SurfIn%NB_Surf + SurfIn%NB_Sheet)
  INTEGER :: ip

  INTEGER ::  c
  TYPE (Point3dQueueType) ::  cutRaw
  TYPE (Point3dQueueType), ALLOCATABLE :: cuts(:)
  REAL*8  :: cut
  TYPE(Plane3D)  :: aPlane

  INTEGER :: it1, it2, ip1, ip2
  INTEGER :: doesCut

  INTEGER :: d  !Dimension of the polynomial
  REAL*8, ALLOCATABLE  :: polyCoef(:)  !Polynomial coefficients
  REAL*8, ALLOCATABLE  :: meanSpace(:)  !Mean spacing of each region
 
 
  REAL*8  :: L1(2), L2(2), V1(2), V2(2), P(2)

  REAL*8, ALLOCATABLE, dimension(:) :: vx,vy

  REAL*8 :: P1(3),P2(3),P3(3), t
  INTEGER :: sa,o

  CHARACTER(len=3) :: fileNum

  INTEGER :: pDim, id, crossDim(2), iDim, jDim, i
  REAL*8  :: maxRange
  INTEGER :: loc(1), maxi, mini

  INTEGER :: completeFlag
  REAL*8  :: sumError, yi

  
  INTEGER :: curvNum,iv,iu,iu2, iv2

  INTEGER :: regCon(SurfIn%NB_Surf + SurfIn%NB_Sheet,SurfIn%NB_Surf + SurfIn%NB_Sheet)
  INTEGER :: si, sj

  TYPE(Point3dQueueType) :: curveList((SurfIn%NB_Surf + SurfIn%NB_Sheet)*10) !SPW - this is a limit on the number of curves, may need changing
  TYPE(IntQueueType), ALLOCATABLE  :: tempIC(:)
  INTEGER :: nextN(SurfIn%NB_Point),lastN(SurfIn%NB_Point),inN(SurfIn%NB_Point)
  INTEGER :: np, start, j, numEdge, e, jee, kee, ne
  INTEGER :: traversing, currentI, R1, R2, looping, checkChange
  REAL*8  :: xl(2), drm, frac, z1, z2, r, d1, d2, spacingReg(SurfIn%NB_Surf + SurfIn%NB_Sheet)
  INTEGER :: keMin, uMin, vMin, numCuts2, iuu, ivv
  REAL*8  :: dist, minDist, threshold
  INTEGER, ALLOCATABLE :: corners(:,:), corners2(:,:)
  INTEGER :: numCorn, numCorn2
  
  INTEGER, ALLOCATABLE :: connectedList(:,:,:)
  INTEGER :: maxCuts, smoothLoop, globLoop

  TYPE(IntQueueType) :: nodesToSurf(SurfIn%NB_Point),nodesToNode(SurfIn%NB_Point),nodesToElem(SurfIn%NB_Point)
  REAL*8 :: surfNorm(3,SurfIn%NB_Point), newPosit(3,SurfIn%NB_Point)
  INTEGER :: isBound(SurfIn%NB_Point), flag,markSurfNode(SurfIn%NB_Point)

  REAL*8 :: pp2(3,2),pp3(3,3)
  INTEGER :: ntype
  REAL*8 :: weight(3)

  !CALL Coarsen_Mesh()

  !CALL Medial_Axis_ID()

  !Build the data structures
  CALL Surf_BuildNext(SurfIn)

  !Find which nodes are on ridges
  CALL Update_Topology(nodesToSurf,nodesToNode,SurfIn)
  isBound(:) = 0
  DO i = 1,SurfIn%NB_Point 
    IF (nodesToSurf(i)%numNodes.GT.1) THEN
        isBound(i) = 1
    END IF
  END DO

  OPEN(922,file = JobName(1:JobNameLength)//'.reg',form='formatted')
    READ(922,'(I10)') noOfRegions
    ALLOCATE(nsreg(noOfRegions),lreg(noOfRegions),opreg(2,noOfRegions))
    ALLOCATE(regMax(3,noOfRegions), regMin(3,noOfRegions))
    DO k = 1,noOfRegions
      READ(922,'(I10)') nsreg(k)
      DO j = 1,nsreg(k)
        READ(922,'(I10)') s
        CALL IntQueue_Push(lreg(k), s)
      END DO
    END DO
    CLOSE(922)


  zmin(:,:) = HUGE(0.0d0)
  zmax(:,:) = -1.0d0 * HUGE(0.0d0)

  maxCuts = 0


  !Need to find the plane values to cut at so will need min and
  !max values for the mesh

  DO ie = 1,SurfIn%NB_Tri
    IF (SurfIn%IP_Tri(5,ie).LE.(SurfIn%NB_Surf + SurfIn%NB_Sheet)) THEN
        S = SurfIn%IP_Tri(5,ie)
        DO ip = 1,3         
        DO id = 1,3
                IF (SurfIn%Posit(id,SurfIn%IP_Tri(ip,ie)) .GT. zmax(id,S)) THEN
                    zmax(id,S) = SurfIn%Posit(id,SurfIn%IP_Tri(ip,ie))
                ELSE IF (SurfIn%Posit(id,SurfIn%IP_Tri(ip,ie)) .LT. zmin(id,S)) THEN
                    zmin(id,S) = SurfIn%Posit(id,SurfIn%IP_Tri(ip,ie))
                END IF
        END DO
        END DO
    END IF
  END DO


  Curv%NB_Region = SurfIn%NB_Sheet+SurfIn%NB_Surf
  ALLOCATE(Curv%Regions(Curv%NB_Region))
  ALLOCATE(meanSpace(Curv%NB_Region))

  DO S = 1,SurfIn%NB_Sheet+SurfIn%NB_Surf

    !Pick a dimension to do cuts in
    maxRange = 0.0d0

    DO id = 1,3
      IF ((zmax(id,S)-zmin(id,S)).GT.maxRange) THEN
        maxRange = (zmax(id,S)-zmin(id,S))
        pDim = id
      END IF
    END DO

    !Work out the other dimension directions
    i = 0
    DO id = 1,3
      IF (id.NE.pDim) THEN
        i = i+1
        crossDim(i) = id
      END IF    
    END DO
    iDim = MINVAL(crossDim)
    jDim = MAXVAL(crossDim)

    !Squeeze the range a little to avoid inersection problems
    r = (zmax(pDim,S)-zmin(pDim,S))
    z1 = 0.5d0*(zmax(pDim,S)+zmin(pDim,S)) - 0.5d0*r
    z2 = 0.5d0*(zmax(pDim,S)+zmin(pDim,S)) + 0.5d0*r
    zmax(pDim,S) = z2
    zmin(pDim,S) = z1

    WRITE(fileNum,'(I3.3)') S
    
   ! numCuts = MAX(CEILING(r/medSpace),2)
   ! maxCuts = MAX(numCuts,maxCuts)
     numCuts = medNumSamp

    ALLOCATE(cuts(numCuts))

    WRITE(*,*) 'Surface ',S, ' max Z ', zmax(pDim,S), ' min z ', zmin(pDim,S)
    
    meanSpace(S) = ((zmax(pDim,S)-zmin(pDim,S))/REAL(numCuts-1))

    DO c = 1,numCuts
        CALL Point3dQueue_Clear(cutRaw)

        cut = zmin(pDim,S) + (C-1)*((zmax(pDim,S)-zmin(pDim,S))/REAL(numCuts-1))

        P1(iDim) = 1
        P1(jDim) = 0
        P1(pDim) = cut

        P2(iDim) = 0
        P2(jDim) = 0
        P2(pDim) = cut

        P3(iDim) = 0
        P3(jDim) = 1
        P3(pDim) = cut

        aPlane = Plane3D_Build(P1,P2,P3)

        !Now to check the edges for intersection
        DO ie = 1,SurfIn%NB_Edge

            !Triangles connected
            it1 = SurfIn%ITR_Edge(1,ie)
            it2 = SurfIn%ITR_Edge(2,ie)

            IF ( (SurfIn%IP_Tri(5,it1).EQ.S).OR.(SurfIn%IP_Tri(5,it2).EQ.S) ) THEN

                !Now check to see if intersection occurs 
                
                ip1 = SurfIn%IP_Edge(1,ie)
                ip2 = SurfIn%IP_Edge(2,ie)

                P1(:) = SurfIn%Posit(:,ip1)
                P2(:) = SurfIn%Posit(:,ip2)

                P3(:) = Plane3D_Line_Plane_Cross(P1,P2,aPlane,t)

                IF ((t.LE.1).AND.(t.GE.0)) THEN

                    CALL Point3dQueue_Push(cutRaw, P3)
                    

                END IF

            END IF


        END DO

        !Now to fit a polynomial

      !First allocate arrays
      ALLOCATE(vx(cutRaw%numNodes))
      ALLOCATE(vy(cutRaw%numNodes))

      DO ie = 1,cutRaw%numNodes

        vx(ie) = cutRaw%Pt(iDim,ie)
        vy(ie) = cutRaw%Pt(jDim,ie)

      END DO

      !Check that we have varying nodes
      loc = MINLOC(vx)
      mini = loc(1)
      loc = MAXLOC(vx)
      maxi = loc(1)      


      IF ((cutRaw%numNodes.GT.1).AND.(maxi.NE.mini)) THEN

            
        IF ((MAXVAL(vx)-MINVAL(vx)).LT.(MAXVAL(vy)-MINVAL(vy))) THEN
          loc = MINLOC(vy)
          mini = loc(1)
          loc = MAXLOC(vy)
          maxi = loc(1)      
        END IF
            

            !...and the start and end point using maxloc    
            L1(1) = vx(maxi)
            L1(2) = vy(maxi)
        
            L2(1) = vx(mini)
            L2(2) = vy(mini)


            !... the vectors of this line
            V1(:) = L2(:) - L1(:)
            V1(:) = V1(:) / DSQRT(V1(1)**2 + V1(2)**2)
            V2(1) = -1.0d0*V1(2)
            V2(2) = V1(1)


            
            !Now to transform the coordinates
            DO ie = 1,cutRaw%numNodes

                !1) Find the projection onto the line
                P(1) = vx(ie)
                P(2) = vy(ie)
                
                
                !2) Convert this into the new coordiate system
                vx(ie) = (P(1) - L1(1))*V1(1) + (P(2) - L1(2))*V1(2)
                vy(ie) = (P(1) - L1(1))*V2(1) + (P(2) - L1(2))*V2(2)
                  
                
                

            END DO

        !Now to continue increasing the order of the polynomial
        !until the error is reduced 
        d = 0
        completeFlag = 0
        DO WHILE (completeFlag.EQ.0)
          !Increase d
          d = d + 1
          ALLOCATE(polyCoef(d+1))
          !Now do the real fit
          polyCoef = polyfit(vx,vy,d)
          sumError = 0.0d0
          DO ie = 1,cutRaw%numNodes
            yi = 0.0d0
            DO o = 1,d+1
              yi =yi + polyCoef(o)*(vx(ie)**REAL(o-1))
            END DO
            sumError = sumError + ABS(yi-vy(ie))
          END DO
          sumError = sumError/REAL(cutRaw%numNodes)
          IF ((sumError.LE.medError).OR.(d.EQ.medOrder)) THEN
            completeFlag = 1
            
          ELSE
            deallocate(polyCoef)
          END IF

        END DO

            

        !And sample (have checked the conversion of axes and it works)
        CALL Point3dQueue_Clear(cuts(c))
        DO sa = 1,numCuts
            P(1) = MINVAL(vx) + REAL(sa-1)*(MAXVAL(vx)-MINVAL(vx))/REAL(numCuts-1)
            P(2) = 0.0d0
            DO o = 1,d+1
              P(2) = P(2) + polyCoef(o)*(P(1)**REAL(o-1))
          END DO

 

            !Now to convert back

            

            !IF (sa.EQ.1) THEN
            !  P3(iDim) = L1(1)
            !  P3(jDim) = L1(2)
            !  P3(pDim) = cut
            !ELSE IF (sa.EQ.numCuts) THEN
            !  P3(iDim) = L2(1)
            !  P3(jDim) = L2(2)
            !  P3(pDim) = cut
            !ELSE
              P3(iDim) = L1(1) + P(1)*V1(1) + P(2)*V2(1)
              P3(jDim) = L1(2) + P(1)*V1(2) + P(2)*V2(2)
              P3(pDim) = cut
            !END IF

            CALL Point3dQueue_Push(cuts(c), P3)
            


        END DO



        CALL Point3dQueue_Clear(cutRaw)
        deallocate(polyCoef)

        ELSE

            !Here is where we will end up with a single point
            
            minDist = HUGE(0.0d0)
            DO i = 1,SurfIn%NB_Point
                DO j = 1,nodesToSurf(i)%numNodes
                    IF (nodesToSurf(i)%Nodes(j).EQ.S) THEN

                        dist = ABS(SurfIn%Posit(pDim,i) - cut)
                        IF (dist.LT.minDist) THEN
                            minDist = dist
                            P3(:) = SurfIn%Posit(:,i)
                            
                        END IF

                    END IF
                END DO
            END DO

            
            CALL Point3dQueue_Push(cuts(c), P3)
           


            CALL Point3dQueue_Clear(cutRaw)
        END IF
      deallocate(vx,vy)

    END DO

    !Region
    Curv%Regions(S)%ID       =  S
    Curv%Regions(S)%TopoType = 1
    Curv%Regions(S)%numNodeU = numCuts
    Curv%Regions(S)%numNodeV = numCuts
    !WRITE(*,*) 'numCuts',Curv%Regions(S)%numNodeU,Curv%Regions(S)%numNodeV
    ALLOCATE(Curv%Regions(S)%Posit(3,numCuts,numCuts))
    DO iu = 1,numCuts

      IF (iu.GT.1) THEN

        IF (cuts(iu)%numNodes.GT.1) THEN
            !Need to check the direction
            P1(:) = cuts(iu)%Pt(:,numCuts) - cuts(iu)%Pt(:,1)
            P2(:) = Curv%Regions(S)%Posit(:,iu-1,numCuts) - Curv%Regions(S)%Posit(:,iu-1,1)

            !Check in same general direction using dot product
            r = P1(1)*P2(1) + P1(2)*P2(2) + P1(3)*P2(3)


            IF (r.LT.0) THEN
             
              DO iv = 1,numCuts
               Curv%Regions(S)%Posit(:,iu,iv) = cuts(iu)%Pt(:,numCuts+1-iv)
              END DO
            ELSE
              DO iv = 1,numCuts
               Curv%Regions(S)%Posit(:,iu,iv) = cuts(iu)%Pt(:,iv)
              END DO
            END iF
        ELSE
            DO iv = 1,numCuts
                 Curv%Regions(S)%Posit(:,iu,iv) = cuts(iu)%Pt(:,1)
            END DO

        END IF
      ELSE

        !This means we have a single point but in reality we want numCuts points
        DO iv = 1,numCuts



            
          IF (cuts(iu)%numNodes.GT.1) THEN
            Curv%Regions(S)%Posit(:,iu,iv) = cuts(iu)%Pt(:,iv)
          ELSE
            Curv%Regions(S)%Posit(:,iu,iv) = cuts(iu)%Pt(:,1)
          END IF


        END DO
      END IF
    END DO

    !The first and last iu's are singularities
    iu = 1
    DO iv = 1,numCuts

      !Guess the position of the next node
      P1(:) = 2.0d0*Curv%Regions(S)%Posit(:,iu+1,iv) - Curv%Regions(S)%Posit(:,iu+2,iv) 

      !Now find the nearest node to this
      minDist = HUGE(0.0d0)
    !DO i = 1,Surf%NB_Point
    !  DO j = 1,nodesToSurf(i)%numNodes
    !    IF (nodesToSurf(i)%Nodes(j).EQ.S) THEN
!
    !      dist = Geo3D_Distance_SQ(Surf%Posit(:,i),P1)
    !      IF (dist.LT.minDist) THEN
    !        minDist = dist
    !        P3(:) = Surf%Posit(:,i)
    !        
    !      END IF
!
    !    END IF
    !  END DO
    !END DO

    Curv%Regions(S)%Posit(:,iu,iv) = P1(:)

    END DO

    iu = numCuts
    DO iv = 1,numCuts

      !Guess the position of the next node
      P1(:) = 2.0d0*Curv%Regions(S)%Posit(:,iu-1,iv) - Curv%Regions(S)%Posit(:,iu-2,iv) 

      !Now find the nearest node to this
      minDist = HUGE(0.0d0)
    !DO i = 1,Surf%NB_Point
    !  DO j = 1,nodesToSurf(i)%numNodes
    !    IF (nodesToSurf(i)%Nodes(j).EQ.S) THEN
!
    !      dist = Geo3D_Distance_SQ(Surf%Posit(:,i),P1)
    !      IF (dist.LT.minDist) THEN
    !        minDist = dist
    !        P3(:) = Surf%Posit(:,i)
    !        
    !      END IF
!
    !    END IF
    !  END DO
    !END DO

    Curv%Regions(S)%Posit(:,iu,iv) = P1(:)

    END DO
   


    CALL RegionType_BuildTangent(Curv%Regions(S))
    deallocate(cuts)
  END DO

  

    

  DO smoothLoop = 1,5
  !Now use the Furgoson patches to smooth the original surface mesh
    WRITE(*,*) 'Smoothing loop ',smoothLoop
    DO i = 1,SurfIn%NB_Point

        flag = 0
        DO si = 1,nodesToSurf(i)%numNodes
          S = nodesToSurf(i)%nodes(si)
          IF (S.LE.noOfRegions) THEN
            flag = 1
          END IF
          !Find the region this surface is in and check there is more than one surface
          DO k = 1,noOfRegions
            IF (IntQueue_Contain(lreg(k), S)) THEN
              IF (nsreg(k).LE.2) THEN
                flag = 1
              END iF
            END IF
          END DO

          
        END DO

        IF (flag.EQ.0) THEN

            !Going to work out the pull from connected nodes, then projected surfaces
            !Pull from connected points
            P1(:) = 0.0d0
            DO j = 1,nodesToNode(i)%numNodes
              P1(:) = P1(:) + SurfIn%Posit(:,nodesToNode(i)%nodes(j)) 
            END DO

            P1(:) = (P1(:)-SurfIn%Posit(:,i))/REAL(nodesToNode(i)%numNodes-1)
            
            P2(:) = 0.0d0
            DO si = 1,nodesToSurf(i)%numNodes
              S = nodesToSurf(i)%nodes(si)
              
              P(1) = 0.5*REAL(numCuts)
              P(2) = 0.5*REAL(numCuts)
              CALL RegionType_GetUVFromXYZ1(Curv%Regions(S), SurfIn%Posit(:,i), P, 0.001d0, 0, dist, P3)
              P2(:) = P2(:) + P3(:)
            END DO
            P3(:) = P2(:)/REAL(nodesToSurf(i)%numNodes)
            
            P3(:) = (P1(:)+P3(:))*0.5d0
            
            SurfIn%Posit(:,i) = SurfIn%Posit(:,i) + 0.1d0*(P3(:)-SurfIn%Posit(:,i))
            
        END IF
          

        

      

    END DO
  END DO

  CALL SurfaceCurvature_Output('medial',6,Curv)

contains

SUBROUTINE Update_Topology(nodesToSurf,nodesToNode, SurfIn)
        USE common_Constants
        USE common_Parameters
        USE Queue
        IMPLICIT NONE
        TYPE(SurfaceMeshStorageType), INTENT(IN) :: SurfIn
        TYPE(IntQueueType), INTENT(OUT) :: nodesToSurf(SurfIn%NB_Point)
        TYPE(IntQueueType), INTENT(OUT) :: nodesToNode(SurfIn%NB_Point)
        TYPE(IntQueueType) :: nodesToElem(SurfIn%NB_Point)
        REAL*8 :: normals(3,SurfIn%NB_Point)
        REAL*8 :: triNorm(3,SurfIn%NB_Tri)
        INTEGER :: ie,i,ip,ne, si,j,jp

        !First the triangles
        DO ie = 1,SurfIn%NB_Tri
            DO i = 1,3
                ip = SurfIn%IP_Tri(i,ie)
                IF (ip.GT.0) THEN
                    IF (.NOT.IntQueue_Contain(nodesToElem(ip),ie)) THEN
                        CALL IntQueue_Push(nodesToElem(ip),ie)
                    END IF
                END IF
            END DO
            
        END DO

        normals(:,:) = 0.0d0

        !...and the normals
        DO ip = 1,SurfIn%NB_Point
            ne = nodesToElem(ip)%numNodes
            IF (ne.GT.0) THEN
                DO i = 1,ne
                    
                    DO j = 1,3

                        jp = SurfIn%IP_Tri(j,nodesToElem(ip)%nodes(i))
                        IF (.NOT.IntQueue_Contain(nodesToNode(ip),jp)) THEN
                            CALL IntQueue_Push(nodesToNode(ip),jp)
                        END IF

                    END DO


                    si = SurfIn%IP_Tri(5,nodesToElem(ip)%nodes(i))
                    IF (.NOT.IntQueue_Contain(nodesToSurf(ip),si)) THEN
                        CALL IntQueue_Push(nodesToSurf(ip),si)
                    END IF
                END DO
                

            END IF
        END DO


    END SUBROUTINE



function polyfit(vx, vy, d)
    implicit none
    integer, intent(in)                   :: d
    integer, parameter                    :: dp = selected_real_kind(15, 307)
    real(dp), dimension(d+1)              :: polyfit
    real(dp), dimension(:), intent(in)    :: vx, vy
 
    real(dp), dimension(:,:), allocatable :: X
    real(dp), dimension(:,:), allocatable :: XT
    real(dp), dimension(:,:), allocatable :: XTX
 
    integer :: i, j
 
    integer     :: n, lda, lwork
    integer :: info
    integer, dimension(:), allocatable :: ipiv
    real(dp), dimension(:), allocatable :: work
 
    n = d+1
    lda = n
    lwork = n
 
    allocate(ipiv(n))
    allocate(work(lwork))
    allocate(XT(n, size(vx)))
    allocate(X(size(vx), n))
    allocate(XTX(n, n))
 
    ! prepare the matrix
    do i = 0, d
       do j = 1, size(vx)
          X(j, i+1) = vx(j)**i
       end do
    end do
 
    XT  = transpose(X)
    XTX = matmul(XT, X)
 
    ! calls to LAPACK subs DGETRF and DGETRI
    call DGETRF(n, n, XTX, lda, ipiv, info)
    if ( info /= 0 ) then
       print *, "problem"
       return
    end if
    call DGETRI(n, XTX, lda, ipiv, work, lwork, info)
    if ( info /= 0 ) then
       print *, "problem"
       return
    end if
 
    polyfit = matmul( matmul(XTX, XT), vy)
 
    deallocate(ipiv)
    deallocate(work)
    deallocate(X)
    deallocate(XT)
    deallocate(XTX)
 
end function

 

END SUBROUTINE




!*******************************************************************************
!>
!!  Coarsen mesh
!!  
!!  Coarsens the mesh based on normals
!!  
!<
!*******************************************************************************
SUBROUTINE Coarsen_Mesh()

    USE common_Constants
    USE common_Parameters
    USE CellConnectivity
    USE Queue
    USE SurfaceMeshManager
    USE SurfaceMeshStorage
    IMPLICIT NONE

    TYPE(IntQueueType) :: nodesToElem(Surf%NB_Point), nodesToSurf(Surf%NB_Point)
    REAL*8 :: normals(3,Surf%NB_Point),triNorm(3,Surf%NB_Tri)
    INTEGER :: ie,e,ip1,ip2,ip1t,ip2t,i,tr,j,markTri(Surf%NB_Tri),markTriNode(Surf%NB_Point),ip
    INTEGER :: ne,  treated(Surf%NB_Tri), numColapse
    INTEGER :: loop, flag
    REAL*8 :: angle,minAngle,count,dist

    !1) Get initial topology and normals
    CALL Update_Normals_Topology(nodesToElem,normals,triNorm,nodesToSurf)

    flag = 1
    loop = 0

    DO WHILE (flag.EQ.1)
        loop = loop+1
        numColapse = 0
        WRITE(*,*)'Loop ',loop
        !2) Loop round each element and check for collapsable edge
        treated(:) = 0
        DO ie = 1,Surf%NB_Tri
            !Loop round each edge
            IF ((Surf%IP_Tri(1,ie).GT.0).AND.(treated(ie).EQ.0)) THEN
                ip1 = 0
                ip2 = 0
                minAngle = HUGE(0.0d0)
                DO e = 1,3
                    ip1t = Surf%IP_Tri(iEdge_Tri(1,e),ie)
                    ip2t = Surf%IP_Tri(iEdge_Tri(2,e),ie)

                    IF (nodesToSurf(ip1t)%numNodes.EQ.nodesToSurf(ip2t)%numNodes) THEN
                        angle = ACOS(Geo3D_Dot_Product(normals(:,ip1t),normals(:,ip2t))/&
                                 (Geo3D_Distance(normals(:,ip1t))*Geo3D_Distance(normals(:,ip2t))))
                        angle = angle*(180.0d0/PI)
                        IF (ABS(angle).LT.medError) THEN
                            dist = Geo3D_Distance(Surf%Posit(:,ip1t),Surf%Posit(:,ip2t))
                            IF (dist.LT.minAngle) THEN
                                ip1 = ip1t
                                ip2 = ip2t
                                minAngle = dist
                            END IF
                        END IF
                    END IF
                END DO

                IF (ip1.GT.0) THEN

                    !We are in business collapse ip1,ip2
                    numColapse = numColapse+1
                    !First put new node in ip1
                    Surf%Posit(:,ip1) = (Surf%Posit(:,ip1)+Surf%Posit(:,ip2))*0.5d0
                    markTri(:) = 0
                    markTriNode(:) = 0

                    !Loop round triangles containing these points and update
                    DO i = 1,nodesToElem(ip1)%numNodes
                        tr = nodesToElem(ip1)%nodes(i)
                        markTri(tr) = 1
                        treated(tr) = 1
                        DO j = 1,3
                            IF (Surf%IP_Tri(j,tr).EQ.ip2) THEN
                                Surf%IP_Tri(:,tr) = 0 !Takes care of elements containing both
                            END iF
                        END DO
                        
                    END DO

                    DO i = 1,nodesToElem(ip2)%numNodes
                        tr = nodesToElem(ip2)%nodes(i)
                        markTri(tr) = 1
                        treated(tr) = 1
                        DO j = 1,3
                            IF (Surf%IP_Tri(j,tr).EQ.ip2) THEN
                                Surf%IP_Tri(j,tr) = ip1 !Points to new node
                            END iF
                        END DO
                    END DO

                    !Now update triangle normals
                    DO tr = 1,Surf%NB_Tri
                        IF ((markTri(tr).EQ.1).AND.(Surf%IP_Tri(1,tr).GT.0)) THEN
                            triNorm(:,tr) = Geo3D_Cross_Product(Surf%Posit(:,Surf%IP_Tri(1,tr)), &
                                                    Surf%Posit(:,Surf%IP_Tri(2,tr)), &
                                                    Surf%Posit(:,Surf%IP_Tri(3,tr)))
                            DO j = 1,3
                                markTriNode(Surf%IP_Tri(j,tr)) = 1
                                IF (.NOT.IntQueue_Contain(nodesToElem(Surf%IP_Tri(j,tr)),tr)) THEN
                                    CALL IntQueue_Push(nodesToElem(Surf%IP_Tri(j,tr)),tr)
                                END IF
                            END DO
                        END IF
                    END DO

                    DO ip = 1,Surf%NB_Point
                        IF (markTriNode(ip).EQ.1) THEN
                          normals(:,ip) = 0.0d0
                          count = 0.0d0
                          ne = nodesToElem(ip)%numNodes
                          DO i = 1,ne
                            IF (Surf%IP_Tri(1,nodesToElem(ip)%nodes(i)).GT.0) THEN
                                normals(:,ip) = normals(:,ip) + triNorm(:,nodesToElem(ip)%nodes(i))
                                count = count + 1.0d0
                            END IF
                          END DO
                          normals(:,ip) = normals(:,ip)/count
                          normals(:,ip) = normals(:,ip)/Geo3D_Distance(normals(:,ip))
                        END IF
                    END DO

                END IF
            END IF

        END DO

        WRITE(*,*)'Number of edges collapsed ',numColapse
        IF (numColapse.EQ.0) THEN
            flag = 0
            EXIT
        END IF

    END DO

    CALL Surf_RemoveFake(Surf)
    CALL SurfaceMeshStorage_Output('coarse',6,Surf)

    contains

    SUBROUTINE Update_Normals_Topology(nodesToElem,normals,triNorm,nodesToSurf)
        USE common_Constants
        USE common_Parameters
        USE Queue
        IMPLICIT NONE
        TYPE(IntQueueType), INTENT(OUT) :: nodesToElem(Surf%NB_Point),nodesToSurf(Surf%NB_Point)
        REAL*8, INTENT(OUT) :: normals(3,Surf%NB_Point)
        REAL*8 , INTENT(OUT):: triNorm(3,Surf%NB_Tri)
        INTEGER :: ie,i,ip,ne, si

        !First the triangles
        DO ie = 1,Surf%NB_Tri
            DO i = 1,3
                ip = Surf%IP_Tri(i,ie)
                IF (ip.GT.0) THEN
                    IF (.NOT.IntQueue_Contain(nodesToElem(ip),ie)) THEN
                        CALL IntQueue_Push(nodesToElem(ip),ie)
                    END IF
                END IF
            END DO
            IF (Surf%IP_Tri(1,ie).GT.0) THEN
               triNorm(:,ie) = Geo3D_Cross_Product(Surf%Posit(:,Surf%IP_Tri(1,ie)), &
                                                Surf%Posit(:,Surf%IP_Tri(2,ie)), &
                                                Surf%Posit(:,Surf%IP_Tri(3,ie)))
            END IF
        END DO

        normals(:,:) = 0.0d0

        !...and the normals
        DO ip = 1,Surf%NB_Point
            ne = nodesToElem(ip)%numNodes
            IF (ne.GT.0) THEN
                DO i = 1,ne
                    normals(:,ip) = normals(:,ip) + triNorm(:,nodesToElem(ip)%nodes(i))
                    si = Surf%IP_Tri(5,nodesToElem(ip)%nodes(i))
                    IF (.NOT.IntQueue_Contain(nodesToSurf(ip),si)) THEN
                        CALL IntQueue_Push(nodesToSurf(ip),si)
                    END IF
                END DO
                normals(:,ip) = normals(:,ip)/REAL(ne)
                normals(:,ip) = normals(:,ip)/Geo3D_Distance(normals(:,ip))

            END IF
        END DO


    END SUBROUTINE


END SUBROUTINE



!*******************************************************************************
!>
!!  Medial Axis Identify
!!  
!!  Identifies the topology of the medial axis rep
!!  medSurf1 is the first medial surface i.e. not the original surface
!!  
!<
!*******************************************************************************
SUBROUTINE Medial_Axis_ID()
    
  USE common_Constants
  USE common_Parameters
  USE Queue
  USE Plane3DGeom
  USE SurfaceCurvatureManager
  IMPLICIT NONE

  INTEGER :: noOfRegions
  INTEGER, ALLOCATABLE :: nsreg(:), opreg(:,:)
  REAL*8, ALLOCATABLE  :: regMax(:,:), regMin(:,:)
  TYPE(IntQueueType), ALLOCATABLE :: lreg(:)
  INTEGER :: k, j, s
  TYPE (Point3dQueueType) ::  cutRaw(medNumSamp,Surf%NB_Surf + Surf%NB_Sheet)
  TYPE (Point3dQueueType) ::  cuts(medNumSamp,Surf%NB_Surf + Surf%NB_Sheet)
  
  REAL*8, ALLOCATABLE :: vx(:), vy(:)
  INTEGER :: loc(1), maxi, mini
  REAL*8  :: L1(2), L2(2), V1(2), V2(2), P(2), cut(medNumSamp,Surf%NB_Surf + Surf%NB_Sheet)

  
  INTEGER :: ie, it1, it2, si, sj, je, ke, ip1, ip2, ip, n, numSurf, i, ipNext
  INTEGER :: ne, e, start, sumFlag, minSize, meanSize
  REAL*8  :: stdDev, t, intFrac,maxRange
  TYPE(IntQueueType) :: surroundSurfs(Surf%NB_Surf + Surf%NB_Sheet)
  TYPE(IntQueueType) :: nodesToElem(Surf%NB_Point), nodesToSurf(Surf%NB_Point)
  INTEGER :: surfSize(Surf%NB_Surf + Surf%NB_Sheet)
  REAL*8 :: surfCentroid(3,Surf%NB_Surf + Surf%NB_Sheet), surfMin(3,Surf%NB_Surf + Surf%NB_Sheet)
  REAL*8 :: surfMax(3,Surf%NB_Surf + Surf%NB_Sheet)
  REAL*8 :: dist, r, n1(3), n2(3),p1(3), p2(3), d, v(3), pc(3), pn(3), vn(3), p3(3)
  INTEGER :: s1, s2, id, sa
  TYPE(Plane3D)  :: aPlane
  INTEGER :: triFlag(Surf%NB_Tri), samp, iDim, jDim, pDim,crossDim(2)

  INTEGER :: dm,o  !Dimension of the polynomial
  REAL*8, ALLOCATABLE  :: polyCoef(:)  !Polynomial coefficients
  INTEGER :: completeFlag,globLoop
  REAL*8  :: sumError, yi
 


  surfSize(:) = 0
  surfCentroid(:,:) = 0.0d0

  numSurf = Surf%NB_Surf + Surf%NB_Sheet

  !First link nodes to triangles
  surfMin(:,:) = HUGE(0.0d0)
  surfMax(:,:) = -1.0d0*HUGE(0.0d0)
  DO ie = 1,Surf%NB_Tri
    DO i = 1,3
        ip = Surf%IP_Tri(i,ie)
        IF (.NOT.IntQueue_Contain(nodesToElem(ip),ie)) THEN
            CALL IntQueue_Push(nodesToElem(ip),ie)
        END IF

        DO j = 1,3

            IF (Surf%Posit(j,ip).GT.surfMax(j,Surf%IP_Tri(5,ie))) THEN
              surfMax(j,Surf%IP_Tri(5,ie)) = Surf%Posit(j,ip)
            END IF

            IF (Surf%Posit(j,ip).LT.surfMin(j,Surf%IP_Tri(5,ie))) THEN
              surfMin(j,Surf%IP_Tri(5,ie)) = Surf%Posit(j,ip)
            END IF

        END DO

    END DO
    surfSize(Surf%IP_Tri(5,ie)) = surfSize(Surf%IP_Tri(5,ie)) + 1
    surfCentroid(:,Surf%IP_Tri(5,ie)) = surfCentroid(:,Surf%IP_Tri(5,ie)) + &
              (Surf%Posit(:,Surf%IP_Tri(1,ie))+Surf%Posit(:,Surf%IP_Tri(2,ie))+Surf%Posit(:,Surf%IP_Tri(3,ie)))/3.0d0
  

  END DO

  DO s = 1,numSurf
    surfCentroid(:,s) = surfCentroid(:,s)/REAL(surfSize(s)) 
  END DO

  DO ip = 1, Surf%NB_Point

    ne = nodesToElem(ip)%numNodes
    DO i = 1,ne
        it1 = nodesToElem(ip)%Nodes(i)

        DO j = 1,ne

            it2 = nodesToElem(ip)%Nodes(j)
            si = Surf%IP_Tri(5,it1)
            sj = Surf%IP_Tri(5,it2)
            IF (.NOT.IntQueue_Contain(nodesToSurf(ip),si)) THEN
                CALL IntQueue_Push(nodesToSurf(ip),si)
            END IF
            IF (.NOT.IntQueue_Contain(nodesToSurf(ip),sj)) THEN
              CALL IntQueue_Push(nodesToSurf(ip),sj)
            END IF


            IF (si.NE.sj) THEN
              IF (.NOT.IntQueue_Contain(surroundSurfs(sj),si)) THEN
                CALL IntQueue_Push(surroundSurfs(sj),si)
              END IF
              IF (.NOT.IntQueue_Contain(surroundSurfs(si),sj)) THEN
                CALL IntQueue_Push(surroundSurfs(si),sj)
              END IF
            END IF

        END DO

    END DO

  END DO


 

 DO s = 1,numSurf

    !Go through each edge and count how many other surfaces are connected

    WRITE(*,*) 'surf',s,'surrounded by'
    DO e = 1,surroundSurfs(s)%numNodes
        WRITE(*,*) surroundSurfs(s)%nodes(e)
    END DO
  END DO
  
  !Read in the reg file
  OPEN(922,file = JobName(1:JobNameLength)//'.reg',form='formatted')
  READ(922,'(I10)') noOfRegions
  ALLOCATE(nsreg(noOfRegions),lreg(noOfRegions),opreg(2,noOfRegions))
  ALLOCATE(regMax(3,noOfRegions), regMin(3,noOfRegions))
  regMin(:,:) = HUGE(0.0d0)
  regMax(:,:) = -1.0d0*HUGE(0.0d0)
  DO k = 1,noOfRegions
    READ(922,'(I10)') nsreg(k)
    DO j = 1,nsreg(k)
        READ(922,'(I10)') s
        CALL IntQueue_Push(lreg(k), s)
        DO id = 1,3
          IF (surfMax(id,s).GT.regMax(id,k)) THEN
            regMax(id,k) = surfMax(id,s)
          END IF
          IF (surfMin(id,s).LT.regMin(id,k)) THEN
            regMin(id,k) = surfMin(id,s)
          END IF
        END DO
    END DO
  END DO
  CLOSE(922)

  !Now to find opposite surfaces for each region
  opreg(:,:) = 0
  DO k = 1,noOfRegions

    !First identify a minimum size for a surface to be valid
    meanSize = 0
    DO j = 1,lreg(k)%numNodes
        s = lreg(k)%Nodes(j)
        meanSize = meanSize + surfSize(s)
    END DO
    meanSize = meanSize/lreg(k)%numNodes
    stdDev = 0
    DO j = 1,lreg(k)%numNodes
        s = lreg(k)%Nodes(j)
        stdDev = stdDev + REAL((surfSize(s)-meanSize)*(surfSize(s)-meanSize))
    END DO
    stdDev = SQRT(stdDev/REAL(lreg(k)%numNodes))

    minSize = meanSize - 1.5*stdDev
    
    dist = 0.0d0

    DO j = 1,lreg(k)%numNodes
        s = lreg(k)%Nodes(j)
        IF ((s.GE.medSurf1).AND.(surfSize(s).GT.minSize)) THEN  !One side must be an original surface

            !Loop round the other surfaces
            DO i = 1,lreg(k)%numNodes
                si = lreg(k)%Nodes(i)

                IF ((si.GE.medSurf1).AND.(si.NE.s)) THEN

                    IF (.NOT.IntQueue_Contain(surroundSurfs(s),si)) THEN
                        IF (surfSize(si).GT.minSize) THEN
                            r = Geo3D_Distance(surfCentroid(:,s),surfCentroid(:,si))
                            IF (r.GT.dist) THEN
                                opreg(1,k) = s
                                opreg(2,k) = si
                                dist = r
                            END IF                      
                        END IF
                    END IF


                END IF

            END DO
        END IF
    END DO
  END DO


  WRITE(*,*) 'regs', noOfRegions
  DO k = 1,noOfRegions
    WRITE(*,*) 'nosurf',nsreg(k)
    DO j = 1,nsreg(k)
        WRITE(*,*) lreg(k)%Nodes(j)
    END DO
    WRITE(*,*)'opposites',opreg(1,k),opreg(2,k)
  END DO

  !Now to use the opposite information to make cuts in the surfaces
  CALL Surf_BuildNext(Surf)
  DO k = 1,noOfRegions

    s1 = opreg(1,k)
    s2 = opreg(2,k)

    

    p1(:) = surfCentroid(:,s1)
    p2(:) = surfCentroid(:,s2)

    !Check that the normals are orientated in the same direction
    r = n1(1)*n2(1) + n1(2)*n2(2) + n1(3)*n2(3)

    IF (r.LT.0) THEN
        !Reverse one of them
        n1(:) = n1(:)*(-1.0d0)
    END IF

    !The normal is going to be interpolated along the line connecting p1 to p2
    d = Geo3D_Distance(p1,p2)
    v(:) = (p2(:) - p1(:))/d

    !To find the normal look for the largest distance
    maxRange = 0.0d0
    DO id = 1,3
        IF (ABS(v(id)).GT.maxRange) THEN
           pDim = id
           maxRange = ABS(v(id))
        END IF
    END DO
    !Work out the other dimension directions
    i = 0
    DO id = 1,3
      IF (id.NE.pDim) THEN
        i = i+1
        crossDim(i) = id
      END IF    
    END DO
    iDim = MINVAL(crossDim)
    jDim = MAXVAL(crossDim)

    vn(:) = 0.0d0
    vn(pDim) = 1.0d0
    pn(:) = vn(:)

    !Now correct p1 and p2 based on this
    p1(pDim) = regMin(pDim,k)
    p2(pDim) = regMax(pDim,k)
    d = Geo3D_Distance(p1,p2)
    v(:) = (p2(:) - p1(:))/d



    DO samp = 1,medNumSamp

        !Interpolate the plane
        intFrac = ((REAL(samp)-1.0d0)/REAL(medNumSamp-1))
        pc(:) = p1(:) + intFrac*d*v(:)
        

        aPlane = Plane3D_Build2(pc,pn)

        triFlag(:) = 0

        !Check edges for intersection
        DO ie = 1,Surf%NB_Edge
            ip1 = Surf%IP_Edge(1,ie)
            ip2 = Surf%IP_Edge(2,ie)
            !Check the points at this edge
            DO i = 1,2
              ip = Surf%IP_Edge(i,ie)
            
              DO j = 1,nodesToElem(ip)%numNodes
                e = nodesToElem(ip)%Nodes(j)
                IF (triFlag(e).EQ.0) THEN
                    triFlag(e) = 1
                    s = Surf%IP_Tri(5,e)        
                    IF ((IntQueue_Contain(lreg(k),s))) THEN
                        P3(:) = Plane3D_Line_Plane_Cross(Surf%Posit(:,ip1),Surf%Posit(:,ip2),aPlane,t)

                        IF ((t.LE.1).AND.(t.GE.0)) THEN
                            CALL Point3dQueue_Push(cutRaw(samp,s), P3)
                            cut(samp,s) = pc(pDim)
                        END IF

                    END IF
                END IF  
                    
              END DO
            END DO
        END DO

    END DO

    !Now write this out
    WRITE(1000+k,'(3E15.6,I10)') p1(1), p1(2), p1(3),0
    WRITE(1000+k,'(3E15.6,I10)') p2(1), p2(2), p2(3),0
    
    DO samp = 1,medNumSamp
       DO s = 1,numSurf
        DO i = 1,cutRaw(samp,s)%numNodes
            WRITE(1000+k,'(3E15.6,I10)') cutRaw(samp,s)%Pt(1,i),cutRaw(samp,s)%Pt(2,i),cutRaw(samp,s)%Pt(3,i),s
        END DO
       END DO
    END DO

    !Now to fit polynomial and resample
    DO samp = 1,medNumSamp
       DO s = 1,numSurf
        !First allocate arrays
        ALLOCATE(vx(cutRaw(samp,s)%numNodes))
        ALLOCATE(vy(cutRaw(samp,s)%numNodes))

        DO ie = 1,cutRaw(samp,s)%numNodes

           vx(ie) = cutRaw(samp,s)%Pt(iDim,ie)
           vy(ie) = cutRaw(samp,s)%Pt(jDim,ie)

        END DO
        !Check that we have varying nodes
        loc = MINLOC(vx)
        mini = loc(1)
        loc = MAXLOC(vx)
        maxi = loc(1)  
        IF ((cutRaw(samp,s)%numNodes.GT.1).AND.(maxi.NE.mini)) THEN
          IF ((MAXVAL(vx)-MINVAL(vx)).LT.(MAXVAL(vy)-MINVAL(vy))) THEN
            loc = MINLOC(vy)
            mini = loc(1)
            loc = MAXLOC(vy)
            maxi = loc(1)      
          END IF
          !...and the start and end point using maxloc    
          L1(1) = vx(maxi)
          L1(2) = vy(maxi)
        
          L2(1) = vx(mini)
          L2(2) = vy(mini)


          !... the vectors of this line
          V1(:) = L2(:) - L1(:)
          V1(:) = V1(:) / DSQRT(V1(1)**2 + V1(2)**2)
          V2(1) = -1.0d0*V1(2)
          V2(2) = V1(1)


            
          !Now to transform the coordinates
          DO ie = 1,cutRaw(samp,s)%numNodes

            !1) Find the projection onto the line
            P(1) = vx(ie)
            P(2) = vy(ie)
            
                
            !2) Convert this into the new coordiate system
            vx(ie) = (P(1) - L1(1))*V1(1) + (P(2) - L1(2))*V1(2)
            vy(ie) = (P(1) - L1(1))*V2(1) + (P(2) - L1(2))*V2(2)
                  
          END DO

          !Now to continue increasing the order of the polynomial
          !until the error is reduced 
          dm = 0
          completeFlag = 0
          DO WHILE (completeFlag.EQ.0)
            !Increase dm
            dm = dm + 1
            ALLOCATE(polyCoef(dm+1))
            !Now do the real fit
            polyCoef = polyfit(vx,vy,dm)
            sumError = 0.0d0
            DO ie = 1,cutRaw(samp,s)%numNodes
              yi = 0.0d0
              DO o = 1,dm+1
                yi =yi + polyCoef(o)*(vx(ie)**REAL(o-1))
              END DO
              sumError = sumError + ABS(yi-vy(ie))
            END DO
            sumError = sumError/REAL(cutRaw(samp,s)%numNodes)
            IF ((sumError.LE.medError).OR.(dm.EQ.medOrder)) THEN
              completeFlag = 1  
            ELSE
              deallocate(polyCoef)
            END IF

          END DO

          !Now sample
          DO sa = 1,medNumSamp


            P(1) = MINVAL(vx) + REAL(sa-1)*(MAXVAL(vx)-MINVAL(vx))/REAL(medNumSamp-1)
            P(2) = 0.0d0
            DO o = 1,dm+1
              P(2) = P(2) + polyCoef(o)*(P(1)**REAL(o-1))
            END DO
            !Now to convert back

            

            IF (sa.EQ.1) THEN
              P3(iDim) = L1(1)
              P3(jDim) = L1(2)
              P3(pDim) = cut(samp,s)
            ELSE IF (sa.EQ.medNumSamp) THEN
              P3(iDim) = L2(1)
              P3(jDim) = L2(2)
              P3(pDim) = cut(samp,s)
            ELSE
              P3(iDim) = L1(1) + P(1)*V1(1) + P(2)*V2(1)
              P3(jDim) = L1(2) + P(1)*V1(2) + P(2)*V2(2)
              P3(pDim) = cut(samp,s)
            END IF
            CALL Point3dQueue_Push(cuts(samp,s), P3)
            


          END DO
          deallocate(polyCoef)


        END IF

        DEALLOCATE(vx)
        DEALLOCATE(vy)

       END DO
    END DO

    DO samp = 1,medNumSamp
       DO s = 1,numSurf
        DO i = 1,cuts(samp,s)%numNodes
            WRITE(2000+k,'(3E15.6,I10)') cuts(samp,s)%Pt(1,i),cuts(samp,s)%Pt(2,i),cuts(samp,s)%Pt(3,i),s
        END DO
       END DO
    END DO


    DO samp = 1,medNumSamp
       DO s = 1,numSurf
         CALL Point3dQueue_Clear(cutRaw(samp,s))
         CALL Point3dQueue_Clear(cuts(samp,s))
       END DO
    END DO
    

  END DO

  contains


function polyfit(vx, vy, d)
    implicit none
    integer, intent(in)                   :: d
    integer, parameter                    :: dp = selected_real_kind(15, 307)
    real(dp), dimension(d+1)              :: polyfit
    real(dp), dimension(:), intent(in)    :: vx, vy
 
    real(dp), dimension(:,:), allocatable :: X
    real(dp), dimension(:,:), allocatable :: XT
    real(dp), dimension(:,:), allocatable :: XTX
 
    integer :: i, j
 
    integer     :: n, lda, lwork
    integer :: info
    integer, dimension(:), allocatable :: ipiv
    real(dp), dimension(:), allocatable :: work
 
    n = d+1
    lda = n
    lwork = n
 
    allocate(ipiv(n))
    allocate(work(lwork))
    allocate(XT(n, size(vx)))
    allocate(X(size(vx), n))
    allocate(XTX(n, n))
 
    ! prepare the matrix
    do i = 0, d
       do j = 1, size(vx)
          X(j, i+1) = vx(j)**i
       end do
    end do
 
    XT  = transpose(X)
    XTX = matmul(XT, X)
 
    ! calls to LAPACK subs DGETRF and DGETRI
    call DGETRF(n, n, XTX, lda, ipiv, info)
    if ( info /= 0 ) then
       print *, "problem"
       return
    end if
    call DGETRI(n, XTX, lda, ipiv, work, lwork, info)
    if ( info /= 0 ) then
       print *, "problem"
       return
    end if
 
    polyfit = matmul( matmul(XTX, XT), vy)
 
    deallocate(ipiv)
    deallocate(work)
    deallocate(X)
    deallocate(XT)
    deallocate(XTX)
 
end function



END SUBROUTINE



!*******************************************************************************
!>
!!  Medial Axis Fit
!!  
!!  
!<
!*******************************************************************************




SUBROUTINE Medial_Axis_Fit()

 
  USE common_Constants
  USE common_Parameters
  USE Queue
  USE Plane3DGeom
  USE SurfaceCurvatureManager
  IMPLICIT NONE

  INTEGER :: noOfRegions
  INTEGER, ALLOCATABLE :: nsreg(:), opreg(:,:)
  REAL*8, ALLOCATABLE  :: regMax(:,:), regMin(:,:)
  TYPE(IntQueueType), ALLOCATABLE :: lreg(:)

  INTEGER :: S, ie, je, ke, numCuts, k
  REAL*8  :: zmin(3,Surf%NB_Surf + Surf%NB_Sheet), zmax(3,Surf%NB_Surf + Surf%NB_Sheet)
  INTEGER :: ip

  INTEGER ::  c
  TYPE (Point3dQueueType) ::  cutRaw
  TYPE (Point3dQueueType), ALLOCATABLE :: cuts(:)
  REAL*8  :: cut
  TYPE(Plane3D)  :: aPlane

  INTEGER :: it1, it2, ip1, ip2
  INTEGER :: doesCut

  INTEGER :: d  !Dimension of the polynomial
  REAL*8, ALLOCATABLE  :: polyCoef(:)  !Polynomial coefficients
  REAL*8, ALLOCATABLE  :: meanSpace(:)  !Mean spacing of each region
 
 
  REAL*8  :: L1(2), L2(2), V1(2), V2(2), P(2)

  REAL*8, ALLOCATABLE, dimension(:) :: vx,vy

  REAL*8 :: P1(3),P2(3),P3(3), t
  INTEGER :: sa,o

  CHARACTER(len=3) :: fileNum

  INTEGER :: pDim, id, crossDim(2), iDim, jDim, i
  REAL*8  :: maxRange
  INTEGER :: loc(1), maxi, mini

  INTEGER :: completeFlag
  REAL*8  :: sumError, yi

  TYPE(SurfaceCurvatureType) :: Curv
  INTEGER :: curvNum,iv,iu,iu2, iv2

  INTEGER :: regCon(Surf%NB_Surf + Surf%NB_Sheet,Surf%NB_Surf + Surf%NB_Sheet)
  INTEGER :: si, sj

  TYPE(Point3dQueueType) :: curveList((Surf%NB_Surf + Surf%NB_Sheet)*10) !SPW - this is a limit on the number of curves, may need changing
  TYPE(IntQueueType), ALLOCATABLE  :: tempIC(:)
  INTEGER :: nextN(Surf%NB_Point),lastN(Surf%NB_Point),inN(Surf%NB_Point)
  INTEGER :: np, start, j, numEdge, e, jee, kee, ne
  INTEGER :: traversing, currentI, R1, R2, looping, checkChange
  REAL*8  :: xl(2), drm, frac, z1, z2, r, d1, d2, spacingReg(Surf%NB_Surf + Surf%NB_Sheet)
  INTEGER :: keMin, uMin, vMin, numCuts2, iuu, ivv
  REAL*8  :: dist, minDist, threshold
  INTEGER, ALLOCATABLE :: corners(:,:), corners2(:,:)
  INTEGER :: numCorn, numCorn2
  
  INTEGER, ALLOCATABLE :: connectedList(:,:,:)
  INTEGER :: maxCuts, smoothLoop, globLoop

  TYPE(IntQueueType) :: nodesToSurf(Surf%NB_Point),nodesToNode(Surf%NB_Point),nodesToElem(Surf%NB_Point)
  REAL*8 :: surfNorm(3,Surf%NB_Point), newPosit(3,Surf%NB_Point)
  INTEGER :: isBound(Surf%NB_Point), flag,markSurfNode(Surf%NB_Point)

  REAL*8 :: pp2(3,2),pp3(3,3)
  INTEGER :: ntype
  REAL*8 :: weight(3)

  !CALL Coarsen_Mesh()

  !CALL Medial_Axis_ID()

  !Build the data structures
  CALL Surf_BuildNext(Surf)

  !Find which nodes are on ridges
  CALL Update_Topology(nodesToSurf,nodesToNode)
  isBound(:) = 0
  DO i = 1,Surf%NB_Point
  
    IF (nodesToSurf(i)%numNodes.GT.1) THEN
        isBound(i) = 1
    END IF
  END DO




  zmin(:,:) = HUGE(0.0d0)
  zmax(:,:) = -1.0d0 * HUGE(0.0d0)

  maxCuts = 0


  !Need to find the plane values to cut at so will need min and
  !max values for the mesh

  DO ie = 1,Surf%NB_Tri
    IF (Surf%IP_Tri(5,ie).LE.(Surf%NB_Surf + Surf%NB_Sheet)) THEN
        S = Surf%IP_Tri(5,ie)
        DO ip = 1,3         
        DO id = 1,3
                IF (Surf%Posit(id,Surf%IP_Tri(ip,ie)) .GT. zmax(id,S)) THEN
                    zmax(id,S) = Surf%Posit(id,Surf%IP_Tri(ip,ie))
                ELSE IF (Surf%Posit(id,Surf%IP_Tri(ip,ie)) .LT. zmin(id,S)) THEN
                    zmin(id,S) = Surf%Posit(id,Surf%IP_Tri(ip,ie))
                END IF
        END DO
        END DO
    END IF
  END DO


  Curv%NB_Region = Surf%NB_Sheet+Surf%NB_Surf
  ALLOCATE(Curv%Regions(Curv%NB_Region))
  ALLOCATE(meanSpace(Curv%NB_Region))

  DO S = 1,Surf%NB_Sheet+Surf%NB_Surf

    !Pick a dimension to do cuts in
    maxRange = 0.0d0

    DO id = 1,3
      IF ((zmax(id,S)-zmin(id,S)).GT.maxRange) THEN
        maxRange = (zmax(id,S)-zmin(id,S))
        pDim = id
      END IF
    END DO

    !Work out the other dimension directions
    i = 0
    DO id = 1,3
      IF (id.NE.pDim) THEN
        i = i+1
        crossDim(i) = id
      END IF    
    END DO
    iDim = MINVAL(crossDim)
    jDim = MAXVAL(crossDim)

    !Squeeze the range a little to avoid inersection problems
    r = (zmax(pDim,S)-zmin(pDim,S))
    z1 = 0.5d0*(zmax(pDim,S)+zmin(pDim,S)) - 0.5d0*r
    z2 = 0.5d0*(zmax(pDim,S)+zmin(pDim,S)) + 0.5d0*r
    zmax(pDim,S) = z2
    zmin(pDim,S) = z1

    WRITE(fileNum,'(I3.3)') S
    
   ! numCuts = MAX(CEILING(r/medSpace),2)
   ! maxCuts = MAX(numCuts,maxCuts)
     numCuts = medNumSamp

    ALLOCATE(cuts(numCuts))

    WRITE(*,*) 'Surface ',S, ' max Z ', zmax(pDim,S), ' min z ', zmin(pDim,S)
    
    meanSpace(S) = ((zmax(pDim,S)-zmin(pDim,S))/REAL(numCuts-1))

    DO c = 1,numCuts
        CALL Point3dQueue_Clear(cutRaw)

        cut = zmin(pDim,S) + (C-1)*((zmax(pDim,S)-zmin(pDim,S))/REAL(numCuts-1))

        P1(iDim) = 1
        P1(jDim) = 0
        P1(pDim) = cut

        P2(iDim) = 0
        P2(jDim) = 0
        P2(pDim) = cut

        P3(iDim) = 0
        P3(jDim) = 1
        P3(pDim) = cut

        aPlane = Plane3D_Build(P1,P2,P3)

        !Now to check the edges for intersection
        DO ie = 1,Surf%NB_Edge

            !Triangles connected
            it1 = Surf%ITR_Edge(1,ie)
            it2 = Surf%ITR_Edge(2,ie)

            IF ( (Surf%IP_Tri(5,it1).EQ.S).OR.(Surf%IP_Tri(5,it2).EQ.S) ) THEN

                !Now check to see if intersection occurs 
                
                ip1 = Surf%IP_Edge(1,ie)
                ip2 = Surf%IP_Edge(2,ie)

                P1(:) = Surf%Posit(:,ip1)
                P2(:) = Surf%Posit(:,ip2)

                P3(:) = Plane3D_Line_Plane_Cross(P1,P2,aPlane,t)

                IF ((t.LE.1).AND.(t.GE.0)) THEN

                    CALL Point3dQueue_Push(cutRaw, P3)
                    

                END IF

            END IF


        END DO

        !Now to fit a polynomial

      !First allocate arrays
      ALLOCATE(vx(cutRaw%numNodes))
      ALLOCATE(vy(cutRaw%numNodes))

      DO ie = 1,cutRaw%numNodes

        vx(ie) = cutRaw%Pt(iDim,ie)
        vy(ie) = cutRaw%Pt(jDim,ie)

      END DO

      !Check that we have varying nodes
      loc = MINLOC(vx)
      mini = loc(1)
      loc = MAXLOC(vx)
      maxi = loc(1)      


      IF ((cutRaw%numNodes.GT.1).AND.(maxi.NE.mini)) THEN

            
        IF ((MAXVAL(vx)-MINVAL(vx)).LT.(MAXVAL(vy)-MINVAL(vy))) THEN
          loc = MINLOC(vy)
          mini = loc(1)
          loc = MAXLOC(vy)
          maxi = loc(1)      
        END IF
            

            !...and the start and end point using maxloc    
            L1(1) = vx(maxi)
            L1(2) = vy(maxi)
        
            L2(1) = vx(mini)
            L2(2) = vy(mini)


            !... the vectors of this line
            V1(:) = L2(:) - L1(:)
            V1(:) = V1(:) / DSQRT(V1(1)**2 + V1(2)**2)
            V2(1) = -1.0d0*V1(2)
            V2(2) = V1(1)


            
            !Now to transform the coordinates
            DO ie = 1,cutRaw%numNodes

                !1) Find the projection onto the line
                P(1) = vx(ie)
                P(2) = vy(ie)
                
                
                !2) Convert this into the new coordiate system
                vx(ie) = (P(1) - L1(1))*V1(1) + (P(2) - L1(2))*V1(2)
                vy(ie) = (P(1) - L1(1))*V2(1) + (P(2) - L1(2))*V2(2)
                  
                
                

            END DO

        !Now to continue increasing the order of the polynomial
        !until the error is reduced 
        d = 0
        completeFlag = 0
        DO WHILE (completeFlag.EQ.0)
          !Increase d
          d = d + 1
          ALLOCATE(polyCoef(d+1))
          !Now do the real fit
          polyCoef = polyfit(vx,vy,d)
          sumError = 0.0d0
          DO ie = 1,cutRaw%numNodes
            yi = 0.0d0
            DO o = 1,d+1
              yi =yi + polyCoef(o)*(vx(ie)**REAL(o-1))
            END DO
            sumError = sumError + ABS(yi-vy(ie))
          END DO
          sumError = sumError/REAL(cutRaw%numNodes)
          IF ((sumError.LE.medError).OR.(d.EQ.medOrder)) THEN
            completeFlag = 1
            
          ELSE
            deallocate(polyCoef)
          END IF

        END DO

            

        !And sample (have checked the conversion of axes and it works)
        CALL Point3dQueue_Clear(cuts(c))
        DO sa = 1,numCuts
            P(1) = MINVAL(vx) + REAL(sa-1)*(MAXVAL(vx)-MINVAL(vx))/REAL(numCuts-1)
            P(2) = 0.0d0
            DO o = 1,d+1
              P(2) = P(2) + polyCoef(o)*(P(1)**REAL(o-1))
          END DO

 

            !Now to convert back

            

            IF (sa.EQ.1) THEN
              P3(iDim) = L1(1)
              P3(jDim) = L1(2)
              P3(pDim) = cut
            ELSE IF (sa.EQ.numCuts) THEN
              P3(iDim) = L2(1)
              P3(jDim) = L2(2)
              P3(pDim) = cut
            ELSE
              P3(iDim) = L1(1) + P(1)*V1(1) + P(2)*V2(1)
              P3(jDim) = L1(2) + P(1)*V1(2) + P(2)*V2(2)
              P3(pDim) = cut
            END IF

            CALL Point3dQueue_Push(cuts(c), P3)
            


        END DO



        CALL Point3dQueue_Clear(cutRaw)
        deallocate(polyCoef)

        ELSE

            !Here is where we will end up with a single point
            
            minDist = HUGE(0.0d0)
            DO i = 1,Surf%NB_Point
                DO j = 1,nodesToSurf(i)%numNodes
                    IF (nodesToSurf(i)%Nodes(j).EQ.S) THEN

                        dist = ABS(Surf%Posit(pDim,i) - cut)
                        IF (dist.LT.minDist) THEN
                            minDist = dist
                            P3(:) = Surf%Posit(:,i)
                            
                        END IF

                    END IF
                END DO
            END DO

            
            CALL Point3dQueue_Push(cuts(c), P3)
           


            CALL Point3dQueue_Clear(cutRaw)
        END IF
      deallocate(vx,vy)

    END DO

    !Region
    Curv%Regions(S)%ID       =  S
    Curv%Regions(S)%TopoType = 1
    Curv%Regions(S)%numNodeU = numCuts
    Curv%Regions(S)%numNodeV = numCuts
    ALLOCATE(Curv%Regions(S)%Posit(3,numCuts,numCuts))
    DO iu = 1,numCuts

      IF (iu.GT.1) THEN

        IF (cuts(iu)%numNodes.GT.1) THEN
            !Need to check the direction
            P1(:) = cuts(iu)%Pt(:,numCuts) - cuts(iu)%Pt(:,1)
            P2(:) = Curv%Regions(S)%Posit(:,iu-1,numCuts) - Curv%Regions(S)%Posit(:,iu-1,1)

            !Check in same general direction using dot product
            r = P1(1)*P2(1) + P1(2)*P2(2) + P1(3)*P2(3)


            IF (r.LT.0) THEN
             
              DO iv = 1,numCuts
               Curv%Regions(S)%Posit(:,iu,iv) = cuts(iu)%Pt(:,numCuts+1-iv)
              END DO
            ELSE
              DO iv = 1,numCuts
               Curv%Regions(S)%Posit(:,iu,iv) = cuts(iu)%Pt(:,iv)
              END DO
            END iF
        ELSE
            DO iv = 1,numCuts
                 Curv%Regions(S)%Posit(:,iu,iv) = cuts(iu)%Pt(:,1)
            END DO

        END IF
      ELSE

        !This means we have a single point but in reality we want numCuts points
        DO iv = 1,numCuts



            
          IF (cuts(iu)%numNodes.GT.1) THEN
            Curv%Regions(S)%Posit(:,iu,iv) = cuts(iu)%Pt(:,iv)
          ELSE
            Curv%Regions(S)%Posit(:,iu,iv) = cuts(iu)%Pt(:,1)
          END IF


        END DO
      END IF
    END DO

    !The first and last iu's are singularities
    iu = 1
    DO iv = 1,numCuts

        !Guess the position of the next node
        P1(:) = 2.0d0*Curv%Regions(S)%Posit(:,iu+1,iv) - Curv%Regions(S)%Posit(:,iu+2,iv) 

        !Now find the nearest node to this
        minDist = HUGE(0.0d0)
        DO i = 1,Surf%NB_Point
            DO j = 1,nodesToSurf(i)%numNodes
                IF (nodesToSurf(i)%Nodes(j).EQ.S) THEN

                    dist = Geo3D_Distance_SQ(Surf%Posit(:,i),P1)
                    IF (dist.LT.minDist) THEN
                        minDist = dist
                        P3(:) = Surf%Posit(:,i)
                        
                    END IF

                END IF
            END DO
        END DO

        Curv%Regions(S)%Posit(:,iu,iv) = P3(:)

    END DO

    iu = numCuts
    DO iv = 1,numCuts

        !Guess the position of the next node
        P1(:) = 2.0d0*Curv%Regions(S)%Posit(:,iu-1,iv) - Curv%Regions(S)%Posit(:,iu-2,iv) 

        !Now find the nearest node to this
        minDist = HUGE(0.0d0)
        DO i = 1,Surf%NB_Point
            DO j = 1,nodesToSurf(i)%numNodes
                IF (nodesToSurf(i)%Nodes(j).EQ.S) THEN

                    dist = Geo3D_Distance_SQ(Surf%Posit(:,i),P1)
                    IF (dist.LT.minDist) THEN
                        minDist = dist
                        P3(:) = Surf%Posit(:,i)
                        
                    END IF

                END IF
            END DO
        END DO

        Curv%Regions(S)%Posit(:,iu,iv) = P3(:)

    END DO

   


    CALL RegionType_BuildTangent(Curv%Regions(S))
    deallocate(cuts)
  END DO

  !Now to attempt stiching together nodes 
  DO S = medSurf1,Curv%NB_Region

        !Now to snap to other regions if needed
        DO iu = 1,numCuts
            DO iv = 1,numCuts
                IF ((iu.EQ.1).OR.(iv.EQ.1).OR.(iu.EQ.numCuts).OR.(iv.EQ.numCuts)) THEN
                    P1(:) = Curv%Regions(S)%Posit(:,iu,iv)
                    minDist = HUGE(0.0d0)
                    DO si = medSurf1,S
                        IF (si.NE.S) THEN
                            threshold = MAX(meanSpace(si),meanSpace(S))*0.5
                            threshold = threshold*threshold
                            DO iuu = 1,numCuts
                                DO ivv = 1,numCuts

                                  IF ((iuu.EQ.1).OR.(ivv.EQ.1).OR.(iuu.EQ.numCuts).OR.(ivv.EQ.numCuts)) THEN
                                    dist = Geo3D_Distance_SQ(P1,Curv%Regions(si)%Posit(:,iuu,ivv))
                                    IF (dist.LT.threshold) THEN
                                        IF (dist.LT.minDist) THEN
                                            minDist = dist
                                            Curv%Regions(S)%Posit(:,iu,iv) = Curv%Regions(si)%Posit(:,iuu,ivv)
                                        END IF
                                    END IF
                                  END IF

                                END DO
                            END DO

                        END iF
                    END DO
                END iF
            END DO
        END DO

        
        CALL RegionType_BuildTangent(Curv%Regions(S))
  END DO

  DO smoothLoop = 1,10
  !Now use the Furgoson patches to smooth the original surface mesh
    WRITE(*,*) 'Smoothing loop ',smoothLoop
      DO i = 1,Surf%NB_Point

        flag = 0  !This step checks for 
        DO si = 1,nodesToSurf(i)%numNodes
            S = nodesToSurf(i)%nodes(si)
            IF (S.LT.medSurf1) THEN
                flag = 1
            END IF
        END DO

        IF (flag.EQ.0) THEN

            !Going to work out the pull from connected nodes, then projected surfaces
            !Pull from connected points
            P1(:) = 0.0d0
            DO j = 1,nodesToNode(i)%numNodes
                P1(:) = P1(:) + Surf%Posit(:,nodesToNode(i)%nodes(j)) 
            END DO

            P1(:) = (P1(:)-Surf%Posit(:,i))/REAL(nodesToNode(i)%numNodes-1)
            
            P2(:) = 0.0d0
            DO si = 1,nodesToSurf(i)%numNodes
                S = nodesToSurf(i)%nodes(si)
                
                P(1) = 0.5*REAL(numCuts)
                P(2) = 0.5*REAL(numCuts)
                CALL RegionType_GetUVFromXYZ1(Curv%Regions(S), Surf%Posit(:,i), P, 0.001d0, 0, dist, P3)
                P2(:) = P2(:) + P3(:)
            END DO
            P3(:) = P2(:)/REAL(nodesToSurf(i)%numNodes)
            
            P3(:) = (P1(:)+P3(:))*0.5d0

            Surf%Posit(:,i) = Surf%Posit(:,i) + 0.1d0*(P3(:)-Surf%Posit(:,i))
                

            

        END IF

      END DO
  END DO

  CALL SurfaceMeshStorage_Output('smooth',6,Surf)



  

  IF (1.EQ.0) THEN
      !Now to update the surface curvature file
      ALLOCATE(Curv%Curves (Curv%NB_Curve) )
      DO i = 1,Curv%NB_Curve
        Curv%Curves(i)%ID       = i
        Curv%Curves(i)%TopoType = 1
        Curv%Curves(i)%numNodes = curveList(i)%numNodes 
        ALLOCATE(Curv%Curves(i)%Posit(3,Curv%Curves(i)%numNodes))
        DO ie = 1,Curv%Curves(i)%numNodes
         Curv%Curves(i)%Posit(:,ie) = curveList(i)%Pt(:,ie)
        END DO
      END DO

      DO i = 1,Curv%NB_Region
        Curv%Regions(i)%numCurve = tempIC(i)%numNodes
        ALLOCATE(Curv%Regions(i)%IC(Curv%NB_Curve))
        DO ie = 1,Curv%Regions(i)%numCurve
          Curv%Regions(i)%IC(ie) = tempIC(i)%Nodes(ie)
        END DO
      END DO
  END IF
  CALL SurfaceCurvature_Output('medial',6,Curv)

contains

SUBROUTINE Update_Topology(nodesToSurf,nodesToNode)
        USE common_Constants
        USE common_Parameters
        USE Queue
        IMPLICIT NONE
        TYPE(IntQueueType), INTENT(OUT) :: nodesToSurf(Surf%NB_Point)
        TYPE(IntQueueType), INTENT(OUT) :: nodesToNode(Surf%NB_Point)
        TYPE(IntQueueType) :: nodesToElem(Surf%NB_Point)
        REAL*8 :: normals(3,Surf%NB_Point)
        REAL*8 :: triNorm(3,Surf%NB_Tri)
        INTEGER :: ie,i,ip,ne, si,j,jp

        !First the triangles
        DO ie = 1,Surf%NB_Tri
            DO i = 1,3
                ip = Surf%IP_Tri(i,ie)
                IF (ip.GT.0) THEN
                    IF (.NOT.IntQueue_Contain(nodesToElem(ip),ie)) THEN
                        CALL IntQueue_Push(nodesToElem(ip),ie)
                    END IF
                END IF
            END DO
            
        END DO

        normals(:,:) = 0.0d0

        !...and the normals
        DO ip = 1,Surf%NB_Point
            ne = nodesToElem(ip)%numNodes
            IF (ne.GT.0) THEN
                DO i = 1,ne
                    
                    DO j = 1,3

                        jp = Surf%IP_Tri(j,nodesToElem(ip)%nodes(i))
                        IF (.NOT.IntQueue_Contain(nodesToNode(ip),jp)) THEN
                            CALL IntQueue_Push(nodesToNode(ip),jp)
                        END IF

                    END DO


                    si = Surf%IP_Tri(5,nodesToElem(ip)%nodes(i))
                    IF (.NOT.IntQueue_Contain(nodesToSurf(ip),si)) THEN
                        CALL IntQueue_Push(nodesToSurf(ip),si)
                    END IF
                END DO
                

            END IF
        END DO


    END SUBROUTINE



function polyfit(vx, vy, d)
    implicit none
    integer, intent(in)                   :: d
    integer, parameter                    :: dp = selected_real_kind(15, 307)
    real(dp), dimension(d+1)              :: polyfit
    real(dp), dimension(:), intent(in)    :: vx, vy
 
    real(dp), dimension(:,:), allocatable :: X
    real(dp), dimension(:,:), allocatable :: XT
    real(dp), dimension(:,:), allocatable :: XTX
 
    integer :: i, j
 
    integer     :: n, lda, lwork
    integer :: info
    integer, dimension(:), allocatable :: ipiv
    real(dp), dimension(:), allocatable :: work
 
    n = d+1
    lda = n
    lwork = n
 
    allocate(ipiv(n))
    allocate(work(lwork))
    allocate(XT(n, size(vx)))
    allocate(X(size(vx), n))
    allocate(XTX(n, n))
 
    ! prepare the matrix
    do i = 0, d
       do j = 1, size(vx)
          X(j, i+1) = vx(j)**i
       end do
    end do
 
    XT  = transpose(X)
    XTX = matmul(XT, X)
 
    ! calls to LAPACK subs DGETRF and DGETRI
    call DGETRF(n, n, XTX, lda, ipiv, info)
    if ( info /= 0 ) then
       print *, "problem"
       return
    end if
    call DGETRI(n, XTX, lda, ipiv, work, lwork, info)
    if ( info /= 0 ) then
       print *, "problem"
       return
    end if
 
    polyfit = matmul( matmul(XTX, XT), vy)
 
    deallocate(ipiv)
    deallocate(work)
    deallocate(X)
    deallocate(XT)
    deallocate(XTX)
 
end function

 

END SUBROUTINE
