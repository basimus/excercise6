!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!    Insert the boundary points.
!!    @param[in]   common_Parameters::Surf
!!    @param[in]   common_Parameters::Bound_Insert_Order
!!    @param[in]   common_Parameters::Background_Model
!!    @param[in]   common_Parameters::Stretched_Bound_Insert
!!    @param[out]  common_Parameters::IP_Tet
!!                           : mesh with box-hull and surface nodes.
!!    @param[out]  common_Parameters::Posit
!!                           : mesh with box-hull and surface nodes.
!< 
!*******************************************************************************
SUBROUTINE Boundary_Insert()

  USE common_Constants
  USE common_Parameters
  USE Geometry3D
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: i,nas
  INTEGER :: IP, IT, IB, ipp(4), ip1, ip2
  INTEGER :: ITmin, ITmax, Isucc, IPfail, Nfail, Itry
  REAL*8  :: v, dd
  INTEGER :: mxseg, maseg, js, ns, ipo, NEW
  INTEGER :: Mapping_Choose_Model_Save
  INTEGER, DIMENSION(:), ALLOCATABLE :: modep


  NB_Point    = Surf%NB_Point
  nallc_Point = NB_Point + nallc_increase
  nallc_Tet   = 6*NB_Point + nallc_increase
  CALL Allocate_Point( )
  CALL Allocate_Tet( )
  CALL allc_2Dpointer(IP_BD_Tri, 5, Surf%NB_Tri+12, 'IP_BD_Tri')

  DO i = 1,Surf%NB_Point
     Posit(1:3,i) = Surf%Posit(1:3,i)
  ENDDO
  DO i =1,Surf%NB_Tri
     IP_BD_Tri(1:5,i) = Surf%IP_Tri(1:5,i)
  ENDDO

  CALL Surface_Scale( )
  IF(Background_Model==-1) CALL Surface_Stretch()
  ALLOCATE(modep(Surf%NB_Point))

  IF(Bound_Insert_Order==1)THEN
     Scale_Point(1:Surf%NB_Point) = -Scale_Point(1:Surf%NB_Point)
     CALL quicksort(Surf%NB_Point,Scale_Point,modep)
     Scale_Point(1:Surf%NB_Point) = -Scale_Point(1:Surf%NB_Point)
  ELSE
     DO ip=1,Surf%NB_Point
        modep(ip) = ip
     ENDDO
  ENDIF

  !---- setup the initial domain, add nodes forming of a frame
  !---- calculate geometry of the frame 

  useStretch = .FALSE.
     Mapping_Choose_Model_Save = Mapping_Choose_Model
     IF(Stretched_Bound_Insert)THEN
        useStretch = .TRUE.
        Mapping_Choose_Model = 2
     ENDIF
  CALL Setup_Domain()

  CALL TetMeshStorage_CellBackup(NB_Tet, IP_Tet(:,1:NB_Tet), HullBackUp)
  CALL TetMeshStorage_NodeBackup(NB_Point, Posit, HullBackUp)

  NB_BD_Point = Surf%NB_Point + NB_Extra_Point
  NB_BD_Tri   = Surf%NB_Tri
  DO IB = 1, Surf%NB_Tri
     IP_BD_Tri(1:3, IB) = Surf%IP_Tri(1:3, IB) + NB_Extra_Point
  ENDDO


  !---- Insert boundary points

  IF(.NOT. CircumUpdated) CALL Get_Tet_Circum(0)
  IF(.NOT. VolumeUpdated) CALL Get_Tet_Volume(0)



  IPfail = 0
  Mark_Tet(1:NB_Tet) = 0
  Mark_Point(1:NB_Point) = 0
  DO i=1,Surf%NB_Point
     IP = modep(i) + NB_Extra_Point
     IF(Mark_BD_Point(IP)==-1) CYCLE
     Isucc = 1-Bound_Insert_Soft

     IF((Debug_Display>0 .and. mod(i,10000)==1) .or.   &
        (Debug_Display>1 .and. mod(i,1000 )==1) .or.   &
        (Debug_Display>2 .and. mod(i,100  )==1) .or. Debug_Display>4)THEN
        WRITE(*,'(a,I6,a,I6,a,I6,a,a$)') '  Point ',IP,' is the ',I,   &
                ' inserted point out of ',Surf%NB_Point,' surface points ', char(13)
     ENDIF
     CALL Insert_Point_Delaunay(IP,0,Isucc,0)

     IF(Isucc<=0)THEN
        IPfail = IPfail + 1
        Mark_Point(IPfail) = IP
     ENDIF
  ENDDO
  WRITE(*,*)' '

  !--- Remove those false tetrahedra

  CALL Remove_Tet()

  IF(IPfail>0)THEN

     !--- for Bound_Insert_Soft=1 case only

     IF(Debug_Display>3)THEN
        CALL Check_Next(-3)
     ENDIF

     !---- Use 12 boundary triangle from the cube

     DO Itry = 1,3
        WRITE(*,*)'Unhappy--- ',IPfail,' points fail to insert, try again'
        Nfail  = IPfail
        IPfail = 0
        Mark_Tet(1:NB_Tet) = 0
        DO i=1,Nfail
           Isucc = 0
           IF(Itry==3) Isucc = 1
           IP = Mark_Point(i)
           CALL Insert_Point_Delaunay(IP,0,Isucc,0)
           IF(Isucc<=0)THEN
              IPfail = IPfail + 1
              Mark_Point(IPfail) = IP
           ENDIF
        ENDDO

        CALL Remove_Tet()
        IF(IPfail==0) EXIT
     ENDDO

  ENDIF


  IF(IPfail>0)THEN
     WRITE(29,*)'Error--- : some points still can not be inserted'
     CALL Error_Stop ('Boundary_Insert')
  ENDIF

  IF(Bound_Insert_Swap>0)THEN
     DO IT = 1,NB_Tet
     DO i=1,4
     IF(NEXT_tet(i,IT)==-1)THEN
        NB_BD_Tri = NB_BD_Tri + 1
        ipp(1:3) = IP_Tet(iTri_Tet(1:3,i),IT)
        CALL sort_ipp_tri(ipp(1:3))
        IP_BD_Tri(1:3, NB_BD_Tri) = ipp(1:3)
        IP_BD_Tri(4:5, NB_BD_Tri) = 0
     ENDIF
     ENDDO
     ENDDO

     IF(Bound_Insert_Swap<=2)THEN
        CALL Swap3D(Bound_Insert_Swap)
     ELSE
        CALL Swap3D_BD()
     ENDIF
     CALL Next_Build()
     CircumUpdated = .FALSE.
     VolumeUpdated = .FALSE.

     NB_BD_Tri = Surf%NB_Tri
     CALL Surface_Edge_Clear()
  ENDIF

  Mapping_Choose_Model = Mapping_Choose_Model_Save

  !---- Check the volume
  IF(Debug_Display>2) CALL Check_Mesh_Geometry('Boundary_Insert')
  IF(Debug_Display>3)THEN
     CALL Check_Next(-3)

     Mark_Point(1:NB_Point) = 0
     DO IT=1,NB_Tet
        ipp(1:4) = IP_Tet(1:4,IT)
        Mark_Point(ipp(1:4)) = 1
     ENDDO
     DO IP=1,NB_Point
        IF(Mark_Point(IP)==0)THEN
           WRITE(29,*)'Error--- : a node lost: ',IP
           CALL Error_Stop ('Boundary_Insert')
        ENDIF
     ENDDO
  ENDIF


  mxseg = 0
  maseg = 1000
  Surf%NB_Seg = 0                !--- do not consider wire at the moment
  IF(Surf%NB_Seg==0) CALL allc_1Dpointer(IB_Surf_Seg, 1, 'IB_Surf_Seg')

  DEALLOCATE(modep)
  IF(ADTree_Search)THEN
     ADTree_Search = .FALSE.
     CALL ADTree_Clear(ADTreeForMesh)
  ENDIF

  RETURN
END SUBROUTINE Boundary_Insert

!*******************************************************************************
!>
!!    Associate boundary triangles with tetrahedral elements.     
!!    @param[in] K   = 1  build triangulation system by return
!!                        common_Parameters::Tri_BD_Tree.                    \n     
!!                   = 2  build triangle system and mark elements in or out
!!                        of surface by return common_Parameters::Mark_Tet (IT)
!!                        (=1, in; =2, out.)                                 \n
!!                   = 3  build triangle system and associate boundary     
!!                        triangles with tetrahedra.                         \n
!!                   =-3  associate boundary triangles with tetrahedra.      \n
!!                   = 4  search and associate boundary triangles by
!!                        common_Parameters::NEXT_Tet
!!                        (i.e. triangles associating with ony one element).
!!
!!   Reminder:  make sure the first common_Parameters::NB_BD_Point points
!!              are boundary points.
!<     
!*******************************************************************************
SUBROUTINE Boundary_Associate(K)

  USE common_Constants
  USE common_Parameters
  USE array_allocator
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: K
  INTEGER :: IT, IB, NB1, NB2, i, ipp(3), itnb, icon, nsurf, mold

  IF(k>0)THEN
     !--- build a triangulation system

     CALL NodeNetTree_clear(Tri_BD_Tree)
     CALL NodeNetTree_allocate(3, NB_BD_Point, NB_BD_Tri, Tri_BD_Tree)

     NB1 = 0
     nsurf = 0
     DO IB = 1,NB_BD_Tri
        nsurf = max(nsurf,IP_BD_Tri(5,IB))
        ipp(1:3) = IP_BD_Tri(1:3,IB)
        CALL NodeNetTree_SearchAdd(3,ipp, Tri_BD_Tree, NB2)
        IF(Debug_Display>0)THEN
           IF(Tri_BD_Tree%numNets/=IB)THEN
              WRITE(29,*)' Error--- : boundary triangles counting'
              WRITE(29,*)'   IB,NB1,NB2=',IB,Tri_BD_Tree%numNets,NB2
              CALL Error_Stop ('Boundary_Associate 1')
           ENDIF
           IF(K/=2 .AND. MAX(ipp(1),ipp(2),ipp(3)) > NB_BD_Point)THEN
              WRITE(29,*)' Error--- : boundary point beyond'
              WRITE(29,*)'   IB,ipp,NB_BD_Point=',IB,ipp(1:3),NB_BD_Point
              CALL Error_Stop ('Boundary_Associate 2')
           ENDIF
        ENDIF
     ENDDO

  ENDIF

  IF(k==2)THEN
     !--- mark those elements with boundary triangles

     Mark_Tet(1:NB_Tet) = 0

     DO IT = 1,NB_Tet
        IF(IP_Tet(4,IT)<=0) CYCLE
        DO i=1,4
           ipp(1:3) = IP_Tet(iTri_Tet(1:3,i),IT)
           CALL Boundary_Tri_Match(ipp(1),ipp(2),ipp(3),IB)
           IF(IB<=0) CYCLE
           IF(IP_BD_Tri(5,IB) > Surf%NB_Surf - Surf%NB_Sheet) CYCLE    !--- interface
           CALL sort_ipp_tri(ipp)

           mold = Mark_Tet(IT)
           IF(ipp(2)==IP_BD_Tri(2,IB))THEN
              !--- point into the tetrahedron              
              Mark_Tet(IT) = 1
           ELSE
              !--- point outside of the tetrahedron
              Mark_Tet(IT) = 2
           ENDIF
           IF(mold/=0 .AND. mold/=Mark_Tet(IT))THEN
                 WRITE(29,*)' Error--- : inside or out,,2?'
                 WRITE(29,*)'    IT,ipp,mark=',IT,IP_Tet(:,IT),Mark_Tet(IT)
                 WRITE(29,*)'    IB,ipp,    =',IB,IP_BD_Tri(:,IB)
                 WRITE(29,*)' Hint: Check the orientation, enclosure of the surface.'
                 WRITE(29,*)'       Check numbers of surfaces and sheets',Surf%NB_Surf, Surf%NB_Sheet
                 CALL Error_Stop ('Boundary_Associate 4')
           ENDIF
        ENDDO
     ENDDO

     !--- propagate
    
     DO
        icon = 0
        DO IT = 1,NB_Tet
           IF(IP_Tet(4,IT)<=0) CYCLE
           IF(Mark_Tet(IT)<=0) CYCLE    !--- including -2, -1, 0

           DO i=1,4
              ipp(1:3) = IP_Tet(iTri_Tet(1:3,i),IT)
              CALL Boundary_Tri_Match(ipp(1),ipp(2),ipp(3),IB)
              IF(IB>0)THEN
                 !--- do no pass boundary surface (not integrface)
                 IF(IP_BD_Tri(5,IB) <= Surf%NB_Surf - Surf%NB_Sheet) CYCLE
              ENDIF
              itnb = Next_Tet(i,IT)
              IF(itnb<=0) CYCLE
              IF(IP_Tet(4,itnb)<=0) CYCLE
              IF(Mark_Tet(itnb)==0)THEN
                 Mark_Tet(itnb) = Mark_Tet(IT)
                 icon = 1
              ELSE IF(ABS(Mark_Tet(itnb)) /= Mark_Tet(IT))THEN
                 WRITE(29,*)' Error--- : inside or out,,3?'
                 WRITE(29,*)'    IT,  ipp,mark=',IT,IP_Tet(:,IT),Mark_Tet(IT)
                 WRITE(29,*)'    itnb,ipp,mark=',itnb,IP_Tet(:,itnb),Mark_Tet(itnb)
                 WRITE(29,*)' Hint: Check the orientation, enclosure of the surface.'
                 WRITE(29,*)'       Check numbers of surfaces and sheets',Surf%NB_Surf, Surf%NB_Sheet
                 CALL Error_Stop ('Boundary_Associate 5')
              ENDIF
           ENDDO
           Mark_Tet(IT) = -Mark_Tet(IT)
        ENDDO
        IF(icon==0) EXIT
     ENDDO

     DO IT = 1,NB_Tet
        IF(IP_Tet(4,IT)<=0) CYCLE
        IF(Mark_Tet(IT)>=0)THEN
           WRITE(29,*)' Error--- : inside or out,,4?'
           WRITE(29,*)'    IT,ipp,mark=',IT,IP_Tet(:,IT),Mark_Tet(IT)
           WRITE(29,*)' Hint: Check the orientation, enclosure of the surface.'
           WRITE(29,*)'       Check numbers of surfaces and sheets',Surf%NB_Surf, Surf%NB_Sheet
           CALL Error_Stop ('Boundary_Associate 6')
        ENDIF
        Mark_Tet(IT) = -Mark_Tet(IT)
     ENDDO

  ENDIF

  IF(ABS(k)==3)THEN

     !--- Search element for boundary triangles

     IP_BD_Tri(4,1:NB_BD_Tri) = 0
     DO IB =1,NB_BD_Tri
        IF(IP_BD_Tri(5,IB) > Surf%NB_Surf - Surf%NB_Sheet)THEN
           !--- an interface triangle
           IP_BD_Tri(4,IB) = -1
        ENDIF
     ENDDO
     IF(NB_Extra_Point>0) RETURN

     DO IT = 1,NB_Tet
        IF(  MIN(IP_Tet(1,IT), IP_Tet(2,IT), IP_Tet(3,IT), IP_Tet(4,IT))   &
             > NB_BD_Point  ) CYCLE

        DO i=1,4
           ipp(1:3) = IP_Tet(iTri_Tet(1:3,i),IT)
           CALL Boundary_Tri_Match(ipp(1),ipp(2),ipp(3),IB)
           IF(IB<=0) CYCLE
           IF(IP_BD_Tri(5,IB) > Surf%NB_Surf - Surf%NB_Sheet) cycle
           CALL sort_ipp_tri(ipp)

           IF(ipp(2)==IP_BD_Tri(2,IB))THEN
              !--- point into the tetrahedron
              IF(IP_BD_Tri(4,IB)<=0)THEN
                 IP_BD_Tri(4,IB) = IT
              ELSE
                 WRITE(29,*)' Error--- two elements match on boundary triangle : '
                 WRITE(29,*)'    IB,ipp=',IB,IP_BD_Tri(:,IB)
                 WRITE(29,*)'    IT1,ipp=',IP_BD_Tri(4,IB),IP_Tet(:,IP_BD_Tri(4,IB))
                 WRITE(29,*)'    IT2,ipp=',IT,IP_Tet(:,IT)
                 CALL Error_Stop ('Boundary_Associate 7  ')
              ENDIF
           ELSE
              !--- point outside of the tetrahedron
              WRITE(29,*)' Error--- An element outside the domain, IT=',IT
              WRITE(29,*)'    IP_Tet=',IP_Tet(:,IT)
              WRITE(29,*)'    IB=',IB,'IP_BD_Tri=',IP_BD_Tri(:,IB)
              CALL Error_Stop ('Boundary_Associate 8  ')
           ENDIF

        ENDDO
     ENDDO

     IF(Debug_Display>3)THEN
        CALL Check_Boundary()
     ENDIF

  ENDIF

  IF(k==4)THEN

     !--- build a boundary triangulation system by searching those
     !    associating with only one element.

     CALL allc_2Dpointer(IP_BD_Tri, 5, 4*NB_Tet, 'IP_BD_Tri')

     NB2 = NB_BD_Tri
     DO it = 1,NB_Tet
        DO i=1,4
           IF(Next_Tet(i,it)>0) CYCLE
           ipp(1:3) = IP_Tet(iTri_Tet(1:3,i),it)

           IF(NB2>0)THEN
             CALL NodeNetTree_Search(3,ipp,Tri_BD_Tree,IB)
             IF(IB>0)THEN
                !--- an old triangle
                IP_BD_Tri(4,IB) = it
                CYCLE
             ENDIF
           ENDIF
           NB_BD_Tri = NB_BD_Tri + 1
           CALL sort_ipp_tri(ipp)
           IP_BD_Tri(1:3,NB_BD_Tri) = ipp(1:3)
           IP_BD_Tri(4,  NB_BD_Tri) = it
           IP_BD_Tri(5,  NB_BD_Tri) = nsurf + 1
        ENDDO
     ENDDO

     CALL allc_2Dpointer(IP_BD_Tri, 5, NB_BD_Tri, 'IP_BD_Tri')
     IF(NB2>0) CALL NodeNetTree_clear(Tri_BD_Tree)

  ENDIF

  RETURN
END SUBROUTINE Boundary_Associate

!*******************************************************************************
!>
!!    Check if three points form a boundary triangle.
!<
!*******************************************************************************
SUBROUTINE Boundary_Tri_Match(IP1,IP2,IP3,IB)

  USE common_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: IP1,IP2,IP3
  INTEGER, INTENT(OUT) :: IB
  INTEGER :: ip(3)

  ip(1:3) = (/IP1,IP2,IP3/)
  CALL NodeNetTree_search(3,ip,Tri_BD_Tree,IB)

  RETURN
END SUBROUTINE Boundary_Tri_Match

!*******************************************************************************
!>     
!!    Check common_Parameters::IP_BD_Edge .
!<     
!*******************************************************************************
SUBROUTINE Check_Boundary()

  USE common_Parameters
  IMPLICIT NONE

  INTEGER :: ib,it,i1,i2,ii

  DO ib=1,NB_BD_Tri
     it=IP_BD_Tri(4,ib)
     IF(it<=0)THEN
        IF(it<0 .and. IP_BD_Tri(5,IB) > Surf%NB_Surf - Surf%NB_Sheet)THEN
           !--- an interface triangle
           CYCLE
        ELSE
           WRITE(29,*)' '
           WRITE(29,*)' Error--- boundary triangle is not associated '
           WRITE(29,*)'    ib,IP_BD_Tri=',ib,IP_BD_Tri(:,ib)
           CALL Error_Stop ('Check_Boundary  ')
        ENDIF
     ENDIF

     ii=0
     DO i1=1,3
        DO i2=1,4
           IF(IP_BD_Tri(i1,ib)==IP_Tet(i2,it)) ii=ii+1
        ENDDO
     ENDDO

     IF(ii/=3)THEN
        WRITE(29,*)' Error--- '
        WRITE(29,*)'    ib,IP_BD_Tri=',ib,IP_BD_Tri(:,ib)
        WRITE(29,*)'    it,ip_Tet =',it,IP_Tet(:,it)
        CALL Error_Stop (' Check_Boundary 2 ')
     ENDIF
  ENDDO

  WRITE(*,*)'Pass a check for Boundary'


END SUBROUTINE Check_Boundary

!*******************************************************************************
!>
!!  @param[in]   common_Parameters::IP_BD_Tri
!!  @param[out]  common_Parameters::Surf
!<     
!*******************************************************************************
SUBROUTINE SurfaceRestore( )

  USE common_Parameters
  USE array_allocator
  IMPLICIT NONE

  INTEGER :: i

  Surf%NB_Tri   = NB_BD_Tri
  Surf%NB_Point = NB_BD_Point
  Surf%NB_Seg   = 0
  !Surf%NB_Sheet = 0

  CALL allc_2Dpointer(Surf%IP_Tri,  5, Surf%NB_Tri,   'SurfaceRestore')
  CALL allc_2Dpointer(Surf%Posit, 3, Surf%NB_Point, 'SurfaceRestore')

  DO i = 1,Surf%NB_Point
     Surf%Posit(1:3,i) = Posit(1:3,i)
  ENDDO

  Surf%NB_Surf = 0
  DO i =1,Surf%NB_Tri
     Surf%IP_Tri(1:5,i) = IP_BD_Tri(1:5,i)
     Surf%IP_Tri(4  ,i) = 0
     Surf%NB_Surf = MAX(Surf%NB_Surf, Surf%IP_Tri(5,i))
  ENDDO

END SUBROUTINE SurfaceRestore

!*******************************************************************************
!>
!!  Measure the surface to get min & max corners.
!!  @param[in]  common_Parameters::Surf
!!  @param[out] common_Parameters::XYZmin
!!  @param[out] common_Parameters::XYZmax
!<     
!*******************************************************************************
SUBROUTINE SurfaceMeasure( )

  USE common_Parameters
  IMPLICIT NONE

  INTEGER :: i, IP

  XYZmax(1:3) = Surf%Posit(1:3,1)
  XYZmin(1:3) = Surf%Posit(1:3,1)
  DO IP = 2,Surf%NB_Point
     DO i=1,3
        XYZmax(i) = MAX(XYZmax(i),Surf%Posit(i,IP))
        XYZmin(i) = MIN(XYZmin(i),Surf%Posit(i,IP))
     ENDDO
  ENDDO

END SUBROUTINE SurfaceMeasure

!*******************************************************************************
!>
!!  Move forward boundary nodes.
!!  Update common_Parameters::NB_BD_Point, common_Parameters::Posit,
!!         common_Parameters::IP_BD_Tri, common_Parameters::IP_Tet    
!<     
!*******************************************************************************
SUBROUTINE Forward_BoundaryNodes( )

  USE common_Parameters
  IMPLICIT NONE

  INTEGER :: NB1, NB2, ib

  Mark_Point(1:NB_Point) = 0
  DO ib=1,NB_BD_Tri
     Mark_Point(IP_BD_Tri(1:3,IB)) = -1
  ENDDO
  CALL Sort_Nodes(NB1,NB2)
  NB_BD_Point = NB1

END SUBROUTINE Forward_BoundaryNodes


