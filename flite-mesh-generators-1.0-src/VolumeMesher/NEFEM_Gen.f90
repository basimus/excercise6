!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Module containing definitions of data types for NEFEM.
!<
MODULE NEFEM_Data
	
	IMPLICIT NONE

	!> NURBs Edge
	TYPE :: NURBSEdge
		!> On intersection curve or not...
		LOGICAL :: onCurve = .FALSE.
		!> Curve ID
		INTEGER :: curveId
		!> Start and end curve parameter
		REAL*8  :: c1, c2
		!> Surface ID
		INTEGER :: surfaceId
		!> Start and end parameters
		REAL*8  :: u1, u2, v1, v2
		!> The position of the nodes in the local element
		INTEGER :: IP_Local(2)
		!> Physical position of the nodes
		REAL*8 :: Posit(3,2)
	END TYPE NURBSEdge

	!> NURBS Face
	TYPE :: NURBSFace	
		!> The position of the nodes in the local element
		INTEGER :: IP_Local(3)
		!> Array containing the edges
		TYPE(NURBSEdge) :: edges(3)
	END TYPE NURBSFace

	!> Integration element
	TYPE :: IntElement
		!> This is the base integration element type
		!> Either this has one NURBS edge or one NURBS face
		INTEGER :: NB_N_Face = 0, NB_N_Edge = 0
		TYPE(NURBSFace) :: face
		TYPE(NURBSEdge) :: edge
		!> Physical position of all the nodes in this element
		REAL*8:: Posit(3,4)
		!> If an edge is nurbs or not
		INTEGER :: isNURBEdge(6)
		!> If an face is nurbs or not 
		INTEGER :: isNURBFace(4)
	END TYPE IntElement

	!> NURBS Element
	TYPE :: NURBSElement
		!> Number of integration elements in this element
		INTEGER :: NB_Int_Elem = 0
		!> Is the element valid
		LOGICAL :: isValid = .FALSE.
		TYPE(IntElement), ALLOCATABLE :: intelem(:)
	END TYPE NURBSElement


END MODULE NEFEM_Data




!*******************************************************************************
!>
!! 
!! Generates mesh for NURBS Enhanced Finite ELement Method   
!<       
!*******************************************************************************
SUBROUTINE NEFEM_Gen()

	USE common_Parameters
	USE SpacingStorage
	USE SurfaceMeshStorage
	USE SurfaceMeshManager
	USE PointAssociation
	USE SurfaceCurvatureManager
	USE SurfaceCurvature
	USE Queue
	USE Geometry2D
	USE kdtree2_precision_module
    USE kdtree2_module
    USE occt_fortran
	USE NEFEM_Data

	IMPLICIT NONE

	INTEGER :: e, ip1, ip2, IT, it1, it2, numEdgeDel, ic, ie, numAdjTri,j, ee, ie1, ie2, it3, ie3
	INTEGER :: finished, numEdgePot, N, row1, row2, row, numSwap
	INTEGER :: ipt1, ipt2, ipt3,ipt4, ip, ipt, ip3, ip4, ie1T(4), ie2T(4), it1T(3), it2T(3)
	INTEGER, ALLOCATABLE :: Mark_Tri(:)
	REAL*8  :: dtar, dact, P1(3), P2(3), P3(3), P4(3),eps, area, t, eps2,d32Tar, d14Tar, e32, e14
	REAL*8  :: m12(3),m23(3),m13(3),dist,minDist, uguess(2), u1(2), u2(2), u3(2), maxDist
	INTEGER :: collapseFlag, i, s, nbb, nb, maxEdge
	TYPE(IntQueueType) :: nodesToSurf(Surf%NB_Point), localSurfs, swapElements
	TYPE(Point3dQueueType) :: paraCoord(Surf%NB_Point)
	TYPE(IntQueueType), ALLOCATABLE :: PointAsso2(:)
	
	TYPE(SurfaceMeshStorageType) :: ActGeom, CurvedEdges, SubFaces, SurfVis, SurfVis1
	REAL*8 :: x21,x31,y21,y31,area2, d32, d14, a1(3), a2(3), areaT1a, areaT1b, areaT2a, areaT2b
	REAL*8 :: d12, d24,d43,d31,dTri
	INTEGER :: orderFlag, k, ee2
	REAL*8, ALLOCATABLE :: edgeLength(:)
	INTEGER, ALLOCATABLE :: edgeSort(:)
	INTEGER :: iFace, Vpt, count, boundEdge(2)
	TYPE(Plane3D) :: TriPlanes(Surf%NB_Tri)
	TYPE(Point3dQueueType) :: tempQueue, tempSub1, tempSub2, tempSub3, tempSub32, tempSub14
	TYPE(Point3dQueueType) :: tempSub12, tempSub24, tempSub43, tempSub31, tempSub23
	TYPE(Point3dQueueType) :: tempPara12, tempPara23, tempPara31
	TYPE(IntQueueType) :: tempSurf12, tempSurf23, tempSurf31 
	TYPE(IntQueueType) :: uniSurf12, uniSurf23, uniSurf31, ICtemp, uniSurf
	TYPE(IntQueueType), ALLOCATABLE :: tempSurfQueue(:)
	TYPE(Point3dQueueType), ALLOCATABLE :: subEdgeNodes(:), subEdgeParams(:)
	type(kdtree2), pointer :: tree
	INTEGER :: correltime, finished2, numTotSwap, swapFlag, iu, Isucc
	REAL*8 :: normals(3,Surf%NB_Point), as(5), segLen
	REAL*8, ALLOCATABLE :: ts(:), Pts(:,:)
	TYPE(Point3dQueueType), ALLOCATABLE :: CurvSamped(:)
	TYPE(Real8QueueType), ALLOCATABLE :: CurvUSamped(:)

	TYPE(IntQueueType), ALLOCATABLE :: CurvToSurf(:), SurfToSurf(:)
	INTEGER :: s1, s2, commEdge
	LOGICAL :: ex, ex2
	REAL*8  :: tMax, tMin, spmin, D, Scalar

	REAL*8  :: umax, umin, vmax, vmin, utemp, vtemp
	INTEGER :: cCount

	REAL*8  :: subFaceBar2(3,10), subFaceBar0(3,4), subFaceBar1(3,6)
	INTEGER :: subFaceIP2(3,9), subFaceIP0(3,3), subFaceIP1(3,4)

	INTEGER :: maxBreaks, startNode, IP_Elem(4)

	TYPE(NURBSElement), ALLOCATABLE :: NURBSiemAll(:)
	TYPE(NURBSElement) :: testNURB

	INTEGER :: subFlag

	subFaceBar0(:,1) = (/1.0,0.0,0.0/)
	subFaceBar0(:,2) = (/0.0,1.0,0.0/)
	subFaceBar0(:,3) = (/0.0,0.0,1.0/)
	subFaceBar0(:,4) = (/0.5,0.5,0.5/)

	subFaceIP0(:,1) = (/1,2,4/)
	subFaceIP0(:,2) = (/2,3,4/)
	subFaceIP0(:,3) = (/1,4,3/)

	subFaceBar1(:,1) = (/1.0,0.0,0.0/)
	subFaceBar1(:,2) = (/0.5,0.5,0.0/)
	subFaceBar1(:,3) = (/0.0,1.0,0.0/)
	subFaceBar1(:,4) = (/0.5,0.0,0.5/)
	subFaceBar1(:,5) = (/0.0,0.5,0.5/)
	subFaceBar1(:,6) = (/0.0,0.0,1.0/)

	subFaceIP1(:,1) = (/1,2,4/)
	subFaceIP1(:,2) = (/2,5,4/)
	subFaceIP1(:,3) = (/2,3,5/)
	subFaceIP1(:,4) = (/4,5,6/)
	


	subFaceBar2(:,1) = (/1.0,0.0,0.0/)
	subFaceBar2(:,2) = (/0.3,0.7,0.0/)
	subFaceBar2(:,3) = (/0.7,0.3,0.0/)
	subFaceBar2(:,4) = (/0.0,1.0,0.0/)
	subFaceBar2(:,5) = (/0.3,0.0,0.7/)
	subFaceBar2(:,6) = (/0.3,0.3,0.3/)
	subFaceBar2(:,7) = (/0.0,0.3,0.7/)
	subFaceBar2(:,8) = (/0.7,0.0,0.3/)
	subFaceBar2(:,9) = (/0.0,0.7,0.3/)
	subFaceBar2(:,10) = (/0.0,0.0,1.0/)

	subFaceIP2(:,1) = (/3,4,7/)
	subFaceIP2(:,2) = (/2,3,6/)
	subFaceIP2(:,3) = (/3,7,6/)
	subFaceIP2(:,4) = (/6,7,9/)
	subFaceIP2(:,5) = (/1,2,5/)
	subFaceIP2(:,6) = (/2,6,5/)
	subFaceIP2(:,7) = (/5,6,8/)
	subFaceIP2(:,8) = (/6,9,8/)
	subFaceIP2(:,9) = (/8,9,10/)



	!We need information on the surface definition so read in the geometry file
	IF (Curvature_Type==1) THEN
		WRITE(29,*) 'Reading '//JobName(1:JobNameLength)//'.dat'
        WRITE(*, *) 'Reading '//JobName(1:JobNameLength)//'.dat'
		CALL SurfaceCurvature_Input(JobName,JobNameLength,Curvt)
		CALL SurfaceCurvature_BuildTangent(Curvt)
     	NumCurves  = Curvt%NB_Curve
     	NumRegions = Curvt%NB_Region

    ELSE IF (Curvature_Type==4) THEN
    	!Now we are trying to load an igs or iges file

    	INQUIRE(file = JobName(1:JobNameLength)//'.igs', EXIST=ex)
    	IF (ex) THEN
    		WRITE(29,*) 'Reading '//JobName(1:JobNameLength)//'.igs'
           	WRITE(*, *) 'Reading '//JobName(1:JobNameLength)//'.igs'
           	CALL OCCT_LoadIGES(JobName(1:LEN_TRIM(JobName))//'.igs',t)
        ELSE
        	INQUIRE(file = JobName(1:JobNameLength)//'.iges', EXIST=ex2)
        	IF (ex2) THEN
        		WRITE(29,*) 'Reading '//JobName(1:JobNameLength)//'.iges'
	           	WRITE(*, *) 'Reading '//JobName(1:JobNameLength)//'.iges'
	           	CALL OCCT_LoadIGES(JobName(1:LEN_TRIM(JobName))//'.iges',t)

        	ELSE
        		CALL Error_Stop( ' Could not find igs or iges file'  )
        	END IF

        END IF

        !Count number of regions and curves
        CALL OCCT_GetNumCurves( NumCurves )
     	CALL OCCT_GetNumSurfaces( NumRegions )

	END IF

	!Sample the intersection curves
    ALLOCATE(CurvSamped(NumCurves+2*NumCurves), CurvToSurf(NumCurves+2*NumCurves), CurvUSamped(NumCurves+2*NumCurves))

    
    IF (Curvature_Type==1) THEN
	    !Each Curve in Curv has a %Segm
	    DO i = 1,NumCurves
	   		DO j = 1,Curvt%Curves(i)%numNodes-1
	    		CALL CubicSegment_Length(Curvt%Curves(i)%Segm(j),as)
	    		segLen = Curvt%Curves(i)%Segm(j)%Length
	    		N = INT(segLen/(1.d-2 *BGSpacing%MinSize))
	    		ALLOCATE(ts(N),Pts(3,N))
	    		CALL CubicSegment_EvenNodeSet(Curvt%Curves(i)%Segm(j),as,N,ts,Pts,Isucc)

	    		DO k = 1,N
	    			CALL Point3dQueue_Push(CurvSamped(i),Pts(:,k))
	    		END DO

	    		DEALLOCATE(ts,Pts)
	    	END DO
	    END DO

	ELSE IF (Curvature_Type==4) THEN
		!Sample each curve
		DO i = 1,NumCurves
			CALL OCCT_GetLineTBox(i,tMin,tMax)

			!First measure the length of the curve and find min spacing
			segLen = 0.0d0
			t = tMin
      		CALL OCCT_GetLineXYZFromT(i,t,P1)
      		CALL SpacingStorage_GetScale(BGSpacing, P1, Scalar)
      		spmin = Scalar * BGSpacing%BasicSize

      		

			DO j = 2,10
				t = tMin + (REAL(j-1)/(9.0d0))*(tMax-tMin)
      			CALL OCCT_GetLineXYZFromT(i,t,P2)
      			segLen = segLen + Geo3D_Distance(P1,P2)

      			P1 = P2
      			CALL SpacingStorage_GetScale(BGSpacing, P1, Scalar)
      			spmin = MIN(spmin, Scalar * BGSpacing%BasicSize)

      		END DO

      		!Now calculate the number of samples
      		N = INT(segLen/(1.d-2 * spmin))

      		DO j = 1,N
      			t = tMin + (REAL(j-1)/REAL(N-1))*(tMax-tMin)
      			CALL OCCT_GetLineXYZFromT(i,t,P1)

      			CALL Point3dQueue_Push(CurvSamped(i),P1)
      			CALL Real8Queue_Push(CurvUSamped(i),t)
      		END DO

		END DO
	END IF
	

	! !Check projections
	! DO i = 1,NumRegions
	! 	CALL OCCT_GetSurfaceUVBox(i,umin,vmin,umax,vmax)
	! 	WRITE(*,*)'Surface ', i, ' umin ',umin, ' umax', umax

	! 	utemp = umin + 0.5d0*(umax-umin)
	! 	vtemp = vmin + 0.5d0*(vmax-vmin)

	! 	CALL GetUVPointInfo0(i,utemp,vtemp,P1)

	! 	WRITE(*,*)'u= ',utemp, 'v= ',vtemp
	! 	WRITE(*,*)P1(1),P1(2),P1(3)

	! 	CALL ProjectToRegionFromXYZ(i, P1, uguess, TinySize, 1, t, P2)

	! 	WRITE(*,*) 'Proj dist=', t
	! 	WRITE(*,*) P2(1),P2(2),P2(3)
	! 	WRITE(*,*) uguess(1), uguess(2)

	! 	WRITE(*,*)'Reval'
	! 	CALL GetUVPointInfo0(i,uguess(1), uguess(2),P1)
	! 	WRITE(*,*) P2(1),P2(2),P2(3)

	! END DO

    !Need to build data structure for CurvSampled so we can get surface numbers from the curve
    !numbers, can do projections when needed....
    DO i = 1,NumRegions

    	CALL GetRegionCurveList(i, ICtemp)

    	DO j = 1,ICtemp%numNodes
    		k = ICtemp%Nodes(j)

    		CALL IntQueue_Push(CurvToSurf(k),i)
    	END DO

    END DO

   
    !This surface to surface info isn't complete, corners connect some surfaces
    !Need to introduce 'curves' which are actually just points or look at curves and make
    !sure all surrounding surfaces are added

    !We need to store the unmodified fine mesh for intersection tests
	CALL SurfaceMeshStorage_BasicCopy(Surf, ActGeom)
	CALL Surf_BuildEdge(ActGeom)
	CALL Surf_BuildTriAsso(ActGeom)
	CALL Surf_BuildPtAsso(ActGeom)
	CALL Surf_BuildEdgeAsso(ActGeom)

	cCount = NumCurves

    DO i = 1,NumCurves
    	!We only need to do this for the first and last node
    	P1(:) = CurvSamped(i)%Pt(:,1)
    	

    	!Find the node in the mesh which matches this point
    	minDist = HUGE(0.0d0)
    	DO ip = 1,ActGeom%NB_Point
    		t = Geo3D_Distance_SQ(ActGeom%Posit(1:3,ip),P1)
    		IF (t.LT.minDist) THEN
    			minDist = t
    			k = ip
    		END IF
    	END DO

    	!All surfaces connected to ip are connected to each other


    	!Now add the surfaces at ip to the curve list
    	cCount = cCount + 1
    	DO j = 1,ActGeom%ITR_Pt%JointLinks(k)%numNodes
			ie = ActGeom%ITR_Pt%JointLinks(k)%nodes(j)
			
			s = ActGeom%IP_Tri(5,ie)
			IF (.NOT.IntQueue_Contain(CurvToSurf(cCount),s)) THEN
				CALL IntQueue_Push(CurvToSurf(cCount),s)
				
			END IF

			CALL Point3dQueue_Push(CurvSamped(cCount),P1)

		END DO

		P1(:) = CurvSamped(i)%Pt(:,CurvSamped(i)%numNodes)
		

    	!Find the node in the mesh which matches this point
    	minDist = HUGE(0.0d0)
    	DO ip = 1,ActGeom%NB_Point
    		t = Geo3D_Distance_SQ(ActGeom%Posit(1:3,ip),P1)
    		IF (t.LT.minDist) THEN
    			minDist = t
    			k = ip
    		END IF
    	END DO


    	!Now add the surfaces at ip to the curve list
    	cCount = cCount + 1
    	DO j = 1,ActGeom%ITR_Pt%JointLinks(k)%numNodes
			ie = ActGeom%ITR_Pt%JointLinks(k)%nodes(j)
			
			s = ActGeom%IP_Tri(5,ie)
			IF (.NOT.IntQueue_Contain(CurvToSurf(cCount),s)) THEN
				CALL IntQueue_Push(CurvToSurf(cCount),s)
				
			END IF

			CALL Point3dQueue_Push(CurvSamped(cCount),P1)

		END DO


    END DO

    !Now we need to build neighbour information using the curve information

    ALLOCATE(SurfToSurf(NumRegions))

    DO i = 1,NumCurves+2*NumCurves
    	DO j = 1,CurvToSurf(i)%numNodes
    		s1 = CurvToSurf(i)%Nodes(j)
    		DO k = 1,CurvToSurf(i)%numNodes
    			s2 = CurvToSurf(i)%Nodes(k)
    			IF (.NOT.IntQueue_Contain(SurfToSurf(s1),s2)) THEN
    				CALL IntQueue_Push(SurfToSurf(s1),s2)		
    			END IF
    		END DO
    	END DO
    END DO


    ! DO i = 1,NumRegions
    ! 	WRITE(*,*)'Surface ',i
    ! 	DO j = 1,SurfToSurf(i)%numNodes
    ! 		WRITE(*,*)SurfToSurf(i)%Nodes(j)
    ! 	END DO
    ! END DO



	correltime = 0

	eps = TinyVolume!
	eps2 = epsilon(0d0)
	WRITE(*,*) eps


	

	!Create a kdtree of nodes in the surface which will be used for edge intersection tests
	tree => kdtree2_create(ActGeom%Posit(1:3,1:ActGeom%NB_Point),sort=.false., &
		                   rearrange=.false.,NumPoints=ActGeom%NB_Point)
                
	!Need to make aPlane for each triangle
	DO ie = 1,ActGeom%NB_Tri
		P1(:) = Posit(:,ActGeom%IP_Tri(1,ie))
		P2(:) = Posit(:,ActGeom%IP_Tri(2,ie))
		P3(:) = Posit(:,ActGeom%IP_Tri(3,ie))
		TriPlanes(ie) = Plane3D_Build(P1,P2,P3)

	END DO

	normals(:,:) = 0.0d0
	
	!Need to calculate the parametric coordinates for each node on the mesh
	DO ip = 1,ActGeom%NB_Point
		DO i = 1,ActGeom%ITR_Pt%JointLinks(ip)%numNodes
			ie = ActGeom%ITR_Pt%JointLinks(ip)%nodes(i)

			!Add normal of this triangle
			normals(:,ip) = normals(:,ip) + TriPlanes(ie)%anor(:)

			s = ActGeom%IP_Tri(5,ie)
			IF (.NOT.IntQueue_Contain(nodesToSurf(ip),s)) THEN
				CALL IntQueue_Push(nodesToSurf(ip),s)

				!Now calculate the parametric coordinates here
				! CALL RegionType_fguess( Curv%Regions(s), Posit(:,ip), uguess )
				! CALL RegionType_GetUVFromXYZ1(Curv%Regions(s), Posit(:,ip), uguess, TinySize, &
				! 	                          1, t, P1)

				CALL ProjectToRegionFromXYZ(s, Posit(:,ip), uguess, TinySize, 1, t, P1)
				WRITE(2608,'(3E17.8,I7)')P1(1),P1(2),P1(3),s

				P1(:) = 0.0d0
				P1(1:2) = uguess(1:2)
				CALL Point3dQueue_Push(paraCoord(ip),P1)


			END IF

		END DO
		normals(:,ip) = normals(:,ip)/Geo3D_Distance(normals(:,ip))
	END DO


	

	!--Testing NURBS data structure
	!Build the visualisation mesh
	ALLOCATE(NURBSiemAll(NB_Tet))

	DO ie = 1,NB_Tet
		NURBSiemAll(ie) =  makeNURBSElement(IP_Tet(:,ie))
	END DO


	SurfVis1 = buildNURBSvis(NURBSiemAll)

	CALL SurfaceMeshStorage_OutputEnSight('SurfVis1',8,SurfVis1)

	DEALLOCATE(NURBSiemAll)

	WRITE(*,*)' '
  	WRITE(*,*)'---- Coarsening Mesh for NEFEM ----'


  	!We are going to loop around edges in the surface mesh and collapse when neccesary
  	DO IP=1,NB_Point
      CALL Get_Point_Mapping(IP)
   	ENDDO
  	

  	finished = 0

  	ALLOCATE(Mark_Tri(Surf%NB_Tri))

  	orderFlag = -1

  	ALLOCATE(PointAsso2(NB_Point))


  	!---Generatr and write out curved edges for testing
  	CALL Surf_BuildEdge(Surf)
	
	ALLOCATE(subEdgeNodes(Surf%NB_Edge))
	ALLOCATE(tempSurfQueue(Surf%NB_Edge))
	ALLOCATE(subEdgeParams(Surf%NB_Edge))

	CurvedEdges%NB_Point = 0

	DO e = 1,Surf%NB_Edge
		ip1 = Surf%IP_Edge(1,e)
		ip2 = Surf%IP_Edge(2,e)
		subEdgeNodes(e) = findSubEdges(ip1,ip2,subFlag,tempSurfQueue(e),subEdgeParams(e))

		IF (subFlag.LT.0) THEN
			WRITE(*,*) 'Invalid element before collapse'
		END IF

		CurvedEdges%NB_Point = CurvedEdges%NB_Point + subEdgeNodes(e)%numNodes
	END DO

	!Write out the sub edges

	!For visualisation output the edges as flat triangles
	CALL allc_Surf(CurvedEdges,CurvedEdges%NB_Point,CurvedEdges%NB_Point,CurvedEdges%NB_Point)

	row = 0
	ip = 0
	ie = 0
	DO e = 1,Surf%NB_Edge
		row1 = row+1

		 DO ee = 1,subEdgeNodes(e)%numNodes
		 	row = row + 1
		 	P1(:) = subEdgeNodes(e)%Pt(:,ee)
		 	WRITE(3006,'(5E20.10,I10)') P1(1),P1(2),P1(3), &
                 subEdgeParams(e)%Pt(1,ee), &
                 subEdgeParams(e)%Pt(2,ee), &
                 tempSurfQueue(e)%Nodes(ee)
		 END DO
		row2 = row
		WRITE(3007,*)row1,row2

		!Now to build the curved edge 'triangles'
		ip = ip + 1
		CurvedEdges%Posit(:,ip) = subEdgeNodes(e)%Pt(:,1)

		DO ee = 2,subEdgeNodes(e)%numNodes
			ip = ip + 1
			CurvedEdges%Posit(:,ip) = subEdgeNodes(e)%Pt(:,ee)

			ie = ie + 1
			CurvedEdges%IP_Tri(1,ie) = ip - 1
			CurvedEdges%IP_Tri(2,ie) = ip
			CurvedEdges%IP_Tri(3,ie) = ip

			CurvedEdges%IP_Tri(4,ie) = 0
			CurvedEdges%IP_Tri(5,ie) = tempSurfQueue(e)%Nodes(ee-1)

		END DO

	END DO
	CurvedEdges%NB_Tri = ie
	CurvedEdges%NB_Point = ip
	CurvedEdges%NB_Surf = Surf%NB_Surf

	CALL SurfaceMeshStorage_OutputEnSight('curvEdgesIni',12,CurvedEdges)

	DEALLOCATE(subEdgeNodes)
	DEALLOCATE(tempSurfQueue)
	DEALLOCATE(subEdgeParams)


  	DO WHILE (finished==0)

  		finished2 = 0
		numTotSwap = 0

		DO finished2=1,1
		
			CALL Next_Build()

			! IF (numEdgeDel==0) THEN
			! 	finished = 1
			! END IF

			!Now to swap edges
			!1) Build the data structure
			CALL Surf_BuildEdge(Surf)
			CALL Surf_BuildTriAsso(Surf)
			CALL Surf_BuildNext(Surf)
			CALL Surf_BuildEdgeAsso(Surf)
			!! Triangle Next_Tri(i,it) is opposite to node IP_Tri(i,it)

			ALLOCATE(subEdgeNodes(Surf%NB_Edge))

			DO e = 1,Surf%NB_Edge
				ip1 = Surf%IP_Edge(1,e)
				ip2 = Surf%IP_Edge(2,e)
				subEdgeNodes(e) = findSubEdges(ip1,ip2,subFlag)
				IF (subFlag.LT.0) THEN
					WRITE(*,*)'Invalid edge found'
				END IF		
			END DO

			!Build element to node info
			DO ipt = 1,NB_Point
				CALL IntQueue_Clear(PointAsso2(ipt))
			END DO

			DO ie = 1,NB_Tet
				DO e = 1,4
					ipt = IP_Tet(e,ie)
					IF (.NOT.IntQueue_Contain(PointAsso2(ipt),ie)) THEN
						CALL IntQueue_Push(PointAsso2(ipt),ie)
					END IF
				END DO
				
			END DO

			Mark_Tri(:) = 0
			numSwap = 0

			CALL Next_Build()
			!Loop around the triangles
			DO it1 = 1,Surf%NB_Tri
				IF (Mark_Tri(it1).EQ.0) THEN
					faceLoop1: DO i = 1,3

						it2 = Surf%Next_Tri(i,it1)

						IF (it2.GT.0) THEN

							IF (Mark_Tri(it2).EQ.0) THEN

								!Get the node numbers for the quad
								ip1 = Surf%IP_Tri(i,it1)
								ip2 = Surf%IP_Tri(iEdge_Tri(1,i),it1)
								ip3 = Surf%IP_Tri(iEdge_Tri(2,i),it1)

								DO j = 1,3
									IF ((Surf%IP_Tri(j,it2).NE.ip2).AND.(Surf%IP_Tri(j,it2).NE.ip3)) THEN

										ip4 = Surf%IP_Tri(j,it2)

									END IF
								END DO


								!Currently the edge is ip2--ip3 but it may be better to have ip1--ip4
								!Find the sub nodes and calculate the edge lengths

								!ip2--ip3
								commEdge = -1
								DO j = 1,Surf%IED_Pt%JointLinks(ip2)%numNodes
									ie = Surf%IED_Pt%JointLinks(ip2)%nodes(j)
									IF (IntQueue_Contain(Surf%IED_Pt%JointLinks(ip3),ie)) THEN
										commEdge = ie
									END IF
								END DO

								tempSub32 = subEdgeNodes(ie) !findSubEdges(ip2,ip3)


								!ip1--ip2
								commEdge = -1
								DO j = 1,Surf%IED_Pt%JointLinks(ip1)%numNodes
									ie = Surf%IED_Pt%JointLinks(ip1)%nodes(j)
									IF (IntQueue_Contain(Surf%IED_Pt%JointLinks(ip2),ie)) THEN
										commEdge = ie
									END IF
								END DO

								tempSub12 = subEdgeNodes(ie)!findSubEdges(ip1,ip2)

								!ip2--ip4
								commEdge = -1
								DO j = 1,Surf%IED_Pt%JointLinks(ip2)%numNodes
									ie = Surf%IED_Pt%JointLinks(ip2)%nodes(j)
									IF (IntQueue_Contain(Surf%IED_Pt%JointLinks(ip4),ie)) THEN
										commEdge = ie
									END IF
								END DO

								tempSub24 = subEdgeNodes(ie)!findSubEdges(ip2,ip4)

								!ip4--ip3
								commEdge = -1
								DO j = 1,Surf%IED_Pt%JointLinks(ip4)%numNodes
									ie = Surf%IED_Pt%JointLinks(ip4)%nodes(j)
									IF (IntQueue_Contain(Surf%IED_Pt%JointLinks(ip3),ie)) THEN
										commEdge = ie
									END IF
								END DO

								tempSub43 = subEdgeNodes(ie)!findSubEdges(ip4,ip3)

								!ip3--ip1
								commEdge = -1
								DO j = 1,Surf%IED_Pt%JointLinks(ip3)%numNodes
									ie = Surf%IED_Pt%JointLinks(ip3)%nodes(j)
									IF (IntQueue_Contain(Surf%IED_Pt%JointLinks(ip1),ie)) THEN
										commEdge = ie
									END IF
								END DO
								
								tempSub31 = subEdgeNodes(ie)!findSubEdges(ip3,ip1)

								tempSub14 = findSubEdges(ip1,ip4,subFlag)

								
								IF (subFlag.GT.0) THEN

									d32 = 0.0d0
									DO ee = 1,tempSub32%numNodes-1
										P1(:) = tempSub32%Pt(:,ee)
										P2(:) = tempSub32%Pt(:,ee+1)
										d32 = d32 + Geo3D_Distance_SQ(P1,P2)
									END DO
				
									d14 = 0.0d0
									DO ee = 1,tempSub14%numNodes-1
										P1(:) = tempSub14%Pt(:,ee)
										P2(:) = tempSub14%Pt(:,ee+1)
										d14 = d14 + Geo3D_Distance_SQ(P1,P2)
									END DO

									d12 = 0.0d0
									DO ee = 1,tempSub12%numNodes-1
										P1(:) = tempSub12%Pt(:,ee)
										P2(:) = tempSub12%Pt(:,ee+1)
										d12 = d12 + Geo3D_Distance_SQ(P1,P2)
									END DO

									d24 = 0.0d0
									DO ee = 1,tempSub24%numNodes-1
										P1(:) = tempSub24%Pt(:,ee)
										P2(:) = tempSub24%Pt(:,ee+1)
										d24 = d24 + Geo3D_Distance_SQ(P1,P2)
									END DO

									d43 = 0.0d0
									DO ee = 1,tempSub43%numNodes-1
										P1(:) = tempSub43%Pt(:,ee)
										P2(:) = tempSub43%Pt(:,ee+1)
										d43 = d43 + Geo3D_Distance_SQ(P1,P2)
									END DO

									d31 = 0.0d0
									DO ee = 1,tempSub31%numNodes-1
										P1(:) = tempSub31%Pt(:,ee)
										P2(:) = tempSub31%Pt(:,ee+1)
										d31 = d31 + Geo3D_Distance_SQ(P1,P2)
									END DO

									e32 = MIN( MIN(d12,d32,d31)/MAX(d12,d32,d31) , MIN(d32,d24,d43)/MAX(d32,d24,d43) )
									e14 = MIN( MIN(d12,d24,d14)/MAX(d12,d24,d14) , MIN(d14,d43,d31)/MAX(d14,d43,d31) )

									IF (e32.EQ.e14) THEN
										e14 = d14
										e32 = d32
									END IF

									IF (e14.GT.e32) THEN
										swapFlag = 1


										!Check areas of new and old triangles
										P1(:) = Posit(:,ip1)
										P2(:) = Posit(:,ip2)
										P3(:) = Posit(:,ip3)
										P4(:) = Posit(:,ip4)

										areaT1a = Geo3D_Triangle_Area(P1,P2,P3)
										areaT2a = Geo3D_Triangle_Area(P2,P4,P3)

										swapFlag = 1


										a1 = Geo3D_Cross_Product(P1,P2,P4)
										area = (a1(1)*a1(1) + a1(2)*a1(2) + a1(3)*a1(3))
										IF (area<eps) THEN
										  swapFlag = 0
										ELSE
										  areaT1b = 0.5d0*dsqrt(area)
										  IF (areaT1b<(TinySize)) THEN
											swapFlag = 0
										  END IF
										END IF

										
										a2 = Geo3D_Cross_Product(P1,P4,P3)
										area = (a2(1)*a2(1) + a2(2)*a2(2) + a2(3)*a2(3))
										IF (area<eps) THEN
										  swapFlag = 0
										ELSE
										  areaT2b = 0.5d0*dsqrt(area)
										  IF (areaT2b<(TinySize)) THEN
											swapFlag = 0
										  END IF
										END IF

										! IF (swapFlag.EQ.1) THEN
										! 	IF (ABS(areaT1a-areaT2a) .LT. ABS(areaT1b-areaT2b)) THEN
										! 	  swapFlag = 0
										! 	END IF
										! END IF	

									ELSE
										swapFlag = 0
									END IF					
								ELSE
									swapFlag = 0
								END IF


								!IF (d14.LT.d32) THEN
								IF (swapFlag.EQ.1) THEN
									!Potential swap

									!Need to find the elements connected to it1 and it2
									DO ic = 1,PointAsso2(ip1)%numNodes
										ie = PointAsso2(ip1)%Nodes(ic)
										IF (IntQueue_Contain(PointAsso2(ip2),ie).AND.IntQueue_Contain(PointAsso2(ip3),ie)) THEN
											ie1 = ie
										END IF
									END DO

									DO ic = 1,PointAsso2(ip4)%numNodes
										ie = PointAsso2(ip4)%Nodes(ic)
										IF (IntQueue_Contain(PointAsso2(ip2),ie).AND.IntQueue_Contain(PointAsso2(ip3),ie)) THEN
											ie2 = ie
										END IF
									END DO

									!Now need a list of elements which contain the edge ip3--ip2 but
									!not ip1 or ip4 ??

									CALL IntQueue_Clear(swapElements)

									DO ic = 1,PointAsso2(ip2)%numNodes
										ie = PointAsso2(ip2)%Nodes(ic)
										IF (IntQueue_Contain(PointAsso2(ip3),ie).AND.(.NOT.IntQueue_Contain(PointAsso2(ip1),ie)).AND. &
											(.NOT.IntQueue_Contain(PointAsso2(ip4),ie))) THEN
											IF (.NOT.IntQueue_Contain(swapElements,ie)) THEN
												CALL IntQueue_Push(swapElements,ie)
											END IF
										END IF

									END DO



									collapseFlag = 0
									DO iFace = 1,4
										IF (NEXT_Tet(iFace,ie1).EQ.ie2) THEN
											collapseFlag = 1
										END IF
									END DO


									IF (swapElements%numNodes.GT.0) THEN
										collapseFlag = 0
									END IF



									IF (collapseFlag.EQ.1) THEN
										!First check volumes
										ipt1 = IP_Tet(1,ie1)
										ipt2 = IP_Tet(2,ie1)
										ipt3 = IP_Tet(3,ie1)
										ipt4 = IP_Tet(4,ie1)
										IF (ipt1==ip2) ipt1=ip4
										IF (ipt2==ip2) ipt2=ip4
										IF (ipt3==ip2) ipt3=ip4
										IF (ipt4==ip2) ipt4=ip4
										P1(:) = Posit(:,ipt1)
										P2(:) = Posit(:,ipt2)
										P3(:) = Posit(:,ipt3)
										P4(:) = Posit(:,ipt4)
										area = Geo3D_Tet_Volume(P1,P2,P3,P4)
										IF (area<eps) THEN
										  collapseFlag = 0
										END IF
									END IF

									IF (collapseFlag.EQ.1) THEN
										ipt1 = IP_Tet(1,ie2)
										ipt2 = IP_Tet(2,ie2)
										ipt3 = IP_Tet(3,ie2)
										ipt4 = IP_Tet(4,ie2)
										IF (ipt1==ip3) ipt1=ip1
										IF (ipt2==ip3) ipt2=ip1
										IF (ipt3==ip3) ipt3=ip1
										IF (ipt4==ip3) ipt4=ip1
										P1(:) = Posit(:,ipt1)
										P2(:) = Posit(:,ipt2)
										P3(:) = Posit(:,ipt3)
										P4(:) = Posit(:,ipt4)
										area = Geo3D_Tet_Volume(P1,P2,P3,P4)
										IF (area<eps) THEN
										  collapseFlag = 0
										END IF
									END IF

									IF (collapseFlag.EQ.1) THEN
										DO ic = 1,swapElements%numNodes
											ie = swapElements%Nodes(ic)
											ipt1 = IP_Tet(1,ie)
											ipt2 = IP_Tet(2,ie)
											ipt3 = IP_Tet(3,ie)
											ipt4 = IP_Tet(4,ie)
											IF (ipt1==ip3) ipt1=ip1
											IF (ipt2==ip3) ipt2=ip1
											IF (ipt3==ip3) ipt3=ip1
											IF (ipt4==ip3) ipt4=ip1
											IF (ipt1==ip2) ipt1=ip4
											IF (ipt2==ip2) ipt2=ip4
											IF (ipt3==ip2) ipt3=ip4
											IF (ipt4==ip2) ipt4=ip4
											P1(:) = Posit(:,ipt1)
											P2(:) = Posit(:,ipt2)
											P3(:) = Posit(:,ipt3)
											P4(:) = Posit(:,ipt4)
											area = Geo3D_Tet_Volume(P1,P2,P3,P4)
											IF (area<eps) THEN
											  collapseFlag = 0
											END IF

										END DO
									END IF

									IF (collapseFlag.EQ.1) THEN
										ipt1 = IP_Tet(1,ie1)
										ipt2 = IP_Tet(2,ie1)
										ipt3 = IP_Tet(3,ie1)
										ipt4 = IP_Tet(4,ie1)
										IF (ipt1==ip2) ipt1=ip4
										IF (ipt2==ip2) ipt2=ip4
										IF (ipt3==ip2) ipt3=ip4
										IF (ipt4==ip2) ipt4=ip4

										IP_Elem(1) = ipt1
										IP_Elem(2) = ipt2
										IP_Elem(3) = ipt3
										IP_Elem(4) = ipt4

										count = 0
										DO ic = 1,4
											IF (IP_Elem(ic).LE.NB_BD_Point) THEN
											 count = count+1
											END IF
										END DO

										IF (count.GT.1) THEN
											testNURB = makeNURBSElement(IP_Elem)
											IF (.NOT.testNURB%isValid) THEN
												collapseFlag = 0
											END IF
										END IF

									END IF

									IF (collapseFlag.EQ.1) THEN
										ipt1 = IP_Tet(1,ie2)
										ipt2 = IP_Tet(2,ie2)
										ipt3 = IP_Tet(3,ie2)
										ipt4 = IP_Tet(4,ie2)
										IF (ipt1==ip3) ipt1=ip1
										IF (ipt2==ip3) ipt2=ip1
										IF (ipt3==ip3) ipt3=ip1
										IF (ipt4==ip3) ipt4=ip1

										IP_Elem(1) = ipt1
										IP_Elem(2) = ipt2
										IP_Elem(3) = ipt3
										IP_Elem(4) = ipt4

										count = 0
										DO ic = 1,4
											IF (IP_Elem(ic).LE.NB_BD_Point) THEN
											 count = count+1
											END IF
										END DO

										IF (count.GT.1) THEN
											testNURB = makeNURBSElement(IP_Elem)
											IF (.NOT.testNURB%isValid) THEN
												collapseFlag = 0
											END IF
										END IF
									END IF

									!Now swap if okay
									IF (collapseFlag.EQ.1) THEN
										DO iFace = 1,4
											IF (IP_Tet(iFace,ie1).EQ.ip2) THEN
												IP_Tet(iFace,ie1) = ip4
											END IF

											IF (IP_Tet(iFace,ie2).EQ.ip3) THEN
												IP_Tet(iFace,ie2) = ip1
											END IF
										END DO

										DO iFace = 1,3
											IF (Surf%IP_Tri(iFace,it1).EQ.ip2) THEN
												Surf%IP_Tri(iFace,it1) = ip4
											END IF

											IF (Surf%IP_Tri(iFace,it2).EQ.ip3) THEN
												Surf%IP_Tri(iFace,it2) = ip1
											END IF
										END DO

										

										Mark_Tri(it1) = 1
										Mark_Tri(it2) = 1

										DO iFace = 1,3
											Mark_Tri(Surf%Next_Tri(iFace,it1)) = 1
											Mark_Tri(Surf%Next_Tri(iFace,it2)) = 1
										END DO


										numSwap = numSwap + 1
										EXIT faceLoop1

									END IF


								END IF


							END IF
						END IF
					END DO faceLoop1
				END IF
			END DO

			WRITE(*,*)numSwap,' edges swapped'

			numTotSwap = numTotSwap + numSwap

			DEALLOCATE(subEdgeNodes)
			
		END DO



	  	!1) Build the data structure
		CALL Surf_BuildEdge(Surf)
		CALL Surf_BuildTriAsso(Surf)
		CALL Surf_BuildNext(Surf)

		DO ipt = 1,NB_Point
			CALL IntQueue_Clear(PointAsso2(ipt))
		END DO

		DO ie = 1,NB_Tet
			DO e = 1,4
				ipt = IP_Tet(e,ie)
				IF (.NOT.IntQueue_Contain(PointAsso2(ipt),ie)) THEN
					CALL IntQueue_Push(PointAsso2(ipt),ie)
				END IF
			END DO
			
		END DO


		!Find the sub edges
		ALLOCATE(subEdgeNodes(Surf%NB_Edge))
		DO e = 1,Surf%NB_Edge
			ip1 = Surf%IP_Edge(1,e)
			ip2 = Surf%IP_Edge(2,e)
			subEdgeNodes(e) = findSubEdges(ip1,ip2,subFlag)
			IF (subFlag.LT.0) THEN
				WRITE(*,*) 'Invalid edge found'
			END IF
		END DO


		!Record edge lengths
		ALLOCATE(edgeLength(Surf%NB_Edge))
		DO e = 1,Surf%NB_Edge
			edgeLength(e) = 0.0d0
			DO ee = 1,subEdgeNodes(e)%numNodes-1
				P1(:) = subEdgeNodes(e)%Pt(:,ee)
				P2(:) = subEdgeNodes(e)%Pt(:,ee+1)
				edgeLength(e) = edgeLength(e) + Geo3D_Distance(P1,P2)
			END DO
		END DO

		ALLOCATE(edgeSort(Surf%NB_Edge))
		CALL quicksort(Surf%NB_Edge,edgeLength,edgeSort)

		!WRITE(*,*)(edgeLength(edgeSort(1))),(edgeLength(edgeSort(Surf%NB_Edge)))

		Mark_Point(:) = 0
		Mark_Tri(:) = 0

		numEdgeDel = 0
		numEdgePot = 0

		orderFlag = -1*orderFlag

		DO ee = 1,Surf%NB_Edge
			e = edgeSort(ee)
			!Get edge nodes
			IF (orderFlag.GT.0) THEN
				ip1 = Surf%IP_Edge(1,e)
				ip2 = Surf%IP_Edge(2,e)
			ELSE
				ip1 = Surf%IP_Edge(2,e)
				ip2 = Surf%IP_Edge(1,e)
			END IF
			

			it1 = Surf%ITR_Edge(1,e)
			it2 = Surf%ITR_Edge(2,e)
			IF ((it1.GT.0).AND.(it2.GT.0)) THEN

				!This will result in ip1 being deleted, 

				IF (((Mark_Point(ip1)==0).AND.(Mark_Point(ip2)==0)) &
					.AND.((Mark_Tri(it1)==0).AND.(Mark_Tri(it2)==0))) THEN
					!Neither of these points have been deleted

					dtar = (BGSpacing%BasicSize*0.5*(Scale_Point(ip1)+Scale_Point(ip2)))
					dact = edgeLength(e)

					IF (dact<dtar) THEN
						!Now we try to collapse
						collapseFlag = 1
						numEdgePot = numEdgePot + 1


						!Check the area of the new triangles
						triLoop1: DO ie = 1,Surf%NB_Tri
							IF (Surf%IP_Tri(1,ie)==ip1 .OR. Surf%IP_Tri(2,ie)==ip1 .OR. Surf%IP_Tri(3,ie)==ip1 ) THEN
								IF (.NOT.(  Surf%IP_Tri(1,ie)==ip2 .OR. Surf%IP_Tri(2,ie)==ip2 .OR.   &
								         Surf%IP_Tri(3,ie)==ip2 )) THEN

									ipt1 = Surf%IP_Tri(1,ie)
									ipt2 = Surf%IP_Tri(2,ie)
									ipt3 = Surf%IP_Tri(3,ie)

									IF (ipt1==ip1) ipt1=ip2
									IF (ipt2==ip1) ipt2=ip2
									IF (ipt3==ip1) ipt3=ip2

									P1(:) = Posit(:,ipt1)
									P2(:) = Posit(:,ipt2)
									P3(:) = Posit(:,ipt3)

									a1 = Geo3D_Cross_Product(P1,P2,P3)
									area = (a1(1)*a1(1) + a1(2)*a1(2) + a1(3)*a1(3))
									IF (area<eps) THEN
										collapseFlag = 0
										EXIT triLoop1
									ELSE
										area = 0.5d0*dsqrt(area)
										IF (area<(TinySize)) THEN
											collapseFlag = 0
										 	EXIT triLoop1
										END IF
									END IF
								END IF
							END IF
						END DO triLoop1

						IF (collapseFlag==1) THEN
							!Check volume of new elements
							cellLoop1: DO ic = 1,PointAsso2(ip1)%numNodes
							  ie = PointAsso2(ip1)%Nodes(ic)
								IF(.NOT.(  IP_Tet(1,ie)==ip2 .OR. IP_Tet(2,ie)==ip2 .OR.   &
								         IP_Tet(3,ie)==ip2 .OR. IP_Tet(4,ie)==ip2     ))THEN
									ipt1 = IP_Tet(1,ie)
									ipt2 = IP_Tet(2,ie)
									ipt3 = IP_Tet(3,ie)
									ipt4 = IP_Tet(4,ie)

									

									IF (ipt1==ip1) ipt1=ip2
									IF (ipt2==ip1) ipt2=ip2
									IF (ipt3==ip1) ipt3=ip2
									IF (ipt4==ip1) ipt4=ip2

									P1(:) = Posit(:,ipt1)
									P2(:) = Posit(:,ipt2)
									P3(:) = Posit(:,ipt3)
									P4(:) = Posit(:,ipt4)

									area = Geo3D_Tet_Volume(P1,P2,P3,P4)

									IF (area<eps) THEN
									  collapseFlag = 0
									  EXIT cellLoop1
									END IF

								END IF

							END DO cellLoop1
						END IF

						!Check face--face intersection
						!This checks for intersection of the face with the geometry
						IF (collapseFlag==1) THEN
							!Now check for intersection
							cellLoop3:  DO ic = 1,PointAsso2(ip1)%numNodes
						  		ie = PointAsso2(ip1)%Nodes(ic)
								IF(.NOT.(  IP_Tet(1,ie)==ip2 .OR. IP_Tet(2,ie)==ip2 .OR.   &
								         IP_Tet(3,ie)==ip2 .OR. IP_Tet(4,ie)==ip2     ))THEN

									ipt1 = IP_Tet(1,ie)
									ipt2 = IP_Tet(2,ie)
									ipt3 = IP_Tet(3,ie)
									ipt4 = IP_Tet(4,ie)
									
									IF (ipt1==ip1) ipt1=ip2
									IF (ipt2==ip1) ipt2=ip2
									IF (ipt3==ip1) ipt3=ip2
									IF (ipt4==ip1) ipt4=ip2

									IP_Elem(1) = ipt1
									IP_Elem(2) = ipt2
									IP_Elem(3) = ipt3
									IP_Elem(4) = ipt4


									count = 0
									DO ipt = 1,4
										IF (IP_Elem(ipt).LE.NB_BD_Point) THEN
										 count = count+1
										END IF
									END DO

									IF (count.GT.1) THEN
										testNURB = makeNURBSElement(IP_Elem)
										IF (.NOT.testNURB%isValid) THEN
											collapseFlag = 0
											EXIT cellLoop3
										END IF
									END IF

									DO iFace = 1,4
										count = 0
										DO ipt = 1,3
											IF (IP_Tet(iTri_Tet(ipt,iFace),ie).LE.NB_BD_Point) THEN
											  count = count + 1
											END IF
										END DO

										IF (count.EQ.3) THEN
											Vpt = IP_Tet(iFace,ie)
											ipt1 = IP_Tet(iTri_Tet(1,iFace),ie)
											ipt2 = IP_Tet(iTri_Tet(2,iFace),ie)
											ipt3 = IP_Tet(iTri_Tet(3,iFace),ie)

											IF (ipt1==ip1) ipt1=ip2
											IF (ipt2==ip1) ipt2=ip2
											IF (ipt3==ip1) ipt3=ip2

											
											!TODO - Check distance of potential edges to see if it's too long
											tempSub12 = findSubEdges(ipt1,ipt2,subFlag)
											IF (subFlag.LT.0) THEN
												collapseFlag = 0
												EXIT cellLoop3
											END IF

											tempSub31 = findSubEdges(ipt3,ipt1,subFlag)
											IF (subFlag.LT.0) THEN
												collapseFlag = 0
												EXIT cellLoop3
											END IF

											tempSub32 = findSubEdges(ipt3,ipt2,subFlag)
											IF (subFlag.LT.0) THEN
												collapseFlag = 0
												EXIT cellLoop3
											END IF

											d12 = 0.0d0
											DO ee2 = 1,tempSub12%numNodes-1
												P1(:) = tempSub12%Pt(:,ee2)
												P2(:) = tempSub12%Pt(:,ee2+1)
												d12 = d12 + Geo3D_Distance(P1,P2)
											END DO

											d31 = 0.0d0
											DO ee2 = 1,tempSub31%numNodes-1
												P1(:) = tempSub31%Pt(:,ee2)
												P2(:) = tempSub31%Pt(:,ee2+1)
												d31 = d31 + Geo3D_Distance(P1,P2)
											END DO

											d32 = 0.0d0
											DO ee2 = 1,tempSub32%numNodes-1
												P1(:) = tempSub32%Pt(:,ee2)
												P2(:) = tempSub32%Pt(:,ee2+1)
												d32 = d32 + Geo3D_Distance(P1,P2)
											END DO

											dTri = (1.5d0/3.0d0)*BGSpacing%BasicSize*(Scale_Point(ipt1) &
												                      +Scale_Point(ipt2)+Scale_Point(ipt3))

											IF ((d12.GT.dTri).OR.(d31.GT.dTri).OR.(d32.GT.dTri)) THEN
												collapseFlag = 0
												EXIT cellLoop3										
											END IF

											


										END IF

									END DO


								END IF

							END DO cellLoop3
						END IF

						IF (collapseFlag==1) THEN

							DO ic = 1,PointAsso2(ip1)%numNodes
						  		ie = PointAsso2(ip1)%Nodes(ic)

								Mark_Point(IP_Tet(1:4,ie)) = 1

							    IF(  IP_Tet(1,ie)==ip2 .OR. IP_Tet(2,ie)==ip2 .OR.   &
							         IP_Tet(3,ie)==ip2 .OR. IP_Tet(4,ie)==ip2     )THEN
							       IP_Tet(4,ie) = -IP_Tet(4,ie)
							    ELSE
							       WHERE(IP_Tet(:,ie)==ip1) IP_Tet(:,ie) = ip2
							    ENDIF

							END DO

							DO ie = 1,Surf%NB_Tri
								IF (Surf%IP_Tri(1,ie)==ip1 .OR. Surf%IP_Tri(2,ie)==ip1 .OR. Surf%IP_Tri(3,ie)==ip1 ) THEN
									Mark_Tri(ie) = 1

									DO iFace = 1,3
										IF (Surf%Next_Tri(iFace,ie).GT.0) THEN
											IF (Surf%IP_Tri(1,Surf%Next_Tri(iFace,ie)).GT.0) THEN
											  Mark_Tri(Surf%Next_Tri(iFace,ie)) = 1
											  Mark_Point(Surf%IP_Tri(1:3,Surf%Next_Tri(iFace,ie))) = 1
											END IF
										END IF
									END DO

									Mark_Point(Surf%IP_Tri(1:3,ie)) = 1
									IF(  Surf%IP_Tri(1,ie)==ip2 .OR. Surf%IP_Tri(2,ie)==ip2 .OR.   &
								         Surf%IP_Tri(3,ie)==ip2     )THEN
								       Surf%IP_Tri(1,ie) = -Surf%IP_Tri(1,ie)

								    ELSE
								       WHERE(Surf%IP_Tri(:,ie)==ip1) Surf%IP_Tri(:,ie) = ip2
								       WHERE(IP_BD_Tri(:,ie)==ip1) IP_BD_Tri(:,ie)=ip2
								    ENDIF

								END IF
							END DO

							WHERE(Surf%IP_Edge(:,e)==ip1) Surf%IP_Edge(:,e)=ip2

							Mark_Point(ip1) = 1
							Mark_Point(ip2) = 1
							numEdgeDel = numEdgeDel + 1

						END IF

					END IF


				END IF
			END IF
			

		END DO

		DEALLOCATE(edgeSort,edgeLength,subEdgeNodes)

		WRITE(*,*)numEdgeDel,'/',numEdgePot,' edges collapsed'

		CALL Remove_Tet()
		
		nbb=0
	    DO nb =1,Surf%NB_Tri
	       IF(Surf%IP_Tri(1,nb)>0)THEN
	          nbb = nbb+1
	          Surf%IP_Tri(:,nbb) = Surf%IP_Tri(:,nb)
	          IP_BD_Tri(:,nbb) = IP_BD_Tri(:,nb)
	       ENDIF
	    ENDDO
	    Surf%NB_Tri   = nbb
	    NB_BD_Tri = nbb

		WRITE(*,*)Surf%NB_Tri,' triangles left'


		
		IF ((numEdgeDel.EQ.0).AND.(orderFlag.LT.0)) THEN
			finished = 1
		END IF

		CALL SurfaceMeshStorage_OutputEnSight('surfMerge',9,Surf)

			!1) Build the data structure
		CALL Surf_BuildEdge(Surf)
		CALL Surf_BuildTriAsso(Surf)
		CALL Surf_BuildNext(Surf)

		DO ipt = 1,NB_Point
			CALL IntQueue_Clear(PointAsso2(ipt))
		END DO

		DO ie = 1,NB_Tet
			DO e = 1,4
				ipt = IP_Tet(e,ie)
				IF (.NOT.IntQueue_Contain(PointAsso2(ipt),ie)) THEN
					CALL IntQueue_Push(PointAsso2(ipt),ie)
				END IF
			END DO
			
		END DO


		!Find the sub edges
		ALLOCATE(subEdgeNodes(Surf%NB_Edge))
		DO e = 1,Surf%NB_Edge
			ip1 = Surf%IP_Edge(1,e)
			ip2 = Surf%IP_Edge(2,e)
			subEdgeNodes(e) = findSubEdges(ip1,ip2,subFlag)
			IF (subFlag.LT.0) THEN
				WRITE(*,*) 'Invalid edge found'
			END IF
		END DO


		!Record edge lengths
		ALLOCATE(edgeLength(Surf%NB_Edge))
		DO e = 1,Surf%NB_Edge
			edgeLength(e) = 0.0d0
			DO ee = 1,subEdgeNodes(e)%numNodes-1
				P1(:) = subEdgeNodes(e)%Pt(:,ee)
				P2(:) = subEdgeNodes(e)%Pt(:,ee+1)
				edgeLength(e) = edgeLength(e) + Geo3D_Distance(P1,P2)
			END DO
		END DO

		ALLOCATE(edgeSort(Surf%NB_Edge))
		CALL quicksort(Surf%NB_Edge,edgeLength,edgeSort)

		WRITE(*,*)(edgeLength(edgeSort(1))),(edgeLength(edgeSort(Surf%NB_Edge)))


		


		DEALLOCATE(edgeSort,edgeLength,subEdgeNodes)


	END DO

	CALL Surf_BuildEdge(Surf)
	
	ALLOCATE(subEdgeNodes(Surf%NB_Edge))
	ALLOCATE(tempSurfQueue(Surf%NB_Edge))
	ALLOCATE(subEdgeParams(Surf%NB_Edge))

	CurvedEdges%NB_Point = 0

	DO e = 1,Surf%NB_Edge
		ip1 = Surf%IP_Edge(1,e)
		ip2 = Surf%IP_Edge(2,e)
		subEdgeNodes(e) = findSubEdges(ip1,ip2,subFlag,tempSurfQueue(e),subEdgeParams(e))
		CurvedEdges%NB_Point = CurvedEdges%NB_Point + subEdgeNodes(e)%numNodes
	END DO

	!Write out the sub edges

	!For visualisation output the edges as flat triangles
	CALL allc_Surf(CurvedEdges,CurvedEdges%NB_Point,CurvedEdges%NB_Point,CurvedEdges%NB_Point)

	row = 0
	ip = 0
	ie = 0
	DO e = 1,Surf%NB_Edge
		row1 = row+1

		 DO ee = 1,subEdgeNodes(e)%numNodes
		 	row = row + 1
		 	P1(:) = subEdgeNodes(e)%Pt(:,ee)
		 	WRITE(3006,'(5E20.10,I10)') P1(1),P1(2),P1(3),subEdgeParams(e)%Pt(1,ee),subEdgeParams(e)%Pt(2,ee),tempSurfQueue(e)%Nodes(ee)
		 END DO
		row2 = row
		WRITE(3007,*)row1,row2

		!Now to build the curved edge 'triangles'
		ip = ip + 1
		CurvedEdges%Posit(:,ip) = subEdgeNodes(e)%Pt(:,1)

		DO ee = 2,subEdgeNodes(e)%numNodes
			ip = ip + 1
			CurvedEdges%Posit(:,ip) = subEdgeNodes(e)%Pt(:,ee)

			ie = ie + 1
			CurvedEdges%IP_Tri(1,ie) = ip - 1
			CurvedEdges%IP_Tri(2,ie) = ip
			CurvedEdges%IP_Tri(3,ie) = ip

			CurvedEdges%IP_Tri(4,ie) = 0
			CurvedEdges%IP_Tri(5,ie) = tempSurfQueue(e)%Nodes(ee-1)

		END DO

	END DO
	CurvedEdges%NB_Tri = ie
	CurvedEdges%NB_Point = ip
	CurvedEdges%NB_Surf = Surf%NB_Surf

	CALL SurfaceMeshStorage_OutputEnSight('curvEdges',9,CurvedEdges)

	CALL SurfaceMeshStorage_OutputEnSight('surfMerge',9,Surf)

	!Build the visualisation mesh
	ALLOCATE(NURBSiemAll(NB_Tet))

	DO ie = 1,NB_Tet
		NURBSiemAll(ie) =  makeNURBSElement(IP_Tet(:,ie))
	END DO


	SurfVis = buildNURBSvis(NURBSiemAll)

	CALL SurfaceMeshStorage_OutputEnSight('SurfVis',7,SurfVis)





	


	CONTAINS




	FUNCTION findSubEdges(ipt1f,ipt2f,valid,subEdgeSurf,subEdgeParam) RESULT(subEdgeTrain)
		IMPLICIT NONE
		INTEGER, INTENT(IN) :: ipt1f, ipt2f
		TYPE(Point3dQueueType) :: subEdgeTrain
		TYPE(IntQueueType), OPTIONAL, INTENT(OUT) :: subEdgeSurf
		TYPE(Point3dQueueType), OPTIONAL, INTENT(OUT) :: subEdgeParam
		INTEGER, INTENT(OUT) :: valid
		INTEGER :: numSamp, Scommon, ifin, Sfin,jfin
		REAL*8 ::  P1f(3), P2f(3), UV1f(2), UV2f(2), UVf(2), Pf(3), PPf(3)
		REAL*8 :: Paramf(3), tfin, minDistf
		INTEGER :: cfin, curf, s1f, s2f, s1tf, s2tf, minNfin

		!This function finds sub edges between ipt1f and ipt2f
		valid = 1
		numSamp = 10

		!First clear the queue
		CALL Point3dQueue_Clear(subEdgeTrain)
		IF (present(subEdgeSurf)) THEN
			CALL IntQueue_Clear(subEdgeSurf)
		END IF
		IF (present(subEdgeParam)) THEN
			CALL Point3dQueue_Clear(subEdgeParam)
		END IF
		

		!Get positions
		P1f(:) = Posit(:,ipt1f)
		P2f(:) = Posit(:,ipt2f)

		!1) Check to see if we can just use the same surface number
		Scommon = -1
		DO ifin = 1,nodesToSurf(ipt1f)%numNodes
			Sfin = nodesToSurf(ipt1f)%Nodes(ifin)
			IF (IntQueue_Contain(nodesToSurf(ipt2f),Sfin)) THEN
				Scommon = Sfin
			END IF
		END DO

		IF (Scommon.GT.0) THEN
			!Now we can just straight line the parameter space
			!Find the correct position for the parametric coordinates
			ifin = IntQueue_Find(nodesToSurf(ipt1f),Scommon)
			UV1f(1:2) = paraCoord(ipt1f)%Pt(1:2,ifin)
			ifin = IntQueue_Find(nodesToSurf(ipt2f),Scommon)
			UV2f(1:2) = paraCoord(ipt2f)%Pt(1:2,ifin)

			CALL Point3dQueue_Push(subEdgeTrain,P1f)
			IF (present(subEdgeSurf)) THEN
				CALL IntQueue_Push(subEdgeSurf,Scommon)
			END IF
			IF (present(subEdgeParam)) THEN
				Paramf(1:2) = UV1f(1:2)
				CALL Point3dQueue_Push(subEdgeParam,Paramf)
			END IF
			DO ifin = 2,numSamp-1
				UVf(:) = UV1f(:) + (REAL(ifin-1)/REAL(numSamp-1))*(UV2f(:)-UV1f(:))
				CALL GetUVPointInfo0(Scommon,UVf(1),UVf(2),Pf)
				CALL Point3dQueue_Push(subEdgeTrain,Pf)
				IF (present(subEdgeSurf)) THEN
					CALL IntQueue_Push(subEdgeSurf,Scommon)
				END IF
				IF (present(subEdgeParam)) THEN
					Paramf(1:2) = UVf(1:2)
					CALL Point3dQueue_Push(subEdgeParam,Paramf)
				END IF
			END DO
			CALL Point3dQueue_Push(subEdgeTrain,P2f)
			IF (present(subEdgeSurf)) THEN
				CALL IntQueue_Push(subEdgeSurf,Scommon)
			END IF
			IF (present(subEdgeParam)) THEN
				Paramf(1:2) = UV2f(1:2)
				CALL Point3dQueue_Push(subEdgeParam,Paramf)
			END IF

			

		ELSE

			!There isn't a common surface but there might be two surfaces connected
			!by a curve

			!Find the mid point
			Pf(:) = 0.5d0*(P1f(:)+P2f(:))

			minDistf = HUGE(0.0d0)
			curf = -1
			s1f = -1
			s2f = -1

			DO cfin = 1,NumCurves+2*NumCurves

				s1tf = -1
				DO ifin = 1,nodesToSurf(ipt1f)%numNodes
					Sfin = nodesToSurf(ipt1f)%Nodes(ifin)
					IF (IntQueue_Contain(CurvToSurf(cfin),Sfin)) THEN
						s1tf = Sfin
					END IF
				END DO

				s2tf = -1
				DO ifin = 1,nodesToSurf(ipt2f)%numNodes
					Sfin = nodesToSurf(ipt2f)%Nodes(ifin)
					IF (IntQueue_Contain(CurvToSurf(cfin),Sfin)) THEN
						s2tf = Sfin
					END IF
				END DO

				IF ((s1tf.GT.0).AND.(s2tf.GT.0)) THEN
					!Now project to this curve
					DO jfin = 1,CurvSamped(cfin)%numNodes
						tfin = Geo3D_Distance_SQ(Pf,CurvSamped(cfin)%Pt(:,jfin))
						IF (tfin.LT.minDistf) THEN
							minDistf = tfin
							curf = cfin
							s1f = s1tf
							s2f = s2tf
							minNfin = jfin
						END IF
					END DO

				END IF


			END DO

			IF (curf.LT.0) THEN
				valid = -1
				RETURN
			ELSE
				!Now we have surface and curve information
				!WRITE(*,*)'curve found'

				!Straight line in parameter space from point to curve
				ifin = IntQueue_Find(nodesToSurf(ipt1f),s1f)
				UV1f(1:2) = paraCoord(ipt1f)%Pt(1:2,ifin)

				Pf = CurvSamped(curf)%Pt(:,minNfin)
				CALL ProjectToRegionFromXYZ(s1f, Pf, UV2f, TinySize, 0, tfin, PPf)

				CALL Point3dQueue_Push(subEdgeTrain,P1f)
				IF (present(subEdgeSurf)) THEN
					CALL IntQueue_Push(subEdgeSurf,s1f)
				END IF
				IF (present(subEdgeParam)) THEN
					Paramf(1:2) = UV1f(1:2)
					CALL Point3dQueue_Push(subEdgeParam,Paramf)
				END IF

				DO ifin = 2,numSamp-1
					UVf(:) = UV1f(:) + (REAL(ifin-1)/REAL(numSamp-1))*(UV2f(:)-UV1f(:))
					CALL GetUVPointInfo0(s1f,UVf(1),UVf(2),Pf)
					CALL Point3dQueue_Push(subEdgeTrain,Pf)
					IF (present(subEdgeSurf)) THEN
						CALL IntQueue_Push(subEdgeSurf,s1f)
					END IF
					IF (present(subEdgeParam)) THEN
						Paramf(1:2) = UVf(1:2)
						CALL Point3dQueue_Push(subEdgeParam,Paramf)
					END IF
				END DO

				!Straight line in paramter space from curve to point
				ifin = IntQueue_Find(nodesToSurf(ipt2f),s2f)
				UV2f(1:2) = paraCoord(ipt2f)%Pt(1:2,ifin)

				Pf = CurvSamped(curf)%Pt(:,minNfin)
				CALL ProjectToRegionFromXYZ(s2f, Pf, UV1f, TinySize, 0, tfin, PPf)

				DO ifin = 1,numSamp-1
					UVf(:) = UV1f(:) + (REAL(ifin-1)/REAL(numSamp-1))*(UV2f(:)-UV1f(:))
					CALL GetUVPointInfo0(s2f,UVf(1),UVf(2),Pf)
					CALL Point3dQueue_Push(subEdgeTrain,Pf)
					IF (present(subEdgeSurf)) THEN
						CALL IntQueue_Push(subEdgeSurf,s2f)
					END IF
					IF (present(subEdgeParam)) THEN
						Paramf(1:2) = UVf(1:2)
						CALL Point3dQueue_Push(subEdgeParam,Paramf)
					END IF
				END DO

				CALL Point3dQueue_Push(subEdgeTrain,P2f)
				IF (present(subEdgeSurf)) THEN
					CALL IntQueue_Push(subEdgeSurf,s2f)
				END IF
				IF (present(subEdgeParam)) THEN
					Paramf(1:2) = UV2f(1:2)
					CALL Point3dQueue_Push(subEdgeParam,Paramf)
				END IF


			END IF

		END IF


	END FUNCTION findSubEdges




	FUNCTION makeNURBSElement(ipTetm) RESULT(NURBSiem)
		USE NEFEM_Data
		USE CellConnectivity

		IMPLICIT NONE
		TYPE(NURBSElement) :: NURBSiem
		INTEGER, INTENT(IN) :: ipTetm(4)
		INTEGER :: npBDm, im, ip1m, ip2m, em, cm, sm, countm, jm, km, lm
		INTEGER :: e1m, e2m, e3m, splitm(3), nextm(3)
		INTEGER :: thisSm, fm
		REAL*8  :: P1m(3), P2m(3), u1m, Ptempm(3), uTempm, tm
		REAL*8  :: u2m, v1m, v2m, UVm(2), PPm(3), c1m, cTempm, c2m
		TYPE(Point3dQueueType) :: subEdgeTrainm, subEdgeParamm
		TYPE(IntQueueType) :: subEdgeSurfm, uniSurfm
		INTEGER :: it1m, it2m, it3m
		TYPE(Point3dQueueType) :: subEdgeTrainTm(3), subEdgeParamTm(3)
		TYPE(IntQueueType) :: subEdgeSurfTm(3), uniSurfTm(3), curveList
		TYPE(NURBSEdge) :: edgeTempm(3)

		INTEGER :: subNodesm(1,5) !This helps us keep track of info when splitting faces
				   !1 - surface number
		REAL*8  :: subNodesPm(5,6)
				   !Coordinate info for each sub node
				   !1:3 coordinates in physical space, 4:5 parameteric

		INTEGER :: edgeOrderm(3,4), subElemm(4,3), subElemm2(4,2)

		INTEGER :: subFlagm

		edgeOrderm(1,1) = -1
		edgeOrderm(2,1) = -1
		edgeOrderm(3,1) = 1

		edgeOrderm(1,2) = -1
		edgeOrderm(2,2) = -1
		edgeOrderm(3,2) = 1

		edgeOrderm(1,3) = -1
		edgeOrderm(2,3) = 1
		edgeOrderm(3,3) = -1

		edgeOrderm(1,4) = 1
		edgeOrderm(2,4) = 1
		edgeOrderm(3,4) = 1

		subElemm(1,1) = 3
		subElemm(2,1) = 4
		subElemm(3,1) = 2
		subElemm(4,1) = 6

		subElemm(1,2) = 2
		subElemm(2,2) = 4
		subElemm(3,2) = 1
		subElemm(4,2) = 6

		subElemm(1,3) = 4
		subElemm(2,3) = 5
		subElemm(3,3) = 1
		subElemm(4,3) = 6

		subElemm2(1,1) = 2
		subElemm2(2,1) = 3
		subElemm2(3,1) = 1
		subElemm2(4,1) = 5

		subElemm2(1,2) = 3
		subElemm2(2,2) = 4
		subElemm2(3,2) = 1
		subElemm2(4,2) = 5



		nextm(1) = 2
		nextm(2) = 3
		nextm(3) = 1

		!This function converts the element iem into a valid NURBS element

		!1) Cound the number of boundary nodes in this elment
		npBDm = 0

		DO im = 1,4
			IF (ipTetm(im).LE.NB_BD_Point) THEN
				npBDm = npBDm + 1
			END IF
		END DO

		IF (npBDm.EQ.2) THEN

			!This is an element which has a single edge on the boundary
			
			!We need to figure out which edge is the nurbs edge
			DO im = 1,6
				ip1m = ipTetm(I_Comb_Tet(1,im))
				ip2m = ipTetm(I_Comb_Tet(2,im))

				IF ((ip1m.LE.NB_BD_Point).AND.(ip2m.LE.NB_BD_Point)) THEN
					em = im
				END IF
			END DO

			ip1m = ipTetm(I_Comb_Tet(1,em))
			ip2m = ipTetm(I_Comb_Tet(2,em))

			!First check to see if this is an intersection curve
			P1m(:) = Posit(:,ip1m)
			P2m(:) = Posit(:,ip2m)

			CALL IntQueue_Clear(curveList)
			DO im = 1,NumCurves
				!Project to curve
				
				CALL OCCT_GetUFromXYZ(im,P1m,uTempm)
				!Get the position on the curve
				
				CALL OCCT_GetLineXYZFromT(im,uTempm,Ptempm)

				tm = Geo3D_Distance(P1m,Ptempm)

				IF (tm.LT.TinySize) THEN
					CALL IntQueue_Push(curveList,im)
				END IF

			END DO

			IF (curveList%numNodes.GT.0) THEN
				!Now check the other node
				DO im = 1,curveList%numNodes
					!Project to curve
					lm = curveList%nodes(im)
					CALL OCCT_GetUFromXYZ(lm,P2m,uTempm)
					!Get the position on the curve
					
					CALL OCCT_GetLineXYZFromT(lm,uTempm,Ptempm)

					tm = Geo3D_Distance(P2m,Ptempm)

					IF (tm.LT.TinySize) THEN
						u2m = uTempm
						cm = lm

						CALL OCCT_GetUFromXYZ(cm,P1m,u1m)

						u2m = uTempm
						
						!We have all the info we need
						NURBSiem%NB_Int_Elem = 1

						ALLOCATE(NURBSiem%intelem(1))

						NURBSiem%intelem(1)%NB_N_Edge = 1

						NURBSiem%intelem(1)%edge%onCurve = .TRUE.
						NURBSiem%intelem(1)%edge%curveId = cm
						NURBSiem%intelem(1)%edge%c1 = u1m
						NURBSiem%intelem(1)%edge%c2 = u2m
						NURBSiem%intelem(1)%edge%Posit(:,1) = P1m(:)
						NURBSiem%intelem(1)%edge%Posit(:,2) = P2m(:)
						NURBSiem%intelem(1)%edge%IP_Local(1) = I_Comb_Tet(1,em)
						NURBSiem%intelem(1)%edge%IP_Local(2) = I_Comb_Tet(2,em)
			
						NURBSiem%intelem(1)%Posit(:,1) = Posit(:,ipTetm(1))
						NURBSiem%intelem(1)%Posit(:,2) = Posit(:,ipTetm(2))
						NURBSiem%intelem(1)%Posit(:,3) = Posit(:,ipTetm(3))
						NURBSiem%intelem(1)%Posit(:,4) = Posit(:,ipTetm(4))

						NURBSiem%intelem(1)%isNURBEdge(:) = 0
						NURBSiem%intelem(1)%isNURBEdge(em) = 1
						EXIT

					END IF
				END DO

			END IF

			IF (NURBSiem%NB_Int_Elem.EQ.0) THEN
				!Then create the sub edges for that edge
				subEdgeTrainm = findSubEdges(ip1m,ip2m,subFlagm,subEdgeSurfm,subEdgeParamm)
				IF (subFlagm.LT.0) THEN
					NURBSiem%isValid = .FALSE.
					RETURN
				END IF

				!Now we need to find out how many surfaces are on this edge
				CALL IntQueue_Clear(uniSurfm)
				DO im = 1,subEdgeSurfm%numNodes
					sm = subEdgeSurfm%Nodes(im)
					IF (.NOT.IntQueue_Contain(uniSurfm,sm)) THEN
						CALL IntQueue_Push(uniSurfm,sm)
					END IF
				END DO

				!One element per surface
				NURBSiem%NB_Int_Elem = uniSurfm%numNodes

				!This will help us keep track of where we are
				countm = 1
				thisSm =  subEdgeSurfm%Nodes(countm)

				!The first node of each group is on the intersection curve
				ALLOCATE(NURBSiem%intelem(NURBSiem%NB_Int_Elem))

				DO im = 1,NURBSiem%NB_Int_Elem
					P1m(:) = subEdgeTrainm%Pt(:,countm)
					u1m = subEdgeParamm%Pt(1,countm)
					v1m = subEdgeParamm%Pt(2,countm)

					!Loop to find the next node
					DO jm = countm+1,subEdgeSurfm%numNodes
						sm = subEdgeSurfm%Nodes(jm)
						IF ((sm.NE.thisSm).OR.(jm.EQ.subEdgeSurfm%numNodes)) THEN
							km = jm
							EXIT
						END IF
					END DO

					P2m(:) = subEdgeTrainm%Pt(:,km)

					CALL ProjectToRegionFromXYZ(thisSm, P2m, UVm, TinySize, 0, tm, PPm)

					u2m = UVm(1)
					v2m = UVm(2)

					NURBSiem%intelem(im)%NB_N_Edge = 1
					NURBSiem%intelem(im)%edge%Posit(:,1) = P1m(:)
					NURBSiem%intelem(im)%edge%Posit(:,2) = P2m(:)
					NURBSiem%intelem(im)%edge%onCurve = .FALSE.

					!Need to add a check to see if these both lie
					!on intersection curve
					CALL IntQueue_Clear(curveList)
					DO jm = 1,NumCurves
						!Project to curve
				
						CALL OCCT_GetUFromXYZ(jm,P1m,cTempm)
						!Get the position on the curve
						
						CALL OCCT_GetLineXYZFromT(jm,cTempm,Ptempm)

						tm = Geo3D_Distance(P1m,Ptempm)

						IF (tm.LT.TinySize) THEN
							CALL IntQueue_Push(curveList,jm)
						END IF

					END DO

					IF (curveList%numNodes.GT.0) THEN

						DO km = 1,curveList%numNodes
							lm = curveList%nodes(km)
							CALL OCCT_GetUFromXYZ(lm,P2m,cTempm)
							!Get the position on the curve
							
							CALL OCCT_GetLineXYZFromT(lm,cTempm,Ptempm)

							tm = Geo3D_Distance(P2m,Ptempm)

							IF (tm.LT.TinySize) THEN
								c2m = cTempm
								cm = lm
								CALL OCCT_GetUFromXYZ(cm,P1m,c1m)

								NURBSiem%intelem(im)%edge%onCurve = .TRUE.
								NURBSiem%intelem(im)%edge%curveId = cm
								NURBSiem%intelem(im)%edge%c1 = c1m
								NURBSiem%intelem(im)%edge%c2 = c2m
								


							END IF
						END DO


					END IF

					IF (.NOT.NURBSiem%intelem(im)%edge%onCurve) THEN		
						NURBSiem%intelem(im)%edge%surfaceId = thisSm
						NURBSiem%intelem(im)%edge%u1 = u1m
						NURBSiem%intelem(im)%edge%u2 = u2m
						NURBSiem%intelem(im)%edge%v1 = v1m
						NURBSiem%intelem(im)%edge%v2 = v2m
					END IF
					
					NURBSiem%intelem(im)%edge%IP_Local(1) = I_Comb_Tet(1,em)
					NURBSiem%intelem(im)%edge%IP_Local(2) = I_Comb_Tet(2,em)

					NURBSiem%intelem(im)%Posit(:,1) = Posit(:,ipTetm(1))
					NURBSiem%intelem(im)%Posit(:,2) = Posit(:,ipTetm(2))
					NURBSiem%intelem(im)%Posit(:,3) = Posit(:,ipTetm(3))
					NURBSiem%intelem(im)%Posit(:,4) = Posit(:,ipTetm(4))

					NURBSiem%intelem(im)%Posit(:,I_Comb_Tet(1,em)) = P1m(:)
					NURBSiem%intelem(im)%Posit(:,I_Comb_Tet(2,em)) = P2m(:)

					NURBSiem%intelem(im)%isNURBEdge(:) = 0
					NURBSiem%intelem(im)%isNURBEdge(em) = 1

					countm = km
					thisSm = subEdgeSurfm%Nodes(countm)

				END DO


			END IF

			NURBSiem%isValid = .TRUE. !TODO  check +ve vol on each case

		ELSE IF (npBDm.EQ.3) THEN
			!A face is on the boundary

			!First find which face is the boundary face

			!Cell connectivities, iTri_Tet(3,i=1:4) the 4 triangles in a tet
			!I_Comb_Tet(1:2,j=1:6) edges of the tet, j=jEdge_Tri_Tet(3,i=1:4) the edges in I_Comb_Tet
			!which associated with the triangles in iTri_Tet

			DO im = 1,4
				it1m = ipTetm(iTri_Tet(1,im))
				it2m = ipTetm(iTri_Tet(2,im))
				it3m = ipTetm(iTri_Tet(3,im))

				IF ((it1m.LE.NB_BD_Point).AND.(it2m.LE.NB_BD_Point).AND.(it3m.LE.NB_BD_Point)) THEN
					em = im !So em is the face we are interested in 
				END IF

			END DO

			!Now we need to decide if this face lies on one surface or not

			!1) Identify edges which are intersection curves and build other sub edges

			countm = 0  !This will keep track of how many edges lie on the same surface or not
			
			DO km = 1,3
				jm = jEdge_Tri_Tet(km,em)
				ip1m = ipTetm(I_Comb_Tet(1,jm))
				ip2m = ipTetm(I_Comb_Tet(2,jm))

				P1m(:) = Posit(:,ip1m)
				P2m(:) = Posit(:,ip2m)

				CALL IntQueue_Clear(curveList)
				DO im = 1,NumCurves
					!Project to curve
					
					CALL OCCT_GetUFromXYZ(im,P1m,uTempm)
					!Get the position on the curve
					
					CALL OCCT_GetLineXYZFromT(im,uTempm,Ptempm)

					tm = Geo3D_Distance(P1m,Ptempm)

					IF (tm.LT.TinySize) THEN
						CALL IntQueue_Push(curveList,im)				
					END IF

				END DO


				IF (curveList%numNodes.GT.0) THEN
					!Now check the other node
					DO im = 1,curveList%numNodes

						lm = curveList%nodes(im)
						!Project to curve
						
						CALL OCCT_GetUFromXYZ(lm,P2m,uTempm)
						!Get the position on the curve
						
						CALL OCCT_GetLineXYZFromT(lm,uTempm,Ptempm)

						tm = Geo3D_Distance(P2m,Ptempm)
						IF (tm.LT.TinySize) THEN
							u2m = uTempm
							cm = lm

							CALL OCCT_GetUFromXYZ(cm,P1m,u1m)

							edgeTempm(km)%onCurve = .TRUE.
							edgeTempm(km)%curveId = cm
							edgeTempm(km)%c1 = u1m
							edgeTempm(km)%c2 = u2m
							edgeTempm(km)%Posit(:,1) = P1m(:)
							edgeTempm(km)%Posit(:,2) = P2m(:)
							edgeTempm(km)%IP_Local(1) = I_Comb_Tet(1,jm)
							edgeTempm(km)%IP_Local(2) = I_Comb_Tet(2,jm)

							countm = countm + 1
							EXIT
						END IF
					END DO
				END IF

				
				!We need to build sub edges
				subEdgeTrainTm(km) = findSubEdges(ip1m,ip2m,subFlagm,subEdgeSurfTm(km),subEdgeParamTm(km))
				IF (subFlagm.LT.0) THEN
					NURBSiem%isValid = .FALSE.
					RETURN
				END IF
				IF (.NOT.edgeTempm(km)%onCurve) THEN
					!Now we need to find out how many surfaces are on this edge
					CALL IntQueue_Clear(uniSurfTm(km))
					DO im = 1,subEdgeSurfTm(km)%numNodes
						sm = subEdgeSurfTm(km)%Nodes(im)
						IF (.NOT.IntQueue_Contain(uniSurfTm(km),sm)) THEN
							CALL IntQueue_Push(uniSurfTm(km),sm)
						END IF
					END DO

					IF (uniSurfTm(km)%numNodes.EQ.1) THEN
						countm = countm + 1
					END IF
				END IF
			END DO
			
			IF (countm.EQ.3) THEN
				!Now we need to check if all the surfaces are the same
				
				CALL IntQueue_Clear(uniSurfm)
				DO km = 1,3
					IF (.NOT.edgeTempm(km)%onCurve) THEN				
						DO im = 1,uniSurfTm(km)%numNodes
							sm = uniSurfTm(km)%Nodes(im)
							IF (.NOT.IntQueue_Contain(uniSurfm,sm)) THEN
								CALL IntQueue_Push(uniSurfm,sm)
							END IF
						END DO
					END IF
				END DO

	

				IF (uniSurfm%numNodes.EQ.1) THEN
					!We can build a single surface face

					NURBSiem%NB_Int_Elem = 1
					ALLOCATE(NURBSiem%intelem(1))
					NURBSiem%intelem(1)%NB_N_Face = 1
					NURBSiem%intelem(1)%face%IP_Local(:) = iTri_Tet(:,em)
					NURBSiem%intelem(1)%Posit(:,1) = Posit(:,ipTetm(1))
					NURBSiem%intelem(1)%Posit(:,2) = Posit(:,ipTetm(2))
					NURBSiem%intelem(1)%Posit(:,3) = Posit(:,ipTetm(3))
					NURBSiem%intelem(1)%Posit(:,4) = Posit(:,ipTetm(4))
					NURBSiem%intelem(1)%isNURBFace(:) = 0
					NURBSiem%intelem(1)%isNURBFace(em) = 1
					NURBSiem%intelem(1)%isNURBEdge(:) = 0

					!Now build the edges
					DO km = 1,3
						jm = jEdge_Tri_Tet(km,em)
						ip1m = ipTetm(I_Comb_Tet(1,jm))
						ip2m = ipTetm(I_Comb_Tet(2,jm))

						P1m(:) = Posit(:,ip1m)
						P2m(:) = Posit(:,ip2m)
						NURBSiem%intelem(1)%isNURBEdge(jm) = 1

						IF (edgeTempm(km)%onCurve) THEN
							NURBSiem%intelem(1)%face%edges(km) = edgeTempm(km)
						ELSE
							NURBSiem%intelem(1)%face%edges(km)%Posit(:,1) = P1m(:)
							NURBSiem%intelem(1)%face%edges(km)%Posit(:,2) = P2m(:)
							NURBSiem%intelem(1)%face%edges(km)%onCurve = .FALSE.

							u1m = subEdgeParamTm(km)%Pt(1,1)
							v1m = subEdgeParamTm(km)%Pt(2,1)

							u2m = subEdgeParamTm(km)%Pt(1,subEdgeParamTm(km)%numNodes)
							v2m = subEdgeParamTm(km)%Pt(2,subEdgeParamTm(km)%numNodes)

							thisSm = subEdgeSurfTm(km)%Nodes(1)

							NURBSiem%intelem(1)%face%edges(km)%surfaceId = thisSm
							NURBSiem%intelem(1)%face%edges(km)%u1 = u1m
							NURBSiem%intelem(1)%face%edges(km)%u2 = u2m
							NURBSiem%intelem(1)%face%edges(km)%v1 = v1m
							NURBSiem%intelem(1)%face%edges(km)%v2 = v2m

							NURBSiem%intelem(1)%face%edges(km)%IP_Local(1) = I_Comb_Tet(1,jm)
							NURBSiem%intelem(1)%face%edges(km)%IP_Local(2) = I_Comb_Tet(1,jm)

						END IF

					END DO

					NURBSiem%isValid = .TRUE. !TODO

				END IF

			ELSE IF (countm.EQ.1) THEN

				

				!To get here not all edges lie on a single surface
				!Now we need to check how many surfaces we have
				splitm(:) = 0
				CALL IntQueue_Clear(uniSurfm)
				DO km = 1,3
					IF (.NOT.edgeTempm(km)%onCurve) THEN
						IF (uniSurfTm(km)%numNodes.GT.1) THEN
							splitm(km) = 1
						END IF
						DO im = 1,uniSurfTm(km)%numNodes
							sm = uniSurfTm(km)%Nodes(im)
							
							IF (.NOT.IntQueue_Contain(uniSurfm,sm)) THEN
								CALL IntQueue_Push(uniSurfm,sm)
							END IF
						END DO
					END IF
				END DO

				IF (uniSurfm%numNodes.EQ.2) THEN

					!This means the triangle is split by a single intersection curve

					!Step 1) find the order of the edges wrt our reference element where
					!we have split edge -> split edge -> normal edge
					e1m = -1
						
					DO km = 1,3
						IF ((splitm(km).EQ.1).AND.(splitm(nextm(km)).EQ.1)) THEN
							e1m = km
							e2m = nextm(km)
							e3m = nextm(e2m)
						END IF
					END DO

					IF (e1m.GT.0) THEN

						!WRITE(*,*) 'Split test 1'
						!WRITE(*,*) e1m, splitm(e1m), edgeTempm(e1m)%onCurve
						!WRITE(*,*) e2m, splitm(e2m), edgeTempm(e2m)%onCurve
						!WRITE(*,*) e3m, splitm(e3m), edgeTempm(e3m)%onCurve

						!We will be dividing this into 3 sub-elements

						NURBSiem%NB_Int_Elem = 3
						ALLOCATE(NURBSiem%intelem(NURBSiem%NB_Int_Elem))
						
						!Now collect all the nodes

						!We are in triangle em
						IF (edgeOrderm(e1m,em).GT.0) THEN
							subNodesPm(1:3,1) = subEdgeTrainTm(e1m)%Pt(1:3,1)
							subNodesm(1,1) = subEdgeSurfTm(e1m)%nodes(1)
							subNodesPm(4:5,1) = subEdgeParamTm(e1m)%Pt(1:2,1)

							!Find the break in the middle of e1m
							thisSm = subNodesm(1,1)
							km = -1
							DO jm = 2,subEdgeSurfTm(e1m)%numNodes-1
								sm = subEdgeSurfTm(e1m)%nodes(jm)
								IF ((sm.NE.thisSm).OR.(jm.EQ.subEdgeSurfTm(e1m)%numNodes-1)) THEN
									km = jm
									EXIT
								END IF
							END DO
						ELSE
							lm = subEdgeTrainTm(e1m)%numNodes
							subNodesPm(1:3,1) = subEdgeTrainTm(e1m)%Pt(1:3,lm)
							subNodesm(1,1) = subEdgeSurfTm(e1m)%nodes(lm)
							subNodesPm(4:5,1) = subEdgeParamTm(e1m)%Pt(1:2,lm)

							!Find the break in the middle of e1m
							thisSm = subNodesm(1,1)
							km = -1
							DO jm = lm-1,1,-1
								sm = subEdgeSurfTm(e1m)%nodes(jm)
								IF ((sm.NE.thisSm).OR.(jm.EQ.1)) THEN
									km = jm+1
									EXIT
								END IF
							END DO
						END IF

						subNodesPm(1:3,2) = subEdgeTrainTm(e1m)%Pt(1:3,km)
						subNodesm(1,2) = subEdgeSurfTm(e1m)%nodes(km)
						subNodesPm(4:5,2) = subEdgeParamTm(e1m)%Pt(1:2,km)


						IF (edgeOrderm(e2m,em).GT.0) THEN

							subNodesPm(1:3,3) = subEdgeTrainTm(e2m)%Pt(1:3,1)
							subNodesm(1,3) = subEdgeSurfTm(e2m)%nodes(1)
							subNodesPm(4:5,3) = subEdgeParamTm(e2m)%Pt(1:2,1)

							!Find the break in the middle of e1m
							thisSm = subNodesm(1,3)
							km = -1
							DO jm = 2,subEdgeSurfTm(e2m)%numNodes-1
								sm = subEdgeSurfTm(e2m)%nodes(jm)
								IF ((sm.NE.thisSm).OR.(jm.EQ.subEdgeSurfTm(e2m)%numNodes-1)) THEN
									km = jm
									EXIT
								END IF
							END DO

						ELSE

							lm = subEdgeTrainTm(e2m)%numNodes
							subNodesPm(1:3,3) = subEdgeTrainTm(e2m)%Pt(1:3,lm)
							subNodesm(1,3) = subEdgeSurfTm(e2m)%nodes(lm)
							subNodesPm(4:5,3) = subEdgeParamTm(e2m)%Pt(1:2,lm)

							!Find the break in the middle of e1m
							thisSm = subNodesm(1,3)
							km = -1
							DO jm = lm-1,1,-1
								sm = subEdgeSurfTm(e2m)%nodes(jm)
								IF ((sm.NE.thisSm).OR.(jm.EQ.1)) THEN
									km = jm+1
									EXIT
								END IF
							END DO

						END IF

						subNodesPm(1:3,4) = subEdgeTrainTm(e2m)%Pt(1:3,km)
						subNodesm(1,4) = subEdgeSurfTm(e2m)%nodes(km)
						subNodesPm(4:5,4) = subEdgeParamTm(e2m)%Pt(1:2,km)

						IF (edgeOrderm(e3m,em).GT.0) THEN
							subNodesPm(1:3,5) = subEdgeTrainTm(e3m)%Pt(1:3,1)
							subNodesm(1,5) = subEdgeSurfTm(e3m)%nodes(1)
							subNodesPm(4:5,5) = subEdgeParamTm(e3m)%Pt(1:2,1)
						ELSE
							lm = subEdgeTrainTm(e3m)%numNodes
							subNodesPm(1:3,5) = subEdgeTrainTm(e3m)%Pt(1:3,lm)
							subNodesm(1,5) = subEdgeSurfTm(e3m)%nodes(lm)
							subNodesPm(4:5,5) = subEdgeParamTm(e3m)%Pt(1:2,lm)
						END IF

						!WRITE(*,*) 'Split check'
						!DO km = 1,5
						!	WRITE(*,*) km, subNodesm(1,km), subNodesPm(4,km),subNodesPm(5,km)
						!END DO

						!We now want to put the volume node in as number 6
						subNodesPm(1:3,6) = Posit(:,ipTetm(em))

						DO fm = 1,3
							NURBSiem%intelem(fm)%NB_N_Face = 1
							NURBSiem%intelem(fm)%face%IP_Local(:) = iTri_Tet(:,4) !4 because node 4 is vol
							NURBSiem%intelem(fm)%Posit(:,1) = subNodesPm(1:3,subElemm(1,fm))
							NURBSiem%intelem(fm)%Posit(:,2) = subNodesPm(1:3,subElemm(2,fm))
							NURBSiem%intelem(fm)%Posit(:,3) = subNodesPm(1:3,subElemm(3,fm))
							NURBSiem%intelem(fm)%Posit(:,4) = subNodesPm(1:3,subElemm(4,fm))
							NURBSiem%intelem(fm)%isNURBFace(:) = 0
							NURBSiem%intelem(fm)%isNURBFace(4) = 1
							NURBSiem%intelem(fm)%isNURBEdge(:) = 0

							!Now build the edges
							DO km = 1,3
								jm = jEdge_Tri_Tet(km,4)
								ip1m = subElemm(I_Comb_Tet(1,jm),fm) !These are nodes pointing in subNodesm
								ip2m = subElemm(I_Comb_Tet(2,jm),fm)

								P1m(:) = subNodesPm(1:3,ip1m)
								P2m(:) = subNodesPm(1:3,ip2m)

								NURBSiem%intelem(fm)%isNURBEdge(jm) = 1

								!Need to check if this is on a curve
								CALL IntQueue_Clear(curveList)
								DO im = 1,NumCurves
									!Project to curve
									
									CALL OCCT_GetUFromXYZ(im,P1m,uTempm)
									!Get the position on the curve
									
									CALL OCCT_GetLineXYZFromT(im,uTempm,Ptempm)

									tm = Geo3D_Distance(P1m,Ptempm)

									IF (tm.LT.TinySize) THEN
										CALL IntQueue_Push(curveList,im)				
									END IF

								END DO
								IF (curveList%numNodes.GT.0) THEN
									!Now check the other node
									DO im = 1,curveList%numNodes

										lm = curveList%nodes(im)
										!Project to curve
										
										CALL OCCT_GetUFromXYZ(lm,P2m,uTempm)
										!Get the position on the curve
										
										CALL OCCT_GetLineXYZFromT(lm,uTempm,Ptempm)

										tm = Geo3D_Distance(P2m,Ptempm)
										IF (tm.LT.TinySize) THEN
											u2m = uTempm
											cm = lm

											CALL OCCT_GetUFromXYZ(cm,P1m,u1m)

											NURBSiem%intelem(fm)%face%edges(km)%onCurve = .TRUE.
											NURBSiem%intelem(fm)%face%edges(km)%curveId = cm
											NURBSiem%intelem(fm)%face%edges(km)%c1 = u1m
											NURBSiem%intelem(fm)%face%edges(km)%c2 = u2m
											NURBSiem%intelem(fm)%face%edges(km)%Posit(:,1) = P1m(:)
											NURBSiem%intelem(fm)%face%edges(km)%Posit(:,2) = P2m(:)
											NURBSiem%intelem(fm)%face%edges(km)%IP_Local(1) = I_Comb_Tet(1,jm)
											NURBSiem%intelem(fm)%face%edges(km)%IP_Local(2) = I_Comb_Tet(2,jm)

											
											EXIT
										END IF
									END DO
								END IF

								IF (.NOT.NURBSiem%intelem(fm)%face%edges(km)%onCurve) THEN
									NURBSiem%intelem(fm)%face%edges(km)%Posit(:,1) = P1m(:)
									NURBSiem%intelem(fm)%face%edges(km)%Posit(:,2) = P2m(:)


									IF (ip1m.EQ.4) THEN
										thisSm = subNodesm(1,ip2m)
									ELSE IF (ip2m.EQ.4) THEN
										thisSm = subNodesm(1,ip1m)
									ELSE IF (ip1m.EQ.2) THEN
										thisSm = subNodesm(1,ip2m)
									ELSE IF (ip2m.EQ.2) THEN
										thisSm = subNodesm(1,ip1m)
									ELSE
										thisSm = subNodesm(1,ip2m)
									END IF

									!Now project to find surface coordinates
									UVm(:) = 0.0d0
									CALL ProjectToRegionFromXYZ(thisSm, P1m, UVm, TinySize, 1, tm, Ptempm)

									u1m = UVm(1)
									v1m = UVm(2)

									CALL ProjectToRegionFromXYZ(thisSm, P2m, UVm, TinySize, 1, tm, Ptempm)

									u2m = UVm(1)
									v2m = UVm(2)

									

									NURBSiem%intelem(fm)%face%edges(km)%surfaceId = thisSm
									NURBSiem%intelem(fm)%face%edges(km)%u1 = u1m
									NURBSiem%intelem(fm)%face%edges(km)%u2 = u2m
									NURBSiem%intelem(fm)%face%edges(km)%v1 = v1m
									NURBSiem%intelem(fm)%face%edges(km)%v2 = v2m

									NURBSiem%intelem(fm)%face%edges(km)%IP_Local(1) = I_Comb_Tet(1,jm)
									NURBSiem%intelem(fm)%face%edges(km)%IP_Local(2) = I_Comb_Tet(2,jm)

								END IF


							END DO


						END DO

						NURBSiem%isValid = .TRUE. 
					END IF
				END IF

			ELSE IF (countm.EQ.2) THEN

				!This has one split edge
				!Now we need to check how many surfaces we have
				splitm(:) = 0
				CALL IntQueue_Clear(uniSurfm)
				DO km = 1,3
					IF (.NOT.edgeTempm(km)%onCurve) THEN
						IF (uniSurfTm(km)%numNodes.GT.1) THEN
							splitm(km) = 1
						END IF
						DO im = 1,uniSurfTm(km)%numNodes
							sm = uniSurfTm(km)%Nodes(im)
							
							IF (.NOT.IntQueue_Contain(uniSurfm,sm)) THEN
								CALL IntQueue_Push(uniSurfm,sm)
							END IF
						END DO
					END IF
				END DO

				IF (uniSurfm%numNodes.EQ.2) THEN

					!Triangle split by a single intersection curve which crosses
					!at a vertex

					!Step 1) find the order of the edges wrt our reference element where
					!we have normal edge -> split edge -> normal edge
					e1m = -1
					DO km = 1,3
						IF ((splitm(km).EQ.0).AND.(splitm(nextm(km)).EQ.0)) THEN
							e3m = km
							e1m = nextm(km)
							e2m = nextm(e1m)
						END IF
					END DO

					IF (e1m.GT.0) THEN

						!WRITE(*,*) 'Split test 2'
						!WRITE(*,*) e1m, splitm(e1m), edgeTempm(e1m)%onCurve
						!WRITE(*,*) e2m, splitm(e2m), edgeTempm(e2m)%onCurve
						!WRITE(*,*) e3m, splitm(e3m), edgeTempm(e3m)%onCurve

						!We will be dividing this into 2 sub-elements
						NURBSiem%NB_Int_Elem = 2
						ALLOCATE(NURBSiem%intelem(NURBSiem%NB_Int_Elem))

						!Now collect all the nodes
						IF (edgeOrderm(e1m,em).GT.0) THEN
							subNodesPm(1:3,1) = subEdgeTrainTm(e1m)%Pt(1:3,1)
							subNodesm(1,1) = subEdgeSurfTm(e1m)%nodes(1)
							subNodesPm(4:5,1) = subEdgeParamTm(e1m)%Pt(1:2,1)
						ELSE
							lm = subEdgeTrainTm(e1m)%numNodes
							subNodesPm(1:3,1) = subEdgeTrainTm(e1m)%Pt(1:3,lm)
							subNodesm(1,1) = subEdgeSurfTm(e1m)%nodes(lm)
							subNodesPm(4:5,1) = subEdgeParamTm(e1m)%Pt(1:2,lm)
						END IF

						IF (edgeOrderm(e2m,em).GT.0) THEN

							subNodesPm(1:3,2) = subEdgeTrainTm(e2m)%Pt(1:3,1)
							subNodesm(1,2) = subEdgeSurfTm(e2m)%nodes(1)
							subNodesPm(4:5,2) = subEdgeParamTm(e2m)%Pt(1:2,1)

							!Find the break in the middle of e1m
							thisSm = subNodesm(1,2)
							km = -1
							DO jm = 2,subEdgeSurfTm(e2m)%numNodes-1
								sm = subEdgeSurfTm(e2m)%nodes(jm)
								IF ((sm.NE.thisSm).OR.(jm.EQ.subEdgeSurfTm(e2m)%numNodes-1)) THEN
									km = jm
									EXIT
								END IF
							END DO

						ELSE

							lm = subEdgeTrainTm(e2m)%numNodes
							subNodesPm(1:3,2) = subEdgeTrainTm(e2m)%Pt(1:3,lm)
							subNodesm(1,2) = subEdgeSurfTm(e2m)%nodes(lm)
							subNodesPm(4:5,2) = subEdgeParamTm(e2m)%Pt(1:2,lm)

							!Find the break in the middle of e1m
							thisSm = subNodesm(1,2)
							km = -1
							DO jm = lm-1,1,-1
								sm = subEdgeSurfTm(e2m)%nodes(jm)
								IF ((sm.NE.thisSm).OR.(jm.EQ.1)) THEN
									km = jm+1
									EXIT
								END IF
							END DO

						END IF

						subNodesPm(1:3,3) = subEdgeTrainTm(e2m)%Pt(1:3,km)
						subNodesm(1,3) = subEdgeSurfTm(e2m)%nodes(km)
						subNodesPm(4:5,3) = subEdgeParamTm(e2m)%Pt(1:2,km)

						!Now collect all the nodes
						IF (edgeOrderm(e3m,em).GT.0) THEN
							subNodesPm(1:3,4) = subEdgeTrainTm(e3m)%Pt(1:3,1)
							subNodesm(1,4) = subEdgeSurfTm(e3m)%nodes(1)
							subNodesPm(4:5,4) = subEdgeParamTm(e3m)%Pt(1:2,1)
						ELSE
							lm = subEdgeTrainTm(e3m)%numNodes
							subNodesPm(1:3,4) = subEdgeTrainTm(e3m)%Pt(1:3,lm)
							subNodesm(1,4) = subEdgeSurfTm(e3m)%nodes(lm)
							subNodesPm(4:5,4) = subEdgeParamTm(e3m)%Pt(1:2,lm)
						END IF

						!WRITE(*,*) 'Split check 2'
						!DO km = 1,4
						!	WRITE(*,*) km, subNodesm(1,km), subNodesPm(4,km),subNodesPm(5,km)
						!END DO

						!We now want to put the volume node in as number 5
						subNodesPm(1:3,5) = Posit(:,ipTetm(em))

						DO fm = 1,2
							NURBSiem%intelem(fm)%NB_N_Face = 1
							NURBSiem%intelem(fm)%face%IP_Local(:) = iTri_Tet(:,4) !4 because node 4 is vol
							NURBSiem%intelem(fm)%Posit(:,1) = subNodesPm(1:3,subElemm2(1,fm))
							NURBSiem%intelem(fm)%Posit(:,2) = subNodesPm(1:3,subElemm2(2,fm))
							NURBSiem%intelem(fm)%Posit(:,3) = subNodesPm(1:3,subElemm2(3,fm))
							NURBSiem%intelem(fm)%Posit(:,4) = subNodesPm(1:3,subElemm2(4,fm))
							NURBSiem%intelem(fm)%isNURBFace(:) = 0
							NURBSiem%intelem(fm)%isNURBFace(4) = 1
							NURBSiem%intelem(fm)%isNURBEdge(:) = 0

							!Now build the edges
							DO km = 1,3
								jm = jEdge_Tri_Tet(km,4)
								ip1m = subElemm2(I_Comb_Tet(1,jm),fm) !These are nodes pointing in subNodesm
								ip2m = subElemm2(I_Comb_Tet(2,jm),fm)

								P1m(:) = subNodesPm(1:3,ip1m)
								P2m(:) = subNodesPm(1:3,ip2m)

								NURBSiem%intelem(fm)%isNURBEdge(jm) = 1

								!Need to check if this is on a curve
								CALL IntQueue_Clear(curveList)
								DO im = 1,NumCurves
									!Project to curve
									
									CALL OCCT_GetUFromXYZ(im,P1m,uTempm)
									!Get the position on the curve
									
									CALL OCCT_GetLineXYZFromT(im,uTempm,Ptempm)

									tm = Geo3D_Distance(P1m,Ptempm)

									IF (tm.LT.TinySize) THEN
										CALL IntQueue_Push(curveList,im)				
									END IF

								END DO
								IF (curveList%numNodes.GT.0) THEN
									!Now check the other node
									DO im = 1,curveList%numNodes

										lm = curveList%nodes(im)
										!Project to curve
										
										CALL OCCT_GetUFromXYZ(lm,P2m,uTempm)
										!Get the position on the curve
										
										CALL OCCT_GetLineXYZFromT(lm,uTempm,Ptempm)

										tm = Geo3D_Distance(P2m,Ptempm)
										IF (tm.LT.TinySize) THEN
											u2m = uTempm
											cm = lm

											CALL OCCT_GetUFromXYZ(cm,P1m,u1m)

											NURBSiem%intelem(fm)%face%edges(km)%onCurve = .TRUE.
											NURBSiem%intelem(fm)%face%edges(km)%curveId = cm
											NURBSiem%intelem(fm)%face%edges(km)%c1 = u1m
											NURBSiem%intelem(fm)%face%edges(km)%c2 = u2m
											NURBSiem%intelem(fm)%face%edges(km)%Posit(:,1) = P1m(:)
											NURBSiem%intelem(fm)%face%edges(km)%Posit(:,2) = P2m(:)
											NURBSiem%intelem(fm)%face%edges(km)%IP_Local(1) = I_Comb_Tet(1,jm)
											NURBSiem%intelem(fm)%face%edges(km)%IP_Local(2) = I_Comb_Tet(2,jm)

											
											EXIT
										END IF
									END DO
								END IF

								IF (.NOT.NURBSiem%intelem(fm)%face%edges(km)%onCurve) THEN
									NURBSiem%intelem(fm)%face%edges(km)%Posit(:,1) = P1m(:)
									NURBSiem%intelem(fm)%face%edges(km)%Posit(:,2) = P2m(:)


									IF (ip1m.EQ.3) THEN
										thisSm = subNodesm(1,ip2m)
									ELSE IF (ip2m.EQ.3) THEN
										thisSm = subNodesm(1,ip1m)
									ELSE IF (ip1m.EQ.1) THEN
										thisSm = subNodesm(1,ip2m)
									ELSE IF (ip2m.EQ.1) THEN
										thisSm = subNodesm(1,ip1m)
									ELSE
										thisSm = subNodesm(1,ip2m)
									END IF

									!Now project to find surface coordinates
									UVm(:) = 0.0d0
									CALL ProjectToRegionFromXYZ(thisSm, P1m, UVm, TinySize, 1, tm, Ptempm)

									u1m = UVm(1)
									v1m = UVm(2)

									CALL ProjectToRegionFromXYZ(thisSm, P2m, UVm, TinySize, 1, tm, Ptempm)

									u2m = UVm(1)
									v2m = UVm(2)

									

									NURBSiem%intelem(fm)%face%edges(km)%surfaceId = thisSm
									NURBSiem%intelem(fm)%face%edges(km)%u1 = u1m
									NURBSiem%intelem(fm)%face%edges(km)%u2 = u2m
									NURBSiem%intelem(fm)%face%edges(km)%v1 = v1m
									NURBSiem%intelem(fm)%face%edges(km)%v2 = v2m


									NURBSiem%intelem(fm)%face%edges(km)%IP_Local(1) = I_Comb_Tet(1,jm)
									NURBSiem%intelem(fm)%face%edges(km)%IP_Local(2) = I_Comb_Tet(2,jm)

								END IF


							END DO


						END DO
						NURBSiem%isValid = .TRUE. 
					END IF
					

				END IF

				

			END IF

		END IF

		

		IF (NURBSiem%isValid) THEN
			!Here we need to check element validity
			!Do this carefully, check each case from before

			DO fm = 1,NURBSiem%NB_Int_Elem
				IF (.NOT.isIntElemValid(NURBSiem%intelem(fm))) THEN
					NURBSiem%isValid = .FALSE.
					EXIT
				END IF
			END DO

		END IF

		

	END FUNCTION makeNURBSElement

	FUNCTION isIntElemValid(intelemi) RESULT(validi)
		USE NEFEM_Data
		USE common_Parameters

		!Function checks to see if an integration element is valid
		TYPE(IntElement) :: intelemi
		LOGICAL :: validi
		REAL*8 :: P1i(3), P2i(3), P3i(3), P4i(3), voli, Positi(3,4), ParamPos(2,3), tmi
		REAL*8 :: Pmidi(3)
		REAL*8 :: allNodesi(3,7), c1i, c2i, cii, u1i, u2i, v1i, v2i, uii, vii, UVmi(2)
		INTEGER :: facei, ii, si, jmi, triI(3,6), ji

		triI(1,1) = 1
		triI(2,1) = 6
		triI(3,1) = 7

		triI(1,2) = 6
		triI(2,2) = 2
		triI(3,2) = 7

		triI(1,3) = 2
		triI(2,3) = 4
		triI(3,3) = 7

		triI(1,4) = 7
		triI(2,4) = 4
		triI(3,4) = 3

		triI(1,5) = 7
		triI(2,5) = 3
		triI(3,5) = 5

		triI(1,6) = 7
		triI(2,6) = 5
		triI(3,6) = 1

		!Assume innocent until proven guilty
		validi = .TRUE.

		P1i(:) = intelemi%Posit(:,1)
		P2i(:) = intelemi%Posit(:,2)
		P3i(:) = intelemi%Posit(:,3)
		P4i(:) = intelemi%Posit(:,4)

		voli = Geo3D_Tet_Volume(P1i,P2i,P3i,P4i)

		IF (voli.LE.TinyVolume) THEN
			validi = .FALSE.
			RETURN
		END IF 
		
		!Now to check for self intersection
		IF (intelemi%NB_N_Face.EQ.1) THEN
			!1) Find which face is the NURBS face
			DO ii = 1,4
				IF (intelemi%isNURBFace(ii).EQ.1) THEN
					facei = ii
				END IF
			END DO

			!2) Copy original nodes 
			Positi = intelemi%Posit

			!3) Now the face nodes
			DO ii = 1,3
				allNodesi(:,ii) = Positi(:,intelemi%face%IP_Local(ii))
				
			END DO

			!4) Now the edges
			DO ii = 1,3
				IF (intelemi%face%edges(ii)%onCurve) THEN
					c1i = intelemi%face%edges(ii)%c1
					c2i = intelemi%face%edges(ii)%c2
					cii = 0.5d0*(c1i+c2i)
					CALL OCCT_GetLineXYZFromT(intelemi%face%edges(ii)%curveId,cii,allNodesi(:,ii+3))
				ELSE
					!Find the local node number

					u1i = intelemi%face%edges(ii)%u1
					u2i = intelemi%face%edges(ii)%u2
					v1i = intelemi%face%edges(ii)%v1
					v2i = intelemi%face%edges(ii)%v2

					si = intelemi%face%edges(ii)%surfaceId

					uii = 0.5d0*(u1i+u2i)
					vii = 0.5d0*(v1i+v2i)

					CALL GetUVPointInfo0(si,uii,vii,allNodesi(:,ii+3))
									
				END IF
				
			END DO

			!5) And the central point
			uii = 0.0d0
			vii = 0.0d0
			UVmi(:) = 0.0d0
			DO ii = 1,3
				CALL ProjectToRegionFromXYZ(si, allNodesi(:,ii), UVmi, TinySize, 1, tmi, P1i)
				uii = uii + UVmi(1)
				vii = vii + UVmi(2)
			END DO

			uii = uii/3.0d0
			vii = vii/3.0d0
			CALL GetUVPointInfo0(si,uii,vii,allNodesi(:,7))



			!Now to test the volumes
			DO ii = 1,6
				

				P1i(:) = allNodesi(:,triI(1,ii))
				P2i(:) = allNodesi(:,triI(2,ii))
				P3i(:) = allNodesi(:,triI(3,ii))
				P4i(:) = Positi(:,facei)

				voli = Geo3D_Tet_Volume(P1i,P2i,P3i,P4i)

				IF (voli.LE.TinyVolume) THEN
					validi = .FALSE.
					RETURN
				END IF 

			END DO

		ELSE

			!Single NURBS edge
			IF (intelemi%edge%onCurve) THEN
				c1i = intelemi%edge%c1
				c2i = intelemi%edge%c2
				cii = 0.5d0*(c1i+c2i)
				CALL OCCT_GetLineXYZFromT(intelemi%edge%curveId,cii,Pmidi)

			ELSE
				u1i = intelemi%edge%u1
				u2i = intelemi%edge%u2
				v1i = intelemi%edge%v1
				v2i = intelemi%edge%v2

				si = intelemi%edge%surfaceId

				uii = 0.5d0*(u1i+u2i)
				vii = 0.5d0*(v1i+v2i)

				CALL GetUVPointInfo0(si,uii,vii,Pmidi)

			END IF

			DO ii = 1,2
				!2) Copy original nodes 
				Positi = intelemi%Posit
				Positi(:,intelemi%edge%IP_Local(ii)) = Pmidi


				P1i(:) = intelemi%Posit(:,1)
				P2i(:) = intelemi%Posit(:,2)
				P3i(:) = intelemi%Posit(:,3)
				P4i(:) = intelemi%Posit(:,4)

				voli = Geo3D_Tet_Volume(P1i,P2i,P3i,P4i)

				

				IF (voli.LE.TinyVolume) THEN
					validi = .FALSE.
					RETURN
				END IF 

			END DO


		END IF

		!Need to add intersection test with original geometry


	END FUNCTION isIntElemValid

	FUNCTION buildNURBSvis(NURBSb) RESULT(surfvisb)

		USE NEFEM_Data
		USE SurfaceMeshStorage
		USE SurfaceMeshManager

		!This function builds a surface mesh to visualise the NURBS elements
		!in the array NURBSb
		TYPE(NURBSElement) :: NURBSb(NB_Tet)
		TYPE(SurfaceMeshStorageType) :: surfvisb
		INTEGER :: numSampb, ieb, ib, jb, kb, volb
		TYPE(IntElement) :: intElm
		REAL*8  :: P1b(3), P2b(3), u1b, u2b, c1b, c2b, cTb, v1b, v2b, uTb, vTb, uvTb(2)
		REAL*8  :: uMinm,vMinm,uMaxm,vMaxm

		WRITE(*,*) 'Building visualisation'
		!Select how many samples to do per NURBS edge
		numSampb = 10

		!Now do an initial loop to estimate number of points for surface allocation
		surfvisb%NB_Point = 0

		DO ieb = 1,NB_Tet
			IF (NURBSb(ieb)%NB_Int_Elem.GT.0) THEN
				DO ib = 1,NURBSb(ieb)%NB_Int_Elem
					surfvisb%NB_Point = surfvisb%NB_Point &
					   + NURBSb(ieb)%intelem(ib)%NB_N_Face*6*numSampb &
					   + NURBSb(ieb)%intelem(ib)%NB_N_Edge*numSampb + 1
				END DO
			END IF
		END DO

		CALL allc_Surf(surfvisb,surfvisb%NB_Point,surfvisb%NB_Point,surfvisb%NB_Point)

		surfvisb%NB_Point = 0
		surfvisb%NB_Tri = 0
		surfvisb%NB_Surf = 1 + NumRegions + NumCurves

		!Now the actual plotting
		DO ieb = 1,NB_Tet
			IF ((NURBSb(ieb)%NB_Int_Elem.GT.1).AND.(NURBSb(ieb)%isValid)) THEN !TODO - should be GT 0, set this to help with debugging
				DO ib = 1,NURBSb(ieb)%NB_Int_Elem

					intElm = NURBSb(ieb)%intelem(ib)

					IF (intElm%NB_N_Face.EQ.0) THEN
						DO jb = 1,6

							IF (intElm%isNURBEdge(jb).EQ.0) THEN

								P1b(:) = intElm%Posit(:,I_Comb_Tet(1,jb))
								P2b(:) = intElm%Posit(:,I_Comb_Tet(2,jb)) 

								surfvisb%NB_Point = surfvisb%NB_Point + 1
								surfvisb%Posit(:,surfvisb%NB_Point) = P1b(:)

								surfvisb%NB_Point = surfvisb%NB_Point + 1
								surfvisb%Posit(:,surfvisb%NB_Point) = P2b(:)

								surfvisb%NB_Tri = surfvisb%NB_Tri + 1

								surfvisb%IP_Tri(1,surfvisb%NB_Tri) = surfvisb%NB_Point - 1
								surfvisb%IP_Tri(2,surfvisb%NB_Tri) = surfvisb%NB_Point
								surfvisb%IP_Tri(3,surfvisb%NB_Tri) = surfvisb%NB_Point

								surfvisb%IP_Tri(4,surfvisb%NB_Tri) = 0
								surfvisb%IP_Tri(5,surfvisb%NB_Tri) = 1

							ELSE
								IF (intElm%edge%onCurve) THEN
									!Need to sample the curve
									c1b = intElm%edge%c1
									c2b = intElm%edge%c2

									CALL OCCT_GetLineXYZFromT(intElm%edge%curveId,c1b,P1b)

									surfvisb%NB_Point = surfvisb%NB_Point + 1
									surfvisb%Posit(:,surfvisb%NB_Point) = P1b(:)

									DO kb = 2,numSampb
										cTb = c1b + (REAL(kb-1)/REAL(numSampb-1))*(c2b-c1b)

										CALL OCCT_GetLineXYZFromT(intElm%edge%curveId,cTb,P1b)

										surfvisb%NB_Point = surfvisb%NB_Point + 1
										surfvisb%Posit(:,surfvisb%NB_Point) = P1b(:)

										surfvisb%NB_Tri = surfvisb%NB_Tri + 1

										surfvisb%IP_Tri(1,surfvisb%NB_Tri) = surfvisb%NB_Point - 1
										surfvisb%IP_Tri(2,surfvisb%NB_Tri) = surfvisb%NB_Point
										surfvisb%IP_Tri(3,surfvisb%NB_Tri) = surfvisb%NB_Point

										surfvisb%IP_Tri(4,surfvisb%NB_Tri) = 0
										surfvisb%IP_Tri(5,surfvisb%NB_Tri) = 1 + NumRegions + intElm%edge%curveId

									END DO
								ELSE
									!Need to sample the surface
									u1b = intElm%edge%u1
									u2b = intElm%edge%u2
									v1b = intElm%edge%v1
									v2b = intElm%edge%v2

									uvTb(1) = u1b
									uvTb(2) = v1b
									CALL GetUVPointInfo0(intElm%edge%surfaceId,uvTb(1),uvTb(2),P1b)
									

									surfvisb%NB_Point = surfvisb%NB_Point + 1
									surfvisb%Posit(:,surfvisb%NB_Point) = P1b(:)

									

									DO kb = 2,numSampb
										uvTb(1) = u1b + (REAL(kb-1)/REAL(numSampb-1))*(u2b-u1b)
										uvTb(2) = v1b + (REAL(kb-1)/REAL(numSampb-1))*(v2b-v1b)

										CALL GetUVPointInfo0(intElm%edge%surfaceId,uvTb(1),uvTb(2),P1b)

										WRITE(89,*) uvTb(1), uvTb(2)

										surfvisb%NB_Point = surfvisb%NB_Point + 1
										surfvisb%Posit(:,surfvisb%NB_Point) = P1b(:)

										surfvisb%NB_Tri = surfvisb%NB_Tri + 1

										surfvisb%IP_Tri(1,surfvisb%NB_Tri) = surfvisb%NB_Point - 1
										surfvisb%IP_Tri(2,surfvisb%NB_Tri) = surfvisb%NB_Point
										surfvisb%IP_Tri(3,surfvisb%NB_Tri) = surfvisb%NB_Point

										surfvisb%IP_Tri(4,surfvisb%NB_Tri) = 0
										surfvisb%IP_Tri(5,surfvisb%NB_Tri) = 1 + intElm%edge%surfaceId


									END DO


								END IF
							END IF

						END DO


					ELSE
						
						! DO jb = 1,6
						! 	IF (intElm%isNURBEdge(jb).EQ.0) THEN

						! 		P1b(:) = intElm%Posit(:,I_Comb_Tet(1,jb))
						! 		P2b(:) = intElm%Posit(:,I_Comb_Tet(2,jb)) 

						! 		surfvisb%NB_Point = surfvisb%NB_Point + 1
						! 		surfvisb%Posit(:,surfvisb%NB_Point) = P1b(:)

						! 		surfvisb%NB_Point = surfvisb%NB_Point + 1
						! 		surfvisb%Posit(:,surfvisb%NB_Point) = P2b(:)

						! 		surfvisb%NB_Tri = surfvisb%NB_Tri + 1

						! 		surfvisb%IP_Tri(1,surfvisb%NB_Tri) = surfvisb%NB_Point - 1
						! 		surfvisb%IP_Tri(2,surfvisb%NB_Tri) = surfvisb%NB_Point
						! 		surfvisb%IP_Tri(3,surfvisb%NB_Tri) = surfvisb%NB_Point

						! 		surfvisb%IP_Tri(4,surfvisb%NB_Tri) = 0
						! 		surfvisb%IP_Tri(5,surfvisb%NB_Tri) = ieb
						! 	END IF
						! END DO

						!Find the volume node and record it
						DO jb = 1,4
							IF (intElm%isNURBFace(jb).EQ.1) THEN
								surfvisb%NB_Point = surfvisb%NB_Point + 1
								surfvisb%Posit(:,surfvisb%NB_Point) = intElm%Posit(:,jb)
								volb = surfvisb%NB_Point
								EXIT
							END IF
						END DO

						DO jb = 1,3
							IF (intElm%face%edges(jb)%onCurve) THEN
									!Need to sample the curve
									c1b = intElm%face%edges(jb)%c1
									c2b = intElm%face%edges(jb)%c2

									CALL OCCT_GetLineXYZFromT(intElm%face%edges(jb)%curveId,c1b,P1b)

									surfvisb%NB_Point = surfvisb%NB_Point + 1
									surfvisb%Posit(:,surfvisb%NB_Point) = P1b(:)

									DO kb = 2,numSampb
										cTb = c1b + (REAL(kb-1)/REAL(numSampb-1))*(c2b-c1b)

										CALL OCCT_GetLineXYZFromT(intElm%face%edges(jb)%curveId,cTb,P1b)

										surfvisb%NB_Point = surfvisb%NB_Point + 1
										surfvisb%Posit(:,surfvisb%NB_Point) = P1b(:)

										surfvisb%NB_Tri = surfvisb%NB_Tri + 1

										surfvisb%IP_Tri(1,surfvisb%NB_Tri) = surfvisb%NB_Point - 1
										surfvisb%IP_Tri(2,surfvisb%NB_Tri) = surfvisb%NB_Point
										surfvisb%IP_Tri(3,surfvisb%NB_Tri) = volb

										surfvisb%IP_Tri(4,surfvisb%NB_Tri) = 0
										surfvisb%IP_Tri(5,surfvisb%NB_Tri) = 1 + NumRegions + intElm%face%edges(jb)%curveId

									END DO
								ELSE
									!Need to sample the surface
									u1b = intElm%face%edges(jb)%u1
									u2b = intElm%face%edges(jb)%u2
									v1b = intElm%face%edges(jb)%v1
									v2b = intElm%face%edges(jb)%v2

									uvTb(1) = u1b
									uvTb(2) = v1b
									CALL GetUVPointInfo0(intElm%face%edges(jb)%surfaceId,uvTb(1),uvTb(2),P1b)
									

									surfvisb%NB_Point = surfvisb%NB_Point + 1
									surfvisb%Posit(:,surfvisb%NB_Point) = P1b(:)

									

									DO kb = 2,numSampb
										uvTb(1) = u1b + (REAL(kb-1)/REAL(numSampb-1))*(u2b-u1b)
										uvTb(2) = v1b + (REAL(kb-1)/REAL(numSampb-1))*(v2b-v1b)

										CALL GetUVPointInfo0(intElm%face%edges(jb)%surfaceId,uvTb(1),uvTb(2),P1b)

										WRITE(89,*) uvTb(1), uvTb(2)

										surfvisb%NB_Point = surfvisb%NB_Point + 1
										surfvisb%Posit(:,surfvisb%NB_Point) = P1b(:)

										surfvisb%NB_Tri = surfvisb%NB_Tri + 1

										surfvisb%IP_Tri(1,surfvisb%NB_Tri) = surfvisb%NB_Point - 1
										surfvisb%IP_Tri(2,surfvisb%NB_Tri) = surfvisb%NB_Point
										surfvisb%IP_Tri(3,surfvisb%NB_Tri) = volb

										surfvisb%IP_Tri(4,surfvisb%NB_Tri) = 0
										surfvisb%IP_Tri(5,surfvisb%NB_Tri) = 1 + intElm%face%edges(jb)%surfaceId


									END DO


								END IF


						END DO


					END IF

				END DO
			END IF
		END DO


	END FUNCTION buildNURBSvis


END SUBROUTINE NEFEM_Gen
