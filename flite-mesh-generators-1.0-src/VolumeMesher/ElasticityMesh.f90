!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


SUBROUTINE Build_ElasticityMesh( )
  USE HybridMeshHighOrder
  USE SurfaceMeshHighOrder
  USE common_Constants
  USE common_Parameters
  IMPLICIT NONE

  INTEGER :: IT, IB, IP, ND, NPF, NP, nt, i, k, iqqt(10), itm, nblock
  REAL*8  :: Vamm, Vamst, Vammin
  REAL*8  :: Pts(3,10), ppts(3,20)
  INTEGER, DIMENSION(:), POINTER :: markp, markt, markpt, BLOCK
  TYPE(HybridMeshStorageType)  :: eMesh, aMesh  
  TYPE(HighOrderCellType)      :: Cell, face
  TYPE(SurfaceMeshStorageType) :: Scell
  LOGICAL :: CarryOn


  !--- take the whole mesh.
  MeshOut%GridOrder = GridOrder
  MeshOut%NB_Point  = NB_Point
  MeshOut%NB_Tet    = NB_Tet
  ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
  ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
  MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)  
  MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)

  !CALL DeAllocate_Tet( )
  CALL DeAllocate_Point( )

  IF(GridOrder >= HighOrder_Model)THEN
     CALL SurfaceMeshStorage_Clear(Surf)
     Surf%NB_Tri   =  NB_BD_Tri
     Surf%NB_Point =  NB_BD_Point
     Surf%NB_Psp   =  0
     Surf%IP_Tri   => IP_BD_Tri
     RETURN
  ENDIF

  GridOrder   = HighOrder_Model
  Cell        = HighOrderCell_getHighCell(GridOrder,3,0)
  face        = HighOrderCell_getHighCell(GridOrder,2,0)
  ND          = Cell%numCellNodes        !--- number of nodes on a tet.
  NPF         = face%numCellNodes        !--- number of nodes on a tri.
  numTetNodes = ND
  WRITE(*,*)'Linear Meshout%NB_Tet, _Point=',Meshout%NB_Tet, Meshout%NB_Point



  IF(NB_BD_Layers>0)THEN
     CALL SurfaceMeshStorage_Input(JobName(1:JobNameLength)//'_v2',JobNameLength+3,Surf)
  ELSE
     CALL SurfaceMeshStorage_Input(JobName,JobNameLength,Surf)
  ENDIF

  if(NB_BD_Tri>0)then
  IF(Surf%NB_Tri/=NB_BD_Tri) CALL Error_Stop(' Build_HighOrder::Surf%NB_Tri/=NB_BD_Tri ')
  DO IB = 1, Surf%NB_Tri
     i = which_RotationinTri (Surf%IP_Tri(1:3,IB), IP_BD_Tri(1:3,IB))
     IF(i==0) CALL Error_Stop ('Build_HighOrder :: wrong triangle')
     Surf%IP_Tri(  1:3,  IB) = Surf%IP_Tri(iRotate_Tri(:,i), IB)
     Surf%IPsp_Tri(1:10, IB) = Surf%IPsp_Tri(iRotate_C3Tri(:,i), IB)
     Surf%IP_Tri(  4,    IB) = IP_BD_Tri(4,IB)
  ENDDO
  else
     call HybridMesh_AssoBoundSurface(Meshout, Surf)
  endif


  CALL HybridMesh_getHighOrder(GridOrder, MeshOut) 
  CALL HybridMesh_setMovement(MeshOut, Surf)  
  CALL HybridMesh_setFrozen(MeshOut)


  IF(MeshOut%NB_Tet<=10000)THEN

     WRITE(*, *)' for elasticity NB_Tet, _Point, dbc=', Meshout%NB_Tet,Meshout%NB_Point,MeshOut%NB_DBC
     WRITE(29,*)' for elasticity NB_Tet, _Point, dbc=', Meshout%NB_Tet,Meshout%NB_Point,MeshOut%NB_DBC
     CALL  HybridMesh_linearSystemSolver(MeshOut, Material, 10)
  ELSE

     ALLOCATE(markp( MeshOut%NB_Point))

     !--- set surface piosition        
     DO I = 1, MeshOut%NB_DBC
        IP = MeshOut%IP_DBC(I)
        MeshOut%Posit(:,IP) = MeshOut%Posit(:,IP) + MeshOut%Pd_DBC(:,I)
     ENDDO

     !--- calc the Jacobian
     Vamst  = 0.2d0
     Vammin = 1.d0
     markp(:) = 0
     DO IT = 1, MeshOut%NB_Tet
        itm = IT
        CALL HybridMesh_calcQuality(MeshOut, 0, itm, Vamm)
        Vammin = MIN(Vamm, Vammin)
        IF(Vamm<Vamst)THEN
           markp(MeshOut%IP_Tet(1:4,IT)) = 1
        ENDIF
     ENDDO

     WRITE(*, *)'   the worst initial jaconbian: ', REAL(Vammin)
     WRITE(29,*)'   the worst initial jaconbian: ', REAL(Vammin)

     DO I = 1, MeshOut%NB_DBC
        IP = MeshOut%IP_DBC(I)
        MeshOut%Posit(:,IP) = MeshOut%Posit(:,IP) - MeshOut%Pd_DBC(:,I)
     ENDDO

     !--- extract the bad cells
     CALL HybridMesh_getHighOrder(GridOrder, aMesh) 
     ALLOCATE(aMesh%IP_Tet(ND, MeshOut%NB_Tet))
     ALLOCATE(aMesh%Posit(3, MeshOut%NB_Point))
     ALLOCATE(aMesh%IP_DBC(  MeshOut%NB_DBC))
     ALLOCATE(aMesh%Pd_DBC(3,MeshOut%NB_DBC))
     aMesh%NB_Point = MeshOut%NB_Point
     aMesh%NB_DBC   = MeshOut%NB_DBC
     aMesh%Posit(:, 1:MeshOut%NB_Point) = MeshOut%Posit(:, 1:MeshOut%NB_Point)
     aMesh%IP_DBC(  1:MeshOut%NB_DBC)   = MeshOut%IP_DBC(  1:MeshOut%NB_DBC) 
     aMesh%Pd_DBC(:,1:MeshOut%NB_DBC)   = MeshOut%Pd_DBC(:,1:MeshOut%NB_DBC) 

     ALLOCATE(markt( MeshOut%NB_Tet))
     ALLOCATE(markpt(MeshOut%NB_Point))

     !--- populate
     nt = 0     
     markt(:) = 0
     DO k = 1, 2
        DO IT = 1, MeshOut%NB_Tet
           IF(markt(it)>0) CYCLE
           DO i = 1, 4
              IF(markp(MeshOut%IP_Tet(i,IT))==k)THEN
                 nt = nt + 1
                 aMesh%IP_Tet(:,nt) = MeshOut%IP_Tet(:,IT)
                 markt(it) = nt
                 WHERE(markp(MeshOut%IP_Tet(1:4,IT))==0) markp(MeshOut%IP_Tet(1:4,IT)) = k+1
                 EXIT
              ENDIF
           ENDDO
        ENDDO
     ENDDO
     aMesh%NB_Tet = nt

     CALL HybridMesh_CleanNodes(aMesh, markpt)  
     WRITE(*, *)' for elasticity NB_Tet, _Point=',aMesh%NB_Tet, aMesh%NB_Point
     WRITE(29,*)' for elasticity NB_Tet, _Point=',aMesh%NB_Tet, aMesh%NB_Point

     !--- frozen boundary
     CALL HybridMesh_setFrozen(aMesh)

     ALLOCATE(BLOCK( aMesh%NB_Tet))

     CALL HybridMesh_getHighOrder(GridOrder, eMesh)
     ALLOCATE(eMesh%IP_Tet(ND,  aMesh%NB_Tet))
     ALLOCATE(eMesh%Posit( 3, aMesh%NB_Point))
     ALLOCATE(eMesh%IP_DBC(   aMesh%NB_Point))
     ALLOCATE(eMesh%Pd_DBC(3, aMesh%NB_Point))

     nblock = 1
     IF( aMesh%NB_Tet>10000)THEN
        CALL HybridMesh_DBCblock(aMesh, nblock, BLOCK)
        IF(Debug_Display>=2)THEN
        do it = 1, aMesh%NB_Tet
          if(block(it)<=0)then
             write(*, *)' Error--- a lonely cell here: it=',it
             write(29,*)' Error--- a lonely cell here: it=',it
             call Error_Stop('Build_ElasticityMesh::')
          endif
        enddo
        ENDIF 
     ELSE
        BLOCK(:) = 1
     ENDIF


     DO k = 1, nblock
        WRITE(*, *)' block ',k, ' out of ', nblock
        WRITE(29,*)' block ',k, ' out of ', nblock
        eMesh%NB_Point = aMesh%NB_Point
        eMesh%NB_DBC   = aMesh%NB_DBC
        eMesh%Posit(:, 1:eMesh%NB_Point) = aMesh%Posit(:, 1:eMesh%NB_Point)
        eMesh%IP_DBC(  1:eMesh%NB_DBC)   = aMesh%IP_DBC(  1:eMesh%NB_DBC) 
        eMesh%Pd_DBC(:,1:eMesh%NB_DBC)   = aMesh%Pd_DBC(:,1:eMesh%NB_DBC) 

        nt = 0
        DO IT = 1, aMesh%NB_Tet
           IF(BLOCK(IT)==k)THEN
              nt = nt + 1
              eMesh%IP_Tet(:,nt) = aMesh%IP_Tet(:,IT)
           ENDIF
        ENDDO
        eMesh%NB_Tet = nt

        CALL HybridMesh_CleanNodes(eMesh, markp)  
        CALL HybridMesh_setFrozen(eMesh)

        WRITE(*, *)' block NB_Tet, _Point, dbc=',eMesh%NB_Tet, eMesh%NB_Point, eMesh%NB_DBC
        WRITE(29,*)' block NB_Tet, _Point, dbc=',eMesh%NB_Tet, eMesh%NB_Point, eMesh%NB_DBC
        CALL HybridMesh_linearSystemSolver(eMesh, Material, 10)
        DO i = 1, aMesh%NB_Point
           ip = markp(i)
           IF(ip==0) CYCLE
           aMesh%Posit(:,i) = eMesh%Posit(:,ip)
        ENDDO
        !--- some DBC nodes might affect two or more blocks.
        DO I = 1, aMesh%NB_DBC
           IP =aMesh%IP_DBC(I)
           IF(markp(IP)>0) THEN
              aMesh%Posit(:,IP) = aMesh%Posit(:,IP) - aMesh%Pd_DBC(:,I)
           ENDIF
        ENDDO

     ENDDO

     DO I = 1, aMesh%NB_DBC
        IP =aMesh%IP_DBC(I)
        aMesh%Posit(:,IP) = aMesh%Posit(:,IP) + aMesh%Pd_DBC(:,I)
     ENDDO


     DO ip = 1, MeshOut%NB_Point
        np = markpt(ip)
        IF(np==0) CYCLE
        MeshOut%Posit(:,ip) = aMesh%Posit(:,np)
     ENDDO
     DO I = 1, MeshOut%NB_DBC
        IP = MeshOut%IP_DBC(I)
        IF(markpt(IP)>0) CYCLE
        MeshOut%Posit(:,IP) = MeshOut%Posit(:,IP) + MeshOut%Pd_DBC(:,I)
     ENDDO

     DEALLOCATE( markp, markt, markpt)
     CALL HybridMeshStorage_Clear(aMesh)
     CALL HybridMeshStorage_Clear(eMesh)

  ENDIF

  IF(Debug_Display>=1)THEN
     !--- compute and compare the Jacobians at the Gauss points
     it = 0
     CALL HybridMesh_calcQuality(MeshOut, 0, it, Vamm)

     WRITE(*, *)' The worst tetrahedron is at IT=', it, ' with Vab=',real(Vamm)
     WRITE(29,*)' The worst tetrahedron is at IT=', it, ' with Vab=',real(Vamm)
     DO i=1,4
        iqqt = MeshOut%IP_Tet(iTri_C3Tet(:,i),it)
        Pts(:,1:10) = MeshOut%Posit(:,iqqt)
        CALL CubicTriFace_Display2 (Pts, 27, 40+i, 1)
     ENDDO
     ppts = MeshOut%Posit(:, MeshOut%IP_Tet(:,it))
     CALL HighOrderCell_TetSurface (MeshOut%Cell, ppts, 9, Scell)
     CALL SurfaceMeshStorage_Output ('worstcell', 9, Scell)
  ENDIF


END SUBROUTINE Build_ElasticityMesh



SUBROUTINE VisLayer_ElasticityMesh(lboun, layer, ipOrig, newLayer )
  USE HybridMeshHighOrder
  USE common_Parameters
  IMPLICIT NONE

  INTEGER,INTENT(IN) :: lboun(*), layer(*), ipOrig(*), newLayer
  INTEGER :: IT, IB, IP, ND, NPF, NPL, NP, nt, i, j, k, is, itm, nblock, Isucc
  INTEGER :: NP0, Nadd, NT0, ip3(3), VMGridOrder, iq, ippt(1000), n1, n2, n3, edgeNodes(100,6)
  REAL*8  :: Vamm, Vamst, Vammin
  REAL*8  :: Pts(3,10), Pvs(3,3), ppts(3,100)
  INTEGER, DIMENSION(:), POINTER  :: markp, markt, markpt, marklay, BLOCK, markdbc
  TYPE(HybridMeshStorageType)  :: eMesh, aMesh
  TYPE(HighOrderCellType)      :: Cell, face, line
  TYPE(SurfaceMeshStorageType) :: Scell
  LOGICAL :: CarryOn

  VMGridOrder = HighOrder_Model
  Cell        = HighOrderCell_getHighCell(VMGridOrder,3,0)
  face        = HighOrderCell_getHighCell(VMGridOrder,2,0)
  line        = HighOrderCell_getHighCell(VMGridOrder,1,0)
  ND          = Cell%numCellNodes        !--- number of nodes on a tet.
  NPF         = face%numCellNodes        !--- number of nodes on a tri.
  NPL         = line%numCellNodes        !--- number of nodes on a edge.
  CALL HighOrderCell_getEdgeNodes_TET (VMGridOrder, edgeNodes)

  IF(newLayer==1)THEN

     CALL HybridMesh_AssoBoundSurface(VM, Surf)

     !--- take the whole mesh.
     NT0 = 0
     NP0 = 0
     VMHO%GridOrder = 1
     VMHO%NB_Point  = VM%NB_Point
     VMHO%NB_Tet    = VM%NB_Tet
     ALLOCATE(VMHO%IP_Tet(ND,      NB_BD_Layers * VMHO%NB_Tet))
     ALLOCATE(VMHO%Posit(3,   ND * NB_BD_Layers * VMHO%NB_Tet))
     VMHO%IP_Tet(1:4,1:VM%NB_Tet)   = VM%IP_Tet(1:4,1:VM%NB_Tet)  
     VMHO%Posit(:,   1:VM%NB_Point) = VM%Posit(:,   1:VM%NB_Point)

     CALL HybridMesh_getHighOrder(VMGridOrder, VMHO) 
     CALL HybridMesh_setMovement(VMHO, Surf)  
     !--- set surface piosition        
     DO I = 1, VMHO%NB_DBC
        IP = VMHO%IP_DBC(I)
        VMHO%Posit(:,IP) = VMHO%Posit(:,IP) + VMHO%Pd_DBC(:,I)
     ENDDO

  ELSE

     NT0  = VMHO%NB_Tet
     NP0  = VMHO%NB_Point - VMHO%NB_Psp
     Nadd = VM%NB_Point - NP0
     DO IP = VMHO%NB_Point, NP0 + 1, -1
        VMHO%Posit(:,IP+Nadd) = VMHO%Posit(:,IP)
     ENDDO
     VMHO%Posit(:,NP0+1:VM%NB_Point) = VM%Posit(:,NP0+1:VM%NB_Point)
     VMHO%NB_Point = VMHO%NB_Point + Nadd

     VMHO%IP_Tet(5:,1:VMHO%NB_Tet) = VMHO%IP_Tet(5:,1:VMHO%NB_Tet) + Nadd
     DO it = NT0+1, VM%NB_Tet
        VMHO%IP_Tet(1:4,it) = VM%IP_Tet(1:4,it)
        VMHO%IP_Tet(5,it)   = 0
     ENDDO
     VMHO%NB_Tet = VM%NB_Tet

     CALL HybridMesh_LinearFill(VMHO, 0)

  ENDIF

  VMHO%NB_Psp = VMHO%NB_Point - VM%NB_Point
  WRITE(*,*) '......... High-order mesh: NB_Tet,_point=',VMHO%NB_Tet, VMHO%NB_Point

  ALLOCATE(markp( VMHO%NB_Point))
  markp(:)  = 0

  IF(NB_SymPlane>0)THEN
     !--- for symmetry plane
     CALL ElasticityMesh_SymmLayer()
  ENDIF


  !--- calc the Jacobian from this layer
  Vamst  = 0.2d0
  Vammin = 1.d0
  markp(:) = 0
  CarryOn  = .FALSE.
  DO IT = NT0+1, VMHO%NB_Tet
     IF(MAXVAL(VMHO%IP_Tet(1:4,IT))<=NP0) CYCLE
     itm = IT
     CALL HybridMesh_calcQuality(VMHO, 0, itm, Vamm)
     Vammin = MIN(Vamm, Vammin)
     IF(Vamm<Vamst)THEN
        markp(VMHO%IP_Tet(1:4,IT)) = 1
        CarryOn = .TRUE.
     ENDIF
  ENDDO


  WRITE(*, *)'            the worst initial jaconbian: ', REAL(Vammin)
  WRITE(29,*)'            the worst initial jaconbian: ', REAL(Vammin)
  IF(.NOT. CarryOn)THEN
     VM%Posit(:,NP0+1:VM%NB_Point) = VMHO%Posit(:,NP0+1:VM%NB_Point)
     DEALLOCATE(markp)
     GOTO 100
  ENDIF

  !--- extract the bad cells
  CALL HybridMesh_getHighOrder(VMGridOrder, aMesh)
  ALLOCATE(aMesh%IP_Tet(ND,  VMHO%NB_Tet))
  ALLOCATE(aMesh%Posit( 3, VMHO%NB_Point))
  aMesh%NB_Point  = VMHO%NB_Point
  aMesh%NB_DBC    = 0
  aMesh%Posit(:, 1:VMHO%NB_Point) = VMHO%Posit(:, 1:VMHO%NB_Point)

  ALLOCATE(markt( VMHO%NB_Tet))
  ALLOCATE(markpt(VMHO%NB_Point))

  !--- populate
  markt(:) = 0
  DO k = 1, 2
     DO IT = NT0+1, VMHO%NB_Tet
        IF(markt(it)>0) CYCLE
        DO i = 1, 4
           IF(markp(VMHO%IP_Tet(i,IT))==k)THEN
              markt(it) = 1
              WHERE(markp(VMHO%IP_Tet(1:4,IT))==0) markp(VMHO%IP_Tet(1:4,IT)) = k+1
              EXIT
           ENDIF
        ENDDO
     ENDDO
  ENDDO

  nt = 0     
  DO IT = NT0+1, VMHO%NB_Tet
     IF(markt(it)==0) CYCLE
     nt = nt + 1
     aMesh%IP_Tet(:,nt) = VMHO%IP_Tet(:,IT)
  ENDDO
  aMesh%NB_Tet = nt

  CALL HybridMesh_CleanNodes(aMesh, markpt)  
  WRITE(*, *)' for elasticity NB_Tet, _Point=',aMesh%NB_Tet, aMesh%NB_Point
  WRITE(29,*)' for elasticity NB_Tet, _Point=',aMesh%NB_Tet, aMesh%NB_Point

  ALLOCATE(marklay(aMesh%NB_Point))


  !--- copy layer number
  marklay(:) = 0
  DO IP = 1, VMHO%NB_Point - VMHO%NB_Psp
        iq = markpt(ip)
        if(iq==0) cycle
        if(iq>ip) stop 'fdslfj9023u4-9'
        markp(iq) = markp(ip)
        IF(layer(ip)==newLayer)THEN
           marklay(iq) = ipOrig(ip)
        ELSE
           marklay(iq) = -ipOrig(ip)
        ENDIF
  ENDDO
  

  CALL HybridMesh_BuildNext(amesh)

  !--- set DBC

  ALLOCATE(aMesh%IP_DBC(   amesh%NB_Point))
  ALLOCATE(aMesh%Pd_DBC(3, amesh%NB_Point))
  ALLOCATE(markdbc(amesh%NB_Point))

  amesh%NB_DBC = 0
  markdbc(:) = 0
  IF(NB_SymPlane>0)THEN
     !--- fro symmtry plane
     DO I = 1, VMHO%NB_DBC
        IP = markpt(VMHO%IP_DBC(I))
        IF(IP==0) CYCLE
        markdbc(IP) = 1
        amesh%NB_DBC = amesh%NB_DBC + 1
        amesh%IP_DBC(  amesh%NB_DBC) = IP
        amesh%Pd_DBC(:,amesh%NB_DBC) = VMHO%PD_DBC(:,I)
        aMesh%Posit(:,IP) = aMesh%Posit(:,IP) - VMHO%PD_DBC(:,I)
     ENDDO
  ENDIF
  !--- for previous surface (populating boundary)
  DO IT = 1, aMesh%NB_Tet
     IF(marklay(aMesh%IP_Tet(1,IT))==0) STOP 'dskljf0v8rhg0249'
     IF(marklay(aMesh%IP_Tet(2,IT))==0) STOP 'dskljf0v8rhg0249'
     IF(marklay(aMesh%IP_Tet(3,IT))==0) STOP 'dskljf0v8rhg0249'
     IF(marklay(aMesh%IP_Tet(4,IT))==0) STOP 'dskljf0v8rhg0249'
     DO j = 1, 4
        IF(aMesh%Next_Tet(j,IT)>0) CYCLE
        ip3(1:3) = aMesh%IP_Tet(Cell%facenodes(1:3,j),IT)
        IF(MAXVAL(marklay(ip3(1:3)))>0) CYCLE
        pvs(:,1:3) = aMesh%Posit(:,ip3(1:3))
        CALL HighOrderCell_linearIsoparTransf(face, pvs, pts )

        DO i = 1, NPF
           ip = aMesh%IP_Tet(Cell%facenodes(i,j),IT)
           IF(markdbc(ip)>0) CYCLE
           markdbc(ip) = 2
           amesh%NB_DBC = amesh%NB_DBC + 1
           amesh%IP_DBC(  amesh%NB_DBC) = ip
           amesh%Pd_DBC(:,amesh%NB_DBC) = aMesh%Posit(:,ip) - pts(:,i)
           aMesh%Posit(:,ip) = pts(:,i)
        ENDDO
     ENDDO
  ENDDO
  !--- for neighbouring surface
  DO IT = 1, aMesh%NB_Tet
     DO j = 1, 4
        IF(aMesh%Next_Tet(j,IT)>0) CYCLE
        ip3(1:3) = aMesh%IP_Tet(Cell%facenodes(1:3,j),IT)
        IF(MINVAL(marklay(ip3(1:3)))>0)   CYCLE     !--- free surface
        IF(MAXVAL(marklay(ip3(1:3)))<0)   CYCLE     !--- previous surface
        IF(MINVAL(markp(ip3(1:3)))<3)     CYCLE     !--- not the populating boundary
        DO i = 1, NPF
           ip = aMesh%IP_Tet(Cell%facenodes(i,j),IT)
           IF(markdbc(ip)>0) CYCLE
           markdbc(ip) = 3
           amesh%NB_DBC = amesh%NB_DBC + 1
           amesh%IP_DBC(  amesh%NB_DBC) = ip
           amesh%Pd_DBC(:,amesh%NB_DBC) = 0
        ENDDO
     ENDDO
  ENDDO
  !--- for vertical edges from trailing edges
  DO IT = 1, aMesh%NB_Tet
     DO j = 1, 6
        ippt(1:NPL) = aMesh%IP_Tet(edgeNodes(1:NPL,j), it)
        IF(marklay(ippt(1))/=-marklay(ippt(2))) CYCLE
        IF(SurfNode(abs(marklay(ippt(1))))%onRidge<=0) CYCLE
        DO i = 1, NPL
           ip = ippt(i)
           IF(markdbc(ip)>0) CYCLE
           markdbc(ip) = 4
           amesh%NB_DBC = amesh%NB_DBC + 1
           amesh%IP_DBC(  amesh%NB_DBC) = ip
           amesh%Pd_DBC(:,amesh%NB_DBC) = 0
        ENDDO
     enddo
  enddo

  DEALLOCATE(markdbc)

  ALLOCATE(BLOCK( amesh%NB_Tet))

  CALL HybridMesh_getHighOrder(VMGridOrder, eMesh)
  ALLOCATE(eMesh%IP_Tet(ND,  amesh%NB_Tet))
  ALLOCATE(eMesh%Posit( 3, amesh%NB_Point))
  ALLOCATE(eMesh%IP_DBC(   amesh%NB_Point))
  ALLOCATE(eMesh%Pd_DBC(3, amesh%NB_Point))

  nblock = 1
  IF( aMesh%NB_Tet>10000)THEN
     CALL HybridMesh_DBCblock(aMesh, nblock, BLOCK)
     IF(Debug_Display>=2)THEN
        do it=1,aMesh%NB_Tet
          if(block(it)<=0)then
             write(*, *)' Error--- a lonely cell here: it=',it
             write(29,*)' Error--- a lonely cell here: it=',it
             call Error_Stop('VisLayer_ElasticityMesh::')
          endif
        enddo
     ENDIF 
  ELSE
     BLOCK(:) = 1
  ENDIF

  DO k = 1, nblock
     WRITE(*, *)' block ',k, ' out of ', nblock
     WRITE(29,*)' block ',k, ' out of ', nblock
     eMesh%NB_Point = aMesh%NB_Point
     eMesh%NB_DBC   = aMesh%NB_DBC
     eMesh%Posit(:, 1:eMesh%NB_Point) = aMesh%Posit(:, 1:eMesh%NB_Point)
     eMesh%IP_DBC(  1:eMesh%NB_DBC)   = aMesh%IP_DBC(  1:eMesh%NB_DBC) 
     eMesh%Pd_DBC(:,1:eMesh%NB_DBC)   = aMesh%Pd_DBC(:,1:eMesh%NB_DBC) 

     nt = 0
     DO IT = 1, aMesh%NB_Tet
        IF(BLOCK(IT)==k)THEN
           nt = nt + 1
           eMesh%IP_Tet(:,nt) = aMesh%IP_Tet(:,IT)
        ENDIF
     ENDDO
     eMesh%NB_Tet = nt

     CALL HybridMesh_CleanNodes(eMesh, markp)  

     WRITE(*, *)' block NB_Tet, _Point, dbc=',eMesh%NB_Tet, eMesh%NB_Point, eMesh%NB_DBC
     WRITE(29,*)' block NB_Tet, _Point, dbc=',eMesh%NB_Tet, eMesh%NB_Point, eMesh%NB_DBC

     
  
     CALL HybridMesh_linearSystemSolver(eMesh, Material, 10)

     DO i = 1, aMesh%NB_Point
        ip = markp(i)
        IF(ip==0) CYCLE
        aMesh%Posit(:,i) = eMesh%Posit(:,ip)
     ENDDO
     !--- some DBC nodes might affect two or more blocks.
     DO I = 1, aMesh%NB_DBC
        IP =aMesh%IP_DBC(I)
        IF(markp(IP)>0) THEN
           aMesh%Posit(:,IP) = aMesh%Posit(:,IP) - aMesh%Pd_DBC(:,I)
        ENDIF
     ENDDO

  ENDDO

  
  DO I = 1, aMesh%NB_DBC
     IP =aMesh%IP_DBC(I)
     aMesh%Posit(:,IP) = aMesh%Posit(:,IP) + aMesh%Pd_DBC(:,I)
  ENDDO


  DO ip = NP0+1, VMHO%NB_Point
     np = markpt(ip)
     IF(np==0) CYCLE
     VMHO%Posit(:,ip) = aMesh%Posit(:,np)
  ENDDO

  VM%Posit(:, NP0+1:VM%NB_Point) = VMHO%Posit(:, NP0+1:VM%NB_Point)

  IF(Debug_Display>=1)THEN
     !--- compute and compare the Jacobians at the Gauss points
     it = 0
     CALL HybridMesh_calcQuality(aMesh, 0, it, Vamm)
     WRITE(*, *)' The worst tet. in this layer is IT=', it, ' with Vab=',real(Vamm)     
     WRITE(29,*)' The worst tet. in this layer is IT=', it, ' with Vab=',real(Vamm)    
     IF(Vamm<0)THEN
        WRITE(*, *)' ips =',aMesh%IP_Tet(1:4,it)
        WRITE(*, *)' orig=',marklay(aMesh%IP_Tet(1:4,it))
        ppts(:,1:ND) = aMesh%Posit(:, aMesh%IP_Tet(1:ND,it))
        CALL HighOrderCell_TetSurface (aMesh%Cell, ppts(:,1:ND), 3, Scell)
        CALL SurfaceMeshStorage_Output ('worstcell', 9, Scell)
        STOP '----negative jacobian'
     ENDIF
  ENDIF

  DEALLOCATE( markp, markt, markpt, marklay)
  CALL HybridMeshStorage_Clear(aMesh)
  CALL HybridMeshStorage_Clear(eMesh)

100  CONTINUE

  !--- copy VMHO to VM for after all
  !    also include those high-order nodes from Surf
  IF(newLayer==NB_BD_Layers)THEN
     n1 = VM%NB_Point
     n2 = VM%NB_Point   + Surf%NB_Psp
     n3 = VMHO%NB_Point + Surf%NB_Psp
     CALL HybridMeshStorage_Clear(VM)
     CALL HybridMesh_getHighOrder(VMGridOrder, VM)
     nadd         = VMHO%NB_Point - Surf%NB_Point
     VM%NB_Point  = VMHO%NB_Point + Surf%NB_Psp
     VM%NB_Psp    = -1             !--- should not be refer
     VM%NB_Tet    = VMHO%NB_Tet     
     ALLOCATE(VM%IP_Tet(ND,  VM%NB_Tet))
     ALLOCATE(VM%Posit(3,    VM%NB_Point+10000))
     VM%IP_Tet(1:4, 1:VM%NB_Tet)   = VMHO%IP_Tet(1:4, 1:VM%NB_Tet)  
     VM%IP_Tet(5:ND,1:VM%NB_Tet)   = VMHO%IP_Tet(5:ND,1:VM%NB_Tet)  + Surf%NB_Psp
     VM%Posit(:,     1 : n1) = VMHO%Posit(:, 1:n1)
     VM%Posit(:,  n1+1 : n2) = Surf%Posit(:, Surf%NB_Point+1 : Surf%NB_Point+Surf%NB_Psp)
     VM%Posit(:,  n2+1 : n3) = VMHO%Posit(:, n1+1:VMHO%NB_Point)

     n3 = n1-Surf%NB_Point
     DO ib = 1, Surf%NB_Tri
        WHERE(Surf%IPsp_Tri(4:, ib)>0) Surf%IPsp_Tri(4:, ib) = Surf%IPsp_Tri(4:, ib) + n3
     ENDDO

     DEALLOCATE(Surf%Posit)
     ALLOCATE(Surf%Posit(3,n2))
     Surf%Posit(:, 1:n2) = VM%Posit(:, 1:n2)
     CALL HybridMeshStorage_Clear(VMHO)
  ENDIF


CONTAINS

  SUBROUTINE ElasticityMesh_SymmLayer()
    USE SurfaceMeshHighOrder
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType) :: Symm
    REAL*8,  DIMENSION(:,:), POINTER :: Disp2D
    INTEGER, DIMENSION(:),   POINTER :: markDbc
    REAL*8 :: pvs2(2,2), pts2(2,100), pt1(3), pt2(3), ct(2)

    Symm%Cell       = HighOrderCell_getHighCell(VMGridOrder,2,0)
    Symm%GridOrder  = Symm%Cell%GridOrder

    ALLOCATE(Symm%IP_Tri(5,      VMHO%NB_Tet))
    ALLOCATE(Symm%IPsp_Tri(NPF,  VMHO%NB_Tet))
    ALLOCATE(Symm%Coord(2,       VMHO%NB_Point))
    ALLOCATE(Symm%IP_DBC(        VMHO%NB_Point))
    ALLOCATE(Symm%Pd_DBC(2,      VMHO%NB_Point))
    ALLOCATE(Disp2D(2,           VMHO%NB_Point))
    ALLOCATE(markDbc(            VMHO%NB_Point))

    !--- loop for each symmetry plane

    VMHO%NB_DBC = 0
    markDbc(:)  = 0

    DO is = 1, NB_SymPlane

       !--- extract surface for on symmtry planes for this layer

       Symm%NB_Point = VMHO%NB_Point
       Symm%NB_Tri   = 0
       DO IT = NT0+1, VMHO%NB_Tet
          DO i = 1,4
             ippt(1:NPF) = VMHO%IP_Tet(cell%faceNodes(:,i),IT)
             IF(MAXVAL(layer(ippt(1:3)))<newLayer) CYCLE
             IF(layer(ippt(1))==layer(ippt(2)) .AND. layer(ippt(1))==layer(ippt(3))) CYCLE
             IF(lboun(ippt(1))/=0 .AND. lboun(ippt(1))/=is) CYCLE
             IF(lboun(ippt(2))/=0 .AND. lboun(ippt(2))/=is) CYCLE
             IF(lboun(ippt(3))/=0 .AND. lboun(ippt(3))/=is) CYCLE

             Symm%NB_Tri = Symm%NB_Tri+1
             Symm%IPsp_Tri(1:NPF, Symm%NB_Tri) = ippt(1:NPF)
             Symm%IP_Tri(  1:3,   Symm%NB_Tri) = ippt(1:3)
             Symm%IP_Tri(  4:5,   Symm%NB_Tri) = (/ 0, is /)
          ENDDO
       ENDDO

       IF(Symm%NB_Tri==0) CYCLE
       
       !--- get 2d coordinates on the symmtry plane
       markp(:) = 0
       DO IB = 1, Symm%NB_Tri
          DO i = 1, NPF
             ip = Symm%IPsp_Tri(i, IB)
             IF(markp(ip)>0) CYCLE
             markp(ip) = 1
             Symm%Coord(:,ip) = Plane3D_project2D(SymPlane(is), VMHO%Posit(:,ip))   
          ENDDO
       ENDDO

       !--- set DBC conditions
       Symm%NB_DBC = 0
       markp(:) = 0
       DO IB = 1, Symm%NB_Tri
          DO j = 1, 3
             ippt(1:NPL) = Symm%IPsp_Tri(face%facenodes(1:NPL,j),IB)
             IF(layer(ippt(1))==newlayer .AND. lboun(ippt(1))/=0) CYCLE
             IF(layer(ippt(2))==newlayer .AND. lboun(ippt(2))/=0) CYCLE

             pvs2(1:2,1:2) = Symm%Coord(:,ippt(1:2))
             CALL HighOrderCell_linearIsoparTransf(line, pvs2, pts2 )

             DO i = 1, NPL
                ip = ippt(i)
                IF(markp(ip)>0) CYCLE
                markp(ip) = 1
                Symm%NB_DBC = Symm%NB_DBC + 1
                Symm%IP_DBC(  Symm%NB_DBC) = ip
                IF(lboun(ippt(1))==0 .AND. lboun(ippt(2))==0)THEN
                   Symm%Pd_DBC(:,Symm%NB_DBC) = 0
                ELSE
                   Symm%Pd_DBC(:,Symm%NB_DBC) = Symm%Coord(:,ip) - pts2(:,i)
                   Symm%Coord(:,ip) = pts2(:,i)
                ENDIF
             ENDDO
          ENDDO
       ENDDO

       CALL Surf_CleanNodes(Symm, markp)
       WRITE(29,*)' Symmetry plane NB_Tri, Pt, DBC=', Symm%NB_Tri, Symm%NB_Point, Symm%NB_DBC
       
       CALL Surf_linearSystemSolver(Symm, material, Disp2D, Isucc)
       IF(Isucc<=0)THEN
          WRITE(*,*)' Error---- negative Jacobian at IB= ',-Isucc
          CALL Surf_CellDisplay (Symm, -Isucc, 3, 71, 1)
          CALL Error_Stop ( 'ElasticityMesh_SymmLayer::')
       ENDIF

       DO ip = 1, Symm%NB_Point
          Symm%Coord(:,ip) = Symm%Coord(:,ip) + Disp2D(:,ip)
       ENDDO


       DO ip = 1, VMHO%NB_Point
          i = markp(ip)
          IF(i==0) CYCLE
          IF(markdbc(ip)>0) CYCLE
          markdbc(ip) = 1
          ct  = Symm%Coord(:,i) - Disp2D(:,i)
          pt1 = Plane3D_get3DPosition (SymPlane(is), ct)                !--- linear position
          pt2 = Plane3D_get3DPosition (SymPlane(is), Symm%Coord(:,i))   !--- target position
          VMHO%NB_DBC = VMHO%NB_DBC + 1
          VMHO%IP_DBC(  VMHO%NB_DBC) = IP
          VMHO%PD_DBC(:,VMHO%NB_DBC) = pt2 - pt1
          VMHO%Posit(:,ip) = pt2
       ENDDO

       !--- Check the quality
       IF(Debug_Display>1)THEN
          WRITE(29,*)' Quality check for SymmLayer:'
          CALL Surf_calcQuality2D(Symm, 0, ib, Vamm)
          WRITE(29,*)' The worst triangle is at ib=', ib, ' with Vab=',Vamm
       ENDIF

    ENDDO

    CALL SurfaceMeshStorage_Clear(Symm)
    DEALLOCATE(Disp2D, markdbc)

  END SUBROUTINE ElasticityMesh_SymmLayer

END SUBROUTINE VisLayer_ElasticityMesh

!>
!!   newn(global nodeID) = nodeID in this symmetry surface.
!!   nold(nodeID in this symmetry surface) = global nodeID.
!<
SUBROUTINE ElasticityMesh_SymmPlane(SurfSym, isf, aPlane, newn, nold)
  USE common_Constants
  USE common_Parameters
  USE HybridMeshManager
  USE SurfaceMeshHighOrder
  USE NodeNetTree
  IMPLICIT NONE

  TYPE(SurfaceMeshStorageType), INTENT(inout) :: SurfSym
  INTEGER, INTENT(in) :: isf
  TYPE(Plane3D), INTENT(in) :: aPlane
  INTEGER, INTENT(inout) :: newn(*), nold(*)
  INTEGER :: i, j, k, ip, ic, ia, NPL, ied, IB, IT
  INTEGER :: ipps(100), ipp2(2), Isucc, edgeNodes(100,6), rev(100)
  INTEGER, DIMENSION(:),   POINTER :: MarkPt
  REAL*8,  DIMENSION(:,:), POINTER :: Disp
  REAL*8  :: Pt(3), co(2), vamm
  TYPE(NodeNetTreeType) :: EdgeTree

  NPL         = Surf%Cell%numFaceNodes      !--- number of nodes on a edge.
  CALL HighOrderCell_getEdgeNodes_TET (HighOrder_Model, edgeNodes)
  rev(1:2)   = (/2,1/)
  rev(3:NPL) = (/ ( i, i=NPL,3,-1 ) /)

  CALL Surf_getHighOrder(HighOrder_Model, SurfSym)  

  !---- build edges  
  CALL Surf_BuildEdge(SurfSym, EdgeTree)
  ALLOCATE(MarkPt(          SurfSym%NB_Point))
  ALLOCATE(SurfSym%IP_DBC(  SurfSym%NB_Point))
  ALLOCATE(SurfSym%Pd_DBC(2,SurfSym%NB_Point))
  ALLOCATE(Disp(2,          SurfSym%NB_Point))

  MarkPt = 0
  ia = 0
  IF(isf==symLine%Region1 .OR. isf==symLine%Region2)THEN
     !--- symmetry x symmetry line 
     DO ic = 1, symLine%numPieces
        DO i = 1,symLine%Ips(ic)%numNodes-1
           ipp2 = newn(symLine%Ips(ic)%Nodes(i:i+1))
           CALL NodeNetTree_Search (2, ipp2, EdgeTree, ied)
           IF(ied==0) STOP 'howslkdjf90eqhgq03'
           DO j=1,NPL
              ip = SurfSym%IP_Edge(j,ied)
              IF(markpt(ip)>0) CYCLE
              markpt(ip) = 1
              ia = ia+1
              SurfSym%IP_DBC(ia) =ip
              SurfSym%Pd_DBC(:,ia) = 0
           ENDDO
        ENDDO
     ENDDO
  ENDIF

  DO IB = 1, Surf%NB_Tri
     IF(Surf%IP_Tri(5,IB)/=-isf) CYCLE
     DO i = 1,3
        ipps(1:NPL) = Surf%IPsp_Tri(Surf%Cell%FaceNodes(1:NPL,i), IB)
        ipp2(1:2)   = newn(ipps(1:2))
        IF(ipp2(1)==0 .OR. ipp2(2)==0) CYCLE
        CALL NodeNetTree_Search (2, ipp2, EdgeTree, ied)
        IF(ied==0) CYCLE
        IF(ipp2(1)==SurfSym%IP_Edge(2,ied)) ipps(1:NPL) = ipps(rev(1:NPL))
        DO j = 1, NPL
           ip = SurfSym%IP_Edge(j,ied)
           IF(markpt(ip)>0) CYCLE
           markpt(ip) = 1
           IF(nold(ip)==0 .AND. newn(ipps(j))==0)THEN
              nold(ip) = ipps(j)
              newn(ipps(j)) = ip
           ELSE IF(nold(ip)/=ipps(j) .OR. newn(ipps(j))/=ip) THEN
              WRITE(*,*)' Error--- nold(ip)/=ipps(j):',nold(ip),ipps(j),newn(ipps(j)),ip
              STOP 'dslfjrehg034fdgfd'
           ENDIF
           ia = ia +1
           SurfSym%IP_DBC(ia) =ip
           Pt = VM%Posit(:,ipps(j))
           co = Plane3D_project2D(aPlane,Pt)
           SurfSym%Pd_DBC(:,ia) = co - SurfSym%Coord(:,ip)
        ENDDO
     ENDDO
  ENDDO

  DO IT = 1, VM%NB_Tet
     DO k = 1,6
        ipps(1:NPL) = VM%IP_Tet(edgeNodes(1:NPL, k), IT)
        ipp2(1:2)   = newn(ipps(1:2))
        IF(ipp2(1)==0 .OR. ipp2(2)==0) CYCLE
        CALL NodeNetTree_Search (2, ipp2, EdgeTree, ied)
        IF(ied==0) CYCLE
        IF(ipp2(1)==SurfSym%IP_Edge(2,ied)) ipps(1:NPL) = ipps(rev(1:NPL))
        DO j = 1, NPL
           ip = SurfSym%IP_Edge(j,ied)
           IF(markpt(ip)>0) CYCLE
           markpt(ip) = 1
           IF(nold(ip)==0 .AND. newn(ipps(j))==0)THEN
              nold(ip) = ipps(j)
              newn(ipps(j)) = ip
           ELSE IF(nold(ip)/=ipps(j) .OR. newn(ipps(j))/=ip)THEN
              WRITE(*,*)' Error--- node mapping for IT,j,ied=',IT,j,ied
              WRITE(*,*)'   ipps=',ipps(1:NPL)
              WRITE(*,*)'   new= ',newn(ipps(1:NPL))
              WRITE(*,*)'   iped=',SurfSym%IP_Edge(1:NPL,ied)
              WRITE(*,*)'   old= ',nold(SurfSym%IP_Edge(1:NPL,ied))
              STOP 'htthddfgdgrt5'
           ENDIF
           ia = ia +1
           SurfSym%IP_DBC(ia) =ip
           Pt = VM%Posit(:,ipps(j))
           co = Plane3D_project2D(aPlane,Pt)
           SurfSym%Pd_DBC(:,ia) = co - SurfSym%Coord(:,ip)
        ENDDO
     ENDDO
  ENDDO


  SurfSym%NB_DBC = ia

  !--- solver elasticity equation
  CALL Surf_linearSystemSolver(SurfSym, material, Disp, Isucc)
  IF(Isucc==0) STOP 'esfjre;ojtg945j0f isuu==0'

  DO ip = 1, SurfSym%NB_Point
     SurfSym%Coord(:,ip) = SurfSym%Coord(:,ip) + disp(:,ip)
  ENDDO

  !--- Check the quality
  IF(Debug_Display>1)THEN
     WRITE(29,*)' Quality check for SymmPlane:'
     CALL Surf_calcQuality2D(SurfSym, 0, ib, Vamm)
     WRITE(29,*)' The worst triangle is at ib=', ib, ' with Vab=',real(Vamm)
  ENDIF


  DEALLOCATE(MarkPt)
  DEALLOCATE(Disp)


  RETURN
END SUBROUTINE ElasticityMesh_SymmPlane


