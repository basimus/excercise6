!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!**************************************************************************
!>
!!  Build_Sources 
!!  This subroutine loops around all the sources and genetates meshes
!!  Initial implimentation writen by S. Walton 29/01/2014
!!
!<
!**************************************************************************
SUBROUTINE Build_Sources()
      USE common_Parameters
      USE array_allocator
      USE SpacingStorage
      USE kdtree2_precision_module
      USE kdtree2_module
      USE Geometry3DAll
      USE SpacingStorageGen
      USE Source_Type
      USE TetMeshStorage
    USE m_mrgrnk
      
      IMPLICIT NONE
      
    INTEGER :: i1,i2,ip1,ip2,ib
    
      INTEGER :: done,allFail,helpFlag,itry,Isucc,flag
      INTEGER :: pStart,pEnd,numIns
      INTEGER :: ip,ic,numAlloc,iRes,itemp
      REAL*8 :: unitCell(3,16),fMap(3,3)
      REAL*8 :: geoD,sqrt34,oneOverSqrt34,p1(3),p2(3),pmid(3),p0(3),pp(3,4),sqrt5o4,dd(4)
    REAL*8 :: pa(3),pb(3),pc(3),p(3)
      REAL*8 :: b,h,b1,h1,alpha,delP1,delM1,criteria,d2,expansionFactor
      REAL*8 :: d,delta,dtet,dmax,dmin,dtemp,maxS,conv,lastObj,nowObj,residual,dLayer
      REAL*8,allocatable :: PositNm1(:,:)
      type(kdtree2), pointer :: tree
      INTEGER :: ie,  numLoops,IT,i,j,myStart,improve,smtFlag,firstNode,insFlag,noBad
      INTEGER :: idxin,correltime,nn,nfound,NB_Tet_Old,NumIP,NumResAloc,ii,NB_Blay
    INTEGER :: ipp(4),ippp(3), Isucc1
      character*(4) :: loopString 
      type(kdtree2_result),allocatable :: results(:)
      type(IntQueueType),allocatable :: child(:)
    type(IntQueueType) :: toOptimise
      INTEGER,allocatable :: marker(:)
      REAL*8  :: RAsq,RAmaxloc,RAminloc,RAvol,RAsph, w4(4)   
      REAL*8 :: dt
    REAL*8 :: gamma, oneOverGammam1, geoDT, tol, gapWidth,geoMin
    
    REAL*8 :: volCheckP(3),volCheck
      
      INTEGER, allocatable :: srcStat(:,:)
    REAL*8, allocatable :: srcD(:,:)
      INTEGER :: pntNum, pntI, numLay
    INTEGER :: numElem,numBad,idxinI,numInsSave,ipClose
    
    INTEGER :: numSrc
    
    INTEGER :: layersEnd
    
    INTEGER :: iFace, iF1,iF2,iF3
    
    INTEGER, allocatable :: Isort(:)
    REAL*8, allocatable :: TempR(:)
    
    REAL*8, allocatable :: SpacingBackup(:),SpacingBackupB(:), BadPointsVol(:)
    !Coordinate converstions
    REAL*8 :: f(3,3), newX(3), newY(3), newZ(3)
    
    TYPE(TetMeshStorageType)     :: BoundaryLayers
    TYPE(TetMeshStorageType)     :: VolumeSources
    
    TYPE(Plane3D) :: aPlane
      
    INTEGER :: smoothSrc, optSrc,smoothAll, optAll,Kswap

    REAL*8 :: dummy

    REAL*8 :: percBound, numBadPointVol, numAdvFro

    REAL*8 :: nbVol, nbAdv, nbLat, nVol, nAdv, nLat
    
  !Set these up to override user input  
  Loop_Smooth = 5
  Smooth_Method = 2
  Collapse_Angle          = (12.0d0*3.141592653589793d0)/180.d0
  Swapping_Angle          = (12.0d0*3.141592653589793d0)/180.d0
    
    
    !Gap width when cutting holes, factor of delta
    gapWidth = 1.0d0
    !This is the tolerance a point is deleted with
    tol = 0.5d0
    
    !This helps with the centroid smooth
      smtFlag=0
   !    OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
   !    READ(84,*) dt
   !    READ(84,*) conv
    ! READ(84,*) gamma
    ! READ(84,*) numLay
    ! READ(84,*) smoothSrc
    ! READ(84,*) optSrc
    ! READ(84,*) smoothAll
    ! READ(84,*) optAll
    ! READ(84,*) dummy
    ! READ(84,*) dummy
    ! READ(84,*) dummy
    
    dt = 0
    conv = 0
    gamma = 0
    numLay = 1
    smoothSrc = 0
    optSrc = 0
    smoothAll = 0
    optAll = 0
   
      CLOSE(84)
      !gamma = 0.0d0
    oneOverGammam1 = 1.0d0/(1.0d0+gamma)
    
    sqrt5o4 = SQRT(5.0d0)/4.0d0
    
      !First generate a few layers and store those elements
    
    CALL Face_Insert()
    numAdvFro = NB_Point
    CALL Set_Domain_Mapping()

    !IF ((numLay-1).GT.0) THEN
    !  CALL Lattice_Insert(numLay-1,NB_Point)
    !ENDIF
    
    gapWidth = REAL(numLay)
    
    MeshOut%GridOrder = GridOrder
      MeshOut%NB_Point  = NB_Point
      MeshOut%NB_Tet    = NB_Tet
      ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
      ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
      MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
      MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)
      CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_bound',JobNameLength+6, -15, MeshOut)  
      DEALLOCATE(MeshOut%IP_Tet)
      DEALLOCATE(MeshOut%Posit)
    
    
    CALL TetMeshStorage_CellBackup(NB_Tet, IP_Tet(:,1:NB_Tet), BoundaryLayers)
    CALL TetMeshStorage_NodeBackup(NB_Point, Posit, BoundaryLayers)
    
    ALLOCATE(SpacingBackupB(NB_Point))
    SpacingBackupB(1:NB_Point) = Scale_Point(1:NB_Point)
    
    
      RR_Point(1:NB_Point) = 0.d0
    CALL Get_Tet_SphereCross(0)
      CALL Check_Mesh_Geometry2('layers')
    
    
    !Insert background points
    !Restore the blank mesh
    CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
    CALL TetMeshStorage_NodeRestore(NB_Point, Posit, HullBackUp)
    
        CALL Next_Build()
        CALL Get_Tet_Circum(0)
        CALL Get_Tet_Volume(0)
    CALL Set_Domain_Mapping()
    firstNode = NB_Point + 1
        Posit(1:3,firstNode:nallc_point)=0.0d0
  
  
  IF (1.EQ.0) THEN
  
  
  
  
    CALL Lattice_Insert_Background()
      RR_Point(1:NB_Point) = 0.d0
    CALL Get_Tet_SphereCross(0)
      
    
    
      !Set up some globals
      sqrt34 = (SQRT(3.0d0/4.0d0))
      oneOverSqrt34 = 1/sqrt34
      b1 = oneOverSqrt34/2.d0  !This means we are aiming for the actual spacing
      h1 = oneOverSqrt34/SQRT(2.d0) !This means we are aiming for the actual spacing
      !b1 = 1.d0/2.d0
      !h1 = 1.d0/SQRT(2.d0)
      NumResAloc = 1000
      allocate(results(NumResAloc))
      numIns = 0
      
      
     
   !We neet to set up a global array of sources (doesn't support ansys or ring atm)
   
   numSrc = BGSpacing%Sources%NB_Pt + BGSpacing%Sources%NB_Ln &
            + BGSpacing%Sources%NB_Tri
        
   allocate(srcStat(3,numSrc))
   allocate(srcD(4,numSrc))
   
   !In srcStat we store the minimum spacing at each source the type of source and the position
   ! it is in it's local vector
   DO pntNum = 1,BGSpacing%Sources%NB_Pt
      srcD(1:3,pntNum) = BGSpacing%Sources%Pt(pntNum)%Posit  !Where to seed this source
      srcD(4,pntNum) = BGSpacing%Sources%Pt(pntNum)%Space
    srcStat(1,pntNum) = 1 !Flag
    srcStat(2,pntNum) = pntNum !Position in local vector
    srcStat(3,pntNum) = 0    !Keeps track of which points have been created
   END DO
    
      
    DO pntNum = 1,BGSpacing%Sources%NB_Ln    
      !srcD(1:3,pntNum+BGSpacing%Sources%NB_Pt) = 0.5d0*(BGSpacing%Sources%Ln(pntNum)%PS1%Posit(1:3) + &
    !                                                  BGSpacing%Sources%Ln(pntNum)%PS2%Posit(1:3) )
      srcD(4,pntNum+BGSpacing%Sources%NB_Pt) = MIN(BGSpacing%Sources%Ln(pntNum)%PS1%Space, &
                                               BGSpacing%Sources%Ln(pntNum)%PS2%Space)
                           
                           
    IF   (BGSpacing%Sources%Ln(pntNum)%PS1%Space.LT.BGSpacing%Sources%Ln(pntNum)%PS2%Space) THEN
      srcD(1:3,pntNum+BGSpacing%Sources%NB_Pt) = BGSpacing%Sources%Ln(pntNum)%PS1%Posit(1:3)
    ELSE
      srcD(1:3,pntNum+BGSpacing%Sources%NB_Pt) = BGSpacing%Sources%Ln(pntNum)%PS2%Posit(1:3)
    ENDIF
                           
    srcStat(1,pntNum+BGSpacing%Sources%NB_Pt) = 2 !Flag
    srcStat(2,pntNum+BGSpacing%Sources%NB_Pt) = pntNum !Position in local vector
    srcStat(3,pntNum+BGSpacing%Sources%NB_Pt) = 0    !Keeps track of which points have been created
    ENDDO
      
    DO pntNum = 1,BGSpacing%Sources%NB_Tri
        !srcD(1:3,pntNum+BGSpacing%Sources%NB_Pt+BGSpacing%Sources%NB_Ln) = (BGSpacing%Sources%Tri(pntNum)%PS1%Posit(1:3) + &
    !                                                  BGSpacing%Sources%Tri(pntNum)%PS2%Posit(1:3) + &
    !                          BGSpacing%Sources%Tri(pntNum)%PS3%Posit(1:3))/3.0d0
      srcD(4,pntNum+BGSpacing%Sources%NB_Pt+BGSpacing%Sources%NB_Ln) = MIN(MIN(BGSpacing%Sources%Tri(pntNum)%PS1%Space, &
                                               BGSpacing%Sources%Tri(pntNum)%PS2%Space),&
                           BGSpacing%Sources%Tri(pntNum)%PS3%Space)
                           
                           
    IF (BGSpacing%Sources%Tri(pntNum)%PS1%Space.LT.BGSpacing%Sources%Tri(pntNum)%PS2%Space) THEN
      IF (BGSpacing%Sources%Tri(pntNum)%PS1%Space.LT.BGSpacing%Sources%Tri(pntNum)%PS3%Space) THEN
        srcD(1:3,pntNum+BGSpacing%Sources%NB_Pt+BGSpacing%Sources%NB_Ln) = BGSpacing%Sources%Tri(pntNum)%PS1%Posit(1:3)
      ELSE
        srcD(1:3,pntNum+BGSpacing%Sources%NB_Pt+BGSpacing%Sources%NB_Ln) = BGSpacing%Sources%Tri(pntNum)%PS3%Posit(1:3)
      ENDIF
    ELSE
      IF (BGSpacing%Sources%Tri(pntNum)%PS2%Space.LT.BGSpacing%Sources%Tri(pntNum)%PS3%Space) THEN
        srcD(1:3,pntNum+BGSpacing%Sources%NB_Pt+BGSpacing%Sources%NB_Ln) = BGSpacing%Sources%Tri(pntNum)%PS2%Posit(1:3)
      ELSE
        srcD(1:3,pntNum+BGSpacing%Sources%NB_Pt+BGSpacing%Sources%NB_Ln) = BGSpacing%Sources%Tri(pntNum)%PS3%Posit(1:3)
      ENDIF
    
    ENDIF
                           
    srcStat(1,pntNum+BGSpacing%Sources%NB_Pt+BGSpacing%Sources%NB_Ln) = 3 !Flag
    srcStat(2,pntNum+BGSpacing%Sources%NB_Pt+BGSpacing%Sources%NB_Ln) = pntNum !Position in local vector
    srcStat(3,pntNum+BGSpacing%Sources%NB_Pt+BGSpacing%Sources%NB_Ln) = 0    !Keeps track of which points have been created
    
    

    ENDDO
    
    
      WRITE(*,*)'Generating sources'
      WRITE(29,*)'Generating ources'
    
      
    
      DO pntNum = 1,numSrc
      
        !We want to do the biggest source first (in terms of the space)
        !This is enificient but we shouldn't have that many sources???
        d = 0.d0
        DO ip = 1,numSrc
          IF ((srcStat(3,ip).EQ.0).AND.(srcD(4,ip).GT.d)) THEN
            d = srcD(4,ip)
            pntI = ip
          ENDIF     
        ENDDO
        srcStat(3,pntI) = 1
        WRITE(*,*)'Now building source',pntI, ' of: ',numSrc ,' Spacing: ',d
        WRITE(29,*)'Now building  source',pntI, ' of: ',numSrc ,' Spacing: ',d
        
        !Now lets insert  
    
    !Restore the blank mesh
    CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
        CALL Next_Build()
        CALL Get_Tet_Circum(0)
        CALL Get_Tet_Volume(0)
        
        
    IF (srcStat(1,pntI).EQ.1) THEN
          NB_Point = NB_Point + 1
          IF(NB_Point>nallc_point) THEN     
           CALL ReAllocate_Point()            
          END IF
          Posit(1:3,NB_Point) = srcD(1:3,pntI)
          CALL Get_Point_Mapping(NB_Point)
          Isucc = 0
          CALL Insert_Point_Delaunay(NB_Point,0,Isucc,0)
          CALL Remove_Tet()
          myStart = NB_Point
          pStart = NB_Point
          pEnd = NB_Point
          Scale_Point(NB_Point) = GetGenScale(Posit(:,NB_Point),srcStat(:,pntI))
      
      !Set up transformation matrix
      f(1,1) = 1.0d0
      f(1,2) = 0.0d0
      f(1,3) = 0.0d0
      f(2,1) = 0.0d0
      f(2,2) = 1.0d0
      f(2,3) = 0.0d0
      f(3,1) = 0.0d0
      f(3,2) = 0.0d0
      f(3,3) = 1.0d0
      
      
      
    ELSE IF (srcStat(1,pntI).EQ.2) THEN
      !NB_Point = NB_Point + 1      
          !IF(NB_Point>nallc_point) THEN     
          ! CALL ReAllocate_Point()            
          !END IF
          !Posit(1:3,NB_Point) =   BGSpacing%Sources%Ln(srcStat(2,pntI))%PS1%Posit(1:3)
      !CALL Get_Point_Mapping(NB_Point)
          !Isucc = 0
          !CALL Insert_Point_Delaunay(NB_Point,0,Isucc,0)
          !CALL Remove_Tet()
      !Scale_Point(NB_Point) = GetGenScale(Posit(:,NB_Point),srcStat(:,pntI))
      
      !myStart = NB_Point
      !pStart = NB_Point
    
    
      !NB_Point = NB_Point + 1      
          !IF(NB_Point>nallc_point) THEN     
          ! CALL ReAllocate_Point()            
          !END IF
          !Posit(1:3,NB_Point) =   BGSpacing%Sources%Ln(srcStat(2,pntI))%PS2%Posit(1:3)
      !CALL Get_Point_Mapping(NB_Point)
          !Isucc = 0
          !CALL Insert_Point_Delaunay(NB_Point,0,Isucc,0)
          !CALL Remove_Tet()
      !Scale_Point(NB_Point) = GetGenScale(Posit(:,NB_Point),srcStat(:,pntI))
      
     
      
      NB_Point = NB_Point + 1    
      myStart = NB_Point
      pStart = NB_Point
          IF(NB_Point>nallc_point) THEN     
           CALL ReAllocate_Point()            
          END IF
          Posit(1:3,NB_Point) =   0.5d0*(BGSpacing%Sources%Ln(srcStat(2,pntI))%PS1%Posit(1:3)+ &
                                     BGSpacing%Sources%Ln(srcStat(2,pntI))%PS2%Posit(1:3))
      CALL Get_Point_Mapping(NB_Point)
          Isucc = 0
          CALL Insert_Point_Delaunay(NB_Point,0,Isucc,0)
          CALL Remove_Tet()
      Scale_Point(NB_Point) = GetGenScale(Posit(:,NB_Point),srcStat(:,pntI))
      
      pEnd = NB_Point
      
      !Find the new coordinate system
      newX(1:3) = BGSpacing%Sources%Ln(srcStat(2,pntI))%PS1%Posit(1:3)-BGSpacing%Sources%Ln(srcStat(2,pntI))%PS2%Posit(1:3)
      newX(:) = newX(:)/Geo3D_Distance(newX)
      CALL Geo3D_BuildOrthogonalAxes(newX,newY,newZ)
      
      f(1,1:3) = newX(1:3)
      f(2,1:3) = newY(1:3)
      f(3,1:3) = newZ(1:3)
      
      
    
    ELSE
      !NB_Point = NB_Point + 1      
          !IF(NB_Point>nallc_point) THEN     
          ! CALL ReAllocate_Point()            
          !END IF
          !Posit(1:3,NB_Point) =   BGSpacing%Sources%Tri(srcStat(2,pntI))%PS1%Posit(1:3)
      !CALL Get_Point_Mapping(NB_Point)
          !Isucc = 0
          !CALL Insert_Point_Delaunay(NB_Point,0,Isucc,0)
          !CALL Remove_Tet()
      !Scale_Point(NB_Point) = GetGenScale(Posit(:,NB_Point),srcStat(:,pntI))
      
      !myStart = NB_Point
      !pStart = NB_Point
      
      !NB_Point = NB_Point + 1      
          !IF(NB_Point>nallc_point) THEN     
          ! CALL ReAllocate_Point()            
          !END IF
          !Posit(1:3,NB_Point) =   BGSpacing%Sources%Tri(srcStat(2,pntI))%PS2%Posit(1:3)
      !CALL Get_Point_Mapping(NB_Point)
          !Isucc = 0
          !CALL Insert_Point_Delaunay(NB_Point,0,Isucc,0)
          !CALL Remove_Tet()
      !Scale_Point(NB_Point) = GetGenScale(Posit(:,NB_Point),srcStat(:,pntI))
    
    
      !NB_Point = NB_Point + 1      
          !IF(NB_Point>nallc_point) THEN     
           !CALL ReAllocate_Point()            
          !END IF
          !Posit(1:3,NB_Point) =   BGSpacing%Sources%Tri(srcStat(2,pntI))%PS3%Posit(1:3)
      !CALL Get_Point_Mapping(NB_Point)
          !Isucc = 0
          !CALL Insert_Point_Delaunay(NB_Point,0,Isucc,0)
          !CALL Remove_Tet()
      !Scale_Point(NB_Point) = GetGenScale(Posit(:,NB_Point),srcStat(:,pntI))
      
      
      
      
      NB_Point = NB_Point + 1      
          IF(NB_Point>nallc_point) THEN     
           CALL ReAllocate_Point()            
          END IF
          Posit(1:3,NB_Point) =   (BGSpacing%Sources%Tri(srcStat(2,pntI))%PS1%Posit(1:3)+ &
                               BGSpacing%Sources%Tri(srcStat(2,pntI))%PS2%Posit(1:3)+ &
                   BGSpacing%Sources%Tri(srcStat(2,pntI))%PS3%Posit(1:3))/3.0d0
      CALL Get_Point_Mapping(NB_Point)
          Isucc = 0
          CALL Insert_Point_Delaunay(NB_Point,0,Isucc,0)
          CALL Remove_Tet()
      Scale_Point(NB_Point) = GetGenScale(Posit(:,NB_Point),srcStat(:,pntI))
      myStart = NB_Point
      pStart = NB_Point
      pEnd = NB_Point
      
      
      
      
      !Find the new coordinate system
      newX(1:3) = BGSpacing%Sources%Tri(srcStat(2,pntI))%PS2%Posit(1:3) - BGSpacing%Sources%Tri(srcStat(2,pntI))%PS1%Posit(1:3)
      newX(:) = newX(:)/Geo3D_Distance(newX)
      
      newY(1:3) = BGSpacing%Sources%Tri(srcStat(2,pntI))%PS3%Posit(1:3) - BGSpacing%Sources%Tri(srcStat(2,pntI))%PS1%Posit(1:3)
      newY(:) = newY(:)/Geo3D_Distance(newX)
      
      !X and Y might not be _|_ so create Z
      newZ(:) = Geo3D_Cross_Product(newX,newY)
      newZ(:) = newZ(:)/Geo3D_Distance(newZ)
      newY(:) = Geo3D_Cross_Product(newZ,newX)
      newY(:) = newY(:)/Geo3D_Distance(newY)
      
      f(1,1:3) = newX(1:3)
      f(2,1:3) = newY(1:3)
      f(3,1:3) = newZ(1:3)
     
    
    ENDIF
    
    
    
    
        done = 0
        allFail = 1
        numLoops = 0
        correltime = 0
        
        WRITE(*,*)'building point source using Lattice Insertion'
        WRITE(29,*)'building point source using Lattice Insertion'
    dLayer = srcD(4,pntI)/(1.0d0+expansionFactor)
        
        DO WHILE (done .EQ. 0)
            numLoops = numLoops + 1
            IF(Debug_Display>0)THEN
                  WRITE(*,*)'Lattice Insertion Loop:',numLoops
                  WRITE(29,*)'Lattice Insertion Loop:',numLoops
                  loopString = INT_TO_CHAR(numLoops,4)
            END IF

            !All fail 
            allFail = 1
            numAlloc = 0
            !Allocate marker and child arrays
            allocate(marker(NB_Tet))
            allocate(child(NB_Tet))
                  
            WRITE(*,*)'NB_Tet',NB_Tet
            WRITE(*,*)'size marker',SIZE(marker)
            WRITE(*,*) 'size child',SIZE(child)
                  
                  
                  
            marker(1:NB_Tet)=0
                  
            CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
            
            IF(Debug_Display>2)THEN           
                   WRITE(*,*)'Lattice Insertion: Generating Points'
                   WRITE(29,*)'Lattice Insertion: Generating Points'
            END IF
            
            Mark_Point(1:NB_Point) = 0  
      
      !d = (SUM(Scale_Point(pStart:pEnd))/MAX(1,pEnd-pStart))*BGSpacing%BasicSize
            
      CALL Get_Tet_Volume(0)
      
            DO ip = pStart,pEnd
                  
                  
                  !Get the spacing and the lattice points
                  
                  d = Scale_Point(ip)*BGSpacing%BasicSize
          
                 
                  IF (BGSpacing%BasicSize.GT.d) THEN
                  
                      p1 = Posit(:,ip)
            IF (1.EQ.0) THEN
                      unitCell(1,1) = d*f(1,1)  + p1(1)
            unitCell(2,1) = d*f(2,1)  + p1(2)
            unitCell(3,1) = d*f(3,1)  + p1(3)
            
            unitCell(1,2) =  d*f(1,2)  + p1(1)
            unitCell(2,2) =  d*f(2,2)  + p1(2)
            unitCell(3,2) =  d*f(3,2)  + p1(3)
            
            unitCell(1,3) = d*f(1,3) + p1(1)
            unitCell(2,3) = d*f(2,3) + p1(2)
            unitCell(3,3) = d*f(3,3) + p1(3)
            
            unitCell(1,4) = -1.0d0*d*f(1,1)  + p1(1)
            unitCell(2,4) = -1.0d0*d*f(2,1)  + p1(2)
            unitCell(3,4) = -1.0d0*d*f(3,1)  + p1(3)
            
            unitCell(1,5) =  -1.0d0*d*f(1,2) + p1(1)
            unitCell(2,5) = -1.0d0*d*f(2,2)  + p1(2)
            unitCell(3,5) =  -1.0d0*d*f(3,2)  + p1(3)
            
            unitCell(1,6) = -1.0d0*d*f(1,3) + p1(1)
            unitCell(2,6) = -1.0d0*d*f(2,3) + p1(2)
            unitCell(3,6) =  -1.0d0*d*f(3,3) + p1(3)
            END IF
            
            
            
                      b = (d*b1)
                      h = (d*h1)            
                                      
                      unitCell(1,1) = -1.0d0*b*f(1,1) + h*f(1,2) + p1(1)
            unitCell(2,1) = -1.0d0*b*f(2,1) + h*f(2,2) + p1(2)
            unitCell(3,1) = -1.0d0*b*f(3,1) + h*f(3,2) + p1(3)
            
                      unitCell(1,2) = h*f(1,2) + h*f(1,3) + p1(1)
            unitCell(2,2) = h*f(2,2) + h*f(2,3) + p1(2)
            unitCell(3,2) = h*f(3,2) + h*f(3,3) + p1(3)
          
            unitCell(1,3) = b*f(1,1) + h*f(1,2) + p1(1)
            unitCell(2,3) = b*f(2,1) + h*f(2,2) + p1(2)
            unitCell(3,3) = b*f(3,1) + h*f(3,2) + p1(3)
      
            unitCell(1,4) = -1.0d0*b*f(1,1) + h*f(1,3) + p1(1)
            unitCell(2,4) = -1.0d0*b*f(2,1) + h*f(2,3) + p1(2)
            unitCell(3,4) = -1.0d0*b*f(3,1) + h*f(3,3) + p1(3)
    
                      
            unitCell(1,5) = b*f(1,1)  + h*f(1,3) + p1(1)
            unitCell(2,5) = b*f(2,1)  + h*f(2,3) + p1(2)
            unitCell(3,5) = b*f(3,1)  + h*f(3,3) + p1(3)
            
  
            unitCell(1,6) = -1.0d0*b*f(1,1) -1.0d0*h*f(1,2) + p1(1)
            unitCell(2,6) = -1.0d0*b*f(2,1) -1.0d0*h*f(2,2) + p1(2)
            unitCell(3,6) = -1.0d0*b*f(3,1) -1.0d0*h*f(3,2) + p1(3)
            
                      
            unitCell(1,7) =  -1.0d0*h*f(1,2) + h*f(1,3) + p1(1)
            unitCell(2,7) =  -1.0d0*h*f(2,2) + h*f(2,3) + p1(2)
            unitCell(3,7) =  -1.0d0*h*f(3,2) + h*f(3,3) + p1(3)
            
            unitCell(1,8) = b*f(1,1) -1.0d0*h*f(1,2) + p1(1)
            unitCell(2,8) = b*f(2,1) -1.0d0*h*f(2,2) + p1(2)
            unitCell(3,8) = b*f(3,1) -1.0d0*h*f(3,2) + p1(3)
            
            unitCell(1,9) =  -1.0d0*h*f(1,2) -1.0d0*h*f(1,3) + p1(1)
            unitCell(2,9) =  -1.0d0*h*f(2,2) -1.0d0*h*f(2,3) + p1(2)
            unitCell(3,9) =  -1.0d0*h*f(3,2) -1.0d0*h*f(3,3) + p1(3)
            
            unitCell(1,10) = -1.0d0*b*f(1,1) -1.0d0*h*f(1,3) + p1(1)
            unitCell(2,10) = -1.0d0*b*f(2,1) -1.0d0*h*f(2,3) + p1(2)
            unitCell(3,10) = -1.0d0*b*f(3,1) -1.0d0*h*f(3,3) + p1(3)
            
            unitCell(1,11) =  h*f(1,2)  -1.0d0*h*f(1,3) + p1(1)
            unitCell(2,11) =  h*f(2,2) -1.0d0*h*f(2,3) + p1(2)
            unitCell(3,11) =  h*f(3,2) -1.0d0*h*f(3,3) + p1(3)
            
                      
            unitCell(1,12) = b*f(1,1) -1.0d0*h*f(1,3) + p1(1)
            unitCell(2,12) = b*f(2,1) -1.0d0*h*f(2,3) + p1(2)
            unitCell(3,12) = b*f(3,1)  -1.0d0*h*f(3,3) + p1(3)
            
            unitCell(1,13) = -1.0d0*h*f(1,2) + p1(1)
            unitCell(2,13) = -1.0d0*h*f(2,2) + p1(2)
            unitCell(3,13) = -1.0d0*h*f(3,2) + p1(3)
            
            unitCell(1,14) = -1.0d0*b*f(1,1) + p1(1)
            unitCell(2,14) = -1.0d0*b*f(2,1) + p1(2)
            unitCell(3,14) = -1.0d0*b*f(3,1) + p1(3)
            
            unitCell(1,15) = h*f(1,2) + p1(1)
            unitCell(2,15) = h*f(2,2) + p1(2)
            unitCell(3,15) = h*f(3,2) + p1(3)
            
            unitCell(1,16) = b*f(1,1) + p1(1)
            unitCell(2,16) = b*f(2,1) + p1(2)
            unitCell(3,16) = b*f(3,1) + p1(3)
            
            
                      !unitCell(:,13) = (/2.0d0*b,0.0d0,0.0d0/) + p1
                      !unitCell(:,14) = (/-2.0d0*b,0.0d0,0.0d0/) + p1
            
            
            
                      
                      CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
                      

                      !Now we have the points we need to loop round them and perform the hull check
                      DO ic = 1,16
                                  
                            !First check we are in the domain
                            IF( unitCell(1,ic) < XYZmax(1) .AND. unitCell(1,ic) > XYZmin(1) .AND. &
                                unitCell(2,ic) < XYZmax(2) .AND. unitCell(2,ic) > XYZmin(2) .AND. &
                                unitCell(3,ic) < XYZmax(3) .AND. unitCell(3,ic) > XYZmin(3)) THEN
                                 ! IF (ic.EQ.1) THEN
                                 !       ie = ITs_List(1)
                                 ! ELSE
                                        ie = -1
                                 ! ENDIF
                                  p1 = unitCell(:,ic)
                                  CALL Locate_Node_Tet(p1,ie,3)
                            
                                  IF (ie .GT. 0) THEN
                  
                    delta = GetGenScale(p1,srcStat(:,pntI))*BGSpacing%BasicSize
                    insFlag = 0
                  
                    DO ii = 1,4
                      p2 = Posit(:,IP_Tet(ii,ie))
                      d2 = Scale_Point(IP_Tet(ii,ie))*BGSpacing%BasicSize
                    !  d2 = MIN(tol*d2,tol*delta)
                      d2 = d2*d2
                      geoD = Geo3D_Distance_SQ(p1,p2)
                      IF (geoD.GE.d2) THEN
                        insFlag = insFlag + 1
                      ENDIF
                    ENDDO
                    
                    !Now check element size
                    !IF (Sphere_Tet(4,ie).GT.Scale_Tet(ie)*BGSpacing%BasicSize) THEN
                    !  insFlag = insFlag + 1
                    !ENDIF
                    
                    volCheckP(:) = (Posit(:,IP_Tet(1,ie))+Posit(:,IP_Tet(2,ie)) &
                                        +Posit(:,IP_Tet(3,ie))+Posit(:,IP_Tet(4,ie)))/4.0d0
                    
                    
                    volCheck = 0.5d0* (( GetGenScale(volCheckP(:),srcStat(:,pntI))* &
                          BGSpacing%BasicSize)**3.0d0)
                          
                    IF (Volume_Tet(ie).LT.volCheck) THEN
                      insFlag = 0
                    END IF
                    
                   IF ((insFlag.EQ.4)) THEN
                                              
                                       
                                          NB_Point = NB_Point + 1
                                          IF(NB_Point>nallc_point) THEN 
                        
                                  
                                             numAlloc = numAlloc + 1
                                             nallc_increase = MAX(14,(pEnd-ip)*14)
                                                             
                                             CALL ReAllocate_Point()
                                                      
                                                             
                                          END IF
                                          Posit(:,NB_Point) = unitCell(:,ic)
                                                          
                                          CALL IntQueue_Push(child(ie),NB_Point)
                                          marker(ie) = marker(ie) + 1
                      
                      
                      
                      Scale_Point(NB_Point) = (GetGenScale(Posit(:,NB_Point),srcStat(:,pntI))+gamma*Scale_Point(ip))*oneOverGammam1
                      
                                          allFail = 0
                                              
                                  
                                     END IF
                                  END IF            
                            END IF
                      
                      END DO
                 END IF
      
            END DO

            
            numIns = NB_Point-pEnd
      numInsSave = numIns
      ALLOCATE(Isort(numIns))
      ALLOCATE(TempR(numIns))
      TempR(1:numIns) = Scale_Point(pEnd+1:NB_Point)
      CALL mrgrnk(TempR, Isort)
      
            IF(Debug_Display>0)THEN
                WRITE(*,*)'Lattice Insertion: number of points to check',numIns
                WRITE(29,*)'Lattice Insertion: number of points to check',numIns
                WRITE(*,*)'Lattice Insertion: number of realocations required',numAlloc
                WRITE(29,*)'Lattice Insertion: number of realocations required',numAlloc
            END IF
            
            IF (allFail .EQ. 1) THEN
                  done = 1
          deallocate(Isort,TempR)
            ELSE
                  
                IF(Debug_Display>0)THEN
                   WRITE(*,*)'Lattice Insertion: creating kdtree'
                   WRITE(29,*)'Lattice Insertion: creating kdtree'
                 END IF
            

                Mark_Point(1:NB_Point)=1
                  
                helpFlag = 0
                  
                 
                tree => kdtree2_create(Posit(1:3,1:NB_Point),sort=.false.,rearrange=.false.)
                          
                IF(Debug_Display>0)THEN
                        WRITE(*,*)'Lattice Insertion: checking proximity to last layer'
                        WRITE(29,*)'Lattice Insertion: checking proximity to last layer'
                END IF
                  
                  
                  
                  DO idxin = myStart,pEnd
                        !First get the spacing
                        !Min distance between points is sqrt(3/4) but kdtree is approximate so search wider area
                        
                        
                        d = Scale_Point(idxin)*BGSpacing%BasicSize
                        
                        
                        !This call searches in a d radius sphere  around the point idxin 
                        nn = kdtree2_r_count_around_point(tree,idxin,correltime,d*d)
                        IF (nn .GT. 0) THEN
                              
                            IF (nn .GT. NumResAloc) THEN
                                              
                             IF(Debug_Display>0)THEN
                                 WRITE(*,*)'Lattice Insertion: reallocate results'
                                 WRITE(29,*)'Lattice Insertion: reallocate results'
                             END IF
                             deallocate(results)
                             NumResAloc = nn
                             allocate(results(NumResAloc))
                             WRITE(*,*)'NumResAloc',NumResAloc
                             WRITE(*,*)'Size results',SIZE(results)
                                                
                            ENDIF
                                            
                            CALL kdtree2_r_nearest_around_point(tree,idxin,correltime,d*d,nfound,nn,results)
                             
                              
                            DO ip = 1,nfound
                                    iRes = results(ip)%idx
                                    
                                    
                                    IF ((ires/=idxin).AND.(Mark_Point(iRes)/=-999).AND.(iRes .GT. pEnd)) THEN
                                          
                                        delta = MIN(Scale_Point(idxin),Scale_Point(iRes)) 
                    !delta = 0.5d0*(Scale_Point(idxin)+Scale_Point(iRes)) 
                    !<<<<<<<<<<<<<<<<<<<<
                    
                                        delta = tol*delta*BGSpacing%BasicSize
                                        p1 = Posit(:,iRes)
                                        p2 = Posit(:,idxin)

                                          
                                        geoD = Geo3D_Distance_SQ(p1,p2)        
                                        
                                        
                                        IF (geoD.LT.(delta*delta)) THEN
                                            !Then this point needs deleting, help flag indicates at least 
                                            !one point needs removing      
                                            helpFlag = 1
                                            Mark_Point(iRes) = -999
                                            numIns = numIns-1
                                        END IF
                              
                                    END IF
                              END DO
                        
                              
                        END IF
                  END DO
                  
                  
                  !Now check mutual proximity

                  !Difference between this loop and the last is in the last loop we didn't
                  !move points from the last layer, in this loop we need to keep track of
                  !which points have been 'swallowed'

                  IF (done .EQ. 0) THEN
                  
                    IF(Debug_Display>0)THEN
                        WRITE(*,*)'Lattice Insertion: checking mutual proximity'
                        WRITE(29,*)'Lattice Insertion: checking mutual proximity'
                    END IF

                  
                    DO idxinI = numInsSave,1,-1
             idxin = pEnd + Isort(idxinI)
             !WRITE(*,*)'idxin',idxin,'Spacing',Scale_Point(idxin)
             
             !Flag to see if we have put this in the optimise list
             flag = 0
             
                       IF (Mark_Point(idxin).EQ.1) THEN
                          
                          !First get the spacing again min dist is sqrt(3/4) but search wider
                                    
                          
                          d = Scale_Point(idxin)*BGSpacing%BasicSize!*sqrt34
                          
                                    
                          nn = kdtree2_r_count_around_point(tree,idxin,correltime,d*d)
                          
                
                          IF (nn .GT. 0) THEN
                                          
                             IF (nn .GT. NumResAloc) THEN
                                                              
                                IF(Debug_Display>0)THEN
                                    WRITE(*,*)'Lattice Insertion: reallocate results'
                                    WRITE(29,*)'Lattice Insertion: reallocate results'
                                END IF
                                                              
                                deallocate(results)
                                NumResAloc = nn
                                allocate(results(NumResAloc))
                                WRITE(*,*)'NumResAloc',NumResAloc
                                WRITE(*,*)'Size results',SIZE(results)
                             ENDIF
                                                              
                              CALL kdtree2_r_nearest_around_point(tree,idxin,correltime,d*d,nfound,nn,results)
                                          
                              DO ip = 1,nfound
                                    iRes = results(ip)%idx
                                    
                                    
                                IF ((ires/=idxin).AND.(iRes .GT. pEnd).AND.(Mark_Point(iRes)/=-999)) THEN
                                    

                                                      
                                     p1 = Posit(:,iRes)
                                     p2 = Posit(:,idxin)
                                    !This may need changing back? 
                                    !delta = 0.25d0*(Scale_Point(iRes)+Scale_Point(idxin))*BGSpacing%BasicSize
                                    delta = MIN(Scale_Point(idxin),Scale_Point(iRes))
                  !delta = 0.5d0*(Scale_Point(idxin)+Scale_Point(iRes))
                                    delta = tol*delta*BGSpacing%BasicSize

                                                      
                                     geoD = Geo3D_Distance_SQ(p1,p2)
                                     
                                     
                                    
                                     IF (geoD.LT.((delta*delta))) THEN
                                                            
                                         helpFlag = 1                                      
                                         Mark_Point(iRes) = -999
                                         numIns = numIns - 1
                     
                     Posit(:,idxin) = (Posit(:,iRes) + Mark_Point(idxin)*Posit(:,idxin))/(Mark_Point(idxin)+1)
                     Mark_Point(idxin) = Mark_Point(idxin)+1
                     Scale_Point(idxin) = (GetGenScale(Posit(:,idxin),srcStat(:,pntI)))
                     
                     IF (flag.EQ.0) THEN
                      CALL IntQueue_Push(toOptimise,idxin)
                      flag = 1
                     END IF
                     
                                     END IF

                                

                                END IF
                                                                  
                              END DO
                                    

                                          
                            END IF  
                       END IF
                    END DO
                    
                    
                    
                    
                  END IF
                  
              !Tidy everything up
              CALL kdtree2_destroy(tree)
              deallocate(Isort,TempR)
       
       
            END IF

            WHERE(Mark_Point(1:NB_Point).GT.0) Mark_Point(1:NB_Point)=1

            
            IF (numIns .GT. 0) THEN
            
                IF(Debug_Display>0)THEN
                    WRITE(*,*)'Lattice Insertion: number of points to insert',numIns
                    WRITE(29,*)'Lattice Insertion: number of points to inser',numIns
                END IF
               
                NB_Tet_Old = NB_Tet
                 !We can insert all the points

                                
                                         
                 nallc_increase = numIns*5
                 DO itry = 1,5
                           DO ie = 1,NB_Tet_Old
                             IF(marker(ie) > 0 )THEN
                                 NumIP = child(ie)%numNodes
                                 
                               DO i = 1,NumIP
                                 ip = child(ie)%Nodes(i)
                                     IF (Mark_Point(ip).EQ.1) THEN
                                       IF (IP_Tet(4,ie)>0) IT = ie
                                          CALL Locate_Node_Tet(Posit(1:3,ip),IT,3)
                                       IF (IT>0) THEN
                                         !Try to insert the point

            
                                           Isucc = 0
                                           CALL Insert_Point_Delaunay(ip,IT,Isucc,0)
                                           IF (Isucc.EQ.1) THEN
                                               Mark_Point(ip) = 0
                                               marker(ie) = marker(ie)-1
                                             
                                           ENDIF
                       
                                       ENDIF
                         
                         
                                 ENDIF
                               ENDDO
                             ENDIF
                           ENDDO
                           CALL Remove_Tet()
                         ENDDO
                         
                         DO ie=1,NB_Tet_Old
                           CALL IntQueue_Clear(child(ie))
                         ENDDO
                         
                 deallocate(child)
         deallocate(marker)
         WHERE (Mark_Point(pEnd+1:NB_Point).EQ.1) Mark_Point(pEnd+1:NB_Point) = -999
                  
                  
          
          !Now optimise the position of the points which were merged
          CALL LinkAssociation_Clear(PointAsso)
          CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
          
          
         !NumIP = toOptimise%numNodes
         !IF(Debug_Display>0)THEN
                 ! WRITE(*,*)'Lattice Insertion: optimising ',NumIP,' nodes' 
                 !   WRITE(29,*)'Lattice Insertion: optimising ',NumIP,' nodes' 
         !END IF
           
         !DO i = 1,NumIP
                  
          !IF (Mark_Point(toOptimise%Nodes(i)).NE.-999) THEN
          !  CALL MCS_Optim_Gen_Point(myStart,toOptimise%Nodes(i))      
          !END IF
          
         !END DO
          
          
                  !Delete failed points after optimisation
          CALL IntQueue_Clear(toOptimise)
                  CALL Remove_Point()
                  
          
                      
                  
                 IF(Debug_Display>300)THEN
                     !Output bad elements to plt
                     
                     MeshOut%GridOrder = GridOrder
                     MeshOut%NB_Point  = NB_Point
                     MeshOut%NB_Tet    = NB_Tet
                     ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
                     ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
                     MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
                     MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)



                     !JobNameT = JobName(1:JobNameLength)//'_'//loopString
                     !JobNameLengthT = JobNameLength + 5
           
                     CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_'//loopString,JobNameLength+5, -15, MeshOut)
                     
                     DEALLOCATE(MeshOut%IP_Tet)
                     DEALLOCATE(MeshOut%Posit)


                  ENDIF

                  
                  IF ((NB_Point .EQ. pEnd)) THEN
                         
                  
                  
                  
                        done = 1
                  ELSE
                        IF(Debug_Display>0)THEN
                                    WRITE(*,*)'Lattice Insertion: number of points inserted',NB_Point-pEnd
                                    WRITE(29,*)'Lattice Insertion: number of points inserted',NB_Point-pEnd
                        END IF
                  
                  
                        pStart = pEnd+1
                        pEnd = NB_Point
                  
                  END IF
            
            ELSE
                        DO ie=1,NB_Tet
                           CALL IntQueue_Clear(child(ie))
                         ENDDO
                        deallocate(child)
                         deallocate(marker)
                  CALL Remove_Point()
                  done = 1
            END IF
!            

          
            
            CALL LinkAssociation_Clear(PointAsso)
      END DO
      
        IF (1 .EQ. smoothSrc) THEN
        !Smoothing
        lastObj = HUGE(0.d0)                 
         improve = 1
         allocate(PositNm1(3,NB_Point))
         
         OPEN(1998,file='centroidenergy.out',status='unknown')
         
                 WRITE(*,*)'Time Step:',dt,'Convergence:',conv*dt

        DO WHILE(improve.EQ.1)
          CALL Centroid_Smooth_Layer(myStart,myStart,nowObj,PositNm1,smtFlag,dt)
          
          CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
          CALL Next_Build()
          CALL Get_Tet_Circum(0)
          CALL Get_Tet_Volume(0)
          Mark_Point(1:NB_Point)=0 
          !DO i = myStart,NB_Point
          
          !  CALL pointLin_space(Posit(:,i),BGSpacing%Sources%Pt(pntI),Scale_Point(i))
      !Scale_Point(i) = PointSource_GetScale(Posit(:,i),BGSpacing%Sources%Pt(pntI))
      !Scale_Point(i) = GetGenScale(Posit(:,i),srcStat(:,pntI))
          
          !END DO
          CALL Insert_Points_EasyPeasy(myStart,NB_Point, 5)
          CALL Remove_Point()
          Mark_Point(1:NB_Point)=0   

          
          
          residual = ABS(1.d0 - (lastObj/nowObj))
          
          IF(Debug_Display>0)THEN
            WRITE(1998,*) nowObj, residual
            WRITE(*,*)'Centroid smooth energy, residual:',nowObj,residual
            WRITE(29,*)'Centroid smooth energy, residual:',nowObj,residual
          END IF
          
          
          IF (residual.LT.(conv*dt)) THEN
            improve = 0
          ELSE
            lastObj = nowObj
          END IF

          
        ENDDO   

        
        CLOSE(1998)
    deallocate(PositNm1)
    ENDIF
    
    IF (1 .EQ. optSrc) THEN
    !Optimisation
    lastObj = HUGE(0.d0)                 
         improve = 1
        
         
         OPEN(1998,file='optimiseenergy.out',status='unknown')
         
                 WRITE(*,*)'Convergence:',conv

        DO WHILE(improve.EQ.1)
          !CALL MCS_Line_Local(myStart-1,nowObj)
          CALL MCS_Optim_Gen_Layer(myStart,myStart,nowObj)
          CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
          CALL Next_Build()
          CALL Get_Tet_Circum(0)
          CALL Get_Tet_Volume(0)
          Mark_Point(1:NB_Point)=0 
          !DO i = myStart,NB_Point
          
            !CALL pointLin_space(Posit(:,i),BGSpacing%Sources%Pt(pntI),Scale_Point(i))
      !Scale_Point(i) = PointSource_GetScale(Posit(:,i),BGSpacing%Sources%Pt(pntI))
    !  Scale_Point(i) = GetGenScale(Posit(:,i),srcStat(:,pntI))
          
        !  END DO
          CALL Insert_Points_EasyPeasy(myStart,NB_Point, 5)
          CALL Remove_Point()
          Mark_Point(1:NB_Point)=0   

          
          
          residual = ABS(1.d0 - (lastObj/nowObj))
          
          IF(Debug_Display>0)THEN
            WRITE(1998,*) nowObj, residual
            WRITE(*,*)'Optimisation energy, residual:',nowObj,residual
            WRITE(29,*)'Optimisation energy, residual:',nowObj,residual
          END IF
          
          
          IF (residual.LT.conv) THEN
            improve = 0
          ELSE
            lastObj = nowObj
          END IF

          
        ENDDO   

        
        CLOSE(1998)
    END IF
  
    
        
    
    
    !Now we are going to cut a hole in the existing points
    !Loop over the points and if they are inside the alpha shape of this source delete
    Mark_Point(1:NB_Point) = 0
    DO ip = firstNode,myStart-1
      ie = -1
      p1 = Posit(:,ip)
      delta = Scale_Point(ip)
      CALL Locate_Node_Tet(p1,ie,3)
      IF (ie .GT. 0) THEN
      
        p2 = Posit(:,IP_Tet(1,ie))
      delta = MIN(delta,Scale_Point(IP_Tet(1,ie)))
      geoD = Geo3D_Distance_SQ(p1,p2)
      
      p2 = Posit(:,IP_Tet(2,ie))    
      delta = MIN(delta,Scale_Point(IP_Tet(2,ie)))
      geoD = MIN(geoD,Geo3D_Distance_SQ(p1,p2))
      
      p2 = Posit(:,IP_Tet(3,ie))    
      delta = MIN(delta,Scale_Point(IP_Tet(3,ie)))
      geoD = MIN(geoD,Geo3D_Distance_SQ(p1,p2))
      
      p2 = Posit(:,IP_Tet(4,ie))    
      delta = MIN(delta,Scale_Point(IP_Tet(4,ie)))
      geoD = MIN(geoD,Geo3D_Distance_SQ(p1,p2))
    
      
      delta = 0.5d0*delta*BGSpacing%BasicSize
       
      
        IF ((Sphere_Tet(4,ie) .LT. Scale_Point(ip)*BGSpacing%BasicSize) .OR. &
                 (geoD.LT.delta*delta))      THEN
      
        Mark_Point(ip) = -999
      
      ENDIF
      ENDIF
    ENDDO
        CALL Remove_Point()
        Mark_Point(1:NB_Point)=0
        smtFlag = 0
      
    END DO  
    deallocate(results)
    !Now we need to reinsert everything
    WRITE(*,*)'Reinserting everything'
      WRITE(29,*)'Reinserting everything'
    nallc_increase = 100000
    CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
      CALL Next_Build()
      CALL Get_Tet_Circum(0)
      CALL Get_Tet_Volume(0)
    Mark_Point(1:NB_Point)=0
    CALL Insert_Points_EasyPeasy(firstNode,NB_Point, 5)
    CALL Remove_Point()
      Mark_Point(1:NB_Point)=0 
    CALL Set_Domain_Mapping()
      
    ELSE
     
    !CALL Lattice_Insert_Background()
    !CALL Set_Domain_Mapping()
    !CALL Lattice_Insert(-1,NB_Point)
    
    CALL Insert_Octree()
    
    
    
    
    
    END IF
      
    !CALL Correct_Density(firstNode,1)
    
    RR_Point(1:NB_Point) = 0.d0
      CALL Get_Tet_Circum(0)
    !    CALL Check_Mesh_Geometry2('volume_raw')
  !  MeshOut%GridOrder = GridOrder
    !    MeshOut%NB_Point  = NB_Point
    !    MeshOut%NB_Tet    = NB_Tet
    !    ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
    !    ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
    !    MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
    !    MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)
  !  
    !    CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_volraw',JobNameLength+7, -15, MeshOut)
    !    
  !  
  !  
  !    noBad = 0
    !    DO ie=1,NB_Tet
    !      IF(Mark_Tet(ie)>0)THEN
  !      noBad = noBad+1
   !     ENDIF
  !    ENDDO 

    !    CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_volrawbad',JobNameLength+10, &
  !                                             -3, MeshOut, Surf,Mark_Tet,noBad)    
  !  
  !   
  !    DEALLOCATE(MeshOut%IP_Tet)
    !    DEALLOCATE(MeshOut%Posit)
     
    IF (1.EQ.1) THEN
      !Gradient based
    
    !CALL Gradient_Based(firstNode,1,NB_Point)
    !CALL  MCS_Optim(firstNode,NB_Point,1)
    !CALL Variational_Based(firstNode,1)
    !CALL CVT_Quick(firstNode,1)
    END IF
    
    
       IF (1 .EQ. smoothAll) THEN
        !Smoothing
        lastObj = HUGE(0.d0)                 
         improve = 1
         allocate(PositNm1(3,NB_Point))
         
         OPEN(1998,file='centroidenergy.out',status='unknown')
         
                 WRITE(*,*)'Time Step:',dt,'Convergence:',conv*dt

        DO WHILE(improve.EQ.1)
          CALL Centroid_Smooth_Layer(firstNode,firstNode,nowObj,PositNm1,smtFlag,dt)
          
          CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
          CALL Next_Build()
          CALL Get_Tet_Circum(0)
          CALL Get_Tet_Volume(0)
          Mark_Point(1:NB_Point)=0 
          CALL Set_Domain_Mapping()
          CALL Insert_Points_EasyPeasy(firstNode,NB_Point, 5)
          CALL Remove_Point()
          Mark_Point(1:NB_Point)=0   

          
          
          residual = ABS(1.d0 - (lastObj/nowObj))
          
          IF(Debug_Display>0)THEN
            WRITE(1998,*) nowObj, residual
            WRITE(*,*)'Centroid smooth energy, residual:',nowObj,residual
            WRITE(29,*)'Centroid smooth energy, residual:',nowObj,residual
          END IF
          
          
          IF ((residual.LT.(conv*dt)).OR.(nowObj.LT.(conv*dt))) THEN
            improve = 0
          ELSE
            lastObj = nowObj
          END IF

          
        ENDDO   

        
        CLOSE(1998)
    deallocate(PositNm1)
  END IF
        IF (1 .EQ. optAll) THEN  
    
    !Optimisation
    lastObj = HUGE(0.d0)                 
         improve = 1
        
         
         OPEN(1998,file='optimiseenergy.out',status='unknown')
         
                 WRITE(*,*)'Convergence:',conv

        DO WHILE(improve.EQ.1)
         ! CALL MCS_Line_Local(firstNode-1,nowObj)
          CALL MCS_Optim_Gen_Layer(firstNode,firstNode,nowObj)
          CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
          CALL Next_Build()
          CALL Get_Tet_Circum(0)
          CALL Get_Tet_Volume(0)
          Mark_Point(1:NB_Point)=0 
          CALL Set_Domain_Mapping()
          CALL Insert_Points_EasyPeasy(firstNode,NB_Point, 5)
          CALL Remove_Point()
          Mark_Point(1:NB_Point)=0   

          
          
          residual = ABS(1.d0 - (lastObj/nowObj))
          
          IF(Debug_Display>0)THEN
            WRITE(1998,*) nowObj, residual
            WRITE(*,*)'Optimisation energy, residual:',nowObj,residual
            WRITE(29,*)'Optimisation energy, residual:',nowObj,residual
          END IF
          
          
          IF ((residual.LT.conv).OR.(nowObj.LT.conv)) THEN
            improve = 0
          ELSE
            lastObj = nowObj
          END IF

          
        ENDDO   

        
        CLOSE(1998)
    
    
        
        
    
    ENDIF
    
    
    
    RR_Point(1:NB_Point) = 0.d0
      CALL Get_Tet_Circum(0)
        CALL Check_Mesh_Geometry2('volume')
    MeshOut%GridOrder = GridOrder
        MeshOut%NB_Point  = NB_Point
        MeshOut%NB_Tet    = NB_Tet
        ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
        ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
        MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
        MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)
    
        CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_vol',JobNameLength+4, -15, MeshOut)
    ALLOCATE(BadPointsVol(NB_Point))
    BadPointsVol(1:NB_Point) = 0
    
      noBad = 0
        DO ie=1,NB_Tet
          IF(Mark_Tet(ie)>0)THEN
        noBad = noBad+1
        BadPointsVol(IP_Tet(1:4,ie)) = 1
        ENDIF
      ENDDO 

        CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_volbad',JobNameLength+7, &
                                               -3, MeshOut, Surf,Mark_Tet,noBad)    
        
    DEALLOCATE(MeshOut%IP_Tet)
        DEALLOCATE(MeshOut%Posit)
    
    
    
    !Now it's time to cut a hole and reinsert
    
    !First store the current mesh
    CALL TetMeshStorage_CellBackup(NB_Tet, IP_Tet, VolumeSources)
    CALL TetMeshStorage_NodeBackup(NB_Point, Posit, VolumeSources)
    
    ALLOCATE(SpacingBackup(NB_Point))
    
    SpacingBackup(1:NB_Point) = Scale_Point(1:NB_Point)
    
    CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, BoundaryLayers)
    CALL TetMeshStorage_NodeRestore(NB_Point, Posit, BoundaryLayers)
    NB_Blay = NB_Point
    
    
    Scale_Point(1:NB_Point) = SpacingBackupB(1:NB_Point)
        CALL Next_Build()
        CALL Get_Tet_Circum(0)
        CALL Get_Tet_Volume(0)
    CALL Set_Domain_Mapping()
    Mark_Point(1:NB_Point) = 1
    layersEnd = NB_Point !This is the last point of the boundary elements
    myStart = NB_Point+1

    ADTree_Search = .TRUE.
    CALL ADTree_SetName(ADTreeForMesh,'buildsources')
    CALL ADTree_SetTree(ADTreeForMesh, Posit, IP_Tet, NB_Tet, NB_Point)

    numBadPointVol = 0
    DO ip = firstNode,VolumeSources%NB_Point
      ie = -1
      p1 = VolumeSources%Posit(:,ip)
      
      Isucc1 = -1
      CALL ADTree_SearchNode(ADTreeForMesh,p1,ie,ipp,w4, Isucc1)

      
      CALL Locate_Node_Tet(p1,ie,3)
      

      IF (ie .GT. 0) THEN
     
        flag = 0
      geoMin = HUGE(0.0d0)
      DO iFace = 1,4  
        p2 = Posit(:,IP_Tet(iFace,ie))
        delta = 0.5d0*MIN(SpacingBackup(ip),Scale_Point(IP_Tet(iFace,ie)))*BGSpacing%BasicSize
        delta = delta*delta*gapWidth*gapWidth
        geoD = Geo3D_Distance_SQ(p1,p2)
        IF (geoD.LT.geoMin) THEN
          geoMin = geoD
          ipClose = IP_Tet(iFace,ie)
        END IF
        IF (geoD.GT.delta) THEN
          flag = flag + 1
        END IF
      END DO
        
      
      DO iFace = 1,4    

        ippp(1:3) = IP_Tet(iTri_Tet(1:3,iFace),ie)
        CALL Boundary_Tri_Match(ippp(1),ippp(2),ippp(3),ib)

        IF (ib.GT.0) THEN
          !Now check what kind of boundary
          IF (  (Type_Wall(Surf%IP_Tri(5,ib)).NE.2).AND. &
              (Type_Wall(Surf%IP_Tri(5,ib)).NE.3) ) THEN
            flag = -1
          ENDIF

        ENDIF


      !  IF ( IP_Tet(iTri_Tet(1,iFace),ie)<=NB_BD_Point.AND. &
      !    IP_Tet(iTri_Tet(2,iFace),ie)<=NB_BD_Point.AND. &
      !    IP_Tet(iTri_Tet(3,iFace),ie)<=NB_BD_Point ) THEN
      !  flag = -1
      ! ENDIF
        
      ENDDO
      
      
        IF (flag.EQ.4)  THEN
      !IF (Sphere_Tet(4,ie) .GT. BGSpacing%BasicSize) THEN
        NB_Point = NB_Point + 1
              IF(NB_Point>nallc_point) THEN  
                CALL ReAllocate_Point()
              END IF
              Posit(:,NB_Point) = p1
        Scale_Point(NB_Point) = SpacingBackup(ip)
        numBadPointVol = numBadPointVol + BadPointsVol(ip)
      ELSE
        !Move towards the one which was nearest
      !  IF (ipClose.GT.NB_BD_Point) THEN
      !    
      !  
      !    Posit(:,ipClose) = (p1(:) + &
      !    Mark_Point(ipClose)*Posit(:,ipClose))/(Mark_Point(ipClose)+1)
      !  Mark_Point(ipClose) = Mark_Point(ipClose)+1
      !  
      !  END IF
        
      ENDIF
      
      ENDIF
    ENDDO

    ADTree_Search = .FALSE.
      CALL ADTree_Clear(ADTreeForMesh)

    DEALLOCATE(BadPointsVol)
    !Restore boundary triangulation and reinsert
    
    CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
    CALL Next_Build()
    CALL Set_Domain_Mapping()
    CALL Get_Tet_SphereCross(0)
    CALL Get_Tet_Volume(0)
    
        
        Mark_Point(1:NB_Point)=0
    
    CALL Insert_Points_EasyPeasy(CVTMeshInit%NB_Point+1,NB_Point, 5)
        CALL Remove_Point()
        Mark_Point(1:NB_Point)=0  
    CALL Next_Build()
        CALL Get_Tet_Circum(0)
        CALL Get_Tet_Volume(0)
    CALL Set_Domain_Mapping()

    !====Break large elements
    CALL Element_Break()
    
    RR_Point(1:NB_Point) = 0.d0
    
    
    
    MeshOut%GridOrder = GridOrder
        MeshOut%NB_Point  = NB_Point
        MeshOut%NB_Tet    = NB_Tet
        ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
        ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
        MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
        MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)
    
        CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_rawstich',JobNameLength+9, -15, MeshOut)
        
    DEALLOCATE(MeshOut%IP_Tet)
        DEALLOCATE(MeshOut%Posit)

        CALL Check_Mesh_Geometry2('before_optimisation')

        !Lets do some stats

      

   
    
    nVol = 0
    nbVol = 0

    nAdv = 0
    nbAdv = 0

    nLat = 0
    nbLat = 0

    DO ie=1,NB_Tet

     DO i1 = 1,4
      ip = IP_Tet(i1,ie)
      
      IF (ip.GT.layersEnd) THEN
        nVol = nVol + 1
        IF(Mark_Tet(ie)>0)THEN
          nbVol = nbVol + 1
          ENDIF
     

      ELSE IF (ip.GT.numAdvFro) THEN
        nAdv = nAdv + 1
        IF(Mark_Tet(ie)>0)THEN
          nbAdv = nbAdv + 1
          ENDIF

        ELSE
          nLat = nLat + 1
        IF(Mark_Tet(ie)>0)THEN
          nbLat = nbLat + 1
          ENDIF


      ENDIF

     ENDDO
          
      ENDDO 


       !1) What percentage of the mesh is the boundary layer
    percBound = (REAL(nAdv+nLat)/REAL(nLat+nAdv+nVol))*100.0d0
    WRITE(*,*)'Percentage of mesh which is in the boundary layers :',percBound
        WRITE(29,*)'Percentage of mesh which is in the boundary layers :',percBound

      !3) What percentage of bad elements in volume post stich
    
    WRITE(*,*)'Percentage bad in volume after stich ', (REAL(nbVol)/REAL(nVol))*100.0d0
         WRITE(29,*)'Percentage bad in volume after stich ', (REAL(nbVol)/REAL(nVol))*100.0d0
    
    
    
         !5) What percentage bad elements in advfron after stich
    
    WRITE(*,*)'Percentage bad in advance front after stich ',(REAL(nbAdv)/REAL(nAdv))*100.0d0
         WRITE(29,*)'Percentage bad in advance front after stich ', (REAL(nbAdv)/REAL(nAdv))*100.0d0
    
    !6) What percentage bad elements in lattice after stich
    
    WRITE(*,*)'Percentage bad in lattice front after stich ', (REAL(nbLat)/REAL(nLat))*100.0d0
         WRITE(29,*)'Percentage bad in lattice front after stich ', (REAL(nbLat)/REAL(nLat))*100.0d0
    

    !Collapse small tets
    !Collapse_Angle          = (12.0d0*3.141592653589793d0)/180.d0
        !Swapping_Angle          = (12.0d0*3.141592653589793d0)/180.d0
    
    !WRITE(*,*)' '
        !WRITE(*,*)'---- Swapping ----'
        !CALL Swap3D(2)
  
        !WRITE(*,*)' '
        !WRITE(*,*)'---- Collapse Tiny Angles ----'
        !CALL Element_Collapse( )
    
  !  CALL Remove_Point()
     !   Mark_Point(1:NB_Point)=0  
  !  CALL Next_Build()
    !    CALL Get_Tet_Circum(0)
    !    CALL Get_Tet_Volume(0)
  !  CALL Set_Domain_Mapping()
    
    !  CALL Check_Mesh_Geometry2('afterCollapse')
    



  !  CALL Gradient_Based(CVTMeshInit%NB_Point+1,-1,NB_Blay)
    Swapping_Angle          = (12.0d0*3.141592653589793d0)/180.d0
    Collapse_Angle          = (12.0d0*3.141592653589793d0)/180.d0
    !WRITE(*,*)' '
    !WRITE(*,*)'---- Collapse Tiny Angles ----'
    !Kswap = 2
    !CALL Swap3D(Kswap)
    !CALL Element_Collapse( )
    !CALL Next_Build()

    !CALL Check_Mesh_Geometry2('afterCollapse1')
    
    RR_Point(1:NB_Point) = 0.d0    
        !CALL Check_Mesh_Geometry2('after_gradient1')
    !CALL Coevolution(CVTMeshInit%NB_Point+1,NB_Blay)
    
    
    !Now generate some points to help grade inbetween the boundary and volume
    !myStart = NB_Point+1
    !CALL Lattice_Insert(-1,NB_Point)
    !Mark_Point(1:NB_Point)=0  
    !CALL Next_Build()
        !CALL Get_Tet_Circum(0)
        !CALL Get_Tet_Volume(0)
    !CALL Set_Domain_Mapping()
    
    
    
    
    
    
    MeshOut%GridOrder = GridOrder
        MeshOut%NB_Point  = NB_Point
        MeshOut%NB_Tet    = NB_Tet
        ALLOCATE(MeshOut%IP_Tet(numTetNodes, MeshOut%NB_Tet))
        ALLOCATE(MeshOut%Posit(3,          MeshOut%NB_Point))
        MeshOut%IP_Tet(:, 1:NB_Tet)   = IP_Tet(:, 1:NB_Tet)
        MeshOut%Posit(:,  1:NB_Point) = Posit(:,  1:NB_Point)
    
        CALL HybridMeshStorage_Output(JobName(1:JobNameLength)//'_raw',JobNameLength+4, -15, MeshOut)
        
    DEALLOCATE(MeshOut%IP_Tet)
        DEALLOCATE(MeshOut%Posit)

        WRITE(*,*) SUM(RR_Point(1:NB_Point))
    
    !RR_Point(1:NB_Point) = 0.d0
  
    CONTAINS
  
  
  FUNCTION GetGenScale(P,inp) RESULT(S)
    USE common_Parameters
    USE SpacingStorage
    USE SpacingStorageGen
    USE Source_Type
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P(3)
    INTEGER, INTENT(IN) :: inp(3)
    REAL*8 :: S
    
    IF (inp(1).EQ.1) THEN
    !Point source
    
    S = PointSource_GetScale(P,BGSpacing%Sources%Pt(inp(2)))
    
    ELSE IF (inp(1).EQ.2) THEN
    !Line source
    
    S = LineSource_GetScale(P,BGSpacing%Sources%Ln(inp(2)))
    
    ELSE
    !Tri source
    S = TriSource_GetScale(P,BGSpacing%Sources%Tri(inp(2)))
    
    ENDIF




  END FUNCTION GetGenScale
      
      

END SUBROUTINE Build_Sources


SUBROUTINE Insert_Octree()
  !This generates a hex tree compliant with the spacing function
  !and inserts it as a tet mesh
  USE common_Constants
    USE common_Parameters
  USE OctreeMesh
  USE OctreeMeshManager
  USE TetMeshStorage
  IMPLICIT NONE
  
  REAL*8 :: Xmax1, Xmin1, Ymax1, Ymin1, Zmax1, Zmin1, dxyz(3), pp1(3),dd
  REAL*8 :: pc(3),vec(3)
  INTEGER :: imax, jmax, kmax, NI(3),ip,NB_PointOld,level,numHang
  INTEGER :: maxHang,ipHang(18),iph,done,numCells,flag
  TYPE(OctreeMesh_Type) :: octmesh
  TYPE(OctreeMesh_Cell_Type), POINTER :: pOct_Cell
  TYPE(TetMeshStorageType) :: TetMesh
  
  maxHang = 0
  
  !Work out initial values for the Oct mesh
  Xmin1 = XYZmin(1) - (XYZmax(1)-XYZmin(1))*0.1d0
    Xmax1 = XYZmax(1) + (XYZmax(1)-XYZmin(1))*0.1d0
    Ymin1 = XYZmin(2) - (XYZmax(2)-XYZmin(2))*0.1d0
    Ymax1 = XYZmax(2) + (XYZmax(2)-XYZmin(2))*0.1d0
    Zmin1 = XYZmin(3) - (XYZmax(3)-XYZmin(3))*0.1d0
    Zmax1 = XYZmax(3) + (XYZmax(3)-XYZmin(3))*0.1d0
  
  Xmin1 = MINVAL((/xmin1,ymin1,zmin1/))
  Xmax1 = MAXVAL((/xmax1,ymax1,zmax1/))
  Ymin1 = Xmin1
  Ymax1 = Xmax1
  Zmin1 = Xmin1
  Zmax1 = Xmax1
  
     WRITE(*,*) 'Octree Domain size ', Xmin1,Xmax1,Ymin1,Ymax1,Zmin1,Zmax1



  dxyz(1) = (Xmax1-Xmin1)*0.5d0
    dxyz(2) = (Ymax1-Ymin1)*0.5d0
    dxyz(3) = (Zmax1-Zmin1)*0.5d0
  dd = MINVAL(dxyz)  
    imax  = (Xmax1-Xmin1)/dd +1
    jmax  = (Ymax1-Ymin1)/dd +1
    kmax  = (Zmax1-Zmin1)/dd +1
    IF(MOD(imax,2)==1) imax = imax+1
    IF(MOD(jmax,2)==1) jmax = jmax+1
    IF(MOD(kmax,2)==1) kmax = kmax+1
  imax = MAX(imax,jmax)
  imax = MAX(imax,kmax)
  jmax = imax
  kmax = imax
    dxyz(1) = dd!(Xmax1-Xmin1)/imax
    dxyz(2) = dd!(Ymax1-Ymin1)/jmax
    dxyz(3) = dd!(Zmax1-Zmin1)/kmax
  pp1(:) = (/xmin1,ymin1,zmin1/)
    NI(:)  = (/imax,jmax,kmax/)

    WRITE(*,*), NI, dxyz,pp1

    CALL OctreeMesh_Set(NI, dxyz, pp1, octmesh)
  
  !Refine by spacing
  CALL OctreeMesh_RefineBySpacing2(octmesh,BGSpacing)
  
  !Find the leaf cells
  CALL OctreeMesh_getLeafCells(octmesh)
  
  !Insert the points into a mesh
  WRITE(*,*) 'The octmesh has ',octmesh%numNodes,' nodes to insert'
  NB_PointOld = NB_Point
  
  done = 0
  
  DO WHILE (done.EQ.0)
    flag = 0
    numCells = octmesh%numCells
    DO ip = 1,octmesh%numCells
    
      pOct_Cell => octmesh%LeafCells(ip)%to
      IF (.NOT.pOct_Cell%Bisected) THEN
      CALL OctreeMesh_getCellHangNodes(pOct_Cell,octmesh,ipHang)
      numHang = 0
      DO iph = 1,18
        IF (ipHang(iph).GT.0) THEN
         numHang = numHang + 1
        END IF
      END DO
    
    
      !IF (numHang.GT.5) THEN
        !  flag = 1
      !  CALL OctreeMesh_CellBisect(pOct_Cell,octmesh)
      
      !END IF
    
      maxHang = MAX(maxHang,numHang)
      END IF
    END DO
    
    !Find the leaf cells
    CALL OctreeMesh_getLeafCells(octmesh)
    WRITE(*,*) 'The octmesh has ',maxHang,' maximum hanging nodes, refining'
    IF (flag.EQ.0) THEN
      done = 1
    END IF
  
    
  
  END DO
  
  IF (1.EQ.0) THEN
    DO ip = 1,octmesh%numNodes
      NB_Point = NB_Point + 1
      IF(NB_Point>nallc_point) THEN     
        CALL ReAllocate_Point()
      END IF
      Posit(:,NB_Point) = octmesh%Posit(ip,:)
      
      CALL Get_Point_Mapping(NB_Point)
    
    END DO
  END IF
  
  maxHang = 0
        WRITE(*,*) 'The octmesh has ',octmesh%numNodes,' nodes to insert'
       WRITE(*,*) 'The octmesh has ', octmesh%numCells,' cells'  
   DO ip = 1,octmesh%numCells
    pOct_Cell => octmesh%LeafCells(ip)%to
    
    CALL OctreeMesh_getCellHangNodes(pOct_Cell,octmesh,ipHang)
    numHang = 0
    DO iph = 1,18
      IF (ipHang(iph).GT.0) THEN
        numHang = numHang + 1
      END IF
    END DO
    
    maxHang = MAX(maxHang,numHang)
    
    level = pOct_Cell%Level
    pc(:) = OctreeMesh_getCellCentre(pOct_Cell,octmesh)
    
    NB_Point = NB_Point + 1
    IF(NB_Point>nallc_point) THEN     
      CALL ReAllocate_Point()
    END IF
    
    Posit(:,NB_Point) = pc(:) 
    
    CALL Get_Point_Mapping(NB_Point)  
   END DO
  
  
  WRITE(*,*) 'The octmesh has ',maxHang,' maximum hanging nodes'
  
    Mark_Point(1:NB_Point) = 0
  CALL Insert_Points_EasyPeasy(NB_PointOld+1, NB_Point, 1)
  usePower = .FALSE.
  CALL Remove_Point()
  

END SUBROUTINE Insert_Octree


SUBROUTINE Centroid_Smooth_Layer(NB1,NB2,residual,PositNm1,smtFlag,h)

  USE common_Constants
  USE common_Parameters
  USE array_allocator
  USE SpacingStorage
  USE Geometry3DAll
  USE RandomModule
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: NB1,NB2
  REAL*8  :: newPosit(3,NB_Point)!,oldPosit !To store new positions
  REAL*8 :: weight(NB_Point) !To store the weights
  REAL*8, INTENT(OUT) :: residual
  INTEGER :: IT,i,ip,nbp,improve,Old_NB_Point,flag,K,ie
  REAL*8 :: wTet,obj,lastObj,sqrt5o4,delta,length,quality,fMap(3,3),unitD,maxD
  REAL*8, DIMENSION(3) :: cTet,p1,pC
  REAL*8, INTENT(INOUT) :: PositNm1(3,NB_Point)
  INTEGER, INTENT(INOUT) :: smtFlag
  REAL*8, INTENT(IN) :: h  
  REAL*8 :: ptet(3,4),p1t(3),dum(4)
  INTEGER :: badTet(NB_Tet)

  sqrt5o4 = sqrt(5.d0)/4.d0
  
  !Step size
  !h = 0.1d0
  
  
  
  !NB1<NB2
  !ip < NB1 is outside the source in question so elements with these inside need to be ignored
  !ip>NB2 are free
 
  
  !Initialise everything
   weight(1:NB_Point) = 0.d0
   newPosit(1:3,1:NB_Point) = 0.d0
   badTet(NB_Tet) = 0
   residual = 0.d0
   
   !Now loop over elements
    DO IT=1,NB_Tet
            
        !First check this tet exists in the current point
        flag = 0
        DO i=1,4
            IF (IP_Tet(i,IT).LT.NB1) flag = 1
       
        END DO
        
    
        
        
        IF ((flag.EQ.0)) THEN
          
          pC(:) = ( Posit(:,IP_Tet(1,IT)) + Posit(:,IP_Tet(2,IT))    &
                                + Posit(:,IP_Tet(3,IT)) + Posit(:,IP_Tet(4,IT)) ) / 4.d0
                                
          !delta = ( Scale_Point(IP_Tet(1,IT)) + Scale_Point(IP_Tet(2,IT))    &
           !                     + Scale_Point(IP_Tet(3,IT)) + Scale_Point(IP_Tet(4,IT)) ) / 4.d0
          
          !delta = delta*BGSpacing%BasicSize*sqrt5o4
          
          DO i=1,4
                ip = IP_Tet(i,IT)
                IF (ip>(NB2)) THEN
                    p1(:) = Posit(:,ip)
                    length = Geo3D_Distance(pC,p1)
                    
                    IF (length>0.d0) THEN
                      delta = Scale_Point(IP_Tet(i,IT))*BGSpacing%BasicSize*sqrt5o4
                      quality = (delta-length)*(delta-length)
                    
                      residual = residual + quality
                    
                      newPosit(:,ip) = newPosit(:,ip) + quality*( pC(:) + &
                                     (delta/length)*(Posit(:,ip)-pC(:)))
                
                      weight(ip) = weight(ip) + quality
                    ENDIF
        
                END IF
            END DO
        
        
        ENDIF
    END DO
  
    !residual = residual/NB_Tet
  
    IF (smtFlag.EQ.0) THEN
        DO ip = NB2,NB_Point
            IF (weight(ip)>0.d0) THEN
                   PositNm1(:,ip) = (newPosit(:,ip)/weight(ip))-Posit(:,ip)
                   Posit(:,ip) = Posit(:,ip) + h*(PositNm1(:,ip))
                
                
            ENDIF
        END DO
        smtFlag = 1
    ELSE
        DO ip = NB2,NB_Point
            IF (weight(ip)>0.d0) THEN
                   pC(:) = (newPosit(:,ip)/weight(ip))-Posit(:,ip)
                   Posit(:,ip) = Posit(:,ip) + (h/2)*((3.0d0*pC(:)) - PositNm1(:,ip))
                   PositNm1(:,ip) = pC(:)
                
            ENDIF
        END DO
    ENDIF
    
     
    

END SUBROUTINE Centroid_Smooth_Layer

SUBROUTINE MCS_Line_Local(NB1,sumObj)
  USE common_Constants
  USE common_Parameters
  USE array_allocator
  USE SpacingStorage
  USE OptimisingModule
  USE Geometry3DAll
  USE RandomModule
  IMPLICIT NONE
  
  INTEGER, INTENT(IN)  :: NB1  !The first point effected by this process
  INTEGER :: K,ie,ip,DD,N,Ibest,I,Loop,Istatus,Iin,Ni,Di,idum,isFinished,numLoops,ic,flag,nbp,improve,NB_old,L,j,ii,le,iii,flag2
  REAL*8, allocatable  :: optx(:), optl(:), optu(:), optg(:) ,ptemp(:),Pt(:,:),Qu(:),vardef(:,:)!,allPsave(:,:)
  REAL*8 :: dd2,ftemp,fbest,ranNum,d,FTarget,FBig,p1(3),dtet,psave(3),rsave,vol,ptet(3,4),p2(3),p3(3),p4(3)
  REAL*8 :: pp4(3,4),dum(4),coef,lastObj,p1t(3),p2t(3),residual,delta,dist
  REAL*8, INTENT(OUT) :: sumObj
  INTEGER :: ITnb,merged
  REAL*8 :: minl, maxl, templ, aspectRatio
  REAL*8 :: v(3),R1,R2,x(3)
  
  lastObj = HUGE(0.d0)
  
  improve = 1
  
  L = 0
  
  
    
    FTarget = (1.D-6)*BGSpacing%MinSize
    FBig = 1.D30
    idum = -1
    sumObj = 0.d0
    
    !Get link info
    CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
    
    !Set up the optimisation problem
    !CALL init_random_seed()
    numLoops = 2 !Number of MCS loops
    DD = 1  !Number of dimensions - we are transforming this to a line search
    N = 10  !Number of eggs
    
    allocate(ptemp(DD),Pt(DD,N),Qu(N),optx(DD), optl(DD), optu(DD), optg(DD),vardef(DD,2))
    
    
    
    !Loop around tets
    DO le = 1,NB_Tet
    
      ! We want to see if this element is bad or not, and if it is bad which element the point
      ! lies in
      CALL Get_Tet_Circum(le) !Need to call this since it changes
      v = Sphere_Tet(1:3,le)
      
      ie = le
      CALL Locate_Node_Tet(v,ie,3)
      
      !First check this tet exists in the current point
      flag2 = 0
      DO i=1,4
        IF (IP_Tet(i,le).LT.NB1) flag2 = 1
         
      END DO
      
      IF ((ie/=le).AND.(flag2.EQ.0)) THEN
        !Just incase we have really tiny elements and the centre is not in adjacent elem
        ip = IP_Tet(1,le)  
        
        IF (ie>0) THEN
          !Then the element is bad and the voronoi point is in ie
          !Now we need to find the node in ie which is opposite le
          DO ii = 1,4    
          
            IF (le.EQ.Next_Tet(ii,ie)) THEN
              ip = IP_Tet(ii,ie)
            
            ENDIF
          ENDDO
        ENDIF
        
        !So ip is our node
        
        
        IF (ip>NB1) THEN
          
          !Get connected elements
          CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)  
          ptemp(1:DD) = 0.0d0
          Ibest = 0
          ftemp = 0.0d0
          I = 0
          Loop = 0
          Istatus = 0
          Iin = 0
          Qu(1:N) = huge(0.0)
          fbest = huge(0.0)

          !v(:) = ( Posit(:,IP_Tet(1,ie)) + Posit(:,IP_Tet(2,ie))    &
                    !            + Posit(:,IP_Tet(3,ie)) + Posit(:,IP_Tet(4,ie)) ) / 4.d0
          
          x = Posit(:,ip)
          R1 = Geo3D_Distance(v,Posit(:,IP_Tet(1,le)))
          R2 = Geo3D_Distance(v,x)
          
        
          !Upper and lower bounds are going to be R1 and R2
          IF (R1>R2) THEN
            optl(1:DD) = R2
            optu(1:DD) = R1
          ELSE
            optl(1:DD) = R1
            optu(1:DD) = R2
          ENDIF
        
          !The last egg will be the initial position
          DO Ni = 1,N
          
            
          
            DO Di = 1,DD
            
              Pt(Di,Ni) = optl(Di) + (Ni/N)*(optu(Di)-optl(Di))
            
            
            ENDDO
            
          ENDDO
          
          !Make one the original point
          Pt(1:DD,N/2) = R2
        
          isFinished = 0
          
          !WRITE(*,*)'x:',x,'R2',R2
          idum = -1
          DO WHILE (isFinished==0)
          
            
          
            CALL Optimising_Cuckoo_Driver(DD,N,ptemp,Ibest,ftemp,Pt,I,Loop,Istatus,Iin,Qu,fbest,optu,optl,idum,0.1d0,0.7d0)
            
            IF ((Loop-1).LT.numLoops) THEN
            
              !Calculate fitness at ptemp and put it into ftemp
              DO iii = 1,3
                Posit(iii,ip) = v(iii) + ((ptemp(1)/R2)*(x(iii)-v(iii)))
              END DO
              !WRITE(*,*)'Posit:',Posit(:,ip), 'ptemp', ptemp
              !ftemp = ptemp(4)*ptemp(4)
              ftemp = 0
              flag = 0
              
              DO ic = 1,List_Length
              
                ie = ITs_List(ic)
                p1 = Posit(:,IP_Tet(1,ie))
                p2 = Posit(:,IP_Tet(2,ie))
                p3 = Posit(:,IP_Tet(3,ie))
                p4 = Posit(:,IP_Tet(4,ie))
                vol = Geo3D_Tet_Volume(p1,p2,p3,p4)
                
                templ = Geo3D_Distance_SQ(p1,p2)
                minl = templ
                maxl = templ
                
                templ = Geo3D_Distance_SQ(p1,p4)
                minl = MIN(minl,templ)
                maxl = MAX(maxl,templ)
                
                templ = Geo3D_Distance_SQ(p1,p3)
                minl = MIN(minl,templ)
                maxl = MAX(maxl,templ)              
                
                templ = Geo3D_Distance_SQ(p3,p4)
                minl = MIN(minl,templ)
                maxl = MAX(maxl,templ)
                
                templ = Geo3D_Distance_SQ(p2,p4)
                minl = MIN(minl,templ)
                maxl = MAX(maxl,templ)
                
                templ = Geo3D_Distance_SQ(p2,p3)
                minl = MIN(minl,templ)
                maxl = MAX(maxl,templ)
                
                !Do 1 minus since we are minimising
                aspectRatio = 1.0d0 -(minl/maxl)
                
                IF(vol.LE.0.d0)THEN
                  flag = 1
                  ftemp = huge(0.d0)
                  EXIT
                ELSE
                  CALL Get_Tet_Circum(ie) 
                  !Check if it's inside
                  ptet(:,1) = p1
                  ptet(:,2) = p2
                  ptet(:,3) = p3
                  ptet(:,4) = p4
                  p1t = Sphere_Tet(1:3,ie)
                  K=0
                  dum =  Geo3D_Tet_Weight(K,ptet,p1t)
                  
                  IF (K==1) THEN
                    !We are inside
                    ftemp = ftemp + aspectRatio/vol
                  ELSE
                    !There is a possibility to merge
                    merged = 0
                    !IF(BGSpacing%Model>0)THEN
                    !
                    !  delta = Scale_Tet(ie)*BGSpacing%BasicSize
                    !      
                    !ELSE IF(BGSpacing%Model<0)THEN
                    !  delta = MINVAL(fMap_Tet(:,:,ie))*BGSpacing%BasicSize
                    !      
                    !END IF
                    
                    delta = ( Scale_Point(IP_Tet(1,ie)) + Scale_Point(IP_Tet(2,ie))    &
                                         + Scale_Point(IP_Tet(3,ie)) + Scale_Point(IP_Tet(4,ie)) ) / 4.d0
                    delta = delta*BGSpacing%BasicSize
                    
                    DO ii = 1,4
                      ITnb = Next_Tet(ii,ie)
                      IF(ITnb<=0) CYCLE
                      CALL Get_Tet_Circum(ITnb)
                      p2t = Sphere_Tet(:,ITnb)
                      dd2 = Geo3D_Distance_SQ(p2t, p1t)
                      IF(dd2<((0.1*delta)*(0.1*delta)))THEN
                         merged = 1
                      ENDIF
                    ENDDO
                    IF (merged.EQ.0) THEN
                      p1t = Sphere_Tet(1:3,ie)
                      coef = 1.d0
                      p2t = (p1+p2+p3+p4) / 4.d0
                      dd2 = Geo3D_Distance_SQ(p2t, p1t)
                      
                      ftemp = ftemp + (((coef*dd2/delta)+aspectRatio)/vol)
                    ELSE
                       ftemp = ftemp + aspectRatio/vol
                    ENDIF
                  ENDIF

                  
                  
                  ENDIF
              
              ENDDO
              
              
              IF (flag.EQ.1) THEN
                ftemp = huge(0.d0)
              ENDIF
              IF (ftemp==0) THEN
                isFinished = 1
                EXIT
              ENDIF
              
            ELSE
            
              !The best result is in Pt(:,Ibest)
              Posit(1:3,ip) = v(1:3) + (Pt(1,Ibest)/R2)*(x(1:3)-v(1:3))
              
              
              sumObj = sumObj + fbest
              isFinished = 1
              EXIT
            ENDIF
        
          ENDDO
            
            
            
            
            
            
            
            
            
            
            ENDIF
          ENDIF
      
        

        
        
          
          
          
    
    ENDDO
    deallocate(ptemp,Pt,Qu,optx, optl, optu, optg,vardef)


END SUBROUTINE MCS_Line_Local


SUBROUTINE MCS_Line_Local_Gen(NB1,sumObj)
  USE common_Constants
  USE common_Parameters
  USE array_allocator
  USE SpacingStorage
  USE OptimisingModule
  USE Geometry3DAll
  USE RandomModule
  IMPLICIT NONE
  
  INTEGER, INTENT(IN)  :: NB1  !The first point effected by this process
  INTEGER :: K,ie,ip,DD,N,Ibest,I,Loop,Istatus,Iin,Ni,Di,idum,isFinished
  INTEGER :: numLoops,ic,flag,nbp,improve,NB_old,MaxL,L,j,ii,le,iii,flag2
  REAL*8, allocatable  :: optx(:), optl(:), optu(:), optg(:) ,ptemp(:),Pt(:,:)
  REAL*8, allocatable :: Qu(:),vardef(:,:)!,allPsave(:,:)
  REAL*8 :: dd2,ftemp,fbest,ranNum,d,FTarget,FBig,p1(3),dtet,psave(3),rsave
  REAL*8 :: vol,ptet(3,4),p2(3),p3(3),p4(3),delta2,sqrt5o4,pC(3),length
  REAL*8 :: pp4(3,4),dum(4),coef,lastObj,p1t(3),p2t(3),residual,delta,dist
  REAL*8, INTENT(OUT) :: sumObj
  INTEGER :: ITnb,merged
  
  REAL*8 :: v(3),R1,R2,x(3)
  
  lastObj = HUGE(0.d0)
  sqrt5o4 = sqrt(5.d0)/4.d0
  improve = 1
  MaxL = 100
  L = 0
  
  
    
    FTarget = (1.D-6)*BGSpacing%MinSize
    FBig = 1.D30
    idum = -1
    sumObj = 0.d0
    
    !Get link info
    CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
    
    !Set up the optimisation problem
    !CALL init_random_seed()
    numLoops = 2 !Number of MCS loops
    DD = 1  !Number of dimensions - we are transforming this to a line search
    N = 10  !Number of eggs
    
    allocate(ptemp(DD),Pt(DD,N),Qu(N),optx(DD), optl(DD), optu(DD), optg(DD),vardef(DD,2))
    
    
    
    !Loop around tets
    DO le = 1,NB_Tet
    
      ! We want to see if this element is bad or not, and if it is bad which element the point
      ! lies in
      CALL Get_Tet_Circum(le) !Need to call this since it changes
      v = Sphere_Tet(1:3,le)
      
      ie = le
      CALL Locate_Node_Tet(v,ie,3)
      
      !First check this tet exists in the current point
      flag2 = 0
      DO i=1,4
        IF (IP_Tet(i,le).LT.NB1) flag2 = 1
         
      END DO
      
      IF ((ie/=le).AND.(flag2.EQ.0)) THEN
        !Just incase we have really tiny elements and the centre is not in adjacent elem
        ip = IP_Tet(1,le)  
        
        IF (ie>0) THEN
          !Then the element is bad and the voronoi point is in ie
          !Now we need to find the node in ie which is opposite le
          DO ii = 1,4    
          
            IF (le.EQ.Next_Tet(ii,ie)) THEN
              ip = IP_Tet(ii,ie)
            
            ENDIF
          ENDDO
        ENDIF
        
        !So ip is our node
        
        
        IF (ip>NB1) THEN
          
          !Get connected elements
          CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)  
          ptemp(1:DD) = 0.0d0
          Ibest = 0
          ftemp = 0.0d0
          I = 0
          Loop = 0
          Istatus = 0
          Iin = 0
          Qu(1:N) = huge(0.0)
          fbest = huge(0.0)

          !v(:) = ( Posit(:,IP_Tet(1,ie)) + Posit(:,IP_Tet(2,ie))    &
                    !            + Posit(:,IP_Tet(3,ie)) + Posit(:,IP_Tet(4,ie)) ) / 4.d0
          
          x = Posit(:,ip)
          R1 = Geo3D_Distance(v,Posit(:,IP_Tet(1,le)))
          R2 = Geo3D_Distance(v,x)
          
        
          !Upper and lower bounds are going to be R1 and R2
          IF (R1>R2) THEN
            optl(1:DD) = R2
            optu(1:DD) = R1
          ELSE
            optl(1:DD) = R1
            optu(1:DD) = R2
          ENDIF
        
          !The last egg will be the initial position
          DO Ni = 1,N
          
            
          
            DO Di = 1,DD
            
              Pt(Di,Ni) = optl(Di) + (Ni/N)*(optu(Di)-optl(Di))
            
            
            ENDDO
            
          ENDDO
          
          !Make one the original point
          Pt(1:DD,N/2) = R2
        
          isFinished = 0
          
          !WRITE(*,*)'x:',x,'R2',R2
          idum = -1
          DO WHILE (isFinished==0)
          
            
          
            CALL Optimising_Cuckoo_Driver(DD,N,ptemp,Ibest,ftemp,Pt,I,Loop,Istatus,Iin,Qu,fbest,optu,optl,idum,0.1d0,0.7d0)
            
            IF ((Loop-1).LT.numLoops) THEN
            
              !Calculate fitness at ptemp and put it into ftemp
              DO iii = 1,3
                Posit(iii,ip) = v(iii) + ((ptemp(1)/R2)*(x(iii)-v(iii)))
              END DO
              !WRITE(*,*)'Posit:',Posit(:,ip), 'ptemp', ptemp
              !ftemp = ptemp(4)*ptemp(4)
              ftemp = 0
              flag = 0
              
              DO ic = 1,List_Length
              
                ie = ITs_List(ic)
                p1 = Posit(:,IP_Tet(1,ie))
                p2 = Posit(:,IP_Tet(2,ie))
                p3 = Posit(:,IP_Tet(3,ie))
                p4 = Posit(:,IP_Tet(4,ie))
                vol = Geo3D_Tet_Volume(p1,p2,p3,p4)
                IF(vol.LE.0.d0)THEN
                  flag = 1
                  ftemp = huge(0.d0)
                  EXIT
                ELSE
                
                  
                  pC(:) = ( p1 + p2 + p3 + p4 ) / 4.d0
                  p1(:) = Posit(:,ip)
                  length = Geo3D_Distance(pC,p1)
                  delta2 = ( Scale_Point(IP_Tet(1,ie)) + Scale_Point(IP_Tet(2,ie))    &
                                         + Scale_Point(IP_Tet(3,ie)) + Scale_Point(IP_Tet(4,ie)) ) / 4.d0
                  delta2 = delta2*BGSpacing%BasicSize*sqrt5o4
                  
                  ftemp = ftemp + (delta2-length)*(delta2-length)
                
                ENDIF

              ENDDO
              
              
              IF (flag.EQ.1) THEN
                ftemp = huge(0.d0)
              ENDIF
              IF (ftemp==0) THEN
                isFinished = 1
                EXIT
              ENDIF
              
            ELSE
            
              !The best result is in Pt(:,Ibest)
              Posit(1:3,ip) = v(1:3) + (Pt(1,Ibest)/R2)*(x(1:3)-v(1:3))
              
              
              sumObj = sumObj + fbest
              isFinished = 1
              EXIT
            ENDIF
        
          ENDDO
            
            
            
            
            
            
            
            
            
            
            ENDIF
          ENDIF
      
        

        
        
          
          
          
    
    ENDDO
    deallocate(ptemp,Pt,Qu,optx, optl, optu, optg,vardef)


END SUBROUTINE MCS_Line_Local_Gen



SUBROUTINE Count_Bad_Layer(NB1,numElem,numBad)

  USE common_Constants
  USE common_Parameters
  USE array_allocator
  USE SpacingStorage
  USE Geometry3DAll
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: NB1
  INTEGER, INTENT(OUT) :: numElem,numBad
  INTEGER :: IT,i,flag,K
  REAL*8 :: ptet(3,4),p1t(3),dum(4)
  numElem = 0
  numBad = 0
  
  !Now loop over elements
  DO IT=1,NB_Tet
    flag = 1
    DO i=1,4
      IF (IP_Tet(i,IT).LT.NB1) flag = 0
    ENDDO
  
    IF (flag.EQ.1) THEN
    numElem = numElem+1
      CALL isBadSources(IT,K)
    numBad = numBad + K
    
    ENDIF
  END DO
END SUBROUTINE Count_Bad_Layer


SUBROUTINE isBadSources(IT,bad)
   USE common_Constants
   USE common_Parameters
   USE array_allocator
   USE SpacingStorage
   USE Geometry3DAll
   IMPLICIT NONE
   
   INTEGER, INTENT(IN)  :: IT
   INTEGER, INTENT(OUT) :: bad
   INTEGER :: i, j1, j2, j3,ITnb
   REAL*8  :: p1(3),p2(3),p3(3),p4(3), al,vomin, delta
   
   bad = 0
   p4(:) = Sphere_Tet(1:3,IT)
   
   DO i = 1, 4
  j1    = IP_Tet(iTri_Tet(1,i),IT)
  j2    = IP_Tet(iTri_Tet(2,i),IT)
  j3    = IP_Tet(iTri_Tet(3,i),IT)
  p1(:) = Posit(:,j1)
  p2(:) = Posit(:,j2)
  p3(:) = Posit(:,j3)
  al    = Geo3D_Tet_Volume(p1, p2, p3, p4)
  
  IF(al<0)THEN
     bad = 1
  ENDIF
  
   END DO
   
  
END SUBROUTINE isBadSources


SUBROUTINE MCS_Optim_Gen_Point(myStart,ip)
   
    USE common_Constants
    USE common_Parameters
    USE array_allocator
    USE SpacingStorage
    USE OptimisingModule
    USE Geometry3DAll
  USE RandomModule
  
  IMPLICIT NONE
    
    INTEGER :: L,LMax,K,ie,DD,N,Ibest,I,Loop,Istatus,Iin,Ni,Di,idum,isFinished
    INTEGER :: numLoops,ic,flag,nbp,improve,NB_old,j,ii,flag2
    REAL*8, allocatable  :: optx(:), optl(:), optu(:), optg(:) ,ptemp(:),Pt(:,:)
    REAL*8, allocatable :: Qu(:),vardef(:,:)!,allPsave(:)
    REAL*8 :: dd2,ftemp,fbest,ranNum,d,FTarget,FBig,p1(3),dtet,psave(3),rsave
    REAL*8 :: vol,ptet(3,4),p2(3),p3(3),p4(3),templ,dimi,targDi,diFact,penalty,length
    REAL*8 :: pp4(3,4),dum(4),coef,lastObj,p1t(3),p2t(3),residual,delta,dist,sqrt5o4
    REAL*8 :: pC(3),delta2,pCent(3)
    INTEGER :: ITnb,merged

   INTEGER, INTENT(IN)  :: myStart,ip
   REAL*8 :: sumObj
   REAL*8 :: minl, maxll, aspectRatio

    !Make sure mapping and geometry is all up to date
    CALL Get_Tet_Circum(0)
    
    FTarget = (1.D-6)*BGSpacing%MinSize
    FBig = 1.D30
    idum = -1
    sumObj = 0.d0
    sqrt5o4 = sqrt(5.d0)/4.d0
    !Set up the optimisation problem
    CALL init_random_seed()
    numLoops = 15 !Number of MCS loops
    DD = 3  !Number of dimensions - one for each space, going to scale by spacing
    N = 30  !Number of eggs
    
  targDi = 12.0d0
  diFact = 1.d0/(targDi)
  
    allocate(ptemp(DD),Pt(DD,N),Qu(N),optx(DD), optl(DD), optu(DD), optg(DD),vardef(DD,2))
   
  !Get delta
  d = Scale_Point(ip)*BGSpacing%BasicSize
  
  ptemp(1:DD) = 0.0d0
  Ibest = 0
  ftemp = 0.0d0
  I = 0
  Loop = 0
  Istatus = 0
  Iin = 0
  Qu(1:N) = huge(0.0)
  fbest = huge(0.0)


  !Upper and lower bounds are going to be 1*d
  optl(1:DD) = 0.1d0
  optu(1:DD) = -0.1d0
  

  !The last egg will be the initial position
  DO Ni = 1,N
    DO Di = 1,DD
    
      Pt(Di,Ni) = optl(Di) + Random_Gauss(idum)*(optu(Di)-optl(Di))
    
    
    ENDDO
    
  ENDDO
  
  Pt(1:DD,N/2) = 0.0d0

  isFinished = 0
  
  psave(:) = Posit(:,ip)
  !rsave = RR_Point(ip)

  CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
  idum = -1
  
  DO WHILE (isFinished==0)
  
    
  
    CALL Optimising_Cuckoo_Driver(DD,N,ptemp,Ibest,ftemp,Pt,I,Loop,Istatus,Iin,Qu,fbest,optu,optl,idum,0.1d0,0.7d0)
    
    IF ((Loop-1).LT.numLoops) THEN
    
      !Calculate fitness at ptemp and put it into ftemp
      Posit(:,ip) = psave(:) + d*ptemp(1:3)
      !ftemp = ptemp(4)*ptemp(4)
      ftemp = 0.0d0
      flag = 0
      
      DO ic = 1,List_Length
      
        ie = ITs_List(ic)
        
        IF (  (IP_Tet(1,ie).GT.(myStart-1)) .AND. &
            (IP_Tet(2,ie).GT.(myStart-1)) .AND. &
            (IP_Tet(3,ie).GT.(myStart-1)) .AND. &
            (IP_Tet(4,ie).GT.(myStart-1)) ) THEN
        
            p1 = Posit(:,IP_Tet(1,ie))
            p2 = Posit(:,IP_Tet(2,ie))
            p3 = Posit(:,IP_Tet(3,ie))
            p4 = Posit(:,IP_Tet(4,ie))
            
            !delta2  = sqrt5o4*(( Geo3D_Distance(p1,p2) + &
            !      Geo3D_Distance(p1,p3) + &
            !      Geo3D_Distance(p1,p4) + &
            !      Geo3D_Distance(p2,p3) + &
            !      Geo3D_Distance(p2,p4) + &
            !      Geo3D_Distance(p3,p4))/6.0d0)
            
            delta2 = sqrt5o4*( Scale_Point(IP_Tet(1,ie)) + Scale_Point(IP_Tet(2,ie))    &
                                        + Scale_Point(IP_Tet(3,ie)) + Scale_Point(IP_Tet(4,ie)) ) / 4.d0

            CALL Get_Tet_Circum(ie)
            pC = Sphere_Tet(1:3,ie)
            pCent(:) = ( p1 + p2 + p3 + p4 ) / 4.d0
            length = Geo3D_Distance(pC,p1)
            ftemp = ftemp + (length/delta2)
            
            
            !DO ii = 1,4
            !  p1(:) = Posit(:,IP_Tet(ii,ie))
            !  length = Geo3D_Distance(pC,p1)                        
            !  ftemp = ftemp + ((delta2-length)*(delta2-length))/(delta2*delta2)
            !ENDDO
            
          
          
        END IF
        
      ENDDO
      
      
      
      
    ELSE
    
      !The best result is in Pt(:,Ibest)
      Posit(:,ip) = psave(:) + d*Pt(1:3,Ibest)
      
      sumObj = sumObj + fbest
      isFinished = 1
      EXIT
    ENDIF

  ENDDO
  
  
        
   
  deallocate(ptemp,Pt,Qu,optx, optl, optu, optg,vardef)
        
   
END SUBROUTINE MCS_Optim_Gen_Point


SUBROUTINE MCS_Optim_Gen_Layer(NB1,NB2,sumObj)
   USE common_Constants
    USE common_Parameters
    USE array_allocator
    USE SpacingStorage
    USE OptimisingModule
    USE Geometry3DAll
    USE RandomModule
  IMPLICIT NONE
  
  
    INTEGER :: L,LMax,K,ie,ip,DD,N,Ibest,I,Loop,Istatus,Iin,Ni,Di,idum
    INTEGER :: isFinished,numLoops,ic,flag,nbp,improve,NB_old,j,ii,flag2
    REAL*8, allocatable  :: optx(:), optl(:), optu(:), optg(:) ,ptemp(:)
    REAL*8, allocatable :: Pt(:,:),Qu(:),vardef(:,:)!,allPsave(:)
    REAL*8 :: dd2,ftemp,fbest,ranNum,d,FTarget,FBig,p1(3),dtet,psave(3),rsave,vol
    real*8 :: ptet(3,4),p2(3),p3(3),p4(3),templ,dimi,targDi,diFact,length,penalty
    REAL*8 :: pp4(3,4),dum(4),coef,lastObj,p1t(3),p2t(3),residual,delta,dist
    REAL*8 :: sqrt5o4,pC(3),delta2
    INTEGER :: ITnb,merged

   INTEGER, INTENT(IN)  :: NB1,NB2
   REAL*8, INTENT(OUT) :: sumObj
   REAL*8 :: minl, maxll, aspectRatio
   CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
    !Make sure mapping and geometry is all up to date
    CALL Get_Tet_Circum(0)
    
    FTarget = (1.D-6)*BGSpacing%MinSize
    FBig = 1.D30
    idum = -1
    sumObj = 0.d0
    sqrt5o4 = sqrt(5.d0)/4.d0
    !Set up the optimisation problem
    CALL init_random_seed()
    numLoops = 15 !Number of MCS loops
    DD = 3  !Number of dimensions - one for each space, going to scale by spacing
    N = 30  !Number of eggs
    
  targDi = 12.0d0
  diFact = 1.d0/(targDi)
  
    allocate(ptemp(DD),Pt(DD,N),Qu(N),optx(DD), optl(DD), optu(DD), optg(DD),vardef(DD,2))
   
   
    !First node positions
        DO ip = NB2, NB_Point
        

        
        
            !Get delta
            d = Scale_Point(ip)*BGSpacing%BasicSize
            
            
            !Get connected elements
            CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
        
            
            
            ptemp(1:DD) = 0.0d0
            Ibest = 0
            ftemp = 0.0d0
            I = 0
            Loop = 0
            Istatus = 0
            Iin = 0
            Qu(1:N) = huge(0.0)
            fbest = huge(0.0)

        
            !Upper and lower bounds are going to be 1*d
            optl(1:DD) = 0.25d0
            optu(1:DD) = -0.25d0
            
        
            !The last egg will be the initial position
            DO Ni = 1,N
                DO Di = 1,DD
                
                    Pt(Di,Ni) = optl(Di) + Random_Gauss(idum)*(optu(Di)-optl(Di))
                
                
                ENDDO
                
            ENDDO
            
            Pt(1:DD,N/2) = 0.0d0
        
            isFinished = 0
            
            psave(:) = Posit(:,ip)
            !rsave = RR_Point(ip)
      idum = -1
            DO WHILE (isFinished==0)
            
                
            
                CALL Optimising_Cuckoo_Driver(DD,N,ptemp,Ibest,ftemp,Pt,I,Loop,Istatus,Iin,Qu,fbest,optu,optl,idum,0.1d0,0.7d0)
                
                IF ((Loop-1).LT.numLoops) THEN
                
                    !Calculate fitness at ptemp and put it into ftemp
                    Posit(:,ip) = psave(:) + d*ptemp(1:3)
                    !ftemp = ptemp(4)*ptemp(4)
                    ftemp = 0
                    flag = 0
                    
                    DO ic = 1,List_Length
                    
                        ie = ITs_List(ic)
            
            IF (  (IP_Tet(1,ie).GT.(NB1-1)) .AND. &
                (IP_Tet(2,ie).GT.(NB1-1)) .AND. &
                (IP_Tet(3,ie).GT.(NB1-1)) .AND. &
                (IP_Tet(4,ie).GT.(NB1-1)) ) THEN
            
            
            
              p1 = Posit(:,IP_Tet(1,ie))
              p2 = Posit(:,IP_Tet(2,ie))
              p3 = Posit(:,IP_Tet(3,ie))
              p4 = Posit(:,IP_Tet(4,ie))
              
              !delta2  = (( Geo3D_Distance(p1,p2) + &
              !      Geo3D_Distance(p1,p3) + &
              !      Geo3D_Distance(p1,p4) + &
              !      Geo3D_Distance(p2,p3) + &
              !      Geo3D_Distance(p2,p4) + &
              !      Geo3D_Distance(p3,p4))/6.0d0)
              
              !delta2 = delta2*sqrt5o4
              
              delta2 = sqrt5o4*BGSpacing%BasicSize*( Scale_Point(IP_Tet(1,ie)) + Scale_Point(IP_Tet(2,ie))    &
                                        + Scale_Point(IP_Tet(3,ie)) + Scale_Point(IP_Tet(4,ie)) ) / 4.d0
              
              !CALL Get_Tet_Circum(ie) 
              !pC = Sphere_Tet(1:3,ie)
              pC(:) = ( p1 + p2 + p3 + p4 ) / 4.d0
              !p1(1:3) = Sphere_Tet(1:3,ie)
              !length = Geo3D_Distance(pC,p1)
                
              !templ = (length/delta2)
              
              DO ii = 1,4
                p1(:) = Posit(:,IP_Tet(ii,ie))
                length = Geo3D_Distance(pC,p1)                        
                ftemp = ftemp + ((delta2-length)*(delta2-length))/(delta2*delta2)
              ENDDO
              
              
            ENDIF
              
                        
            
                    ENDDO
                    
                    
                    
                    
                ELSE
                
                    !The best result is in Pt(:,Ibest)
                    Posit(:,ip) = psave(:) + d*Pt(1:3,Ibest)
                    
                    sumObj = sumObj + fbest
                    isFinished = 1
                    EXIT
                ENDIF
        
            ENDDO
            
            
        
        ENDDO
        deallocate(ptemp,Pt,Qu,optx, optl, optu, optg,vardef)
        
   
END SUBROUTINE MCS_Optim_Gen_Layer




SUBROUTINE MCS_Optim_Weight_Layer(NB1,NB2,sumObj)
   USE common_Constants
    USE common_Parameters
    USE array_allocator
    USE SpacingStorage
    USE OptimisingModule
    USE Geometry3DAll
    USE RandomModule
  
  IMPLICIT NONE
  
  
    INTEGER :: L,LMax,K,ie,ip,DD,N,Ibest,I,Loop,Istatus,Iin,Ni,Di,idum,isFinished,numLoops,ic,flag,nbp,improve,NB_old,j,ii,flag2
    REAL*8, allocatable  :: optx(:), optl(:), optu(:), optg(:) ,ptemp(:),Pt(:,:),Qu(:),vardef(:,:)!,allPsave(:)
    REAL*8 :: dd2,ftemp,fbest,ranNum,d,FTarget,FBig,p1(3),dtet,psave,rsave,vol,ptet(3,4),p2(3),p3(3),p4(3)
    REAL*8 :: pp4(3,4),dum(4),coef,lastObj,p1t(3),p2t(3),residual,delta,dist
    INTEGER :: ITnb,merged

   INTEGER, INTENT(IN)  :: NB1,NB2
   REAL*8, INTENT(OUT) :: sumObj

   CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
    !Make sure mapping and geometry is all up to date
    CALL Get_Tet_Circum(0)
    
    FTarget = (1.D-6)*BGSpacing%MinSize
    FBig = 1.D30
    idum = -1
    sumObj = 0.d0
    
    !Set up the optimisation problem
    CALL init_random_seed()
    numLoops = 2 !Number of MCS loops
    DD = 1  !Number of dimensions - one for each space, going to scale by spacing
    N = 10  !Number of eggs
        
    allocate(ptemp(DD),Pt(DD,N),Qu(N),optx(DD), optl(DD), optu(DD), optg(DD),vardef(DD,2))
   
   
    !First node positions
        DO ip = NB2+1, NB_Point
        

        
        
            !Get delta
            d = Scale_Point(ip)*BGSpacing%BasicSize
            
            
            !Get connected elements
            CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
        
            
            
            ptemp(1:DD) = 0.0d0
            Ibest = 0
            ftemp = 0.0d0
            I = 0
            Loop = 0
            Istatus = 0
            Iin = 0
            Qu(1:N) = huge(0.0)
            fbest = huge(0.0)

        
            !Upper and lower bounds are going to be 1*d
            optl(1:DD) = 1.0d0
            optu(1:DD) = -1.0d0
            
        
            !The last egg will be the initial position
            DO Ni = 1,N
                DO Di = 1,DD
                
                    Pt(Di,Ni) = optl(Di) + (Ni/N)*(optu(Di)-optl(Di))
                
                
                ENDDO
                
            ENDDO
            
            Pt(1:DD,N/2) = 0.0d0
        
            isFinished = 0
            
            
            rsave = RR_Point(ip)
      idum = -1
            DO WHILE (isFinished==0)
            
                
            
                CALL Optimising_Cuckoo_Driver(DD,N,ptemp,Ibest,ftemp,Pt,I,Loop,Istatus,Iin,Qu,fbest,optu,optl,idum,0.1d0,0.7d0)
                
                IF ((Loop-1).LT.numLoops) THEN
                
                    !Calculate fitness at ptemp and put it into ftemp
                    RR_Point(ip) = rsave + d*ptemp(1)
                    !ftemp = ptemp(4)*ptemp(4)
                    ftemp = 0
                    flag = 0
                    
                    DO ic = 1,List_Length
                    
                        ie = ITs_List(ic)
                        
                        flag2 = 0
                        DO ii=1,4
                            IF (IP_Tet(ii,ie).LT.NB1) flag2 = 1
                        END DO
                        
                        IF ((flag2.EQ.0)) THEN
                        
                            p1 = Posit(:,IP_Tet(1,ie))
                            p2 = Posit(:,IP_Tet(2,ie))
                            p3 = Posit(:,IP_Tet(3,ie))
                            p4 = Posit(:,IP_Tet(4,ie))
                            vol = Geo3D_Tet_Volume(p1,p2,p3,p4)
                            IF(vol.LE.0.d0)THEN
                                flag = 1
                                ftemp = huge(0.d0)
                                EXIT
                            ELSE
                                CALL Get_Tet_Circum(ie) 
                                !Check if it's inside
                                
                            ptet(:,1) = p1
                            ptet(:,2) = p2
                            ptet(:,3) = p3
                            ptet(:,4) = p4
                            p1t = Sphere_Tet(1:3,ie)
                            K=0
                            dum =  Geo3D_Tet_Weight(K,ptet,p1t)
                                
                                IF (K==1) THEN
                                !We are inside
                                     
                                ELSE
                                    !There is a possibility to merge
                                    merged = 0
                                    delta = ( Scale_Point(IP_Tet(1,ie)) + Scale_Point(IP_Tet(2,ie))    &
                                + Scale_Point(IP_Tet(3,ie)) + Scale_Point(IP_Tet(4,ie)) ) / 4.d0
          
                                    delta = delta*BGSpacing%BasicSize
                                    
                                    DO ii = 1,4
                                        ITnb = Next_Tet(ii,ie)
                                        IF(ITnb<=0) CYCLE
                                        CALL Get_Tet_Circum(ITnb)
                                        p2t = Sphere_Tet(:,ITnb)
                                        dd2 = Geo3D_Distance_SQ(p2t, p1t)
                                        IF(dd2<((0.1*delta)*(0.1*delta)))THEN
                                           merged = 1
                                        ENDIF
                                    ENDDO
                                    IF (merged.EQ.0) THEN
                                        coef = 1.d0
                                        p1t = Sphere_Tet(1:3,ie)
                                        p2t = (p1+p2+p3+p4) / 4.d0
                                        dd2 = Geo3D_Distance_SQ(p2t, p1t)
                                        
                                            
                                        ftemp = ftemp + coef*dd2
                                    ENDIF
                                ENDIF
                                     
                                     
                                ENDIF
                            ENDIF
                        
                    
                    ENDDO
                    
                    
                    IF (flag.EQ.1) THEN
                        ftemp = huge(0.d0)
                    ENDIF
                    IF (ftemp==0) THEN
                        isFinished = 1
                        EXIT
                    ENDIF
                    
                ELSE
                
                    !The best result is in Pt(:,Ibest)
                    RR_Point(ip) = rsave + d*Pt(1,Ibest)
                    
                    sumObj = sumObj + fbest
                    isFinished = 1
                    EXIT
                ENDIF
        
            ENDDO
            
            
        
        ENDDO
        deallocate(ptemp,Pt,Qu,optx, optl, optu, optg,vardef)
        
   
END SUBROUTINE MCS_Optim_Weight_Layer




SUBROUTINE pointLin_space(P,PS,S)
        
        USE Geometry3DAll
        USE Source_Type
        IMPLICIT NONE
        
        TYPE(PointSource_Type), INTENT(IN) :: PS
        REAL*8, INTENT(IN) :: P(3)
        REAL*8, INTENT(OUT) :: S
        REAL*8 :: dd
        
        dd = Geo3D_Distance(P,PS%Posit) ! Distance from the point source
        
        IF (dd<=PS%RA1) THEN
            S = PS%Space
        ELSE
            S = PS%Space*(((dd-PS%RA1)/(PS%RA2-PS%RA1))+1.d0) !Linear
      
      
        ENDIF
END SUBROUTINE pointLin_space  

