!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  MCS_Optim
!!  
!!  This routine uses MCS to optimise positions and weights of nodes in the mesh
!!
!!  
!!
!!  Initial implimentation by S. Walton 29/10/1013
!!
!<
!*******************************************************************************
SUBROUTINE MCS_Optim(NB1,NB2,hull)
	
	USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	
	
	INTEGER, INTENT(IN)  :: NB1,NB2,hull  !The first point effected by this process
	INTEGER :: K,ie,ip,DD,N,Ibest,I,Loop,Istatus,Iin,Ni,Di,idum,isFinished,numLoops,ic,flag,nbp,improve,NB_old,LMax,L,j,ii
	REAL*8, allocatable  :: optx(:), optl(:), optu(:), optg(:) ,ptemp(:),Pt(:,:),Qu(:),vardef(:,:)!,allPsave(:,:)
	REAL*8 :: dd2,ftemp,fbest,ranNum,d,FTarget,FBig,p1(3),dtet,psave(3),rsave,vol,ptet(3,4),p2(3),p3(3),p4(3)
	REAL*8 :: pp4(3,4),dum(4),coef,sumObj,lastObj,p1t(3),p2t(3),residual,delta,dist,pp42(3,4)
	INTEGER :: ITnb,merged,numBadLoc,Mark_Point2(NB_Point),iRan,jRan,tRan,valancy,numEffElem,iFace
	REAL*8 :: dimi,diFact,targDi,mergeFact,dt,conv,dum2,dw
	type(IntQueueType) :: surroundNodes,effElem
	
	REAL*8, ALLOCATABLE :: snapStore(:,:),qualStore(:)
	INTEGER, ALLOCATABLE :: maskStore(:,:),order(:)
	INTEGER :: DENp,DEd,DEnumLoops,DEloop,DEi,DEj,DEl,DEk,bestL,badMask(NB_Point),DENoimp,DEimpFlag
	REAL*8 :: F,CR,DEQual,bestLSum,F2
	INTEGER :: globFlag, globL,improve2,numLocLoops,Lconv
	REAL*8 :: globObj, globObjLast,globRes,randn,penalty,oldQual
	REAL*8 :: Layers(NB_Point), globObj2, globObjLast2,globRes2
	REAL*8, ALLOCATABLE :: Fstore(:),CRstore(:)
	INTEGER, ALLOCATABLE :: lastPointStore(:)
	REAL*8, ALLOCATABLE :: searchParam(:,:)
	TYPE(Plane3D) :: aPlane

	INTEGER :: runL,iep,Kswap
	LOGICAL :: negExist

	REAL*8 :: timePerFun,numFun,time1,time2,numTimePerFun,totTimeFun

	REAL*8 :: localConv,invPi
	INTEGER :: isConved

	INTEGER :: nextPoint,worstElem,nextFlag
	REAL*8 :: worstQual

	idum = -1

	invPi = 1.0d0/PI

	OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
    READ(84,*) dum2
    READ(84,*) conv
	READ(84,*) dum2
	READ(84,*) dum2
	READ(84,*) dum2
	READ(84,*) dum2
	READ(84,*) dum2
	READ(84,*) dum2
	READ(84,*) numLocLoops
	READ(84,*) LMax
	READ(84,*) Lconv
    CLOSE(84)
	
	conv = 10.0d0/REAL(NB_Tet)! 0.001d0!1.0d0/REAL(NB_Tet)
	
	WRITE(*,*)'conv',conv
	
	lastObj = HUGE(0.d0)
	
	!LMax = 25
	
	!Initial parameters for DE
	CALL RANDOM_NUMBER(CR)
	CALL RANDOM_NUMBER(randn)
	F = 0.1d0 + (randn*0.9d0)
	
	ALLOCATE(snapStore(4*(LMax),NB_Point),qualStore(LMax),maskStore(LMax,NB_Point))
	ALLOCATE(Fstore(LMax+1),CRstore(LMax+1),lastPointStore(LMax+1))
	ALLOCATE(searchParam(2,LMax+1))
	qualStore(:) = HUGE(0.0d0)
	
	DD = 4  !Number of dimensions - one for each space, going to scale by spacing
	N = 40  !Number of eggs		
	allocate(ptemp(DD),Pt(DD,N),Qu(N),optx(DD), optl(DD), optu(DD), optg(DD),vardef(DD,2))

	!Initialise the search parameters
	DO L = 1,LMax+1
		!CALL RANDOM_NUMBER(randn)
		searchParam(1,L) = 0.01d0
		!CALL RANDOM_NUMBER(randn)
		searchParam(2,L) = 0.7d0
		CALL RANDOM_NUMBER(randn)
		nextPoint = NB_Point * randn + 1
		lastPointStore(L) = nextPoint
	END DO


	globFlag = 0
	
	globL = 0

	CALL init_random_seed()
	
	
	DO WHILE(globFlag.EQ.0)
		
		numTimePerFun = 0.0d0
		totTimeFun = 0.0d0

		ALLOCATE(order(NB_Point))
	
		targDi = 6.0d0
		!An order array to help shuffle
		DO iRan = 1,NB_Point
			order(iRan) = iRan
		ENDDO
		
		improve = 1
		
		
		L = 1
		
		CALL Label_Layers(Layers)

		IF (globL .EQ. 0) THEN
		    
			snapStore(4*(L-1)+1,1:NB_Point) = Posit(1,1:NB_Point)
			snapStore(4*(L-1)+2,1:NB_Point) = Posit(2,1:NB_Point)
			snapStore(4*(L-1)+3,1:NB_Point) = Posit(3,1:NB_Point)
			snapStore(4*(L-1)+4,1:NB_Point) = RR_Point(1:NB_Point)
			CALL Get_Global_Quality(NB1,NB2,bestLSum,badMask,Layers)
			qualStore(L) = bestLSum	
		    globObjLast = bestLSum
			globObjLast2 = bestLSum
			bestL = L
			lastObj = bestLSum
			maskStore(L,1:NB_Point) = badMask(1:NB_Point)
		ELSE
		  snapStore(4*(L-1)+1,1:NB_Point) = snapStore(4*(bestL-1)+1,1:NB_Point)
		  snapStore(4*(L-1)+2,1:NB_Point) = snapStore(4*(bestL-1)+2,1:NB_Point)
		  snapStore(4*(L-1)+3,1:NB_Point) = snapStore(4*(bestL-1)+3,1:NB_Point)
		  snapStore(4*(L-1)+4,1:NB_Point) = snapStore(4*(bestL-1)+4,1:NB_Point)
		  CALL Get_Global_Quality(NB1,NB2,bestLSum,badMask,Layers)
		  qualStore(L) = bestLSum		  
		  bestL = L
		  maskStore(L,1:NB_Point) = badMask(1:NB_Point)
		  
		END IF
	
		
		
		
		
		IF (1.EQ.1) THEN
			IF(Debug_Display>0)THEN				
				WRITE(*,*)'Quality before tunnel:',bestLSum
				WRITE(29,*)'Quality before tunnel:',bestLSum
			END IF
		
		
			!At this stage do a tunnel
			L = 2
			CircumUpdated = .TRUE.
		    VolumeUpdated = .TRUE.
			
			DO iep = 1,NB_Tet
				CALL Get_Tet_Circum(iep)
				CALL Get_Actual_Tet_Mapping(iep)
			END DO
			
			CircumUpdated = .TRUE.
			 VolumeUpdated = .TRUE.
			improve2 = 1
			oldQual = bestLSum
			DO WHILE (improve2.EQ.1) 
				CALL Tunnel_Node(NB1,NB2,badMask,0)
				CALL Label_Layers(Layers)
				IF(Debug_Display>0)THEN			
					WRITE(*,*)'Tunnelled Node:',oldQual
					WRITE(29,*)'Tunnelled Node:',oldQual
				END IF
				CircumUpdated = .TRUE.
			    VolumeUpdated = .TRUE.
				
				DO iep = 1,NB_Tet
					CALL Get_Tet_SphereCross(iep)
					CALL Get_Tet_Volume(iep)
					CALL Get_Actual_Tet_Mapping(iep)
				END DO
				
				CALL Get_Global_Quality(NB1,NB2,sumObj,badMask,Layers)
		
				IF(Debug_Display>0)THEN			
					WRITE(*,*)'Tunnelled Node:',sumObj
					WRITE(29,*)'Tunnelled Node:',sumObj
				END IF
				
				IF (sumObj.GE.oldQual) THEN
				  improve2 = 0
				ELSE
				  oldQual = sumObj
				END IF
				IF (globL.EQ.0) THEN
				   qualStore(L) = HUGE(0.0d0)
			    END IF
			
				IF ( (sumObj.LT.qualStore(L)) ) THEN

					snapStore(4*(L-1)+1,1:NB_Point) = Posit(1,1:NB_Point)
					snapStore(4*(L-1)+2,1:NB_Point) = Posit(2,1:NB_Point)
					snapStore(4*(L-1)+3,1:NB_Point) = Posit(3,1:NB_Point)
					snapStore(4*(L-1)+4,1:NB_Point) = RR_Point(1:NB_Point)
					!CALL Get_Global_Quality(sumObj,badMask)
					qualStore(L) = sumObj
					maskStore(L,1:NB_Point) = badMask(1:NB_Point)
					IF (sumObj<bestLSum) THEN
					  bestL = L
					  bestLSum = sumObj
					END IF
					
				
				
					IF(Debug_Display>0)THEN				
						WRITE(*,*)'Quality after tunnel:',sumObj
						WRITE(29,*)'Quality after tunnel:',sumObj
					END IF
				END IF
			
			END DO
		
		END IF
		
		!OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
		!READ(84,*) dt
		!READ(84,*) conv
		!READ(84,*) dum2
		!CLOSE(84)
		
		IF (hull.NE.1) THEN
		  CALL Swap3D(2)
		  CALL Next_Build()
		END IF
		
		DO WHILE((L<LMax))
			
			!Select a starting point
			CALL RANDOM_NUMBER(randn)
			IF (randn.GT.(REAL(L)/REAL(LMax))) THEN
			
				IF (globL.EQ.0) THEN
				CALL RANDOM_NUMBER(randn)
				jRan = L * randn + 1
				IF (jRan.GT.L) THEN
				  jRan = L
				ELSE IF (jRan.EQ.0) THEN
				  jRan = 1
				END IF
			  ELSE
			    CALL RANDOM_NUMBER(randn)
				jRan = LMax * randn + 1
				IF (jRan.GT.LMax) THEN
				  jRan = LMax
				ELSE IF (jRan.EQ.0) THEN
				  jRan = 1
				END IF
			  END IF
				
			ELSE
				jRan = bestL
			END IF
			
			!Get the best and show quality
			Posit(1,1:NB_Point) = snapStore(4*(jRan-1)+1,1:NB_Point)
			Posit(2,1:NB_Point) = snapStore(4*(jRan-1)+2,1:NB_Point)
			Posit(3,1:NB_Point) = snapStore(4*(jRan-1)+3,1:NB_Point)
			RR_Point(1:NB_Point) = snapStore(4*(jRan-1)+4,1:NB_Point) 
			runL = jRan

			!Reinsert
			nbp = NB1-1
			IF (hull.EQ.1) THEN
			  CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
			ELSE
			  CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
			  CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
			END IF
			CALL Next_Build()
			!CALL Set_Domain_Mapping()
			CircumUpdated = .TRUE.
		    VolumeUpdated = .TRUE.
			
			DO iep = 1,NB_Tet
				CALL Get_Tet_SphereCross(iep)
				CALL Get_Tet_Volume(iep)
			END DO
			
			 CircumUpdated = .TRUE.
			  VolumeUpdated = .TRUE.
			!Mark_Point(1:NB_Point) = 0
			CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
			usePower = .FALSE.
			IF (hull.NE.1) THEN
			  CALL Swap3D(2)
			  CALL Next_Build()
			END IF
			CircumUpdated = .TRUE.
		    VolumeUpdated = .TRUE.
			
			DO iep = 1,NB_Tet
				CALL Get_Tet_SphereCross(iep)
				CALL Get_Actual_Tet_Mapping(iep)
				CALL Get_Tet_Volume(iep)
			END DO
			
			 CircumUpdated = .TRUE.
			  VolumeUpdated = .TRUE.
			CALL Label_Layers(Layers)

			FTarget = (1.D-6)*BGSpacing%MinSize
			FBig = 1.D30
			
			sumObj = 0.d0
			
			
			CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
			
			!Set up the optimisation problem
			
			numLoops = numLocLoops !Number of MCS loops
			
			!allocate(allPsave(3,NB_Point))
			!allPsave(:,1:NB_Point) = Posit(:,1:NB_Point)
			NB_old = NB_Point
			
			!Shuffle the order
			!DO iRan = NB_Point,1,-1
			!	CALL RANDOM_NUMBER(randn)
			!   jRan = iRan * randn + 1
			!	tRan = order(iRan)
			!	order(iRan) = order(jRan)
			!	order(jRan) = tRan
			!ENDDO
			
			

			!First node positions
			nextPoint = lastPointStore(runL)
			Mark_Point(1:NB_Point) = 0
			WRITE(*,*),nextPoint
			
			DO WHILE(SUM(Mark_Point(1:NB_Point)).LT.NB_Point)
			
				ip = nextPoint
				
				Mark_Point(ip) = 1
				nextFlag = 0
				
				IF ((ip.GE.NB1).AND.(ip.LE.NB2).AND. (maskStore(runL,ip).EQ.1)) THEN
				
					!Get connected elements
					
					!Calculate the actual spacing here
					
					d = HUGE(0.0d0)
					DO ic = 1,PointAsso%JointLinks(ip)%numNodes
						ie = PointAsso%JointLinks(ip)%Nodes(ic)
						d = MIN(d,Scale_Tet(ie)*BGSpacing%BasicSize)
					END DO
					
					dw = d*d
				
					ptemp(1:DD) = 0.0d0
					Ibest = 0
					ftemp = 0.0d0
					I = 0
					Loop = 0
					Istatus = 0
					Iin = 0
					Qu(1:N) = huge(0.0)
					fbest = huge(0.0)

				
					!Upper and lower bounds are going to be 1*d
					optl(1:DD) = -1.0d0
					optu(1:DD) = 1.0d0
					
				
					!The last egg will be the initial position
					DO Ni = 1,N
						DO Di = 1,DD
							CALL RANDOM_NUMBER(randn)
							Pt(Di,Ni) = optl(Di) + randn*(optu(Di)-optl(Di))
						
						
						ENDDO
						
					ENDDO
					
					Pt(1:DD,N) = 0.0d0
				
					isFinished = 0
					
					psave(:) = Posit(:,ip)
					rsave = RR_Point(ip)
				
					DO WHILE (isFinished==0)
					
						
					
						CALL Optimising_Cuckoo_Driver(DD,N,ptemp,Ibest,ftemp,Pt,I,Loop, &
							                         Istatus,Iin,Qu,fbest,optu,optl,idum,&
							                         searchParam(1,runL),searchParam(2,runL))
						

						IF (((Loop-1).LT.numLoops)) THEN
						
							!numFun = numFun + 1.0d0

							!Calculate fitness at ptemp and put it into ftemp
							Posit(:,ip) = psave(:) + d*ptemp(1:3)
							RR_Point(ip) = rsave + dw*ptemp(4)
							!ftemp = ptemp(4)*ptemp(4)
							ftemp = 0.0d0
							flag = 0

							
							DO ic = 1,PointAsso%JointLinks(ip)%numNodes
								ie = PointAsso%JointLinks(ip)%Nodes(ic)
								!CALL Get_Tet_Circum(ie)
								CALL Get_Tet_SphereCross(ie)
								CALL Get_Actual_Tet_Mapping(ie)
							END DO
							
							DO ic = 1,PointAsso%JointLinks(ip)%numNodes
							
								ie = PointAsso%JointLinks(ip)%Nodes(ic)
								p1 = Posit(:,IP_Tet(1,ie))
								p2 = Posit(:,IP_Tet(2,ie))
								p3 = Posit(:,IP_Tet(3,ie))
								p4 = Posit(:,IP_Tet(4,ie))
								!vol = Geo3D_Tet_Volume(p1,p2,p3,p4)
								dimi = Geo3D_Tet_Quality(p1,p2,p3,p4,2) * 180.d0 * invPi
								
								
								IF(dimi.LT.10.0d0)THEN
									
									ftemp = ftemp + huge(0.d0)
									
								ELSE
									!CALL Get_Tet_SphereCross(ie) 
									!Check if it's inside
									CALL isInside(ie,K,dum2)
									
									
									
									IF (K==0) THEN
									
										!There is a possibility to merge
										
										CALL isMerged(ie,mergeFact)							
										IF(mergeFact.GE.(0.0d0))THEN
										
										
											p1t = Sphere_Tet(1:3,ie)
											

										    p2t = (p1+p2+p3+p4) * 0.25d0
										    coef = d

											dd2 = (Geo3D_Distance_SQ(p2t, p1t)&
											        /(coef*coef))

											coef =  (MAXVAL(Layers(IP_Tet(1:4,ie))))
																	
												
											ftemp = ftemp + coef*dd2
										
										
									
										
										 ENDIF
									ENDIF

									
									
									ENDIF
							
							ENDDO
							
							
							IF (ftemp==0) THEN
								isFinished = 1
								EXIT
							ENDIF
							
						ELSE
						
							!The best result is in Pt(:,Ibest)
							Posit(:,ip) = psave(:) + d*Pt(1:3,Ibest)
							RR_Point(ip) = rsave + dw*Pt(4,Ibest)
							!sumObj = sumObj + fbest
							isFinished = 1
							EXIT
						ENDIF
				
					ENDDO

					worstQual = 0.0d0
					worstElem = 0
					
					DO ic = 1,PointAsso%JointLinks(ip)%numNodes
						ie = PointAsso%JointLinks(ip)%Nodes(ic)
						!CALL Get_Tet_Circum(ie)
						CALL Get_Tet_SphereCross(ie)	
						CALL Get_Actual_Tet_Mapping(ie)	
						p1t = Sphere_Tet(1:3,ie)
						p2t = (p1+p2+p3+p4) * 0.25d0	
						coef = Scale_Tet(ie)*BGSpacing%BasicSize
						ftemp = (Geo3D_Distance_SQ(p2t, p1t)&
											        /(coef*coef))
						IF (ftemp.GT.worstQual) THEN
						  worstQual = ftemp
						  worstElem = ie
						END IF
					END DO

					!Figure out where to go next
					
					DO ic = 1,4
					  IF (Mark_Point(IP_Tet(ic,worstElem)).EQ.0) THEN
					    nextPoint = IP_Tet(ic,worstElem)
					    nextFlag = 1
					  ENDIF
					ENDDO			
					
				END IF

				IF ((nextFlag.EQ.0).AND.(SUM(Mark_Point(1:NB_Point)).LT.NB_Point)) THEN
				  !We need to find a point to go to next
				  nextPoint=minloc(Mark_Point, 1, mask=Mark_Point.eq.0)
				  
				END IF
				
			
			ENDDO
			
			!CALL CPU_TIME(time2)
			
			!timePerFun = (time2-time1)/numFun
			!totTimeFun = totTimeFun + timePerFun
			!numTimePerFun = numTimePerFun + 1.0d0

			!Reinsert
			nbp = NB1-1
			IF (hull.EQ.1) THEN
			  CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
			ELSE
			  CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
			  CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
			END IF
			CALL Next_Build()
			!CALL Set_Domain_Mapping()
			CircumUpdated = .TRUE.
		    VolumeUpdated = .TRUE.
			
			DO iep = 1,NB_Tet
				CALL Get_Tet_SphereCross(iep)
				CALL Get_Tet_Volume(iep)
				
			END DO
			
			 CircumUpdated = .TRUE.
			  VolumeUpdated = .TRUE.
			Mark_Point(1:NB_Point) = 0
			CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
			usePower = .FALSE.
			!CALL Remove_Point()
			negExist = .FALSE.
			DO iep = 1, NB_Tet
				p1(:)   = Posit(:,IP_Tet(1,iep))
		        p2(:)   = Posit(:,IP_Tet(2,iep))
		        p3(:)   = Posit(:,IP_Tet(3,iep))
		        p4(:)   = Posit(:,IP_Tet(4,iep))
			    IF(Geo3D_Tet_Quality(p1,p2,p3,p4,2)<=0.d0)THEN
			       negExist = .TRUE.
			       EXIT
			    ENDIF
			ENDDO

			

			IF (hull.NE.1) THEN
			  Kswap = 2
			  CALL Swap3D(Kswap)
			  CALL Next_Build()
			END IF
			IF (Kswap.EQ.2) THEN	
				CircumUpdated = .TRUE.
			    VolumeUpdated = .TRUE.
			
				DO iep = 1,NB_Tet
					CALL Get_Tet_SphereCross(iep)
					CALL Get_Tet_Volume(iep)
					CALL Get_Actual_Tet_Mapping(iep)
				END DO
				
				 CircumUpdated = .TRUE.
				 VolumeUpdated = .TRUE.
				CALL Label_Layers(Layers)


				
				
				!IF (residual>conv) THEN
					
					!deallocate(allPsave)
					L = L+1
					CALL Get_Global_Quality(NB1,NB2,sumObj,badMask,Layers)


					IF(Debug_Display>0)THEN
						
						WRITE(*,*)'Node Obj fun val, A, pa',sumObj,searchParam(1,runL),searchParam(2,runL)
						WRITE(29,*)'Node Obj fun val, A, pa',sumObj,searchParam(1,runL),searchParam(2,runL)
					END IF
					
					!This is the adaptive MCS step
					IF (sumObj.LT.qualStore(runL)) THEN
						CALL RANDOM_NUMBER(randn)
						searchParam(1,L) = searchParam(1,L)+randn*(searchParam(1,runL)-searchParam(1,L))
						CALL RANDOM_NUMBER(randn)
						searchParam(2,L) = searchParam(2,L)+randn*(searchParam(2,runL)-searchParam(2,L))
					ELSE
						CALL RANDOM_NUMBER(randn)
						searchParam(1,runL) = randn
						CALL RANDOM_NUMBER(randn)
						searchParam(2,runL) = randn
					END IF


					IF ((sumObj.LT.qualStore(L))) THEN
					
						

						snapStore(4*(L-1)+1,1:NB_Point) = Posit(1,1:NB_Point)
						snapStore(4*(L-1)+2,1:NB_Point) = Posit(2,1:NB_Point)
						snapStore(4*(L-1)+3,1:NB_Point) = Posit(3,1:NB_Point)
						snapStore(4*(L-1)+4,1:NB_Point) = RR_Point(1:NB_Point)
						qualStore(L) = sumObj
						maskStore(L,1:NB_Point) = badMask(1:NB_Point)

						IF (sumObj<bestLSum) THEN
						  bestL = L
						  bestLSum = sumObj
						END IF
					END IF
					
					residual = ABS(1.d0 - (lastObj/sumObj))
					
					
					
					lastObj = sumObj
			ENDIF	
			
		ENDDO

		
		
		!Get the best and show quality
		Posit(1,1:NB_Point) = snapStore(4*(bestL-1)+1,1:NB_Point)
		Posit(2,1:NB_Point) = snapStore(4*(bestL-1)+2,1:NB_Point)
		Posit(3,1:NB_Point) = snapStore(4*(bestL-1)+3,1:NB_Point)
		RR_Point(1:NB_Point) = snapStore(4*(bestL-1)+4,1:NB_Point)
		
		!Reinsert
		nbp = NB1-1
		IF (hull.EQ.1) THEN
			  CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
			ELSE
			  CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
			  CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
			END IF
		CALL Next_Build()
		!CALL Set_Domain_Mapping()
		CircumUpdated = .TRUE.
		 VolumeUpdated = .TRUE.
		
		DO iep = 1,NB_Tet
			CALL Get_Tet_SphereCross(iep)
			CALL Get_Tet_Volume(iep)
		END DO
		
		 CircumUpdated = .TRUE.
		 VolumeUpdated = .TRUE.
		!Mark_Point(1:NB_Point) = 0
		CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
		usePower = .FALSE.
		IF (hull.NE.1) THEN
		  CALL Swap3D(2)
		  CALL Next_Build()
		END IF
		CircumUpdated = .TRUE.
		VolumeUpdated = .TRUE.
		
		DO iep = 1,NB_Tet
			CALL Get_Tet_SphereCross(iep)
			CALL Get_Tet_Volume(iep)
			CALL Get_Actual_Tet_Mapping(iep)
		END DO
		
		 CircumUpdated = .TRUE.
		 VolumeUpdated = .TRUE.
		CALL Label_Layers(Layers)
		
		
		
		CALL Check_Mesh_Geometry2('afterGlobalnode')
	
		globObj =  SUM(qualStore)/REAL(LMax)
		globObj2 = qualStore(bestL)
		globL = globL + 1
		
		DEALLOCATE(order)

		!globFlag = 1
	
		
		globRes = ABS(1.d0 - (globObjLast/globObj))
		globRes2 = ABS(1.d0 - (globObjLast2/globObj2))
		IF(Debug_Display>0)THEN
				
			WRITE(*,*)'DE Global res1, res2:',globRes,globRes2
			WRITE(29,*)'DE Global res1, res2:',globRes,globRes2
		END IF
		IF ((globRes.LT.(10.0d0*conv)).AND.(globRes2.LT.conv)) THEN
            globFlag = 1
	    ELSE
		   globObjLast = globObj
		   globObjLast2 = globObj2
	    END IF
		
	END DO
	
	DEALLOCATE(snapStore,qualStore,maskStore,Fstore,CRstore,searchParam)
	deallocate(ptemp,Pt,Qu,optx, optl, optu, optg,vardef,lastPointStore)


END SUBROUTINE MCS_Optim


SUBROUTINE Tunnel_Node(NB1,NB2,badMask,hull)

	!This subroutine tunnels a node from the smallest bad element to the 
	!largest bad element

	USE common_Constants
	USE common_Parameters
	USE Geometry3DAll
	
	
	INTEGER, intent(IN) :: badMask(NB_Point)
	INTEGER, intent(IN) :: NB1,NB2,hull
	INTEGER :: ip, iSmall, ic, ie,nbp,iep
	REAL*8  :: maxCirc, minCirc, pC(3),avCirc,numTet
	
	CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
	
	maxCirc = 0.0d0
	minCirc = HUGE(0.0d0)
	
	CALL Get_Tet_Volume(0)
	
	DO ip = NB1,NB2
		IF (badMask(ip).EQ.1) THEN
	
			CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)
			avCirc = 0.0d0
			numTet = 0.0d0
			DO ic = 1,List_Length
				ie = ITs_List(ic)
				!avCirc = avCirc + Sphere_Tet(4,ie)
				avCirc = avCirc + Volume_Tet(ie)
				numTet = numTet + 1.0d0
				IF (Sphere_Tet(4,ie).GT.maxCirc) THEN
				!IF (Volume_Tet(ie).GT.maxCirc) THEN
				   maxCirc = Sphere_Tet(4,ie)
				   pC(:) = (Posit(:,IP_Tet(1,ie))+Posit(:,IP_Tet(2,ie)) &
						   +Posit(:,IP_Tet(3,ie))+Posit(:,IP_Tet(4,ie)))/4.0d0
				   
				END IF
				
				
						
			END DO
			!avCirc = avCirc/numTet
			IF (avCirc.LT.minCirc) THEN
			   minCirc = avCirc			   
			   iSmall = ip
			END IF
			
			
	    END IF
	END DO
	
	!Tunnel the node and reinsert
	Posit(:,iSmall) = pC(:)
	nbp = NB1-1
	IF (hull.EQ.1) THEN
	  CALL TetMeshStorage_CellRestore(NB_Tet, IP_Tet, HullBackUp)
	ELSE
	  CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
	  CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
	END IF
	CALL Next_Build()
	!CALL Set_Domain_Mapping()
	CircumUpdated = .TRUE.
	VolumeUpdated = .TRUE.
	
	DO iep = 1,NB_Tet
		CALL Get_Tet_SphereCross(iep)
		CALL Get_Tet_Volume(iep)
	END DO
	
	 CircumUpdated = .TRUE.
			 VolumeUpdated = .TRUE.
	!Mark_Point(1:NB_Point) = 0
	CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
	usePower = .FALSE.
	IF (hull.NE.1) THEN
		CALL Swap3D(2)
		CALL Next_Build()
	END IF
	CircumUpdated = .TRUE.
	VolumeUpdated = .TRUE.
	
	DO iep = 1,NB_Tet
		CALL Get_Tet_SphereCross(iep)
		CALL Get_Tet_Volume(iep)
	END DO
	
	 CircumUpdated = .TRUE.
			 VolumeUpdated = .TRUE.
	

END SUBROUTINE Tunnel_Node

























