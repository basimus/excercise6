!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!*******************************************************************************
!>
!!  Build the edge data structure of the domain.
!!  The boundary edges are first  
!!  @param[in]  Mside             : The size of array IP_Edge
!!                                  (no smaller than the number of edges).
!!  @param[out] IP_Edge(2,Mside)  : The two nodes of each edge.
!!  @param[out] NB_Edge           : The number of edges
!<     
!*******************************************************************************
SUBROUTINE Build_Edge( IP_Edge,NB_Edge,Mside)

  USE common_Constants
  USE common_Parameters
  IMPLICIT NONE

  TYPE(NodeNetTreeType) :: Edge_Tree

  INTEGER, INTENT(IN)  :: Mside
  INTEGER, INTENT(OUT) :: IP_Edge(2,Mside)
  INTEGER, INTENT(OUT) :: NB_Edge

  INTEGER :: IT, js, IEdge, ipp(2), ip1, ip2, n
  INTEGER, DIMENSION(:,:), POINTER :: icorner

  CALL Surface_Edge()
  CALL NodeNetTree_copy(Edge_BD_Tree, Edge_Tree)
  CALL NodeNetTree_allocate(2, NB_Point, Mside, Edge_Tree)

  IP_Edge(:,1:NB_BD_Edge) = IP_BD_Edge(:,1:NB_BD_Edge)
  IF(NB_Extra_Point==8)THEN
     !--- the extra points may lead a huge number of edges.
     !    A special treatment for them to save cpu time.
     ALLOCATE(icorner(NB_Extra_Point,NB_Point))
     icorner(:,:) = 0
  ENDIF

  NB_Edge = Edge_Tree%numNets
  DO  IT = 1,NB_Tet
     Loop_js : DO  js = 1,6
        ipp(1:2) = IP_Tet(I_Comb_Tet(1:2,js),IT)
        ip1 = MIN(ipp(1),ipp(2))
        ip2 = MAX(ipp(1),ipp(2))
        IF(NB_Extra_Point==8 .AND. ip1<=NB_Extra_Point)THEN
           CALL NodeNetTree_Search(2, ipp, Edge_Tree, IEdge)
           IF(IEdge>0) CYCLE Loop_js
           DO n = 1, NB_Extra_Point
              IF(icorner(n,ip2)==ip1) CYCLE Loop_js
              IF(icorner(n,ip2)==0)THEN
                 icorner(n,ip2) = ip1
                 CYCLE Loop_js
              ENDIF
           ENDDO
           CALL Error_Stop ('Build_Edge : icorner')
        ENDIF
           
        CALL NodeNetTree_SearchAdd(2, ipp, Edge_Tree, IEdge)
        IF(IEdge>Mside)THEN
           WRITE(29,*)'Error--- Mside is too small:',Mside
           CALL Error_Stop ('Build_Edge')
        ENDIF
        IF(IEdge>NB_Edge)THEN
           !-- a new edge
           IP_Edge(1,IEdge) = ip1
           IP_Edge(2,IEdge) = ip2
           NB_Edge = IEdge
        ENDIF
     ENDDO Loop_js
  ENDDO
  
  IF(NB_Edge/=Edge_Tree%numNets)THEN
     WRITE(29,*)'Error--- : NB_Edge/=Edge_Tree%numNets:',NB_Edge,Edge_Tree%numNets
     CALL Error_Stop ('Build_Edge')
  ENDIF
  
  WRITE(29,*)' '
  IF(NB_Extra_Point==8)THEN
     WRITE(29,*) ' No. of sides (without extra points) :', NB_Edge
     DO IP2 = 1, NB_Point
        DO n = 1, NB_Extra_Point
           IF(icorner(n,ip2)==0) EXIT
           NB_Edge = NB_Edge + 1
           IF(IEdge>Mside)THEN
              WRITE(29,*)'Error--- Mside is too small:',Mside
              CALL Error_Stop ('Build_Edge')
           ENDIF
           IP_Edge(1,NB_Edge) = icorner(n,ip2)
           IP_Edge(2,NB_Edge) = ip2
        ENDDO
     ENDDO
     DEALLOCATE(icorner)
  ENDIF

  WRITE(29,*) ' No. of sides :', NB_Edge
  WRITE(29,*)' '

  CALL NodeNetTree_clear(Edge_Tree)

  RETURN
END SUBROUTINE Build_Edge

!*******************************************************************************
!>     
!!  Count surface edges
!!  @param[in]   common_Parameters::NB_BD_Tri
!!  @param[in]   common_Parameters::NB_BD_Point
!!  @param[in]   common_Parameters::IP_BD_Tri
!!  @param[out]  common_Parameters::NB_BD_Edge
!!  @param[out]  common_Parameters::Edge_BD_Tree
!<
!*******************************************************************************
SUBROUTINE Surface_Edge()

  USE common_Parameters
  USE array_allocator
  USE Geometry3D
  IMPLICIT NONE

  INTEGER :: IB, i, IEdge, ippa(2), ippi(2), ipp(2)
  REAL*8  :: xlmax,xlmin,xl

  CALL Surface_Edge_Clear()
  CALL allc_2Dpointer(IP_BD_Edge,     2,20*NB_BD_Point,   'IP_BD_Edge')
  CALL NodeNetTree_allocate(2, NB_BD_Point, 3*NB_BD_Tri, Edge_BD_Tree)

  DO IB = 1,NB_BD_Tri
     DO i = 1,3
        ipp(1) = IP_BD_Tri(i,IB)
        ipp(2) = IP_BD_Tri(MOD(i,3)+1,IB)
        CALL NodeNetTree_SearchAdd(2,ipp,Edge_BD_Tree,IEdge)

        IF(IEdge>NB_BD_Edge)THEN
           !-- a new edge
           IP_BD_Edge(1,IEdge) = -ipp(1)
           IP_BD_Edge(2,IEdge) = ipp(2)
           NB_BD_Edge = IEdge
        ELSE
           IP_BD_Edge(1,IEdge) = ABS( IP_BD_Edge(1,IEdge) )
        ENDIF
     ENDDO
  ENDDO

  IF(NB_BD_Edge/=Edge_BD_Tree%numNets)THEN
     WRITE(29,*)'Error--- : NB_BD_Edge/=Edge_BD_Tree%numNets:',   &
               NB_BD_Edge,Edge_BD_Tree%numNets
     CALL Error_Stop ('Surface_Edge')
  ENDIF

  !WRITE(*, *)'Number of surface points/triangles/edges:',   &
  !     NB_BD_Point,NB_BD_Tri,NB_BD_Edge
  !WRITE(29,*)'Number of surface points/triangles/edges:',   &
  !     NB_BD_Point,NB_BD_Tri,NB_BD_Edge

  IF(Debug_Display>1)THEN
     xlmax = 0.d0
     xlmin = HugeValue
     DO IEdge=1,NB_BD_Edge
        IF(IP_BD_Edge(1,IEdge)<0)THEN
           WRITE(29,*)' Error--- : surface is not enclosed'
           WRITE(29,*)'    IEdge,IP_BD_Edge=',IEdge,IP_BD_Edge(:,IEdge)
           CALL Error_Stop ('Surface_Edge')
        ENDIF

        ipp(:) = IP_BD_Edge(:,IEdge)
        xl  = Geo3D_Distance_SQ(Posit(:,ipp(1)),Posit(:,ipp(2)))
        IF(xlmax<xl)THEN
           xlmax = xl
           ippa(:) = ipp(:)
        ENDIF
        IF(xlmin>xl)THEN
           xlmin = xl
           ippi(:) = ipp(:)
        ENDIF
     ENDDO
     WRITE(29,'(a,E12.4,a,i7,a,i7)')'max length of surface edges:',   &
          SQRT(xlmax),' between node',ippa(1),' and',ippa(2)
     WRITE(29,'(a,E12.4,a,i7,a,i7)')'min length of surface edges:',   &
          SQRT(xlmin),' between node',ippi(1),' and',ippi(2)
  ENDIF

  RETURN

END SUBROUTINE  Surface_Edge

!*******************************************************************************
!>     
!!  clear surface edges.
!<
!*******************************************************************************
SUBROUTINE Surface_Edge_Clear()
  USE common_Parameters
  IMPLICIT NONE

  CALL NodeNetTree_clear(Edge_BD_Tree)
  NB_BD_Edge = 0
  RETURN
END SUBROUTINE Surface_Edge_Clear

!*******************************************************************************
!>
!!    Check if two points form a boundary edge 
!!    @param[in]  IP1,IP2   The two points.
!!    @param[in]  common_Parameters::Edge_BD_Tree
!!    @param[out] Iedge    >0  The edge number which match the two points.    \n
!!                         <=0 No edge match the two points.
!<
!*******************************************************************************
SUBROUTINE Boundary_Edg_Match(IP1,IP2,Iedg)

  USE common_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: IP1,IP2
  INTEGER, INTENT(OUT) :: Iedg
  INTEGER :: ipp(2)

  ipp(1:2) = (/IP1,IP2/)
  CALL NodeNetTree_search(2,ipp,Edge_BD_Tree,Iedg)

  RETURN
END SUBROUTINE Boundary_Edg_Match

