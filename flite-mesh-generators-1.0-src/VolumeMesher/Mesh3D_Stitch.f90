!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!*******************************************************************************
!>      
!!   March boundary nodes of present mesh with a group of nodes pts(1:numNodes).
!!   Make sure the boundary nodes of present mesh have been put forward,
!!     but no particular order is needed for pts.
!!   @param[in] numNodes  : the number of the group of points.
!!   @param[in] pts       : the coordinates of the group of points.
!!   @param[in] common_Parameters::NB_BD_Point
!!   @param[in] common_Parameters::Posit
!!   @param[in]  Mark_BD (n) =-1  : do NOT investigate node pts(n).             \n
!!                       (n) = 0  : investigate node pts(n). 
!!   @param[out] Mark_BD (n) =-1  : node pts(n) has not been tested.            \n
!!                       (n) = 0  : node pts(n) do NOT march any boundary node. \n
!!                       (n) =i>0 : node pts(n) march node i of present mesh.
!!   @param[out] common_Parameters::Mark_Point
!!                       (i) = 0  : node i of present mesh
!!                                : do NOT march any node in the group.         \n
!!                       (i) =n>0 : node i of present mesh marchs node pts(n).       
!!  
!<
!*******************************************************************************
SUBROUTINE March_BoundNodes(numNodes, Mark_BD, pts)
  USE common_Parameters
  USE PtADTreeModule
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: numNodes
  REAL*8,  INTENT(IN) :: pts(3,*)
  INTEGER :: Mark_BD(*)
  TYPE (PtADTreeType) PtADTree
  INTEGER :: Isucc, Ips(2), ip,  NB
  REAL*8 :: HalfSize(3), Dist(2), pt(3)

  !--- build a Point-ADTree by the boundary points
  HalfSize = TinySize
  CALL PtADTree_SetName(PtADTree, 'March_BoundNodes')
  CALL PtADTree_SetTree(PtADTree, Posit, NB_BD_Point, HalfSize)
  Mark_Point(1:NB_Point) = 0

  DO ip = 1, numNodes
        if(Mark_BD(ip)==-1) cycle
        Isucc = -1
        NB = 2
        pt(:) = pts(:,ip)
        CALL PtADTree_SearchNode(PtADTree,pt,IPs, Dist, NB, Isucc)
        IF(Isucc==1 .AND. NB==1)THEN
           !--- match one node
           IF(Mark_Point(Ips(1))==0)THEN
              Mark_Point(Ips(1)) = ip
              Mark_BD(ip)        = Ips(1)
           ELSE
              CALL Error_Stop ('March_BoundNodes :: many nodes from hybrid mesh mapping to one point?')
           ENDIF
        ELSE IF(Isucc==0)THEN
           Mark_BD(ip) = 0
        ELSE
           CALL Error_Stop (' March_BoundNodes :: one nodes from hybrid mesh mapping to many points?')
        ENDIF
  ENDDO

  CALL PtADTree_Clear(PtADTree)

  RETURN

END SUBROUTINE March_BoundNodes


!*******************************************************************************
!>      
!!   Merge a tetrahedra mesh, TetMesh, to the present mesh.
!!   Two meshes may share some boundary points.      
!!   Make sure the boundary nodes of the present mesh have been put forward.
!<
!*******************************************************************************
SUBROUTINE Mesh3D_Stitch(TetMesh)
  USE common_Parameters
  USE PtADTreeModule
  USE Geometry3D
  IMPLICIT NONE

  TYPE(TetMeshStorageType) :: TetMesh
  INTEGER :: ip, it, ib, NB, ip4(4)
  INTEGER, DIMENSION(:),  POINTER :: Mark_BD

  !--- march boundary nodes with TetMesh points
  ALLOCATE(Mark_BD(TetMesh%NB_Point))
  Mark_BD(1:TetMesh%NB_Point) = 0
  CALL March_BoundNodes(TetMesh%NB_Point, Mark_BD, TetMesh%Posit)

  DO ip = 1, TetMesh%NB_Point
     IF(Mark_BD(ip)<=0)THEN
       !--- add a point
       NB_Point = NB_Point + 1
       Mark_BD(ip) = NB_Point
       IF(NB_Point>nallc_point) CALL ReAllocate_Point()
       Posit(:,NB_Point) = TetMesh%Posit(:,ip)
       RR_Point(NB_Point) = 0.d0
     ENDIF
  ENDDO

  DO it = 1, TetMesh%NB_Tet
     NB_Tet = NB_Tet+1
     IF(NB_Tet>nallc_Tet) CALL ReAllocate_Tet()
     IP_Tet(:,NB_Tet) = Mark_BD(TetMesh%IP_Tet(:,it))
     ip4(1:4) = IP_Tet(1:4,NB_Tet)
     sphere_Tet(1:3,NB_Tet) = Geo3D_Sphere_Centre(Posit(:,ip4(1)),  &
                Posit(:,ip4(2)),Posit(:,ip4(3)),Posit(:,ip4(4)))
  ENDDO

  NB = 0
  DO ib = 1,NB_BD_Tri
     if(Mark_Point(IP_BD_Tri(1,ib))==0 .or.    &
        Mark_Point(IP_BD_Tri(2,ib))==0 .or.    &
        Mark_Point(IP_BD_Tri(3,ib))==0)then
        NB = NB + 1
        IP_BD_Tri(:,NB) = IP_BD_Tri(:,ib)
     endif
  ENDDO
  NB_BD_Tri = NB

  CALL Next_Build()
  CALL Boundary_Associate(4)
  CALL Forward_BoundaryNodes()

  VolumeUpdated = .FALSE.
  CircumUpdated = .FALSE.

  DEALLOCATE(Mark_BD)

  RETURN

END SUBROUTINE Mesh3D_Stitch

!*******************************************************************************
!>      
!!   Merge a hybrid mesh, HybMesh with HybSurf, to the present mesh.
!!   Two meshes may share some boundary points.      
!!   Make sure the boundary nodes of the present mesh have been put forward.
!<
!*******************************************************************************
SUBROUTINE Mesh3D_StitchHybrid(HybMesh, HybSurf)
  USE common_Parameters
  USE PtADTreeModule
  USE array_allocator
  IMPLICIT NONE

  TYPE(HybridMeshStorageType)  :: HybMesh
  TYPE(SurfaceMeshStorageType) :: HybSurf
  INTEGER :: ip, it, ib, jp, NB, nbtot, ns, i, ip3(3), NB1, NB2, NBold
  INTEGER, DIMENSION(:),  POINTER :: Mark_BD

  IF( Frame_Type==4 .and. (HybMesh%NB_Hex>0 .OR. HybMesh%NB_Pyr>0   &
      .OR. HybSurf%NB_Quad>0) )THEN
      WRITE(29,*)'Error---- Frame_Type=4 not for Hybrid-mesh'
      CALL Error_Stop ('Mesh3D_StitchHybrid')
  ENDIF

  !--- march boundary nodes with HybMesh points
  ALLOCATE(Mark_BD(HybMesh%NB_Point))
  Mark_BD(1:HybMesh%NB_Point) = -1
  DO ib = 1, HybSurf%NB_Tri
     Mark_BD(HybSurf%IP_Tri(1:3,ib)) = 0
  ENDDO
  CALL March_BoundNodes(HybMesh%NB_Point, Mark_BD, HybMesh%Posit)


  NBold = NB_Point
  DO ip = 1, HybMesh%NB_Point
     IF(Mark_BD(ip)<=0)THEN
       !--- add a point
       NB_Point = NB_Point + 1
       Mark_BD(ip) = NB_Point
       IF(NB_Point>nallc_point) CALL ReAllocate_Point()
       Posit(:,NB_Point) = HybMesh%Posit(:,ip)
     ENDIF
  ENDDO


  nbtot = HybMesh%NB_Tet + NB_Tet
  nallc_Tet = nbtot
  CALL ReAllocate_Tet()
  DO it = 1,HybMesh%NB_Tet
     IP_Tet(:,it+NB_Tet) = Mark_BD(HybMesh%IP_Tet(:,it))
  ENDDO
  NB_Tet = nbtot

  CALL allc_2Dpointer(IP_BD_Tri, 5, NB_BD_Tri+HybSurf%NB_Tri, 'HybSurf%IP_Tri')
  ns = 0
  nbtot = 0
  DO ib = 1,NB_BD_Tri
     IF(  Mark_Point(IP_BD_Tri(1,ib))==0 .OR.    &
          Mark_Point(IP_BD_Tri(2,ib))==0 .OR.    &
          Mark_Point(IP_BD_Tri(3,ib))==0 )THEN
        nbtot = nbtot + 1
        IP_BD_Tri(:,nbtot) = IP_BD_Tri(:,ib)
        ns = MAX(ns, IP_BD_Tri(5,ib))
     ENDIF
  ENDDO
  DO ib = 1,HybSurf%NB_Tri
     IF(  Mark_BD(HybSurf%IP_Tri(1,ib))>NBold .OR.    &
          Mark_BD(HybSurf%IP_Tri(2,ib))>NBold .OR.    &
          Mark_BD(HybSurf%IP_Tri(3,ib))>NBold )THEN
        nbtot = nbtot + 1
        ip3(:) = Mark_BD(HybSurf%IP_Tri(1:3,ib))
        CALL sort_ipp_tri(ip3)
        IP_BD_Tri(1:3,nbtot) = ip3(:)
        IP_BD_Tri(4,  nbtot) = 0
        IP_BD_Tri(5,  nbtot) = HybSurf%IP_Tri(5,ib) + ns
     ENDIF
  ENDDO
  write(*,*)' number of triangles: from ',NB_BD_Tri, HybSurf%NB_Tri,  &
            ' to ', nbtot
  NB_BD_Tri = nbtot

  NB_BD_Point = NB_Point
  CALL Boundary_Associate(3)

  IF(Frame_Type==4)THEN
     CALL Forward_BoundaryNodes()
  ELSE IF(Frame_Type==2 .OR. Frame_Type==5)THEN
     !--- restore mesh to HybMesh & HybSurf
     DO it = 1, HybMesh%NB_Pyr
        HybMesh%IP_Pyr(:,it) = Mark_BD(HybMesh%IP_Pyr(:,it))
     ENDDO
     DO it = 1, HybMesh%NB_Hex
        HybMesh%IP_Hex(:,it) = Mark_BD(HybMesh%IP_Hex(:,it))
     ENDDO
     DO ib = 1, HybSurf%NB_Quad
        HybSurf%IP_Quad(1:4,ib) = Mark_BD(HybSurf%IP_Quad(1:4,ib))
        HybSurf%IP_Quad(5,  ib) = HybSurf%IP_Quad(5,ib) + ns
     ENDDO

     Mark_Point(1:NB_Point) = 0
     DO ib=1,NB_BD_Tri
        Mark_Point(IP_BD_Tri(1:3,IB)) = -1
     ENDDO
     DO ib=1,HybSurf%NB_Quad
        Mark_Point(HybSurf%IP_Quad(1:4,IB)) = -1
     ENDDO
     CALL Sort_Nodes(NB1,NB2)
     NB_BD_Point = NB1

     DEALLOCATE(HybMesh%Posit)
     ALLOCATE(HybMesh%Posit(3,NB_Point))
     HybMesh%Posit(:,1:NB_Point) = Posit(:,1:NB_Point)
     HybMesh%NB_Point = NB_Point

     IF(ASSOCIATED(HybMesh%IP_Tet)) DEALLOCATE(HybMesh%IP_Tet)
     ALLOCATE(HybMesh%IP_Tet(4,NB_Tet))
     DO it = 1, NB_Tet
        HybMesh%IP_Tet(:,it) = IP_Tet(:,it)    !--- already sorted in Sort_Nodes()
     ENDDO
     HybMesh%NB_Tet = NB_Tet

     DO it = 1, HybMesh%NB_Pyr
        HybMesh%IP_Pyr(:,it) = Mark_Point(HybMesh%IP_Pyr(:,it))
     ENDDO
     DO it = 1, HybMesh%NB_Hex
        HybMesh%IP_Hex(:,it) = Mark_Point(HybMesh%IP_Hex(:,it))
     ENDDO

     DEALLOCATE(HybSurf%IP_Tri)
     ALLOCATE(HybSurf%IP_Tri(5,NB_BD_Tri))
     DO ib=1,NB_BD_Tri
        HybSurf%IP_Tri(:,ib) = IP_BD_Tri(:,IB)     !--- already sorted in Sort_Nodes()
     ENDDO
     HybSurf%NB_Tri = NB_BD_Tri

     DO ib=1,HybSurf%NB_Quad
        HybSurf%IP_Quad(1:4,IB) = Mark_Point(HybSurf%IP_Quad(1:4,IB))
     ENDDO
     HybSurf%NB_Point = NB_BD_Point
  ELSE
     CALL Error_Stop ('Mesh3D_StitchHybrid :: not for this Frame_Type')
  ENDIF

  VolumeUpdated = .FALSE.
  CircumUpdated = .FALSE.
  DEALLOCATE(Mark_BD)


  RETURN

END SUBROUTINE Mesh3D_StitchHybrid


!*******************************************************************************
!>      
!!   Merge a tetrahedra mesh, VM, to the present mesh.
!!   Two meshes may share some boundary points.      
!!   Make sure the boundary nodes of the present mesh have been put forward.
!<
!*******************************************************************************
SUBROUTINE Mesh3D_StitchVisLayer(Isucc)
  USE common_Parameters
  USE Geometry3D
  USE HybridMeshHighOrder
  IMPLICIT NONE

  INTEGER, INTENT(OUT) :: Isucc
  INTEGER :: ip, it, itnew, ib, i, id, j, ND
  INTEGER, DIMENSION(:),  POINTER :: Mark_BD
  REAL*8, DIMENSION(:,:),  POINTER :: coorpold

  Isucc = 0
  IF(NB_BD_Layers==0) RETURN
  IF(VM%NB_Point==0) RETURN
  IF(.NOT. ASSOCIATED(Mark_Vis_Point)) RETURN
      
  !--- march boundary nodes with TetMesh points
  ALLOCATE(Mark_BD(VM%NB_Point+MeshOut%NB_Point))
  Mark_BD(:) = 0
  DO i = 1, VM%NB_Point
     IF(Mark_Vis_Point(i)>0)THEN
        ip = Mark_Vis_Point(i)
        IF(ip>NB_BD_Point) CYCLE
        Mark_BD(ip) = i
     ENDIF
  ENDDO
  
  IF(Debug_Display>2)THEN
     IF(NB_BD_Point/=Surf%NB_Point-Surf%NB_Psp)THEN
        CALL Error_Stop ('Mesh3D_StitchVisLayer :: NB_BD_Point/=Surf%NB_Point')
     ENDIF
     DO ip = 1, NB_BD_Point
        i = Mark_BD(ip)
        IF(i==0)  CALL Error_Stop (' i==0')
        DO j = 1, 3
           IF(ABS(VM%Posit(j,i)-MeshOut%Posit(j,ip))>MAX(1.D-8*ABS(MeshOut%Posit(j,ip)), TinySize))THEN
              WRITE(29,*)'Error--- position not match boundary layer:',i,ip
              WRITE(29,*) VM%Posit(:,i)
              WRITE(29,*) MeshOut%Posit(:,ip)
              CALL Error_Stop (' Mesh3D_StitchVisLayer ::')
           ENDIF
        ENDDO
     ENDDO
  ENDIF
  
  !--- add MeshOut to VM
  
  CALL allc_2Dpointer(VM%Posit, 3, VM%NB_Point+MeshOut%NB_Point)
  id = VM%NB_Point - NB_BD_Point
  DO ip = NB_BD_Point+1, MeshOut%NB_Point
     Mark_BD(ip) = ip + id
     VM%Posit(:,Mark_BD(ip)) = MeshOut%Posit(:,ip)
  ENDDO
  
  VM%NB_Point = MeshOut%NB_Point + id
    
  ND = MAX(Npoint_Tet(VM%GridOrder), Npoint_Tet(MeshOut%GridOrder))
  CALL allc_2Dpointer(VM%IP_Tet, ND, VM%NB_Tet+MeshOut%NB_Tet)
  DO it = 1, MeshOut%NB_Tet
     itnew = it + VM%NB_Tet
     VM%IP_Tet(:,itnew) = Mark_BD(MeshOut%IP_Tet(:, it))
  ENDDO
  VM%NB_Tet = VM%NB_Tet + MeshOut%NB_Tet

  IF(VM%GridOrder>1)THEN
     !--- match high-order nodes
     CALL HybridMesh_LinearFill(VM, 0)
     CALL HybridMesh_CleanNodes(VM, Mark_BD)
        DO IB = 1,SurfOld%NB_Tri
           SurfOld%IP_Tri(1:3,IB) = Mark_BD(SurfOld%IP_Tri(1:3,IB))
        ENDDO
  ENDIF
  
  !--- Forward Boundary Nodes

  Mark_BD(1:VM%NB_Point) = 0
  DO IB = 1,SurfOld%NB_Tri
     CALL sort_ipp_tri(SurfOld%IP_Tri(1:3,IB))
     Mark_BD(SurfOld%IP_Tri(1:3,IB)) = -1     
  ENDDO
  
  CALL SignSort(VM%NB_Point, Mark_BD, i,j)
  SurfOld%NB_Point = i
  
  ALLOCATE(coorpold(3,VM%NB_Point))
  coorpold(:,1:VM%NB_Point) = VM%Posit(:,1:VM%NB_Point)
  DO ip = 1, VM%NB_Point
     VM%Posit(:,Mark_BD(ip))  = coorpold(:,ip)
  ENDDO
  DO it=1,VM%NB_Tet
     VM%IP_Tet(:,it) = Mark_BD(VM%IP_Tet(:,it))
  ENDDO
  DO it=1,VM%NB_Pyr
     VM%IP_Pyr(:,it) = Mark_BD(VM%IP_Pyr(:,it))
  ENDDO
  DO it=1,VM%NB_Prm
     VM%IP_Prm(:,it) = Mark_BD(VM%IP_Prm(:,it))
  ENDDO
  DO IB = 1,SurfOld%NB_Tri
     SurfOld%IP_Tri(1:3,IB) = Mark_BD(SurfOld%IP_Tri(1:3,IB))  
     CALL sort_ipp_tri(SurfOld%IP_Tri(1:3,IB))
  ENDDO
  
  CALL HybridMesh_AssoBoundSurface(VM, SurfOld)  

  DEALLOCATE(Mark_BD, coorpold)
  Isucc = 1

  RETURN

END SUBROUTINE Mesh3D_StitchVisLayer

