c
c  Copyright (C) 2017 College of Engineering, Swansea University
c
c  This file is part of the SwanSim FLITE suite of tools.
c
c  SwanSim FLITE is free software: you can redistribute it and/or modify
c  it under the terms of the GNU General Public License as published by
c  the Free Software Foundation, either version 3 of the License, or
c  (at your option) any later version.
c
c  SwanSim FLITE is distributed in the hope that it will be useful,
c  but WITHOUT ANY WARRANTY; without even the implied warranty of
c  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c  GNU General Public License for more details.
c
c  You should have received a copy of the GNU General Public License
c  along with this SwanSim FLITE product. 
c  If not, see <http://www.gnu.org/licenses/>.
c


c                                                                                      
c  L-BFGS-B is released under the “New BSD License” (aka “Modified BSD License”        
c  or “3-clause license”)                                                              
c  Please read attached file License.txt                                               
c                                        
      subroutine timer(ttime)
      double precision ttime
c
      real temp
c
c     This routine computes cpu time in double precision; it makes use of 
c     the intrinsic f90 cpu_time therefore a conversion type is
c     needed.
c
c           J.L Morales  Departamento de Matematicas, 
c                        Instituto Tecnologico Autonomo de Mexico
c                        Mexico D.F.
c
c           J.L Nocedal  Department of Electrical Engineering and
c                        Computer Science.
c                        Northwestern University. Evanston, IL. USA
c                         
c                        January 21, 2011
c
      temp = sngl(ttime)
      call cpu_time(temp)
      ttime = dble(temp) 

      return

      end
      
