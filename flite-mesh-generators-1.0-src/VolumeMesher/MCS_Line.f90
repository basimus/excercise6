!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  MCS_Line
!!  
!!  This routine uses MCS to optimise positions of each node
!!  It acts on nodes opposite the faces of bad elements
!!
!!  
!!
!!  Initial implimentation by S. Walton 16/01/1013
!!
!<
!*******************************************************************************
SUBROUTINE MCS_Line(NB1)
	USE common_Constants
	USE common_Parameters
	USE array_allocator
	USE SpacingStorage
	USE OptimisingModule
	USE Geometry3DAll
	
	INTEGER, INTENT(IN)  :: NB1  !The first point effected by this process
	INTEGER :: K,ie,ip,DD,N,Ibest,I,Loop,Istatus,Iin,Ni,Di,idum,isFinished,numLoops,ic,flag,nbp,improve,NB_old,MaxL,L,j,ii,le,iii
	REAL*8, allocatable  :: optx(:), optl(:), optu(:), optg(:) ,ptemp(:),Pt(:,:),Qu(:),vardef(:,:)!,allPsave(:,:)
	REAL*8 :: dd2,ftemp,fbest,ranNum,d,FTarget,FBig,p1(3),dtet,psave(3),rsave,vol,ptet(3,4),p2(3),p3(3),p4(3)
	REAL*8 :: pp4(3,4),dum(4),coef,sumObj,lastObj,p1t(3),p2t(3),residual,delta,dist
	INTEGER :: ITnb,merged
	
	REAL*8 :: v(3),R1,R2,x(3)
	REAL*8 :: dum1,conv,dum2
	
	OPEN(84,file='centroidsmooth.ctl',status='old',BLANK='NULL')
      READ(84,*) dum1
      READ(84,*) conv
	  READ(84,*) dum2
      CLOSE(84)
	
	lastObj = HUGE(0.d0)
	
	improve = 1
	MaxL = 100
	L = 0
	
	DO WHILE((improve.EQ.1).AND.(L<MaxL))
		
		FTarget = (1.D-6)*BGSpacing%MinSize
		FBig = 1.D30
		idum = -1
		sumObj = 0.d0
		
		!Get link info
		CALL Next_Build()
		!Make sure mapping and geometry is all up to date
		!CALL Get_Tet_SphereCross(0)  Not used
		!CALL Get_Tet_Volume(0)  Not used
		CALL Set_Domain_Mapping()
		CALL LinkAssociation_Build(4, NB_Tet, IP_Tet, NB_Point, PointAsso)
		
		!Set up the optimisation problem
		CALL init_random_seed()
		numLoops = 2 !Number of MCS loops
		DD = 1  !Number of dimensions - we are transforming this to a line search
		N = 10  !Number of eggs
		
		allocate(ptemp(DD),Pt(DD,N),Qu(N),optx(DD), optl(DD), optu(DD), optg(DD),vardef(DD,2))
		!allocate(allPsave(3,NB_Point))
		!allPsave(:,1:NB_Point) = Posit(:,1:NB_Point)
		NB_old = NB_Point
		
		!Loop around tets
		DO le = 1,NB_Tet
		
			! We want to see if this element is bad or not, and if it is bad which element the point
			! lies in
			CALL Get_Tet_SphereCross(le) 
			
			v = Sphere_Tet(1:3,le)
			
			ie = le
			CALL Locate_Node_Tet(v,ie,3)
			
			
			
			IF ((ie>0).AND.(ie/=le)) THEN
				!Then the element is bad and the voronoi point is in ie
				!Now we need to find the node in ie which is opposite le
				ip = IP_Tet(1,le)
				DO ii = 1,4		
					
					IF (le.EQ.Next_Tet(ii,ie)) THEN
						ip = IP_Tet(ii,ie)
						
					ENDIF
				ENDDO
				
				!So ip is our node
				
				
				IF (ip>NB1) THEN
					
					!Get connected elements
					CALL LinkAssociation_List(ip,List_Length,ITs_List,PointAsso)	
					ptemp(1:DD) = 0.0d0
					Ibest = 0
					ftemp = 0.0d0
					I = 0
					Loop = 0
					Istatus = 0
					Iin = 0
					Qu(1:N) = huge(0.0)
					fbest = huge(0.0)

					x = Posit(:,ip)
					R1 = Geo3D_Distance(v,Posit(:,IP_Tet(1,le)))
					R2 = Geo3D_Distance(v,x)
					
				
					!Upper and lower bounds are going to be R1 and R2
					IF (R1>R2) THEN
						optl(1:DD) = R2
						optu(1:DD) = R1
					ELSE
						optl(1:DD) = R1
						optu(1:DD) = R2
					ENDIF
				
					!The last egg will be the initial position
					DO Ni = 1,N
						DO Di = 1,DD
						
							Pt(Di,Ni) = optl(Di) + (Ni/N)*(optu(Di)-optl(Di))
						
						
						ENDDO
						
					ENDDO
					
					!Make one the original point
					Pt(1:DD,N/2) = R2
				
					isFinished = 0
					
					!WRITE(*,*)'x:',x,'R2',R2
				
					DO WHILE (isFinished==0)
					
						
					
						CALL Optimising_Cuckoo_Driver(DD,N,ptemp,Ibest,ftemp,Pt,I,Loop,Istatus,Iin,Qu,fbest,optu,optl,idum)
						
						IF ((Loop-1).LT.numLoops) THEN
						
							!Calculate fitness at ptemp and put it into ftemp
							DO iii = 1,3
							  Posit(iii,ip) = v(iii) + ((ptemp(1)/R2)*(x(iii)-v(iii)))
							END DO
							!WRITE(*,*)'Posit:',Posit(:,ip), 'ptemp', ptemp
							!ftemp = ptemp(4)*ptemp(4)
							ftemp = 0
							flag = 0
							
							DO ic = 1,List_Length
							
								ie = ITs_List(ic)
								p1 = Posit(:,IP_Tet(1,ie))
								p2 = Posit(:,IP_Tet(2,ie))
								p3 = Posit(:,IP_Tet(3,ie))
								p4 = Posit(:,IP_Tet(4,ie))
								vol = Geo3D_Tet_Volume(p1,p2,p3,p4)
								IF(vol.LE.0.d0)THEN
									flag = 1
									ftemp = huge(0.d0)
									EXIT
								ELSE
									CALL Get_Tet_SphereCross(ie) 
									!Check if it's inside
									ptet(:,1) = p1
									ptet(:,2) = p2
									ptet(:,3) = p3
									ptet(:,4) = p4
									p1t = Sphere_Tet(1:3,ie)
									K=0
									dum =  Geo3D_Tet_Weight(K,ptet,p1t)
									
									IF (K==1) THEN
										!We are inside
										
									ELSE
										!There is a possibility to merge
										merged = 0
										IF(BGSpacing%Model>0)THEN
				
											delta = Scale_Tet(ie)*BGSpacing%BasicSize
													
										ELSE IF(BGSpacing%Model<0)THEN
											delta = MINVAL(fMap_Tet(:,:,ie))*BGSpacing%BasicSize
													
										END IF
										
										DO ii = 1,4
											ITnb = Next_Tet(ii,ie)
											IF(ITnb<=0) CYCLE
											CALL Get_Tet_SphereCross(ITnb)
											p2t = Sphere_Tet(:,ITnb)
											dd2 = Geo3D_Distance_SQ(p2t, p1t)
											IF(dd2<((0.1*delta)*(0.1*delta)))THEN
											   merged = 1
											ENDIF
										ENDDO
										IF (merged.EQ.0) THEN
											p1t = Sphere_Tet(1:3,ie)
											 coef = 1.d0
											 
												p2t = p1+p2+p3+p4 / 4.d0
											dd2 = Geo3D_Distance_SQ(p2t, p1t)
											DO j=1,4
											  IF (IP_Tet(j,ie).EQ.ip) THEN
											   IF (Next_Tet(j,ie)<0)THEN
												!Boundary
												coef = 2.d0								
											   ENDIF
											   EXIT
											  ENDIF
											ENDDO
											ftemp = ftemp + (coef*dd2)/vol
										 ENDIF
									ENDIF

									
									
									ENDIF
							
							ENDDO
							
							
							IF (flag.EQ.1) THEN
								ftemp = huge(0.d0)
							ENDIF
							IF (ftemp==0) THEN
								isFinished = 1
								EXIT
							ENDIF
							
						ELSE
						
							!The best result is in Pt(:,Ibest)
							Posit(1:3,ip) = v(1:3) + (Pt(1,Ibest)/R2)*(x(1:3)-v(1:3))
							
							
							sumObj = sumObj + fbest
							isFinished = 1
							EXIT
						ENDIF
				
					ENDDO
						
						
						
						
						
						
						
						
						
						
						ENDIF
					ENDIF
			
				

				
				
					
					
					
		
		ENDDO
		deallocate(ptemp,Pt,Qu,optx, optl, optu, optg,vardef)

		!Reinsert
		nbp = NB1-1
		CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
		CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
		CALL Next_Build()
		CALL Set_Domain_Mapping()
		CALL Get_Tet_SphereCross(0)
		CALL Get_Tet_Volume(0)
		Mark_Point(1:NB_Point) = 0
		CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
		usePower = .FALSE.
		CALL Remove_Point()
                !CALL Swap3D(2)
		!CALL Element_Collapse( )
		CALL Next_Build()
                CALL Get_Tet_SphereCross(0)
                CALL Get_Tet_Volume(0)



		residual = ABS(1.d0 - (lastObj/sumObj))
		IF(Debug_Display>0)THEN
			
			WRITE(*,*)'MCS Line Objective funtion value:',sumObj,residual
			WRITE(29,*)'MCS Line Objective funtion value:',sumObj,residual
		END IF
		
		IF (residual>conv) THEN
			lastObj = sumObj
			!deallocate(allPsave)
			L = L+1
		ELSE
		!	nbp = NB1-1
		!	CALL TetMeshStorage_NodeRestore(nbp,     Posit,  CVTMeshInit)
		!	CALL TetMeshStorage_CellRestore(NB_Tet,  IP_Tet, CVTMeshInit)
			
		!	Posit(:,1:NB_Old) = allPsave(:,1:NB_Old)
         !               NB_Point = NB_Old

		!	CALL Next_Build()
		!	CALL Set_Domain_Mapping()
		!	CALL Get_Tet_SphereCross(0)
		!	CALL Get_Tet_Volume(0)
		!	Mark_Point(1:NB_Point) = 0
		!	CALL Insert_Points_EasyPeasy(NB1, NB_Point, 10)
		!	usePower = .FALSE.
		!	CALL Remove_Point()
		!	CALL Next_Build()
         !                CALL Get_Tet_SphereCross(0)
         !       CALL Get_Tet_Volume(0)


			improve = 0
			EXIT
		
		ENDIF

	
	ENDDO


END SUBROUTINE MCS_Line




























