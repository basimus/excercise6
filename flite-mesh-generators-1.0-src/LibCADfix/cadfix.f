c
c  Copyright (C) 2017 College of Engineering, Swansea University
c
c  This file is part of the SwanSim FLITE suite of tools.
c
c  SwanSim FLITE is free software: you can redistribute it and/or modify
c  it under the terms of the GNU General Public License as published by
c  the Free Software Foundation, either version 3 of the License, or
c  (at your option) any later version.
c
c  SwanSim FLITE is distributed in the hope that it will be useful,
c  but WITHOUT ANY WARRANTY; without even the implied warranty of
c  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c  GNU General Public License for more details.
c
c  You should have received a copy of the GNU General Public License
c  along with this SwanSim FLITE product. 
c  If not, see <http://www.gnu.org/licenses/>.
c


#if 0
#ifdef _WIN32
#define CFI_INIT cficInit
#define CFI_EXIT cficExit
#define CFI_FILE_MODEL_OPEN cficFileModelOpen
#define CFI_GET_MODEL_BOX cficGetModelBox
#define CFI_GET_MODEL_ENTITY_LIST cficGetModelEntityList
#define CFI_CALC_LINE_FACET_TOTAL cficCalcLineFacetTotal
#define CFI_GET_LINE_FACET_VERT_LIST cficGetLineFacetVertList
#define CFI_CALC_LINE_T_FROM_XYZ cficCalcLineTFromXYZ
#define CFI_CALC_LINE_DERIV_AT_T cficCalcLineDerivAtT
#define CFI_GET_FACE_LOOP_TOTAL cficGetFaceLoopTotal
#define CFI_GET_FACE_LOOP_LINE_TOTAL cficGetFaceLoopLineTotal
#define CFI_GET_FACE_LOOP_LINE_LIST cficGetFaceLoopLineList
#define CFI_CALC_FACE_UV_FROM_XYZ cficCalcFaceUVFromXYZ
#define CFI_CALC_FACE_XYZ_AT_UV cficCalcFaceXYZAtUV
#define CFI_CALC_FACE_DERIV_AT_UV cficCalcFaceDerivAtUV
#define CFI_GET_FACE_UV_BOX cficGetFaceUVBox
#define CFI_GET_MODEL_NAME cficGetModelName
#define CFI_GET_MODEL_TYPE_TOTAL cficGetModelTypeTotal
#define CFI_GET_MODEL_TYPE_LIST cficGetModelTypeList
#define CFI_GET_MODEL_ENTITY_TOTAL cficGetModelEntityTotal
#define CFI_GET_POINT_DEFN cficGetPointDefn
#define CFI_GET_LINE_T_BOX cficGetLineTBox
#define CFI_GET_ENTITY_NAME cficGetEntityName
#define CFI_GET_PARAMETRIC_UV_BOX cficGetParametricUVBox
#define CFI_GET_TOPO_EMBED cficGetTopoEmbed
#define CFI_TRACE cficTrace
#define CFI_CHECK cficCheck
#endif
#endif

#ifndef _CADFIX
      

      SUBROUTINE CADFIX_Check_Availability( TF )
        IMPLICIT NONE
        LOGICAL TF
        TF = .FALSE.
        RETURN
      END SUBROUTINE CADFIX_Check_Availability

      SUBROUTINE CADFIX_Init()
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_Init

      SUBROUTINE CADFIX_IsClientMode( TF )
      LOGICAL TF
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END

      SUBROUTINE CADFIX_Exit()
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_Exit

      SUBROUTINE CADFIX_GetTolerance( tolerance )
        REAL*8  tolerance
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetTolerance

      SUBROUTINE CADFIX_GetNumCurves( numCurves )
        INTEGER numCurves
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetNumCurves

      SUBROUTINE CADFIX_GetCurveName( curveNum, entityName )
        CHARACTER*255  entityName
        INTEGER  curveNum
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END

      SUBROUTINE CADFIX_GetSurfaceName( SurfaceNum, entityName )
        CHARACTER*255  entityName
        INTEGER  SurfaceNum
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END

      SUBROUTINE CADFIX_GetNumSurfaces( numSurfaces )
        INTEGER numSurfaces
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetNumSurfaces

      FUNCTION CADFIX_GetCurveID( curveNum )
        INTEGER  CADFIX_GetCurveID, curveNum
        CADFIX_GetCurveID = 0
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END

      FUNCTION CADFIX_GetSurfaceID( surfaceNum )
        INTEGER  CADFIX_GetSurfaceID, surfaceNum
        CADFIX_GetSurfaceID = 0
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END

      SUBROUTINE CADFIX_CalculateCurveFacets( curveNum, numVertices,
     &      pVertices, maxEdgeLength )
        INTEGER curveNum, numVertices
        REAL*8  maxEdgeLength
        POINTER (pVertices, vertices(7,1))
        REAL*8  vertices
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_CalculateCurveFacets


      SUBROUTINE CADFIX_GetUVFromXYZ( surfaceNum, numPoints, xyz, uv )
        INTEGER surfaceNum, numPoints
        REAL*8  xyz(3,numPoints), uv(2,numPoints)
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetUVFromXYZ

      SUBROUTINE CADFIX_GetXYZFromUV1( surfaceNum, uv, xyz )
        INTEGER surfaceNum
        REAL*8  xyz(3), uv(2)
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetXYZFromUV1

      SUBROUTINE CADFIX_GetUVPointInfo( surfaceNum, u, v, info )
        INTEGER surfaceNum
        REAL*8  u, v, uv(2), info(18)
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetUVPointInfo

      SUBROUTINE CADFIX_GetUVPointInfoAll
     &          ( surfaceNum, u, v, R,Ru,Rv,Ruv,Ruu,Rvv )
        INTEGER  surfaceNum
        REAL*8   u, v, uv(2), R(3), Ru(3),Rv(3),Ruv(3),Ruu(3),Rvv(3)
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetUVPointInfoAll


      SUBROUTINE CADFIX_LoadGeom( filenam )
        CHARACTER*80  filenam
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_LoadGeom

      SUBROUTINE CADFIX_GetLineTBox( curveNum, tMin, tMax )
        INTEGER  curveNum
        REAL*8   tMin, tMax
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetLineTBox
    
      SUBROUTINE CADFIX_GetLineXYZFromT ( CurveNum, u, XYZ )
        INTEGER  curveNum
        REAL*8   u,XYZ(3)
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetLineXYZFromT

      SUBROUTINE CADFIX_GetLineCurv ( CurveNum, u, ck )
        INTEGER  curveNum
        REAL*8   u,ck
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetLineCurv

      SUBROUTINE CADFIX_GetLinePointDeriv( CurveNum, u, Ru,Ruu)
        INTEGER  curveNum
        REAL*8   u,Ru(3), Ruu(3)
        WRITE(*,*) 'Error --- no CADfix linked'
      END
      
      SUBROUTINE CADFIX_GetSurfaceUVBox(surfaceNum,uMin,vMin,uMax,vMax)
        INTEGER surfaceNum
        REAL*8  uMin, vMin, uMax, vMax
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetSurfaceUVBox


      SUBROUTINE CADFIX_AnalyseModel()
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_AnalyseModel

      SUBROUTINE CADFIX_GetSurfaceNumCurves( surfaceNum, nc)
        INTEGER surfaceNum, nc
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetSurfaceNumCurves

      SUBROUTINE CADFIX_GetSurfaceTopoType( surfaceNum, Itopo)
        INTEGER surfaceNum, Itopo
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetSurfaceTopoType
      
      SUBROUTINE CADFIX_GetSurfaceCurves( surfaceNum, nc, List )
        INTEGER surfaceNum, nc, List(*)
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetSurfaceCurves

      SUBROUTINE CADFIX_Output(filenam, NB_Point, NB_Tri, IP_Tri,
     &                Posit, NumCurves, CurveNodeNum, CurveNodeList)
        INTEGER NB_Point, NB_Tri, IP_Tri(4,*)
        INTEGER NumCurves, CurveNodeNum(*), CurveNodeList(10000,*)
        REAL*8  Posit(3,*)
        CHARACTER*80  filenam
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_Output

      SUBROUTINE CADFIX_GetMeshNodes( pCoords, numNodes )
      POINTER( pCoords, coords(3,1) )
      REAL*8  coords
      INTEGER numNodes
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetMeshNodes
            
      SUBROUTINE CADFIX_GetMeshElements( pTriangles, numTriangles )
      POINTER( pTriangles, triangles(3,1) )
      INTEGER  triangles
      INTEGER numTriangles
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetMeshElements
            
      SUBROUTINE CADFIX_GetSources( numPSources, pPoints,
     &                              numLSources, pLines,
     &                              numTSources, pTriangles,
     &                              bgSpacing,
     &                              Isucc )
      INTEGER numPSources, numLSources, numTSources, Isucc
      REAL*8  bgSpacing
      POINTER( pPoints, points(6,1) ), ( pLines, lines(12,1) )
      POINTER( pTriangles, triangles(18,1) )
      REAL*8 points, lines, triangles
        WRITE(*,*) 'Error --- no CADfix linked'
        STOP
      END SUBROUTINE CADFIX_GetSources
      
#else

      SUBROUTINE CADFIX_Check_Availability( TF )
      IMPLICIT NONE
      LOGICAL TF
      TF = .TRUE.
      RETURN
      END

      SUBROUTINE CADFIX_Check_Status(IERROR,text )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER IERROR
      CHARACTER*(*) text
      CHARACTER*(CFI_MAX_MESLEN) KRESULT
      INTEGER IRESULT
      IF( IERROR.NE.CFI_ERR_OK ) THEN
        PRINT *,'Error ', trim(text)
        CALL CFI_ERROR_STRING (IERROR, KRESULT, IRESULT)
        PRINT *,IERROR, KRESULT
        STOP
      ENDIF
      RETURN
      END
 
      SUBROUTINE Check_Equal( i, j )
      IMPLICIT NONE
      INTEGER  i,j

      IF( i.NE.j ) THEN
        PRINT *,i,' and ',j,' not equal'
        stop
      ENDIF
      RETURN
      END

      SUBROUTINE CADFIX_Init()
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INCLUDE 'cadfix_parent.inc'

      INTEGER      mode, IERROR
      CHARACTER*4  kversion, kfixinfo
      LOGICAL TF

      IERROR = CFI_ERR_OK
      CALL CFI_INIT( mode, kversion, kfixinfo, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_Init.1' )
      READ(kversion,'(F4.1)') CFIVersion
      IF(mode.eq.CFI_MODE_CLIENT)THEN
         TF=.TRUE.
         PRINT *,'CADFIX CLIENT'
      ELSE
         TF = .FALSE.
         PRINT *,'STANDALONE'
      ENDIF
      clientMode = TF
c      CALL CFI_TRACE( "*", 5, CFI_MODE_BOTH, IERROR )
c      CALL CADFIX_Check_Status(IERROR, '_Init.2' )
c      CALL CFI_CHECK( "*", 5, CFI_MODE_BOTH, IERROR )
c      CALL CADFIX_Check_Status(IERROR, '_Init.3' )
      RETURN
      END
      
      SUBROUTINE CADFIX_IsFLITE3DSourceMode( TF )
      LOGICAL TF
      INCLUDE 'cadfix_parent.inc'
      IF(CFIVersion>8.0999)THEN
         TF = .TRUE.
      ELSE
         TF = .FALSE.
      ENDIF
      RETURN
      END
      
      SUBROUTINE CADFIX_IsClientMode( TF )
      LOGICAL TF
      INCLUDE 'cadfix_parent.inc'      
      TF = clientMode
      RETURN
      END
      
      SUBROUTINE CADFIX_Exit()
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  IERROR
      IERROR = CFI_ERR_OK
      CALL CFI_EXIT( IERROR )
      CALL CADFIX_Check_Status(IERROR, '_Exit' )
      RETURN
      END

      SUBROUTINE CADFIX_GetTolerance( tolerance )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'

      INTEGER  IERROR
      REAL*8   bbox(3,2), dm(3), nxp, EPS, tolerance
      PARAMETER ( EPS   = 1.e-06 )

      IERROR = CFI_ERR_OK
      CALL CFI_GET_MODEL_BOX( bbox(1,1), bbox(1,2),
     &                        bbox(2,1), bbox(2,2),
     &                        bbox(3,1), bbox(3,2), IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetTolerance' )
      WRITE(99,*)  'Lower Bounds=',bbox(1,1),bbox(2,1),bbox(3,1)
      WRITE(99,*)  'Upper Bounds=',bbox(1,2),bbox(2,2),bbox(3,2)

      dm(1) = bbox(1,2) - bbox(1,1)
      dm(2) = bbox(2,2) - bbox(2,1)
      dm(3) = bbox(3,2) - bbox(3,1)
      nxp  = DLOG10(DBLE(MAX(dm(1),dm(2),dm(3))))+1
      nxp  = MIN(nxp,2.0D00)
      tolerance = EPS*(10.0**nxp)

      RETURN
      END

      SUBROUTINE CADFIX_GetNumCurves( numCurves )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  numCurves, IERROR

      IERROR = CFI_ERR_OK
      CALL CFI_GET_MODEL_ENTITY_TOTAL( CFI_TYPE_LINE, CFI_SUBTYPE_ALL,
     &                                 numCurves, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetNumCurves' )
      RETURN
      END

      SUBROUTINE CADFIX_GetCurveName( curveNum, entityName )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      CHARACTER*255  entityName
      INTEGER  i, j, CADFIX_GetCurveID, curveNum, curveID, IERROR

      IERROR = CFI_ERR_OK
      curveID = CADFIX_GetCurveID( curveNum )
      CALL CFI_GET_ENTITY_NAME( CFI_TYPE_LINE, curveNum,
     &                          entityName, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetCurveName' )

      DO i = 1, 255
        IF( entityName(i:i).EQ.' ' ) GOTO 100
      ENDDO
 100  CONTINUE
      DO j = i, 255
        entityName(j:j) = ' '
      ENDDO
      RETURN
      END

      SUBROUTINE CADFIX_GetSurfaceName( SurfaceNum, entityName )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      CHARACTER*255  entityName
      INTEGER  i, j, CADFIX_GetSurfaceID, SurfaceNum, SurfaceID, IERROR

      IERROR = CFI_ERR_OK
      SurfaceID = CADFIX_GetSurfaceID( SurfaceNum )
c     --- old code      
c      CALL CFI_GET_ENTITY_NAME( CFI_TYPE_FACE, SurfaceNum,
c     &                          entityName, IERROR )
c     ----- XIE: change SurfaceNum to SurfaceID
      CALL CFI_GET_ENTITY_NAME( CFI_TYPE_FACE, SurfaceID,
     &                          entityName, IERROR )
      CALL CADFIX_Check_Status(IERROR, 
     &     '_GetSurfaceName:: wrong modification here?---XIE' )

      DO i = 1, 255
        IF( entityName(i:i).EQ.' ' ) GOTO 100
      ENDDO
 100  CONTINUE
      DO j = i, 255
        entityName(j:j) = ' '
      ENDDO
      RETURN
      END

      SUBROUTINE CADFIX_GetNumSurfaces( numSurfaces )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  numSurfaces, IERROR

      IERROR = CFI_ERR_OK
      CALL CFI_GET_MODEL_ENTITY_TOTAL( CFI_TYPE_FACE, 
     &                                 CFI_SUBTYPE_ALL,
     &                                 numSurfaces, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetNumSurfaces' )
c      PRINT *,'CADFIX_GetNumSurfaces: ',numSurfaces
      RETURN
      END

      SUBROUTINE CADFIX_GetNumBodies( numBodies )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  numBodies, IERROR
      
      IERROR = CFI_ERR_OK
      CALL CFI_GET_MODEL_ENTITY_TOTAL( CFI_TYPE_BODY,
     &                                 CFI_SUBTYPE_ALL,
     &                                 numBodies, IERROR )
      CALL CADFIX_Check_Status( IERROR, '_GetNumBodies' )
      print *,'CADFIX_GetNumBodies: ',numBodies
      RETURN
      END
      
      FUNCTION CADFIX_GetCurveID( curveNum )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  CADFIX_GetCurveID, curveNum
      INTEGER  curveIDs(10000), magic, IERROR, numReturned

      magic = 0
      IERROR = CFI_ERR_OK
      CALL CFI_GET_MODEL_ENTITY_LIST( CFI_TYPE_LINE, CFI_SUBTYPE_ALL,
     &                                10000, magic, numReturned,
     &                                curveIDs, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetCurveID' )
      CADFIX_GetCurveID = curveIDs( curveNum )
      RETURN
      END

      FUNCTION CADFIX_GetSurfaceID( surfaceNum )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  CADFIX_GetSurfaceID, surfaceNum
      INTEGER  surfaceIDs(10000), magic, IERROR, numReturned

      IERROR = CFI_ERR_OK
      magic = 0
      CALL CFI_GET_MODEL_ENTITY_LIST( CFI_TYPE_FACE, CFI_SUBTYPE_ALL,
     &                                10000, magic, numReturned,
     &                                surfaceIDs, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetSurfaceID' )
      CADFIX_GetSurfaceID = surfaceIDs( surfaceNum )
c      PRINT *,'Surface Num',surfaceNum,'=',CADFIX_GetSurfaceID
      RETURN
      END

      FUNCTION CADFIX_GetBodyID( bodyNum )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  CADFIX_GetBodyID, bodyNum
      INTEGER  bodyIDs(10000), magic, IERROR, numReturned

      IERROR = CFI_ERR_OK
      magic = 0
      CALL CFI_GET_MODEL_ENTITY_LIST( CFI_TYPE_BODY, CFI_SUBTYPE_ALL,
     &                                10000, magic, numReturned,
     &                                bodyIDs, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetBodyID' )
      CADFIX_GetBodyID = bodyIDs( bodyNum )
      PRINT *,'Body Num',bodyNum,'=',CADFIX_GetBodyID
      RETURN
      END
      
      FUNCTION CADFIX_GetNumBodyShells( bodyID )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER CADFIX_GetNumBodyShells, bodyID, IERROR
      
      IERROR = CFI_ERR_OK
      CALL CFI_GET_BODY_SHELL_TOTAL( bodyID, CADFIX_GetNumBodyShells,
     &                               IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetNumBodyShells' )
      print *,'Num Shells for body ',bodyID,' is ',
     &                               CADFIX_GetNumBodyShells
      RETURN
      END
      
      FUNCTION CADFIX_GetNumShellFaces( bodyID, shellNum )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER CADFIX_GetNumShellFaces, bodyID, shellNum
      INTEGER numFaces, IERROR
      
      IERROR = CFI_ERR_OK
      CALL CFI_GET_BODY_SHELL_FACE_TOTAL( bodyID, shellNum, 
     &                                    numFaces, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetNumShellFaces' )
      CADFIX_GetNumShellFaces = numFaces
      RETURN
      END
      
      SUBROUTINE CADFIX_GetShellFaceList( bodyID, shellNum, 
     &                                    pFaceList, numFaces, IERROR )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      POINTER ( pFaceList, faceList(1) )
      INTEGER bodyID, shellNum, numFaces, faceList, IERROR
      INTEGER CADFIX_GetNumShellFaces, magic, numReturned
      
      numFaces = CADFIX_GetNumShellFaces( bodyID, shellNum )
      CALL allc( pFaceList, numFaces,
     &           'faceList in CADFIX_GetShellFaceList' )
      IERROR = CFI_ERR_OK
      magic = 0
      CALL CFI_GET_BODY_SHELL_FACE_LIST( bodyID, shellNum, numFaces,
     &                                   magic, numReturned, faceList,
     &                                   IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetShellFaceList' )
      CALL Check_Equal( numFaces, numReturned )
      RETURN
      END
     
      SUBROUTINE CADFIX_GetBodyFaceList( bodyID, pFaceList, numFaces )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      POINTER( pFaceList, faceList(1) )
      POINTER( pShellFaceList, shellFaceList(1) )
      INTEGER bodyID, faceList, numFaces, numShellFaces, shellFaceList
      
      INTEGER numShells, shellCount, CADFIX_GetNumBodyShells, IERROR
      INTEGER faceCount
      
      numShells = CADFIX_GetNumBodyShells( bodyID )
      numFaces = 0
      DO shellCount = 1, numShells
        CALL CADFIX_GetShellFaceList( bodyID, shellCount, 
     &                                pShellFaceList, numShellFaces,
     &                                IERROR )
        IF( numShellFaces.gt.0 ) THEN
          IF( numFaces.eq.0 ) THEN
            CALL ALLC( pFaceList, numShellFaces,
     &                 'ALLC in CADFIX_GetBodyFaceList' )
          ELSE
            CALL REALLC( pFaceList, numFaces, numFaces + numShellFaces,
     &                   'REALLC in CADFIX_GetBodyFaceList' )
          ENDIF
          DO faceCount = 1, numShellFaces
            faceList(numFaces+faceCount) = shellFaceList(faceCount)
          ENDDO
          CALL deallc( pShellFaceList, 
     &                 'DEALLC in CADFIX_GetBodyFaceList' )
          numFaces = numFaces + numShellFaces
        ENDIF
      ENDDO
      RETURN
      END
      
      SUBROUTINE CADFIX_CalculateCurveFacets( curveNum, numVertices,
     &                                        pVertices, maxEdgeLength )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  CADFIX_GetCurveID, magic
      INTEGER  curveNum, numVertices, IERROR, found
      INTEGER  curveID, numVerticesReturned, i, j
      REAL*8   edgeTolerance, edgeTolerance2
      REAL*8   maxEdgeLength, maxEdgeLength2
      POINTER  (pVertices, vertices(7,1))
      POINTER  (pX, x(1)), (pY, y(1)), (pZ, z(1) )
      REAL*8   vertices, x, y, z, xyzOnLine(3)
      REAL*8   separation, tParam
      REAL*8   dxt, dyt, dzt, dxtt, dytt, dztt
      INTEGER  numLevels, PASS
      REAL*8   cadfixTol

      IERROR = CFI_ERR_OK
      CALL CFI_GET_MODEL_GTOL( cadfixTol, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_CalculateCurveFacets.1' )
      WRITE(5556,*)'CADFIX_CalculateCurveFacets',curveNum,cadfixTol
      edgeTolerance = 1e-4
      curveID = CADFIX_GetCurveID( curveNum )
      WRITE(5556,*)'  Curve ID',curveID
      IERROR = CFI_ERR_OK
      edgeTolerance2 = edgeTolerance
      maxEdgeLength2 = maxEdgeLength
      edgeTolerance2 = 0.1 * maxEdgeLength2
      numLevels = 0
  100  CONTINUE
      CALL CFI_CALC_LINE_FACET_TOTAL( curveID, edgeTolerance2,
     &                                maxEdgeLength2,
     &                                numVertices, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_CalculateCurveFacets.2' )
      WRITE(5556,*)'At level ',numLevels
      WRITE(5556,*)'Max Edge Length = ',maxEdgeLength2
      WRITE(5556,*)'Max Tolerance = ',edgeTolerance2
      WRITE(5556,*)'NumVertices to generate = ', numVertices
      
      IF( numVertices.GE.10000 ) THEN
        maxEdgeLength2 = maxEdgeLength2 * 2.0
        edgeTolerance2 = edgeTolerance2 * 2.0
        numLevels = numLevels + 1
        IF( numLevels.GT.40 )  STOP 'Too many levels'      
        GOTO 100
      ENDIF
      
      IF( numVertices.LE.5 .AND. numLevels.GT.-20) THEN
        maxEdgeLength2 = maxEdgeLength2 / 2.0
        edgeTolerance2 = edgeTolerance2 / 2.0
        numLevels = numLevels -1
        GOTO 100
      ENDIF
      
      IF(numLevels<0) numLevels = 0
      IF(numVertices.LE.2)THEN
         numLevels = 5
      ELSE IF(numVertices.LE.3)THEN
         numLevels = 4
      ELSE IF(numVertices.LE.5)THEN
         numLevels = 3
      ELSE IF(numVertices.LE.9)THEN
         numLevels = 2
      ELSE IF(numVertices.LE.17)THEN
         numLevels = 1
      ENDIF 
      
      CALL allc( pVertices, 2 * 7 * numVertices,
     &           'vertices in CADFIX_CalculateCurveFacets' )
      CALL allc( pX, 2 * numVertices,
     &           'x in CADFIX_CalculateCurveFacets' )
      CALL allc( pY, 2 * numVertices,
     &           'y in CADFIX_CalculateCurveFacets' )
      CALL allc( pZ, 2 * numVertices,
     &           'z in CADFIX_CalculateCurveFacets' )
      IERROR = CFI_ERR_OK
      magic = 0
      CALL CFI_GET_LINE_FACET_VERT_LIST( curveID, numVertices,
     &                                   magic, numVerticesReturned,
     &                                   x, y, z, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_CalculateCurveFacets.3' )
      CALL Check_Equal( numVertices, numVerticesReturned )
      DO i = 1, numVertices
        vertices(1,i) = x(i)
        vertices(2,i) = y(i)
        vertices(3,i) = z(i)
      ENDDO
      CALL deallc( pX, 'x in CADFIX_CalculateCurveFacets' )
      CALL deallc( pY, 'y in CADFIX_CalculateCurveFacets' )
      CALL deallc( pZ, 'z in CADFIX_CalculateCurveFacets' )

      DO i = 1, numVertices
        CALL CFI_CALC_LINE_T_FROM_XYZ( curveID, vertices(1,i),
     &                                 edgeTolerance * 10,
     &                                 found, tParam, xyzOnLine, 
     &                                 separation, IERROR )
        
        CALL CADFIX_Check_Status(IERROR, '_CalculateCurveFacets.4' )
c        CALL Check_Equal( found, CFI_FALSE )
        CALL CFI_CALC_LINE_DERIV_AT_T( curveID, tParam,
     &                                 dxt, dyt, dzt, dxtt, dytt, dztt,
     &                                 IERROR );
        CALL CADFIX_Check_Status(IERROR, '_CalculateCurveFacets.5' )
        vertices(4,i) = dxt
        vertices(5,i) = dyt
        vertices(6,i) = dzt
        vertices(7,i) = tParam
      ENDDO

      IF(numLevels.GT.0)THEN
        j = (numVertices-1) * (2**numLevels)  + 1
        CALL reallc( pVertices,  2 * 7 * j,  'pVertices' )      
      ENDIF
      
      DO PASS = 1, numLevels
        DO i = numVertices, 2, -1
          DO j = 1, 7
            Vertices(j,i*2-1) = vertices(j,i)
          ENDDO
        ENDDO
        numVertices = numVertices * 2 - 1
        DO i = 2, numVertices - 1, 2
          DO j = 1, 7
            Vertices(j,i) = (Vertices(j,i-1) + Vertices(j,i+1)) / 2.0
          ENDDO
        ENDDO        
      END DO
      WRITE(5556,*)'Final Level=',numLevels,
     &             ' NumVertices=',numVertices

      RETURN
      END

#define NEW_LOOPS

#ifdef NEW_LOOPS
      SUBROUTINE CADFIX_GetLoops( plprg, pisrg, totalNumLoops, ptldom )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      POINTER ( plprg, lprg(1) ), (pisrg, isrg(1) )
      POINTER (ptldom, ldom(1) )
      INTEGER lprg, isrg, ldom

      INTEGER  numSurfaces, surfaceID, numLoops, is, IERROR
      INTEGER  totalNumLoops, numCurves, magic, i
      INTEGER  CADFIX_GetSurfaceID
      INTEGER  isrgIndex, isrgSize, il, numCurvesReturned
      INTEGER  isrgSavedIndex
      REAL*8  abs

      COMMON /types/  IFERG, INORM, ISTRU, ITRAN
      INTEGER  IFERG, INORM, ISTRU, ITRAN

c      PRINT *,'CADFIX_GetLoops started'
      IERROR = CFI_ERR_OK
      magic = 0
      CALL CADFIX_GetNumSurfaces( numSurfaces)

c      totalNumLoops = 0
      isrgSize = 0
      DO is = 1, numSurfaces
        surfaceID = CADFIX_GetSurfaceID( is )
        CALL CFI_GET_FACE_LOOP_TOTAL( surfaceID, numLoops, IERROR )
        CALL CADFIX_Check_Status(IERROR, '_GetLoops.1' )
c        PRINT *,'Surface',is,' has ',numLoops,'loops'
cc        totalNumLoops = totalNumLoops + numLoops
        isrgSize = isrgSize + 3
        DO il = 1, numLoops
          CALL CFI_GET_FACE_LOOP_LINE_TOTAL( surfaceID, il,
     &                                       numCurves, IERROR )
          CALL CADFIX_Check_Status(IERROR, '_GetLoops.2' )
c          PRINT *,'Loop ',il,' has ',numCurves,' curves'
          isrgSize = isrgSize + numCurves
        ENDDO
      ENDDO

      totalNumLoops = numSurfaces

      CALL allc( plprg, totalNumLoops + 1, 'lprg in CADFIX_GetLoops' )
      CALL allc( pisrg, isrgSize, 'isrg in CADFIX_GetLoops' )

      PRINT *,'Total Num Loops',totalNumLoops

      CALL allc( ptldom, totalNumLoops, 'ldom in CADFIX_GetLoops' )
      DO is = 1, totalNumLoops
        ldom(is) = 1
      ENDDO

      isrgIndex = 1
      totalNumLoops = 0
      DO is = 1, numSurfaces
        surfaceID = CADFIX_GetSurfaceID( is )
        CALL CFI_GET_FACE_LOOP_TOTAL( surfaceID, numLoops, IERROR )
        CALL CADFIX_Check_Status(IERROR, '_GetLoops.3' )

        totalNumLoops = totalNumLoops + 1

        lprg(totalNumLoops) = isrgIndex
        isrg(isrgIndex + 0) = INORM
        isrg(isrgIndex + 1) = is
        isrg(isrgIndex + 2) = 0
        isrgSavedIndex = isrgIndex + 2
        isrgIndex = isrgIndex + 3

        DO il = 1, numLoops
          CALL CFI_GET_FACE_LOOP_LINE_TOTAL( surfaceID, il,
     &                                       numCurves, IERROR )
          CALL CADFIX_Check_Status(IERROR, '_GetLoops.4' )
          isrg(isrgSavedIndex) = isrg(isrgSavedIndex) + numCurves
c          totalNumLoops = totalNumLoops + 1

          CALL CFI_GET_FACE_LOOP_LINE_LIST( surfaceID, il, numCurves,
     &                                      magic, numCurvesReturned,
     &                                      isrg(isrgIndex), IERROR )
          CALL CADFIX_Check_Status(IERROR, '_GetLoops.5' )
          CALL Check_Equal( numCurves, numCurvesReturned )
          DO i = 1, numCurves
            isrg(isrgIndex + i - 1 ) = ABS( isrg(isrgIndex + i - 1 ) )
          ENDDO
          isrgIndex = isrgIndex + numCurves
        ENDDO
      ENDDO
      lprg(totalNumLoops + 1) = isrgIndex
      IF( isrgIndex.NE.isrgSize + 1 ) THEN
        PRINT *,'ISRG PROBLEM ',isrgIndex, isrgSize
        STOP
      ENDIF
      DO il = 1,totalNumLoops
        isrgIndex = lprg(il)
        PRINT *,'Loop ',il,'Type',isrg(isrgIndex),
     &          'Surf',isrg(isrgIndex + 1)
        PRINT *,'Num Curves',isrg(isrgIndex + 2)
        PRINT *,'Curves',(isrg(isrgIndex + 2 + is),
     &                       is=1,isrg(isrgIndex + 2 ) )
        IF( isrgIndex + 3 + isrg(isrgIndex + 2 ).NE.lprg(il + 1 ) ) THEN
          PRINT *,'Bugger',isrgIndex, lprg(il), lprg(il + 1 )
          STOP
        ENDIF
      ENDDO
      RETURN
      END
#else
      SUBROUTINE CADFIX_GetLoops( plprg, pisrg, totalNumLoops, ptldom )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      POINTER ( plprg, lprg(1) ), (pisrg, isrg(1) )
      POINTER (ptldom, ldom(1) )
      INTEGER lprg, isrg, ldom

      INTEGER  numSurfaces, surfaceID, numLoops, is, IERROR
      INTEGER  totalNumLoops, numCurves, magic, i
      INTEGER  CADFIX_GetSurfaceID
      INTEGER  isrgIndex, isrgSize, il, numCurvesReturned

      COMMON /types/  IFERG, INORM, ISTRU, ITRAN
      INTEGER  IFERG, INORM, ISTRU, ITRAN

c      PRINT *,'CADFIX_GetLoops started'
      IERROR = CFI_ERR_OK
      magic = 0
      CALL CADFIX_GetNumSurfaces( numSurfaces)

      totalNumLoops = 0
      isrgSize = 0
      DO is = 1, numSurfaces
        surfaceID = CADFIX_GetSurfaceID( is )
        CALL CFI_GET_FACE_LOOP_TOTAL( surfaceID, numLoops, IERROR )
        CALL CADFIX_Check_Status(IERROR, '_GetLoops.6' )
c        PRINT *,'Surface',is,' has ',numLoops,'loops'
        totalNumLoops = totalNumLoops + numLoops
        DO il = 1, numLoops
          CALL CFI_GET_FACE_LOOP_LINE_TOTAL( surfaceID, il,
     &                                       numCurves, IERROR )
          CALL CADFIX_Check_Status(IERROR, '_GetLoops.7' )
c          PRINT *,'Loop ',il,' has ',numCurves,' curves'
          isrgSize = isrgSize + 3 + numCurves
        ENDDO
      ENDDO

      CALL allc( plprg, totalNumLoops + 1, 'lprg in CADFIX_GetLoops' )
      CALL allc( pisrg, isrgSize, 'isrg in CADFIX_GetLoops' )

      PRINT *,'Total Num Loops',totalNumLoops

      CALL allc( ptldom, totalNumLoops, 'ldom in CADFIX_GetLoops' )
      DO is = 1, totalNumLoops
        ldom(is) = 1
      ENDDO

      isrgIndex = 1
      totalNumLoops = 0
      DO is = 1, numSurfaces
        surfaceID = CADFIX_GetSurfaceID( is )
        CALL CFI_GET_FACE_LOOP_TOTAL( surfaceID, numLoops, IERROR )
        CALL CADFIX_Check_Status(IERROR, '_GetLoops.8' )

        DO il = 1, numLoops
          CALL CFI_GET_FACE_LOOP_LINE_TOTAL( surfaceID, il,
     &                                       numCurves, IERROR )
          CALL CADFIX_Check_Status(IERROR, '_GetLoops.9' )
          totalNumLoops = totalNumLoops + 1
          lprg(totalNumLoops) = isrgIndex
          isrg(isrgIndex + 0) = INORM
          isrg(isrgIndex + 1) = is
          isrg(isrgIndex + 2) = numCurves
          isrgIndex = isrgIndex + 3

          CALL CFI_GET_FACE_LOOP_LINE_LIST( surfaceID, il, numCurves,
     &                                      magic, numCurvesReturned,
     &                                      isrg(isrgIndex), IERROR )
          CALL CADFIX_Check_Status(IERROR, '_GetLoops.10' )
          CALL Check_Equal( numCurves, numCurvesReturned )
          DO i = 1, numCurves
            isrg(isrgIndex + i - 1 ) = ABS( isrg(isrgIndex + i - 1 ) )
          ENDDO
          isrgIndex = isrgIndex + numCurves
        ENDDO
      ENDDO
      lprg(totalNumLoops + 1) = isrgIndex
      IF( isrgIndex.NE.isrgSize + 1 ) THEN
        PRINT *,'ISRG PROBLEM ',isrgIndex, isrgSize
        STOP
      ENDIF
      DO il = 1,totalNumLoops
        isrgIndex = lprg(il)
        PRINT *,'Loop ',il,'Type',isrg(isrgIndex),
     &          'Surf',isrg(isrgIndex + 1)
        PRINT *,'Num Curves',isrg(isrgIndex + 2)
        PRINT *,'Curves',(isrg(isrgIndex + 2 + is),
     &                       is=1,isrg(isrgIndex + 2 ) )
        IF( isrgIndex + 3 + isrg(isrgIndex + 2 ).NE.lprg(il + 1 ) ) THEN
          PRINT *,'Bugger',isrgIndex, lprg(il), lprg(il + 1 )
          STOP
        ENDIF
      ENDDO
      RETURN
      END
#endif

      SUBROUTINE CADFIX_GetUVFromXYZ( surfaceNum, numPoints, xyz, uv )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'

      INTEGER  surfaceNum, numPoints, CADFIX_GetSurfaceID
      REAL*8   xyz(3,*), uv(2,*)
      REAL*8   uv00(2), uv0(2), uv1(2)
      INTEGER  surfaceID, i, IERROR, closest
      REAL*8  uMin, vMin, uMax, vMax

c      PRINT *,'CADFIX_GetUVFromXYZ started'
      IERROR = CFI_ERR_OK
      surfaceID = CADFIX_GetSurfaceID( surfaceNum )
      
      CALL CADFIX_GetSurfaceUVBox(surfaceNum,uMin,vMin,uMax,vMax)
      uv00(1) = (uMin+uMax) /2.d0
      uv00(2) = (vMin+vMax) /2.d0
      uv0(1)  = uv00(1)
      uv0(2)  = uv00(2)
      DO i = 1, numPoints
        CALL CFI_CALC_FACE_UV_FROM_XYZUV( surfaceID, xyz(1,i), uv0,
     &                                  closest, uv1, IERROR )
        CALL CADFIX_Check_Status(IERROR, '_GetUVFromXYZ' )
c        CALL Check_Equal( closest, CFI_FALSE )
        uv(1,i) = uv1(1)
        uv(2,i) = uv1(2)
        uv0(1)  = 0.9*uv1(1) + 0.1*uv00(1)
        uv0(2)  = 0.9*uv1(2) + 0.1*uv00(2)
      ENDDO
c      DO i = 1, numPoints
c        WRITE(6,'(a,i,5f15.8)'),'Point',i,xyz(1,i),xyz(2,i),xyz(3,i),
c     &                          uv(1,i),uv(2,i)
c      ENDDO
      RETURN
      END

      SUBROUTINE CADFIX_GetXYZFromUV( surfaceNum, numPoints, uv, xyz )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'

      INTEGER  surfaceNum, numPoints, CADFIX_GetSurfaceID
      REAL*8   xyz(3,numPoints), uv(2,numPoints), uvTemp(2)
      INTEGER  surfaceID, i, IERROR

      IERROR = CFI_ERR_OK
      surfaceID = CADFIX_GetSurfaceID( surfaceNum )
      CALL CADFIX_Check_Status(IERROR, '_GetXYZFromUV.1' )
      DO i = 1, numPoints
        uvTemp(1) = uv(1,i)
        uvTemp(2) = uv(2,i)
        CALL CFI_CALC_FACE_XYZ_AT_UV( surfaceID, uvTemp,
     &                                xyz(1,i), IERROR )
        CALL CADFIX_Check_Status(IERROR, '_GetXYZFromUV.2' )
      ENDDO
c      DO i = 1, numPoints
c        WRITE(6,'(a,i,5f15.8)'),'Point',i,uv(1,i),uv(2,i),
c     &                          xyz(1,i),xyz(2,i),xyz(3,i)
c      ENDDO
      RETURN
      END

      SUBROUTINE CADFIX_GetXYZFromUV1( surfaceNum, uv, xyz )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'

      INTEGER  surfaceNum, CADFIX_GetSurfaceID
      REAL*8   xyz(3), uv(2)
      INTEGER  surfaceID, i, IERROR

      IERROR = CFI_ERR_OK
      surfaceID = CADFIX_GetSurfaceID( surfaceNum )
      CALL CADFIX_Check_Status(IERROR, '_GetXYZFromUV1.1' )
      CALL CFI_CALC_FACE_XYZ_AT_UV( surfaceID, uv, xyz, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetXYZFromUV1.2' )
      RETURN
      END

      SUBROUTINE CADFIX_GetUVPointInfo( surfaceNum, u, v, info )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  surfaceNum, surfaceID, IERROR
      INTEGER  CADFIX_GetSurfaceID
      REAL*8   u, v, uv(2), info(18)

      uv(1) = u
      uv(2) = v

      IERROR = CFI_ERR_OK
      surfaceID = CADFIX_GetSurfaceID( surfaceNum )
      CALL CADFIX_Check_Status(IERROR, '_GetUVPointInfo.1' )
      
      CALL CFI_CALC_FACE_XYZ_AT_UV( surfaceID, uv,
     &                              info(1), IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetUVPointInfo.2' )
      CALL CFI_CALC_FACE_DERIV_AT_UV( surfaceID, uv,
     &                  info(4), info(5), info(6),
     &                  info(7), info(8), info(9),
     &                  info(13), info(14), info(15),           !--dxuu, dyuu, dzuu,
     &                  info(10), info(11), info(12),
     &                  info(16), info(17), info(18),           !--dxvv, dyvv, dzvv,
     &                  IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetUVPointInfo.3' )
c       IF( ABS( info(10) ).GE.1e-7 )  STOP
c       IF( ABS( info(11) ).GE.1e-7 )  STOP
c       IF( ABS( info(12) ).GE.1e-7 )  STOP
c      WRITE(6000,'(a,5f18.8)'),'GetUVPointInfo UV',uv(1),uv(2)
c      WRITE(6000,'(a,5f18.8)'),'            X,Y,Z',
c     &                         info(1),info(2),info(3)
c      WRITE(6000,'(a,5f18.8)'),'      DXU,DYU,DZU',
c     &                         info(4),info(5),info(6)
c      WRITE(6000,'(a,5f18.8)'),'      DXV,DYV,DZV',
c     &                         info(7),info(8),info(9)
c      WRITE(6000,'(a,5f18.8)'),'   DXUV,DYUV,DZUV',
c     &                         info(10),info(11),info(12)
      RETURN
      END

      SUBROUTINE CADFIX_GetUVPointInfoAll
     &          ( surfaceNum, u, v, R,Ru,Rv,Ruv,Ruu,Rvv )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  surfaceNum, surfaceID, IERROR
      INTEGER  CADFIX_GetSurfaceID
      REAL*8   u, v, uv(2), R(3), Ru(3),Rv(3),Ruv(3),Ruu(3),Rvv(3)

      uv(1) = u
      uv(2) = v

      IERROR = CFI_ERR_OK
      surfaceID = CADFIX_GetSurfaceID( surfaceNum )
      
      CALL CFI_CALC_FACE_XYZ_AT_UV( surfaceID, uv, R, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetUVPointInfoAll.1' )
      CALL CFI_CALC_FACE_DERIV_AT_UV( surfaceID, uv,
     &                  Ru(1), Ru(2), Ru(3),
     &                  Rv(1), Rv(2), Rv(3),
     &                  Ruu(1), Ruu(2), Ruu(3),
     &                  Ruv(1), Ruv(2), Ruv(3),
     &                  Rvv(1), Rvv(2), Rvv(3),  IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetUVPointInfoAll.2' )
      RETURN
      END

      SUBROUTINE CADFIX_GetUVPointCurv
     &           ( surfaceNum, u, v, ck1, dir1, ck2, dir2 )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  surfaceNum, surfaceID, IERROR
      INTEGER  CADFIX_GetSurfaceID
      REAL*8   u, v, uv(2), ck1,ck2
      REAL*8   dir1(3), dir2(3)
      LOGICAL  whether_spherical

      uv(1) = u
      uv(2) = v

      IERROR = CFI_ERR_OK
      surfaceID = CADFIX_GetSurfaceID( surfaceNum )
      CALL CADFIX_Check_Status(IERROR, '_GetUVPointCurv.1' )
      CALL cfi_calc_face_curv_at_uv (surfaceID, uv,  
     &                      whether_spherical, 
     &                      ck1,  dir1, ck2, dir2, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetUVPointCurv.2' )
      RETURN
      END


      SUBROUTINE CADFIX_LoadGeom( filenam )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      CHARACTER*80  filenam
      INTEGER  IERROR

      IERROR = CFI_ERR_OK
      CALL CFI_FILE_MODEL_OPEN( filenam, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_LoadGeom' )

      RETURN
      END

      SUBROUTINE CADFIX_CloseDatabase()
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  IERROR
      INTEGER  save
      
      print *,'Closing Database'
      IERROR = CFI_ERR_OK
      save = 0
      CALL CFI_FILE_MODEL_CLOSE( save, IERROR )
      CALL CADFIX_Check_Status( IERROR, '-CloseDatabase' )
      RETURN
      END
      
      SUBROUTINE CADFIX_SaveDatabase( filenam )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      CHARACTER*80  filenam
      INTEGER  IERROR

      PRINT *,'Saving database to',filenam
      IERROR = CFI_ERR_OK
      CALL CFI_FILE_MODEL_SAVE( filenam, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_SaveDatabase' )
      RETURN
      END

      SUBROUTINE CADFIX_GetLineTBox( curveNum, tMin, tMax )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'

      INTEGER  curveNum, curveID, IERROR
      INTEGER  CADFIX_GetCurveID
      REAL*8   tMin, tMax

      IERROR = CFI_ERR_OK
      curveID = CADFIX_GetCurveID( curveNum )
      CALL cfi_get_line_t_box( curveID, tMin, tMax, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetLineTBox' ) 

      RETURN
      END     
    
      SUBROUTINE CADFIX_GetLineXYZFromT ( CurveNum, u, XYZ )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  curveNum, curveID, IERROR
      INTEGER  CADFIX_GetCurveID
      REAL*8   u,XYZ(3)

      IERROR = CFI_ERR_OK
      curveID = CADFIX_GetCurveID( curveNum )
      CALL cfi_calc_line_xyz_at_t (curveNum, u, XYZ, IERROR)                         
      CALL CADFIX_Check_Status(IERROR, '_GetLineXYZFromT' )
      RETURN
      END

      SUBROUTINE CADFIX_GetLineCurv ( CurveNum, u, ck )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  curveNum, curveID, IERROR
      INTEGER  CADFIX_GetCurveID
      REAL*8   u,ck
      REAL*8   dir(3), r
      LOGICAL  whether_curved

      IERROR = CFI_ERR_OK
      curveID = CADFIX_GetCurveID( curveNum )
      CALL cfi_calc_line_curv_at_t (curveID, u, ck,  
     &                    whether_curved, dir,  r, IERROR)                         
      CALL CADFIX_Check_Status(IERROR, '_GetLineCurv' )
      RETURN
      END


      SUBROUTINE CADFIX_GetLinePointDeriv( CurveNum, u, Ru,Ruu)
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER  curveNum, curveID, IERROR
      INTEGER  CADFIX_GetCurveID
      REAL*8   u,Ru(3), Ruu(3)

      IERROR = CFI_ERR_OK
      curveID = CADFIX_GetCurveID( curveNum )
      CALL cfi_calc_line_deriv_at_t (curveID, u,  
     &      Ru(1), Ru(2), Ru(3), Ruu(1), Ruu(2), Ruu(3), IERROR)
      CALL CADFIX_Check_Status(IERROR, '_GetLinePointDeriv' )
      RETURN
      END


      SUBROUTINE CADFIX_GetSurfaceUVBox( surfaceNum, uMin, vMin,
     &                                   uMax, vMax )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'

      INTEGER  surfaceNum, surfaceID, IERROR
      INTEGER  CADFIX_GetSurfaceID
      REAL*8   uMin, vMin, uMax, vMax

c      PRINT *,'CADFIX_GetSurfaceUVBox started'
      IERROR = CFI_ERR_OK
      surfaceID = CADFIX_GetSurfaceID( surfaceNum )
      CALL CADFIX_Check_Status(IERROR, '_GetSurfaceUVBox.1' )

      CALL CFI_GET_FACE_UV_BOX( surfaceID, uMin, uMax,
     &                          vMin, vMax, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetSurfaceUVBox.2' ) 

      RETURN
      END     

      SUBROUTINE CADFIX_GetCurveEndPoints( curveNum, endPoints )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'

      INTEGER  curveNum, endPoints(2)

      INTEGER  IERROR, subType, dummy(10)

      IERROR = CFI_ERR_OK
      CALL CFI_GET_GEOM_SUBTYPE( CFI_TYPE_LINE, curveNum,
     &                           subType, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetCurveEndPoints.1' )

      IF( subType.EQ.CFI_SUBTYPE_STRAIGHT ) THEN
        CALL CFI_GET_STRAIGHT_DEFN( curveNum, endPoints(1),
     &                              endPoints(2), IERROR )
      ELSE IF( subType.EQ.CFI_SUBTYPE_ARC ) THEN
        CALL CFI_GET_ARC_DEFN( curveNum, endPoints(1),
     &                         endPoints(2), dummy(1), IERROR )
      ELSE IF( subType.EQ.CFI_SUBTYPE_INTERSECTION ) THEN
        CALL CFI_GET_INTERSECTION_DEFN( curveNum, endPoints(1),
     &                                  endPoints(2), dummy(1),
     &                                  dummy(2), dummy(3), IERROR )
      ELSE IF( subType.EQ.CFI_SUBTYPE_LNURBS ) THEN
        CALL CFI_GET_LNURBS_DEFN( curveNum, endPoints(1),
     &                            endPoints(2), dummy(1), dummy(2),
     &                            dummy(3), dummy(4), dummy(5), IERROR )
      ELSE IF( ( subType.EQ.CFI_SUBTYPE_SET_TRACK )
     &       .OR. ( subType.EQ.CFI_SUBTYPE_SEQ_TRACK ) ) THEN
        CALL CFI_GET_TRACK_DEFN( curveNum, endPoints(1),
     &                           endPoints(2), dummy(1), dummy(2),
     &                           IERROR )
      ELSE IF( subType.EQ.CFI_SUBTYPE_POLYNODE ) THEN
        CALL CFI_GET_POLYNODE_DEFN( curveNum, endPoints(1),
     &                              endPoints(2), dummy(1), dummy(2),
     &                              IERROR )
      ELSE
        STOP 'Invalid Line Type'
      ENDIF
      CALL CADFIX_Check_Status(IERROR, '_GetCurveEndPoints.2' )

      RETURN
      END

      FUNCTION CADFIX_FindPointNum( x, y, z, points )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'

      INTEGER CADFIX_FindPointNum
      REAL*8  x, y, z
      INTEGER  points(2)

      INTEGER IERROR
      REAL*8  xyz1(3), xyz2(3), dxyz(3), dist(3)
      REAL*8  toleranceSq
      PARAMETER ( toleranceSq = 1.d-12 );

      IERROR = CFI_ERR_OK
      CALL CFI_GET_POINT_DEFN( points(1), xyz1, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_FindPointNum.1' )
      CALL CFI_GET_POINT_DEFN( points(2), xyz2, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_FindPointNum.2' )

      dxyz(1) = xyz1(1) - x
      dxyz(2) = xyz1(2) - y
      dxyz(3) = xyz1(3) - z
      dist(1) = dxyz(1)*dxyz(1) + dxyz(2)*dxyz(2) + dxyz(3)*dxyz(3)
      
      dxyz(1) = xyz2(1) - x
      dxyz(2) = xyz2(2) - y
      dxyz(3) = xyz2(3) - z
      dist(2) = dxyz(1)*dxyz(1) + dxyz(2)*dxyz(2) + dxyz(3)*dxyz(3)
      
      dist(3) = x*x + y*y + z*z
      dist(3) = MAX(dist(3), 1.d0) * toleranceSq
      
      IF( dist(1).LT.dist(2) ) THEN
        IF( dist(1).LT.dist(3) ) THEN
          CADFIX_FindPointNum = 1
          RETURN
        ENDIF
      ELSE
        IF( dist(2).LT.dist(3) ) THEN
          CADFIX_FindPointNum = 2
          RETURN
        ENDIF
      ENDIF
      WRITE(*,*)  'Points are    ',xyz1(1), xyz1(2),xyz1(3)
      WRITE(*,*)  '       and    ',xyz2(1), xyz2(2),xyz2(3)
      WRITE(*,*)  'Test point is ',x, y, z
      WRITE(*,*)  'Distances(SQ) ',dist(1), dist(2), dist(3)
      STOP 'Error - No points are close enough'
      RETURN
      END

      SUBROUTINE CADFIX_AddMeshNodes( numNodes, parents,
     &                                types, x, y, z )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INCLUDE 'cadfix_parent.inc'

      INTEGER numNodes, parents(numNodes), types(numNodes)
      REAL*8  x(numNodes), y(numNodes), z(numNodes)

      INTEGER CADFIX_FindPointNum
      INTEGER CADFIX_GetSurfaceID, CADFIX_GetCurveID
      INTEGER  IERROR, firstNum, retFirstNum, i
      INTEGER  endPoints(2), curveNum, pointNum

      DO i = 1, numNodes
        IF( parents(i).LT.0 ) THEN
c Its on an endpoint so we need to find it
          curveNum = CADFIX_GetCurveID( -parents(i) )
          CALL CADFIX_GetCurveEndPoints( curveNum, endPoints )
          WRITE(5557,*)'Finding point for ',i,x(i),y(i),z(i)
          pointNum = CADFIX_FindPointNum( x(i), y(i), z(i),
     &                                    endPoints )
          parents(i) = endPoints(pointNum)
          types(i) = CFI_TYPE_POINT
        ELSE IF( parents(i).GT.PARENT_SURFACE ) THEN
c Its on a surface
          parents(i) = CADFIX_GetSurfaceID( parents(i)
     &                                    - PARENT_SURFACE )
        ELSE IF( parents(i).GT.PARENT_CURVE ) THEN
c Its on a curve
          parents(i) = CADFIX_GetCurveID( parents(i) - PARENT_CURVE )
        ELSE
c Its on a point
        ENDIF
      ENDDO
      IERROR = CFI_ERR_OK
      firstNum = 1
      CALL CFI_CRE_FENODE_MLIST( numNodes, firstNum, x, y, z, types,
     &                           parents, retFirstNum, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AddMeshNodes' )
      CALL Check_Equal( retFirstNum, firstNum )

      RETURN
      END

      SUBROUTINE CADFIX_AddMeshElements( numFaces, faces )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'

      INTEGER numFaces, faces(4,numFaces)

      POINTER (pconns, conns(3,1) )
      INTEGER  conns
      INTEGER  IERROR, firstFace, numPartFaces, i, currentSurface
      INTEGER  numNodesPerElement, nodalOffset, retFirstElementNum
      INTEGER  CADFIX_GetSurfaceID

      CALL allc( pconns, 3 * numFaces,
     &           'conns in CADFIX_AddMeshElements' )
      DO i = 1, numFaces
        conns(1,i) = faces(1,i)
        conns(2,i) = faces(2,i)
        conns(3,i) = faces(3,i)
      ENDDO
      numNodesPerElement = 3
      nodalOffset = 0
      firstFace = 1
      currentSurface = faces(4,1)
      DO i = 1, numFaces
        IF( faces(4,i).NE.currentSurface ) THEN
          print *,'Adding Surface ',currentSurface,firstFace,i-firstFace
          IERROR = CFI_ERR_OK
          CALL CFI_CRE_ELEMENT_MLIST( i - firstFace, CFI_SUBTYPE_TR3,
     &                         CFI_TYPE_FACE,
     &                         CADFIX_GetSurfaceID( currentSurface ),
     &                         firstFace, numNodesPerElement,
     &                         nodalOffset, conns(1,firstFace),
     &                         retFirstElementNum, IERROR )
          CALL CADFIX_Check_Status(IERROR, '_AddMeshElements' )
          CALL Check_Equal( retFirstElementNum, firstFace )
          firstface = i
          currentSurface = faces(4,i)
        ENDIF
      ENDDO
      print *,'Adding Surface ',currentSurface,
     &                          firstFace,i-firstFace-1
      CALL CFI_CRE_ELEMENT_MLIST( i - firstFace, CFI_SUBTYPE_TR3,
     &                       CFI_TYPE_FACE,
     &                       CADFIX_GetSurfaceID( currentSurface ),
     &                       firstFace, numNodesPerElement,
     &                       nodalOffset, conns(1,firstFace),
     &                       retFirstElementNum, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AddMeshElements' )
      CALL Check_Equal( retFirstElementNum, firstFace )
      print *,'At End ',firstFace,numFaces,i-firstFace
      RETURN
      END

      SUBROUTINE CADFIX_AnalyseModel
      IMPLICIT NONE
      INCLUDE 'cfi.prm'

      INTEGER           IERROR, i, j, k
      INTEGER           magic, numReturned
      INTEGER           numTypes, types(10000)
      INTEGER           typePos
      INTEGER           numPoints, pointNums(10000)
      INTEGER           numLines, lineNums(10000)
      INTEGER           numFaces, faceNums(10000)
      INTEGER           numSurfaces, surfaceNums(10000)
      INTEGER           numBodies, bodyNums(10000)
      INTEGER           numSets, setNums(10000)
      INTEGER           numLoops, numLoopLines, loopLines(10000)
      INTEGER           signedGeom, signedGeomPos
      DOUBLE PRECISION  bbox(3,2), xyz(3)
      DOUBLE PRECISION  minU, maxU, minV, maxV
      CHARACTER*255     modelName, entityName, message, message2
      REAL*8  abs

      IERROR = CFI_ERR_OK
      CALL CFI_GET_MODEL_NAME( modelName, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.1' )
      WRITE(99,*)  'Model Name=',trim(modelName)

      CALL CFI_GET_MODEL_BOX( bbox(1,1), bbox(1,2),
     &                        bbox(2,1), bbox(2,2),
     &                        bbox(3,1), bbox(3,2), IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.2' )
      WRITE(99,*)  'Lower Bounds=',bbox(1,1),bbox(2,1),bbox(3,1)
      WRITE(99,*)  'Upper Bounds=',bbox(1,2),bbox(2,2),bbox(3,2)

      CALL CFI_GET_MODEL_TYPE_TOTAL( numTypes, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.3' )
      WRITE(99,*) 'Num Types=',numTypes

      typePos = 0
      CALL CFI_GET_MODEL_TYPE_LIST( 10000, typePos, numReturned,
     &                              types, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.4' )
      CALL Check_Equal( numReturned, numTypes )

      DO i = 1, numTypes
        WRITE(99,*) 'Type ',i,'=',types(i)
      ENDDO
   
      CALL CFI_GET_MODEL_ENTITY_TOTAL( CFI_TYPE_POINT, CFI_SUBTYPE_ALL,
     &                                 numPoints, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.5' )
      WRITE(99,*) 'Num POINT Entities=',numPoints

      CALL CFI_GET_MODEL_ENTITY_TOTAL( CFI_TYPE_LINE, CFI_SUBTYPE_ALL,
     &                                 numLines, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.6' )
      WRITE(99,*) 'Num LINE Entities=',numLines

      CALL CFI_GET_MODEL_ENTITY_TOTAL( CFI_TYPE_FACE, CFI_SUBTYPE_ALL,
     &                                 numFaces, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.7' )
      WRITE(99,*) 'Num FACE Entities=',numFaces

      CALL CFI_GET_MODEL_ENTITY_TOTAL( CFI_TYPE_BODY, CFI_SUBTYPE_ALL,
     &                                 numBodies, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.8' )
      WRITE(99,*) 'Num BODY Entities=',numBodies

      CALL CFI_GET_MODEL_ENTITY_TOTAL( CFI_TYPE_SURFACE,
     &                                 CFI_SUBTYPE_ALL,
     &                                 numSurfaces, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.9' )
      WRITE(99,*) 'Num SURFACE Entities=',numSurfaces

      CALL CFI_GET_MODEL_ENTITY_TOTAL( CFI_TYPE_SET, CFI_SUBTYPE_ALL,
     &                                 numSets, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.10' )
      WRITE(99,*) 'Num SET Entities=',numSets

      magic = 0
      CALL CFI_GET_MODEL_ENTITY_LIST( CFI_TYPE_POINT, CFI_SUBTYPE_ALL,
     &                                10000, magic, numReturned,
     &                                pointNums, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.11' )
      CALL Check_Equal( numPoints, numReturned )
      DO i = 1, numPoints
        CALL CFI_GET_POINT_DEFN( pointNums(i), xyz, IERROR )
        CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.12' )
        WRITE(99,'(a,2i,3(1x,e15.8))') 'Point',i,pointNums(i),
     &                            xyz(1),xyz(2),xyz(3)
      ENDDO

      magic = 0
      CALL CFI_GET_MODEL_ENTITY_LIST( CFI_TYPE_LINE, CFI_SUBTYPE_ALL,
     &                                10000, magic, numReturned,
     &                                lineNums, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.13' )
      CALL Check_Equal( numLines, numReturned )
      DO i = 1, numLines
        CALL CFI_GET_LINE_T_BOX( lineNums( i ), minU, maxU, IERROR )
        CALL CFI_GET_ENTITY_NAME( CFI_TYPE_LINE, lineNums(i),
     &                            entityName, IERROR )
        CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.14' )
        WRITE(99,'(a,2i,1x,a,1x,2f15.8)') 'Line',i,lineNums(i),
     &                            trim(entityName),minU, maxU
      ENDDO

      magic = 0
      CALL CFI_GET_MODEL_ENTITY_LIST( CFI_TYPE_SURFACE, CFI_SUBTYPE_ALL,
     &                                10000, magic, numReturned,
     &                                surfaceNums, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.15' )
      CALL Check_Equal( numSurfaces, numReturned )
      DO i = 1, numSurfaces
!       CALL CFI_GET_PARAMETRIC_UV_BOX( surfaceNums( i ), minU, maxU,
!    &                                  minV, maxV, IERROR )
!       CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.16' )
        CALL CFI_GET_ENTITY_NAME( CFI_TYPE_SURFACE, surfaceNums(i),
     &                            entityName, IERROR )
        CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.17' )
        WRITE(99,'(a,2i,1x,a,4f15.8)') 'Surface',i,surfaceNums(i),
     &                            trim(entityName)
!    &                            entityName,minU, maxU, minV, maxV
      ENDDO

      magic = 0
      CALL CFI_GET_MODEL_ENTITY_LIST( CFI_TYPE_FACE, CFI_SUBTYPE_ALL,
     &                                10000, magic, numReturned,
     &                                faceNums, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_AnalyseModel.18' )
      CALL Check_Equal( numFaces, numReturned )
      DO i = 1, numFaces
        CALL CFI_GET_ENTITY_NAME( CFI_TYPE_FACE, faceNums(i),
     &                            entityName, IERROR )
        WRITE(message,'(a,i5,a,i5,a,a100)')' _AnalyseModel:: face i=',i,
     &     ' id=',faceNums(i),' name=', trim(entityName)
        CALL CADFIX_Check_Status(IERROR, message )
        CALL CFI_GET_FACE_UV_BOX( faceNums( i ), minU, maxU,
     &                            minV, maxV, IERROR )
        message2 = trim(message)//' @CFI_GET_FACE_UV_BOX()'        
        CALL CADFIX_Check_Status(IERROR, message2 )
        CALL CFI_GET_FACE_LOOP_TOTAL( faceNums( i ), numLoops,
     &                               IERROR )
        message2 = trim(message)//' @CFI_GET_FACE_LOOP_TOTAL()'
        CALL CADFIX_Check_Status(IERROR, message2 )
        WRITE(99,'(a,2i,a,4f15.8,i)') 'Face',i,faceNums(i),
     &                            trim(entityName),minU, maxU, minV,
     &                            maxV, numLoops
        CALL CFI_GET_TOPO_EMBED( CFI_TYPE_FACE, faceNums( i ),
     &                           signedGeom, IERROR )
        message2 = trim(message)//' @CFI_GET_TOPO_EMBED()'
        CALL CADFIX_Check_Status(IERROR, message2 )
        signedGeomPos = -99
        DO j = 1, numSurfaces
          IF( surfaceNums( j ).EQ.ABS( signedGeom ) ) THEN
            signedGeomPos = j
          ENDIF
        ENDDO
        WRITE(99,'(a,i,i)') '    Embedded in surface',signedGeom,
     &                      signedGeomPos
        DO j = 1, numLoops
          CALL CFI_GET_FACE_LOOP_LINE_TOTAL( faceNums( i ), j,
     &                                       numLoopLines, IERROR )
          message2 = trim(message)//' @CFI_GET_FACE_LOOP_LINE_TOTAL()'
          CALL CADFIX_Check_Status(IERROR, message2 )
          WRITE(99,'(a,i,i)') '    Loop',j,numLoopLines

          magic = 0
          CALL CFI_GET_FACE_LOOP_LINE_LIST( faceNums( i ), j, 10000,
     &                                      magic, numReturned,
     &                                      loopLines, IERROR )
          message2 = trim(message)//' @CFI_GET_FACE_LOOP_LINE_LIST()'
          CALL CADFIX_Check_Status(IERROR, message2 )
          CALL Check_Equal( numLoopLines, numReturned )
          DO k = 1, numLoopLines
            WRITE(99,'(a,i,i)') '        Line',k,loopLines(k)
          ENDDO
        ENDDO
      ENDDO

      RETURN
      END
      
      SUBROUTINE CADFIX_GetSurfaceNumCurves( surfaceNum, nc)
        IMPLICIT NONE
        INCLUDE 'cfi.prm'
        INTEGER surfaceNum, surfaceID, IERROR, CADFIX_GetSurfaceID
        INTEGER nc, numLoops, il, numCurves
        IERROR = CFI_ERR_OK
        surfaceID = CADFIX_GetSurfaceID( surfaceNum )
        CALL CFI_GET_FACE_LOOP_TOTAL (surfaceID, numLoops, IERROR) 
        CALL CADFIX_Check_Status(IERROR, '_GetSurfaceNumCurves.1' ) 
        nc = 0
        DO il = 1, numLoops
          CALL CFI_GET_FACE_LOOP_LINE_TOTAL( surfaceID, il, 
     &                                       numCurves, IERROR )
          CALL CADFIX_Check_Status(IERROR, '_GetSurfaceNumCurves.2' )
          nc = nc + numCurves
        ENDDO
        RETURN
      END SUBROUTINE CADFIX_GetSurfaceNumCurves

      SUBROUTINE CADFIX_GetSurfaceTopoType( surfaceNum, Itopo)
        IMPLICIT NONE
        INCLUDE 'cfi.prm'
        INTEGER surfaceNum, Itopo
        INTEGER surfaceID, IERROR, CADFIX_GetSurfaceID
        
C  Argument declarations and descriptions
C     Input
      INTEGER IENTITY_TYPE   ! Entity type to be queried
      INTEGER IENTITY_NUMBER ! Entity number to be queried
      INTEGER ILIST_SIZE     ! Size of list to be returned
C     Input/Output
      INTEGER IMAGIC        ! 0 on entry: return first entities in list
C     .                       0 on exit: last batch of entities in list 
C     Output
      INTEGER ISINGLE_ELEMENT_VARIANT ! Distinguishes between elements..
C     .                                 ...of the same subtype
      INTEGER IMESH_FLIP_FLAG         ! Indicates orientation of...
C     .                                 ...element wrt parent
      INTEGER INUMBER_RETURNED         ! Number of eltys returned
      INTEGER ILIST_OF_ELEMENT_TYPES(10000) ! List of types
      
        INTEGER magic
        
        IERROR = CFI_ERR_OK
        surfaceID = CADFIX_GetSurfaceID( surfaceNum )
        magic = 0
        CALL CFI_GET_TOPO_ELTY_LIST(CFI_TYPE_LINE, CFI_SUBTYPE_ALL,
     &     10000, magic, ISINGLE_ELEMENT_VARIANT, IMESH_FLIP_FLAG,
     &     INUMBER_RETURNED, ILIST_OF_ELEMENT_TYPES, IERROR)
        CALL CADFIX_Check_Status(IERROR, '_GetSurfaceTopoType.1' )
        Itopo = ILIST_OF_ELEMENT_TYPES(surfaceID)
        RETURN
      END SUBROUTINE CADFIX_GetSurfaceTopoType
      
      

C  Returns a list of element types on a given TOPO entity, together with
C  the element variant and mesh flip flag. The IMESH_FLIP_FLAG controls
C  the cyclic order in which CADfix will generate elements on this part.



      SUBROUTINE CADFIX_GetSurfaceCurves( surfaceNum, nc, List )
        IMPLICIT NONE
        INCLUDE 'cfi.prm'
        INTEGER surfaceNum, surfaceID, IERROR, CADFIX_GetSurfaceID
        INTEGER List(*)
        INTEGER nc, numLoops, il, numCurves, isrg(1000), magic
        INTEGER numCurvesReturned
        
        IERROR = CFI_ERR_OK
        surfaceID = CADFIX_GetSurfaceID( surfaceNum )
        CALL CFI_GET_FACE_LOOP_TOTAL( surfaceID, numLoops, IERROR )
        CALL CADFIX_Check_Status(IERROR, '_GetSurfaceCurves.1' )

        nc = 0
        magic = 0
        DO il = 1, numLoops
          CALL CFI_GET_FACE_LOOP_LINE_TOTAL( surfaceID, il,   
     &                                       numCurves, IERROR )
          CALL CADFIX_Check_Status(IERROR, '_GetSurfaceCurves.2' )

          CALL CFI_GET_FACE_LOOP_LINE_LIST( surfaceID, il, numCurves,   
     &                                      magic, numCurvesReturned,   
     &                                      isrg, IERROR )
          CALL CADFIX_Check_Status(IERROR, '_GetSurfaceCurves.3' )
          CALL Check_Equal( numCurves, numCurvesReturned )
          List(nc+1 : nc+numCurves ) = ABS( isrg(1:numCurves) )
          nc = nc + numCurves
        ENDDO

        RETURN
      END SUBROUTINE CADFIX_GetSurfaceCurves


      SUBROUTINE CADFIX_Output(filenam, NB_Point, NB_Tri, IP_Tri,
     &                Posit, NumCurves, CurveNodeNum, CurveNodeList)
        IMPLICIT NONE      
        INCLUDE 'cfi.prm'
        INCLUDE 'cadfix_parent.inc'
      
        INTEGER NB_Point, NB_Tri, IP_Tri(4,*)
        INTEGER NumCurves, CurveNodeNum(*), CurveNodeList(10000,*)
        REAL*8  Posit(3,*)
        CHARACTER*80  filenam, newFilename
        INTEGER  is, i, ip, ie
        POINTER ( pcadparent, cadparent(1) )
        POINTER ( pcadtype, cadtype(1) )
        POINTER ( pcadx, cadx(1) ), ( pcady, cady(1) )
        POINTER ( pcadz, cadz(1) )
        INTEGER  cadparent, cadtype
        REAL*8   cadx, cady, cadz
      
        print *,'Num Points = ',NB_Point
        print *,'Num Triangles = ',NB_Tri
        CALL allc( pcadparent, NB_Point, 'cadparent in outst' )
        CALL allc( pcadtype, NB_Point, 'cadtype in outst' )
        CALL allc( pcadx, NB_Point * 2, 'cadz in outst' )
        CALL allc( pcady, NB_Point * 2, 'cady in outst' )
        CALL allc( pcadz, NB_Point * 2, 'cadz in outst' )
        DO ip = 1, NB_Point
           cadparent(ip) = 0
           cadx(ip) = Posit(1,ip)
           cady(ip) = Posit(2,ip)
           cadz(ip) = Posit(3,ip)
        ENDDO
      
        CALL CADFIX_AddMeshElements( NB_Tri, IP_Tri )
      
        ! *** nodes on curves
      
        DO is = 1, NumCurves
           DO i = 1, CurveNodeNum(is)
              ip = CurveNodeList(is,i)
              IF( cadparent(ip)/=0 ) CYCLE
              IF(  i==1 .OR. i==CurveNodeNum(is)  ) THEN
                 cadparent(ip) = -is
              ELSE
                 cadparent(ip) = is + PARENT_CURVE
              ENDIF
              cadtype(ip) = CFI_TYPE_LINE
           ENDDO
        ENDDO
      
        ! *** nodes inside regions
      
        DO ie = 1, NB_Tri
           is = IP_Tri(4,ie)
           DO i = 1,3
              ip = IP_Tri(i,ie)
              IF( cadparent(ip)/=0 ) CYCLE
              cadparent(ip) = is + PARENT_SURFACE
              cadtype(ip) = CFI_TYPE_FACE
           ENDDO
        ENDDO
      
        CALL CADFIX_AddMeshNodes    
     &       ( NB_Point, cadparent, cadtype, cadx, cady, cadz )
        i = LEN( TRIM(filenam) )
        newFilename = filenam(1:i)//'_backup'
        CALL CADFIX_SaveDatabase( newFilename )
        CALL CADFIX_CloseDatabase()
      
        CALL deallc( pcadparent, 'cadparent in outst' )
        CALL deallc( pcadtype, 'cadtype in outst' )
        CALL deallc( pcadx, 'cadx in outst' )
        CALL deallc( pcady, 'cady in outst' )
        CALL deallc( pcadz, 'cadz in outst' )
      
        RETURN
      END SUBROUTINE CADFIX_Output
      
      SUBROUTINE CADFIX_GetMeshNodes( pCoords, numNodes )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER numNodes, IERROR, magic, numReturned
      POINTER( pCoords, coords(3,1) ), ( pX, x(1) )
      POINTER( pY, y(1) ), ( pZ, z(1) ), ( pNodeList, nodeList(1) )
      POINTER( pParentTypes, parentTypes(1) )
      POINTER( pParentList, parentList(1) )
      REAL*8  coords, x, y, z
      INTEGER  nodeList, parentTypes, parentList, i, nodeNum
      
      IERROR = CFI_ERR_OK
      CALL CFI_GET_MODEL_ENTITY_TOTAL( CFI_TYPE_NODE, CFI_SUBTYPE_ALL,
     &                                 numNodes, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetMeshNodes' )
      CALL ALLC( pNodeList, numNodes, 'ALLC NodeList _GetMeshNodes' )
      CALL ALLC( pParentTypes, numNodes, 
     &           'ALLC TypeList _GetMeshNodes' )
      CALL ALLC( pParentList, numNodes, 
     &           'ALLC ParentList _GetMeshNodes' )
      CALL ALLC( pX, numNodes * 2, 'ALLC X _GetMeshNodes' )
      CALL ALLC( pY, numNodes * 2, 'ALLC Y _GetMeshNodes' )
      CALL ALLC( pZ, numNodes * 2, 'ALLC Z _GetMeshNodes' )
      
      magic = 0
      CALL CFI_GET_TOPO_FENODE_MLIST( CFI_TYPE_BODY, 1, CFI_IMPLICIT,
     &                                numNodes, magic, numReturned,
     &                                nodeList, x, y, z, 
     &                                parentTypes, parentList, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetMeshNodes_2' )
      CALL Check_Equal( numNodes, numReturned )
           
      CALL ALLC( pCoords, 3 * numNodes * 2, 'ALLC _GetMeshNodes' )
      DO i = 1, numNodes
        nodeNum = nodeList(i)
c        write (199,'(i8,3f20.7,i8)') i,x(i),y(i),z(i),nodeList(i)
        coords(1,nodeNum) = x(i)
        coords(2,nodeNum) = y(i)
        coords(3,nodeNum) = z(i)
      ENDDO
      
      RETURN
      END
      
      SUBROUTINE CADFIX_GetMeshElements( pTriangles, numTriangles )
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      INTEGER numTriangles, IERROR, magic, numReturned, numNodesRet
      POINTER( pTriangles, triangles(4,1) )
      POINTER( pFTriangles, fTriangles(3,1) )
      POINTER( pFTriangleList, fTriangleList(1) )
      POINTER( pTriangleList, triangleList(1) )
      POINTER( pFaceList, faceList(1) )
      INTEGER  triangles, fTriangles, fTriangleList, faceList, numFaces
      INTEGER  triangleList, absFaceNum
      INTEGER  i, faceNum, triNum, numFTriangles, faceCount
      
      IERROR = CFI_ERR_OK
      CALL CFI_GET_MODEL_ENTITY_TOTAL( CFI_TYPE_ELEMENT, 
     &                                 CFI_SUBTYPE_TR3,
     &                                 numTriangles, IERROR )
      CALL CADFIX_Check_Status(IERROR, '_GetMeshElements' )
      CALL ALLC( pFTriangleList, numTriangles,
     &           'ALLC FTriangleList _GetMeshElements' )
      CALL ALLC( pTriangleList, numTriangles,
     &           'ALLC TriangleList _GetMeshElements' )
      CALL ALLC( pTriangles, numTriangles * 4, 
     &           'ALLC Triangles _GetMeshElements' )
      CALL ALLC( pFTriangles, numTriangles * 3, 
     &           'ALLC FTriangles _GetMeshElements' )
      
      DO i = 1, numTriangles
        triangleList(i) = 0
      ENDDO
      CALL CADFIX_GetBodyFaceList( 1, pFaceList, numFaces )
      
      DO faceCount = 1, numFaces
        write( 399,'(i8,i8)') faceCount,faceList(faceCount)
      ENDDO
      DO faceCount = 1, numFaces
        faceNum = faceList(faceCount)
        absFaceNum = ABS( faceNum )
        magic = 0
        CALL CFI_GET_TOPO_ELEMENT_MLIST( CFI_TYPE_FACE, 
     &                                   absFaceNum,
     &                                   CFI_SUBTYPE_TR3,
     &                                   numTriangles, 3, magic,
     &                                   numFTriangles, numNodesRet,
     &                                   fTriangleList, fTriangles,
     &                                   IERROR )
        CALL CADFIX_Check_Status(IERROR, '_GetMeshElements_2' )
        CALL Check_Equal( numNodesRet, 3 )
           
        DO i = 1, numFTriangles
          triNum = fTriangleList(i)
          IF( faceNum.lt.0 ) THEN
            triangles(1,triNum) = fTriangles(1,i)
            triangles(2,triNum) = fTriangles(3,i)
            triangles(3,triNum) = fTriangles(2,i)
          ELSE
            triangles(1,triNum) = fTriangles(1,i)
            triangles(2,triNum) = fTriangles(2,i)
            triangles(3,triNum) = fTriangles(3,i)
          ENDIF
          triangles(4,triNum) = faceNum
        ENDDO
      ENDDO

c      DO i = 1, numTriangles
c        write (299,'(i8,3i8,i8)') i,triangles(1,i),
c     &                            triangles(2,i), triangles(3,i),
c     &                            triangles(4,i)
c      ENDDO      
      RETURN
      END
      
      SUBROUTINE CADFIX_GetSources( numPSources, pPoints,
     &                              numLSources, pLines,
     &                              numTSources, pTriangles,
     &                              bgSpacing,
     &                              Isucc)
      IMPLICIT NONE
      INCLUDE 'cfi.prm'
      
      INTEGER numPSources, numLSources, numTSources
      INTEGER numSources
      INTEGER Isucc
      REAL*8  bgSpacing, anisotropy(3), spacing(3)
      REAL*8  radius1(3), radius2(3)
      REAL*8  x(3), y(3), z(3)
      POINTER( pParts, parts(1) ), ( pSubTypes, subTypes(1) )
      POINTER( pPoints, points(6,1) ), ( pLines, lines(12,1) )
      POINTER( pTriangles, triangles(18,1) )
      REAL*8 points, lines, triangles
      INTEGER IERROR, magic, parts, subTypes, srcCount
      INTEGER pCount, lCount, tCount, numPartsReturned
      INTEGER sourceAllFlag, sourceSetNum, domainAllFlag, domainSetNum
      LOGICAL TF
      
      ISucc  = 0
      CALL CADFIX_IsFLITE3DSourceMode( TF )
      IF(.NOT. TF) RETURN

      IERROR = CFI_ERR_OK
      CALL CFI_GET_FSRC_FIELD_DEFN( sourceAllFlag, sourceSetNum,
     &                              domainAllFlag, domainSetNum,
     &                              bgSpacing, IERROR )
      IF(IERROR == CFI_ERR_NO_FLITE3D_FIELD) RETURN
      CALL CADFIX_Check_Status( IERROR, '_GetSources_0' )
      CALL CFI_GET_FSRC_TOTAL( CFI_SUBTYPE_PSRC, numPSources, IERROR )
      CALL CADFIX_Check_Status( IERROR, '_GetSources_1' )
      CALL CFI_GET_FSRC_TOTAL( CFI_SUBTYPE_LSRC, numLSources, IERROR )
      CALL CADFIX_Check_Status( IERROR, '_GetSources_2' )
      CALL CFI_GET_FSRC_TOTAL( CFI_SUBTYPE_TSRC, numTSources, IERROR )
      CALL CADFIX_Check_Status( IERROR, '_GetSources_3' )
      
      CALL ALLC( pPoints, numPSources * 2 * 6, 'pPoints in GetSources' )
      CALL ALLC( pLines, numLSources * 2 * 12, 'pLines in GETSources' )
      CALL ALLC( pTriangles, numTSources * 2 * 18,
     &           'pTris in GetSources' )
      numSources = numPSources + numLSources + numTSources
      magic = 0
      call ALLC( pParts, numSources, 'Parts in GetSources' )
      call ALLC( pSubTypes, numSources, 'SubTypes in GetSources' )
      CALL CFI_GET_FSRC_LIST( CFI_SUBTYPE_ALL, numSources,
     &                        magic, numPartsReturned,
     &                        subTypes, parts, IERROR )
      CALL CADFIX_Check_Status( IERROR, '_GetSources_4' )
      CALL Check_Equal( numSources, numPartsReturned )
     
      pCount = 1
      lCount = 1
      tCount = 1
      DO srcCount = 1, numSources
        CALL CFI_GET_FSRC_DEFN( parts(srcCount), subTypes(srcCount),
     &                          x, y, z, spacing, radius1, radius2,
     &                          anisotropy, IERROR )
        IF( subTypes(srcCount).eq.CFI_SUBTYPE_PSRC ) THEN
          points(1,pCount) = x(1)
          points(2,pCount) = y(1)
          points(3,pCount) = z(1)
          points(4,pCount) = spacing(1)
          points(5,pCount) = radius1(1)
          points(6,pCount) = radius2(1)
          pCount = pCount + 1
        ELSE IF( subTypes(srcCount).eq.CFI_SUBTYPE_LSRC ) THEN
          lines(1,lCount) = x(1)
          lines(2,lCount) = y(1)
          lines(3,lCount) = z(1)
          lines(4,lCount) = spacing(1)
          lines(5,lCount) = radius1(1)
          lines(6,lCount) = radius2(1)
          lines(7,lCount) = x(2)
          lines(8,lCount) = y(2)
          lines(9,lCount) = z(2)
          lines(10,lCount) = spacing(2)
          lines(11,lCount) = radius1(2)
          lines(12,lCount) = radius2(2)
          lCount = lCount + 1
        ELSE IF( subTypes(srcCount).eq.CFI_SUBTYPE_TSRC ) THEN
          triangles(1,tCount) = x(1)
          triangles(2,tCount) = y(1)
          triangles(3,tCount) = z(1)
          triangles(4,tCount) = spacing(1)
          triangles(5,tCount) = radius1(1)
          triangles(6,tCount) = radius2(1)
          triangles(7,tCount) = x(2)
          triangles(8,tCount) = y(2)
          triangles(9,tCount) = z(2)
          triangles(10,tCount) = spacing(2)
          triangles(11,tCount) = radius1(2)
          triangles(12,tCount) = radius2(2)
          triangles(13,tCount) = x(3)
          triangles(14,tCount) = y(3)
          triangles(15,tCount) = z(3)
          triangles(16,tCount) = spacing(3)
          triangles(17,tCount) = radius1(3)
          triangles(18,tCount) = radius2(3)
          tCount = tCount + 1
        ELSE
          STOP 'Wrong Source Type'
        ENDIF
      ENDDO
      
      ISucc  = 1
      
      RETURN
      END
      
#endif
