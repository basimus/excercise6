[SwanSim]: http://www.swansim.org

Introduction
============

The FLITE Mesh Generation suite of tools are part of the [SwanSim] initiative.
They enable the automatic generation of high-quality
unstructured 3D surface and volume meshes suitable for computational simulation. The FLITE mesh
generators have been adopted by such companies as BAE Systems, iHPC Singapore and Airbus -
where it was heavily used in the wing design process for the A380 and subsequent aircraft.
The FLITE software also has a long history of being used in groundbreaking projects such as the
land-speed record attempts - [Thrust SSC] and [Bloodhound SSC].

[Thrust SSC]: http://www.thrustssc.com/
[Bloodhound SSC]: http://www.bloodhoundssc.com/

The suite comprises two main tools:

- Surface Mesh Generator
- Volume Mesh Generator

Learning Resources
==================

- General information and product documentation is available at the [SwanSim] homepage.

Reporting Bugs
==============

If you have found a bug then please open an issue in the [SwanSim Issue Tracker].

[SwanSim Issue Tracker]: http://git.swansim.org/swansim-public/flite-mesh-generators/issues

Requirements
============

In general, FLITE tries to be as portable as possible. It uses [CMake] for building and gets
regularly tested on the following platforms:

- Windows 10 (64 bit)
  * Visual Studio Community 2013 with Intel Fortran Compilers 2015
  * Visual Studio Community 2015 with Intel Fortran Compilers 2017
  * [MinGW-64] compilers
- Linux (CentOS 7 - 64 bit)
  * GCC and GFortran

[CMake]: https://cmake.org/
[MinGW-64]: https://mingw-w64.org/doku.php/download/mingw-builds

Contributing
============

This is a new open-source initiative so we are currently working on the guidelines for
contributing to this project. In the meantime, please contact [Jason Jones].

[Jason Jones]: mailto:j.w.jones@swansea.ac.uk

License
=======

FLITE is distributed under the [GPLv3 License].
See [Copyright.txt] for details.

[GPLv3 License]: https://www.gnu.org/licenses/gpl.html
[Copyright.txt]: http://git.swansim.org/swansim-public/flite-mesh-generators/blob/master/Copyright.txt

