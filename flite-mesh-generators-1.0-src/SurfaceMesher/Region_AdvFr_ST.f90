!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*-------------------------------------------------------------*
!>
!!   Generates points and triangulates by advance front method.  
!<
!*-------------------------------------------------------------*
SUBROUTINE RegionType_trian2(RegionID, BGSpacing, Tri3D,  ipc, inext  )
  USE SpacingStorage
  USE SurfaceMeshStorage
  USE Geometry3DAll
  USE array_allocator
  USE control_Parameters
  IMPLICIT NONE
  INTEGER,             INTENT(in)    :: RegionID
  TYPE(SpacingStorageType),     INTENT(in)    :: BGSpacing
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D  
  INTEGER, INTENT(in)  ::  inext(*), ipc(*)

  INTEGER, PARAMETER :: NEPLT =  2000      ! * Elements per output
  INTEGER, PARAMETER :: NCAND =   200      ! * Candidates to Point to add
  INTEGER, PARAMETER :: NPTAD =  1000      ! * Mesh Points to add
  INTEGER, PARAMETER :: NSFAD =   500      ! * Front Sides to add

  POINTER ( ptwle ,    wle(1) )
  POINTER ( ptlhl ,    lhl(1) ), ( ptlhk ,    lhk(1) )
  POINTER ( ptipfr,   ipfr(1) ), ( ptiqfr,   iqfr(1) )
  POINTER ( ptnreg,   nreg(1) ), ( ptnchk,   nchk(1) )
  POINTER ( ptnear,   near(1) ), ( ptner1,   ner1(1) )
  POINTER ( pthowf,   howf(1) ), ( pthwf1,   hwf1(1) )
  INTEGER :: nreg, ipfr, iqfr, lhl, lhk
  INTEGER :: nchk, near, ner1     
  REAL*8  :: hwf1, wle, howf,  ang3d, vnxm,vnym, xs,xe,ys,ye,dist1,dist2
  INTEGER :: ks(2,2)   
  REAL*8  :: r1(3), r2(3), r3(3), rm(3), ru(3), rv(3), vs(3), vl(3) , vr(3)
  REAL*8  :: Rp(3), Rpu(3), Rpv(3), Rpuv(3), cr(3),du1,du2,xmp0,ymp0,DGUESS
  REAL*8  :: fmap(3,3),distm, dist,fac,fac0,faco,vnx0,vny0,vtx0,vty0,fmin
  LOGICAL :: valid
  INTEGER :: nit, nsfr, mlfrs, mlpn1, mlcan
  INTEGER :: is, kn1, kn, i1, i2, npfr, kcant, nsd, it, inum, ken, nd2
  INTEGER :: i, j, k, l, kp, kp1, kount, knear, in
  INTEGER :: IUTOP, IVTOP,iter,nit_md,ik
  REAL*8  :: EPS, EPS1, EPS2, EPS3, UBOT, VBOT, UTOP, VTOP, BIG, SMALL, TLIM
  REAL*8  :: xhg, hlng, alngt, dsiz, alen, alen1, dl2, a, b, c, dlast, len
  REAL*8  :: xn1, yn1, xn, yn, xmp, ymp, xdi, ydi, vnx, vny, xp, yp, alf, bet
  REAL*8  :: u1, u2, f1, f2, xtemp, ytemp, xken, yken, dst, xin1, yin1
  REAL*8  :: b11, b12, b21, b22, det, uin1, uin2, tinc, dline, radius, h1, distf
  REAL*8  :: ang1, angl, angi, wfar, d1, d2, di, d12, comp,scale,diff
  REAL*8  :: dist_mp,dist_mp2,drast,EPS4,f3, relax, rsiz, u10, u20, sp(3),spm
  LOGICAL :: VarySpace
  INTEGER :: numProject

  numProject = 0
  CALL GetRegionUVBox(RegionID, UBOT, VBOT, UTOP, VTOP)
  CALL GetRegionUVmax(RegionID, IUTOP, IVTOP)

  nit   = MAX(1500,IUTOP,IVTOP)
  nit_md= MAX(150,IUTOP,IVTOP)

 ! write(399,*) 'Region:',RegionID, Curvature_Type
 ! write(377,*) 'Region:',RegionID, Curvature_Type
 ! write(366,*) 'Region:',RegionID, Curvature_Type
 ! write(287,*) 'Region:',RegionID, Curvature_Type


 ! write(399,*)UBOT, VBOT, UTOP, VTOP,IUTOP, IVTOP
  ! *** parameters and tolerances for the N-R iteration

  varySpace = .TRUE.
  IF(BGSpacing%BasicSize / BGSpacing%MinSize > 1.02) varySpace = .TRUE.
 !print *,'VarySpace: ',varyspace

  EPS   = 1.e-14
  EPS1  = 1.e-03
  EPS2  = 1.e-04
  EPS3  = 1.e-06
  EPS4  = 0.04
  BIG   =  1.e+38
  TLIM  = min(1.,0.25*(UTOP-UBOT),0.25*(VTOP-VBOT))
  TLIM  = min(0.005,0.15*(UTOP-UBOT),0.15*(VTOP-VBOT))
  DGUESS=min(.15*(UTOP-UBOT),.15*(VTOP-VBOT))

  ! *** parameters and tolerances for the N-R iteration

  ! *** initialize.

  nsfr = Tri3D%NB_Point
  Tri3D%NB_Tri = 0

  ! *** Memory allocation

  CALL allc( ptipfr,   Tri3D%NB_Point, 'ipfr')
  CALL allc( ptiqfr,   Tri3D%NB_Point, 'ipfr')
  CALL allc( ptwle , 2*Tri3D%NB_Point, 'wle  in trian2' )
  CALL allc( ptlhl ,   Tri3D%NB_Point, 'lhl  in trian2' )
  CALL allc( ptlhk ,   Tri3D%NB_Point, 'lhk  in trian2' )
  CALL allc( ptnreg,   Tri3D%NB_Point, 'nreg in trian2' )
  CALL allc( ptnchk,   Tri3D%NB_Point, 'nchk in trian2' )
  CALL allc( pthowf, 2*NCAND,          'howf in trian2' )
  CALL allc( pthwf1, 2*NCAND,          'hwf1 in trian2' )
  CALL allc( ptnear,   NCAND,          'near in trian2' )
  CALL allc( ptner1,   NCAND,          'ner1 in trian2' )
  mlfrs = Tri3D%NB_Point
  mlpn1 = Tri3D%NB_Point
  mlcan = NCAND

  ipfr(1:Tri3D%NB_Point) = ipc(1:Tri3D%NB_Point)
  iqfr(1:Tri3D%NB_Point) = ipc(inext(1:Tri3D%NB_Point))


  ! *** orders the sides of the front according to the length
  !     1.- computes the length

  !write(398,*)
  !write(398,'(i8,3E15.6)') nsfr
  DO is=1,nsfr
     kn1     = ipfr(is)
     kn      = iqfr(is)
     wle(is) = Geo3D_Distance_SQ(Tri3D%Posit(:,kn), Tri3D%Posit(:,kn1)) 

     cr(:) = 0.5*(Tri3D%Posit(:,kn)+Tri3D%Posit(:,kn1)) 
     CALL SpacingStorage_GetMappingGridSize(BGSpacing,cr(1:3),fMap)
     CALL SpacingStorage_GetMinGridSize(BGSpacing, cr(1:3),spm)

     !sp(1) = sqrt(fMap(1,1)**2+fMap(1,2)**2+fMap(1,3)**2)
     !sp(2) = sqrt(fMap(2,1)**2+fMap(2,2)**2+fMap(2,3)**2)
     !sp(3) = sqrt(fMap(3,1)**2+fMap(3,2)**2+fMap(3,3)**2)
     !spm   = min (sp(1),sp(2),sp(3))
     dist_mp = Mapping3D_Distance_SQ(Tri3D%Posit(:,kn),Tri3D%Posit(:,kn1),fMap)*spm**2
     write(398,'(2i8,4E15.6)') kn1,kn,sqrt(dist_mp),sqrt(wle(is)),spm
     wle(is) = dist_mp
     !write(398,'(3E15.6)') (Tri3D%Posit(ik,kn1),ik=1,3)
  ENDDO

  ! *** 2.- forms the heap

  CALL formhp( nsfr, wle, lhl, lhk )
  !write(398,*) nsfr
  !write(398,*) (lhl(is),is= 1,nsfr)

  ! *** this is the start of a step of the advancing front method.
! JWJ Start
!  if( RegionID.eq.1 ) then
!    write(1000,*) 'Testing'
!    xmp0=4.852914729483058
!    ymp0=14.172693437089128
!    CALL GetUVPointInfo1(RegionID,xmp0,ymp0,Rp,Rpu,Rpv,Rpuv)
!  endif
! JWJ End
  Loop_nsfr : DO WHILE(nsfr>0)

     ! *** Updates the list of active nodes

     xhg = BIG
     nchk(1:Tri3D%NB_Point) = 0
     npfr = 0
     DO is = 1,nsfr
        i1 = ipfr(is)
        i2 = iqfr(is)
        IF( nchk(i1)  ==  0 ) THEN
           npfr       = npfr+1
           nreg(npfr) = i1
           nchk(i1)   = 1
        ENDIF
        hlng = (Tri3D%Coord(1,i2)-Tri3D%Coord(1,i1))**2   &
             + (Tri3D%Coord(2,i2)-Tri3D%Coord(2,i1))**2
        xhg  = MIN(xhg,hlng)
     ENDDO
     xhg = SQRT(xhg)
     !write(900,*)' xhg ',xhg

     ! *** come to this point to try another side (kcant>1)
     !     kcant is a counter of the attemps to generate a triangle

     Loop_kcant : DO kcant = 1, nsfr

        IF(varySpace) THEN
           nsd   = lhl(kcant)
        ELSE
           nsd = nsfr
        END IF
        !write(399,*) 'nsd:', nsd,xhg
        kn1   = ipfr(nsd)
        kn    = iqfr(nsd)
        xn1   = Tri3D%Coord(1,kn1)
        yn1   = Tri3D%Coord(2,kn1)
        xn    = Tri3D%Coord(1,kn)
        yn    = Tri3D%Coord(2,kn)
        len   = sqrt((xn1-xn)**2+(yn1-yn)**2)
        xmp   = 0.5*(xn+xn1)
        ymp   = 0.5*(yn+yn1)
        cr(:) = 0.5*(Tri3D%Posit(:,kn)+Tri3D%Posit(:,kn1)) 
        
        CALL SpacingStorage_GetMappingGridSize(BGSpacing,cr(1:3),fMap)

        ! *** Newton-Raphson iteration to find the "real" position of the ideal point.

        ! *** coordinates of the end points of the side in the 3D
        !     normalised space where the spacing is 1.

        r1(:) = Mapping3D_Posit_Transf(Tri3D%Posit(:,kn1), fMap, 1) 
        r2(:) = Mapping3D_Posit_Transf(Tri3D%Posit(:,kn ), fMap, 1) 

        !write(399,*) ' Side:', nsd
        !write(399,*) kn1, kn
        !write(399,'(4E14.5)')xn1,yn1,xn,yn
        !write(399,'(4E14.5)')xmp,ymp
        rm(:) = 0.5*(r1(:)+r2(:))
        vs(:) = r2(:)-r1(:)

        alngt = SQRT(vs(1)**2+vs(2)**2+vs(3)**2)
        dsiz  = MIN(   1.d+00, 2.00d+00*alngt )
        dsiz  = MAX( dsiz, 0.55d+00*alngt )
        dsiz=  max(.90,min(dsiz,1.1))

        ! *** initial guess (u1,u2)

        xdi   = xn-xn1
        ydi   = yn-yn1
        alen1 = SQRT(xdi*xdi+ydi*ydi)
        d12   = 1./alen1
        vnx   = -ydi*d12
        vny   =  xdi*d12
        tinc  = xhg
        du1   = vnx
        du2   = vny
!
! compute distance from real mid-point to guessed one
!
        xmp0=xmp
        ymp0=ymp
        CALL GetUVPointInfo1(RegionID,xmp0,ymp0,Rp,Rpu,Rpv,Rpuv)
        numProject = numProject + 1
        r3(:) = Mapping3D_Posit_Transf(rp(1:3),  fMap, 1)
        dist_mp=sqrt((rm(1)-r3(1))**2+(rm(2)-r3(2))**2+(rm(3)-r3(3))**2)
!     !write(399,*)'MP:',r3
!     !write(399,*)dist_mp
        if(dist_mp.gt.EPS1) then

!       tlimMP = 0.2*alen1
!       do itrial = 1, ntrialMP
!        tlimMP = tlimMP/2.
!
! newton-raphson iteration for computing better guess for mid-point
!
          tinc=min(tinc,DGUESS)
          xmp=xmp+du1*tinc !start from mid point
          ymp=ymp+du2*tinc !start from mid point
          relax=.25
          do it=1,nit_md
            iter = it
            xmp    = max( xmp, UBOT )
            xmp    = min( xmp, UTOP )
            ymp    = max( ymp, VBOT )
            ymp    = min( ymp, VTOP )
            CALL GetUVPointInfo1(RegionID,xmp,ymp,Rp,Rpu,Rpv,Rpuv)
            numProject = numProject + 1
!     !write(399,*)'MP:',r3
            r3(:) = Mapping3D_Posit_Transf(rp(1:3),  fMap, 1)
            ru(:) = Mapping3D_Posit_Transf(rpu(1:3),  fMap, 1)
            rv(:) = Mapping3D_Posit_Transf(rpv(1:3),  fMap, 1)
       ! alf=sqrt(ru(1)**2+ru(2)**2+ru(3)**2)
       ! bet=sqrt(rv(1)**2+rv(2)**2+rv(3)**2)
            alf=1.
            bet=1.
            vl(:) = r3(:)-r1(:)
            vr(:) = r3(:)-r2(:)
            rsiz=sqrt((r2(1)-r1(1))**2+(r2(2)-r1(2))**2+(r2(3)-r1(3))**2)
            dlast = vl(1)**2+vl(2)**2+vl(3)**2
            f1    = dlast-.25*rsiz**2
            drast=vr(1)**2+vr(2)**2+vr(3)**2
!     !write(399,*)'distMP:',sqrt(dlast),sqrt(drast),rsiz
            f3    =drast-.25*rsiz**2
            dlast = .5*(sqrt(dlast)+sqrt(drast))
!        if(abs(f1).lt.f1min.and.abs(f3).lt.f3min) then
!
! discard iterated xmp,ymp when distance r3-rm > dist0
!

!          dist_mp2=sqrt((rm(1)-r3(1))**2+(rm(2)-r3(2))**2+(rm(3)-r3(3))**2)
!          if(dist_mp2.lt.dist_mp) then
!
!            f1min = abs(f1)
!            f3min = abs(f3)
!            dist_mp    = dist_mp2
!          end if
!        end if
            if(abs(f1).lt.EPS3.and.abs(f3).lt.EPS3) then
              Exit
            endif
            b11   = 2.*bet*(vl(1)*ru(1)+vl(2)*ru(2)+vl(3)*ru(3))
            b12   = 2.*alf*(vl(1)*rv(1)+vl(2)*rv(2)+vl(3)*rv(3))
            b21   = 2.*bet*(vr(1)*ru(1)+vr(2)*ru(2)+vr(3)*ru(3))
            b22   = 2.*alf*(vr(1)*rv(1)+vr(2)*rv(2)+vr(3)*rv(3))
            det   = b11*b22-b12*b21
            if(abs(det).lt.EPS) then
              Exit
            endif
            det   = 1./det
            xin1  = -det*( b22*f1-b12*f3)*alf
            yin1  = -det*(-b21*f1+b11*f3)*bet
            tinc  = sqrt(xin1**2+yin1**2)
            if(tinc.lt.EPS3) then
              Exit
            endif
            xin1=xin1/tinc
            yin1=yin1/tinc
            tinc  = min(tinc,relax)
            xmp=xmp+xin1*tinc
            ymp=ymp+yin1*tinc
            xmp    = max( xmp, UBOT )
            xmp    = min( xmp, UTOP )
            ymp    = max( ymp, VBOT )
            ymp    = min( ymp, VTOP )
           end do

           dist_mp2=sqrt((rm(1)-r3(1))**2+(rm(2)-r3(2))**2+(rm(3)-r3(3))**2)
           if(dist_mp2.gt.dist_mp) then
!
!  better guess for mid point discarded
!
             xmp=xmp0
             ymp=ymp0
           endif

      endif
      !write(399,*)'Mid Point:', kn1,kn
      !write(399,*)   it,xmp,ymp

      CALL GetUVPointInfo1(RegionID,xmp,ymp,Rp,Rpu,Rpv,Rpuv)
      numProject = numProject + 1
      alf=sqrt(rpu(1)**2+rpu(2)**2+rpu(3)**2+eps1)
      bet=sqrt(rpv(1)**2+rpv(2)**2+rpv(3)**2+eps1)
      alf  = 1
      bet  = 1

      xdi   = xn-xn1
      ydi   = yn-yn1
      alen1 = SQRT(xdi*xdi+ydi*ydi)
      d12   = 1./alen1
      vnx   = -ydi*d12
      vny   =  xdi*d12
      tinc  = xhg
      du1   = vnx
      du2   = vny
      tlim = min(DGUESS,tinc)
      tinc =min(tinc,DGUESS)
      u1=xmp+du1*tinc
      u2=ymp+du2*tinc
      u1    = max( u1, UBOT )
      u1    = min( u1, UTOP )
      u2    = max( u2, VBOT )
      u2    = min( u2, VTOP )


      f1     = BIG
      f2     = BIG

      a = vnx 
      b = vny 
      c = (-xdi*yn1+ydi*xn1)*d12

      u10 = BIG
      u20 = BIG
      Loop_it : DO it=1,nit
    !   write(399,*) it,u1,u2
        CALL GetUVPointInfo1(RegionID,u1,u2,Rp,Rpu,Rpv,Rpuv)
        numProject = numProject + 1

        r3(:) = Mapping3D_Posit_Transf(rp(1:3),  fMap, 1) 
        ru(:) = Mapping3D_Posit_Transf(rpu(1:3), fMap, 1) 
        rv(:) = Mapping3D_Posit_Transf(rpv(1:3), fMap, 1) 
    !   write(399,'(a,3E14.7)') ' r3:',r3
        vl(:) = r3(:)-r1(:)
        vr(:) = r3(:) - r2(:)
        dlast = vl(1)**2+vl(2)**2+vl(3)**2
        f1    = dlast-dsiz**2
        f2    = (r3(1)-rm(1))*vs(1)+  (r3(2)-rm(2))*vs(2)+  (r3(3)-rm(3))*vs(3)
        drast=vr(1)**2+vr(2)**2+vr(3)**2
        f3    =drast-dsiz**2
    !   write(399,'(a,3E14.7)') ' fs:',f1,f2,f3
        dlast = .5*(sqrt(dlast)+sqrt(drast))
        if(abs(f1).lt.EPS3*dsiz**2.and.abs(f3).lt.EPS3*dsiz**2) EXIT Loop_it

        b11   = 2.*(vl(1)*ru(1)+vl(2)*ru(2)+vl(3)*ru(3))
        b12   = 2.*(vl(1)*rv(1)+vl(2)*rv(2)+vl(3)*rv(3))
        b21   = 2.*(vr(1)*ru(1)+vr(2)*ru(2)+vr(3)*ru(3))
        b22   = 2.*(vr(1)*rv(1)+vr(2)*rv(2)+vr(3)*rv(3))
     !  b21   = ru(1)*vs(1)+ru(2)*vs(2)+ru(3)*vs(3)
     !  b22   = rv(1)*vs(1)+rv(2)*vs(2)+rv(3)*vs(3)
        det   = b11*b22-b12*b21
        IF(ABS(det) < EPS) EXIT Loop_it

        det   = 1./det
        uin1  = -det*( b22*f1-b12*f3)
        uin2  = -det*(-b21*f1+b11*f3)
        tinc  = SQRT(uin1**2+uin2**2)
        IF(tinc < EPS3**2) EXIT Loop_it

        ! *** if required, limits the length of the timestep to TLIM 

    !   write(399,'(a,3E14.7)') 'increment:',uin1,uin2,tinc
        uin1  = uin1/tinc
        uin2  = uin2/tinc
        tinc  = MIN(tinc,TLIM,1.)
        u1    = u1+uin1*tinc
        u2    = u2+uin2*tinc
        dline = a*u1+b*u2+c
        IF(dline <= 0.) THEN
           u1 = u1-2.0*dline*a
           u2 = u2-2.0*dline*b
        ENDIF
        u1    = MAX(u1,UBOT)
        u1    = MIN(u1,UTOP)
        u2    = MAX(u2,VBOT)
        u2    = MIN(u2,VTOP)
        if(abs(u10-u1).lt.1.d-16.and.abs(u20-u2).lt.1.d-16) Exit
        u10=u1
        u20=u2
      ENDDO Loop_it
      !write(399,*)'Ideal Point:', kn1,kn
      !write(399,*)   it,u1,u2
      !write(399,*)   rp(1),rp(2),rp(3)

      xtemp = u1
      ytemp = u2

        ! *** loops over possible nodes and finds the closest neighbours

        inum   = 0
        radius = dsiz
        IF(ABS(dlast-dsiz) < dsiz) THEN
           h1    = 0.8*radius
        ELSE
           h1    = 2.0*radius
           r3(:) = rm(:)
        ENDIF
        h1 = h1*h1
        DO  kp=1,npfr
           ken   = nreg(kp)
           IF( ken  ==  kn  .OR.  ken  ==  kn1 ) CYCLE
           xken  = Tri3D%Coord(1,ken)
           yken  = Tri3D%Coord(2,ken)
           IF( a*xken+b*yken+c  <=  0.0 ) CYCLE
           rm(:) = Mapping3D_Posit_Transf(Tri3D%Posit(:,ken),  fMap, 1) 
           distf = (rm(1)-r3(1))**2+(rm(2)-r3(2))**2+(rm(3)-r3(3))**2
           IF( distf  >  h1 ) CYCLE

           inum  = inum+1
           IF( inum  >  mlcan ) THEN
              nd2   = mlcan+NCAND
              CALL reallc( pthowf, 2*nd2, 'howf in trian2' )
              CALL reallc( pthwf1, 2*nd2, 'hwf1 in trian2' )
              CALL reallc( ptner1,   nd2, 'ner1 in trian2' )
              CALL reallc( ptnear,   nd2, 'near in trian2' )
              mlcan = nd2 
           ENDIF
          ! if(RegionID.eq.110) then
             !write(399,*) inum,distf,ken
          ! end if
           hwf1(inum) = distf
           ner1(inum) = ken
        ENDDO

        ! *** decide which of the nodes is chosen

        IF(inum == 0) THEN
           dst = a*xtemp+b*ytemp+c
           IF( dst  >=   0.00001*alen1 ) THEN
              inum       = inum+1
              near(inum) = 0
              howf(inum) = 0.0
           ENDIF
        ELSE
           ! *** orders them

           DO i=1,inum
              comp = 1.e+6
              DO j=1,inum
                 IF( ner1(j)  ==     0 ) CYCLE
                 IF( hwf1(j)  <=  comp ) THEN
                    is   = j
                    comp = hwf1(j)
                 ENDIF
              ENDDO
              near(i)  = ner1(is)
              howf(i)  = hwf1(is)
              ner1(is) = 0
           ENDDO

           ! *** adds the "ideal" point to the list.

           dst = a*xtemp+b*ytemp+c
           IF( dst  >=  0.001*alen1 ) THEN
              inum = inum+1
              IF( inum  >  mlcan ) THEN
                 nd2   = mlcan+NCAND
                 CALL reallc( pthwf1, 2*nd2, ' hwf1 in trian2' )
                 CALL reallc( ptner1,   nd2, ' ner1 in trian2' )
                 CALL reallc( ptnear,   nd2, ' near in trian2' )
                 mlcan = nd2 
              ENDIF
              near(inum) = 0
              howf(inum) = 0.0
           ENDIF
        ENDIF
        if(RegionID.eq.110) then
          !write(399,*) 
          !write(399,*) inum
          do i = 1 , inum
           !write(399,*) i,near(i),howf(i)
          end do
        end if
        !write(399,*) 

        ! *** selects starting by the closest

        DO i=1,inum
           kp = near(i)
           IF(kp == 0) THEN
              xp = xtemp
              yp = ytemp
           ELSE
              xp = Tri3D%Coord(1,kp)
              yp = Tri3D%Coord(2,kp)
           ENDIF

           ! *** sees if this connection is possible

           CALL possib( kn1, kn, kp, xn1, yn1, xn, yn, xp, yp,    & 
                nsfr, npfr, ipfr, iqfr, nreg, Tri3D%Coord, valid)
           if(RegionID.eq.110) then
             !write(399,*) i,xp,yp,valid
           end if
           IF(valid) EXIT Loop_kcant
        ENDDO

        ! *** if the previous strategy does not succeed, then
        !     find the existing nodes that give maximum angle

        !write(399,*)' checking angles' 
        inum = 0
        ang1 = 0.0
        Loop_1900 : DO kp=1,npfr
           ken  = nreg(kp)
           IF( ken  ==  kn  .OR.  ken  ==  kn1 ) CYCLE Loop_1900
           xken = Tri3D%Coord(1,ken)
           yken = Tri3D%Coord(2,ken)
           IF( a*xken+b*yken+c  <=  0.0 ) CYCLE Loop_1900

           ! *** see if this connection is possible

           CALL possib( kn1, kn, ken, xn1, yn1, xn, yn, xken, yken,    & 
                nsfr, npfr, ipfr, iqfr, nreg, Tri3D%Coord, valid)
           if(RegionID.eq.110) then
             !write(399,*) i,xken,yken,valid
           end if
           IF( .NOT. valid ) CYCLE Loop_1900
           rm(:) = Mapping3D_Posit_Transf(Tri3D%Posit(:,ken),  fMap, 1)
           angl  = ang3d(r2,rm,r1)
           IF( angl  <  ang1 ) CYCLE Loop_1900
           IF( inum+1  >  mlcan ) THEN
              nd2   = mlcan+NCAND
              CALL reallc( pthowf, 2*nd2, 'howf in trian2' )
              CALL reallc( pthwf1, 2*nd2, 'hwf1 in trian2' )
              CALL reallc( ptner1,   nd2, 'ner1 in trian2' )
              CALL reallc( ptnear,   nd2, 'near in trian2' )
              mlcan = nd2 
           ENDIF
           howf(inum+1) = angl
           near(inum+1) = ken

           DO  i=1,inum
              k    = i
              angi = howf(i)
              IF( angi  >  angl ) CYCLE
              DO j=1,inum-k
                 l         = inum-j
                 near(l+1) = near(l)
                 howf(l+1) = howf(l)
              ENDDO
              near(k) = ken
              howf(k) = angl
              EXIT
           ENDDO

           inum = inum+1
        ENDDO Loop_1900

       ! if(RegionID.eq.110) then
          !write(399,*) 
          !write(399,*) inum
        !  do i = 1 , inum
           !write(399,*) i,near(i),howf(i)
        !  end do
       ! end if
        !write(399,*) 

        ! *** selects starting by the closest

        IF( inum  /=  0 ) THEN 
           wfar = BIG 
           DO i=1,inum
              kp   = near(i)
              angi = howf(i)
              xp   = Tri3D%Coord(1,kp)
              yp   = Tri3D%Coord(2,kp)

              ! *** checks now the size

              rm(:) = Mapping3D_Posit_Transf(Tri3D%Posit(:,kp),  fMap, 1)
              d1    = (rm(1)-r1(1))**2+(rm(2)-r1(2))**2+(rm(3)-r1(3))**2
              d2    = (rm(1)-r2(1))**2+(rm(2)-r2(2))**2+(rm(3)-r2(3))**2
              di    = MAX(d1,d2)
              IF(di < wfar) THEN
                 kp1  = kp
                 wfar = di
              ENDIF
           ENDDO
       ! if(RegionID.eq.110) then
          !write(399,*) 
         ! write(399,*) kp1,xp,yp
       ! end if
           kp = kp1
           xp = Tri3D%Coord(1,kp)
           yp = Tri3D%Coord(2,kp)
           EXIT Loop_kcant
        ENDIF

        ! *** if the number of attempts is greater than 10 or
        !     all the sides in the front have been tried -> stop

        IF( kcant >= 40  .OR.  kcant == nsfr ) THEN
           WRITE(29,*) 'ST-trian2::cannot find the connectivity', kcant ,  & 
                ' unsuccessful attempts'
           CALL Error_Stop ('RegionType_trian2 :: ')
        ENDIF

        ! *** try another side

     ENDDO Loop_kcant


     ! *** nothing wrong with it

     kount = 0
     knear = kp
     IF( knear  ==  0 ) THEN 
        Tri3D%NB_Point = Tri3D%NB_Point+1
        IF(Tri3D%NB_Point>SIZE(Tri3D%Coord,2))THEN
           CALL allc_2Dpointer(Tri3D%Coord, 2, Tri3D%NB_Point+Surf_allc_Increase)
        ENDIF
        IF(Tri3D%NB_Point>SIZE(Tri3D%Posit,2))THEN
           CALL allc_2Dpointer(Tri3D%Posit, 3, Tri3D%NB_Point+Surf_allc_Increase)
        ENDIF
        IF( Tri3D%NB_Point  >  mlpn1 ) THEN 
           nd2 = mlpn1+NPTAD
           CALL reallc( ptnchk, nd2, 'nchk in trian2' )
           mlpn1 = nd2 
        ENDIF
        Tri3D%Coord(1,Tri3D%NB_Point) = xp
        Tri3D%Coord(2,Tri3D%NB_Point) = yp
        CALL GetUVPointInfo0(RegionID,xp,yp,cr(1:3))
        numProject = numProject + 1
        Tri3D%Posit(:,Tri3D%NB_Point)  = cr(:)
        knear        = Tri3D%NB_Point
        !write(399,*)'node created', Tri3D%NB_Point
     ENDIF

     ! *** forms element

     Tri3D%NB_Tri = Tri3D%NB_Tri+1
     IF(Tri3D%NB_Tri>SIZE(Tri3D%IP_Tri,2))THEN
         CALL allc_2Dpointer( Tri3D%IP_Tri,  5,Tri3D%NB_Tri + Surf_allc_increase )
     ENDIF
     Tri3D%IP_Tri(1,Tri3D%NB_Tri) = kn1
     Tri3D%IP_Tri(2,Tri3D%NB_Tri) = kn
     Tri3D%IP_Tri(3,Tri3D%NB_Tri) = knear
      !write(399,*) 'element created:',kn1,kn,knear

     IF( Mod(Tri3D%NB_Tri,NEPLT)==0) THEN
        WRITE(*,'(15x,3(a,i9))')    & 
             'NB_Tri = ',Tri3D%NB_Tri,' NB_Point = ',Tri3D%NB_Point,' nsfr = ',nsfr
        WRITE(29,'(15x,3(a,i9))')    & 
             'NB_Tri = ',Tri3D%NB_Tri,' NB_Point = ',Tri3D%NB_Point,' nsfr = ',nsfr
     ENDIF

     ! *** Updates the front

     ks(1,1)   = kn
     ks(2,1)   = knear
     ks(1,2)   = knear
     ks(2,2)   = kn1
     ipfr(nsd) = ipfr(nsfr)     ! deletes the current side
     iqfr(nsd) = iqfr(nsfr)
     nsfr      = nsfr-1
     IF(varySpace) THEN
        CALL dehsi( nsd, nsfr, lhl, lhk, wle )
     END IF

     !                                ! deletes or includes the others
     DO in = 1,2
        i1 = ks(1,in)
        i2 = ks(2,in)
        nsd = 0
        DO is = 1,nsfr 
           IF( ipfr(is)  ==  i1  .AND.  iqfr(is)  ==  i2 ) THEN 
              nsd = is
              EXIT
           ENDIF
        ENDDO

        IF( nsd  ==  0 ) THEN
           nsfr = nsfr+1
           IF( nsfr  >  mlfrs) THEN
              nd2   = mlfrs+NSFAD
              CALL reallc( ptipfr, nd2, 'ipfr in trian2' )
              CALL reallc( ptiqfr, nd2, 'iqfr in trian2' )
              CALL reallc( ptnreg, nd2, 'nreg in trian2' )
              CALL reallc(  ptwle, 2*nd2, ' wle in trian2' )
              CALL reallc(  ptlhl, nd2, ' lhl in trian2' )
              CALL reallc(  ptlhk, nd2, ' lhk in trian2' )
              mlfrs = nd2 
           ENDIF
           ipfr(nsfr) = i2 
           iqfr(nsfr) = i1
           IF(varySpace) THEN
              alen = Geo3D_Distance_SQ(Tri3D%Posit(:,i2),Tri3D%Posit(:,i1))
              CALL adhsi(alen, nsfr, lhl, lhk, wle)
           END IF
        ELSE
           ipfr(nsd) = ipfr(nsfr)
           iqfr(nsd) = iqfr(nsfr)
           nsfr      = nsfr-1
           IF(varySpace) THEN
              CALL dehsi( nsd, nsfr, lhl, lhk, wle )
           END IF
        ENDIF
     ENDDO

     ! *** Continues the generation process if the front is not empty 

  ENDDO Loop_nsfr

  WRITE(*,'(15x,3(a,i9))')    & 
       'Tri3D%NB_Tri = ',Tri3D%NB_Tri,' nodes = ',Tri3D%NB_Point,' nsfr = ',nsfr
  WRITE(29,'(15x,3(a,i9))')    & 
       'Tri3D%NB_Tri = ',Tri3D%NB_Tri,' nodes = ',Tri3D%NB_Point,' nsfr = ',nsfr


  CALL deallc( ptwle , 'wle  in trian2' )
  CALL deallc( ptlhl , 'lhl  in trian2' )
  CALL deallc( ptlhk ,  'lhk  in trian2' )
  print *,'  Number of projections', numProject

  RETURN
END SUBROUTINE RegionType_trian2


!*---------------------------------------------------------------------*
!*    [ang3d] computes, in 3D, the angle w defined by the points of    *
!*    coordinates r1, r2 and r3                                        *
!*---------------------------------------------------------------------*
REAL*8 FUNCTION ang3d(r1,r2,r3)									
  IMPLICIT NONE
  REAL*8, PARAMETER :: pi2 = 6.2831853071796d0
  REAL*8, PARAMETER :: tol = 1.d-09

  REAL*8  :: r1(*)  ,r2(*)  ,r3(*)
  REAL*8  :: cs1(3) ,cs2(3) ,cn(3)
  REAL*8  :: c1, c2, aw, cw, cosw, sinw, an

  !                            1
  !                           * 
  !                          /
  !                         / w
  !                        *------*
  !                       2         3

  cs1(1:3)   = r3(1:3)-r2(1:3)
  cs2(1:3)   = r1(1:3)-r2(1:3)
  c1       = SQRT(cs1(1)**2+cs1(2)**2+cs1(3)**2)
  c2       = SQRT(cs2(1)**2+cs2(2)**2+cs2(3)**2)
  IF(c1<1.e-10) CALL Error_Stop ('ST-ang3d::points 3 and 2 coincide  ') 
  IF(c2<1.e-10) CALL Error_Stop ('ST-ang3d::points 1 and 2 coincide  ') 
  aw       = 1./(c1*c2)
  cn(1)    = cs1(2)*cs2(3)-cs1(3)*cs2(2)  
  cn(2)    = cs1(3)*cs2(1)-cs1(1)*cs2(3)  
  cn(3)    = cs1(1)*cs2(2)-cs1(2)*cs2(1)  
  cw       = SQRT(cn(1)**2+cn(2)**2+cn(3)**2)
  cosw     = aw*(cs1(1)*cs2(1)+cs1(2)*cs2(2)+cs1(3)*cs2(3))
  sinw     = aw*cw
  an       = ATAN2(sinw,cosw)
  IF(an<-tol) an = pi2+an
  ang3d    = an

  RETURN
END FUNCTION ang3d

!*-----------------------------------------------------------------------*
!*    [possib] finds out whether connection whith point kp is possible   *
!*-----------------------------------------------------------------------*
SUBROUTINE possib(kn1   ,kn    ,kp    ,xn1   ,yn1   ,xn    ,    & 
     yn    ,xp    ,yp    ,nonf  ,nonr  ,ipfr  ,    & 
     iqfr  ,nregi ,coor  ,valid)

  IMPLICIT REAL*8(a-h,o-z)

  INTEGER  :: nregi(*)  ,ipfr(*)  ,iqfr(*)
  REAL*8   ::   coor(2,*)
  LOGICAL  :: valid

  deter(p1,q1,p2,q2,p3,q3) = (p2-p1)*(q3-q1)-(p3-p1)*(q2-q1)

  valid = .TRUE.

  ! *** box-test

  xmn = MIN(xn1,xn,xp)
  xmx = MAX(xn1,xn,xp)
  ymn = MIN(yn1,yn,yp)
  ymx = MAX(yn1,yn,yp)

  ! *** check if the front nodes are interior

  area2 = deter(xn1,yn1,xn,yn,xp,yp)
  IF(area2>=1.e-12) THEN
     asm = -1.d-4 * area2
     DO it=1,nonr
        kj   = nregi(it)
        IF(kj==kn1 .OR. kj==kn .OR. kj==kp) CYCLE
        xt   = coor(1,kj)
        yt   = coor(2,kj)
        IF(xt<xmn .OR. xt>xmx ) CYCLE
        IF(yt<ymn .OR. yt>ymx ) CYCLE
        a1    = deter(xt ,yt ,xn,yn,xp,yp)
        IF(a1<asm) CYCLE
        a2    = deter(xn1,yn1,xt,yt,xp,yp)
        IF(a2<asm) CYCLE
        a3    = area2-a1-a2
        IF(a3<asm) CYCLE
        valid = .FALSE.
        !write(399,*) 'Wrong area:'
        !write(399,*) a1,a2,a3,area2
        RETURN
     ENDDO
  ENDIF

  ! *** equation of the mid-base : kp line

  xmb  = 0.5*(xn1+xn)
  ymb  = 0.5*(yn1+yn)
  as   = ymb-yp
  bs   = xp-xmb
  cs   = (xmb-xp)*ymb+(yp-ymb)*xmb

  ! *** loop over the front sides : check for intersection

  !write(399,*) 'nonf in possible', nonf
  DO ir=1,nonf
     knt1 = ipfr(ir)
     knt  = iqfr(ir)
     !write(399,*) knt1,knt
     IF(knt1==0) CYCLE
     IF(knt1==kn1 .AND. knt==kn) CYCLE
     IF(knt1==kp  .OR.  knt==kp) CYCLE
     xnt1 = coor(1,knt1)
     ynt1 = coor(2,knt1)
     xnt  = coor(1,knt)
     ynt  = coor(2,knt)
     !write(399,*) xnt1,xnt
     !write(399,*) ynt1,ynt
     !write(399,*) xmx,xmn
     !write(399,*) ymx,ymn
     IF(MIN(xnt1,xnt)>xmx) CYCLE
     IF(MAX(xnt1,xnt)<xmn) CYCLE
     IF(MIN(ynt1,ynt)>ymx) CYCLE
     IF(MAX(ynt1,ynt)<ymn) CYCLE

     at   = ynt1-ynt
     bt   = xnt-xnt1
     ct   = (xnt1-xnt)*ynt1+(ynt-ynt1)*xnt1
     s1   = at*xmb+bt*ymb+ct
     s2   = at*xp+bt*yp+ct
     !write(399,*) '1+2',s1,s2
     IF(s1>0  .AND.  s2>0) CYCLE
     IF(s1<0  .AND.  s2<0) CYCLE
     s3   = as*xnt1+bs*ynt1+cs
     s4   = as*xnt+bs*ynt+cs
     !write(399,*) '3+4',s3,s4
     IF(s3>0  .AND.  s4>0) CYCLE
     IF(s3<0  .AND.  s4<0) CYCLE

     !write(399,*) 'Wrong intersection:'
     valid = .FALSE.
     RETURN
  ENDDO

  RETURN
END SUBROUTINE possib


!*----------------------------------------------------------*
!*                                                          *
!*    [formhp] forms a heap "lhl" of the values "wle" and   *
!*             stores the position in the heap in "lhk"     *
!*                                                          *
!*----------------------------------------------------------*
      SUBROUTINE formhp( nsl, wle, lhl, lhk )

      IMPLICIT REAL*8(a-h,o-z)

      REAL*8      wle(*)
      INTEGER   lhl(*), lhk(*)

! *** The value the length front is used to set up a "priority queue" for
!     the sides in the generation front. The priority queue is represented 
!     by a binary tree (heap structure) with the number of the side with the
!     smallest value on the root of the tree. The value for a father node in
!     the heap structure is always smaller than the values of its two sons.
!                             
!                   if
!                  /  \             v(if) < v(is1)  and
!                is1  is2           v(if) < v(is2)


! *** forms the heap of lengths

      DO 200 is=1,nsl
      lhl(is) = is
      lhk(is) = is
      IF(is.NE.1) THEN
        iso = is
        ifa = is/2
  100   wls = wle(lhl(iso))
        wlf = wle(lhl(ifa))
        IF(wls<wlf) THEN
          lhk(lhl(iso)) = ifa
          lhk(lhl(ifa)) = iso
          ikeep         = lhl(iso)
          lhl(iso)      = lhl(ifa)
          lhl(ifa)      = ikeep
          iso           = ifa
          ifa           = iso/2
          IF(ifa>0) GOTO 100
        ENDIF
      ENDIF
  200 CONTINUE

      RETURN
      END
!*----------------------------------------------------------*
!*                                                          * 
!*    [adhsi] adds the side in the front "isid" of lenght   *
!*            "slen" to the heap structure "lha". "ars"     *
!*            contains the length of the sides of the       *
!*            front. "trs" stores the position in the heap  *
!*            of the side "isid". "nele" is the number of   *
!*            sides in the front.                           *
!*                                                          * 
!*----------------------------------------------------------*
      SUBROUTINE adhsi(slen, nele, lha, trs, ars )

      IMPLICIT REAL*8(a-h,o-z)

      INTEGER     lha(*)    ,trs(*)
      REAL*8        ars(*)

! *** "isid" is the last entry in the heap

      ars(nele) = slen
      trs(nele) = nele
      lha(nele) = nele

! *** Updates the heap structure going up the binary tree

      IF(nele.NE.1) THEN
         iso = nele                      ! son
         ifa = nele/2                    ! father
  100    as  = ars(lha(iso))
         af  = ars(lha(ifa))
         IF(as<af) THEN               ! checks the key
            trs(lha(iso)) = ifa
            trs(lha(ifa)) = iso
            ikeep         = lha(iso)
            lha(iso)      = lha(ifa)
            lha(ifa)      = ikeep
            iso           = ifa
            ifa           = iso/2
            IF(ifa>0)       GOTO 100
         ENDIF
      ENDIF
      RETURN
      END
!*---------------------------------------------------*
!*                                                   * 
!*    [dehsi] deletes the side in the front "isid"   *
!*            from the heap structure "lha".         *
!*                                                   * 
!*---------------------------------------------------*
      SUBROUTINE dehsi( isid, nele, lha, trs, ars )

      IMPLICIT REAL*8(a-h,o-z)

      INTEGER     lha(*)    ,trs(*)
      REAL*8        ars(*)

! *** Replaces the "isid" by the last entry in the heap

      ifa           = trs(isid)
      lha(ifa)      = lha(nele+1)
      trs(lha(ifa)) = ifa

! *** Updates the heap structure going down the binary tree    !--- XIE why not UP ??

  100 is1 = 2*ifa                    ! left son
      IF(is1>nele) GOTO 200
      as1 = ars(lha(is1))
      af  = ars(lha(ifa))
      is2 = is1+1                    ! right son
      IF(is2<=nele) THEN
         as2 = ars(lha(is2))
         IF(as2<as1) THEN         ! checks the key
            is1 = is2
            as1 = as2
         ENDIF
      ENDIF
      IF(as1<af) THEN             ! checks the key
         trs(lha(is1))  = ifa
         trs(lha(ifa))  = is1
         ikeep          = lha(is1)
         lha(is1)       = lha(ifa)
         lha(ifa)       = ikeep
         ifa            = is1
                             GOTO 100
      ENDIF

  200 CONTINUE

      IF( isid .NE. nele+1 ) THEN
        ars(isid)        = ars(nele+1)
        trs(isid)        = trs(nele+1)
        lha(trs(nele+1)) = isid
      ENDIF

      RETURN
      END


