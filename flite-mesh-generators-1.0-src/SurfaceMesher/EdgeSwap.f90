!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*-------------------------------------------------------------------*
!>
!!    [Oubay's code]
!!    loops over the sides of the mesh and swaps diagonals    
!!             to obtain a more even distribution of minimum angles.
!!    @param[in]  RegionID  : the curvature Region ID.
!!    @param[in]  Nss_IP_ideal  : the ideal number of connection of each point.
!!    @param[in]  Kstr   =1  for a stretching domain;              \n                     
!!                       =0  for a non-stretching domain.
!!    @param[in]  iswtp <0  swap according to connectivities.      \n
!!                      >0  swap according to min. angle.          \n
!!                      =0  swap according to both.
!!    @param[in,out] Tri3D  : the (updated) surface mesh.
!!    @param[in,out] Nss_IP : the (updated) number of connection of each point.
!<
!*-------------------------------------------------------------------*
SUBROUTINE Surf_DelaunaySwap(RegionID, Tri3D, Nss_IP, Nss_IP_ideal, iswtp, Kstr )
  USE control_Parameters
  USE Spacing_Parameters
  USE SurfaceMeshManager
  USE Geometry2D
  USE Geometry3D
  IMPLICIT NONE
  INTEGER, INTENT(IN)                :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  INTEGER, INTENT(INOUT)  :: Nss_IP(*)
  INTEGER, INTENT(IN)     :: Nss_IP_ideal(*)
  INTEGER, INTENT(IN)     :: iswtp, Kstr

  INTEGER :: ichan, ichans, iLoop, ip1, ip2, ip3, ip4, it1, it2, is, i
  INTEGER :: ih1, ih2, ih3, ih4, ihf1, ihf2, ihf3, ihf4
  INTEGER :: iswap, iactu, ifutu, iam ,ifm, Isucc
  REAL*8  :: r0(2), x0(3), xn(3)
  REAL*8  :: Rp(3), Ru(3), Rv(3), Ruv(3), chord1, chord2, TOLG_SQ, vm
  REAL*8  :: angSwap
  INTEGER, DIMENSION(:), POINTER :: Mark


  ! *** notation (original configuration) 

  !                        1         3
  !                        *---------*
  !                       / \       /
  !                      /   \  T1 /
  !                     / T2  \   /
  !                    /       \ /
  !                   *---------*
  !                   4         2

  WRITE(*, '(/,a)') '  .... Diagonal swapping. '
  WRITE(29,'(/,a)') '  .... Diagonal swapping. '

  ALLOCATE(Mark(Tri3D%NB_Edge))
  Mark(:) = 0

  TOLG_SQ = TOLG*TOLG
  
  ichans = 0
  Loop_ichan : DO iLoop = 0, 6

     ichan = 0

     Loop_is : DO is=1,Tri3D%NB_Edge

        it1 = Tri3D%ITR_Edge(1,is)
        it2 = Tri3D%ITR_Edge(2,is)
        IF(it2==0)        CYCLE Loop_is       !--- boundary side
        IF(Mark(IS)==1)   CYCLE Loop_is       !--- checked side

        !--- mark edge
        Mark(IS) = 1 

        ip1 = Tri3D%IP_Edge(1,is)
        ip2 = Tri3D%IP_Edge(2,is)
        
        IF(ip1>Tri3D%NB_BD_Point .AND. Nss_IP(ip1)<=3)    CYCLE Loop_is
        IF(ip2>Tri3D%NB_BD_Point .AND. Nss_IP(ip2)<=3)    CYCLE Loop_is

        ! *** determines ip3 & ip4

        ip3 = Tri3D%IP_Tri(1,it1) + Tri3D%IP_Tri(2,it1) + Tri3D%IP_Tri(3,it1) -ip1-ip2
        ip4 = Tri3D%IP_Tri(1,it2) + Tri3D%IP_Tri(2,it2) + Tri3D%IP_Tri(3,it2) -ip1-ip2

        ! *** checks the area in the parameter plane

        CALL Surf_SwapCheck1(Tri3D, ip1, ip2, ip3, ip4, 1, Isucc)
        IF(Isucc==0)  CYCLE Loop_is

        !--- Check which edge is bower
        r0(:)  = ( Tri3D%Coord(:,ip1) + Tri3D%Coord(:,ip2)   &
                 + Tri3D%Coord(:,ip3) + Tri3D%Coord(:,ip4) ) /4
        CALL GetUVPointInfo1(RegionID,r0(1),r0(2),Rp,Ru,Rv,Ruv)

        xn(:) = Geo3D_Cross_Product(Ru, Rv)
        vm    = xn(1)*xn(1) + xn(2)*xn(2) + xn(3)*xn(3)
        IF( vm > TOLG_SQ )THEN
           !--- a curved face, check bowing
           x0(:)  = ( Tri3D%Posit(:,ip1) +Tri3D%Posit(:,ip2) ) / 2.d0 - Rp(:)
           chord1 = Geo3D_Dot_Product(x0, xn) 
           x0(:)  = ( Tri3D%Posit(:,ip3) +Tri3D%Posit(:,ip4) ) / 2.d0 - Rp(:)
           chord2 = Geo3D_Dot_Product(x0, xn) 
           IF( ABS(chord2) > 1.01d0 * ABS(chord1) )THEN
              vm = vm * Geo3D_Distance_SQ(Tri3D%Posit(:,ip3), Tri3D%Posit(:,ip4))
              IF( chord2*chord2 > 1.d-4 * vm ) CYCLE Loop_is
           ENDIF
        ENDIF

        ! *** swapping according to connectivities

        iswap = 1
        IF(iswtp<=0) THEN
           ih1  = ABS(Nss_IP(ip1)    - Nss_IP_ideal(ip1))
           ih2  = ABS(Nss_IP(ip2)    - Nss_IP_ideal(ip2))
           ih3  = ABS(Nss_IP(ip3)    - Nss_IP_ideal(ip3))
           ih4  = ABS(Nss_IP(ip4)    - Nss_IP_ideal(ip4))
           ihf1 = ABS(Nss_IP(ip1) -1 - Nss_IP_ideal(ip1))
           ihf2 = ABS(Nss_IP(ip2) -1 - Nss_IP_ideal(ip2))
           ihf3 = ABS(Nss_IP(ip3) +1 - Nss_IP_ideal(ip3))
           ihf4 = ABS(Nss_IP(ip4) +1 - Nss_IP_ideal(ip4))
           iactu = ih1+ih2+ih3+ih4
           ifutu = ihf1+ihf2+ihf3+ihf4
           iam   = MAX(ih1 ,ih2 ,ih3 ,ih4)
           ifm   = MAX(ihf1,ihf2,ihf3,ihf4)
           IF(iactu>ifutu)THEN
              !--- strong swap
              iswap=1
           ELSE IF(iactu==ifutu .AND. iam>(ifm+1))THEN
              !--- strong swap
              iswap=1
           ELSE IF(iswtp==0 .OR. (iactu==ifutu .AND. iam>=(ifm-1)) )THEN
              !--- weak swap
              iswap=2
           ELSE
              !--- no swap
              CYCLE Loop_is
           ENDIF
        ENDIF

        ! *** check the new angles: do not swap if min. new angle is smaller

        IF(iswtp>=0) THEN
           AngSwap = Swapping_Angle
           IF(iswap/=2) AngSwap = 1000.
           CALL  Surf_SwapDelaunayCheck(Tri3D, IS, kstr, AngSwap, Isucc )
           IF(Isucc==0) CYCLE Loop_is
        ENDIF

        ! *** changes connectivities

        ichan      = ichan+1
        Nss_IP(ip1) = Nss_IP(ip1) -1
        Nss_IP(ip2) = Nss_IP(ip2) -1
        Nss_IP(ip3) = Nss_IP(ip3) +1
        Nss_IP(ip4) = Nss_IP(ip4) +1

        CALL Surf_SwapSingleEdge(Tri3D, is)

        Mark(Tri3D%IED_Tri(:,it1)) = 0
        Mark(Tri3D%IED_Tri(:,it2)) = 0

     ENDDO Loop_is

     IF(ichan==0) EXIT
     ichans = ichans + ichan

  ENDDO Loop_ichan

  WRITE(*, '(a,i7,a,i5,a)') '  Surf_DelaunaySwap: ',ichans, ' sides swapped in ',iLoop,' loops.'
  WRITE(29,'(a,i7,a,i5,a)') '  Surf_DelaunaySwap: ',ichans, ' sides swapped in ',iLoop,' loops.'

  DEALLOCATE(Mark)
  
  RETURN
END SUBROUTINE Surf_DelaunaySwap



!*-------------------------------------------------------------------*
!>
!!    Loops over the sides of the mesh and swaps diagonals    
!!             to obtain a more even distribution of minimum angles.
!!    This is for super-patch treatment.
!!    @param[in]  Kstr   =1  for a stretching domain;              \n                     
!!                       =0  for a non-stretching domain.
!<
!*-------------------------------------------------------------------*
SUBROUTINE SuperPatch_DelaunaySwap( Kstr )
  USE control_Parameters
  USE surface_Parameters
  USE SurfaceMeshManager
  IMPLICIT NONE
  INTEGER, INTENT(IN)     :: Kstr
  INTEGER :: ichan, ichans, iLoop, is, it1, it2, Isucc, ip1, ip2
  INTEGER, DIMENSION(:), POINTER :: Mark

  WRITE(*, '(/,a)') '  .... Diagonal swapping for SuperPatch. '
  WRITE(29,'(/,a)') '  .... Diagonal swapping for SuperPatch. '
  
  ALLOCATE(Mark(Surf%NB_Edge))
  Mark(:) = 0

  ichans = 0
  Loop_ichan : DO iLoop = 0, 6

     ichan = 0

     Loop_is : DO is=1,Surf%NB_Edge

        it1 = Surf%ITR_Edge(1,is)
        it2 = Surf%ITR_Edge(2,is)
        IF(it2==0)      CYCLE Loop_is       !--- boundary side
        IF(Mark(IS)==1) CYCLE Loop_is       !--- checked side

        !--- mark edge
        Mark(IS) = 1 

        IF(Surf%IP_Tri(4,it1) >  NB_SuperPatch)      CYCLE Loop_is      !--- triangle on a normal patch
        IF(Surf%IP_Tri(4,it2) >  NB_SuperPatch)      CYCLE Loop_is      !--- triangle on a normal patch
        IF(Surf%IP_Tri(4,it1) /= Surf%IP_Tri(4,it2)) CYCLE Loop_is      !--- edge on a super-curve
        IF(EdgeSuperID(IS)    >  0 )                 CYCLE Loop_is      !--- edge on a super-curve

        CALL Surf_SwapDelaunayCheck(Surf, IS, Kstr, Swapping_Angle, Isucc )
        IF(Isucc==0) CYCLE

        ! *** changes connectivities

        IF(SuperCosmetic_Method==3)THEN
           ip1 = Surf%IP_Edge(1,IS)
           ip2 = Surf%IP_Edge(2,IS)
        ENDIF
        
        CALL Surf_SwapSingleEdge(Surf, is)

        ichan = ichan+1

        CALL SurfTri_CalGeom(Surf, SurfTri, it1)
        CALL SurfTri_CalGeom(Surf, SurfTri, it2)

        Mark(Surf%IED_Tri(:,it1)) = 0
        Mark(Surf%IED_Tri(:,it2)) = 0

        IF(SuperCosmetic_Method==3)THEN
           CALL removepart(ip1, it1)
           CALL removepart(ip2, it2)
           ip1 = Surf%IP_Edge(1,IS)    !--- ipR
           ip2 = Surf%IP_Edge(2,IS)    !--- ipL
           CALL addpart(   ip1, it1, it2)
           CALL addpart(   ip2, it2, it1)
        ENDIF
        
     ENDDO Loop_is

     IF(ichan==0) EXIT
     ichans = ichans + ichan

  ENDDO Loop_ichan

  WRITE(*, '(a,i7,a,i5,a)') '  SuperPatch_DelaunaySwap: ',ichans, ' sides swapped in ',iLoop,' loops.'
  WRITE(29,'(a,i7,a,i5,a)') '  SuperPatch_DelaunaySwap: ',ichans, ' sides swapped in ',iLoop,' loops.'

  DEALLOCATE(Mark)

  RETURN
END SUBROUTINE SuperPatch_DelaunaySwap

!*-------------------------------------------------------------------*
!>
!!    Check if an edge can be swapped by Delaunay criterion
!!    @param[in]  Surf  : the surface mesh.
!!    @param[in]  IS    : the edge being checked.
!!    @param[in]  Kstr   =1  for a stretching domain;              \n                     
!!                       =0  for a non-stretching domain.
!!    @param[in]  AngSwap  : the swapping angle. 
!!                           If the smallest angle of present mesh is big than 
!!                           this swapping angle, do not do swapping.
!!    @param[out] Isucc =1  can swap;              \n                     
!!                      =0  can not swap.
!<
!!-------------------------------------------------------------------*
SUBROUTINE Surf_SwapDelaunayCheck(Surf, IS, kstr, AngSwap, Isucc )
  USE Spacing_Parameters
  USE Geometry3D
  USE SurfaceMeshStorage
  IMPLICIT NONE
  REAL*8, PARAMETER :: PI = 3.141592654D0
  TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
  INTEGER, INTENT(IN)  :: IS, Kstr
  REAL*8,  INTENT(IN)  :: AngSwap
  INTEGER, INTENT(OUT) :: Isucc

  INTEGER :: ip1, ip2, ip3, ip4, it1, it2, ied, it3, i, j, Isucc1
  REAL*8  :: x1(3), x2(3), x3(3), x4(3), x0(3)
  REAL*8  :: a123, a421, a312, a214, anin, adi
  REAL*8  :: a234, a143, a342, a431, anfi 
  REAL*8  :: fMap(3,3), scalar


  ! *** notation (original configuration) 

  !                        1         3
  !                        *---------*
  !                       / \       /
  !                      /   \  T1 /
  !                     / T2  \   /
  !                    /       \ /
  !                   *---------*
  !                   4         2

  Isucc = 0

        it1 = Surf%ITR_Edge(1,is)
        it2 = Surf%ITR_Edge(2,is)
        IF(it1==0 .OR. it2==0) RETURN
        
        ip1 = Surf%IP_Edge(1,is)
        ip2 = Surf%IP_Edge(2,is)
        ip3 = Surf%IP_Tri(1,it1) + Surf%IP_Tri(2,it1) + Surf%IP_Tri(3,it1) -ip1-ip2
        ip4 = Surf%IP_Tri(1,it2) + Surf%IP_Tri(2,it2) + Surf%IP_Tri(3,it2) -ip1-ip2

        x1(:) = Surf%Posit(:,ip1)
        x2(:) = Surf%Posit(:,ip2)
        x3(:) = Surf%Posit(:,ip3)
        x4(:) = Surf%Posit(:,ip4)
        
        !---dihedral angle check
        !---switch off due to not suiting for high-order mesh.
        !
        !   adi = Geo3D_Dihedral_Angle (x1,x2,x3,x4, Isucc1)
        !   IF(abs(abs(adi)-PI)>0.25) RETURN
        
        !--- delaunay angle check
        
        IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
           x0(:) = ( Surf%Posit(:,ip1) +Surf%Posit(:,ip2) ) / 2.d0
           CALL SpacingStorage_GetMapping(BGSpacing, x0, fMap)
           x1(:) = Mapping3D_Posit_Transf(Surf%Posit(:,ip1),fMap,1)
           x2(:) = Mapping3D_Posit_Transf(Surf%Posit(:,ip2),fMap,1)
           x3(:) = Mapping3D_Posit_Transf(Surf%Posit(:,ip3),fMap,1)
           x4(:) = Mapping3D_Posit_Transf(Surf%Posit(:,ip4),fMap,1)
        ENDIF

        !--- present angles
        a123  = Geo3D_Included_Angle(x1,x2,x3)
        a312  = Geo3D_Included_Angle(x3,x1,x2)
        a214  = Geo3D_Included_Angle(x2,x1,x4)
        a421  = Geo3D_Included_Angle(x4,x2,x1)
        anin  = MIN(a123,a312,a421,a214)

        IF(anin>AngSwap)     RETURN
        IF(a123+a421 >= PI)  RETURN
        IF(a312+a214 >= PI)  RETURN
        

        !--- expected angles
        IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
           x0(:) = ( Surf%Posit(:,ip3) +Surf%Posit(:,ip4) ) / 2.d0
           CALL SpacingStorage_GetMapping(BGSpacing, x0, fMap)
           x1(:) = Mapping3D_Posit_Transf(Surf%Posit(:,ip1),fMap,1)
           x2(:) = Mapping3D_Posit_Transf(Surf%Posit(:,ip2),fMap,1)
           x3(:) = Mapping3D_Posit_Transf(Surf%Posit(:,ip3),fMap,1)
           x4(:) = Mapping3D_Posit_Transf(Surf%Posit(:,ip4),fMap,1)
        ENDIF
        IF( Geo3D_Included_Angle(x2,x3,x4) <= anin ) RETURN
        IF( Geo3D_Included_Angle(x3,x4,x2) <= anin ) RETURN
        IF( Geo3D_Included_Angle(x1,x4,x3) <= anin ) RETURN
        IF( Geo3D_Included_Angle(x4,x3,x1) <= anin ) RETURN

        !--- topo check
        
        do i = 1,3
           ied = Surf%IED_Tri(i,it1)
           IF(ied==IS) cycle
           it3 = Surf%ITR_Edge(1,ied) + Surf%ITR_Edge(2,ied) - it1
           IF(it3==0) cycle
           do j = 1,3
              IF(Surf%IP_Tri(j,it3)==ip4) RETURN
           enddo
        enddo

  Isucc = 1

  RETURN
END SUBROUTINE Surf_SwapDelaunayCheck

