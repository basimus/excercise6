/*
 *  Copyright (C) 2017 College of Engineering, Swansea University
 *
 *  This file is part of the SwanSim FLITE suite of tools.
 *
 *  SwanSim FLITE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SwanSim FLITE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this SwanSim FLITE product. 
 *  If not, see <http://www.gnu.org/licenses/>.
*/


#include <occt_flite.h>

#include <iostream>

int main( int argc, char *argv[] )
{
	char  line[100];

	using namespace std;
	
	OCCT_FLITE occt;
	Standard_Real tMin, tMax, tHalf, two, u1, u2, v1, v2,u,v;
	Standard_Real* XYZ;
	Standard_Real* Ru, *Ruu, *Rv, *Rvv, *Ruv;
	Standard_Integer n,i,numsurf,numcurves;
	

	XYZ = (Standard_Real*) calloc((3),sizeof(Standard_Real ));
	
	Ru =  (Standard_Real*) calloc((3),sizeof(Standard_Real ));
	Rv =  (Standard_Real*) calloc((3),sizeof(Standard_Real ));
	Ru =  (Standard_Real*) calloc((3),sizeof(Standard_Real ));
	Ruv =  (Standard_Real*) calloc((3),sizeof(Standard_Real ));
	Ruu =  (Standard_Real*) calloc((3),sizeof(Standard_Real ));
	Rvv =  (Standard_Real*) calloc((3),sizeof(Standard_Real ));


	cout << "IGES Test C++	";
	//occt.OCCT_LoadIGES("cubebrep.igs");
	occt.OCCT_LoadSTEP("cube.stp");
	//occt.OCCT_LoadIGES("cube2.igs");
	//occt.OCCT_LoadIGES("Opti.iges");
	//occt.OCCT_LoadIGES("bearing.iges");
	occt.OCCT_GetNumSurfaces(numsurf);
	occt.OCCT_GetNumCurves(numcurves);

	cout << "\n Number of surfaces :	";
	cout << numsurf;

	cout << "\n Number of curves  :	";
	cout << numcurves;

	gets( line );

	

	for (i=1;i<=numcurves;i++){
		occt.OCCT_GetLineTBox(i, tMin, tMax);
		two = 2.0;
		tHalf = tMin + ((tMax-tMin)/two);
		
		occt.OCCT_GetLineXYZFromT(i,tHalf,XYZ);
		cout << "\n Curve "<< i << "	XYZ @ tHalf:	\n";
		cout << XYZ[0] << "	" <<XYZ[1] << "	" <<XYZ[2] <<"\n";
	
	};

	gets( line );
	occt.OCCT_SurfaceUVBox(1,u1,v1,u2,v2);

	cout << "\n Surf 1 u1:	";
	cout << u1;
	cout << "\n Surf 1 u2:	";
	cout << u2;
	cout << "\n Surf 1 v1:	";
	cout << v1;
	cout << "\n Surf 1 v2:	";
	cout << v2;

	u = u1 + ((u2-u1)/two);
	v = v1 + ((v2-v1)/two);

	cout << "\n Surf 1 info @ u:	";
	cout << u;
	cout << "\n v:	";
	cout << v;
	gets( line );
//	Standard_Real* XYZ;
	occt.OCCT_GetUVPointInfoAll(1,u,v,XYZ,Ru,Rv,Ruv,Ruu,Rvv);

	cout << "\n XYZ :	";
	cout << XYZ[0];
	cout << "\n";
	cout << XYZ[1];
	cout << "\n";
	cout << XYZ[2];

	cout << "\n Ru :	";
	cout << Ru[0];
	cout << "\n";
	cout << Ru[1];
	cout << "\n";
	cout << Ru[3];

	cout << "\n Rv :	";
	cout << Rv[0];
	cout << "\n";
	cout << Rv[1];
	cout << "\n";
	cout << Rv[2];

	cout << "\n Ruv :	";
	cout << Ruv[0];
	cout << "\n";
	cout << Ruv[1];
	cout << "\n";
	cout << Ruv[2];

	cout << "\n Ruu :	";
	cout << Ruu[0];
	cout << "\n";
	cout << Ruu[1];
	cout << "\n";
	cout << Ruu[2];

	cout << "\n Rvv :	";
	cout << Rvv[0];
	cout << "\n";
	cout << Rvv[1];
	cout << "\n";
	cout << Rvv[2];

	occt.OCCT_GetXYZFromUV1(1,u,v,XYZ);

	cout << "\n XYZ using different function :	";
	cout << XYZ[0];
	cout << "\n";
	cout << XYZ[1];
	cout << "\n";
	cout << XYZ[3];

	Standard_Integer j;
	for (j=1; j <=numsurf;j++){

		occt.OCCT_GetSurfaceNumCurves(j,n);
		cout << "\n Number of curves on surface " << j <<"\n" ;
		cout << n;
	
		Standard_Integer* listt;

		listt = occt.OCCT_GetSurfaceCurves(j,n);
		cout << "\n Index of curves on surface 1 :	";

		for (i=0; i < n; i++) {
			cout << listt[i] << "\n";
		};

	};
	gets( line );
	Standard_Real ** vertices;
	
	vertices = occt.OCCT_CalculateCurveFacets(1,n,10);

	cout << "\n Curve Facet Parameter values :	\n";

	for (i=0; i < n; i++) {
		cout << vertices[i][6] << "\n";
	};

	cout << "\n Curve Facet Coordinates values :	\n";

	for (i=0; i < n; i++) {
		cout << vertices[i][0] << "		" << vertices[i][1] << "		" << vertices[i][2]<<"\n";
	};

        gets( line );

	return 0;

};

