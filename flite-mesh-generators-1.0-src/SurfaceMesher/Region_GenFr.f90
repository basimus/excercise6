!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*---------------------------------------------------------------------*
!>
!!    [genfr] constructs the initial generation front for a surface    
!!            region. The input required is simply the list of         
!!            segments forming the boundary of the region to be        
!!            triangulated. This routine chooses automatically the     
!!            orientation of the of the curve segments according to    
!!            the normal of the surface so the area of the region in   
!!            the parameter plane is positive.                         
!!                                                                      
!!            IMPORTANT, we are assuming the following:                
!!                                                                      
!!         -> The surface region is simply or multiply connected.      
!!            A region is connected if any two points in the region    
!!            can be joined by a curve all of whose points belong to   
!!            the region.                                             \n 
!!         -> The boundary of the region is formed by one or more      
!!            closed loops of segments.                               \n
!!         -> Segments in the loop connect with the other segments     
!!            only at their end points. A common point must be shared  
!!            by two and only two segments.                           \n
!!         -> The loops of segments are closed curves which have no    
!!            common points.                                          \n
!!         -> If the boundary of the region is formed by more than     
!!            loop, then there is a loop that contains the rest. The   
!!            other loops are interior only to the exterior loop.     \n
!!         -> The exterior loop is the one with the maximum area in    
!!            the parameter plane.                                    \n
!!         -> The orientation of the loops defining the boundary of    
!!            the region is selected so the area of the exterior       
!!            loop is positive and the area of the interior loops,     
!!            if any, is negative.                                    \n
!!         -> Each point in the initial generation front is visited    
!!            only once in a loop along its length. Consequently, the  
!!            number of points and sides in the discretized front      
!!            should be the same.                                     \n
!!                                                                     
!!                       *** NASA Langley AUG-93  ***                  
!!
!!  @param[in]     RegionID   : the Region ID. 
!!  @param[in]     Surf       : the surface with node positions on curves.
!!  @param[in]     CurveNodes : the nodes on each curve.
!!                              This is a globe numbering.
!!  @param[in]     TOLG       : the tolerance
!!  @param[out]    Tri3D      : The nodes on boundary with 2D (u,v) positions.
!!                              The nodes are in order forming loops.
!!  @param[out]    map_Point  : map_Point(Tri3D node ID) = (Surf node ID).
!!  @param[out]    nloop      : the number of loops
!!  @param[out]    LoopEnds   :  start and end position of each loop.

!<
SUBROUTINE RegionType_genfr( RegionID, Surf, CurveNodes,  Tri3D, & 
     map_Point,  nloop, LoopEnds , TOLG)
  USE control_Parameters, ONLY : Debug_Display
  USE SurfaceMeshStorage
  USE Queue
  IMPLICIT NONE
  REAL*8,  PARAMETER   :: BIG = 1.e+30
  INTEGER, PARAMETER   :: MaxLoop = 100
  INTEGER, INTENT(in)             :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(IN)    :: Surf
  TYPE(SurfaceMeshStorageType), INTENT(inout) :: Tri3D
  TYPE (IntQueueType), INTENT(IN)             :: CurveNodes(*)
  INTEGER, INTENT(OUT) :: nloop
  INTEGER :: map_Point(*), LoopEnds(2,*)
  REAL*8, INTENT(in)  :: TOLG

  INTEGER, DIMENSION(:  ), POINTER :: lst1, ipfr
  REAL*8,  DIMENSION(:  ), POINTER :: area
  REAL*8,  DIMENSION(:,:), POINTER :: cl, cg
  LOGICAL :: store

  INTEGER :: npl, ntp, ip, is, js, ks, np1,np2, k1, k2, kst
  INTEGER :: k, i2, nsd, itmp(2), nkpt, nkpt1, in, n1, i, j
  REAL*8  :: EPS1, ARMX, aru, arv,  tmp, ar
  REAL*8  :: UBOT, VBOT, UTOP, VTOP
  INTEGER :: nCurv
  TYPE (IntQueueType), SAVE :: CurveRing

  CALL GetRegionUVBox(RegionID, UBOT, VBOT, UTOP, VTOP)
  CALL GetRegionCurveList(RegionID, CurveRing)
  nCurv = CurveRing%numNodes

  ! *** Area of the parameter plane where the surface Region is defined.

  EPS1 = 1.e-06
  ARMX = (UTOP-UBOT + EPS1) * (VTOP-VBOT + EPS1)

  ! 
  ! *** Stores the current list of boundary segments in lst1
  !     lst1 ... list of ordered segments forming the boundary

  lst1 => CurveRing%Nodes
  IF(Debug_Display>1)THEN
     DO i = 1,nCurv-1
        DO j = i+1,nCurv
           IF(lst1(i)==lst1(j))THEN
              WRITE(29,*) '  Error--- Forming the initial front for Region: ',RegionID,    & 
                   ',  Curve ',lst1(i),' appears twrice.'
              CALL Error_Stop ('RegionType_genfr ::   ') 
           ENDIF
        ENDDO
     ENDDO
  ENDIF


  ! *** Number of points (and sides) in the front

  ntp = 0
  DO is = 1,nCurv 
     ks  = lst1(is)
     ntp = ntp + CurveNodes(ks)%numNodes
  ENDDO


  ! *** Auxiliary arrays:
  !     area ... area in the parameter plane enclosed by the loops
  !     cg ..... 3D coordinates of the points in the initial front
  !     cl ..... parametric coordinates of the points in the initial front
  !     ipfr ... the nodes of each boundary side.

  ALLOCATE( area(   nCurv) )
  ALLOCATE( cl(2,ntp), cg(3,ntp), ipfr(ntp) )
  

  ! *** Forms the initial front and finds the boundary loops

  npl   = 0
  nloop = 0
  store = .TRUE.
  Loop_is : DO is = 1,nCurv 
     IF(store) THEN
        ! Beginning of a new loop 
        ks     = ABS(lst1(is))
        np1    = CurveNodes(ks)%Nodes(1)   !First node in this curve
        np2    = CurveNodes(ks)%Nodes(CurveNodes(ks)%numNodes) !Last Node in this curve
	store  = .FALSE.
        nloop  = nloop+1
        IF(nloop>MaxLoop) CALL Error_Stop ('RegionType_genfr :: nloop>MaxLoop  ') 
        LoopEnds(1,nloop) = npl+1


     ELSE
        ! Finds a contiguous segment
        Loop_js : DO js = is,nCurv
           k1  = CurveNodes(lst1(js))%Nodes(1)
           k2  = CurveNodes(lst1(js))%back
           IF( np2==k1 ) THEN         ! Preserves the original orientation
              kst = lst1(js)
              np2 = k2
              EXIT Loop_js
           ELSE IF( np2==k2 ) THEN    ! Changes the original orientation
              kst = -lst1(js)
              np2 = k1
              EXIT Loop_js
           ENDIF
           IF(js==nCurv)THEN
              WRITE(29,*) ' Error--- Forming the initial front for Region: ',RegionID,    & 
                   '        Cannot find segment adjacent to ...... ',lst1(is-1)
              CALL Error_Stop ('RegionType_genfr :: Broken loop ---1  ') 
	   ENDIF
        ENDDO Loop_js

        lst1(js) = lst1(is)    ! Shuffles the list
        lst1(is) = kst
     ENDIF

     IF(lst1(is)>0)THEN
        DO i = 1, CurveNodes(lst1(is))%numNodes -1
           ip        = CurveNodes(lst1(is))%Nodes(i)
           npl       = npl+1
           cg(:,npl) = Surf%Posit(1:3,ip)
           ipfr(npl) = ip
        ENDDO
     ELSE
        DO i = CurveNodes(-lst1(is))%numNodes, 2, -1
           ip = CurveNodes(-lst1(is))%Nodes(i)
           npl       = npl+1
           cg(:,npl) = Surf%Posit(1:3,ip)
           ipfr(npl) = ip
        ENDDO
     ENDIF

     !--- checks against the first point
     IF( np1==np2 ) THEN
        !--- loop closed
        store             = .TRUE.
        LoopEnds(2,nloop) = npl
     ENDIF
  ENDDO Loop_is

  IF( .NOT. store ) THEN
     WRITE(29,*) '  Error--- Cannot close the front for Region: ',RegionID,    & 
          '               Last loop of segments is not closed'   &
	  ,UBOT, VBOT, UTOP, VTOP
     CALL Error_Stop ('RegionType_genfr :: Broken loop ---2  ')  
  ENDIF

  ! *** Computes the parametric coordinates of the points in the
  !     initial generation front

  WRITE(*, '(/,a,/)') '  .... Computing parametric coordinates.'
  WRITE(29,'(/,a,/)') '  .... Computing parametric coordinates.'

  CALL GetUVFromXYZ( RegionID, npl, cg, cl, TOLG)
 !  IF(Debug_Display>0)THEN
 !   write(2000,*) RegionID
 !   DO k = 1,npl
 !     WRITE(2000,*) cl(1,k), cl(2,k)
 !   END DO
 ! END IF
  ! *** After forming the loops of segments computes the area
  !     in the parameter plane enclosed in each of them.

  DO k = 1,nloop          ! Computes the areas
     aru = 0.
     arv = 0.
     DO is = LoopEnds(1,k),LoopEnds(2,k)
        i2  = is+1
        IF(is==LoopEnds(2,k)) i2 = LoopEnds(1,k)
        aru = aru - 0.5 * (cl(2,is)+cl(2,i2)) * (cl(1,i2)-cl(1,is))
        arv = arv + 0.5 * (cl(1,is)+cl(1,i2)) * (cl(2,i2)-cl(2,is))
     ENDDO
     area(k) = 0.5*(aru+arv)
     IF( ABS(area(k)) > 1.0001d0*ARMX ) THEN
        WRITE(29,'(/,a,i7)')   '  Warning:  Area error in Region: ',RegionID 
        WRITE(29,'(a,e12.5)')  '        Loop of segments has an invalid area: ',ABS(area(k))
        WRITE(29,'(a,e12.5)')  '        which is bigger than the whole patch: ',ARMX
        WRITE(29,'(a)')        '  Suggestion: Check the geometry to see if any boundary curves'
        WRITE(29,'(a)')        '              go out of the region.'
     ENDIF
  ENDDO

  ! *** Sorts the loops according to the area

  k = 1
  DO j = 2,nloop
     IF( ABS(area(j)) > ABS(area(k)) ) k = j
  ENDDO
  IF(k/=1)THEN
     tmp       = area(k)
     area(k)   = area(1)
     area(1)   = tmp
     itmp(:)   = LoopEnds(:,k)
     LoopEnds(:,k) = LoopEnds(:,1)
     LoopEnds(:,1) = itmp(:)
  ENDIF

  ! *** Modifies the orientation of the loops and the sides in the front
  !     according to the sign of the computed areas. It stores the new
  !     orientation of the segments in Region%IC

  nkpt = 0
  DO i = 1, nloop
     IF( i == 1) THEN           ! Outer loop
        ar = area(i)
     ELSE                         ! Inner loops
        ar = -area(i)
     ENDIF

     nkpt1 = nkpt+1
     IF( ar > 0. ) THEN        ! Correct orientation
        DO is = LoopEnds(1,i),LoopEnds(2,i)
           nkpt                = nkpt+1
           map_Point(nkpt)     = ipfr(is)
           Tri3D%Coord(:,nkpt) = cl(1:2,is)
        ENDDO
     ELSE                         ! Opposite orientation required
        DO is = LoopEnds(2,i),LoopEnds(1,i),-1
           nkpt                  = nkpt+1
           map_Point(nkpt)       = ipfr(is)
           Tri3D%Coord(:,nkpt)   = cl(1:2,is)
        ENDDO
     ENDIF
     LoopEnds(1,i) = nkpt1
     LoopEnds(2,i) = nkpt
  ENDDO

  IF( nkpt /= npl )   CALL Error_Stop ('RegionType_genfr :: nkpt /= npl  ') 

  Tri3D%NB_Point    = npl
  Tri3D%NB_BD_Point = npl
  DO ip = 1, npl
     Tri3D%Posit(:,ip) = Surf%Posit(:,map_Point(ip))
  ENDDO

  ! *** Frees memory for auxiliary vectors

  DEALLOCATE( ipfr )
  DEALLOCATE( area,  cl, cg  )
  NULLIFY (lst1)

  RETURN
END SUBROUTINE RegionType_genfr


