!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*******************************************************************************
!>
!!  Set default control parameters, and modify them by reading NAMELIST
!!    or by command line arguments.
!<       
!*******************************************************************************
SUBROUTINE Control_Input( )

  USE occt_fortran  !SPW
  USE control_Parameters
  USE surface_Parameters
  USE Number_Char_Transfer
  IMPLICIT NONE
  REAL*8, PARAMETER :: PI = 3.141592654D0
  INTEGER :: iargc,i, Isucc
  CHARACTER(LEN=256) :: sw, path, controlFile
  LOGICAL :: ex_ctl, ex_name, ex_path, output_ctl, allRight, TF

  !---- Default setting

  controlFile  ='Surf3D_v25.ctl '
  JobName      = ' '

  !--- NAMELIST /ControlParameters/
  Debug_Display           = 1
  Start_Point             = 1
  Curvature_Type          = 4
  Generate_Method         = 1
  Orientation_Direct      = 0
  !--- NAMELIST /BackgroundParameters/
  Background_Model        = 5
  Globe_GridSize          = 1000.d0
  Stretch_Limit           = 10.d0
  Mapping_Interp_Model    = 1
  Interpolate_Oct_Mapping = .FALSE.
  Gradation_Factor        = 0.5d0
  Curvature_Factors(1:7)  = (/ 0.2d0, 0.1d0, 1.d0, 10.d0, 150.d0, 120.d0, 1.d0 /)
  GridSize_Adjustor       = 1.d0
  !--- NAMELIST /CosmeticParameters/
  Loop_Cosmetic           = 1
  Loop_Smooth             = 1
  Smooth_Method           = 3
  Collapse_Angle          = 12.5d0
  Swapping_Angle          = 30.0d0
  Loop_SuperCosmetic      = 3
  SuperCosmetic_Method    = 0
  SuperCosmetic_Quad      = -1
  Loop_SuperFrqFilter     = 0
  SuperFrqFilter_power    = 1.d0

  NB_SuperPatch = 0
  SuperCurve_RidgeAngle  = 120.d0

  NameUc = ''
  NameUs = ''
  NameSs = ''
  NameFs = ''
  NameHi = ''
  NameGs = ''
  path   = ''
  JobName= ''

  ex_name      = .FALSE.
  ex_path      = .FALSE.
  output_ctl   = .FALSE.

  i = 0
  DO WHILE(i<iargc())
     i = i+1
     CALL GETARG(i,sw)
     IF(sw(1:1)=='-')THEN
        IF(sw(2:3)=='h ' .OR. sw(2:3)=='H ')THEN
           CALL Help_Message(2)
           STOP
        ELSE IF(sw(2:4)=='hh ' .OR. sw(2:4)=='HH ')THEN
           CALL Help_Message(3)
           STOP
        ELSE IF(sw(2:3)=='o ' .OR. sw(2:3)=='O ')THEN
           output_ctl = .TRUE.
        ELSE IF(sw(2:6)=='path ' .OR. sw(2:6)=='PATH ')THEN
           i = i+1
           CALL GETARG(i,sw)
           path = sw
           ex_path = .TRUE.
        ELSE
          i = i+1
        ENDIF
     ELSE IF(.NOT. ex_name)THEN
        JobName = sw
        ex_name = .TRUE.
     ELSE
        WRITE(*,*)'Error--- more than one job names'
        CALL Help_Message(2)
        STOP
     ENDIF
  ENDDO

     !---- Input Job Name if not given on the command line
     IF(JobName==' ' .and. (.not.output_ctl))THEN
        WRITE(*,*) 'Input Problem Name : '
        READ (*,*) JobName
     ENDIF

     JobNameLength = LEN_TRIM( JobName )
     IF( JobNameLength == 0  .and. (.not.output_ctl)) THEN
        WRITE(*,*)'Error--- : file name has zero JobNameLength'
        CALL Error_Stop ('Control_Input') 
     ENDIF

     IF(ex_path)THEN
        JobName = TRIM(path) // '/' // JobName
        JobNameLength = LEN_TRIM( JobName )
     ENDIF
     
     
  !---- Input control parameters by namelist
  ex_path = CHAR_FilenamePath(JobName,path)
  IF(ex_path) controlFile = TRIM(path) // '/' // controlFile
  INQUIRE(file = TRIM(controlFile), EXIST=ex_ctl)
  IF(ex_ctl)THEN
     OPEN(1,file=TRIM(controlFile), status='old')
     READ(1,ControlParameters,end=101)
101  CLOSE (1)
     OPEN(1,file=TRIM(controlFile), status='old')
     READ(1,BackgroundParameters,end=102)
102  CLOSE (1)
     OPEN(1,file=TRIM(controlFile), status='old')
     READ(1,CosmeticParameters,end=103)
103  CLOSE (1)
  ENDIF

  
  !---- get job-name
  IF(Curvature_Type==2 .OR. Curvature_Type==3)THEN
     CALL CADFIX_Check_Availability( TF )
     IF(TF)THEN
        CALL CADFIX_Init()
        CALL CADFIX_IsClientMode( TF )
        IF(TF)THEN
           JobName = 'CADfixMode'
           ex_name = .TRUE.
        ENDIF
     ENDIF
  ENDIF


  IF(.NOT. output_ctl)THEN

     !--- read other parameters from .bpp

     INQUIRE(file = JobName(1:JobNameLength)//'.bpp', EXIST=ex_ctl)
     IF(ex_ctl)THEN
        OPEN(1,file=JobName(1:JobNameLength)//'.bpp', status='old')
        DO 
           READ(1,*,IOSTAT=Isucc)sw
           IF(Isucc<0)THEN
              EXIT
           ELSE IF(CHAR_isEqual (sw, '#end', .FALSE.) ) THEN
              EXIT
           ELSE IF(CHAR_isEqual (sw, '#Globe_GridSize', .FALSE.) ) THEN
              READ(1,*) Globe_GridSize
           ELSE IF(CHAR_isEqual (sw, '#Background_Model', .FALSE.) ) THEN
              READ(1,*) Background_Model
           ELSE IF(CHAR_isEqual (sw, '#Curvature_Factors', .FALSE.) ) THEN
              READ(1,*) Curvature_Factors(1:7)
           ELSE IF(CHAR_isEqual (sw, '#unchecking_curve_ids', .FALSE.) ) THEN
              CALL IntQueue_Read(ListUc, 1)
           ELSE IF(CHAR_isEqual (sw, '#unchecking_surf_ids', .FALSE.) ) THEN
              CALL IntQueue_Read(ListUs, 1)
           ELSE IF(CHAR_isEqual (sw, '#Highorder_surf_ids', .FALSE.) ) THEN
              CALL IntQueue_Read(ListHi, 1)
           ELSE IF(CHAR_isEqual (sw, '#Generating_surf_ids', .FALSE.) ) THEN
              CALL IntQueue_Read(ListGs, 1)
           ELSE IF(CHAR_isEqual (sw, '#unchecking_curve_names', .FALSE.) ) THEN
              READ(1,'(a)') NameUc
           ELSE IF(CHAR_isEqual (sw, '#unchecking_surf_names', .FALSE.) ) THEN
              READ(1,'(a)') NameUs
           ELSE IF(CHAR_isEqual (sw, '#Generating_surf_names', .FALSE.) ) THEN
              READ(1,'(a)') NameGs
           ELSE IF(CHAR_isEqual (sw, '#SuperPatch_Groups', .FALSE.) ) THEN
              READ(1, *) NB_SuperPatch
              ALLOCATE(SuperPatchList(NB_SuperPatch+1))
              DO i = 1, NB_SuperPatch
                 CALL IntQueue_Read(SuperPatchList(i), 1)
              ENDDO
           ELSE IF(CHAR_isEqual (sw, '#SuperCurve_RidgeAngle', .FALSE.) ) THEN
              READ(1,*) SuperCurve_RidgeAngle

           ELSE IF(CHAR_isEqual (sw, '#Layer_Heights', .FALSE.) ) THEN
           ELSE IF(CHAR_isEqual (sw, '#symmetry_surf_ids', .FALSE.) ) THEN
           ELSE IF(CHAR_isEqual (sw, '#farfield_surf_ids', .FALSE.) ) THEN
           ELSE IF(CHAR_isEqual (sw, '#symmetry_surf_names', .FALSE.) ) THEN
           ELSE IF(CHAR_isEqual (sw, '#farfield_surf_names', .FALSE.) ) THEN               
           ELSE IF(CHAR_isEqual (sw, '#Frame_Domain', .FALSE.) ) THEN
           ELSE
              sw = ADJUSTL(sw)
              IF(sw(1:1)=='#') THEN
                 WRITE(*,*)' Warning---- unkonwn title:', sw
              ENDIF
           ENDIF
        ENDDO
     ENDIF
  ENDIF

  !---- Command Options Input
  i     = 0
  Isucc = 1
  DO WHILE(i<iargc())
     i = i+1
     CALL GETARG(i,sw)
     IF(sw(1:3)=='-h ' .OR. sw(1:3)=='-H ' .OR. sw(1:4)=='-hh ' .OR. sw(1:4)=='-HH ')THEN
     ELSE IF(sw(1:3)=='-o ' .OR. sw(1:3)=='-O ')THEN
     ELSE IF(i<iargc() .AND. (sw(1:6)=='-path ' .OR. sw(1:6)=='-PATH '))THEN
        i = i+1
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-dd ' .OR. sw(1:4)=='-DD '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Debug_Display = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-sp ' .OR. sw(1:4)=='-SP '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Start_Point = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-gm ' .OR. sw(1:4)=='-GM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Generate_Method = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-od ' .OR. sw(1:4)=='-OD '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Orientation_Direct = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-bm ' .OR. sw(1:4)=='-BM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Background_Model = CHAR_to_INT(sw,Isucc)
        PRINT *,'Background Model (-bm)=',Background_Model
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-ggs ' .OR. sw(1:4)=='-GGS '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Globe_GridSize = CHAR_to_REAL8(sw,Isucc)
        PRINT *,'Global Grid Size (-ggs)=',Globe_GridSize
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-sl ' .OR. sw(1:4)=='-SL '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Stretch_Limit = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf1 ' .OR. sw(1:5)=='-CF1 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(1) = CHAR_to_REAL8(sw,Isucc)
        PRINT *,'Curvature Factor 1 (-cf1)=',Curvature_Factors(1)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf2 ' .OR. sw(1:5)=='-CF2 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(2) = CHAR_to_REAL8(sw,Isucc)
        PRINT *,'Curvature Factor 2 (-cf2)=',Curvature_Factors(2)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf3 ' .OR. sw(1:5)=='-CF3 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(3) = CHAR_to_REAL8(sw,Isucc)
        PRINT *,'Curvature Factor 3 (-cf3)=',Curvature_Factors(3)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf4 ' .OR. sw(1:5)=='-CF4 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(4) = CHAR_to_REAL8(sw,Isucc)
        PRINT *,'Curvature Factor 4 (-cf4)=',Curvature_Factors(4)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf5 ' .OR. sw(1:5)=='-CF5 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(5) = CHAR_to_REAL8(sw,Isucc)
        PRINT *,'Curvature Factor 5 (-cf5)=',Curvature_Factors(5)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf6 ' .OR. sw(1:5)=='-CF6 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(6) = CHAR_to_REAL8(sw,Isucc)
        PRINT *,'Curvature Factor 6 (-cf6)=',Curvature_Factors(6)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-cf7 ' .OR. sw(1:5)=='-CF7 '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Curvature_Factors(7) = CHAR_to_REAL8(sw,Isucc)
        PRINT *,'Curvature Factor 7 (-cf7)=',Curvature_Factors(7)
      ELSE IF(i<iargc() .AND. (sw(1:5)=='-mim ' .OR. sw(1:5)=='-MIM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Mapping_Interp_Model = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-iom ' .OR. sw(1:5)=='-IOM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Isucc = 1
        IF(sw(1:1)=='t' .OR. sw(1:1)=='T')THEN
           Interpolate_Oct_Mapping = .TRUE.
        ELSE IF(sw(1:1)=='f' .OR. sw(1:1)=='F')THEN
           Interpolate_Oct_Mapping = .FALSE.
        ELSE
           Isucc = 0
        ENDIF
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-ga ' .OR. sw(1:4)=='-GA '))THEN
        i = i+1
        CALL GETARG(i,sw)
        GridSize_Adjustor = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-gf ' .OR. sw(1:4)=='-GF '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Gradation_Factor = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-lc ' .OR. sw(1:4)=='-LC '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Loop_Cosmetic = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-ls ' .OR. sw(1:4)=='-LS '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Loop_Smooth = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-sm ' .OR. sw(1:4)=='-SM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Smooth_Method = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-ca ' .OR. sw(1:4)=='-CA '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Collapse_Angle = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:4)=='-sa ' .OR. sw(1:4)=='-SA '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Swapping_Angle = CHAR_to_REAL8(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-lsc ' .OR. sw(1:5)=='-LSC '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Loop_SuperCosmetic = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-scm ' .OR. sw(1:5)=='-SCM '))THEN
        i = i+1
        CALL GETARG(i,sw)
        SuperCosmetic_Method = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-scq ' .OR. sw(1:5)=='-SCQ '))THEN
        i = i+1
        CALL GETARG(i,sw)
        SuperCosmetic_Quad = CHAR_to_INT(sw,Isucc)
     ELSE IF(i<iargc() .AND. (sw(1:5)=='-lsf ' .OR. sw(1:5)=='-LSF '))THEN
        i = i+1
        CALL GETARG(i,sw)
        Loop_SuperFrqFilter = CHAR_to_INT(sw,Isucc)
     ELSE IF(sw(1:1)=='-')THEN
        Isucc = 0
     ENDIF

     IF(Isucc/=1)THEN
        IF(Isucc==0)THEN
           WRITE(*, *)'Error--- Control_Input: Illegal flags'
        ENDIF
        CALL Help_Message(Isucc)
        STOP
     ENDIF

  ENDDO

  IF(output_ctl)THEN
     OPEN(1,file=TRIM(controlFile))
     WRITE(1,'(a)')'!--- please refer README for the control parameters reading...'
     WRITE(1,'(a)')' '
     WRITE(1,ControlParameters)
     WRITE(1,BackgroundParameters)
     WRITE(1,CosmeticParameters)
     CLOSE(1)
     WRITE(*,*)'---- Output a new file for control parameters ----'
     STOP
  ENDIF

  WRITE(*,*) '      Program Name : ',TRIM( JobName )
  WRITE(*,*)' '
  OPEN(29, file=JobName(1:JobNameLength)//'_ST.log', status='unknown')
  CALL Happy_Start(JobName)

  !---- Check control parameters

  AllRight = .TRUE.

  IF(Start_Point<0 .OR. Start_Point>2)THEN
     WRITE(29,*)'Error--- Invalid input: Start_Point=',Start_Point
     AllRight = .FALSE.
  ENDIF
  IF(Curvature_Type<1 .OR. Curvature_Type>4)THEN
     WRITE(29,*)'Error--- Invalid input: Curvature_Type=',Curvature_Type
     AllRight = .FALSE.
  ENDIF

 
 IF(Curvature_Type==2 .OR. Curvature_Type==3)THEN
     CALL CADFIX_Check_Availability( TF )
    IF(.NOT. TF)THEN
        WRITE(29,*)'Error--- Invalid input: Curvature_Type=',Curvature_Type
        WRITE(29,*)'         No Cadfix library being linked.'
        WRITE(29,*)'         Either reset Curvature_Type=1, '
        WRITE(29,*)'         or re-compile the code with CADfix library. '
        AllRight = .FALSE.
     ENDIF
  ENDIF

  IF(Generate_Method<-2 .OR. Generate_Method>2 .OR. Generate_Method==0)THEN
     WRITE(29,*)'Error--- Invalid input: Generate_Method=',Generate_Method
     AllRight = .FALSE.
  ENDIF
  IF(Orientation_Direct<-1 .OR. Orientation_Direct>1)THEN
     WRITE(29,*)'Error--- Invalid input: Orientation_Direct=',Orientation_Direct
     AllRight = .FALSE.
  ENDIF

  IF(ABS(Background_Model)>7 .OR. ABS(Background_Model)<2 .OR. ABS(Background_Model)==3)THEN
     WRITE(29,*)'Error--- Invalid input: Background_Model=',Background_Model
     AllRight = .FALSE.
  ENDIF

  IF(Mapping_Interp_Model>3 .OR. Mapping_Interp_Model<0)THEN
     WRITE(29,*)'Error--- Invalid input: Mapping_Interp_Model=',Mapping_Interp_Model
     AllRight = .FALSE.
  ENDIF
  IF(Interpolate_Oct_Mapping .AND. Background_Model<0)THEN
     WRITE(29,*)'Warning--- Control_Input: Interpolate_Oct_Mapping is .true.'
     WRITE(29,*)'           So it may take a longer time for an anisotropic case.'
     WRITE(29,*)' '
  ENDIF
  IF(Gradation_Factor<0.05)THEN
     WRITE(29,*)'Error--- Invalid input: Gradation_Factor=',Gradation_Factor
     AllRight = .FALSE.
  ENDIF
  IF(GridSize_Adjustor<0.01 .OR. GridSize_Adjustor>100)THEN
     WRITE(29,*)'Error--- Invalid input: GridSize_Adjustor=',GridSize_Adjustor
     AllRight = .FALSE.
  ENDIF

  IF(Smooth_Method<1 .OR. Smooth_Method>4)THEN
     WRITE(29,*)'Error--- Invalid input: Smooth_Method=',Smooth_Method
     AllRight = .FALSE.
  ENDIF
  IF(SuperCosmetic_Method<0 .OR. SuperCosmetic_Method>3)THEN
     WRITE(29,*)'Error--- Invalid input: SuperCosmetic_Method=',SuperCosmetic_Method
     AllRight = .FALSE.
  ENDIF
  IF(Start_Point==2 .AND. SuperCosmetic_Method==1)THEN
     WRITE(29,*)'Error--- Conflict input: Start_Point vs. SuperCosmetic_Method=',  &
          Start_Point, SuperCosmetic_Method
     AllRight = .FALSE.
  ENDIF
  IF(SuperCosmetic_Quad<-1 .OR. SuperCosmetic_Quad>1)THEN
     WRITE(29,*)'Error--- Invalid input: SuperCosmetic_Quad=',SuperCosmetic_Quad
     AllRight = .FALSE.
  ENDIF


  IF(.NOT. AllRight)THEN
     WRITE(29,*)'--- Use flag -h for more information.'
     CALL Error_Stop (' Control_Input  ')
  ENDIF

  !--- Angle unit transfer

  Collapse_Angle        = Collapse_Angle                   *PI/180.d0
  Swapping_Angle        = Swapping_Angle                   *PI/180.d0
  SuperCurve_RidgeAngle = (180.d0 - SuperCurve_RidgeAngle) *PI/180.d0


END SUBROUTINE Control_Input


!*******************************************************************************
!>
!!   Screen output help message.
!<       
!*******************************************************************************
SUBROUTINE Help_Message(Isucc)

  IMPLICIT NONE

  INTEGER, INTENT(IN) :: Isucc
  CHARACTER(LEN=256)  :: sw

  CALL GETARG(0,sw)

  WRITE(*,'(a)')' SYNOPSIS'
  WRITE(*,'(a)')'     '//TRIM(sw)//' [-h] [-o] [jobname] [-path xxx] [setting]'
  WRITE(*,'(a)')'  '
  WRITE(*,'(a)')' DESCRIPTION'
  WRITE(*,'(a)')'     All flags, except the jobname and path, are NOT case sensitive.'
  WRITE(*,'(a)')'     The flags can be used with NO particular order.'
  WRITE(*,'(a)')'  '
  WRITE(*,'(a)')'     -h        :  For this help message and defination of parameters.'
  WRITE(*,'(a)')'     -o        :  Save present control parameters and write them in "Surf3D_v25.ctl".'
  WRITE(*,'(a)')'                  If a "Surf3D_v25.ctl" already exsits, it will be replaced.'
  WRITE(*,'(a)')'  '
  WRITE(*,'(a)')'     jobname   :  program name (prefix of the files involved);'
  WRITE(*,'(a)')'                  If no jobname given here, you will be asked to input it '
  WRITE(*,'(a)')'                  after a screen prompt.'
  WRITE(*,'(a)')'     -path xxx :  Give the path of the directory where the input/output files locate.'
  WRITE(*,'(a)')'                  Normally we need not to give a path if the input/output files '
  WRITE(*,'(a)')'                  locate in the current working directory.'
  WRITE(*,'(a)')'                  However, it is always a must for submitting a job by "qsub".'
  WRITE(*,'(a)')'  '
  WRITE(*,'(a)')'     All control parameters have their default values, and can be reset as Namelist '
  WRITE(*,'(a)')'     variables read from file "Surf3D_v25.ctl".'
  WRITE(*,'(a)')'     You can also correct some of them further by using following flags:'
  WRITE(*,'(a)')'  '
  WRITE(*,'(a)')'  |  -DD   |  0,1,2,3,4        | Debug_Display            |'
  IF(Isucc==3) &
  WRITE(*,'(a)')'  |  -SP   |  0,1,2              | Start_Point              |'
  IF(Isucc==3) &
  WRITE(*,'(a)')'  |  -GM   |  1,2,-1,-2        | Generate_Method          |'
  IF(Isucc==3) &
  WRITE(*,'(a)')'  |  -OD   |  -1,0,1           | Orientation_Direct       |'
  WRITE(*,'(a)')'  |        |                   |                          |'
  WRITE(*,'(a)')'  |  -BM   |  (±) 2,4,5,6,7    | Background_Model         |'
  WRITE(*,'(a)')'  |  -GGS  |                   | Globe_GridSize           |'
  WRITE(*,'(a)')'  |  -CF1, -CF2, ..., -CF7     | Curvature_Factors(1:7)   |'
  WRITE(*,'(a)')'  |  -SL   |  >2.0             | Stretch_Limit            | '
  IF(Isucc==3) &
  WRITE(*,'(a)')'  |  -GA   |  0.01<=V<=100.0   | GridSize_Adjustor        |'
  
  IF(Isucc==3) &
  WRITE(*,'(a)')'  |  -IOM  |  T,F              | Interpolate_Oct_Mapping  |'
  WRITE(*,'(a)')'  |  -GF   |  >0.05            | Gradation_Factor         |'
  WRITE(*,'(a)')'  |        |                   |                          |'
  WRITE(*,'(a)')'  |  -LC   |  0,1,2,...        | Loop_Cosmetic            |'
  WRITE(*,'(a)')'  |  -LS   |  0,1,2,...        | Loop_Smooth              |'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  |  -SM   |  1,2,3            | Smooth_Method            |'
  WRITE(*,'(a)')'  |  -CA   |                   | Collapse_Angle           |'
  WRITE(*,'(a)')'  |  -SA   |                   | Swapping_Angle           |'
  ENDIF
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  |  -LSC  |  0,1,2,...        | Loop_SuperCosmetic       |'
  WRITE(*,'(a)')'  |  -SCM  |  0,1,2,3          | SuperCosmetic_Method     |'
  WRITE(*,'(a)')'  |  -SCQ  |  -1,0,1           | SuperCosmetic_Quad       |'
  WRITE(*,'(a)')'  |  -LSF  |  0,1,2...         | Loop_SuperFrqFilter      |'
  ENDIF
  WRITE(*,'(a)')'  '
  IF(Isucc==0)WRITE(*,'(a)')'      Use flag -h to get more detail of the parameters.'

  IF(Isucc==0)RETURN

  WRITE(*,'(a)')' EXAMPLE:'
  WRITE(*,'(a)')'     '//TRIM(sw)//' s_2l -path /home/work/sphere/ -DD 2 '
  WRITE(*,'(a)')' '
  WRITE(*,'(a)')' The defination of the parameters (default):'
  WRITE(*,'(a)')' '
  WRITE(*,'(a)')'  Debug_Display(1)'
  WRITE(*,'(a)')'        How verbose the mesh generator is'
  WRITE(*,'(a)')'        A bigger number indicates more checking in the code '
  WRITE(*,'(a)')'          and more output on the screen.'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  Start_Point(1)'
  WRITE(*,'(a)')'          =1,  read surface geometry from <program-name>.dat / .fbm / CAD file '
  WRITE(*,'(a)')'               and generate surface mesh.'
  WRITE(*,'(a)')'          =2,  Adaptation option. Read surface mesh from "*_0.fro" file.'
  ENDIF
  WRITE(*,'(a)')'  Curvature_Type(4)'
  WRITE(*,'(a)')'          =1,  Swansea curvature type; Read "*.dat" file.'
  WRITE(*,'(a)')'          =2,  CADfix curvature type;  Read "*.fbm" file.'
  WRITE(*,'(a)')'          =3,  Read CADfix curvature ("*.fbm" file),'
  WRITE(*,'(a)')'               and convert it to Swansea curvature type.'
  WRITE(*,'(a)')'               It will be reset as 1 after converting.'
  WRITE(*,'(a)')'          =4,  IGES or STEP input (.igs, .stp, .iges or .step) '
  WRITE(*,'(a)')'  Generate_Method(1)'
  WRITE(*,'(a)')'          = 1,    generate mesh using advance front (faster method).'
  WRITE(*,'(a)')'          = 2,    generate mesh using advance front (ST - use if 1 fails).'
  WRITE(*,'(a)')'          =-1,    generate mesh using Delaunay method, connect boundary nodes directly.'
  WRITE(*,'(a)')'          =-2,    generate mesh using Delaunay method, recover boundary edges.'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  Orientation_Direct(0)'
  WRITE(*,'(a)')'          =1,  set the surface orientating inside.'
  WRITE(*,'(a)')'          =-1, set the surface orientating outside.'
  WRITE(*,'(a)')'          =0,  do not change surface orientation after generation.'
  WRITE(*,'(a)')' '
  ENDIF
  WRITE(*,'(a)')'  Background_Model(5)'
  WRITE(*,'(a)')'          = ±2,  Tet. background spacing (read "*.bac").'
  WRITE(*,'(a)')'          = ±4,  input "*.bac" file, build an octree mesh as the background.'
  WRITE(*,'(a)')'          = ±5,  input optional "*.bac" file and geometry, '
  WRITE(*,'(a)')'                 build a curvature-controlled octree mesh as the background.'
  WRITE(*,'(a)')'          = ±6,  input "*.bac" & "*.fro" files, and generate spacing from existing surface mesh '
  WRITE(*,'(a)')'                 build a curvature-controlled octree mesh as the background.'
  WRITE(*,'(a)')'          = ±7,  Oct. background (read "*.Obac").'
  WRITE(*,'(a)')'          >0          for   isotropic problems.'
  WRITE(*,'(a)')'          <0          for anisotropic problems.'
  WRITE(*,'(a)')'  Globe_GridSize(1.0)'
  WRITE(*,'(a)')'          The maximum global grid size. '
  WRITE(*,'(a)')'          This is will be revaluated if a .bac file is input'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  Stretch_Limit(10.0)'
  WRITE(*,'(a)')'        The maximum value of allowed stretching (>2.0). '
  WRITE(*,'(a)')'        Only be referred for anisotropic problems. '
  ENDIF
  
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  Interpolate_Oct_Mapping(F)'
  WRITE(*,'(a)')'          If .true.,  insert surface nodes utilizing the anisotropic Delaunay kernel.'
  WRITE(*,'(a)')'          If .false., insert surface nodes utilizing the isotropic Delaunay kernel.'
  ENDIF
  WRITE(*,'(a)')'  Gradation_Factor(0.5)'
  WRITE(*,'(a)')'        Gradation factor of background mesh (0.05~3.0).'
  WRITE(*,'(a)')'        A small value results in a strong gradting and a smoother mesh.'
  WRITE(*,'(a)')'          If >=3.0, no gradation.'
  WRITE(*,'(a)')'  Curvature_Factors(:)'
  WRITE(*,'(a)')'        Factors of curvature affect spacing.'
  WRITE(*,'(a)')'        *(1)  contains the minimum spacing as a ratio of the radius of curvature. '
  WRITE(*,'(a)')'        *(2)  contains the maximum distance allowed between the generated '
  WRITE(*,'(a)')'                       surface element and the true geometry (with dimension).'
  WRITE(*,'(a)')'        *(3)  contains the minimum spacing allowed when element sizes are computed '
  WRITE(*,'(a)')'                       according to the underlying curvature (with dimension).'
  WRITE(*,'(a)')'        *(4)  maximum spacing allowed when element sizes are computed according to '
  WRITE(*,'(a)')'                       the underlying curvature (with dimension).'
  WRITE(*,'(a)')'        *(5)  contains the angle dihedral below which ridges are assumed (in degree, 0~180).'
  WRITE(*,'(a)')'        *(6)  contains the angle dihedral below which minimum spacing will be used at the '
  WRITE(*,'(a)')'                       ridges (in degree, 0~180).'
  WRITE(*,'(a)')'        *(7)  The minimum grid size (with dimension) for trailing.'
  WRITE(*,'(a)')' '
  WRITE(*,'(a)')'  Loop_Cosmetic(1)'
  WRITE(*,'(a)')'        The number of cosmetic loops.'
  WRITE(*,'(a)')'  Loop_Smooth(1)'
  WRITE(*,'(a)')'        The number of mesh smoothing loops'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  Smooth_Method(3)'
  WRITE(*,'(a)')'        Mesh smoothing method'
  WRITE(*,'(a)')'          =1,  smooth in 2D parameter plane.'
  WRITE(*,'(a)')'          =2,  smooth in 3D physical space: checking edge length from a node.'
  WRITE(*,'(a)')'          =3,  smooth in 3D physical space: checking midline length from a node.'
  WRITE(*,'(a)')'          =4,  smooth in 2D parameter plane using Lloyds algorithm (experimental feature)'
  ENDIF
  WRITE(*,'(a)')'  Collapse_Angle(12.5)'
  WRITE(*,'(a)')'       the maximum angle (in degree) below which element collapse will be attempted.'
  WRITE(*,'(a)')'  Swapping_Angle(30.0)'
  WRITE(*,'(a)')'       the maximum angle below which edge swapping will be attempted.'
  IF(Isucc==3)THEN
  WRITE(*,'(a)')'  Loop_SuperCosmetic(3)'
  WRITE(*,'(a)')'        The number of cosmetic loops for super patches.'
  WRITE(*,'(a)')'  SuperCosmetic_Method(0)'
  WRITE(*,'(a)')'        The method to choose geometry support for super patches.'
  WRITE(*,'(a)')'          =0,  no Super-Patch Cosmetic. '
  WRITE(*,'(a)')'          =1,  use regular surface curvature  (with Start_Point=1 only). '
  WRITE(*,'(a)')'          =2,  use existing triangulation as static base. '
  WRITE(*,'(a)')'          =3,  use existing triangulation as dynamic base'
  WRITE(*,'(a)')'  SuperCosmetic_Quad(-1)'
  WRITE(*,'(a)')'        The index if merge triangular elements to quadrilateral elements.'
  WRITE(*,'(a)')'          = 1,  merge to quadrangles only mesh. '
  WRITE(*,'(a)')'          = 0,  merge to hybrid mesh. '
  WRITE(*,'(a)')'          =-1,  do not merge, leave triangles only mesh. '
  WRITE(*,'(a)')'  Loop_SuperFrqFilter(0)'
  WRITE(*,'(a)')'        The number of loops of frequency filter for super patches.'
  WRITE(*,'(a)')'          =0,  no Super-Patch frequency filter. '
  WRITE(*,'(a)')'  SuperFrqFilter_power(1.0)'
  WRITE(*,'(a)')'        The power index of frequency filter for super patches.'
  ENDIF
  WRITE(*,'(a)')' '
  WRITE(*,'(a)')' '
  WRITE(*,'(a)')'  Please visit "http://git.swansim.org/swansim/flite-mesh-generators/wikis/home" for more information.'


  RETURN
END SUBROUTINE Help_Message

