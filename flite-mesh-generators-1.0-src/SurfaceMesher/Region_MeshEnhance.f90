!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*-------------------------------------------------------*
!*    [enhanc] enhances the quality of the surface mesh  *
!*-------------------------------------------------------*
SUBROUTINE Region_MeshEnhance(RegionID, Tri3D )
  USE control_Parameters
  USE Spacing_Parameters
  USE SurfaceMeshStorage
  USE SurfaceMeshManager
  USE SurfaceMeshManager2D
  IMPLICIT NONE
  INTEGER, INTENT(IN)                :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D

  INTEGER, DIMENSION(:  ), POINTER :: Nss_IP        !--- (*)    the number of surrounding elements of each node.
  INTEGER, DIMENSION(:  ), POINTER :: Nss_IP_ideal  !--- (*)    the ideal number of surrounding elements of each node.

  INTEGER  :: npOld, maico, i, ip, isw(100), ksw, k
  INTEGER  :: IUTOP, IVTOP
    
  IF(LOOP_COSMETIC==0) RETURN
  
  CALL GetRegionUVmax (RegionID, IUTOP, IVTOP)

  !--- Build edges

  CALL Surf_BuildEdge(Tri3D)

  ! *** splits the sides with two nodes on the boundary

  npOld = Tri3D%NB_Point
!  CALL Surf_SplitFlatEdge(RegionID, Tri3D)
!  IF(Tri3D%NB_Point/=npOld)THEN
!     !--- rebuild the edges
!     CALL Surf_BuildEdge(Tri3D)
!     npOld = Tri3D%NB_Point
!  ENDIF

!DO k=1,LOOP_COSMETIC

  !--- collapse tiny edges
  
  CALL Surf_AngleCollapse(Tri3D, 3)
  IF(Tri3D%NB_Point/=npOld)THEN
     !--- rebuild the edges
     CALL Surf_BuildEdge(Tri3D)
     npOld = Tri3D%NB_Point
  ENDIF

  IF(HighSurf(RegionID)>0)THEN
     CALL Surf_AngleCollapse(Tri3D, 2)  
  ELSE
     CALL Surf_EdgeCollapse(RegionID, Tri3D)
  ENDIF
  IF(Tri3D%NB_Point/=npOld)THEN
     !--- rebuild the edges
     CALL Surf_BuildEdge(Tri3D)
     npOld = Tri3D%NB_Point
  ENDIF


  ! *** diagonal swapping + mesh smoothing

  ALLOCATE(Nss_IP(Tri3D%NB_Point), Nss_IP_ideal(Tri3D%NB_Point))
  CALL conere( Tri3D, Nss_IP )
  CALL coneid( Tri3D, Nss_IP_ideal )


  isw(:) =  0 
  DO k=1,LOOP_COSMETIC
     ksw = isw(k)

	CALL Surf_DelaunaySwap(RegionID, Tri3D,  Nss_IP, Nss_IP_ideal, ksw, 1  )
         CALL Surf_Smooth_2D_3(RegionID, Tri3D, LOOP_SMOOTH, Nss_IP )
	 IF(SMOOTH_METHOD==4) THEN
		CALL Surf_BuildNext(Tri3D)
		CALL Surf_Lattice_2D(RegionID, Tri3D, LOOP_SMOOTH, Nss_IP  )
	 ENDIF
     IF(SMOOTH_METHOD==1 .OR. (IUTOP==2 .AND. IVTOP==2 .AND. Curvature_Type==1)) THEN
     ELSE IF(SMOOTH_METHOD==2) THEN
        CALL Surf_Smooth_3D(RegionID, Tri3D, LOOP_SMOOTH, 1) 
     ELSE IF(SMOOTH_METHOD==3) THEN
        CALL Surf_Smooth_3D_3(RegionID, Tri3D, LOOP_SMOOTH, 1) 
     ENDIF
  ENDDO

  
  !--- collapse tiny edges
  
  !CALL Surf_AngleCollapse(Tri3D, 3)
  !IF(Tri3D%NB_Point/=npOld)THEN
     !--- rebuild the edges
  !   CALL Surf_BuildEdge(Tri3D)
  !   npOld = Tri3D%NB_Point
  !ENDIF

  !IF(HighSurf(RegionID)>0)THEN
  !   CALL Surf_AngleCollapse(Tri3D, 2)  
  !ELSE
  !   CALL Surf_EdgeCollapse(RegionID, Tri3D)
  !ENDIF
  !IF(Tri3D%NB_Point/=npOld)THEN
     !--- rebuild the edges
  !   CALL Surf_BuildEdge(Tri3D)
  !   npOld = Tri3D%NB_Point
  !ENDIF
  
  !CALL Surf_DelaunaySwap(RegionID, Tri3D,  Nss_IP, Nss_IP_ideal, ksw, 1  )

  ! *** removing nodes surrounded by three elements

  npOld = Tri3D%NB_Point
  CALL Surf_RemoveYNode(Tri3D)
  IF(npOld>Tri3D%NB_Point)THEN
     WRITE(*, '(a,i7)') '       No. of Y-nodes removed: ',npOld-Tri3D%NB_Point
     WRITE(29,'(a,i7)') '       No. of Y-nodes removed: ',npOld-Tri3D%NB_Point
  ENDIF

  
  CALL Surf_ClearEdge( Tri3D )
  

  DEALLOCATE( Nss_IP, Nss_IP_ideal)

  RETURN
END SUBROUTINE Region_MeshEnhance


!
!*---------------------------------------------------------------------*
!*    [conere] calculates the number of elements surrounding a point   *
!*---------------------------------------------------------------------*
SUBROUTINE conere(Tri3D,Nss_IP)
  USE SurfaceMeshStorage
  IMPLICIT NONE
  TYPE(SurfaceMeshStorageType), INTENT(IN)      :: Tri3D
  INTEGER, INTENT(out) ::   Nss_IP(*)
  INTEGER :: it, ip1, ip2, ip3

  Nss_IP(1:Tri3D%NB_Point) = 0
  DO it=1,Tri3D%NB_Tri
     ip1        = Tri3D%IP_Tri(1,it)
     ip2        = Tri3D%IP_Tri(2,it)
     ip3        = Tri3D%IP_Tri(3,it)
     Nss_IP(ip1) = Nss_IP(ip1)+1
     Nss_IP(ip2) = Nss_IP(ip2)+1
     Nss_IP(ip3) = Nss_IP(ip3)+1
  ENDDO

  RETURN
END SUBROUTINE conere
!*-------------------------------------------------------------------*
!*    [coneid] determines the number of ideal connectivities for a   *
!*    point according to the values of the mesh parameters           *
!*-------------------------------------------------------------------*
SUBROUTINE coneid(Tri3D, Nss_IP_ideal  )
  USE SurfaceMeshStorage
  USE Geometry2D
  IMPLICIT NONE
  REAL*8, PARAMETER :: PI = 3.141592654D0
  TYPE(SurfaceMeshStorageType), INTENT(IN)      :: Tri3D
  INTEGER, INTENT(OUT)    :: Nss_IP_ideal(*)
  REAL*8,  DIMENSION(:), POINTER :: ang
  INTEGER :: it, i, ip1, ip2, ip3, ip, idiv
  REAL*8  :: c1(2), c2(2), c3(2)

  ALLOCATE(ang(Tri3D%NB_BD_Point))
  ang(1:Tri3D%NB_BD_Point) = 0.d0

  DO it = 1,Tri3D%NB_Tri
     DO i = 1,3
        ip1        = Tri3D%IP_Tri(i,            it)
        ip2        = Tri3D%IP_Tri(MOD(i,3)+1,   it)
        ip3        = Tri3D%IP_Tri(MOD(i+1,3)+1, it)
        IF(ip1<=Tri3D%NB_BD_Point)THEN
           c1    = Tri3D%Coord(:,ip1)
           c2    = Tri3D%Coord(:,ip2)
           c3    = Tri3D%Coord(:,ip3)
           ang(ip1) = ang(ip1) + ABS(Included_Angle_2D(c2,c1,c3))
        ENDIF
     ENDDO
  ENDDO

  Nss_IP_ideal(1:Tri3D%NB_Point) = 6
  DO ip = 1, Tri3D%NB_BD_Point
     idiv = (3.*ang(ip)/PI)+0.5
     Nss_IP_ideal(ip) = MAX(1, idiv)
  ENDDO

  DEALLOCATE(ang)

  RETURN
END SUBROUTINE coneid

!*---------------------------------------------------------------*
!!    In a curved surface, splits those sides too long or bowing
!!    This subroutine may go wrong for bad uv-plane.
!!          (sigular, sharp angle between u & v, ...)
!*---------------------------------------------------------------*
SUBROUTINE Surf_SplitFlatEdge(RegionID, Tri3D)
  USE Spacing_Parameters
  USE SurfaceMeshStorage
  USE array_allocator
  IMPLICIT NONE
  INTEGER, INTENT(IN)                :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D

  INTEGER, DIMENSION(:), POINTER :: IPnew_Edge
  REAL*8,  DIMENSION(:), POINTER :: xl_Edge
  INTEGER :: is, nsplit, ip1, ip2, ip3(3), id3(3)
  INTEGER :: NNew, ie, ne, NeOld, IP_New(3,4)
  REAL*8  :: x1(3), rp(12), xn(3), un, vn, vmd, chord, xl3(3)
  REAL*8  :: fac, r1(3), r2(3), v1(3), as12, ab12, tolg_bow
  LOGICAL :: dosplit

  WRITE(*, '(/,a)') '  .... Splitting flat sides. '
  WRITE(29,'(/,a)') '  .... Splitting flat sides. '

  ! *** use IPnew_Edge(*) to store the new point if split

  ALLOCATE(IPnew_Edge(Tri3D%NB_Edge), xl_Edge(Tri3D%NB_Edge))
  IPnew_Edge(1:Tri3D%NB_Edge) = 0

  fac = 1.9
  tolg_bow = 1.d-1

  ! *** loops over the sides and splits those for which their
  !     two end points are on the boundary

  nsplit = 0

  DO is = 1,Tri3D%NB_Edge
     IF(Tri3D%ITR_Edge(2,is)==0) CYCLE    !---- skip boundary edge
     dosplit = .FALSE.

     ip1 = Tri3D%IP_Edge(1,is)
     ip2 = Tri3D%IP_Edge(2,is)

     un  = 0.5* (Tri3D%Coord(1,ip1) + Tri3D%Coord(1,ip2))
     vn  = 0.5* (Tri3D%Coord(2,ip1) + Tri3D%Coord(2,ip2))
     CALL GetUVPointInfo1(RegionID,un,vn,Rp(1:3),Rp(4:6),Rp(7:9),Rp(10:12))

     r1(:) = Tri3D%Posit(:,ip1)
     r2(:) = Tri3D%Posit(:,ip2)
     v1(:) = r2(:)- r1(:)
     as12  = SQRT(v1(1)**2 + v1(2)**2 + v1(3)**2)
     
     !--- check if edge connect with two boundary nodes
     IF(ip1<=Tri3D%NB_BD_Point .AND. ip2<=Tri3D%NB_BD_Point) dosplit = .TRUE.
     IF(.NOT. dosplit)THEN
        !--- check if edge long
        CALL SpacingStorage_GetDirectGridSize(BGSpacing, Rp(1:3),v1(1:3),ab12)
        IF( as12 > fac*ab12 ) dosplit = .TRUE.
     ENDIF

     IF(.NOT. dosplit)THEN
        !--- check if edge bowing
        xn(1) = rp(5)*rp(9)-rp(8)*rp(6)
        xn(2) = rp(6)*rp(7)-rp(9)*rp(4)
        xn(3) = rp(4)*rp(8)-rp(7)*rp(5)
        vmd   = SQRT( xn(1)**2+xn(2)**2+xn(3)**2 )
        IF( vmd > TOLG ) THEN
           vmd   = 1./vmd
           xn(:) = xn(:)*vmd
           x1(:) = 0.5* (r1(:)   + r2(:))
           chord = (rp(1)-x1(1))*xn(1) + (rp(2)-x1(2))*xn(2) + (rp(3)-x1(3))*xn(3)
           IF( ABS(chord) > tolg_bow*as12 ) dosplit = .TRUE.
        ENDIF
     ENDIF

     IF(dosplit)THEN
        !-- add a new point
        nsplit = nsplit + 1
        Tri3D%NB_Point = Tri3D%NB_Point +1
        IF(Tri3D%NB_Point > SIZE(Tri3D%Posit,2))THEN
           CALL allc_2Dpointer(Tri3D%Posit, 3, Tri3D%NB_Point+100000)
           CALL allc_2Dpointer(Tri3D%Coord, 2, Tri3D%NB_Point+100000)
        ENDIF
        Tri3D%Coord(1,Tri3D%NB_Point) = un
        Tri3D%Coord(2,Tri3D%NB_Point) = vn
        Tri3D%Posit(:,Tri3D%NB_Point) = rp(1:3)

        IPnew_Edge(is) = Tri3D%NB_Point
        xl_Edge(is)    = as12
     ENDIF

  ENDDO

  IF( nsplit > 0 ) THEN

     WRITE(*, '(a,i7)') '       No. of sides divided: ',nsplit
     WRITE(29,'(a,i7)') '       No. of sides divided: ',nsplit

     ! *** finds the new connectivities

     NeOld = Tri3D%NB_Tri
     DO ie = 1, NeOld
        ip3(1:3) = Tri3D%IP_Tri(1:3,ie)
        id3(1:3) = IPnew_Edge(Tri3D%IED_Tri(1:3,ie))
        xl3(1:3) = xl_Edge(Tri3D%IED_Tri(1:3,ie))
        CALL Tri_Divider(ip3, id3, xl3, NNew, IP_New)
        IF(NNew==1) CYCLE

        Tri3D%IP_Tri(1:3,ie) = IP_New(1:3,1)
        DO ne = 2, NNew
           Tri3D%NB_Tri = Tri3D%NB_Tri +1
           IF(Tri3D%NB_Tri > SIZE(Tri3D%IP_Tri,2))THEN
              CALL allc_2Dpointer(Tri3D%IP_Tri, 5, Tri3D%NB_Tri+100000)
           ENDIF

           Tri3D%IP_Tri(1:3,Tri3D%NB_Tri) = IP_New(:,ne)
        ENDDO
     ENDDO

  ENDIF

  DEALLOCATE(IPnew_Edge, xl_Edge)

  RETURN
END SUBROUTINE Surf_SplitFlatEdge



