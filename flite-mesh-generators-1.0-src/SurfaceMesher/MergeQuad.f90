!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!-------------------------------------------------------------------
!>
!!  Merge triangular surface mesh into quadrilateral elements.
!<
!--------------------------------------------------------------------
SUBROUTINE Surf_Merge_Quad(Surf, RegionID)
  USE array_allocator
  USE CellConnectivity
  USE HeapTree
  USE SurfaceMeshStorage
  USE Geometry3D
  IMPLICIT NONE
  REAL*8 , PARAMETER :: PI    = 3.141592653589793D0
  REAL*8 , PARAMETER :: aBig  = 10000.D0
  INTEGER, PARAMETER :: ii(5) = (/1,2,3,1,2/)
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
  INTEGER, INTENT(IN)                :: RegionID


  INTEGER :: IT, IS, is1, is2, itnb, i, j, k, n, IP, ip9(9), Lmin
  REAL*8  :: amin, a(3), ps(3,3), p0(3)
  REAL*8,  DIMENSION(:),   POINTER :: edgeAng
  INTEGER, DIMENSION(:),   POINTER :: MarkEdge, MarkTri
  TYPE(HeapTreeType) :: Heap

  CALL Surf_BuildEdge(Surf)

  !--- calculate the quality (angle) of each edge.
  !--- MarkTri(IT) : (=-1,0,1,2) the number of inside edges of the triangles IT mines ONE.

  ALLOCATE(MarkEdge(Surf%NB_Edge))
  ALLOCATE(edgeAng(Surf%NB_Edge))
  edgeAng(:) = 0
  ALLOCATE(MarkTri(Surf%NB_Tri))
  MarkTri(:) = -1

  DO IT = 1, Surf%NB_Tri
     ip9(1:3)     = Surf%IP_Tri(1:3, IT)
     ps(1:3, 1:3) = Surf%Posit(1:3, ip9(1:3))
     a(1) = Geo3D_Included_Angle(ps(:,2), ps(:,1), ps(:,3))
     a(2) = Geo3D_Included_Angle(ps(:,3), ps(:,2), ps(:,1))
     a(3) = PI - a(1) - a(2)

     DO i = 1,3
        IS = Surf%IED_Tri(i,IT)
        IF(Surf%ITR_Edge(2,IS)<=0)THEN
           MarkEdge(IS) = -2
        ELSE
           MarkTri(IT)  = MarkTri(IT) + 1
           MarkEdge(IS) = 0
           edgeAng(IS)  = edgeAng(IS) + ABS(PI/2.d0 - a(i))
        ENDIF
     ENDDO
  ENDDO
  edgeAng(:) = edgeAng(:) / PI

  !--- build a heap-tree by the number of links of nodes.

  Heap%Ascending = .TRUE.
  CALL HeapTree_Allocate( Heap, Surf%NB_Edge )
  DO IS = 1, Surf%NB_Edge
     IF(MarkEdge(IS)<0)THEN
        amin = aBig
     ELSE
        Lmin = MarkTri(Surf%ITR_Edge(1,IS)) * MarkTri(Surf%ITR_Edge(2,IS))
        amin = Lmin + edgeAng(IS)
     ENDIF
     CALL HeapTree_AddValue( Heap, amin )
  ENDDO

  DO

     !--- start the edge which most likely from a corner and with worst quality.

     is = Heap%toList(1)
     IF(Heap%v(is)>=aBig) EXIT
     IF(Heap%v(is)<=0)    CALL Error_Stop ('Surf_Merge_Quad :: Heap%v(is)<=0  ') 
     IF(MarkEdge(is)/=0)  CALL Error_Stop ('Surf_Merge_Quad :: MarkEdge(is)/=0  ')

     !--- remove the edge found by marking it with 1.
     !--- Mark new boundary edges with -1.

     DO i= 1,2
        it = Surf%ITR_Edge(i,IS)
        IF(it==0)         CALL Error_Stop ('Surf_Merge_Quad ::  it==0  ') 
        IF(MarkTri(it)<0) CALL Error_Stop ('Surf_Merge_Quad ::  MarkTri(it)<0  ')

        MarkTri(it) = -2

        DO j = 1,3
           is1 = Surf%IED_Tri(j,it)
           IF(MarkEdge(is1)<0)    CYCLE
           IF(MarkEdge(is1)==1)   CALL Error_Stop ('Surf_Merge_Quad ::  MarkEdge(is1)==1  ') 

           !--- a new boundary (for triangulation) appears
           MarkEdge(is1) = -1
           CALL HeapTree_ResetNodeValue( Heap, is1, aBig )

           IF(is1==is) CYCLE
           itnb = Surf%ITR_Edge(1,is1) + Surf%ITR_Edge(2,is1) - it
           IF(itnb<=0)         CALL Error_Stop ('Surf_Merge_Quad ::  itnb<=0  ') 
           IF(MarkTri(itnb)<0) CALL Error_Stop ('Surf_Merge_Quad ::  MarkTri(itnb)<0  ') 
           MarkTri(itnb) = MarkTri(itnb) - 1

           DO k = 1,3
              is2 = Surf%IED_Tri(k,itnb)
              IF(MarkEdge(is2)<0)   CYCLE
              IF(MarkEdge(is2)==1)  CALL Error_Stop (' Surf_Merge_Quad ::  MarkEdge(is2)==1 ') 

              Lmin = MarkTri(Surf%ITR_Edge(1,IS2)) * MarkTri(Surf%ITR_Edge(2,IS2))
              amin = Lmin + edgeAng(IS2)
              CALL HeapTree_ResetNodeValue( Heap, is2, amin )
           ENDDO
        ENDDO
     ENDDO

     MarkEdge(is) = 1

  ENDDO

  !--- return all-quad mesh.

  CALL allc_2Dpointer(Surf%IP_Quad,5,3*Surf%NB_Tri)
  Surf%NB_Quad = 0

  !-- Add new nodes

  DO IS = 1, Surf%NB_Edge
     IF(MarkEdge(IS)==0) CALL Error_Stop ('Surf_Merge_Quad ::  MarkEdge(IS)==0  ')

     IF(MarkEdge(IS)==-2)THEN
        !--- original boundary edge
        ip9(1:2) = Surf%IP_Edge(1:2,IS)
        ip9(3:4) = (/ MAX(ip9(1),ip9(2)), MIN(ip9(1),ip9(2)) /)
        IF(ip9(3)-ip9(4)==2 .AND. ip9(3)<Surf%NB_BD_Point )THEN
           IP = ip9(3) - 1
        ELSE IF(ip9(3)-ip9(4)>2 .AND. ip9(3)<Surf%NB_BD_Point)THEN
           IP = ip9(3) + 1
        ELSE
           WRITE(29,*)' Error--- original boundary IS=',IS
           WRITE(29,*)'      ip1,2=',ip9(1:2), Surf%NB_BD_Point
           CALL Error_Stop (' Surf_Merge_Quad ') 
        ENDIF
     ELSE
        Surf%NB_Point = Surf%NB_Point + 1
        IP = Surf%NB_Point
        IF(IP>SIZE(Surf%Coord,2))THEN
           CALL allc_2Dpointer(Surf%Coord, 2, IP+Surf_allc_Increase)
        ENDIF
        IF(IP>SIZE(Surf%Posit,2))THEN
           CALL allc_2Dpointer(Surf%Posit, 3, IP+Surf_allc_Increase)
        ENDIF
        Surf%Coord(:,IP) = 0.5d0* (  Surf%Coord(:,Surf%IP_Edge(1,IS))   &
             + Surf%Coord(:,Surf%IP_Edge(2,IS))  )
        CALL GetUVPointInfo0(RegionID,Surf%Coord(1,IP),Surf%Coord(2,IP),p0)
        Surf%Posit(:,IP) = p0
     ENDIF

     IF(MarkEdge(IS)>0)THEN
        MarkEdge(IS) = IP
     ELSE
        MarkEdge(IS) = -IP
     ENDIF
  ENDDO

  !--- form quadrilateral elements from triangle pairs

  DO IS = 1, Surf%NB_Edge
     IF(MarkEdge(IS)<0) CYCLE
     it   = Surf%ITR_Edge(1,IS)
     itnb = Surf%ITR_Edge(2,IS)
     i    = which_NodeinTri(IS, Surf%IED_Tri(:,it))
     j    = which_NodeinTri(IS, Surf%IED_Tri(:,itnb))

     ip9(1:4) = (/ Surf%IP_Edge(1,IS), Surf%IP_Tri(j,itnb),  &
          Surf%IP_Edge(2,IS), Surf%IP_Tri(i,it) /)
     ip9(5:9) = (/ Surf%IED_Tri(ii(j+1),itnb), Surf%IED_Tri(ii(j+2),itnb),  &
          Surf%IED_Tri(ii(i+1),it),   Surf%IED_Tri(ii(i+2),it),  IS /)
     ip9(5:9) = ABS(MarkEdge(ip9(5:9)))

     Surf%IP_Quad(1:4,Surf%NB_Quad+1) = (/ ip9(1), ip9(5), ip9(9), ip9(8) /)
     Surf%IP_Quad(1:4,Surf%NB_Quad+2) = (/ ip9(5), ip9(2), ip9(6), ip9(9) /)
     Surf%IP_Quad(1:4,Surf%NB_Quad+3) = (/ ip9(8), ip9(9), ip9(7), ip9(4) /)
     Surf%IP_Quad(1:4,Surf%NB_Quad+4) = (/ ip9(9), ip9(6), ip9(3), ip9(7) /)
     Surf%IP_Quad(5,  Surf%NB_Quad+1 : Surf%NB_Quad+4) = Surf%IP_Tri(5,it)
     Surf%NB_Quad = Surf%NB_Quad + 4
  ENDDO

  !--- form quadrilateral elements from serperating triangles

  DO it = 1, Surf%NB_Tri
     IF(MarkTri(it)>=0)  CALL Error_Stop (' Surf_Merge_Quad ::  MarkTri(it)>=0 ') 
     IF(MarkTri(it)==-2) CYCLE

     Surf%NB_Point = Surf%NB_Point + 1
     IP = Surf%NB_Point
     IF(IP>SIZE(Surf%Coord,2))THEN
        CALL allc_2Dpointer(Surf%Coord, 2, IP+Surf_allc_Increase)
     ENDIF
     IF(IP>SIZE(Surf%Posit,2))THEN
        CALL allc_2Dpointer(Surf%Posit, 3, IP+Surf_allc_Increase)
     ENDIF
     Surf%Coord(:,IP) = (  Surf%Coord(:,Surf%IP_Tri(1,IT))   &
          + Surf%Coord(:,Surf%IP_Tri(2,IT))   &
          + Surf%Coord(:,Surf%IP_Tri(3,IT)) ) / 3.d0
     CALL GetUVPointInfo0(RegionID,Surf%Coord(1,IP),Surf%Coord(2,IP),p0)
     Surf%Posit(:,IP) = p0

     ip9(1:3) = Surf%IP_Tri(1:3,IT)
     ip9(4:6) = ABS(MarkEdge(Surf%IED_Tri(1:3,IT)))

     Surf%IP_Quad(1:4,Surf%NB_Quad+1) = (/ ip9(1), ip9(6), IP,     ip9(5) /)
     Surf%IP_Quad(1:4,Surf%NB_Quad+2) = (/ ip9(6), ip9(2), ip9(4), IP     /)
     Surf%IP_Quad(1:4,Surf%NB_Quad+3) = (/ IP,     ip9(4), ip9(3), ip9(5) /)
     Surf%IP_Quad(5,  Surf%NB_Quad+1 : Surf%NB_Quad+3) = Surf%IP_Tri(5,it)
     Surf%NB_Quad = Surf%NB_Quad + 3
  ENDDO


  WRITE(*, *) ' NB_Quad=',Surf%NB_Quad
  WRITE(29,*) ' NB_Quad=',Surf%NB_Quad
  Surf%NB_Tri = 0


  DEALLOCATE(edgeAng, MarkEdge, MarkTri)
  CALL HeapTree_Clear(Heap)

  RETURN  
END SUBROUTINE Surf_Merge_Quad



!-------------------------------------------------------------------
!>
!!  Merge triangular surface mesh into quadrilateral elements.
!!  This is a part of super-patch treatment.
!<
!--------------------------------------------------------------------
SUBROUTINE SuperPatch_Merge_Quad( )
  USE array_allocator
  USE CellConnectivity
  USE HeapTree
  USE Geometry3D
  USE control_Parameters
  USE surface_Parameters
  IMPLICIT NONE
  REAL*8 , PARAMETER :: PI    = 3.141592653589793D0
  REAL*8 , PARAMETER :: aBig  = 10000.D0
  INTEGER, PARAMETER :: ii(5) = (/1,2,3,1,2/)


  INTEGER :: IT, IS, is1, is2, itnb, i, j, k, n, IP, ip9(9), Lmin, Isucc
  REAL*8  :: amin, a(3), ps(3,3), p0(3)
  REAL*8,  DIMENSION(:),   POINTER :: edgeAng
  INTEGER, DIMENSION(:),   POINTER :: MarkEdge, MarkTri
  TYPE(HeapTreeType) :: Heap

  !--- calculate the quality (angle) of each edge.
  !--- MarkTri(IT) : (=-1,0,1,2) the number of inside edges of the triangles IT mines ONE.

  ALLOCATE(MarkEdge(Surf%NB_Edge))
  ALLOCATE(edgeAng(Surf%NB_Edge))
  edgeAng(:) = 0
  ALLOCATE(MarkTri(Surf%NB_Tri))
  MarkTri(:) = -1

  DO IT = 1, Surf%NB_Tri
     ip9(1:3)     = Surf%IP_Tri(1:3, IT)
     ps(1:3, 1:3) = Surf%Posit(1:3, ip9(1:3))
     a(1) = Geo3D_Included_Angle(ps(:,2), ps(:,1), ps(:,3))
     a(2) = Geo3D_Included_Angle(ps(:,3), ps(:,2), ps(:,1))
     a(3) = PI - a(1) - a(2)

     DO i = 1,3
        IS = Surf%IED_Tri(i,IT)
        IF(EdgeSuperID(IS)>0)THEN
           MarkEdge(IS) = -1
        ELSE
           MarkTri(IT)  = MarkTri(IT) + 1
           MarkEdge(IS) = 0
           edgeAng(IS)  = edgeAng(IS) + ABS(PI/2.d0 - a(i))
        ENDIF
     ENDDO
  ENDDO
  edgeAng(:) = edgeAng(:) / PI

  !--- build a heap-tree by the number of links of nodes.

  Heap%Ascending = .TRUE.
  CALL HeapTree_Allocate( Heap, Surf%NB_Edge )
  DO IS = 1, Surf%NB_Edge
     IF(MarkEdge(IS)==-1)THEN
        amin = aBig
     ELSE
        Lmin = MarkTri(Surf%ITR_Edge(1,IS)) * MarkTri(Surf%ITR_Edge(2,IS))
        amin = Lmin + edgeAng(IS)
     ENDIF
     CALL HeapTree_AddValue( Heap, amin )
  ENDDO

  DO

     !--- start the edge which most likely from a corner and with worst quality.

     is = Heap%toList(1)
     IF(Heap%v(is)>=aBig) EXIT
     IF(Heap%v(is)<=0)    CALL Error_Stop ('Surf_Merge_Quad :: Heap%v(is)<=0  ') 
     IF(MarkEdge(is)/=0)  CALL Error_Stop ('Surf_Merge_Quad :: MarkEdge(is)/=0  ') 

     !--- remove the edge found by marking it with 1.
     !--- Mark new boundary edges with -1.

     DO i= 1,2
        it = Surf%ITR_Edge(i,IS)
        IF(it==0)         CALL Error_Stop ('Surf_Merge_Quad ::  it==0  ') 
        IF(MarkTri(it)<0) CALL Error_Stop ('Surf_Merge_Quad ::  MarkTri(it)<0  ') 

        MarkTri(it) = -2

        DO j = 1,3
           is1 = Surf%IED_Tri(j,it)
           IF(MarkEdge(is1)==-1)  CYCLE
           IF(MarkEdge(is1)==1)   CALL Error_Stop ('Surf_Merge_Quad ::  MarkEdge(is1)==1  ') 

           !--- a new boundary (for triangulation) appears
           MarkEdge(is1) = -1
           CALL HeapTree_ResetNodeValue( Heap, is1, aBig )

           IF(is1==is) CYCLE
           itnb = Surf%ITR_Edge(1,is1) + Surf%ITR_Edge(2,is1) - it
           IF(itnb<=0)         CALL Error_Stop ('Surf_Merge_Quad ::  itnb<=0  ') 
           IF(MarkTri(itnb)<0) CALL Error_Stop (' Surf_Merge_Quad ::  MarkTri(itnb)<0 ')
           MarkTri(itnb) = MarkTri(itnb) - 1
           
           DO k = 1,3
              is2 = Surf%IED_Tri(k,itnb)
              IF(MarkEdge(is2)==-1) CYCLE
              IF(MarkEdge(is2)==1)  CALL Error_Stop ('Surf_Merge_Quad ::  MarkEdge(is2)==1  ') 

              Lmin = MarkTri(Surf%ITR_Edge(1,IS2)) * MarkTri(Surf%ITR_Edge(2,IS2))
              amin = Lmin + edgeAng(IS2)
              CALL HeapTree_ResetNodeValue( Heap, is2, amin )
           ENDDO
        ENDDO
     ENDDO

     MarkEdge(is) = 1

  ENDDO

  IF(SuperCosmetic_Quad==0)THEN
     !--- return hybrid mesh

     CALL allc_2Dpointer(Surf%IP_Quad,5,Surf%NB_Tri/2)
     Surf%NB_Quad = 0

     DO IS = 1, Surf%NB_Edge
        IF(MarkEdge(IS)/=1) CYCLE
        it   = Surf%ITR_Edge(1,IS)
        itnb = Surf%ITR_Edge(2,IS)
        i    = which_NodeinTri(IS, Surf%IED_Tri(:,it))
        j    = which_NodeinTri(IS, Surf%IED_Tri(:,itnb))

        Surf%NB_Quad = Surf%NB_Quad + 1
        Surf%IP_Quad(1:4,Surf%NB_Quad) = (/ Surf%IP_Edge(1,IS), Surf%IP_Tri(j,itnb),  &
             Surf%IP_Edge(2,IS), Surf%IP_Tri(i,it) /)
        Surf%IP_Quad(5,  Surf%NB_Quad) = Surf%IP_Tri(5,it)
     ENDDO

     WRITE(*, *) ' NB_Quad=',Surf%NB_Quad
     WRITE(29,*) ' NB_Quad=',Surf%NB_Quad
     !--- recount triangles left.

     n = 0
     DO it = 1, Surf%NB_Tri
        IF(MarkTri(it)>=0)  CALL Error_Stop ('Surf_Merge_Quad ::  MarkTri(it)>=0  ')  
        IF(MarkTri(it)==-2) CYCLE
        n = n+1
        Surf%IP_Tri(:,n) = Surf%IP_Tri(:,it)
     ENDDO

     Surf%NB_Tri = n
     WRITE(*, *) ' NB_Tri=',Surf%NB_Tri
     WRITE(29,*) ' NB_Tri=',Surf%NB_Tri

  ELSE
     !--- return all-quad mesh.

     CALL allc_2Dpointer(Surf%IP_Quad,5,3*Surf%NB_Tri)
     Surf%NB_Quad = 0

     !-- Add new nodes
     
     DO IS = 1, Surf%NB_Edge
        IF(MarkEdge(IS)==0) CALL Error_Stop (' Surf_Merge_Quad ::  MarkEdge(IS)==0 ') 

        Surf%NB_Point = Surf%NB_Point + 1
        IP = Surf%NB_Point
        IF(MarkEdge(IS)>0)THEN
           MarkEdge(IS) = IP
        ELSE
           MarkEdge(IS) = -IP
        ENDIF
        IF(IP>SIZE(Surf%Coord,2))THEN
           CALL allc_2Dpointer(Surf%Coord, 2, IP+Surf_allc_Increase)
        ENDIF
        IF(IP>SIZE(Surf%Posit,2))THEN
           CALL allc_2Dpointer(Surf%Posit, 3, IP+Surf_allc_Increase)
        ENDIF
        
        CALL SuperPatch_midpointposition(IS, p0, j)
        Surf%Posit(:,IP) = p0
     ENDDO

     !--- form quadrilateral elements from triangle pairs
     
     DO IS = 1, Surf%NB_Edge
        IF(MarkEdge(IS)<0) CYCLE
        it   = Surf%ITR_Edge(1,IS)
        itnb = Surf%ITR_Edge(2,IS)
        i    = which_NodeinTri(IS, Surf%IED_Tri(:,it))
        j    = which_NodeinTri(IS, Surf%IED_Tri(:,itnb))

        ip9(1:4) = (/ Surf%IP_Edge(1,IS), Surf%IP_Tri(j,itnb),  &
             Surf%IP_Edge(2,IS), Surf%IP_Tri(i,it) /)
        ip9(5:9) = (/ Surf%IED_Tri(ii(j+1),itnb), Surf%IED_Tri(ii(j+2),itnb),  &
             Surf%IED_Tri(ii(i+1),it),   Surf%IED_Tri(ii(i+2),it),  IS /)
        ip9(5:9) = ABS(MarkEdge(ip9(5:9)))

        Surf%IP_Quad(1:4,Surf%NB_Quad+1) = (/ ip9(1), ip9(5), ip9(9), ip9(8) /)
        Surf%IP_Quad(1:4,Surf%NB_Quad+2) = (/ ip9(5), ip9(2), ip9(6), ip9(9) /)
        Surf%IP_Quad(1:4,Surf%NB_Quad+3) = (/ ip9(8), ip9(9), ip9(7), ip9(4) /)
        Surf%IP_Quad(1:4,Surf%NB_Quad+4) = (/ ip9(9), ip9(6), ip9(3), ip9(7) /)
        Surf%IP_Quad(5,  Surf%NB_Quad+1 : Surf%NB_Quad+4) = Surf%IP_Tri(5,it)
        Surf%NB_Quad = Surf%NB_Quad + 4
     ENDDO

     !--- form quadrilateral elements from serperating triangles
     
     DO it = 1, Surf%NB_Tri
        IF(MarkTri(it)>=0)  CALL Error_Stop ('Surf_Merge_Quad ::  MarkTri(it)>=0  ')  
        IF(MarkTri(it)==-2) CYCLE

        Surf%NB_Point = Surf%NB_Point + 1
        IP = Surf%NB_Point
        IF(IP>SIZE(Surf%Coord,2))THEN
           CALL allc_2Dpointer(Surf%Coord, 2, IP+Surf_allc_Increase)
        ENDIF
        IF(IP>SIZE(Surf%Posit,2))THEN
           CALL allc_2Dpointer(Surf%Posit, 3, IP+Surf_allc_Increase)
        ENDIF

        p0(:) = (Surf%Posit(:,Surf%IP_Tri(1,IT))    &
               + Surf%Posit(:,Surf%IP_Tri(2,IT))   &
               + Surf%Posit(:,Surf%IP_Tri(3,IT)) ) / 3.d0
        j = inBaseTri(Surf%IP_Tri(1,IT))
        IF(j<=0) j = inBaseTri(Surf%IP_Tri(2,IT))
        IF(j<=0) j = inBaseTri(Surf%IP_Tri(3,IT))
        IF(j<=0) CALL Error_Stop ('  Surf_Merge_Quad :: j<=0 ') 
  
        CALL SuperPatch_project(p0, j, Isucc)
        IF(Isucc==0) CALL Error_Stop (' Surf_Merge_Quad ::  Isucc==0 ')
        Surf%Posit(:,IP) = p0
 
        ip9(1:3) = Surf%IP_Tri(1:3,IT)
        ip9(4:6) = ABS(MarkEdge(Surf%IED_Tri(1:3,IT)))

        Surf%IP_Quad(1:4,Surf%NB_Quad+1) = (/ ip9(1), ip9(6), IP,     ip9(5) /)
        Surf%IP_Quad(1:4,Surf%NB_Quad+2) = (/ ip9(6), ip9(2), ip9(4), IP     /)
        Surf%IP_Quad(1:4,Surf%NB_Quad+3) = (/ IP,     ip9(4), ip9(3), ip9(5) /)
        Surf%IP_Quad(5,  Surf%NB_Quad+1 : Surf%NB_Quad+3) = Surf%IP_Tri(5,it)
        Surf%NB_Quad = Surf%NB_Quad + 3
     ENDDO


     WRITE(*, *) ' NB_Quad=',Surf%NB_Quad
     WRITE(29,*) ' NB_Quad=',Surf%NB_Quad
     Surf%NB_Tri = 0

  ENDIF

  DEALLOCATE(edgeAng, MarkEdge, MarkTri)
  CALL HeapTree_Clear(Heap)

  RETURN  
END SUBROUTINE SuperPatch_Merge_Quad



