!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!******************************************************************************
!>
!!     Break large edges which are searched by triangles.                                             
!!     Reminder : egde system must be built in advance ( i.e. CALL Surf_BuildNext() ).
!<
!******************************************************************************
SUBROUTINE Surf_Refine(RegionID, Tri3D)
  USE Spacing_Parameters
  USE SurfaceMeshStorage
  USE array_allocator
  IMPLICIT NONE
  INTEGER, INTENT(IN)                :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D

  INTEGER :: IP, IT, i, j, itnb, ip2, ip3, IB, Isucc, Kstr, Kstr1
  INTEGER :: NB_old, Idou, IP_old, IP_older
  REAL*8  :: p2(3), p3(3), p0(3)
  REAL*8  :: RAsq, RAfac, RAfac0, RAmax, fMap(3,3), Scalar

  Kstr    = 1          !--- consider a stretching domaion
  IF(Kstr==0 .OR. BGSpacing%Model>0)THEN
     Kstr1 = 0
  ELSE
     Kstr1 = 1
  ENDIF
  Idou    = 3
  RAfac0  = 2.0d0
  Rafac   = 1.5d0 * BGSpacing%BasicSize
  DO i=1,Idou
     RAfac = RAfac * RAfac0
  ENDDO
  IP_older = 0

  DO WHILE(Idou>=0)

     NB_old = Tri3D%NB_Tri
     IP_old = Tri3D%NB_Point
     RAmax  = 0.
     DO IT = 1,NB_old

        IF(Tri3D%IP_Tri(1,IT) <= IP_older) CYCLE     !--- a checked element.
        IF(Tri3D%IP_Tri(1,IT) >  IP_old  ) CYCLE     !--- a new generated element in this loop

        DO i=1,3
           !--- calculate edge length
           itnb = Tri3D%Next_Tri(i,IT)
           IF(itnb<=0)   CYCLE

           ip2   = Tri3D%IP_Tri(MOD(i  ,3)+1,IT)
           ip3   = Tri3D%IP_Tri(MOD(i+1,3)+1,IT)
           IF(ip2>ip3) CYCLE

           p2(:) = Tri3D%Posit(:,ip2)
           p3(:) = Tri3D%Posit(:,ip3)
           p0(:) = (p3(:)+p2(:))*0.5
           IF(Kstr/=0 .AND. BGSpacing%Model<0)THEN
              CALL SpacingStorage_GetMapping(BGSpacing, P0, fMap)
              RAsq = Mapping3D_Distance(p2, p3, fMap)
           ELSE IF(Kstr/=0 .AND. BGSpacing%Model>0)THEN
              CALL SpacingStorage_GetScale(BGSpacing, P0, Scalar)
              RAsq  = Geo3D_distance(p2,p3) / Scalar
           ELSE
              RAsq  = Geo3D_distance(p2,p3)
           ENDIF

           IF(RAmax < RAsq) RAmax = RAsq
           IF(RAsq<=RAfac) CYCLE

           !--- split this edge by inserting a new point

           Tri3D%NB_Point    = Tri3D%NB_Point +1
           IP                = Tri3D%NB_Point
           IF(IP>SIZE(Tri3D%Coord,2))THEN
              CALL allc_2Dpointer(Tri3D%Coord, 2, IP+Surf_allc_Increase)
           ENDIF
           IF(IP>SIZE(Tri3D%Posit,2))THEN
              CALL allc_2Dpointer(Tri3D%Posit, 3, IP+Surf_allc_Increase)
           ENDIF

           Tri3D%Coord(:,IP) = ( Tri3D%Coord(:,ip2) + Tri3D%Coord(:,ip3) ) / 2.d0

           CALL GetUVPointInfo0(RegionID,Tri3D%Coord(1,IP),Tri3D%Coord(2,IP),p0)
           Tri3D%Posit(:,IP) = p0

           CALL Surf_InsertPoint(Tri3D, IP, IT, Kstr1, Isucc, fMap)

           IF(Isucc==1)THEN
              EXIT
           ELSE
              WRITE(29,*)'Warning--- Surf_Refine:: fail to insert a Point'
              WRITE(29,*)'IP=',IP, ' Isucc=',Isucc
              Tri3D%NB_Point = Tri3D%NB_Point -1
           ENDIF
        ENDDO

     ENDDO

     IF(NB_old == Tri3D%NB_Tri)THEN
        Idou  = Idou-1
        RAfac = RAfac / RAfac0
        DO WHILE(RAmax<RAfac)
           Idou  = Idou-1
           RAfac = RAfac / RAfac0
        ENDDO
        IP_older = 0
     ELSE
        IP_older = IP_old
        WRITE(*,'($,a,i8,a,i8,a)')      &
             ' New tri.:', Tri3D%NB_Tri-NB_old, '  Total Tri. ',Tri3D%NB_Tri,CHAR(13)
     ENDIF

  ENDDO
  WRITE(*,*)

  RETURN
END SUBROUTINE Surf_Refine



!******************************************************************************
!>
!!     Search a cavity of a new point and split the cavity with balls.
!!
!!     @param[in,out] Tri3D  :  the triangular surface mesh.
!!     @param[in]     IP     :  the insected point.        
!!     @param[in]     IT0    :  the initial triangle in which point IP lies.
!!     @param[in]     Kstr   =1  for a stretching domain;                    \n                     
!!                           =0  for a non-stretching domain.
!!     @param[in]     fMap   :  the mapping (applied for anisotropic problems.)
!!     @param[out]    Isucc  =1  inserting done successfully.                \n
!!                           =-1 fail due to too close the boundary.         \n
!!                           =-2 fail due to too many triangles in cavity.
!!
!!     Reminder : Tri3D%Next_Tri must be built in advance, and will be updated.
!< 
!******************************************************************************
SUBROUTINE Surf_InsertPoint(Tri3D, IP, IT0, Kstr, Isucc, fMap)
  USE SurfaceMeshManager
  IMPLICIT NONE      
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  INTEGER, INTENT(IN)  :: IP, IT0, Kstr
  INTEGER, INTENT(OUT) :: Isucc
  REAL*8,  INTENT(IN)  :: fMap(3,3)
  INTEGER :: IT_Cavity(500), Nins
  INTEGER :: IP_Hull(2,500), Nside
  INTEGER :: IT_Outs(500), ID_Hull(2,500)
  INTEGER :: i, it, it1, it2, i1, i4, ip2, ip3, ip4, isd, Ns1, itnb
  REAL*8  :: P0(3), p2(3), p3(3), PM(3)
  REAL*8  :: RAfac,  RAsq
  REAL*8  :: tol1 = 1.d-8
  REAL*8  :: tol2 = 5.d-2

  !******************************************************************************
  !     Search a cavity of a new point                                     
  !     Output:  IT_Cavity(*) : a serial of triangles which compose a cavity     
  !              Nins         : the length of IT_Cavity                    
  !              IP_Hull(2,*) : two nodes of each edge of the hull         
  !              ID_Hull(2,*) : the triangle adjacent to each edge of the hull
  !                             and an index.
  !              Nside        : the length of IP_Hull, i.e. number of edges
  !              IT_Outs(*)   : neighbour triangles of the cavity          
  !                             This array is used for reseting next-system
  !******************************************************************************

  !---- Criterion of swapping

  RAfac  = 1.D0
  Isucc  = 0

  !---- Initialise a hull

  P0(:) = Tri3D%Posit(:,IP)

  it1 = IT0
  IF(it1==0)THEN
     WRITE(29,*)'Error---can not locate a point :: ip,p0=',ip,p0
     CALL Error_Stop (' Surf_InsertPoint :: ')
  ENDIF
  IT_Cavity(1) = it1

  Nside = 0
  DO i=1,3
     Nside            = Nside+1
     IP_Hull(1,Nside) = Tri3D%IP_Tri(MOD(i,3)+1,  it1)
     IP_Hull(2,Nside) = Tri3D%IP_Tri(MOD(i+1,3)+1,it1)
     ID_Hull(1,Nside) = it1
     ID_Hull(2,Nside) = i
  ENDDO

  Nins  = 1

  !---- Enlarge the hull if possible

  Ns1 = 0
  DO WHILE(Nside>Ns1)
     Ns1 = Nside
     DO isd=1,Ns1
        it1 = ID_Hull(1,isd)
        i1  = ID_Hull(2,isd)
        IF(i1<0) CYCLE
        ID_Hull(2,isd) = -i1    !--- mark a checked hull edge

        ip2 = IP_Hull(1,isd)
        ip3 = IP_Hull(2,isd)

        it2 = Tri3D%Next_Tri(i1,it1)
        IF(it2 <= 0)THEN
           !-- for a boundary edge
           p2(:) = Tri3D%Posit(:,ip2)
           p3(:) = Tri3D%Posit(:,ip3)
           IF(Kstr==0)THEN
              PM = p0
           ELSE
              PM = Mapping3D_Posit_Transf(p0,fMap,1)
              p2 = Mapping3D_Posit_Transf(p2,fMap,1)
              p3 = Mapping3D_Posit_Transf(p3,fMap,1) 
           ENDIF
           IF( Geo3D_Triangle_Area(PM,p2,p3)<tol2 *Geo3D_Distance_SQ(p2,p3) ) THEN
              !-- the point close to a boundary Edge, return with Isucc=-1
              Isucc = -1
              RETURN
           ELSE
              CYCLE
           ENDIF
        ENDIF

        DO i=1,3
           IF(Tri3D%IP_Tri(i,it2)/=ip2 .AND. Tri3D%IP_Tri(i,it2)/=ip3) i4=i
        ENDDO
        ip4   = Tri3D%IP_Tri(i4,it2)
        IF(Kstr==0)THEN
           RAsq  = Surf_Criterion_inCircle(Tri3D,it2, P0)
        ELSE
           RAsq  = Surf_Criterion_inCircle(Tri3D,it2, P0, fMap)
        ENDIF

        IF(RAsq < RAfac)THEN
           Nins = Nins+1
           IF(Nins>490)THEN
              !-- Too many triangles in a cavity, return with Isucc=-2
              Isucc = -2
              RETURN
           ENDIF
           IT_Cavity(Nins) = it2

           Nside = Nside+1
           IP_Hull(1,isd)   = ip2
           IP_Hull(2,isd)   = ip4
           IP_Hull(1,Nside) = ip4
           IP_Hull(2,Nside) = ip3
           ID_Hull(1,isd)   = it2
           ID_Hull(2,isd)   = MOD(i4,3)+1
           ID_Hull(1,Nside) = it2
           ID_Hull(2,Nside) = MOD(i4+1,3)+1
        ENDIF
     ENDDO
  ENDDO

  !--- check if urgly triangles there
  !  DO isd = 1, Nins
  !     DO Ns1 = isd+1, Nins
  !        IF(IT_Cavity(isd)==IT_Cavity(Ns1))THEN
  !           !-- urgly triangles there, return with Isucc=-3
  !           Isucc = -3
  !           RETURN
  !        ENDIF
  !     ENDDO
  !  ENDDO

  DO isd = 1, Nside
     ID_Hull(2,isd) = ABS(ID_Hull(2,isd))
     IT_Outs(isd)   = Tri3D%Next_Tri(ID_Hull(2,isd), ID_Hull(1,isd))
  ENDDO


  !******************************************************************************
  !     Split a cavity into triangles                                      
  !     Input:  IP  :  the central point of the cavity                     
  !             IT_Cavity(*) : a serial of triangles which compose the cavity    
  !                            This array will be modified                 
  !             Nins         : the length of IT_Cavity                     
  !             IP_Hull(2,*) : two nodes of each edge of the hull          
  !             ID_Hull(2,*) : the triangle adjacent to each edge of the hull
  !                            and an index.
  !             Nside        : the length of IP_Hull, i.e. number of edges 
  !             IT_Outs(*)   : neighbour triangles of the cavity           
  !                            This array is used for reseting next-system 
  !     Update:  Tri3D%IP_Tri and Tri3D%Next_Tri          
  !******************************************************************************

  DO isd = 1,Nside
     IF(isd<=Nins)THEN
        it=IT_Cavity(isd)
     ELSE
        Tri3D%NB_Tri = Tri3D%NB_Tri+1
        IF(Tri3D%NB_Tri>SIZE(Tri3D%IP_Tri,2))THEN
           CALL allc_2Dpointer( Tri3D%IP_Tri,  5,Tri3D%NB_Tri + Surf_allc_increase )
        ENDIF
        IF(Tri3D%NB_Tri>SIZE(Tri3D%Next_Tri,2))THEN
           CALL allc_2Dpointer( Tri3D%Next_Tri,3,Tri3D%NB_Tri + Surf_allc_increase )
        ENDIF
        it = Tri3D%NB_Tri
        IT_Cavity(isd) = it
     ENDIF
     Tri3D%IP_Tri(1,  it) = IP
     Tri3D%IP_Tri(2:3,it) = IP_Hull(1:2,isd)
  ENDDO

  !--- update next

  DO isd = 1,Nside
     it   = IT_Cavity(isd)
     itnb = IT_Outs(isd)
     Tri3D%Next_Tri(1,it) = itnb
     IF(itnb>0)THEN
        DO i=1,3
           IF(  Tri3D%IP_Tri(MOD(i,  3)+1,itnb)==IP_Hull(2,isd) .AND.  &
                Tri3D%IP_Tri(MOD(i+1,3)+1,itnb)==IP_Hull(1,isd) ) EXIT
        ENDDO
        IF(i>3)THEN
           WRITE(29,*) ' Error--- wrong next: , IP=',IP
           CALL Error_Stop (' Surf_InsertPoint :: ')
        ENDIF
        Tri3D%Next_Tri(i,itnb) = it
     ENDIF

     DO i = 1,Nside
        IF(i==isd) CYCLE
        itnb = IT_Cavity(i)
        IF(Tri3D%IP_Tri(2,it) == Tri3D%IP_Tri(3,itnb))THEN             
           Tri3D%Next_Tri(3,it)   = itnb
           Tri3D%Next_Tri(2,itnb) = it
           EXIT
        ENDIF
     ENDDO

     IF(i>Nside)THEN
        WRITE(29,*)' Error---miss next , IP=',IP, ' Nside=',Nside
        WRITE(29,*)'    IP_Hull(1,:)=',IP_Hull(1,1:Nside)
        WRITE(29,*)'    IP_Hull(2,:)=',IP_Hull(2,1:Nside)
        CALL Error_Stop (' Surf_InsertPoint :: ')
     ENDIF
  ENDDO

  Isucc = 1

  RETURN
END SUBROUTINE Surf_InsertPoint


!******************************************************************************
!>
!!     Break large edges with the logest edge being split first.    
!!     This might be the better way to refine the mesh than SUBROUTINE Surf_Refine().
!!     Reminder : egde system must be built in advance ( i.e. CALL Surf_BuildNext() ).
!<
!******************************************************************************
SUBROUTINE Surf_Refine2(RegionID, Tri3D)
  USE Spacing_Parameters
  USE SurfaceMeshManager
  USE array_allocator
  USE HeapTree
  IMPLICIT NONE
  INTEGER, INTENT(IN)                :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  TYPE(HeapTreeType) :: Heap

  INTEGER :: IP, IS, i, ip2, ip3, Isucc, IS_Change(500), it, j, ie, nFail
  REAL*8  :: p2(3), p3(3), p0(3)
  REAL*8  :: RAsq, RAfac,  Scalar


  Rafac   = (1.5d0 * BGSpacing%BasicSize) **2

  !--- set descending heap-tree with length (the logest edge takes the first)

  Heap%Ascending = .FALSE.
  CALL HeapTree_Allocate( Heap, Tri3D%NB_Edge )

  DO IS = 1, Tri3D%NB_Edge
     ip2   = Tri3D%IP_Edge(1,IS)
     ip3   = Tri3D%IP_Edge(2,IS)

     p2(:) = Tri3D%Posit(:,ip2)
     p3(:) = Tri3D%Posit(:,ip3)
     CALL GetEgdeLength_SQ(p2,p3, RAsq)
     IF(Tri3D%ITR_Edge(2,IS)<=0)THEN
        !--- boundary edge
        RAsq = - RAsq
     ENDIF
     CALL  HeapTree_AddValue( Heap, RAsq )
  ENDDO

  nFail = 0

  Loop_IS : DO 

     IS = Heap%toList(1)         !--- the longest edge

     IF(Tri3D%ITR_Edge(2,IS)<=0)   CALL Error_Stop ('Surf_Refine2 :: touch the boundary edge ')
     
     RAsq = Heap%v(IS)
     IF( RAsq <= RAfac ) EXIT

     DO i=1,2
        it = Tri3D%ITR_Edge(i,IS)
        DO j = 1,3
           ie = Tri3D%IEd_Tri(j,it)
           IF(Tri3D%ITR_Edge(2,ie)<=0 .AND. ABS(Heap%v(ie))>=RAsq)THEN
              RAsq = - RAsq
              CALL HeapTree_ResetNodeValue( Heap, IS, RAsq )
              CYCLE Loop_IS
           ENDIF
        ENDDO
     ENDDO

     !--- add a new point

     Tri3D%NB_Point    = Tri3D%NB_Point +1
     IP                = Tri3D%NB_Point
     IF(IP>SIZE(Tri3D%Coord,2))THEN
        CALL allc_2Dpointer(Tri3D%Coord, 2, IP+Surf_allc_Increase)
     ENDIF
     IF(IP>SIZE(Tri3D%Posit,2))THEN
        CALL allc_2Dpointer(Tri3D%Posit, 3, IP+Surf_allc_Increase)
     ENDIF

     ip2   = Tri3D%IP_Edge(1,IS)
     ip3   = Tri3D%IP_Edge(2,IS)
     Tri3D%Coord(:,IP) = ( Tri3D%Coord(:,ip2) + Tri3D%Coord(:,ip3) ) / 2.d0
     CALL GetUVPointInfo0(RegionID,Tri3D%Coord(1,IP),Tri3D%Coord(2,IP),p0)
     Tri3D%Posit(:,IP) = p0
        
     !--- split the edge
     CALL Surf_InsertPoint2(Tri3D, IP, IS, Isucc, IS_Change)
     
     IF(Isucc/=1)THEN
        WRITE(29,*)' Warning--- Surf_Refine2:: fail to insert a Point, ISucc=', Isucc
        WRITE(29,*)'    IS, IPs=',IS, Tri3D%IP_Edge(:,IS), ' Rv=',real(Heap%v(IS))
        WRITE(29,*)'    IP=',IP, '    Pt=', REAL(Tri3D%Posit(:,IP))
        Tri3D%NB_Point = Tri3D%NB_Point -1
        RAsq = - Heap%v(IS)
        CALL HeapTree_ResetNodeValue( Heap, IS, RAsq )
        nFail = nFail + 1
        IF(nFail>100) EXIT
        CYCLE
     ENDIF

     DO i = 1,500
        is = IS_Change(i)
        IF(is==0) EXIT

        ip2   = Tri3D%IP_Edge(1,IS)
        ip3   = Tri3D%IP_Edge(2,IS)
        p2(:) = Tri3D%Posit(:,ip2)
        p3(:) = Tri3D%Posit(:,ip3)

        CALL GetEgdeLength_SQ(p2,p3, RAsq)

        IF(is<=Heap%numNodes)THEN
           CALL HeapTree_ResetNodeValue( Heap, is, RAsq )
        ELSE IF(is==Heap%numNodes+1) THEN
           CALL  HeapTree_AddValue( Heap, RAsq )
        ELSE
           CALL Error_Stop (' Surf_Refine3:: is/= ')
        ENDIF

     ENDDO

  ENDDO Loop_IS

  WRITE(*,*)' '

  CALL HeapTree_Clear(Heap)

  RETURN
END SUBROUTINE Surf_Refine2



!******************************************************************************
!>
!!     Search a cavity of a new point and split the cavity with balls.
!!
!!     @param[in,out] Tri3D  :  the triangular surface mesh.
!!     @param[in]     IP     :  the insected point.        
!!     @param[in]     IS     :  the edge being splitted.
!!     @param[out]    Isucc  =1  inserting done successfully.                \n
!!                           =0  fail due to too close to an old node.       \n
!!                           =-1 fail due to too no hull.                    \n
!!                           =-2 fail due to too many triangles in cavity.   \n
!!                           =-3 fail due to something wrong.
!!     @param[out]    IS_Change : the list of new edges.
!!
!!     Reminder : egde system must be built in advance, and will be updated.
!< 
!******************************************************************************
SUBROUTINE Surf_InsertPoint2(Tri3D, IP, IS, Isucc, IS_Change)
  USE control_Parameters, ONLY : Debug_Display
  USE Spacing_Parameters
  USE SurfaceMeshManager2D
  IMPLICIT NONE      
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  INTEGER, INTENT(IN)  :: IP, IS
  INTEGER, INTENT(OUT) :: Isucc
  INTEGER, INTENT(OUT) :: IS_Change(*)
  REAL*8  :: fMap(3,3)
  INTEGER :: NinsT, Nside, NinsS, Kstr
  INTEGER :: IT_Cavity(500), IP_Hull(2,500), IS_Hull(2,500), itemp(2)
  INTEGER :: i, it, it1, it2, is1, is2, i1, i4, ip2, ip3, ip4, isd, Ns1, itnb, n
  REAL*8  :: P0(3), p2(3), p3(3), PM(3), C0(2)
  REAL*8  :: RAfac,  RAsq,  ShortLen
  REAL*8  :: tol1 = 1.d-8
  REAL*8  :: tol2 = 5.d-2

  !******************************************************************************
  !     Search a cavity of a new point                                     
  !     Output:  IT_Cavity(*) : a serial of triangles which compose a cavity     
  !              NinsT        : the length of IT_Cavity                    
  !              IP_Hull(2,*) : two nodes of each edge on the hull.
  !              IS_Hull(2,*) : the edges and index on the hull.
  !              Nside        : the length of IP_Hull, i.e. number of edges
  !              IS_Change(*) : the old edges which inside the cavity
  !              NinsS        : the length of IS_Change
  !******************************************************************************

  !---- Criterion of swapping

  RAfac    = 1.D0
  ShortLen = (0.25d0 * BGSpacing%BasicSize) **2
  Isucc    = 0

  !---- Initialise a hull

  P0(:) = Tri3D%Posit(:,IP)
  C0(:) = Tri3D%Coord(:,IP)
  IF(BGSpacing%Model<0)THEN
     CALL SpacingStorage_GetMapping(BGSpacing, P0, fMap)
     Kstr = 1
  ELSE
     Kstr = 0
  ENDIF

  Nside = 0
  DO n = 1,2
     it1 = Tri3D%ITR_Edge(n,IS)
     IF(it1==0) CALL Error_Stop ('Surf_InsertPoint2 :: try to split a boundary edge. ')
     IT_Cavity(n) = it1
     DO i=1,3
        IF(Tri3D%IED_Tri(i,it1)==IS) CYCLE
        Nside            = Nside+1
        IP_Hull(1,Nside) = Tri3D%IP_Tri(MOD(i,3)+1,  it1)
        IP_Hull(2,Nside) = Tri3D%IP_Tri(MOD(i+1,3)+1,it1)
        is1              = Tri3D%IED_Tri(i,it1)
        IS_Hull(1,Nside) = is1
        IF(Tri3D%ITR_Edge(1,is1)==it1)THEN
           IS_Hull(2,Nside) = 1
        ELSE
           IS_Hull(2,Nside) = 2
        ENDIF
     ENDDO
  ENDDO

  !---- check if new point is too close an old point
  DO isd = 1,Nside
        ip2 = IP_Hull(1,isd) 
        CALL GetEgdeLength_SQ(Tri3D%Posit(:,ip2),P0, RAsq)
        IF(RAsq<ShortLen)THEN
           ISucc = 0
            write(29,*)IP,ShortLen,RAsq
           !write(29,*)P0
           !write(29,*)C0
           !write(29,*)Nside
           !do i = 1,Nside
           !write(29,*)IP_Hull(1,i),IP_Hull(2,i)
           !end do
           !write(29,*)
           !do i = 1,Nside
           !write(29,*)IS_Hull(1,i),IS_Hull(2,i)
           !end do
           !write(29,*)
           !write(29,*)
!
           RETURN
        ENDIF
  ENDDO
  DO n = 1,2
        IF(Kstr==0)THEN
           RAsq  = Surf_Criterion_inCircle(Tri3D,IT_Cavity(n), P0)
        ELSE
           RAsq  = Surf_Criterion_inCircle(Tri3D,IT_Cavity(n), P0, fMap)
        ENDIF
        IF(RAsq >= 0.9)THEN
           ISucc = -1
           RETURN
        ENDIF
  ENDDO
        
        
  NinsT = 2
  NinsS = 1
  IS_Change(1) = IS

  !---- Enlarge the hull if possible

  Ns1 = 0
  DO WHILE(Nside>Ns1)
     Ns1 = Nside
     DO isd=1,Ns1
        is1 = IS_Hull(1,isd)
        i1  = IS_Hull(2,isd)
        ip2 = IP_Hull(1,isd)
        ip3 = IP_Hull(2,isd)

        it2 = Tri3D%ITR_Edge(3-i1,is1)
        IF(it2 <= 0) CYCLE   !-- for a boundary edge

        DO i=1,3
           IF(Tri3D%IP_Tri(i,it2)/=ip2 .AND. Tri3D%IP_Tri(i,it2)/=ip3) i4=i
        ENDDO
        ip4   = Tri3D%IP_Tri(i4,it2)

        !--- check distance
        CALL GetEgdeLength_SQ(Tri3D%Posit(:,ip4),P0, RAsq)
        IF(RAsq<ShortLen)THEN
           ISucc = 0
           RETURN
        ENDIF
        
        !--- check by UV plane

        RAsq = Surf2D_Criterion_inCircle(   &
             Tri3D%Coord(:,ip4), Tri3D%Coord(:,ip3), Tri3D%Coord(:,ip2), C0)
        IF(RAsq >= RAfac) CYCLE      

        !--- check by real plane

        IF(Kstr==0)THEN
           RAsq  = Surf_Criterion_inCircle(Tri3D,it2, P0)
        ELSE
           RAsq  = Surf_Criterion_inCircle(Tri3D,it2, P0, fMap)
        ENDIF
        IF(RAsq >= RAfac) CYCLE

        !--- expand the hull by add a triangle to the cavity

        NinsT = NinsT+1
        IF(NinsT>490)THEN
           !-- Too many triangles in a cavity, return with Isucc=-2
           Isucc = -2
           RETURN
        ENDIF
        IT_Cavity(NinsT) = it2
        DO i = 1, NinsT-1
           IF(IT_Cavity(i) == it2)THEN
              !--- cavity mess
              Isucc = -3
              RETURN
           ENDIF
        ENDDO
        

        Nside = Nside+1
        IP_Hull(1,isd)   = ip2
        IP_Hull(2,isd)   = ip4
        IP_Hull(1,Nside) = ip4
        IP_Hull(2,Nside) = ip3

        is2              = Tri3D%IED_Tri(MOD(i4,3)+1, it2)
        IS_Hull(1,isd)   = is2
        IF(Tri3D%ITR_Edge(1,is2)==it2)THEN
           IS_Hull(2,isd) = 1
        ELSE
           IS_Hull(2,isd) = 2
        ENDIF

        is2              = Tri3D%IED_Tri(MOD(i4+1,3)+1, it2)
        IS_Hull(1,Nside) = is2
        IF(Tri3D%ITR_Edge(1,is2)==it2)THEN
           IS_Hull(2,Nside) = 1
        ELSE
           IS_Hull(2,Nside) = 2
        ENDIF

        NinsS = NinsS+1
        IS_Change(NinsS) = is1

     ENDDO
  ENDDO

  IF(NinsS+3/=Nside) CALL Error_Stop (' Surf_InsertPoint2 :: NinsS+3/=Nside ')

  !--- swap IP_Hull to make a single loop

  ip2 = IP_Hull(1,1)
  DO isd = 2, Nside-1
     ip3 = IP_Hull(2,isd-1)
     IF(ip3==ip2)THEN
        !   extract pieace for the loop,  return with Isucc=-3
        Isucc = -3
        RETURN
     ENDIF
     DO Ns1 = isd, Nside
        IF(IP_Hull(1,Ns1)==ip3) EXIT
     ENDDO
     IF(Ns1==isd)THEN
        CYCLE
     ELSE IF(Ns1>Nside)THEN
        !   broken loop,  return with Isucc=-3
        Isucc = -3
        RETURN
     ENDIF
     itemp(:)       = IP_Hull(:,isd)
     IP_Hull(:,isd) = IP_Hull(:,Ns1)
     IP_Hull(:,Ns1) = itemp(:)
     itemp(:)       = IS_Hull(:,isd)
     IS_Hull(:,isd) = IS_Hull(:,Ns1)
     IS_Hull(:,Ns1) = itemp(:)
  ENDDO
  IF( IP_Hull(1,Nside)/=IP_Hull(2,Nside-1) .OR.     &
       IP_Hull(2,Nside)/=ip2) THEN
     !   broken loop,  return with Isucc=-3
     Isucc = -3
     RETURN
  ENDIF


  !******************************************************************************
  !     Split a cavity into triangles                                      
  !     Input:  IP  :  the central point of the cavity                     
  !             IT_Cavity(*) : a serial of triangles which compose the cavity    
  !                            This array will be modified                 
  !             NinsT         : the length of IT_Cavity                     
  !             IP_Hull(2,*) : two nodes of each edge of the hull          
  !             IS_Hull(2,*) : the edges and index on the hull.
  !             Nside        : the length of IP_Hull, i.e. number of edges
  !             IS_Change(*) : the old edges which inside the cavity
  !             NinsS        : the length of IS_Change
  !     Update:  Tri3D%IP_Tri,  Tri3D%IP_Edge etc.            
  !******************************************************************************

  DO isd = 1,Nside
     IF(isd<=NinsT)THEN
        it = IT_Cavity(isd)
     ELSE
        Tri3D%NB_Tri = Tri3D%NB_Tri+1
        IF(Tri3D%NB_Tri>SIZE(Tri3D%IP_Tri,2))THEN
           CALL allc_2Dpointer( Tri3D%IP_Tri,  5,Tri3D%NB_Tri + Surf_allc_increase )
        ENDIF
        IF(Tri3D%NB_Tri>SIZE(Tri3D%IED_Tri,2))THEN
           CALL allc_2Dpointer( Tri3D%IED_Tri, 3,Tri3D%NB_Tri + Surf_allc_increase )
        ENDIF
        it = Tri3D%NB_Tri
        IT_Cavity(isd) = it
     ENDIF
     Tri3D%IP_Tri(1,  it) = IP
     Tri3D%IP_Tri(2:3,it) = IP_Hull(1:2,isd)
  ENDDO

  !--- update edges

  DO isd = 1,Nside
     it   = IT_Cavity(isd)
     is1  = IS_Hull(1,isd)
     i1   = IS_Hull(2,isd)
     Tri3D%IED_Tri(1,it)    = is1
     Tri3D%ITR_Edge(i1,is1) = it

     i = isd-1
     IF(isd==1) i = Nside
     itnb = IT_Cavity(i)

     IF(isd>NinsS)THEN
        Tri3D%NB_Edge = Tri3D%NB_Edge + 1
        IF(Tri3D%NB_Edge>SIZE(Tri3D%IP_Edge,2))THEN
           CALL allc_2Dpointer(Tri3D%IP_Edge,  2, Tri3D%NB_Edge + Surf_allc_increase )
        ENDIF
        IF(Tri3D%NB_Edge>SIZE(Tri3D%ITR_Edge,2))THEN
           CALL allc_2Dpointer(Tri3D%ITR_Edge, 2, Tri3D%NB_Edge + Surf_allc_increase )
        ENDIF
        IS_Change(isd) = Tri3D%NB_Edge
     ENDIF
     is1   = IS_Change(isd)

     Tri3D%IP_Edge(:,is1)  = (/IP, Tri3D%IP_Tri(2,it)/)
     Tri3D%ITR_Edge(:,is1) = (/it, itnb/)
     Tri3D%IED_Tri(3,it)   = is1
     Tri3D%IED_Tri(2,itnb) = is1

  ENDDO

  IS_Change(Nside+1) = 0

  Isucc = 1

  RETURN
END SUBROUTINE Surf_InsertPoint2

!******************************************************************************
!>
!!     Break large edges with the logest edge being split first.    
!!     This is for super-patch treatment.
!!     Reminder : egde system must be built in advance ( i.e. CALL Surf_BuildNext() ).
!<
!******************************************************************************
SUBROUTINE SuperPatch_EdgeBreak( )
  USE control_Parameters
  USE surface_Parameters
  USE spacing_Parameters
  USE HeapTree
  USE SurfaceMeshManager
  IMPLICIT NONE
  TYPE(HeapTreeType) :: Heap

  INTEGER :: IS, i, nsplit, nswap, its(4), Isucc, it, j, ip1, ip2, ip3, ip4, is1, ITR, ITL
  REAL*8  :: RAsq, RAfac, Rsmall, p1(3), p2(3)

  WRITE(*, '(/,a)') '  .... Edge break for super-patches'
  WRITE(29,'(/,a)') '  .... Edge break for super-patches'

  Rafac   = (1.5d0 * BGSpacing%BasicSize) **2
  Rsmall  = 0.9d0* Rafac

  !--- set descending heap-tree with length (the logest edge takes the first)

  Heap%Ascending = .FALSE.
  CALL HeapTree_Allocate( Heap, Surf%NB_Edge )

  DO IS = 1, Surf%NB_Edge
     ip1   = Surf%IP_Edge(1,IS)
     ip2   = Surf%IP_Edge(2,IS)

     p1(:) = Surf%Posit(:,ip1)
     p2(:) = Surf%Posit(:,ip2)
     CALL GetEgdeLength_SQ(p1,p2, RAsq)
     IF(SuperCosmetic_Method<=2)THEN
        IF(inBaseTri(ip1)==0 .AND. inBaseTri(ip2)==0)THEN
           !--- untouched edge
           RAsq = -RAsq
        ENDIF
     ENDIF
     IF(RAsq<RAfac .AND. RAsq>0)THEN
        RAsq = Rsmall
     ENDIF
     CALL HeapTree_AddValue( Heap, RAsq )
  ENDDO

  nsplit = 0
  nswap  = 0

  Loop_IS : DO

     IS   = Heap%toList(1)         !--- the longest edge
     RAsq = Heap%v(IS)
     IF( RAsq <= RAfac ) EXIT

     !--- record two triangles involved and two would-be triangles for later
     its(1) = Surf%ITR_Edge(1, IS)
     its(2) = Surf%ITR_Edge(2, IS)
     its(3) = Surf%NB_Tri + 1
     its(4) = Surf%NB_Tri + 2

     IF(SuperCosmetic_Method<=2)THEN
        DO i=1,2
           it = Surf%ITR_Edge(i,IS)
           DO j = 1,3
              is1 = Surf%IEd_Tri(j,it)
              IF(Heap%v(is1)<0 .AND. ABS(Heap%v(is1))>=RAsq)THEN
                 !--- close to boundary
                 RAsq = Rsmall
                 CALL HeapTree_ResetNodeValue( Heap, IS, RAsq )
                 CYCLE Loop_IS
              ENDIF
           ENDDO
        ENDDO
     ENDIF

     !--- split the edge
     IF(SuperCosmetic_Method<=2)THEN
        CALL SuperPatch_SplitSingleEdge(IS)
     ELSE
        CALL SuperPatch_SplitSingleEdge3(IS)
     ENDIF

     nsplit = nsplit + 1
     IF(MOD(nsplit,2000)==0)   &
          WRITE(*,*) ' nsplit=',nsplit, ' NB_Point=',Surf%NB_Point

     !...........For computing the edges' Length..................

     DO i = 1,4
        IF(i>1)THEN
           !--- a new edge entry
           IS = Heap%numNodes+1
        ENDIF
        p1(:) = Surf%Posit(:,Surf%IP_Edge(1,IS))
        p2(:) = Surf%Posit(:,Surf%IP_Edge(2,IS))
        CALL GetEgdeLength_SQ(p1,p2, RAsq)
        IF(RAsq<Rafac) RAsq = Rsmall
        IF(i==1)THEN
           CALL HeapTree_ResetNodeValue( Heap, IS, RAsq )
        ELSE
           CALL HeapTree_AddValue( Heap, RAsq )
        ENDIF
     ENDDO

     !-----------Finally,we perform edges swapping to the neighboring edges.
     DO i = 1,4
        IF(Surf%IP_Tri(3,its(i))/=Surf%NB_Point) CALL Error_Stop ('wrong triangle  ')
        IS = Surf%IED_Tri(3,its(i))

        IF(EdgeSuperID(IS)>0) CYCLE

        CALL Surf_SwapDelaunayCheck(Surf, IS, 1, Swapping_Angle, Isucc )
        IF(Isucc==0) CYCLE

        ip1 = Surf%IP_Edge(1,IS)
        ip2 = Surf%IP_Edge(2,IS)

        CALL Surf_SwapSingleEdge(Surf, is)

        ip3 = Surf%IP_Edge(1,IS)    !--- ipR
        ip4 = Surf%IP_Edge(2,IS)    !--- ipL
        ITR = Surf%ITR_Edge(1,IS)
        ITL = Surf%ITR_Edge(2,IS)

        CALL SurfTri_CalGeom(Surf, SurfTri, ITR)
        CALL SurfTri_CalGeom(Surf, SurfTri, ITL)

        IF(SuperCosmetic_Method==3)THEN
           CALL removepart(ip1, itR)
           CALL removepart(ip2, itL)
           CALL addpart(   ip3, itR, itL)
           CALL addpart(   ip4, itL, itR)
        ENDIF

        nswap = nswap + 1

        p1(:) = Surf%Posit(:,ip3)
        p2(:) = Surf%Posit(:,ip4)
        CALL GetEgdeLength_SQ(p1,p2, RAsq)
        IF(RAsq<Rafac) RAsq = Rsmall
        CALL HeapTree_ResetNodeValue( Heap, IS, RAsq )
     ENDDO

  ENDDO Loop_IS

  WRITE(*, *)' '
  WRITE(*, *) ' nsplit=',nsplit, ' nswap=', nswap, ' NB_Edge=',Surf%NB_Edge
  WRITE(29,*)' '
  WRITE(29,*) ' nsplit=',nsplit, ' nswap=', nswap, ' NB_Edge=',Surf%NB_Edge

  CALL HeapTree_Clear(Heap)

  RETURN
END SUBROUTINE SuperPatch_EdgeBreak


!>
!!     Split an edge directly and update the geometry of relevant elements.
!!
!!     @param[in]     IS     :  the edge being splitted.
!!
!!     Reminder : egde system must be built in advance, and will be updated.
!< 
SUBROUTINE SuperPatch_SplitSingleEdge(IS)
  USE surface_Parameters
  USE SurfaceMeshManager
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: IS
  INTEGER :: ip1, ip2, ipL, ipR, itL, itR, j1L,j2L, j1R,j2R
  INTEGER :: ied1, ied2, ied3, ied4, itr1, itr2, itr3, itr4
  INTEGER :: i, isp, ITinBase
  INTEGER :: IP_New
  REAL*8  :: Pt_New(3), u

  !.......First IT IS for some data's recovery

  ip1 = Surf%IP_Edge(1,IS)
  ip2 = Surf%IP_Edge(2,IS)
  CALL Surf_GetEdgeSurround(Surf, IS, itL,itR,ipL,ipR,j2L,j1L,j2R,j1R)

  !.......Second,we performe G1 interpoLation for the new point's position.

  CALL SuperPatch_midpointposition(IS, Pt_New, ITinBase)

  !..........Now we come for the points, edges, and triangLes index updating
  !.........................................................................

  IP_New = Surf%NB_Point + 1

  ied1   = IS
  ied2   = Surf%NB_Edge + 1
  ied3   = Surf%NB_Edge + 2
  ied4   = Surf%NB_Edge + 3

  itr1   = itL
  itr2   = itR
  itr3   = Surf%NB_Tri + 1
  itr4   = Surf%NB_Tri + 2

  Surf%NB_Point = IP_New
  Surf%NB_Edge  = ied4
  Surf%NB_Tri   = itr4
  CALL allc_Surf(Surf,           itr4, IP_New, ied4 )
  CALL allc_SurfTri(SurfTri,     itr4  )
  IF(Surf%NB_Point>SIZE(inBaseTri))THEN
     CALL allc_INT_1Dpointer(inBaseTri, Surf%NB_Point + Surf_allc_increase)
  ENDIF
  IF(ied4>SIZE(EdgeSuperID,1))THEN
     CALL allc_INT_1Dpointer(EdgeSuperID, ied4 + Surf_allc_increase)
  ENDIF


  Surf%Posit(:,IP_New) = Pt_New(:)
  IF(EdgeSuperID(IS)==0) THEN
     inBaseTri(IP_New)  = ITinBase
  ELSE
     inBaseTri(IP_New)  = -EdgeSuperID(IS)
  ENDIF

  !...............................................................
  !----------Now, we come for the connection updating..............

  Surf%IP_Tri(1:3, itr1) = (/ip1, ipL, IP_New/)
  Surf%IP_Tri(1:3, itr2) = (/ipR, ip1, IP_New/)
  Surf%IP_Tri(1:3, itr3) = (/ipL, ip2, IP_New/)
  Surf%IP_Tri(1:3, itr4) = (/ip2, ipR, IP_New/)
  Surf%IP_Tri(4:5, itr3) = Surf%IP_Tri(4:5, itr1)
  Surf%IP_Tri(4:5, itr4) = Surf%IP_Tri(4:5, itr2)

  Surf%IP_Edge(:,ied1) = (/ ip1,     IP_New /)
  Surf%IP_Edge(:,ied2) = (/ IP_New,  ip2    /)
  Surf%IP_Edge(:,ied3) = (/ ipL,     IP_New /)
  Surf%IP_Edge(:,ied4) = (/ IP_New,  ipR    /)

  Surf%ITR_Edge(:,ied1) = (/itr2, itr1/)
  Surf%ITR_Edge(:,ied2) = (/itr4, itr3/)
  Surf%ITR_Edge(:,ied3) = (/itr1, itr3/)
  Surf%ITR_Edge(:,ied4) = (/itr2, itr4/)

  Surf%IED_Tri(:,itr1) = (/ ied3, ied1, j1L /)
  Surf%IED_Tri(:,itr2) = (/ ied1, ied4, j1R /)
  Surf%IED_Tri(:,itr3) = (/ ied2, ied3, j2L /)
  Surf%IED_Tri(:,itr4) = (/ ied4, ied2, j2R /)

  IF(Surf%ITR_Edge(1,j2L)==itL)THEN
     Surf%ITR_Edge(1,j2L) = itr3
  ELSE
     Surf%ITR_Edge(2,j2L) = itr3
  ENDIF
  IF(Surf%ITR_Edge(1,j2R)==itR)THEN
     Surf%ITR_Edge(1,j2R) = itr4
  ELSE
     Surf%ITR_Edge(2,j2R) = itr4
  ENDIF

  !..........Now IT IS for triangLes' normaL direction updating...
  !............................     

  CALL SurfTri_CalGeom(Surf, SurfTri, itr1)
  CALL SurfTri_CalGeom(Surf, SurfTri, itr2)
  CALL SurfTri_CalGeom(Surf, SurfTri, itr3)
  CALL SurfTri_CalGeom(Surf, SurfTri, itr4)


  !..........Now we update something reLated to ridge or corner points
  !..........edges................................

  isp = EdgeSuperID(IS)

  EdgeSuperID(ied2) = isp
  EdgeSuperID(ied3) = 0
  EdgeSuperID(ied4) = 0

  IF(isp>0)THEN
     i  = ITinBase
     CALL IntQueue_Add(SuperCurveNodes(isp), i, IP_New)
     u = 0.5d0 * (SuperCurveNodePt(isp)%V(i-1) + SuperCurveNodePt(isp)%V(i) )
     CALL Real8Queue_Add(SuperCurveNodePt(isp), i, u)
  ENDIF


  RETURN
END SUBROUTINE SuperPatch_SplitSingleEdge


SUBROUTINE SuperPatch_SplitSingleEdge3(IS)
  USE surface_Parameters
  USE SurfaceMeshManager
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: IS
  INTEGER :: ip1, ip2, ipL, ipR, itL, itR, j1L,j2L, j1R,j2R
  INTEGER :: ied1, ied2, ied3, ied4, itr1, itr2, itr3, itr4
  INTEGER :: i, isp, ITinBase, ipart
  INTEGER :: IP_New
  REAL*8  :: Pt_New(3), u


  !.......First IT IS for some data's recovery

  ip1 = Surf%IP_Edge(1,IS)
  ip2 = Surf%IP_Edge(2,IS)
  CALL Surf_GetEdgeSurround(Surf, IS, itL,itR,ipL,ipR,j2L,j1L,j2R,j1R)

  !.......Second,we performe G1 interpoLation for the new point's position.

  CALL SuperPatch_midpointposition(IS, Pt_New, ITinBase)

  !..........Now we come for the points, edges, and triangLes index updating
  !.........................................................................

  IP_New = Surf%NB_Point + 1

  ied1   = IS
  ied2   = Surf%NB_Edge + 1
  ied3   = Surf%NB_Edge + 2
  ied4   = Surf%NB_Edge + 3

  itr1   = itL
  itr2   = itR
  itr3   = Surf%NB_Tri + 1
  itr4   = Surf%NB_Tri + 2

  Surf%NB_Point = IP_New
  Surf%NB_Edge  = ied4
  Surf%NB_Tri   = itr4
  CALL allc_Surf(Surf,           itr4, IP_New, ied4 )
  CALL allc_SurfTri(SurfTri,     itr4  )
  CALL allc_SurfNode(SurfNode,   IP_New )
  IF(ied4>SIZE(EdgeSuperID,1))THEN
     CALL allc_INT_1Dpointer(EdgeSuperID, ied4 + Surf_allc_increase)
  ENDIF


  Surf%Posit(:,IP_New) = Pt_New(:)

  CALL SuperPatch_midpointgeom(IS, ip_new, Pt_New)

  !...............................................................
  !----------Now, we come for the connection updating..............

  Surf%IP_Tri(1:3, itr1) = (/ip1, ipL, IP_New/)
  Surf%IP_Tri(1:3, itr2) = (/ipR, ip1, IP_New/)
  Surf%IP_Tri(1:3, itr3) = (/ipL, ip2, IP_New/)
  Surf%IP_Tri(1:3, itr4) = (/ip2, ipR, IP_New/)
  Surf%IP_Tri(4:5, itr3) = Surf%IP_Tri(4:5, itr1)
  Surf%IP_Tri(4:5, itr4) = Surf%IP_Tri(4:5, itr2)

  Surf%IP_Edge(:,ied1) = (/ ip1,     IP_New /)
  Surf%IP_Edge(:,ied2) = (/ IP_New,  ip2    /)
  Surf%IP_Edge(:,ied3) = (/ ipL,     IP_New /)
  Surf%IP_Edge(:,ied4) = (/ IP_New,  ipR    /)

  Surf%ITR_Edge(:,ied1) = (/itr2, itr1/)
  Surf%ITR_Edge(:,ied2) = (/itr4, itr3/)
  Surf%ITR_Edge(:,ied3) = (/itr1, itr3/)
  Surf%ITR_Edge(:,ied4) = (/itr2, itr4/)

  Surf%IED_Tri(:,itr1) = (/ ied3, ied1, j1L /)
  Surf%IED_Tri(:,itr2) = (/ ied1, ied4, j1R /)
  Surf%IED_Tri(:,itr3) = (/ ied2, ied3, j2L /)
  Surf%IED_Tri(:,itr4) = (/ ied4, ied2, j2R /)

  IF(Surf%ITR_Edge(1,j2L)==itL)THEN
     Surf%ITR_Edge(1,j2L) = itr3
  ELSE
     Surf%ITR_Edge(2,j2L) = itr3
  ENDIF
  IF(Surf%ITR_Edge(1,j2R)==itR)THEN
     Surf%ITR_Edge(1,j2R) = itr4
  ELSE
     Surf%ITR_Edge(2,j2R) = itr4
  ENDIF

  !..........Now IT IS for triangLes' normaL direction updating...
  !............................     

  CALL SurfTri_CalGeom(Surf, SurfTri, itr1)
  CALL SurfTri_CalGeom(Surf, SurfTri, itr2)
  CALL SurfTri_CalGeom(Surf, SurfTri, itr3)
  CALL SurfTri_CalGeom(Surf, SurfTri, itr4)


  !..........Now we update something reLated to ridge or corner points
  !..........edges................................

  isp = EdgeSuperID(IS)

  EdgeSuperID(ied2) = isp
  EdgeSuperID(ied3) = 0
  EdgeSuperID(ied4) = 0

  !...........Updating surrounding points' data..............

  IF(SurfNode(ip2)%onRidge>0)THEN
     CALL changepart(ip2, itL, itr3)
     CALL changepart(ip2, itR, itr4)
  ENDIF
  IF(SurfNode(ipL)%onRidge>0)THEN
     CALL addpart(ipL, itL, itr3)
  ENDIF
  IF(SurfNode(ipR)%onRidge>0)THEN
     CALL addpart(ipR, itR, itr4)
  ENDIF

  IF(EdgeSuperID(IS)==0)THEN
     !.............no ridge point updating......................

     SurfNode(IP_New)%onRidge  = 0
     SurfNode(IP_New)%movable  = .TRUE.

  ELSE
     !........... ridge point updating.........

     i  = ITinBase
     CALL IntQueue_Add(SuperCurveNodes(isp), i, IP_New)
     u = 0.5d0 * (SuperCurveNodePt(isp)%V(i-1) + SuperCurveNodePt(isp)%V(i) )
     CALL Real8Queue_Add(SuperCurveNodePt(isp), i, u)

     SurfNode(IP_New)%onRidge   = 1
     SurfNode(IP_New)%movable   = .TRUE.
     SurfNode(IP_New)%numParts = 2

     CALL IntQueue_Push( SurfNode(IP_New)%PartTri(1), itr1)
     CALL IntQueue_Push( SurfNode(IP_New)%PartTri(1), itr3)
     CALL IntQueue_Push( SurfNode(IP_New)%PartTri(2), itr4)
     CALL IntQueue_Push( SurfNode(IP_New)%PartTri(2), itr2)

  ENDIF


  RETURN
END SUBROUTINE SuperPatch_SplitSingleEdge3


