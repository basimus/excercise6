!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!/////////////////////////////////////////////////////////////
!/////////////////////////////////////////////////////////////
!/////////////////////////////////////////////////////////////
!
!   IT IS a adjacent node of a ridge node IP.
!   output
!   ipart --- the part number which IT located in.
!
SUBROUTINE whichpart(IP,IT,ipart)
  USE surface_Parameters
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: IP, IT
  INTEGER, INTENT(OUT) :: ipart
  INTEGER :: i

  ipart = 0
  
  DO i = 1, SurfNode(IP)%numParts
     IF(IntQueue_Contain(SurfNode(IP)%PartTri(i), IT))THEN
        ipart = i
        EXIT
     ENDIF
  ENDDO

  IF(ipart==0)THEN
     WRITE(29,*) 'Error---ridge construction: IP=',  IP,  ' IT=',IT
     CALL Error_Stop (' whichpart :: ') 
  ENDIF

  RETURN
END SUBROUTINE whichpart

SUBROUTINE changepart(IP, IT, inew)
  USE surface_Parameters
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: IP, IT, inew
  INTEGER :: i, j

  IF(SurfNode(IP)%onRidge<=0) RETURN
 
  DO i = 1,SurfNode(IP)%numParts
     DO j = 1,SurfNode(IP)%PartTri(i)%numNodes
        IF(SurfNode(IP)%PartTri(i)%Nodes(j)==IT)THEN
           SurfNode(IP)%PartTri(i)%Nodes(j) = inew
           RETURN
        ENDIF
     ENDDO
  ENDDO

  WRITE(29,*) 'Error---ridge construction: IP=',  IP,  ' IT=',IT
  CALL Error_Stop (' changepart ::  ') 

  RETURN
END SUBROUTINE changepart


SUBROUTINE addpart(IP, IT, inew2)
  USE surface_Parameters
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: IP, IT, inew2
  INTEGER :: ipart

  IF(SurfNode(IP)%onRidge<=0) RETURN

  CALL whichpart(IP,IT,ipart)
  CALL IntQueue_Push(SurfNode(IP)%PartTri(ipart), inew2)

  RETURN
END SUBROUTINE addpart



SUBROUTINE RemovePart(IP, Iold)
  USE surface_Parameters
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: IP, Iold
  INTEGER :: i, Isucc

  IF(SurfNode(IP)%onRidge<=0) RETURN

  DO i = 1, SurfNode(IP)%numParts
     CALL IntQueue_RemoveByOrder(SurfNode(IP)%PartTri(i), iold, Isucc)
     IF(Isucc==1) RETURN
  ENDDO

  RETURN
END SUBROUTINE RemovePart



SUBROUTINE choosenormalpt(IP,IT,anormpt)
  USE surface_Parameters
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: IP, IT
  REAL*8,  INTENT(OUT) :: anormpt(3)
  INTEGER :: ipart

  IF(SurfNode(IP)%onRidge<=0)THEN
     ipart = 1
  ELSE
     CALL whichpart(IP,IT,ipart)
  ENDIF
  anormpt(:) = SurfNode(IP)%anor(:,ipart)

  RETURN
END SUBROUTINE choosenormalpt


