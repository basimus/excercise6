!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Compute the outer unit normal vector by the mean of 
!!     a list of triangles.
!!  Get surface information from module surface_Parameters.
!!  @param[in]  ms   : the number of triangles.
!!  @param[in]  its  : the list of triangles.
!!  @param[out] pns  : the unit normal vector.
!<
SUBROUTINE normp(ms,its,pn)
  USE surface_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(in)  :: ms, its(*)
  REAL*8,  INTENT(out) :: pn(3)
  INTEGER :: i, IT
  REAL*8  :: dl

  pn(:) = 0.d0
  DO i = 1,ms
     IT    = its(i)
     pn(:) = pn(:) + SurfTri(IT)%anor(:) / SurfTri(IT)%Area
  ENDDO
  dl    = dsqrt(pn(1)*pn(1)+pn(2)*pn(2)+pn(3)*pn(3))
  pn(:) = pn(:) / dl

  RETURN
END SUBROUTINE normp


!>
!!  Compute the SQUARE of the distance of two points under computaional domain.
!!  Get background information from module Spacing_Parameters.
!<
SUBROUTINE GetEgdeLength_SQ(P1,P2, RAsq)
  USE Spacing_Parameters
  USE Geometry3DAll
  IMPLICIT NONE
  REAL*8,  INTENT(in)  :: p1(3), p2(3)
  REAL*8,  INTENT(out) :: RAsq
  REAL*8  :: p0(3), Scalar, fMap(3,3)

  IF(BGSpacing%Model<0)THEN
     p0(:) = (p1(:) + p2(:)) * 0.5d0
     CALL SpacingStorage_GetMapping(BGSpacing, P0, fMap)
     RAsq = Mapping3D_Distance_SQ(p1, p2, fMap)
  ELSE IF(BGSpacing%Model>1)THEN
     p0(:) = (p1(:) + p2(:)) * 0.5d0
     CALL SpacingStorage_GetScale(BGSpacing, P0, Scalar)
     RAsq = Geo3D_Distance_SQ(p1, p2) / (Scalar*Scalar)
  ELSE
     RAsq = Geo3D_Distance_SQ(p1, p2)
  ENDIF

  RETURN
END SUBROUTINE GetEgdeLength_SQ

!>
!!  Project a point to a super-patch surface. 
!!  Get surface and super-patch information from module surface_Parameters.
!!  @param[in]  Pt    : the 3D position.
!!  @param[in]  jt    : the initial guess of based-triangle ID in which the Pt located.
!!  @param[out] Pt    : the 3D position on the surface.
!!  @param[out] jt    : the final based-triangle ID in which the Pt located.
!!  @param[out] Isucc =1 succeed.   \n
!!                    =0 fail.
!<
SUBROUTINE SuperPatch_project(Pt, jt, Isucc)
  USE control_Parameters
  USE surface_Parameters
  USE Geometry3DAll
  USE Geometry2D
  IMPLICIT NONE

  INTEGER, INTENT(INOUT)  :: jt
  INTEGER, INTENT(OUT)    :: Isucc
  REAL*8,  INTENT(INOUT)  :: Pt(3)
  INTEGER :: iVisit, icc, jt1, it, i, idt, ip3(3), ipart, ip, jtbest
  REAL*8  :: Pt2D(2), w3(3), Ps(3,3), Anor(3,3)
  REAL*8  :: UBOT, VBOT, UTOP, VTOP, wmin, ws

  iVisit = GetVisitCount()
  IF(iVisit==1) MarkVisit(:) = 0
  Isucc = 0
  jtbest = 0
  wmin = -1.d36

  !--- project pt to the based triangulation
  DO it = 1,100
     MarkVisit(jt) = iVisit
     CALL Triangle_GetWeight(jt, Pt, w3)
     idt = 1
     DO i=2,3
        IF(w3(i)<w3(idt)) idt = i
     ENDDO
     IF(w3(idt)>=0) EXIT

     IF(w3(idt)>wmin)THEN
        wmin = w3(idt)
        jtbest = jt
     ENDIF

     jt1 = SurfBase%Next_Tri(idt,jt)
     IF(jt1<=0 .OR. it==100) RETURN
     IF(MarkVisit(jt1)==iVisit)THEN
        !--- dead loop found
        IF(wmin>-1.d-2)THEN
           IF(jtbest/=jt)THEN
              jt = jtbest
              CALL Triangle_GetWeight(jt, Pt, w3)
           ENDIF
           WHERE(w3(:)<0) w3(:) = 0
           ws = w3(1) + w3(2) + w3(3)
           w3(:) = w3(:) / ws
           EXIT
        ELSE
           RETURN
        ENDIF
     ENDIF
     jt = jt1
  ENDDO

  !--- project to the geometry
  IF(SuperCosmetic_Method==1)THEN
     !--- apply regular curvature

     Pt2d(:) = w3(1)*TriBase(jt)%Coord(:,1) + w3(2)*TriBase(jt)%Coord(:,2) + w3(3)*TriBase(jt)%Coord(:,3)
     icc     = SurfBase%IP_Tri(5,jt)
     CALL GetRegionUVBox (icc, UBOT, VBOT, UTOP, VTOP)

     IF(Pt2d(1)<UBOT)THEN
        Pt2d(1) = UBOT
     ELSE IF(Pt2d(1)>UTOP)THEN
        Pt2d(1) = UTOP
     ENDIF
     IF(Pt2d(2)<VBOT)THEN
        Pt2d(2) = VBOT
     ELSE IF(Pt2d(2)>VTOP)THEN
        Pt2d(2) = VTOP
     ENDIF
     CALL GetUVPointInfo0(icc, Pt2d(1), Pt2d(2), Pt)

  ELSE IF(SuperCosmetic_Method==2)THEN
     !--- apply based triangulation

     DO i = 1,3
        ip        = SurfBase%IP_Tri(i,jt)
        Ps(:,i)   = SurfBase%Posit(:,ip)

        !--- choose normal direction
        IF(NodeBase(IP)%numParts==1)THEN
           Anor(:,i) = NodeBase(IP)%anor(:,1)
        ELSE 
           DO ipart = 1, NodeBase(IP)%numParts
              IF(IntQueue_Contain(NodeBase(IP)%PartTri(ipart), jt))THEN
                 Anor(:,i) = NodeBase(IP)%anor(:,ipart)
                 EXIT
              ENDIF
           ENDDO
           IF(ipart>NodeBase(IP)%numParts)THEN
              WRITE(29,*) ' Error--- fail to find part: IP, jt=',IP,jt
              CALL Error_Stop (' SuperPatch_project :: ')         
           ENDIF
        ENDIF
     ENDDO

     CALL CubicZone_Tri_Interpolate(Ps, Anor, w3, Pt)

  ENDIF
  Isucc = 1

END SUBROUTINE SuperPatch_project

!>
!!  Calculate the central position of an edge on a super-patch surface. 
!!  Get surface and super-patch information from module surface_Parameters.
!!  @param[in]  IS    : the edge.
!!  @param[out] Pnew  : the 3D position on the surface.
!!  @param[out] jt    : for ridge node, the position on the super-curve.
!!                      for normal node, the final based-triangle ID in which the Pnew located.
!<
SUBROUTINE SuperPatch_midpointposition(IS, pnew, jt)
  USE control_Parameters
  USE surface_Parameters
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: IS
  REAL*8,  INTENT(OUT) :: pnew(3)
  INTEGER, INTENT(OUT) :: jt

  INTEGER :: ip1, ip2, isp, Isucc, i, ip, it, k, i1, i2
  REAL*8  :: u, av1(3), av2(3)

  ip1     = Surf%IP_Edge(1,IS)
  ip2     = Surf%IP_Edge(2,IS)

  IF(EdgeSuperID(IS)==0) THEN

     IF(SuperCosmetic_Method<=2)THEN

        pnew(:) = (Surf%Posit(:,ip1) + Surf%Posit(:,ip2)) * 0.5d0
        jt = inBaseTri(ip1)
        IF(jt<=0) jt = inBaseTri(ip2)
        IF(jt<=0)THEN
           DO i = 1,2
              it = Surf%ITR_Edge(i,IS)
              k  = which_NodeinTri(IS,Surf%IED_Tri(:,it))
              ip = Surf%IP_Tri(k,it)
              jt = inBaseTri(ip)
              IF(jt>0) EXIT
           ENDDO
        ENDIF

        Isucc = 0
        IF(jt>0)  CALL SuperPatch_project(pnew, jt, Isucc)

        IF(Isucc==0) THEN
           WRITE(*, *)' Warning--- fail to project an edge middle point. IS=',IS
           WRITE(29,*)' Warning--- fail to project an edge middle point.' 
           WRITE(29,*)'     IS=',IS,' jt=', inBaseTri(ip1),  inBaseTri(ip2)   
           WRITE(29,*)'     pnew=',REAL(pnew)
        ENDIF

     ELSE

        IT  = Surf%ITR_Edge(1,IS)
        CALL choosenormalpt(ip1, IT, av1)
        CALL choosenormalpt(ip2, IT, av2)
        CALL CubicZone_Line_Interpolate(Surf%Posit(:,ip1), av1, Surf%Posit(:,ip2), av2, 0.5d0, pnew)

     ENDIF

  ELSE

     isp = EdgeSuperID(IS)
     i1  = IntQueue_Find(SuperCurveNodes(isp),ip1)
     i2  = IntQueue_Find(SuperCurveNodes(isp),ip2)
     IF(ABS(i2-i1)==1)THEN
        jt   = MAX(i1,i2)
     ELSE IF( (i1==1 .AND. ip1==SuperCurveNodes(isp)%back    &
          .AND. i2==SuperCurveNodes(isp)%numNodes-1) .OR.   &
          (i2==1 .AND. ip2==SuperCurveNodes(isp)%back    &
          .AND. i1==SuperCurveNodes(isp)%numNodes-1) )THEN
        jt = SuperCurveNodes(isp)%numNodes
     ELSE
        WRITE(29,*) 'Error---wrong layout of super-curve nodes: IS, isp=',IS,isp
        WRITE(29,*) '         ip1,ip2,i1,i2=',ip1,ip2,i1,i2
        CALL Error_Stop (' SuperPatch_midpointposition ::  ')
     ENDIF
     u = 0.5d0 * (SuperCurveNodePt(isp)%V(jt-1) + SuperCurveNodePt(isp)%V(jt) )

     CALL CurveType_Interpolate (SuperCurve(isp), 0, u, pnew)


  ENDIF

  RETURN
END SUBROUTINE SuperPatch_midpointposition

!>
!!  [Desheng's code]
SUBROUTINE SuperPatch_midpointgeom(IS, ip_new, pnew)
  USE surface_Parameters
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: IS, ip_new
  REAL*8,  INTENT(IN)  :: pnew(3)
  INTEGER :: ipart, ip1, ip2, itR, itL, isp
  REAL*8  :: vp1(3), vp2(3), v1t(3), v2t(3), w(2)
  REAL*8  :: r1, r2


  ip1 = Surf%IP_Edge(1,IS)
  ip2 = Surf%IP_Edge(2,IS)
  itR = Surf%ITR_Edge(1,IS)
  itL = Surf%ITR_Edge(2,IS)

  w(2) = 0.5d0
  w(1) = 1.d0 - w(2)

  !..........Now we update something reLated to ridge or corner points
  !..........edges................................

  IF(EdgeSuperID(IS)==0)THEN

     CALL choosenormalpt(ip1, itL, vp1)
     CALL choosenormalpt(ip2, itR, vp2)

     SurfNode(IP_New)%anor(:,1) = w(1) * vp1(:) + w(2) * vp2(:)

  ELSE
     !...........eLse ridge point updating.........

     isp = EdgeSuperID(IS)

     DO ipart = 1, 2
        IF(ipart==1)THEN
           CALL choosenormalpt(ip1, itL, vp1)
           CALL choosenormalpt(ip2, itL, vp2)
        ELSE
           CALL choosenormalpt(ip1, itR, vp1)
           CALL choosenormalpt(ip2, itR, vp2)
        ENDIF

        !.............First side of updating.........

        SurfNode(IP_New)%anor(:,ipart) = w(1) * vp1(:) + w(2) * vp2(:)

     ENDDO

  ENDIF

  RETURN
END SUBROUTINE SuperPatch_midpointgeom

!>
!!  [Desheng's code]
!!  Calculate the worst quality of the surrounding triangles if a point move to a new position.
!!  Get surface and super-patch information from module surface_Parameters.
!!  Get background information from module Spacing_Parameters.
!!  @param[in]  IP    : the point which is considered to move.
!!  @param[in]  Pnew  : the target position.
!!  @param[in]  itL,itR  : two triangles being ignorred from the quality computation.
!!  @param[out] negative  =1  negative triangle will appear if move, so do not move the point.   \n
!!                        =0  no negative triangle will appear if move, so the point can be moved.
!!  @param[out] qumin  :  the worst quality of the surrounding triangles after moving.
!<
SUBROUTINE SuperPatch_areacheck(IP, pnew, itL,itR, negative, qumin)
  USE control_Parameters
  USE surface_Parameters
  USE Spacing_Parameters
  USE Geometry3DALL
  IMPLICIT NONE

  INTEGER, INTENT(in)  :: IP, itL, itR
  REAL*8,  INTENT(in)  :: pnew(3)
  INTEGER, INTENT(out) :: negative
  REAL*8,  INTENT(out) :: qumin

  INTEGER :: i, jt, j, k, keg, jtt, jtan, jtpr, jtnx
  REAL*8  :: pk(3,3), vp(3), vpk(3), fMap(3,3), p0(3), vs(3)
  REAL*8  :: tol, dis1, dis2, dis3, dsmax, prod, qual, vpL, vpkL

!  tol = .70710678     !---- COS(45 degree)
   tol = COS(SuperCurve_RidgeAngle)

  negative = 1

  qumin = 1.d36

  jtnx = 0
  DO j = 1,3
     k   = Surf%IP_Tri(j,itL)
     keg = Surf%IED_Tri(j,itL)
     IF(Surf%ITR_Edge(1,keg)==itL)THEN
        jtt = Surf%ITR_Edge(2,keg)
     ELSE
        jtt = Surf%ITR_Edge(1,keg)
     ENDIF
     IF(k==IP)THEN
        vs(:) = SurfTri(jtt)%anor(:)
     ELSE IF(jtt/=itR)THEN
        jtnx = jtt
     ENDIF
  ENDDO
  IF(jtnx==0)THEN
     WRITE(29,*)' Error--- jtnx=0'
     WRITE(29,*)' IP, itL,itR=',IP, itL,itR
     DO j=1,3
        k   = Surf%IP_Tri(j,itL)
        keg = Surf%IED_Tri(j,itL)
        WRITE(29,*)' k,keg,jtt=',k,keg,Surf%ITR_Edge(1:2,keg)
     ENDDO
     CALL Error_Stop('SuperPatch_areacheck:: jtnx=0')
  ENDIF

  jt   = jtnx
  jtpr = itL
  Loop_i : DO  i = 1,Surf%ITR_Pt%JointLinks(IP)%numNodes     

     jtan = 0
     DO j = 1,3
        k   = Surf%IP_Tri(j,jt)
        keg = Surf%IED_Tri(j,jt)
        IF(Surf%ITR_Edge(1,keg)==jt)THEN
           jtt = Surf%ITR_Edge(2,keg)
        ELSE
           jtt = Surf%ITR_Edge(1,keg)
        ENDIF
        IF(k==IP)THEN
           pk(:, j) = pnew(:)
           IF(EdgeSuperID(keg)<=0) jtan = jtt
        ELSE
           pk(:, j) = Surf%Posit(:,k)
           IF(jtt/=jtpr)  jtnx = jtt
        ENDIF
     ENDDO

     !..........we continue to check the validity........
     !..........compute the new normal and the Area....
     vp(:)  = Geo3D_Cross_Product(pk(:,2), pk(:,1), pk(:,3))
     vpL    = Geo3D_Distance(vp)
     IF(BGSpacing%Model<0)THEN
        p0(:) = (pk(:,1) + pk(:,2) + pk(:,3)) /3.d0
        CALL SpacingStorage_GetMapping(BGSpacing, P0, fMap)
        pk(:,1) = Mapping3D_Posit_Transf(pk(:,1), fMap, 1)
        pk(:,2) = Mapping3D_Posit_Transf(pk(:,2), fMap, 1)
        pk(:,3) = Mapping3D_Posit_Transf(pk(:,3), fMap, 1)
        vpk(:)  = Geo3D_Cross_Product(pk(:,2), pk(:,1), pk(:,3))
        vpkL    = Geo3D_Distance(vpk)
     ELSE
        vpk(:) = vp
        vpkL   = vpL
     ENDIF

     qual = vpkL
     qumin = MIN(qumin,qual)

     vp = vp / vpL
     IF(jtan>0) THEN
        !.........we don't consider the ridge edges
        prod = Geo3D_Dot_Product(SurfTri(jtan)%anor(:), vp)
        IF(prod<=tol)   RETURN
        prod = Geo3D_Dot_Product(SurfTri(jt)%anor(:),   vp)
        IF(prod<=tol)   RETURN
     ENDIF

     prod = Geo3D_Dot_Product(vs, vp)
     IF(prod<=tol)   RETURN

     jtpr = jt
     jt   = jtnx
     vs   = vp
     IF(jt==itR) EXIT Loop_i
     IF(i>=Surf%ITR_Pt%JointLinks(IP)%numNodes-2) CALL Error_Stop ('SuperPatch_areacheck :: dead loop ')

  ENDDO Loop_i

  DO j = 1,3
     k   = Surf%IP_Tri(j,itR)
     IF(k==IP)THEN
        keg = Surf%IED_Tri(j,itR)
        IF(Surf%ITR_Edge(1,keg)==itR)THEN
           jtt = Surf%ITR_Edge(2,keg)
        ELSE
           jtt = Surf%ITR_Edge(1,keg)
        ENDIF
        vp(:) = SurfTri(jtt)%anor(:)
        exit
     ENDIF
  ENDDO
  
  prod = Geo3D_Dot_Product(vs, vp)
  IF(prod<=tol)   RETURN
  

  negative = 0

  RETURN
END SUBROUTINE SuperPatch_areacheck

!>
!!  Calculate the worst quality of the surrounding triangles if a point move to a noew position.
!!  Get background information from module Spacing_Parameters.
!!  @param[in]  Tri3D : the surface mesh.
!!  @param[in]  IP    : the point which is considered to move.
!!  @param[in]  Pnew  : the target position.
!!  @param[in]  itL,itrR  : two triangles being ignorred from the quality computation.
!!  @param[in]  Surface_Parameters::SurfTri   : the united normal of each triangle of Tri3D.
!!  @param[out] negative  =1  negative triangle will appear if move, so do not move the point.   \n
!!                        =0  no negative triangle will appear if move, so the point can be moved.
!!  @param[out] qumin  :  the worst quality of the surrounding triangles after moving.
!<
SUBROUTINE areachecknew(Tri3D, IP, pnew, itL,itR, negative, qumin)
  USE Spacing_Parameters
  USE Surface_Parameters, ONLY : SurfTri
  USE Geometry2D
  USE Geometry3D
  USE Queue
  USE SurfaceMeshStorage
  IMPLICIT NONE

  TYPE(SurfaceMeshStorageType), INTENT(IN) :: Tri3D
  INTEGER, INTENT(in)  :: IP, itL, itR
  REAL*8,  INTENT(in)  :: pnew(3)
  INTEGER, INTENT(out) :: negative
  REAL*8,  INTENT(out) :: qumin

  INTEGER :: i, jt, j, k
  REAL*8  :: pk(3,3), vpk(3), fMap(3,3), p0(3)
  REAL*8  :: tol, dis1, dis2, dis3, dsmax, prod, qual, vpkL

  tol = .70710678     !---- COS(45 degree)

  negative = 1

  qumin = 1000.0
  Loop_i : DO  i = 1,Tri3D%ITR_Pt%JointLinks(IP)%numNodes
     jt = Tri3D%ITR_Pt%JointLinks(IP)%Nodes(i)
     IF(jt==itL .OR.  jt==itR) CYCLE Loop_i
     DO j = 1,3
        k = Tri3D%IP_Tri(j,jt)
        IF(k==IP)THEN
           pk(:, j) = pnew(:)
        ELSE
           pk(:, j) = Tri3D%Posit(:,k)
        ENDIF
     ENDDO

     !..........we continue to check the validity........
     !..........compute the new normal and the Area....
     IF(BGSpacing%Model<0)THEN
        p0(:) = (pk(:,1) + pk(:,2) + pk(:,3)) /3.d0
        CALL SpacingStorage_GetMapping(BGSpacing, P0, fMap)
        pk(:,1) = Mapping3D_Posit_Transf(pk(:,1), fMap, 1)
        pk(:,2) = Mapping3D_Posit_Transf(pk(:,2), fMap, 1)
        pk(:,3) = Mapping3D_Posit_Transf(pk(:,3), fMap, 1)
     ENDIF
     vpk(:)= Geo3D_Cross_Product(pk(:,2), pk(:,1), pk(:,3))
     vpkL  = Geo3D_Dot_Product(vpk, SurfTri(jt)%anor)

     dis1  = Geo3D_Distance_SQ(pk(:,2), pk(:,1))
     dis2  = Geo3D_Distance_SQ(pk(:,3), pk(:,1))
     dis3  = Geo3D_Distance_SQ(pk(:,3), pk(:,2))
     dsmax = MAX(dis1,dis2,dis3)
     IF(vpkL<=dsmax*1.0e-10)  RETURN

     qual  = 2.0*1.732051*vpkL/(dis1+dis2+dis3)
     qumin = MIN(qumin,qual)

  ENDDO Loop_i

  negative = 0

  RETURN
END SUBROUTINE areachecknew

!>
!!  [Desheng's code]
!!  Calculate the worst quality of the surrounding triangles if a point move to a noew position.
!!  Get surface and super-patch information from module surface_Parameters.
!!  Get background information from module Spacing_Parameters.
!!  @param[in]  IP    : the point which is considered to move.
!!  @param[in]  Pnew  : the target position.
!!  @param[in]  itL,itrR  : two triangles being ignorred from the quality computation.
!!  @param[out] negative  =1  negative triangle will appear if move, so do not move the point.   \n
!!                        =0  no negative triangle will appear if move, so the point can be moved.
!!  @param[out] qumin  :  the worst quality of the surrounding triangles after moving.
!<
SUBROUTINE SuperFrqFlt_areacheck(IP, IPto,negative, qumin)
  USE surface_Parameters
  USE Spacing_Parameters
  USE Geometry3DALL
  IMPLICIT NONE

  INTEGER, INTENT(in)  :: IP, IPto
  INTEGER, INTENT(out) :: negative
  REAL*8,  INTENT(out) :: qumin

  INTEGER :: i, jt, jt2, j, k, IS
  REAL*8  :: pk(3,3), vpk(3), fMap(3,3), p0(3), pnew(3)
  REAL*8  :: qual, vpkL

  negative = 1
  qumin    = 1.d36
  pnew     = Surf%Posit(:,IPto)

  DO  i = 1,Surf%ITR_Pt%JointLinks(IP)%numNodes     

     jt = Surf%ITR_Pt%JointLinks(IP)%Nodes(i)
     k  = Which_NodeinTri(IP, Surf%IP_Tri(1:3,jt))
     IS = Surf%IED_Tri(k,jt)
     jt2= Surf%ITR_Edge(1,IS)
     IF(jt2==jt) jt2= Surf%ITR_Edge(2,IS)
     IF(jt2>0)THEN
        IF(Which_NodeinTri(IPto, Surf%IP_Tri(1:3,jt2))>0) CYCLE
     ENDIF
     
     DO j = 1,3
        k   = Surf%IP_Tri(j,jt)
        IF(k==IP)THEN
           pk(:, j) = pnew(:)
        ELSE
           pk(:, j) = Surf%Posit(:,k)
        ENDIF
     ENDDO

     !..........we continue to check the validity........
     !..........compute the new normal and the Area....
     vpk(:) = Geo3D_Cross_Product(pk(:,2), pk(:,1), pk(:,3))
     vpkL   = Geo3D_Distance(vpk)
     IF(BGSpacing%Model<0)THEN
        p0(:) = (pk(:,1) + pk(:,2) + pk(:,3)) /3.d0
        CALL SpacingStorage_GetMapping(BGSpacing, P0, fMap)
        pk(:,1) = Mapping3D_Posit_Transf(pk(:,1), fMap, 1)
        pk(:,2) = Mapping3D_Posit_Transf(pk(:,2), fMap, 1)
        pk(:,3) = Mapping3D_Posit_Transf(pk(:,3), fMap, 1)
        vpk(:)  = Geo3D_Cross_Product(pk(:,2), pk(:,1), pk(:,3))
        vpkL    = Geo3D_Distance(vpk)
     ENDIF
     qual = vpkL
     qumin = MIN(qumin,qual)

  ENDDO

  negative = 0

  RETURN
END SUBROUTINE SuperFrqFlt_areacheck

!>
!!  Compute the weights of the three nodes of a triangle
!!    respecting a point inside.
!!  The weight-axes of the triangle are applied here.
!!  Get surface information from module surface_Parameters.
!!  @param[in]  IT   : the triangle.
!!  @param[in]  P0   : the point.
!!  @param[out] W    : the weights.
!<
SUBROUTINE Triangle_GetWeight(IT, P0, W)
  USE surface_Parameters
  USE Geometry3D
  IMPLICIT NONE  
  INTEGER, INTENT(IN)  :: IT
  REAL*8,  INTENT(IN)  :: P0(3)
  REAL*8,  INTENT(OUT) :: w(3)
  W = Geo3D_Triangle_Axes_Weight(TriBase(IT)%WeightAxes, TriBase(IT)%WeightShift, P0)
  RETURN
END SUBROUTINE Triangle_GetWeight



