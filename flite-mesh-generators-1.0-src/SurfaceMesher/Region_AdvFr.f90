!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*-------------------------------------------------------------*
!>
!!   Generates points and triangulates by advance front method.  
!!   
!!   @param[in]  RegionID  :  the Region ID.
!!   @param[in]  BGSpacing :  the background spacing.
!!   @param[in]  Tri3D     :  the number and coordinates of nodes.
!!   @param[in]  cLoop     :  the initial loops of corners.    
!!   @param[out] Tri3D     : the triangular mesh.                    
!!                                                                              
!!
!!   Reminder : The domain can contain one or several independed doamins, 
!!              and each of them must be SIMPLY CONNECTED.
!!              A SIMPLY CONNECTED is a domain enclosed by one loop of corners.
!!              For this subroutine, two corners on the same loop
!!              can NOT based on the same node; 
!!              but is does not matter if the two corners are from different loops
!!
!<
!*-------------------------------------------------------------*
SUBROUTINE RegionType_AdvanceFront(RegionID, BGSpacing, Tri3D, cLoop, Isucc  )
  USE SpacingStorage
  USE SurfaceMeshStorage
  USE Geometry3DAll
  USE array_allocator
  USE HeapTree
  USE CornerLoop
  USE control_Parameters
  IMPLICIT NONE
  INTEGER,             INTENT(in)    :: RegionID
  TYPE(SpacingStorageType),     INTENT(in)    :: BGSpacing
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D  
  TYPE(CornerLoopType),         INTENT(in)    :: cLoop
  INTEGER, INTENT(out) ::  Isucc

  INTEGER, PARAMETER :: NEPLT =  5000      ! * Elements per output
  INTEGER, PARAMETER :: NCAND =   200      ! * Candidates to Point to add
  INTEGER, PARAMETER :: NPTAD =  1000      ! * Mesh Points to add
  INTEGER, PARAMETER :: NSFAD =   500      ! * Front Sides to add

  POINTER ( ptipfr,   ipfr(*) )
  POINTER ( ptisPrev, isPrev(*) )
  POINTER ( ptisNext, isNext(*) )
  POINTER ( ptnear,   near(*) )
  POINTER ( pthowf,   howf(*) )
  POINTER ( pthlng,   hlng(*) )
! POINTER ( ptinuv,   inuv(*) )
  INTEGER :: ipfr, isPrev, isNext, near    
  REAL*8  :: howf, hlng
  INTEGER :: ks(2,2)   , index(1000000)
  REAL*8  :: r1(3), r2(3), r3(3), rm(3), ru(3), rv(3), vs(3), vl(3) , vr(3)
  REAL*8  :: Rp(3), Rpu(3), Rpv(3), Rpuv(3), cr(3)
  REAL*8  :: fmap(3,3),pmpsave(2),u10,u20
  INTEGER :: nit, nsfr, mlfrs, mlcan,nit_md
  INTEGER :: is, kn1, kn, nsd, it, inum, ken, nd2, ics(3)
  INTEGER :: i, j, k, kp, kount, knear, in, istag
  INTEGER :: IUTOP, IVTOP,iter
  REAL*8  :: EPS, EPS1, EPS2, EPS3, UBOT, VBOT, UTOP, VTOP, BIG, TLIM, TLIMU, TLIMV
  REAL*8  :: xhg, alngt, dsiz, dsizSQ, alen, alen1, dl2, a, b, c, dlast, rsiz
  REAL*8  :: pn1(2), pn2(2), pmp(2), pdi(2), vn(2), uvNew(2), uvold(2)
  REAL*8  :: pken(2), pt(2), pt1(2), pt2(2) , u1opt,u2opt,xmp0,ymp0
  REAL*8  :: u1, u2, f1, f2, f3,dst,dist_mp2,drast,du1,du2,f1min,f2min,f3min
  REAL*8  :: b11, b12, b21, b22, det, uin1, uin2, tinc, dline, radius, h1, distf
  REAL*8  :: ang1, angl, wfar, d1, d2, di, d12, comp, scale, relax , EPS4
  REAL*8  :: DGUESS,alf,alf1,alf2,bet,bet1,bet2,ab,ar,dist_mp,xin1,yin1
  LOGICAL :: varySpace, valid
  TYPE(HeapTreeType), SAVE :: Heap
  CHARACTER(LEN=255) :: SW  

  Isucc = 0
  CALL GetRegionUVBox(RegionID, UBOT, VBOT, UTOP, VTOP)
  CALL GetRegionUVmax(RegionID, IUTOP, IVTOP)

  nit   = MAX(1000,IUTOP,IVTOP)
  nit_md= MAX(150,IUTOP,IVTOP)

  write(399,*) 'Region:',RegionID, Curvature_Type
  write(377,*) 'Region:',RegionID, Curvature_Type
  write(366,*) 'Region:',RegionID, Curvature_Type
  write(287,*) 'Region:',RegionID, Curvature_Type


  write(399,*)UBOT, VBOT, UTOP, VTOP,IUTOP, IVTOP
  ! *** parameters and tolerances for the N-R iteration

  varySpace = .FALSE.
  IF(BGSpacing%BasicSize / BGSpacing%MinSize > 1.02) varySpace = .TRUE.

  EPS   = 1.e-14
  EPS1  = 1.e-03
  EPS2  = 1.e-04
  EPS3  = 1.e-06
  EPS4  = 0.04
  BIG   =  1.e+38
  TLIM  = min(1.,0.25*(UTOP-UBOT),0.25*(VTOP-VBOT))
  TLIM  = min(0.005,0.15*(UTOP-UBOT),0.15*(VTOP-VBOT))
  DGUESS=min(.15*(UTOP-UBOT),.15*(VTOP-VBOT))


  ! *** initialize.

  nsfr = cLoop%numCorners
  Tri3D%NB_Tri = 0

  ! *** Memory allocation

  mlfrs = nsfr + NCAND
  mlcan = NCAND

  CALL allc( ptipfr,     mlfrs, 'ipfr')
  CALL allc( ptisPrev,   mlfrs, 'isPrev')
  CALL allc( ptisNext,   mlfrs, 'isNext')
  CALL allc( pthlng,   2*mlfrs, 'hlng')
  CALL allc( pthowf,   2*NCAND, 'howf' )
  CALL allc( ptnear,     NCAND, 'near' )

  DO is = 1, nsfr
     ipfr(is)   = cLoop%nodeID(is)
     isNext(is) = cLoop%next(is)
     isPrev(isNext(is)) = is
  ENDDO

  DO is = 1, nsfr
     hlng(is) = edge2DLength(is)
  ENDDO

  IF(varySpace)THEN
     ! *** orders the sides of the front according to the length and forms a heap
     Heap%numNodes  = 0
     Heap%Ascending = .TRUE.
     IF(2*nsfr>KeyLength(Heap%v))THEN
        CALL HeapTree_Allocate( Heap, 4*nsfr )
     ENDIF

     DO is = 1, nsfr
        alen = edgeOpt(is)
        CALL HeapTree_AddValue(Heap, alen)
     ENDDO
  ENDIF

      write(660,*)0
      write(660,*)' fro'

      write(660,*) nsfr,Tri3D%NB_Point,0
      write(660,*)' connectivity'
      do i =1 , nsfr
       write(660,*)i, ipfr(i),ipfr(isNext(is)),ipfr(i)
      end do
      write(660,*)' 2D'
      do i = 1 ,  Tri3D%NB_Point
       write(660,*)i,Tri3D%Coord(:,i)
      end do
      write(660,*)' 3D'
      do i = 1 ,  Tri3D%NB_Point
       write(660,'(i7,4F15.7)')i,Tri3D%Posit(:,i)
      end do
      write(660,*)' boundary'


  ! *** this is the start of a step of the advancing front method.

  Loop_nsfr : DO WHILE(nsfr>0)

     IF(varySpace) THEN
        !--- choose the worst corner to develop
        nsd = Heap%toList(1)
     ELSE
        !--- choose the last corner to develop
        nsd = nsfr
     END IF

     IF(isPrev(nsd)==isNext(isNext(nsd)))THEN
        !--- the triangle formed
        Tri3D%NB_Tri = Tri3D%NB_Tri+1
        IF(Tri3D%NB_Tri>keyLength(Tri3D%IP_Tri))THEN
           CALL allc_2Dpointer( Tri3D%IP_Tri,  5,Tri3D%NB_Tri + Surf_allc_increase )
        ENDIF
        ics(1:3) = (/nsd, isNext(nsd), isPrev(nsd)/)
        Tri3D%IP_Tri(1:3,Tri3D%NB_Tri) = ipfr(ics(1:3))

        CALL remove3Corners(ics)
        CYCLE Loop_nsfr
     ENDIF
     !--- check the loop
     is    = isNext(nsd)
     kount = 1
     DO  WHILE(is/=nsd)
        is = isNext(is)
        kount = kount + 1
        IF(kount>nsfr) EXIT
     ENDDO
     IF(kount>nsfr)THEN
        WRITE(29,*)' Error--- loop getting wrong'
        WRITE(29,*)'   NB_Tri, nsfr =',Tri3D%NB_Tri, nsfr
        WRITE(29,*)'   pr, nsd, ne, ne2= ', isPrev(nsd), nsd, isNext(nsd), isNext(isNext(nsd))
        is    = nsd
        DO j = 1, nsfr          
           IF(is>nsfr) WRITE(29,*)'  is>nsfr:',is
           i = ipfr(is)
           WRITE(77,*) REAL(Tri3D%Coord(:,i)), i, is
           is = isNext(is)
           IF(is==nsd) EXIT
        ENDDO
        CALL Error_Stop ('RegionType_AdvanceFront  ') 
     ENDIF


     xhg = hlng(nsd)
     is  = isNext(nsd)
     DO  WHILE(is/=nsd)
        xhg  = MIN(xhg,hlng(is))
        is   = isNext(is)
     ENDDO
     xhg = SQRT(xhg)

     kn1    = ipfr(nsd)
     kn     = ipfr(isNext(nsd))
     pn1(:) = Tri3D%Coord(:,kn1)
     pn2(:) = Tri3D%Coord(:,kn)
!    write(399,*) kn1,kn
!    write(399,*) pn1
!    write(399,*) pn2
     pmp(:) = 0.5*(pn2(:)+pn1(:))
     write(399,*) pn1
     write(399,*) pn2
     write(399,*) pmp
     write(399,*) Tri3D%Posit(:,kn1)
     write(399,*) Tri3D%Posit(:,kn)
     cr(:)  = 0.5*(Tri3D%Posit(:,kn)+Tri3D%Posit(:,kn1)) 


     CALL SpacingStorage_GetMappingGridSize(BGSpacing,cr(1:3),fMap)
      write(287,*)kn1,kn,fmap(1,1)

     ! *** Newton-Raphson iteration to find the "real" position of the ideal point.

     ! *** coordinates of the end points of the side in the 3D
     !     normalised space where the spacing is 1.

     r1(:) = Mapping3D_Posit_Transf(Tri3D%Posit(:,kn1), fMap, 1) 
     r2(:) = Mapping3D_Posit_Transf(Tri3D%Posit(:,kn ), fMap, 1) 

     rm(:) = 0.5*(r1(:)+r2(:))
     vs(:) = r2(:)-r1(:)

     ! *** determine the size in the normalised space of
     !     the element to be generated

     alngt = SQRT(vs(1)**2 + vs(2)**2 + vs(3)**2)
     dsiz  = MIN(   1.d+00, 2.00d+00*alngt )
     dsiz  = MAX( dsiz,     0.55d+00*alngt )
     dsiz=  max(.90,min(dsiz,1.1))

     dsizSQ= dsiz * dsiz
!       write(399,*) 'r1:',r1
!       write(399,*) 'r2:',r2
!       write(399,*) 'vs:',vs
!       write(399,*) dsiz,alngt

!
! determine stretching
!
      CALL GetUVPointInfo1(RegionID,pmp(1),pmp(2),Rp,Rpu,Rpv,Rpuv)
      alf1=sqrt(rpu(1)**2+rpu(2)**2+rpu(3)**2)
      bet1=sqrt(rpv(1)**2+rpv(2)**2+rpv(3)**2)
      CALL GetUVPointInfo1(RegionID,pn1(1),pn1(2),Rp,Rpu,Rpv,Rpuv)
      alf2=sqrt(rpu(1)**2+rpu(2)**2+rpu(3)**2)
      bet2=sqrt(rpv(1)**2+rpv(2)**2+rpv(3)**2)

      alf=.5*(alf1+alf2)+EPS1
      bet=.5*(bet1+bet2)+EPS1

      ab=sqrt(alf**2+bet**2)

  !   if(abs(alf).gt.0.and.abs(bet).gt.0) then
  !     ar= max(alf/bet,bet/alf)
  !   else
  !     ar = BIG
  !   end if
      
      alf = alf/ab
      bet = bet/ab

      ar= max(alf/bet,bet/alf)
      alf=1.
      bet=1.
     ! *** initial guess (u1,u2)

     pdi(:) = pn2(:)-pn1(:)
     alen1  = SQRT(pdi(1)*pdi(1)+pdi(2)*pdi(2))
     d12    = 1./alen1
! Check !! 
     vn(1)  = -pdi(2)*d12/alf
     vn(2)  =  pdi(1)*d12/bet
     tinc=sqrt(vn(1)*vn(1)+vn(2)*vn(2))*abs(xhg)
     du1=(vn(1)*xhg)/tinc
     du2=(vn(2)*xhg)/tinc
!    pmp(1) = pn1(1) + 0.5*vn(1)*sqrt(2.)*bet
!    pmp(2) = pn1(2) + 0.5*vn(2)*sqrt(2.)*alf
!    pmp(1)    = max( pmp(1), UBOT +EPS3)
!    pmp(1)    = min( pmp(1), UTOP -EPS3)
!    pmp(2)    = max( pmp(2), VBOT +EPS3)
!    pmp(2)    = min( pmp(2), VTOP -EPS3)
!
! compute distance from real mid-point to guessed one
!
      xmp0=pmp(1)
      ymp0=pmp(2)
      CALL GetUVPointInfo1(RegionID,xmp0,ymp0,Rp,Rpu,Rpv,Rpuv)
      r3(:) = Mapping3D_Posit_Transf(rp(1:3),  fMap, 1) 
      dist_mp=sqrt((rm(1)-r3(1))**2+(rm(2)-r3(2))**2+(rm(3)-r3(3))**2)
!     write(399,*)'MP:',r3
!     write(399,*)dist_mp
      if(dist_mp.gt.EPS1) then
!       pmpSave(:) = pmp(:)

!       pmp(:) = pn1(:)
!       tlimMP = 0.2*alen1
!       do itrial = 1, ntrialMP
!        tlimMP = tlimMP/2.
!
! newton-raphson iteration for computing better guess for mid-point
!
        tinc=min(tinc,DGUESS)
        pmp(1)=pmp(1)+du1*tinc !start from mid point
        pmp(2)=pmp(2)+du2*tinc !start from mid point
        relax=.25

!       f1min = BIG
!       f3min = BIG
        
        do it=1,nit_md
         pmp(1)    = max( pmp(1), UBOT )
         pmp(1)    = min( pmp(1), UTOP )
         pmp(2)    = max( pmp(2), VBOT )
         pmp(2)    = min( pmp(2), VTOP )
         CALL GetUVPointInfo1(RegionID,pmp(1),pmp(2),Rp,Rpu,Rpv,Rpuv)
!     write(399,*)'MP:',it,pmp(:)
!     write(399,*)'MP:',r3
         r3(:) = Mapping3D_Posit_Transf(rp(1:3),  fMap, 1) 
         ru(:) = Mapping3D_Posit_Transf(rpu(1:3),  fMap, 1) 
         rv(:) = Mapping3D_Posit_Transf(rpv(1:3),  fMap, 1) 
       ! alf=sqrt(ru(1)**2+ru(2)**2+ru(3)**2)
       ! bet=sqrt(rv(1)**2+rv(2)**2+rv(3)**2)
         alf=1.
         bet=1.
         vl(:) = r3(:)-r1(:)
         vr(:) = r3(:)-r2(:)
         rsiz=sqrt((r2(1)-r1(1))**2+(r2(2)-r1(2))**2+(r2(3)-r1(3))**2)
         dlast = vl(1)**2+vl(2)**2+vl(3)**2
         f1    = dlast-.25*rsiz**2
         drast=vr(1)**2+vr(2)**2+vr(3)**2
!     write(399,*)'distMP:',sqrt(dlast),sqrt(drast),rsiz
         f3    =drast-.25*rsiz**2
         dlast = .5*(sqrt(dlast)+sqrt(drast))
!        if(abs(f1).lt.f1min.and.abs(f3).lt.f3min) then
!
! discard iterated xmp,ymp when distance r3-rm > dist0
!

!          dist_mp2=sqrt((rm(1)-r3(1))**2+(rm(2)-r3(2))**2+(rm(3)-r3(3))**2)
!          if(dist_mp2.lt.dist_mp) then
!
!            f1min = abs(f1)
!            f3min = abs(f3)
!            pmpsave(1) = pmp(1)
!            pmpsave(2) = pmp(2)
!            dist_mp    = dist_mp2
!          end if
!        end if
         if(abs(f1).lt.EPS3.and.abs(f3).lt.EPS3) then
           Exit
         endif
         b11   = 2.*bet*(vl(1)*ru(1)+vl(2)*ru(2)+vl(3)*ru(3))
         b12   = 2.*alf*(vl(1)*rv(1)+vl(2)*rv(2)+vl(3)*rv(3))
         b21   = 2.*bet*(vr(1)*ru(1)+vr(2)*ru(2)+vr(3)*ru(3))
         b22   = 2.*alf*(vr(1)*rv(1)+vr(2)*rv(2)+vr(3)*rv(3))
         det   = b11*b22-b12*b21
         if(abs(det).lt.EPS) then
!          pmpsave(1) = pmp(1)
!          pmpsave(2) = pmp(2)
           Exit
         endif
         det   = 1./det
         xin1  = -det*( b22*f1-b12*f3)*alf
         yin1  = -det*(-b21*f1+b11*f3)*bet
         tinc  = sqrt(xin1**2+yin1**2)
         if(tinc.lt.EPS3) then
           Exit
         endif
         xin1=xin1/tinc
         yin1=yin1/tinc
         tinc  = min(tinc,relax)
         pmp(1)=pmp(1)+xin1*tinc
         pmp(2)=pmp(2)+yin1*tinc
         pmp(1)    = max( pmp(1), UBOT )
         pmp(1)    = min( pmp(1), UTOP )
         pmp(2)    = max( pmp(2), VBOT )
         pmp(2)    = min( pmp(2), VTOP )
        end do
        write(399,*)'Mid Point:', kn1,kn
        write(399,*)   it,pmp

        dist_mp2=sqrt((rm(1)-r3(1))**2+(rm(2)-r3(2))**2+(rm(3)-r3(3))**2)
        if(dist_mp2.gt.dist_mp) then
!
!  better guess for mid point discarded
!
          pmp(1)=xmp0
          pmp(2)=ymp0
        endif

!     else 
!       pmpsave(:)= pmp(:)
      endif
!
! *** initial guess (u1,u2)
!
!      pmp(:) = pmpsave(:)
!     write(399,*)'pmp:',pmp

       CALL GetUVPointInfo1(RegionID,pmp(1),pmp(2),Rp,Rpu,Rpv,Rpuv)
       alf=sqrt(rpu(1)**2+rpu(2)**2+rpu(3)**2+eps1)
       bet=sqrt(rpv(1)**2+rpv(2)**2+rpv(3)**2+eps1)
       alf  = 1 
       bet  = 1
       pdi(:) = pn2(:)-pn1(:)
       alen1  = SQRT(pdi(1)*pdi(1)+pdi(2)*pdi(2))
       d12    = 1./alen1
       vn(1)  = -pdi(2)*d12*bet/alf
       vn(2)  =  pdi(1)*d12*alf/bet
       tinc=sqrt(vn(1)*vn(1)+vn(2)*vn(2))*abs(xhg)
       du1=(vn(1)*xhg)/tinc
       du2=(vn(2)*xhg)/tinc
       tlim = min(DGUESS,tinc)
       tinc =min(tinc,DGUESS)
       u1=pmp(1)+du1*tinc
       u2=pmp(2)+du2*tinc
       u1    = max( u1, UBOT )
       u1    = min( u1, UTOP )
       u2    = max( u2, VBOT )
       u2    = min( u2, VTOP )

!      IF(u1>UTOP)THEN
!         u2 = pmp(2) + (UTOP-pmp(1)) * (u2-pmp(2)) / (u1-pmp(1))
!         u1 = UTOP
!      ELSE IF(u1<UBOT)THEN
!         u2 = pmp(2) + (UBOT-pmp(1)) * (u2-pmp(2)) / (u1-pmp(1))
!         u1 = UBOT
!      ENDIF
!      IF(u2>VTOP)THEN
!         u1 = pmp(1) + (VTOP-pmp(2)) * (u1-pmp(1)) / (u2-pmp(2))
!         u2 = VTOP
!      ELSE IF(u2<VBOT)THEN
!         u1 = pmp(1) + (VBOT-pmp(2)) * (u1-pmp(1)) / (u2-pmp(2))
!         u2 = VBOT
!      ENDIF

       f1     = BIG 
       f2     = BIG

     ! *** Newton-Raphson iteration. 

       a = vn(1) 
       b = vn(2) 
       c = (-pdi(1)*pn1(2)+pdi(2)*pn1(1))*d12

!      u1opt = u1
!      u2opt = u2
!      f1min = BIG
!      f2min = BIG
!      f3min = BIG
     u10 = BIG
     u20 = BIG
     Loop_it : DO it=1,nit
        uvold(:) = (/u1, u2/)
        CALL GetUVPointInfo1(RegionID,u1,u2,Rp,Rpu,Rpv,Rpuv)
!       write(399,*) it,u1,u2
!       write(399,'(3E14.7)') Rp
!       write(399,'(3E14.7)') Rpu
!       write(399,'(3E14.7)') Rpv
!       write(399,'(3E14.7)') Rpuv

        r3(:) = Mapping3D_Posit_Transf(rp(1:3),  fMap, 1) 
        ru(:) = Mapping3D_Posit_Transf(rpu(1:3), fMap, 1) 
        rv(:) = Mapping3D_Posit_Transf(rpv(1:3), fMap, 1)         
        vl(:) = r3(:) - r1(:)
        vr(:) = r3(:) - r2(:)
        dlast = vl(1)**2 + vl(2)**2 + vl(3)**2
        f1    = dlast - dsiz**2
        f2    = (r3(1)-rm(1))*vs(1) + (r3(2)-rm(2))*vs(2) + (r3(3)-rm(3))*vs(3)
        drast=vr(1)**2+vr(2)**2+vr(3)**2
        f3    =drast-dsiz**2
!       write(399,*)'distTR:',sqrt(dlast),sqrt(drast),dsiz
        dlast = .5*(sqrt(dlast)+sqrt(drast))
!       write(399,'(5E15.7)') f1,f2,f3,dsizSQ

 !      if(abs(f1).lt.f1min.and.abs(f3).lt.f3min) then
 !        f1min = abs(f1)
 !        f2min = abs(f2)
 !        f3min = abs(f3)
 !        u1opt = u1
 !        u2opt = u2
 !      end if

        if(abs(f1).lt.EPS3*dsiz**2.and.abs(f3).lt.EPS3*dsiz**2) EXIT Loop_it

        b11   = 2.*(vl(1)*ru(1)+vl(2)*ru(2)+vl(3)*ru(3))
        b12   = 2.*(vl(1)*rv(1)+vl(2)*rv(2)+vl(3)*rv(3))
        b21   = ru(1)*vr(1)+ru(2)*vr(2)+ru(3)*vr(3)
        b22   = rv(1)*vr(1)+rv(2)*vr(2)+rv(3)*vr(3)
        det   = b11*b22-b12*b21
        IF(ABS(det) < EPS) then
 !        u1opt = u1
 !        u2opt = u2
          EXIT Loop_it
        end if

        uin1  = alf*(-b22*f1 + b12*f2) / det
        uin2  = bet*( b21*f1 - b11*f2) / det

        tinc  = SQRT(uin1**2+uin2**2)
!       write(399,*) 'ini:', uin1,uin2
!       write(399,'(4E14.7)') tinc,EPS3,TLIM
        IF(tinc < EPS3**2) then
!         u1opt = u1
!         u2opt = u2
          EXIT Loop_it
        end if
        uin1  = uin1/tinc
        uin2  = uin2/tinc
        tinc  = min(tinc,TLIM,1.)
        u1    = u1+uin1*tinc
        u2    = u2+uin2*tinc
!       write(399,*) 'line:',a,b,c
!       write(399,*) 'new:',u1,u2
        dline = a*u1+b*u2+c
        if(dline.le.0.) then
         u1 = u1-2.0*dline*a
         u2 = u2-2.0*dline*b
       endif
       u1    = max(u1,UBOT)
       u1    = min(u1,UTOP)
       u2    = max(u2,VBOT)
       u2    = min(u2,VTOP)

!       IF(u1>UTOP)THEN
!          u2 = uvold(2) + (UTOP-uvold(1)) * (u2-uvold(2)) / (u1-uvold(1))
!          u1 = UTOP
!       ELSE IF(u1<UBOT)THEN
!          u2 = uvold(2) + (UBOT-uvold(1)) * (u2-uvold(2)) / (u1-uvold(1))
!          u1 = UBOT
!       ENDIF
!       IF(u2>VTOP)THEN
!          u1 = uvold(1) + (VTOP-uvold(2)) * (u1-uvold(1)) / (u2-uvold(2))
!          u2 = VTOP
!       ELSE IF(u2<VBOT)THEN
!          u1 = uvold(1) + (VBOT-uvold(2)) * (u1-uvold(1)) / (u2-uvold(2))
!          u2 = VBOT
!       ENDIF
!       write(399,*) 'final:',dline,u1,u2
        if(abs(u10-u1).lt.1.d-16.and.abs(u20-u2).lt.1.d-16) Exit
        u10=u1
        u20=u2

     ENDDO Loop_it

     write(399,*)'Ideal Point:', kn1,kn
     write(399,*)   it,u1,u2
     write(399,*)   rp(1),rp(2),rp(3)

     uvNew(:) = (/u1, u2/)

     ! *** loops over possible nodes and finds the closest neighbours

     inum   = 0
     radius = dsiz
     IF(ABS(dlast-dsiz) < dsiz) THEN
        h1    = 0.8*radius
     ELSE
        h1    = 2.0*radius
        r3(:) = rm(:)
     ENDIF
     h1 = h1*h1
     is = isNext(nsd)
     DO  WHILE(is/=nsd)
        ken = ipfr(is)
        is  = isNext(is)

        IF( ken==kn  .OR.  ken==kn1 ) CYCLE
        pken(:)  = Tri3D%Coord(:,ken)
        IF( a*pken(1)+b*pken(2)+c  <=  0.0 ) CYCLE
        rm(:) = Mapping3D_Posit_Transf(Tri3D%Posit(:,ken),  fMap, 1) 
        distf = (rm(1)-r3(1))**2+(rm(2)-r3(2))**2+(rm(3)-r3(3))**2
        IF( distf  >  h1 ) CYCLE

        IF( inum+1  >  mlcan ) THEN
           nd2   = mlcan+NCAND
           CALL reallc( pthowf, 2*nd2, 'howf in trian2' )
           CALL reallc( ptnear,   nd2, 'near in trian2' )
           mlcan = nd2 
        ENDIF

        DO  i=1,inum
           IF( howf(i)  <  distf ) CYCLE
           DO j=inum,i,-1
              near(j+1) = near(j)
              howf(j+1) = howf(j)
           ENDDO
           EXIT
        ENDDO
        near(i) = isPrev(is)
        howf(i) = distf

        inum  = inum+1
     ENDDO

     ! *** selects starting by the closest
     ! *** sees if this connection is possible

     valid = .FALSE.
     DO i=1,inum+1
        IF(i<=inum) THEN
           istag = near(i)
           kp    = ipfr(istag)
           pt(:) = Tri3D%Coord(:,kp)
        ELSE
           istag = 0
           kp    = 0
           pt(:) = uvNew(:)
           dst   = a*uvNew(1)+b*uvNew(2)+c
           IF( dst  <  0.001*alen1 ) EXIT
        ENDIF

        CALL TriangleValid(Tri3D, nsfr, ipfr, isNext, nsd, kp, pt, valid)                      
        IF(valid) EXIT 
     ENDDO

     IF(.NOT. valid)THEN
        ! *** The previous strategy does not succeed, then
        !     find the existing nodes that give maximum angle

        kp   = 0
        ang1 = 0.0
        is   = isNext(nsd)
        DO  WHILE(is/=nsd)
           ken   = ipfr(is)
           is    = isNext(is)

           IF( ken  ==  kn  .OR.  ken  ==  kn1 ) CYCLE
           pken(:) = Tri3D%Coord(:,ken)
           IF( a*pken(1)+b*pken(2)+c  <=  0.0 )  CYCLE

           ! *** see if this connection is possible

           CALL TriangleValid(Tri3D,  nsfr, ipfr, isNext, nsd, ken, pken, valid)   

           IF( .NOT. valid ) CYCLE
           rm(:) = Mapping3D_Posit_Transf(Tri3D%Posit(:,ken),  fMap, 1)
           angl  = Geo3D_Included_Angle(r2,rm,r1)
           IF( angl  <  ang1 ) CYCLE
           ang1  = angl
           istag = isPrev(is)
           kp    = ken
        ENDDO

        IF(kp==0) THEN
           !---- Sorry, we fail  :(
           WRITE(29,*)'Error--- no way to advance'
           WRITE(29,*)'   NB_Tri, nsfr =',Tri3D%NB_Tri, nsfr
           WRITE(29,*)'   pr, nsd, ne, ne2= ', isPrev(nsd), nsd, isNext(nsd), isNext(isNext(nsd))
           WRITE(29,*)'   Output the generated part of mesh to FailPart_*.fro'
           WRITE(29,*)'   Output present loop to fort.77 (2D) and fort.78 (3D)'
           Tri3D%NB_Surf = 1
           Tri3D%IP_Tri(5, 1:Tri3D%NB_Tri) = 1
           CALL GetRegionName(RegionID, SW)
           SW = 'FailPart_'//trim(SW)
           CALL SurfaceMeshStorage_Output (SW, len_trim(SW), Tri3D)
           is = nsd
           DO       
              i = ipfr(is)
              WRITE(77,*) REAL(Tri3D%Coord(:,i)), i, is
              WRITE(78,*) REAL(Tri3d%Posit(:,i))
              is = isNext(is)
              IF(is==nsd)THEN
                 i = ipfr(is)
                 WRITE(77,*) REAL(Tri3D%Coord(:,i)), i, is
                 WRITE(78,*) REAL(Tri3d%Posit(:,i))
                 EXIT
              ENDIF
           ENDDO
           RETURN
        ENDIF

     ENDIF


     ! *** nothing wrong with it

     knear = kp
     IF( knear  ==  0 ) THEN 
        Tri3D%NB_Point = Tri3D%NB_Point+1
        IF(Tri3D%NB_Point>keyLength(Tri3D%Coord))THEN
           CALL allc_2Dpointer(Tri3D%Coord, 2, Tri3D%NB_Point+Surf_allc_Increase)
        ENDIF
        IF(Tri3D%NB_Point>keyLength(Tri3D%Posit))THEN
           CALL allc_2Dpointer(Tri3D%Posit, 3, Tri3D%NB_Point+Surf_allc_Increase)
        ENDIF
        Tri3D%Coord(:,Tri3D%NB_Point) = uvNew(:)
        CALL GetUVPointInfo0(RegionID,uvNew(1),uvNew(2),cr(1:3))
        Tri3D%Posit(:,Tri3D%NB_Point)  = cr(:)
        write(366,*) Tri3D%NB_Point,uvNew(1:2)
        write(366,*) cr(1:3)
        knear = Tri3D%NB_Point
     ENDIF

     ! *** forms element

     Tri3D%NB_Tri = Tri3D%NB_Tri+1
     IF(Tri3D%NB_Tri>keyLength(Tri3D%IP_Tri))THEN
        CALL allc_2Dpointer( Tri3D%IP_Tri,  5,Tri3D%NB_Tri + Surf_allc_increase )
     ENDIF
     Tri3D%IP_Tri(1:3,Tri3D%NB_Tri) = (/kn1, kn, knear/)
     write(377,*) kn1,kn,knear

     IF( MOD(Tri3D%NB_Tri,NEPLT)==0) THEN
        WRITE(*,'(15x,3(a,i9))')    & 
             'NB_Tri = ',Tri3D%NB_Tri,' NB_Point = ',Tri3D%NB_Point,' nsfr = ',nsfr
        WRITE(29,'(15x,3(a,i9))')    & 
             'NB_Tri = ',Tri3D%NB_Tri,' NB_Point = ',Tri3D%NB_Point,' nsfr = ',nsfr
     ENDIF

     ! *** Updates the front

     IF( nsfr+1  >  mlfrs) THEN
        nd2   = mlfrs+NSFAD
        CALL reallc( ptipfr,    nd2, 'ipfr in trian2' )
        CALL reallc( ptisPrev,  nd2, 'isPrev')
        CALL reallc( ptisNext,  nd2, 'isNext')
        CALL reallc( pthlng,  2*nd2, 'hlng')
        mlfrs = nd2 
     ENDIF

     IF(istag==0)THEN
        CALL addCornerNode(nsd, knear)
     ELSE IF(istag==isPrev(nsd))THEN
        CALL removeCorner(nsd)
     ELSE IF(istag==isNext(isNext(nsd)))THEN
        is = isNext(nsd)
        CALL removeCorner(is)
     ELSE IF(kp==ipfr(istag))THEN
        CALL LinkCorner(nsd, istag)
     ELSE
        WRITE(29,*)'Error--- kp,istag: ',kp,istag,ipfr(istag)
        CALL Error_Stop ('RegionType_AdvanceFront  ') 
     ENDIF

     IF(varySpace)THEN
        IF(nsfr/=heap%numNodes)THEN
           WRITE(29,*)'Error--- it=',Tri3D%NB_Tri,' ips=',Tri3D%IP_Tri(1:3,Tri3D%NB_Tri)
           WRITE(29,*) '  nsfr = ',nsfr,heap%numNodes
           CALL Error_Stop ('RegionType_AdvanceFront  ') 
        ENDIF
     ENDIF

  ENDDO Loop_nsfr


  WRITE(*,'(15x,3(a,i9))')    & 
       'Tri3D%NB_Tri = ',Tri3D%NB_Tri,' nodes = ',Tri3D%NB_Point,' nsfr = ',nsfr
  WRITE(29,'(15x,3(a,i9))')    & 
       'Tri3D%NB_Tri = ',Tri3D%NB_Tri,' nodes = ',Tri3D%NB_Point,' nsfr = ',nsfr

  Isucc = 1
  RETURN

CONTAINS

  FUNCTION edgeOpt(ic) RESULT(Opt)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: ic
    INTEGER :: ip1, ip2
    REAL*8  :: Opt
    ip1  = ipfr(ic)
    ip2  = ipfr(isNext(ic))
    Opt = Geo3D_Distance_SQ(Tri3D%Posit(:,ip1), Tri3D%Posit(:,ip2)) 
    RETURN
  END FUNCTION edgeOpt

  FUNCTION edge2DLength(ic) RESULT(Opt)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: ic
    INTEGER :: ip1, ip2
    REAL*8  :: Opt
    ip1  = ipfr(ic)
    ip2  = ipfr(isNext(ic))
    Opt =  (Tri3D%Coord(1,ip2)-Tri3D%Coord(1,ip1))**2      &
         + (Tri3D%Coord(2,ip2)-Tri3D%Coord(2,ip1))**2
    RETURN
  END FUNCTION edge2DLength

  SUBROUTINE removeCorner(ic)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: ic
    INTEGER :: icfr, icnx
    REAL*8 :: alen

    icfr = isPrev(ic)
    icnx = isNext(ic)
    isNext(icfr) = icnx
    isPrev(icnx) = icfr
    hlng(icfr)   =  edge2DLength(icfr)


    IF(varySpace)THEN
       alen = edgeOpt(icfr)
       CALL HeapTree_ResetNodeValue (Heap, icfr, alen)
       alen = edgeOpt(icnx)
       CALL HeapTree_ResetNodeValue (Heap, icnx, alen)
    ENDIF

    IF(ic/=Nsfr)THEN
       icfr = isPrev(Nsfr)
       icnx = isNext(Nsfr)
       ipfr(ic) = ipfr(Nsfr)
       isPrev(ic) = icfr
       isNext(ic) = icnx
       isNext(icfr) = ic
       isPrev(icnx) = ic
       hlng(ic) = hlng(Nsfr)
    ENDIF

    Nsfr = Nsfr - 1
    IF(varySpace)THEN
       CALL HeapTree_RemoveNode( Heap, ic )
    ENDIF

  END SUBROUTINE removeCorner

  SUBROUTINE remove3Corners(ics)
    IMPLICIT NONE
    INTEGER, INTENT(inout) :: ics(3)
    INTEGER :: icfr, icnx,  ir1, ir2, ic
    REAL*8 :: alen

    DO ir1 = 1,3
       ic = ics(ir1)
       DO ir2 = ir1+1,3
          IF(ic<ics(ir2))THEN
             ic = ics(ir2)
             ics(ir2) = ics(ir1)
             ics(ir1) = ic
          ENDIF
       ENDDO

       IF(ic<Nsfr)THEN
          icfr = isPrev(Nsfr)
          icnx = isNext(Nsfr)
          ipfr(ic) = ipfr(Nsfr)
          isPrev(ic) = icfr
          isNext(ic) = icnx
          isNext(icfr) = ic
          isPrev(icnx) = ic
          hlng(ic) = hlng(Nsfr)
       ENDIF

       Nsfr = Nsfr - 1
       IF(varySpace)THEN
          CALL HeapTree_RemoveNode( Heap, ic )
       ENDIF

    ENDDO

  END SUBROUTINE remove3Corners

  SUBROUTINE addCornerNode(ic, ip)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: ic, ip
    INTEGER :: icfr, icnx, ic1, ic2

    Nsfr = Nsfr + 1
    ic1 = ic
    ic2 = isNext(ic1)
    ipfr(Nsfr)   = ip

    icfr         = isPrev(ic1)
    icnx         = isNext(ic2)
    isPrev(ic2)   = Nsfr
    isNext(Nsfr) = ic2
    isPrev(Nsfr) = ic1
    isNext(ic1)   = Nsfr

    hlng(ic1)  = edge2DLength(ic1)
    hlng(nsfr) = edge2DLength(nsfr)



    IF(varySpace)THEN
       alen = edgeOpt(ic1)
       CALL HeapTree_ResetNodeValue (Heap, ic1, alen)
       alen = edgeOpt(ic2)
       CALL HeapTree_ResetNodeValue (Heap, ic2, alen)
       alen = edgeOpt(Nsfr)
       CALL HeapTree_AddValue (Heap,  alen)
    ENDIF

  END SUBROUTINE addCornerNode

  SUBROUTINE LinkCorner(ic1, ic2)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: ic1, ic2
    INTEGER :: icfr, icnx

    Nsfr = Nsfr + 1
    ipfr(Nsfr)   = ipfr(ic2)

    icnx = isNext(ic1)
    icfr = isPrev(ic2)

    isNext(ic1) = ic2
    isPrev(ic2) = ic1
    isPrev(icnx) = Nsfr
    isNext(Nsfr) = icnx
    isPrev(Nsfr) = icfr
    isNext(icfr) = Nsfr       

    hlng(ic1)  = edge2DLength(ic1)
    hlng(nsfr) = edge2DLength(nsfr)

    IF(varySpace)THEN
       alen = edgeOpt(ic1)
       CALL HeapTree_ResetNodeValue (Heap, ic1, alen)
       alen = edgeOpt(ic2)
       CALL HeapTree_ResetNodeValue (Heap, ic2, alen)
       alen = edgeOpt(icnx)
       CALL HeapTree_ResetNodeValue (Heap, icnx, alen)
       alen = edgeOpt(Nsfr)
       CALL HeapTree_AddValue (Heap,  alen)
    ENDIF

  END SUBROUTINE LinkCorner

END SUBROUTINE RegionType_AdvanceFront


!*-----------------------------------------------------------------------*
!*    [possib] finds out whether connection whith point ip3 is possible   *
!*-----------------------------------------------------------------------*
SUBROUTINE TriangleValid(Tri3D, nsfr, ipfr, isNext, nsd, ip3, pt3, valid)
  USE SurfaceMeshStorage
  USE Geometry2D
  IMPLICIT NONE

  TYPE(SurfaceMeshStorageType), INTENT(IN) :: Tri3D 
  INTEGER, INTENT(in)  :: nsfr,  ipfr(*), isNext(*)
  INTEGER, INTENT(in)  :: nsd, ip3
  REAL*8,  INTENT(in)  :: pt3(2)
  REAL*8   :: pt1(2), pt2(2), pp(2), pq(2)
  REAL*8   :: xmn, xmx, ymn, ymx, area2, a1, a2, a3, pmb(2), asm, asm0
  REAL*8   :: s1, s2
  INTEGER  :: ip1, ip2, ipp, ipq, nex
  LOGICAL, INTENT(out) :: valid

  valid = .FALSE.
  ip1   = ipfr(nsd)
  ip2   = ipfr(isnext(nsd))
  pt1(:) = Tri3D%Coord(:,ip1)
  pt2(:) = Tri3D%Coord(:,ip2)

  ! *** box-test

  asm  = (pt2(1)-pt1(1))**2 + (pt2(2)-pt1(2))**2
  asm0 = 1.d-6  * asm
  asm  = 1.d-12 * asm
  xmn  = MIN(pt1(1),pt2(1),pt3(1))
  xmx  = MAX(pt1(1),pt2(1),pt3(1))
  ymn  = MIN(pt1(2),pt2(2),pt3(2))
  ymx  = MAX(pt1(2),pt2(2),pt3(2))
  a1   = dsqrt(asm)
  xmn  = xmn - a1
  xmx  = xmx + a1
  ymn  = ymn - a1
  ymx  = ymx + a1

  ! *** check if the front nodes are interior

  area2 = Cross_Product_2D (pt2, pt1, pt3)
  IF(area2<asm) RETURN

  nex = isnext(nsd)
  DO WHILE(nex/=nsd)
     ipp   = ipfr(nex)
     nex = isnext(nex)

     IF(ipp==ip1 .OR. ipp==ip2 .OR. ipp==ip3) CYCLE
     pp(:)   = Tri3D%Coord(:,ipp)
     IF(pp(1)<xmn .OR. pp(1)>xmx ) CYCLE
     IF(pp(2)<ymn .OR. pp(2)>ymx ) CYCLE
     a1    = Cross_Product_2D (pt2, pp, pt3)
     IF(a1<-asm) CYCLE
     a2    = Cross_Product_2D (pp, pt1, pt3)
     IF(a2<-asm) CYCLE
     a3    = area2-a1-a2
     IF(a3<-asm) CYCLE
     RETURN
  ENDDO

  ! *** equation of the mid-base : ip3 line

  pmb(:)  = 0.5*(pt1(:)+pt2(:))

  ! *** loop over the front sides : check for intersection

  nex = isnext(nsd)
  DO WHILE(nex/=nsd)
     ipp   = ipfr(nex)
     nex   = isnext(nex)
     ipq   = ipfr(nex)

     IF(ipp==ip1 .AND. ipq==ip2) CALL Error_Stop ('TriangleValid :: ipp==ip1 .AND. ipq==ip2 ') 
     IF(ipp==ip3 .OR.  ipq==ip3) CYCLE

     pp(:) = Tri3D%Coord(:,ipp)
     pq(:) = Tri3D%Coord(:,ipq)
     IF(MIN(pp(1),pq(1)) > xmx) CYCLE
     IF(MAX(pp(1),pq(1)) < xmn) CYCLE
     IF(MIN(pp(2),pq(2)) > ymx) CYCLE
     IF(MAX(pp(2),pq(2)) < ymn) CYCLE

     s1   = Cross_Product_2D (pmb, pt3, pp)
     s2   = Cross_Product_2D (pmb, pt3, pq)
     IF(s1>0  .AND.  s2>0) CYCLE
     IF(s1<0  .AND.  s2<0) CYCLE

     s1   = Cross_Product_2D (pp, pq, pmb)
     s2   = Cross_Product_2D (pp, pq, pt3)
     IF(ABS(s2)<asm)               RETURN
     IF(ip3==0 .AND. ABS(s2)<asm0) RETURN
     IF(s1>0  .AND.  s2>0) CYCLE
     IF(s1<0  .AND.  s2<0) CYCLE

     RETURN
  ENDDO

  valid = .TRUE.
  RETURN
END SUBROUTINE TriangleValid


!*---------------------------------------------------------------------*
!>                                                                     
!!   change a domain to a simpliy connected domain by add lines
!!   @param[out] Isucc = 1   successful
!!                     = 0   bug
!!                     =-1   outer loop broken
!!                     =-2   inter loop broken
!!                     =-3   can not find proper node on outer loop.
!!                     =-4   no divider.
!!                     =-5   too many dividers.  
!!                     =-6   boundary edges and dividers cross.
!<                                                                     
!*---------------------------------------------------------------------*
SUBROUTINE SimplyConnected(RegionID, BGSpacing, Tri3D, cLoop, Isucc )
  USE SpacingStorage
  USE SurfaceMeshStorage
  USE Geometry3DAll
  USE Line2DGeom
  USE CornerLoop
  IMPLICIT NONE
  INTEGER,             INTENT(in)    :: RegionID
  TYPE(SpacingStorageType),     INTENT(in)    :: BGSpacing
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D  
  TYPE(CornerLoopType), INTENT(INOUT) :: cLoop  
  INTEGER, INTENT(inout)  ::  Isucc
  INTEGER, PARAMETER :: Mesh2D_Max_NB_Point = 100000
  INTEGER :: mark(Mesh2D_Max_NB_Point)
  INTEGER :: is, iss, isin, isout, is1, is2, ip1, ip2, ipp, ipq, msize
  INTEGER :: jmEnd1(1000), jmEnd2(1000), jm0, jm1, jm0n, jm1p, k, numLink, thisMark
  INTEGER :: npadd, ncadd, ncadd1, ncadd2
  REAL*8  :: RAsq, RAfac, Rstd, Rstd1, RA, d, dmn
  REAL*8  :: s1, s2, xmn, xmx, ymn, ymx
  REAL*8  :: dv(2), dp(2), dst
  REAL*8  :: cr0(2), cr1(2), c0(2), c1(2), cr(2)
  REAL*8  :: pr0(3), pr1(3), p0(3), p1(3), pr(3)
  TYPE(Line2D) :: aLine


  Isucc = 0

  !--- length Criteria

  Rafac   = (1.5d0 * BGSpacing%BasicSize) **2
  Rstd    = BGSpacing%BasicSize **2

  !--- set previous corner

  CALL CornerLoop_setPrev(Cloop)
  msize = Cloop%numCorners

  mark(:) = 0

  !--- mark outer loop

  d = 1.d36
  DO is = 1, cLoop%numCorners
     ip1 = cLoop%nodeID(is)
     IF(Tri3D%Coord(1,ip1)<d)THEN
        d = Tri3D%Coord(1,ip1)
        iss = is
     ENDIF
  ENDDO

  dst = 1.d36
  is  = iss
  k   = 0
  DO 
     k        = k+1
     mark(is) = 1
     ip1 = cLoop%nodeID(is)
     ip2 = cLoop%nodeID(cLoop%next(is))
     dp  = Tri3D%Coord(:,ip1) - Tri3D%Coord(:,ip2)
     dst = MIN( dst, (dp(1)**2 + dp(2)**2) )

     is  = cLoop%next(is)
     IF(is==iss) EXIT
     IF(k>cLoop%numCorners)THEN
        Isucc = -1
        RETURN
     ENDIF
  ENDDO
  dst = dsqrt(dst)

  !--- search and mark inner loop

  isin = 1
  DO
     DO is = 1, cLoop%numCorners
        IF(mark(is)==0)THEN
           isin = isin + 1
           EXIT
        ENDIF
     ENDDO
     IF(is>cLoop%numCorners) EXIT

     iss = is
     k   = 0
     DO 
        k        = k+1
        mark(is) = isin
        is  = cLoop%next(is)
        IF(is==iss) EXIT
        IF(k>cLoop%numCorners)THEN
           Isucc = -2
           RETURN
        ENDIF
     ENDDO

  ENDDO

  IF(isin==1)THEN
     !--- no inner loop
     Isucc = 1
     RETURN
  ENDIF

  numLink = 0
  Loop_Big : DO WHILE(numLink<100)

     !--- search the shortest distance between the inner loops and the outer loop

     dmn = 1.d36
     isout = 0
     DO is1 = 1, cLoop%numCorners
        IF(mark(is1)/=1) CYCLE
        ip1 = cLoop%nodeID(is1)
        DO is2 = 1, cLoop%numCorners
           IF(mark(is2)==1 .OR. mark(is2)<0) CYCLE
           ip2 = cLoop%nodeID(is2)
           d   = Distance_SQ_2D(Tri3D%Coord(:,ip1), Tri3D%Coord(:,ip2))
           IF(d<dmn)THEN
              dmn   = d
              isout = is1
              isin  = is2
           ENDIF
        ENDDO
     ENDDO

     IF(isout==0) EXIT Loop_Big

     numLink         = numLink +1
     jmEnd1(numLink) = isout
     jmEnd2(numLink) = isin
     Mark(isout)     = - Mark(isout)
     Mark(isin)      = - Mark(isin)

     Loop_Small : DO WHILE(numLink<100)

        !--- search the longest distance on the inner loop
        dmn = 0
        thisMark = ABS(mark(isin))
        ip1 = cLoop%nodeID(isin)
        DO is = 1, cLoop%numCorners
           IF(mark(is)/=thisMark) CYCLE
           ip2 = cLoop%nodeID(is)
           d   = Distance_SQ_2D(Tri3D%Coord(:,ip1), Tri3D%Coord(:,ip2))
           IF(d>dmn)THEN
              dmn   = d
              is1 = is
           ENDIF
        ENDDO

        ip2   = cLoop%nodeID(is1)
        dv(:) = Tri3D%Coord(:,ip2) - Tri3D%Coord(:,ip1)
        dv(:) = dv(:) / dsqrt(dv(1)**2 + dv(2)**2)
        dv(:) = dv(:) * 2.0


        !--- search jm0 on the outer loop or other inner loops which is the closest to ip2
        dmn = 1.d36
        DO is = 1, cLoop%numCorners
           IF(mark(is)==thisMark .OR. mark(is)<0) CYCLE
           ip1 = cLoop%nodeID(is)
           dp = Tri3D%Coord(:,ip1) - Tri3D%Coord(:,ip2)
           d   = dsqrt(dp(1)**2 + dp(2)**2)
           IF(dp(1)*dv(1)+dp(2)*dv(2)<d) CYCLE
           IF(d<dmn)THEN
              dmn = d
              is2 = is
           ENDIF
        ENDDO
        IF(is2==0)THEN
           Isucc = -3
           RETURN
        ENDIF

        numLink = numLink +1
        jmEnd1(numLink) = is1
        jmEnd2(numLink) = is2
        Mark(jmEnd1(numLink)) = - Mark(jmEnd1(numLink))
        Mark(jmEnd2(numLink)) = - Mark(jmEnd2(numLink))

        WHERE(Mark(1:cLoop%numCorners)==thisMark) Mark(1:cLoop%numCorners) = 1

        IF(ABS(Mark(is2))==1)THEN
           EXIT Loop_Small
        ELSE
           isin = is2
        ENDIF

     ENDDO Loop_Small


  ENDDO Loop_Big

  IF(numLink==0)THEN
     Isucc = -4
     RETURN
  ENDIF
  IF(numLink>=100)THEN
     Isucc = -5
     RETURN
  ENDIF

  !--- check if dividers cross

  DO k = 1, numLink

     ip1   = cLoop%nodeID(jmEnd1(k))
     ip2   = cLoop%nodeID(jmEnd2(k))
     cr0(:) = Tri3D%Coord(:,ip1)
     cr1(:) = Tri3D%Coord(:,ip2)

     xmn = MIN(cr0(1),cr1(1))
     xmx = MAX(cr0(1),cr1(1))
     ymn = MIN(cr0(2),cr1(2))
     ymx = MAX(cr0(2),cr1(2))

     ! *** check if the front nodes are interior

     DO is= 1, cLoop%numCorners + numLink
        IF(is<=cLoop%numCorners)THEN
           ipp = cLoop%nodeID(is)
           ipq = cLoop%nodeID(cLoop%next(is))
        ELSE
           is1 = is - cLoop%numCorners
           IF(is1==k) CYCLE
           ipp = cLoop%nodeID(jmEnd1(is1))
           ipq = cLoop%nodeID(jmEnd2(is1))        
        ENDIF

        IF(ipp==ip1 .OR. ipp==ip2) CYCLE
        IF(ipq==ip1 .OR. ipq==ip2) CYCLE
        c0(:) = Tri3D%Coord(:,ipp)
        c1(:) = Tri3D%Coord(:,ipq)
        IF(MIN(c0(1),c1(1)) > xmx) CYCLE
        IF(MAX(c0(1),c1(1)) < xmn) CYCLE
        IF(MIN(c0(2),c1(2)) > ymx) CYCLE
        IF(MAX(c0(2),c1(2)) < ymn) CYCLE

        s1   = Cross_Product_2D (cr0, cr1, c0)
        s2   = Cross_Product_2D (cr0, cr1, c1)
        IF(s1>0  .AND.  s2>0) CYCLE
        IF(s1<0  .AND.  s2<0) CYCLE

        s1   = Cross_Product_2D (c0, c1, cr0)
        s2   = Cross_Product_2D (c0, c1, cr1)
        IF(s1>0  .AND.  s2>0) CYCLE
        IF(s1<0  .AND.  s2<0) CYCLE

        Isucc = -6
        RETURN
     ENDDO

  ENDDO

  !--- split the dividers according the computational length

  DO k = 1, numLink
     jm1 = jmEnd1(k)
     jm0 = jmEnd2(k)

     !--- duplicate the corners

     IF(cLoop%numCorners+2>msize)THEN
        msize = msize+1000
        CALL allc_CornerLoop(Cloop, msize)
     ENDIF
     jm0n                             = cLoop%next(jm0)
     cLoop%nodeID(cLoop%numCorners+1) = cLoop%nodeID(jm0)
     cLoop%next(cLoop%numCorners+1)   = jm0n
     cLoop%prev(jm0n)                 = cLoop%numCorners+1
     jm1p                             = cLoop%prev(jm1)
     cLoop%nodeID(cLoop%numCorners+2) = cLoop%nodeID(jm1)
     cLoop%next(jm1p)                 = cLoop%numCorners+2
     cLoop%prev(cLoop%numCorners+2)   = jm1p

     npadd  = Tri3D%NB_Point
     ncadd  = cLoop%numCorners+2
     ncadd1 = jm0
     ncadd2 = cLoop%numCorners+1
     IF(npadd+1>keyLength(Tri3D%Coord)) CALL Error_Stop (' npadd too big ') 
     IF(ncadd+2>2*cLoop%numCorners)            CALL Error_Stop (' ncadd might be too big ') 

     cr0 = Tri3D%Coord(:,cLoop%nodeID(jm0))
     cr1 = Tri3D%Coord(:,cLoop%nodeID(jm1))
     pr0 = Tri3D%Posit(:,cLoop%nodeID(jm0))
     pr1 = Tri3D%Posit(:,cLoop%nodeID(jm1))
     dmn = 0.0001*Distance_SQ_2D(cr0, cr1)
     DO
        CALL GetEgdeLength_SQ(Pr0, Pr1, RAsq)
        IF(RAsq<Rafac) EXIT
        Rstd1 = MIN(Rstd, RAsq/4.d0)

        c0 = cr0
        c1 = cr1
        p0 = pr0
        p1 = pr1
        DO 
           cr = (c0+c1) / 2.d0
           CALL GetUVPointInfo0(RegionID,cr(1),cr(2),pr)
           d = Distance_SQ_2D(c0, c1)
           IF(d<dmn) EXIT
           CALL GetEgdeLength_SQ(Pr0, Pr, RA)
           IF(RA>Rstd1)THEN
              c1 = cr
              p1 = pr
           ELSE
              c0 = cr
              p0 = pr
           ENDIF
        ENDDO

        npadd = npadd+1
        Tri3d%Coord(:,npadd) = cr
        Tri3d%Posit(:,npadd) = pr

        IF(ncadd+2>msize)THEN
           msize = msize+1000
           CALL allc_CornerLoop(Cloop, msize)
        ENDIF
        ncadd               = ncadd+1
        cLoop%nodeID(ncadd) = npadd
        cLoop%next(ncadd1)  = ncadd
        cLoop%prev(ncadd)   = ncadd1
        ncadd1              = ncadd

        ncadd               = ncadd+1
        cLoop%nodeID(ncadd) = npadd
        cLoop%prev(ncadd2)  = ncadd
        cLoop%next(ncadd)   = ncadd2
        ncadd2              = ncadd           

        cr0 = cr
        pr0 = pr

     ENDDO

     cLoop%next(ncadd1)             = jm1
     cLoop%prev(jm1)                = ncadd1
     cLoop%prev(ncadd2)             = cLoop%numCorners+2
     cLoop%next(cLoop%numCorners+2) = ncadd2
     cLoop%numCorners               = MAX(cLoop%numCorners+2, ncadd2)
     Tri3D%NB_Point                 = npadd
  ENDDO

  Isucc = 1

  RETURN
END SUBROUTINE SimplyConnected


