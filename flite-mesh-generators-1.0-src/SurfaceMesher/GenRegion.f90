!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*---------------------------------------------------------------------*
!>                                                                     
!!    Generates mesh points on every curvature Region.
!<                                                                     
!*---------------------------------------------------------------------*
SUBROUTINE GenRegion( )
  USE control_Parameters
  USE surface_Parameters
  USE Spacing_Parameters  
  USE SurfaceMeshManager
  USE SurfaceMeshManager2D
  USE CornerLoop
  IMPLICIT NONE
  INTEGER, PARAMETER :: Mesh2D_Max_NB_Point = 100000
  INTEGER, PARAMETER :: Mesh2D_Max_NB_Tri   = 2* Mesh2D_Max_NB_Point
  INTEGER, PARAMETER :: Mesh2D_Max_NB_Edge  = 3* Mesh2D_Max_NB_Point
  INTEGER :: nloop, LoopEnds(2,100)
  INTEGER,DIMENSION(:),   POINTER ::  map_Point

  TYPE(CornerLoopType)         :: cLoop
  TYPE(SurfaceMeshStorageType) :: Tri3D
  TYPE(SpacingStorage2DType)   :: Spacing2D

  INTEGER :: GeoType, Isucc
  INTEGER :: is, ip, n, i, nbreak, ipg, j, it
  REAL*8  :: Criterion
  REAL*8  :: u1, u2, rp(3), c1(2), c2(2)
  CHARACTER*2 :: ss
  INTEGER, DIMENSION(:  ), POINTER :: Nss_IP        !--- (*)    the number of surrounding elements of each node.
  INTEGER, DIMENSION(:  ), POINTER :: Nss_IP_ideal  !--- (*)    the ideal number of surrounding elements of each node.
  REAL*8,  DIMENSION(:,:), POINTER :: halfPts
  
  REAL*4 :: tStart, tEnd

! JWJ Start
  REAL*8 :: UBOT, VBOT, UTOP, VTOP
  INTEGER :: IUTOP, IVTOP 
  REAL*8 :: tRp(3),tRpu(3),tRpv(3),tRpuv(3)
  INTEGER :: ii, jj
  REAL*8 :: u, v

  DO is = 1, NumRegions
    write(99,*) 'Region',is
    CALL GetRegionUVBox(is, UBOT, VBOT, UTOP, VTOP)
    write(99,*) 'Range', UBOT, VBOT, UTOP, VTOP
    CALL GetRegionUVmax(is, IUTOP, IVTOP)
    write(99,*) 'I Range', IUTOP, IVTOP
    DO ii = 0, 24
      u = ii
      v = 0
      CALL GetUVPointInfo1(is,u, v,tRp,tRpu,tRpv,tRpuv)
    ENDDO
  ENDDO
!  STOP

 ! JWJ End

  ALLOCATE(Tri3D%IP_Tri(5,Mesh2D_Max_NB_Tri))
  ALLOCATE(Tri3D%Posit( 3,Mesh2D_Max_NB_Point))
  ALLOCATE(Tri3D%Coord( 2,Mesh2D_Max_NB_Point))

  CALL allc_2Dpointer(Surf%Coord, 2, SIZE(Surf%Posit,2))
  ALLOCATE( Surf%IP_Tri(5,Surf%NB_Point+20))
  IF(Surf%GridOrder>1)THEN
     ALLOCATE( Surf%IPsp_Tri(10,Surf%NB_Point+20))
  ENDIF
  ALLOCATE( Surf%IP_Quad(5,1))
  ALLOCATE( map_Point(Mesh2D_Max_NB_Point) )
  ALLOCATE( halfPts(5, Surf%NB_Point) )
  CALL allc_CornerLoop(Cloop, 2*Surf%NB_Point)  !--- extra need for trangulation
  Surf%NB_Surf = NumRegions

  numAllCurveNodes = Surf%NB_Point         !--- only curvature nodes exsit at the moment.
  ALLOCATE(InterNodes(numAllCurveNodes))

  DO is = 1,NumRegions
     CALL CPU_TIME(tStart)
     IF(RegionMark(is)==0) CYCLE

     ! JWJ Quick fix to stop waiting for Surface 110
!     if( is.ne.110 ) CYCLE
     IF(Debug_Display>0)THEN
        WRITE(*, '(/,a,i4)') '  Discretizing RegionID: ',is
        WRITE(29,'(/,a,i4)') '  Discretizing RegionID: ',is
        FLUSH(6)
        FLUSH(29)
     ENDIF

     Tri3D%NB_Point    = 0
     Tri3D%NB_Tri      = 0
     Tri3D%NB_Quad     = 0
     Tri3D%NB_BD_Point = 0
     Tri3D%NB_Edge     = 0
     Tri3D%GridOrder   = 1

     CALL RegionType_genfr( is, Surf, CurveNodes, Tri3D,   & 
          map_Point,  nloop, LoopEnds, TOLG)
     CALL CPU_TIME(tEnd)
     IF(Debug_Display>0)THEN
        WRITE(*, *) '  Generated Front: ',tEnd-tStart
        WRITE(29, *) '  Generated Front: ',tEnd-tStart
        FLUSH(6)
        FLUSH(29)
     ENDIF

     !---- GeoType = 1 then trangal mesh; =-1 the quad mesh.
     CALL GetRegionGeoType(is, GeoType)

     IF(GeoType<0)THEN
        BGSpacing%BasicSize = BGSpacing%BasicSize * 2.D0
        
        IF(MOD(Tri3D%NB_Point, 2)/=0) CALL Error_Stop ('GenRegion :: odd number ')
        Tri3D%NB_Point    = Tri3D%NB_Point / 2
        Tri3D%NB_BD_Point = Tri3D%NB_BD_Point / 2
        DO ip = 1, Tri3D%NB_Point
           n = 2*ip -1
           Tri3D%Posit(:,ip) = Tri3D%Posit(:,n)
           Tri3D%Coord(:,ip) = Tri3D%Coord(:,n)
           halfPts(1:3,  ip) = Tri3D%Posit(:,n+1)
           halfPts(4:5,  ip) = Tri3D%Coord(:,n+1)
        ENDDO
        DO n=1,nloop
           IF(MOD(LoopEnds(1,n), 2)/=1) CALL Error_Stop ('GenRegion :: loop even ')
           IF(MOD(LoopEnds(2,n), 2)/=0) CALL Error_Stop ('GenRegion :: loop odd ')
           LoopEnds(1,n) = (LoopEnds(1,n) + 1) / 2
           LoopEnds(2,n) =  LoopEnds(2,n) / 2
        ENDDO
     ENDIF
     
     write(398,*)is
     DO i = 1, Tri3D%NB_Point
        cLoop%nodeID(i) = i
        cLoop%next(i)   = i+1
  !     write(398,*) i,Tri3D%coord(:,i)
  !     write(398,*) Tri3D%Posit(:,i)
     ENDDO
     DO n=1,nloop
        cLoop%next(LoopEnds(2,n)) = LoopEnds(1,n)
     ENDDO
     cLoop%numCorners = Tri3D%NB_Point
     
     IF(Generate_Method==1) THEN

        IF(nloop==1)THEN
           Isucc = 1
        ELSE IF(nloop>=2)THEN
           CALL SimplyConnected(is, BGSpacing, Tri3D, cLoop, Isucc )
           IF(Debug_Display>2 .and. nloop>2)THEN
              INCLUDE 'GenRegion.h2'   
           endif
        ENDIF
        IF(Isucc==1)THEN
           CALL RegionType_AdvanceFront (is, BGSpacing, Tri3D, cLoop, Isucc)
           IF(Isucc==0)THEN
               WRITE(* ,*)' Error---- Fail to AdvanceFront:: is=',is
               WRITE(29,*)' Error---- Fail to AdvanceFront:: is=',is
               IF(Debug_Display>2)THEN
                  INCLUDE 'GenRegion.h2'   
                  CALL Error_Stop('GenRegion :: ')
               ENDIF
               CYCLE
           ENDIF
        ELSE
           WRITE(*,*) ' Warning---- fail to simplize: ', is, nloop, Isucc
           WRITE(29,*)' Warning---- fail to simplize: ', is, nloop, Isucc
           IF(Debug_Display>2)THEN
              INCLUDE 'GenRegion.h1'
           ENDIF
           
           CALL RegionType_trian2     (is, BGSpacing, Tri3D, cLoop%nodeID, cLoop%next)
        ENDIF

     ELSE IF(Generate_Method==2) THEN

        CALL RegionType_trian2     (is, BGSpacing, Tri3D, cLoop%nodeID, cLoop%next)

     ELSE

        !--- criterion for refining in 2D plane
        Criterion = 0
        DO i = 1, Tri3D%NB_Point
           c1(:) = Tri3D%Coord(:,cLoop%nodeID(i))
           c2(:) = Tri3D%Coord(:,cLoop%next(i))
           Criterion = MAX( Criterion, ABS(c1(1)-c2(1))+ABS(c1(2)-c2(2)) )
        ENDDO
        Criterion = 2*Criterion
        Spacing2D%BasicSize = Criterion
        Spacing2D%Model     = 1

        IF(Generate_Method==-1) THEN
           !--- triangulate the Region (2D mesh) by connecting boundary nodes
           CALL Surf2D_BoundaryConnect(Tri3D, cLoop%numCorners, cLoop%nodeID, cLoop%next)
        ELSE
           !--- triangulate the Region (2D mesh) by recover boundary edges
           CALL Surf2D_BoundaryRecovConnect(Tri3D, cLoop%numCorners, cLoop%nodeID, cLoop%next)
        ENDIF

        IF(Tri3D%NB_Tri/=Tri3D%NB_Point+2*(nloop-1)-2)THEN
           WRITE(29,*)' Error--- wrong connectivity'
           WRITE(29,*)'    NB_Tri, NB_Point, nloop=',Tri3D%NB_Tri, Tri3D%NB_Point, nloop
           CALL Error_Stop ('GenRegion :: ')
        ENDIF

        !--- refine the mesh on the parameter plane    
        CALL Surf2D_DelaunaySwap2(Tri3D)
        CALL Surf_BuildNext(Tri3D)
        CALL Surf2D_Refine(Tri3D, Spacing2D)
        WRITE(29,*)' no. of points/triangles on parameter plane :', Tri3D%NB_Point, Tri3D%NB_Tri


        !--- project new points to the surface     
        DO ip = Tri3D%NB_BD_Point +1, Tri3D%NB_Point
           u1 = Tri3D%Coord(1,ip)
           u2 = Tri3D%Coord(2,ip)
         IF(Debug_Display>2)THEN
              WRITE(1984,*) 'Points to project',is,ip,u1,u2
           ENDIF
  
           CALL GetUVPointInfo0(is,u1,u2,Rp)
         IF(Debug_Display>2)THEN
              WRITE(1984,*) 'Projected',Rp
           ENDIF

           Tri3D%Posit(:,ip) = Rp(:)
        ENDDO

        !--- Build edges
        CALL Surf_BuildEdge(Tri3D)
        
        !--- swap edges
        ALLOCATE(Nss_IP(Tri3D%NB_Point), Nss_IP_ideal(Tri3D%NB_Point))
        CALL conere( Tri3D, Nss_IP )
        CALL coneid( Tri3D, Nss_IP_ideal )
        CALL Surf_DelaunaySwap(is, Tri3D,  Nss_IP, Nss_IP_ideal, 1, 1  )
        DEALLOCATE(Nss_IP, Nss_IP_ideal)
        

        !--- refine 3D surface mesh     
        CALL Surf_Refine2(is, Tri3D)
        CALL Surf_ClearEdge(Tri3D)
     ENDIF

     WRITE(*, '(/,a,2i9)') '  Tri3D% NB_Point, NB_Tri= ', Tri3D%NB_Point, Tri3D%NB_Tri
     WRITE(29,'(/,a,2i9)') '  Tri3D% NB_Point, NB_Tri= ', Tri3D%NB_Point, Tri3D%NB_Tri

     !--- enhance the mesh
     IF(Generate_Method>0) THEN
        CALL Surf_BuildEdge(Tri3D)
        CALL Surf_Refine2(is, Tri3D)
        CALL Surf_ClearEdge(Tri3D)
     ENDIF

     CALL Region_MeshEnhance(is, Tri3D )
     
     !--- highorder
     IF(10*Tri3D%NB_Point>SIZE(map_Point,1))THEN
        CALL allc_1Dpointer(map_Point, 10*Tri3D%NB_Point+100000)
     ENDIF
     map_Point(Tri3D%NB_BD_Point+1 : ) = 0
     CALL ElasticityAdjust(is, Tri3D, map_Point )
     CALL HighOrder_Build(is, Tri3D)

     IF(GeoType<0)THEN
        !--- change back the grid size
        BGSpacing%BasicSize = BGSpacing%BasicSize / 2.D0
        
        DO ip = Tri3D%NB_Point, Tri3D%NB_BD_Point+1, -1
           n = ip + Tri3D%NB_BD_Point 
           Tri3D%Posit(:,n) = Tri3D%Posit(:,ip)
           Tri3D%Coord(:,n) = Tri3D%Coord(:,ip)
        ENDDO
        DO ip = Tri3D%NB_BD_Point, 1, -1
           n = 2*ip -1
           Tri3D%Posit(:,n)   = Tri3D%Posit(:,ip)
           Tri3D%Coord(:,n)   = Tri3D%Coord(:,ip)
           Tri3D%Posit(:,n+1) = halfPts(1:3,  ip)
           Tri3D%Coord(:,n+1) = halfPts(4:5,  ip)
        ENDDO
        DO it = 1, Tri3D%NB_Tri
           DO i = 1,3
              ip = Tri3D%IP_Tri(i,it)
              IF(ip>Tri3D%NB_BD_Point)THEN
                 Tri3D%IP_Tri(i,it) = ip + Tri3D%NB_BD_Point
              ELSE
                 Tri3D%IP_Tri(i,it) = 2*ip - 1
              ENDIF
           ENDDO
        ENDDO
        Tri3D%NB_Point    = Tri3D%NB_Point + Tri3D%NB_BD_Point
        Tri3D%NB_BD_Point = Tri3D%NB_BD_Point * 2

        CALL Surf_Merge_Quad(Tri3D, is)
     ENDIF

     !--- record to the global mesh

     DO ip = 1, Tri3D%NB_Point
         ipg = map_Point(ip)
         if(ipg==0) CYCLE
         j = InterNodes(ipg)%numPatches + 1
         if(j>10) CALL Error_Stop ('GenRegion :: j>10 ') 
         InterNodes(ipg)%numPatches = j
         InterNodes(ipg)%Patches(j) = is
         InterNodes(ipg)%Coord(:,j) = Tri3D%Coord(:,ip)
     ENDDO

     DO ip = Tri3D%NB_BD_Point +1, Tri3D%NB_Point
        IF(map_Point(ip)>0) CYCLE
        Surf%NB_Point = Surf%NB_Point + 1
        IF(Surf%NB_Point>SIZE(Surf%Posit,2))THEN
           CALL allc_2Dpointer(Surf%Posit, 3, Surf%NB_Point+100000)
           CALL allc_2Dpointer(Surf%Coord, 2, Surf%NB_Point+100000)
        ENDIF
        map_Point(ip) = Surf%NB_Point
        Surf%Posit(:,Surf%NB_Point) =  Tri3D%Posit(:,ip)
        Surf%Coord(:,Surf%NB_Point) =  Tri3D%Coord(:,ip)
     ENDDO

     IF(Tri3D%NB_Tri + Surf%NB_Tri > SIZE(Surf%IP_Tri,2))THEN
        CALL allc_2Dpointer(Surf%IP_Tri, 5, Tri3D%NB_Tri + Surf%NB_Tri +100000)
        IF(Surf%GridOrder>1)THEN
           CALL allc_2Dpointer(Surf%IPsp_Tri, 10, Tri3D%NB_Tri + Surf%NB_Tri +100000)
        ENDIF
     ENDIF
     DO n = 1, Tri3D%NB_Tri
        Surf%IP_Tri(1:3, Surf%NB_Tri+n) = map_Point(Tri3D%IP_Tri(1:3,n))
        Surf%IP_Tri(4:5, Surf%NB_Tri+n) = is
        IF(Surf%GridOrder>1)THEN
        IF(Tri3D%GridOrder>1)THEN
           Surf%IPsp_Tri(:, Surf%NB_Tri+n) = map_Point(Tri3D%IPsp_Tri(:,n))
        ELSE
           Surf%IPsp_Tri(1:3, Surf%NB_Tri+n) = Surf%IP_Tri(1:3, Surf%NB_Tri+n)
        ENDIF
        ENDIF
     ENDDO
     Surf%NB_Tri = Surf%NB_Tri + Tri3D%NB_Tri

     IF(Tri3D%NB_Quad + Surf%NB_Quad > SIZE(Surf%IP_Quad,2))THEN
        CALL allc_2Dpointer(Surf%IP_Quad, 5, Tri3D%NB_Quad + Surf%NB_Quad +100000)
     ENDIF
     DO n = 1, Tri3D%NB_Quad
        Surf%IP_Quad(1:4, Surf%NB_Quad+n) = map_Point(Tri3D%IP_Quad(1:4,n))
        Surf%IP_Quad(5,   Surf%NB_Quad+n) = is
     ENDDO
     Surf%NB_Quad = Surf%NB_Quad + Tri3D%NB_Quad

     CALL CPU_TIME(tEnd)
     WRITE(*, '(/,a,i4)') '  Finished discretizing RegionID: ',is
     WRITE(29,'(/,a,i4)') '  Finished discretizing RegionID: ',is
     
     IF(Debug_Display>0)THEN
        WRITE(*, *) '  CPU Time ',tEnd-tStart
        WRITE(29,*) '  CPU Time ',tEnd-tStart
        FLUSH(6)
        FLUSH(29)
     ENDIF
! JWJ
!     CALL SLEEP( 4 )
  ENDDO

  DEALLOCATE( halfPts )
  CALL  CornerLoop_Clear(cLoop)

  RETURN
END SUBROUTINE GenRegion



