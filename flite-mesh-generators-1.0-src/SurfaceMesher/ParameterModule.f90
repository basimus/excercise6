!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!*******************************************************************************
!>
!!  Control parameters and global data.
!<
!*******************************************************************************

MODULE control_Parameters
  USE Queue
  USE Elasticity
  IMPLICIT NONE

  !================================================  Title

  INTEGER, PARAMETER :: MaxFileNameLength = 256
  CHARACTER ( LEN = MaxFileNameLength ) :: JobName   !<  Prefix of the input files.
  INTEGER, SAVE      :: JobNameLength                !<  Length of JobName.
  INTEGER, PARAMETER :: LogCh = 77                   !<  IO unit to recode screen message.

  !================================================  NAMELIST input parameters
  !--- NAMELIST /ControlParameters/
  !--- NAMELIST /BackgroundParameters/
  !--- NAMELIST /CosmeticParameters/
  !
  !

  !>  The level of debug and display.
  !!   A bigger number indicates more checking in the code
  !!        and more output on the screen.
  INTEGER, SAVE :: Debug_Display
  !>
  !!  @param Start_Point  =1,  read surface geometry from "*.dat" file.    \n
  !!                      =2,  read surface mesh from "*_0.fro" file.
  INTEGER, SAVE :: Start_Point
  !>
  !!  @param Curvature_Type =1,  Swansea curvature type;  Read "*.dat" file.   \n
  !!                        =2,  CADfix curvature type;   Read "*.fbm" file.   \n
  !!                        =3,  Read CADfix curvature ("*.fbm" file),
  !!                             and convert it to Swansea curvature type.
  !!                             It will be reset as 1 after converting.
  INTEGER, SAVE :: Curvature_Type
  !>
  !!  @param Generate_Method
  !!    = 1,    generate regions by advance front (faster method).                      \n
  !!    = 2,    generate regions by advance front (ST).                                 \n
  !!    =-1,    generate regions by Delaunay method, connect boundary nodes directly.   \n
  !!    =-2     generate regions by Delaunay method, recovery boundary edges.           
  INTEGER, SAVE :: Generate_Method
  !>
  !!  @param Orientation_Direct
  !!         =0,  keep the surface orientation after generation. \n
  !!         =1,  set the surface orientating inside.            \n
  !!         =-1, set the surface orientating outside.
  INTEGER, SAVE :: Orientation_Direct

  !>  The index of the background type.
  !!  @param Background_Model
  !!         = 2 or -2,  Tet. background (read "*.bac").                            \n
  !!         = 4 or -4,  Oct. background (read "*.bac").                            \n
  !!         = 5 or -5,  Oct. background (read "*.bac" & "*.dat").                  \n
  !!         = 6 or -6,  Oct. background (read "*.bac" & "*_0.fro").                \n
  !!         = 7 or -7,  Oct. background (read "*.Obac").                           \n
  !!         >0          for   BGSpacing%Model problems.                            \n
  !!         <0          for anisotropic problems.                                  \n
  !!
  INTEGER, SAVE :: Background_Model
  !>  The globe grid size. 
  !!  This is only be refered if the *.bac file is needed but not existing.
  REAL*8, SAVE  :: Globe_GridSize
  !>  The maximum value of stretch (>2.0).
  !!      Only be referred for anisotropic problems.
  REAL*8,  SAVE :: Stretch_Limit
  !>      The method to interpolate two mappings or surface scale.
  !!
  !!      For anisotropic problem, interpolate a mapping by:
  !!  @param Mapping_Interp_Model
  !!         =1,  by using simultaneous matrix reduction.                   \n
  !!         =2,  by using a matrix power -1, so large grid sizes dominate. \n
  !!         =3,  by using a matrix power  1, so small grid sizes dominate.
  !!
  !!      For BGSpacing%Model problem, the interpolate scale by:
  !!  @param Mapping_Interp_Model
  !!         =1,  by using geometric  mean.                                 \n
  !!         =2,  by using harmonic   mean, so large grid sizes dominate.   \n
  !!         =3,  by using arithmetic mean, so small grid sizes dominate.
  INTEGER, SAVE :: Mapping_Interp_Model
  !>
  !!  @param Interpolate_Oct_Mapping
  !!         =.FALSE., set the value of a point by the value of the Cube_grid
  !!                   in which the point lies.                                  \n
  !!         =.TRUE.,  set the value of a point by interpolating the Cube_grid
  !!                   in which the point lies. This is a more precise but slower way.
  LOGICAL, SAVE :: Interpolate_Oct_Mapping
  !>  Factor of background mesh gradation (0.05~3.0).
  !!      A small value results in a strong gradating and a smoother mesh.
  !!      If >=3.0, no gradation.
  REAL*8,  SAVE :: Gradation_Factor
  !>  Factors of curvature affect spacing.
  !!    @param Curvature_Factors (1)  The cofficient. if  = 0.2, then the grid size for 
  !!                                  a sphere surface with a radius of 1 is 0.2.    \n
  !!                             (2)  The maximum Sagitta (with dimension).          \n
  !!                             (3)  The minimum grid size (with dimension).        \n
  !!                             (4)  The maximum grid size (with dimension).        \n
  !!                             (5)  The least trailing angle (in degree, 0~180).   \n
  !!                             (6)  The full trailing angle (in degree, 0~180).    \n
  !!                             (7)  The minimum grid size (with dimension) for trailing.
  REAL*8,  SAVE :: Curvature_Factors(7)
  !>  The adjustor for the grid size (0.01~100).
  REAL*8,  SAVE :: GridSize_Adjustor

  !>  The number of cosmetic loops.
  INTEGER, SAVE :: Loop_Cosmetic
  !>  The number of cosmetic smooth iteration.
  INTEGER, SAVE :: Loop_Smooth
  !>  The way choosing the direction of a smoothing node.
  !!  @param Smooth_Method
  !!         =1,  smooth in 2D parameter plane.                                  \n
  !!         =2,  smooth in 3D physical space: checking edge length from a node.  \n
  !!         =3,  smooth in 3D physical space: checking midline length from a node.
  INTEGER, SAVE :: Smooth_Method
  !>  The maximum collapse angle. Input in degree.
  REAL*8,  SAVE :: Collapse_Angle
  !>  The maximum swapping angle, Input in degree.
  REAL*8,  SAVE :: Swapping_Angle
  !>  The number of cosmetic loops for super patches.
  INTEGER, SAVE :: Loop_SuperCosmetic
  !>  The method to choose geometry support for super patches.
  !!  @param SuperCosmetic_Method
  !!         =0,  no no Super-Patch Cosmetic.      \n
  !!         =1,  use regular surface curvature.   \n
  !!         =2,  use existing triangulation as static base.     \n
  !!         =3,  use existing triangulation as dynamic base.
  INTEGER, SAVE :: SuperCosmetic_Method
  !>  The index if merge triangular elements to quadrilateral elements.
  !!  @param SuperCosmetic_Quad
  !!         = 1,  merge to quadrangles only mesh.   \n
  !!         = 0,  merge to hybrid mesh.        \n
  !!         =-1,  do not merge, leave triangles only mesh.
  INTEGER, SAVE :: SuperCosmetic_Quad
  !>  The number of loops of frequency filter for super patches.
  !!  @param  Loop_SuperFrqFilter  =0,  no Super-Patch frequency filter. '
  INTEGER, SAVE :: Loop_SuperFrqFilter
  !>  The power index of frequency filter for super patches.
  REAL*8,  SAVE :: SuperFrqFilter_power

  NAMELIST /ControlParameters/        &
       Debug_Display,                 &
       Start_Point,                   &
       Curvature_Type,                &
       Generate_Method,               &
       Orientation_Direct
  NAMELIST /BackgroundParameters/     &
       Background_Model,              &
       Globe_GridSize,                &
       Curvature_Factors,             &
       Stretch_Limit,                 &
       Interpolate_Oct_Mapping,       &
       Gradation_Factor
  NAMELIST /CosmeticParameters/       &
       Loop_Cosmetic,                 &
       Loop_Smooth,                   &
       Smooth_Method,                 &
       Collapse_Angle,                &
       Swapping_Angle,                &
       Loop_SuperCosmetic,            &
       SuperCosmetic_Method,          &
       SuperCosmetic_Quad,            &
       Loop_SuperFrqFilter,           &
       SuperFrqFilter_power

  CHARACTER(LEN=512)  :: NameUc, NameUs, NameSs, NameFs, NameHi, NameGs
  TYPE (IntQueueType), SAVE :: ListUc, ListUs, ListSs, ListFs, ListHi, ListGs
  
  !>  IF an egde with dihedral angle sharper than SupperCurve_RidgeAngle (in degree),
  !!    then this edge will be treaded as a ridge egde.
  !!  Only applied for  SuperCosmetic_Method=2 or 3, and Loop_SuperFrqFilter=0.
  !!  IF SupperCurve_RidgeAngle=0,   all edges are NOT ridge egdes;
  !!  IF SupperCurve_RidgeAngle=180, all edges are     ridge egdes;
  REAL*8,  SAVE :: SuperCurve_RidgeAngle

  TYPE(MaterialPar), SAVE :: Material
    
  !>  the surfaces with high-order treatment
  !!  =0, linear surface; =1, with high-order buondary; =2 high-order surface.
  INTEGER, DIMENSION(:), POINTER :: HighSurf
  !>  the curves with high-order treatment
  !!  =0, both sides linear; =1, one side linear one side high-order; =2 both sides high-order.
  INTEGER, DIMENSION(:), POINTER :: HighCurve
  

END MODULE control_Parameters

!*******************************************************************************
!>
!!  Surface and curvature data.
!<
!*******************************************************************************

MODULE surface_Parameters
  USE Queue
  USE SurfaceMeshStorage
  USE SurfaceCurvature
  USE SurfaceMeshGeometry
  IMPLICIT NONE

  TYPE(SurfaceMeshStorageType),SAVE  :: Surf           !<   Surface mesh.
  TYPE(SurfaceCurvatureType),SAVE    :: Curvt          !<   Surface curvature.
  INTEGER, SAVE :: NumRegions                     !<   Number of regions.
  INTEGER, SAVE :: NumCurves                      !<   Number of curves.
  INTEGER, DIMENSION(:), POINTER :: RegionMark    !<   =1, generate; =0, don't.
  INTEGER, DIMENSION(:), POINTER :: CurveMark     !<   =1, generate; =0, don't.
  
  
  !>  CurveNodes(NumCurves): Global IDs of the nodes on each curve.
  TYPE (IntQueueType), DIMENSION(:), POINTER :: CurveNodes
  !>  CurveNodes(NumCurves): Global IDs of the high-order nodes on each curve.
  TYPE (IntQueueType), DIMENSION(:), POINTER :: CurveHighNodes
  !>  CurveNodePar(NumCurves): parameters of the nodes on each curve. For high-order mesh.
  TYPE (Real8QueueType), DIMENSION(:), POINTER :: CurveNodePar
  
  !>  Information for those nodes inter multi-patches(regions)
  TYPE :: MultiNode
     INTEGER :: numPatches = 0
     INTEGER :: Patches(10)
     REAL*8  :: coord(2,10)
  END TYPE MultiNode
  
  !>  number of nodes on all curves
  INTEGER :: numAllCurveNodes

  !>  InterNodes(numAllCurveNodes), information for each inter-curve nodes
  TYPE (MultiNode), DIMENSION(:), POINTER :: InterNodes    

  !>  number od super-patches.
  !!  A super-patch includes a group of regions which will be merged for cosmetric.
  !!  The boundary between two super-patches are a super-curve.
  !!  A super-patch can be a single patch.
  INTEGER :: NB_SuperPatch

  !>  number of super-curves. 
  !!  A super-curve includes all curves between two specified super-patches.
  !!  The nodes on a super-curve can be move along the curves for cosmetric.
  INTEGER :: NB_SuperCurve

  !>  The original patches of each super-patch.
  TYPE(IntQueueType), DIMENSION(:), POINTER :: SuperPatchList

  !>  The original curves of each super-curve.
  TYPE(IntQueueType), DIMENSION(:), POINTER :: SuperCurveList
  
  !>  The surface nodes on each super-curve.
  TYPE(IntQueueType),   DIMENSION(:), POINTER :: SuperCurveNodes
  TYPE(Real8QueueType), DIMENSION(:), POINTER :: SuperCurveNodePt

  !>  The curvature of each super-curve.
  TYPE(CurveType),    DIMENSION(:), POINTER :: SuperCurve

  !---  Based triangulation for super patches
  
  TYPE :: TriangleCurvatureType
     REAL*8  :: Coord(2,3)       !<  The 2D coordinate of 3 nodes on the geometry Region.
     REAL*8  :: WeightAxes(3,3)  !<  3 axes for weight calculation.
     REAL*8  :: WeightShift(3)   !<  3 shift for weight calculation.
  END TYPE TriangleCurvatureType

  TYPE(TriangleCurvatureType), DIMENSION(:), POINTER :: TriBase
  TYPE(SurfaceMeshStorageType),SAVE  :: SurfBase           !<   Surface mesh as base for super-patch.

  !>  inBaseTri(Surf%NB_Point) : the location in the based triangulation 
  !!  of a node in the present mesh.
  INTEGER, DIMENSION(:),     POINTER :: inBaseTri
  
  TYPE(SurfaceNodeType), DIMENSION(:), POINTER :: NodeBase

  !>  EdgeSuperID(Surf%NB_Edge) : The Super-Curve on which this edge is.
  !!    =0, not on a super-curve.
  INTEGER, DIMENSION(:), POINTER :: EdgeSuperID

  TYPE(SurfaceTriangleType),  DIMENSION(:), POINTER :: SurfTri
  TYPE(SurfaceNodeType),      DIMENSION(:), POINTER :: SurfNode
  
  !>   MarkVisit(NB_Tri)  :  used with common_Parameters::VisitCount
  !!                         to identify a element not being or just being visited.
  !!                         Make sure that abs(MarkVisit(:))<VisitCount for every element
  !!                         when VisitCount is update be function GetVisitCount().
  !!  @param MarkVisit |MarkVisit(IT)| < VisitCount : cell IT has not been visited.  \n
  !!                   |MarkVisit(IT)| = VisitCount : cell IT has been visited.      \n
  !!                   |MarkVisit(IT)| > VisitCount : error.
  INTEGER, DIMENSION(:), POINTER :: MarkVisit
  INTEGER, SAVE :: VisitCount = 0
  
  !>
  !!   Temporary integer for debuging perpus.
  INTEGER :: Kdebug = 0

  REAL*8, ALLOCATABLE :: SurfMaxUV(:,:)
  
CONTAINS

  !>
  !!  Increasing common_Parameters::VisitCount by 1 every time
  !!    and return the value.
  !<
  FUNCTION GetVisitCount() RESULT (ThisVisit)
    IMPLICIT NONE
    INTEGER :: ThisVisit
    VisitCount = VisitCount + 1
    IF(VisitCount > 10000000) VisitCount = 1
    ThisVisit = VisitCount
  END FUNCTION GetVisitCount

END MODULE surface_Parameters

!*******************************************************************************
!>
!!   The background spacing data.
!<
!*******************************************************************************
MODULE Spacing_Parameters
  USE SpacingStorage
  IMPLICIT NONE
  
  TYPE(SpacingStorageType),SAVE     :: BGSpacing      !<   background mesh and spacing.
  REAL*8 , SAVE :: TOLG

END MODULE Spacing_Parameters


