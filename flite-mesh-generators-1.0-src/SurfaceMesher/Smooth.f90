!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!
!*------------------------------------------------------------------*
!*    [smooth Lattice] moves the nodes to enhance the quality of the mesh based on  
!                      projection of lattice points   *   
!*------------------------------------------------------------------*

SUBROUTINE Surf_Lattice_2D(RegionID, Tri3D, LOOP_SMOOTH, Nss_IP  )
  USE SurfaceMeshStorage
  USE Spacing_Parameters
  USE LinkAssociation
  USE Geometry2D
  IMPLICIT NONE

  INTEGER, INTENT(IN)                :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  INTEGER, INTENT(IN) :: LOOP_SMOOTH
  INTEGER, INTENT(IN) :: Nss_IP(*)

  INTEGER :: ip, it, i1,i2,i3, ismoo, ip1, ip2, ip3, ie,i,j
  REAL*8  :: fMap(3,3),f, cn, u, v, pt(3),p0(3)
  REAL*8, ALLOCATABLE :: coor0(:,:),ipNum(:)
  REAL*8  :: UBOT, VBOT, UTOP, VTOP,d,oneOverSqrt34,unitCell(3,14),sqrt34
  REAL*8  :: unitCelluv(2,14),drm(14),uvTemp(2),minD,uvD
  REAL*8  :: tol = 1.d-8
  
  TYPE (LinkAssociationType)     :: PPAsso
  INTEGER, DIMENSION(:), POINTER :: ipList
  INTEGER   ::  nList,onSurf

  ! *** smooths the grid in LOOP_SMOOTH steps

  IF(LOOP_SMOOTH==0) RETURN 

  WRITE(*, '(/,a,i4)') '  .... Mesh smoothing.' , LOOP_SMOOTH
  WRITE(29,'(/,a,i4)') '  .... Mesh smoothing.' , LOOP_SMOOTH

  ! *** displacement factor f
  CALL GetRegionUVBox (RegionID, UBOT, VBOT, UTOP, VTOP)

  sqrt34 = (SQRT(3.0d0/4.0d0))
  oneOverSqrt34 = 1.d0/sqrt34
  onSurf = 0
  f = 0.2d0

  ALLOCATE(coor0(2,Tri3D%NB_Point))
  ALLOCATE(ipNum(Tri3D%NB_Point))
  
  
  !--- Build point-point association
  CALL LinkAssociation_Allocate(Tri3D%NB_Point, PPAsso)
  DO ie = 1,Tri3D%NB_Tri
     ip1 = Tri3D%IP_Tri(1,ie)
     ip2 = Tri3D%IP_Tri(2,ie)
     ip3 = Tri3D%IP_Tri(3,ie)
     CALL IntQueue_Push36(PPAsso%JointLinks(ip1), ip2) 
     CALL IntQueue_Push36(PPAsso%JointLinks(ip2), ip3) 
     CALL IntQueue_Push36(PPAsso%JointLinks(ip3), ip1) 
  ENDDO

  DO ismoo = 1,LOOP_SMOOTH
  
	
     coor0(:,1:Tri3D%NB_Point) = 0.0d0
     ipNum(1:Tri3D%NB_Point) = 0.0d0
	 
	 
	DO ip = Tri3D%NB_BD_Point +1,Tri3D%NB_Point
		
		!Get points connected to ip
		CALL LinkAssociation_List(ip, nList, ipList, PPAsso)
		p0 = Tri3D%Posit(:,ip)
		IF(BGSpacing%Model>0)THEN
				CALL SpacingStorage_GetScale(BGSpacing, p0, d)
		ELSE IF(BGSpacing%Model<0)THEN
				CALL SpacingStorage_GetMapping(BGSpacing, p0, fMap)
				d = MINVAL(fMap)
		END IF
		d = d*BGSpacing%BasicSize
	    CALL unitCellR(p0,d*oneOverSqrt34,unitCell)
		
		DO i=1,14
			uvTemp = Tri3D%Coord(:,ip)
			CALL ProjectToRegionFromXYZ(RegionID, unitCell(:,i), uvTemp, TOLG, onSurf, drm(i))
			unitCelluv(:,i) = uvTemp
		ENDDO

		DO i = 1,nList
			minD = HUGE(0.d0)
			ip3 = 0
			ip2 = ipList(i)
			!Find the closest
			DO j=1,14
			
				!First see if we are close enough to the surface
				IF (drm(j).LT.(sqrt34*d)) THEN
				
					uvD = (Tri3D%Coord(1,ip2)-unitCelluv(1,j))*(Tri3D%Coord(1,ip2)-unitCelluv(1,j)) + &
						  (Tri3D%Coord(2,ip2)-unitCelluv(2,j))*(Tri3D%Coord(2,ip2)-unitCelluv(2,j))
					!uvD = (Tri3D%Posit(1,ip2)-unitCell(1,j))*(Tri3D%Posit(1,ip2)-unitCell(1,j)) + &
					!      (Tri3D%Posit(2,ip2)-unitCell(2,j))*(Tri3D%Posit(2,ip2)-unitCell(2,j)) + &
					!      (Tri3D%Posit(3,ip2)-unitCell(3,j))*(Tri3D%Posit(3,ip2)-unitCell(3,j))
					IF (uvD.LT.minD) THEN
						minD = uvD
						ip3 = j
				
					ENDIF
				
				ENDIF
		
			ENDDO
			
			IF (ip3.GT.0) THEN
				IF ((unitCelluv(1,ip3) .GE. UBOT+tol) .AND. &
				    (unitCelluv(2,ip3) .GE. VBOT+tol) .AND. &
					(unitCelluv(1,ip3) .LE. UTOP-tol) .AND. &
					(unitCelluv(2,ip3) .LE. VTOP-tol) ) THEN
					ipNum(ip2) = ipNum(ip2)+1.d0
					coor0(:,ip2) = unitCelluv(:,ip3) + coor0(:,ip2) 
				ENDIF
			
			ENDIF
			
		ENDDO
		
		
	 
	ENDDO
	 
	 
	DO ip = Tri3D%NB_BD_Point +1, Tri3D%NB_Point
		IF (ipNum(ip).GT.0) THEN
			Tri3D%Coord(:,ip) = (1.-f) * Tri3D%Coord(:,ip) + (f/(ipNum(ip))) * coor0(:,ip)  
			
			IF (Tri3D%Coord(1,ip)>UTOP-tol) THEN
				Tri3D%Coord(1,ip)=UTOP-tol
			ELSEIF (Tri3D%Coord(1,ip)<UBOT+tol) THEN
				Tri3D%Coord(1,ip)=UBOT+tol
			ENDIF
			
			IF (Tri3D%Coord(2,ip)>VTOP-tol) THEN
				Tri3D%Coord(2,ip)=VTOP-tol
			ELSEIF (Tri3D%Coord(2,ip)<VBOT+tol) THEN
				Tri3D%Coord(2,ip)=VBOT+tol
			ENDIF
			
			!Tri3D%Coord(1,ip) = MIN(Tri3D%Coord(1,ip),UTOP-TOLG)  !TODO: This check isn't precise enough?
			!Tri3D%Coord(2,ip) = MIN(Tri3D%Coord(2,ip),VTOP-TOLG)
			!Tri3D%Coord(1,ip) = MAX(Tri3D%Coord(1,ip),UBOT+TOLG)
			!Tri3D%Coord(2,ip) = MAX(Tri3D%Coord(2,ip),VBOT+TOLG)
		ENDIF
    ENDDO
	 
  ENDDO

  ! *** Computes new 3D coordinates

  DO ip = Tri3D%NB_BD_Point +1, Tri3D%NB_Point
     u = Tri3D%Coord(1,ip)
     v = Tri3D%Coord(2,ip)
     CALL GetUVPointInfo0(RegionID,u,v,pt)
     Tri3D%Posit(:,ip) = pt(1:3)
  ENDDO

  DEALLOCATE(coor0)

  RETURN
END SUBROUTINE Surf_Lattice_2D


!**************************************************************************
!>
!!  unitCell 
!!  This subroutine returns the unit cell around Pt at spacing d
!!
!<
!**************************************************************************
SUBROUTINE unitCellR(Pt,d,xyz)

	REAL*8, INTENT(IN)	:: Pt(3)
	REAL*8, INTENT(IN) 	:: d
	REAL*8, INTENT(OUT) :: xyz(3,14)
	REAL*8 				:: b,h,s
	
	b = (d/2.d0)
	h = (d/SQRT(2.d0))

	s = sqrt(3.d0/4.d0)
	
	xyz(:,1) = (/-1.d0*b,h,0.d0/) + Pt(:)
	xyz(:,2) = (/0.d0,h,h/)*s + Pt(:)
	xyz(:,3) = (/b,h,0.d0/) + Pt(:)
	xyz(:,4) = (/-1.d0*b,0.d0,h/) + Pt(:)
	xyz(:,5) = (/b,0.d0,h/) + Pt(:)
	xyz(:,6) = (/-1.d0*b,-1.d0*h,0.d0/) + Pt(:)
	xyz(:,7) = (/0.d0,-1.d0*h,h/)*s + Pt(:)
	xyz(:,8) = (/b,-1.0d0*h,0.d0/) + Pt(:)
	xyz(:,9) = (/0.0d0,-1.0d0*h,-1.0d0*h/)*s + Pt(:)
	xyz(:,10) = (/-1.d0*b,0.d0,-1.0d0*h/) + Pt(:)
	xyz(:,11) = (/0.0d0,h,-1.0d0*h/)*s + Pt(:)
	xyz(:,12) = (/b,0.d0,-1.0d0*h/) + Pt(:)
	xyz(:,13) = (/2.d0*b,0.d0,0.d0/)*s + Pt(:)
	xyz(:,14) = (/-2.d0*b,0.d0,0.d0/)*s + Pt(:)
	


END SUBROUTINE unitCellR




!
!*------------------------------------------------------------------*
!*    [smooth Laplace] moves the nodes to enhance the quality of the mesh based on surrounding 
!                      centroids   *   
!*------------------------------------------------------------------*

SUBROUTINE Surf_Laplace_2D(RegionID, Tri3D, LOOP_SMOOTH, Nss_IP  )
  USE SurfaceMeshStorage
  USE Geometry2D
  IMPLICIT NONE

  INTEGER, INTENT(IN)                :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  INTEGER, INTENT(IN) :: LOOP_SMOOTH
  INTEGER, INTENT(IN) :: Nss_IP(*)

  INTEGER :: ip, it, i1,i2,i3, ismoo
  REAL*8  :: f, cn, u, v, pt(3),p0(2)
  REAL*8, DIMENSION(:,:), POINTER :: coor0
  REAL*8  :: UBOT, VBOT, UTOP, VTOP

  ! *** smooths the grid in LOOP_SMOOTH steps

  IF(LOOP_SMOOTH==0) RETURN 

  WRITE(*, '(/,a,i4)') '  .... Mesh smoothing.' , LOOP_SMOOTH
  WRITE(29,'(/,a,i4)') '  .... Mesh smoothing.' , LOOP_SMOOTH

  ! *** displacement factor f
  CALL GetRegionUVBox (RegionID, UBOT, VBOT, UTOP, VTOP)

  f = 0.1d0

  ALLOCATE(coor0(2,Tri3D%NB_Point))

  DO ismoo = 1,LOOP_SMOOTH
     coor0(:,1:Tri3D%NB_Point) = 0.0
     DO it = 1,Tri3D%NB_Tri
        i1          = Tri3D%IP_Tri(1,it)
        i2          = Tri3D%IP_Tri(2,it)
        i3          = Tri3D%IP_Tri(3,it)
		
	!	IF( (Tri3D%Next_Tri(i1,it)>0).AND.(Tri3D%Next_Tri(i2,it)>0).AND.(Tri3D%Next_Tri(i3,it)>0)) THEN
	!		p0 = Circle_Centre_2D(Tri3D%Coord(:,i1),Tri3D%Coord(:,i2),Tri3D%Coord(:,i3))
	!	ELSE
			!Boundary element
			p0 = (Tri3D%Coord(:,i1)+Tri3D%Coord(:,i2)+Tri3D%Coord(:,i3))/3
	!	ENDIF
		
        coor0(:,i1) = coor0(:,i1) + p0(:)
        coor0(:,i2) = coor0(:,i2) + p0(:)
        coor0(:,i3) = coor0(:,i3) + p0(:)
     ENDDO

     DO ip = Tri3D%NB_BD_Point +1, Tri3D%NB_Point
        Tri3D%Coord(:,ip) = (1.-f) * Tri3D%Coord(:,ip) + (f/(Nss_IP(ip))) * coor0(:,ip)
		Tri3D%Coord(1,ip) = MIN(Tri3D%Coord(1,ip),UTOP)
		Tri3D%Coord(2,ip) = MIN(Tri3D%Coord(2,ip),VTOP)
		Tri3D%Coord(1,ip) = MAX(Tri3D%Coord(1,ip),UBOT)
		Tri3D%Coord(2,ip) = MAX(Tri3D%Coord(2,ip),VBOT)
     ENDDO
  ENDDO

  ! *** Computes new 3D coordinates

  DO ip = Tri3D%NB_BD_Point +1, Tri3D%NB_Point
     u = Tri3D%Coord(1,ip)
     v = Tri3D%Coord(2,ip)
     CALL GetUVPointInfo0(RegionID,u,v,pt)
     Tri3D%Posit(:,ip) = pt(1:3)
  ENDDO

  DEALLOCATE(coor0)

  RETURN
END SUBROUTINE Surf_Laplace_2D



!
!*------------------------------------------------------------------*
!*    [smooth] moves the nodes to enhance the quality of the mesh   *   
!*------------------------------------------------------------------*

SUBROUTINE Surf_Smooth_2D(RegionID, Tri3D, LOOP_SMOOTH, Nss_IP  )
  USE SurfaceMeshStorage
  
  IMPLICIT NONE

  INTEGER, INTENT(IN)                :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  INTEGER, INTENT(IN) :: LOOP_SMOOTH
  INTEGER, INTENT(IN) :: Nss_IP(*)

  INTEGER :: ip, it, i1,i2,i3, ismoo
  REAL*8  :: f, cn, u, v, pt(3)
  REAL*8, DIMENSION(:,:), POINTER :: coor0

  ! *** smooths the grid in LOOP_SMOOTH steps

  IF(LOOP_SMOOTH==0) RETURN 

  WRITE(*, '(/,a,i4)') '  .... Mesh smoothing.' , LOOP_SMOOTH
  WRITE(29,'(/,a,i4)') '  .... Mesh smoothing.' , LOOP_SMOOTH

  ! *** displacement factor f

  f = 0.2

  ALLOCATE(coor0(2,Tri3D%NB_Point))

  DO ismoo = 1,LOOP_SMOOTH
     coor0(:,1:Tri3D%NB_Point) = 0.0
     DO it = 1,Tri3D%NB_Tri
        i1          = Tri3D%IP_Tri(1,it)
        i2          = Tri3D%IP_Tri(2,it)
        i3          = Tri3D%IP_Tri(3,it)
        coor0(:,i1) = coor0(:,i1) + Tri3D%Coord(:,i2) + Tri3D%Coord(:,i3)
        coor0(:,i2) = coor0(:,i2) + Tri3D%Coord(:,i1) + Tri3D%Coord(:,i3)
        coor0(:,i3) = coor0(:,i3) + Tri3D%Coord(:,i1) + Tri3D%Coord(:,i2)
     ENDDO

     DO ip = Tri3D%NB_BD_Point +1, Tri3D%NB_Point
        Tri3D%Coord(:,ip) = (1.-f) * Tri3D%Coord(:,ip) + f/(2*Nss_IP(ip)) * coor0(:,ip)
     ENDDO
  ENDDO

  ! *** Computes new 3D coordinates

  DO ip = Tri3D%NB_BD_Point +1, Tri3D%NB_Point
     u = Tri3D%Coord(1,ip)
     v = Tri3D%Coord(2,ip)
     CALL GetUVPointInfo0(RegionID,u,v,pt)
     Tri3D%Posit(:,ip) = pt(1:3)
  ENDDO

  DEALLOCATE(coor0)

  RETURN
END SUBROUTINE Surf_Smooth_2D

!*------------------------------------------------------------------
!*    [smoothp] moves the nodes to enhance the quality of the mesh   
!*     smooth in physical space 
!*     -- try to make all edges from one points have similar lengths : Eads idea --                       
!*------------------------------------------------------------------

SUBROUTINE Surf_Smooth_2D_3(RegionID, Tri3D, LOOP_SMOOTH, Kstr)
  USE Spacing_Parameters
  USE SurfaceMeshStorage
  USE LinkAssociation
  USE Geometry3DAll
  IMPLICIT NONE

  INTEGER, INTENT(IN)                :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  INTEGER, INTENT(IN)    :: LOOP_SMOOTH, Kstr

  INTEGER :: nit, ip, it, ismoo, ismoo1, i, ip1, ip2, ip3, nMove, ie
  REAL*8  :: f, sideMin, sideMax, xl
  REAL*8  :: fMap(3,3), scalar, p3(3), p0(3), pt(3), Coo(2), u1, u2
  INTEGER, DIMENSION(:), POINTER :: Mark
  TYPE (LinkAssociationType)     :: PPAsso
  INTEGER, DIMENSION(:), POINTER :: ipList
  INTEGER   ::  nList

  IF(LOOP_SMOOTH==0) RETURN 

  ! *** smooths the grid in LOOP_SMOOTH steps

  WRITE(*, '(/,a,i4)') '  .. Mesh smoothing in 2D ',LOOP_SMOOTH
  WRITE(29,'(/,a,i4)') '  .. Mesh smoothing in 2D ',LOOP_SMOOTH

  nit = 10

  ALLOCATE(Mark(Tri3D%NB_Point))

  !--- Build point-point association
  CALL LinkAssociation_Allocate(Tri3D%NB_Point, PPAsso)
  DO ie = 1,Tri3D%NB_Tri
     ip1 = Tri3D%IP_Tri(1,ie)
     ip2 = Tri3D%IP_Tri(2,ie)
     ip3 = Tri3D%IP_Tri(3,ie)
     CALL IntQueue_Push36(PPAsso%JointLinks(ip1), ip2) 
     CALL IntQueue_Push36(PPAsso%JointLinks(ip2), ip3) 
     CALL IntQueue_Push36(PPAsso%JointLinks(ip3), ip1) 
  ENDDO


  ! *** displacement factor f

  Mark(1:Tri3D%NB_Point) = 1

  Loop_ismoo : DO ismoo = 1,LOOP_SMOOTH

     ismoo1 = ismoo + 1
     nMove  = 0

     ! Newton-Raphson to find new parametric position for point ip

     Loop_ip : DO ip = Tri3D%NB_BD_Point +1, Tri3D%NB_Point

        IF(Mark(ip)<ismoo) CYCLE         !--- a good node from previous loop

        CALL LinkAssociation_List(IP, nList, ipList, PPAsso)
        IF(nList<3)THEN
           WRITE(29,*)'Error---flat loop: IP, nlist, list=',IP, nList, ipList(1:nList)
           CALL Error_Stop ('Surf_Smooth_3D::  ') 
        ENDIF

        IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
           CALL SpacingStorage_GetMapping(BGSpacing, Tri3D%Posit(:,IP), fMap)
           p0 = Mapping3D_Posit_Transf(Tri3D%Posit(:,IP), fMap, 1)
        ELSE
           p0 = Tri3D%Posit(:,IP)
        ENDIF

        !  sideMin  minimum length side at point ip
        !  sideMax  maximum length side at point ip

        sideMin = 1.e30
        sideMax = 0.
        Coo = 0.
        DO i = 1,nList
           ip3 = ipList(i)       
           Coo = Coo + Tri3D%Coord(:,ip3) 

           IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
              p3 = Mapping3D_Posit_Transf(Tri3D%Posit(:,ip3), fMap, 1)
           ELSE
              p3  = Tri3D%Posit(:,ip3)
           ENDIF
           xl = Geo3D_Distance_SQ(p0,p3)
           IF(sideMin>xl)   sideMin = xl
           IF(sideMax<xl)   sideMax = xl
        ENDDO
        Coo     = Coo / nList

        Loop_it : DO it = 1, nit
           f  = it * 1.d0 / nit
           u1 = (1.-f)*Coo(1) + f* Tri3D%Coord(1,ip)
           u2 = (1.-f)*Coo(2) + f* Tri3D%Coord(2,ip)
           CALL GetUVPointInfo0(RegionID,u1,u2,pt(1:3))

           !--- check if a better solution found

           IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
              CALL SpacingStorage_GetMapping(BGSpacing, pt, fMap)
           ENDIF

           DO i = 1,nList
              ip3 = ipList(i)           
              IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
                 xl = Mapping3D_Distance_SQ(pt,Tri3D%Posit(:,ip3),fMap)
              ELSE
                 xl = Geo3D_Distance_SQ(pt,Tri3D%Posit(:,ip3))
              ENDIF
              IF(xl < sideMin) EXIT
              IF(xl > sideMax) EXIT
           ENDDO

           IF(  i>nList ) THEN
              !--- move this node and mark affected nodes
              nMove             = nMove + 1
              Tri3D%Coord(1,ip) = u1
              Tri3D%Coord(2,ip) = u2
              Tri3D%Posit(:,ip) = pt(1:3)
              DO i = 1,nList
                 Mark(ipList(i)) = ismoo1
              ENDDO
              EXIT Loop_it
           ENDIF

        ENDDO Loop_it

        mark(ip) = 0

     ENDDO Loop_ip

     IF(nMove==0) EXIT

     WRITE(*, '(a,i7)') '       No. of nodes moved: ',nMove
     WRITE(29,'(a,i7)') '       No. of nodes moved: ',nMove

  ENDDO Loop_ismoo

  DEALLOCATE(Mark)
  CALL LinkAssociation_Clear(PPAsso)

  RETURN
END SUBROUTINE Surf_Smooth_2D_3

!*------------------------------------------------------------------
!*    [smoothp] moves the nodes to enhance the quality of the mesh   
!*     smooth in physical space 
!*     -- try to make all edges from one points have similar lengths : Eads idea --                       
!*------------------------------------------------------------------

SUBROUTINE Surf_Smooth_3D(RegionID, Tri3D, LOOP_SMOOTH, Kstr)
  USE Spacing_Parameters
  USE SurfaceMeshStorage
  USE LinkAssociation
  USE Geometry3DAll
  IMPLICIT NONE

  INTEGER, INTENT(IN)                :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  INTEGER, INTENT(IN)    :: LOOP_SMOOTH, Kstr

  INTEGER :: nit, ip, it, ismoo, ismoo1, i, ip1, ip2, ip3, nMove, ie
  REAL*8  :: UBOT, VBOT, UTOP, VTOP, UBOTL, VBOTL, UTOPL, VTOPL, EPS, deteps
  REAL*8  :: f, f1, f2, u1, u2, dsiz, du1, du2, TLIMU, TLIMV
  REAL*8  :: b11, b12, b21, b22, det, uin1, uin2, fakt
  REAL*8  :: sideMin, sideMax, sideAvg, xl
  REAL*8  :: fMap(3,3), scalar
  REAL*8  :: rp(12), v1(3), v2(3), p1(3), p2(3), p3(3), p0(3), pt(3)
  REAL*8  :: r(3), ru(3), rv(3)
  INTEGER, DIMENSION(:), POINTER :: Mark
  TYPE (LinkAssociationType)     :: PPAsso
  INTEGER, DIMENSION(:), POINTER :: ipList
  INTEGER   ::  nList

  IF(LOOP_SMOOTH==0) RETURN 

  ! *** smooths the grid in LOOP_SMOOTH steps

  WRITE(*, '(/,a,i4)') '  .. Mesh smoothing in physical space.',LOOP_SMOOTH
  WRITE(29,'(/,a,i4)') '  .. Mesh smoothing in physical space.',LOOP_SMOOTH

  CALL GetRegionUVBox (RegionID, UBOT, VBOT, UTOP, VTOP)

  nit = 10
  EPS = 1.d-06
  EPS = 1.d-03

  ALLOCATE(Mark(Tri3D%NB_Point))

  !--- Build point-point association
  CALL LinkAssociation_Allocate(Tri3D%NB_Point, PPAsso)
  DO ie = 1,Tri3D%NB_Tri
     ip1 = Tri3D%IP_Tri(1,ie)
     ip2 = Tri3D%IP_Tri(2,ie)
     ip3 = Tri3D%IP_Tri(3,ie)
     CALL IntQueue_Push36(PPAsso%JointLinks(ip1), ip2) 
     CALL IntQueue_Push36(PPAsso%JointLinks(ip2), ip3) 
     CALL IntQueue_Push36(PPAsso%JointLinks(ip3), ip1) 
  ENDDO


  ! *** displacement factor f

  f = 0.1
  Mark(1:Tri3D%NB_Point) = 1

  Loop_ismoo : DO ismoo = 1,LOOP_SMOOTH

     ismoo1 = ismoo + 1
     nMove  = 0

     ! Newton-Raphson to find new parametric position for point ip

     Loop_ip : DO ip = Tri3D%NB_BD_Point +1, Tri3D%NB_Point

        IF(Mark(ip)<ismoo) CYCLE         !--- a good node from previous loop

        CALL LinkAssociation_List(IP, nList, ipList, PPAsso)
        IF(nList<3)THEN
           WRITE(29,*)'Error---flat loop: IP, nlist, list=',IP, nList, ipList(1:nList)
           CALL Error_Stop ('Surf_Smooth_3D::  ') 
        ENDIF

        IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
           CALL SpacingStorage_GetMapping(BGSpacing, Tri3D%Posit(:,IP), fMap)
           p0 = Mapping3D_Posit_Transf(Tri3D%Posit(:,IP), fMap, 1)
        ELSE
           p0 = Tri3D%Posit(:,IP)
        ENDIF

        ! smoothing in physical space (EADS Oct 2001)

        !  sideMin  minimum length side at point ip
        !  sideMax  maximum length side at point ip
        !  sideAvg  mean length  side at point ip

        sideMin = 1.e30
        sideMax = 0.
        sideAvg = 0.
        DO i = 1,nList
           ip3 = ipList(i)           
           IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
              p3 = Mapping3D_Posit_Transf(Tri3D%Posit(:,ip3), fMap, 1)
           ELSE
              p3  = Tri3D%Posit(:,ip3)
           ENDIF
           xl = Geo3D_Distance(p0,p3)
           IF(sideMin>xl) THEN
              sideMin = xl
              ip1     = ip3
              p1      = p3
           ENDIF
           IF(sideMax<xl) THEN
              sideMax = xl
              ip2     = ip3
              p2      = p3
           ENDIF
           sideAvg = sideAvg + xl
        ENDDO
        sideAvg = sideAvg / nList

        dsiz = sideAvg**2
        u1   = Tri3D%Coord(1,ip)
        u2   = Tri3D%Coord(2,ip)
        Loop_it : DO it = 1,nit

           CALL GetUVPointInfo1(RegionID,u1,u2,Rp(1:3),Rp(4:6),Rp(7:9),Rp(10:12))
           IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
              r(1:3)  = Mapping3D_Posit_Transf(Rp(1:3), fMap, 1)
           ELSE
              r(1:3)  = Rp(1:3)
           ENDIF

           v1(:) = r(:) - p1(:)
           v2(:) = r(:) - p2(:)
           f1    = ((v1(1)**2+v1(2)**2+v1(3)**2)-dsiz)/dsiz
           f2    = ((v2(1)**2+v2(2)**2+v2(3)**2)-dsiz)/dsiz

           IF(ABS(f1)<0.1 .AND. ABS(f2)<0.1) EXIT Loop_it

           IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
              ru(1:3) = Mapping3D_Posit_Transf(Rp(4:6), fMap, 1)
              rv(1:3) = Mapping3D_Posit_Transf(Rp(7:9), fMap, 1)
           ELSE
              ru(1:3) = Rp(4:6)
              rv(1:3) = Rp(7:9)
           ENDIF

           du1 = sideAvg/MAX(EPS,SQRT(ru(1)**2+ru(2)**2+ru(3)**2))
           du2 = sideAvg/MAX(EPS,SQRT(rv(1)**2+rv(2)**2+rv(3)**2))

           TLIMU = .01*du1
           TLIMV = .01*du2

           UBOTL = MAX(UBOT, Tri3D%Coord(1,ip)-du1)
           UTOPL = MIN(UTOP, Tri3D%Coord(1,ip)+du1)
           VBOTL = MAX(VBOT, Tri3D%Coord(2,ip)-du2)
           VTOPL = MIN(VTOP, Tri3D%Coord(2,ip)+du2)

           b11 = v1(1)*ru(1) + v1(2)*ru(2) + v1(3)*ru(3)
           b12 = v1(1)*rv(1) + v1(2)*rv(2) + v1(3)*rv(3)
           b21 = v2(1)*ru(1) + v2(2)*ru(2) + v2(3)*ru(3)
           b22 = v2(1)*rv(1) + v2(2)*rv(2) + v2(3)*rv(3)
           det = b11*b22-b12*b21

           deteps = MAX(ABS(b11*b22), ABS(b12*b21))
           deteps = MAX(1.d-8, 1.d-3*deteps)
           IF(ABS(det)>deteps)THEN
              uin1 = ( b22*f1 - b12*f2) / det
              uin2 = (-b21*f1 + b11*f2) / det
           ELSE
              uin1 = u1 - Tri3D%Coord(1,ip2)
              uin2 = u2 - Tri3D%Coord(2,ip2)
           ENDIF
           fakt = MAX(ABS(uin1)/TLIMU, ABS(uin2)/TLIMV, 1.0)
           u1   = u1 - uin1 / fakt
           u2   = u2 - uin2 / fakt
           u1   = MAX(MIN(u1,UTOPL), UBOTL)
           u2   = MAX(MIN(u2,VTOPL), VBOTL)

        ENDDO Loop_it

        u1 = (1.-f)*Tri3D%Coord(1,ip) + f*u1
        u2 = (1.-f)*Tri3D%Coord(2,ip) + f*u2
        CALL GetUVPointInfo0(RegionID,u1,u2,pt(1:3))

        !--- check if a better solution found

        IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
           CALL SpacingStorage_GetMapping(BGSpacing, pt, fMap)
        ENDIF
        sideMax = sideMax**2
        sideMin = sideMin**2

        DO i = 1,nList
           ip3 = ipList(i)           
           IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
              xl = Mapping3D_Distance_SQ(pt,Tri3D%Posit(:,ip3),fMap)
           ELSE
              xl = Geo3D_Distance_SQ(pt,Tri3D%Posit(:,ip3))
           ENDIF
           IF(xl < sideMin) EXIT
           IF(xl > sideMax) EXIT
        ENDDO


        IF(  i>nList ) THEN
           !--- move this node and mark affected nodes
           nMove             = nMove + 1
           Tri3D%Coord(1,ip) = u1
           Tri3D%Coord(2,ip) = u2
           Tri3D%Posit(:,ip) = pt(1:3)
           DO i = 1,nList
              Mark(ipList(i)) = ismoo1
           ENDDO
        ENDIF

        mark(ip) = 0

     ENDDO Loop_ip

     IF(nMove==0) EXIT

     WRITE(*, '(a,i7)') '       No. of nodes moved: ',nMove
     WRITE(29,'(a,i7)') '       No. of nodes moved: ',nMove

  ENDDO Loop_ismoo

  DEALLOCATE(Mark)
  CALL LinkAssociation_Clear(PPAsso)

  RETURN
END SUBROUTINE Surf_Smooth_3D
!*------------------------------------------------------------------
!*    [smoothp] moves the nodes to enhance the quality of the mesh   
!*     smooth in physical space 
!*     -- try to make all midlines from one points have similar lengths --                       
!*------------------------------------------------------------------

SUBROUTINE Surf_Smooth_3D_3(RegionID, Tri3D, LOOP_SMOOTH, Kstr)
  USE Spacing_Parameters
  USE SurfaceMeshStorage
  USE LinkAssociation
  USE Geometry3DAll
  IMPLICIT NONE

  INTEGER, INTENT(IN)                :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  INTEGER, INTENT(IN)    :: LOOP_SMOOTH, Kstr

  INTEGER :: nit, ip, it, ismoo, ismoo1, i, nMove, ie
  REAL*8  :: UBOT, VBOT, UTOP, VTOP, UBOTL, VBOTL, UTOPL, VTOPL, EPS, deteps
  REAL*8  :: f, f1, f2, u1, u2, dsiz, du1, du2, TLIMU, TLIMV
  REAL*8  :: b11, b12, b21, b22, det, uin1, uin2, fakt
  REAL*8  :: sideMin, sideMax, sideAvg, xl
  REAL*8  :: fMap(3,3), scalar
  REAL*8  :: rp(12), v1(3), v2(3), p1(3), p2(3), p3(3), p0(3), pt(3)
  REAL*8  :: r(3), ru(3), rv(3)
  INTEGER, DIMENSION(:), POINTER :: Mark
  INTEGER, DIMENSION(:), POINTER :: ipList
  INTEGER   ::  nList

  IF(LOOP_SMOOTH==0) RETURN 

  ! *** smooths the grid in LOOP_SMOOTH steps

  WRITE(*, '(/,a,i4)') '  .. Mesh smoothing in physical space.',LOOP_SMOOTH
  WRITE(29,'(/,a,i4)') '  .. Mesh smoothing in physical space.',LOOP_SMOOTH

  CALL GetRegionUVBox (RegionID, UBOT, VBOT, UTOP, VTOP)

  nit = 10
  EPS = 1.d-06
  EPS = 1.d-03

  ALLOCATE(Mark(Tri3D%NB_Point))

  !--- Build point-triangle association
  CALL Surf_BuildTriAsso (Tri3D)

  ! *** displacement factor f

  f = 0.1
  Mark(1:Tri3D%NB_Point) = 1

  Loop_ismoo : DO ismoo = 1,LOOP_SMOOTH

     ismoo1 = ismoo + 1
     nMove  = 0

     ! Newton-Raphson to find new parametric position for point ip

     Loop_ip : DO ip = Tri3D%NB_BD_Point +1, Tri3D%NB_Point

        IF(Mark(ip)<ismoo) CYCLE         !--- a good node from previous loop

        CALL LinkAssociation_List(IP, nList, ipList, Tri3D%ITR_Pt)
        IF(nList<3)THEN
           WRITE(29,*)'Error---flat loop: IP, nlist, list=',IP, nList, ipList(1:nList)
           CALL Error_Stop ('Surf_Smooth_3D::  ') 
        ENDIF

        IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
           CALL SpacingStorage_GetMapping(BGSpacing, Tri3D%Posit(:,IP), fMap)
           p0 = Mapping3D_Posit_Transf(Tri3D%Posit(:,IP), fMap, 1)
        ELSE
           p0 = Tri3D%Posit(:,IP)
        ENDIF

        ! smoothing in physical space (EADS Oct 2001)

        !  sideMin  minimum length side at point ip
        !  sideMax  maximum length side at point ip
        !  sideAvg  mean length  side at point ip

        sideMin = 1.e30
        sideMax = 0.
        sideAvg = 0.
        DO i = 1,nList
           IT = ipList(i)
           p3 = 0
           IF(Tri3D%IP_Tri(1,IT)/=IP) p3 = p3 + Tri3D%Posit(:,Tri3D%IP_Tri(1,IT))
           IF(Tri3D%IP_Tri(2,IT)/=IP) p3 = p3 + Tri3D%Posit(:,Tri3D%IP_Tri(2,IT))
           IF(Tri3D%IP_Tri(3,IT)/=IP) p3 = p3 + Tri3D%Posit(:,Tri3D%IP_Tri(3,IT))
           p3 = p3 / 2.d0
           IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
              p3 = Mapping3D_Posit_Transf(p3, fMap, 1)
           ENDIF
           xl = Geo3D_Distance(p0,p3)
           IF(sideMin>xl) THEN
              sideMin = xl
              p1      = p3
           ENDIF
           IF(sideMax<xl) THEN
              sideMax = xl
              p2      = p3
           ENDIF
           sideAvg = sideAvg + xl
        ENDDO
        sideAvg = sideAvg / nList

        dsiz = sideAvg**2
        u1   = Tri3D%Coord(1,ip)
        u2   = Tri3D%Coord(2,ip)
        Loop_it : DO it = 1,nit

           CALL GetUVPointInfo1(RegionID,u1,u2,Rp(1:3),Rp(4:6),Rp(7:9),Rp(10:12))
           IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
              r(1:3)  = Mapping3D_Posit_Transf(Rp(1:3), fMap, 1)
           ELSE
              r(1:3)  = Rp(1:3)
           ENDIF

           v1(:) = r(:) - p1(:)
           v2(:) = r(:) - p2(:)
           f1    = ((v1(1)**2+v1(2)**2+v1(3)**2)-dsiz)/dsiz
           f2    = ((v2(1)**2+v2(2)**2+v2(3)**2)-dsiz)/dsiz

           IF(ABS(f1)<0.1 .AND. ABS(f2)<0.1) EXIT Loop_it

           IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
              ru(1:3) = Mapping3D_Posit_Transf(Rp(4:6), fMap, 1)
              rv(1:3) = Mapping3D_Posit_Transf(Rp(7:9), fMap, 1)
           ELSE
              ru(1:3) = Rp(4:6)
              rv(1:3) = Rp(7:9)
           ENDIF

           du1 = sideAvg/MAX(EPS,SQRT(ru(1)**2+ru(2)**2+ru(3)**2))
           du2 = sideAvg/MAX(EPS,SQRT(rv(1)**2+rv(2)**2+rv(3)**2))

           TLIMU = .01*du1
           TLIMV = .01*du2

           UBOTL = MAX(UBOT, Tri3D%Coord(1,ip)-du1)
           UTOPL = MIN(UTOP, Tri3D%Coord(1,ip)+du1)
           VBOTL = MAX(VBOT, Tri3D%Coord(2,ip)-du2)
           VTOPL = MIN(VTOP, Tri3D%Coord(2,ip)+du2)

           b11 = v1(1)*ru(1) + v1(2)*ru(2) + v1(3)*ru(3)
           b12 = v1(1)*rv(1) + v1(2)*rv(2) + v1(3)*rv(3)
           b21 = v2(1)*ru(1) + v2(2)*ru(2) + v2(3)*ru(3)
           b22 = v2(1)*rv(1) + v2(2)*rv(2) + v2(3)*rv(3)
           det = b11*b22-b12*b21

           deteps = MAX(ABS(b11*b22), ABS(b12*b21))
           deteps = MAX(1.d-8, 1.d-3*deteps)
           IF(ABS(det)>deteps)THEN
              uin1 = ( b22*f1 - b12*f2) / det
              uin2 = (-b21*f1 + b11*f2) / det
           ELSE
              mark(ip) = 0
              CYCLE Loop_ip
           ENDIF
           fakt = MAX(ABS(uin1)/TLIMU, ABS(uin2)/TLIMV, 1.0)
           u1   = u1 - uin1 / fakt
           u2   = u2 - uin2 / fakt
           u1   = MAX(MIN(u1,UTOPL), UBOTL)
           u2   = MAX(MIN(u2,VTOPL), VBOTL)

        ENDDO Loop_it

        u1 = (1.-f)*Tri3D%Coord(1,ip) + f*u1
        u2 = (1.-f)*Tri3D%Coord(2,ip) + f*u2
        CALL GetUVPointInfo0(RegionID,u1,u2,pt(1:3))

        !--- check if a better solution found

        IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
           CALL SpacingStorage_GetMapping(BGSpacing, pt, fMap)
        ENDIF
        sideMax = sideMax**2
        sideMin = sideMin**2

        DO i = 1,nList
           IT = ipList(i)
           p3 = 0
           IF(Tri3D%IP_Tri(1,IT)/=IP) p3 = p3 + Tri3D%Posit(:,Tri3D%IP_Tri(1,IT))
           IF(Tri3D%IP_Tri(2,IT)/=IP) p3 = p3 + Tri3D%Posit(:,Tri3D%IP_Tri(2,IT))
           IF(Tri3D%IP_Tri(3,IT)/=IP) p3 = p3 + Tri3D%Posit(:,Tri3D%IP_Tri(3,IT))
           p3 = p3 / 2.d0 
           IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
              xl = Mapping3D_Distance_SQ(pt,p3,fMap)
           ELSE
              xl = Geo3D_Distance_SQ(pt,p3)
           ENDIF
           IF(xl < sideMin) EXIT
           IF(xl > sideMax) EXIT
        ENDDO


        IF(  i>nList ) THEN
           !--- move this node and mark affected nodes
           nMove             = nMove + 1
           Tri3D%Coord(1,ip) = u1
           Tri3D%Coord(2,ip) = u2
           Tri3D%Posit(:,ip) = pt(1:3)
           DO i = 1,nList
              IT = ipList(i)
              Mark(Tri3D%IP_Tri(1:3,IT)) = ismoo1
           ENDDO
        ENDIF

        mark(ip) = 0

     ENDDO Loop_ip

     IF(nMove==0) EXIT

     WRITE(*, '(a,i7)') '       No. of nodes moved: ',nMove
     WRITE(29,'(a,i7)') '       No. of nodes moved: ',nMove

  ENDDO Loop_ismoo

  DEALLOCATE(Mark)
  CALL LinkAssociation_Clear(Tri3D%ITR_Pt)

  RETURN
END SUBROUTINE Surf_Smooth_3D_3

!*------------------------------------------------------------------
!*    [smoothp] moves the Nodes to enhance the quality of the mesh   
!*     smooth in physical space - Eads idea-                        
!*------------------------------------------------------------------

SUBROUTINE SuperPatch_Smooth(Kstr)
  USE control_Parameters
  USE Spacing_Parameters
  USE surface_Parameters
  USE Geometry3DAll
  USE Geometry2D
  USE LinkAssociation
  IMPLICIT NONE

  INTEGER, INTENT(IN)    :: Kstr
  INTEGER :: ip, ie, i, ip3(3), ismoo, jt, it, i1, i2, isp, Isucc
  REAL*8  :: p1(3), p2(3), fMap(3,3), pt(3), f
  REAL*8  :: t1, t2, t, u, u1, u2
  INTEGER, DIMENSION(:), POINTER :: ipList
  INTEGER   ::  nList

  WRITE(*, '(/,a)') '  .... Node smooth for super-patches'
  WRITE(29,'(/,a)') '  .... Node smooth for super-patches'

  ! *** displacement factor f

  f = 0.2

  !--- Build point-point association
  CALL Surf_BuildPtAsso(Surf)

  Loop_ismoo : DO ismoo = 1, 10

     !--- smooth on super curves

     DO isp = 1, NB_SuperCurve
        DO i = 2, SuperCurveNodes(isp)%numNodes -1
           ip = SuperCurveNodes(isp)%Nodes(i)
           IF(inBaseTri(ip)/=-isp) CALL Error_Stop (' not on a super-curve ')
           i1 = SuperCurveNodes(isp)%Nodes(i-1)
           i2 = SuperCurveNodes(isp)%Nodes(i+1)
           p1(:) = Surf%Posit(:,i1)
           p2(:) = Surf%Posit(:,i2)
           pt(:) = Surf%Posit(:,ip)
           CALL GetEgdeLength_SQ(P1,Pt, t1)
           CALL GetEgdeLength_SQ(P2,Pt, t2)
           t1 = dsqrt(t1)
           t2 = dsqrt(t2)
           u1 = SuperCurveNodePt(isp)%V(i-1)
           u2 = SuperCurveNodePt(isp)%V(i+1)
           IF(t1>t2)THEN
              t = (t1-t2)/(2*t1)/2.d0
              u = t * u1 + (1.-t) * SuperCurveNodePt(isp)%V(i)
           ELSE IF(t1<t2)THEN
              t = (t2-t1)/(2*t2)/2.d0
              u = t * u2 + (1.-t) * SuperCurveNodePt(isp)%V(i)
           ENDIF

           SuperCurveNodePt(isp)%V(i) = u
           CALL CurveType_Interpolate (SuperCurve(isp), 0, u, pt)
           Surf%Posit(:,ip) = pt
        ENDDO
     ENDDO

     !--- smooth in super patches

     Loop_ip : DO ip = 1, Surf%NB_Point
        jt = inBaseTri(ip)
        IF(jt<=0) CYCLE

        IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
           CALL SpacingStorage_GetMapping(BGSpacing, Surf%Posit(:,ip), fMap)
        ENDIF

        !--- Calculate the will-be position
        CALL LinkAssociation_List(IP, nList, ipList, Surf%IP_Pt)
        IF(nList<3)THEN
           WRITE(29,*)' Error--- bad connectivety :: IP = ',IP,' nList=',nList
           CALL Error_Stop ('SuperPatch_Smooth  ')
        ENDIF

        pt(:) = 0.d0
        DO i=1,nList
           p2(:)  = Surf%Posit(:,ipList(i))
           IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
              p2(:) = Mapping3D_Posit_Transf(p2, fMap, 1)
           ENDIF
           pt(:) = pt(:) + p2(:)
        ENDDO

        Pt(:) = Pt(:) / nList
        IF(Kstr==1 .AND. BGSpacing%Model<0)THEN
           pt = Mapping3D_Posit_Transf(pt, fMap, -1)
        ENDIF
        pt(:) = (1.-f) * Surf%Posit(:,ip) + f * pt

        !--- project pt to the based triangulation
        CALL SuperPatch_project(Pt, jt, Isucc)

        IF(Isucc==0) CYCLE Loop_ip

        Surf%Posit(:,ip) = Pt
        inBaseTri(ip)    = jt

     ENDDO Loop_ip

  ENDDO Loop_ismoo

  CALL LinkAssociation_Clear(Surf%IP_Pt)

  RETURN
END SUBROUTINE SuperPatch_Smooth



