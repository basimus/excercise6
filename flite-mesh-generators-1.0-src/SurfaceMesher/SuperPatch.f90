!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*------------------------------------------------------------------
!>
!!  Read super-patches defination from file '*.spp'.
!!  The format of the file is                                                         \n
!!      n                  --->  the number if super-patches.                         \n
!!      i_11, i_12, ...    --->  the list of patches forming the 1st super-patches.   \n
!!      i_21, i_22, ...    --->  the list of patches forming the 2nd super-patches.   \n
!!      ...                                                                           \n
!!      i_n1, i_n2, ...    --->  the list of patches forming the last super-patches. 
!!
!!   The sign ':' can be used to express the list. 
!!   Please refer subroutine Number_CHAR_Transfer::Read_INT_Array()
!<
!*------------------------------------------------------------------
SUBROUTINE SuperPatch_Input()
  USE control_Parameters
  USE surface_Parameters
  USE Number_Char_Transfer
  IMPLICIT NONE

  INTEGER :: Ne, list(100000)
  INTEGER :: i, j, ir1, ir2, ic1, ic2, n1, n2

  IF(NB_SuperPatch==0)THEN
     SuperCosmetic_Method = 0
     RETURN
  ENDIF

  IF(SuperCosmetic_Quad==1)THEN
     !--- to generate a quadrangles only mesh, we need set all patchs to super.
     list(1:NumRegions) = 0
     DO i = 1, NB_SuperPatch
        j = SuperPatchList(i)%numNodes
        list(SuperPatchList(i)%Nodes(1:j)) = 1
     ENDDO
     DO i = 1, NumRegions
        IF(list(i)==0)THEN
           NB_SuperPatch = NB_SuperPatch + 1
           CALL IntQueue_Push(SuperPatchList(NB_SuperPatch), i)
        ENDIF
     ENDDO
  ENDIF

  WRITE(*, *)' NB_SuperPatch =',NB_SuperPatch
  WRITE(29,*)' NB_SuperPatch =',NB_SuperPatch
  DO i = 1, NB_SuperPatch
     CALL IntQueue_Write(SuperPatchList(i),6)
     CALL IntQueue_Write(SuperPatchList(i),29)
  ENDDO

END SUBROUTINE SuperPatch_Input

!*------------------------------------------------------------------
!>
!!   Build a basic triangulation for super-patch treatments.
!!   The regular geometry curvature is used.
!<
!*------------------------------------------------------------------
SUBROUTINE SuperPatch_Build()
  USE control_Parameters
  USE surface_Parameters
  USE Geometry3DAll
  USE SurfaceMeshManager
  USE Queue
  IMPLICIT NONE

  INTEGER :: i, i0, i1, i2, i3, i4, j, ie, is, ic, ic1, icc
  INTEGER :: ip3(3), ip, isp, itemp, n
  REAL*8  :: pp3(3,3), u
  TYPE(IntQueueType) :: SuperCurveRing, CurveRing
  INTEGER :: MarkRegion(Surf_MAxNB_Surf)
  INTEGER, DIMENSION(:,:), POINTER :: IReg_Curve

  !--- mark each Region with super-patch ID
  MarkRegion(1:NumRegions) = 0
  DO isp = 1, NB_SuperPatch
     DO i = 1, SuperPatchList(isp)%numNodes
        icc = SuperPatchList(isp)%Nodes(i)
        MarkRegion(icc) = isp
     ENDDO
  ENDDO

  DO icc = 1, NumRegions
     IF(MarkRegion(icc)==0)THEN
        MarkRegion(icc) = icc + NB_SuperPatch
     ENDIF
  ENDDO

  DO ie = 1, Surf%NB_Tri
     Surf%IP_Tri(4,ie) =  MarkRegion(Surf%IP_Tri(5,ie))
  ENDDO

  !--- search two adjacent super-patches for each curve
  ALLOCATE(IReg_Curve(3,NumCurves))
  IReg_Curve(:,:) = -1

  DO icc = 1, NumRegions
     CALL GetRegionCurveList(icc, CurveRing)
     DO i = 1, CurveRing%numNodes
        ic = CurveRing%Nodes(i)
        IF(IReg_Curve(1,ic)==-1)THEN
           IReg_Curve(1,ic) = MarkRegion(icc)
        ELSE
           IReg_Curve(2,ic) = MarkRegion(icc)
        ENDIF
     ENDDO
  ENDDO


  !--- build super-curves for each super-patch
  !    A super-curve concludes all curves between two specified super-patches.

  NB_SuperCurve = 0
  ALLOCATE(SuperCurveList(NumCurves))

  DO isp = 1, NB_SuperPatch

     SuperCurveRing%numNodes = 0

     !--- pick boundary curves for the super patch
     DO i = 1, SuperPatchList(isp)%numNodes
        icc = SuperPatchList(isp)%Nodes(i)
        CALL GetRegionCurveList(icc, CurveRing)
        DO j = 1, CurveRing%numNodes
           ic = CurveRing%Nodes(j)
           IF(IReg_Curve(1,ic)/=IReg_Curve(2,ic)) THEN
              !--- this is a curve on the boundary of the super-patch
              CALL IntQueue_Push (SuperCurveRing, ic)
           ENDIF
        ENDDO
     ENDDO

     !--- from a loop
     ic = SuperCurveRing%Nodes(1)
     i1 = CurveNodes(ic)%Nodes(1)
     i2 = CurveNodes(ic)%back
     DO i = 2, SuperCurveRing%numNodes
        DO j = i, SuperCurveRing%numNodes
           ic = SuperCurveRing%Nodes(j)
           i3 = CurveNodes(ic)%Nodes(1)
           i4 = CurveNodes(ic)%back
           IF(i3==i2 .OR. i4==i2)THEN
              IF(i3==i2)THEN
                 i2 = i4
              ELSE
                 i2 = i3
                 SuperCurveRing%Nodes(j) = -ic
              ENDIF
              IF(j==i) EXIT
              itemp = SuperCurveRing%Nodes(j)
              SuperCurveRing%Nodes(j) = SuperCurveRing%Nodes(i)
              SuperCurveRing%Nodes(i) = itemp
              EXIT
           ENDIF
        ENDDO
        IF(j>SuperCurveRing%numNodes) CALL Error_Stop ('SuperPatch_Build : broken ring  ')
     ENDDO

     !---check
     IF(Debug_Display>0)THEN
        ic = SuperCurveRing%back
        IF(ic<0)THEN
           i4 = CurveNodes(-ic)%Nodes(1)
        ELSE
           i4 = CurveNodes(ic)%back
        ENDIF
        IF(i4/=i1) CALL Error_Stop ('SuperPatch_Build : wrong ring  ') 
     ENDIF

     !--- find a start
     i0 = 0
     DO i = 1, SuperCurveRing%numNodes
        ic = ABS(SuperCurveRing%Nodes(i))
        i1 = i-1
        IF(i1==0) i1 = SuperCurveRing%numNodes
        ic1 = ABS(SuperCurveRing%Nodes(i1))
        IF(IReg_Curve(1,ic)+IReg_Curve(2,ic) /= IReg_Curve(1,ic1)+IReg_Curve(2,ic1))THEN
           i0 = i
           EXIT
        ENDIF
     ENDDO
     IF(i0==0) i0 = 1

     !--- build super curve

     i = i0
     DO
        ic  = ABS(SuperCurveRing%Nodes(i))
        IF( IReg_Curve(3,ic)==-1 .AND.    &
             MAX(IReg_Curve(1,ic),IReg_Curve(2,ic))<=NB_SuperPatch)THEN
           i1 = i-1
           IF(i1==0) i1 = SuperCurveRing%numNodes
           ic1 = ABS(SuperCurveRing%Nodes(i1))
           IF( i==i0 .OR.   &
                IReg_Curve(1,ic)+IReg_Curve(2,ic) /= IReg_Curve(1,ic1)+IReg_Curve(2,ic1))THEN
              !--- start a new super-curve
              NB_SuperCurve = NB_SuperCurve + 1
           ENDIF
           CALL IntQueue_Push (SuperCurveList(NB_SuperCurve), SuperCurveRing%Nodes(i))
           IReg_Curve(3,ic) = NB_SuperCurve
        ENDIF
        i = i+1
        IF(i>SuperCurveRing%numNodes) i = 1
        IF(i==i0) EXIT
     ENDDO

  ENDDO

  CALL IntQueue_Clear(SuperCurveRing)
  CALL IntQueue_Clear(CurveRing)

  WRITE(*, *)' NB_SuperCurve=', NB_SuperCurve
  WRITE(29,*)' NB_SuperCurve=', NB_SuperCurve
  DO isp = 1, NB_SuperCurve
     CALL IntQueue_Write(SuperCurveList(isp),6)
     CALL IntQueue_Write(SuperCurveList(isp),29)
  ENDDO


  !--- build based triangulation

  CALL Surf_BuildNext(Surf)

  ALLOCATE(TriBase(Surf%NB_Tri))
  ALLOCATE(inBaseTri(Surf%NB_Point))

  ALLOCATE(MarkVisit(Surf%NB_Tri))
  MarkVisit(:) = 0

  CALL SurfaceMeshStorage_BasicCopy(Surf, SurfBase)
  DO ie = 1, SurfBase%NB_Tri
     pp3(:,1) = SurfBase%Posit(:, SurfBase%IP_Tri(1,ie))
     pp3(:,2) = SurfBase%Posit(:, SurfBase%IP_Tri(2,ie))
     pp3(:,3) = SurfBase%Posit(:, SurfBase%IP_Tri(3,ie))
     CALL Geo3D_Triangle_Axes(pp3,TriBase(ie)%WeightAxes,TriBase(ie)%WeightShift)
  ENDDO

  !--- Build next
  ALLOCATE(EdgeSuperID(Surf%NB_Edge))
  EdgeSuperID(:) = 0

  DO ie = 1, SurfBase%NB_Tri
     IF(SurfBase%IP_Tri(4,ie)>NB_SuperPatch) CYCLE

     icc = SurfBase%IP_Tri(5,ie)

     ip3(1:3) = Surf%IP_Tri(1:3,ie)
     inBaseTri(ip3(1:3)) = ie
     DO i= 1,3
        i1 = SurfBase%Next_Tri(i,ie)        
        IF(SurfBase%IP_Tri(4,i1)/=SurfBase%IP_Tri(4,ie))THEN
           !--- two triangles on different patchs (regions)
           SurfBase%Next_Tri(i,ie) = -1
           IF(SurfBase%IP_Tri(4,i1)<=NB_SuperPatch)THEN
              IS = Surf%IED_Tri(i,ie)
              EdgeSuperID(IS) = -1
           ENDIF
        ENDIF
     ENDDO


     !--- record coord
     DO i=1,3
        IF(ip3(i)>numAllCurveNodes)THEN
           !--- node inside a Region
           TriBase(ie)%Coord(:,i) = SurfBase%Coord(:,ip3(i))
        ELSE
           !--- node on a curve
           DO j = 1, InterNodes(ip3(i))%numPatches
              IF(InterNodes(ip3(i))%Patches(j)==icc)THEN
                 TriBase(ie)%Coord(:,i) = InterNodes(ip3(i))%Coord(:,j)
              ENDIF
           ENDDO
        ENDIF
     ENDDO

  ENDDO


  !--- calculate the curveture of super curves

  ALLOCATE(SuperCurve (NB_SuperCurve) )
  ALLOCATE(SuperCurveNodes (NB_SuperCurve) )
  ALLOCATE(SuperCurveNodept(NB_SuperCurve) )

  DO isp = 1, NB_SuperCurve
     SuperCurve(isp)%ID       = isp
     SuperCurve(isp)%TopoType = 1
     n = 0
     DO i = 1, SuperCurveList(isp)%numNodes
        ic = ABS(SuperCurveList(isp)%Nodes(i))
        n  = n + CurveNodes(ic)%numNodes
     ENDDO
     ALLOCATE(SuperCurve(isp)%Posit(3,n))

     n = 0
     DO i = 1, SuperCurveList(isp)%numNodes
        ic = SuperCurveList(isp)%Nodes(i)

        IF(ic>0)THEN
           i1 = 1
           i2 = CurveNodes(ic)%numNodes-1
           i3 = 1
        ELSE
           ic = -ic
           i1 = CurveNodes(ic)%numNodes
           i2 = 2
           i3 = -1
        ENDIF
        IF(i==SuperCurveList(isp)%numNodes) i2 = i2 + i3

        DO j = i1, i2, i3
           n  = n+1
           ip = CurveNodes(ic)%Nodes(j)
           SuperCurve(isp)%Posit(:,n) = SurfBase%Posit(:,ip)
           CALL IntQueue_Push(SuperCurveNodes(isp), ip)
        ENDDO
     ENDDO

     DO i = 0, SuperCurveList(isp)%numNodes-1
        u = i
        CALL Real8Queue_Push36(SuperCurveNodePt(isp), u)
     ENDDO

     SuperCurve(isp)%numNodes = n
     CALL CurveType_BuildTangent (SuperCurve(isp))

     !--- mark those Nodes on super curves with inBaseTri(ip) = -isp or 0 on ends
     DO i = 1, SuperCurveNodes(isp)%numNodes
        ip = SuperCurveNodes(isp)%Nodes(i)
        IF(i==1 .OR. i==SuperCurveNodes(isp)%numNodes)THEN
           !--- end of a super-curve
           inBaseTri(ip) = 0
        ELSE
           !--- node on a super-curve
           inBaseTri(ip) = -isp
        ENDIF
     ENDDO

  ENDDO


  !--- mark those Nodes on normal Region with inBaseTri(ip) = 0
  DO ie = 1, Surf%NB_Tri
     IF(Surf%IP_Tri(4,ie)>NB_SuperPatch)THEN
        ip3(1:3) = Surf%IP_Tri(1:3,ie)
        inBaseTri(ip3(1:3)) = 0
     ENDIF
  ENDDO

  !--- mark those edges on super-curve
  DO is = 1, Surf%NB_Edge
     ip3(1:2) = Surf%IP_Edge(:,IS)
     IF(EdgeSuperID(IS)<0)THEN
        IF(inBaseTri(ip3(1))<0 .AND. inBaseTri(ip3(1))==inBaseTri(ip3(2)))THEN
           EdgeSuperID(IS) = -inBaseTri(ip3(1))
        ELSE IF(inBaseTri(ip3(1))<0 .AND. inBaseTri(ip3(2))==0)THEN
           EdgeSuperID(IS) = -inBaseTri(ip3(1))
        ELSE IF(inBaseTri(ip3(2))<0 .AND. inBaseTri(ip3(1))==0)THEN
           EdgeSuperID(IS) = -inBaseTri(ip3(2))
        ELSE
           EXIT
        ENDIF
     ENDIF
  ENDDO

  IF(IS<=Surf%NB_Edge)THEN
     WRITE(29,*) ' Error---- wrong EdgeSuperID'
     WRITE(29,*) '   IS, ip1,ip2=',IS, ip3(1:2)
     WRITE(29,*) '   EdgeSuperID(IS), inBaseTri(ip)=',EdgeSuperID(IS), inBaseTri(ip3(1:2))
     CALL Error_Stop ('SuperPatch_Build  ') 
  ENDIF


  !--- some geometry
  ALLOCATE( SurfTri(     Surf%NB_Tri   ) )
  DO ie = 1, Surf%NB_Tri
     CALL SurfTri_CalGeom  (Surf, SurfTri, ie)
  ENDDO

  DEALLOCATE(InterNodes)

  RETURN
END SUBROUTINE SuperPatch_Build


!>
!!   Build a basic triangulation for super-patch treatments.
!!   The present triangulation is used to calculate the curvature.
!<
SUBROUTINE SuperPatch_Build3(  )
  USE control_Parameters
  USE surface_Parameters
  USE CellConnectivity
  USE Geometry3DAll
  IMPLICIT NONE

  REAL*8  :: cosang, cosa, dss, ds1, ds2, ds11, ds22, pp3(3,3), u
  INTEGER :: IS, IP, itR, itL, ip0, ip1, ip2, ipR, ipL, k, j, ip1t, ip2t, n, isp
  INTEGER :: i, ie, ip3(3)
  INTEGER, DIMENSION(:), POINTER :: Markp
  TYPE(IntQueueType) :: edgelist
  LOGICAL :: CarryOn


  cosang = COS(SuperCurve_RidgeAngle)

  !--- build edges and association.
  CALL Surf_BuildNext( Surf )
  CALL Surf_BuildTriAsso( Surf )

  !--- calculate geometry for each traingle and node.

  ALLOCATE( SurfTri(     Surf%NB_Tri   ) )
  ALLOCATE( SurfNode(    Surf%NB_Point ) )

  CALL SurfGeo_Build(Surf, SurfTri, SurfNode)

  !--- Search ridge edges by dihedral angle and edge length

  ALLOCATE( EdgeSuperID(   Surf%NB_Edge  ) )

  EdgeSuperID(:) = 0
  DO  IS = 1, Surf%NB_Edge

     !--- for frequency filter, no ridge-curve set.
     IF(Loop_SuperFrqFilter>0) CYCLE

     itR   = Surf%ITR_Edge(1,IS)
     itL   = Surf%ITR_Edge(2,IS)
     IF(itR==0 .OR. itL==0)THEN
        !--- a boundary edge
        CALL Error_Stop (' SuperPatch_Build3:: a boundary edge ')
     ENDIF

     cosa = Geo3D_Dot_Product(SurfTri(itR)%anor, SurfTri(itL)%anor)
     IF(cosa<=cosang)THEN
        !--- a ridge edge (sharp blade)
        EdgeSuperID(IS) = 1
        CYCLE
     ENDIF

     IF(1==0)THEN
     !............comparing the edge lengthes to specify the ridges.....
     ip1 = Surf%IP_Edge(1,IS)
     ip2 = Surf%IP_Edge(2,IS)

     k   = which_NodeinTri(IS, Surf%IED_Tri(:,itL))
     ipL = Surf%IP_Tri(k,itL)
     k   = which_NodeinTri(IS, Surf%IED_Tri(:,itR))
     ipR = Surf%IP_Tri(k,itR)

     dss  = Geo3D_Distance(Surf%Posit(:,ip1), Surf%Posit(:,ip2)) 
     ds1  = Geo3D_Distance(Surf%Posit(:,ip1), Surf%Posit(:,ipR)) 
     ds2  = Geo3D_Distance(Surf%Posit(:,ip2), Surf%Posit(:,ipR)) 
     ds11 = MAX(ds1,ds2)
     IF(ds11>=6*dss)THEN
        !--- a ridge edge (big triangles)
        EdgeSuperID(IS) = 1
        CYCLE
     ENDIF

     ds1  = Geo3D_Distance(Surf%Posit(:,ip1), Surf%Posit(:,ipL)) 
     ds2  = Geo3D_Distance(Surf%Posit(:,ip2), Surf%Posit(:,ipL)) 
     ds22 = MAX(ds1,ds2)
     IF(ds22>=6*dss) EdgeSuperID(IS) = 1
     ENDIF

  ENDDO

  !--- search ridge nodes

  ALLOCATE(Markp(SUrf%NB_Point))
  Markp(:) = 0

  DO IS = 1,Surf%NB_Edge
     IF(EdgeSuperID(IS)==0) CYCLE
     DO j = 1,2
        IP = Surf%IP_Edge(j,IS)
        Markp(IP) = Markp(IP) + 1
        IF(Markp(IP)>MaxNumParts) CALL Error_Stop ('SuperPatch_Build3: Markp(IP)>MaxNumParts  ')
     ENDDO
  ENDDO

  !--- remove those single-edge

  DO IS = 1,Surf%NB_Edge
     IF(EdgeSuperID(IS)==0) CYCLE
     ip1t = Markp(Surf%IP_Edge(1,IS))
     ip2t = Markp(Surf%IP_Edge(2,IS))
     IF(  (ip1t==1 .AND. (ip2t==1 .OR. ip2t>=3) ) .OR.    &
          (ip1t>=3 .AND.  ip2t==1 ) )THEN
        EdgeSuperID(IS) = 0
        Markp(Surf%IP_Edge(1,IS)) = ip1t - 1
        Markp(Surf%IP_Edge(2,IS)) = ip2t - 1
     ENDIF
  ENDDO
  CarryOn = .TRUE.
  DO WHILE(CarryOn)
     CarryOn = .FALSE.
     DO IS = 1,Surf%NB_Edge
        IF(EdgeSuperID(IS)==0) CYCLE
        ip1t = Markp(Surf%IP_Edge(1,IS))
        ip2t = Markp(Surf%IP_Edge(2,IS))
        IF( ip1t>=3 .AND. ip2t>=3 )THEN
           CarryOn = .TRUE.
           EdgeSuperID(IS) = 0
           Markp(Surf%IP_Edge(1,IS)) = ip1t - 1
           Markp(Surf%IP_Edge(2,IS)) = ip2t - 1
        ENDIF
     ENDDO
  ENDDO


  !--- count ridge nodes and corner node

  DO IP = 1,Surf%NB_Point
     IF(Markp(IP)>=2)THEN
        SurfNode(IP)%numParts = Markp(IP)
        SurfNode(IP)%onRidge  = 1
     ELSE IF(Markp(IP)==1)THEN
        !--- the end node of a ridge line
        SurfNode(IP)%onRidge = -1
     ELSE
        SurfNode(IP)%onRidge = 0
     ENDIF

     IF(Markp(IP)>=3 .OR. Markp(IP)==1)THEN
        SurfNode(IP)%movable = .FALSE.
     ELSE
        SurfNode(IP)%movable = .TRUE.
     ENDIF
  ENDDO


  !........check of the ridge identification.....
  IF(Debug_Display>0)THEN
     DO IS = 1, Surf%NB_Edge
        IF(EdgeSuperID(IS)==0) CYCLE
        DO j = 1,2
           IF(SurfNode(Surf%IP_Edge(j,IS))%onRidge==0)THEN
              WRITE(29,*) ' Error--- in ridge identification'
              WRITE(29,*) '    IS', IS,   Surf%IP_Edge(1,IS), Surf%IP_Edge(2,IS)
              CALL Error_Stop (' SuperPatch_Build3:: ridge identification ')
           ENDIF
        ENDDO
     ENDDO
  ENDIF

  !--- build super-curve by ridge

  DO IP = 1, Surf%NB_Point
     IF(Markp(IP)==2)THEN
        Markp(IP) = IP
     ELSE
        Markp(IP) = 0
     ENDIF
  ENDDO

  CarryOn = .TRUE.
  DO WHILE(CarryOn)
     CarryOn = .FALSE.
     DO IS = 1, Surf%NB_Edge
        IF(EdgeSuperID(IS)==0) CYCLE
        IF(Markp(Surf%IP_Edge(1,IS))==0) CYCLE
        IF(Markp(Surf%IP_Edge(2,IS))==0) CYCLE
        IF(Markp(Surf%IP_Edge(1,IS))==Markp(Surf%IP_Edge(2,IS))) CYCLE

        CarryOn = .TRUE.
        IF(Markp(Surf%IP_Edge(1,IS)) > Markp(Surf%IP_Edge(2,IS))) THEN
           Markp(Surf%IP_Edge(1,IS)) = Markp(Surf%IP_Edge(2,IS))
        ELSE
           Markp(Surf%IP_Edge(2,IS)) = Markp(Surf%IP_Edge(1,IS))
        ENDIF
     ENDDO
  ENDDO

  NB_SuperCurve = 0
  DO IP = 1, Surf%NB_Point
     IF(Markp(IP)==0) CYCLE
     IF(Markp(IP)==IP)THEN
        NB_SuperCurve = NB_SuperCurve + 1
        Markp(IP)     = NB_SuperCurve
     ELSE IF(Markp(IP)<IP)THEN
        Markp(IP) = Markp(Markp(IP))
     ELSE
        CALL Error_Stop ('SuperPatch_Build3: mark(ip)>ip  ') 
     ENDIF
     IF(Markp(IP)==0) CALL Error_Stop (' SuperPatch_Build3: Markp(IP)==0 ')
  ENDDO

  WRITE(*, *) '  NB_SuperCurve=',NB_SuperCurve
  WRITE(29,*) '  NB_SuperCurve=',NB_SuperCurve

  DO IS = 1, Surf%NB_Edge
     IF(EdgeSuperID(IS)==0) CYCLE
     IF(Markp(Surf%IP_Edge(1,IS))>0)THEN
        EdgeSuperID(IS) = Markp(Surf%IP_Edge(1,IS))
     ELSE IF(Markp(Surf%IP_Edge(2,IS))>0)THEN
        EdgeSuperID(IS) = Markp(Surf%IP_Edge(2,IS))
     ELSE 
        WRITE(29,*) 'Error--- wrong ridge: IS,IPs,Marks :'
        WRITE(29,*) '  ',IS,Surf%IP_Edge(:,IS),Markp(Surf%IP_Edge(:,IS))
        CALL Error_Stop (' SuperPatch_Build3:: wrong ridge')
     ENDIF
  ENDDO

  !--- build the curveture of super curves

  ALLOCATE(SuperCurve (NB_SuperCurve) )
  ALLOCATE(SuperCurveNodes (NB_SuperCurve) )
  ALLOCATE(SuperCurveNodept(NB_SuperCurve) )

  !--- find edges in each super-curve

  DO IS = 1, Surf%NB_Edge
     isp = EdgeSuperID(IS)
     IF(isp>0) THEN
        CALL IntQueue_Push36(SuperCurveNodes(isp), IS)
     ENDIF
  ENDDO


  DO isp = 1, NB_SuperCurve
     SuperCurve(isp)%ID       = isp
     SuperCurve(isp)%TopoType = 1

     CALL IntQueue_COPY(SuperCurveNodes(isp), edgelist)
     IF(edgelist%numNodes<2) CALL Error_Stop ('SuperPatch_Build3: edgelist%numNodes<2  ') 

     !--- find nodes in each super-curve

     !--- search is there is a break at first
     n = edgelist%numNodes
     DO j = 1, n
        is = edgelist%Nodes(j)
        IF(Markp(Surf%IP_Edge(1,is))==0)THEN
           ip0 = Surf%IP_Edge(1,is)
           ip1 = Surf%IP_Edge(2,is)
           CALL IntQueue_Remove(edgelist, is)
           EXIT
        ELSE IF(Markp(Surf%IP_Edge(2,is))==0)THEN
           ip0 = Surf%IP_Edge(2,is)
           ip1 = Surf%IP_Edge(1,is)
           CALL IntQueue_Remove(edgelist, is)
           EXIT
        ENDIF
     ENDDO

     IF(j>n)THEN
        !--- no broken end, the surper edge forms a loop.
        is  = edgelist%Nodes(1)
        ip0 = Surf%IP_Edge(1,is)
        ip1 = Surf%IP_Edge(2,is)
        CALL IntQueue_Remove(edgelist, is)
     ENDIF

     SuperCurveNodes(isp)%numNodes = 0
     CALL IntQueue_Push36(SuperCurveNodes(isp), ip0)
     CALL IntQueue_Push36(SuperCurveNodes(isp), ip1)

     DO WHILE(edgelist%numNodes>0)
        n = edgelist%numNodes
        DO j = 1, n
           is = edgelist%Nodes(j)
           IF(Surf%IP_Edge(1,is)==ip1)THEN
              ip2 = Surf%IP_Edge(2,is)
           ELSE IF(Surf%IP_Edge(2,is)==ip1)THEN
              ip2 = Surf%IP_Edge(1,is)
           ELSE
              CYCLE
           ENDIF

           CALL IntQueue_Remove(edgelist, is)
           CALL IntQueue_Push36(SuperCurveNodes(isp), ip2)
           IF(ip2==ip0 .AND. edgelist%numNodes>0)THEN
              WRITE(29,*)'Error--- found multi-loop. isp=',isp
              CALL Error_Stop (' SuperPatch_Build3  ')
           ENDIF

           ip1 = ip2
           EXIT
        ENDDO
        IF(j>n)  CALL Error_Stop (' SuperPatch_Build3:  broken curve ')
     ENDDO

     !--- set the ends as corner nodes

     SurfNode(SuperCurveNodes(isp)%Nodes(1)) %movable = .FALSE.
     SurfNode(SuperCurveNodes(isp)%back)     %movable = .FALSE.

     !--- build super-curve by those nodes

     SuperCurve(isp)%numNodes = SuperCurveNodes(isp)%numNodes
     ALLOCATE( SuperCurve(isp)%Posit(3,SuperCurve(isp)%numNodes) )

     DO j = 1, SuperCurveNodes(isp)%numNodes
        ip = SuperCurveNodes(isp)%Nodes(j)
        SuperCurve(isp)%Posit(:,j) = Surf%Posit(:,ip)
        u = j-1
        CALL Real8Queue_Push36(SuperCurveNodePt(isp), u)
     ENDDO
     CALL CurveType_BuildTangent (SuperCurve(isp))

  ENDDO

  DEALLOCATE(Markp)
  CALL IntQueue_Clear(edgelist)

  !--- For ridge and corner points' information....    

  CALL Ridge_CountPart3( )   

  IF(SuperCosmetic_Method==2)THEN
     !--- build basic trangulation for projecting

     CALL SurfaceMeshStorage_BasicCopy(Surf, SurfBase)

     ALLOCATE(TriBase(Surf%NB_Tri))
     DO ie = 1, SurfBase%NB_Tri
        pp3(:,1) = SurfBase%Posit(:, SurfBase%IP_Tri(1,ie))
        pp3(:,2) = SurfBase%Posit(:, SurfBase%IP_Tri(2,ie))
        pp3(:,3) = SurfBase%Posit(:, SurfBase%IP_Tri(3,ie))
        CALL Geo3D_Triangle_Axes(pp3,TriBase(ie)%WeightAxes,TriBase(ie)%WeightShift)
     ENDDO

     !--- set inBaseTri

     ALLOCATE(inBaseTri(Surf%NB_Point))

     DO ie = 1, SurfBase%NB_Tri
        ip3(1:3) = Surf%IP_Tri(1:3,ie)
        inBaseTri(ip3(1:3)) = ie
     ENDDO

     DO isp = 1, NB_SuperCurve
        DO i = 1, SuperCurveNodes(isp)%numNodes
           ip = SuperCurveNodes(isp)%Nodes(i)
           IF(i==1 .OR. i==SuperCurveNodes(isp)%numNodes)THEN
              !--- end of a super-curve
              inBaseTri(ip) = 0
           ELSE
              !--- node on a super-curve
              inBaseTri(ip) = -isp
           ENDIF
        ENDDO
     ENDDO

     ALLOCATE(NodeBase(Surf%NB_Point))
     DO ip = 1, Surf%NB_Point
        CALL SurfaceNode_Copy(SurfNode(ip), NodeBase(ip))
     ENDDO
     DEALLOCATE(SurfNode)

     ALLOCATE(MarkVisit(Surf%NB_Tri))
     MarkVisit(:) = 0

  ENDIF

  RETURN
END SUBROUTINE SuperPatch_Build3

!>
!!   For a node on a ridge-edge, this edge splits the domain into 
!!      two or mode parts which have uncontinuous normal direction.
!!   This routine is counting parts for such nodes.
!<
SUBROUTINE Ridge_CountPart3()

  USE surface_Parameters
  USE CellConnectivity
  IMPLICIT NONE

  INTEGER :: mf, IP, IT, IT0, IS, ipart, nList
  INTEGER :: i, k, i0
  REAL*8  :: pn(3)
  INTEGER, DIMENSION(:), POINTER :: isList


  Loop_ip : DO IP = 1, Surf%NB_Point

     IF(SurfNode(IP)%onRidge<=0) CYCLE

     mf  = SurfNode(IP)%numParts

     CALL LinkAssociation_List(ip, nList, isList, Surf%ITR_Pt)

     !............Now, we divide them into several parts and compute
     !............the normal direction as well.......

     ipart = 0
     i0    = 0
     IT0   = 0
     IT    = isList(1)
     IF(IT==0) CALL Error_Stop ('Ridge_CountPart :: IT==0  ') 

     DO i = 1, 2*nList       
        k  = which_NodeinTri_Next(IP, Surf%IP_Tri(1:3,IT))
        IS = Surf%IED_Tri(k,IT)
        IF(Surf%ITR_Edge(1,IS)==IT)THEN
           IT = Surf%ITR_Edge(2,IS)
        ELSE
           IT = Surf%ITR_Edge(1,IS)
        ENDIF
        IF(IT==IT0) EXIT

        IF(EdgeSuperID(IS)>0)THEN
           ipart = ipart+1
           IF(ipart==1) IT0 = IT
           IF(ipart==1) i0  = i
        ENDIF

        IF(ipart>0) CALL IntQueue_Push(SurfNode(IP)%PartTri(ipart), IT)
     ENDDO

     IF(i-i0/=nList .OR. IT/=IT0 .OR. EdgeSuperID(IS)==0 .OR. ipart/=mf)THEN
        WRITE(29,*)' Error--- i-i0/=nList .OR. IT/=IT0 .OR. EdgeSuperID(IS)==0 .OR. ipart<=1'
        WRITE(29,*)   i, i0,nList, IT, IT0, EdgeSuperID(IS), ipart, ' IP=', ip
        CALL Error_Stop ('Ridge_CountPart3  ') 
     ENDIF

     DO ipart = 1, mf
        nList  =  SurfNode(IP)%PartTri(ipart)%numNodes
        isList => SurfNode(IP)%PartTri(ipart)%Nodes

        CALL normp(nList,isList,pn)
        SurfNode(IP)%anor(:,ipart) = pn(:)
     ENDDO

  ENDDO Loop_ip

  RETURN
END SUBROUTINE Ridge_CountPart3



!*-------------------------------------------------------*
!>
!!  Cosmetric loops for super-patches.
!<
!*-------------------------------------------------------*
SUBROUTINE SuperPatch_Cosmetric()
  USE control_Parameters
  USE surface_Parameters
  USE Spacing_Parameters
  USE SurfaceMeshManager
  IMPLICIT NONE
  INTEGER :: iloop
  REAL*8  :: BasicSize_Save

  IF(Loop_SuperCosmetic==0 .AND. Loop_SuperFrqFilter==0) RETURN

  BasicSize_Save = BGSpacing%BasicSize

  DO iloop = 1, Loop_SuperFrqFilter
     BGSpacing%BasicSize = 4*SuperFrqFilter_power * BasicSize_Save
     CALL SuperPatch_EdgeCollapse( )
     CALL SuperFrqFlt_EdgeCollapse( )
     BGSpacing%BasicSize = BasicSize_Save
     CALL SuperPatch_EdgeBreak( )
  ENDDO

  IF(Loop_SuperFrqFilter==0)THEN
     CALL SuperPatch_EdgeBreak( )
  ENDIF
  CALL SuperPatch_EdgeCollapse( )


  DO iloop = 1, Loop_SuperCosmetic
     CALL SuperPatch_DelaunaySwap(1)
     IF(SuperCosmetic_Method==3)THEN
        WRITE(*, *)'warning----------ooooout---under constructured'
        WRITE(29,*)'warning----------ooooout---under constructured'
        RETURN
     ENDIF
     CALL SuperPatch_Smooth(0)
  ENDDO

END SUBROUTINE SuperPatch_Cosmetric



