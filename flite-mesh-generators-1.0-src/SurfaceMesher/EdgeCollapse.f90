!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!   Collapse tiny angle.
!<
SUBROUTINE Surf_AngleCollapse(Tri3D, nDim)
  USE control_Parameters
  USE Spacing_Parameters
  USE surface_Parameters, ONLY : SurfTri
  USE SurfaceMeshManager
  USE LinkAssociation
  IMPLICIT NONE
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  INTEGER, INTENT(IN) :: nDim

  INTEGER :: ip1, ip2, ip3, IS, IT, itt, nmove, is0, IE
  INTEGER :: icheck, idirt, idir0, itmp, ipL, ipR, itL, itR, j1L, j1R, j2L, j2R
  INTEGER :: n, i, iss
  REAL*8  :: p0(3), p1(3), p2(3), p3(3), pnew(3), ratio, qual, qual0, fmap(3,3)
  REAL*8  :: q1(2), q2(2), q3(2), tinyang2D
  INTEGER, DIMENSION(:), POINTER :: Mark
  INTEGER, DIMENSION(:), POINTER :: isList
  INTEGER   ::  nList


  WRITE(*, '(/,a,I3)') '  .... Angle Collapse, nDim=',nDim
  WRITE(29,'(/,a,I3)') '  .... Angle Collapse, nDim=',nDim

  IF(nDim/=2 .AND. nDim/=3) CALL Error_Stop(' Surf_AngleCollapse:: wrong nDim')
  tinyang2D = 0.01 * Collapse_Angle

  !--- build normal of triangles
  
  IF(ASSOCIATED(SurfTri))THEN
     IF(SIZE(SurfTri)<Tri3D%NB_Tri) DEALLOCATE(SurfTri)
  ENDIF
  IF(.NOT. ASSOCIATED(SurfTri))  ALLOCATE(SurfTri(2*Tri3D%NB_Tri))  
  DO IE = 1, Tri3D%NB_Tri
     CALL SurfTri_CalGeom(Tri3D, SurfTri, IE)
  ENDDO
  
  !--- Build point-edge & point-tri association

  CALL Surf_BuildTriAsso(Tri3D)
  CALL Surf_BuildEdgeAsso(Tri3D)

  ALLOCATE(Mark(Tri3D%NB_Point))
  Mark(1:Tri3D%NB_Point) = 0


  nMove = 0

  DO IE = 1, Tri3D%NB_Tri
     IF(Tri3D%IP_Tri(1,IE)==0) CYCLE

     IF(nDim==3)THEN

        !--- check the angles for the triangle
        P1(:) = Tri3D%Posit(:,Tri3D%IP_Tri(1,IE))
        P2(:) = Tri3D%Posit(:,Tri3D%IP_Tri(2,IE))
        P3(:) = Tri3D%Posit(:,Tri3D%IP_Tri(3,IE))
        IF(BGSpacing%Model<0)THEN
           p0(:) = (p1 + p2 + p3) /3.d0
           CALL SpacingStorage_GetMapping(BGSpacing, P0, fMap)
           p1 = Mapping3D_Posit_Transf(p1, fMap, 1)
           p2 = Mapping3D_Posit_Transf(p2, fMap, 1)
           p3 = Mapping3D_Posit_Transf(p3, fMap, 1)
        ENDIF

        IF(      Geo3D_Included_Angle (P2, P1, P3)>Collapse_Angle) THEN
           IF(   Geo3D_Included_Angle (P3, P2, P1)>Collapse_Angle) THEN
              IF(Geo3D_Included_Angle (P1, P3, P2)>Collapse_Angle) CYCLE
           ENDIF
        ENDIF

     ELSE

        !--- check the angles for the triangle
        Q1(:) = Tri3D%Coord(:,Tri3D%IP_Tri(1,IE))
        Q2(:) = Tri3D%Coord(:,Tri3D%IP_Tri(2,IE))
        Q3(:) = Tri3D%Coord(:,Tri3D%IP_Tri(3,IE))

        IF(      Included_Angle_2DP (Q2, Q1, Q3)>tinyang2D) THEN
           IF(   Included_Angle_2DP (Q3, Q2, Q1)>tinyang2D) THEN
              IF(Included_Angle_2DP (Q1, Q3, Q2)>tinyang2D) CYCLE
           ENDIF
        ENDIF

     ENDIF

     !--- check 3 edges to find the best direction to collapse
     is0   = 0
     qual0 = 0
     idir0 = 0
     DO i = 1,3
        IS   = Tri3D%IED_Tri(i,IE)

        !--- check the topo
        CALL movedirect_topo(Tri3D, IS, idirt)
        IF(idirt==0) CYCLE

        icheck = idirt
        pnew   = 0
        CALL movedirect_Qual(Tri3D, IS, icheck, idirt, pnew, qual)
        IF(idirt==0) CYCLE
        IF(idirt==3) CALL Error_Stop('Surf_AngleCollapse:: no this option')

        IF(qual>qual0)THEN
           is0 = IS
           qual0 = qual
           idir0 = idirt
        ENDIF
     ENDDO

     IF(IS0==0) CYCLE     !---- no good direction found
     idirt = idir0
     IS  = IS0
     ip1 = Tri3D%IP_Edge(1,IS)
     ip2 = Tri3D%IP_Edge(2,IS)

     nMove = nMove + 1

     !--- move ip1 to ip2
     IF(Mark(ip1)>0 .OR. Mark(ip2)>0) CALL Error_Stop ('Surf_AngleCollapse ::  marked ip1 ip2 ')
     CALL Surf_GetEdgeSurround(Tri3D, IS,itL,itR,ipL,ipR,j2L,j1L,j2R,j1R)

     IF(idirt==1)THEN     
        ip1 = Tri3D%IP_Edge(2,IS)
        ip2 = Tri3D%IP_Edge(1,IS)
        itmp = j1L
        j1L  = j2L
        j2L  = itmp
        itmp = j1R
        j1R  = j2R
        j2R  = itmp
     ENDIF

     Mark(ip1) = ip2           !--- move ip1 to ip2

     !--- update ip_tri

     CALL LinkAssociation_Remove(3, itR, Tri3D%IP_tri(1:3,itR), Tri3D%ITR_Pt)
     CALL LinkAssociation_Remove(3, itL, Tri3D%IP_tri(1:3,itL), Tri3D%ITR_Pt)
     Tri3D%IP_tri(1,itR) = 0
     Tri3D%IP_tri(1,itL) = 0

     CALL LinkAssociation_List(ip1, nList, isList, Tri3D%ITR_Pt)
     DO n = 1, nList
        IT = isList(n)
        WHERE(Tri3D%IP_Tri(1:3,IT)==ip1) Tri3D%IP_Tri(1:3,IT) = ip2
        CALL IntQueue_Push36(Tri3D%ITR_Pt%JointLinks(ip2), IT)
     ENDDO

     !--- update edge

     CALL LinkAssociation_Remove(2, IS,  Tri3D%IP_Edge(:,IS),  Tri3D%IED_Pt)
     CALL LinkAssociation_Remove(2, j1L, Tri3D%IP_Edge(:,j1L), Tri3D%IED_Pt)
     CALL LinkAssociation_Remove(2, j1R, Tri3D%IP_Edge(:,j1R), Tri3D%IED_Pt)

     Tri3D%IP_Edge(1,IS)  = 0
     Tri3D%IP_Edge(1,j1L) = 0
     Tri3D%IP_Edge(1,j1R) = 0

     !---- update IED_Tri
     IF(Tri3D%ITR_Edge(1,j1L)==itL)THEN
        itt = Tri3D%ITR_Edge(2,j1L)
     ELSE
        itt = Tri3D%ITR_Edge(1,j1L)
     ENDIF
     WHERE(Tri3D%ITR_Edge(:,j2L)==itL) Tri3D%ITR_Edge(:,j2L) = itt
     WHERE(Tri3D%IED_Tri(:, itt)==j1L) Tri3D%IED_Tri(:, itt) = j2L

     IF(Tri3D%ITR_Edge(1,j1R)==itR)THEN
        itt = Tri3D%ITR_Edge(2,j1R)
     ELSE
        itt = Tri3D%ITR_Edge(1,j1R)
     ENDIF
     WHERE(Tri3D%ITR_Edge(:,j2R)==itR) Tri3D%ITR_Edge(:,j2R) = itt
     WHERE(Tri3D%IED_Tri(:, itt)==j1R) Tri3D%IED_Tri(:, itt) = j2R

     CALL LinkAssociation_List(ip1, nList, isList, Tri3D%IED_Pt)
     DO n = 1, nList
        iss = isList(n)
        ip3 = 0
        DO i=1,2
           IF(Tri3D%IP_Edge(i,iss)==ip1)THEN
              Tri3D%IP_Edge(i,iss) = ip2
           ELSE
              ip3 = Tri3D%IP_Edge(i,iss)
           ENDIF
        ENDDO

        IF(ip3==0 .OR. ip3==ip2) CALL Error_Stop ('Surf_AngleCollapse :: ip3==0 .OR. ip3==ip2 ')

        CALL IntQueue_Push36(Tri3D%IED_Pt%JointLinks(ip2),iss) 
     ENDDO
     
  ENDDO

  CALL LinkAssociation_Clear(Tri3D%ITR_Pt)
  CALL LinkAssociation_Clear(Tri3D%IED_Pt)

  WRITE(*,  '(a,i7,/)') '          .. No. of removed nodes : ',  nMove
  WRITE(29, '(a,i7,/)') '          .. No. of removed nodes : ',  nMove

  IF(nMove/=0)THEN
     !--- recount nodes, edges, and triangles
     CALL Clean_Collapsed(Tri3D, Mark)
  ENDIF

  DEALLOCATE(Mark)

  RETURN
END SUBROUTINE Surf_AngleCollapse

!>
!!   Collapse tiny edges.
!<
SUBROUTINE Surf_EdgeCollapse(RegionID, Tri3D)
  USE Spacing_Parameters
  USE surface_Parameters, ONLY : SurfTri
  USE SurfaceMeshManager
  USE LinkAssociation
  USE HeapTree
  IMPLICIT NONE
  INTEGER, INTENT(IN)                :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D

  INTEGER :: ip1, ip2, ip3, IS, IT, itt, nmove, IE
  INTEGER :: icheck, idirt, itmp, ipL, ipR, itL, itR, j1L, j1R, j2L, j2R
  INTEGER ::  n, i, iss
  REAL*8  :: p2(3), p3(3), pnew(3), ratio, qual, cr(2)
  REAL*8  :: RAsq, SizeCollapse, SizeKeep, aBig
  INTEGER, DIMENSION(:), POINTER :: Mark
  TYPE(HeapTreeType) :: Heap
  INTEGER, DIMENSION(:), POINTER :: isList
  INTEGER   ::  nList


  WRITE(*, '(/,a)') '  .... Edge Collapse'
  WRITE(29,'(/,a)') '  .... Edge Collapse'

  SizeCollapse = (0.25d0 * BGSpacing%BasicSize)**2
  SizeKeep     = 9 * SizeCollapse
  aBig         = 1.d30 * SizeKeep

  !--- build normal of triangles
  
  IF(ASSOCIATED(SurfTri))THEN
     IF(SIZE(SurfTri)<Tri3D%NB_Tri) DEALLOCATE(SurfTri)
  ENDIF
  IF(.NOT. ASSOCIATED(SurfTri))  ALLOCATE(SurfTri(2*Tri3D%NB_Tri))  
  DO IE = 1, Tri3D%NB_Tri
     CALL SurfTri_CalGeom(Tri3D, SurfTri, IE)
  ENDDO
  
  !--- Build point-edge & point-tri association

  CALL Surf_BuildTriAsso(Tri3D)
  CALL Surf_BuildEdgeAsso(Tri3D)

  ALLOCATE(Mark(Tri3D%NB_Point))
  Mark(1:Tri3D%NB_Point) = 0

  !--- set ascending heap-tree with length (the shortest edge takes the first)

  Heap%Ascending = .TRUE.
  CALL HeapTree_Allocate( Heap, Tri3D%NB_Edge )

  DO IS = 1, Tri3D%NB_Edge
     p2(:) = Tri3D%Posit(:,Tri3D%IP_Edge(1,IS))
     p3(:) = Tri3D%Posit(:,Tri3D%IP_Edge(2,IS))
     CALL GetEgdeLength_SQ(P2,P3, RAsq)

     IF(RAsq>=SizeKeep) RAsq = aBig
     CALL HeapTree_AddValue( Heap, RAsq )
  ENDDO

  !--- collapse the shortest edge

  nMove = 0
  Loop_IS : DO 

     IS   = Heap%toList(1)         !--- the shortest edge
     RAsq = Heap%v(IS)
     IF( RAsq >= SizeCollapse ) EXIT

     !--- check the quality if move
     CALL movedirect_topo(Tri3D, IS, idirt)

     ip1 = Tri3D%IP_Edge(1,IS)
     ip2 = Tri3D%IP_Edge(2,IS)
     IF(idirt==3)THEN
        !--- Find the middle position of the edge
        icheck = 4
        cr(:) = (Tri3D%Coord(:,ip1) + Tri3D%Coord(:,ip2))/2.d0
        CALL GetUVPointInfo0(RegionID,cr(1),cr(2),pnew)
     ELSE
        icheck = idirt
        pnew   = 0
     ENDIF

     IF(idirt>0)THEN
        CALL movedirect_Qual(Tri3D, IS, icheck, idirt, pnew, qual)
     ENDIF

     IF(idirt==0)THEN
        !--- can not collapse
        RAsq = aBig
        CALL HeapTree_ResetNodeValue( Heap, IS, RAsq )
        CYCLE
     ENDIF

     nMove = nMove + 1

     !--- move ip1 to ip2
     IF(Mark(ip1)>0 .OR. Mark(ip2)>0) CALL Error_Stop ('Surf_EdgeCollapse ::  marked ip1 ip2 ')
     CALL Surf_GetEdgeSurround(Tri3D, IS,itL,itR,ipL,ipR,j2L,j1L,j2R,j1R)

     IF(idirt==1)THEN     
        ip1 = Tri3D%IP_Edge(2,IS)
        ip2 = Tri3D%IP_Edge(1,IS)
        itmp = j1L
        j1L  = j2L
        j2L  = itmp
        itmp = j1R
        j1R  = j2R
        j2R  = itmp
     ENDIF

     Mark(ip1) = ip2           !--- move ip1 to ip2

     IF(idirt==3)THEN
        !---- middle position apply
        Tri3D%Posit(:,ip2) = pnew(:)
        Tri3D%Coord(:,ip2) = cr(:)
     ENDIF


     !--- update ip_tri

     CALL LinkAssociation_Remove(3, itR, Tri3D%IP_tri(1:3,itR), Tri3D%ITR_Pt)
     CALL LinkAssociation_Remove(3, itL, Tri3D%IP_tri(1:3,itL), Tri3D%ITR_Pt)
     Tri3D%IP_tri(1,itR) = 0
     Tri3D%IP_tri(1,itL) = 0

     CALL LinkAssociation_List(ip1, nList, isList, Tri3D%ITR_Pt)
     DO n = 1, nList
        IT = isList(n)
        WHERE(Tri3D%IP_Tri(1:3,IT)==ip1) Tri3D%IP_Tri(1:3,IT) = ip2
        CALL IntQueue_Push36(Tri3D%ITR_Pt%JointLinks(ip2), IT)
     ENDDO


     !--- update edge

     CALL HeapTree_ResetNodeValue( Heap, IS,  aBig )
     CALL HeapTree_ResetNodeValue( Heap, j1L, aBig )
     CALL HeapTree_ResetNodeValue( Heap, j1R, aBig )

     CALL LinkAssociation_Remove(2, IS,  Tri3D%IP_Edge(:,IS),  Tri3D%IED_Pt)
     CALL LinkAssociation_Remove(2, j1L, Tri3D%IP_Edge(:,j1L), Tri3D%IED_Pt)
     CALL LinkAssociation_Remove(2, j1R, Tri3D%IP_Edge(:,j1R), Tri3D%IED_Pt)

     Tri3D%IP_Edge(1,IS)  = 0
     Tri3D%IP_Edge(1,j1L) = 0
     Tri3D%IP_Edge(1,j1R) = 0

     !---- update IED_Tri
     IF(Tri3D%ITR_Edge(1,j1L)==itL)THEN
        itt = Tri3D%ITR_Edge(2,j1L)
     ELSE
        itt = Tri3D%ITR_Edge(1,j1L)
     ENDIF
     WHERE(Tri3D%ITR_Edge(:,j2L)==itL) Tri3D%ITR_Edge(:,j2L) = itt
     WHERE(Tri3D%IED_Tri(:, itt)==j1L) Tri3D%IED_Tri(:, itt) = j2L

     IF(Tri3D%ITR_Edge(1,j1R)==itR)THEN
        itt = Tri3D%ITR_Edge(2,j1R)
     ELSE
        itt = Tri3D%ITR_Edge(1,j1R)
     ENDIF
     WHERE(Tri3D%ITR_Edge(:,j2R)==itR) Tri3D%ITR_Edge(:,j2R) = itt
     WHERE(Tri3D%IED_Tri(:, itt)==j1R) Tri3D%IED_Tri(:, itt) = j2R

     CALL LinkAssociation_List(ip1, nList, isList, Tri3D%IED_Pt)
     DO n = 1, nList
        iss = isList(n)
        ip3 = 0
        DO i=1,2
           IF(Tri3D%IP_Edge(i,iss)==ip1)THEN
              Tri3D%IP_Edge(i,iss) = ip2
           ELSE
              ip3 = Tri3D%IP_Edge(i,iss)
           ENDIF
        ENDDO

        IF(ip3==0 .OR. ip3==ip2) CALL Error_Stop ('Surf_EdgeCollapse :: ip3==0 .OR. ip3==ip2 ')

        CALL IntQueue_Push36(Tri3D%IED_Pt%JointLinks(ip2),iss) 
     ENDDO

     !--- update edge length

     CALL LinkAssociation_List(ip2, nList, isList, Tri3D%IED_Pt)
     DO n = 1, nList
        iss = isList(n)
        IF( Heap%v(iss) >= SizeKeep ) CYCLE

        p2(:) = Tri3D%Posit(:,Tri3D%IP_Edge(1,iss))
        p3(:) = Tri3D%Posit(:,Tri3D%IP_Edge(2,iss))
        CALL GetEgdeLength_SQ(p2,p3, RAsq)
        IF(RAsq>=SizeKeep) RAsq = aBig
        CALL HeapTree_ResetNodeValue( Heap, iss, RAsq )
     ENDDO

  ENDDO Loop_IS

  CALL HeapTree_Clear(Heap)
  CALL LinkAssociation_Clear(Tri3D%ITR_Pt)
  CALL LinkAssociation_Clear(Tri3D%IED_Pt)

  WRITE(*,  '(a,i7,/)') '          .. No. of removed nodes : ',  nMove
  WRITE(29, '(a,i7,/)') '          .. No. of removed nodes : ',  nMove

  IF(nMove/=0)THEN
     !--- recount nodes, edges, and triangles
     CALL Clean_Collapsed(Tri3D, Mark)
  ENDIF

  DEALLOCATE(Mark)


  RETURN
END SUBROUTINE Surf_EdgeCollapse

!>
!!   Clean those deleted nodes, triangles and edges.
!!    @param[in,out] Tri3D : the surface mesh being cleaned. 
!!    @param[in]     Mark  (IP)=0    IP IS kept (but may be renumbered).            \n
!!                         (IP)=iq   IP IS the identical node with iq (the original id).
!!
!!     IF Tri3D%IP_Tri(1,IT)=0  then IT IS a deleted triangle.
!<
SUBROUTINE Clean_Collapsed(Tri3D, Mark)
  USE SurfaceMeshStorage
  IMPLICIT NONE

  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  INTEGER, INTENT(INOUT) :: Mark(*)
  INTEGER :: ip1, ip2, ip3, IS, IT, nenew, npnew, nsnew, isp
  INTEGER :: ips(3), its(2), itt
  INTEGER ::  n, i

  !--- simplify the chain

  DO ip1 = 1, Tri3D%NB_Point
     ip2 = Mark(ip1)
     IF(ip2==0) CYCLE
     DO
        ip3 = Mark(ip2)
        IF(ip3==0) EXIT
        ip2 = ip3
     ENDDO
     Mark(ip1) = ip2
  ENDDO

  !--- recount nodes

  npnew = 0
  DO ip1 = 1, Tri3D%NB_Point
     IF(Mark(ip1)==0)THEN
        npnew     = npnew+1
        Mark(ip1) = npnew
        Tri3D%Coord(:,npnew) = Tri3D%Coord(:,ip1)
        Tri3D%Posit(:,npnew) = Tri3D%Posit(:,ip1)
     ELSE
        Mark(ip1) = -Mark(ip1)
     ENDIF
  ENDDO
  DO ip1 = 1, Tri3D%NB_Point
     IF(Mark(ip1)<0)THEN
        ip2       = -Mark(ip1)
        Mark(ip1) = -Mark(ip2)
        IF(Mark(ip1)>=0 .OR. Mark(ip1)<-npnew)THEN
           CALL Error_Stop (' Ridge_EdgeCollapse :: wrong move target ')
        ENDIF
     ENDIF
  ENDDO

  !--- recount triangles

  nenew=0
  DO IT=1,Tri3D%NB_Tri
     IF(Tri3D%IP_Tri(1,IT)==0) CYCLE
     ips(1:3) = ABS(Mark(Tri3D%IP_Tri(1:3,IT)))
     IF(ips(1)==ips(2) .OR. ips(1)==ips(3) .OR. ips(2)==ips(3))THEN
        CALL Error_Stop (' Ridge_EdgeCollapse :: wrong triangle  ')
     ENDIF

     nenew = nenew+1
     Tri3D%IP_Tri(1:3,nenew) = ips(1:3)
     Tri3D%IP_Tri(4:5,nenew) = Tri3D%IP_Tri(4:5,IT)
  ENDDO


  Tri3D%NB_Point = npnew
  Tri3D%NB_Tri   = nenew
  Tri3D%NB_Edge  = 0


  RETURN
END SUBROUTINE Clean_Collapsed


!>
!!  @param[in]  Tri3D : the surface mesh.
!!  @param[in]  IS    : the checked edge.
!!  @param[out] idirt =0    no move allowed.    \n
!!                    =1    only Tri3D%IP_Edge(2,IS) can move.    \n
!!                    =2    only Tri3D%IP_Edge(1,IS) can move.    \n
!!                    =3    both can move.
!<
SUBROUTINE movedirect_Topo(Tri3D, IS, idirt)
  USE SurfaceMeshStorage
  IMPLICIT NONE

  TYPE(SurfaceMeshStorageType), INTENT(IN) :: Tri3D
  INTEGER, INTENT(in)    :: IS
  INTEGER, INTENT(out)   :: idirt
  INTEGER :: imove, ip1,ip2,  ioverlap
  INTEGER, DIMENSION(:), POINTER :: isList
  INTEGER   ::  nList, n, i, IT

  idirt = 0

  ip1 = Tri3D%IP_Edge(1,IS)
  ip2 = Tri3D%IP_Edge(2,IS)

  !--- imove =1 only ip2 can be moved
  !          =2 only ip1 can be moved
  !          =3 both can be moved

  imove = 0
  IF(ip1>Tri3D%NB_BD_Point)THEN
     IF(ip2>Tri3D%NB_BD_Point)THEN
        imove = 3
     ELSE
        imove = 2
     ENDIF
  ELSE
     IF(ip2<=Tri3D%NB_BD_Point) RETURN
     imove = 1
  ENDIF
  IF(imove==0) CALL Error_Stop ('movedirect_Topo :: miss something ')


  !--- check if collapsing resulting in all-boundary-node triangles
  IF(imove==1 .OR. imove==2)THEN
     IF(imove==1)THEN
        ip1 = Tri3D%IP_Edge(2,IS)
        ip2 = Tri3D%IP_Edge(1,IS)
     ENDIF
     CALL LinkAssociation_List(ip1, nList, isList, Tri3D%ITR_Pt)
     DO n = 1, nList
        IT = isList(n)
        DO i = 1,3
           IF(Tri3D%IP_Tri(i,IT)==ip1) CYCLE
           IF(Tri3D%IP_Tri(i,IT)==ip2) EXIT
           IF(Tri3D%IP_Tri(i,IT)>Tri3D%NB_BD_Point) EXIT
        ENDDO
        IF(i>3) RETURN          !---- willbe all-boundary-node triangle found
     ENDDO
  ENDIF


  !--- check if there exsiting an overlap edge
  CALL overlap(Tri3D, IS, ioverlap)
  IF(ioverlap==1) RETURN

  idirt = imove

END SUBROUTINE movedirect_Topo


!>
!!  @param[in]  Tri3D : the surface mesh.
!!  @param[in]  IS    : the checked edge.
!!  @param[in]  icheck  =1   check Tri3D%IP_Edge(2,IS) to Tri3D%IP_Edge(1,IS).    \n
!!                      =2   check Tri3D%IP_Edge(1,IS) to Tri3D%IP_Edge(2,IS).    \n
!!                      =3   check above both.                                  \n
!!                      =4   check above both and
!!                           Tri3D%IP_Edge(1,IS) & Tri3D%IP_Edge(2,IS) to centre.  
!!  @param[in]  pnew  : the cnetral position of the checked edge. Only used if icheck=4
!!  @param[out] idirt =0    no move allowed.    \n
!!                    =1    move Tri3D%IP_Edge(2,IS) to Tri3D%IP_Edge(1,IS).    \n
!!                    =2    move Tri3D%IP_Edge(1,IS) to Tri3D%IP_Edge(2,IS).    \n
!!                    =3    move Tri3D%IP_Edge(1,IS) and Tri3D%IP_Edge(2,IS) to pnew.
!!  @param[out] qual  : the quality of involved triangles if move the point.
!<
SUBROUTINE movedirect_qual(Tri3D, IS, icheck, idirt, pnew, qual)
  USE SurfaceMeshStorage
  IMPLICIT NONE

  TYPE(SurfaceMeshStorageType), INTENT(IN) :: Tri3D
  INTEGER, INTENT(in)    :: IS, icheck
  INTEGER, INTENT(out)   :: idirt
  REAL*8,  INTENT(IN)    :: pnew(3)
  REAL*8,  INTENT(OUT)   :: qual
  INTEGER :: ip1,ip2, itL,itR, negat(4), IP,  k, j
  REAL*8  :: pto(3), qut(4)

  !--- check and compare the mesh quality after moving, 
  !    and choose the best.

  ip1 = Tri3D%IP_Edge(1,IS)
  ip2 = Tri3D%IP_Edge(2,IS)
  itR = Tri3D%ITR_Edge(1,IS)
  itL = Tri3D%ITR_Edge(2,IS)

  negat(1:4) = 1
  qut(1:4)   = -1.
  DO k = 1,4

     IF(k==1 .AND. icheck==2) CYCLE
     IF(k==2 .AND. icheck==1) CYCLE
     IF(k>=3 .AND. icheck/=4) EXIT

     IF(k==1 .OR. k==3)THEN
        IP = ip2
     ELSE
        IP = ip1
     ENDIF
     IF(k==1)THEN
        pto(:) = Tri3D%Posit(:,ip1)
     ELSE IF(k==2)THEN
        pto(:) = Tri3D%Posit(:,ip2)
     ELSE
        pto(:) = pnew(:)
     ENDIF

     CALL areachecknew(Tri3D, IP, pto, itL,itR, negat(k),qut(k))

     IF(k==3 .AND. negat(3)==1) EXIT
  ENDDO

  negat(3) = MAX(negat(3), negat(4))
  qut(3)   = MIN(qut(3),   qut(4))

  idirt = 0
  qual  = 0.
  DO k = 1,3
     IF(negat(k)==0 .AND. qut(k)>qual)THEN
        idirt = k
        qual    = qut(k)
     ENDIF
  ENDDO

  RETURN
END SUBROUTINE movedirect_qual


!>
!!   Collapse tiny edges for super-patches.
!<
SUBROUTINE SuperPatch_EdgeCollapse( )
  USE control_Parameters
  USE Spacing_Parameters
  USE surface_Parameters
  USE SurfaceMeshManager
  USE LinkAssociation
  USE HeapTree
  IMPLICIT NONE

  INTEGER :: ip1, ip2, ip3, IS, IT, itt, nmove
  INTEGER :: icheck, idirt, itmp, ipL, ipR, itL, itR, j1L, j1R, j2L, j2R
  INTEGER :: ipartR, ipartL, ipart
  INTEGER ::  n, i, iss, ITinBase
  REAL*8  :: p1(3), p2(3), ratio, qual, qualold
  REAL*8  :: RAsq, SizeCollapse, SizeKeep, aBig
  INTEGER, DIMENSION(:), POINTER :: Mark
  TYPE(HeapTreeType) :: Heap
  INTEGER, DIMENSION(:), POINTER :: isList
  INTEGER   ::  nList


  WRITE(*, '(/,a)') '  .... Edge Collapse for super-patches'
  WRITE(29,'(/,a)') '  .... Edge Collapse for super-patches'

  SizeCollapse = (0.25d0 * BGSpacing%BasicSize)**2
  SizeKeep     = 9 * SizeCollapse
  aBig         = 1.d30 * SizeKeep


  !--- Build point-edge & point-tri association
  CALL Surf_BuildTriAsso(Surf)
  CALL Surf_BuildEdgeAsso(Surf)

  ALLOCATE(Mark(Surf%NB_Point))
  Mark(1:Surf%NB_Point) = 0

  !--- set ascending heap-tree with length (the shortest edge takes the first)

  Heap%Ascending = .TRUE.
  CALL HeapTree_Allocate( Heap, Surf%NB_Edge )

  DO IS = 1, Surf%NB_Edge
     p1(:) = Surf%Posit(:,Surf%IP_Edge(1,IS))
     p2(:) = Surf%Posit(:,Surf%IP_Edge(2,IS))
     CALL GetEgdeLength_SQ(p1,p2, RAsq)

     IF(RAsq>=SizeKeep) RAsq = aBig
     CALL HeapTree_AddValue( Heap, RAsq )
  ENDDO

  !--- collapse the shortest edge

  nMove = 0
  Loop_IS : DO 

     IS   = Heap%toList(1)         !--- the shortest edge
     RAsq = Heap%v(IS)
     IF( RAsq >= SizeCollapse ) EXIT

     !--- check the quality if move
     CALL SuperPatch_movedirect_topo(IS, idirt)

     IF(idirt==3 .AND. EdgeSuperID(IS)/=0) CALL Error_Stop (' SuperPatch_EdgeCollapse :: ridge curve  ') 
     icheck = idirt

     IF(icheck>0)THEN
        CALL SuperPatch_movedirect_Qual(IS, icheck, idirt, qual)
     ENDIF

     qualold = 1.d30
     IF(idirt>0)THEN
        ip1 = Surf%IP_Edge(3-idirt,IS)
        CALL LinkAssociation_List(ip1, nList, isList, Surf%ITR_Pt)
        DO n = 1, nList
           IT = isList(n)
           qualold = MIN(qualold, SurfTri(IT)%Area)
        ENDDO
        IF(qualold>= 0.9* qual) idirt = 0
     ENDIF

     IF(idirt==0)THEN
        !--- can not collapse
        RAsq = aBig
        CALL HeapTree_ResetNodeValue( Heap, IS, RAsq )
        CYCLE
     ENDIF

     nMove = nMove + 1

     !--- move ip1 to ip2

     ip1 = Surf%IP_Edge(1,IS)
     ip2 = Surf%IP_Edge(2,IS)
     IF(Mark(ip1)>0 .OR. Mark(ip2)>0) CALL Error_Stop (' SuperPatch_EdgeCollapse :: marked ip1 ip2 ')
     CALL Surf_GetEdgeSurround(Surf, IS,itL,itR,ipL,ipR,j2L,j1L,j2R,j1R)

     IF(idirt==1)THEN     
        ip1 = Surf%IP_Edge(2,IS)
        ip2 = Surf%IP_Edge(1,IS)
        itmp = j1L
        j1L  = j2L
        j2L  = itmp
        itmp = j1R
        j1R  = j2R
        j2R  = itmp
     ENDIF

     Mark(ip1) = ip2           !--- move ip1 to ip2

     IF(SuperCosmetic_Method==3)THEN
        IF(SurfNode(ip1)%onRidge>0)THEN
           CALL whichpart(ip1, itR, ipartR)
           CALL whichpart(ip1, itL, ipartL)
        ENDIF
     ENDIF

     !--- update ip_tri

     CALL LinkAssociation_Remove(3, itR, Surf%IP_tri(1:3,itR), Surf%ITR_Pt)
     CALL LinkAssociation_Remove(3, itL, Surf%IP_tri(1:3,itL), Surf%ITR_Pt)
     Surf%IP_tri(1,itR) = 0
     Surf%IP_tri(1,itL) = 0

     CALL LinkAssociation_List(ip1, nList, isList, Surf%ITR_Pt)
     DO n = 1, nList
        IT = isList(n)
        WHERE(Surf%IP_Tri(1:3,IT)==ip1) Surf%IP_Tri(1:3,IT) = ip2
        CALL SurfTri_CalGeom  (Surf, SurfTri, IT)
        CALL IntQueue_Push36(Surf%ITR_Pt%JointLinks(ip2), IT)
     ENDDO


     !--- update ridge node

     IF(SuperCosmetic_Method==3)THEN
        IF(SurfNode(ip2)%onRidge>0)THEN
           IF(SurfNode(ip1)%onRidge>0)THEN
              DO n = 1, nList
                 IT = isList(n)
                 CALL whichpart(ip1, IT, ipart)
                 IF(ipart == ipartR)THEN
                    CALL addpart(IP2, itR, IT)
                 ELSE IF(ipart == ipartL)THEN
                    CALL addpart(IP2, itL, IT)
                 ELSE
                    CALL Error_Stop (' SuperPatch_EdgeCollapse :: what part here  ') 
                 ENDIF
              ENDDO
           ELSE
              DO n = 1, nList
                 IT = isList(n)
                 CALL addpart(IP2, itR, IT)
              ENDDO
           ENDIF
           CALL RemovePart(IP2, itR)
           CALL RemovePart(IP2, itL)
        ENDIF

        CALL removepart(ipR,itR)
        CALL removepart(ipL,itL)
     ENDIF

     !--- update edge

     CALL HeapTree_ResetNodeValue( Heap, IS,  aBig )
     CALL HeapTree_ResetNodeValue( Heap, j1L, aBig )
     CALL HeapTree_ResetNodeValue( Heap, j1R, aBig )

     CALL LinkAssociation_Remove(2, IS,  Surf%IP_Edge(:,IS),  Surf%IED_Pt)
     CALL LinkAssociation_Remove(2, j1L, Surf%IP_Edge(:,j1L), Surf%IED_Pt)
     CALL LinkAssociation_Remove(2, j1R, Surf%IP_Edge(:,j1R), Surf%IED_Pt)

     Surf%IP_Edge(1,IS)  = 0
     Surf%IP_Edge(1,j1L) = 0
     Surf%IP_Edge(1,j1R) = 0

     !---- update IED_Tri
     IF(Surf%ITR_Edge(1,j1L)==itL)THEN
        itt = Surf%ITR_Edge(2,j1L)
     ELSE
        itt = Surf%ITR_Edge(1,j1L)
     ENDIF
     WHERE(Surf%ITR_Edge(:,j2L)==itL) Surf%ITR_Edge(:,j2L) = itt
     WHERE(Surf%IED_Tri(:, itt)==j1L) Surf%IED_Tri(:, itt) = j2L

     IF(Surf%ITR_Edge(1,j1R)==itR)THEN
        itt = Surf%ITR_Edge(2,j1R)
     ELSE
        itt = Surf%ITR_Edge(1,j1R)
     ENDIF
     WHERE(Surf%ITR_Edge(:,j2R)==itR) Surf%ITR_Edge(:,j2R) = itt
     WHERE(Surf%IED_Tri(:, itt)==j1R) Surf%IED_Tri(:, itt) = j2R

     CALL LinkAssociation_List(ip1, nList, isList, Surf%IED_Pt)
     DO n = 1, nList
        iss = isList(n)
        ip3 = 0
        DO i=1,2
           IF(Surf%IP_Edge(i,iss)==ip1)THEN
              Surf%IP_Edge(i,iss) = ip2
           ELSE
              ip3 = Surf%IP_Edge(i,iss)
           ENDIF
        ENDDO

        IF(ip3==0 .OR. ip3==ip2) CALL Error_Stop ('SuperPatch_EdgeCollapse :: ip3==0 .OR. ip3==ip2  ') 

        CALL IntQueue_Push36(Surf%IED_Pt%JointLinks(ip2),iss) 
     ENDDO

     !--- update edge length

     CALL LinkAssociation_List(ip2, nList, isList, Surf%IED_Pt)
     DO n = 1, nList
        iss = isList(n)
        IF( Heap%v(iss) >= SizeKeep ) CYCLE

        p1(:) = Surf%Posit(:,Surf%IP_Edge(1,iss))
        p2(:) = Surf%Posit(:,Surf%IP_Edge(2,iss))
        CALL GetEgdeLength_SQ(p1,p2, RAsq)
        IF(RAsq>=SizeKeep) RAsq = aBig
        CALL HeapTree_ResetNodeValue( Heap, iss, RAsq )
     ENDDO


     IF(Debug_Display>2)THEN
        !--- check if exist identical edges        
        CALL LinkAssociation_List(ip2, nList, isList, Surf%IED_Pt)
        DO n = 1, nList
           j1L = isList(n)
           DO i = n+1, nList
              j1R = isList(i)
              IF(  Surf%IP_Edge(1,j1L)+Surf%IP_Edge(2,j1L) ==    &
                   Surf%IP_Edge(1,j1R)+Surf%IP_Edge(2,j1R)  )THEN
                 WRITE(29,*)' Error---- idential edge-pair for ip2=',ip2,ip1
                 WRITE(29,*)'    egde 1: ',j1L,Surf%IP_Edge(:,j1L)
                 WRITE(29,*)'    egde 2: ',j1R,Surf%IP_Edge(:,j1R)
                 CALL Error_Stop (' SuperPatch_EdgeCollapse ::idential edge-pair ')
              ENDIF
           ENDDO
        ENDDO
     ENDIF


  ENDDO Loop_IS

  CALL HeapTree_Clear(Heap)
  CALL LinkAssociation_Clear(Surf%ITR_Pt)
  CALL LinkAssociation_Clear(Surf%IED_Pt)

  WRITE(*,  '(a,i7,/)') '          .. No. of removed nodes : ',  nMove
  WRITE(29, '(a,i7,/)') '          .. No. of removed nodes : ',  nMove

  IF(nMove/=0)THEN
     !--- recount nodes, edges, and triangles
     CALL SuperPatch_Clean_Collapsed(Mark)
     IF(Debug_Display>1)THEN
        CALL Surf_CheckEdge(Surf,i)
        IF(i/=0) CALL Error_Stop (' SuperPatch_EdgeCollapse :: nsnew/=0 ')
     ENDIF
  ENDIF

  DEALLOCATE(Mark)


  RETURN
END SUBROUTINE SuperPatch_EdgeCollapse

!>
!!   Clean those deleted nodes, triangles and edges.
!!    @param[in] Mark  (IP)=0    IP IS kept (but may be renumbered).
!!                         =iq   IP IS the identical node with iq (the original id).
!!
!!     IF Surf%IP_Tri(1,IT)=0  then IT IS a deleted triangle.
!!     IF Surf%IP_Edge(1,IS)=0 then IS IS a deleted edge.
!<
SUBROUTINE SuperPatch_Clean_Collapsed(Mark)
  USE control_Parameters
  USE surface_Parameters
  IMPLICIT NONE

  INTEGER :: Mark(*)
  INTEGER :: ip1, ip2, ip3, IS, IT, nenew, npnew, nsnew, isp
  INTEGER :: ips(3), its(2), itt
  INTEGER ::  n, i
  INTEGER, DIMENSION(:), POINTER :: mark_t, mark_s
  TYPE(IntQueueType) :: list

  !--- simplify the chain
  DO ip1 = 1, Surf%NB_Point
     ip2 = Mark(ip1)
     IF(ip2==0) CYCLE
     DO
        ip3 = Mark(ip2)
        IF(ip3==0) EXIT
        ip2 = ip3
     ENDDO
     Mark(ip1) = ip2
  ENDDO

  !--- recount nodes
  npnew = 0
  DO ip1 = 1, Surf%NB_Point
     IF(Mark(ip1)==0)THEN
        npnew     = npnew+1
        Mark(ip1) = npnew
        IF(ASSOCIATED(Surf%Coord))THEN
           Surf%Coord(:,npnew) = Surf%Coord(:,ip1)
        ENDIF
        Surf%Posit(:,npnew) = Surf%Posit(:,ip1)
        IF(SuperCosmetic_Method<=2)THEN
           inBaseTri(   npNew) = inBaseTri(   ip1)
        ELSE
           IF(npnew/=ip1) CALL SurfaceNode_Copy(SurfNode(ip1), SurfNode(npnew) )
        ENDIF
     ELSE
        Mark(ip1) = -Mark(ip1)
     ENDIF
  ENDDO
  DO ip1 = 1, Surf%NB_Point
     IF(Mark(ip1)<0)THEN
        ip2       = -Mark(ip1)
        Mark(ip1) = -Mark(ip2)
        IF(Mark(ip1)>=0 .OR. Mark(ip1)<-npnew)THEN
           CALL Error_Stop (' SuperPatch_Clean_Collapsed :: wrong move target ')
        ENDIF
     ENDIF
  ENDDO


  !--- recount triangles
  ALLOCATE(mark_t(Surf%NB_Tri))
  mark_t(:) = 0

  nenew=0
  DO IT=1,Surf%NB_Tri
     IF(Surf%IP_Tri(1,IT)==0) CYCLE
     ips(1:3) = ABS(Mark(Surf%IP_Tri(1:3,IT)))
     IF(ips(1)==ips(2) .OR. ips(1)==ips(3) .OR. ips(2)==ips(3))THEN
        CALL Error_Stop (' SuperPatch_Clean_Collapsed:: wrong triangle  ')
     ENDIF

     nenew = nenew+1
     Surf%IP_Tri(1:3,nenew) = ips(1:3)
     Surf%IP_Tri(4:5,nenew) = Surf%IP_Tri(4:5,IT)
     SurfTri(nenew) = SurfTri(IT)
     mark_t(IT) = nenew
  ENDDO


  !--- recount edge
  ALLOCATE(mark_s(Surf%NB_Edge))
  mark_s(:) = 0

  nsnew=0
  DO IS=1,Surf%NB_Edge
     IF(Surf%IP_Edge(1,IS)==0) CYCLE
     ips(1:2) = ABS(Mark(Surf%IP_Edge(1:2,IS)))
     its(1:2) =    mark_t(Surf%ITR_Edge(:,IS))
     IF(ips(1)==ips(2) .OR. its(1)==0 .OR. its(2)==0)THEN
        CALL Error_Stop (' SuperPatch_Clean_Collapsed:: wrong Edge  ') 
     ENDIF

     nsnew = nsnew+1
     Surf%IP_Edge(:, nsnew) = ips(1:2)
     Surf%ITR_Edge(:,nsnew) = its(1:2)
     EdgeSuperID(nsnew) = EdgeSuperID(IS)
     mark_s(IS) = nsnew
  ENDDO

  DO IT=1,Surf%NB_Tri
     itt = mark_t(IT)
     IF(itt==0) CYCLE
     Surf%IED_Tri(:,itt) = mark_s(Surf%IED_Tri(:,IT))
  ENDDO


  Surf%NB_Point = npnew
  Surf%NB_Tri   = nenew
  Surf%NB_Edge  = nsnew

  IF(SuperCosmetic_Method==3)THEN
     !--- recount ridge nodes
     npnew = 0
     DO ip1 = 1, Surf%NB_Point
        IF(SurfNode(ip1)%onRidge<=0) CYCLE

        DO i = 1, SurfNode(ip1)%numParts
           CALL IntQueue_Copy(SurfNode(ip1)%PartTri(i), list)
           SurfNode(ip1)%PartTri(i)%numNodes = 0
           DO n = 1, list%numNodes
              itt = list%Nodes(n)
              itt = mark_t(itt)
              IF(itt>0)THEN
                 CALL IntQueue_Push(SurfNode(ip1)%PartTri(i), itt)
              ENDIF
           ENDDO
           IF(SurfNode(ip1)%PartTri(i)%numNodes==0)THEN
              WRITE(29,*)' Error--- whole part gone: ip1,rig=',ip1, SurfNode(ip1)%onRidge
              WRITE(29,*)'    i, np=',i,SurfNode(ip1)%numParts
              WRITE(29,*)'   Bug. It likely due to the collapse of a 3-edge supper curve.'
              CALL Error_Stop ('SuperPatch_Clean_Collapsed:: whole part gone  ')
           ENDIF
        ENDDO
     ENDDO
  ENDIF

  !--- recount ridge nodes on each super-curve
  DO isp = 1, NB_SuperCurve
     npnew = 0
     DO i = 1, SuperCurveNodes(isp)%numNodes
        ip1 = Mark(SuperCurveNodes(isp)%Nodes(i))
        IF(ip1>0)THEN
           npnew = npnew+1
           SuperCurveNodes(isp)%Nodes(npnew) = ip1
           SuperCurveNodePt(isp)%V(npnew)    = SuperCurveNodePt(isp)%V(i)
        ELSE IF(i==1 .OR. i==SuperCurveNodes(isp)%numNodes)THEN
           WRITE(29,*)' Error---the end of super-curve gone : ip=',SuperCurveNodes(isp)%Nodes(i)
           CALL Error_Stop (' SuperPatch_Clean_Collapsed :: ')
        ENDIF
     ENDDO
     SuperCurveNodes(isp)%numNodes  = npnew
     SuperCurveNodes(isp)%back      => SuperCurveNodes(isp)%Nodes(npnew)   
     SuperCurveNodePt(isp)%numNodes = npnew
  ENDDO

  DEALLOCATE(mark_t, mark_s)

  RETURN
END SUBROUTINE SuperPatch_Clean_Collapsed


!>
!!  @param[in]  IS    :  the checked edge.
!!  @param[out] idirt =0    no move allowed.    \n
!!                    =1    only Surf%IP_Edge(2,IS) can move.    \n
!!                    =2    only Surf%IP_Edge(1,IS) can move.    \n
!!                    =3    both can move.
!<
SUBROUTINE SuperPatch_movedirect_Topo(IS, idirt)
  USE control_Parameters
  USE surface_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(in)    :: IS
  INTEGER, INTENT(out)   :: idirt
  INTEGER :: imove, ip1,ip2,  jridge, ipt1, ipt2, ioverlap

  idirt = 0

  ip1 = Surf%IP_Edge(1,IS)
  ip2 = Surf%IP_Edge(2,IS)

  !--- check the topology to see which node can be moved

  jridge = EdgeSuperID(IS)
  IF(SuperCosmetic_Method<=2)THEN
     ipt1   = inBaseTri(ip1)
     ipt2   = inBaseTri(ip2)
  ELSE
     IF(.NOT. SurfNode(ip1)%movable)THEN
        !--- a corner node, or a end of ridge; don't move
        ipt1 = 0
     ELSE IF(SurfNode(ip1)%onRidge>0)THEN
        !--- a ridge node
        ipt1 = -1
     ELSE
        !--- a normal node
        ipt1 = 1
     ENDIF
     IF(.NOT. SurfNode(ip2)%movable)THEN
        !--- a corner node, or a end of ridge; don't move
        ipt2 = 0
     ELSE IF(SurfNode(ip2)%onRidge>0)THEN
        !--- a ridge node
        ipt2 = -1
     ELSE
        !--- a normal node
        ipt2 = 1
     ENDIF
  ENDIF

  !--- imove =1 only ip2 can be moved
  !          =2 only ip1 can be moved
  !          =3 both can be moved

  imove = 0
  IF(ipt1>0)THEN
     IF(ipt2>0)THEN
        imove = 3
     ELSE
        imove = 2
     ENDIF
  ELSE IF(ipt1<0)THEN
     IF(ipt2>0)THEN
        imove = 1
     ELSE IF(ipt2<0)THEN
        IF(jridge==0) RETURN
        imove = 2
     ELSE IF(ipt2==0)THEN
        IF(jridge==0) RETURN
        imove = 2
     ENDIF
  ELSE IF(ipt1==0)THEN
     IF(ipt2==0)                RETURN
     IF(ipt2<0 .AND. jridge==0) RETURN
     imove = 1
  ENDIF
  IF(imove==0) CALL Error_Stop (' SuperPatch_movedirect_Topo :: miss something ')

  !--- check if there exsiting an overlap edge
  CALL overlap(Surf, IS, ioverlap)
  IF(ioverlap==1) RETURN

  idirt = imove

END SUBROUTINE SuperPatch_movedirect_Topo


!>
!!  @param[in]  IS    : the checked edge.
!!  @param[in]  icheck  =1   check Surf%IP_Edge(2,IS) to Surf%IP_Edge(1,IS).    \n
!!                      =2   check Surf%IP_Edge(1,IS) to Surf%IP_Edge(2,IS).    \n
!!                      =3   check above both.                                  \n
!!                      =4   check above both and
!!                           Surf%IP_Edge(1,IS) & Surf%IP_Edge(2,IS) to centre.  
!!  @param[out] idirt =0    no move allowed.    \n
!!                    =1    move Surf%IP_Edge(2,IS) to Surf%IP_Edge(1,IS).    \n
!!                    =2    move Surf%IP_Edge(1,IS) to Surf%IP_Edge(2,IS).    \n
!!  @param[out] qual  : the quality of involved triangles if move the point.
!<
SUBROUTINE SuperPatch_movedirect_qual(IS, icheck, idirt, qual)
  USE surface_Parameters
  IMPLICIT NONE

  INTEGER, INTENT(in)    :: IS, icheck
  INTEGER, INTENT(out)   :: idirt
  REAL*8,  INTENT(OUT)   :: qual
  INTEGER :: ip1,ip2, itL,itR, negat, IP,  k, j
  REAL*8  :: pto(3), qut

  !--- check and compare the mesh quality after moving, 
  !    and choose the best.

  ip1 = Surf%IP_Edge(1,IS)
  ip2 = Surf%IP_Edge(2,IS)
  itR = Surf%ITR_Edge(1,IS)
  itL = Surf%ITR_Edge(2,IS)

  idirt = 0
  qual  = 0.
  DO k = 1,2

     IF(k==1 .AND. icheck==2) CYCLE
     IF(k==2 .AND. icheck==1) CYCLE

     IF(k==1)THEN
        IP = ip2
        pto(:) = Surf%Posit(:,ip1)
     ELSE
        IP = ip1
        pto(:) = Surf%Posit(:,ip2)
     ENDIF

     negat = 1
     CALL SuperPatch_areacheck(IP, pto, itL,itR, negat,qut)
     IF(negat==0 .AND. qut>qual)THEN
        idirt = k
        qual  = qut
     ENDIF

  ENDDO

  RETURN
END SUBROUTINE SuperPatch_movedirect_qual


!>
!!  Check if existing an edge-pair which will overlap after collapsing.
!!  @param[in]  Surf     :  the triangular mesh.
!!  @param[in]  IS       :  the edge to be collapsed.
!!  @param[out] ioverlap =0  no such edge-pair existing.
!!                       =1  there exists such edge-pair, which will overlap after collapsing.
!<
SUBROUTINE overlap(Surf, IS, ioverlap)
  USE SurfaceMeshStorage
  USE CellConnectivity
  IMPLICIT NONE

  TYPE(SurfaceMeshStorageType), INTENT(IN)  :: Surf 
  INTEGER, INTENT(IN)  :: IS
  INTEGER, INTENT(OUT) :: ioverlap
  INTEGER :: ip1, ip2, ip3, itR, itL, ipR, ipL, i, j, k, Ied

  ioverlap = 1

  ip1 = Surf%IP_Edge(1,IS)
  ip2 = Surf%IP_Edge(2,IS)
  itR = Surf%ITR_Edge(1,IS)
  itL = Surf%ITR_Edge(2,IS)
  k   = which_NodeinTri(IS, Surf%IED_Tri(1:3,itR))
  ipR = Surf%IP_Tri(k,itR)
  k   = which_NodeinTri(IS, Surf%IED_Tri(1:3,itL))
  ipL = Surf%IP_Tri(k,itL)

  DO i = 1, Surf%IED_Pt%JointLinks(ip1)%numNodes
     Ied =  Surf%IED_Pt%JointLinks(ip1)%Nodes(i)
     ip3 = Surf%IP_Edge(1,Ied)
     IF(ip3==ip1) ip3 = Surf%IP_Edge(2,Ied)
     IF(ip3==ip2) CYCLE
     IF(ip3==ipL) CYCLE
     IF(ip3==ipR) CYCLE

     DO j = 1, Surf%IED_Pt%JointLinks(ip2)%numNodes
        Ied =  Surf%IED_Pt%JointLinks(ip2)%Nodes(j)
        IF(Surf%IP_Edge(1,Ied)==ip3)  RETURN
        IF(Surf%IP_Edge(2,Ied)==ip3)  RETURN
     ENDDO
  ENDDO

  ioverlap = 0

  RETURN
END SUBROUTINE overlap

!>
!!   Collapse tiny dihedral angle for super-freqFilter.
!<
SUBROUTINE SuperFrqFlt_EdgeCollapse( )
  USE control_Parameters
  USE Spacing_Parameters
  USE surface_Parameters
  USE SurfaceMeshManager
  IMPLICIT NONE

  INTEGER :: ip1, ip2, ip3, IS, IT, nmove, icheck, idirt, iMove, iLoop
  INTEGER :: itL, itR, j1L, j1R, j2L, j2R, ipL, ipR, jL, jR, JsL, JsR
  INTEGER :: n, i, iss, i1, i2, j, j1, j2, negat, k, ied, it2
  REAL*8  :: qual, qualold, qut, cosa, cosang
  REAL*8  :: RAsq, SizeCollapse
  INTEGER, DIMENSION(:), POINTER :: Mark
  INTEGER, DIMENSION(:), POINTER :: isList
  INTEGER   ::  nList
  REAL*8, PARAMETER  :: PI = 3.141592653589793D0
  TYPE(IntQueueType) :: edges

  WRITE(*, '(/,a)') '  .... dihedral Collapse for super-freqFilter'
  WRITE(29,'(/,a)') '  .... dihedral Collapse for super-freqFilter'

  SizeCollapse = (0.25d0 * BGSpacing%BasicSize)**2
  cosang = -COS(PI*12/180.0)

  !--- Build point-edge & point-tri association
  CALL Surf_BuildTriAsso(Surf)
  CALL Surf_BuildEdgeAsso(Surf)

  ALLOCATE(Mark(Surf%NB_Point))
  Mark(1:Surf%NB_Point) = 0

  nMove  = 0

  DO iLoop = 1, 10

     iMove = 0

     Loop_IS : DO IS = 1, Surf%NB_Edge
        IF(Surf%IP_Edge(1,IS)==0) CYCLE

        CALL Surf_GetEdgeSurround(Surf, IS,itL,itR,ipL,ipR,j2L,j1L,j2R,j1R)

        !--- if ipR-ipL is an existing edge, skip
        CALL LinkAssociation_List(ipL, nList, isList, Surf%IED_Pt)
        DO n = 1, nList
           iss = isList(n)
           IF(Surf%IP_Edge(1,iss)==ipR) CYCLE Loop_IS
           IF(Surf%IP_Edge(2,iss)==ipR) CYCLE Loop_IS
        ENDDO

        !--- if ipR-ipL has broken divider, skip
        CALL LinkAssociation_List(ipL, nList, isList, Surf%ITR_Pt)
        edges%numNodes = 0
        DO n = 1, nList
           it  = isList(n)
           k   = Which_NodeinTri(ipL, Surf%IP_Tri(1:3,it))
           iss = Surf%IED_Tri(k,it)
           it2 = Surf%ITR_Edge(1,iss)
           IF(it2==it) it2= Surf%ITR_Edge(2,iss)
           IF(it2>0)THEN
              IF(Which_NodeinTri(ipR, Surf%IP_Tri(1:3,it2))>0) CALL IntQueue_Push(edges,iss)
           ENDIF
        ENDDO

        CALL IntQueue_Remove (edges, IS)
        DO i = 1,2
           ip1 = Surf%IP_Edge(i,IS)
           n = 0
           DO WHILE(n<edges%numNodes)
              n = n+1
              iss = edges%nodes(n)
              DO k = 1,2
                 IF(Surf%IP_Edge(k,iss)==ip1)THEN
                    ip1 = Surf%IP_Edge(3-k,iss)
                    n = 0
                    CALL IntQueue_Remove(edges,iss)
                    EXIT
                 ENDIF
              ENDDO
           ENDDO
        ENDDO
        IF(edges%numNodes>0) CYCLE       

        !--- check diheadral angle

        cosa = Geo3D_Dot_Product(SurfTri(itR)%anor, SurfTri(itL)%anor)
        IF(cosa>cosang) CYCLE

        CALL GetEgdeLength_SQ(Surf%Posit(:,ipR),Surf%Posit(:,ipL), RAsq)
        IF(RAsq>=SizeCollapse) CYCLE

        idirt = 0
        qual  = 0.
        DO k = 1,2
           negat = 1
           IF(k==1)THEN
              CALL SuperFrqFlt_areacheck(IPL, ipR, negat,qut)
           ELSE
              CALL SuperFrqFlt_areacheck(IPR, ipL, negat,qut)
           ENDIF
           IF(negat==0 .AND. qut>qual)THEN
              idirt = k
              qual  = qut
           ENDIF
        ENDDO

        !--- if idirt=1, move ipL to ipR
        !--- if idirt=2, move ipR to ipL

        IF(idirt==1)THEN  
           CALL LinkAssociation_List(IPL, nList, isList, Surf%ITR_Pt)
        ELSE
           CALL LinkAssociation_List(IPR, nList, isList, Surf%ITR_Pt)
        ENDIF
        qualold = 1.d30
        DO n = 1, nList
           IT = isList(n)
           qualold = MIN(qualold, SurfTri(IT)%Area)
        ENDDO
        IF(qualold>= 0.5* qual)  CYCLE

        iMove = iMove + 1

        IF(idirt==1)THEN  
           Mark(ipL) = ipR           !--- move ipL to ipR
        ELSE
           Mark(ipR) = ipL           !--- move ipR to ipL
        ENDIF

        !--- update ip_tri & edge

        CALL LinkAssociation_Remove(3, itL, Surf%IP_tri(1:3,itL), Surf%ITR_Pt)
        CALL LinkAssociation_Remove(3, itR, Surf%IP_tri(1:3,itR), Surf%ITR_Pt)
        Surf%IP_tri(1,itL) = 0
        Surf%IP_tri(1,itR) = 0
        CALL LinkAssociation_Remove(2, IS,  Surf%IP_Edge(:,IS),  Surf%IED_Pt)
        Surf%IP_Edge(1,IS)  = 0

        DO j = 1,2
           IF(j==1)THEN
              jL = j1L
              jR = j1R
           ELSE
              jL = j2L
              jR = j2R
           ENDIF

           DO
              itL = Surf%ITR_Edge(1,jL)
              IF(Surf%IP_tri(1,itL)==0) itL = Surf%ITR_Edge(2,jL)
              IF(Surf%IP_tri(1,itL)==0) CALL Error_Stop('SuperFrqFlt_EdgeCollapse:: fiwejfqi')
              k = which_NodeinTri(ipL,Surf%IP_Tri(1:3,itL))
              JsL = Surf%IED_Tri(k,itL)

              itR = Surf%ITR_Edge(1,jR)
              IF(Surf%IP_tri(1,itR)==0) itR = Surf%ITR_Edge(2,jR)
              IF(Surf%IP_tri(1,itR)==0) CALL Error_Stop('SuperFrqFlt_EdgeCollapse:: svnfdjvno')
              k = which_NodeinTri(ipR,Surf%IP_Tri(1:3,itR))
              JsR = Surf%IED_Tri(k,itR)
              IF(JsL/=JsR) EXIT

              CALL LinkAssociation_Remove(3, itL, Surf%IP_tri(1:3,itL), Surf%ITR_Pt)
              CALL LinkAssociation_Remove(3, itR, Surf%IP_tri(1:3,itR), Surf%ITR_Pt)
              Surf%IP_tri(1,itL) = 0
              Surf%IP_tri(1,itR) = 0
              CALL LinkAssociation_Remove(2, JSL,  Surf%IP_Edge(:,JSL),  Surf%IED_Pt)
              CALL LinkAssociation_Remove(2, JL,   Surf%IP_Edge(:,JL),   Surf%IED_Pt)
              CALL LinkAssociation_Remove(2, JR,   Surf%IP_Edge(:,JR),   Surf%IED_Pt)
              Surf%IP_Edge(1,JSL)  = 0
              Surf%IP_Edge(1,JL)   = 0
              Surf%IP_Edge(1,JR)   = 0

              jL = SUM(Surf%IED_Tri(1:3,itL)) - jL - JsL
              jR = SUM(Surf%IED_Tri(1:3,itR)) - jR - JsR                  
           ENDDO

           IF(Surf%IP_tri(1,Surf%ITR_Edge(1,jR))==0)THEN
              Surf%ITR_Edge(1,jR) = itL
           ELSE
              Surf%ITR_Edge(2,jR) = itL
           ENDIF
           WHERE(Surf%IED_Tri(:, itL)==jL) Surf%IED_Tri(:, itL) = jR
           CALL LinkAssociation_Remove(2, jL,  Surf%IP_Edge(:,jL),  Surf%IED_Pt)
           Surf%IP_Edge(1,jL)  = 0

        ENDDO


        IF(idirt==1)THEN     
           ip1 = ipL
           ip2 = ipR
        ELSE
           ip1 = ipR
           ip2 = ipL
        ENDIF

        CALL LinkAssociation_List(ip1, nList, isList, Surf%ITR_Pt)
        DO n = 1, nList
           IT = isList(n)
           IF(Surf%IP_tri(1,IT)==0) CYCLE
           WHERE(Surf%IP_Tri(1:3,IT)==ip1) Surf%IP_Tri(1:3,IT) = ip2
           CALL SurfTri_CalGeom  (Surf, SurfTri, IT)
           CALL IntQueue_Push36(Surf%ITR_Pt%JointLinks(ip2), IT)
        ENDDO

        CALL LinkAssociation_List(ip1, nList, isList, Surf%IED_Pt)
        DO n = 1, nList
           iss = isList(n)
           ip3 = 0
           DO i=1,2
              IF(Surf%IP_Edge(i,iss)==ip1)THEN
                 Surf%IP_Edge(i,iss) = ip2
              ELSE
                 ip3 = Surf%IP_Edge(i,iss)
              ENDIF
           ENDDO

           IF(ip3==0 .OR. ip3==ip2)THEN
              WRITE(29,*)' Error--- ip3==0 .OR. ip3==ip2'
              WRITE(29,*)'   IS,ip1,ip2,ip3=',IS,ip1,ip2,ip3
              CALL Error_Stop (' SuperFrqFlt_EdgeCollapse :: ') 
           ENDIF

           CALL IntQueue_Push36(Surf%IED_Pt%JointLinks(ip2),iss) 
        ENDDO

        IF(Debug_Display>2)THEN
           !--- check if exist identical edges        
           CALL LinkAssociation_List(ip2, nList, isList, Surf%IED_Pt)
           DO n = 1, nList
              j1L = isList(n)
              DO i = n+1, nList
                 j1R = isList(i)
                 IF(  Surf%IP_Edge(1,j1L)+Surf%IP_Edge(2,j1L) ==    &
                      Surf%IP_Edge(1,j1R)+Surf%IP_Edge(2,j1R)  )THEN
                    WRITE(29,*)' Error---- idential edge-pair for ip2=',ip2,ip1, ' IS=',IS
                    WRITE(29,*)'    egde 1: ',j1L,Surf%IP_Edge(:,j1L)
                    WRITE(29,*)'    egde 2: ',j1R,Surf%IP_Edge(:,j1R)
                    CALL Error_Stop (' SuperFrqFlt_EdgeCollapse ::idential edge-pair ')
                 ENDIF
              ENDDO
           ENDDO
        ENDIF

     ENDDO Loop_IS

     nMove = nMove + iMove
     IF(iMove==0) EXIT
  ENDDO

  CALL LinkAssociation_Clear(Surf%ITR_Pt)
  CALL LinkAssociation_Clear(Surf%IED_Pt)

  WRITE(*,  '(a,i7,/)') '          .. No. of removed nodes : ',  nMove
  WRITE(29, '(a,i7,/)') '          .. No. of removed nodes : ',  nMove

  IF(nMove/=0)THEN
     !--- recount nodes, edges, and triangles
     CALL SuperPatch_Clean_Collapsed(Mark)
     IF(Debug_Display>1)THEN
        CALL Surf_CheckEdge(Surf,i)
        IF(i/=0) CALL Error_Stop (' SuperFrqFlt_EdgeCollapse :: i/=0 ')
     ENDIF
  ENDIF

  DEALLOCATE(Mark)


  RETURN
END SUBROUTINE SuperFrqFlt_EdgeCollapse



