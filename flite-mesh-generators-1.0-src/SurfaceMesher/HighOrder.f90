!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*------------------------------------------------------------------
!>
!<
!*------------------------------------------------------------------
SUBROUTINE HighOrder_find()
  USE control_Parameters
  USE surface_Parameters
  IMPLICIT NONE

  INTEGER :: is, i
  TYPE(IntQueueType)    :: List

  Material%E  = 1.d1
  Material%Nu = 0.4d0   
  CALL MaterialPar_set(Material)

  ALLOCATE(HighSurf(NumRegions), HighCurve(NumCurves))
  HighSurf(:)  = 0
  HighCurve(:) = 0

  Surf%GridOrder = 1
  DO i = 1, ListHi%numNodes
     is = ListHi%Nodes(i)
     IF(is<=0 .OR. is>NumRegions) CYCLE
     HighSurf(is) = 2
     Surf%GridOrder = 3
  ENDDO

  IF(Surf%GridOrder==1) RETURN
  
  Surf%Cell = HighOrderCell_getHighCell(Surf%GridOrder,2,0)

  HighCurve(:) = 2
  DO is = 1, NumRegions
     IF(HighSurf(is)==2) CYCLE
     CALL GetRegionCurveList(is, List)
     DO i = 1, List%numNodes
        HighCurve(List%Nodes(i)) = HighCurve(List%Nodes(i)) -1
     ENDDO
  ENDDO

  DO is = 1, NumRegions
     IF(HighSurf(is)==2) CYCLE
     CALL GetRegionCurveList(is, List)
     DO i = 1, List%numNodes
        IF(HighCurve(List%Nodes(i))==0) CYCLE
        HighSurf(is) = 1
        EXIT
     ENDDO
  ENDDO

END SUBROUTINE HighOrder_find


!*------------------------------------------------------------------
!>
!!   Build a basic triangulation for super-patch treatments.
!!   The regular geometry curvature is used.
!<
!*------------------------------------------------------------------
SUBROUTINE HighOrder_Curve()
  USE occt_fortran
  USE control_Parameters
  USE surface_Parameters
  USE Spacing_Parameters
  USE SurfaceMeshHighOrder
  IMPLICIT NONE

  INTEGER :: is, i, j, ip2(2), nptot, Isucc
  REAL*8  :: t, t1, t2, Pts(3,4), Pt2(3,2)
  REAL*8  :: segas(5), uv(2)
  TYPE(CubicSegment)    :: Segm

  IF(Surf%GridOrder==1) RETURN

  ALLOCATE(CurveHighNodes(NumCurves))

  nptot = Surf%NB_Point
  DO is = 1,NumCurves
     IF(HighCurve(is)==0) CYCLE

     DO i = 1, CurveNodes(is)%numNodes-1
        ip2 = (/CurveNodes(is)%Nodes(i), CurveNodes(is)%Nodes(i+1)/)
      
       IF (Curvature_Type==1) THEN
         t1 = CurveNodePar(is)%V(i)
         t2 = CurveNodePar(is)%V(i+1) - t1
       ELSE IF (Curvature_Type==4) THEN
          CALL OCCT_GetUFromXYZ(is,Surf%Posit(:,ip2(1)),t1)
          CALL OCCT_GetUFromXYZ(is,Surf%Posit(:,ip2(2)),t2)
          t2 = t2 - t1
       ELSE
        WRITE(*,*)"HighOrder not implimented with CADfix"
       END IF
       

       DO j = 1,2
          t = t1 + j / 3.D0 * t2
          CALL GetRegionXYZFromT(is, t, Pts(:,j+2))
       ENDDO
        

        Pts(:,1:2) = Surf%Posit(:,ip2(1:2))
        CALL CubicSegment_Build2(Pts(:,1),Pts(:,3),Pts(:,4),Pts(:,2),Segm)
        CALL CubicSegment_Length (Segm, segas)
        CALL CubicSegment_EvenNodeSet (Segm, segas, 2, uv, Pt2, Isucc)
        DO j = 1,2
           nptot = nptot+1
           IF(nptot>keyLength(Surf%Posit)) CALL allc_2DPointer(Surf%Posit, 3, 2*nptot)
           Surf%Posit(:,nptot) = Pt2(:,j)
           CALL IntQueue_Push(CurveHighNodes(is), nptot)
        ENDDO

     ENDDO
 
  ENDDO
  Surf%NB_Point = nptot

END SUBROUTINE HighOrder_Curve


!=========================================================================
SUBROUTINE ElasticityAdjust(RegionID, Tri3D, map_Point )
  USE control_Parameters
  USE surface_Parameters
  USE Spacing_Parameters
  USE SurfaceMeshHighOrder
  USE SurfaceCurvatureManager
  IMPLICIT NONE
  INTEGER, INTENT(IN)                :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  INTEGER, INTENT(INOUT) :: map_Point(*)

  INTEGER :: IT, IB, IP, NPF, NPL, Msize, nDBC, CurveID, IEdge, Isucc
  INTEGER :: nt, i, iqqt(10), ipg, n, ip2(2), j, k, i1, i2, ip10(10)
  REAL*8  :: Vamm
  REAL*8  :: Pts(3,10), Ptt(3,10), ppts(3,20), duv(2), uv(2), Pt(3), drm
  REAL*8  :: co1(2), co2(2), ull(2,10),  d1, d2
  INTEGER, DIMENSION(:), POINTER :: markp, markt
  REAL*8, DIMENSION(:,:), POINTER :: disp
  TYPE(SurfaceMeshStorageType) :: Scell
  TYPE(NodeNetTreeType) :: Edge_Tree
  TYPE(IntQueueType)    :: CurveRing

  IF(HighSurf(RegionID)==0) RETURN

  CALL Surf_getHighOrder(3, Tri3D)    

  NPF = Tri3D%Cell%numCellNodes      !--- number of nodes on a triangle.
  NPL = Tri3D%Cell%numFaceNodes      !--- number of nodes on an edge.

  CALL Surf_BuildEdge(Tri3D, Edge_Tree)

  !--- reset edge by shortest path
  DO IEdge = 1, Tri3D%NB_Edge
     ip2(1:2) = Tri3D%IP_Edge(1:2,IEdge)
     co1(:) = Tri3D%Coord(:,ip2(1))
     co2(:) = Tri3D%Coord(:,ip2(2))
     CALL Gen_GetShortPath(RegionID,  co1, co2, 2, ull) 
     
     Tri3D%Coord(:,Tri3D%IP_Edge(3,IEdge)) = ull(:,1)
     Tri3D%Coord(:,Tri3D%IP_Edge(4,IEdge)) = ull(:,2)
  ENDDO

    !--- reset centre by shortest path
  DO IT = 1, Tri3D%NB_Tri
     ip10 = Tri3D%IPsp_Tri(:,iT)
     uv = 0
     do j = 1,3
        k = j-1
        if(k<1) k = 3
        ip2(1) = ip10(Tri3D%Cell%faceNodes(NPL,k))
        k = j+1
        if(k>3) k = 1
        ip2(2) = ip10(Tri3D%Cell%faceNodes(3, k))
        co1(:) = Tri3D%Coord(:,ip2(1))
        co2(:) = Tri3D%Coord(:,ip2(2))
        CALL Gen_GetShortPath(RegionID,  co1, co2, 1, ull)
        
        uv  = uv + ull(:,1)
     enddo
     Tri3D%Coord(:,ip10(7)) = uv(:) / 3
  ENDDO
  

  !--- calculate boundary movement and mapping to global mesh
    
  ALLOCATE(markp(Surf%NB_Point))
  DO ip = 1, Tri3D%NB_BD_Point
     ipg = map_Point(ip)
     markp(ipg)  = ip
  ENDDO

  Msize = NPL * Tri3D%NB_BD_Point
  ALLOCATE(Tri3D%IP_DBC(Msize))
  ALLOCATE(Tri3D%Pd_DBC(2,Msize))

  Tri3D%IP_DBC(  1:Tri3D%NB_BD_Point) = (/ (i, i=1,Tri3D%NB_BD_Point) /)
  Tri3D%Pd_DBC(:,1:Tri3D%NB_BD_Point) = 0
  nDBC = Tri3D%NB_BD_Point

  CALL GetRegionCurveList(RegionID, CurveRing)
  

  DO n = 1, CurveRing%numNodes
     CurveID = CurveRing%Nodes(n)

     DO i = 1, CurveNodes(CurveID)%numNodes -1
        ip2(1:2) = CurveNodes(CurveID)%Nodes(i:i+1)           
        ip2(1:2) = markp(ip2(1:2))
        CALL NodeNetTree_Search (2, ip2, Edge_Tree, IEdge)
        IF(IEdge==0) STOP 'dmdkeidu023fjirefj'

        duv(:) = 1.d0/(NPL-1)* (Tri3D%Coord(:,ip2(2)) - Tri3D%Coord(:,ip2(1)))
        DO j = 1,NPL-2
           nDBC = nDBC + 1
           IF(ip2(1)==Tri3D%IP_Edge(1,Iedge))THEN
              Tri3D%IP_DBC(nDBC) = Tri3D%IP_Edge(j+2,Iedge)
           ELSE
              Tri3D%IP_DBC(nDBC) = Tri3D%IP_Edge(NPL+1-j,Iedge)
           ENDIF
           IF(HighCurve(CurveID)==0)THEN
              Tri3D%Pd_DBC(:,nDBC) = 0
           ELSE
              k     = (NPL-2)*(i-1) +j
              ipg   = CurveHighNodes(CurveID)%Nodes(k)
              Pt(:) = Surf%Posit(:,ipg)
              uv(:) = Tri3D%Coord(:,ip2(1)) + j*duv(:)
              CALL ProjectToRegionFromXYZ(RegionID, Pt, uv, TOLG, 1, drm)
              Tri3D%Pd_DBC(:,nDBC) = uv - Tri3D%Coord(:,Tri3D%IP_DBC(nDBC))
              map_Point(Tri3D%IP_DBC(nDBC)) = ipg
           ENDIF
        ENDDO
     ENDDO
  ENDDO
  Tri3D%NB_DBC = nDBC

  !--- fit boundary by elasticity
  
  ALLOCATE(Disp(2, Tri3D%NB_Point))
  CALL allc_2Dpointer(Tri3D%Posit, 3, keyLength(Tri3D%Coord))

!  CALL Surf_linearSystemSolver(Tri3D, material, Disp, Isucc)
  Isucc = 0
  IF(Isucc<=0)THEN
     ! WRITE(29,*)' Warning--- negative Jacobian at triangle ',abs(Isucc)
      Disp(:,:) = 0
  ENDIF

  DO i = 1, Tri3D%NB_DBC 
     ip = Tri3D%IP_DBC(i)
     Tri3D%Coord(:,ip) = Tri3D%Coord(:,ip) + Tri3D%Pd_DBC(:,i)
  ENDDO
  DO ip = 1, Tri3D%NB_Point
     IF(ip<=Tri3D%NB_BD_Point) CYCLE
     IF(map_Point(IP)>0)THEN
        Tri3D%Posit(:,ip) = Surf%Posit(:,map_Point(IP))
     ELSE
        Tri3D%Coord(:,ip) = Tri3D%Coord(:,ip) + disp(:,ip)
        CALL GetUVPointInfo0(RegionID, Tri3D%Coord(1,ip), Tri3D%Coord(2,ip), Pt)
        Tri3D%Posit(:,ip) = Pt
     ENDIF
  ENDDO

  IF(Debug_Display>=1)THEN
     DO ip = 1, Tri3D%NB_Point
       !WRITE(37,'(3f9.4)') disp(:,ip)
     ENDDO
     !--- compute and compare the Jacobians at the Gauss points
     CALL Surf_calcQuality2D(Tri3D, 0, it, Vamm)

     WRITE(29,*)' The worst 2D triangle is at IT=', it, ' with Vab=',Vamm
     CALL Surf_CellDisplay (Tri3d, it, 9, 20, 1) 
  ENDIF

  DEALLOCATE(Disp)

END SUBROUTINE ElasticityAdjust


!>
  !!  Search the shortest path on a parametric surface.
  !!
  !!  @param[in]  Region  : the surface Region ID.
  !!  @param[in]  uv1,uv2 : the two ends on the parameter plane.
  !!  @param[in]  NP      : the number of nodes on the path (not including two ends). 
  !!  @param[out] uvn (2,NP) : the coordinates of the nodes on the parameter plane.
  !<
  SUBROUTINE Gen_GetShortPath(Region,  uv1, uv2, NP, uvn)
    USE Geometry3DAll
    IMPLICIT NONE
    INTEGER, INTENT(in) :: Region
    REAL*8,  INTENT(in)   :: uv1(2), uv2(2)
    INTEGER, INTENT(in)   :: NP
    REAL*8,  INTENT(out)  :: uvn(2,*)
    INTEGER, PARAMETER    :: N = 65   !-- precise index, (2**K+1)
    REAL*8  :: uvs(2,N), pts(3,N), Dis(N)
    INTEGER :: i1, i2, i, m, k, nlist, iloop
    INTEGER, DIMENSION(N) :: next, list, level
    REAL*8  :: t1(2), t2(2), um(2), uvmin(2), uvmax(2)
    REAL*8  :: p1(3), p2(3), pm(3), d, dd

    CALL GetRegionUVBox(Region,uvmin(1),uvmin(2),uvmax(1),uvmax(2))

    uvs(:,1) = uv1(:)
    uvs(:,2) = uv2(:)
    CALL GetUVPointInfo0(Region, uv1(1), uv1(2), P1)
    CALL GetUVPointInfo0(Region, uv2(1), uv2(2), P2)
    pts(:,1) = P1
    pts(:,2) = P2
    level(:) = 0
    next(1)  = 2

    nlist = 2
    i1    = 1
    iloop = 0
    DO WHILE(nlist<N)
       IF(i1==1)  iloop = iloop+1      
       i2 = next(i1)

       p1 = pts(:,i1)
       p2 = pts(:,i2)
       t1 = uvs(:,i1)
       t2 = uvs(:,i2)
       m  = 2* (N-nlist) + 7      
       CALL Gen_GetShortPath_bisect()

       !--- add a node
       nlist = nlist+1
       uvs(:,nlist) = um(:)
       pts(:,nlist) = Pm(:)
       next(i1)     = nlist
       next(nlist)  = i2
       level(nlist) = iloop

       IF(level(nlist-1)==iloop)THEN
          t1 = uvs(:,nlist-1)
          t2 = uvs(:,nlist)
          p1 = pts(:,nlist-1)
          p2 = pts(:,nlist)
          CALL Gen_GetShortPath_bisect()
          uvs(:,i1) = um(:)
          pts(:,i1) = Pm(:)
       ENDIF

       i1 = i2
       IF(i1==2)  i1 = 1
    ENDDO

    !--- sort
    list(1) = 1
    DO i = 2,N
       list(i) = next(list(i-1))
    ENDDO

    uvs(:,1:N) = uvs(:,list(1:N))
    pts(:,1:N) = pts(:,list(1:N))

    !--- distance accumulate
    Dis(1) = 0.
    DO i = 2,N
       Dis(i) = Dis(i-1) + Geo3D_Distance(pts(:,i-1),pts(:,i))
    ENDDO

    i2 = 2
    dd = Dis(N) / (NP+1)
    DO k = 1, NP
       d = k*dd
       do i = i2, N
          if(Dis(i)>d) exit
       enddo
       i2 = i
       i1 = i2-1
       uvn(:,k) = uvs(:,i1) + (d-Dis(i1)) / (Dis(i2)-Dis(i1)) * (uvs(:,i2) - uvs(:,i1))
    ENDDO
    
    RETURN
    
  CONTAINS

    SUBROUTINE Gen_GetShortPath_bisect()
      USE Geometry3DAll
      IMPLICIT NONE
      INTEGER :: i
      REAL*8  :: duv(2), unc(2), un(2), dmin, d, Pt(3)

      duv(1) =  t2(2) - t1(2)
      duv(2) =  t1(1) - t2(1)
      unc(:) = (t1(:) + t2(:)) / 2.d0
      duv(:) = duv(:) / (2*m)            
      dmin = 1.d30
      DO i = -m,m
         un = unc(:) + i * duv(:)
         IF(un(1)<uvmin(1)) CYCLE
         IF(un(2)<uvmin(2)) CYCLE
         IF(un(1)>uvmax(1)) CYCLE
         IF(un(2)>uvmax(2)) CYCLE
         CALL GetUVPointInfo0(Region, un(1), un(2), Pt)
         d = Geo3D_Distance(Pt,P1) + Geo3D_Distance(Pt,P2)
         IF(d<dmin)THEN
            dmin = d
            um   = un
            Pm   = Pt
         ENDIF
      ENDDO
    END SUBROUTINE Gen_GetShortPath_bisect

  END SUBROUTINE Gen_GetShortPath


!*------------------------------------------------------------------
!>
!!   Build a basic triangulation for super-patch treatments.
!!   The regular geometry curvature is used.
!<
!*------------------------------------------------------------------
SUBROUTINE HighOrder_Build(RegionID, Tri3D)
  USE control_Parameters
  USE surface_Parameters
  USE Spacing_Parameters
  USE SurfaceMeshHighOrder
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  INTEGER :: i, j, k, ip, is, ie
  INTEGER :: ip10(10), ip4(4), ip3(3), ip2(2), IEdge, itry, iLoop, Isucc
  INTEGER :: itL,itR,ipL,ipR,Jed(2,2)
  INTEGER, DIMENSION(:), POINTER ::  MarkEd
  REAL*8  :: Co1(2), Co2(2), Co3(2),  Coord(2,10), drm
  REAL*8  :: Pt(3), P1(3), P2(3), P3(3), P4(3), p00(3), Pts(3,10), Pt2(3,2)
  REAL*8  :: t1, t2, Va, Vamax, Vamin, Vab, Vamm, Vas(10), dotp, crop
  REAL*8  :: Vt(3), Vs(3), Vtp(3,2), Pu(3), Pv(3), Puv(3)
  REAL*8  :: uvRt(2), uvLf(2), uvCv(2), uvSt(2), uv(2)
  REAL*8  :: WDs(4) = (/-5.5d0, 9.0d0, -4.5d0,  1.d0/)
  REAL*8  :: coseps = dcos( 5.0d0/180.d0 *3.14d0)
  REAL*8  :: cosast = dcos(25.0d0/180.d0 *3.14d0)
  REAL*8  :: cosahf = dcos(12.5d0/180.d0 *3.14d0)
  REAL*8  :: sinahf = dsin(12.5d0/180.d0 *3.14d0)
  REAL*8  :: costiny, sintiny
  REAL*8  :: segas(5)
  INTEGER, PARAMETER :: ii(4,2) = reshape( (/1,3,4,2, 2,4,3,1/), (/4,2/) )
  TYPE(NodeNetTreeType) :: Edge_Tree
  TYPE(Plane3D)         :: aPlane
  TYPE(CubicSegment)    :: Segm
  REAL*8, DIMENSION(:,:), POINTER :: coordbak


  IF(Tri3D%GridOrder==1) RETURN


  !--- copy the old coord
  ALLOCATE(coordbak(2, Tri3D%NB_Point))
  coordbak(:,1:Tri3D%NB_Point) = Tri3D%Coord(:,1:Tri3D%NB_Point)

  !---- build edges  
  CALL Surf_BuildEdge(Tri3D, Edge_Tree)
  ALLOCATE(MarkEd(Tri3D%NB_Edge))
  MarkEd(:) = 1
     
  P00 = 0.d0
  DO iLoop = 1, 3

     DO IEdge = 1, Tri3D%NB_Edge
        !--- find neighbour edges
        !    Jed(2,2) :: edge with node Tri3D%IP_Edge(2,IEdge) and on left
        !    Jed(1,1) :: edge with node Tri3D%IP_Edge(1,IEdge) and on Left
        !    Jed(2,1) :: edge with node Tri3D%IP_Edge(2,IEdge) and on right
        !    Jed(1,2) :: edge with node Tri3D%IP_Edge(1,IEdge) and on right

        IF(Tri3D%ITR_Edge(1,IEdge)==0 .OR. Tri3D%ITR_Edge(2,IEdge)==0) CYCLE
        CALL Surf_GetEdgeSurround(Tri3D, IEdge,itL,itR,ipL,ipR,jed(2,2),Jed(1,1),Jed(2,1),Jed(1,2))
        IF(  MarkEd(Jed(1,1))<iLoop .AND. MarkEd(Jed(2,1))<iLoop .AND.   &
             MarkEd(Jed(1,2))<iLoop .AND. MarkEd(Jed(2,2))<iLoop ) CYCLE

        ip2(:) = Tri3D%IP_Edge(1:2,IEdge)

        !--- get tangent direction at two end of the edge
        itry = 0
        DO i = 1,2

           ip = ip2(i)
           Coord(:,i) = Tri3D%Coord(:,ip)

           !--- get the direction of the edge
           ip4(:) = Tri3D%IP_Edge(ii(1:4,i), IEdge)
           Vt(:)  = Tri3D%Posit(:,ip4(4)) - Tri3D%Posit(:,ip4(1))
           Vs(:)  = WDs(1)*Tri3D%Posit(:,ip4(1)) + WDs(2)*Tri3D%Posit(:,ip4(2))    &
                  + WDs(3)*Tri3D%Posit(:,ip4(3)) + WDs(4)*Tri3D%Posit(:,ip4(4))
           Vtp(:,i) = Vs

           !--- get the tangent plane
           !    is the parameter plane (u-v) always a right-hand system???

           CALL GetUVPointInfo1(RegionID, Coord(1,i), Coord(2,i), Pt, Pu, Pv, Puv)
           IF(Geo3D_Distance_SQ(Pv) < 1.d-6*Geo3D_Distance_SQ(Pu)) CYCLE    !--- 
           IF(Geo3D_Distance_SQ(Pu) < 1.d-6*Geo3D_Distance_SQ(Pv)) CYCLE
           aPlane = Plane3D_Build (p00,pu,pv)
           CALL Plane3D_buildTangentAxes (aPlane)

           !--- get the tangent direction of the edge
           uvSt(:) = Plane3D_Project2D (aPlane, Vt)
           uvCv(:) = Plane3D_Project2D (aPlane, Vs)
           uvSt(:) = uvSt(:) / dsqrt(uvSt(1)**2 + uvSt(2)**2)
           uvCv(:) = uvCv(:) / dsqrt(uvCv(1)**2 + uvCv(2)**2)

           !--- get the tangent direction of the neighbour edges
           costiny = cosahf
           DO k = 1,2
              !--- k=1, egde on anti-clockwise; k=2, edge on clockwise
              is = Jed(i,k)
              IF(Tri3D%IP_Edge(1,is)==ip)THEN
                 ip4(:) = Tri3D%IP_Edge(ii(1:4,1),is)
              ELSE IF(Tri3D%IP_Edge(2,is)==ip)THEN
                 ip4(:) = Tri3D%IP_Edge(ii(1:4,2),is)
              ELSE
                 CALL Error_Stop('HighOrder_Build:: wrong neighbour edge')
              ENDIF
              Vs(:) = WDs(1)*Tri3D%Posit(:,ip4(1)) + WDs(2)*Tri3D%Posit(:,ip4(2))    &
                    + WDs(3)*Tri3D%Posit(:,ip4(3)) + WDs(4)*Tri3D%Posit(:,ip4(4))
              uv = Plane3D_Project2D (aPlane, VS)
              uv = uv / dsqrt(uv(1)**2 + uv(2)**2)
              IF(k==1)THEN
                 uvLf = uv
              ELSE
                 uvRt = uv
              ENDIF
              Vt(:) = Tri3D%Posit(:,ip4(4)) - Tri3D%Posit(:,ip4(1))
              uv(:) = Plane3D_Project2D (aPlane, Vt)
              uv    = uv / dsqrt(uv(1)**2 + uv(2)**2)
              dotp  = uv(1)*uvSt(1) + uv(2)*uvST(2)
              costiny = max(costiny, dotp)
           ENDDO
           sintiny = dsqrt(1.d0-costiny*costiny)

           dotp = uvLf(1)*uvRt(1) + uvLf(2)*uvRt(2)
           crop = uvLf(1)*uvRt(2) - uvLf(2)*uvRt(1)
           IF(dotp>cosast .AND. crop>0)THEN
              !--- small angle between left and right edge, choose the bisection
              uv = Included_Angle_Bisect(uvLf(:), uvRt(:), 0)
           ELSE
              uv = Included_Angle_Bisect(uvSt(:), uvCv(:), 0)                     
              dotp = uvLf(1)*uv(1) + uvLf(2)*uv(2)
              crop = uvLf(1)*uv(2) - uvLf(2)*uv(1)
              IF(dotp>costiny .AND. crop>0)THEN
                 !--- small angle between left and original edge, choose the bound.
                 uv(1) = costiny*uvLf(1) - sintiny*uvLf(2)
                 uv(2) = sintiny*uvLf(1) + costiny*uvLf(2)
              ELSE IF(crop<=0)THEN
                 uv = Included_Angle_Bisect(uvLf(:), uvRt(:), 1)
              ELSE
                 dotp =  uvRt(1)*uv(1) + uvRt(2)*uv(2)
                 crop = -uvRt(1)*uv(2) + uvRt(2)*uv(1)
                 IF(dotp>costiny .AND. crop>0)THEN
                    !--- small angle between right and original edge, choose the bound.
                    uv(1) =  costiny*uvRt(1) + sintiny*uvRt(2)
                    uv(2) = -sintiny*uvRt(1) + costiny*uvRt(2)
                 ELSE  IF(crop<=0)THEN
                    uv = Included_Angle_Bisect(uvLf(:), uvRt(:), 1)
                 ENDIF
              ENDIF
           ENDIF

           dotp = uv(1)*uvCv(1) + uv(2)*uvCv(2)
           IF(dotp>coseps)THEN
              !--- only a small difference, do not modify
           ELSE
              itry     = 1            
              t1       = Geo3D_Distance(Vtp(:,i))
              Vtp(:,i) = Plane3D_get3DPosition (aPlane, uv)
              Vtp(:,i) = Vtp(:,i) * t1
           ENDIF

        ENDDO

        IF(itry==0) CYCLE   !--- good enough or only straight edge arround

        !--- adjust the segment
        Vt =   Vtp(:,1)
        Vs = - Vtp(:,2)
        CALL CubicSegment_Build(Tri3D%Posit(:,ip2(1)),Vt,Tri3D%Posit(:,ip2(2)),Vs,Segm)
        CALL CubicSegment_Length (Segm, segas)
        CALL CubicSegment_EvenNodeSet (Segm, segas, 2, uv, Pt2, Isucc)
        IF(Isucc==0)THEN
           WRITE(29,*)' Error---- cat not do EvenNodeSet '
           WRITE(29,*)'   Iedge=',Iedge, ' ips=',Tri3D%IP_Edge(1:2,IEdge)
           CALL CubicSegment_Display (Segm, 9, 31)
           CALL Error_STOP( 'HighOrder_Build:: EvenNodeSet')
        ENDIF
        DO j = 1,2
           Pt  = Pt2(:,j)
           ip  = Tri3D%IP_Edge(j+2,IEdge)
           Co1 = Tri3D%Coord(:,ip)
           CALL ProjectToRegionFromXYZ(RegionID, Pt, Co1, TOLG, 1, drm)
           Tri3D%Posit(:,ip) = Pt
           Tri3D%Coord(:,ip) = Co1
        ENDDO
        MarkEd(IEdge) = iLoop+1

     ENDDO

  ENDDO

  CALL HighOrder_CentreSmooth(RegionID, Tri3D, coordbak)

  CALL NodeNetTree_Clear(Edge_Tree)
  DEALLOCATE(MarkEd)
  DEALLOCATE(coordbak)
  CALL Surf_ClearEdge(Tri3D)

  RETURN
END SUBROUTINE HighOrder_Build

!*------------------------------------------------------------------
!>
!!   
!<
!*------------------------------------------------------------------
SUBROUTINE HighOrder_CentreSmooth(RegionID, Tri3D, coordbak)
  USE control_Parameters
  USE Spacing_Parameters
  USE SurfaceMeshHighOrder
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: RegionID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Tri3D
  REAL*8, DIMENSION(2,*) :: coordbak
  INTEGER :: i, j, k, ie, ip10(10), iLoop, Isucc, itm
  REAL*8, DIMENSION(:,:), POINTER :: disp
  REAL*8 :: Pt(3), Co1(2), Co2(2)
  TYPE(SurfaceMeshStorageType) :: OneTri
  REAL*8 :: Vamax, Vamin, Vamm, Vamm2D, Vas(10), puv(3), Pts(3,10), Vtp(3,2), Vt(3)
  REAL*8 :: GaussShapeFD(10,2,12)


  !--- build one-cell mesh
  OneTri%NB_Tri   = 1
  OneTri%NB_Point = 3
  ALLOCATE(OneTri%IP_Tri(5,1))
  ALLOCATE(OneTri%Coord(2,10))
  OneTri%IP_Tri(:,1) = (/1,2,3,0,0/)
  OneTri%Coord(:,1)  = (/0.d0,0.d0/)
  OneTri%Coord(:,2)  = (/1.d0,0.d0/)
  OneTri%Coord(:,3)  = (/0.d0,1.d0/)
  CALL Surf_getHighOrder(3, OneTri)
  OneTri%NB_DBC = 9
  ALLOCATE(OneTri%IP_DBC(  OneTri%NB_DBC))
  ALLOCATE(OneTri%Pd_DBC(2,OneTri%NB_DBC))
  OneTri%IP_DBC(:) = (/1,2,3,4,5,6,8,9,10/)
  ALLOCATE(Disp(2, OneTri%NB_Point))

  !--- get shape function (for derivative) at edge points
  DO k = 4,10
     Co1 = Coord_C3Tri(:,k)
     CALL CubicTriFace_ShapeFuncDiff (Co1(1), Co1(2), GaussShapeFD(:,:,k))
  ENDDO
  
  !--- set centroid points of triangles
  DO ie = 1,Tri3D%NB_Tri
     itm = ie
     CALL Surf_calcQuality3D(Tri3D, -1, itm, Vamm)
     IF(Vamm>0.65) CYCLE
     CALL Surf_calcQuality2D(Tri3D, -1, itm, Vamm2D)
     
     ip10 = Tri3D%IPsp_Tri(:,ie)
     
     IF(Vamm2D>Vamm)THEN
     OneTri%Coord(:,1:10) = coordbak(:,ip10(1:10))
     DO i = 1, OneTri%NB_DBC
        j = OneTri%IP_DBC(i)
        OneTri%Pd_DBC(:,i) = Tri3D%Coord(:,ip10(j)) - coordbak(:,ip10(j))
     ENDDO

     CALL  Surf_linearSystemSolver(OneTri, material, Disp, Isucc)

     IF(Isucc==1)THEN
        Co1 = coordbak(:,ip10(7)) + disp(:,7)    
     ELSE
        Co1 = ( Tri3D%Coord(:,ip10(4))+Tri3D%Coord(:,ip10(5))+Tri3D%Coord(:,ip10(6))   &
            +   Tri3D%Coord(:,ip10(8))+Tri3D%Coord(:,ip10(9))+Tri3D%Coord(:,ip10(10)) ) / 4   &
            - ( Tri3D%Coord(:,ip10(1))+Tri3D%Coord(:,ip10(2))+Tri3D%Coord(:,ip10(3)) ) / 6

     ENDIF
     
     ELSE
        Pt =  ( Tri3D%Posit(:,ip10(4))+Tri3D%Posit(:,ip10(5))+Tri3D%Posit(:,ip10(6))   &
            +   Tri3D%Posit(:,ip10(8))+Tri3D%Posit(:,ip10(9))+Tri3D%Posit(:,ip10(10)) ) / 4   &
            - ( Tri3D%Posit(:,ip10(1))+Tri3D%Posit(:,ip10(2))+Tri3D%Posit(:,ip10(3)) ) / 6
        CALL ProjectToRegionFromXYZ(RegionID, Pt, Co1, TOLG, 1, Vamm)
     ENDIF
     
     CALL GetUVPointInfo0(RegionID, Co1(1), Co1(2), Pt)     
     Tri3D%Posit(:,ip10(7)) = Pt
     Tri3D%Coord(:,ip10(7)) = Co1
     
     OneTri%Coord(:,1:10) = Tri3D%Coord(:,ip10(1:10))
     Pts(:,1:10) = Tri3D%Posit(:,ip10(1:10))
     Puv = Geo3D_Cross_Product(Pts(:,2),Pts(:,1),Pts(:,3))
     
     Vas(7) = 0
     DO iLoop = 1, 10
        Vamax =  1.d-36
        Vamin =  1.d12
        Vamm  =  0
        DO k = 4,10
           IF(k==7) CYCLE
           Vtp(1:3,1:2) = MATMUL(Pts, GaussShapeFD(:,:,k))
           Vt     = Geo3D_Cross_Product(Vtp(:,1), Vtp(:,2))
           Vas(k) = Geo3d_Distance(Vt)
           IF(Geo3D_Dot_Product(Vt,Puv)<0 .or. Vas(k)<=1.d-36) Vas(k) = 1.d-36
           Vamax = MAX(Vamax, Vas(k))
           Vamin = MIN(Vamin, Vas(k))
           Vamm  = Vamm + Vas(k)       
        ENDDO
        IF(Vamin>0.7*Vamax) EXIT
        Vas(4:10) = Vas(4:10)/Vamm
        Where(Vas(4:6 )<0.05) Vas(4:6 ) = 0.05
        Where(Vas(8:10)<0.05) Vas(8:10) = 0.05
        Vamm = SUM(Vas(4:6)) + SUM(Vas(8:10))
        Vas(4:10) = Vas(4:10)/Vamm - 1.d0/6.d0
        Co2 =  Vas(4)* OneTri%Coord(:,4) + Vas(5) * OneTri%Coord(:,5)   &
             + Vas(6)* OneTri%Coord(:,6) + Vas(8) * OneTri%Coord(:,8)   &
             + Vas(9)* OneTri%Coord(:,9) + Vas(10)* OneTri%Coord(:,10)
        Co1 = Co1 + 0.25*Co2
        CALL GetUVPointInfo0(RegionID, Co1(1), Co1(2), Pts(:,7))
     ENDDO

     Tri3D%Posit(:,ip10(7)) = Pts(:,7)
     Tri3D%Coord(:,ip10(7)) = Co1

  ENDDO

  DEALLOCATE(Disp)

  RETURN
END SUBROUTINE HighOrder_CentreSmooth


!*------------------------------------------------------------------
!>
!!   check jacobian
!<
!*------------------------------------------------------------------
SUBROUTINE HighOrder_calcQuality( )
  USE surface_Parameters
  USE SurfaceMeshHighOrder
  IMPLICIT NONE

  INTEGER :: itL
  REAL*8  :: Vamm

  IF(Surf%GridOrder==1) RETURN
     CALL Surf_calcQuality3D(Surf, 0, itL, Vamm)
     WRITE(29,*)' The worst triangle is at ie=',itL, ' with Vab=',Vamm
     IF(itL==0) RETURN
     WRITE(29,*)'  ips=',Surf%IP_Tri(:,itL)
     CALL Surf_CellDisplay3D (Surf, itL, 9, 30, 1) 

  RETURN
END SUBROUTINE HighOrder_calcQuality