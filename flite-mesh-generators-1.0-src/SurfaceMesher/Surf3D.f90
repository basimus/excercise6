!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*----------------------------------------------------------------------------*
!*                                                                            *
!*                                                                            *
!*  >>>>   S U R F A C E   T R I A N G U L A T I O N    P R O G R A M   <<<<  *
!*                                                                            *
!*                                                                            *
!*----------------------------------------------------------------------------*
!>
!!  The main routine of the surface generator.
!<
PROGRAM SURFACE

  USE control_Parameters
  USE surface_Parameters
  USE SurfaceMeshManager
  USE SurfaceMeshHighOrder
  USE Spacing_Parameters
  IMPLICIT NONE
  REAL    :: TimeStart, TimeEnd
  LOGICAL :: TF
  INTEGER :: ierr
  REAL    :: tStart, tEnd

!---  licence check
!  call lictest(2094,5,1)

  CALL CPU_TIME(TimeStart)
  
  !---- Input job name and control parameters
  CALL Control_Input()


  IF(Start_Point<=1)THEN
     
     

     ! IF (Curvature_Type.EQ.4) THEN

     !   CALL FLUSH( 6, ierr )
     !   !---- Input/build background
     !   WRITE(*,*)' '
     !   WRITE(*,*)'---- Read Background ----'
     !   CALL Background_Input()

     !   WRITE(*,*)' '
     !   WRITE(6,*)'---- Read geometry ----'
       
     !   !---- Input geometry
     !   CALL Curvature_Input()

     !   !--- find high-order surfaces and curves
     !   CALL HighOrder_find()

       

     ! ELSE

       WRITE(*,*)' '
       WRITE(6,*)'---- Read geometry ----'
       !---- Input geometry
       CALL Curvature_Input()

       !--- find high-order surfaces and curves
       CALL HighOrder_find()

       !---- Input/build background
       WRITE(*,*)' '
       WRITE(*,*)'---- Read Background ----'
       CALL Background_Input()
     !ENDIF

     IF (Start_Point==0) THEN
      BGSpacing%BasicSize = BGSpacing%BasicSize
     END IF

     !---- Input Super Patches
     CALL SuperPatch_Input()


     !--- Generate Curves
     WRITE(*,*)' '
     WRITE(*,*)'---- Generate Curves ----'
     CALL  GenCurve( )

     !--- Generate Regions
     WRITE(*,*)' '
     WRITE(*,*)'---- Generate Regions ----'
     CALL  GenRegion( )

     CALL CPU_TIME(tStart)
     !--- Check if identical triangle existing between 2 patches
     WRITE(*,*)' '
     WRITE(*,*)'---- Remove identical triangle pairs ----'
!    CALL Surf_IdenticalTri ( ) 
     CALL CPU_TIME(tEnd)
     WRITE(*, *) 'Identical pairs cpu_time: ',tEnd-tStart
     WRITE(29,*) 'Identical pairs cpu_time: ',tEnd-tStart

     !--- Reset surface orientation
     IF(ABS(Orientation_Direct)==1)THEN
        WRITE(*,*)' '
        WRITE(*,*)'---- Reset surface orientation ----'
        CALL CPU_TIME(tStart)
        CALL Surf_Reverse (Surf, Orientation_Direct) 
        CALL CPU_TIME(tEnd)
        WRITE(*, *) 'Reset surface orientation cpu_time: ',tEnd-tStart
        WRITE(29,*) 'Reset surface orientation cpu_time: ',tEnd-tStart
     ENDIF

  ELSE

     !---- Input/build background
     WRITE(*,*)' '
     WRITE(*,*)'---- Read Background ----'
     CALL Background_Input()

     WRITE(*,*)'---- Read surface mesh ----'
     CALL SurfaceMeshStorage_Input (JobName(1:JobNameLength)//'_0', JobNameLength+2, Surf)

  ENDIF

  !--- Super-Patches
  IF(SuperCosmetic_Method>0)THEN
     WRITE(*,*)' '
     WRITE(*,*)'---- Super-patches build ----'
     IF(Surf%NB_Quad>0)THEN
        WRITE(29,*)' Warning----: no super-patch treatment done'
        WRITE(29,*)'              due to existing quadrilateral elements ----'
     ELSE
        IF(SuperCosmetic_Method==1)THEN
           CALL SuperPatch_Build()
        ELSE
           CALL SuperPatch_Build3()
        ENDIF

        WRITE(*,*)' '
        WRITE(*,*)'---- Super-patches cosmetic ----'
        CALL SuperPatch_Cosmetric()

        IF(SuperCosmetic_Quad/=-1)THEN
           WRITE(*,*)' '
           WRITE(*,*)'---- Super-patches merge to quadrangles ----'
           CALL SuperPatch_Merge_Quad( )
        ENDIF
     ENDIF
  ENDIF

  
  IF(Surf%GridOrder>1)THEN
     !--- sort the high-order nodes
     CALL Surf_SortHighOrderNodes(Surf)
     CALL HighOrder_calcQuality( )
  ENDIF

  !--- output surface mesh
  WRITE(*,*)' '
  WRITE(*,*)'---- output surface mesh ----'
  CALL SurfaceMeshStorage_Output (JobName, JobNameLength, Surf)         

  
  !CALL CADFIX_Check_Availability( TF )
  !IF(TF)THEN
    !--- output surface mesh  with CADfix
  !   WRITE(*,*)' '
  !  WRITE(*,*)'---- output surface CADfix mesh ----'
  !   CALL Output_CAD ( )         
  !ENDIF

  CALL CPU_TIME(TimeEnd)
  IF(Debug_Display>0)THEN
     WRITE(*, *) 'Total cpu_time: ',TimeEnd-TimeStart
     WRITE(29,*) 'Total cpu_time: ',TimeEnd-TimeStart
  ENDIF


  WRITE(*,*)' '
  WRITE(*,*) '---- Successfully ----'
  CALL WellDone_Stop()

END PROGRAM SURFACE


!******************************************************************************
!>
!!  Check and remove identical triangles.
!!  Two triangles might be identical from different pitches if both of them
!!      have only pitch-boundary nodes.
!<
!******************************************************************************
SUBROUTINE Surf_IdenticalTri( )
  USE surface_Parameters
  USE array_allocator
  USE SurfaceMeshManager
  USE SurfaceMeshManager2D
  USE NodeNetTree
  IMPLICIT NONE

  INTEGER, SAVE :: ii(5) = (/1,2,3,1,2/)
  INTEGER :: IC, IE, IS, IP, IT, IT1, IT2, IT3, ips(4), ITs(4), NT1, NT2
  INTEGER :: i, j, Mside, numEdgesOnCurve, numEdgesLinkCurve, itry
  INTEGER :: npair, nswap, nins
  REAL*8  :: p0(3), rc(2,4)
  TYPE(NodeNetTreeType) :: Edge_Tree
  INTEGER, DIMENSION(:,:), POINTER :: Itri_Edge
  INTEGER, DIMENSION(:,:), POINTER :: IP_Edge


  !--- build edges on curves
  Mside = 4* numAllCurveNodes
  CALL NodeNetTree_Allocate(2, Surf%NB_Point, Mside, Edge_Tree)
  ALLOCATE(ITri_Edge(4,Mside), IP_Edge(2,Mside))
  ITri_Edge(1:4,1:Mside) = 0

  DO IC = 1, NumCurves
     DO i = 1, CurveNodes(IC)%numNodes -1
        ips(1:2) = CurveNodes(IC)%Nodes(i:i+1)
        CALL NodeNetTree_SearchAdd (2, ips(1:2), Edge_Tree, IE)
     ENDDO
  ENDDO
  numEdgesOnCurve = Edge_Tree%numNets

  DO IT = 1, Surf%NB_Tri
     DO i=1,3
        ips(1:2)   = Surf%IP_Tri(ii(i:i+1),IT)
        IF(ips(1)>numAllCurveNodes .OR. ips(2)>numAllCurveNodes) CYCLE

        CALL NodeNetTree_SearchAdd (2, ips(1:2), Edge_Tree, IE)
        IF(IE>Mside) CALL Error_Stop ('Surf_IdenticalTri:: IE>Mside  ') 
        IF(IE<=numEdgesOnCurve) CYCLE

        IF(ITri_Edge(1,IE)==0)THEN
           ITri_Edge(1,IE) = IT
           IP_Edge(:,IE) = ips(1:2)
        ELSE IF(ips(1)==IP_Edge(1,IE) .AND. ITri_Edge(3,IE)==0)THEN
           ITri_Edge(3,IE) = IT
        ELSE IF(ips(1)==IP_Edge(2,IE) .AND. ITri_Edge(2,IE)==0)THEN
           ITri_Edge(2,IE) = IT
        ELSE IF(ips(1)==IP_Edge(2,IE) .AND. ITri_Edge(4,IE)==0)THEN
           ITri_Edge(4,IE) = IT
        ELSE
           WRITE(*,*)' Error--- what a case,  ips=',ips(1:2),' IE=',IE
           WRITE(*,*)'  IP_Edge(:,IE) =', IP_Edge(:,IE)
           WRITE(*,*)'  ITri_Edge(:,IE)=',ITri_Edge(:,IE)
           CALL Error_Stop ('Surf_IdenticalTri:: 1  ') 
        ENDIF
     ENDDO
  ENDDO
  numEdgesLinkCurve = Edge_Tree%numNets

  npair = 0
  nswap = 0
  nins  = 0

     !write(399,*) numEdgesOnCurve+1, numEdgesLinkCurve
     !write(399,*) 
  Loop_IE : DO IE = numEdgesOnCurve+1, numEdgesLinkCurve

     ITs(1:4) = ITri_Edge(1:4,IE)
     !write(399,*) 'triangles and surfaces'
     !write(399,*) its(1:4)
     !write(399,*) Surf%IP_Tri(5,its(1:4))

     IF(ITs(2)==0) CALL Error_Stop ('Surf_IdenticalTri:: 2  ') 
     IF(ITs(3)==0 .AND. ITs(4)==0) CYCLE Loop_IE
     IF(ITs(3)==0 .OR.  ITs(4)==0) CALL Error_Stop ('Surf_IdenticalTri:: 3  ') 


     IF(Surf%IP_Tri(5,ITs(1))/=Surf%IP_Tri(5,ITs(2)))   &
          CALL Error_Stop ('Surf_IdenticalTri:: 4  ') 
     IF(Surf%IP_Tri(5,ITs(3))/=Surf%IP_Tri(5,ITs(4)))   &
          CALL Error_Stop ('Surf_IdenticalTri:: 5  ') 
     IF(Surf%IP_Tri(5,ITs(1))==Surf%IP_Tri(5,ITs(3)))   &
          CALL Error_Stop ('Surf_IdenticalTri:: 6  ') 

     IT1 = ITs(1)
     IT2 = ITs(2)
     ips(1:2) = IP_Edge(:,IE)
     ips(3)   = SUM(Surf%IP_Tri(1:3,IT1)) - ips(1) - ips(2)
     ips(4)   = SUM(Surf%IP_Tri(1:3,IT2)) - ips(1) - ips(2)

     Loop_j : DO j = 3,4
        IT3 = ITs(j)
        DO i=1,3
           IF(Surf%IP_Tri(i,IT3)==ips(3) .OR. Surf%IP_Tri(i,IT3)==ips(4)) EXIT loop_j
        ENDDO
     ENDDO Loop_j

     !write(399,*) 'Points'
     !write(399,*) ips(1:4)
     IF(j>4) CYCLE Loop_IE   !--- no identical pair here

     npair = npair + 1

     !--- swap IT1 & IT2
     DO itry = 1,2

        IF(itry==2)THEN
           IT1    = ITs(3)
           IT2    = ITs(4)
           ips(3) = SUM(Surf%IP_Tri(1:3,IT1)) - ips(1) - ips(2)
           ips(4) = SUM(Surf%IP_Tri(1:3,IT2)) - ips(1) - ips(2)
        ENDIF
        IS = Surf%IP_Tri(5,IT1)

        !--- get back (u,v) from InterNodes
        DO i=1,4
           IP = ips(i)
           IF(IP<=numAllCurveNodes)THEN
              DO j = 1, InterNodes(IP)%numPatches
                 write(399,*)ip,is,InterNodes(IP)%Patches(j)
                 IF(InterNodes(IP)%Patches(j) == IS)THEN
                    rc(:,i) = InterNodes(IP)%Coord(:,j)
                    EXIT
                 ENDIF
              ENDDO
              IF(j>InterNodes(IP)%numPatches) CALL Error_Stop ('Surf_IdenticalTri:: 8  ') 
           ELSE
              IF(i<=2) CALL Error_Stop ('Surf_IdenticalTri:: 9  ') 
              rc(:,i) = Surf%Coord(:,IP)
           ENDIF
        ENDDO

        IF(Cross_Product_2D(rc(:,4),rc(:,1),rc(:,3)) <= 0.0) CYCLE   !-- can not swap
        IF(Cross_Product_2D(rc(:,3),rc(:,2),rc(:,4)) <= 0.0) CYCLE   !-- can not swap

        !---SAWP

        Surf%IP_Tri(1:3,IT1) = ips(2:4)
        Surf%IP_Tri(1:3,IT2) = (/ips(1), ips(4), ips(3)/)
        nswap = nswap + 1

        EXIT
     ENDDO

     IF(iTry<=2) CYCLE Loop_IE          !--- swap done

     !--- swap fail, then insert a point

     Surf%NB_Point = Surf%NB_Point +1
     IP            = Surf%NB_Point
     IF(IP>SIZE(Surf%Coord,2))THEN
        CALL allc_2Dpointer(Surf%Coord, 2, IP+Surf_allc_Increase)
     ENDIF
     IF(IP>SIZE(Surf%Posit,2))THEN
        CALL allc_2Dpointer(Surf%Posit, 3, IP+Surf_allc_Increase)
     ENDIF

     Surf%Coord(:,IP) = ( rc(:,1) + rc(:,2) ) / 2.d0
     CALL GetUVPointInfo0(IS,Surf%Coord(1,IP),Surf%Coord(2,IP),p0)
     Surf%Posit(:,IP) = p0(:)

     NT1         = Surf%NB_Tri + 1
     NT2         = Surf%NB_Tri + 2
     Surf%NB_Tri = NT2
     IF(NT2>SIZE(Surf%IP_Tri,2))THEN
        CALL allc_2Dpointer( Surf%IP_Tri,  5,NT2 + Surf_allc_increase )
     ENDIF

     Surf%IP_Tri(1:3,IT1) = (/ ips(1), IP,     ips(3) /)
     Surf%IP_Tri(1:3,IT2) = (/ ips(1), ips(4), IP     /)
     Surf%IP_Tri(1:3,NT1) = (/ IP,     ips(2), ips(3) /)
     Surf%IP_Tri(1:3,NT2) = (/ IP,     ips(4), ips(2) /)
     Surf%IP_Tri(4:5,NT1) = Surf%IP_Tri(4:5,IT1)
     Surf%IP_Tri(4:5,NT2) = Surf%IP_Tri(4:5,IT2)

     nins = nins + 1
  ENDDO Loop_IE

  DEALLOCATE(ITri_Edge, IP_Edge)
  CALL NodeNetTree_Clear(Edge_Tree)

  IF(npair>0)THEN
     WRITE(*, '(i4,a,i4,a,i4)') npair,' identical pairs found.  Swap: ', nswap, '  Split: ',nins
     WRITE(29,'(i4,a,i4,a,i4)') npair,' identical pairs found.  Swap: ', nswap, '  Split: ',nins
  ENDIF

END SUBROUTINE Surf_IdenticalTri


!*---------------------------------------------------------------------*
!*                                                                     *
!*   output surface mesh with cadfix format                            *
!*                                                                     *
!*---------------------------------------------------------------------*
SUBROUTINE output_cad( )
  USE control_Parameters
  USE surface_Parameters
  IMPLICIT NONE

  INTEGER, DIMENSION(:,:), POINTER :: IP_Tri
  INTEGER, DIMENSION(:  ), POINTER :: CurveNodeNum
  INTEGER, DIMENSION(:,:), POINTER :: CurveNodeList
  INTEGER :: nm, is, ie, i
  LOGICAL :: ex, TF

  IF(Curvature_Type==1)THEN
     INQUIRE(file = JobName(1:JobNameLength)//'.fbm', EXIST=ex)
     IF( .NOT. ex) RETURN
     
     CALL CADFIX_Init()
     CALL CADFIX_LoadGeom( JobName )
     CALL CADFIX_AnalyseModel  
  ENDIF
  
  CALL CADFIX_IsClientMode(TF)
  IF(TF) RETURN
  
  ALLOCATE(IP_Tri(4,Surf%NB_Tri))
  ALLOCATE(CurveNodeNum(NumCurves))
  nm = 0
  DO is = 1, NumCurves
     CurveNodeNum(is) = CurveNodes(is)%numNodes
     nm = MAX(nm,CurveNodeNum(is))
  ENDDO
  ALLOCATE(CurveNodeList(10000, nm))
  IF(NumCurves>10000) CALL Error_Stop ('output_cad :: NumCurves>10000  ') 
  DO is = 1, NumCurves
     DO i = 1, CurveNodes(is)%numNodes
        CurveNodeList(is, i) = CurveNodes(is)%Nodes(i)
     ENDDO
  ENDDO

  DO ie=1,Surf%NB_Tri
     IP_Tri(1:3,ie) = Surf%IP_Tri(1:3,ie)
     IP_Tri(4,ie) = Surf%IP_Tri(5,ie)
  ENDDO

  CALL CADFIX_Output(JobName, Surf%NB_Point, Surf%NB_Tri, IP_Tri,   &
       Surf%Posit, NumCurves, CurveNodeNum, CurveNodeList)

  DEALLOCATE(IP_Tri, CurveNodeNum, CurveNodeList)

  RETURN
END SUBROUTINE output_cad
 
