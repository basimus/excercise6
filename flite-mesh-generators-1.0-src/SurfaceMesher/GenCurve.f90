!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!*---------------------------------------------------------------------*
!>                                                                     
!!    Generates mesh points on every curves.
!<                                                                     
!*---------------------------------------------------------------------*
SUBROUTINE GenCurve( )
  USE occt_fortran !SPW
  USE control_Parameters
  USE surface_Parameters
  USE Spacing_Parameters
  IMPLICIT NONE
  INTEGER :: is, ip, i1, i2, i, numNodes
  INTEGER, DIMENSION(:), POINTER :: CurveNodeStart
  INTEGER, DIMENSION(:), POINTER :: map_Point
  REAL*4 :: tStart, tEnd

  ALLOCATE(CurveNodeStart(NumCurves+1))
  ALLOCATE(CurveNodes(NumCurves))
  ALLOCATE(CurveNodePar(NumCurves))
  
  CurveNodeStart(1) = 1
  DO is = 1,NumCurves
     IF(CurveMark(is)==0)THEN
        CurveNodeStart(is+1) = CurveNodeStart(is)
        CYCLE
     ENDIF
     
     CALL CPU_TIME(tStart)
     IF(Debug_Display>0)THEN
        WRITE(*, '(/,a,i4)') '  Discretizing segment: ',is
        WRITE(29,'(/,a,i4)') '  Discretizing segment: ',is
        FLUSH(6)
        FLUSH(29)
     ENDIF
     
     !   Generate curvature nodes and save them to the global surface.
     !   On the global surface, some nodes (at the ends of curvatures) are repeated.
          
    IF(Curvature_Type==1)THEN
       CALL GenCurvePoints(Curvt%Curves(is),  Surf, CurveNodePar(is), BGSpacing,TOLG)    
    ELSE
       CALL GenCurvePoints_CAD(is, Surf, BGSpacing)
    ENDIF
     CurveNodeStart(is+1) = Surf%NB_Point+1
     CALL CPU_TIME(tEnd)
     numNodes = CurveNodeStart(is+1) - CurveNodeStart(is)
     WRITE(*, '(/,a,i4,a,i4,a)') '  Finished discretizing segment: ',is,' with ',numNodes,' nodes '
     WRITE(29,'(/,a,i4,a,i4,a)') '  Finished discretizing segment: ',is,' with ',numNodes,' nodes '
    IF(Debug_Display>0)THEN
        WRITE(*, *) '  CPU Time: ',tEnd-tStart
        WRITE(29,*) '  CPU Time: ',tEnd-tStart
        FLUSH(6)
        FLUSH(29)
     ENDIF
        
 
  ENDDO

  ALLOCATE(map_Point(Surf%NB_Point))
  
  map_Point(1:Surf%NB_Point) = 1
  DO is = 1,NumCurves
     IF(CurveMark(is)==0) CYCLE
     map_Point(CurveNodeStart(is)    ) = 0
     map_Point(CurveNodeStart(is+1)-1) = 0
  ENDDO
  
  !   Check nodes by it position and remove ones from identical pairs.
  ! *** TOLG is the tolerance for identifying the points which are 
  !     common to two (or more edges). The check is performed in 
  !     terms of the 3D coordinates of the edges defined by user. 
  !     Therefore, it depends on the accuracy of the given definition 
  !     of the edges.
  CALL NodeIdentify(Surf%NB_Point, Surf%Posit, map_Point, TOLG)

  !  number curvature nodes by new global numbering
  DO is = 1,NumCurves
     IF(CurveMark(is)==0) CYCLE
     i1 = CurveNodeStart(is)
     i2 = CurveNodeStart(is+1)
     ALLOCATE(CurveNodes(is)%Nodes(i2-i1))
     do ip=i1,i2-1
        call  IntQueue_Push(CurveNodes(is), map_Point(ip))
     enddo
  ! DO i = 1,CurveNodes(is)%numNodes
  !      WRITE(1700,*) Surf%Posit(1:3,CurveNodes(is)%Nodes(i))
  ! END DO


  ENDDO
  
  DEALLOCATE(map_Point, CurveNodeStart)
 


 
  CALL  HighOrder_Curve()

  RETURN
END SUBROUTINE GenCurve

!>
!!   Check nodes by it position and remove ones from identical pairs.
!!
!!   @param[in]   map_Point   if map_Point(i)/=0  uncheck and remain node i.
!!                            if map_Point(i)==0  check node i and  
!!                               remove it if it match with another node with a smaller id.
!!   @param[out]  map_Point   map_Point(old) = new
!<

SUBROUTINE NodeIdentify(NB_Point, Posit, map_Point, TOLG)
  USE Geometry3D
  IMPLICIT NONE
  INTEGER, INTENT(inout) :: NB_Point
  REAL*8,  INTENT(inout) :: Posit(3,*)
  INTEGER, INTENT(inout) :: map_Point(*)
  REAL*8,  INTENT(in) :: TOLG
  INTEGER, DIMENSION(:), POINTER :: idx
  INTEGER :: i, n, j, nlist, nmove
  REAL*8  :: tt, dd

  tt = TOLG*TOLG
  ALLOCATE(idx(NB_Point))

  !--- check those point with map_Point(i)<=0
  
  nlist = 0
  DO i = 1, NB_Point
     IF(map_Point(i)/=0)THEN
        map_Point(i) = i
        CYCLE
     ENDIF

     DO n = 1,nlist
        dd = Geo3D_Distance_SQ(Posit(:,idx(n)),Posit(:,i))
        IF(dd<=tt)THEN
           !-- match a point in the list
           map_Point(i) = idx(n)
           EXIT
        ENDIF
     ENDDO

     IF(n>nlist)THEN
        !--- match no points in the list, add it to the list
        nlist = nlist+1
        idx(nlist) = i
        map_Point(i) = i
     ENDIF

  ENDDO

  DEALLOCATE(idx)

  !--- remove those point with map_Point(i)<i
  
  nmove = 0
  DO i = 1, NB_Point
     j = map_Point(i)
     IF(j<i)THEN
        !---remove this point
        nmove = nmove+1
        map_Point(i) = map_Point(j)
        CYCLE
     ENDIF
     IF(nmove>0)then
        !--- shift
        Posit(:,i-nmove) = Posit(:,i)
        map_Point(i)     = i-nmove
     ENDIF
  ENDDO

  NB_Point = NB_Point - nmove

END SUBROUTINE NodeIdentify


!*---------------------------------------------------------------------*
!>                                                                     
!!    Generates mesh points on one curve.
!!    @param[in,out]  Curve  : the geometry curve.
!!    @param[in,out]  Surf   : the surface mesh. New points added here.
!!    @param[in]      BGSpacing  :  the bachground spacing.
!<                                                                     
!*---------------------------------------------------------------------*
SUBROUTINE GenCurvePoints(Curve, Surf, CurveNodePar, BGSpacing, EPS)
  USE SurfaceMeshStorage
  USE SurfaceCurvature
  USE array_allocator
  USE SpacingStorage
  USE Queue
  IMPLICIT NONE
  TYPE(CurveType), INTENT(INOUT) :: Curve
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
  TYPE(SpacingStorageType), INTENT(IN)        :: BGSpacing
  TYPE (Real8QueueType), INTENT(OUT) :: CurveNodePar
  REAL*8, INTENT(IN) :: EPS
  !REAL*8,PARAMETER :: EPS  = 1.d-05 
  REAL*8, DIMENSION(:),   POINTER :: ssp, xne, sip
  REAL*8, DIMENSION(:,:), POINTER :: coef
  REAL*8  :: cr(9), u1, u2, xr, st, ane, d1, d2, av, anel, sc1, an, xin
  INTEGER :: ip, numEvenPt, kp, kp0, kp1, numPt, kr0, numVertices

  !--- calculates the length of each segment : ssp
  numVertices = Curve%numNodes

  ALLOCATE(ssp(numVertices), coef(5,numVertices))

  ssp(1)   = 0.d0
  DO ip=1,numVertices-1
     CALL CubicSegment_Length (Curve%Segm(ip),  coef(1:5,ip))
     ssp(ip+1) = ssp(ip) + Curve%Segm(ip)%Length
  ENDDO

  !--- distribute numEvenPt nodes evenly for sampling

  numEvenPt = INT( ssp(numVertices) / BGSpacing%MinSize + 0.5)
  numEvenPt = MAX(numEvenPt, 2) +1
  xr = ssp(numVertices)/(numEvenPt -1)

  ALLOCATE(sip(numEvenPt), xne(numEvenPt))

  !--- interpolate the spacings on sampling nodes.

  cr(1:3) = Curve%Posit(1:3,1)
  CALL SpacingStorage_GetDirectGridSize(BGSpacing, cr(1:3),Curve%Tangent(1:3,1),sip(1))
  cr(1:3) = Curve%Posit(1:3,numVertices)
  CALL SpacingStorage_GetDirectGridSize(BGSpacing, cr(1:3),Curve%Tangent(1:3,numVertices),sip(numEvenPt))

  kp0 = 1
  DO ip = 2, numEvenPt-1
     xin = (ip-1)*xr
     DO kp = kp0, numVertices-1
        IF(xin>ssp(kp) .AND. xin<=ssp(kp+1)) EXIT
        IF(kp==numVertices-1)THEN
           CALL Error_Stop ('GenCurvePoints :: wrong sampling points length  ')
        ENDIF
     ENDDO
     kp0 = kp
     st  = xin-ssp(kp)
     u1  = 0.d0
     CALL CubicSegment_Truncate(Curve%Segm(kp), coef(1:5,kp),u1,u2,st)
     CALL CubicSegment_Interpolate (1, Curve%Segm(kp), u2, cr(1:3), cr(4:6), cr(7:9))
     CALL SpacingStorage_GetDirectGridSize(BGSpacing, cr(1:3),cr(4:6),sip(ip))
  ENDDO

  !--- Integrates the distribution of spacings and calculates 
  !     the number of elements to be generated.
  !    xne : the summed distance in computational domain of those even-distributed points.  

  xne(1) = 0.d0
  DO ip = 1,numEvenPt-1
     d1 = sip(ip)
     d2 = sip(ip+1)
     av = (d2-d1)/xr
     IF(av<=EPS) THEN
        anel = 2.d0*xr/(d1+d2)
     ELSE
        anel = LOG(d2/d1)/av
     ENDIF
     xne(ip+1) = xne(ip) + anel
  ENDDO

  numPt = MAX(INT(xne(numEvenPt)+0.5),1)
  numPt = ((numPt+1)/2)*2
  numPt = numPt+1                       !--- always odd.
  sc1   = xne(numEvenPt) / (numPt-1)

  !--- computes the parametric and 3D coordinates of the new points.

  kr0 = Surf%NB_Point
  Surf%NB_Point = Surf%NB_Point + numPt
  IF(Surf%NB_Point>SIZE(Surf%Posit,2))THEN
     CALL allc_2Dpointer(Surf%Posit,3, Surf%NB_Point+Surf_allc_increase)
  ENDIF
  Surf%Posit(1:3,kr0+1)         = Curve%Posit(1:3,1)
  Surf%Posit(1:3,Surf%NB_Point) = Curve%Posit(1:3,numVertices)
  CALL Real8Queue_Push(CurveNodePar, 0.d0)

  kp0 = 1
  kp1 = 1
  DO ip = 2, numPt-1
     an = (ip-1)*sc1
     DO kp = kp0, numEvenPt-1
        IF(an>xne(kp) .AND. an<=xne(kp+1)) EXIT
        IF(kp==numEvenPt-1)THEN
           write(*,*) 1 , kp,numEvenPt-1
           write(*,*) an,xne(kp-1),xne(kp)
           CALL Error_Stop ('GenCurvePoints :: wrong new interval numbering  ')
        ENDIF
     ENDDO

     kp0 = kp
     d1  = sip(kp)
     d2  = sip(kp+1)
     av  = (d2-d1)/xr
     ane = an - xne(kp)
     IF(av<=EPS) THEN
        xin = 0.5d0*ane*(d1+d2) 
     ELSE
        xin = d1*(EXP(av*ane)-1.)/av 
     ENDIF
     xin  = (kp-1)*xr + xin

     DO kp=kp1,numVertices-1
        IF(xin>ssp(kp) .AND. xin<=ssp(kp+1)) EXIT
        IF(kp==numVertices-1)THEN
           write(*,*) 2 , kp,numVertices-1
           write(*,*) xin,ssp(kp-1),ssp(kp)
           CALL Error_Stop ('GenCurvePoints :: wrong length for new points  ')
        ENDIF
     ENDDO

     kp1 = kp
     st  = xin-ssp(kp)
     u1  = 0.d0
     CALL CubicSegment_Truncate(Curve%Segm(kp), coef(1:5,kp),u1,u2,st)
     CALL CubicSegment_Interpolate (0, Curve%Segm(kp), u2, cr(1:3), cr(4:6), cr(7:9))
     Surf%Posit(1:3,kr0+ip) = cr(1:3)
     CALL Real8Queue_Push(CurveNodePar, u2+kp-1.d0)
  ENDDO
  
  u2 = Curve%numNodes - 1.d0
  CALL Real8Queue_Push(CurveNodePar, u2)

  WRITE(29,'(2(15x,a,i7,a,e12.5,/),15x,a,i7)')    & 
       'No. of support points:   ',numVertices,', length:   ',ssp(numVertices),    & 
       'No. of sampling points:  ',numEvenPt,', interval: ',xr,    & 
       'No. of generated points: ',numPt
  IF(xne(numEvenPt)<0.2)THEN
     WRITE(29,'(5x,a)')     &
       'Warning--- The segment length is too short compared with local spacing.'
  ENDIF

  DEALLOCATE(ssp, xne, sip, coef)

  RETURN
END SUBROUTINE GenCurvePoints
!*---------------------------------------------------------------------*
!>                                                                     
!!    Generates mesh points on one Cadfix curve.
!!    @param[in]    CurveID  : the geometry curve ID.
!!    @param[in,out]  Surf   : the surface mesh. New points added here.
!!    @param[in]      BGSpacing  :  the bachground spacing.
!<                                                                     
!*---------------------------------------------------------------------*
SUBROUTINE GenCurvePoints_CAD(CurveID, Surf, BGSpacing)
  USE occt_fortran !SPW
  USE control_Parameters
  USE SurfaceMeshStorage
  USE array_allocator
  USE SpacingStorage
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: CurveID
  TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
  TYPE(SpacingStorageType), INTENT(IN)        :: BGSpacing
  REAL*8,PARAMETER :: EPS  = 1.d-05 
  !REAL*8 :: EPS
  REAL*8, DIMENSION(:),   POINTER :: ssp, xne, sip
  REAL*8  :: cr(9), xr, st, ane, d1, d2, av, anel, sc1, an, xin, p1(3), p2(3)
  INTEGER :: ip, numEvenPt, kp, kp0, numPt, kr0, numVertices,i

  POINTER (pVertices, vertices(7,*) )  !SPW
  REAL*8  :: vertices!(7,10000)
  REAL*8  :: spmin

  REAL*8  :: tMin, tMax, t, P(3), Scalar, D
  INTEGER :: j



  spmin = BGSpacing%MinSize

  IF (Curvature_Type==4) THEN
     !--- calculates the length of each segment : ssp
     spmin = HUGE(0.0d0)

     !--- to find the min spacing look at the spacing along this curve
     CALL OCCT_GetLineTBox(CurveID,tMin,tMax)
     DO j = 1,25
       t = tMin + (REAL(j-1)/(24.0d0))*(tMax-tMin)
       CALL OCCT_GetLineXYZFromT(CurveID,t,P)
      
       !CALL SpacingStorage_GetScale(BGSpacing, P, Scalar)
       !D = Scalar * BGSpacing%BasicSize

       CALL SpacingStorage_GetMinGridSize(BGSpacing, P,D)
      
       spmin = MIN(D,spmin)
       
       !rite(398,*) CurveID, spmin, BGSpacing%MinSize

     END DO
     

  
   ! WRITE(*,*) 'Calculating facets for curve ',CurveID
   ! WRITE(*,*) 'spmin = ',spmin
       CALL OCCT_CalculateCurveFacets( CurveID, numVertices,  pVertices, spmin )
  ELSE
       CALL CADFIX_CalculateCurveFacets( CurveID, numVertices,  pVertices, spmin )
  END IF
  


  ALLOCATE(ssp(numVertices))
  
  ssp(1) = 0.d0
  DO ip =  1, numVertices-1
     p1(1:3) = vertices(1:3,ip)
     p2(1:3) = vertices(1:3,ip+1)
     an = Geo3D_Distance(p1,p2)
     ssp(ip+1) = ssp(ip) + an
  ENDDO
  !RITE(398,*) 'Allocating vertices ',numVertices,ssp(numVertices)


  !SPW
  !DO i=1,numVertices
  !     WRITE(1500+CurveID,*) vertices(1:3,i)
!       WRITE(1500,*) vertices(1:3,i)
!  ENDDO


  numEvenPt = numVertices
 !numEvenPt = INT( ssp(numVertices) / BGSpacing%MinSize + 0.5)
 !numEvenPt = MAX(numEvenPt, 2) +1
  !RITE(398,*) 'NumEvenPt ',numEvenPt

  ALLOCATE(sip(numEvenPt), xne(numEvenPt))
  
  DO ip = 1, numVertices
     cr(1:6) = vertices(1:6,ip)
     CALL SpacingStorage_GetDirectGridSize(BGSpacing, cr(1:3),cr(4:6),sip(ip))
  END DO

  !--- Integrates the distribution of spacings and calculates 
  !     the number of elements to be generated.
  !    xne : the summed distance in computational domain of those supporting points.  

  xne(1) = 0.d0
  DO ip = 1, numEvenPt-1
     d1 = sip(ip)
     d2 = sip(ip+1)
     xr = (ssp(ip+1)-ssp(ip))
     av = (d2-d1)/xr
     IF(av<=EPS) THEN
        anel = 2.d0*xr/(d1+d2)
     ELSE
        anel = LOG(d2/d1)/av
     ENDIF
     xne(ip+1) = xne(ip) +anel
  ENDDO

  numPt = MAX(INT(xne(numEvenPt)+0.5),1)
  numPt = ((numPt+1)/2)*2
  numPt = numPt+1                       !--- always odd.
  sc1   = xne(numEvenPt)/(numPt-1)
  !RITE(398,*) 'sc1 ',numEvenPt,xne(numEvenPt),sc1

  !--- computes the parametric and 3D coordinates of the new points.

  kr0 = Surf%NB_Point
  Surf%NB_Point = Surf%NB_Point + numPt
  IF(Surf%NB_Point>SIZE(Surf%Posit,2))THEN
     CALL allc_2Dpointer(Surf%Posit,3, Surf%NB_Point+Surf_allc_increase)
  ENDIF
  Surf%Posit(1:3,kr0+1)         = vertices(1:3,1)
  Surf%Posit(1:3,Surf%NB_Point) = vertices(1:3,numVertices)

  kp0 = 1
  DO ip = 2, numPt-1
     an  = (ip-1)*sc1
     DO kp = kp0, numEvenPt-1
        IF(an>xne(kp) .AND. an<=xne(kp+1))  EXIT
        IF(kp==numEvenPt-1)THEN
                 WRITE(102,*) numPt
            DO i=1,numPt
                  WRITE(102,*) vertices(1:3,i)
            ENDDO
            write(*,*) 1 , kp,numEvenPt-1
            write(*,*) an,xne(kp-1),xne(kp)
            CALL Error_Stop ('GenCurvePoints_CAD:: wrong new interval numbering  ')
        ENDIF
     ENDDO

     kp0 = kp
     d1  = sip(kp)
     d2  = sip(kp+1)
     xr  = ssp(kp+1)-ssp(kp)
     av  = (d2-d1)/xr

     ane = an-xne(kp)
     IF(av<=EPS) THEN
        xin = 0.5d0 * ane * (d1+d2) 
     ELSE
        xin = d1*(EXP(av*ane)-1.)/av 
     ENDIF
     xin = ssp(kp) + xin
     
     st      = (xin-ssp(kp)) / (ssp(kp+1)-ssp(kp))
     cr(1:3) = vertices(1:3,kp) + (vertices(1:3,kp+1)-vertices(1:3,kp)) * st
     Surf%Posit(1:3,kr0+ip) = cr(1:3)
     IF(1==0)THEN
        WRITE(1600+CurveID, *) cr(1:3)
       WRITE(1600, *) cr(1:3)

     ENDIF

 
  ENDDO

  WRITE(29,'(2(15x,a,i7,a,e12.5,/),15x,a,i7)')                          &
       'No. of support points:   ',numVertices,', length:  ',ssp(numVertices),    &
       'No. of sampling points:  ',numEvenPt,', interval:',xr,     &
       'No. of generated points: ',numPt
  IF(xne(numEvenPt)<0.2)THEN
     WRITE(29,'(5x,a)')     &
       'Warning--- The segment length is too short compared with local spacing.'
  ENDIF

  CALL deallc( pVertices, 'pVertices in gensg1')
  DEALLOCATE(ssp, xne, sip)

  RETURN
END SUBROUTINE GenCurvePoints_CAD

