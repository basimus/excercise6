!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


#ifdef _MUMPS
       
       MODULE MUMPS_MODULE
         IMPLICIT NONE
       
         INCLUDE 'mpif.h'
         INCLUDE 'dmumps_struc.h'
         TYPE (DMUMPS_STRUC) :: MUMPS_MAT
         INTEGER :: theHost = 0
       
       CONTAINS
       
         !>
         !!   Nv   : the number of unknowns (size of the matrix) (global)
         !!   Nz_loc   : the number of entries (local)
         !<
         SUBROUTINE MUMPS_create_parr(Nv, Nz_loc, IRN_loc, JCN_loc, A_loc)
           IMPLICIT NONE
           INTEGER, INTENT(IN) :: Nv, Nz_loc
           INTEGER, DIMENSION(:), INTENT(IN) :: IRN_loc, JCN_loc
           REAL*8,  DIMENSION(:), INTENT(IN) :: A_loc
           INTEGER :: IERR, ICNTL18, mpi_size, mpi_rank
           INTEGER mpi_stat(MPI_STATUS_SIZE)
           INTEGER :: NZ_locs(10000), displs(10000)
       
           CALL MPI_INIT(IERR)
           
           CALL MPI_Comm_size (MPI_COMM_WORLD, mpi_size, IERR )
           CALL MPI_Comm_rank (MPI_COMM_WORLD, mpi_rank, IERR )

           MUMPS_MAT%COMM = MPI_COMM_WORLD
           MUMPS_MAT%JOB  = -1
           MUMPS_MAT%SYM  = 2
           MUMPS_MAT%PAR  = 0
           ICNTL18        = 3
       
           IF(mpi_size<=2)THEN
              MUMPS_MAT1%PAR  = 1
              ICNTL18         = 0
           ENDIF
       
           CALL DMUMPS(MUMPS_MAT)
       
           MUMPS_MAT%N  = Nv
       
           IF(ICNTL18==0)THEN
       
       
              CALL MPI_GATHER(NZ_loc, 1, MPI_INTEGER, NZ_locs, 1,    &
                   MPI_INTEGER, theHost, MPI_COMM_WORLD, IERR)
       
              IF(mpi_rank/=theHost)THEN
                 MUMPS_MAT%NZ = SUM(NZ_locs(1:mpi_size))
                 ALLOCATE(MUMPS_MAT%IRN_loc(MUMPS_MAT%NZ))
                 ALLOCATE(MUMPS_MAT%JCN_loc(MUMPS_MAT%NZ))
                 ALLOCATE(MUMPS_MAT%  A_loc(MUMPS_MAT%NZ))
       
                 displs(1) = 0
                 DO i = 2, mpi_size
                    displs(i) = displs(i-1) + NZ_locs(i-1)
                 ENDDO
              ENDIF
       
              CALL MPI_GATHERV(IRN_loc, NZ_loc, MPI_INTEGER, MUMPS_MAT%IRN_loc, NZ_locs, displs,   &
                   MPI_INTEGER, theHost, MPI_COMM_WORLD, IERR) 
              CALL MPI_GATHERV(JCN_loc, NZ_loc, MPI_INTEGER, MUMPS_MAT%JCN_loc, NZ_locs, displs,   &
                   MPI_INTEGER, theHost, MPI_COMM_WORLD, IERR) 
              CALL MPI_GATHERV(A_loc, NZ_loc, MPI_INTEGER, MUMPS_MAT%A_loc, NZ_locs, displs,   &
                   MPI_INTEGER, theHost, MPI_COMM_WORLD, IERR) 
       
           ELSE
           
              ALLOCATE( MUMPS_MAT%IRN_loc(NZ_loc) )
              ALLOCATE( MUMPS_MAT%JCN_loc(NZ_loc) )
              ALLOCATE( MUMPS_MAT%  A_loc(NZ_loc) )
       
              MUMPS_MAT%IRN_loc(1:Nz_loc) = IRN_loc(1:Nz_loc)
              MUMPS_MAT%JCN_loc(1:Nz_loc) = JCN_loc(1:Nz_loc)
              MUMPS_MAT%  A_loc(1:Nz_loc) =   A_loc(1:Nz_loc)
       
           ENDIF
       
           MUMPS_MAT%ICNTL(1)  = 1       !---- Only error message output
           MUMPS_MAT%ICNTL(3)  = 0       !---- No global information output
           MUMPS_MAT%ICNTL(18) = ICNTL18
       
           MUMPS_MAT%JOB = 4
           CALL DMUMPS(MUMPS_MAT)
           
           IF(mpi_Rank==theHost)THEN
            IF(MUMPS_MAT%INFO(1)>0)THEN
               WRITE(*,*)' Warning--- info for MUMPS_create:',MUMPS_MAT%INFO(1)
            ELSEIF(MUMPS_MAT%INFO(1)<0)THEN
               WRITE(*,*)' Error--- info for MUMPS_create:',MUMPS_MAT%INFO(1)
               STOP 'MUMPS_create::'
            ENDIF
           ENDIF
       
           IF(mpi_Rank==theHost)THEN
              IF(ICNTL18==0)THEN
                 DEALLOCATE(MUMPS_MAT%IRN_loc)
                 DEALLOCATE(MUMPS_MAT%JCN_loc)
                 DEALLOCATE(MUMPS_MAT%A_loc)
              ENDIF
       
              ALLOCATE( MUMPS_MAT%RHS(MUMPS_MAT%N) )
           ENDIF
       
           IF(ICNTL18==3)THEN
              DEALLOCATE(MUMPS_MAT%IRN_loc)
              DEALLOCATE(MUMPS_MAT%JCN_loc)
              DEALLOCATE(MUMPS_MAT%A_loc)
           ENDIF
       
         END SUBROUTINE MUMPS_create_parr
       
       
         !>
         !!   Nv   : the number of unknowns (length of the R)(significant only at theHost) 
         !<
         SUBROUTINE MUMPS_solver_parr(Nv, R)
           IMPLICIT NONE
           INTEGER, INTENT(IN) :: Nv
           REAL*8,  DIMENSION(:), INTENT(INOUT) :: R
           INTEGER :: IERR, mpi_rank
       
           CALL MPI_Comm_rank (MPI_COMM_WORLD, mpi_rank, IERR )
           
           IF(mpi_Rank==theHost)THEN
              MUMPS_MAT%RHS(1:NV) = R(1:Nv)
           ENDIF
       
           MUMPS_MAT%JOB = 3
           CALL DMUMPS(MUMPS_MAT)
       
       
       
         END SUBROUTINE MUMPS_solver_parr
       
         !>
         !!     Destroy the instance (deallocate internal data structures)               
         !<
         SUBROUTINE MUMPS_destroy_parr( )
           IMPLICIT NONE
           INTEGER :: IERR
           MUMPS_MAT%JOB = -2
           CALL DMUMPS(MUMPS_MAT)
           CALL MPI_FINALIZE(IERR)
         END SUBROUTINE MUMPS_destroy_parr
#endif       
