!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!    A module for running type LinkAssociation::LinkAssociationType.
!<
MODULE LinkAssociation
  USE array_allocator
  USE Queue

  IMPLICIT NONE

  INTEGER, PARAMETER :: LinkAssociation_ArrayBeginLength = 36
  INTEGER, PARAMETER :: LinkAssociation_nallc_increase   = 10000

  !>
  !!    A structure storing the links between Joints and its adjacent targets.
  !!    Here a target can be a joint, a edge, a triangle, a tetrahedron or else.
  !!    The type of element is controled by Norder used in building, searching,
  !!    and so on. Keep this Norder unchanged throughout.
  !<
  TYPE LinkAssociationType
     INTEGER :: numJoints = 0              !<   the size of JointLinks
     TYPE (IntQueueType), DIMENSION(:), POINTER :: JointLinks => null()
  END TYPE LinkAssociationType

CONTAINS

  !>
  !!   Check if the association has been allocated.
  !<
  FUNCTION LinkAssociation_Exist(LinkAsso) RESULT(YesNo)
    IMPLICIT NONE
    LOGICAL :: YesNo
    TYPE (LinkAssociationType) LinkAsso

    IF(LinkAsso%numJoints>0)THEN
       YesNo = .TRUE.
    ELSE
       YesNo = .FALSE.
    ENDIF
    
  END FUNCTION LinkAssociation_Exist

  !>
  !!   Return the target from N-th link of Joint IP.
  !<
  FUNCTION LinkAssociation_Target(IP,N,LinkAsso) RESULT(Cell)
    IMPLICIT NONE
    INTEGER :: Cell, IP, N
    TYPE (LinkAssociationType) LinkAsso

    IF(IP>LinkAsso%numJoints)THEN
       Cell = 0
    ELSE IF(N>LinkAsso%JointLinks(IP)%numNodes)THEN
       Cell = 0
    ELSE
       Cell = LinkAsso%JointLinks(IP)%Nodes(N)
    ENDIF
    
  END FUNCTION LinkAssociation_Target

  !>
  !!   Get the list of targets which are associated with a joint.
  !!   @param[in]  IP            the ID of the joint.
  !!   @param[in]  LinkAsso      the association.
  !!   @param[out] List_Length   the number of elements associated with the joint.
  !!   @param[out] JointLinks    the list of elements associated with the joint.
  !!
  !!   Reminder:  JointLinks is an interger array pointer. 
  !!             Modify it anywhere may change the structure of the association. 
  !!             Vice versa. i.e.    \n
  !!       List_Length =  LinkAsso\%JointLinks(IP)\%numNodes           \n
  !!       JointLinks  => LinkAsso\%JointLinks(IP)\%Nodes
  !<
  SUBROUTINE LinkAssociation_List(IP, List_Length, JointLinks, LinkAsso)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: IP
    INTEGER :: List_Length
    INTEGER, DIMENSION(:), POINTER :: JointLinks
    TYPE (LinkAssociationType), INTENT(IN) :: LinkAsso
    
    IF(IP>LinkAsso%numJoints)THEN
       List_Length = 0
    ELSE
       List_Length =  LinkAsso%JointLinks(IP)%numNodes
       JointLinks  => LinkAsso%JointLinks(IP)%Nodes
    ENDIF
    
  END SUBROUTINE LinkAssociation_List


  !>
  !!   Allocate memory for the association. 
  !!   @param[in]  NB_Joint   the number of Joints.
  !!   @param[out] LinkAsso  the association.
  !!
  !!   Reminder:  This routine is contained in SUBROUTINE LinkAssociation_Build(), 
  !!              and calling this subroutine WILL destroy the exsiting association.
  !!              However, if we build the association by calling 
  !!              subroutine LinkAssociation_Add() repeatly, it is better to 
  !!              allocate memory  in advace for the cpu interest.
  !<
  SUBROUTINE LinkAssociation_Allocate(NB_Joint, LinkAsso)
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  NB_Joint
    TYPE (LinkAssociationType), INTENT(INOUT) :: LinkAsso
    
    CALL LinkAssociation_Clear(LinkAsso)
    LinkAsso%numJoints = NB_Joint
    ALLOCATE (LinkAsso%JointLinks(NB_Joint))
    
  END SUBROUTINE LinkAssociation_Allocate

  !>
  !!   Build the association.
  !!   @param[in]  Norder     the integer controlling the shape of element.
  !!   @param[in]  NB_Cell    the number of elements
  !!   @param[in]  IP_Cell(Norder,*)   the connectivity of elements
  !!   @param[in]  NB_Joint   the number of Joints.
  !!   @param[in]  LinkAsso  the association.
  !!   @param[out] LinkAsso  the revised association.
  !<
  SUBROUTINE LinkAssociation_Build(Norder, NB_Cell, IP_Cell, NB_Joint, LinkAsso)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Norder, NB_Cell, IP_Cell(Norder,*), NB_Joint
    TYPE (LinkAssociationType), INTENT(INOUT) :: LinkAsso
    INTEGER :: Cell,IP,i

    CALL LinkAssociation_Allocate(NB_Joint, LinkAsso)
    DO Cell = 1,NB_Cell
       DO i = 1,Norder
          IP = IP_Cell(i,Cell)
          CALL IntQueue_Push36(LinkAsso%JointLinks(IP), Cell) 
       ENDDO
    ENDDO

  END SUBROUTINE LinkAssociation_Build

  !>                               
  !!    Private function : copy LinkAssociationType TT1 to TT2.   \n                                     
  !!    Reminder: TT2 can be allocated in advance.    
  !!              If TT2 has a smaller size than TT1, then TT2 will be reallocated.
  !<
  SUBROUTINE LinkAssociation_Copy(TT1, TT2)
    IMPLICIT NONE
    TYPE (LinkAssociationType), INTENT(IN) :: TT1
    TYPE (LinkAssociationType), INTENT(INOUT) :: TT2
    INTEGER :: IP

    IF(TT2%numJoints<TT1%numJoints)THEN
       CALL LinkAssociation_Allocate(TT1%numJoints, TT2)
    ENDIF
    DO IP = 1, TT1%numJoints
       CALL IntQueue_Copy( TT1%JointLinks(IP), TT2%JointLinks(IP) )
    ENDDO

  END SUBROUTINE LinkAssociation_Copy

  !>
  !!   An old name of SUBROUTINE LinkAssociation_AddCell()
  !<
  SUBROUTINE LinkAssociation_Add(Norder, Cell, IPs, LinkAsso)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Norder, Cell, IPs(Norder)
    TYPE (LinkAssociationType), INTENT(INOUT) :: LinkAsso
    CALL LinkAssociation_AddCell(Norder, Cell, IPs, LinkAsso)
  END SUBROUTINE LinkAssociation_Add
  
  !>
  !!   Add a new element to the association.
  !!   @param[in]  Norder     the length of input array IPs.
  !!   @param[in]  Cell       the ID of the new element.
  !!   @param[in]  IPs(Norder) the connectivity of the new element.
  !!   @param[in]  LinkAsso  the association.
  !!   @param[out] LinkAsso  the revised association.
  !<
  SUBROUTINE LinkAssociation_AddCell(Norder, Cell, IPs, LinkAsso)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Norder, Cell, IPs(Norder)
    TYPE (LinkAssociationType), INTENT(INOUT) :: LinkAsso
    INTEGER :: IP,i,sizeNew
    TYPE (LinkAssociationType) :: LinkAsso_temp

    DO i = 1,Norder
       IP = IPs(i)
       IF(LinkAsso%numJoints==0)THEN
          sizeNew = IP + LinkAssociation_nallc_increase
          CALL LinkAssociation_Allocate(sizeNew, LinkAsso)
       ELSE IF(IP>LinkAsso%numJoints)THEN
          sizeNew = IP + LinkAssociation_nallc_increase
          CALL LinkAssociation_Copy( LinkAsso, LinkAsso_temp)
          CALL LinkAssociation_Allocate(sizeNew, LinkAsso)
          CALL LinkAssociation_Copy( LinkAsso_temp, LinkAsso)
          CALL LinkAssociation_Clear(LinkAsso_temp)
       ENDIF

       IF(IntQueue_Contain(LinkAsso%JointLinks(IP), Cell)) CYCLE
       CALL IntQueue_Push36(LinkAsso%JointLinks(IP), Cell) 
    ENDDO

  END SUBROUTINE LinkAssociation_AddCell

  !>
  !!   Add a new link to a point.
  !!   If the link already exists, do nothing.
  !!   @param[in]  iLink     the ID of the new link.
  !!   @param[in]  IP        the ID of the node.
  !!   @param[in]  LinkAsso  the association.
  !!   @param[out] LinkAsso  the revised association.
  !<
  SUBROUTINE LinkAssociation_AddLink(iLink, IP, LinkAsso)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: iLink, IP
    TYPE (LinkAssociationType), INTENT(INOUT) :: LinkAsso
    INTEGER :: sizeNew
    TYPE (LinkAssociationType) :: LinkAsso_temp

       IF(LinkAsso%numJoints==0)THEN
          sizeNew = IP + LinkAssociation_nallc_increase
          CALL LinkAssociation_Allocate(sizeNew, LinkAsso)
       ELSE IF(IP>LinkAsso%numJoints)THEN
          sizeNew = IP + LinkAssociation_nallc_increase
          CALL LinkAssociation_Copy( LinkAsso, LinkAsso_temp)
          CALL LinkAssociation_Allocate(sizeNew, LinkAsso)
          CALL LinkAssociation_Copy( LinkAsso_temp, LinkAsso)
          CALL LinkAssociation_Clear(LinkAsso_temp)
       ENDIF

       IF(IntQueue_Contain(LinkAsso%JointLinks(IP), iLink)) RETURN
       CALL IntQueue_Push36(LinkAsso%JointLinks(IP), iLink) 

  END SUBROUTINE LinkAssociation_AddLink

  !>
  !!   Remove an element from the association.
  !!   @param[in]  Norder     the length of input array IPs.
  !!   @param[in]  Cell       the ID of the element being removed.
  !!   @param[in]  IPs(Norder) the connectivity of the element being removed.
  !!   @param[in]  LinkAsso  the association.
  !!   @param[out] LinkAsso  the revised association.
  !<
  SUBROUTINE LinkAssociation_Remove(Norder, Cell, IPs, LinkAsso)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Norder, Cell, IPs(Norder)
    TYPE (LinkAssociationType), INTENT(INOUT) :: LinkAsso
    INTEGER :: IP,i, Isucc

    DO i = 1,Norder
       IP = IPs(i)
       CALL IntQueue_Remove(LinkAsso%JointLinks(IP), Cell, ISucc)   
       IF(Isucc==0)THEN
          WRITE(*,*)'Error--- : Cell is not in the list'
          WRITE(*,*)'Norder, IP, Cell=',Norder, IP, Cell
          CALL IntQueue_Write(LinkAsso%JointLinks(IP), 6)
          CALL Error_STOP ( '--- LinkAssociation_Remove')
       ENDIF
    ENDDO

  END SUBROUTINE LinkAssociation_Remove

  !>
  !!   Check the structure of the association
  !!   @param[in]  Norder     the integer controlling the shape of element.
  !!   @param[in]  LinkAsso  the association.
  !!   @param[out] Isucc      index for success.                               \n
  !!                = 1   successful.                                          \n
  !!                = -1  a joint is associated to a none-cell.                \n
  !!                = -2  a joint is associated to a cell more than once.      \n
  !!                = -3  more than Norder Joints are associated to one cell.  \n
  !!                = -4  less than Norder Joints are associated to one cell
  !<
  SUBROUTINE LinkAssociation_Check(Norder, LinkAsso, Isucc)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Norder
    TYPE (LinkAssociationType), INTENT(IN) :: LinkAsso
    INTEGER, INTENT(OUT) :: Isucc
    INTEGER :: i, n, ip, it, NB_Cell, nallc, nallc1
    INTEGER, DIMENSION(:,:),  POINTER :: IP_Cell

    nallc = 10 * LinkAssociation_nallc_increase
    CALL allc_2Dpointer(IP_Cell, Norder, nallc, 'LinkAsso_Check')
    IP_Cell(:,:) = 0
    NB_Cell = 0

    DO ip = 1,LinkAsso%numJoints
       DO i = 1,LinkAsso%JointLinks(ip)%numNodes
          it = LinkAsso%JointLinks(ip)%Nodes(i)
          IF(it<=0)THEN
             !--associated with a negative ID
             Isucc = -1
             RETURN
          ENDIF

          NB_Cell = MAX(NB_Cell, it)
          IF(it>nallc)THEN
             nallc1 = nallc
             nallc  = it + LinkAssociation_nallc_increase
             CALL allc_2Dpointer(IP_Cell, Norder, nallc, 'LinkAsso_Check')
             IP_Cell(:,nallc1+1:nallc) = 0
          ENDIF
          DO n=1,Norder
             IF(IP_Cell(n,it)==ip)THEN
                !---'multi associated'
                Isucc = -2
                RETURN
             ENDIF
             IF(IP_Cell(n,it)==0) EXIT
          ENDDO
          IF(n>Norder)THEN
             !--- exceed Norder
             Isucc = -3
             RETURN
          ENDIF
          IP_Cell(n,it) = ip
       ENDDO
    ENDDO

    DO it=1,NB_Cell
       IF(IP_Cell(1,it)==0) CYCLE
       IF(IP_Cell(Norder,it)==0)THEN
          !--- 'deficit associated'
          Isucc = -4
          RETURN
       ENDIF
    ENDDO

    Isucc = 1
    DEALLOCATE(IP_Cell)

    RETURN
  END SUBROUTINE LinkAssociation_Check

  !>
  !!   Colour the nodes by the node-node association (rather than node-net).
  !!   The colours of two nodes are the same if they have a link.  \n
  !!   Reminder: 1. Make sure the links are always bi-direction.
  !!             2. If independent nodes (those nodes without anylink to others)
  !!                need to be coloured, build self-links for them.
  !!                
  !!   @param[in]  LinkAsso  the association (node to node).
  !!   @param[in]  NB_node   the number of nodes.
  !!   @param[in]  IPstart   the start node to colour.
  !!   @param[out] Mark      The colour of each node.  
  !!   @param[out] nb_col    The number of colours 
  !<
  SUBROUTINE LinkAssociation_Colour(LinkAsso, NB_node, IPstart, Mark, nb_col)
    IMPLICIT NONE
    TYPE (LinkAssociationType), INTENT(IN) :: LinkAsso
    INTEGER, INTENT(IN) :: NB_node, IPstart
    INTEGER, INTENT(INOUT) :: Mark(*), nb_col
    INTEGER :: i, ip, ip1
    Type(IntQueueType) :: qu

    Mark(1:NB_node) = 0
    ALLOCATE(qu%Nodes(NB_node))

    nb_col = 1
    ip     = IPstart
    DO 
   
       Mark(ip) = nb_col
       DO i   = 1,LinkAsso%JointLinks(ip)%numNodes
          ip1 =   LinkAsso%JointLinks(ip)%Nodes(i)
          IF(Mark(ip1)==0)THEN          
             Mark(ip1) = nb_col
             CALL IntQueue_Push(qu, ip1)
          ENDIF
       ENDDO
       
       IF(qu%numNodes>0)THEN
          ip = qu%Nodes(qu%numNodes)
          qu%numNodes = qu%numNodes -1
       ELSE
          DO ip = 1, NB_node
             IF(LinkAsso%JointLinks(ip)%numNodes==0) CYCLE
             IF(Mark(ip)==0) EXIT
          ENDDO
          IF(ip>NB_node) EXIT
          nb_col = nb_col + 1
       ENDIF
       
    ENDDO
 
    CALL IntQueue_Clear(qu)

    RETURN
  END SUBROUTINE LinkAssociation_Colour

  !>
  !!   Delete the association and release memory.
  !<
  SUBROUTINE LinkAssociation_Clear(LinkAsso)
    IMPLICIT NONE
    TYPE (LinkAssociationType), INTENT(INOUT) :: LinkAsso
    TYPE (IntQueueType), POINTER :: pPoint
    INTEGER :: IP
    IF(ASSOCIATED(LinkAsso%JointLinks))THEN 
       DO  IP = 1, SIZE(LinkAsso%JointLinks)
          CALL IntQueue_Clear( LinkAsso%JointLinks(IP) )
       ENDDO
       DEALLOCATE(LinkAsso%JointLinks)
    ENDIF
    LinkAsso%numJoints = 0
    RETURN
  END SUBROUTINE LinkAssociation_Clear


END MODULE LinkAssociation

