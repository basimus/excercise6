!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!    An ADT structure used for a list of points.
!<
MODULE PtADTreeModule
  USE Geometry3D
  USE array_allocator
  USE BiTreeNodeModule
  IMPLICIT NONE

  TYPE :: PtADTreeParameter
     INTEGER :: maxLevel     = 360
     INTEGER :: numDimension = 6
     REAL*8  :: toler        = 1.d-02
     REAL*8, DIMENSION(0:360) :: shiftAmount
     LOGICAL :: ready = .FALSE.
  END TYPE PtADTreeParameter

  TYPE (PtADTreeParameter),SAVE :: PtADTreePar

  !>
  !!   The data structure of the tree.
  !<
  TYPE :: PtADTreeType
     CHARACTER ( LEN = 60 ) :: theName = ' '
     REAL*8  :: Origin(3), ScaleFactor(3), HalfSize(3)
     TYPE (BiTreeNodeType), POINTER :: Root => null()
     INTEGER :: numPoints = 0
     REAL*8,  DIMENSION(:,:),  POINTER :: Posit => null()       !--(3,:)
  END TYPE PtADTreeType

CONTAINS

  !>
  !!  Prepare the common Parameter.
  !<
  SUBROUTINE PtADTreeParameter_Prepare( )
    IMPLICIT NONE
    REAL*8  :: xc
    INTEGER :: i, ip
    IF(PtADTreePar%ready) RETURN
    xc = 0.5d0
    DO i = 0, PtADTreePar%maxLevel / PtADTreePar%numDimension - 1
       ip = i * PtADTreePar%numDimension
       PtADTreePar%shiftAmount(ip : ip+PtADTreePar%numDimension-1) = xc
       xc = 0.5d0 * xc
    ENDDO
    PtADTreePar%ready = .TRUE.
  END SUBROUTINE PtADTreeParameter_Prepare

  FUNCTION PtADTree_getBox(PtADTree, IP) RESULT (box)
    IMPLICIT NONE    
    TYPE (PtADTreeType) :: PtADTree
    INTEGER, INTENT(IN) :: IP
    INTEGER :: i
    REAL*8  :: box(6)
    DO i=1,3
       box(i)   = PtADTree%Posit(i,IP) - PtADTree%HalfSize(i)
       box(i+3) = PtADTree%Posit(i,IP) + PtADTree%HalfSize(i)
    ENDDO
  END FUNCTION PtADTree_getBox

  FUNCTION PtADTree_normalise(PtADTree, Pt) RESULT (scale_Pt)
    IMPLICIT NONE
    TYPE (PtADTreeType) :: PtADTree
    REAL*8  :: Pt(3), scale_Pt(3)
    scale_Pt(1:3) = ( Pt(1:3) - PtADTree%Origin(1:3) )    &
         * PtADTree%ScaleFactor(1:3)
  END FUNCTION PtADTree_normalise

  !>
  !!   Set a name for this tree (optional function).
  !<
  SUBROUTINE PtADTree_SetName(PtADTree, theName)
    IMPLICIT NONE
    TYPE (PtADTreeType), INTENT(INOUT) :: PtADTree
    CHARACTER ( LEN = * ), INTENT(IN) :: theName
    PtADTree%theName(:) = ' '
    PtADTree%theName = theName
  END SUBROUTINE PtADTree_SetName

  !>
  !!   Set domain range of the tree.
  !!   @param[in]  pMin       the minimum positions of the domain.
  !!   @param[in]  pMax       the maximum positions of the domain.
  !!   @param[in]  HalfSize   a radius. A smaller value brings a higher efficience 
  !!                                    but smaller number of points will be found.
  !!   @param[out] PtADTree   the tree.
  !<
  SUBROUTINE PtADTree_SetTreeDomain(PtADTree, pMin, pMax, HalfSize)
    IMPLICIT NONE
    TYPE (PtADTreeType), INTENT(INOUT) :: PtADTree
    REAL*8, INTENT(IN)  :: pMin(3), pMax(3), HalfSize(3)
    REAL*8  :: pMin1(3), pMax1(3), xc(3)

    CALL PtADTreeParameter_Prepare( )
    xc(:)   = ( pMax(:) - pMin(:) ) * PtADTreePar%toler
    pMin1(:) = pMin(:) - xc(:)
    pMax1(:) = pMax(:) + xc(:)
    PtADTree%Origin(1:3)      = pMin1(1:3)
    PtADTree%ScaleFactor(1:3) = 1.d0 / (pMax1(1:3)-pMin1(1:3))
    PtADTree%HalfSize(1:3)    = HalfSize(1:3)
    ALLOCATE(PtADTree%Root)

    RETURN
  END SUBROUTINE PtADTree_SetTreeDomain

  !>
  !!   Build the tree.
  !!   @param[in]  NB_Point   the number of points to be stored in the tree.
  !!   @param[in]  Posit      the position of each point.
  !!   @param[in]  HalfSize   a radius. A smaller value brings a higher efficience 
  !!                                    but smaller number of points will be found.
  !!   @param[out] PtADTree   the tree.
  !!
  !!   This routine contains calling SUBROUTINE PtADTree_SetTreeDomain()
  !<
  SUBROUTINE PtADTree_SetTree(PtADTree, Posit, NB_Point, HalfSize)
    IMPLICIT NONE
    TYPE (PtADTreeType), INTENT(INOUT) :: PtADTree
    REAL*8, INTENT(IN)  :: Posit(3,*), HalfSize(3)
    INTEGER, INTENT(IN) :: NB_Point
    REAL*8  :: pMin(3), pMax(3), xc
    INTEGER :: ip

    !--- set domain and scalar
    pMin(1:3) = Posit(:,1)
    pMax(1:3) = Posit(:,1)
    DO ip=2,NB_Point
       pMin(:) = MIN(pMin(:),Posit(:,ip))
       pMax(:) = MAX(pMax(:),Posit(:,ip))
    ENDDO
    pMin(:) = pMin(:) - HalfSize(:)
    pMax(:) = pMax(:) + HalfSize(:)

    CALL PtADTree_SetTreeDomain(PtADTree, pMin, pMax, HalfSize)

    !--- set the tree
    PtADTree%numPoints = 1.1* NB_Point + 1000
    CALL allc_real8_2Dpointer( PtADTree%Posit,  3, PtADTree%numPoints, 'PtADTree') 
    DO ip=1,NB_Point
       CALL PtADTree_AddNode(PtADTree,IP,Posit(:,ip))
    ENDDO

    RETURN
  END SUBROUTINE PtADTree_SetTree

  !>
  !!   Add a new point to the tree.
  !!   @param[in]  IP      the node ID of the new point.
  !!   @param[in]  Pt      the position of the new point.
  !!   @param[in,out] PtADTree   the tree.
  !<
  SUBROUTINE PtADTree_AddNode(PtADTree,IP,Pt)
    IMPLICIT NONE

    TYPE (PtADTreeType), INTENT(INOUT) :: PtADTree
    INTEGER, INTENT(IN) :: IP
    REAL*8, INTENT(IN)  :: Pt(3)
    REAL*8  :: x(6)
    INTEGER :: id, lev
    TYPE (BiTreeNodeType), POINTER :: node

    IF(PtADTree%numPoints<IP)THEN
       PtADTree%numPoints = 1.1* IP + 1000
       CALL allc_real8_2Dpointer( PtADTree%Posit,  3, PtADTree%numPoints, 'PtADTree') 
    ENDIF
    PtADTree%Posit(:,IP) = Pt

    x(1:6) = PtADTree_getBox(PtADTree, IP)
    x(1:3) = PtADTree_normalise(PtADTree, x(1:3))
    x(4:6) = PtADTree_normalise(PtADTree, x(4:6))

    DO id = 1,PtADTreePar%numDimension
       IF(x(id)<=0.d0 .OR. x(id)>=1.d0)THEN
          WRITE(*,*)'New Point IP',IP,' is out of PtADTree::',  &
               TRIM(PtADTree%theName),' domain'
          CALL Error_STOP ( '--- PtADTree_AddNode')
       ENDIF
    ENDDO

    node => PtADTree%Root
    lev  =  0
    DO WHILE( .NOT. BiTreeNodeType_isEmpty(node) )
       !---   walk way down the tree
       lev   = lev + 1
       id    = MOD(lev,PtADTreePar%numDimension)+1
       x(id) = 2.d0*x(id)
       IF(x(id)<1.d0) THEN
          node => node%branchLeft
       ELSE
          node => node%branchRight
          x(id) = x(id)-1.d0
       ENDIF
    ENDDO

    !---  add a new tree-node
    CALL BiTreeNodeType_Assign(node,IP,lev)

    IF(lev>PtADTreePar%maxLevel)THEN
       WRITE(*,*)'Error---  maxLevel is set too small:', PtADTreePar%maxLevel
       CALL Error_STOP ( '--- PtADTree_AddNode')
    ENDIF

    RETURN
  END SUBROUTINE PtADTree_AddNode

  !>
  !!   Add a new point to the tree if it is not indentical to one in the tree.
  !!   @param[in]  IP      the node ID of the new point.
  !!   @param[in]  Pt      the position of the new point.
  !!   @param[in,out] PtADTree   the tree.
  !!   @param[out] ipMarch  = IP, then this is a new node, and the tree is updated.  \n
  !!                        /=IP, then ipMarch is the identical node already in the tree,
  !!                              and the tree is unchange.
  !<
  SUBROUTINE PtADTree_AddUniqueNode(PtADTree,IP,Pt, ipMarch)
    IMPLICIT NONE

    TYPE (PtADTreeType), INTENT(INOUT) :: PtADTree
    INTEGER, INTENT(IN)  :: IP
    REAL*8,  INTENT(IN)  :: Pt(3)
    INTEGER, INTENT(OUT) :: ipMarch
    REAL*8  :: x(6)
    INTEGER :: id, lev
    TYPE (BiTreeNodeType), POINTER :: node

    IF(PtADTree%numPoints<IP)THEN
       PtADTree%numPoints = 1.1* IP + 1000
       CALL allc_real8_2Dpointer( PtADTree%Posit,  3, PtADTree%numPoints, 'PtADTree') 
    ENDIF
    PtADTree%Posit(:,IP) = Pt

    x(1:6) = PtADTree_getBox(PtADTree, IP)
    x(1:3) = PtADTree_normalise(PtADTree, x(1:3))
    x(4:6) = PtADTree_normalise(PtADTree, x(4:6))

    DO id = 1,PtADTreePar%numDimension
       IF(x(id)<=0.d0 .OR. x(id)>=1.d0)THEN
          WRITE(*,*)'New Point IP',IP,' is out of PtADTree::',  &
               TRIM(PtADTree%theName),' domain'
          CALL Error_STOP ( '--- PtADTree_AddNode')
       ENDIF
    ENDDO

    node => PtADTree%Root
    lev  =  0
    DO WHILE( .NOT. BiTreeNodeType_isEmpty(node) )
       ipMarch = node%ID
       IF(  PtADTree%Posit(1,ipMarch)==Pt(1) .AND.   &
            PtADTree%Posit(2,ipMarch)==Pt(2) .AND.   &
            PtADTree%Posit(3,ipMarch)==Pt(3)  ) THEN
          !--- found an identical node, return
          RETURN
       ENDIF

       !---   walk way down the tree
       lev   = lev + 1
       id    = MOD(lev,PtADTreePar%numDimension)+1
       x(id) = 2.d0*x(id)
       IF(x(id)<1.d0) THEN
          node => node%branchLeft
       ELSE
          node => node%branchRight
          x(id) = x(id)-1.d0
       ENDIF
    ENDDO

    !---  add a new tree-node
    CALL BiTreeNodeType_Assign(node,IP,lev)
    ipMarch = IP

    IF(lev>PtADTreePar%maxLevel)THEN
       WRITE(*,*)'Error---  maxLevel is set too small:', PtADTreePar%maxLevel
       CALL Error_STOP ( '--- PtADTree_AddNode')
    ENDIF

    RETURN
  END SUBROUTINE PtADTree_AddUniqueNode

  !>
  !!   Remove a node (point) from the tree.
  !!   @param[in]  IP     the ID of the removing point.
  !!                      If this point does not exist in the tree,
  !!                      the program will be terminated.
  !!   @param[in,out] PtADTree   the tree.
  !<
  SUBROUTINE PtADTree_RemoveNode(PtADTree, IP)
    IMPLICIT NONE

    TYPE (PtADTreeType), INTENT(INOUT) :: PtADTree
    INTEGER, INTENT(IN) :: IP
    REAL*8  :: x(6)
    INTEGER :: id, lev
    TYPE (BiTreeNodeType), POINTER :: node, node0

    x(1:6) = PtADTree_getBox(PtADTree, IP)
    x(1:3) = PtADTree_normalise(PtADTree, x(1:3))
    x(4:6) = PtADTree_normalise(PtADTree, x(4:6))

    !--- find the node to delete

    node => PtADTree%Root
    DO WHILE( .NOT. BiTreeNodeType_isEmpty(node) )
       IF( IP==node%ID ) THEN
          ! ---  found node to be deleted
          EXIT
       ELSE
          !---   walk way down the tree
          lev   = node%Level + 1
          id    = MOD(lev,PtADTreePar%numDimension)+1
          x(id) = 2.d0*x(id)
          IF(x(id)<1.d0) THEN
             node => node%branchLeft
          ELSE
             node => node%branchRight
             x(id) = x(id)-1.d0
          ENDIF
       ENDIF
    ENDDO

    IF( BiTreeNodeType_isEmpty(node) )THEN
       WRITE(*,*)'Error---  can not find this Node, IP=', IP
       CALL Error_STOP ( '--- PtADTree_RemoveNode')
    ENDIF

    !--- Find a node on the tip, 
    !    copy information of this node to the node being deleted,
    !    and delete the node on tip.

    node0 => node
    DO
       IF( .NOT. BiTreeNodeType_isEmpty(node%branchLeft))THEN
          node => node%branchLeft
       ELSE IF( .NOT. BiTreeNodeType_isEmpty(node%branchRight))THEN
          node => node%branchRight
       ELSE 
          EXIT
       ENDIF
    ENDDO

    node0%ID = node%ID
    CALL BiTreeNodeType_deAssign(node)

    RETURN
  END SUBROUTINE PtADTree_RemoveNode

  !>
  !!   Find the point in the tree that lie in box centred at Pt 
  !!        and its size is 2*HalfSize(1:3) ).
  !!     HalfSize is a value of type PtADTreeModule::PtADTreeType
  !!     which can be set in SUBROUTINE PtADTree_SetTree().
  !!   @param[in]  PtADTree  :  the tree.
  !!   @param[in]  Pt        :  the centre of the box.
  !!   @param[in]  NB        :  the maximum number of output points.
  !!                       If only want to find the closest point, input NB as 1.  \n
  !!                       =-1, output the first point found in the quad-domain
  !!   @param[in]  Isucc = 1 :  if fail, stop.                     \n
  !!                     = 0 :  if fail, return with warning.      \n
  !!                     =-1 :  if fail, return without warning.   \n
  !!   @param[out]   NB      :  the number of points found in the box.
  !!   @param[out]   IPs     :  the list of points found in the box.
  !!                       This array has been sorted in ascending order,
  !!                       so IPs(1) is the closest point to Pt.
  !!   @param[out]   Dist    :  the distance between the centre (Pt) and those points.
  !!   @param[out]   Isucc = 1 :  succeed.  \n
  !!                       = 0 :  fail.
  !<
  SUBROUTINE PtADTree_SearchNode(PtADTree,Pt,IPs, Dist, NB, Isucc)
    IMPLICIT NONE

    TYPE (PtADTreeType), INTENT(IN) :: PtADTree
    INTEGER, INTENT(OUT)  :: IPs(*)
    REAL*8,  INTENT(OUT)  :: Dist(*)
    REAL*8,  INTENT(IN)   :: Pt(3)
    INTEGER, INTENT(INOUT):: NB, Isucc
    INTEGER :: istk, n, id, lev, ipt
    INTEGER :: indx(PtADTree%numPoints), list(PtADTree%numPoints)
    REAL*8  :: DD(PtADTree%numPoints)
    REAL*8  :: amov, d
    REAL*8  :: scale_Pt(6), xl(6), box(6), pp(3)
    TYPE (BiTreeNodeType), POINTER :: node  

    TYPE :: clueNode
       TYPE (BiTreeNodeType), POINTER :: node => null()
       REAL*8  :: box(6)
    END TYPE clueNode
    TYPE (clueNode), DIMENSION(PtADTreePar%maxLevel) :: clue

    scale_Pt(1:3) = PtADTree_normalise(PtADTree, Pt)
    scale_Pt(4:6) = scale_Pt(1:3)
    DO id = 1,3
       IF(scale_Pt(id)<=0.d0 .OR. scale_Pt(id)>=1.d0)THEN
          IF(Isucc==1)THEN
             WRITE(*,*)'PtADTree Name: ',PtADTree%theName
             WRITE(*,*)'Pt=',Pt, 'scale_Pt=',scale_Pt(1:3)
             WRITE(*,*)'Error--- Point is out of PtADTree domain, search fail'
             CALL Error_STOP ( '--- PtADTree_SearchNode')
          ELSE IF(Isucc==-1)THEN
          ELSE
             WRITE(*,*)'PtADTree Name: ',PtADTree%theName
             WRITE(*,*)'Pt=',Pt, 'scale_Pt=',scale_Pt(1:3)
             WRITE(*,*)'Warning--- Point is out of PtADTree domain, search fail'
          ENDIF
          Isucc = 0
          RETURN
       ENDIF
    ENDDO

    xl(1:6) =  0.d0
    node    => PtADTree%Root
    istk    =  0
    n       =  0

    DO 

       IF( BiTreeNodeType_isEmpty(node) ) THEN
          IF(istk==0)THEN
             !---finish searching
             EXIT
          ENDIF

          node    => clue(istk)%node%branchRight
          xl(1:6) =  clue(istk)%box(1:6)
          istk    = istk-1
          IF( .NOT. BiTreeNodeType_isEmpty(node) ) THEN
             lev    = node%Level
             id     = MOD(lev,PtADTreePar%numDimension)+1
             amov   = PtADTreePar%shiftAmount(lev)
             xl(id) = xl(id)+amov
             IF(id <= 3) THEN
                IF(xl(id)      > scale_Pt(id)) node => emptyNode
             ELSE
                IF(xl(id)+amov < scale_Pt(id)) node => emptyNode
             ENDIF
          ENDIF
       ELSE
          !--- visit 'node'
          ipt      = node%ID
          box(1:6) = PtADTree_getBox(PtADTree, ipt)

          IF( Geo3D_InsideBox2(Pt, box, 0.d0) )THEN
             IF(NB==-1)THEN
                NB = 1
                IPs(1) = ipt
                Isucc  = 1
                RETURN
             ENDIF
             pp(:) = PtADTree%Posit(:,ipt)
             d     = Geo3D_Distance_SQ(pp,Pt)
             n     = n+1
             list(n) = ipt
             DD(n)   = d
          ENDIF

          istk                = istk+1
          clue(istk)%node     => node
          clue(istk)%box(1:6) = xl(1:6)
          node => node%branchLeft
          IF( .NOT. BiTreeNodeType_isEmpty(node) ) THEN
             lev  = node%Level
             id   = MOD(lev,PtADTreePar%numDimension)+1
             amov = PtADTreePar%shiftAmount(lev)
             IF(id <= 3) THEN
                IF(xl(id)      > scale_Pt(id)) node => emptyNode
             ELSE
                IF(xl(id)+amov < scale_Pt(id)) node => emptyNode
             ENDIF
          ENDIF
       ENDIF

    ENDDO

    IF(n==0)THEN
       NB = 0
       IF(Isucc==1)THEN
          WRITE(*,*)'PtADTree Name: ',PtADTree%theName
          WRITE(*,*)'Pt=',Pt, 'scale_Pt=',scale_Pt(1:3)
          WRITE(*,*)'Error--- No point is found to this point in a radius ',  &
               PtADTree%HalfSize(1:3) / PtADTree%ScaleFactor(1:3)
          CALL Error_STOP ( '--- PtADTree_SearchNode')
       ELSE IF(Isucc==-1)THEN
       ELSE
          WRITE(*,*)'PtADTree Name: ',PtADTree%theName
          WRITE(*,*)'Pt=',Pt, 'scale_Pt=',scale_Pt(1:3)
          WRITE(*,*)'Warning--- No point is found to this point in a radius ',  &
               PtADTree%HalfSize(1:3) / PtADTree%ScaleFactor(1:3)
       ENDIF
       Isucc = 0
       RETURN
    ENDIF

    IF(NB==1)THEN
       Dist(1) = DD(1)
       Ips(1)  = list(1)
       DO ipt = 2,n
          IF(DD(ipt)<Dist(1))THEN
             Dist(1) = DD(ipt)
             Ips(1)  = list(ipt)
          ENDIF
       ENDDO
    ELSE
       CALL quicksort(n,DD,indx)
       NB = MIN(NB,n)
       DO ipt = 1,NB
          Dist(ipt) = DD(indx(ipt))
          Ips(ipt)  = list(indx(ipt))
       ENDDO
    ENDIF

    Dist(1:NB) = dsqrt(Dist(1:NB))
    Isucc = 1

    RETURN
  END SUBROUTINE PtADTree_SearchNode

  !>
  !!   Delete the tree and release the memory.
  !<
  SUBROUTINE PtADTree_Clear(PtADTree)
    IMPLICIT NONE
    TYPE (PtADTreeType), INTENT(INOUT) :: PtADTree
    PtADTree%numPoints = 0
    IF( ASSOCIATED(PtADTree%Posit)       ) DEALLOCATE( PtADTree%Posit ) 
    IF( .NOT. ASSOCIATED(PtADTree%Root)  ) RETURN
    IF( .NOT. BiTreeNodeType_isEmpty(PtADTree%Root) ) CALL BiTreeNodeType_Delete( PtADTree%Root )
    RETURN
  END SUBROUTINE PtADTree_Clear

  !>
  !!  Output the information of the AD-Tree
  !!  @param[in] PtADTree  : the tree.
  !!  @param[in] io      : the IO channel. 
  !!                       If =6 or not present, output on screen. 
  !!  @param[out] isReady : if the tree is ready.
  !<
  SUBROUTINE PtADTree_Info(PtADTree, io, isReady)
    IMPLICIT NONE
    TYPE (PtADTreeType), INTENT(IN) :: PtADTree
    INTEGER, INTENT(IN),  OPTIONAL  :: io
    LOGICAL, INTENT(OUT), OPTIONAL  :: isReady
    INTEGER :: ic
    ic = 6
    IF(PRESENT(io)) ic = io
    WRITE(ic,*)'  Information for PtADTree '
    WRITE(ic,*)'     Name:          ', TRIM(PtADTree%theName)
    IF(BiTreeNodeType_isEmpty (PtADTree%Root))THEN
       IF(PRESENT(isReady)) isReady = .FALSE.
       WRITE(ic,*)'  The tree is empty!'
       RETURN
    ENDIF
    WRITE(ic,*)'     Origin:        ', REAL(PtADTree%Origin)
    WRITE(ic,*)'     ScaleFactor:   ', REAL(PtADTree%ScaleFactor)
    WRITE(ic,*)'     HalfSize:      ', PtADTree%HalfSize
    IF(PRESENT(isReady)) isReady = .TRUE.
    RETURN
  END SUBROUTINE PtADTree_Info

  !>
  !!  An application of point-ADtree.
  !!  Identify those nodes by the position.
  !!   @param[in]  NB_Point   the number of points to be checked.
  !!   @param[in]  Posit      the position of each point.
  !!   @param[in]  tolg       the tolerance. Two nodes with a distance less than tolg 
  !!                          will be treated as identical.
  !!   @param[out] Niden      the number of identical pairs.
  !!   @param[out] Markp      the identical mapping. 
  !!                          If 2 or more (say ip1, ip2, ip3...) nodes are identical,
  !!                          then by return, we have Markp(ip1)=Markp(ip2)=Markp(ip3)=...=ip, 
  !!                          with ip=min(ip1,ip2,ip3,...).
  !!                          IF the node ip is unique, then Markp(ip)=ip.
  !<
  SUBROUTINE PtADTree_IdentifyNodes(NB_Point, Posit, tolg, Niden, Markp)   
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: NB_Point
    REAL*8,  INTENT(IN)  :: Posit(3,*), tolg
    INTEGER, INTENT(OUT) :: Niden, Markp(*)
    TYPE (PtADTreeType)  :: PtADTree
    INTEGER :: ip, Isucc, IPs(1), NB
    REAL*8  :: Dist(1), dd(3), pMin(3), pMax(3)

    dd(1:3) = tolg

    IF(tolg>0)THEN

       CALL PtADTree_SetTree(PtADTree, Posit, NB_Point, dd)
       Niden = 0
       DO ip = 1, NB_Point
          NB = 1
          CALL PtADTree_SearchNode (PtADTree, Posit(:,ip), IPs, Dist, NB, Isucc)
          markp(ip) = IPS(1)
          IF(IPs(1)<ip) Niden = Niden +1
       ENDDO

    ELSE

       !--- set domain and scalar
       pMin(1:3) = Posit(:,1)
       pMax(1:3) = Posit(:,1)
       DO ip=2,NB_Point
          pMin(:) = MIN(pMin(:),Posit(:,ip))
          pMax(:) = MAX(pMax(:),Posit(:,ip))
       ENDDO
       CALL PtADTree_SetTreeDomain(PtADTree, pMin, pMax, dd)    

       !--- set the tree
       PtADTree%numPoints = NB_Point
       CALL allc_real8_2Dpointer( PtADTree%Posit,  3, PtADTree%numPoints, 'PtADTree') 

       Niden = 0
       DO ip=1,NB_Point
          CALL PtADTree_AddUniqueNode(PtADTree,ip,Posit(:,ip),NB)
          Markp(ip) = NB
          IF(NB/=ip) Niden = Niden +1
       ENDDO

    ENDIF

    CALL PtADTree_Clear(PtADTree)

    RETURN
  END SUBROUTINE PtADTree_IdentifyNodes


END MODULE PtADTreeModule

!>
!!   Another name of module PtADTreeModule
!<
MODULE PtADTreeAvailables
  USE PtADTreeModule
END MODULE PtADTreeAvailables

