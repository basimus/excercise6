!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Hybrid mesh storage, including the defination of type and the basic functions.
!<
MODULE HybridMeshHighOrder
  USE HybridMeshManager
  USE HighOrderCellManager
  USE LinkAssociation

  IMPLICIT NONE

CONTAINS

  !>
  !!  upgrade the gridorder. 
  !!  @param[in,out] HybMesh  the hybrid mesh.
  !<
  SUBROUTINE HybridMesh_getHighOrder(GridOrder, HybMesh)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: GridOrder
    TYPE(HybridMeshStorageType),  INTENT(INOUT) :: HybMesh
    INTEGER :: ND, oldOrder, i

    oldOrder          = HybMesh%GridOrder
    CALL HighOrderCell_Clear(HybMesh%Cell)
    HybMesh%Cell      = HighOrderCell_getHighCell(GridOrder,3,0)
    HybMesh%GridOrder = HybMesh%Cell%GridOrder

    ND = HybMesh%Cell%numCellNodes
    CALL allc_int_2Dpointer(HybMesh%IP_Tet, ND, HybMesh%NB_Tet)

    IF(oldOrder==1)THEN    
       DO i = 5, ND
          HybMesh%IP_Tet(i,:) = 0
       ENDDO
    ENDIF

    CALL HybridMesh_LinearFill(HybMesh, 0)

  END SUBROUTINE HybridMesh_getHighOrder


  !>
  !!  Fill missing high-order nodes by linear interpolation. 
  !!  Match the exsiting high-order nodes.
  !!
  !!  @param[in,out] HybMesh  the hybrid mesh.
  !!  @param[in]  force  =0  don't change the position of exist high-nodes.   \n
  !!                     =1  change the position of exist high-nodes to linear position.
  !!
  !!  Reminder: if high-order nodes on different cell do not match 
  !!            (for example, the I-th node of cell N should be the same with 
  !!             the J-th node of cell M, but the mesh gives different ID or position),
  !!            the the CELL with lower ID takes advantage.
  !<
  SUBROUTINE HybridMesh_LinearFill(HybMesh, force)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    INTEGER, INTENT(IN) :: force
    INTEGER :: ND, NPT, NPF, Nsd, NPE, Ord, Mside
    INTEGER :: IT, i, j, k, itnb, Iedge, nEdge, ir
    INTEGER :: ips(1000), ip3(3)
    INTEGER, DIMENSION(:,:), POINTER :: edgenodes, faceNodes, irr, IP_Edge
    REAL*8,  DIMENSION(:,:), POINTER :: pv, pt
    TYPE(NodeNetTreeType)   :: Edge_Tree
    TYPE(HighOrderCellType) :: edge


    ND  = HybMesh%Cell%numCellNodes
    NPT = HybMesh%Cell%numVertices
    NPF = HybMesh%Cell%numFaceNodes
    Nsd = HybMesh%Cell%Nsd
    Ord = HybMesh%Cell%GridOrder
    NPE = Ord + 1

    ALLOCATE(pv(Nsd,NPT), pt(Nsd,ND))
    ALLOCATE(edgeNodes(NPE, 6))
    ALLOCATE(irr(NPF, -3:-1))
    CALL HighOrderCell_getEdgeNodes_TET(Ord, edgeNodes)
    faceNodes => HybMesh%Cell%faceNodes
    DO i = -3, -1
       irr(:,i) = HighOrderCell_trianglePermutation(Ord,i)
    ENDDO

    edge = HighOrderCell_getHighCell(Ord,1,HybMesh%Cell%optionNodes)

    !--- fill high-order nodes on edges.

    Mside = 4* HybMesh%NB_Tet
    ALLOCATE(IP_Edge(NPE,Mside))
    CALL NodeNetTree_allocate(2, HybMesh%NB_Point, Mside, Edge_Tree)

    nEdge = 0
    DO IT = 1, HybMesh%NB_Tet
       DO j = 1,6
          ips(1:NPE) = HybMesh%IP_Tet(edgeNodes(:,j),IT)
          CALL NodeNetTree_SearchAdd(2, ips(1:2), Edge_Tree, IEdge)

          IF(Iedge>nedge) THEN
             !--- a new edge
             nedge = Iedge          
             pv(:,1:2) = HybMesh%Posit(:, ips(1:2))
             CALL HighOrderCell_linearIsoparTransf( edge, pv, pt )
             DO i = 3, NPE
                IF(ips(i)==0)THEN
                   HybMesh%NB_Point  = HybMesh%NB_Point + 1
                   IF(HybMesh%NB_Point>keyLength(HybMesh%Posit))THEN
                      CALL allc_2Dpointer(HybMesh%Posit,Nsd, 1+2*HybMesh%NB_Point*2)
                   ENDIF
                   ips(i) = HybMesh%NB_Point
                   HybMesh%Posit(:,ips(i)) = Pt(:,i)
                ELSE IF(force>=1)THEN
                   HybMesh%Posit(:,ips(i)) = Pt(:,i)
                ENDIF
             ENDDO
             IP_Edge(1:NPE, Iedge) = ips(1:NPE)
          ENDIF

          IF(HybMesh%IP_Tet(edgeNodes(1,j),IT) == IP_Edge(1, Iedge))THEN
             HybMesh%IP_Tet(edgeNodes(3:NPE,j),IT) = IP_Edge(3:NPE, Iedge) 
          ELSE IF(HybMesh%IP_Tet(edgeNodes(1,j),IT) == IP_Edge(2, Iedge))THEN
             HybMesh%IP_Tet(edgeNodes(3:NPE,j),IT) = IP_Edge(NPE:3:-1, Iedge) 
          ELSE
             STOP 'dfsjf0923irrjfifgow;'
          ENDIF
       ENDDO
    ENDDO

    DEALLOCATE(IP_Edge)
    CALL NodeNetTree_clear(Edge_Tree)

    !--- fill high-order nodes in elements.

    CALL HybridMesh_BuildNext (HybMesh)

    DO IT = 1, HybMesh%NB_Tet
       pv(1:Nsd,1:NPT) = HybMesh%Posit(:,HybMesh%IP_Tet(1:NPT,IT) )
       CALL HighOrderCell_linearIsoparTransf( HybMesh%Cell,pv, pt )
       DO i = NPT+1, ND
          IF(HybMesh%IP_Tet(i,IT)==0) THEN
             HybMesh%NB_Point  = HybMesh%NB_Point + 1
             IF(HybMesh%NB_Point>keyLength(HybMesh%Posit))THEN
                CALL allc_2Dpointer(HybMesh%Posit,Nsd, 1+2*HybMesh%NB_Point*2)
             ENDIF
             HybMesh%IP_Tet(i,IT) = HybMesh%NB_Point
             HybMesh%Posit(:,HybMesh%IP_Tet(i,IT)) = Pt(:,i)
          ELSE IF(force>=1)THEN
             HybMesh%Posit(:,HybMesh%IP_Tet(i,IT)) = Pt(:,i)
          ENDIF
       ENDDO

       !--- pass to neighbour
       DO j = 1,4
          itnb = HybMesh%Next_Tet(j, IT)
          IF(itnb<IT) CYCLE
          ips(1:NPF) = HybMesh%IP_Tet(faceNodes(:,j),IT)
          k = which_NodeinTet(IT, HybMesh%Next_Tet(:, itnb))
          ip3(1:3) = HybMesh%IP_Tet(faceNodes(1:3,k),itnb)
          ir = which_RotationinTri (ips(1:3), ip3)
          IF(ir>=0)THEN
             WRITE(*,*)' Error--- neighbour not match, ir=',ir
             WRITE(*,*)'   IT =',IT, 'ips=',HybMesh%IP_Tet(1:4,IT), ' j=', j
             WRITE(*,*)'   ITn=',ITnb, 'ips=',HybMesh%IP_Tet(1:4,ITnb), ' j=', k
             CALL Error_STOP ('HybridMesh_LinearFill:: ')
          ENDIF
          HybMesh%IP_Tet(faceNodes(:,k),itnb) = ips(irr(:,ir))        
       ENDDO
    ENDDO

    DEALLOCATE(pv, pt)
    DEALLOCATE(edgeNodes, irr)


  END SUBROUTINE HybridMesh_LinearFill



  !>
  !!  remove those nodes that are not associated with any elements.
  !!  @param[in,out] HybMesh  the mesh.
  !<
  SUBROUTINE HybridMesh_CleanNodes(HybMesh, old_to_new)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    INTEGER, INTENT(OUT), OPTIONAL :: old_to_new(*)
    INTEGER :: IP, NP, IT, I
    INTEGER, DIMENSION(:), POINTER :: markp

    ALLOCATE(markp(0:HybMesh%nb_point))
    markp(:) = 0
    DO IT = 1, HybMesh%NB_Tet
       markp(HybMesh%IP_Tet(:,IT)) = 1
    ENDDO

    NP = 0
    DO IP = 1, HybMesh%NB_Point
       IF(markp(IP)==0) CYCLE
       NP = NP + 1
       markp(ip) = np
       HybMesh%Posit(:,NP) = HybMesh%Posit(:,IP)
    ENDDO

    IF(PRESENT(old_to_new))THEN
       old_to_new(1:HybMesh%NB_Point) = markp(1:HybMesh%NB_Point)
    ENDIF

    HybMesh%NB_Point = NP
    markp(0) = 0

    DO IT = 1, HybMesh%NB_Tet
       HybMesh%IP_Tet(:,IT) = markp(HybMesh%IP_Tet(:,IT))
    ENDDO

    NP = 0
    DO I = 1, HybMesh%NB_DBC
       IP = HybMesh%IP_DBC(I)
       IF(markp(IP)==0) CYCLE
       NP = NP + 1
       HybMesh%IP_DBC(NP) = markp(IP)
       HybMesh%Pd_DBC(:,NP) = HybMesh%Pd_DBC(:,I)
    ENDDO
    HybMesh%NB_DBC = NP

    DEALLOCATE(markp)

  END SUBROUTINE HybridMesh_CleanNodes


  !>     
  !!    Calculate the movements of boundary nodes.   
  !!
  !!    @param[in]  HybMesh    a LINEAR mesh.
  !!    @param[in]  Surf    A surface mesh.  The position of those nodes on high-oder triangle, 
  !!                           i.e. Surf.IPsp(:,:)/=0, are the target positions of the movement.
  !!                        The numbering of vertice, i.e. Surf.IP_Tri(1:3,:), and element id
  !!                           i.e. Surf.IP_Tri(4,:), mush match the volume mesh.
  !!                           The numbering of other nodes, i.e. Surf.IPsp(4:,:), don't have to.
  !!    @param[out] HybMesh.NB_DBC    the number of boundary nodes
  !!    @param[out] HybMesh.IP_DBC         (HybMesh.NB_DBC) the ID of each boundary node.
  !!    @param[out] HybMesh.Pd_DBC         (3,HybMesh%NB_DBC) the movement.
  !<     
  SUBROUTINE HybridMesh_setMovement(HybMesh, Surf)
    IMPLICIT NONE

    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER :: Msize, NPF, Ord, IB, IT
    INTEGER :: i, j, k,  ip4(1000), ip3(1000), nf(1000)
    INTEGER, DIMENSION(:), POINTER :: markp
    INTEGER, DIMENSION(:,:), POINTER :: irr


    Msize = HybMesh%NB_Point
    NPF   = HybMesh%Cell%numFaceNodes
    Ord   = HybMesh%Cell%GridOrder

    CALL allc_1Dpointer(HybMesh%IP_DBC,    Msize)
    CALL allc_2Dpointer(HybMesh%Pd_DBC, 3, Msize)
    ALLOCATE(markp(HybMesh%NB_Point))
    ALLOCATE(irr(NPF, -3:3))
    DO i = -3, 3
       IF(i==0) CYCLE
       irr(:,i) = HighOrderCell_trianglePermutation(Ord,i)
    ENDDO

    markp(:) = 0

    DO IB = 1,Surf%NB_Tri
       IT = Surf%IP_Tri(4, IB)
       IF(IT==0) CYCLE
       ip3(1:NPF) = Surf%IPsp_Tri(1:NPF, IB)
       IF(MINVAL(ip3(1:NPF))==0) CYCLE
       DO k = 1,4
          nf(1:NPF)  = HybMesh%Cell%faceNodes(1:NPF,k)
          ip4(1:NPF) = HybMesh%IP_Tet(nf(1:NPF),IT)
          j = which_RotationinTri (IP3(1:3), IP4(1:3))
          IF(j/=0) EXIT
       ENDDO
       IF(k>4)THEN
          WRITE(*,*)' Error--- triangle not match: IB,IT=',IB,IT
          WRITE(*,*)'   ip3=',ip3(1:3)
          WRITE(*,*)'   ip4=',HybMesh%IP_Tet(nf(1:4),IT)
          CALL Error_STOP( 'HybridMesh_setMovement::')
       ENDIF

       ip3(1:NPF) = ip3(irr(1:NPF, j))
       DO i = 1, NPF
          IF(markp(ip4(i))>0) CYCLE
          HybMesh%NB_DBC = HybMesh%NB_DBC + 1
          HybMesh%IP_DBC(HybMesh%NB_DBC) = ip4(i)
          HybMesh%Pd_DBC(:,HybMesh%NB_DBC) = Surf%Posit(:,ip3(i)) - HybMesh%Posit(:,ip4(i))
          markp(ip4(i)) = 1
       ENDDO
    ENDDO

    DEALLOCATE(markp)
  END SUBROUTINE HybridMesh_setMovement

  !>     
  !!    Set those boundary nodes not being DBC points as frozen nodes.
  !!
  !!    @param[in]  HybMesh    a high-order mesh.
  !!    @param[out] HybMesh.NB_DBC    (updated) the number of boundary nodes
  !!    @param[out] HybMesh.IP_DBC         (HybMesh.NB_DBC) the ID of each boundary node.
  !!    @param[out] HybMesh.Pd_DBC         (3,HybMesh%NB_DBC) the movement.
  !<     
  SUBROUTINE HybridMesh_setFrozen(HybMesh)
    IMPLICIT NONE

    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    INTEGER :: Msize, NPF, Ord
    INTEGER :: IT, i, j, ips(1000)
    INTEGER, DIMENSION(:),   POINTER :: markp
    INTEGER, DIMENSION(:,:), POINTER :: faceNodes

    Msize = HybMesh%NB_Point
    NPF   = HybMesh%Cell%numFaceNodes
    Ord   = HybMesh%Cell%GridOrder

    CALL allc_1Dpointer(HybMesh%IP_DBC,    Msize)
    CALL allc_2Dpointer(HybMesh%Pd_DBC, 3, Msize)
    ALLOCATE(markp(HybMesh%NB_Point))

    faceNodes =>  HybMesh%Cell%faceNodes
    markp(:) = 0
    DO i = 1, HybMesh%NB_DBC
       markp(HybMesh%IP_DBC(i)) = 1
    ENDDO

    CALL HybridMesh_BuildNext (HybMesh)

    DO IT = 1,HybMesh%NB_Tet
       DO j = 1,4
          IF(HybMesh%Next_Tet(j,IT)>0) CYCLE
          ips(1:NPF) = HybMesh%IP_Tet(faceNodes(:,j),IT)
          DO i = 1, NPF
             IF(markp(ips(i))>0) CYCLE
             HybMesh%NB_DBC = HybMesh%NB_DBC + 1
             HybMesh%IP_DBC(HybMesh%NB_DBC) = ips(i)
             HybMesh%Pd_DBC(:,HybMesh%NB_DBC) = 0
             markp(ips(i)) = 1
          ENDDO
       ENDDO
    ENDDO

    DEALLOCATE(markp)
  END SUBROUTINE HybridMesh_setFrozen

  !>     
  !!    Split the mesh by DBC points.
  !!
  !!    @param[in]  HybMesh    a high-order mesh with DBCnodes ready.
  !!    @param[out] nblock     the number of blocks.
  !!    @param[out] block      the block ID of each element.
  !<     
  SUBROUTINE HybridMesh_DBCblock(HybMesh, nblock, BLOCK)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    INTEGER, INTENT(OUT) :: nblock
    INTEGER, INTENT(OUT) :: BLOCK(*)
    INTEGER :: ND, Ord, NPF, ICC
    INTEGER :: IT, i, ip, itnb
    INTEGER, DIMENSION(:,:), POINTER :: faceNodes
    INTEGER, DIMENSION(:),   POINTER :: markp
    TYPE (LinkAssociationType) :: LinkAsso

    ND  = HybMesh%Cell%numCellNodes
    Ord = HybMesh%Cell%GridOrder
    NPF = HybMesh%Cell%numFaceNodes
    ICC = Ord + 4     !--- the node in a triangle (not vertix or edge)

    ALLOCATE(faceNodes(NPF, 4))
    CALL HighOrderCell_getFaceNodes_TET(Ord, faceNodes)


    ALLOCATE(markp(HybMesh%NB_Point))    
    CALL LinkAssociation_Allocate (HybMesh%NB_Tet, LinkAsso)

    Markp(:) = 0
    Markp( HybMesh%IP_DBC(1:HybMesh%NB_DBC) ) = 1

    !--- search links between two cell which not divided by DBC nodes
    DO IT = 1, HybMesh%NB_Tet
       DO i = 1, 4
          itnb = HybMesh%Next_Tet(i,IT)
          IF(itnb<IT) CYCLE          
          ip = HybMesh%IP_Tet(FaceNodes(Icc,i), IT)
          IF(markp(ip)==0)THEN
             !--- inner egde, add a link
             CALL LinkAssociation_AddLink (IT, itnb, LinkAsso)
             CALL LinkAssociation_AddLink (itnb, IT, LinkAsso)
          ENDIF          
       ENDDO
    ENDDO

    CALL LinkAssociation_Colour (LinkAsso, HybMesh%NB_Tet, 1, BLOCK, nblock)

    DEALLOCATE( faceNodes,  markp )
    CALL LinkAssociation_Clear (LinkAsso)

  END  SUBROUTINE HybridMesh_DBCblock


  !>
  !! Draw a cell IT a channel.
  !! @param[in]  N        the number of points along on each side.
  !! @param[in]  IO       the IO channel. 
  !! @param[in]  inside   =0 no inside detail (boundary only).   \n
  !!                      =1 with inside detail
  !<
  SUBROUTINE HybridMesh_CellDisplay(HybMesh, IT, N, IO, inside)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(IN) :: HybMesh
    INTEGER, INTENT(IN) :: IT, N, IO, inside
    REAL*8,  DIMENSION(HybMesh%Cell%Nsd,HybMesh%Cell%numCellNodes) :: Pts

    Pts(:,:) = HybMesh%Posit(:,HybMesh%IP_Tet(:,IT))
    CALL HighOrderCell_TetDisplay(HybMesh%Cell, Pts, N, IO, inside)

  END SUBROUTINE HybridMesh_CellDisplay


  !>
  !! @param[in]  HybMesh  the mesh.
  !! @param[in]  material the material.
  !! @param[in]  niter    the number of maximum iteritions.
  !! @param[out] HybMesh  the mesh with new position. Pd_DBC maybe destroyed.
  !<
  SUBROUTINE  HybridMesh_linearSystemSolver(HybMesh, material, niter)
    USE MUMPS_MODULE
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    TYPE(MaterialPar), INTENT(IN) :: Material
    INTEGER, INTENT(IN) :: niter
    TYPE(MaterialPar) :: vMat
    INTEGER :: NP, NB_Point, NB_Cell, Nsd, nofDBC, Nfree, Msize, Isucc
    INTEGER :: IT, i, k, n, m, ip, j, ip2(2), iter
    INTEGER :: nOfValuesElem, nOfValuesElem2, nOfValues, nOfEquations
    INTEGER, DIMENSION(:), POINTER :: currentIndex, currentIndex2
    INTEGER, DIMENSION(:), POINTER :: IRN, JCN
    REAL*8,  DIMENSION(:), POINTER :: Aij, b, d
    INTEGER, DIMENSION(                HybMesh%Cell%numCellNodes) :: IPs
    INTEGER, DIMENSION(HybMesh%Cell%Nsd* HybMesh%Cell%numCellNodes) :: TeAll
    REAL*8,  DIMENSION(HybMesh%Cell%Nsd, HybMesh%Cell%numCellNodes) :: Pts
    REAL*8,  DIMENSION(HybMesh%Cell%Nsd* HybMesh%Cell%numCellNodes,    &
                       HybMesh%Cell%Nsd* HybMesh%Cell%numCellNodes) :: eMat
    INTEGER, DIMENSION(:), POINTER :: markp
    TYPE (NodeNetTreeType) :: LinkTree
    REAL*8, DIMENSION(:,:), POINTER :: disp, ePosit, PdAll
    REAL*8  :: PdStep, PdLeft, Vamm
    INTEGER :: kmethod = 2

    vMat = Material
    
    NP       = HybMesh%Cell%numCellNodes
    NB_Point = HybMesh%NB_Point
    NB_Cell  = HybMesh%NB_Tet
    Nsd      = HybMesh%Cell%Nsd

    nOfValuesElem  = Nsd * NP
    nOfValuesElem2 = nOfValuesElem**2
    nOfValues      = nOfValuesElem2 * NB_Cell
    nOfEquations   = Nsd * NB_Point
    nofDBC         = Nsd * HybMesh%NB_DBC

    ! Allocate
    ALLOCATE(currentIndex (nOfValuesElem))
    ALLOCATE(currentIndex2(nOfValuesElem2))
    IF(kmethod==1)THEN
       Msize = nOfValues + 2*nofDBC
    ELSE
       Msize = nOfValues
    ENDIF
    ALLOCATE(IRN(Msize))
    ALLOCATE(JCN(Msize))
    ALLOCATE(Aij(Msize))
    ALLOCATE(Disp(  Nsd, HybMesh%NB_Point))
    ALLOCATE(ePosit(Nsd, HybMesh%NB_Point))
    ALLOCATE(PdAll( Nsd, HybMesh%NB_DBC))
    ALLOCATE(markp( nOfEquations))
    ALLOCATE(d(     nofDBC))
    ALLOCATE(b(nOfEquations + nofDBC))

    ePosit(:,1:HybMesh%NB_Point) = HybMesh%Posit(:, 1:HybMesh%NB_Point)
    PdAll(:, 1:HybMesh%NB_DBC)   = HybMesh%Pd_DBC(:,1:HybMesh%NB_DBC)
    PdLeft = 1.d0     

    DO iter = 1, niter

       nOfValues      = nOfValuesElem2 * NB_Cell
       nOfEquations   = Nsd * NB_Point
       currentIndex  = (/ (i, i=1,nOfValuesElem) /)
       currentIndex2 = (/ (i, i=1,nOfValuesElem2) /)
       IRN = 0
       JCN = 0
       Aij = 0.d0

       ! Loop on elements
       DO IT = 1, NB_Cell
          IPs(1:NP)   = HybMesh%IP_Tet(1:NP,IT)
          Pts(:,1:NP) = HybMesh%Posit(:,IPs(1:NP))
          CALL HighOrderCell_ElasticityMatrix3D(HybMesh%Cell, Pts, vMat, eMat, Isucc)
          IF(Isucc==0)THEN
             WRITE(*,*)' Error--- negative Jacobian, IT=',IT
             CALL Error_Stop('HybridMesh_linearSystemSolver')
          ENDIF

          TeAll(     1 :   NP) = IPs(1:NP)
          TeAll(  NP+1 : 2*NP) = IPs(1:NP) +   NB_Point
          TeAll(2*NP+1 : 3*NP) = IPs(1:NP) + 2*NB_Point       

          DO i=1,nOfValuesElem
             IRN(currentIndex) = TeAll(i)
             JCN(currentIndex) = TeAll(:)
             currentIndex = currentIndex + nOfValuesElem
          ENDDO
          Aij(currentIndex2) = RESHAPE(eMat, (/nOfValuesElem2/))
          currentIndex2 = currentIndex2 + nOfValuesElem2
       ENDDO

       IF(kmethod==1)THEN
          !--- ruben's original code: expand the matrix to meet the boundary condition

          !--- symmetry reduce
          m = 0
          DO I = 1, nOfValues
             IF(IRN(I)>JCN(I)) CYCLE
             m = m + 1
             IRN(m) = IRN(I)
             JCN(m) = JCN(I)
             Aij(m) = Aij(I)
          ENDDO
          nOfValues = m

          ! disp = solveSystemElasticity(mesh, refElem, 
          ! Dirichlet boundary conditions via Lagrange multipliers

          b(1:nOfEquations) = 0
          n = nOfEquations
          m = nOfValues
          DO k  = 1, Nsd
             DO i = 1, HybMesh%NB_DBC
                n = n + 1 
                m = m + 1 
                b(n)   = HybMesh%Pd_DBC(k,i)
                IRN(m) = HybMesh%IP_DBC(i) + (k-1) * NB_Point
                JCN(m) = n
                Aij(m) = 1
             ENDDO
          ENDDO

          ! Total system of equations to be solved
          nOfEquations = n
          nOfValues    = m

          CALL MUMPS_create(nOfEquations, nOfValues, IRN, JCN, Aij)

          ! Solve linear system

          CALL MUMPS_solver(nOfEquations, b)
          disp(1:Nsd, 1:NB_Point) = TRANSPOSE( RESHAPE( b, (/NB_Point, Nsd/)) )

       ELSE
          !--- move known displacement (boundary condition) to the right

          Nfree = NB_Point - HybMesh%NB_DBC

          markp(:) = 0
          DO i = 1, HybMesh%NB_DBC
             ip = HybMesh%IP_DBC(i)
             DO j = 1, nsd
                markp(ip+(j-1)*NB_Point) = -i - (j-1)*HybMesh%NB_DBC
                d(i+(j-1)*HybMesh%NB_DBC) =  HybMesh%PD_DBC(j,i)
             ENDDO
          ENDDO

          n = 0
          DO ip = 1, NB_Point
             IF(markp(ip)==0)THEN
                n = n + 1
                DO j = 1, nsd
                   markp(ip+(j-1)*NB_Point) = n + (j-1)*Nfree
                ENDDO
             ENDIF
          ENDDO

          b(:) = 0
          m = 0
          DO k = 1, nOfValues
             i = IRN(k)
             j = JCN(k)
             IF(markp(i)<0) CYCLE
             IF(markp(j)<0) THEN
                b(markp(i)) = b(markp(i)) - Aij(k) * d(-markp(j))
             ELSE IF(i>=j)THEN
                m = m+1
                IRN(m) = markp(i)
                JCN(m) = markp(j)
                Aij(m) = Aij(k)
             ENDIF
          ENDDO

          nOfEquations = NSD*Nfree
          nOfValues    = m

          !--- assem the matrix
          CALL NodeNetTree_Allocate(2, nOfEquations, nOfValues, linktree)
          m = 0
          DO k = 1, nOfValues
             ip2(1:2) = (/IRN(k), JCN(k)/)
             CALL NodeNetTree_SearchAdd(2,ip2,linktree,i)
             IF(i>m)THEN
                m = i
                IRN(i) = IRN(k)
                JCN(i) = JCN(k)
                Aij(i) = Aij(k)
             ELSE
                Aij(i) = Aij(i) + Aij(k)
             ENDIF
          ENDDO
          CALL NodeNetTree_Clear(linktree)

          nOfValues    = m

          ! Total system of equations to be solved

          CALL MUMPS_create(nOfEquations, nOfValues, IRN, JCN, Aij)
          CALL MUMPS_solver(nOfEquations, b)

          DO ip = 1, HybMesh%NB_Point
             n = markp(ip)
             IF(n>0)THEN
                DO j = 1, nsd
                   disp(j,ip) = b(n+(j-1)*Nfree)
                ENDDO
             ELSE
                DO j = 1, nsd
                   disp(j,ip) = HybMesh%PD_DBC(j,-n)
                ENDDO
             ENDIF
          ENDDO

       ENDIF

       CALL MUMPS_destroy( )

       !--- update mesh and check quality

       PdStep = 1.0d0 
       DO
          DO ip = 1, HybMesh%NB_Point
             HybMesh%Posit(:,ip) = ePosit(:,ip) + PdStep*disp(:,ip)
          ENDDO
          IF(iter==niter) EXIT
          it = 0
          CALL HybridMesh_calcQuality(HybMesh, 0, it, Vamm)
          WRITE(*, *)iter,'   PdStep  v=',REAL(PdStep), REAL(PdLeft), REAL(Vamm)
          WRITE(29,*)iter,'   PdStep  v=',REAL(PdStep), REAL(PdLeft), REAL(Vamm)
          IF(Vamm>0.3/(iter*iter)) EXIT

          PdStep = PdStep / 2.d0
          PdLeft = PdLeft / 2.d0
          IF(MOD(iter,4)==0)THEN
             PdStep = PdStep / 2.d0
             PdLeft = PdLeft / 2.d0
          ENDIF
       ENDDO

       IF(PdLeft>0.999999) EXIT

       PdLeft = PdLeft * 2.d0 
       IF(iter==niter-1) PdLeft = 1.d0
       HybMesh%Pd_DBC(:,1:HybMesh%NB_DBC) = PdLeft*PdAll(:, 1:HybMesh%NB_DBC)

       vMat%Nu = (vMat%Nu + 0.5d0) / 2   
       CALL MaterialPar_set(vMat)


    ENDDO

    DEALLOCATE(Disp, ePosit, PdAll)
    DEALLOCATE(IRN, JCN, Aij)
    DEALLOCATE(currentIndex , currentIndex2)
    DEALLOCATE(markp, d, b)

  END SUBROUTINE  HybridMesh_linearSystemSolver


  !======================================================================
  !>
  !!   @param[in]  IO : the channel to write all Jacobian values   \n
  !!                    = 0 no output.
  !!   @param[in]  itm: the cell to check.   \n
  !!                   <=0 check all cells.
  !!   @param[out] itm: the worest cell.
  !!   @param[out] jbm: the jacobian at the worest cell.
  !!                    =2, none cell is fully filled.
  !<
  SUBROUTINE HybridMesh_calcQuality(HybMesh, IO, itm, jbm)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(IN) :: HybMesh
    INTEGER, INTENT(IN)    :: IO
    INTEGER, INTENT(INOUT) :: itm
    REAL*8,  INTENT(OUT)   :: jbm
    REAL*8, DIMENSION(HybMesh%Cell%Nsd, HybMesh%Cell%numCellNodes) :: Pts
    INTEGER :: IT, ipp(2), Iedge, js, Mside
    REAL*8  :: Jb, JacMax, JacMin, x1, x2
    TYPE(NodeNetTreeType) :: EdgeTree
    REAL*8, DIMENSION(:), POINTER :: edgeLen

    Jbm = 2.
    IF(itm>0)THEN
       IT = itm
       IF(MINVAL(HybMesh%IP_Tet(:,IT))==0) RETURN
       Pts = HybMesh%Posit(:,HybMesh%IP_Tet(:,IT))
       CALL HighOrderCell_GaussJacobian(HybMesh%Cell, Pts, JacMax, JacMin)
       Jbm = JacMin / JacMax
       RETURN
    ENDIF

    IF(IO>0)THEN
        WRITE(IO,*)'#    element   quality      stretch  ---- '
        Mside = 4*HybMesh%NB_Tet
        CALL NodeNetTree_allocate(2, HybMesh%NB_Point, Mside, EdgeTree)
        ALLOCATE (edgeLen(Mside))
        edgeLen(:) = -1
    ENDIF

    ! Loop on gauss points

    itm = 0
    DO IT = 1, HybMesh%NB_Tet
       IF(MINVAL(HybMesh%IP_Tet(:,IT))==0) CYCLE
       Pts = HybMesh%Posit(:,HybMesh%IP_Tet(:,IT))
       CALL HighOrderCell_GaussJacobian(HybMesh%Cell, Pts, JacMax, JacMin)
       Jb = JacMin / JacMax
       IF(Jb<Jbm)THEN
          Jbm = Jb
          itm = it
       ENDIF       

       IF(IO>0)THEN
          x1 =  1.d36
          x2 = -1.d36
          DO  js = 1,6
             ipp(1:2) = HybMesh%IP_Tet(I_Comb_Tet(1:2,js),IT)           
             CALL NodeNetTree_SearchAdd(2, ipp, EdgeTree, IEdge)
             IF(edgeLen(IEdge)<0)THEN
                !-- a new edge
                edgeLen(IEdge) = Geo3D_Distance(HybMesh%Posit(:,ipp(1)), HybMesh%Posit(:,ipp(2)))
             ENDIF
             x1 = min(x1,edgeLen(IEdge))
             x2 = max(x2,edgeLen(IEdge))        
          ENDDO
       
          WRITE(IO,*)it, REAL(jb), REAL(x2/x1)       
       ENDIF
    ENDDO

    IF(IO>0)THEN
        CALL NodeNetTree_Clear(EdgeTree)
        DEALLOCATE (edgeLen)
    ENDIF
    
  END SUBROUTINE HybridMesh_calcQuality

END MODULE HybridMeshHighOrder



