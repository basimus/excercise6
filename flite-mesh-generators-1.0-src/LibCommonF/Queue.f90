!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!>
!!  Module of Queues.
!<
MODULE Queue
  USE array_allocator
  USE Number_CHAR_Transfer
  IMPLICIT NONE

  !>  A queue of integers.
  TYPE :: IntQueueType
     !>  The number of valid entries, may smaller than the total length of the array.
     INTEGER :: numNodes = 0
     !>  The list of integers.
     INTEGER, DIMENSION(:), POINTER :: Nodes => null()
     !>  The pointer to the last entry.
     INTEGER, POINTER :: back => null()

  END TYPE IntQueueType

  !>  A queue of real*8.
  TYPE :: Real8QueueType
     !>  The number of valid entries, may smaller than the total length of the array.
     INTEGER :: numNodes = 0
     !>  The list of values.
     REAL*8, DIMENSION(:), POINTER :: V => null()
  END TYPE Real8QueueType


  !>  A queue of 3D points
  TYPE :: Point3dQueueType
     !>  The number of valid entries, may smaller than the total length of the array.
     INTEGER :: numNodes = 0
     !>  The list of values.
     REAL*8, DIMENSION(:,:), POINTER :: Pt => null()
  END TYPE Point3dQueueType

CONTAINS

  SUBROUTINE IntQueue_Init( List )
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(INOUT)    :: List
    deallocate( List%Nodes )
    nullify( List%back )
  END SUBROUTINE 

  !>  Set a queue by a integer array.
  SUBROUTINE IntQueue_Set(List, n, Nodes)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: n, Nodes(*)
    IF(n>keyLength(List%Nodes))THEN
       CALL IntQueue_Clear(List)
       ALLOCATE(List%Nodes(n))
    ENDIF
    List%numNodes   = n
    IF(n==0) RETURN
    List%Nodes(1:n) = Nodes(1:n)
    List%back => List%Nodes(n)
  END SUBROUTINE IntQueue_Set


  !>  Return .true. if the queue contains the given integer.
  !!  otherwise, return .false.
  FUNCTION IntQueue_Contain(List, Node) RESULT(TF)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(IN)    :: List
    INTEGER, INTENT(IN) :: Node
    LOGICAL :: TF
    INTEGER :: i
    TF = .FALSE.
    DO i = 1, List%numNodes
       IF(List%Nodes(i)==Node)THEN
          TF = .TRUE.
          RETURN
       ENDIF
    ENDDO
  END FUNCTION IntQueue_Contain

  !>  Return the position which takes the given integer.
  !!  If no such entry exist, return 0
  FUNCTION IntQueue_Find(List, Node) RESULT(ID)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(IN)    :: List
    INTEGER, INTENT(IN) :: Node
    INTEGER :: ID
    INTEGER :: i
    ID = 0
    DO i = 1, List%numNodes
       IF(List%Nodes(i)==Node)THEN
          ID = i
          RETURN
       ENDIF
    ENDDO
  END FUNCTION IntQueue_Find

  !>  Add an entry to the end.
  SUBROUTINE IntQueue_Push(List, Node)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: Node
    List%numNodes = List%numNodes + 1
    IF(List%numNodes>KeyLength(List%Nodes))THEN
       IF(List%numNodes<=6)THEN
          CALL allc_int_1Dpointer(List%Nodes, List%numNodes+6, 'IntQueue_Push')
       ELSE IF(List%numNodes<=100000)THEN
          CALL allc_int_1Dpointer(List%Nodes, 2*List%numNodes, 'IntQueue_Push')
       ELSE
          CALL allc_int_1Dpointer(List%Nodes, List%numNodes+100000, 'IntQueue_Push')
       ENDIF
    ENDIF
    List%Nodes(List%numNodes) = Node
    List%back => List%Nodes(List%numNodes)
  END SUBROUTINE IntQueue_Push

  !>  Add an entry to the end.
  SUBROUTINE IntQueue_Push36(List, Node)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: Node
    List%numNodes = List%numNodes + 1
    IF(List%numNodes>KeyLength(List%Nodes))   &
         CALL allc_int_1Dpointer(List%Nodes, List%numNodes+36, 'IntQueue_Push36')
    List%Nodes(List%numNodes) = Node
    List%back => List%Nodes(List%numNodes)
  END SUBROUTINE IntQueue_Push36

  !>  Add an entry to the end.
  SUBROUTINE IntQueue_Push10k(List, Node)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: Node
    List%numNodes = List%numNodes + 1
    IF(List%numNodes>KeyLength(List%Nodes))   &
         CALL allc_int_1Dpointer(List%Nodes, List%numNodes+10000, 'IntQueue_Push10k')
    List%Nodes(List%numNodes) = Node
    List%back => List%Nodes(List%numNodes)
  END SUBROUTINE IntQueue_Push10k

  !>
  !!   And a integer to the specified entry, and shift
  !!   the rest, so the order of entries is kept.
  !!   @param[in,out] List  : the queue of integers.
  !!   @param[in]     ID    : the entry position to add a integer.
  !!   @param[in]     Node  : the integer to be add.
  !<
  SUBROUTINE IntQueue_Add(List, ID, Node)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: ID, Node
    INTEGER :: i

    IF(ID>List%numNodes+1 .OR. ID<=0) CALL Error_STOP ( '--- IntQueue_Add :: fake ID')

    List%numNodes = List%numNodes+1

    IF(List%numNodes>KeyLength(List%Nodes))   &
         CALL allc_int_1Dpointer(List%Nodes, List%numNodes+36, 'IntQueue_Add')

    IF(ID<List%numNodes)THEN
       DO i = List%numNodes, ID+1, -1
          List%Nodes(i) = List%Nodes(i-1)
       ENDDO
    ENDIF
    List%Nodes(ID) = Node
    List%back      => List%Nodes(List%numNodes)
  END SUBROUTINE IntQueue_Add

  !>
  !!   Remove the first entry with value 'Node'.
  !!   Replace this entry with the last value, 
  !!   so the order of entries is crushed.
  !<
  SUBROUTINE IntQueue_Remove(List, Node, Isucc)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: Node
    INTEGER, OPTIONAL, INTENT(OUT) :: Isucc
    INTEGER :: i, n
    n = List%numNodes
    DO i = 1, n
       IF(List%Nodes(i)==Node)THEN
          List%Nodes(i) = List%Nodes(n)
          List%numNodes = n - 1
          IF(List%numNodes>0) List%back => List%Nodes(List%numNodes)
          EXIT
       ENDIF
    ENDDO
    IF(PRESENT(Isucc))THEN
       IF(n==List%numNodes)THEN
          Isucc = 0
       ELSE
          Isucc = 1
       ENDIF
    ENDIF
  END SUBROUTINE IntQueue_Remove

  !>
  !!   Remove the last entry.
  !<
  SUBROUTINE IntQueue_RemoveLast(List)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(INOUT)    :: List
    IF(List%numNodes==0) RETURN
    List%numNodes = List%numNodes -1
    IF(List%numNodes>0)THEN
       List%back => List%Nodes(List%numNodes)
    ELSE
       NULLIFY(List%back)
    ENDIF
  END SUBROUTINE IntQueue_RemoveLast

  !>
  !!   Remove the first entry with value 'Node' and shift
  !!   the rest, so the order of entries is kept, which deffers
  !!   SUBROUTINE IntQueue_Remove()
  !<
  SUBROUTINE IntQueue_RemoveByOrder(List, Node, Isucc)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: Node
    INTEGER, OPTIONAL, INTENT(OUT) :: Isucc
    INTEGER :: i, n, j
    n = List%numNodes
    DO i = 1, n
       IF(List%Nodes(i)==Node)THEN
          DO j = i, n-1
             List%Nodes(j) = List%Nodes(j+1)
          ENDDO
          List%numNodes = n - 1
          IF(List%numNodes>0) List%back => List%Nodes(List%numNodes)
          EXIT
       ENDIF
    ENDDO
    IF(PRESENT(Isucc))THEN
       IF(n==List%numNodes)THEN
          Isucc = 0
       ELSE
          Isucc = 1
       ENDIF
    ENDIF
  END SUBROUTINE IntQueue_RemoveByOrder

  !>
  !!   Replace the first entry with value OldNode by NewNode.
  !!   If no such entry found, return Isucc=0; otherwise =1.
  !<
  SUBROUTINE IntQueue_Replace(List, OldNode, NewNode, Isucc)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: OldNode, NewNode
    INTEGER, INTENT(OUT) :: Isucc
    INTEGER :: i
    Isucc = 1
    DO i = 1, List%numNodes
       IF(List%Nodes(i)==OldNode)THEN
          List%Nodes(i) = NewNode
          RETURN
       ENDIF
    ENDDO
    Isucc = 0
  END SUBROUTINE IntQueue_Replace

  !>
  !!  Copy a IntQueueType object to another.    \n
  !!  Reminder: NEVER copy a object to itself.
  !<
  SUBROUTINE IntQueue_Copy(SourceList, TargetList)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(IN)    :: SourceList
    TYPE (IntQueueType), INTENT(INOUT) :: TargetList
    IF(.NOT. ASSOCIATED(SourceList%Nodes))THEN
       TargetList%numNodes = SourceList%numNodes
       RETURN
    ENDIF
    CALL IntQueue_Set(TargetList, SourceList%numNodes, SourceList%Nodes)
  END SUBROUTINE IntQueue_Copy

  !>                                                                            
  !!    READ A STRING FROM A CHANNEL AND CONVERT IT TO AN INTEGER QUEUE.
  !!   @param[in]  IO  : the IO channel. 
  !!                     IF =6 ot not present, read from the screen.
  !!   @param[out] List  : the array of integers as a queue. 
  !!
  !!    FOR EXAMPLE, READ C='3 241:245 5,9 3*12, -21 :-18' ,
  !!    THEN OUPUT List(1:15)=(/3,241,242,243,244,245,5,9,12,12,12,-21,-20,-19,-18/)    
  !!        
  !!    Reminder: the length of the string should NOT be more than 512.     
  !<                                                                            
  SUBROUTINE IntQueue_Read(List, IO)
    IMPLICIT NONE
    CHARACTER(LEN=512) :: C 
    INTEGER, INTENT(IN), OPTIONAL :: IO
    TYPE (IntQueueType), INTENT(OUT) :: List
    INTEGER :: lenT,N,I,ISS,j,NEW,isigned,K,M0,MP,MD
    CHARACTER(LEN=1) :: C1

    N=0
    ISS=0
    NEW=0
    isigned=1 
    List%numNodes = 0

    DO
       IF(PRESENT(IO))THEN
          READ(IO,'(a)')C
       ELSE
          WRITE(*,'(a)')'....Input a series of integers as a format like 0:5,8,-20:-17'
          READ(*,'(a)')C
       ENDIF

       lenT = LEN_TRIM(C)
       IF(lenT==0) EXIT
       IF(lenT>=LEN(C))THEN
          WRITE(*,*)'Warning---- IntQueue_Read :: too long line, you may lost data'
       ENDIF

       DO I=1,lenT+1
          IF(I<=lenT)THEN
             C1 = C(I:I)
          ELSE
             C1 = ' '
          ENDIF
          IF((C1>='0' .AND. C1<='9') )THEN
             N = N*10 + isigned*(ICHAR(C1)-ICHAR('0')) 
             NEW=1 
          ELSE
             IF(NEW==1 .AND. ISS==1)THEN
                M0 = List%back
                MP = N-M0
                IF(MP>0)THEN
                   MD = 1
                ELSE
                   MD = -1
                ENDIF
                DO j = 1, ABS(MP)
                   M0 = M0 + MD
                   CALL IntQueue_push(List, M0)
                ENDDO
                ISS=-1
                isigned=1 
             ELSE IF(NEW==1 .AND. ISS==2)THEN
                M0 = List%back
                IF(M0<1) CALL Error_Stop('IntQueue_Read ::  *')
                CALL IntQueue_RemoveLast (List)
                DO j = 1, M0
                   CALL IntQueue_push(List, N)
                ENDDO
                ISS=-2
                isigned=1
             ELSE IF(NEW==1)THEN
                CALL IntQueue_push(List, N)
                ISS=0 
                isigned=1 
             ENDIF
             NEW=0 
             N=0 
             IF(C1=='-')isigned=-1 
             IF(C1==':')THEN
                IF(ISS==0)THEN
                   ISS=1 
                ELSE
                   CALL Error_Stop('IntQueue_Read ::  :')
                ENDIF
             ENDIF
             IF(C1=='*')THEN
                IF(ISS==0)THEN
                   ISS=2
                ELSE
                   CALL Error_Stop('IntQueue_Read ::  *')
                ENDIF
             ENDIF
          ENDIF
       ENDDO

       IF(C(lenT:lenT)/='&') EXIT
    ENDDO

  END SUBROUTINE IntQueue_Read

  !>   
  !!   write the queue
  !<                                                                         
  SUBROUTINE IntQueue_Write(List, IO)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(IN)    :: List
    INTEGER, INTENT(IN) :: IO
    INTEGER:: NN, L
    INTEGER, DIMENSION(:), POINTER :: IA
    INTEGER :: M, I, length, lenT, Mut, M0
    CHARACTER (LEN=72) :: C
    CHARACTER (LEN=16) :: aa

    lenT = LEN(C)
    IA => List%Nodes
    NN =  List%numNodes
    C(1:lenT) = ' '

    L   = 0
    Mut = 1
    M0  = 1
    DO M=1,NN
       IF( M>M0 .AND. M<NN .AND.    &
            ABS(IA(M)-IA(M-1))==1 .AND. 2*IA(M)==IA(M-1)+IA(M+1) )THEN          
          Mut = -1
       ELSE IF( M>M0 .AND. M<NN .AND.    &
            IA(M)==IA(M-1) .AND. IA(M)==IA(M+1) )THEN
          Mut = Mut+1
       ELSE
          IF(Mut==-1)THEN
             L = L-1
             C(L:L) = ':' 
          ELSE IF(Mut>=2)THEN
             Mut = Mut + 1
             L = L-length-2
             aa = Int_to_CHAR(Mut,0)
             length = LEN(TRIM(aa))
             L = L+length
             C(L-length+1 : L)=aa(1:length)
             L=L+1
             C(L:L)='*'
          ENDIF

          aa = Int_to_CHAR(IA(M),0)
          length = LEN(TRIM(aa))
          IF(L+length+9>lenT)THEN
             C(L+1:lenT-1) = ' '
             C(lenT:lenT ) = '&'
             WRITE(IO,'(a)') C(1:lenT)
             C(1:lenT) = ' '
             L = 0
          ENDIF
          L = L+length
          C(L-length+1 : L)=aa(1:length)

          IF(M<NN)THEN
             L=L+2
             C(L-1:L)=', '
          ENDIF
          IF(Mut/=1) M0 = M+1
          Mut = 1
       ENDIF

    ENDDO

    WRITE(IO,'(a)') C(1:L)

  END SUBROUTINE IntQueue_Write


  !>   
  !!   release the memory.
  !<                                                                         
  SUBROUTINE IntQueue_Clear(List)
    IMPLICIT NONE
    TYPE (IntQueueType), INTENT(INOUT) :: List
    List%numNodes = 0
    IF(ASSOCIATED(List%back))  NULLIFY(List%back)
    IF(ASSOCIATED(List%Nodes)) DEALLOCATE(List%Nodes)
  END  SUBROUTINE IntQueue_Clear


  SUBROUTINE allc_IntQueue(ListGroup, m, message)
    IMPLICIT NONE
    TYPE (IntQueueType), DIMENSION(:), POINTER :: ListGroup, temp
    INTEGER, INTENT(IN) :: m
    CHARACTER*(*), INTENT(in), OPTIONAL :: message
    INTEGER :: m1, i

    m1 = 0
    IF(ASSOCIATED(ListGroup))THEN
       m1 = SIZE(ListGroup)
       IF(m1>m) RETURN
       ALLOCATE(temp(m1))  
       DO i = 1,m1
          IF(ASSOCIATED(ListGroup(i)%Nodes))THEN
             CALL IntQueue_Copy(ListGroup(i), temp(i))
             CALL IntQueue_Clear(ListGroup(i))
          ENDIF
       ENDDO
       DEALLOCATE(ListGroup)
    ENDIF

    ALLOCATE(ListGroup(m))

    IF(m1>0)THEN
       DO i = 1,m1
          IF(ASSOCIATED(temp(i)%Nodes))THEN
             CALL IntQueue_Copy(temp(i), ListGroup(i))
             CALL IntQueue_Clear(temp(i))
          ENDIF
       ENDDO
       DEALLOCATE(temp)
    ENDIF
  END SUBROUTINE allc_IntQueue



  SUBROUTINE deallc_IntQueue(ListGroup, message)
    IMPLICIT NONE
    TYPE (IntQueueType), DIMENSION(:), POINTER :: ListGroup
    CHARACTER*(*), INTENT(in), OPTIONAL :: message
    INTEGER :: m1, i     
    IF(ASSOCIATED(ListGroup))THEN
       m1 = SIZE(ListGroup)
       DO i = 1,m1
          CALL IntQueue_Clear(ListGroup(i))
       ENDDO
       DEALLOCATE(ListGroup)
    ENDIF
  END  SUBROUTINE deallc_IntQueue





  !>  Set a queue by a integer array.
  SUBROUTINE Real8Queue_Set(List, n, V)
    IMPLICIT NONE
    TYPE (Real8QueueType), INTENT(INOUT) :: List
    INTEGER, INTENT(IN) :: n
    REAL*8,  INTENT(IN) :: V(*)
    IF(n>keyLength(List%V))THEN
       CALL Real8Queue_Clear(List)
       ALLOCATE(List%V(n))
    ENDIF
    List%numNodes = n
    IF(n==0) RETURN
    List%V(1:n) = V(1:n)
  END SUBROUTINE Real8Queue_Set


  !>  Add an entry to the end.
  SUBROUTINE Real8Queue_Push(List, V)
    IMPLICIT NONE
    TYPE (Real8QueueType), INTENT(INOUT)    :: List
    REAL*8, INTENT(IN) :: V
    List%numNodes = List%numNodes + 1
    IF(List%numNodes>KeyLength(List%V))THEN
       IF(List%numNodes<=6)THEN
          CALL allc_real8_1Dpointer(List%V, List%numNodes+6, 'Real8Queue_Push')
       ELSE IF(List%numNodes<=100000)THEN
          CALL allc_real8_1Dpointer(List%V, 2*List%numNodes, 'Real8Queue_Push')
       ELSE
          CALL allc_real8_1Dpointer(List%V, List%numNodes+100000, 'Real8Queue_Push')
       ENDIF
    ENDIF
    List%V(List%numNodes) = V
  END SUBROUTINE Real8Queue_Push

  !>  Add an entry to the end.
  SUBROUTINE Real8Queue_Push36(List, V)
    IMPLICIT NONE
    TYPE (Real8QueueType), INTENT(INOUT)    :: List
    REAL*8, INTENT(IN) :: V
    List%numNodes = List%numNodes + 1
    IF(List%numNodes>KeyLength(List%V))   &
         CALL allc_real8_1Dpointer(List%V, List%numNodes+36, 'Real8Queue_Push36')
    List%V(List%numNodes) = V
  END SUBROUTINE Real8Queue_Push36

  !>  Add an entry to the end.
  SUBROUTINE Real8Queue_Push10k(List, V)
    IMPLICIT NONE
    TYPE (Real8QueueType), INTENT(INOUT)    :: List
    REAL*8, INTENT(IN) :: V
    List%numNodes = List%numNodes + 1
    IF(List%numNodes>KeyLength(List%V))   &
         CALL allc_real8_1Dpointer(List%V, List%numNodes+10000, 'Real8Queue_Push10k')
    List%V(List%numNodes) = V
  END SUBROUTINE Real8Queue_Push10k

  !>
  !!   And a value at the given position, and shift
  !!   the rest, so the order of entries is kept.
  !!   @param[in,out] List  : the queue of integers.
  !!   @param[in]     ID    : the entry position to add a value.
  !!   @param[in]     V     : the value to be add.
  !<
  SUBROUTINE Real8Queue_Add(List, ID, V)
    IMPLICIT NONE
    TYPE (Real8QueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: ID
    REAL*8,  INTENT(IN) :: V
    INTEGER :: i

    IF(ID>List%numNodes+1 .OR. ID<=0) CALL Error_STOP ( '--- Real8Queue_Add :: fake ID')

    List%numNodes = List%numNodes+1

    IF(List%numNodes>KeyLength(List%V))   &
         CALL allc_real8_1Dpointer(List%V, List%numNodes+36, 'Real8Queue_Add')

    IF(ID<List%numNodes)THEN
       DO i = List%numNodes, ID+1, -1
          List%V(i) = List%V(i-1)
       ENDDO
    ENDIF
    List%V(ID) = V
  END SUBROUTINE Real8Queue_Add

  !>
  !!   Remove the entry at the given position.
  !!   Replace this entry with the last value, 
  !!   so the order of entries is crushed.
  !<
  SUBROUTINE Real8Queue_RemoveNode(List, ID)
    IMPLICIT NONE
    TYPE (Real8QueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: ID
    IF(List%numNodes<ID) RETURN
    List%V(ID) = List%V(List%numNodes)
    List%numNodes = List%numNodes - 1
  END SUBROUTINE Real8Queue_RemoveNode


  !>
  !!   Remove the last entry.
  !<
  SUBROUTINE Real8Queue_RemoveLast(List)
    IMPLICIT NONE
    TYPE (Real8QueueType), INTENT(INOUT)    :: List
    IF(List%numNodes==0) RETURN
    List%numNodes = List%numNodes -1
  END SUBROUTINE Real8Queue_RemoveLast

  !>
  !!   Remove the entry at the given position and shift
  !!   the rest, so the order of entries is kept, which deffers
  !!   SUBROUTINE Real8Queue_RemoveNode()
  !<
  SUBROUTINE Real8Queue_RemoveNodeByOrder(List, ID)
    IMPLICIT NONE
    TYPE (Real8QueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: ID
    INTEGER :: j
    IF(List%numNodes<ID) RETURN
    DO j = ID, List%numNodes-1
       List%V(j) = List%V(j+1)
    ENDDO
    List%numNodes = List%numNodes - 1
  END SUBROUTINE Real8Queue_RemoveNodeByOrder

  !>
  !!   Replace the value at the given position.
  !<
  SUBROUTINE Real8Queue_ReplaceNode(List, ID, Vnew)
    IMPLICIT NONE
    TYPE (Real8QueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: ID
    REAL*8,  INTENT(IN) :: Vnew
    IF(List%numNodes<ID) RETURN
    List%V(ID) = Vnew
  END SUBROUTINE Real8Queue_ReplaceNode

  !>
  !!  Copy a Real8QueueType object to another.    \n
  !!  Reminder: NEVER copy a object to itself.
  !<
  SUBROUTINE Real8Queue_Copy(SourceList, TargetList)
    IMPLICIT NONE
    TYPE (Real8QueueType), INTENT(IN)    :: SourceList
    TYPE (Real8QueueType), INTENT(INOUT) :: TargetList
    IF(.NOT. ASSOCIATED(SourceList%V))THEN
       TargetList%numNodes = SourceList%numNodes
       RETURN
    ENDIF
    CALL Real8Queue_Set(TargetList, SourceList%numNodes, SourceList%V)
  END SUBROUTINE Real8Queue_Copy


  SUBROUTINE Real8Queue_Clear(List)
    IMPLICIT NONE
    TYPE (Real8QueueType), INTENT(INOUT) :: List
    List%numNodes = 0
    IF(ASSOCIATED(List%V))     DEALLOCATE(List%V)
  END  SUBROUTINE Real8Queue_Clear


 

  !>  Set a queue by a integer array.
  SUBROUTINE Point3dQueue_Set(List, n, Pt)
    IMPLICIT NONE
    TYPE (Point3dQueueType), INTENT(INOUT) :: List
    INTEGER, INTENT(IN) :: n
    REAL*8,  INTENT(IN) :: Pt(3,*)
       CALL Point3dQueue_Clear(List)
       ALLOCATE(List%Pt(3,n))
    List%numNodes = n
    IF(n==0) RETURN
    List%Pt(:,1:n) = Pt(:,1:n)
  END SUBROUTINE Point3dQueue_Set


  !>  Add an entry to the end.
  SUBROUTINE Point3dQueue_Push(List, Pt)
    IMPLICIT NONE
    TYPE (Point3dQueueType), INTENT(INOUT)    :: List
    REAL*8, INTENT(IN) :: Pt(3)
    List%numNodes = List%numNodes + 1
    IF(List%numNodes>KeyLength(List%Pt))THEN
       IF(List%numNodes<=6)THEN
          CALL allc_real8_2Dpointer(List%Pt, 3, List%numNodes+6, 'Point3dQueue_Push')
       ELSE IF(List%numNodes<=100000)THEN
          CALL allc_real8_2Dpointer(List%Pt, 3, 2*List%numNodes, 'Point3dQueue_Push')
       ELSE
          CALL allc_real8_2Dpointer(List%Pt, 3, List%numNodes+100000, 'Point3dQueue_Push')
       ENDIF
    ENDIF
    List%Pt(:,List%numNodes) = Pt(:)
  END SUBROUTINE Point3dQueue_Push

  !>
  !!   And a point at the given position, and shift
  !!   the rest, so the order of entries is kept.
  !!   @param[in,out] List  : the queue of integers.
  !!   @param[in]     ID    : the entry position to add a point.
  !!   @param[in]     Pt     : the point to be add.
  !<
  SUBROUTINE Point3dQueue_Add(List, ID, Pt)
    IMPLICIT NONE
    TYPE (Point3dQueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: ID
    REAL*8,  INTENT(IN) :: Pt(3)
    INTEGER :: i

    IF(ID>List%numNodes+1 .OR. ID<=0) CALL Error_STOP ( '--- Point3dQueue_Add :: fake ID')

    List%numNodes = List%numNodes+1

    IF(List%numNodes>KeyLength(List%Pt))   &
         CALL allc_real8_2Dpointer(List%Pt, 3, List%numNodes+36, 'Point3dQueue_Add')

    IF(ID<List%numNodes)THEN
       DO i = List%numNodes, ID+1, -1
          List%Pt(:,i) = List%Pt(:,i-1)
       ENDDO
    ENDIF
    List%Pt(:,ID) = Pt
  END SUBROUTINE Point3dQueue_Add

  !>
  !!   Remove the entry at the given position.
  !!   Replace this entry with the last point, 
  !!   so the order of entries is crushed.
  !<
  SUBROUTINE Point3dQueue_RemoveNode(List, ID)
    IMPLICIT NONE
    TYPE (Point3dQueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: ID
    IF(List%numNodes<ID) RETURN
    List%Pt(:,ID) = List%Pt(:,List%numNodes)
    List%numNodes = List%numNodes - 1
  END SUBROUTINE Point3dQueue_RemoveNode


  !>
  !!   Remove the last entry.
  !<
  SUBROUTINE Point3dQueue_RemoveLast(List)
    IMPLICIT NONE
    TYPE (Point3dQueueType), INTENT(INOUT)    :: List
    IF(List%numNodes==0) RETURN
    List%numNodes = List%numNodes -1
  END SUBROUTINE Point3dQueue_RemoveLast

  !>
  !!   Remove the entry at the given position and shift
  !!   the rest, so the order of entries is kept, which deffers
  !!   SUBROUTINE Point3dQueue_RemoveNode()
  !<
  SUBROUTINE Point3dQueue_RemoveNodeByOrder(List, ID)
    IMPLICIT NONE
    TYPE (Point3dQueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: ID
    INTEGER :: j
    IF(List%numNodes<ID) RETURN
    DO j = ID, List%numNodes-1
       List%Pt(:,j) = List%Pt(:,j+1)
    ENDDO
    List%numNodes = List%numNodes - 1
  END SUBROUTINE Point3dQueue_RemoveNodeByOrder

  !>
  !!   Replace the point at the given position.
  !<
  SUBROUTINE Point3dQueue_ReplaceNode(List, ID, Vnew)
    IMPLICIT NONE
    TYPE (Point3dQueueType), INTENT(INOUT)    :: List
    INTEGER, INTENT(IN) :: ID
    REAL*8,  INTENT(IN) :: Vnew(3)
    IF(List%numNodes<ID) RETURN
    List%Pt(:,ID) = Vnew(:)
  END SUBROUTINE Point3dQueue_ReplaceNode

  !>
  !!  Copy a Point3dQueueType object to another.    \n
  !!  Reminder: NEVER copy a object to itself.
  !<
  SUBROUTINE Point3dQueue_Copy(SourceList, TargetList)
    IMPLICIT NONE
    TYPE (Point3dQueueType), INTENT(IN)    :: SourceList
    TYPE (Point3dQueueType), INTENT(INOUT) :: TargetList
    IF(.NOT. ASSOCIATED(SourceList%Pt))THEN
       TargetList%numNodes = SourceList%numNodes
       RETURN
    ENDIF
    CALL Point3dQueue_Set(TargetList, SourceList%numNodes, SourceList%Pt)
  END SUBROUTINE Point3dQueue_Copy


  SUBROUTINE Point3dQueue_Clear(List)
    IMPLICIT NONE
    TYPE (Point3dQueueType), INTENT(INOUT) :: List
    List%numNodes = 0
    IF(ASSOCIATED(List%Pt))     DEALLOCATE(List%Pt)
  END  SUBROUTINE Point3dQueue_Clear

END MODULE Queue
