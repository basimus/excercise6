!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Definition of types of surface curvature with basic functions.   
!!
!<
MODULE SurfaceCurvature
  USE Geometry3DAll
  USE DiffGeometry
  USE Number_Char_Transfer
  IMPLICIT NONE

  TYPE :: CurveType
     CHARACTER ( LEN = 60 ) :: theName = ' '
     INTEGER :: ID       = 0
     INTEGER :: TopoType = 0
     INTEGER :: numNodes = 0
     REAL*8,  DIMENSION(:,:),  POINTER :: Posit => null()
     REAL*8,  DIMENSION(:,:),  POINTER :: Tangent => null()
     TYPE(CubicSegment), DIMENSION(:), POINTER :: Segm => null()
  END TYPE CurveType

  TYPE :: RegionType
     CHARACTER ( LEN = 60 ) :: theName = ' '
     INTEGER :: ID       = 0
     INTEGER :: TopoType = 0
     INTEGER :: GeoType  = 0
     INTEGER :: numNodeU = 0
     INTEGER :: numNodeV = 0
     INTEGER :: numCurve = 0

     !>  The IDs of surrounding curves.
     INTEGER, DIMENSION(:), POINTER :: IC => null()

     !>  Posit(1:3,iu,iv): the 3D position \f$ x, y, z \f$
     !!                    of a supporting node (iu,iv) with 1<=iu<=numNodeU, 1<=iv<=numNodeV.
     REAL*8,  DIMENSION(:,:,:),  POINTER :: Posit => null()

     !>  TangentU(1:3,iu,iv):
     !!  \f$  \partial x / \partial u, \partial y / \partial u, \partial z / \partial u \f$
     !!                    of a supporting node (iu,iv) with 1<=iu<=numNodeU, 1<=iv<=numNodeV.
     REAL*8,  DIMENSION(:,:,:),  POINTER :: TangentU => null()

     !>  TangentV(1:3,iu,iv):
     !!  \f$  \partial x / \partial v, \partial y / \partial v, \partial z / \partial v \f$
     !!                    of a supporting node (iu,iv) with 1<=iu<=numNodeU, 1<=iv<=numNodeV.
     REAL*8,  DIMENSION(:,:,:),  POINTER :: TangentV => null()

     !>  TangentUV(1:3,iu,iv):
     !!  \f$  \partial^2 x / \partial u \partial v, \partial^2 y / \partial u  \partial v, \partial^2 z / \partial u  \partial v\f$
     !!                    of a supporting node (iu,iv) with 1<=iu<=numNodeU, 1<=iv<=numNodeV.
     REAL*8,  DIMENSION(:,:,:),  POINTER :: TangentUV => null()

     !>  Zone(iu,iv) records the coefficients of the zone {(u,v): iu<u<iu+1, iv<v<iv+1}.
     TYPE(CubicZone), DIMENSION(:,:), POINTER :: Zone => null()
  END TYPE RegionType

  TYPE :: SurfaceCurvatureType
     CHARACTER ( LEN = 80 ) :: CurvatureName = ' '
     INTEGER :: NB_Curve  = 0
     INTEGER :: NB_Region = 0
     TYPE(CurveType),   DIMENSION(:), POINTER :: Curves => null() 
     TYPE(RegionType), DIMENSION(:), POINTER :: Regions => null()
  END TYPE SurfaceCurvatureType

CONTAINS

  SUBROUTINE CurveType_BuildTangent(Curve)
    IMPLICIT NONE
    TYPE(CurveType), INTENT(INOUT) :: Curve
    INTEGER :: iu, i
    REAL*8, DIMENSION(:), POINTER :: pList, qList
    ALLOCATE(Curve%Tangent(3,Curve%numNodes))
    ALLOCATE(Curve%Segm(Curve%numNodes-1))
    ALLOCATE(pList(Curve%numNodes), qList(Curve%numNodes))
    DO i = 1,3
       pList(1:Curve%numNodes) = Curve%Posit(i, 1:Curve%numNodes)
       CALL DiffGeo_Spline(2, Curve%numNodes, pList, qList)
       Curve%Tangent(i, 1:Curve%numNodes) = qList(1:Curve%numNodes)
    ENDDO
    DO iu = 1, Curve%numNodes-1
       CALL CubicSegment_Build(            &
            Curve%Posit(:,iu),  Curve%Tangent(:,iu),  &
            Curve%Posit(:,iu+1),Curve%Tangent(:,iu+1), Curve%Segm(iu) )
    ENDDO
    DEALLOCATE(pList, qList)
  END SUBROUTINE CurveType_BuildTangent

  SUBROUTINE RegionType_BuildTangent(Region)
    IMPLICIT NONE
    TYPE(RegionType) :: Region
    INTEGER :: nu, nv, id, ip, iu, iv, i, iud(4), ivd(4)
    REAL*8  :: Ps(3,4), PsU(3,4), PsV(3,4), PsUV(3,4)
    REAL*8, DIMENSION(:), POINTER :: pListU, pListV, qListU, qListV

    nu = Region%numNodeU
    nv = Region%numNodeV
    ALLOCATE(Region%TangentU (3,nu,nv))
    ALLOCATE(Region%TangentV (3,nu,nv))
    ALLOCATE(Region%TangentUV(3,nu,nv))
    ALLOCATE(Region%Zone(nu-1, nv-1))
    ALLOCATE(pListU(nu), qListU(nu))
    ALLOCATE(pListV(nv), qListV(nv))

    DO id = 1,3
       DO ip = 1,nu
          pListV(1:nv) = Region%Posit(id,ip,1:nv)
          CALL DiffGeo_Spline(2, nv, pListV, qListV)
          Region%TangentV(id,ip,1:nv) = qListV(1:nv)
       ENDDO
       DO ip = 1,nv
          pListU(1:nu) = Region%Posit(id,1:nu,ip)
          CALL DiffGeo_Spline(2, nu, pListU, qListU)
          Region%TangentU(id,1:nu,ip) = qListU(1:nu)
       ENDDO

       DO ip = 1, nu, nu-1
          pListV(1:nv) = Region%TangentU(id,ip,1:nv)
          CALL DiffGeo_Spline(2, nv, pListV, qListV)
          Region%TangentUV(id,ip,1:nv) = qListV(1:nv)
       ENDDO

       DO ip = 1,nv
          pListU(1:nu) = Region%TangentV( id, 1:nu, ip)
          qListU(1)    = Region%TangentUV(id, 1,    ip)
          qListU(nu)   = Region%TangentUV(id, nu,   ip)          
          CALL DiffGeo_Spline(1, nu, pListU, qListU)
          Region%TangentUV(id,1:nu,ip) = qListU(1:nu)
       ENDDO
    ENDDO


    DO iu = 1, nu-1
       DO iv = 1, nv-1
          iud = (/iu, iu+1, iu, iu+1/)
          ivd = (/iv, iv, iv+1, iv+1/)
          DO i=1,4
             Ps(:,i)   = Region%Posit    (:, iud(i), ivd(i))
             PsU(:,i)  = Region%TangentU (:, iud(i), ivd(i))
             PsV(:,i)  = Region%TangentV (:, iud(i), ivd(i))
             PsUV(:,i) = Region%TangentUV(:, iud(i), ivd(i))
          ENDDO
          CALL CubicZone_Build(Ps, PsU, PsV,PsUV, Region%Zone(iu,iv))
       ENDDO
    ENDDO

    DEALLOCATE(pListU, qListU, pListV, qListV)

  END SUBROUTINE RegionType_BuildTangent


  !>
  !!   Copy a curve to another.
  !!   @param[in]   Curve1  : the source curve.
  !!   @param[out]  Curve2  : the target curve.
  !!
  !!   Reminder: NEVER copy a curve to itself.
  !<
  SUBROUTINE CurveType_Copy(Curve1, Curve2)
    IMPLICIT NONE
    TYPE(CurveType), INTENT(IN)    :: Curve1
    TYPE(CurveType), INTENT(INOUT) :: Curve2
    INTEGER :: n, i

    n = Curve1%numNodes
    IF(ASSOCIATED(Curve2%Posit)) DEALLOCATE(Curve2%Posit)
    ALLOCATE(Curve2%Posit(3,n))
    Curve2%theName  = Curve1%theName
    Curve2%ID       = Curve1%ID
    Curve2%TopoType = Curve1%TopoType
    Curve2%numNodes = n
    DO i = 1, n
       Curve2%Posit(:,i) = Curve1%Posit(:,i)
    ENDDO

  END SUBROUTINE CurveType_Copy

  !>
  !!   Copy a Region to another.
  !!   @param[in]   Region1  : the source region.
  !!   @param[out]  Region2  : the target region.
  !!
  !!   Reminder: NEVER copy a region to itself.
  !<
  SUBROUTINE RegionType_Copy(Region1,Region2)
    IMPLICIT NONE
    TYPE(RegionType), INTENT(IN)    :: Region1
    TYPE(RegionType), INTENT(inOUT) :: Region2
    INTEGER :: nu, nv, nn, iv, iu

    nn = Region1%numCurve
    nu = Region1%numNodeU
    nv = Region1%numNodeV
    IF(ASSOCIATED(Region2%IC)) DEALLOCATE(Region2%IC)
    ALLOCATE(Region2%IC(nn))
    IF(ASSOCIATED(Region2%Posit)) DEALLOCATE(Region2%Posit)
    ALLOCATE(Region2%Posit(3,nu,nv))
    Region2%theName  = Region1%theName
    Region2%ID       = Region1%ID
    Region2%TopoType = Region1%TopoType
    Region2%GeoType  = Region1%GeoType
    Region2%numNodeU = nu
    Region2%numNodeV = nv
    Region2%numCurve = nn
    Region2%IC(1:nn) = Region1%IC(1:nn)
    DO iv=1,nv
       DO iu=1,nu
          Region2%Posit(:,iu,iv) = Region1%Posit(:,iu,iv)
       ENDDO
    ENDDO

  END SUBROUTINE RegionType_Copy


  !>
  !!  Input a surface definition by reading a *.dat file. 
  !!  @param[in]  JobName  the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[out] Curv     a surface curvature.
  !<
  SUBROUTINE SurfaceCurvature_Input(JobName,JobNameLength,Curv)
    IMPLICIT NONE
    TYPE(SurfaceCurvatureType), INTENT(INOUT) :: Curv
    CHARACTER(LEN=*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: icv, isf, id, it, np, nu, nv, ip, iu, iv
    INTEGER :: ncsg,nsrg,nshet,nwire, js,idcv,idsf,idm, nn, Isucc
    CHARACTER ( LEN = 80 ) :: String
    CHARACTER ( LEN = 60 ) :: C1,C2,theName 
    
    OPEN(1,file=JobName(1:JobNameLength)//'.dat', status='old')

    READ(1,'(a)') Curv%CurvatureName
    READ(1,*) Curv%NB_Curve, Curv%NB_Region
    ALLOCATE(Curv%Curves (Curv%NB_Curve) )
    ALLOCATE(Curv%Regions(Curv%NB_Region))

    !---  'Reading curves'
    READ(1,*) 
    DO icv = 1,Curv%NB_Curve
       READ(1,'(a)')String
       CALL CHAR_SPLIT(String, C1, C2)
       id = CHAR_to_INT(C1,Isucc)
       IF(Isucc==0) CALL Error_STOP ( 'SurfaceCurvature_Input:: Isucc=0')
       IF(id <= 0)  CALL Error_STOP ( 'SurfaceCurvature_Input:: id<0')
       CALL CHAR_SPLIT(C2, C1, theName)
       it = CHAR_to_INT(C1,Isucc)
       IF(Isucc==0) CALL Error_STOP ( 'SurfaceCurvature_Input:: Isucc=0')
       
       READ(1,*) np 
       Curv%Curves(icv)%ID       = ID
       Curv%Curves(icv)%TopoType = IT
       Curv%Curves(icv)%numNodes = np
       Curv%Curves(icv)%theName  = theName
       ALLOCATE(Curv%Curves(icv)%Posit(3,np))
       DO ip=1,np
          READ(1,*) Curv%Curves(icv)%Posit(:,ip)
       ENDDO
    ENDDO

    !---  'Reading Surfaces'
    READ(1,*)
    DO isf = 1,Curv%NB_Region
       READ(1,'(a)')String
       CALL CHAR_SPLIT(String, C1, C2)
       id = CHAR_to_INT(C1,Isucc)
       IF(Isucc==0) CALL Error_STOP ( 'SurfaceCurvature_Input:: Isucc=0')
       IF(id <= 0)  CALL Error_STOP ( 'SurfaceCurvature_Input:: id<0')
       CALL CHAR_SPLIT(C2, C1, theName)
       it = CHAR_to_INT(C1,Isucc)
       IF(Isucc==0) CALL Error_STOP ( 'SurfaceCurvature_Input:: Isucc=0')

       READ(1,*) nu,nv
       Curv%Regions(isf)%ID       = ID
       Curv%Regions(isf)%TopoType = IT
       Curv%Regions(isf)%numNodeU = nu
       Curv%Regions(isf)%numNodeV = nv
       Curv%Regions(isf)%theName  = theName
       ALLOCATE(Curv%Regions(isf)%Posit(3,nu,nv))
       DO iv=1,nv
          DO iu=1,nu
             READ(1,*) Curv%Regions(isf)%Posit(:,iu,iv)
          ENDDO
       ENDDO
    ENDDO

    !---  'Reading Topology Header'
    READ(1,*)
    READ(1,*) ncsg,nsrg,nshet,nwire

    IF(ncsg/=Curv%NB_Curve)  CALL Error_STOP ( 'ncsg/=Curv%NB_Curve')
    IF(nsrg/=Curv%NB_Region) CALL Error_STOP ( 'nsrg/=Curv%NB_Region')

    READ(1,*)
    DO icv = 1,ncsg
       READ(1,*) js,idcv,it       ! No, associated curve ID, type
       IF(js/=icv .OR. idcv/=icv .OR. it/=Curv%Curves(icv)%TopoType) CALL Error_STOP ( 'js,idcv,it')
    ENDDO

    !---  'Reading Topology Regions'
    READ(1,*)
    DO isf = 1,Curv%NB_Region
       READ(1,*) js,idsf,it, idm
       IF(js/=isf .OR. idsf/=isf .OR. it/=Curv%Regions(isf)%TopoType) CALL Error_STOP ( 'js,idsf,it')
       Curv%Regions(isf)%GeoType = idm
       READ(1,*) nn
       Curv%Regions(isf)%numCurve = nn
       ALLOCATE(Curv%Regions(isf)%IC(nn))
       READ(1,*) Curv%Regions(isf)%IC(1:nn)
    ENDDO

    CLOSE(1)
    RETURN
  END SUBROUTINE SurfaceCurvature_Input


  !>
  !!  Input a surface definition by reading an CADfix-version *.dat file. 
  !!  @param[in]  JobName  the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[out] Curv     a surface curvature.
  !<
  SUBROUTINE SurfaceCurvature_Input_old(JobName,JobNameLength,Curv)
    IMPLICIT NONE
    TYPE(SurfaceCurvatureType), INTENT(INOUT) :: Curv
    CHARACTER(LEN=*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: icv, isf, id, it, np, nu, nv, ip, iu, iv
    INTEGER :: ncsg,nsrg,nshet,nwire, js,idcv,idsf,idm, nn

    OPEN(1,file=JobName(1:JobNameLength)//'.dat', status='old')

    READ(1,'(a)') Curv%CurvatureName
    READ(1,*) Curv%NB_Curve, Curv%NB_Region
    ALLOCATE(Curv%Curves (Curv%NB_Curve) )
    ALLOCATE(Curv%Regions(Curv%NB_Region))

    !---  'Reading curves'
    READ(1,*) 
    DO icv = 1,Curv%NB_Curve
       READ(1,*) id,it, np
       IF( id <= 0 )  CALL Error_STOP ( 'id<0')
       Curv%Curves(icv)%ID       = ID
       Curv%Curves(icv)%TopoType = IT
       Curv%Curves(icv)%numNodes = np
       ALLOCATE(Curv%Curves(icv)%Posit(3,np))
       DO ip=1,np
          READ(1,*) Curv%Curves(icv)%Posit(:,ip)
       ENDDO
    ENDDO

    !---  'Reading Surfaces'
    READ(1,*)
    DO isf = 1,Curv%NB_Region
       READ(1,*) id,it, nu,nv
       Curv%Regions(isf)%ID       = ID
       Curv%Regions(isf)%TopoType = IT-1
       Curv%Regions(isf)%numNodeU = nu
       Curv%Regions(isf)%numNodeV = nv
       ALLOCATE(Curv%Regions(isf)%Posit(3,nu,nv))
       DO iv=1,nv
          DO iu=1,nu
             READ(1,*) Curv%Regions(isf)%Posit(:,iu,iv)
          ENDDO
       ENDDO
    ENDDO

    !---  'Reading Topology Regions'
    READ(1,*)
    DO isf = 1,Curv%NB_Region
       READ(1,*) js,nn,idsf
       IF(js/=isf .OR. idsf/=isf) CALL Error_STOP ( 'js,idsf')
       Curv%Regions(isf)%GeoType = 1
       Curv%Regions(isf)%numCurve = nn
       ALLOCATE(Curv%Regions(isf)%IC(nn))
       READ(1,*) Curv%Regions(isf)%IC(1:nn)
    ENDDO

    CLOSE(1)
    RETURN
  END SUBROUTINE SurfaceCurvature_Input_old


  !>
  !!  Output a surface definition by writting a *.dat file. 
  !!  @param[in]  JobName  the name (prefix) of output file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[out] Curv     a surface curvature.
  !<
  SUBROUTINE SurfaceCurvature_Output(JobName,JobNameLength,Curv)
    IMPLICIT NONE
    TYPE(SurfaceCurvatureType), INTENT(IN) :: Curv
    CHARACTER(LEN=*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: icv, isf, id, it, np, nu, nv, ip, iu, iv
    INTEGER :: ncsg,nsrg,nshet,nwire, js,idcv,idsf,idm, nn

    OPEN(1,file=JobName(1:JobNameLength)//'.dat',   &
         status='UNKNOWN',form='FORMATTED')

    WRITE(1,'(a)') Curv%CurvatureName
    WRITE(1,*) Curv%NB_Curve, Curv%NB_Region

    !---  'writting curves'
    WRITE(1,*) 'Curve Components'
    DO icv = 1,Curv%NB_Curve
       WRITE(1,'(2I8,2x,a)') Curv%Curves(icv)%ID, Curv%Curves(icv)%TopoType, TRIM(Curv%Curves(icv)%theName)
       WRITE(1,*) Curv%Curves(icv)%numNodes 
       DO ip=1,Curv%Curves(icv)%numNodes
          WRITE(1,'(3(1x,E16.9))') Curv%Curves(icv)%Posit(:,ip)
       ENDDO
    ENDDO

    !---  'writting Surfaces'
    WRITE(1,*)'Surface Components'
    DO isf = 1,Curv%NB_Region
       WRITE(1,'(2I8,2x,a)') Curv%Regions(isf)%ID, Curv%Regions(isf)%TopoType, TRIM(Curv%Regions(isf)%theName)
       WRITE(1,*) Curv%Regions(isf)%numNodeU, Curv%Regions(isf)%numNodeV
       DO iv=1,Curv%Regions(isf)%numNodeV
          DO iu=1,Curv%Regions(isf)%numNodeU
             WRITE(1,'(3(1x,E16.9))') Curv%Regions(isf)%Posit(:,iu,iv)
          ENDDO
       ENDDO
    ENDDO

    !---  'writting Topology Header'
    WRITE(1,*)'Regions Definition'
    WRITE(1,*) Curv%NB_Curve, Curv%NB_Region, 0,0

    WRITE(1,*)'Curve Information'
    DO icv = 1,Curv%NB_Curve
       WRITE(1,*) icv,icv,Curv%Curves(icv)%TopoType
    ENDDO

    !---  'writting Topology Regions'
    WRITE(1,*)'Surface Region Connectivity'
    DO isf = 1,Curv%NB_Region
       WRITE(1,*) isf,isf,Curv%Regions(isf)%TopoType,Curv%Regions(isf)%GeoType
       WRITE(1,*) Curv%Regions(isf)%numCurve
       WRITE(1,*) Curv%Regions(isf)%IC(1:Curv%Regions(isf)%numCurve)
    ENDDO

    CLOSE(1)
    RETURN
  END SUBROUTINE SurfaceCurvature_Output

  !>
  !!  Output a surface definition by writting an CADfix-version *.dat file. 
  !!  @param[in]  JobName  the name (prefix) of output file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[out] Curv     a surface curvature.
  !<
  SUBROUTINE SurfaceCurvature_Output_old(JobName,JobNameLength,Curv)
    IMPLICIT NONE
    TYPE(SurfaceCurvatureType), INTENT(IN) :: Curv
    CHARACTER(LEN=*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: icv, isf, id, it, np, nu, nv, ip, iu, iv
    INTEGER :: ncsg,nsrg,nshet,nwire, js,idcv,idsf,idm, nn

    OPEN(1,file=JobName(1:JobNameLength)//'.dat',   &
         status='UNKNOWN',form='FORMATTED')

    WRITE(1,'(a)') 'Flite3D file created from CADfix model: '//   &
         TRIM(Curv%CurvatureName)//'_tailsection'
    WRITE(1,*) Curv%NB_Curve, Curv%NB_Region

    !---  'writting curves'
    WRITE(1,'(a)') ' Intersection lines:'
    DO icv = 1,Curv%NB_Curve
       WRITE(1,'(3I8,2x,a)') Curv%Curves(icv)%ID, Curv%Curves(icv)%TopoType,     &
            Curv%Curves(icv)%numNodes, TRIM(Curv%Curves(icv)%theName)
       DO ip=1,Curv%Curves(icv)%numNodes
          WRITE(1,'(3(1x,E16.9))') Curv%Curves(icv)%Posit(:,ip)
       ENDDO
    ENDDO

    !---  'writting Surfaces'
    WRITE(1,'(a)')' Surface Patches:'
    DO isf = 1,Curv%NB_Region
       WRITE(1,'(4I8,2x,a)') Curv%Regions(isf)%ID,       Curv%Regions(isf)%TopoType+1,    &
            Curv%Regions(isf)%numNodeU, Curv%Regions(isf)%numNodeV,    &
            TRIM(Curv%Regions(isf)%theName)
       DO iv=1,Curv%Regions(isf)%numNodeV
          DO iu=1,Curv%Regions(isf)%numNodeU
             WRITE(1,'(3(1x,E16.9))') Curv%Regions(isf)%Posit(:,iu,iv)
          ENDDO
       ENDDO
    ENDDO

    !---  'writting Topology Regions'
    WRITE(1,'(a)')' Boundary lines:'
    DO isf = 1,Curv%NB_Region
       WRITE(1,*) isf, Curv%Regions(isf)%numCurve, isf
       WRITE(1,*) Curv%Regions(isf)%IC(1:Curv%Regions(isf)%numCurve)
    ENDDO

    CLOSE(1)
    RETURN
  END SUBROUTINE SurfaceCurvature_Output_old


  !>
  !!  Calculate the tangent direction at every node of the geometry.
  !!  This must be prepared before the curvature being used.
  !<
  SUBROUTINE SurfaceCurvature_BuildTangent(Curv)
    IMPLICIT NONE
    TYPE(SurfaceCurvatureType), INTENT(INOUT) :: Curv
    INTEGER :: i
    DO i=1,Curv%NB_Curve
       CALL CurveType_BuildTangent(Curv%Curves(i))
    ENDDO
    DO i=1,Curv%NB_Region
       CALL RegionType_BuildTangent(Curv%Regions(i))
    ENDDO
  END SUBROUTINE SurfaceCurvature_BuildTangent

  !>
  !!    (Code from Oubay)                                                         
  !!    Returns the 3D coordinates of a point on a Ferguson curve segment         
  !!      by given the local coordinate: 0<=un<=N-1 .                              
  !!   @param[in] koutput =0 ...... returns position vector R only.   \n                      
  !!                      =1 ...... also returns derivative Rp.       \n              
  !!                      =2 ...... also returns second order derivative Rpp.   
  !!   @param[in]  Curve  
  !!   @param[in]  un 
  !!   @param[out] R,Rp,Rpp
  !<
  SUBROUTINE CurveType_Interpolate(Curve,koutput,un,R, Rp, Rpp)
    IMPLICIT NONE
    TYPE(CurveType), INTENT(in) :: Curve
    INTEGER, INTENT(in) :: koutput
    REAL*8, INTENT(in)  :: un
    REAL*8, INTENT(out) :: R(3)
    REAL*8, OPTIONAL, INTENT(out) :: Rp(3), Rpp(3)
    INTEGER :: iu
    REAL*8  :: u
    iu = INT(un) +1
    IF(iu>=Curve%numNodes) iu = Curve%numNodes -1
    u  = un+1 -iu
    IF(koutput==0)THEN
       CALL CubicSegment_Interpolate(koutput, Curve%Segm(iu), u, R)
    ELSE IF(koutput==1 .AND. PRESENT(Rp))THEN
       CALL CubicSegment_Interpolate(koutput, Curve%Segm(iu), u, R, Rp)
    ELSE IF(koutput==2 .AND. PRESENT(Rpp))THEN
       CALL CubicSegment_Interpolate(koutput, Curve%Segm(iu), u, R, Rp, Rpp)
    ELSE
       WRITE(*,*) 'Error--- koutput against optional arguments.'
       CALL Error_STOP ( '--- CurveType_Interpolate')
    ENDIF
    RETURN
  END SUBROUTINE CurveType_Interpolate

  !>
  !!    (Code from Oubay)                                                         
  !!    Returns the 3D coordinates of a point on a Ferguson partch                
  !!     by given the global coordinates 0<un<nu-1; 0<vn<nv-1                     
  !!   @param[in] koutput =0 ...... returns position vector R only.   \n                      
  !!                      =1 ...... also returns (R,u); (R,v) and (R,uv) .       \n              
  !!                      =2 ...... also returns (R,uu) and (R,vv).   
  !!   @param[in]  Region  
  !!   @param[in]  un,vn   0<un<Region\%numNodeU-1; 0<vn<Region\%numNodeV-1  
  !!   @param[out] R,Ru,Rv,Ruv,Ruu,Rvv
  !<
  SUBROUTINE RegionType_Interpolate(Region,koutput,un,vn,R,Ru,Rv,Ruv,Ruu,Rvv)
    IMPLICIT NONE
    TYPE(RegionType), INTENT(in) :: Region
    INTEGER, INTENT(in) :: koutput
    REAL*8, INTENT(in)  :: un, vn
    REAL*8, INTENT(out) :: R(3)
    REAL*8, OPTIONAL, INTENT(out) :: Ru(3), Rv(3), Ruv(3), Ruu(3), Rvv(3)
    INTEGER :: id, iu, iv
    REAL*8  :: UBOT, VBOT, UTOP, VTOP, u, v
    REAL*8  :: tol = 1.d-8

    UBOT = -tol
    VBOT = -tol
    UTOP = Region%numNodeU -1 + tol
    VTOP = Region%numNodeV -1 + tol
    IF(un<UBOT .OR. un>UTOP .OR. vn<VBOT .OR. vn>VTOP) THEN
       WRITE(*,*) 'Error--- out of boundary. ID=', Region%ID
       WRITE(*,*) '  un, UBOT, UTOP, tol =',un, 0, Region%numNodeU -1, REAL(tol)
       WRITE(*,*) '  vn, VBOT, VTOP, tol =',vn, 0, Region%numNodeV -1, REAL(tol)
       CALL Error_STOP ( '--- RegionType_Interpolate')
    ENDIF

    !--- finds the patch containing the point un,vn
    iu = INT(un) +1
    iv = INT(vn) +1
    IF(iu>=Region%numNodeU) iu = Region%numNodeU -1
    IF(iv>=Region%numNodeV) iv = Region%numNodeV -1
    u  = un+1 -iu
    v  = vn+1 -iv

    !--- computes the values of the position, tangent and
    !     and twist vectors from the local coordinates 0<u,v<1 
    IF(koutput==0)THEN
       CALL CubicZone_Interpolate(koutput,Region%Zone(iu,iv),u,v,R)
    ELSE IF(koutput==1 .AND. PRESENT(Ruv))THEN
       CALL CubicZone_Interpolate(koutput,Region%Zone(iu,iv),u,v,R,Ru,Rv,Ruv)
    ELSE IF(koutput==2 .AND. PRESENT(Rvv))THEN
       CALL CubicZone_Interpolate(koutput,Region%Zone(iu,iv),u,v,R,Ru,Rv,Ruv,Ruu,Rvv)
    ELSE
       WRITE(*,*) 'Error--- koutput against optional arguments.'
       CALL Error_STOP ( '--- RegionType_Interpolate')
    ENDIF

    RETURN
  END SUBROUTINE RegionType_Interpolate

  !>
  !!    (Code from Oubay)                                                         
  !!   Computes the maximum stretching ratio of a Region .           
  !!   @param[in]  Region  
  !!   @param[out] smin : the minimum length of a segment.
  !!   @param[out] smax : the maximum length of a segment.
  !!   @param[out] smax : the maximum stretch of a zone.
  !<
  SUBROUTINE RegionType_StretchCheck(Region, smin, smax, stmx)
    IMPLICIT NONE
    TYPE(RegionType), INTENT(in) :: Region
    REAL*8, INTENT(out) :: smin, smax, stmx
    INTEGER :: iu, iv
    REAL*8  :: eps, s12, s13, s24, s34, sm1, sm2, stre
    TYPE(CubicSegment) :: Segm
    REAL*8  :: a(5)
    INTEGER :: Ierr

    eps  = 1.d-05
    stmx = 0
    smin = 1.d38
    smax = 0
    Ierr = 1
    DO iv = 1, Region%numNodeV -1
       DO iu = 1, Region%numNodeU -1
          CALL CubicSegment_Build(                                                  &
               Region%Posit(:,iu,  iv),   Region%TangentU(:,iu,  iv),               &
               Region%Posit(:,iu+1,iv),   Region%TangentU(:,iu+1,iv),    segm)
          CALL CubicSegment_Length(Segm,a)
          s12 = Segm%Length
          CALL CubicSegment_Build(                                                  &
               Region%Posit(:,iu,  iv+1), Region%TangentU(:,iu,  iv+1),             &
               Region%Posit(:,iu+1,iv+1), Region%TangentU(:,iu+1,iv+1),  segm)
          CALL CubicSegment_Length(Segm,a)
          s34 = Segm%Length
          CALL CubicSegment_Build(                                                  &
               Region%Posit(:,iu,iv  ),   Region%TangentV(:,iu,iv  ),               &
               Region%Posit(:,iu,iv+1),   Region%TangentV(:,iu,iv+1),    segm)
          CALL CubicSegment_Length(Segm,a)
          s13 = Segm%Length
          CALL CubicSegment_Build(                                                  &
               Region%Posit(:,iu+1,iv  ), Region%TangentV(:,iu+1,iv  ),             &
               Region%Posit(:,iu+1,iv+1), Region%TangentV(:,iu+1,iv+1),  segm)
          CALL CubicSegment_Length(Segm,a)
          s24 = Segm%Length
          sm1 = MIN(s12,s13,s24,s34)
          sm2 = MAX(s12,s13,s24,s34)
          IF(sm1>eps*sm2) THEN
             stre = sm2/sm1
          ELSE
             IF(Ierr>0)THEN
                WRITE(*, '(a,i4,a,i4,a)')  ' Warning--- singular patch at ( ',iu,', ',iv,' )'
                Ierr = Ierr - 1
             ELSE IF(Ierr==0)THEN
                WRITE(*, '(a)')  '     and more ----'
                Ierr = Ierr - 1
             ENDIF
             stre = 1.d0 / eps
          ENDIF
          stmx = MAX(stmx,stre)
          smin = MIN(smin, sm1)
          smax = MAX(smax, sm2)
       ENDDO
    ENDDO

    RETURN
  END SUBROUTINE RegionType_StretchCheck



  !>
  !!  Write a .geo file for EnSight.
  !!  @param[in]  JobName        the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  Curv           the surface curvature.
  !<
  SUBROUTINE SurfaceCurvature_OutputEnSight(JobName,JobNameLength,Curv)
    IMPLICIT NONE
    TYPE(SurfaceCurvatureType), INTENT(IN) :: Curv
    CHARACTER(LEN=*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: i, j, ip, is, nbn, nbp, ic
    INTEGER :: marke(10000), markc(10000), mark(10000), nas, nas2, na
    CHARACTER*80 :: string
    CHARACTER*4  :: fileExtension 
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: ips
    REAL*4,  DIMENSION(:,:), ALLOCATABLE :: xyz

    WRITE(*,'((a))')'Input the order numbers of surfaces:'
    WRITE(*,'((a))')'   (  0 for no surface; -9 for all surfaces; or.. )'
    CALL read_int_array(marke,nas)

    IF(nas>0 .AND. marke(1)==-9)THEN
       nas = Curv%NB_Region
       DO na = 1,nas
          marke(na) = na
       ENDDO
    ELSE IF(nas>0 .AND. marke(1)==0)THEN
       nas = 0
    ENDIF

    WRITE(*,'((a))')'Input the order numbers of curvatures:'
    WRITE(*,'((a))')'   (  0 for no curvature; -9 for all curvatures; '
    WRITE(*,'((a))')'    -99 for those surrounding faces; or...)'
    CALL read_int_array(markc,nas2)

    IF(nas2>0 .AND. markc(1)==-9)THEN
       nas2 = Curv%NB_Curve
       DO na = 1, nas2
          markc(na) = na
       ENDDO
    ELSE IF(nas2>0 .AND. markc(1)==-99)THEN

       nas2 = 0
       mark(:) = 0
       DO na = 1,nas
          is = marke(na)
          IF(is<=0 .OR. is>Curv%NB_Region) CYCLE

          DO j = 1,Curv%Regions(is)%numCurve
             ic = Curv%Regions(is)%IC(j)
             if(mark(ic)==0)then
                mark(ic) = 1
                nas2 = nas2 + 1
                markc(nas2) = ic
             endif
          ENDDO
       ENDDO
       
    ELSE IF(nas2>0 .AND. markc(1)==0)THEN
       nas2 = 0
    ENDIF
   
    OPEN(1,file=JobName(1:JobNameLength)//'.geo',    &
         status='UNKNOWN',form='UNFORMATTED',err=111)

    nbp = 0
    DO is = 1,Curv%NB_Curve
       nbp = MAX(nbp, Curv%Curves(is)%numNodes)
    ENDDO
    DO is = 1,Curv%NB_Region
       nbp = MAX(nbp, Curv%Regions(is)%numNodeV * Curv%Regions(is)%numNodeU)
    ENDDO
    ALLOCATE (xyz(3,nbp), ips(4,nbp))

    string = 'Fortran Binary' 
    WRITE(1,err=111) string
    string = 'ENSIGHT'
    WRITE(1,err=111) string
    string = 'GEO FILE'
    WRITE(1,err=111) string
    string = 'node id off'
    WRITE(1,err=111) string
    string = 'element id off'
    WRITE(1,err=111) string

    DO na = 1,nas2
       is = markc(na)
       nbp = Curv%Curves(is)%numNodes
       nbn = nbp-1
       DO i = 1,nbp
          xyz(:,i) = Curv%Curves(is)%Posit(:,i)
       ENDDO

       string = 'part'
       WRITE(1,err=111) string
       WRITE(1,err=111) na
       WRITE(fileExtension,'(i4)') is
       string = 'Curve '//fileExtension
       WRITE(1,err=111) string
       string = 'coordinates'
       WRITE(1,err=111) string
       WRITE(1,err=111) nbp
       WRITE(1,err=111) xyz(1,1:nbp)
       WRITE(1,err=111) xyz(2,1:nbp)
       WRITE(1,err=111) xyz(3,1:nbp)

       string = 'bar2'
       WRITE(1,err=111) string
       WRITE(1,err=111) nbn
       WRITE(1,err=111) (i,i+1, i=1,nbn)
    ENDDO

    DO na = 1,nas
       is = marke(na)
       nbp = 0
       nbn = 0
       DO i = 1,Curv%Regions(is)%numNodeU
          DO j = 1,Curv%Regions(is)%numNodeV
             nbp = nbp + 1
             xyz(:,nbp) = Curv%Regions(is)%Posit(:,i,j)
             IF(i<Curv%Regions(is)%numNodeU .AND. j<Curv%Regions(is)%numNodeV)THEN
                nbn = nbn+1
                ips(1,nbn) = nbp
                ips(2,nbn) = nbp + Curv%Regions(is)%numNodeV
                ips(3,nbn) = nbp + Curv%Regions(is)%numNodeV + 1
                ips(4,nbn) = nbp + 1
             ENDIF
          ENDDO
       ENDDO

       string = 'part'
       WRITE(1,err=111) string
       WRITE(1,err=111) na + nas2
       WRITE(fileExtension,'(i4)') is
       string = 'Face section '//fileExtension
       WRITE(1,err=111) string
       string = 'coordinates'
       WRITE(1,err=111) string
       WRITE(1,err=111) nbp
       WRITE(1,err=111) xyz(1,1:nbp)
       WRITE(1,err=111) xyz(2,1:nbp)
       WRITE(1,err=111) xyz(3,1:nbp)

       string = 'quad4'
       WRITE(1,err=111) string
       WRITE(1,err=111) nbn
       WRITE(1,err=111) ((ips(j,i),j=1,4), i=1,nbn)
    ENDDO

    DEALLOCATE (xyz, ips)
    CLOSE(1)
    RETURN
111 WRITE(*,*) 'Error--- SurfaceCurvature_OutputEnSight: ',      &
         JobName(1:JobNameLength)//'.geo'
    CALL Error_STOP (' ')

  END SUBROUTINE SurfaceCurvature_OutputEnSight


  !>
  !!  Write region structures to a .net1 file
  !!        curves to a .net2 file for gnuplot.
  !!  @param[in]  JobName        the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  Curv           the surface curvature.
  !<
  SUBROUTINE SurfaceCurvature_OutputNet(JobName,JobNameLength,Curv)
    IMPLICIT NONE
    TYPE(SurfaceCurvatureType), INTENT(IN) :: Curv
    CHARACTER(LEN=*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: i, j, ip, is, ic, marke(10000), markc(10000), nas, nas2, na
    CHARACTER*80 :: string
    CHARACTER*4  :: fileExtension 

    WRITE(*,'((a))')'Input the order numbers of surfaces:'
    WRITE(*,'((a))')'   (  0 for no surface; -9 for all surfaces; or.. )'
    CALL read_int_array(marke,nas)

    IF(nas>0 .AND. marke(1)==-9)THEN
       nas = Curv%NB_Region
       DO na = 1,nas
          marke(na) = na
       ENDDO
    ENDIF

    IF(nas>0 .AND. marke(1)/=0)THEN
       OPEN(1,file=JobName(1:JobNameLength)//'.net1',    &
            status='UNKNOWN',form='FORMATTED')

       DO na = 1,nas
          is = marke(na)
          IF(is<=0 .OR. is>Curv%NB_Region) CYCLE
          WRITE(fileExtension,'(i4)') is
          string = '# Face section '//fileExtension
          WRITE(1,*) string

          DO j = 1,Curv%Regions(is)%numNodeV
             DO i = 1,Curv%Regions(is)%numNodeU
                WRITE(1,*) Curv%Regions(is)%Posit(:,i,j)
             ENDDO
             WRITE(1,*)' '
             WRITE(1,*)' '
          ENDDO

          DO i = 1,Curv%Regions(is)%numNodeU
             DO j = 1,Curv%Regions(is)%numNodeV
                WRITE(1,*) Curv%Regions(is)%Posit(:,i,j)
             ENDDO
             WRITE(1,*)' '
             WRITE(1,*)' '
          ENDDO
       ENDDO

       CLOSE(1)

    ENDIF


    WRITE(*,'((a))')'Input the order numbers of curvatures:'
    WRITE(*,'((a))')'   (  0 for no curvature; -9 for all curvatures; '
    WRITE(*,'((a))')'    -99 for those surrounding faces; or...)'
    CALL read_int_array(markc,nas2)

    IF(nas2>0 .AND. markc(1)==-9)THEN
       nas2 = Curv%NB_Curve
       DO na = 1, nas2
          markc(na) = na
       ENDDO
    ENDIF

    IF(nas2>0 .AND. markc(1)>0)THEN
       OPEN(1,file=JobName(1:JobNameLength)//'.net2',    &
            status='UNKNOWN',form='FORMATTED')

       DO na = 1,nas2
          ic = markc(na)
          IF(ic<=0 .OR. ic>Curv%NB_Curve) CYCLE
          WRITE(fileExtension,'(i4)') ic
          string = '# Curve '//fileExtension
          WRITE(1,*) string

          DO i = 1,Curv%Curves(ic)%numNodes
             WRITE(1,*) Curv%Curves(ic)%Posit(:,i)
          ENDDO
          WRITE(1,*)' '
          WRITE(1,*)' '
       ENDDO

       CLOSE(1)

    ELSE IF(nas2>0 .AND. markc(1)==-99 .AND. nas>0 .AND. marke(1)/=0)THEN
       OPEN(1,file=JobName(1:JobNameLength)//'.net2',    &
            status='UNKNOWN',form='FORMATTED')

       DO na = 1,nas
          is = marke(na)
          IF(is<=0 .OR. is>Curv%NB_Region) CYCLE
          WRITE(fileExtension,'(i4)') is
          string = '# Face section '//fileExtension
          WRITE(1,*) string
          WRITE(1,*)' '
          WRITE(1,*)' '

          DO j = 1,Curv%Regions(is)%numCurve
             ic = Curv%Regions(is)%IC(j)
             WRITE(fileExtension,'(i4)') ic
             string = '# Curve '//fileExtension
             WRITE(1,*) string

             DO i = 1,Curv%Curves(ic)%numNodes
                WRITE(1,*) Curv%Curves(ic)%Posit(:,i)
             ENDDO
             WRITE(1,*)' '
             WRITE(1,*)' '
          ENDDO
       ENDDO

       CLOSE(1)

    ENDIF

    RETURN

  END SUBROUTINE SurfaceCurvature_OutputNet


END MODULE SurfaceCurvature


