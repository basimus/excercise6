!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Face-based volume mesh storage, 
!!  including the defination of type and the basic functions.
!<
MODULE FacedMeshStorage
  USE Queue
  USE Plane3DGeom
  IMPLICIT NONE


  TYPE :: FacedMeshStorageType
     INTEGER :: NB_Cell    = 0                         !< the number of cells.
     INTEGER :: NB_Point   = 0                         !< the number of points.
     INTEGER :: NB_Face    = 0                         !< the number of faces.
     INTEGER :: NB_BD_Cond = 0                         !< the number of boundary condition type.
     !>
     !!  A face is defined by a loop of points and two adjacent cells.  
     TYPE (IntQueueType), DIMENSION(:), POINTER :: FaceNode => null()
     !>  Face_Left  represents the cell on the left  hand direction of the loop.  
     INTEGER, DIMENSION(:),   POINTER :: Face_Left => null()
     !>  Face_Right represents the cell on the right hand direction of the loop.
     INTEGER, DIMENSION(:),   POINTER :: Face_Right => null()
     INTEGER, DIMENSION(:),   POINTER :: NB_Cond_Face => null()  !< numbers of faces by each boundary condition type
     REAL*8,  DIMENSION(:,:), POINTER :: Posit => null()         !< (3,*) coordinates of each point.
  END TYPE FacedMeshStorageType


CONTAINS


  !>
  !!   (re)allocate the mesh strucutre.
  !<
  SUBROUTINE allc_FacedMesh(FBMesh, NB_Face, NB_Point)
    IMPLICIT NONE
    TYPE(FacedMeshStorageType), INTENT(INOUT) :: FBMesh
    INTEGER, INTENT(IN) :: NB_Face, NB_Point
    CALL allc_IntQueue(FBMesh%FaceNode,    NB_Face)
    CALL allc_1DPointer(FBMesh%Face_Left,  NB_Face) 
    CALL allc_1DPointer(FBMesh%Face_Right, NB_Face) 
    CALL allc_2DPointer(FBMesh%Posit,  3,  NB_Point)    
  END SUBROUTINE allc_FacedMesh

  !>
  !!   Delete the mesh strucutre and release memory.
  !<
  SUBROUTINE FacedMeshStorage_Clear(FBMesh)
    IMPLICIT NONE
    TYPE(FacedMeshStorageType), INTENT(INOUT) :: FBMesh
    FBMesh%NB_Cell    = 0
    FBMesh%NB_Point   = 0
    FBMesh%NB_Face    = 0
    FBMesh%NB_BD_Cond = 0
    IF(ASSOCIATED(FBMesh%FaceNode))     DEALLOCATE(FBMesh%FaceNode)
    IF(ASSOCIATED(FBMesh%Face_Left))    DEALLOCATE(FBMesh%Face_Left)
    IF(ASSOCIATED(FBMesh%Face_Right))   DEALLOCATE(FBMesh%Face_Right)
    IF(ASSOCIATED(FBMesh%NB_Cond_Face)) DEALLOCATE(FBMesh%NB_Cond_Face)
    IF(ASSOCIATED(FBMesh%Posit))        DEALLOCATE(FBMesh%Posit)
    RETURN
  END SUBROUTINE FacedMeshStorage_Clear

  !>
  !!  Input a face-based mesh by reading a *.fmsh file. 
  !!  @param[in]  JobName  the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[out] FBMesh  the face-based mesh.
  !<
  SUBROUTINE FacedMeshStorage_Input(JobName,JobNameLength,FBMesh)
    IMPLICIT NONE
    TYPE(FacedMeshStorageType), INTENT(INOUT) :: FBMesh
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: i,IP,IB, ipp(10000)

    OPEN(11,file= JobName(1:JobNameLength)//'.fmsh', status='UNKNOWN',form='UNFORMATTED',err=111)
    READ(11,err=111)FBMesh%NB_Cell,FBMesh%NB_Point,FBMesh%NB_Face

    ALLOCATE(FBMesh%FaceNode(  FBMesh%NB_Face)) 
    ALLOCATE(FBMesh%Face_Left( FBMesh%NB_Face)) 
    ALLOCATE(FBMesh%Face_Right(FBMesh%NB_Face)) 
    ALLOCATE(FBMesh%Posit(3,   FBMesh%NB_Point)) 

    READ(11,err=111)((FBMesh%Posit(i,IP),IP=1,FBMesh%NB_Point),i=1,3)
    DO IB = 1,FBMesh%NB_Face
       READ(11,err=111) i,ipp(1:i),FBMesh%Face_Left(IB),FBMesh%Face_Right(IB)
       CALL IntQueue_Set(FBMesh%FaceNode(IB), i, ipp(1:i))
    ENDDO
    READ(11,err=111) FBMesh%NB_BD_Cond
    ALLOCATE(FBMesh%NB_Cond_Face(FBMesh%NB_BD_Cond)) 
    READ(11,err=111) FBMesh%NB_Cond_Face(1:FBMesh%NB_BD_Cond)

    CLOSE(11)
    RETURN

111 WRITE(*,*) 'Error--- FacedMeshStorage_Input: ', JobName(1:JobNameLength)//'.fmsh'
    CALL Error_STOP (' ')
  END SUBROUTINE FacedMeshStorage_Input


  !>
  !!  Output a face-based mesh by writting a *.fmsh file. 
  !!  @param[in]  JobName  the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  FBMesh  the face-based mesh.
  !<
  SUBROUTINE FacedMeshStorage_Output(JobName,JobNameLength,FBMesh)
    IMPLICIT NONE
    TYPE(FacedMeshStorageType), INTENT(IN) :: FBMesh
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: i,IP,IB

    OPEN(11,file= JobName(1:JobNameLength)//'.fmsh', status='UNKNOWN',form='UNFORMATTED',err=111)
    WRITE(11,err=111)FBMesh%NB_Cell,FBMesh%NB_Point,FBMesh%NB_Face

    WRITE(11,err=111)((FBMesh%Posit(i,IP),IP=1,FBMesh%NB_Point),i=1,3)
    DO IB = 1,FBMesh%NB_Face
       i = FBMesh%FaceNode(IB)%numNodes
       WRITE(11,err=111) i,FBMesh%FaceNode(IB)%Nodes(1:i),FBMesh%Face_Left(IB),FBMesh%Face_Right(IB)
    ENDDO
    WRITE(11,err=111) FBMesh%NB_BD_Cond
    WRITE(11,err=111) FBMesh%NB_Cond_Face(1:FBMesh%NB_BD_Cond)

    CLOSE(11)
    RETURN

111 WRITE(*,*) 'Error--- FacedMeshStorage_Output: ', JobName(1:JobNameLength)//'.fmsh'
    CALL Error_STOP (' ')
  END SUBROUTINE FacedMeshStorage_Output
  
  !>
  !!  Write a .geo file for EnSight.
  !!  @param[in]  JobName  the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  FBMesh  the face-based mesh.
  !<
  SUBROUTINE FacedMeshStorage_OutputEnSight(JobName,JobNameLength,FBMesh)
    IMPLICIT NONE
    TYPE(FacedMeshStorageType), INTENT(IN) :: FBMesh
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    TYPE (IntQueueType), DIMENSION(:), POINTER :: CellFace 
    INTEGER :: i, j, ip, is, nbn, nbp, ib
    INTEGER, DIMENSION(:),   ALLOCATABLE :: gloloc, nps
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: ips
    REAL*4,  DIMENSION(:,:), ALLOCATABLE :: xyz
    CHARACTER*80 :: string
    CHARACTER*4  :: fileExtension 

    CALL FacedMeshStorage_PopulateCellFace(FBMesh, CellFace)

    ALLOCATE (xyz(3,FBMesh%NB_Point))
    DO i=1,FBMesh%NB_Point
       xyz(:,i) = FBMesh%Posit(:,i)
    ENDDO

    WRITE(6,*) 'Generating unformatted ensightgold .geo file: ',JobName(1:JobNameLength)//'.geo'
    OPEN(10,file = JobName(1:JobNameLength)//'.geo',form = 'unformatted',status = 'unknown')
    string = 'Fortran Binary' 
    WRITE(10) string
    string = 'ENSIGHT'
    WRITE(10) string
    string = 'GEO FILE'
    WRITE(10) string
    string = 'node id off'
    WRITE(10) string
    string = 'element id off'
    WRITE(10) string

    string = 'part'
    WRITE(10) string
    is = 1          
    WRITE(10) is    
    string = 'Volume Mesh'
    WRITE(10) string
    string = 'coordinates'
    WRITE(10) string
    WRITE(10) FBMesh%NB_Point
    WRITE(10) xyz(1,1:FBMesh%NB_Point)
    WRITE(10) xyz(2,1:FBMesh%NB_Point)
    WRITE(10) xyz(3,1:FBMesh%NB_Point)

    string = 'nfaced'
    WRITE(10) string
    WRITE(10) FBMesh%NB_Cell
    WRITE(10) CellFace(1:FBMesh%NB_Cell)%numNodes
    WRITE(10) (FBMesh%FaceNode(CellFace(i)%Nodes(1:CellFace(i)%numNodes))%numNodes, i=1,FBMesh%NB_Cell)
    WRITE(10) ((FBMesh%FaceNode(CellFace(i)%Nodes(j))  &
                %Nodes(1:FBMesh%FaceNode(CellFace(i)%Nodes(j))%numNodes), &
                j=1,CellFace(i)%numNodes), i=1,FBMesh%NB_Cell)

    ALLOCATE (gloloc(FBMesh%NB_Point),ips(20,FBMesh%NB_Face), nps(FBMesh%NB_Face))

    DO is = 1,FBMesh%NB_BD_Cond
       gloloc(1:FBMesh%NB_Point) = 0
       nbp = 0
       nbn = 0
       DO ib = 1, FBMesh%NB_Face
          IF(FBMesh%Face_left(ib)/=is) CYCLE
          nbn = nbn+1
          nps(nbn) = FBMesh%FaceNode(ib)%numNodes
          DO j=1,nps(nbn)
             ip = FBMesh%FaceNode(ib)%Nodes(j)
             IF(gloloc(ip) == 0)THEN
                nbp = nbp + 1
                gloloc(ip) = nbp
                xyz(:,nbp) = FBMesh%Posit(:,ip)
             ENDIF
             ips(j,nbn) = gloloc(ip)
          ENDDO
       ENDDO
       IF(nbn==0) CYCLE

       string = 'part'
       WRITE(10) string
       WRITE(10) is+1
       WRITE(fileExtension,'(i4)') is
       string = 'Tri Surface Mesh '//fileExtension
       WRITE(10) string
       string = 'coordinates'
       WRITE(10) string
       WRITE(10) nbp
       WRITE(10) xyz(1,1:nbp)
       WRITE(10) xyz(2,1:nbp)
       WRITE(10) xyz(3,1:nbp)

          string = 'nsided'
          WRITE(10) string
          WRITE(10) nbn
          WRITE(10) (nps(i), i=1,nbn)
          WRITE(10) ((ips(j,i),j=1,nps(i)), i=1,nbn)
    ENDDO

    CLOSE(10)

    DEALLOCATE (gloloc, ips, xyz)

    RETURN
  END SUBROUTINE FacedMeshStorage_OutputEnSight

  !>
  !!  Output a cut-plane. 
  !!  @param[in]  JobName  the name (prefix) of output file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  FBMesh  the face-based mesh.
  !!  @param[in]  Dir     the normal direction of the cut face.
  !!  @param[in]  xyz0    a point located on the cut face.
  !!  @param[in]  fmat = -3: unformatted .geo  (for enSight).  \n
  !!                   =  3:   formatted .net  (for gnuplot).
  !<
  SUBROUTINE FacedMeshStorage_Output2DCut(JobName,JobNameLength,FBMesh,Dir,xyz0,fmat)
    IMPLICIT NONE
    TYPE(FacedMeshStorageType), INTENT(IN) :: FBMesh
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength, fmat
    REAL*8, INTENT(IN)  ::  Dir(3),xyz0(3)
    INTEGER :: i, j, is, ip, nbn, nbp, k
    INTEGER, DIMENSION(:),   ALLOCATABLE :: gloloc, sig, np
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: ips
    REAL*4,  DIMENSION(:,:), ALLOCATABLE :: xyz
    CHARACTER*80 :: string
    CHARACTER*4  :: fileExtension 

    TYPE(Plane3D) :: aPlane

    aPlane%anor = Dir / Geo3D_Distance(Dir)
    aPlane%d = -(aPlane%anor(1) * xyz0(1) + aPlane%anor(2) * xyz0(2)  + aPlane%anor(3) * xyz0(3))

    ALLOCATE (xyz(3,FBMesh%NB_Point), sig(FBMesh%NB_Point))
    DO i=1,FBMesh%NB_Point
       IF(Plane3D_SignedDistancePointToPlane(FBMesh%Posit(:,i),aPlane)>0)THEN
          sig(i) = 1
       ELSE
          sig(i) = -1
       ENDIF
    ENDDO

    ALLOCATE (gloloc(FBMesh%NB_Point),ips(100,FBMesh%NB_Face),np(FBMesh%NB_Face))

    gloloc(1:FBMesh%NB_Point) = 0
    np(1:FBMesh%NB_Face) = 1
    nbp = 0
    nbn = 0
    DO i = 1,FBMesh%NB_Face
       k = sig(FBMesh%FaceNode(i)%Nodes(1))
       DO j = 2,FBMesh%FaceNode(i)%numNodes
          ip =  FBMesh%FaceNode(i)%Nodes(j)
          IF(k*sig(ip)<0)THEN
             k = 0
             EXIT
          ENDIF
       ENDDO
       IF(k==0) np(i) = 0
    ENDDO
    DO i = 1,FBMesh%NB_Face
       IF(np(i)==0)THEN
          DO j=1,FBMesh%FaceNode(i)%numNodes
             sig(FBMesh%FaceNode(i)%Nodes(j)) = 0
          ENDDO
       ENDIF
    ENDDO
    DO i = 1,FBMesh%NB_Face
       IF(np(i)/=0)THEN
          DO j=1,FBMesh%FaceNode(i)%numNodes
             IF(sig(FBMesh%FaceNode(i)%Nodes(j)) /= 0) EXIT
          ENDDO
          IF(j>FBMesh%FaceNode(i)%numNodes) np(i) = 0
       ENDIF
    ENDDO

    DO i = 1,FBMesh%NB_Face
       IF(np(i)/=0) CYCLE
       nbn = nbn+1
       DO j=1,FBMesh%FaceNode(i)%numNodes
          ip = FBMesh%FaceNode(i)%Nodes(j)
          IF(gloloc(ip) == 0)THEN
             nbp = nbp + 1
             gloloc(ip) = nbp
             xyz(:,nbp) = FBMesh%Posit(:,ip)
          ENDIF
          ips(j,nbn) = gloloc(ip)
       ENDDO
       np(nbn) = FBMesh%FaceNode(i)%numNodes
    ENDDO
    IF(nbn==0) CALL Error_STOP ( '---no FaceNode intersected')

    IF(fmat==-3)THEN
       WRITE(6,*) 'Generating unformatted ensightgold .geo file: ',JobName(1:JobNameLength)//'.geo'
       OPEN(10,file = JobName(1:JobNameLength)//'.geo',form = 'unformatted',status = 'unknown')
       string = 'Fortran Binary' 
       WRITE(10) string
       string = 'ENSIGHT'
       WRITE(10) string
       string = 'GEO FILE'
       WRITE(10) string
       string = 'node id off'
       WRITE(10) string
       string = 'element id off'
       WRITE(10) string

       string = 'part'
       WRITE(10) string
       is = 1
       WRITE(10) is
       string = 'Cut Mesh '
       WRITE(10) string
       string = 'coordinates'
       WRITE(10) string
       WRITE(10) nbp
       WRITE(10) xyz(1,1:nbp)
       WRITE(10) xyz(2,1:nbp)
       WRITE(10) xyz(3,1:nbp)

       string = 'nsided'
       WRITE(10) string
       WRITE(10) nbn 
       WRITE(10) np(1:nbn)
       WRITE(10) ((ips(j,i),j=1,np(i)), i=1,nbn)

    ELSE

       WRITE(6,*) 'Generating formatted .net file: ',JobName(1:JobNameLength)//'.net'
       OPEN(10,file = JobName(1:JobNameLength)//'.net',form = 'formatted',status = 'unknown')
       WRITE(10,*)'# number of faces:', nbn 
       DO i=1,nbn
          DO j=1,np(i)
             WRITE(10,*) xyz(:,ips(j,i)),ips(j,i)
          ENDDO
          WRITE(10,*) ' '
          WRITE(10,*) ' '
       ENDDO

    ENDIF

    CLOSE(10)

    DEALLOCATE (gloloc, ips, xyz, sig)

    RETURN

  END SUBROUTINE FacedMeshStorage_Output2DCut


  !>
  !!  Sort face so that the boundary face first.
  !!  @param[inout]  FBMesh    the face-based mesh.
  !!  @param[out] Isucc =0  fail.    \n
  !!                    =1  succeed.
  !<
  SUBROUTINE FacedMeshStorage_SortFace(FBMesh, Isucc)
    IMPLICIT NONE
    TYPE(FacedMeshStorageType), INTENT(INOUT) :: FBMesh
    TYPE(FacedMeshStorageType) :: Ftemp
    INTEGER, INTENT(OUT) :: Isucc
    INTEGER :: nbn, is, ib, ibn
    
    Isucc = 0
          
    !--- check first
    DO ib = 1,FBMesh%NB_Face
       IF(FBMesh%Face_left(ib)==0)THEN
           WRITE(*,*)' Warning--- fail to sort face. ib=',ib
           RETURN
       ENDIF
    ENDDO
    
    nbn = 0
    DO is = 1,FBMesh%NB_BD_Cond
       nbn = 0
       DO ib = 1,FBMesh%NB_Face
          IF(FBMesh%Face_left(ib)/=-is) CYCLE
          nbn = nbn+1
       ENDDO
       IF(nbn /= FBMesh%NB_Cond_Face(is))THEN
          WRITE(*,*)' Warning--- fail to sort face is,nbn,n=',is,nbn,FBMesh%NB_Cond_Face(is)
          RETURN
       ENDIF
    ENDDO
    
    ALLOCATE(Ftemp%FaceNode(  FBMesh%NB_Face))
    ALLOCATE(Ftemp%Face_Left( FBMesh%NB_Face))
    ALLOCATE(Ftemp%Face_Right(FBMesh%NB_Face))
    
    nbn = 0
    DO is = 1,FBMesh%NB_BD_Cond
       DO ib = 1,FBMesh%NB_Face
          IF(FBMesh%Face_left(ib)/=-is) CYCLE
          nbn = nbn+1
          CALL IntQueue_Copy (FBMesh%FaceNode(ib), Ftemp%FaceNode(nbn))
          Ftemp%Face_left(nbn)  = FBMesh%Face_left(ib)
          Ftemp%Face_right(nbn) = FBMesh%Face_right(ib)
       ENDDO
    ENDDO
    
    ibn = FBMesh%NB_Face+1
    DO ib = FBMesh%NB_Face, 1, -1
       IF(FBMesh%Face_left(ib)<=0) cycle
       ibn = ibn-1
       IF(ib==ibn) cycle
       CALL IntQueue_Copy (FBMesh%FaceNode(ib), FBMesh%FaceNode(ibn))
       FBMesh%Face_left(ibn)  = FBMesh%Face_left(ib)
       FBMesh%Face_right(ibn) = FBMesh%Face_right(ib)
    ENDDO
    
    IF(ibn/=nbn+1) CALL Error_Stop('FacedMeshStorage_SortFace:: ibn/=nbn+1')
    
    DO ib = 1, nbn
       CALL IntQueue_Copy (Ftemp%FaceNode(ib), FBMesh%FaceNode(ib))
       FBMesh%Face_left(ib)  = Ftemp%Face_left(ib)
       FBMesh%Face_right(ib) = Ftemp%Face_right(ib)
    ENDDO

    DEALLOCATE(Ftemp%FaceNode)
    DEALLOCATE(Ftemp%Face_Left)
    DEALLOCATE(Ftemp%Face_Right)
    Isucc = 1
    
  END SUBROUTINE FacedMeshStorage_SortFace
    
  !>
  !!  Populate cells from a face-based mesh.
  !!  @param[in]  FBMesh    the face-based mesh.
  !!  @param[out] CellFace  a queue of faces of each cell.
  !!  @param[out] CellNode  a queue of nodes of each cell.
  !<
  SUBROUTINE FacedMeshStorage_PopulateCell(FBMesh, CellFace, CellNode)
    IMPLICIT NONE
    TYPE(FacedMeshStorageType), INTENT(IN) :: FBMesh
    TYPE (IntQueueType), DIMENSION(:), POINTER :: CellNode
    TYPE (IntQueueType), DIMENSION(:), POINTER :: CellFace 
    INTEGER :: i, j, lr, iCell, iFace
    INTEGER :: numNodes, nodes(1000), nodesr(1000)

    ALLOCATE(CellNode(FBMesh%NB_Cell),  CellFace(FBMesh%NB_Cell))

    DO iFace = 1,FBMesh%NB_Face
       numNodes = FBMesh%FaceNode(iFace)%numNodes
       IF(numNodes>1000) CALL Error_STOP ( '--- FacedMeshStorage_PopulateCells 1')
       nodes(1:numNodes) = FBMesh%FaceNode(iFace)%nodes(1:numNodes)
       DO lr=1,2
          IF(lr ==1)THEN
             iCell = FBMesh%Face_Left(iFace)
          ELSE
             iCell = FBMesh%Face_Right(iFace)
          ENDIF
          IF(iCell<=0) CYCLE
          IF(iCell>FBMesh%NB_Cell)THEN
             WRITE(*,*)'Error--- iFace, iCell, NB_Cell=',iFace, iCell, FBMesh%NB_Cell
             CALL Error_STOP ( '--- FacedMeshStorage_PopulateCells 2')
          ENDIF
          CALL IntQueue_Push(CellFace(iCell), iFace)

          IF(CellNode(iCell)%numNodes==0)THEN
             IF(lr ==1)THEN
                !--- left cell
                nodesr(1:numNodes) = nodes(numNodes:1:-1)
             ELSE
                !--- right cell
                nodesr(1:numNodes) = nodes(1:numNodes)
             ENDIF
             CALL IntQueue_Set(CellNode(iCell), numNodes, nodesr(1:numNodes))
          ELSE
             Loop_i : DO i = 1,numNodes
                DO j = 1,CellNode(iCell)%numNodes
                   IF(CellNode(iCell)%Nodes(j) == nodes(i)) CYCLE Loop_i
                ENDDO
                CALL IntQueue_Push(CellNode(iCell), nodes(i))
             ENDDO Loop_i
          ENDIF
       ENDDO
    ENDDO

  END SUBROUTINE FacedMeshStorage_PopulateCell


  !>
  !!  Populate cells from a face-based mesh.
  !!  @param[in]  FBMesh    the face-based mesh.
  !!  @param[out] CellFace  a queue of faces of each cell.
  !<
  SUBROUTINE FacedMeshStorage_PopulateCellFace(FBMesh, CellFace)
    IMPLICIT NONE
    TYPE(FacedMeshStorageType), INTENT(IN) :: FBMesh
    TYPE (IntQueueType), DIMENSION(:), POINTER :: CellFace 
    INTEGER :: lr, iCell, iFace
    INTEGER :: numNodes, nodes(1000)

    ALLOCATE(CellFace(FBMesh%NB_Cell))

    DO iFace = 1,FBMesh%NB_Face
       numNodes = FBMesh%FaceNode(iFace)%numNodes
       IF(numNodes>1000) CALL Error_STOP ( '--- FacedMeshStorage_PopulateCellFace 1')
       nodes(1:numNodes) = FBMesh%FaceNode(iFace)%nodes(1:numNodes)
       DO lr=1,2
          IF(lr ==1)THEN
             iCell = FBMesh%Face_Left(iFace)
          ELSE
             iCell = FBMesh%Face_Right(iFace)
          ENDIF
          IF(iCell<=0) CYCLE
          IF(iCell>FBMesh%NB_Cell)THEN
             WRITE(*,*)'Error--- iFace, iCell, NB_Cell=',iFace, iCell, FBMesh%NB_Cell
             CALL Error_STOP ( '--- FacedMeshStorage_PopulateCellFace 2')
          ENDIF
          CALL IntQueue_Push(CellFace(iCell), iFace)
       ENDDO
    ENDDO

  END SUBROUTINE FacedMeshStorage_PopulateCellFace
  
  !>
  !!  Populate cells from a face-based mesh.
  !!  @param[in]  FBMesh    the face-based mesh.
  !!  @param[out] CellNode  a queue of nodes of each cell.
  !<
  SUBROUTINE FacedMeshStorage_PopulateCellNode(FBMesh, CellNode)
    IMPLICIT NONE
    TYPE(FacedMeshStorageType), INTENT(IN) :: FBMesh
    TYPE (IntQueueType), DIMENSION(:), POINTER :: CellNode
    INTEGER :: i, j, lr, iCell, iFace
    INTEGER :: numNodes, nodes(1000), nodesr(1000)

    ALLOCATE(CellNode(FBMesh%NB_Cell))

    DO iFace = 1,FBMesh%NB_Face
       numNodes = FBMesh%FaceNode(iFace)%numNodes
       IF(numNodes>1000) CALL Error_STOP ( '--- FacedMeshStorage_PopulateCellNode 1')
       nodes(1:numNodes) = FBMesh%FaceNode(iFace)%nodes(1:numNodes)
       DO lr=1,2
          IF(lr ==1)THEN
             iCell = FBMesh%Face_Left(iFace)
          ELSE
             iCell = FBMesh%Face_Right(iFace)
          ENDIF
          IF(iCell<=0) CYCLE
          IF(iCell>FBMesh%NB_Cell)THEN
             WRITE(*,*)'Error--- iFace, iCell, NB_Cell=',iFace, iCell, FBMesh%NB_Cell
             CALL Error_STOP ( '--- FacedMeshStorage_PopulateCellNode 2')
          ENDIF

          IF(CellNode(iCell)%numNodes==0)THEN
             IF(lr ==1)THEN
                !--- left cell
                nodesr(1:numNodes) = nodes(numNodes:1:-1)
             ELSE
                !--- right cell
                nodesr(1:numNodes) = nodes(1:numNodes)
             ENDIF
             CALL IntQueue_Set(CellNode(iCell), numNodes, nodesr(1:numNodes))
          ELSE
             Loop_i : DO i = 1,numNodes
                DO j = 1,CellNode(iCell)%numNodes
                   IF(CellNode(iCell)%Nodes(j) == nodes(i)) CYCLE Loop_i
                ENDDO
                CALL IntQueue_Push(CellNode(iCell), nodes(i))
             ENDDO Loop_i
          ENDIF
       ENDDO
    ENDDO

  END SUBROUTINE FacedMeshStorage_PopulateCellNode


END MODULE FacedMeshStorage



