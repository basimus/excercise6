!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Tetrahedral mesh storage, including the defination of type and the basic functions.
!<
MODULE TetMeshStorage
  USE array_allocator 
  USE SurfaceMeshStorage
  IMPLICIT NONE
  !>
  !!  A structure of tettrahedral mesh (nodes and connectivities).
  !<
  TYPE :: TetMeshStorageType
     INTEGER, DIMENSION(:,:),  POINTER :: IP_Tet => null()     !<  (4,*) connectivities of each tetrahedron.
     REAL*8,  DIMENSION(:,:),  POINTER :: Posit => null()      !<  (3,*) coordinates of each point.
     INTEGER :: NB_Point  = 0                        !<  the number of points.
     INTEGER :: NB_Tet    = 0                        !<  the number of tetrahedra.
  END TYPE TetMeshStorageType

CONTAINS

  !>
  !!   Save coordinate of nodes to a mesh strucutre.
  !<
  SUBROUTINE TetMeshStorage_NodeBackup(numNodes, Posit, TetMesh)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: numNodes
    REAL*8,  INTENT(IN) :: Posit(3,*)
    TYPE(TetMeshStorageType), INTENT(INOUT) :: TetMesh
    IF(ASSOCIATED(TetMesh%Posit))  DEALLOCATE(TetMesh%Posit)
    TetMesh%NB_Point = numNodes
    ALLOCATE(TetMesh%Posit(3,TetMesh%NB_Point))
    TetMesh%Posit(:,1:numNodes) = Posit(:,1:numNodes)
    RETURN
  END SUBROUTINE TetMeshStorage_NodeBackup

  !>
  !!   Save connectivities of cells to a mesh strucutre.
  !<
  SUBROUTINE TetMeshStorage_CellBackup(numCells, IPs, TetMesh)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: numCells, IPs(4,*)
    TYPE(TetMeshStorageType), INTENT(INOUT) :: TetMesh
    IF(ASSOCIATED(TetMesh%IP_Tet)) DEALLOCATE(TetMesh%IP_Tet)
    TetMesh%NB_Tet   = numCells
    ALLOCATE(TetMesh%IP_Tet(4,TetMesh%NB_Tet))
    TetMesh%IP_Tet(:,1:numCells) = IPs(:,1:numCells)
    RETURN
  END SUBROUTINE TetMeshStorage_CellBackup


  !>
  !!   Get coordinate of nodes from a mesh strucutre.
  !<
  SUBROUTINE TetMeshStorage_NodeRestore(numNodes, Posit, TetMesh)
    IMPLICIT NONE
    INTEGER, INTENT(OUT) :: numNodes
    REAL*8,  INTENT(OUT) :: Posit(3,*)
    TYPE(TetMeshStorageType), INTENT(IN) :: TetMesh
    numNodes = TetMesh%NB_Point
    Posit(:,1:numNodes) = TetMesh%Posit(:,1:numNodes)
    RETURN   
  END SUBROUTINE TetMeshStorage_NodeRestore

  !>
  !!   Get connectivities of cells from a mesh strucutre.
  !<
  SUBROUTINE TetMeshStorage_CellRestore(numCells, IPs, TetMesh)
    IMPLICIT NONE
    INTEGER, INTENT(OUT) :: numCells, IPs(4,*)
    TYPE(TetMeshStorageType), INTENT(IN) :: TetMesh
    numCells = TetMesh%NB_Tet  
    IPs(:,1:numCells) = TetMesh%IP_Tet(:,1:numCells)
    RETURN   
  END SUBROUTINE TetMeshStorage_CellRestore


  !>
  !!   Get coordinate of nodes (POINTER) from a mesh strucutre.
  !<
  SUBROUTINE TetMeshStorage_NodeImage(numNodes, Posit, TetMesh)
    IMPLICIT NONE
    INTEGER, INTENT(OUT) :: numNodes
    REAL*8,  DIMENSION(:,:), POINTER :: Posit
    TYPE(TetMeshStorageType), INTENT(IN) :: TetMesh
    numNodes =  TetMesh%NB_Point
    Posit    => TetMesh%Posit
    RETURN   
  END SUBROUTINE TetMeshStorage_NodeImage

  !>
  !!   Get connectivities of cells (POINTER) from a mesh strucutre.
  !<
  SUBROUTINE TetMeshStorage_CellImage(numCells, IPs, TetMesh)
    IMPLICIT NONE
    INTEGER, INTENT(OUT) :: numCells
    INTEGER, DIMENSION(:,:), POINTER :: IPs
    TYPE(TetMeshStorageType), INTENT(IN) :: TetMesh
    numCells =  TetMesh%NB_Tet  
    IPs      => TetMesh%IP_Tet
    RETURN   
  END SUBROUTINE TetMeshStorage_CellImage

  !>
  !!   Add nodes to TetMesh.
  !<
  SUBROUTINE TetMeshStorage_NodeBackupAdd(numNodes, Posit, TetMesh)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: numNodes
    REAL*8,  INTENT(IN) :: Posit(3,*)
    TYPE(TetMeshStorageType), INTENT(INOUT) :: TetMesh
    REAL*8,  DIMENSION(:,:),  POINTER :: Pos
    INTEGER :: n1, n2
    n1 = TetMesh%NB_Point
    n2 = n1 + numNodes
    ALLOCATE(Pos(3,n2))
    Pos(:,    1:n1) = TetMesh%Posit(:,1:n1)
    Pos(:, n1+1:n2) = Posit(:,1:numNodes)
    CALL TetMeshStorage_NodeBackup(n2, Pos, TetMesh)
    DEALLOCATE(Pos)
    RETURN
  END SUBROUTINE TetMeshStorage_NodeBackupAdd

  !>
  !!   Add cells to TetMesh.
  !!     All nodes in added cells must be contained by TetMesh.
  !<
  SUBROUTINE TetMeshStorage_CellBackupAdd(numCells, IPs, TetMesh)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: numCells, IPs(4,*)
    TYPE(TetMeshStorageType), INTENT(INOUT) :: TetMesh
    INTEGER, DIMENSION(:,:),  POINTER :: IPss
    INTEGER :: n1, n2
    n1 = TetMesh%NB_Tet
    n2 = n1 + numCells
    ALLOCATE(IPss(4,n2))
    IPss(:,    1:n1) = TetMesh%IP_Tet(:,1:n1)
    IPss(:, n1+1:n2) = IPs(:,1:numCells)
    CALL TetMeshStorage_CellBackup(n2, IPss, TetMesh)
    DEALLOCATE(IPss)
    RETURN   
  END SUBROUTINE TetMeshStorage_CellBackupAdd

  !>
  !!  Input a tetrahedral mesh by reading a *.plt file. 
  !!  @param[in]  JobName  the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[out] TetMesh  the tet. mesh.
  !!  @param[out] Surf     a surface mesh represent the boundary.
  !<
  SUBROUTINE TetMeshStorage_Input(JobName,JobNameLength,TetMesh,Surf)
    IMPLICIT NONE
    TYPE(TetMeshStorageType), INTENT(INOUT) :: TetMesh
    TYPE(SurfaceMeshStorageType), INTENT(INOUT), OPTIONAL :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: I, IT, IP, nb, nas
    
    OPEN(1,file=JobName(1:JobNameLength)//'.plt', status='old',   &
       form='UNFORMATTED',err=110)
    READ(1,err=111) TetMesh%NB_Tet,TetMesh%NB_Point,nas
    CALL allc_2Dpointer(TetMesh%IP_Tet, 4, TetMesh%NB_Tet,   'IP_Tet') 
    CALL allc_2Dpointer(TetMesh%Posit,  3, TetMesh%NB_Point, 'Posit') 
    READ(1,err=111)((TetMesh%IP_Tet(i,IT),IT=1,TetMesh%NB_Tet),i=1,4)
    READ(1,err=111)((TetMesh%Posit(i,IP),IP=1,TetMesh%NB_Point),i=1,3)

    IF(PRESENT(Surf))THEN
       Surf%NB_Tri = nas
       CALL allc_2Dpointer(Surf%IP_Tri,  5, Surf%NB_Tri,    'IP_Tri') 
       READ(1,err=111)((Surf%IP_Tri(i,nb), nb=1,Surf%NB_Tri), i=1,5)
       Surf%NB_Surf = 0
       DO nb=1,Surf%NB_Tri
         Surf%NB_Surf = Max(Surf%NB_Surf,Surf%IP_Tri(5,nb))
       ENDDO
    ENDIF

    CLOSE(1)
    
    RETURN
110 WRITE(*,*) 'Error--- TetMeshStorage_Input: fail to open ', JobName(1:JobNameLength)//'.plt'
    CALL Error_STOP (' ')
111 WRITE(*,*) 'Error--- TetMeshStorage_Input: fail to read ', JobName(1:JobNameLength)//'.plt'
    CALL Error_STOP (' ')
  END SUBROUTINE TetMeshStorage_Input

  !>
  !!  Output a tetrahedral mesh by writting a *.plt file. 
  !!  @param[in]  JobName  the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  TetMesh  the tet. mesh.
  !!  @param[in]  Surf     a surface mesh represent the boundary.
  !<
  SUBROUTINE TetMeshStorage_Output(JobName,JobNameLength,TetMesh,Surf)
    IMPLICIT NONE
    TYPE(TetMeshStorageType), INTENT(IN) :: TetMesh
    TYPE(SurfaceMeshStorageType), INTENT(IN), OPTIONAL :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: I, IT, IP, nb, nas
    
    OPEN(1,file=JobName(1:JobNameLength)//'.plt', status='unknown',   &
       form='UNFORMATTED',err=111)
    IF(PRESENT(Surf))THEN
       WRITE(1,err=111) TetMesh%NB_Tet,TetMesh%NB_Point,Surf%NB_Tri
    ELSE
       WRITE(1,err=111) TetMesh%NB_Tet,TetMesh%NB_Point,0
    ENDIF
    WRITE(1,err=111)((TetMesh%IP_Tet(i,IT),IT=1,TetMesh%NB_Tet),i=1,4)
    WRITE(1,err=111)((TetMesh%Posit(i,IP),IP=1,TetMesh%NB_Point),i=1,3)

    IF(PRESENT(Surf))THEN
       WRITE(1,err=111)((Surf%IP_Tri(i,nb), nb=1,Surf%NB_Tri), i=1,5)
    ELSE
       WRITE(1,err=111)
    ENDIF

    CLOSE(1)
    
    RETURN    
111 WRITE(*,*) 'Error--- TetMeshStorage_Output: ', JobName(1:JobNameLength)//'.plt'
    CALL Error_STOP (' ')
  END SUBROUTINE TetMeshStorage_Output

  !>
  !!   Delete the mesh strucutre and release memory.
  !<
  SUBROUTINE TetMeshStorage_Clear(TetMesh)
    IMPLICIT NONE
    TYPE(TetMeshStorageType), INTENT(INOUT) :: TetMesh

    TetMesh%NB_Point    = 0
    TetMesh%NB_Tet      = 0
    IF(ASSOCIATED(TetMesh%IP_Tet))    DEALLOCATE(TetMesh%IP_Tet)
    IF(ASSOCIATED(TetMesh%Posit))     DEALLOCATE(TetMesh%Posit)
    RETURN
  END SUBROUTINE TetMeshStorage_Clear


END MODULE TetMeshStorage



