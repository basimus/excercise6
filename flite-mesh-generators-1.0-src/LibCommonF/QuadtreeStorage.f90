!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!******************************************************************************!
!
!  Order of nodes in each grid:
!                      3 ________ 4
!                        |      |
!                        |      |
!                        |      |
!                      1 |______| 2
!
!
!  Index Pointers:
!
!       Son_                 Next_                     Next_
!
!       ___________               | Up |    |               |   Up    |
!       |    |    |             __|____|____|__           __|_________|
!       | LT | RT |               |         |               |         | 
!       |____|____|          Left |         | Right         |         | Right
!       |    |    |             __|         |__             |         |
!       | LB | RB |               |         |          Left |         |
!       |____|____|               |         |               |         |
!                               __|_________|__           __|_________|__
!                                 |Down|    |                Down     |
!                                 |    |    |                         |
!
!
!
!******************************************************************************!

MODULE QuadtreeMesh

  IMPLICIT NONE

  INTEGER, PARAMETER :: Max_Quad_Level = 20
  INTEGER, PARAMETER :: Max_QuadNodes=10000000

  REAL*8,  PARAMETER :: Quad_PI = 3.14159265359D0


  INTEGER, SAVE :: Counter_Quad_Node(Max_QuadNodes)
  INTEGER, SAVE :: Counter2_Quad_Node(Max_QuadNodes)
  REAL*8,  SAVE :: Value_Quad_Node(3,Max_QuadNodes)

  TYPE :: QuadtreeMesh_Cell_Type
     INTEGER :: Level = 0
     INTEGER :: Node(4)
     REAL*8  :: Centre(2)
     REAL*8  :: V(3)
     LOGICAL :: Bisected
     TYPE (QuadtreeMesh_Cell_Type), POINTER :: Son_LT => null()      !--   Left_Top
     TYPE (QuadtreeMesh_Cell_Type), POINTER :: Son_LB => null()      !--   Left_Bottom
     TYPE (QuadtreeMesh_Cell_Type), POINTER :: Son_RT => null()      !--  Right_Top
     TYPE (QuadtreeMesh_Cell_Type), POINTER :: Son_RB => null()      !--  Right_Bottom
     TYPE (QuadtreeMesh_Cell_Type), POINTER :: Next_Left => null()     !-- Left
     TYPE (QuadtreeMesh_Cell_Type), POINTER :: Next_Right => null()    !-- Right
     TYPE (QuadtreeMesh_Cell_Type), POINTER :: Next_Up => null()       !-- Up
     TYPE (QuadtreeMesh_Cell_Type), POINTER :: Next_Down => null()     !-- Down
  END TYPE QuadtreeMesh_Cell_Type

  TYPE (QuadtreeMesh_Cell_Type), DIMENSION(:,:), ALLOCATABLE, TARGET :: Quad_Grid
  TYPE (QuadtreeMesh_Cell_Type), TARGET, SAVE :: Quad_Empty

  TYPE :: QuadtreeMesh_Type
     CHARACTER ( LEN = 60 ) :: theName = ' '
     TYPE (QuadtreeMesh_Cell_Type), DIMENSION(:,:,:), POINTER :: Cells => null()
     INTEGER :: numNodes    = 0
     INTEGER :: numCells    = 0
     INTEGER :: imax        = 0
     INTEGER :: jmax        = 0
     INTEGER :: nallc_nodes = 0
     REAL*8  :: DX(Max_Quad_Level+1)
     REAL*8  :: DY(Max_Quad_Level+1)
     REAL*8  :: Pmin(2), Pmax(2), TinyD(2)
     REAL*8,  DIMENSION(:,:), POINTER :: Coord => null()   !---(2,:)
  END TYPE QuadtreeMesh_Type
  
  TYPE(QuadtreeMesh_Type), SAVE :: QuadMesh

CONTAINS

  !******************************************************************************!

  SUBROUTINE QuadtreeMesh_Set(Dx, Dy, Xmin, Ymin, imax, jmax)
    IMPLICIT NONE

    REAL*8,  INTENT(IN) :: Dx, Dy, Xmin, Ymin
    INTEGER, INTENT(IN) :: imax, jmax
    INTEGER :: i, j, k

    Quad_Empty%Level = 0

    IF((imax+1)*(jmax+1)>Max_QuadNodes)THEN
       WRITE(*,*)'Error---  : Reset Max_QuadNodes'
       WRITE(*,*)'imax,jmax,Max_QuadNodes = ',imax,jmax,Max_QuadNodes
       CALL Error_Stop (' --- QuadtreeMesh_Set')
    ENDIF

    QuadMesh%numNodes = (imax+1)*(jmax+1)

    QuadMesh%imax = imax
    QuadMesh%jmax = jmax
    QuadMesh%Pmin(1) = Xmin
    QuadMesh%Pmin(2) = Ymin
    QuadMesh%Pmax(1) = QuadMesh%Pmin(1) + QuadMesh%imax * Dx
    QuadMesh%Pmax(2) = QuadMesh%Pmin(2) + QuadMesh%jmax * Dy
    ALLOCATE (Quad_Grid(QuadMesh%imax, QuadMesh%jmax))
    WRITE(*,'(a,i4,a,i4)')'QuadMesh%imax=',QuadMesh%imax,'  QuadMesh%jmax=',QuadMesh%jmax

    QuadMesh%DX(1) = Dx
    QuadMesh%DY(1) = Dy
    DO i=2,Max_Quad_Level+1
       QuadMesh%DX(i) = QuadMesh%DX(i-1)/2.D0
       QuadMesh%DY(i) = QuadMesh%DY(i-1)/2.D0
    ENDDO

    DO i=1,QuadMesh%imax+1
       DO j=1,QuadMesh%jmax+1
          k = (j-1)*(QuadMesh%imax+1) + i
          QuadMesh%Coord(1,k) = (i-1)*QuadMesh%DX(1) + QuadMesh%Pmin(1)
          QuadMesh%Coord(2,k) = (j-1)*QuadMesh%DY(1) + QuadMesh%Pmin(2)
       ENDDO
    ENDDO

    DO i=1,QuadMesh%imax
       DO j=1,QuadMesh%jmax
          Quad_Grid(i,j)%Level = 1

          Quad_Grid(i,j)%Node(1) = (j-1)*(QuadMesh%imax+1) + i
          Quad_Grid(i,j)%Node(2) = Quad_Grid(i,j)%Node(1) + 1
          Quad_Grid(i,j)%Node(3) = Quad_Grid(i,j)%Node(1) + (QuadMesh%imax+1)
          Quad_Grid(i,j)%Node(4) = Quad_Grid(i,j)%Node(3) + 1

          Quad_Grid(i,j)%Centre(1) = (i-0.5d0)*QuadMesh%DX(1) + QuadMesh%Pmin(1)
          Quad_Grid(i,j)%Centre(2) = (j-0.5d0)*QuadMesh%DY(1) + QuadMesh%Pmin(2)

          Quad_Grid(i,j)%Bisected = .FALSE.

          ALLOCATE (Quad_Grid(i,j)%Next_Left)
          ALLOCATE (Quad_Grid(i,j)%Next_Right)
          ALLOCATE (Quad_Grid(i,j)%Next_Up)
          ALLOCATE (Quad_Grid(i,j)%Next_Down)
          IF(i>1)THEN
             Quad_Grid(i,j)%Next_Left => Quad_Grid(i-1,j)
          ELSE
             Quad_Grid(i,j)%Next_Left => Quad_Empty
          ENDIF
          IF(i<QuadMesh%imax)THEN
             Quad_Grid(i,j)%Next_Right => Quad_Grid(i+1,j)
          ELSE
             Quad_Grid(i,j)%Next_Right => Quad_Empty
          ENDIF
          IF(j>1)THEN
             Quad_Grid(i,j)%Next_Down => Quad_Grid(i,j-1)
          ELSE
             Quad_Grid(i,j)%Next_Down => Quad_Empty
          ENDIF
          IF(j<QuadMesh%jmax)THEN
             Quad_Grid(i,j)%Next_Up => Quad_Grid(i,j+1)
          ELSE
             Quad_Grid(i,j)%Next_Up => Quad_Empty
          ENDIF

       ENDDO
    ENDDO

    RETURN
  END SUBROUTINE QuadtreeMesh_Set
  !******************************************************************************!

  RECURSIVE SUBROUTINE QuadtreeMesh_CellBisect(pQuad_Grid)
    IMPLICIT NONE

    TYPE (QuadtreeMesh_Cell_Type), POINTER :: pQuad_Grid, p2Quad_Grid
    INTEGER :: ip(4), ipnew(5), Level, i
    REAL*8  :: x0, y0, rx, ry


    IF(QuadMesh%numNodes+5>Max_QuadNodes)THEN
       WRITE(*,*)'Error : Reset Max_QuadNodes'
       WRITE(*,*)'QuadMesh%numNodes,Max_QuadNodes = ',   &
            QuadMesh%numNodes,Max_QuadNodes
       CALL Error_Stop ('--- QuadtreeMesh_CellBisect')
    ENDIF

    pQuad_Grid%Bisected = .TRUE.

    Level = pQuad_Grid%Level
    DO i=1,4
       IF(i==1) p2Quad_Grid => pQuad_Grid%Next_Left
       IF(i==2) p2Quad_Grid => pQuad_Grid%Next_Right
       IF(i==3) p2Quad_Grid => pQuad_Grid%Next_Up
       IF(i==4) p2Quad_Grid => pQuad_Grid%Next_Down
       IF(p2Quad_Grid%Level > 0 .AND. p2Quad_Grid%Level < Level)    &
            CALL QuadtreeMesh_CellBisect(p2Quad_Grid)
    ENDDO


    x0 = pQuad_Grid%Centre(1)
    y0 = pQuad_Grid%Centre(2)
    rx = QuadMesh%DX(Level+1)
    ry = QuadMesh%DY(Level+1)

    ip(1:4)    = pQuad_Grid%Node(1:4)
    ipnew(1:5) = 0

    !--- for existing points      
    p2Quad_Grid => pQuad_Grid%Next_Up
    IF(p2Quad_Grid%Level > Level) ipnew(1) = p2Quad_Grid%Node(2)
    p2Quad_Grid => pQuad_Grid%Next_Left
    IF(p2Quad_Grid%Level > Level) ipnew(2) = p2Quad_Grid%Node(2)
    p2Quad_Grid => pQuad_Grid%Next_Right
    IF(p2Quad_Grid%Level > Level) ipnew(3) = p2Quad_Grid%Node(1)
    p2Quad_Grid => pQuad_Grid%Next_Down
    IF(p2Quad_Grid%Level > Level) ipnew(4) = p2Quad_Grid%Node(4)

    !--- for new points
    DO i=1,5
       IF(ipnew(i) == 0)THEN
          QuadMesh%numNodes = QuadMesh%numNodes +1
          ipnew(i) = QuadMesh%numNodes
          IF(i==1)THEN
             QuadMesh%Coord(:,QuadMesh%numNodes) =     &
                  (QuadMesh%Coord(:,ip(4)) + QuadMesh%Coord(:,ip(3)))/2.D0
          ELSE IF(i==2)THEN
             QuadMesh%Coord(:,QuadMesh%numNodes) =     &
                  (QuadMesh%Coord(:,ip(3)) + QuadMesh%Coord(:,ip(1)))/2.D0
          ELSE IF(i==3)THEN
             QuadMesh%Coord(:,QuadMesh%numNodes) =     &
                  (QuadMesh%Coord(:,ip(2)) + QuadMesh%Coord(:,ip(4)))/2.D0
          ELSE IF(i==4)THEN
             QuadMesh%Coord(:,QuadMesh%numNodes) =     &
                  (QuadMesh%Coord(:,ip(1)) + QuadMesh%Coord(:,ip(2)))/2.D0
          ELSE IF(i==5)THEN
             QuadMesh%Coord(:,QuadMesh%numNodes) =     &
                  (QuadMesh%Coord(:,ip(1)) + QuadMesh%Coord(:,ip(2)) +     &
                  QuadMesh%Coord(:,ip(3)) + QuadMesh%Coord(:,ip(4)))/4.D0
          ENDIF
       ENDIF
    ENDDO

    ALLOCATE (pQuad_Grid%Son_LT)
    ALLOCATE (pQuad_Grid%Son_LB)
    ALLOCATE (pQuad_Grid%Son_RT)
    ALLOCATE (pQuad_Grid%Son_RB)

    !--- Left-Bottom sub-grid

    p2Quad_Grid => pQuad_Grid%Son_LB
    p2Quad_Grid%Level        = Level + 1
    p2Quad_Grid%Node(1:4)    = (/ip(1), ipnew(4), ipnew(2), ipnew(5)/)         
    p2Quad_Grid%Centre(1)    = x0 - rx/2.D0
    p2Quad_Grid%Centre(2)    = y0 - ry/2.D0
    p2Quad_Grid%Bisected     = .FALSE.
    ALLOCATE (p2Quad_Grid%Next_Left)
    ALLOCATE (p2Quad_Grid%Next_Right)
    ALLOCATE (p2Quad_Grid%Next_Up)
    ALLOCATE (p2Quad_Grid%Next_Down)

    IF(pQuad_Grid%Next_Left%level == Level)THEN
       p2Quad_Grid%Next_Left => pQuad_Grid%Next_Left
    ELSE IF(pQuad_Grid%Next_Left%level == Level+1)THEN
       p2Quad_Grid%Next_Left => pQuad_Grid%Next_Left%Next_Down
       p2Quad_Grid%Next_Left%Next_Right => p2Quad_Grid
    ELSE IF(pQuad_Grid%Next_Left%level == 0)THEN
       p2Quad_Grid%Next_Left => Quad_Empty
    ELSE
       WRITE(*,*)'Error: wrong dividing LB'
       CALL Error_Stop ('--- QuadtreeMesh_CellBisect')
    ENDIF
    p2Quad_Grid%Next_Right => pQuad_Grid%Son_RB
    p2Quad_Grid%Next_Up    => pQuad_Grid%Son_LT
    p2Quad_Grid%Next_Down  => pQuad_Grid%Next_Down
    IF(p2Quad_Grid%Next_Down%Level >0)THEN
       p2Quad_Grid%Next_Down%Next_Up => p2Quad_Grid
    ENDIF

    !--- Right-Bottom sub-grid

    p2Quad_Grid => pQuad_Grid%Son_RB
    p2Quad_Grid%Level        = Level + 1
    p2Quad_Grid%Node(1:4)    = (/ipnew(4), ip(2), ipnew(5), ipnew(3)/)         
    p2Quad_Grid%Centre(1)    = x0 + rx/2.D0
    p2Quad_Grid%Centre(2)    = y0 - ry/2.D0
    p2Quad_Grid%Bisected     = .FALSE.
    ALLOCATE (p2Quad_Grid%Next_Left)
    ALLOCATE (p2Quad_Grid%Next_Right)
    ALLOCATE (p2Quad_Grid%Next_Up)
    ALLOCATE (p2Quad_Grid%Next_Down)

    p2Quad_Grid%Next_Left => pQuad_Grid%Son_LB
    IF(pQuad_Grid%Next_Right%level == Level)THEN
       p2Quad_Grid%Next_Right => pQuad_Grid%Next_Right
    ELSE IF(pQuad_Grid%Next_Right%level == Level+1)THEN
       p2Quad_Grid%Next_Right => pQuad_Grid%Next_Right%Next_Down
       p2Quad_Grid%Next_Right%Next_Left => p2Quad_Grid
    ELSE IF(pQuad_Grid%Next_Right%level == 0)THEN
       p2Quad_Grid%Next_Right => Quad_Empty
    ELSE
       WRITE(*,*)'Error: wrong dividing RB1'
       CALL Error_Stop ('--- QuadtreeMesh_CellBisect')
    ENDIF

    p2Quad_Grid%Next_Up   => pQuad_Grid%Son_RT
    IF(pQuad_Grid%Next_Down%level == Level)THEN
       p2Quad_Grid%Next_Down => pQuad_Grid%Next_Down
    ELSE IF(pQuad_Grid%Next_Down%level == Level+1)THEN
       p2Quad_Grid%Next_Down => pQuad_Grid%Next_Down%Next_Right
       p2Quad_Grid%Next_Down%Next_up => p2Quad_Grid
    ELSE IF(pQuad_Grid%Next_Down%level == 0)THEN
       p2Quad_Grid%Next_Down => Quad_Empty
    ELSE
       WRITE(*,*)'Error: wrong dividing RB2'
       CALL Error_Stop ('--- QuadtreeMesh_CellBisect ')
    ENDIF

    !--- Left-Top sub-grid

    p2Quad_Grid => pQuad_Grid%Son_LT
    p2Quad_Grid%Level        = Level + 1
    p2Quad_Grid%Node(1:4)    = (/ipnew(2), ipnew(5), ip(3), ipnew(1)/)         
    p2Quad_Grid%Centre(1)    = x0 - rx/2.D0
    p2Quad_Grid%Centre(2)    = y0 + ry/2.D0
    p2Quad_Grid%Bisected     = .FALSE.
    ALLOCATE (p2Quad_Grid%Next_Left)
    ALLOCATE (p2Quad_Grid%Next_Right)
    ALLOCATE (p2Quad_Grid%Next_Up)
    ALLOCATE (p2Quad_Grid%Next_Down)

    p2Quad_Grid%Next_Left  => pQuad_Grid%Next_Left
    p2Quad_Grid%Next_Right => pQuad_Grid%Son_RT
    p2Quad_Grid%Next_Up    => pQuad_Grid%Next_Up
    p2Quad_Grid%Next_Down  => pQuad_Grid%Son_LB
    IF(p2Quad_Grid%Next_Left%Level >0)THEN
       p2Quad_Grid%Next_Left%Next_Right => p2Quad_Grid
    ENDIF
    IF(p2Quad_Grid%Next_Up%Level >0)THEN
       p2Quad_Grid%Next_Up%Next_Down    => p2Quad_Grid
    ENDIF

    !--- Right-Top sub-grid

    p2Quad_Grid => pQuad_Grid%Son_RT
    p2Quad_Grid%Level        = Level + 1
    p2Quad_Grid%Node(1:4)    = (/ipnew(5), ipnew(3), ipnew(1), ip(4)/)         
    p2Quad_Grid%Centre(1)    = x0 + rx/2.D0
    p2Quad_Grid%Centre(2)    = y0 + ry/2.D0
    p2Quad_Grid%Bisected     = .FALSE.
    ALLOCATE (p2Quad_Grid%Next_Left)
    ALLOCATE (p2Quad_Grid%Next_Right)
    ALLOCATE (p2Quad_Grid%Next_Up)
    ALLOCATE (p2Quad_Grid%Next_Down)

    p2Quad_Grid%Next_Left  => pQuad_Grid%Son_LT
    p2Quad_Grid%Next_Right => pQuad_Grid%Next_Right
    IF(pQuad_Grid%Next_Up%level == Level)THEN
       p2Quad_Grid%Next_Up => pQuad_Grid%Next_Up
    ELSE IF(pQuad_Grid%Next_Up%level == Level+1)THEN
       p2Quad_Grid%Next_Up => pQuad_Grid%Next_Up%Next_Right
       p2Quad_Grid%Next_Up%Next_Down => p2Quad_Grid
    ELSE IF(pQuad_Grid%Next_Up%level == 0)THEN
       p2Quad_Grid%Next_Up => Quad_Empty
    ELSE
       WRITE(*,*)'Error: wrong dividing RT'
       CALL Error_Stop ('--- QuadtreeMesh_CellBisect ')
    ENDIF
    p2Quad_Grid%Next_Down  => pQuad_Grid%Son_RB
    IF(p2Quad_Grid%Next_Right%Level >0)THEN
       p2Quad_Grid%Next_Right%Next_Left => p2Quad_Grid
    ENDIF

    RETURN
  END SUBROUTINE QuadtreeMesh_CellBisect

  !******************************************************************************!
  FUNCTION QuadtreeMesh_CellSearch(pp,Level) RESULT(pQuad_Grid)
    IMPLICIT NONE

    TYPE (QuadtreeMesh_Cell_Type), POINTER :: pQuad_Grid
    REAL*8  :: pp(2), pp1(2)
    INTEGER :: Level,Lev,i0,j0

    i0 = (pp(1) - QuadMesh%Pmin(1)) / QuadMesh%DX(1) +1
    j0 = (pp(2) - QuadMesh%Pmin(2)) / QuadMesh%DY(1) +1
    IF(i0>QuadMesh%imax .OR. j0>QuadMesh%jmax .OR. i0<=0 .OR. j0<=0)THEN
       WRITE(*,*)'Error : beyond the domain'
       WRITE(*,*)'Imax,Jmax,i0,j0,pp=',QuadMesh%imax,QuadMesh%jmax,i0,j0,pp(:)
       CALL Error_Stop (' --- QuadtreeMesh_CellSearch')
    ENDIF

    Lev = Level
    IF(Lev<0 .OR. Lev>Max_Quad_Level) Lev=Max_Quad_Level

    pQuad_Grid => Quad_Grid(i0,j0)
    DO WHILE(pQuad_Grid%Bisected .AND. pQuad_Grid%Level<Lev)
       pp1(:) = pp(:) - pQuad_Grid%Centre(:)
       IF(pp1(1)<=0)THEN
          IF(pp1(2)<=0)THEN
             pQuad_Grid => pQuad_Grid%Son_LB
          ELSE
             pQuad_Grid => pQuad_Grid%Son_LT
          ENDIF
       ELSE
          IF(pp1(2)<=0)THEN
             pQuad_Grid => pQuad_Grid%Son_RB
          ELSE
             pQuad_Grid => pQuad_Grid%Son_RT
          ENDIF
       ENDIF
    ENDDO

    RETURN
  END FUNCTION QuadtreeMesh_CellSearch

  !******************************************************************************!

  RECURSIVE SUBROUTINE QuadtreeMesh_CellDraw(pQuad_Grid)
    IMPLICIT NONE

    TYPE (QuadtreeMesh_Cell_Type), POINTER :: pQuad_Grid, p2Quad_Grid
    INTEGER :: ip(4)

    IF(pQuad_Grid%Bisected)THEN
       p2Quad_Grid => pQuad_Grid%Son_LT
       CALL QuadtreeMesh_CellDraw(p2Quad_Grid)
       p2Quad_Grid => pQuad_Grid%Son_LB
       CALL QuadtreeMesh_CellDraw(p2Quad_Grid)
       p2Quad_Grid => pQuad_Grid%Son_RT
       CALL QuadtreeMesh_CellDraw(p2Quad_Grid)
       p2Quad_Grid => pQuad_Grid%Son_RB
       CALL QuadtreeMesh_CellDraw(p2Quad_Grid)
    ELSE
       ip(1:4) = pQuad_Grid%Node(1:4)
       WRITE(29,'(I7,6(1X,E14.7),I3)')ip(1),QuadMesh%Coord(1:2,ip(1)),      &
            Value_Quad_Node(1:3,ip(1)),pQuad_Grid%V(1),Counter_Quad_Node(ip(1))
       WRITE(29,'(I7,6(1X,E14.7),I3)')ip(2),QuadMesh%Coord(1:2,ip(2)),      &
            Value_Quad_Node(1:3,ip(2)),pQuad_Grid%V(1),Counter_Quad_Node(ip(2))
       WRITE(29,'(I7,6(1X,E14.7),I3)')ip(4),QuadMesh%Coord(1:2,ip(4)),      &
            Value_Quad_Node(1:3,ip(4)),pQuad_Grid%V(1),Counter_Quad_Node(ip(4))
       WRITE(29,'(I7,6(1X,E14.7),I3)')ip(3),QuadMesh%Coord(1:2,ip(3)),      &
            Value_Quad_Node(1:3,ip(3)),pQuad_Grid%V(1),Counter_Quad_Node(ip(3))
       WRITE(29,'(I7,6(1X,E14.7),I3)')ip(1),QuadMesh%Coord(1:2,ip(1)),      &
            Value_Quad_Node(1:3,ip(1)),pQuad_Grid%V(1),Counter_Quad_Node(ip(1))
       WRITE(29,*)' '
    ENDIF

    RETURN
  END SUBROUTINE QuadtreeMesh_CellDraw

  !******************************************************************************!

  RECURSIVE SUBROUTINE QuadtreeMesh_Draw(JobName,JobNameLength)
    IMPLICIT NONE

    INTEGER :: JobNameLength
    CHARACTER(LEN=JobNameLength) JobName
    TYPE (QuadtreeMesh_Cell_Type), POINTER :: pQuad_Grid
    INTEGER :: i,j

    OPEN(29,file=JobName(1:JobNameLength)//'.net', status='unknown')

    DO i=1,QuadMesh%imax
       DO j=1,QuadMesh%jmax
          pQuad_Grid => Quad_Grid(i,j)
          CALL QuadtreeMesh_CellDraw(pQuad_Grid)
       ENDDO
    ENDDO

    CLOSE(29)

    RETURN
  END SUBROUTINE QuadtreeMesh_Draw

  !******************************************************************************!

  RECURSIVE SUBROUTINE QuadtreeMesh_CellCheck(pQuad_Grid)
    IMPLICIT NONE

    TYPE (QuadtreeMesh_Cell_Type), POINTER :: pQuad_Grid, p2Quad_Grid, p3Quad_Grid
    INTEGER :: ip(4),ierror,i
    REAL*8  :: x0, y0, dx, dy, xer, yer, xermin, yermin

    IF(pQuad_Grid%Bisected)THEN
       p2Quad_Grid => pQuad_Grid%Son_LT
       CALL QuadtreeMesh_CellCheck(p2Quad_Grid)
       p2Quad_Grid => pQuad_Grid%Son_LB
       CALL QuadtreeMesh_CellCheck(p2Quad_Grid)
       p2Quad_Grid => pQuad_Grid%Son_RT
       CALL QuadtreeMesh_CellCheck(p2Quad_Grid)
       p2Quad_Grid => pQuad_Grid%Son_RB
       CALL QuadtreeMesh_CellCheck(p2Quad_Grid)
    ELSE
       ierror = 0
       DO i=1,4
          IF(i==1)THEN
             p3Quad_Grid => pQuad_Grid%Next_Left
             ip(1:2) = (/  pQuad_Grid%Node(1), pQuad_Grid%Node(3) /)
             ip(3:4) = (/ p3Quad_Grid%Node(2),p3Quad_Grid%Node(4) /)
          ELSE IF(i==2)THEN
             p3Quad_Grid => pQuad_Grid%Next_Right
             ip(1:2) = (/  pQuad_Grid%Node(2), pQuad_Grid%Node(4) /)
             ip(3:4) = (/ p3Quad_Grid%Node(1),p3Quad_Grid%Node(3) /)
          ELSE IF(i==3)THEN
             p3Quad_Grid => pQuad_Grid%Next_Up
             ip(1:2) = (/  pQuad_Grid%Node(3), pQuad_Grid%Node(4) /)
             ip(3:4) = (/ p3Quad_Grid%Node(1),p3Quad_Grid%Node(2) /)
          ELSE IF(i==4)THEN
             p3Quad_Grid => pQuad_Grid%Next_Down
             ip(1:2) = (/  pQuad_Grid%Node(1), pQuad_Grid%Node(2) /)
             ip(3:4) = (/ p3Quad_Grid%Node(3),p3Quad_Grid%Node(4) /)
          ENDIF
          IF(p3Quad_Grid%Level==0) CYCLE

          IF(ABS(p3Quad_Grid%Level-pQuad_Grid%Level)>1)THEN
             ierror = 1
          ELSE IF(p3Quad_Grid%Level-pQuad_Grid%Level==0 .AND.   &
               (ip(1)/=ip(3) .OR.  ip(2)/=ip(4)) )THEN
             ierror = 1
          ELSE IF(ABS(p3Quad_Grid%Level-pQuad_Grid%Level)==1 .AND.   &
               (ip(1)/=ip(3) .AND.  ip(2)/=ip(4)) )THEN
             ierror = 1
          ENDIF
          IF(ierror==1)THEN
             WRITE(*,*)'Error:'
             WRITE(*,*)'Level, left_Level=',pQuad_Grid%Level,p3Quad_Grid%Level
             WRITE(*,*)'Node=',ip(1:4)
             WRITE(*,*)'left_Node=',p3Quad_Grid%Node(1:4)
             CALL Error_Stop ('--- QuadtreeMesh_CellCheck ')
          ENDIF
       ENDDO

       dx  = QuadMesh%DX(pQuad_Grid%Level+1)
       dy  = QuadMesh%DY(pQuad_Grid%Level+1)
       x0  = pQuad_Grid%Centre(1)
       y0  = pQuad_Grid%Centre(2)
       xermin = QuadMesh%DX(Max_Quad_Level+1)
       yermin = QuadMesh%DY(Max_Quad_Level+1)

       xer = QuadMesh%Coord(1,pQuad_Grid%Node(1)) + dx - x0
       yer = QuadMesh%Coord(2,pQuad_Grid%Node(1)) + dy - y0
       IF(ABS(xer)>xermin) ierror = 1

       xer = QuadMesh%Coord(1,pQuad_Grid%Node(2)) - dx - x0
       yer = QuadMesh%Coord(2,pQuad_Grid%Node(2)) + dy - y0
       IF(ABS(xer)>xermin) ierror = 1

       xer = QuadMesh%Coord(1,pQuad_Grid%Node(3)) + dx - x0
       yer = QuadMesh%Coord(2,pQuad_Grid%Node(3)) - dy - y0
       IF(ABS(xer)>xermin) ierror = 1

       xer = QuadMesh%Coord(1,pQuad_Grid%Node(4)) - dx - x0
       yer = QuadMesh%Coord(2,pQuad_Grid%Node(4)) - dy - y0
       IF(ABS(xer)>xermin) ierror = 1

       IF(ierror==1)THEN
          WRITE(*,*)'Error:'
          WRITE(*,*)'Level, left_Level=',pQuad_Grid%Level,p3Quad_Grid%Level
          WRITE(*,*)'Node=',ip(1:4)
          WRITE(*,*)'Posit1=',QuadMesh%Coord(:,pQuad_Grid%Node(1))
          WRITE(*,*)'Posit2=',QuadMesh%Coord(:,pQuad_Grid%Node(2))
          WRITE(*,*)'Posit3=',QuadMesh%Coord(:,pQuad_Grid%Node(3))
          WRITE(*,*)'Posit4=',QuadMesh%Coord(:,pQuad_Grid%Node(4))
          WRITE(*,*)'Centre,dx,dy=',x0,y0,dx,dy
          CALL Error_Stop ('--- QuadtreeMesh_CellCheck ')
       ENDIF

    ENDIF

    RETURN
  END SUBROUTINE QuadtreeMesh_CellCheck

  !******************************************************************************!

  RECURSIVE SUBROUTINE QuadtreeMesh_Check( )
    IMPLICIT NONE

    TYPE (QuadtreeMesh_Cell_Type), POINTER :: pQuad_Grid
    INTEGER :: i,j

    DO i=1,QuadMesh%imax
       DO j=1,QuadMesh%jmax
          pQuad_Grid => Quad_Grid(i,j)
          CALL QuadtreeMesh_CellCheck(pQuad_Grid)
       ENDDO
    ENDDO

    WRITE(*,*)' pass Quad_Web Checking...'

    RETURN
  END SUBROUTINE QuadtreeMesh_Check

  !******************************************************************************!

END MODULE QuadtreeMesh
