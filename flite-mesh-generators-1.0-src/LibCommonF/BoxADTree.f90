!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!    A tree structure used for a list of box cells.
!!
!<
MODULE BoxADTreeModule
  USE Geometry3D
  USE array_allocator
  USE BiTreeNodeModule
  IMPLICIT NONE

  TYPE :: BoxADTreeParameter
     INTEGER :: maxLevel     = 360
     INTEGER :: numDimension = 6
     REAL*8  :: toler        = 1.d-02
     REAL*8, DIMENSION(0:360) :: shiftAmount
     INTEGER,DIMENSION(0:360) :: shiftDim
     LOGICAL :: ready = .FALSE.
  END TYPE BoxADTreeParameter

  TYPE (BoxADTreeParameter),SAVE :: BoxADTreePar

  !>
  !!   The data structure of the tree.
  !<
  TYPE :: BoxADTreeType
     CHARACTER ( LEN = 60 ) :: theName = ' '
     REAL*8  :: Origin(3), ScaleFactor(3)
     TYPE (BiTreeNodeType), POINTER :: Root => null()
     !>   Boxes(1:3,IX)  : normalised position of the minimum corner of box IX.
     !!   Boxes(4:6,IX)  : normalised position of the maximum corner of box IX.
     REAL*8,  DIMENSION(:,:),  POINTER :: Boxes => null()
  END TYPE BoxADTreeType

CONTAINS

  !>
  !!  Prepare the common Parameter.
  !<
  SUBROUTINE BoxADTreeParameter_Prepare( )
    IMPLICIT NONE
    REAL*8  :: xc
    INTEGER :: i, IX
    IF(BoxADTreePar%ready) RETURN
    xc = 0.5d0
    DO i = 0, BoxADTreePar%maxLevel / BoxADTreePar%numDimension - 1
       IX = i * BoxADTreePar%numDimension
       BoxADTreePar%shiftAmount(IX : IX+BoxADTreePar%numDimension-1) = xc
       xc = 0.5d0 * xc
    ENDDO
    DO i = 0, BoxADTreePar%maxLevel
       IX = MOD(i,BoxADTreePar%numDimension) +1
       BoxADTreePar%shiftDim(i) = IX
    ENDDO
    BoxADTreePar%ready = .TRUE.
  END SUBROUTINE BoxADTreeParameter_Prepare

  FUNCTION BoxADTree_getBox(BoxADTree, IX) RESULT (box)
    IMPLICIT NONE
    TYPE (BoxADTreeType) :: BoxADTree
    INTEGER, INTENT(IN) :: IX
    REAL*8  :: box(6)
    box(1:6) = BoxADTree%Boxes(1:6, IX)
  END FUNCTION BoxADTree_getBox

  FUNCTION BoxADTree_normalise(BoxADTree, Pt) RESULT (scale_Pt)
    IMPLICIT NONE
    TYPE (BoxADTreeType) :: BoxADTree
    REAL*8  :: Pt(3), scale_Pt(3)
    scale_Pt(1:3) = ( Pt(1:3) - BoxADTree%Origin(1:3) )    &
         * BoxADTree%ScaleFactor(1:3)
  END FUNCTION BoxADTree_normalise

  !>
  !!   Set a name for this tree (optional function).
  !<
  SUBROUTINE BoxADTree_SetName(BoxADTree, theName)
    IMPLICIT NONE
    TYPE (BoxADTreeType), INTENT(INOUT) :: BoxADTree
    CHARACTER ( LEN = * ), INTENT(IN) :: theName
    BoxADTree%theName(:) = ' '
    BoxADTree%theName = theName
  END SUBROUTINE BoxADTree_SetName

  !>
  !!   Set domain range of the tree.
  !!   @param[in]  pMin       the minimum positions of the domain.
  !!   @param[in]  pMax       the maximum positions of the domain.
  !!   @param[out] BoxADTree     the tree.
  !<
  SUBROUTINE BoxADTree_SetTreeDomain(BoxADTree, pMin, pMax)
    IMPLICIT NONE
    TYPE (BoxADTreeType), INTENT(INOUT) :: BoxADTree
    REAL*8, INTENT(IN)  :: pMin(3), pMax(3)
    REAL*8  ::  pMin1(3), pMax1(3), xc(3)

    CALL BoxADTreeParameter_Prepare( )
    xc(:)    = ( pMax(:) - pMin(:) ) * BoxADTreePar%toler
    pMin1(:) = pMin(:) - xc(:)
    pMax1(:) = pMax(:) + xc(:)
    BoxADTree%Origin(1:3)      = pMin1(1:3)
    BoxADTree%ScaleFactor(1:3) = 1.d0 / (pMax1(1:3)-pMin1(1:3))
    ALLOCATE(BoxADTree%Root)

    RETURN
  END SUBROUTINE BoxADTree_SetTreeDomain

  !>
  !!   Build the tree.
  !!   @param[in]  NB_Box     the number of Boxes.
  !!   @param[in]  pMins      the minimum positions of each box.
  !!   @param[in]  pMaxs      the maximum positions of each box.
  !!   @param[out] BoxADTree     the tree.
  !!
  !!   This routine contains calling SUBROUTINE BoxADTree_SetTreeDomain()
  !<
  SUBROUTINE BoxADTree_SetTree(BoxADTree, NB_Box, pMins, pMaxs)
    IMPLICIT NONE
    TYPE (BoxADTreeType), INTENT(INOUT) :: BoxADTree
    REAL*8, INTENT(IN)  :: pMins(3,*), pMaxs(3,*)
    INTEGER, INTENT(IN) :: NB_Box
    REAL*8  :: pMin(3), pMax(3), xc
    INTEGER :: IX, i

    !--- set domain and scalar
    pMin(:) = pMins(:,1)
    pMax(:) = pMaxs(:,1)
    DO IX = 2,NB_Box
       DO  i = 1,3
          pMin(i) = MIN(pMin(i),pMins(i,IX))
          pMax(i) = MAX(pMax(i),pMaxs(i,IX))
       ENDDO
    ENDDO

    CALL BoxADTree_SetTreeDomain(BoxADTree, pMin, pMax)

    !--- set the tree
    IX = 1.1*NB_Box+1000
    CALL allc_real8_2Dpointer( BoxADTree%Boxes,  6, IX, 'BoxADTree') 
    DO IX = 1,NB_Box
       CALL BoxADTree_AddNode(BoxADTree, IX, pMins(:,IX), pMaxs(:,IX))
    ENDDO

    RETURN
  END SUBROUTINE BoxADTree_SetTree

  !>
  !!   Add a new box to the tree.
  !!   @param[in]  IX        the box ID.
  !!   @param[in]  pMin      the minimum positions of the box.
  !!   @param[in]  pMax      the maximum positions of the box.
  !!   @param[in,out] BoxADTree  the tree.
  !<
  SUBROUTINE BoxADTree_AddNode(BoxADTree, IX, pMin, pMax)
    IMPLICIT NONE

    TYPE (BoxADTreeType), INTENT(INOUT) :: BoxADTree
    INTEGER, INTENT(IN) :: IX
    REAL*8, INTENT(IN)  :: pMin(3), pMax(3)
    REAL*8  :: x(6)
    INTEGER :: id, lev, IX1
    TYPE (BiTreeNodeType), POINTER :: node

    x(1:3)   = BoxADTree_normalise(BoxADTree,pMin(1:3))
    x(4:6)   = BoxADTree_normalise(BoxADTree,pMax(1:3))
    
    IF(keyLength(BoxADTree%Boxes)<IX)THEN
       IX1 = 1.1*IX+10000
       CALL allc_real8_2Dpointer( BoxADTree%Boxes,  6, IX1, 'BoxADTree') 
    ENDIF
    BoxADTree%Boxes(:,IX) = x(1:6)

    DO id = 1,BoxADTreePar%numDimension
       IF(x(id)<=0.d0 .OR. x(id)>=1.d0)THEN
          WRITE(*,*)'New box IX',IX,' is out of BoxADTree::',  &
               TRIM(BoxADTree%theName),' domain'
          CALL Error_STOP ( '--- BoxADTree_AddNode')
       ENDIF
    ENDDO

    node => BoxADTree%Root
    lev  =  0
    DO WHILE( .NOT. BiTreeNodeType_isEmpty(node) )
       !---   walk way down the tree
       lev   = lev + 1
       id    = BoxADTreePar%shiftDim(lev)
       x(id) = 2.d0*x(id)
       IF(x(id)<1.d0) THEN
          node => node%branchLeft
       ELSE
          node => node%branchRight
          x(id) = x(id)-1.d0
       ENDIF
    ENDDO

    !---  add a new tree-node
    CALL BiTreeNodeType_Assign(node,IX,lev)

    IF(lev>BoxADTreePar%maxLevel)THEN
       WRITE(*,*)'Error---  maxLevel is set too small:', BoxADTreePar%maxLevel
       CALL Error_STOP ( '--- BoxADTree_AddNode')
    ENDIF

    RETURN
  END SUBROUTINE BoxADTree_AddNode
  
  !>
  !!   Remove a node (box) from the tree.
  !!   @param[in]  IX     the ID of the removing box.
  !!                      If this box does not exist in the tree,
  !!                      the program will be terminated.
  !!   @param[in,out] BoxADTree   the tree.
  !<
  SUBROUTINE BoxADTree_RemoveNode(BoxADTree, IX)
    IMPLICIT NONE

    TYPE (BoxADTreeType), INTENT(INOUT) :: BoxADTree
    INTEGER, INTENT(IN) :: IX
    REAL*8  :: x(6)
    INTEGER :: id, lev
    TYPE (BiTreeNodeType), POINTER :: node, node0

    x(1:6)   = BoxADTree_getBox(BoxADTree, IX)

    !--- find the node to delete

    node => BoxADTree%Root
    DO WHILE( .NOT. BiTreeNodeType_isEmpty(node) )
       IF(  IX==node%ID ) THEN
          ! ---  found node to be deleted
          EXIT
       ELSE
          !---   walk way down the tree
          lev   = node%Level + 1
          id    = BoxADTreePar%shiftDim(lev)
          x(id) = 2.d0*x(id)
          IF(x(id)<1.d0) THEN
             node => node%branchLeft
          ELSE
             node => node%branchRight
             x(id) = x(id)-1.d0
          ENDIF
       ENDIF
    ENDDO

    IF( BiTreeNodeType_isEmpty(node) )THEN
       WRITE(*,*)'Error---  can not find this Node, IX=', IX
       CALL Error_STOP ( '--- BoxADTree_RemoveNode')
    ENDIF

    !--- Find a node on the tip, 
    !    copy information of this node to the node being deleted,
    !    and delete the node on tip.

    node0 => node
    DO
       IF( .NOT. BiTreeNodeType_isEmpty(node%branchLeft))THEN
          node => node%branchLeft
       ELSE IF( .NOT. BiTreeNodeType_isEmpty(node%branchRight))THEN
          node => node%branchRight
       ELSE 
          EXIT
       ENDIF
    ENDDO

    node0%ID = node%ID  
    CALL BiTreeNodeType_deAssign(node)

    RETURN
  END SUBROUTINE BoxADTree_RemoveNode

  !>
  !!   Search for boxes which a point lies in or a box intersect with.
  !!   @param[in]  BoxADTree    :  the tree.
  !!   @param[in]  Pt        :  the position of the point (if pt2 not present).
  !!   @param[in]  Pt,Pt2    :  the minimum and maximum positions of a box.
  !!   @param[in]  NB        :  the maximum number of output boxes.
  !!   @param[out]  NB        :  the number of boxes found.
  !!   @param[out]  IXs       :  the list of boxes satisfied.
  !<
  SUBROUTINE BoxADTree_SearchNode(BoxADTree, NB, IXs, Pt, Pt2)
    IMPLICIT NONE

    TYPE (BoxADTreeType), INTENT(IN) :: BoxADTree
    INTEGER, INTENT(OUT)  :: IXs(*)
    REAL*8,  INTENT(IN)   :: Pt(3)
    REAL*8,  OPTIONAL, INTENT(IN)   :: Pt2(3)
    INTEGER, INTENT(INOUT):: NB
    INTEGER :: istk, id, lev, IX, NBmax
    REAL*8  :: scale_Pt(6), xl(6), box(6), amov
    TYPE (BiTreeNodeType), POINTER :: node

    TYPE :: clueNode
       TYPE (BiTreeNodeType), POINTER :: node => null()
       REAL*8  :: box(6)
    END TYPE clueNode
    TYPE (clueNode), DIMENSION(BoxADTreePar%maxLevel) :: clue

    NBmax = NB
    NB    = 0
    scale_Pt(1:3) = BoxADTree_normalise(BoxADTree, Pt)
    IF(scale_Pt(1)>=1.d0) RETURN
    IF(scale_Pt(2)>=1.d0) RETURN
    IF(scale_Pt(3)>=1.d0) RETURN
    IF(present(Pt2))THEN
       scale_Pt(4:6) = BoxADTree_normalise(BoxADTree, Pt2)
    ELSE
       scale_Pt(4:6) = scale_Pt(1:3)
    ENDIF
    IF(scale_Pt(4)<=0.d0) RETURN
    IF(scale_Pt(5)<=0.d0) RETURN
    IF(scale_Pt(6)<=0.d0) RETURN

    xl(1:6) =  0.d0
    node    => BoxADTree%Root
    istk    =  0
    
    DO 

       IF( BiTreeNodeType_isEmpty(node) ) THEN
          IF(istk==0)THEN
             !---finish searching
             EXIT
          ENDIF

          node    => clue(istk)%node%branchRight
          xl(1:6) =  clue(istk)%box(1:6)
          istk    = istk-1
          IF( .NOT. BiTreeNodeType_isEmpty(node) ) THEN
             lev    = node%Level
             id     = BoxADTreePar%shiftDim(lev)
             amov   = BoxADTreePar%shiftAmount(lev)
             xl(id) = xl(id)+amov
             IF(id <= 3) THEN
                IF(xl(id)      > scale_Pt(id+3)) node => emptyNode
             ELSE
                IF(xl(id)+amov < scale_Pt(id-3)) node => emptyNode
             ENDIF
          ENDIF
       ELSE
          !--- visit 'node'
          IX       = node%ID
          box(1:6) = BoxADTree_getBox(BoxADTree, IX)

          IF( Geo3D_InsideBox3(scale_Pt, box, 0.d0) )THEN
             NB      = NB+1
             IXs(NB) = IX
             IF(NB>=NBmax) RETURN
          ENDIF

          istk                = istk+1
          clue(istk)%node     => node
          clue(istk)%box(1:6) = xl(1:6)
          node => node%branchLeft
          IF( .NOT. BiTreeNodeType_isEmpty(node) ) THEN
             lev  = node%Level
             id   = BoxADTreePar%shiftDim(lev)
             amov = BoxADTreePar%shiftAmount(lev)
             IF(id <= 3) THEN
                IF(xl(id)      > scale_Pt(id+3)) node => emptyNode
             ELSE
                IF(xl(id)+amov < scale_Pt(id-3)) node => emptyNode
             ENDIF
          ENDIF
       ENDIF

    ENDDO

    IF(NB>=NBmax) CALL Error_STOP ( 'Error---- NB>=NBmax')

    RETURN
  END SUBROUTINE BoxADTree_SearchNode
  
  !>
  !!   Delete the tree and release the memory.
  !<
  SUBROUTINE BoxADTree_Clear(BoxADTree)
    IMPLICIT NONE
    TYPE (BoxADTreeType), INTENT(INOUT) :: BoxADTree
    IF( ASSOCIATED(BoxADTree%Boxes)       ) DEALLOCATE( BoxADTree%Boxes ) 
    IF( .NOT. ASSOCIATED(BoxADTree%Root)  ) RETURN
    IF( .NOT. BiTreeNodeType_isEmpty(BoxADTree%Root) ) CALL BiTreeNodeType_Delete( BoxADTree%Root )
    RETURN
  END SUBROUTINE BoxADTree_Clear
  
  !>
  !!  Output the information of the AD-Tree
  !!  @param[in] BoxADTree  : the tree.
  !!  @param[in] io      : the IO channel. 
  !!                       If =6 or not present, output on screen. 
  !!  @param[out] isReady : if the tree is ready.
  !<
  SUBROUTINE BoxADTree_Info(BoxADTree, io, isReady)
    IMPLICIT NONE
    TYPE (BoxADTreeType), INTENT(IN) :: BoxADTree
    INTEGER, INTENT(IN),  OPTIONAL :: io
    LOGICAL, INTENT(OUT), OPTIONAL :: isReady
    INTEGER :: ic
    ic = 6
    IF(PRESENT(io)) ic = io
    WRITE(ic,*)'  Information for BoxADTree '
    WRITE(ic,*)'     Name:          ', TRIM(BoxADTree%theName)
    IF(BiTreeNodeType_isEmpty (BoxADTree%Root))THEN
       IF(PRESENT(isReady)) isReady = .FALSE.
       WRITE(ic,*)'  The tree is empty!'
       RETURN
    ENDIF
    WRITE(ic,*)'     Origin:        ', REAL(BoxADTree%Origin)
    WRITE(ic,*)'     ScaleFactor:   ', REAL(BoxADTree%ScaleFactor)
    IF(PRESENT(isReady)) isReady = .TRUE.
    RETURN
  END SUBROUTINE BoxADTree_Info

END MODULE BoxADTreeModule


