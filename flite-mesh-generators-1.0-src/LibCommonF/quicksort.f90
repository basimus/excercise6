!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!   Sorting in ascending order of a real*8 array arr(1:n).
!!   return index by indx(1:n).   
!!
!!   Sample: if indx(1)=m, then the m_th element in the array arr
!!        is the smallest element.
!<
SUBROUTINE quicksort(n,arr,indx)
  IMPLICIT NONE
  INTEGER :: n,indx(n)
  REAL*8  :: arr(n)
  INTEGER, PARAMETER :: M=7, NSTACK=50
  INTEGER :: i,indxt,ir,itemp,j,jstack,k,l,istack(NSTACK)
  REAL*8  :: a

  IF(n==0) RETURN

  DO j=1,n
     indx(j)=j
  ENDDO
  jstack=0
  l=1
  ir=n
1 IF(ir-l.LT.M) THEN
     DO j=l+1,ir
        indxt=indx(j)
        a=arr(indxt)
        DO i=j-1,l,-1
           IF(arr(indx(i)).LE.a) GOTO 2
           indx(i+1)=indx(i)
        ENDDO
        i=l-1
2       indx(i+1)=indxt
     ENDDO
     IF(jstack.EQ.0) RETURN
     ir=istack(jstack)
     l=istack(jstack-1)
     jstack=jstack-2
  ELSE
     k=(l+ir)/2
     itemp=indx(k)
     indx(k)=indx(l+1)
     indx(l+1)=itemp
     IF(arr(indx(l)).GT.arr(indx(ir))) THEN
        itemp=indx(l)
        indx(l)=indx(ir)
        indx(ir)=itemp
     ENDIF
     IF(arr(indx(l+1)).GT.arr(indx(ir))) THEN
        itemp=indx(l+1)
        indx(l+1)=indx(ir)
        indx(ir)=itemp
     ENDIF
     IF(arr(indx(l)).GT.arr(indx(l+1))) THEN
        itemp=indx(l)
        indx(l)=indx(l+1)
        indx(l+1)=itemp
     ENDIF
     i=l+1
     j=ir
     indxt=indx(l+1)
     a=arr(indxt)
3    CONTINUE
     i=i+1
     IF(arr(indx(i)).LT.a) GOTO 3
4    CONTINUE
     j=j-1
     IF(arr(indx(j)).GT.a) GOTO 4
     IF(j.LT.i) GOTO 5
     itemp=indx(i)
     indx(i)=indx(j)
     indx(j)=itemp
     GOTO 3
5    indx(l+1)=indx(j)
     indx(j)=indxt
     jstack=jstack+2
     IF(jstack.GT.NSTACK) THEN
        WRITE(6,200) NSTACK
200     FORMAT('Quicksort: NSTACK=',i4,' apparently ',  &
             'too small for this problem')
        RETURN
     ENDIF
     IF(ir-i+1.GE.j-l) THEN
        istack(jstack)=ir
        istack(jstack-1)=i
        ir=j-1
     ELSE
        istack(jstack)=j-1
        istack(jstack-1)=l
        l=i
     ENDIF
  ENDIF
  GOTO 1
END SUBROUTINE quicksort


!>
!!   Sorting in ascending order of an integer array arr(1:n).
!!   return index by indx(1:n).   
!!
!!   Sample: if indx(1)=m, then the m_th element in the array arr
!!        is the smallest element.
!<
SUBROUTINE iquicksort(n,arr,indx)
  IMPLICIT NONE
  INTEGER :: n,indx(n)
  INTEGER :: arr(n)
  INTEGER, PARAMETER :: M=7, NSTACK=50
  INTEGER :: i,indxt,ir,itemp,j,jstack,k,l,istack(NSTACK)
  INTEGER :: a

  IF(n==0) RETURN

  DO j=1,n
     indx(j)=j
  ENDDO
  jstack=0
  l=1
  ir=n
1 IF(ir-l.LT.M) THEN
     DO j=l+1,ir
        indxt=indx(j)
        a=arr(indxt)
        DO i=j-1,l,-1
           IF(arr(indx(i)).LE.a) GOTO 2
           indx(i+1)=indx(i)
        ENDDO
        i=l-1
2       indx(i+1)=indxt
     ENDDO
     IF(jstack.EQ.0) RETURN
     ir=istack(jstack)
     l=istack(jstack-1)
     jstack=jstack-2
  ELSE
     k=(l+ir)/2
     itemp=indx(k)
     indx(k)=indx(l+1)
     indx(l+1)=itemp
     IF(arr(indx(l)).GT.arr(indx(ir))) THEN
        itemp=indx(l)
        indx(l)=indx(ir)
        indx(ir)=itemp
     ENDIF
     IF(arr(indx(l+1)).GT.arr(indx(ir))) THEN
        itemp=indx(l+1)
        indx(l+1)=indx(ir)
        indx(ir)=itemp
     ENDIF
     IF(arr(indx(l)).GT.arr(indx(l+1))) THEN
        itemp=indx(l)
        indx(l)=indx(l+1)
        indx(l+1)=itemp
     ENDIF
     i=l+1
     j=ir
     indxt=indx(l+1)
     a=arr(indxt)
3    CONTINUE
     i=i+1
     IF(arr(indx(i)).LT.a) GOTO 3
4    CONTINUE
     j=j-1
     IF(arr(indx(j)).GT.a) GOTO 4
     IF(j.LT.i) GOTO 5
     itemp=indx(i)
     indx(i)=indx(j)
     indx(j)=itemp
     GOTO 3
5    indx(l+1)=indx(j)
     indx(j)=indxt
     jstack=jstack+2
     IF(jstack.GT.NSTACK) THEN
        WRITE(6,200) NSTACK
200     FORMAT('Quicksort: NSTACK=',i4,' apparently ',  &
             'too small for this problem')
        RETURN
     ENDIF
     IF(ir-i+1.GE.j-l) THEN
        istack(jstack)=ir
        istack(jstack-1)=i
        ir=j-1
     ELSE
        istack(jstack)=j-1
        istack(jstack-1)=l
        l=i
     ENDIF
  ENDIF
  GOTO 1
END SUBROUTINE iquicksort

!>
!!   Return the position in an ascedning integer series arr(1:n).
!!   in which the value identifies the input k.
!!
!!   If fail, return 0.
!<
FUNCTION pointsearch(n,arr,k)
  IMPLICIT NONE
  INTEGER :: n, arr(n),k
  INTEGER :: pointsearch
  INTEGER :: il,ir,m

  IF(n==0 .OR. arr(1)>k .OR. arr(n)<k)THEN
     pointsearch=0
     RETURN
  ENDIF

  il=1
  ir=n
  DO WHILE(ir.GT.il+1)
     m=(il+ir)/2
     IF(arr(m).EQ.k)THEN
        pointsearch=m
        RETURN
     ELSE IF(arr(m).LT.k)THEN
        il=m
     ELSE
        ir=m
     ENDIF
  ENDDO

  IF(arr(il).EQ.k)THEN
     pointsearch=il
  ELSE IF(arr(ir).EQ.k)THEN
     pointsearch=ir
  ELSE
     pointsearch=0
  ENDIF

  RETURN
END FUNCTION pointsearch

!>
!!   Insert a integer k to an ascending integer series arr(1:n).
!!
!!   Do not do anything if k is an element of arr.
!!   Otherwise, return a new ascending integer series arr(1:n+1)
!!   which include the k in a proper position, and reset n=n+1.  \n
!!
!!   MAKE SURE that the input array arr has a length of n+1
!<
SUBROUTINE pointinsert(n,arr,k)
  IMPLICIT NONE
  INTEGER, INTENT (IN OUT) :: n, arr(n+1)
  INTEGER, INTENT (IN) :: k
  INTEGER :: pointsearch
  INTEGER :: il,ir,m

  IF(n==0 .OR. arr(n)<k)THEN
     n=n+1
     arr(n)=k
     RETURN
  ENDIF

  il=1
  ir=n
  IF(arr(1)>=k)THEN
     ir=1
  ENDIF

  DO WHILE(ir.GT.il+1)
     m=(il+ir)/2
     IF(arr(m).EQ.k)THEN
        RETURN
     ELSE IF(arr(m).LT.k)THEN
        il=m
     ELSE
        ir=m
     ENDIF
  ENDDO

  IF(arr(il).EQ.k .OR. arr(ir).EQ.k)THEN
     RETURN
  ELSE
     arr(ir+1:n+1)=arr(ir:n)
     arr(ir)=k
     n=n+1
  ENDIF

  RETURN
END SUBROUTINE pointinsert

!>
!!     An appendix subroutine of iquicksort().
!!     @param[in] n     the number of entries of input array arr.
!!     @param[in] arr   an integer array.
!!     @param[in] indx  the ascend index of the input array arr(1:n),
!!                      i.e. the output of subroutine iquicksort().
!!     @param[in] m     =0, do not remove those entries with the same value.  \n
!!                      else, remove those entries with the same value.
!!     @param[out] n     the number of entries of output array arr.
!!     @param[out] arr   the ascending integer array after sorting. 
!!                      The length of this array might be changed if m/=0.
!!     @param[out] indx  the reverse of input indx.
!!                       if i=indx(j) by output, then the element in place j in
!!                       the input array arr is now move the the place i 
!!                       in the output array arr.
!!                       indx keeps the same length as input.
!<
SUBROUTINE iascending(n,arr,indx,m)
  IMPLICIT NONE
  INTEGER :: n,arr(n),indx(n),m,indx1(n)
  INTEGER :: i,ip

  IF(n==0) RETURN

  arr(1:n)=arr(indx(1:n))
  indx1(indx(1:n))=(/ (i, i=1,n) /)

  IF(m/=0)THEN
     ip=1
     DO i=2,n
        IF(arr(i)/=arr(ip))THEN
           ip=ip+1
           arr(ip)=arr(i)
        ENDIF
        indx1(indx(i))=ip
     ENDDO
     n=ip
  ENDIF

  indx(:)=indx1(:)     ! keep original length

  RETURN
END SUBROUTINE iascending



!>                                                               
!!   Selection sort. Adapted from: R. Sedgewick (1988), "Algorithms".                                       *
!!     @param[in] n     the number of entries of input array arr.
!!     @param[in] arr   an real*8 array.
!!     @param[out] arr   the ascending array after sorting. 
!<
SUBROUTINE SelectionSort(n, arr)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: n
  REAL*8,  INTENT(INOUT), DIMENSION(n)  :: arr
  REAL*8  :: tmp
  INTEGER :: i, j, k

  DO i = 1,n-1
     k = i
     DO j = i+1,n
        IF( arr(j) < arr(k) ) k = j
     ENDDO
     tmp    = arr(k)
     arr(k) = arr(i)
     arr(i) = tmp
  ENDDO
  RETURN
END SUBROUTINE SelectionSort

!>
!!     simple bubble sort (for those small arrays almost being sorted).
!!     @param[in] n     the number of entries of input array arr.
!!     @param[in] arr   an real*8 array.
!!     @param[out] arr   the ascending array after sorting. 
!<
SUBROUTINE BubbleSort(N,arr)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: N
  REAL*8, INTENT(INOUT), DIMENSION(N)   :: arr
  REAL*8  :: tmp
  INTEGER :: I
  LOGICAL :: CarryOn

  CarryOn = .TRUE.  
  DO WHILE (CarryOn)
     CarryOn = .FALSE.
     DO I = 1, N-1
        IF (arr(I) > arr(I+1)) THEN
           tmp      = arr(I)
           arr(I)   = arr(I+1)
           arr(I+1) = tmp
           CarryOn  = .TRUE.
        ENDIF
     ENDDO
  ENDDO
  RETURN
END SUBROUTINE BubbleSort

!>       
!!     sort nodes according to the sign of Mark(:).    
!!     @param[in]    N : the length of Mark.
!!     @param[in]    Mark  (ip)  <0 : node put first;             \n
!!                         (ip)  >0 : node put last;              \n
!!                         (ip)  =0 : node put in middle.
!!     @param[out]   IPfirstEnd   :  the End node in the first part
!!     @param[out]   IPlastStart  :  the Start node in the last part
!!     @param[out]   Mark :   mapping of points. Mark(old_ID) = new_ID.
!!
!!     The order of node in each part is kept.  e.g. if by input ip1<ip2
!!         and sign(Mark_Point(ip1))=sign(Mark_Point(ip2))
!!         then by output we have Mark_Point(ip1)<Mark_Point(ip2).
!!
!<       
SUBROUTINE SignSort(N, Mark, IPfirstEnd, IPlastStart)
  IMPLICIT NONE
  INTEGER, INTENT(IN)    :: N
  INTEGER, INTENT(INOUT) :: Mark(*)
  INTEGER, INTENT(OUT)   :: IPfirstEnd, IPlastStart
  INTEGER :: m, ip

  IPlastStart = N+1
  DO ip = N, 1, -1
     IF(Mark(ip)>0) THEN
        IPlastStart = IPlastStart - 1
        Mark(ip) = IPlastStart
     ENDIF
  ENDDO
  IPfirstEnd = 0
  DO ip = 1,N
     IF(Mark(ip)<0) THEN
        IPfirstEnd = IPfirstEnd + 1
        Mark(ip) = IPfirstEnd
     ENDIF
  ENDDO
  m = IPfirstEnd
  DO ip=1,N
     IF(Mark(ip)==0) THEN
        m = m + 1
        Mark(ip) = m
     ENDIF
  ENDDO
  
  RETURN
END SUBROUTINE SignSort
