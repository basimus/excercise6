!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Definition of background spacing with basic functions.  
!<
MODULE SpacingStorage
  USE ADTreeModule
  USE OctreeMesh
  USE Source_Type
  USE Geometry3DAll
  IMPLICIT NONE

  TYPE :: SpacingStorageType

     !>   (an)isotropic index.
     !!   @param  Model      =  1 :   isotropic domain, fixed GridSize.         \n
     !!                      = -1 : anisotropic domain, fixed stretching.       \n
     !!                      =  2 :   isotropic domain with Tet. background.    \n
     !!                      = -2 : anisotropic domain with Tet. background.    \n
     !!                      =  3 :   isotropic domain with Oct. background.    \n
     !!                      = -3 : anisotropic domain with Oct. background.    \n
     !!                      =  4 :   isotropic domain with source only.        \n
     !!                      = -4 : anisotropic domain with source only.        \n
     !!                      else : error
     !<
     INTEGER :: Model          = 1
     REAL*8  :: BasicSize      = -1.d0                                    !<  Basic grid-size
     REAL*8  :: MinSize        = 1.d30                                    !<  Minimun grid-size
     REAL*8  :: BasicMap(3,3)  =      &
         reshape( (/1,0,0, 0,1,0, 0,0,1/), (/3,3/) )    !<  Basic mapping

     REAL*8  :: pMin(3) =  1.d36        !<   The position of the minimum corner.
     REAL*8  :: pMax(3) = -1.d36        !<   The position of the maximum corner.

     TYPE(SourceGroup_Type) :: Sources   !<  The source-group.
     TYPE(ADTreeType)       :: ADTree    !<  ADTree Tetrahedral mesh.

     REAL*8, DIMENSION(:,:,:), POINTER :: fMap_Point => null()     !<  (3,3,*) : mapping at each tet. mesh node.
     REAL*8, DIMENSION(:    ), POINTER :: Scalar_Point => null()   !<  scalar at each tet. mesh node.

     INTEGER :: NB_BoxSource = 0                                  !<   The number of box-sources.
     TYPE(BoxSource_Type), DIMENSION(:), POINTER :: BoxSources => null()     !<  Box-sources

     TYPE (OctreeMesh_Type) :: Octree       !<   Octree mesh.
     REAL*8, DIMENSION(  :,:), POINTER :: Metric_OctNode => null()    !<  (6,*)   : mapping at each octree node.
     REAL*8, DIMENSION(:,:,:), POINTER :: Mapping_OctNode => null()   !<  (3,3,*) : mapping at each octree node.
     REAL*8, DIMENSION(    :), POINTER :: Scalar_OctNode => null()    !<   scalar at each octree  node.
     REAL*8, DIMENSION(:,:,:), POINTER :: Mapping_OctCell => null()   !<  (3,3,*) : mapping at each octree cell.
     REAL*8, DIMENSION(    :), POINTER :: Scalar_OctCell => null()    !<   scale at each octree cell.

     !>  The level of debug and display.
     !!   A bigger number indicates more checking in the code
     !!        and more output on the screen.
     INTEGER :: CheckLevel = 0

     !>      The method to interpolate two mappings or surface scale.
     !!
     !!      For anisotropic problem, interpolate a mapping by:
     !!  @param Interp_Method
     !!         =1,  by using simultaneous matrix reduction.                   \n
     !!         =2,  by using a matrix power -1, so large grid sizes dominate. \n
     !!         =3,  by using a matrix power  1, so small grid sizes dominate.
     !!
     !!      For isotropic problem, the interpolate scale by:
     !!  @param Interp_Method
     !!         =1,  by using geometric  mean.                                 \n
     !!         =2,  by using harmonic   mean, so large grid sizes dominate.   \n
     !!         =3,  by using arithmetic mean, so small grid sizes dominate.
     INTEGER :: Interp_Method = 2

     !>
     !!  @param OctCell_Vary
     !!         =.FALSE., set the value of a point by the value of the Cube_grid
     !!                   in which the point lies.                                  \n
     !!         =.TRUE.,  set the value of a point by interpolating the Cube_grid
     !!                   in which the point lies. This is a more precise but slower way.
     LOGICAL :: OctCell_Vary = .FALSE.

     !>  Factor of background spacing gradation (0.1~3.0).
     !!      A small value results in a strong gradting and a smoother mesh.
     !!      If >=3.0, no gradation.
     REAL*8  :: Gradation_Factor = 4.d0
  END TYPE SpacingStorageType

CONTAINS

  !>
  !!   Check the BasicSize to see it has been set propertely
  !< 
  SUBROUTINE SpacingStorage_CheckBasicSize(BGSpacing)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    IF(BGSpacing%BasicSize<0)THEN
       WRITE(*,*) 'Negative BasicSize--- might due to no BasicSize import'
       CALL Error_Stop('SpacingStorage_CheckBasicSize')
    ENDIF
    IF(1.001*BGSpacing%BasicSize<BGSpacing%MinSize)THEN
       WRITE(*,*) 'Negative BasicSize--- might due to no MinSize import'
       CALL Error_Stop('SpacingStorage_CheckBasicSize')
    ENDIF
  END SUBROUTINE SpacingStorage_CheckBasicSize
  
  !*******************************************************************************
  !>
  !!   Calculate the space mapping at a specific position from the background mesh.
  !!   @param[in] BGSpacing : the background spacing
  !!   @param[in] P         : the position.
  !!   @param[in] inDomain  =.true.  : if P is not in domain, return (for ADTree);     \n
  !!                        =.false. or not present : if P is not in domain, carry on
  !!                          the computation by expolating (for ADTree).
  !!   @param[out] fMap     : the mapping at P
  !!   @param[out] inDomain =.true.  point P is in the background domain;     \n
  !!                        =.false. point P is NOT in the background domain,
  !!                                 and return fMap by BGSpacing%BasicMap.
  !!
  !!   Remind: the outputs are dimensionless which differs from
  !!           SUBROUTINE SpacingStorage_GetMappingGridSize().
  !<     
  !*******************************************************************************
  SUBROUTINE SpacingStorage_GetMapping(BGSpacing, P, fMap, inDomain)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    REAL*8, INTENT(IN)  :: P(3)
    REAL*8, INTENT(OUT) :: fMap(3,3)
    LOGICAL,INTENT(INOUT), OPTIONAL :: inDomain

    INTEGER :: IT,ip4(4),k,i, Isucc, iso
    REAL*8  :: fMaps(3,3,4), w4(4), fMap1(3,3), Scalar
    TYPE (OctreeMesh_Cell_Type), POINTER ::pOct_Cell
    INTEGER,SAVE :: showWarn1 = 1
    INTEGER,SAVE :: showWarn2 = 1
    LOGICAL :: inDo

    inDo = .FALSE.
    IF(PRESENT(inDomain))THEN
       inDo     = inDomain
       inDomain = .TRUE.
    ENDIF

    IF(BGSpacing%Model==-1)THEN

       fMap = BGSpacing%BasicMap(:,:)

    ELSE IF(BGSpacing%Model==-2)THEN

       !--- Tet. background applied.
       Isucc = -1
       CALL ADTree_SearchNode(BGSpacing%ADTree,P,IT,ip4,w4,Isucc)

       IF(IT==-1)THEN
          !--fail to search
          IF(showWarn1>0)THEN
             showWarn1 = showWarn1 -1
             WRITE(*,*)'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
             WRITE(*,*)'Warning --- SpacingStorage_GetMapping:',       &
                  ' a point is out of background tetrahedra'
             WRITE(*,'(a,3E13.5)')'    P=',P(:)
             WRITE(*,*)'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
          ENDIF
          fMap = BGSpacing%BasicMap
          IF(PRESENT(inDomain)) inDomain = .FALSE.
          RETURN
       ENDIF

       IF(ABS(1.d0-SUM(w4))>0.01)THEN
          WRITE(*,'(a,4E12.4,a,E12.4)')'Error--- w4=',w4,' sum=',SUM(w4)
          CALL Error_STOP ( '--- SpacingStorage_GetMapping')
       ENDIF
       IF(MIN(w4(1),w4(2),w4(3),w4(4))<-0.1)THEN
          IF(inDo)THEN
             fMap = BGSpacing%BasicMap
             inDomain = .FALSE.
             RETURN
          ELSE IF(showWarn2>0)THEN
             showWarn2 = showWarn2 -1
             WRITE(*,*)'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
             WRITE(*,*)'Warning --- SpacingStorage_GetMapping:',       &
                  ' a point is out of background tetrahedra'
             WRITE(*,'(a,3E13.5)')'    P=',P(:)
             WRITE(*,'(a,4E13.5)')'    with weight:',w4
             WRITE(*,*)'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
          ENDIF
       ENDIF

       DO i=1,4
          IF(w4(i)<0.d0)THEN
             w4(:) = w4(:) /(1.d0-w4(i))
             w4(i) = 0.d0
          ENDIF
       ENDDO

       fMaps(:,:,1) = BGSpacing%fMap_Point(:,:,ip4(1))
       fMaps(:,:,2) = BGSpacing%fMap_Point(:,:,ip4(2))
       fMaps(:,:,3) = BGSpacing%fMap_Point(:,:,ip4(3))
       fMaps(:,:,4) = BGSpacing%fMap_Point(:,:,ip4(4))

       DO i=1,4
          IF(1.d0-w4(i)<1.e-6)THEN
             fMap = fMaps(:,:,i)
             EXIT
          ENDIF
       ENDDO
       IF(i>4)THEN
          fMap = Mapping3D_Mean_Weight(4,fMaps,w4,BGSpacing%Interp_Method)
       ENDIF
       IF(SourceGroup_isReady(BGSpacing%Sources))THEN
          fMap1  = SourceGroup_GetMapping(P,BGSpacing%Sources,iso)
          IF(iso==1)THEN
             Scalar = 1.d0 / fMap1(1,1)
             CALL Mapping3D_ScalarIntersect(fMap,Scalar)
          ELSE
             fMap = Mapping3D_Intersect(fMap, fMap1)
          ENDIF
       ENDIF

    ELSE IF(BGSpacing%Model==-3)THEN

       pOct_Cell => OctreeMesh_CellSearch(P,Max_Oct_Level, BGSpacing%Octree)
       IF(pOct_Cell%Level >0)THEN
          CALL SpacingStorage_OctCell_SpaceMapping(BGSpacing, P, pOct_Cell, fMap)
       ELSE
          fMap = BGSpacing%BasicMap(:,:)
          IF(PRESENT(inDomain)) inDomain = .FALSE.
       ENDIF

    ELSE IF(BGSpacing%Model==-4)THEN
       fMap   = SourceGroup_GetMapping(P,BGSpacing%Sources,iso)
    ELSE
       WRITE(*,*)'Error--- illegal Spacing Model: ',BGSpacing%Model, '       '
       CALL Error_STOP ( '--- SpacingStorage_GetMapping')
    ENDIF

    RETURN
  END SUBROUTINE SpacingStorage_GetMapping

  !*******************************************************************************
  !>
  !!   Calculate the space Scalar at a specific position from the background mesh.
  !!   @param[in] BGSpacing : the background spacing
  !!   @param[in] P         : the position.
  !!   @param[in] inDomain  =.true.  : if P is not in domain, return (for ADTree);     \n
  !!                        =.false. or not present : if P is not in domain, carry on
  !!                          the computation by expolating (for ADTree).
  !!   @param[out] Scalar   : the Scalar  at P.
  !!   @param[out] inDomain =.true.  point P is in the background domain;     \n
  !!                        =.false. point P is NOT in the background domain,
  !!                                 and return Scalar by 1.0
  !!
  !!   Remind: the outputs are dimensionless which differs from
  !!           SUBROUTINE SpacingStorage_GetMinGridSize().
  !<     
  !*******************************************************************************
  SUBROUTINE SpacingStorage_GetScale(BGSpacing, P, Scalar, inDomain)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    REAL*8, INTENT(IN)  :: P(3)
    REAL*8, INTENT(OUT) :: Scalar
    LOGICAL,INTENT(INOUT), OPTIONAL :: inDomain

    INTEGER :: IT,ip4(4),k,i, Isucc, iso
    REAL*8  :: w4(4), Scalars(4)
    TYPE (OctreeMesh_Cell_Type), POINTER ::pOct_Cell
    INTEGER,SAVE :: showWarn1 = 1
    INTEGER,SAVE :: showWarn2 = 1
    LOGICAL :: inDo

    inDo = .FALSE.
    IF(PRESENT(inDomain))THEN
       inDo     = inDomain
       inDomain = .TRUE.
    ENDIF

    IF(BGSpacing%Model==1)THEN
       Scalar = 1.d0
    ELSE IF(BGSpacing%Model==2)THEN

       !--- Tet. background applied.
       Isucc = -1
       CALL ADTree_SearchNode(BGSpacing%ADTree,P,IT,ip4,w4,Isucc)

       IF(IT==-1)THEN
          !--fail to search
          IF(showWarn1>0)THEN
             showWarn1 = showWarn1 -1
             WRITE(*,*)'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
             WRITE(*,*)'Warning --- SpacingStorage_GetScale:',       &
                  ' a point is out of background tetrahedra'
             WRITE(*,'(a,3E13.5)')'    P=',P(:)
             WRITE(*,*)'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
          ENDIF
          Scalar = 1.d0
          IF(PRESENT(inDomain)) inDomain = .FALSE.
          RETURN
       ENDIF

       IF(ABS(1.d0-SUM(w4))>0.01)THEN
          WRITE(*,'(a,4E12.4,a,E12.4)')'Error--- w4=',w4,' sum=',SUM(w4)
          CALL Error_STOP ( '--- SpacingStorage_GetScale')
       ENDIF
       IF(MIN(w4(1),w4(2),w4(3),w4(4))<-0.1)THEN
          IF(inDo)THEN
             Scalar   = 1.d0
             inDomain = .FALSE.
             RETURN
          ELSE IF(showWarn2>0)THEN
             showWarn2 = showWarn2 -1
             WRITE(*,*)'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
             WRITE(*,*)'Warning --- SpacingStorage_GetScale:',       &
                  ' a point is out of background tetrahedra'
             WRITE(*,'(a,3E13.5)')'    P=',P(:)
             WRITE(*,'(a,4E13.5)')'    with weight:',w4
             WRITE(*,*)'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
          ENDIF
       ENDIF

       DO i=1,4
          IF(w4(i)<0.d0)THEN
             w4(:) = w4(:) /(1.d0-w4(i))
             w4(i) = 0.d0
          ENDIF
       ENDDO

       Scalars(1:4) = BGSpacing%Scalar_Point(ip4(1:4))
       Scalar = Scalar_Mean_Weight(4,Scalars,w4,BGSpacing%Interp_Method)
       IF(SourceGroup_isReady(BGSpacing%Sources))THEN
          Scalar = MIN(Scalar, SourceGroup_GetScale(P,BGSpacing%Sources))
       ENDIF

    ELSE IF(BGSpacing%Model==3)THEN

       pOct_Cell => OctreeMesh_CellSearch(P,Max_Oct_Level, BGSpacing%Octree)
       IF(pOct_Cell%Level >0)THEN
          CALL SpacingStorage_OctCell_Scalar(BGSpacing, P, pOct_Cell, Scalar)
       ELSE
          Scalar = 1.d0
          IF(PRESENT(inDomain)) inDomain = .FALSE.
       ENDIF

    ELSE IF(BGSpacing%Model==4)THEN
       Scalar = SourceGroup_GetScale(P,BGSpacing%Sources)
    ELSE
       WRITE(*,*)'Error--- illegal Spacing Model: ',BGSpacing%Model, '       '
       CALL Error_STOP ( '--- SpacingStorage_GetScale')
    ENDIF

    RETURN
  END SUBROUTINE SpacingStorage_GetScale

  !*******************************************************************************
  !>
  !!   Calculate minimum grid-size at a specific position
  !!   from the background mesh. 
  !!   @param[in] BGSpacing : the background spacing
  !!   @param[in]  P        : the position.
  !!   @param[out] D        : the Grid-Size (with dimension) at P.
  !<     
  !*******************************************************************************
  SUBROUTINE SpacingStorage_GetMinGridSize(BGSpacing, P,D)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    REAL*8, INTENT(IN)  :: P(3)
    REAL*8, INTENT(OUT) :: D
    REAL*8 :: Scalar,dd(3), fmap(3,3)

    IF(BGSpacing%Model==1)THEN
       D = BGSpacing%BasicSize
    ELSE IF(BGSpacing%Model>1)THEN
       CALL SpacingStorage_GetScale(BGSpacing, P, Scalar)
       D = Scalar * BGSpacing%BasicSize
    ELSE
       CALL SpacingStorage_GetMapping(BGSpacing, P, fMap)
       dd(1:3) = BGSpacing%BasicSize / dsqrt( fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2 )
       D = MIN(dd(1),dd(2),dd(3))
    ENDIF

  END SUBROUTINE SpacingStorage_GetMinGridSize

  !*******************************************************************************
  !>
  !!   Calculate maximum grid-size at a specific position
  !!   from the background mesh. 
  !!   @param[in] BGSpacing : the background spacing
  !!   @param[in]  P        : the position.
  !!   @param[out] D        : the Grid-Size (with dimension) at P.
  !<     
  !*******************************************************************************
  SUBROUTINE SpacingStorage_GetMaxGridSize(BGSpacing, P,D)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    REAL*8, INTENT(IN)  :: P(3)
    REAL*8, INTENT(OUT) :: D
    REAL*8 :: Scalar,dd(3), fmap(3,3)

    IF(BGSpacing%Model==1)THEN
       D = BGSpacing%BasicSize
    ELSE IF(BGSpacing%Model>1)THEN
       CALL SpacingStorage_GetScale(BGSpacing, P, Scalar)
       D = Scalar * BGSpacing%BasicSize
    ELSE
       CALL SpacingStorage_GetMapping(BGSpacing, P, fMap)
       dd(1:3) = BGSpacing%BasicSize / dsqrt( fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2 )
       D = MAX(dd(1),dd(2),dd(3))
    ENDIF

  END SUBROUTINE SpacingStorage_GetMaxGridSize

  !*******************************************************************************
  !>
  !!   Calculate local gridsizes
  !!   along x-,y-,z- direction respectively, at a specific position
  !!   from the background mesh.
  !!   @param[in] BGSpacing : the background spacing
  !!   @param[in]  P        : the position.
  !!   @param[out] D        : local Grid-Sizes along x-,y-,z- direction respectively.
  !<     
  !*******************************************************************************
  SUBROUTINE SpacingStorage_Get3DGridSize(BGSpacing, P,D)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    REAL*8, INTENT(IN)  :: P(3)
    REAL*8, INTENT(OUT) :: D(3)
    REAL*8  :: fMap(3,3), Scalar, dd(3)
    INTEGER :: i

    IF(BGSpacing%Model>0)THEN
       CALL SpacingStorage_GetScale(BGSpacing, P, Scalar)
       D = Scalar * BGSpacing%BasicSize
    ELSE
       CALL SpacingStorage_GetMapping(BGSpacing, P, fMap)
       DO i = 1,3
          dd(:) = 0.d0
          dd(i) = 1.d0
          dd(:) = Mapping3D_Posit_Transf(dd, fmap, 1)
          D(i)  = BGSpacing%BasicSize / dsqrt(dd(1)*dd(1)+dd(2)*dd(2)+dd(3)*dd(3))
       ENDDO
    ENDIF

  END SUBROUTINE SpacingStorage_Get3DGridSize

  !*******************************************************************************
  !>
  !!   Calculate grid-size at a specific position and along a specific direction
  !!   from the background mesh.
  !!   @param[in] BGSpacing : the background spacing
  !!   @param[in]  P        : the position.
  !!   @param[in]  Dir      : the direction.
  !!   @param[out] D        : the grid-size (with dimension) along Dir at P.
  !<     
  !*******************************************************************************
  SUBROUTINE SpacingStorage_GetDirectGridSize(BGSpacing, P,Dir,D)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    REAL*8, INTENT(IN)  :: P(3), Dir(3)
    REAL*8, INTENT(OUT) :: D
    REAL*8  :: DD(3), fMap(3,3), Scalar

    IF(BGSpacing%Model>0)THEN
       CALL SpacingStorage_GetScale(BGSpacing, P, Scalar)
       D = Scalar * BGSpacing%BasicSize
    ELSE
       CALL SpacingStorage_GetMapping(BGSpacing, P, fMap)
       dd = Mapping3D_Posit_Transf(Dir, fMap, 1)
       D  = dsqrt( (dir(1)**2 + dir(2)**2 + dir(3)**2) / (dd(1)**2 + dd(2)**2 + dd(3)**2) )
       D  = D * BGSpacing%BasicSize
    ENDIF

    RETURN
  END SUBROUTINE SpacingStorage_GetDirectGridSize

  !*******************************************************************************
  !>
  !!   Calculate the space mapping at a specific position from the background mesh.
  !!   @param[in] BGSpacing : the background spacing
  !!   @param[in] P         : the position.
  !!   @param[out] fMap     : the space mapping (with dimension) at P.
  !<     
  !*******************************************************************************
  SUBROUTINE SpacingStorage_GetMappingGridSize(BGSpacing, P,fMap)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    REAL*8, INTENT(IN)  :: P(3)
    REAL*8, INTENT(OUT) :: fMap(3,3)
    REAL*8  :: Scalar

    IF(BGSpacing%Model>0)THEN
       CALL SpacingStorage_GetScale(BGSpacing, P, Scalar)
       fmap = 0
       fmap(1,1) = 1.d0 / (Scalar * BGSpacing%BasicSize)
       fmap(2,2) = fmap(1,1)
       fmap(3,3) = fmap(1,1)
    ELSE
       CALL SpacingStorage_GetMapping(BGSpacing, P, fMap)
       fmap = fMap / BGSpacing%BasicSize
    ENDIF

    RETURN
  END SUBROUTINE SpacingStorage_GetMappingGridSize


  !*******************************************************************************
  !>
  !!   Calculate the gradient at a specific position from the background mesh.
  !!   @param[in] BGSpacing : the background spacing
  !!   @param[in]  P        : the position.
  !!   @param[out] D        : space gradient at P.
  !<     
  !*******************************************************************************
  SUBROUTINE SpacingStorage_GetGradient(BGSpacing, P,D)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    REAL*8, INTENT(IN)  :: P(3)
    REAL*8, INTENT(OUT) :: D
    REAL*8  :: fMap(3,3),Scalars(4), w4(4), S, SS(2), Stretch(3), p1(3), p2(3)
    INTEGER :: IT,ip4(4), i, Isucc, i2(2)

    S = 1.d0
    D = 0.d0

    IF(SourceGroup_isReady(BGSpacing%Sources))THEN
       SS = SourceGroup_GetGradient(P,BGSpacing%Sources)
       S  = SS(1)
       D  = SS(2)
    ENDIF

    IF(BGSpacing%ADTree%numPoints>0 .AND. ABS(BGSpacing%Model)==2)THEN
       Isucc = -1
       CALL ADTree_SearchNode(BGSpacing%ADTree,P,IT,ip4,w4,Isucc)
       IF(Isucc>0 .AND. IT>0)THEN
          DO i=1,4
             IF(w4(i)<0.d0)THEN
                w4(:) = w4(:) /(1.d0-w4(i))
                w4(i) = 0.d0
             ENDIF
          ENDDO

          IF(BGSpacing%Model==2)THEN
             Scalars(1:4) = BGSpacing%Scalar_Point(ip4(1:4))
          ELSE IF(BGSpacing%Model==-2)THEN
             DO i=1,4
                fMap(:,:)    = BGSpacing%fMap_Point(:,:,ip4(i))
                Stretch(1:3) = 1.d0 / dsqrt( fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2 )
                Scalars(i)   = MIN(Stretch(1), Stretch(2), Stretch(3))
             ENDDO
          ENDIF

          SS(1) = Scalar_Mean_Weight(4,Scalars,w4,BGSpacing%Interp_Method)
          IF(SS(1)<S)THEN
             D = 0.D0
             Scalars(1:4) = LOG(Scalars(1:4))
             DO i = 1,6
                i2(:) = I_Comb_Tet(1:2,i)
                p1(:) = BGSpacing%ADTree%Posit(:,ip4(i2(1)))
                p2(:) = BGSpacing%ADTree%Posit(:,ip4(i2(2)))
                SS(2) = ABS(Scalars(i2(1))-Scalars(i2(2))) / Geo3D_Distance(P1, P2)
                IF(D<SS(2)) D = SS(2)
             ENDDO
          ENDIF
       ENDIF
    ENDIF


    RETURN
  END SUBROUTINE SpacingStorage_GetGradient

  !*******************************************************************************
  !>
  !!   Linearly interpolate the space mappint at specific point
  !!       from a cell of the octree background mesh.
  !!   @param[in] BGSpacing : the background spacing
  !!   @param[in] P         : the position.
  !!   @param[in] pOct_Cell : the pointer of a octree cell.
  !!   @param[out] fMap     : the mapping (dimensionless) at P.
  !<       
  !*******************************************************************************
  SUBROUTINE SpacingStorage_OctCell_SpaceMapping(BGSpacing, P,pOct_Cell,fMap)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    REAL*8, INTENT(IN)  :: P(3)
    REAL*8, INTENT(OUT) :: fMap(3,3)
    TYPE (OctreeMesh_Cell_Type), POINTER ::pOct_Cell
    REAL*8  :: t, fMap1(3,3), fMap2(3,3), fMap3(3,3), fMap4(3,3)
    INTEGER :: ip(8)

    IF(BGSpacing%OctCell_Vary)THEN
       ip(:) = pOct_Cell%Nodes(:)

       t = ( P(1)-BGSpacing%Octree%Posit(1,ip(1)) ) / BGSpacing%Octree%DX(pOct_Cell%Level)
       fMap1 = Mapping3D_Interpolate(BGSpacing%Mapping_OctNode(:,:,ip(1)),      &
            BGSpacing%Mapping_OctNode(:,:,ip(2)), t, BGSpacing%Interp_Method)
       fMap2 = Mapping3D_Interpolate(BGSpacing%Mapping_OctNode(:,:,ip(4)),      &
            BGSpacing%Mapping_OctNode(:,:,ip(3)), t, BGSpacing%Interp_Method)
       fMap3 = Mapping3D_Interpolate(BGSpacing%Mapping_OctNode(:,:,ip(5)),      &
            BGSpacing%Mapping_OctNode(:,:,ip(6)), t, BGSpacing%Interp_Method)
       fMap4 = Mapping3D_Interpolate(BGSpacing%Mapping_OctNode(:,:,ip(8)),      &
            BGSpacing%Mapping_OctNode(:,:,ip(7)), t, BGSpacing%Interp_Method)

       t = ( P(2)-BGSpacing%Octree%Posit(2,ip(1)) ) / BGSpacing%Octree%DY(pOct_Cell%Level)
       fMap1 = Mapping3D_Interpolate(fMap1,fMap2,t,BGSpacing%Interp_Method)
       fMap2 = Mapping3D_Interpolate(fMap3,fMap4,t,BGSpacing%Interp_Method)

       t = ( P(3)-BGSpacing%Octree%Posit(3,ip(1)) ) / BGSpacing%Octree%DZ(pOct_Cell%Level)
       fMap = Mapping3D_Interpolate(fMap1,fMap2,t, BGSpacing%Interp_Method)
    ELSE
       fMap = BGSpacing%Mapping_OctCell(:,:,pOct_Cell%ID)
    ENDIF

    RETURN
  END SUBROUTINE SpacingStorage_OctCell_SpaceMapping

  !*******************************************************************************
  !>
  !!   Linearly interpolate of scalar mapping at a specific point
  !!       from a cell of the octree background mesh.
  !!   @param[in] BGSpacing : the background spacing
  !!   @param[in] P         : the position.
  !!   @param[in] pOct_Cell : the pointer of a octree cell.
  !!   @param[out] Scalar   : the scale (dimensionless) at P.
  !<       
  !*******************************************************************************
  SUBROUTINE SpacingStorage_OctCell_Scalar(BGSpacing, P,pOct_Cell,Scalar)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    REAL*8, INTENT(IN)  :: P(3)
    REAL*8, INTENT(OUT) :: Scalar
    TYPE (OctreeMesh_Cell_Type), POINTER ::pOct_Cell
    REAL*8  :: t, Scalar1, Scalar2, Scalar3, Scalar4
    INTEGER :: ip(8)

    IF(BGSpacing%OctCell_Vary)THEN
       ip(:) = pOct_Cell%Nodes(:)

       t = ( P(1)-BGSpacing%Octree%Posit(1,ip(1)) ) / BGSpacing%Octree%DX(pOct_Cell%Level)
       Scalar1 = Scalar_Interpolate(BGSpacing%Scalar_OctNode(ip(1)),      &
            BGSpacing%Scalar_OctNode(ip(2)), t, BGSpacing%Interp_Method)
       Scalar2 = Scalar_Interpolate(BGSpacing%Scalar_OctNode(ip(4)),      &
            BGSpacing%Scalar_OctNode(ip(3)), t, BGSpacing%Interp_Method)
       Scalar3 = Scalar_Interpolate(BGSpacing%Scalar_OctNode(ip(5)),      &
            BGSpacing%Scalar_OctNode(ip(6)), t, BGSpacing%Interp_Method)
       Scalar4 = Scalar_Interpolate(BGSpacing%Scalar_OctNode(ip(8)),      &
            BGSpacing%Scalar_OctNode(ip(7)), t, BGSpacing%Interp_Method)

       t = ( P(2)-BGSpacing%Octree%Posit(2,ip(1)) ) / BGSpacing%Octree%DY(pOct_Cell%Level)
       Scalar1 = Scalar_Interpolate(Scalar1,Scalar2,t,BGSpacing%Interp_Method)
       Scalar2 = Scalar_Interpolate(Scalar3,Scalar4,t,BGSpacing%Interp_Method)

       t = ( P(3)-BGSpacing%Octree%Posit(3,ip(1)) ) / BGSpacing%Octree%DZ(pOct_Cell%Level)
       Scalar = Scalar_Interpolate(Scalar1,Scalar2,t, BGSpacing%Interp_Method)
    ELSE
       Scalar = BGSpacing%Scalar_OctCell(pOct_Cell%ID)
    ENDIF

    RETURN
  END SUBROUTINE SpacingStorage_OctCell_Scalar

  !*******************************************************************************
  !>
  !!   Calculate the distance between P1 and P2.
  !!   @param[in] BGSpacing : the background spacing
  !!   @param[in]  P1,P2  coordinates of the two points.
  !!   @param[in]  Dist   (if Kjob=2) the truncated distance.
  !!   @param[out] Dist   (if Kjob=1) the distance of P1-P2 in Rieman space.
  !!   @param[out] P2     (if Kjob=2) updated position which satisfies
  !!                                  |P1-P|=Dist in Riemannion space.
  !<       
  !*******************************************************************************
  SUBROUTINE SpacingStorage_Distance(BGSpacing,P1,P2,Dist)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    REAL*8, INTENT(IN)  :: P1(3), P2(3)
    REAL*8, INTENT(OUT) :: Dist
    REAL*8  :: decline(3), pp1(3), pp2(3), ppc(3), p0(3), d2(3)
    REAL*8  :: Dist1, Dist2, ddir
    REAL*8  :: fMap(3,3), Scalar
    INTEGER :: idirct, kdir, Level, i, k, Ksign
    TYPE (OctreeMesh_Cell_Type), POINTER ::pOct_Cell

    IF(ABS(BGSpacing%Model)/=3)THEN
       CALL Error_Stop('SpacingStorage_Distance:: not for this model')
    ENDIF

    d2(:) = ABS(P2(:)-P1(:))
    Dist = d2(1)
    idirct = 1
    DO i=2,3
       IF(d2(i)>Dist)THEN
          Dist = d2(i)
          idirct = i
       ENDIF
    ENDDO

    decline(:) = (P2(:)-P1(:)) / (P2(idirct)-P1(idirct))

    pp1(:) = P1(:)
    pOct_Cell => OctreeMesh_CellSearch(pp1,Max_Oct_Level,BGSpacing%Octree)
    IF(pOct_Cell%Level == 0)THEN
       WRITE(*,*)'Error--- : A Point is beyond the Octree mesh: pp1=',pp1
       CALL Error_STOP ( 'SpacingStorage_Distance ::')
    ENDIF

    IF( Dist<MIN(BGSpacing%Octree%DX(Max_Oct_Level),    &
         BGSpacing%Octree%DY(Max_Oct_Level),    &
         BGSpacing%Octree%DZ(Max_Oct_Level)) )THEN
       IF(BGSpacing%Model>0)THEN
          CALL SpacingStorage_OctCell_Scalar(BGSpacing, pp1,pOct_Cell,Scalar)
          Dist = Geo3D_Distance(P1,P2) / Scalar
       ELSE
          CALL SpacingStorage_OctCell_SpaceMapping(BGSpacing, pp1,pOct_Cell,fMap)
          Dist = Mapping3D_Distance(P1,P2,fMap)
       ENDIF
       RETURN
    ENDIF

    Dist = 0

    DO

       Level = pOct_Cell%Level
       p0(:) = OctreeMesh_getCellCentre(pOct_Cell,BGSpacing%Octree)
       d2(1) = BGSpacing%Octree%DX(Level+1)
       d2(2) = BGSpacing%Octree%DY(Level+1)
       d2(3) = BGSpacing%Octree%DZ(Level+1)

       pp2(:) = p0(:)+d2(:)
       WHERE(P2(:)<P1(:)) pp2(:) = p0(:)-d2(:)

       d2(:) = pp2(:)-pp1(:)
       ddir  = pp2(idirct)-pp1(idirct)
       kdir  = idirct
       DO i=1,3
          IF(i==idirct) CYCLE
          IF(ABS(d2(i))<ABS(ddir*decline(i)))THEN
             ddir = d2(i)/decline(i)
             kdir = i
          ENDIF
       ENDDO

       pp2(:) = pp1(:) + ddir*decline(:)
       ppc(:) = ( pp1(:) + pp2(:) ) /2.d0
       IF(BGSpacing%Model>0)THEN
          CALL SpacingStorage_OctCell_Scalar(BGSpacing, ppc,pOct_Cell,Scalar)
          Dist2 = Geo3D_Distance(pp1,pp2) / Scalar
       ELSE
          CALL SpacingStorage_OctCell_SpaceMapping(BGSpacing, ppc,pOct_Cell,fMap)
          Dist2 = Mapping3D_Distance(pp1,pp2,fMap)
       ENDIF

       IF(ABS(pp2(idirct)-pp1(idirct))>=ABS(P2(idirct)-pp1(idirct)))THEN
          pp2(:) = P2(:)
          Dist2  = Dist2 * (P2(Idirct)-pp1(Idirct)) / (pp2(Idirct)-pp1(Idirct))
          kdir   = 0
       ENDIF
       Dist = Dist + Dist2

       IF(kdir==0) EXIT

       pp1(:) = pp2(:)

       Ksign = 1
       IF(P2(Kdir)<P1(Kdir)) Ksign = -1
       pOct_Cell => OctreeMesh_NextCell(pOct_Cell,2*Kdir+(Ksign-1)/2)
       IF(pOct_Cell%Level > Level)THEN
          DO i=Kdir,Kdir+1
             k = MOD(i,3)+1
             IF(Ksign*(pp2(k)-p0(k)) < 0)    &
                  pOct_Cell =>  OctreeMesh_NextCell(pOct_Cell,2*k-(1+Ksign)/2)
          ENDDO
       ENDIF

       IF(pOct_Cell%Level == 0)THEN
          WRITE(*,*)'Error--- : A Point is beyond the Octree mesh: pp2=',pp2
          WRITE(*,*)'    P1=',P1
          WRITE(*,*)'    P2=',P2
          WRITE(*,*)'   pp2=',pp2
          CALL Error_STOP ( 'SpacingStorage_Distance ::')
       ENDIF

    ENDDO

    RETURN
  END SUBROUTINE SpacingStorage_Distance

  !>
  !!  Output the information of the spacing.
  !!  @param[in] BGSpacing  : the sbackground spacing.
  !!  @param[in] io         : the IO channel. 
  !!                         If =6 or not present, output on screen. 
  !!  @param[out] isReady   : if the spacing is ready.
  !<
  SUBROUTINE SpacingStorage_info(BGSpacing, io, isReady)
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    INTEGER, INTENT(IN),  OPTIONAL :: io
    LOGICAL, INTENT(OUT), OPTIONAL :: isReady
    INTEGER :: ic
    LOGICAL :: ch
    ic = 6
    IF(PRESENT(io)) ic = io
    IF(PRESENT(isReady)) isReady = .TRUE.

    WRITE(ic,*)' Information of Background Spacing '
    WRITE(ic,*)'  MODEL: ', BGSpacing%Model
    WRITE(ic,*)'  Global & Minimum GridSize: ',    &
         REAL(BGSpacing%BasicSize), REAL(BGSpacing%MinSize)
    IF(BGSpacing%Model==1) THEN
       WRITE(ic,*)'  An isotropic space with fixed GridSize. '
    ELSE IF(BGSpacing%Model==-1) THEN
       WRITE(ic,*)'  An anisotropic space with fixed GridSize and space mapping. '
       WRITE(ic,*)'  Global Space Mapping:'
       WRITE(ic,*)'      ', REAL(BGSpacing%BasicMap(1,:))
       WRITE(ic,*)'      ', REAL(BGSpacing%BasicMap(1,:))
       WRITE(ic,*)'      ', REAL(BGSpacing%BasicMap(1,:))
       RETURN
    ELSE IF(ABS(BGSpacing%Model)==2) THEN
       IF(BGSpacing%Model==2) THEN
          WRITE(ic,*)'  An isotropic space with tetrahedral mesh.'
       ELSE
          WRITE(ic,*)'  An anisotropic space with tetrahedral mesh.'
       ENDIF
       WRITE(ic,*)'  Interpolation Method: ', BGSpacing%Interp_Method
       CALL ADTree_Info(BGSpacing%ADTree, ic, ch)
       IF(.NOT. ch)THEN
          WRITE(ic,*)'  Warning--- The AD-Tree for tetrahedral mesh is NOT ready!'
          IF(PRESENT(isReady)) isReady = .FALSE.
       ENDIF
       IF(SourceGroup_isReady (BGSpacing%Sources))THEN
          CALL SourceGroup_info(BGSpacing%Sources, ic)
       ENDIF
    ELSE IF(ABS(BGSpacing%Model)==3) THEN
       IF(BGSpacing%Model==3) THEN
          WRITE(ic,*)'  An isotropic space with octree mesh.'
       ELSE
          WRITE(ic,*)'  An anisotropic space with octree mesh.'
       ENDIF
       WRITE(ic,*)'  OctCell_Vary:         ', BGSpacing%OctCell_Vary
       WRITE(ic,*)'  Interpolation Method: ', BGSpacing%Interp_Method
       IF(BGSpacing%Octree%numNodes==0)THEN
          WRITE(ic,*)'  Warning--- The octree is NOT ready!'
          IF(PRESENT(isReady)) isReady = .FALSE.
       ELSE
          CALL OctreeMesh_info(BGSpacing%Octree, ic)
       ENDIF
       IF(BGSpacing%OctCell_Vary)THEN
          WRITE(ic,*)'  The spacing varies in each octree cubic cell'
          IF(BGSpacing%Model==3) THEN
             IF(.NOT. ASSOCIATED(BGSpacing%Scalar_OctNode))THEN
                WRITE(ic,*)'   Warning--- Scalar_OctNode is NOT ready!'
                WRITE(ic,*)'              You need to call SpacingStorage_FillOctCell()'
                IF(PRESENT(isReady)) isReady = .FALSE.
             ENDIF
          ELSE
             IF(.NOT. ASSOCIATED(BGSpacing%Mapping_OctNode))THEN
                WRITE(ic,*)'   Warning--- Mapping_OctNode is NOT ready!'
                WRITE(ic,*)'              You need to call SpacingStorage_FillOctCell()'
                IF(PRESENT(isReady)) isReady = .FALSE.
             ENDIF
          ENDIF
       ELSE
          WRITE(ic,*)'  The spacing is a constant in each octree cubic cell'
          IF(BGSpacing%Model==3) THEN
             IF(.NOT. ASSOCIATED(BGSpacing%Scalar_OctCell))THEN
                WRITE(ic,*)'   Warning--- Scalar_OctCell is NOT ready!'
                WRITE(ic,*)'              You need to call SpacingStorage_FillOctCell()'
                IF(PRESENT(isReady)) isReady = .FALSE.
             ENDIF
          ELSE
             IF(.NOT. ASSOCIATED(BGSpacing%Mapping_OctCell))THEN
                WRITE(ic,*)'   Warning--- Mapping_OctCell is NOT ready!'
                WRITE(ic,*)'              You need to call SpacingStorage_FillOctCell()'
                IF(PRESENT(isReady)) isReady = .FALSE.
             ENDIF
          ENDIF
       ENDIF
    ELSE IF(ABS(BGSpacing%Model)==4) THEN
       IF(BGSpacing%Model==4) THEN
          WRITE(ic,*)'  An isotropic space controlled by sources.'
       ELSE
          WRITE(ic,*)'  An anisotropic space controlled by sources.'
       ENDIF
       IF(SourceGroup_isReady (BGSpacing%Sources))THEN
          CALL SourceGroup_info(BGSpacing%Sources, ic)
       ELSE
          WRITE(ic,*)'  Warning--- No Source involved.'
          IF(PRESENT(isReady)) isReady = .FALSE.
       ENDIF
    ELSE
       WRITE(ic,*)'  Warning--- An illegal space'
       IF(PRESENT(isReady)) isReady = .FALSE.
    ENDIF

  END SUBROUTINE SpacingStorage_info

  !*******************************************************************************
  !>
  !!   @param[in] BGSpacing : the background spacing
  !!   @return = true  : no sources or spacing has been set.
  !<     
  !*******************************************************************************
  FUNCTION SpacingStorage_isEmpty(BGSpacing) RESULT(yesno)
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    LOGICAL :: yesno
    yesno = .FALSE.
    IF(SourceGroup_isReady(BGSpacing%Sources) ) RETURN
    IF(BGSpacing%NB_BoxSource               >0) RETURN
    IF(KeyLength(BGSpacing%fMap_Point)      >0) RETURN
    IF(KeyLength(BGSpacing%Scalar_Point)    >0) RETURN
    IF(KeyLength(BGSpacing%Metric_OctNode)  >0) RETURN
    IF(KeyLength(BGSpacing%Mapping_OctNode) >0) RETURN
    IF(KeyLength(BGSpacing%Scalar_OctNode)  >0) RETURN
    IF(KeyLength(BGSpacing%Mapping_OctCell) >0) RETURN
    IF(KeyLength(BGSpacing%Scalar_OctCell)  >0) RETURN
    yesno = .TRUE.
  END FUNCTION SpacingStorage_isEmpty


END MODULE SpacingStorage


