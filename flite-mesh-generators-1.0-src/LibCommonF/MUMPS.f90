!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


#ifdef _MUMPS
!******************************************************************************!
!                                                                                                !
!******************************************************************************!
      MODULE MUMPS_MODULE
         IMPLICIT NONE
          
         INCLUDE 'mpif.h'
         INCLUDE 'dmumps_struc.h'
         TYPE (DMUMPS_STRUC) :: MUMPS_MAT
          
         CONTAINS
          
!>
!!   Nv   : the number of unknowns (size of the matrix)
!!   Nz   : the number of none-zeros in the matrix
!<
         SUBROUTINE MUMPS_create(Nv, Nz, IRN, JCN, A)
            IMPLICIT NONE
            INTEGER, INTENT(IN) :: Nv, Nz
            INTEGER, DIMENSION(:), INTENT(IN) :: IRN, JCN
            REAL*8,  DIMENSION(:), INTENT(IN) :: A
            INTEGER :: IERR
          
            CALL MPI_INIT(IERR)
            MUMPS_MAT%COMM = MPI_COMM_WORLD
            MUMPS_MAT%JOB  = -1
            MUMPS_MAT%SYM  = 2
            MUMPS_MAT%PAR  = 1
            CALL DMUMPS(MUMPS_MAT)

            ALLOCATE(MUMPS_MAT%IRN(Nz))
            ALLOCATE(MUMPS_MAT%JCN(Nz))
            ALLOCATE(MUMPS_MAT%A(Nz))
           
            MUMPS_MAT%IRN(1:Nz) = IRN(1:Nz)
            MUMPS_MAT%JCN(1:Nz) = JCN(1:Nz)
            MUMPS_MAT%A(1:Nz)   = A(1:Nz)

            MUMPS_MAT%NZ = Nz
            MUMPS_MAT%N  = Nv
            MUMPS_MAT%ICNTL(1)=1          !---- Only error message output
            MUMPS_MAT%ICNTL(3)=0          !---- No global information output
            MUMPS_MAT%JOB = 4
            CALL DMUMPS(MUMPS_MAT)
          
            IF(MUMPS_MAT%INFO(1)>0)THEN
               WRITE(*,*)' Warning--- info for MUMPS_create:',MUMPS_MAT%INFO(1)
            ELSEIF(MUMPS_MAT%INFO(1)<0)THEN
               WRITE(*,*)' Error--- info for MUMPS_create:',MUMPS_MAT%INFO(1)
               STOP 'MUMPS_create::'
            ENDIF
            
            ALLOCATE( MUMPS_MAT%RHS(MUMPS_MAT%N) )
            DEALLOCATE(MUMPS_MAT%IRN)
            DEALLOCATE(MUMPS_MAT%JCN)
            DEALLOCATE(MUMPS_MAT%A)

         END SUBROUTINE MUMPS_create

!>
!!   Nv   : the number of unknowns (length of the R)
!<
         SUBROUTINE MUMPS_solver(Nv, R)
            IMPLICIT NONE
            INTEGER, INTENT(IN) :: Nv
            REAL*8,  DIMENSION(:), INTENT(INOUT) :: R
            MUMPS_MAT%RHS(1:NV) = R(1:Nv)
            MUMPS_MAT%JOB = 3
            CALL DMUMPS(MUMPS_MAT)          
            IF(MUMPS_MAT%INFO(1)>0)THEN
               WRITE(*,*)' Warning--- info for MUMPS_solver:',MUMPS_MAT%INFO(1)
            ELSEIF(MUMPS_MAT%INFO(1)<0)THEN
               WRITE(*,*)' Error--- info for MUMPS_solver:',MUMPS_MAT%INFO(1)
               STOP 'MUMPS_solver::'
            ENDIF
            R(1:Nv) = MUMPS_MAT%RHS(1:NV)         
         END SUBROUTINE MUMPS_solver
          
!>
!!     Destroy the instance (deallocate internal data structures)               
!<
         SUBROUTINE MUMPS_destroy( )
            IMPLICIT NONE
            INTEGER :: IERR
            MUMPS_MAT%JOB = -2
            CALL DMUMPS(MUMPS_MAT)
            CALL MPI_FINALIZE(IERR)
         END SUBROUTINE MUMPS_destroy
          
      END MODULE MUMPS_MODULE
      
#else

      MODULE MUMPS_MODULE
         CONTAINS
          
         SUBROUTINE MUMPS_create(Nv, Nz, IRN, JCN, A)
            INTEGER, INTENT(IN) :: Nv, Nz
            INTEGER, DIMENSION(:), INTENT(IN) :: IRN, JCN
            REAL*8,  DIMENSION(:), INTENT(IN) :: A
            STOP 'MUMPS_create::---- no MUMPS library'
         END SUBROUTINE MUMPS_create

         SUBROUTINE MUMPS_solver(Nv, R)
            INTEGER, INTENT(IN) :: Nv
            REAL*8,  DIMENSION(:), INTENT(INOUT) :: R
            STOP 'MUMPS_solver::---- no MUMPS library'
         END SUBROUTINE MUMPS_solver          
          
         SUBROUTINE MUMPS_destroy( )
            IMPLICIT NONE
            STOP 'MUMPS_destroy::---- no MUMPS library'
         END SUBROUTINE MUMPS_destroy
          
      END MODULE MUMPS_MODULE
                
          
#endif
