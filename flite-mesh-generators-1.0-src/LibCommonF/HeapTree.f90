!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  A binary tree structure (heap) always make the first node (root) the smallest.
!<
MODULE HeapTree
  !>
  !!     The value the length front is used to set up a "priority queue" for
  !!     the sides in the generation front. The priority queue is represented 
  !!     by a binary tree (heap structure) with the number of the side with the
  !!     smallest value on the root of the heap. The value for a father node in
  !!     the heap structure is always smaller than the values of its two sons, i.e. 
  !!                             
  !!    Sort the list %v(:) to get a index %toList(:) so that
  !!    for any i (1<=i<=numNodes) there are                                     \n
  !!            %v(%toList(i)) <= %v(%toList(2*i))     if 2*i<=numNodes          \n
  !!    and     %v(%toList(i)) <= %v(%toList(2*i+1))   if 2*i+1<=numNodes
  !!
  !!    So, the %v(%toList(1)) is the smallest value in the list.
  !!
  !!    %toHeap(%toList(i)) = i
  !!
  !!    The default setting of a heap is ascending.
  !!    To build a descending heap tree, make sure %Ascending == .FALSE.
  !<
  TYPE  :: HeapTreeType
     !>  IF =.true. (default), the heap tree is ascending;
     !!  IF = .false., the heap tree is descending.
     LOGICAL  :: Ascending = .TRUE.
     !>  The number of entries.
     INTEGER  :: numNodes = 0
     !>  If toList(iH)=iL, then the node iH in the heap is the iL-th node of the list v(:).
     INTEGER, DIMENSION(:), POINTER :: toList => null()
     !>  If toHeap(iL)=iH, then the iL-th node of the list v(:) take the iH-th position in the heap.
     INTEGER, DIMENSION(:), POINTER :: toHeap => null()
     !>  A list of values.
     REAL*8,  DIMENSION(:), POINTER :: v => null()
  END TYPE HeapTreeType

  !>
  !!     The integer version of HeapTreeType
  !<
  TYPE  :: IntHeapTreeType
     !>  IF =.true. (default), the heap tree is ascending;
     !!  IF = .false., the heap tree is descending.
     LOGICAL  :: Ascending = .TRUE.
     !>  The number of entries.
     INTEGER  :: numNodes = 0
     !>  If toList(iH)=iL, then the node iH in the heap is the iL-th node of the list iv(:).
     INTEGER, DIMENSION(:), POINTER :: toList => null()
     !>  If toHeap(iL)=iH, then the iL-th node of the list iv(:) take the iH-th position in the heap.
     INTEGER, DIMENSION(:), POINTER :: toHeap => null()
     !>  A list of values.
     INTEGER,  DIMENSION(:), POINTER :: iv => null()
  END TYPE IntHeapTreeType

CONTAINS

  !>
  !!  Allocate memory the heap.
  !!
  !<
  SUBROUTINE HeapTree_Allocate( Heap, nsl )
    IMPLICIT NONE
    TYPE(HeapTreeType),  INTENT(INOUT)  :: Heap
    INTEGER, INTENT(IN)  :: nsl
    CALL HeapTree_Clear( Heap )
    ALLOCATE(Heap%toList(nsl), Heap%toHeap(nsl), Heap%v(nsl))
    RETURN
  END SUBROUTINE HeapTree_Allocate

  !>
  !!  Build the heap from an array of valurs.
  !!
  !<
  SUBROUTINE HeapTree_Build( nsl, v, Ascending, Heap )
    IMPLICIT NONE
    TYPE(HeapTreeType),  INTENT(INOUT)  :: Heap
    REAL*8,  INTENT(IN)  :: v(*)
    INTEGER, INTENT(IN)  :: nsl
    LOGICAL, INTENT(IN)  :: Ascending
    INTEGER  :: is

    Heap%Ascending = Ascending
    CALL HeapTree_Allocate( Heap, nsl )
    DO is=1,nsl
       CALL HeapTree_AddValue(Heap, v(is) )
    ENDDO

    RETURN
  END SUBROUTINE HeapTree_Build

  !>
  !!   Add a value to the heap.
  !<
  SUBROUTINE HeapTree_AddValue( Heap, v )
    USE  array_allocator
    IMPLICIT NONE
    TYPE(HeapTreeType),  INTENT(INOUT) :: Heap
    REAL*8,  INTENT(IN)    :: v
    INTEGER  :: is, iso, ifa

    Heap%numNodes = Heap%numNodes + 1
    IF(Heap%numNodes>SIZE(Heap%toList))THEN
       CALL allc_1Dpointer(Heap%toList,  Heap%numNodes+10000, 'Heap')
       CALL allc_1Dpointer(Heap%toHeap,  Heap%numNodes+10000, 'Heap')
       CALL allc_1Dpointer(Heap%v,       Heap%numNodes+10000, 'Heap')
    ENDIF

    Heap%v(Heap%numNodes)      = v
    Heap%toHeap(Heap%numNodes) = Heap%numNodes
    Heap%toList(Heap%numNodes) = Heap%numNodes
    IF(Heap%numNodes==1) RETURN

    ! *** Updates the heap structure going up the binary tree

    iso = Heap%numNodes                      ! son
    ifa = Heap%numNodes/2                    ! father
    DO WHILE(ifa>0)
       IF( Heap%Ascending )THEN
          IF(Heap%v(Heap%toList(iso)) >= Heap%v(Heap%toList(ifa))) EXIT
       ELSE
          IF(Heap%v(Heap%toList(iso)) <= Heap%v(Heap%toList(ifa))) EXIT
       ENDIF
       CALL HeapTree_SwapIndx( Heap, iso, ifa )
       iso  = ifa
       ifa  = iso/2
    ENDDO

    RETURN
  END SUBROUTINE HeapTree_AddValue

  !>
  !!   change a value of a node of the list, and resort the heap.
  !<
  SUBROUTINE HeapTree_ResetNodeValue( Heap, isid, v )
    IMPLICIT NONE
    TYPE(HeapTreeType),  INTENT(INOUT) :: Heap
    INTEGER, INTENT(IN) :: isid
    REAL*8,  INTENT(IN) :: v
    INTEGER  :: iso, is2, ifa
    REAL*8   :: as0, as1, as2

    as0 = Heap%v(isid)
    IF(as0==v) RETURN
    Heap%v(isid) = v

    IF(Heap%Ascending .EQV. as0<v)THEN

       ! *** Updates the heap structure going down the binary tree

       ifa = Heap%toHeap(isid)
       iso = 2*ifa                       ! left son
       DO WHILE(iso<=Heap%numNodes)
          as1 = Heap%v(Heap%toList(iso))
          is2 = iso+1                    ! right son
          IF(is2<=Heap%numNodes) THEN
             as2 = Heap%v(Heap%toList(is2))
             IF(Heap%Ascending .EQV. as2 < as1) THEN         ! checks the key
                iso = is2
                as1 = as2
             ENDIF
          ENDIF

          IF(Heap%Ascending)THEN
             IF(as1 >= v) RETURN
          ELSE
             IF(as1 <= v) RETURN
          ENDIF

          CALL HeapTree_SwapIndx( Heap, iso, ifa )
          ifa = iso
          iso = 2*ifa
       ENDDO

    ELSE IF(Heap%Ascending .EQV. as0>v)THEN

       ! *** Updates the heap structure going up the binary tree

       iso = Heap%toHeap(isid)          ! son
       ifa = iso/2                      ! father
       DO WHILE(ifa>0)
          IF(Heap%Ascending)THEN
             IF(v >= Heap%v(Heap%toList(ifa))) RETURN
          ELSE
             IF(v <= Heap%v(Heap%toList(ifa))) RETURN
          ENDIF
          CALL HeapTree_SwapIndx( Heap, iso, ifa )
          iso  = ifa
          ifa  = iso/2
       ENDDO

    ENDIF

    RETURN
  END SUBROUTINE HeapTree_ResetNodeValue

  !>
  !!   Remove a node from the list and resort the heap.
  !!   The last node of the list will take the removed position, and 
  !!   length of the list will be reduced by 1.
  !<
  SUBROUTINE HeapTree_RemoveNode( Heap, isid )
    IMPLICIT NONE
    TYPE(HeapTreeType),  INTENT(INOUT) :: Heap
    INTEGER, INTENT(IN) :: isid
    INTEGER  :: ifa, iLs
    REAL*8   :: as0

    ! *** Replaces the link with "isid" by the last entry in the heap
    Heap%numNodes     = Heap%numNodes -1
    ifa               = Heap%toHeap(isid)
    iLs               = Heap%toList(Heap%numNodes+1)
    Heap%toList(ifa)  = iLs
    Heap%toHeap(iLs)  = ifa

    !--- Resort the heap
    as0         = Heap%v(iLs)
    Heap%v(iLs) = Heap%v(isid)
    CALL HeapTree_ResetNodeValue( Heap, iLs, as0 )

    !--- replace the node by the last value in the list
    IF( isid /= Heap%numNodes+1 ) THEN
       Heap%v(isid)      = Heap%v(Heap%numNodes+1)
       Heap%toHeap(isid) = Heap%toHeap(Heap%numNodes+1)
       Heap%toList(Heap%toHeap(Heap%numNodes+1)) = isid
    ENDIF

    RETURN
  END SUBROUTINE HeapTree_RemoveNode

  !>
  !!   Check the heap tree.
  !<
  SUBROUTINE HeapTree_Check( Heap )
    IMPLICIT NONE
    TYPE(HeapTreeType),  INTENT(IN) :: Heap
    INTEGER :: N, i, iH, iL, iL2, k
    N = Heap%numNodes
    DO i = 1,N
       iH = Heap%toHeap(i)
       iL = Heap%toList(i)
       IF(iH<=0 .OR. iH>N)    CALL Error_STOP ( '--- HeapTree_Check ---1')
       IF(iL<=0 .OR. iL>N)    CALL Error_STOP ( '--- HeapTree_Check ---2')
       IF(Heap%toList(iH)/=i) CALL Error_STOP ( '--- HeapTree_Check ---3')
       IF(Heap%toHeap(iL)/=i) CALL Error_STOP ( '--- HeapTree_Check ---4')
       DO k = 0,1
          IF(2*i+k>N) EXIT
          iL2 = Heap%toList(2*i+k)
          IF(      Heap%Ascending  .AND. Heap%v(iL) > Heap%v(iL2))       &
                              CALL Error_STOP ( '--- HeapTree_Check ---5')
          IF((.NOT.Heap%Ascending) .AND. Heap%v(iL) < Heap%v(iL2))       &
                              CALL Error_STOP ( '--- HeapTree_Check ---6')
       ENDDO
    ENDDO
  END SUBROUTINE HeapTree_Check


  !>
  !!   Internal function.
  !<
  SUBROUTINE HeapTree_SwapIndx( Heap, iso, ifa )
    IMPLICIT NONE
    TYPE(HeapTreeType),  INTENT(INOUT)  :: Heap
    INTEGER, INTENT(IN)   :: iso, ifa
    INTEGER  :: ikeep
    Heap%toHeap(Heap%toList(iso)) = ifa
    Heap%toHeap(Heap%toList(ifa)) = iso
    ikeep             = Heap%toList(iso)
    Heap%toList(iso)  = Heap%toList(ifa)
    Heap%toList(ifa)  = ikeep
    RETURN
  END SUBROUTINE HeapTree_SwapIndx

  !>
  !!   Delete the heap and release the memory.
  !<
  SUBROUTINE HeapTree_Clear(Heap)
    IMPLICIT NONE
    TYPE(HeapTreeType),  INTENT(INOUT) :: Heap
    Heap%numNodes = 0
    IF(ASSOCIATED(Heap%toList)) DEALLOCATE(Heap%toList)
    IF(ASSOCIATED(Heap%toHeap)) DEALLOCATE(Heap%toHeap)
    IF(ASSOCIATED(Heap%v))    DEALLOCATE(Heap%v)
  END SUBROUTINE HeapTree_Clear


  !>
  !!  Allocate memory the heap.
  !!
  !<
  SUBROUTINE IntHeapTree_Allocate( Heap, nsl )
    IMPLICIT NONE
    TYPE(IntHeapTreeType),  INTENT(INOUT)  :: Heap
    INTEGER, INTENT(IN)  :: nsl
    CALL IntHeapTree_Clear( Heap )
    ALLOCATE(Heap%toList(nsl), Heap%toHeap(nsl), Heap%iv(nsl))
    RETURN
  END SUBROUTINE IntHeapTree_Allocate


  !>
  !!  Build the heap from an array of valurs.
  !!
  !<
  SUBROUTINE IntHeapTree_Build( nsl, iv, Ascending, Heap )
    IMPLICIT NONE
    TYPE(IntHeapTreeType),  INTENT(INOUT)  :: Heap
    INTEGER, INTENT(IN)  :: iv(*)
    INTEGER, INTENT(IN)  :: nsl
    LOGICAL, INTENT(IN)  :: Ascending
    INTEGER  :: is

    Heap%Ascending = Ascending
    CALL IntHeapTree_Allocate( Heap, nsl)
    DO is=1,nsl
       CALL IntHeapTree_AddValue(Heap, iv(is) )
    ENDDO

    RETURN
  END SUBROUTINE IntHeapTree_Build

  !>
  !!   Add a value to the heap.
  !<
  SUBROUTINE IntHeapTree_AddValue( Heap, iv )
    USE  array_allocator
    IMPLICIT NONE
    TYPE(IntHeapTreeType),  INTENT(INOUT) :: Heap
    INTEGER, INTENT(IN)    :: iv
    INTEGER  :: is, iso, ifa

    Heap%numNodes = Heap%numNodes + 1
    IF(Heap%numNodes>SIZE(Heap%toList))THEN
       CALL allc_1Dpointer(Heap%toList,  Heap%numNodes+10000, 'Heap')
       CALL allc_1Dpointer(Heap%toHeap,  Heap%numNodes+10000, 'Heap')
       CALL allc_1Dpointer(Heap%iv,      Heap%numNodes+10000, 'Heap')
    ENDIF

    Heap%iv(Heap%numNodes)     = iv
    Heap%toHeap(Heap%numNodes) = Heap%numNodes
    Heap%toList(Heap%numNodes) = Heap%numNodes
    IF(Heap%numNodes==1) RETURN

    ! *** Updates the heap structure going up the binary tree

    iso = Heap%numNodes                      ! son
    ifa = Heap%numNodes/2                    ! father
    DO WHILE(ifa>0)
       IF( Heap%Ascending )THEN
          IF(Heap%iv(Heap%toList(iso)) >= Heap%iv(Heap%toList(ifa))) EXIT
       ELSE
          IF(Heap%iv(Heap%toList(iso)) <= Heap%iv(Heap%toList(ifa))) EXIT
       ENDIF
       CALL IntHeapTree_SwapIndx( Heap, iso, ifa )
       iso  = ifa
       ifa  = iso/2
    ENDDO

    RETURN
  END SUBROUTINE IntHeapTree_AddValue

  !>
  !!   change a value of a node of the list, and resort the heap.
  !<
  SUBROUTINE IntHeapTree_ResetNodeValue( Heap, isid, iv )
    IMPLICIT NONE
    TYPE(IntHeapTreeType),  INTENT(INOUT) :: Heap
    INTEGER, INTENT(IN) :: isid
    INTEGER, INTENT(IN) :: iv
    INTEGER  :: iso, is2, ifa
    INTEGER  :: as0, as1, as2

    as0 = Heap%iv(isid)
    IF(as0==iv) RETURN
    Heap%iv(isid) = iv

    IF(Heap%Ascending .EQV. as0<iv)THEN

       ! *** Updates the heap structure going down the binary tree

       ifa = Heap%toHeap(isid)
       iso = 2*ifa                       ! left son
       DO WHILE(iso<=Heap%numNodes)
          as1 = Heap%iv(Heap%toList(iso))
          is2 = iso+1                    ! right son
          IF(is2<=Heap%numNodes) THEN
             as2 = Heap%iv(Heap%toList(is2))
             IF(Heap%Ascending .EQV. as2 < as1) THEN         ! checks the key
                iso = is2
                as1 = as2
             ENDIF
          ENDIF

          IF(Heap%Ascending)THEN
             IF(as1 >= iv) RETURN
          ELSE
             IF(as1 <= iv) RETURN
          ENDIF

          CALL IntHeapTree_SwapIndx( Heap, iso, ifa )
          ifa = iso
          iso = 2*ifa
       ENDDO

    ELSE IF(Heap%Ascending .EQV. as0>iv)THEN

       ! *** Updates the heap structure going up the binary tree

       iso = Heap%toHeap(isid)          ! son
       ifa = iso/2                      ! father
       DO WHILE(ifa>0)
          IF(Heap%Ascending)THEN
             IF(iv >= Heap%iv(Heap%toList(ifa))) RETURN
          ELSE
             IF(iv <= Heap%iv(Heap%toList(ifa))) RETURN
          ENDIF
          CALL IntHeapTree_SwapIndx( Heap, iso, ifa )
          iso  = ifa
          ifa  = iso/2
       ENDDO

    ENDIF

    RETURN
  END SUBROUTINE IntHeapTree_ResetNodeValue

  !>
  !!   Remove a node from the list and resort the heap.
  !!   The last node of the list will take the removed position, and 
  !!   length of the list will be reduced by 1.
  !<
  SUBROUTINE IntHeapTree_RemoveNode( Heap, isid )
    IMPLICIT NONE
    TYPE(IntHeapTreeType),  INTENT(INOUT) :: Heap
    INTEGER, INTENT(IN) :: isid
    INTEGER  :: ifa, iLs
    INTEGER  :: as0

    ! *** Replaces the link with "isid" by the last entry in the heap
    Heap%numNodes     = Heap%numNodes -1
    ifa               = Heap%toHeap(isid)
    iLs               = Heap%toList(Heap%numNodes+1)
    Heap%toList(ifa)  = iLs
    Heap%toHeap(iLs)  = ifa

    !--- Resort the heap
    as0         = Heap%iv(iLs)
    Heap%iv(iLs) = Heap%iv(isid)
    CALL IntHeapTree_ResetNodeValue( Heap, iLs, as0 )

    !--- replace the node by the last value in the list
    IF( isid /= Heap%numNodes+1 ) THEN
       Heap%iv(isid)     = Heap%iv(Heap%numNodes+1)
       Heap%toHeap(isid) = Heap%toHeap(Heap%numNodes+1)
       Heap%toList(Heap%toHeap(Heap%numNodes+1)) = isid
    ENDIF

    RETURN
  END SUBROUTINE IntHeapTree_RemoveNode

  !>
  !!   Check the heap tree.
  !<
  SUBROUTINE IntHeapTree_Check( Heap )
    IMPLICIT NONE
    TYPE(IntHeapTreeType),  INTENT(IN) :: Heap
    INTEGER :: N, i, iH, iL, iL2, k
    N = Heap%numNodes
    DO i = 1,N
       iH = Heap%toHeap(i)
       iL = Heap%toList(i)
       IF(iH<=0 .OR. iH>N)    CALL Error_STOP ( '--- IntHeapTree_Check ---1')
       IF(iL<=0 .OR. iL>N)    CALL Error_STOP ( '--- IntHeapTree_Check ---2')
       IF(Heap%toList(iH)/=i) CALL Error_STOP ( '--- IntHeapTree_Check ---3')
       IF(Heap%toHeap(iL)/=i) CALL Error_STOP ( '--- IntHeapTree_Check ---4')
       DO k = 0,1
          IF(2*i+k>N) EXIT
          iL2 = Heap%toList(2*i+k)
          IF(      Heap%Ascending  .AND. Heap%iv(iL) > Heap%iv(iL2))       &
                              CALL Error_STOP ( '--- IntHeapTree_Check ---5')
          IF((.NOT.Heap%Ascending) .AND. Heap%iv(iL) < Heap%iv(iL2))       &
                              CALL Error_STOP ( '--- IntHeapTree_Check ---6')
       ENDDO
    ENDDO
  END SUBROUTINE IntHeapTree_Check


  !>
  !!   Internal function.
  !<
  SUBROUTINE IntHeapTree_SwapIndx( Heap, iso, ifa )
    IMPLICIT NONE
    TYPE(IntHeapTreeType),  INTENT(INOUT)  :: Heap
    INTEGER, INTENT(IN)   :: iso, ifa
    INTEGER  :: ikeep
    Heap%toHeap(Heap%toList(iso)) = ifa
    Heap%toHeap(Heap%toList(ifa)) = iso
    ikeep             = Heap%toList(iso)
    Heap%toList(iso)  = Heap%toList(ifa)
    Heap%toList(ifa)  = ikeep
    RETURN
  END SUBROUTINE IntHeapTree_SwapIndx

  !>
  !!   Delete the heap and release the memory.
  !<
  SUBROUTINE IntHeapTree_Clear(Heap)
    IMPLICIT NONE
    TYPE(IntHeapTreeType),  INTENT(INOUT) :: Heap
    Heap%numNodes = 0
    IF(ASSOCIATED(Heap%toList)) DEALLOCATE(Heap%toList)
    IF(ASSOCIATED(Heap%toHeap)) DEALLOCATE(Heap%toHeap)
    IF(ASSOCIATED(Heap%iv))     DEALLOCATE(Heap%iv)
  END SUBROUTINE IntHeapTree_Clear


END MODULE HeapTree

