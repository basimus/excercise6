!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!
!<
MODULE SurfaceCurvatureCADfix
  USE SurfaceCurvature
  USE SurfaceMeshStorage
  USE SpacingStorage
  IMPLICIT NONE

CONTAINS

  !>
  !!  Input a surface definition by reading a CADfix *.fbm file,
  !!      and convert it to a curvature type.
  !!  @param[in]  JobName        the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  alpha          accuracy coefficient.
  !!  @param[out] Curv           a surface curvature.
  !<
  SUBROUTINE SurfaceCurvature_Input_CADfix(JobName,JobNameLength,alpha, Curv)
    IMPLICIT NONE   
    CHARACTER(LEN=*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    REAL*8,  INTENT(IN) ::  alpha
    TYPE(SurfaceCurvatureType), INTENT(INOUT) :: Curv
    LOGICAL :: TF

    CALL CADFIX_Init
    CALL CADFIX_IsClientMode( TF )
    IF(.NOT. TF)THEN
       CALL CADFIX_LoadGeom( JobName )
    ENDIF
    CALL CADFIX_AnalyseModel
    CALL SurfaceCurvature_from_CADfix(alpha, Curv, 11)

    RETURN
  END SUBROUTINE SurfaceCurvature_Input_CADfix


  !>
  !!  Input a surface definition by reading a CADfix *.fbm file,
  !!      and convert it to a curvature type.
  !!  @param[in]  alpha          accuracy coefficient.
  !!  @param[in]  NReStr         the number of iterations to reduce the stretching
  !!  @param[out] Curv           a surface curvature.
  !<
  SUBROUTINE SurfaceCurvature_from_CADfix(alpha, Curv, NReStr)
    IMPLICIT NONE   
    REAL*8,  INTENT(IN) ::  alpha
    TYPE(SurfaceCurvatureType), INTENT(INOUT) :: Curv
    INTEGER, INTENT(IN) ::  NReStr
    INTEGER :: icv, isf, id, nu, nv, ip, iu, iv, nn, imax, Isucc, nt
    REAL*8  :: dd, du, dv, dmin, dmax, StrMax
    INTEGER, PARAMETER :: maxN = 1001
    INTEGER :: im(maxN), it(maxN)
    REAL*8  :: ui(maxN), vi(maxN), ddd(maxN), uv(2), tt(maxN), ttd(maxN)
    REAL*8  :: p1(3), p2(3), p3(3), p4(3)
    REAL*8  :: UBOT, VBOT, UTOP, VTOP, u, v
    REAL*8  :: R(3), Ru(3),Rv(3),Ruv(3),Ruu(3),Rvv(3), ck
    LOGICAL :: CarryOn
    CHARACTER*256 :: theName

    CALL CADFIX_GetNumCurves( Curv%NB_Curve )
    CALL CADFIX_GetNumSurfaces( Curv%NB_Region ) 
    ALLOCATE(Curv%Curves (Curv%NB_Curve) )
    ALLOCATE(Curv%Regions(Curv%NB_Region))
    WRITE(*,*) 'SurfaceCurvature_Input_CADfix :: Curv% NB_Curve, NB_Region='
    WRITE(*,*) Curv%NB_Curve, Curv%NB_Region

    dmin   = 0.7d0 * alpha
    StrMax = 300.d0

    !--- generate curves

    DO icv = 1, Curv%NB_Curve
       CALL CADFIX_GetCurveName( icv, theName )
       Curv%Curves(icv)%theName  = theName
       Curv%Curves(icv)%ID       = icv
       Curv%Curves(icv)%TopoType = 1

       CALL CADFIX_GetLineTBox( icv, uBOT, uTOP )

       nu = 9
       du = (UTOP - UBOT) / (nu-1)
       DO iu=1,nu
          ui(iu) = (iu-1) * du + uBOT
       ENDDO

       ddd(:) = -1
       DO WHILE(nu<MaxN/2)

          dmax = 0
          DO iu = 1, nu-1
             IF(ddd(iu)<0)THEN
                u = (ui(iu) + ui(iu+1) ) / 2.d0
                CALL CADFIX_GetLinePointDeriv( icv, u, Ru,Ruu)
                CALL DiffGeo_CalcLineCurvature(Ru, Ruu, ck, Isucc)
                CALL CADFIX_GetLineXYZFromT ( icv, ui(iu), p1 )
                CALL CADFIX_GetLineXYZFromT ( icv, ui(iu+1), p2 )
                dd = Geo3D_Distance(p1,p2)
                ddd(iu) = ABS(ck)*dd
             ENDIF
             IF(dmax<ddd(iu)) dmax = ddd(iu)
          ENDDO

          IF(dmax<=dmin) EXIT

          tt(1:nu)  = ui(1:nu)
          ttd(1:nu) = ddd(1:nu)
          imax = 0
          DO iu = 1, nu
             imax = imax+1
             ui(imax) = tt(iu)
             ddd(imax) = ttd(iu)
             IF(iu<nu .AND. ttd(iu)>dmin)THEN
                imax = imax+1
                ui(imax) = (tt(iu)+tt(iu+1))/2.d0
                ddd(imax-1) = -1
                ddd(imax) = -1
             ENDIF
          ENDDO
          nu = imax
       ENDDO

       !--- smooth
       DO ip =1,nu-1
          ddd(ip) = ui(ip+1) - ui(ip)
       ENDDO

       CarryOn = .TRUE.
       DO WHILE(CarryOn)
          CarryOn = .FALSE.
          DO ip =1,nu-2
             IF(ddd(ip)>1.2d0*ddd(ip+1))THEN
                dd        = ddd(ip) + ddd(ip+1)
                ddd(ip)   = 0.545d0 * dd
                ddd(ip+1) = dd - ddd(ip)
                CarryOn = .TRUE.
             ENDIF
          ENDDO
          DO ip =nu-2, 1, -1
             IF(ddd(ip+1)>1.2d0*ddd(ip))THEN
                dd        = ddd(ip) + ddd(ip+1)
                ddd(ip+1) = 0.545d0 * dd
                ddd(ip)   = dd - ddd(ip+1)
                CarryOn = .TRUE.
             ENDIF
          ENDDO
       ENDDO

       DO ip =2,nu-1
          ui(ip) = ui(ip-1) + ddd(ip-1)
       ENDDO

       !--- preject

       Curv%Curves(icv)%numNodes = nu
       ALLOCATE(Curv%Curves(icv)%Posit(3,nu))
       DO ip=1,nu
          u = ui(ip)
          CALL CADFIX_GetLineXYZFromT ( icv, u, R )
          Curv%Curves(icv)%Posit(:,ip) = R(1:3)
       ENDDO
    ENDDO

    !--- generate regions

    DO isf = 1,Curv%NB_Region

       CALL CADFIX_GetSurfaceName( isf, theName )
       Curv%Regions(isf)%theName  = theName
       Curv%Regions(isf)%ID       =  isf
       Curv%Regions(isf)%TopoType = 1
       CALL CADFIX_GetSurfaceUVBox( isf, UBOT, VBOT, UTOP, VTOP )

       nu = 9
       nv = 9
       du = (UTOP - UBOT) / (nu-1)
       dv = (VTOP - VBOT) / (nv-1)   
       DO iu=1,nu
          ui(iu) = (iu-1) * du + uBOT
       ENDDO
       DO iv=1,nv
          vi(iv) = (iv-1) * dv + vBOT
       ENDDO

       !--- set ui

       ddd(:) = -1
       DO WHILE(nu<MaxN/2)

          dmax = 0
          DO iu = 1, nu-1
             IF(ddd(iu)<0)THEN
                DO iv = 1, nv-1
                   u = (ui(iu) + ui(iu+1)) / 2.d0
                   v = (vi(iv) + vi(iv+1)) / 2.d0
                   CALL CADFIX_GetUVPointInfoAll( isf, u, v, R,Ru,Rv,Ruv,Ruu,Rvv )
                   CALL DiffGeo_CalcNormalCurvature(Ru, Rv, Ruv, Ruu, Rvv, Ru, ck, Isucc)
                   IF(Isucc==0) CALL Error_STOP ( '--- sharp point?')
                   uv = (/ui(iu),v/)
                   CALL CADFIX_GetXYZFromUV1( isf, uv, p1 )
                   uv = (/ui(iu+1),v/)
                   CALL CADFIX_GetXYZFromUV1( isf, uv, p2 )
                   dd = ABS(ck) * Geo3D_Distance(p1,p2)
                   ddd(iu) = MAX(ddd(iu), dd)
                ENDDO
             ENDIF
             IF(dmax<ddd(iu)) dmax = ddd(iu)
          ENDDO

          IF(dmax<=dmin) EXIT
          tt(1:nu) = ui(1:nu)
          ttd(1:nu) = ddd(1:nu)
          imax = 0
          DO iu = 1, nu
             imax = imax+1
             ui(imax) = tt(iu)
             ddd(imax) = ttd(iu)
             IF(iu<nu .AND. ttd(iu)>dmin)THEN
                imax = imax+1
                ui(imax) = (tt(iu)+tt(iu+1))/2.d0
                ddd(imax-1) = -1
                ddd(imax) = -1
             ENDIF
          ENDDO
          nu = imax

       ENDDO

       !--- smooth
       DO ip =1,nu-1
          ddd(ip) = ui(ip+1) - ui(ip)
       ENDDO

       CarryOn = .TRUE.
       DO WHILE(CarryOn)
          CarryOn = .FALSE.
          DO ip =1,nu-2
             IF(ddd(ip)>1.2d0*ddd(ip+1))THEN
                dd        = ddd(ip) + ddd(ip+1)
                ddd(ip)   = 0.545d0 * dd
                ddd(ip+1) = dd - ddd(ip)
                CarryOn = .TRUE.
             ENDIF
          ENDDO
          DO ip =nu-2, 1, -1
             IF(ddd(ip+1)>1.2d0*ddd(ip))THEN
                dd        = ddd(ip) + ddd(ip+1)
                ddd(ip+1) = 0.545d0 * dd
                ddd(ip)   = dd - ddd(ip+1)
                CarryOn = .TRUE.
             ENDIF
          ENDDO
       ENDDO

       DO ip =2,nu-1
          ui(ip) = ui(ip-1) + ddd(ip-1)
       ENDDO

       !--- set vi

       ddd(:) = -1
       DO WHILE(nv<MaxN/2)

          dmax = 0
          DO iv = 1, nv-1
             IF(ddd(iv)<0)THEN
                DO iu = 1, nu-1
                   u = (ui(iu) + ui(iu+1)) / 2.d0
                   v = (vi(iv) + vi(iv+1)) / 2.d0
                   CALL CADFIX_GetUVPointInfoAll( isf, u, v, R,Ru,Rv,Ruv,Ruu,Rvv )
                   CALL DiffGeo_CalcNormalCurvature(Ru, Rv, Ruv, Ruu, Rvv, Rv, ck, Isucc)
                   IF(Isucc==0) CALL Error_STOP ( '--- sharp point?')
                   uv = (/u,vi(iv)/)
                   CALL CADFIX_GetXYZFromUV1( isf, uv, p1 )
                   uv = (/u,vi(iv+1)/)
                   CALL CADFIX_GetXYZFromUV1( isf, uv, p2 )
                   dd = ABS(ck) * Geo3D_Distance(p1,p2)
                   ddd(iv) = MAX(ddd(iv), dd)                   
                ENDDO
             ENDIF
             IF(dmax<ddd(iv)) dmax = ddd(iv)
          ENDDO

          IF(dmax<=dmin) EXIT
          tt(1:nv) = vi(1:nv)
          ttd(1:nv) = ddd(1:nv)
          imax = 0
          DO iv = 1, nv
             imax = imax+1
             vi(imax) = tt(iv)
             ddd(imax) = ttd(iv)
             IF(iv<nv .AND. ttd(iv)>dmin)THEN
                imax = imax+1
                vi(imax) = (tt(iv)+tt(iv+1))/2.d0
                ddd(imax-1) = -1
                ddd(imax) = -1
             ENDIF
          ENDDO
          nv = imax

       ENDDO


       !--- smooth
       DO ip =1,nv-1
          ddd(ip) = vi(ip+1) - vi(ip)
       ENDDO

       CarryOn = .TRUE.
       DO WHILE(CarryOn)
          CarryOn = .FALSE.
          DO ip =1,nv-2
             IF(ddd(ip)>1.2d0*ddd(ip+1))THEN
                dd        = ddd(ip) + ddd(ip+1)
                ddd(ip)   = 0.545d0 * dd
                ddd(ip+1) = dd - ddd(ip)
                CarryOn = .TRUE.
             ENDIF
          ENDDO
          DO ip =nv-2, 1, -1
             IF(ddd(ip+1)>1.2d0*ddd(ip))THEN
                dd        = ddd(ip) + ddd(ip+1)
                ddd(ip+1) = 0.545d0 * dd
                ddd(ip)   = dd - ddd(ip+1)
                CarryOn = .TRUE.
             ENDIF
          ENDDO
       ENDDO

       DO ip =2,nv-1
          vi(ip) = vi(ip-1) + ddd(ip-1)
       ENDDO


       !--- reset ui to avoid big stretching
       nt = 0
       DO WHILE(nu<MaxN/2 .AND. nt<NReStr)

          nt = nt + 1
          dmax = 0
          DO iu = 1, nu-1
             IF(nt==1)THEN
                ddd(iu) = 0
                im(iu)  = 0
                DO iv = 1, nv-1
                   u = (ui(iu) + ui(iu+1)) / 2.d0
                   v = (vi(iv) + vi(iv+1)) / 2.d0
                   uv = (/ui(iu),v/)
                   CALL CADFIX_GetXYZFromUV1( isf, uv, p1 )
                   uv = (/ui(iu+1),v/)
                   CALL CADFIX_GetXYZFromUV1( isf, uv, p2 )
                   uv = (/u,vi(iv)/)
                   CALL CADFIX_GetXYZFromUV1( isf, uv, p3 )
                   uv = (/u,vi(iv+1)/)
                   CALL CADFIX_GetXYZFromUV1( isf, uv, p4 )
                   dd = Geo3D_Distance_SQ(p1,p2) / Geo3D_Distance_SQ(p3,p4)
                   IF(dd>ddd(iu) .AND. dd>StrMax)THEN
                      ddd(iu) = dd
                      im(iu)  = iv
                   ENDIF
                ENDDO
             ELSE IF(im(iu)>0)THEN
                iv = im(iu)
                u = (ui(iu) + ui(iu+1)) / 2.d0
                v = (vi(iv) + vi(iv+1)) / 2.d0
                uv = (/ui(iu),v/)
                CALL CADFIX_GetXYZFromUV1( isf, uv, p1 )
                uv = (/ui(iu+1),v/)
                CALL CADFIX_GetXYZFromUV1( isf, uv, p2 )
                uv = (/u,vi(iv)/)
                CALL CADFIX_GetXYZFromUV1( isf, uv, p3 )
                uv = (/u,vi(iv+1)/)
                CALL CADFIX_GetXYZFromUV1( isf, uv, p4 )
                dd = Geo3D_Distance_SQ(p1,p2) / Geo3D_Distance_SQ(p3,p4)
                IF(dd>StrMax)THEN
                   ddd(iu) = dd
                ELSE
                   im(iu) = 0
                ENDIF
             ENDIF
             IF(im(iu)>0 .AND. dmax<ddd(iu)) dmax = ddd(iu)
          ENDDO

          IF(dmax<=StrMax) EXIT
          tt(1:nu) = ui(1:nu)
          it(1:nu) = im(1:nu)
          imax = 0
          DO iu = 1, nu
             imax = imax+1
             ui(imax) = tt(iu)
             im(imax) = it(iu)
             IF(iu<nu .AND. it(iu)>0)THEN
                imax = imax+1
                ui(imax) = (tt(iu)+tt(iu+1))/2.d0
                im(imax) = it(iu)
             ENDIF
          ENDDO
          nu = imax

       ENDDO

       IF(nt>1)THEN
          !--- smooth
          DO ip =1,nu-1
             ddd(ip) = ui(ip+1) - ui(ip)
          ENDDO

          CarryOn = .TRUE.
          DO WHILE(CarryOn)
             CarryOn = .FALSE.
             DO ip =1,nu-2
                IF(ddd(ip)>1.2d0*ddd(ip+1))THEN
                   dd        = ddd(ip) + ddd(ip+1)
                   ddd(ip)   = 0.545d0 * dd
                   ddd(ip+1) = dd - ddd(ip)
                   CarryOn = .TRUE.
                ENDIF
             ENDDO
             DO ip =nu-2, 1, -1
                IF(ddd(ip+1)>1.2d0*ddd(ip))THEN
                   dd        = ddd(ip) + ddd(ip+1)
                   ddd(ip+1) = 0.545d0 * dd
                   ddd(ip)   = dd - ddd(ip+1)
                   CarryOn = .TRUE.
                ENDIF
             ENDDO
          ENDDO

          DO ip =2,nu-1
             ui(ip) = ui(ip-1) + ddd(ip-1)
          ENDDO
       ENDIF

       !--- reset vi to avoid big stretching

       nt = 0
       DO WHILE(nv<MaxN/2 .AND. nt<NReStr)

          nt = nt + 1
          dmax = 0
          DO iv = 1, nv-1
             IF(nt==1)THEN
                ddd(iv) = 0
                im(iv)  = 0
                DO iu = 1, nu-1
                   u = (ui(iu) + ui(iu+1)) / 2.d0
                   v = (vi(iv) + vi(iv+1)) / 2.d0
                   uv = (/ui(iu),v/)
                   CALL CADFIX_GetXYZFromUV1( isf, uv, p1 )
                   uv = (/ui(iu+1),v/)
                   CALL CADFIX_GetXYZFromUV1( isf, uv, p2 )
                   uv = (/u,vi(iv)/)
                   CALL CADFIX_GetXYZFromUV1( isf, uv, p3 )
                   uv = (/u,vi(iv+1)/)
                   CALL CADFIX_GetXYZFromUV1( isf, uv, p4 )
                   dd = Geo3D_Distance_SQ(p3,p4) / Geo3D_Distance_SQ(p1,p2)
                   IF(dd>ddd(iv) .AND. dd>StrMax)THEN
                      ddd(iv) = dd
                      im(iv)  = iu
                   ENDIF
                ENDDO
             ELSE IF(im(iv)>0)THEN
                iu = im(iv)
                u = (ui(iu) + ui(iu+1)) / 2.d0
                v = (vi(iv) + vi(iv+1)) / 2.d0
                uv = (/ui(iu),v/)
                CALL CADFIX_GetXYZFromUV1( isf, uv, p1 )
                uv = (/ui(iu+1),v/)
                CALL CADFIX_GetXYZFromUV1( isf, uv, p2 )
                uv = (/u,vi(iv)/)
                CALL CADFIX_GetXYZFromUV1( isf, uv, p3 )
                uv = (/u,vi(iv+1)/)
                CALL CADFIX_GetXYZFromUV1( isf, uv, p4 )
                dd = Geo3D_Distance_SQ(p3,p4) / Geo3D_Distance_SQ(p1,p2)
                IF(dd>StrMax)THEN
                   ddd(iv) = dd
                ELSE
                   im(iv) = 0
                ENDIF
             ENDIF
             IF(im(iv)>0 .AND. dmax<ddd(iv)) dmax = ddd(iv)
          ENDDO

          IF(dmax<=StrMax) EXIT
          tt(1:nv) = vi(1:nv)
          it(1:nv) = im(1:nv)
          imax = 0
          DO iv = 1, nv
             imax = imax+1
             vi(imax) = tt(iv)
             im(imax) = it(iv)
             IF(iv<nv .AND. it(iv)>0)THEN
                imax = imax+1
                vi(imax) = (tt(iv)+tt(iv+1))/2.d0
                im(imax) = it(iv)
             ENDIF
          ENDDO
          nv = imax

       ENDDO


       IF(nt>1)THEN
          !--- smooth
          DO ip =1,nv-1
             ddd(ip) = vi(ip+1) - vi(ip)
          ENDDO

          CarryOn = .TRUE.
          DO WHILE(CarryOn)
             CarryOn = .FALSE.
             DO ip =1,nv-2
                IF(ddd(ip)>1.2d0*ddd(ip+1))THEN
                   dd        = ddd(ip) + ddd(ip+1)
                   ddd(ip)   = 0.545d0 * dd
                   ddd(ip+1) = dd - ddd(ip)
                   CarryOn = .TRUE.
                ENDIF
             ENDDO
             DO ip =nv-2, 1, -1
                IF(ddd(ip+1)>1.2d0*ddd(ip))THEN
                   dd        = ddd(ip) + ddd(ip+1)
                   ddd(ip+1) = 0.545d0 * dd
                   ddd(ip)   = dd - ddd(ip+1)
                   CarryOn = .TRUE.
                ENDIF
             ENDDO
          ENDDO

          DO ip =2,nv-1
             vi(ip) = vi(ip-1) + ddd(ip-1)
          ENDDO
       ENDIF

       !--- project

       Curv%Regions(isf)%numNodeU = nu
       Curv%Regions(isf)%numNodeV = nv
       ALLOCATE(Curv%Regions(isf)%Posit(3,nu,nv))
       DO iv=1,nv
          DO iu=1,nu
             uv = (/ ui(iu), vi(iv) /)
             CALL CADFIX_GetXYZFromUV1( isf, uv, R(1:3) )
             Curv%Regions(isf)%Posit(:,iu,iv) = R(1:3)
          ENDDO
       ENDDO


       Curv%Regions(isf)%GeoType = 1
       CALL CADFIX_GetSurfaceNumCurves(isf, nn)

       Curv%Regions(isf)%numCurve = nn
       ALLOCATE(Curv%Regions(isf)%IC(nn))
       CALL CADFIX_GetSurfaceCurves(isf, nn, Curv%Regions(isf)%IC)
    ENDDO

    RETURN

  END SUBROUTINE SurfaceCurvature_from_CADfix
  
  SUBROUTINE SurfaceMeshStorage_from_CADfix(Surf)
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
      POINTER  (pPosit, Posit(3,1))
      POINTER  (pIP_Tri, IP_Tri(4,1))
      REAL*8  :: Posit
      INTEGER :: IP_Tri
      INTEGER :: NB_Tri, NB_Point, NB_Surf
      INTEGER :: i
      
      
    CALL CADFIX_GetMeshNodes( pPosit, NB_Point )
    CALL CADFIX_GetMeshElements( pIP_Tri, NB_Tri )

    Surf%NB_Tri   = NB_Tri
    Surf%NB_Point = NB_Point
    Surf%NB_Seg   = 0
    Surf%NB_Surf  = 1
    Surf%NB_Sheet = 0
    ALLOCATE(Surf%IP_Tri(5, Surf%NB_Tri))
    ALLOCATE(Surf%Posit(3, Surf%NB_Point))
    DO i = 1,Surf%NB_Point
       Surf%Posit(1:3,i) = Posit(:,i)
    ENDDO
    DO i =1,Surf%NB_Tri
       Surf%IP_Tri(1:3,i) = IP_Tri(1:3,i)
       Surf%IP_Tri(4,  i) = 0
       Surf%IP_Tri(5,  i) = ABS(IP_Tri(4,i))
       IF(IP_Tri(4,i)<0)THEN
          Surf%IP_Tri(2,i) = IP_Tri(3,i)
          Surf%IP_Tri(3,i) = IP_Tri(2,i)
       ENDIF
    ENDDO
      
    RETURN
  END SUBROUTINE SurfaceMeshStorage_from_CADfix
  
  
  SUBROUTINE SpacingStorage_from_CADfix(BGSpacing, isotr, Isucc)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(INOUT) :: BGSpacing
    LOGICAL, INTENT(in)  :: isotr
    INTEGER, INTENT(out) :: Isucc
    REAL*8  :: p(3), p1(3), p2(3),p3(3), pm(3), stemp(7), pMin(3), pMax(3)
    REAL*8  :: r11,r12, r21,r22, r31,r32, scale1, scale2, scale3
    REAL*8  :: str1, str2, str3
    INTEGER :: NB_Point, NB_Tet, nb_ptsc, nb_lnsc, nb_trsc, nb_box
    INTEGER :: i
      REAL*8 ::  BasicSize
      POINTER( pPoints, points(6,1) ), ( pLines, lines(12,1) )
      POINTER( pTriangles, triangles(18,1) )
      REAL*8 :: points, lines, triangles

    CALL CADFIX_GetSources( nb_ptsc, pPoints,     &
                            nb_lnsc, pLines,      &
                            nb_trsc, pTriangles,  BasicSize, Isucc )
    IF(Isucc==0) RETURN
    
    NB_Point = 0
    NB_Tet   = 0
    nb_box   = 0


    BGSpacing%BasicSize = BasicSize
    BGSpacing%MinSize   = BasicSize

    IF(isotr)THEN
          IF(nb_ptsc==0 .AND. nb_lnsc==0 .AND. nb_trsc==0)THEN
             BGSpacing%Model = 1
          ELSE
             BGSpacing%Model = 4
          ENDIF
    ELSE
          IF(nb_ptsc==0 .AND. nb_lnsc==0 .AND. nb_trsc==0)THEN
             BGSpacing%Model = -1
          ELSE
             BGSpacing%Model = -4
          ENDIF
    ENDIF

    CALL SourceGroup_Allocate(BGSpacing%Sources, nb_ptsc, nb_lnsc, nb_trsc, .FALSE.)
    !--  read point Sources
    DO i=1,nb_ptsc
       p(1:3) = points(1:3,i)
       scale1 = points(4,  i)
       r11    = points(5,  i)
       r12    = points(6,  i)
       BGSpacing%MinSize = MIN(BGSpacing%MinSize, scale1)
       pm = p - r12
       WHERE(BGSpacing%pMin(1:3)>pm(1:3)) BGSpacing%pMin(1:3) = pm(1:3)
       pm = p + r12
       WHERE(BGSpacing%pMax(1:3)<pm(1:3)) BGSpacing%pMax(1:3) = pm(1:3)
       scale1 = scale1/BGSpacing%BasicSize
       CALL SourceGroup_AddPointSource(BGSpacing%Sources,p,scale1,r11,r12)
    ENDDO

    !--  read line Sources
    DO i=1,nb_lnsc
       p1(1:3) = lines(1:3,i)
       scale1  = lines(4,  i)
       r11     = lines(5,  i)
       r12     = lines(6,  i)
       str1    = 1.d0
       p2(1:3) = lines(7:9,i)
       scale2  = lines(10, i)
       r21     = lines(11, i)
       r22     = lines(12, i)
       str2    = 1.d0
       BGSpacing%MinSize = MIN(BGSpacing%MinSize, ABS(scale1), ABS(scale2))
       pm = p1 - r12
       WHERE(BGSpacing%pMin(1:3)>pm(1:3)) BGSpacing%pMin(1:3) = pm(1:3)
       pm = p1 + r12
       WHERE(BGSpacing%pMax(1:3)<pm(1:3)) BGSpacing%pMax(1:3) = pm(1:3)
       pm = p2 - r22
       WHERE(BGSpacing%pMin(1:3)>pm(1:3)) BGSpacing%pMin(1:3) = pm(1:3)
       pm = p2 + r22
       WHERE(BGSpacing%pMax(1:3)<pm(1:3)) BGSpacing%pMax(1:3) = pm(1:3)
       scale1 = scale1/BGSpacing%BasicSize
       scale2 = scale2/BGSpacing%BasicSize

       CALL SourceGroup_AddLineSource(BGSpacing%Sources,p1,scale1,r11,r12,p2,scale2,r21,r22)
    ENDDO

    !--  read triangle Sources
    DO i=1,nb_trsc
       p1(1:3) = triangles(1:3,i)
       scale1  = triangles(4,  i)
       r11     = triangles(5,  i)
       r12     = triangles(6,  i)
       str1    = 1.d0
       p2(1:3) = triangles(7:9,i)
       scale2  = triangles(10, i)
       r21     = triangles(11, i)
       r22     = triangles(12, i)
       str2    = 1.d0
       p3(1:3) = triangles(13:15,i)
       scale3  = triangles(16,   i)
       r31     = triangles(17,   i)
       r32     = triangles(18,   i)
       str3    = 1.d0
       BGSpacing%MinSize = MIN(BGSpacing%MinSize, ABS(scale1), ABS(scale2), ABS(scale3))
       pm = p1 - r12
       WHERE(BGSpacing%pMin(1:3)>pm(1:3)) BGSpacing%pMin(1:3) = pm(1:3)
       pm = p1 + r12
       WHERE(BGSpacing%pMax(1:3)<pm(1:3)) BGSpacing%pMax(1:3) = pm(1:3)
       pm = p2 - r22
       WHERE(BGSpacing%pMin(1:3)>pm(1:3)) BGSpacing%pMin(1:3) = pm(1:3)
       pm = p2 + r22
       WHERE(BGSpacing%pMax(1:3)<pm(1:3)) BGSpacing%pMax(1:3) = pm(1:3)
       pm = p3 - r32
       WHERE(BGSpacing%pMin(1:3)>pm(1:3)) BGSpacing%pMin(1:3) = pm(1:3)
       pm = p3 + r32
       WHERE(BGSpacing%pMax(1:3)<pm(1:3)) BGSpacing%pMax(1:3) = pm(1:3)
          scale1 = scale1 / BGSpacing%BasicSize
          scale2 = scale2 / BGSpacing%BasicSize
          scale3 = scale3 / BGSpacing%BasicSize
             CALL SourceGroup_AddTriSource(BGSpacing%Sources,p1,scale1,r11,r12,  &
                  p2,scale2,r21,r22, p3,scale3,r31,r32)
    ENDDO

  END SUBROUTINE SpacingStorage_from_CADfix
  

END MODULE SurfaceCurvatureCADfix
