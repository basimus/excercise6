!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


module occt_fortran
  USE Geometry3D
      !use, intrinsic :: ISO_C_Binding, only: C_int, C_ptr, C_NULL_ptr
      implicit none

      !interface
      !      function C_OCCT_new() bind(c,name="cocct_flite__new")
      !            import
      !            type(C_ptr) :: C_OCCT_new
      !      end function C_OCCT_new
      !end interface

      contains      
      

      !subroutine OCCT_new(this)
      !      type(C_ptr), intent(inout) :: this
      !      call COCCT_FLITE_new(this)
      !      !this%object = C_OCCT_new()
      !end subroutine OCCT_new

      !subroutine OCCT_delete(this)
      !      type(C_ptr), intent(inout) :: this
      !      call COCCT_FLITE_delete(this)
      !      this = C_NULL_ptr
      !end subroutine OCCT_delete

      subroutine OCCT_LoadSTEP(filename,prec)
            CHARACTER*(*) ::  filename
            CHARACTER(50) :: cfile
    real*8        :: prec
    
            cfile=filename(1:LEN_TRIM(filename))//CHAR(0)
            !write(*,*) cfile
            call COCCT_LoadSTEP(cfile,prec)
    !WRITE(*,*) 'precision of step = ',prec
      end subroutine

      subroutine OCCT_LoadIGES(filename,prec)
            CHARACTER*(*) :: filename
            CHARACTER(50) :: cfile
    real*8        :: prec
            cfile = filename(1:LEN_TRIM(filename))//CHAR(0)
            call COCCT_LoadIGES(cfile,prec)
    ! WRITE(*,*) 'precision of iges = ',prec
    end subroutine
    
    subroutine OCCT_LoadBRep(filename)
            CHARACTER*(*) :: filename
            CHARACTER(50) :: cfile
            cfile = filename(1:LEN_TRIM(filename))//CHAR(0)
            call COCCT_LoadBRep(cfile)
      end subroutine

      subroutine OCCT_GetNumCurves(n)
            integer :: n
            
            call COCCT_GetNumCurves(n)
            WRITE(101,*) 'NumCurves',n
      end subroutine


      subroutine OCCT_GetNumSurfaces(n)
            integer :: n
            call cocct_getnumsurfaces(n)
            WRITE(101,*) 'NumSurfaces',n

      end subroutine

      subroutine OCCT_GetLineTBox(curveNum,tMin,tMax)
            integer :: curveNum
            real*8  :: tMin,tMax
            call cocct_getlinetbox(curveNum,tMin,tMax)
      end subroutine

      subroutine OCCT_GetLineXYZFromT(curveNum,u,XYZ)
            integer :: curveNum
            real*8 :: u,XYZ(3)
            call cocct_getlinexyzfromt(curveNum,u,XYZ)
      end subroutine

      subroutine OCCT_GetLinePointDeriv(curveNum,u,Ru,Ruu)
            integer :: curveNum
            real*8 :: u,Ru(3),Ruu(3)
            call cocct_getlinepointderiv(curveNum,u,Ru,Ruu)
      end subroutine


      subroutine OCCT_GetSurfaceUVBox(surfNum,uMin,vMin,uMax,vMax)
            integer :: surfNum
            real*8  :: uMin, vMin, uMax, vMax
    
            call cocct_surfaceuvbox(surfNum,uMin,vMin,uMax,vMax)
      end subroutine


      subroutine OCCT_GetUVPointInfoAll(surfNum,u,v,R,Ru,Rv,Ruv,Ruu,Rvv)
            integer :: surfNum
            real*8 :: u,v,R(3),Ru(3),Rv(3),Ruv(3),Ruu(3),Rvv(3)

            call cocct_getuvpointinfoall(surfNum,u,v,R,Ru,Rv,Ruv,Ruu,Rvv)
      end subroutine

      subroutine OCCT_GetXYZFromUV1(surfNum,uv,xyz)
            integer :: surfNum
            real*8  :: uv(2), xyz(3), u, v
    !For some reason this doesn't work
            call cocct_getxyzfromuv1(surfNum,uv(1),uv(2),xyz)
      end subroutine

      subroutine OCCT_GetSurfaceNumCurves(surfNum,nc)
            integer :: surfNum, nc
            call cocct_getsurfacenumcurves(surfNum,nc)
      end subroutine
      
      subroutine OCCT_GetSurfaceName(surfNum,entityName)
            integer :: surfNum
            CHARACTER*255 :: entityName
            INTEGER i,j
            call cocct_setsurfname(surfNum, entityName)
             DO i = 1, 255
        IF( entityName(i:i).EQ.' ' ) GOTO 100
      ENDDO
 100  CONTINUE
      DO j = i, 255
        entityName(j:j) = ' '
      ENDDO
      RETURN
            
      end subroutine
      
      
      subroutine OCCT_GetCurveName(surfNum,entityName)
            integer :: surfNum
            CHARACTER*255 :: entityName
            INTEGER i,j
            call cocct_setcurvname(surfNum, entityName)
             DO i = 1, 255
        IF( entityName(i:i).EQ.' ' ) GOTO 100
      ENDDO
 100  CONTINUE
      DO j = i, 255
        entityName(j:j) = ' '
      ENDDO
      RETURN
            
      end subroutine
      

      subroutine OCCT_GetSurfaceCurves(surfNum, nc, List)
            integer :: surfNum,nc
            integer :: List(*)
            call cocct_getsurfacecurves(surfNum,nc,List)
      end subroutine
      
      subroutine OCCT_GetUVFromXYZ(surfNum,numPoints,xyz,uv,tol)
            integer :: surfNum,numPoints,i, useUV
            real*8 :: xyz(3,numPoints),uv(2,numPoints),u,v,tol

            !write(2000,*) surfNum
            useUV = 0
            DO i=1,numPoints
                  call cocct_getuvfromxyz(surfNum,tol,useUV,u,v,xyz(:,i))
                  useUV = 1
                  uv(1,i) = u
                  uv(2,i) = v
                  !write(2000,'(I8,4E15.6)') i,u,v,uv(1,i),uv(2,i)
            END DO
      end subroutine

  subroutine OCCT_GetUFromXYZ(curveNum,xyz,u)
    integer :: curveNum,i
    real*8 :: xyz(3),uv,u,tol

    tol = 1e-8
    call cocct_getufromxyz(curveNum,tol,u,xyz)
     
  end subroutine


      subroutine OCCT_WriteSurfVis(surfNum)
            integer :: surfNum
            
            call cocct_getsurfvis(surfNum)
      end subroutine

       subroutine OCCT_WriteCurvVis(surfNum)
                integer :: surfNum

                call cocct_writecurvvis(surfNum)
        end subroutine


      subroutine OCCT_CalculateCurveFacets( curveID, numVertices,pVertices, maxEdgeLength )
      INTEGER  curveNum, numVertices, IERROR, found
      INTEGER  curveID, numVerticesReturned, i, j
      REAL*8   edgeTolerance, edgeTolerance2
      REAL*8   maxEdgeLength, maxEdgeLength2
      POINTER  (pVertices, vertices(7,1))
      POINTER  (pX, x(1)), (pY, y(1)), (pZ, z(1) )
      REAL*8   vertices, x, y, z, xyzOnLine(3)
      REAL*8   separation, tParam
      REAL*8   dxt, dyt, dzt, dxtt, dytt, dztt
      INTEGER  numLevels, PASS
      REAL*8   cadfixTol
      REAL*8  occtVertices(7,500000)  !Was 7,100000


      !WRITE(*,*) 'Starting OCCT_CalculateCurveFacets ',curveID

      call cocct_calculatecurvefacets(curveID,numVertices,maxEdgeLength, &
                                      occtVertices(1,:),occtVertices(2,:), &
                                      occtVertices(3,:),occtVertices(4,:), &
                                      occtVertices(5,:),occtVertices(6,:), &
                                      occtVertices(7,:))
       

   ! WRITE(*,*) 'Allocating arrays ',curveID

   CALL allc( pVertices, 2* 7 * numVertices,'vertices in CADFIX_CalculateCurveFacets' )
       
      WRITE(555,*) 'CurveID', curveID,'numVert',numVertices
      DO i = 1, numVertices
        vertices(1,i) = occtVertices(1,i)!x(i)
!       WRITE(5556,*)'Vertex = ',i
        vertices(2,i) = occtVertices(2,i)!y(i)
        vertices(3,i) = occtVertices(3,i)!z(i)
!       WRITE(5556,*) occtVertices(1:3,i)

      vertices(4,i) = occtVertices(4,i)
        vertices(5,i) = occtVertices(5,i)
        vertices(6,i) = occtVertices(6,i)
        vertices(7,i) = occtVertices(7,i)
      ENDDO
            
      
!          call cocct_calculatecurvefacets(curveID,numVertices,maxEdgeLength,vertices(1,:),vertices(2,:),vertices(3,:),vertices(4,:),vertices(5,:),vertices(6,:),vertices(7,:))



      end subroutine




         SUBROUTINE oldOCCT_CalculateCurveFacets( curveNum, numVertices,pVertices, maxEdgeLength )
      IMPLICIT NONE
!      !INCLUDE 'cfi.prm'
      INTEGER  CADFIX_GetCurveID, magic
      INTEGER  curveNum, numVertices, IERROR, found
      INTEGER  curveID, numVerticesReturned, i, j
      REAL*8   edgeTolerance, edgeTolerance2
      REAL*8   maxEdgeLength, maxEdgeLength2
      POINTER  (pVertices, vertices(7,1))
      POINTER  (pX, x(1)), (pY, y(1)), (pZ, z(1) )
      REAL*8   vertices, x, y, z, xyzOnLine(3)
      REAL*8   separation, tParam
      REAL*8   dxt, dyt, dzt, dxtt, dytt, dztt
      INTEGER  numLevels, PASS
      REAL*8   cadfixTol
      REAL*8 occtVertices(7,10000)

      !IERROR = CFI_ERR_OK
      !CALL CFI_GET_MODEL_GTOL( cadfixTol, IERROR )
      !CALL CADFIX_Check_Status(IERROR, '_CalculateCurveFacets.1' )
      WRITE(5556,*)'CADFIX_CalculateCurveFacets',curveNum,cadfixTol
      edgeTolerance = 1e-4
      !curveID = CADFIX_GetCurveID( curveNum )
      curveID = curveNum
      WRITE(5556,*)'  Curve ID',curveID
      !IERROR = CFI_ERR_OK
      edgeTolerance2 = edgeTolerance
      maxEdgeLength2 = maxEdgeLength
      edgeTolerance2 = 0.1 * maxEdgeLength2
      numLevels = 0
  100  CONTINUE
    !  CALL CFI_CALC_LINE_FACET_TOTAL( curveID, edgeTolerance2,
    ! &                                maxEdgeLength2,
    ! &                                numVertices, IERROR )
     call cocct_calculatecurvefacets(curveID,numVertices,maxEdgeLength2, &
                                     occtVertices(1,:),occtVertices(2,:), &
                                     occtVertices(3,:),occtVertices(4,:), &
                                     occtVertices(5,:),occtVertices(6,:), &
                                     occtVertices(7,:))
      !CALL CADFIX_Check_Status(IERROR, '_CalculateCurveFacets.2' )
      WRITE(5556,*)'At level ',numLevels
      WRITE(5556,*)'Max Edge Length = ',maxEdgeLength2
      WRITE(5556,*)'Max Tolerance = ',edgeTolerance2
      WRITE(5556,*)'NumVertices to generate = ', numVertices
      
      IF( numVertices.GE.10000 ) THEN
        maxEdgeLength2 = maxEdgeLength2 * 2.0
        edgeTolerance2 = edgeTolerance2 * 2.0
        numLevels = numLevels + 1
        IF( numLevels.GT.40 )  STOP 'Too many levels'      
        GOTO 100
      ENDIF
      
      IF( numVertices.LE.5 .AND. numLevels.GT.-20) THEN
        maxEdgeLength2 = maxEdgeLength2 / 2.0
        edgeTolerance2 = edgeTolerance2 / 2.0
        numLevels = numLevels -1
        GOTO 100
      ENDIF
      
      IF(numLevels<0) numLevels = 0
      IF(numVertices.LE.2)THEN
         numLevels = 5
      ELSE IF(numVertices.LE.3)THEN
         numLevels = 4
      ELSE IF(numVertices.LE.5)THEN
         numLevels = 3
      ELSE IF(numVertices.LE.9)THEN
         numLevels = 2
      ELSE IF(numVertices.LE.17)THEN
         numLevels = 1
      ENDIF 
      
      CALL allc( pVertices, 2 * 7 * numVertices,'vertices in CADFIX_CalculateCurveFacets' )
      CALL allc( pX, 2 * numVertices,'x in CADFIX_CalculateCurveFacets' )
      CALL allc( pY, 2 * numVertices,'y in CADFIX_CalculateCurveFacets' )
      CALL allc( pZ, 2 * numVertices,'z in CADFIX_CalculateCurveFacets' )
      !IERROR = CFI_ERR_OK
      !magic = 0
      !CALL CFI_GET_LINE_FACET_VERT_LIST( curveID, numVertices,
     !&                                   magic, numVerticesReturned,
     !&                                   x, y, z, IERROR )
      !CALL CADFIX_Check_Status(IERROR, '_CalculateCurveFacets.3' )
      !CALL Check_Equal( numVertices, numVerticesReturned )
      DO i = 1, numVertices
        vertices(1,i) = occtVertices(1,i)!x(i)
!      WRITE(5556,*)'Vertex = ',i
        vertices(2,i) = occtVertices(2,i)!y(i)
        vertices(3,i) = occtVertices(3,i)!z(i)
      !WRITE(5556,*) occtVertices(1:3,i)
      ENDDO
      CALL deallc( pX, 'x in CADFIX_CalculateCurveFacets' )
      CALL deallc( pY, 'y in CADFIX_CalculateCurveFacets' )
      CALL deallc( pZ, 'z in CADFIX_CalculateCurveFacets' )

      DO i = 1, numVertices
     !   CALL CFI_CALC_LINE_T_FROM_XYZ( curveID, vertices(1,i),
     !&                                 edgeTolerance * 10,
     !&                                 found, tParam, xyzOnLine, 
     !&                                 separation, IERROR )
        
     !   CALL CADFIX_Check_Status(IERROR, '_CalculateCurveFacets.4' )
!c        CALL Check_Equal( found, CFI_FALSE )
 !       CALL CFI_CALC_LINE_DERIV_AT_T( curveID, tParam,
    ! &                                 dxt, dyt, dzt, dxtt, dytt, dztt,
    ! &                                 IERROR );
    !    CALL CADFIX_Check_Status(IERROR, '_CalculateCurveFacets.5' )
        vertices(4,i) = occtVertices(4,i)
        vertices(5,i) = occtVertices(5,i)
        vertices(6,i) = occtVertices(6,i)
        vertices(7,i) = occtVertices(7,i)
      ENDDO

      DO i=1,numVertices
         WRITE(5557,*) occtVertices(1:3,i)
      END DO


      IF(numLevels.GT.0)THEN
        j = (numVertices-1) * (2**numLevels)  + 1
        CALL reallc( pVertices,  2 * 7 * j,  'pVertices' )      
      ENDIF
      
      DO PASS = 1, numLevels
        DO i = numVertices, 2, -1
          DO j = 1, 7
            Vertices(j,i*2-1) = vertices(j,i)
          ENDDO
        ENDDO
        numVertices = numVertices * 2 - 1
        DO i = 2, numVertices - 1, 2
          DO j = 1, 7
            Vertices(j,i) = (Vertices(j,i-1) + Vertices(j,i+1)) / 2.0
          ENDDO
        ENDDO        
      END DO

      !DO i=1,numVertices
!       WRITE(5556,*) occtVertices(1:7,i)
 !     END DO 

      WRITE(5556,*)'Final Level=',numLevels,' NumVertices=',numVertices

      RETURN
      END SUBROUTINE









end module occt_fortran
