!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!******************************************************************************!
!
!  Order of nodes in each grid  :
!
!                         8________7
!                         /|      /|
!                      5 /_______6 |
!                        | |     | |
!                        | |     | |
!                        | |4____|_|3
!                        |/      | /
!                        |_______|/ 
!                       1         2
!
!   Attention: above connectivity conform with those of 
!              Hexahedral cell in module CellConnectivity
!
!
!******************************************************************************!
!>
!!  Bi-tree mesh storage, including the defination of types and basic functions.
!!
!!  A cell of octree meshes is a hexahedron whose connectivity 
!!    is defined in module CellConnectivity .
!!
!!  A cell has 2 children if it has been splitted.
!!  A leaf cell is a cell that has no child (not be splitted).
!<
MODULE BitreeMesh

  USE CellConnectivity
  USE PtADTreeModule
  IMPLICIT NONE

  INTEGER, PARAMETER :: Max_Bit_Level = 20
  INTEGER, PARAMETER :: nallc_Bit_increase = 100000
  INTEGER, PARAMETER :: iCorner_Oct(3,8) =    &
       reshape( (/-1,-1,-1, 1,-1,-1, 1, 1,-1, -1, 1,-1,   &
                  -1,-1, 1, 1,-1, 1, 1, 1, 1, -1, 1, 1/), &
                (/3,8/) )


  TYPE :: BitreeMesh_Cell_Type
     INTEGER :: ID = 0
     INTEGER :: Level(3) = 0
     !>  Eight nodes of the hexahedral cell, ref module CellConnectivity.
     INTEGER :: Nodes(8)
     !>  =0, a leaf cell;
     !!  =1, bisected along x-direction;
     !!  =2, bisected along y-direction;
     !!  =3, bisected along z-direction;
     INTEGER :: BiDir = 0
     !--- sub cell pointers
     TYPE (BitreeMesh_Cell_Type), POINTER :: Son1 => null()        !<  Left  Front Bottom,  with Nodes(1)
     TYPE (BitreeMesh_Cell_Type), POINTER :: Son7 => null()        !<  Right Back  Top,     with Nodes(7)
     !--- neighbour leaf cell pointers
     TYPE (BitreeMesh_Cell_Type), POINTER :: Left1 => null()        !<  Left  with Nodes(1),   X-
     TYPE (BitreeMesh_Cell_Type), POINTER :: Left4 => null()        !<  Left  with Nodes(4),   X-
     TYPE (BitreeMesh_Cell_Type), POINTER :: Left8 => null()        !<  Left  with Nodes(8),   X-
     TYPE (BitreeMesh_Cell_Type), POINTER :: Left5 => null()        !<  Left  with Nodes(5),   X-
     TYPE (BitreeMesh_Cell_Type), POINTER :: Right2 => null()       !<  Right with Nodes(2),   X+
     TYPE (BitreeMesh_Cell_Type), POINTER :: Right3 => null()       !<  Right with Nodes(3),   X+
     TYPE (BitreeMesh_Cell_Type), POINTER :: Right7 => null()       !<  Right with Nodes(7),   X+
     TYPE (BitreeMesh_Cell_Type), POINTER :: Right6 => null()       !<  Right with Nodes(6),   X+
     TYPE (BitreeMesh_Cell_Type), POINTER :: Front1 => null()       !<  Front with Nodes(1),   Y-
     TYPE (BitreeMesh_Cell_Type), POINTER :: Front5 => null()       !<  Front with Nodes(5),   Y-
     TYPE (BitreeMesh_Cell_Type), POINTER :: Front6 => null()       !<  Front with Nodes(6),   Y-
     TYPE (BitreeMesh_Cell_Type), POINTER :: Front2 => null()       !<  Front with Nodes(2),   Y-
     TYPE (BitreeMesh_Cell_Type), POINTER :: Back4 => null()        !<  Back  with Nodes(4),   Y+
     TYPE (BitreeMesh_Cell_Type), POINTER :: Back8 => null()        !<  Back  with Nodes(8),   Y+
     TYPE (BitreeMesh_Cell_Type), POINTER :: Back7 => null()        !<  Back  with Nodes(7),   Y+
     TYPE (BitreeMesh_Cell_Type), POINTER :: Back3 => null()        !<  Back  with Nodes(3),   Y+
     TYPE (BitreeMesh_Cell_Type), POINTER :: Down1 => null()        !<  Down  with Nodes(1),   Z-
     TYPE (BitreeMesh_Cell_Type), POINTER :: Down2 => null()        !<  Down  with Nodes(2),   Z-
     TYPE (BitreeMesh_Cell_Type), POINTER :: Down3 => null()        !<  Down  with Nodes(3),   Z-
     TYPE (BitreeMesh_Cell_Type), POINTER :: Down4 => null()        !<  Down  with Nodes(4),   Z-
     TYPE (BitreeMesh_Cell_Type), POINTER :: Up5 => null()          !<  Up    with Nodes(5),   Z+
     TYPE (BitreeMesh_Cell_Type), POINTER :: Up6 => null()          !<  Up    with Nodes(6),   Z+
     TYPE (BitreeMesh_Cell_Type), POINTER :: Up7 => null()          !<  Up    with Nodes(7),   Z+
     TYPE (BitreeMesh_Cell_Type), POINTER :: Up8 => null()          !<  Up    with Nodes(8),   Z+
  END TYPE BitreeMesh_Cell_Type

  TYPE :: BitreeMesh_CellList_Type
     TYPE (BitreeMesh_Cell_Type), POINTER :: to => null()
  END TYPE BitreeMesh_CellList_Type


  TYPE (BitreeMesh_Cell_Type), TARGET,SAVE :: BitreeMesh_EmptyCell

  TYPE :: BitreeMesh_Type
     CHARACTER ( LEN = 60 ) :: theName = ' '
     TYPE (BitreeMesh_Cell_Type), DIMENSION(:,:,:), POINTER :: Cells => null()
     TYPE (BitreeMesh_CellList_Type), DIMENSION(:), POINTER :: LeafCells => null()
     INTEGER :: numNodes, numCells, imax, jmax, kmax
     INTEGER :: nallc_nodes
     REAL*8  :: DX(Max_Bit_Level+1)
     REAL*8  :: DY(Max_Bit_Level+1)
     REAL*8  :: DZ(Max_Bit_Level+1)
     REAL*8  :: Pmin(3), Pmax(3), TinyD(3)
     REAL*8,  DIMENSION(:,:), POINTER :: Posit => null()   !---(3,:)
  END TYPE BitreeMesh_Type

  TYPE(PtADTreeType), SAVE :: Bit_PtADTree

CONTAINS

  !>
  !!     Allocate a bitree-mesh. \n
  !!     Set BitMesh%nallc_nodes beforehand.
  !<
  SUBROUTINE BitreeMesh_NodeAllocate(BitMesh)
    USE array_allocator
    IMPLICIT NONE
    TYPE (BitreeMesh_Type), INTENT(INOUT) :: BitMesh
    CALL allc_real8_2Dpointer(BitMesh%Posit,  3, BitMesh%nallc_nodes, 'BitMesh')
    BitMesh%Posit(:,  1:BitMesh%nallc_nodes) = 0.D0
    RETURN
  END SUBROUTINE BitreeMesh_NodeAllocate

  !>
  !!     ReAllocate a bitree-mesh. \n
  !!     re-Set BitMesh%nallc_nodes beforehand.
  !<
  SUBROUTINE BitreeMesh_NodeReAllocate(BitMesh)
    USE array_allocator
    IMPLICIT NONE
    TYPE (BitreeMesh_Type), INTENT(INOUT) :: BitMesh
    INTEGER :: n1
    n1 = BitMesh%nallc_nodes + 1
    BitMesh%nallc_nodes = BitMesh%nallc_nodes + nallc_Bit_increase
    CALL allc_real8_2Dpointer(BitMesh%Posit,  3, BitMesh%nallc_nodes, 'BitMesh')
    BitMesh%Posit(:,  n1:BitMesh%nallc_nodes) = 0.D0
    RETURN
  END SUBROUTINE BitreeMesh_NodeReAllocate

  !>
  !!     return a pointer associated with a neighbour cell.
  !!     @param[in] pBit_Cell the cell pointer.
  !!     @param[in] idirct =-1,1,-2,2,-3,3 
  !!                 indicating  x-,x+,y-,y+,z-,z+ direction repectively.
  !!     @param[in] i =1,2,3,4: the node (one of 4) sharing whit the neighbour cell.
  !<
  FUNCTION BitreeMesh_GetNextCell(pBit_Cell,idirct,i) RESULT(p2Bit_Cell)
    IMPLICIT NONE
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell, p2Bit_Cell
    INTEGER,INTENT(in) :: idirct,i

    !--- match the order of iQuadFD2_Hex(4,-3:3) at MODULE CellConnectivity
    SELECT CASE(idirct)
    CASE(-1)
       IF(i==1)THEN
          p2Bit_Cell => pBit_Cell%Left1        !--  Left  with Nodes(1)   X-
       ELSE IF(i==2)THEN
          p2Bit_Cell => pBit_Cell%Left4        !--  Left  with Nodes(4)   X-
       ELSE IF(i==3)THEN
          p2Bit_Cell => pBit_Cell%Left8        !--  Left  with Nodes(8)   X-
       ELSE IF(i==4)THEN
          p2Bit_Cell => pBit_Cell%Left5        !--  Left  with Nodes(5)   X-
       ENDIF
    CASE(1)
       IF(i==1)THEN
          p2Bit_Cell => pBit_Cell%Right2       !--  Right with Nodes(2)   X+
       ELSE IF(i==2)THEN
          p2Bit_Cell => pBit_Cell%Right3       !--  Right with Nodes(3)   X+
       ELSE IF(i==3)THEN
          p2Bit_Cell => pBit_Cell%Right7       !--  Right with Nodes(7)   X+
       ELSE IF(i==4)THEN
          p2Bit_Cell => pBit_Cell%Right6       !--  Right with Nodes(6)   X+
       ENDIF
    CASE(-2)
       IF(i==1)THEN
          p2Bit_Cell => pBit_Cell%Front1       !--  Front with Nodes(1)   Y-
       ELSE IF(i==2)THEN
          p2Bit_Cell => pBit_Cell%Front5       !--  Front with Nodes(5)   Y-
       ELSE IF(i==3)THEN
          p2Bit_Cell => pBit_Cell%Front6       !--  Front with Nodes(6)   Y-
       ELSE IF(i==4)THEN
          p2Bit_Cell => pBit_Cell%Front2       !--  Front with Nodes(2)   Y-
       ENDIF
    CASE(2)
       IF(i==1)THEN
          p2Bit_Cell => pBit_Cell%Back4        !--  Back  with Nodes(4)   Y+
       ELSE IF(i==2)THEN
          p2Bit_Cell => pBit_Cell%Back8        !--  Back  with Nodes(8)   Y+
       ELSE IF(i==3)THEN
          p2Bit_Cell => pBit_Cell%Back7        !--  Back  with Nodes(7)   Y+
       ELSE IF(i==4)THEN
          p2Bit_Cell => pBit_Cell%Back3        !--  Back  with Nodes(3)   Y+
       ENDIF
    CASE(-3)
       IF(i==1)THEN
          p2Bit_Cell => pBit_Cell%Down1        !--  Down  with Nodes(1)   Z-
       ELSE IF(i==2)THEN
          p2Bit_Cell => pBit_Cell%Down2        !--  Down  with Nodes(2)   Z-
       ELSE IF(i==3)THEN
          p2Bit_Cell => pBit_Cell%Down3        !--  Down  with Nodes(3)   Z-
       ELSE IF(i==4)THEN
          p2Bit_Cell => pBit_Cell%Down4        !--  Down  with Nodes(4)   Z-
       ENDIF
    CASE(3)
       IF(i==1)THEN
          p2Bit_Cell => pBit_Cell%Up5          !--  Up    with Nodes(5)   Z+
       ELSE IF(i==2)THEN
          p2Bit_Cell => pBit_Cell%Up6          !--  Up    with Nodes(6)   Z+
       ELSE IF(i==3)THEN
          p2Bit_Cell => pBit_Cell%Up7          !--  Up    with Nodes(7)   Z+
       ELSE IF(i==4)THEN
          p2Bit_Cell => pBit_Cell%Up8          !--  Up    with Nodes(8)   Z+
       ENDIF
    CASE DEFAULT
       WRITE(*,*) 'Error--- wrong case at BitreeMesh_GetNextCell'
       CALL Error_STOP ( '--- BitreeMesh_GetNextCell')
    END SELECT

    RETURN
  END FUNCTION BitreeMesh_GetNextCell

  !>
  !!     Associate the neighbour pointer to a target cell
  !!     @param[in] pBit_Cell the cell pointer.
  !!     @param[in] idirct =-1,1,-2,2,-3,3 
  !!                 indicating  x-,x+,y-,y+,z-,z+ direction repectively.
  !!     @param[in] i =1,2,3,4: the node (one of 4) sharing whit the neighbour cell.
  !!     @param[in] p2Bit_Cell  the target cell.
  !<
  SUBROUTINE BitreeMesh_SetNextCell(pBit_Cell,idirct,i,p2Bit_Cell)
    IMPLICIT NONE
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell
    TYPE (BitreeMesh_Cell_Type), TARGET  :: p2Bit_Cell
    INTEGER,INTENT(in) :: idirct,i

    !--- match the order of iQuadFD2_Hex(4,-3:3) at MODULE CellConnectivity
    SELECT CASE(idirct)
    CASE(-1)
       IF(i==1)THEN
          pBit_Cell%Left1 => p2Bit_Cell        !--  Left  with Nodes(1)   X-
       ELSE IF(i==2)THEN
          pBit_Cell%Left4 => p2Bit_Cell        !--  Left  with Nodes(4)   X-
       ELSE IF(i==3)THEN
          pBit_Cell%Left8 => p2Bit_Cell        !--  Left  with Nodes(8)   X-
       ELSE IF(i==4)THEN
          pBit_Cell%Left5 => p2Bit_Cell        !--  Left  with Nodes(5)   X-
       ENDIF
    CASE(1)
       IF(i==1)THEN
          pBit_Cell%Right2 => p2Bit_Cell       !--  Right with Nodes(2)   X+
       ELSE IF(i==2)THEN
          pBit_Cell%Right3 => p2Bit_Cell       !--  Right with Nodes(3)   X+
       ELSE IF(i==3)THEN
          pBit_Cell%Right7 => p2Bit_Cell       !--  Right with Nodes(7)   X+
       ELSE IF(i==4)THEN
          pBit_Cell%Right6 => p2Bit_Cell       !--  Right with Nodes(6)   X+
       ENDIF
    CASE(-2)
       IF(i==1)THEN
          pBit_Cell%Front1 => p2Bit_Cell       !--  Front with Nodes(1)   Y-
       ELSE IF(i==2)THEN
          pBit_Cell%Front5 => p2Bit_Cell       !--  Front with Nodes(5)   Y-
       ELSE IF(i==3)THEN
          pBit_Cell%Front6 => p2Bit_Cell       !--  Front with Nodes(6)   Y-
       ELSE IF(i==4)THEN
          pBit_Cell%Front2 => p2Bit_Cell       !--  Front with Nodes(2)   Y-
       ENDIF
    CASE(2)
       IF(i==1)THEN
          pBit_Cell%Back4 => p2Bit_Cell        !--  Back  with Nodes(4)   Y+
       ELSE IF(i==2)THEN
          pBit_Cell%Back8 => p2Bit_Cell        !--  Back  with Nodes(8)   Y+
       ELSE IF(i==3)THEN
          pBit_Cell%Back7 => p2Bit_Cell        !--  Back  with Nodes(7)   Y+
       ELSE IF(i==4)THEN
          pBit_Cell%Back3 => p2Bit_Cell        !--  Back  with Nodes(3)   Y+
       ENDIF
    CASE(-3)
       IF(i==1)THEN
          pBit_Cell%Down1 => p2Bit_Cell        !--  Down  with Nodes(1)   Z-
       ELSE IF(i==2)THEN
          pBit_Cell%Down2 => p2Bit_Cell        !--  Down  with Nodes(2)   Z-
       ELSE IF(i==3)THEN
          pBit_Cell%Down3 => p2Bit_Cell        !--  Down  with Nodes(3)   Z-
       ELSE IF(i==4)THEN
          pBit_Cell%Down4 => p2Bit_Cell        !--  Down  with Nodes(4)   Z-
       ENDIF
    CASE(3)
       IF(i==1)THEN
          pBit_Cell%Up5   => p2Bit_Cell        !--  Up    with Nodes(5)   Z+
       ELSE IF(i==2)THEN
          pBit_Cell%Up6   => p2Bit_Cell        !--  Up    with Nodes(6)   Z+
       ELSE IF(i==3)THEN
          pBit_Cell%Up7   => p2Bit_Cell        !--  Up    with Nodes(7)   Z+
       ELSE IF(i==4)THEN
          pBit_Cell%Up8   => p2Bit_Cell        !--  Up    with Nodes(8)   Z+
       ENDIF
    CASE DEFAULT
       WRITE(*,*) 'Error--- wrong case at BitreeMesh_SetNextCell'
       CALL Error_STOP ( '--- BitreeMesh_SetNextCell')
    END SELECT

    RETURN
  END SUBROUTINE BitreeMesh_SetNextCell

  !>
  !!     return a pointer associated with a son cell.
  !!     @param[in] pBit_Cell the cell pointer.
  !!     @param[in] idirct =1,2
  !<
  FUNCTION BitreeMesh_SonCell(pBit_Cell,idirct) RESULT(p2Bit_Cell)
    IMPLICIT NONE
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell, p2Bit_Cell
    INTEGER :: idirct
    SELECT CASE(idirct)
    CASE(1)
       p2Bit_Cell => pBit_Cell%Son1
    CASE(2)
       p2Bit_Cell => pBit_Cell%Son7
    CASE DEFAULT
       CALL Error_STOP ( ' wrong case at BitreeMesh_SonCell')
    END SELECT
    RETURN
  END FUNCTION BitreeMesh_SonCell

  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE BitreeMesh_Cell_getLeafCells(pBit_Cell,BitMesh)
    IMPLICIT NONE
    TYPE (BitreeMesh_Type), INTENT(INOUT) :: BitMesh
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell, p2Bit_Cell
    INTEGER :: idirct, id

    IF(pBit_Cell%Level(1) == 0) RETURN 
    IF(pBit_Cell%ID <= 0) RETURN 

    IF(pBit_Cell%BiDir>0)THEN
       DO idirct = 1,2
          p2Bit_Cell => BitreeMesh_SonCell(pBit_Cell,idirct)
          CALL BitreeMesh_Cell_getLeafCells(p2Bit_Cell,BitMesh)
       ENDDO
       RETURN
    ENDIF

    id = pBit_Cell%ID
    IF(id>BitMesh%numCells) CALL Error_STOP ( ' wrong id at BitreeMesh_Cell_getLeafCells')
    BitMesh%LeafCells(id)%to => pBit_Cell

    RETURN
  END SUBROUTINE BitreeMesh_Cell_getLeafCells

  !>
  !!     Associate BitMesh%LeafCells
  !<
  SUBROUTINE BitreeMesh_getLeafCells(BitMesh)
    IMPLICIT NONE
    TYPE (BitreeMesh_Type), INTENT(INOUT) :: BitMesh
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell
    INTEGER :: i,j,k

    IF( ASSOCIATED(BitMesh%LeafCells) )  DEALLOCATE(BitMesh%LeafCells)
    ALLOCATE(BitMesh%LeafCells(BitMesh%numCells))

    DO k=1,BitMesh%kmax
       DO j=1,BitMesh%jmax
          DO i=1,BitMesh%imax
             pBit_Cell => BitMesh%Cells(i,j,k)
             CALL BitreeMesh_Cell_getLeafCells(pBit_Cell,BitMesh)
          ENDDO
       ENDDO
    ENDDO

    RETURN
  END SUBROUTINE BitreeMesh_getLeafCells

  !>
  !!     Set a BitreeMesh.
  !!     @param[in] NI    number of basic girds in 3 direction.
  !!     @param[in] D     grid-size of a basic gird in 3 direction.
  !!     @param[in] Pmin  the original corner of the domain.
  !!     @param[in,out] BitMesh  the bitree mesh.  
  !<
  SUBROUTINE BitreeMesh_Set(NI, D, Pmin, BitMesh)
    IMPLICIT NONE
    TYPE (BitreeMesh_Type), INTENT(INOUT) :: BitMesh
    INTEGER, INTENT(IN) :: NI(3)
    REAL*8,  INTENT(IN) :: D(3), Pmin(3)
    INTEGER :: i, j, k, n, ijlayer, m, ii

    BitMesh%imax     = NI(1)
    BitMesh%jmax     = NI(2)
    BitMesh%kmax     = NI(3)
    BitMesh%Pmin(:)  = Pmin(:)
    BitMesh%Pmax(:)  = Pmin(:) + NI(:) * D(:)
    BitMesh%numNodes = (BitMesh%imax+1)*(BitMesh%jmax+1)*(BitMesh%kmax+1)
    BitMesh%numCells = (BitMesh%imax  )*(BitMesh%jmax  )*(BitMesh%kmax  )

    BitMesh%nallc_nodes = BitMesh%numNodes + nallc_Bit_Increase
    CALL BitreeMesh_NodeAllocate(BitMesh)
    ALLOCATE (BitMesh%Cells(BitMesh%imax, BitMesh%jmax, BitMesh%kmax))

    BitMesh%DX(1) = D(1)
    BitMesh%DY(1) = D(2)
    BitMesh%DZ(1) = D(3)
    DO n=2,Max_Bit_Level+1
       BitMesh%DX(n) = BitMesh%DX(n-1)/2.D0
       BitMesh%DY(n) = BitMesh%DY(n-1)/2.D0
       BitMesh%DZ(n) = BitMesh%DZ(n-1)/2.D0
    ENDDO
    BitMesh%TinyD(1) = BitMesh%DX(Max_Bit_Level+1)
    BitMesh%TinyD(2) = BitMesh%DY(Max_Bit_Level+1)
    BitMesh%TinyD(3) = BitMesh%DZ(Max_Bit_Level+1)

    ijlayer = (BitMesh%imax+1)*(BitMesh%jmax+1)
    n = 0
    m = 0
    DO k=1,BitMesh%kmax+1
       DO j=1,BitMesh%jmax+1
          DO i=1,BitMesh%imax+1
             n = n+1
             BitMesh%Posit(1,n) = (i-1)*BitMesh%DX(1) + BitMesh%Pmin(1)
             BitMesh%Posit(2,n) = (j-1)*BitMesh%DY(1) + BitMesh%Pmin(2)
             BitMesh%Posit(3,n) = (k-1)*BitMesh%DZ(1) + BitMesh%Pmin(3)

             IF(i==BitMesh%imax+1 .OR. j==BitMesh%jmax+1 .OR. k==BitMesh%kmax+1) CYCLE

             m = m+1
             BitMesh%Cells(i,j,k)%ID    = m
             BitMesh%Cells(i,j,k)%Level = 1

             BitMesh%Cells(i,j,k)%Nodes(1) = n
             BitMesh%Cells(i,j,k)%Nodes(2) = n + 1
             BitMesh%Cells(i,j,k)%Nodes(3) = n + (BitMesh%imax+1) + 1
             BitMesh%Cells(i,j,k)%Nodes(4) = n + (BitMesh%imax+1)
             BitMesh%Cells(i,j,k)%Nodes(5) = BitMesh%Cells(i,j,k)%Nodes(1) + ijlayer
             BitMesh%Cells(i,j,k)%Nodes(6) = BitMesh%Cells(i,j,k)%Nodes(2) + ijlayer
             BitMesh%Cells(i,j,k)%Nodes(7) = BitMesh%Cells(i,j,k)%Nodes(3) + ijlayer
             BitMesh%Cells(i,j,k)%Nodes(8) = BitMesh%Cells(i,j,k)%Nodes(4) + ijlayer

             IF(i>1)THEN
                BitMesh%Cells(i,j,k)%Left1 => BitMesh%Cells(i-1,j,k)
                BitMesh%Cells(i,j,k)%Left4 => BitMesh%Cells(i-1,j,k)
                BitMesh%Cells(i,j,k)%Left8 => BitMesh%Cells(i-1,j,k)
                BitMesh%Cells(i,j,k)%Left5 => BitMesh%Cells(i-1,j,k)
             ELSE
                BitMesh%Cells(i,j,k)%Left1 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Left4 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Left8 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Left5 => BitreeMesh_EmptyCell
             ENDIF
             IF(i<BitMesh%imax)THEN
                BitMesh%Cells(i,j,k)%Right2 => BitMesh%Cells(i+1,j,k)
                BitMesh%Cells(i,j,k)%Right3 => BitMesh%Cells(i+1,j,k)
                BitMesh%Cells(i,j,k)%Right7 => BitMesh%Cells(i+1,j,k)
                BitMesh%Cells(i,j,k)%Right6 => BitMesh%Cells(i+1,j,k)
             ELSE
                BitMesh%Cells(i,j,k)%Right2 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Right3 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Right7 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Right6 => BitreeMesh_EmptyCell
             ENDIF

             IF(j>1)THEN
                BitMesh%Cells(i,j,k)%Front1 => BitMesh%Cells(i,j-1,k)
                BitMesh%Cells(i,j,k)%Front5 => BitMesh%Cells(i,j-1,k)
                BitMesh%Cells(i,j,k)%Front6 => BitMesh%Cells(i,j-1,k)
                BitMesh%Cells(i,j,k)%Front2 => BitMesh%Cells(i,j-1,k)
             ELSE
                BitMesh%Cells(i,j,k)%Front1 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Front5 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Front6 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Front2 => BitreeMesh_EmptyCell
             ENDIF
             IF(j<BitMesh%jmax)THEN
                BitMesh%Cells(i,j,k)%Back4 => BitMesh%Cells(i,j+1,k)
                BitMesh%Cells(i,j,k)%Back8 => BitMesh%Cells(i,j+1,k)
                BitMesh%Cells(i,j,k)%Back7 => BitMesh%Cells(i,j+1,k)
                BitMesh%Cells(i,j,k)%Back3 => BitMesh%Cells(i,j+1,k)
             ELSE
                BitMesh%Cells(i,j,k)%Back4 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Back8 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Back7 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Back3 => BitreeMesh_EmptyCell
             ENDIF

             IF(k>1)THEN
                BitMesh%Cells(i,j,k)%Down1 => BitMesh%Cells(i,j,k-1)
                BitMesh%Cells(i,j,k)%Down2 => BitMesh%Cells(i,j,k-1)
                BitMesh%Cells(i,j,k)%Down3 => BitMesh%Cells(i,j,k-1)
                BitMesh%Cells(i,j,k)%Down4 => BitMesh%Cells(i,j,k-1)
             ELSE
                BitMesh%Cells(i,j,k)%Down1 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Down2 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Down3 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Down4 => BitreeMesh_EmptyCell
             ENDIF
             IF(k<BitMesh%kmax)THEN
                BitMesh%Cells(i,j,k)%Up5 => BitMesh%Cells(i,j,k+1)
                BitMesh%Cells(i,j,k)%Up6 => BitMesh%Cells(i,j,k+1)
                BitMesh%Cells(i,j,k)%Up7 => BitMesh%Cells(i,j,k+1)
                BitMesh%Cells(i,j,k)%Up8 => BitMesh%Cells(i,j,k+1)
             ELSE
                BitMesh%Cells(i,j,k)%Up5 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Up6 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Up7 => BitreeMesh_EmptyCell
                BitMesh%Cells(i,j,k)%Up8 => BitreeMesh_EmptyCell
             ENDIF
          ENDDO
       ENDDO
    ENDDO

    RETURN
  END SUBROUTINE BitreeMesh_Set

  !>
  !!     Associate neighbour points for every cells.
  !<
  SUBROUTINE BitreeMesh_AssoNeighbour(BitMesh)
    IMPLICIT NONE
    TYPE(BitreeMesh_Type), INTENT(INOUT) :: BitMesh
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell, pNext
    REAL*8  :: p0(3), pp(3)
    INTEGER :: level(3), nel, i, idirct, ic

    CALL BitreeMesh_getLeafCells(BitMesh)

    level(:)   = Max_Bit_Level

    DO nel = 1, BitMesh%numCells
       pBit_Cell => BitMesh%LeafCells(nel)%to

       DO idirct = -3,3
          IF(idirct==0) CYCLE
          P0 = 0.5d0*( BitMesh%Posit(:,pBit_Cell%Nodes(iQuadFD2_Hex(1,idirct)))    &
               +BitMesh%Posit(:,pBit_Cell%Nodes(iQuadFD2_Hex(3,idirct))) )
          DO i=1,4
             ic = iQuadFD2_Hex(i,idirct)
             PP = 0.5d0*( BitMesh%Posit(:,pBit_Cell%Nodes(ic)) +P0)
             IF(idirct<0)THEN
                PP(-idirct) = PP(-idirct) - BitMesh%TinyD(-idirct)
             ELSE
                PP(idirct) = PP(idirct) + BitMesh%TinyD(idirct)
             ENDIF
             pNext  => BitreeMesh_CellSearch(pp,level,BitMesh)
             CALL BitreeMesh_SetNextCell(pBit_Cell,idirct,i,pNext)
          ENDDO
       ENDDO

    ENDDO

    RETURN
  END SUBROUTINE BitreeMesh_AssoNeighbour

  !>
  !!    Find hang nodes on 12 edges and 6 faces of a leaf cell.
  !!    @param[in] pBit_Cell the cell pointer.
  !!    @param[in] BitMesh  the bitree mesh.  
  !!    @param[in] Dir =1, output ipHang(1:4).  \n
  !!                   =2, output ipHang(5:8).  \n
  !!                   =3, output iphang(9:12). \n
  !!                   =4, output iphang(1:18).
  !!    @param[out] ipHang the hanging nodes.
  !<
  SUBROUTINE BitreeMesh_getCellHangNodes(pBit_Cell,BitMesh,ipHang, Dir)
    IMPLICIT NONE
    TYPE (BitreeMesh_Type), INTENT(IN) :: BitMesh
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell, p2Bit_Cell
    INTEGER, INTENT(IN)  :: Dir
    INTEGER, INTENT(OUT) :: ipHang(18)
    INTEGER :: ip(8), Level(3), LeM(3), i, k, ic, id
    REAL*8  :: pp(3), pp0(3)

    IF(pBit_Cell%Level(1) == 0)THEN
       CALL Error_STOP ( ' a fake call')
    ENDIF
    IF(pBit_Cell%BiDir>0)THEN
       CALL Error_STOP ( ' not leaf cell')
    ENDIF

    leM          = Max_Bit_Level
    Level        = pBit_Cell%Level
    ip(1:8)      = pBit_Cell%Nodes(1:8)

    !--- for hanging points
    IF(Dir==1 .OR. Dir==4)THEN
       ipHang(1:4) = 0

       p2Bit_Cell => pBit_Cell%Front1
       IF(p2Bit_Cell%Level(1) > Level(1)) THEN
          ipHang(1)  = p2Bit_Cell%Nodes(3)
       ELSE
          p2Bit_Cell => pBit_Cell%Down1
          IF(p2Bit_Cell%Level(1) > Level(1)) THEN
             ipHang(1)  = p2Bit_Cell%Nodes(6)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Down3
       IF(p2Bit_Cell%Level(1) > Level(1)) THEN 
          ipHang(2)  = p2Bit_Cell%Nodes(8)
       ELSE
          p2Bit_Cell => pBit_Cell%Back4
          IF(p2Bit_Cell%Level(1) > Level(1)) THEN
             ipHang(2)  = p2Bit_Cell%Nodes(2)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Front6
       IF(p2Bit_Cell%Level(1) > Level(1)) THEN
          ipHang(3)  = p2Bit_Cell%Nodes(8)
       ELSE
          p2Bit_Cell => pBit_Cell%Up5
          IF(p2Bit_Cell%Level(1) > Level(1)) THEN
             ipHang(3)  = p2Bit_Cell%Nodes(2)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Back7
       IF(p2Bit_Cell%Level(1) > Level(1)) THEN
          ipHang(4)  = p2Bit_Cell%Nodes(5)
       ELSE
          p2Bit_Cell => pBit_Cell%Up7
          IF(p2Bit_Cell%Level(1) > Level(1)) THEN
             ipHang(4)  = p2Bit_Cell%Nodes(4)
          ENDIF
       ENDIF

    ENDIF
    IF(Dir==2 .OR. Dir==4)THEN
       ipHang(5:8) = 0

       p2Bit_Cell => pBit_Cell%Left1
       IF(p2Bit_Cell%Level(2) > Level(2)) THEN
          ipHang(5)  = p2Bit_Cell%Nodes(3)
       ELSE
          p2Bit_Cell => pBit_Cell%Down1
          IF(p2Bit_Cell%Level(2) > Level(2)) THEN
             ipHang(5)  = p2Bit_Cell%Nodes(8)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Down3
       IF(p2Bit_Cell%Level(2) > Level(2)) THEN
          ipHang(6)  = p2Bit_Cell%Nodes(6)
       ELSE
          p2Bit_Cell => pBit_Cell%Right2
          IF(p2Bit_Cell%Level(2) > Level(2)) THEN
             ipHang(6)  = p2Bit_Cell%Nodes(4)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Left8
       IF(p2Bit_Cell%Level(2) > Level(2)) THEN
          ipHang(7)  = p2Bit_Cell%Nodes(6)
       ELSE
          p2Bit_Cell => pBit_Cell%Up5
          IF(p2Bit_Cell%Level(2) > Level(2)) THEN
             ipHang(7)  = p2Bit_Cell%Nodes(4)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Right7
       IF(p2Bit_Cell%Level(2) > Level(2)) THEN
          ipHang(8)  = p2Bit_Cell%Nodes(5)
       ELSE
          p2Bit_Cell => pBit_Cell%Up7
          IF(p2Bit_Cell%Level(2) > Level(2)) THEN
             ipHang(8)  = p2Bit_Cell%Nodes(2)
          ENDIF
       ENDIF

    ENDIF
    IF(Dir==3 .OR. Dir==4)THEN
       ipHang(9:12) = 0

       p2Bit_Cell => pBit_Cell%Left1
       IF(p2Bit_Cell%Level(3) > Level(3)) THEN
          ipHang(9)  = p2Bit_Cell%Nodes(6)
       ELSE
          p2Bit_Cell => pBit_Cell%Front1
          IF(p2Bit_Cell%Level(3) > Level(3)) THEN
             ipHang(9)  = p2Bit_Cell%Nodes(8)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Front6
       IF(p2Bit_Cell%Level(3) > Level(3)) THEN
          ipHang(10) = p2Bit_Cell%Nodes(3)
       ELSE
          p2Bit_Cell => pBit_Cell%Right2
          IF(p2Bit_Cell%Level(3) > Level(3)) THEN
             ipHang(10) = p2Bit_Cell%Nodes(5)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Left8
       IF(p2Bit_Cell%Level(3) > Level(3)) THEN
          ipHang(11) = p2Bit_Cell%Nodes(3)
       ELSE
          p2Bit_Cell => pBit_Cell%Back4
          IF(p2Bit_Cell%Level(3) > Level(3)) THEN
             ipHang(11) = p2Bit_Cell%Nodes(5)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Right7
       IF(p2Bit_Cell%Level(3) > Level(3)) THEN
          ipHang(12) = p2Bit_Cell%Nodes(4)
       ELSE
          p2Bit_Cell => pBit_Cell%Back7
          IF(p2Bit_Cell%Level(3) > Level(3)) THEN
             ipHang(12) = p2Bit_Cell%Nodes(2)
          ENDIF
       ENDIF

    ENDIF

    DO i=1,12
       IF(Dir<4 .AND. (i<=4*(Dir-1) .OR. i>4*Dir)) CYCLE
       IF(ipHang(i)==0)THEN
          pp0 = ( BitMesh%Posit(:,ip(iEdge_Hex(1,i)))   &
               + BitMesh%Posit(:,ip(iEdge_Hex(2,i))) )/2.D0
          id = (i-1)/4 +1
          DO k=1,2
             ic = iEdge_Hex(k,i)
             pp  = pp0 + iCorner_Oct(1:3,ic)*BitMesh%TinyD(:)
             p2Bit_Cell => BitreeMesh_CellSearch(pp,LeM,BitMesh)
             IF(p2Bit_Cell%Level(1)<=0) CYCLE
             IF(p2Bit_Cell%Level(id)>level(id)) ipHang(i) = p2Bit_Cell%Nodes(iOpp_Hex(ic))
             EXIT
          ENDDO
       ENDIF
    ENDDO


    IF(Dir==4)THEN
       ipHang(13:18) = 0

       p2Bit_Cell => pBit_Cell%Left1
       IF(p2Bit_Cell%Level(2) > Level(2) .AND. p2Bit_Cell%Level(3) > Level(3)) THEN
          ipHang(13) = p2Bit_Cell%Nodes(7)
       ELSE
          p2Bit_Cell => pBit_Cell%Left8
          IF(p2Bit_Cell%Level(2) > Level(2) .AND. p2Bit_Cell%Level(3) > Level(3)) THEN
             ipHang(13) = p2Bit_Cell%Nodes(2)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Right7
       IF(p2Bit_Cell%Level(2) > Level(2) .AND. p2Bit_Cell%Level(3) > Level(3)) THEN
          ipHang(14) = p2Bit_Cell%Nodes(1)
       ELSE
          p2Bit_Cell => pBit_Cell%Right2
          IF(p2Bit_Cell%Level(2) > Level(2) .AND. p2Bit_Cell%Level(3) > Level(3)) THEN
             ipHang(14) = p2Bit_Cell%Nodes(8)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Front1
       IF(p2Bit_Cell%Level(1) > Level(1) .AND. p2Bit_Cell%Level(3) > Level(3)) THEN
          ipHang(15) = p2Bit_Cell%Nodes(7)
       ELSE
          p2Bit_Cell => pBit_Cell%Front6
          IF(p2Bit_Cell%Level(1) > Level(1) .AND. p2Bit_Cell%Level(3) > Level(3)) THEN
             ipHang(15) = p2Bit_Cell%Nodes(4)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Back7
       IF(p2Bit_Cell%Level(1) > Level(1) .AND. p2Bit_Cell%Level(3) > Level(3)) THEN
          ipHang(16) = p2Bit_Cell%Nodes(1)
       ELSE
          p2Bit_Cell => pBit_Cell%Back4
          IF(p2Bit_Cell%Level(1) > Level(1) .AND. p2Bit_Cell%Level(3) > Level(3)) THEN
             ipHang(16) = p2Bit_Cell%Nodes(6)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Down1
       IF(p2Bit_Cell%Level(1) > Level(1) .AND. p2Bit_Cell%Level(2) > Level(2)) THEN
          ipHang(17) = p2Bit_Cell%Nodes(7)
       ELSE
          p2Bit_Cell => pBit_Cell%Down3
          IF(p2Bit_Cell%Level(1) > Level(1) .AND. p2Bit_Cell%Level(2) > Level(2)) THEN
             ipHang(17) = p2Bit_Cell%Nodes(5)
          ENDIF
       ENDIF

       p2Bit_Cell => pBit_Cell%Up7
       IF(p2Bit_Cell%Level(1) > Level(1) .AND. p2Bit_Cell%Level(2) > Level(2)) THEN
          ipHang(18) = p2Bit_Cell%Nodes(1)
       ELSE
          p2Bit_Cell => pBit_Cell%Up5
          IF(p2Bit_Cell%Level(1) > Level(1) .AND. p2Bit_Cell%Level(2) > Level(2)) THEN
             ipHang(18) = p2Bit_Cell%Nodes(3)
          ENDIF
       ENDIF

    ENDIF


    RETURN
  END SUBROUTINE BitreeMesh_getCellHangNodes

  !>
  !!    Bisect a leaf cell.
  !!    @param[in] pBit_Cell the cell pointer.
  !!    @param[in] Dir =1, bisect the cell along x-direction.  \n
  !!                   =2, bisect the cell along y-direction.  \n
  !!                   =3, bisect the cell along z-direction.
  !!    @param[in,out] BitMesh  the bitree mesh.  
  !<
  RECURSIVE SUBROUTINE BitreeMesh_CellBisect(pBit_Cell,Dir,BitMesh)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Dir
    TYPE (BitreeMesh_Type), INTENT(INOUT) :: BitMesh
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell, pNext, pSon
    INTEGER :: ip(8), ipHang(18), Level(3), i, id1, id2, j, k
    INTEGER :: iQ4(4), iQ4t(4), idirct
    REAL*8 :: pp(3), pp0(3)

    IF(Dir<1 .OR. Dir>3)                    &
         CALL Error_STOP ( '--- BitreeMesh_CellBisect: wrong dir')
    IF(pBit_Cell%BiDir>0)                   &
         CALL Error_STOP ( '--- BitreeMesh_CellBisect: a Bisected cell')
    IF(pBit_Cell%Level(1) == 0)             &
         CALL Error_STOP ( '--- BitreeMesh_CellBisect: a fake cell')
    IF(pBit_Cell%Level(Dir)>=Max_Bit_Level) &
         CALL Error_STOP ( '--- BitreeMesh_CellBisect: a tiny cell')

    Level = pBit_Cell%Level

    DO idirct = -3,3
       IF(idirct==0) CYCLE
       DO i=1,4
          pNext => BitreeMesh_GetNextCell(pBit_Cell,idirct,i)
          IF(pNext%Level(1)==0) CYCLE
          IF(pNext%Level(Dir) < Level(Dir))THEN
             CALL BitreeMesh_CellBisect(pNext,Dir,BitMesh)
          ELSE IF(ABS(idirct)/=Dir .AND. pNext%Level(Dir) == Level(Dir))THEN
             id2 = 6-ABS(idirct)-Dir
             IF(pNext%Level(id2) > Level(id2))THEN
                CALL BitreeMesh_CellBisect(pNext,Dir,BitMesh)
             ENDIF
          ENDIF
       ENDDO
    ENDDO

    ip(1:8)     = pBit_Cell%Nodes(1:8)

    !--- find exsiting points on edges
    CALL BitreeMesh_getCellHangNodes(pBit_Cell,BitMesh,ipHang,Dir)

    !--- for new points

    DO i=1,12
       IF(i<=4*(Dir-1) .OR. i>4*Dir .OR. ipHang(i)>0) CYCLE
       BitMesh%numNodes = BitMesh%numNodes +1
       IF( BitMesh%numNodes>=BitMesh%nallc_nodes)   &
            CALL BitreeMesh_NodeReAllocate(BitMesh)
       ipHang(i) = BitMesh%numNodes
       BitMesh%Posit(:,BitMesh%numNodes) =  ( BitMesh%Posit(:,ip(iEdge_Hex(1,i)))    &
            + BitMesh%Posit(:,ip(iEdge_Hex(2,i))) )/2.D0
    ENDDO

    pBit_Cell%BiDir = Dir
    ALLOCATE (pBit_Cell%Son1)
    ALLOCATE (pBit_Cell%Son7)

    BitMesh%numCells   = BitMesh%numCells + 1
    pBit_Cell%Son1%ID  = pBit_Cell%ID
    pBit_Cell%Son7%ID  = BitMesh%numCells
    pBit_Cell%Son1%Level(:)   = pBit_Cell%Level(:)
    pBit_Cell%Son7%Level(:)   = pBit_Cell%Level(:)
    pBit_Cell%Son1%Level(Dir) = pBit_Cell%Level(Dir) +1
    pBit_Cell%Son7%Level(Dir) = pBit_Cell%Level(Dir) +1

    !--- set nodes

    IF(Dir==1)THEN
       pBit_Cell%Son1%Nodes = (/ip(1),ipHang(1),ipHang(2),ip(4),ip(5),ipHang(3),ipHang(4),ip(8)/)
       pBit_Cell%Son7%Nodes = (/ipHang(1),ip(2),ip(3),ipHang(2),ipHang(3),ip(6),ip(7),ipHang(4)/)
    ELSE IF(Dir==2)THEN
       pBit_Cell%Son1%Nodes = (/ip(1),ip(2),ipHang(6),ipHang(5),ip(5),ip(6),ipHang(8),ipHang(7)/)
       pBit_Cell%Son7%Nodes = (/ipHang(5),ipHang(6),ip(3),ip(4),ipHang(7),ipHang(8),ip(7),ip(8)/)    
    ELSE IF(Dir==3)THEN    
       pBit_Cell%Son1%Nodes = (/ip(1),ip(2),ip(3),ip(4),ipHang(9),ipHang(10),ipHang(12),ipHang(11)/)
       pBit_Cell%Son7%Nodes = (/ipHang(9),ipHang(10),ipHang(12),ipHang(11),ip(5),ip(6),ip(7),ip(8)/)
    ENDIF

    !--- set next

    DO idirct=-3,3
       IF(idirct==0) CYCLE
       DO i=1,4
          CALL BitreeMesh_SetNextCell(pBit_Cell%Son1, idirct,i,BitreeMesh_EmptyCell)
          CALL BitreeMesh_SetNextCell(pBit_Cell%Son7, idirct,i,BitreeMesh_EmptyCell)
       ENDDO
    ENDDO

    DO i=1,4
       CALL BitreeMesh_SetNextCell(pBit_Cell%Son1, Dir,i,pBit_Cell%Son7)
       CALL BitreeMesh_SetNextCell(pBit_Cell%Son7,-Dir,i,pBit_Cell%Son1)
    ENDDO

    DO idirct=-3,3
       IF(idirct==0) CYCLE
       iQ4(:)  = iQuadFD2_Hex(:, idirct)
       iQ4t(:) = iQuadFD2_Hex(:,-idirct)

       IF(ABS(idirct)==Dir)THEN

          id1 = MOD(Dir,3)+1
          id2 = 6-Dir-id1
          IF(idirct<0)THEN
             pSon => pBit_Cell%Son1
          ELSE
             pSon => pBit_Cell%Son7
          ENDIF
          DO i=1,4
             pNext => BitreeMesh_GetNextCell(pBit_Cell, idirct, i)
             IF(pNext%level(1)==0)THEN
             ELSE IF(pNext%level(id1) < Level(id1) .OR. pNext%level(id2) < Level(id2))THEN
                !--- next is bigger
                DO j=1,4
                   CALL BitreeMesh_SetNextCell(pSon, idirct, j, pNext)
                   IF(pNext%Nodes(iQ4t(j))==pSon%Nodes(iQ4(j)))THEN
                      CALL BitreeMesh_SetNextCell(pNext, -idirct, j, pSon)
                   ENDIF
                ENDDO
                EXIT
             ELSE IF(pNext%level(id1) == Level(id1) .AND. pNext%level(id2) == Level(id2))THEN
                !--- next is equal size
                DO j=1,4
                   CALL BitreeMesh_SetNextCell(pSon,   idirct, j, pNext)
                   CALL BitreeMesh_SetNextCell(pNext, -idirct, j, pSon)
                ENDDO
                EXIT
             ELSE
                !--- next is smaller
                CALL BitreeMesh_SetNextCell(pSon,   idirct, i, pNext)
                CALL BitreeMesh_SetNextCell(pNext, -idirct, 1, pSon)
                CALL BitreeMesh_SetNextCell(pNext, -idirct, 2, pSon)
                CALL BitreeMesh_SetNextCell(pNext, -idirct, 3, pSon)
                CALL BitreeMesh_SetNextCell(pNext, -idirct, 4, pSon)
             ENDIF
          ENDDO

       ELSE

          id2 = 6-Dir-ABS(idirct)
          DO k=1,2
             IF(k==1)THEN
                pSon => pBit_Cell%Son1
             ELSE
                pSon => pBit_Cell%Son7
             ENDIF
             DO i=1,4
                pNext => BitreeMesh_GetNextCell(pBit_Cell, idirct, i)
                IF(pNext%level(1)==0)THEN
                ELSE IF(pNext%level(Dir) < Level(Dir))THEN
                   CALL Error_STOP ( '--- BitreeMesh_CellBisect: neighbour too big')
                ELSE IF(pNext%level(Dir) == Level(Dir))THEN
                   !--- next is bigger than son
                   DO j=1,4
                      CALL BitreeMesh_SetNextCell(pSon, idirct, j, pNext)
                      IF(pNext%Nodes(iQ4t(j))==pSon%Nodes(iQ4(j)))THEN
                         CALL BitreeMesh_SetNextCell(pNext, -idirct, j, pSon)
                      ENDIF
                   ENDDO
                   EXIT
                ELSE IF(pNext%level(id2) <= Level(id2))THEN
                   !--- next is eqaul to son at Dir direction, no smaller at id2 direction
                   DO j=1,4
                      IF(pNext%Nodes(iQ4t(j))==pSon%Nodes(iQ4(j)))THEN
                         CALL BitreeMesh_SetNextCell(pNext, -idirct, j, pSon)
                      ENDIF
                   ENDDO
                   DO j=1,4
                      IF(pNext%Nodes(iQ4t(j))==pSon%Nodes(iQ4(j)))THEN
                         CALL BitreeMesh_SetNextCell(pSon, idirct, 1, pNext)
                         CALL BitreeMesh_SetNextCell(pSon, idirct, 2, pNext)
                         CALL BitreeMesh_SetNextCell(pSon, idirct, 3, pNext)
                         CALL BitreeMesh_SetNextCell(pSon, idirct, 4, pNext)
                         EXIT
                      ENDIF
                   ENDDO
                ELSE
                   !--- next is smaller than son
                   DO j=1,4
                      IF(pNext%Nodes(iQ4t(j))==pSon%Nodes(iQ4(j)))THEN
                         CALL BitreeMesh_SetNextCell(pNext, -idirct, 1, pSon)
                         CALL BitreeMesh_SetNextCell(pNext, -idirct, 2, pSon)
                         CALL BitreeMesh_SetNextCell(pNext, -idirct, 3, pSon)
                         CALL BitreeMesh_SetNextCell(pNext, -idirct, 4, pSon)
                         EXIT
                      ENDIF
                   ENDDO
                   DO j=1,4
                      IF(pNext%Nodes(iQ4t(j))==pSon%Nodes(iQ4(j)))THEN
                         CALL BitreeMesh_SetNextCell(pSon, idirct, j, pNext)
                      ENDIF
                   ENDDO
                ENDIF
             ENDDO
          ENDDO

       ENDIF

    ENDDO

    RETURN
  END SUBROUTINE BitreeMesh_CellBisect

  !>
  !!     Split a cell until all offspring leaf cells have levels not lower than 
  !!           to given values.
  !<
  RECURSIVE SUBROUTINE BitreeMesh_CellMatchLevel(pBit_Cell,Level,BitMesh)
    IMPLICIT NONE
    TYPE (BitreeMesh_Type), INTENT(INOUT) :: BitMesh
    INTEGER, INTENT(IN) :: Level(3)
    INTEGER :: idirct, idlev(3)
    TYPE (BitreeMesh_Cell_Type), POINTER ::pBit_Cell,p2Bit_Cell

    IF(pBit_Cell%Level(1) == 0) RETURN      !--- Empty Cell
    IF(pBit_Cell%Level(1)>=Max_Bit_Level) RETURN
    IF(pBit_Cell%Level(2)>=Max_Bit_Level) RETURN
    IF(pBit_Cell%Level(3)>=Max_Bit_Level) RETURN
    IF(  pBit_Cell%Level(1)>=Level(1) .AND.    &
         pBit_Cell%Level(2)>=Level(2) .AND.    &
         pBit_Cell%Level(3)>=Level(3)) RETURN

    IF(pBit_Cell%BiDir>0)THEN
       DO idirct = 1,2
          p2Bit_Cell => BitreeMesh_SonCell(pBit_Cell,idirct)
          CALL BitreeMesh_CellMatchLevel(p2Bit_Cell,Level,BitMesh)
       ENDDO
    ELSE
       idlev = Level(:) - pBit_Cell%Level(:)
       idirct = 1
       IF(idlev(2)>idlev(idirct)) idirct = 2
       IF(idlev(3)>idlev(idirct)) idirct = 3
       CALL BitreeMesh_CellBisect(pBit_Cell, idirct, BitMesh)
       CALL BitreeMesh_CellMatchLevel(pBit_Cell,Level,BitMesh)
    ENDIF

    RETURN
  END SUBROUTINE BitreeMesh_CellMatchLevel

  !>
  !!     return a pointer associated with a cell with a specifing level
  !!            which contains a given point. 
  !!            If level is given too high, return a leaf cell.
  !!     @param[in] pp     the position of a point.
  !!     @param[in] Level  the level to reach.
  !!     @param[in] BitMesh  the bitree mesh.  
  !<
  FUNCTION BitreeMesh_CellSearch(pp,Level,BitMesh) RESULT(pBit_Cell)
    IMPLICIT NONE
    TYPE (BitreeMesh_Type), INTENT(IN) :: BitMesh
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell
    REAL*8,  INTENT(IN) :: pp(3)
    INTEGER, INTENT(IN) :: Level(3)
    REAL*8  :: d
    INTEGER :: Lev(3),i0,j0,k0

    i0 = (pp(1) - BitMesh%Pmin(1)) / BitMesh%DX(1) +1
    j0 = (pp(2) - BitMesh%Pmin(2)) / BitMesh%DY(1) +1
    k0 = (pp(3) - BitMesh%Pmin(3)) / BitMesh%DZ(1) +1
    IF( i0>BitMesh%imax .OR. j0>BitMesh%jmax .OR. k0>BitMesh%kmax .OR.  &
         i0<=0 .OR. j0<=0 .OR. k0<=0 )THEN
       pBit_Cell => BitreeMesh_EmptyCell
       RETURN
    ENDIF

    Lev = Level
    WHERE(Lev<0 .OR. Lev>Max_Bit_Level) Lev=Max_Bit_Level

    pBit_Cell => BitMesh%Cells(i0,j0,k0)
    DO WHILE(pBit_Cell%BiDir>0 .AND. pBit_Cell%Level(1)>0)
       IF(pBit_Cell%Level(1)>=Lev(1)) EXIT
       IF(pBit_Cell%Level(2)>=Lev(2)) EXIT
       IF(pBit_Cell%Level(3)>=Lev(3)) EXIT
       d = pp(pBit_Cell%BiDir) - BitMesh%Posit(pBit_Cell%BiDir,pBit_Cell%Son1%Nodes(7))
       IF(d>0)THEN
          pBit_Cell => BitreeMesh_SonCell(pBit_Cell, 2)
       ELSE
          pBit_Cell => BitreeMesh_SonCell(pBit_Cell, 1)
       ENDIF
    ENDDO

    RETURN
  END FUNCTION BitreeMesh_CellSearch

  !>
  !!     reutrn the centre position of a cell.
  !<
  FUNCTION BitreeMesh_getCellCentre(pBit_Cell,BitMesh) RESULT(pp)
    IMPLICIT NONE
    TYPE (BitreeMesh_Type), INTENT(IN) :: BitMesh
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell
    REAL*8 :: pp(3)
    pp = ( BitMesh%Posit(:,pBit_Cell%Nodes(1))   &
         + BitMesh%Posit(:,pBit_Cell%Nodes(7)) ) / 2.d0
    RETURN
  END FUNCTION BitreeMesh_getCellCentre

  !>
  !!     Mark all leaf cells under pBit_Cell.
  !<
  RECURSIVE SUBROUTINE BitreeMesh_CellMark(pBit_Cell,CellMark,Mark)
    IMPLICIT NONE
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell, p2Bit_Cell
    INTEGER :: CellMark(*)
    INTEGER, INTENT(IN) :: Mark
    INTEGER :: ip(8), idirct

    IF(pBit_Cell%Level(1) == 0) RETURN
    IF(pBit_Cell%ID<=0)         RETURN
    IF(pBit_Cell%BiDir>0)THEN
       DO idirct = 1,2
          p2Bit_Cell => BitreeMesh_SonCell(pBit_Cell,idirct)
          CALL BitreeMesh_CellMark(p2Bit_Cell,CellMark,Mark)
       ENDDO
    ELSE
       CellMark(pBit_Cell%ID) = Mark
    ENDIF
  END SUBROUTINE BitreeMesh_CellMark

  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE BitreeMesh_Cell_Output(IO, pBit_Cell)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: IO
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell, p2Bit_Cell

    WRITE(IO,err=111) pBit_Cell%ID, pBit_Cell%Level, pBit_Cell%BiDir
    WRITE(IO,err=111) pBit_Cell%Nodes(1:8)    

    IF(pBit_Cell%BiDir>0)THEN
       p2Bit_Cell => pBit_Cell%son1
       CALL BitreeMesh_Cell_Output(IO,p2Bit_Cell)
       p2Bit_Cell => pBit_Cell%son7
       CALL BitreeMesh_Cell_Output(IO,p2Bit_Cell)
    ENDIF

    RETURN
111 WRITE(*,*) 'Error--- BitreeMesh_Cell_Output:'
    CALL Error_STOP (' ')
  END SUBROUTINE BitreeMesh_Cell_Output

  !>
  !!     Output the mesh through a IO channel.
  !<
  SUBROUTINE BitreeMesh_Output(IO,BitMesh)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: IO
    TYPE(BitreeMesh_Type), INTENT(IN) :: BitMesh
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell
    INTEGER :: i, j, k, ip


    WRITE(IO,err=111) BitMesh%theName
    WRITE(IO,err=111) BitMesh%numNodes, BitMesh%numCells,    &
         BitMesh%imax, BitMesh%jmax, BitMesh%kmax
    WRITE(IO,err=111) BitMesh%DX(1:Max_Bit_Level+1)
    WRITE(IO,err=111) BitMesh%DY(1:Max_Bit_Level+1)
    WRITE(IO,err=111) BitMesh%DZ(1:Max_Bit_Level+1)
    WRITE(IO,err=111) BitMesh%Pmin(:)
    WRITE(IO,err=111) BitMesh%Pmax(:)
    WRITE(IO,err=111) BitMesh%TinyD(:)
    WRITE(IO,err=111) (BitMesh%Posit(:,ip), ip=1,BitMesh%numNodes)

    DO i=1,BitMesh%imax
       DO j=1,BitMesh%jmax
          DO k=1,BitMesh%kmax
             pBit_Cell => BitMesh%Cells(i,j,k)
             CALL BitreeMesh_Cell_Output(IO,pBit_Cell)
          ENDDO
       ENDDO
    ENDDO

    RETURN
111 WRITE(*,*) 'Error--- BitreeMesh_Output: '
    CALL Error_STOP (' ')

  END SUBROUTINE BitreeMesh_Output

  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE BitreeMesh_Cell_Input(IO,pBit_Cell)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: IO
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell, p2Bit_Cell

    READ(IO,err=111) pBit_Cell%ID, pBit_Cell%Level, pBit_Cell%BiDir
    READ(IO,err=111) pBit_Cell%Nodes(1:8)    

    IF(pBit_Cell%BiDir>0)THEN
       ALLOCATE (pBit_Cell%Son1)
       ALLOCATE (pBit_Cell%Son7)
       p2Bit_Cell => pBit_Cell%son1
       CALL BitreeMesh_Cell_Input(IO,p2Bit_Cell)
       p2Bit_Cell => pBit_Cell%son7
       CALL BitreeMesh_Cell_Input(IO,p2Bit_Cell)
    ENDIF

    RETURN
111 WRITE(*,*) 'Error--- BitreeMesh_Cell_Input: '
    CALL Error_STOP (' ')

  END SUBROUTINE BitreeMesh_Cell_Input

  !>
  !!     Input the mesh through a IO channel.
  !<
  SUBROUTINE BitreeMesh_Input(IO,BitMesh)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: IO
    TYPE(BitreeMesh_Type), INTENT(INOUT) :: BitMesh
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell
    INTEGER :: i, j, k, ip

    READ(IO,err=111) BitMesh%theName
    READ(IO,err=111) BitMesh%numNodes, BitMesh%numCells,    &
         BitMesh%imax, BitMesh%jmax, BitMesh%kmax
    READ(IO,err=111) BitMesh%DX(1:Max_Bit_Level+1)
    READ(IO,err=111) BitMesh%DY(1:Max_Bit_Level+1)
    READ(IO,err=111) BitMesh%DZ(1:Max_Bit_Level+1)
    READ(IO,err=111) BitMesh%Pmin(:)
    READ(IO,err=111) BitMesh%Pmax(:)
    READ(IO,err=111) BitMesh%TinyD(:)

    BitMesh%nallc_nodes = BitMesh%numNodes + nallc_Bit_Increase
    CALL BitreeMesh_NodeAllocate(BitMesh)
    ALLOCATE (BitMesh%Cells(BitMesh%imax, BitMesh%jmax, BitMesh%kmax))

    READ(IO,err=111) (BitMesh%Posit(:,ip), ip=1,BitMesh%numNodes)

    DO i=1,BitMesh%imax
       DO j=1,BitMesh%jmax
          DO k=1,BitMesh%kmax
             pBit_Cell => BitMesh%Cells(i,j,k)
             CALL BitreeMesh_Cell_Input(IO,pBit_Cell)
          ENDDO
       ENDDO
    ENDDO


    CALL BitreeMesh_AssoNeighbour(BitMesh)

    RETURN
111 WRITE(*,*) 'Error--- BitreeMesh_Input: '
    CALL Error_STOP (' ')

  END SUBROUTINE BitreeMesh_Input

  !>
  !!    Output the mesh to a file as EnSight format.
  !<
  SUBROUTINE BitreeMesh_OutputEnSight(JobName,JobNameLength,BitMesh)
    IMPLICIT NONE
    TYPE (BitreeMesh_Type), INTENT(INOUT) :: BitMesh
    INTEGER :: JobNameLength
    CHARACTER(LEN=JobNameLength) JobName
    INTEGER :: i,j,is
    REAL*4,  DIMENSION(:,:), ALLOCATABLE :: xyz
    CHARACTER*80 :: string

    OPEN(11,file=JobName(1:JobNameLength)//'.geo',    &
         status='UNKNOWN',form='UNFORMATTED',err=111)

    CALL BitreeMesh_getLeafCells(BitMesh)
    ALLOCATE (xyz(3,BitMesh%numNodes))
    DO i=1,BitMesh%numNodes
       xyz(:,i) = BitMesh%Posit(:,i)
    ENDDO

    string = 'Fortran Binary' 
    WRITE(11,err=111) string
    string = 'ENSIGHT'
    WRITE(11,err=111) string
    string = 'GEO FILE'
    WRITE(11,err=111) string
    string = 'node id off'
    WRITE(11,err=111) string
    string = 'element id off'
    WRITE(11,err=111) string
    string = 'part'
    WRITE(11,err=111) string
    is = 1 
    WRITE(11,err=111) is
    string = 'Volume Mesh'
    WRITE(11,err=111) string

    string = 'coordinates'
    WRITE(11,err=111) string
    WRITE(11,err=111) BitMesh%numNodes
    WRITE(11,err=111) xyz(1,1:BitMesh%numNodes)
    WRITE(11,err=111) xyz(2,1:BitMesh%numNodes)
    WRITE(11,err=111) xyz(3,1:BitMesh%numNodes)

    string = 'hexa8'
    WRITE(11,err=111) string
    WRITE(11,err=111) BitMesh%numCells 
    WRITE(11,err=111) ((BitMesh%LeafCells(i)%to%Nodes(j),j=1,8), i=1,BitMesh%numCells)
    CLOSE(11)

    DEALLOCATE (xyz)
    RETURN
111 WRITE(*,*) 'Error--- BitreeMesh_OutputEnSight: ',      &
         JobName(1:JobNameLength)//'.geo'
    CALL Error_STOP (' ')
  END SUBROUTINE BitreeMesh_OutputEnSight

  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE BitreeMesh_CellCheck(pBit_Cell,BitMesh)
    IMPLICIT NONE
    TYPE (BitreeMesh_Type), INTENT(IN) :: BitMesh
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell, p2Bit_Cell
    INTEGER :: ip(4),iq(4),ierror,i,idirct, id2, id3
    REAL*8  :: pp(3)

    IF(pBit_Cell%Level(1) == 0) RETURN
    IF(pBit_Cell%BiDir>0)THEN
       p2Bit_Cell => pBit_Cell%son1
       CALL BitreeMesh_CellCheck(p2Bit_Cell,BitMesh)
       p2Bit_Cell => pBit_Cell%son7
       CALL BitreeMesh_CellCheck(p2Bit_Cell,BitMesh)
    ELSE

       !--- check nodes and cell_id
       IF(pBit_Cell%ID<=0 .OR. pBit_Cell%ID>BitMesh%numCells)THEN
          WRITE(*,*)'Error--- id check:'
          WRITE(*,*)'ID=', pBit_Cell%ID
          CALL Error_STOP ( '--- BitreeMesh_CellCheck 3')
       ENDIF
       DO i=1,8
          IF(pBit_Cell%Nodes(i)<=0 .OR. pBit_Cell%Nodes(i)>BitMesh%numNodes)THEN
             WRITE(*,*)'Error--- node check:'
             WRITE(*,*)'Nodes=', pBit_Cell%Nodes(1:8)
             CALL Error_STOP ( '--- BitreeMesh_CellCheck 4')
          ENDIF
       ENDDO

       !--- check connectivity with neighbours

       ierror = 0
       DO idirct = -3,3
          IF(idirct==0) CYCLE
          id2 = MOD(ABS(idirct),3)+1
          id3 = 6-ABS(idirct)-id2

          DO i=1,4
             p2Bit_Cell => BitreeMesh_GetNextCell(pBit_Cell,idirct,i)
             IF(p2Bit_Cell%Level(1)==0) CYCLE

             ip(1:4) =  pBit_Cell%Nodes(iQuadFD2_Hex(1:4, idirct))
             iq(1:4) = p2Bit_Cell%Nodes(iQuadFD2_Hex(1:4,-idirct))

             IF(  ABS(p2Bit_Cell%Level(1) - pBit_Cell%Level(1))>1     .OR.    &
                  ABS(p2Bit_Cell%Level(2) - pBit_Cell%Level(2))>1     .OR.    &
                  ABS(p2Bit_Cell%Level(3) - pBit_Cell%Level(3))>1    )THEN
                ierror = 1
             ELSE IF( p2Bit_Cell%Level(id2) < pBit_Cell%Level(id2)   .AND.   &
                  p2Bit_Cell%Level(id3) > pBit_Cell%Level(id3)  )THEN
                ierror = 1
             ELSE IF( p2Bit_Cell%Level(id2) > pBit_Cell%Level(id2)   .AND.   &
                  p2Bit_Cell%Level(id3) < pBit_Cell%Level(id3)  )THEN
                ierror = 1
             ELSE IF( p2Bit_Cell%Level(id2) == pBit_Cell%Level(id2)  .AND.   &
                  p2Bit_Cell%Level(id3) == pBit_Cell%Level(id3) )THEN
                IF(  ip(1)/=iq(1) .OR. ip(2)/=iq(2) .OR.    &
                     ip(3)/=iq(3) .OR. ip(4)/=iq(4) ) THEN
                   ierror = 1
                ENDIF
             ELSE 
                IF(  ip(1)/=iq(1) .AND. ip(2)/=iq(2) .AND.     &
                     ip(3)/=iq(3) .AND. ip(4)/=iq(4) )THEN
                   ierror = 1
                ENDIF
             ENDIF
             IF(ierror==1)THEN
                WRITE(*,*)'Error--- next check: i=',i
                WRITE(*,*)'this ID,Level: ',pBit_Cell%ID,pBit_Cell%Level
                WRITE(*,*)'next ID,Level: ',p2Bit_Cell%ID,p2Bit_Cell%Level
                WRITE(*,*)'this Nodes: ',pBit_Cell%Nodes(1:8)
                WRITE(*,*)'next Nodes: ',p2Bit_Cell%Nodes(1:8)
                CALL Error_STOP ( '--- BitreeMesh_CellCheck 1')
             ENDIF
          ENDDO
       ENDDO

       !---Check Nodes positions

       DO i=2,8
          pp(:) = BitMesh%Posit(:,pBit_Cell%Nodes(i))   &
               - BitMesh%Posit(:,pBit_Cell%Nodes(1))
          IF(i==2 .OR. i==3 .OR. i==6 .OR. i==7)THEN
             pp(1) = ABS(pp(1) - BitMesh%DX(pBit_Cell%Level(1)))
          ENDIF
          IF(i==3 .OR. i==4 .OR. i==7 .OR. i==8)THEN
             pp(2) = ABS(pp(2) - BitMesh%DY(pBit_Cell%Level(2)))
          ENDIF
          IF(i>4)THEN
             pp(3) = ABS(pp(3) - BitMesh%DZ(pBit_Cell%Level(3)))
          ENDIF

          IF(  pp(1)>BitMesh%TinyD(1) .OR. pp(2)>BitMesh%TinyD(2) .OR.   &
               pp(3)>BitMesh%TinyD(3) ) THEN
             WRITE(*,*)'Error--- position check:'
             WRITE(*,*)'Level =',pBit_Cell%Level
             WRITE(*,*)'Nodes=', pBit_Cell%Nodes(1:8)
             WRITE(*,*)'Posit1=',BitMesh%Posit(:,pBit_Cell%Nodes(1))
             WRITE(*,*)'Posit2=',BitMesh%Posit(:,pBit_Cell%Nodes(2))
             WRITE(*,*)'Posit3=',BitMesh%Posit(:,pBit_Cell%Nodes(3))
             WRITE(*,*)'Posit4=',BitMesh%Posit(:,pBit_Cell%Nodes(4))
             WRITE(*,*)'Posit5=',BitMesh%Posit(:,pBit_Cell%Nodes(5))
             WRITE(*,*)'Posit6=',BitMesh%Posit(:,pBit_Cell%Nodes(6))
             WRITE(*,*)'Posit7=',BitMesh%Posit(:,pBit_Cell%Nodes(7))
             WRITE(*,*)'Posit8=',BitMesh%Posit(:,pBit_Cell%Nodes(8))
             WRITE(*,*)'dx,dy,dz=',BitMesh%DX(pBit_Cell%Level),   &
                  BitMesh%DY(pBit_Cell%Level), BitMesh%DZ(pBit_Cell%Level)
             CALL Error_STOP ( '--- BitreeMesh_CellCheck 2')
          ENDIF
       ENDDO

    ENDIF

    RETURN
  END SUBROUTINE BitreeMesh_CellCheck

  !>
  !!    check the bitree-mesh
  !<
  SUBROUTINE BitreeMesh_Check(BitMesh)
    IMPLICIT NONE
    TYPE (BitreeMesh_Type), INTENT(IN) :: BitMesh
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell
    INTEGER :: i,j,k

    DO k=1,BitMesh%kmax
       DO j=1,BitMesh%jmax
          DO i=1,BitMesh%imax
             pBit_Cell => BitMesh%Cells(i,j,k)
             CALL BitreeMesh_CellCheck(pBit_Cell,BitMesh)
          ENDDO
       ENDDO
    ENDDO
    WRITE(*,*)'BitMesh: numNodes, numCells=',BitMesh%numNodes, BitMesh%numCells
    WRITE(*,*)' ...pass Bit_Web Checking...'

    RETURN
  END SUBROUTINE BitreeMesh_Check

  !>
  !!     Check the positions of nodes by ADTree
  !<
  SUBROUTINE BitreeMesh_CheckNodes(BitMesh)
    IMPLICIT NONE
    TYPE (BitreeMesh_Type) :: BitMesh
    INTEGER :: Isucc, Ips(2), ip, NB
    REAL*8 :: HalfSize(3), Dist(2), pt(3)

    CALL PtADTree_SetTree(Bit_PtADTree, BitMesh%Posit, BitMesh%numNodes, BitMesh%TinyD)

    DO ip = 1,BitMesh%numNodes
       pt = BitMesh%Posit(:,ip)
       Isucc = -1
       NB = 2
       CALL PtADTree_SearchNode(Bit_PtADTree,pt,IPs, Dist, NB, Isucc)
       IF(Isucc==1 .AND. NB==1)THEN
       ELSE IF(Isucc==0)THEN
          CALL Error_STOP ( '---1')
       ELSE
          WRITE(*,*)IPs
          CALL Error_STOP ( '---2')
       ENDIF
    ENDDO

    CALL PtADTree_Clear(Bit_PtADTree)
    WRITE(*,*)'--- pass BitreeMesh_CheckNodes'

    RETURN
  END SUBROUTINE BitreeMesh_CheckNodes

  !>
  !!     Split all cells into Hybrid-Mesh.
  !!     @param[in] BitMesh  the bitree mesh.  
  !!     @param[out]   HybridMesh  the hybrid mesh.
  !!     @param[in]    isTet  
  !!               if isTet(i)=1, leaf-cell i will splitted by tetrahedra only.
  !!               otherwise, leaf-cell i will splitted by hybrid-cells.
  !<
  SUBROUTINE BitreeMesh_getHybridMesh(BitMesh,HybridMesh, isTet)
    USE HybridMeshStorage
    USE NodeNetTree
    IMPLICIT NONE
    TYPE (BitreeMesh_Type) :: BitMesh
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell, p2Bit_Cell
    TYPE (NodeNetTreeType) :: QuadTree
    TYPE (HybridMeshStorageType) :: HybridMesh
    INTEGER, INTENT(IN), OPTIONAL :: isTet(*)
    INTEGER, DIMENSION(:,:), POINTER :: IP_Face
    INTEGER, DIMENSION(  :), POINTER :: counter_Quad
    LOGICAL, DIMENSION(  :), POINTER :: noHang
    INTEGER :: numFace, numQuad, i,j,k, nel, level(3), iQuad, keep, idnext, Dir
    INTEGER :: ip(8), ip4(4), ipe(4), ip2(2), ipHang(18), ID, IDs(4)
    LOGICAL :: triOnly

    ALLOCATE(IP_Face(4,    18*BitMesh%numCells))
    ALLOCATE(counter_Quad(0:4*BitMesh%numCells))
    ALLOCATE(noHang(         BitMesh%numCells))
    counter_Quad(0) = 0

    CALL NodeNetTree_Allocate(2, BitMesh%numNodes, 4*BitMesh%numCells, QuadTree)
    CALL BitreeMesh_getLeafCells(BitMesh)

    !--- split non-hang quads
    numFace  = 0
    numQuad = 0
    DO nel = 1, BitMesh%numCells
       pBit_Cell => BitMesh%LeafCells(nel)%to
       Level(1:3)= pBit_Cell%Level(1:3)
       ip(1:8)   = pBit_Cell%Nodes(1:8)

       CALL BitreeMesh_getCellHangNodes(pBit_Cell,BitMesh,ipHang,4)

       noHang(nel) = .TRUE.
       DO i=1,18
          IF(ipHang(i)>0) noHang(nel) = .FALSE.
       ENDDO

       DO Dir=-3,3
          IF(Dir==0) CYCLE
          p2Bit_Cell => BitreeMesh_GetNextCell(pBit_Cell,Dir,1)
          IF(p2Bit_Cell%Level(1)>Level(1)) CYCLE    !--- a hanging face
          IF(p2Bit_Cell%Level(2)>Level(2)) CYCLE    !--- a hanging face
          IF(p2Bit_Cell%Level(3)>Level(3)) CYCLE    !--- a hanging face

          ip4(1:4) = ip(iQuadFD2_Hex(:,Dir))
          ip2(:)   = (/ip4(1), ip4(3)/)
          CALL NodeNetTree_SearchAdd(2,ip2,QuadTree,iQuad)
          IF(iQuad>numQuad)THEN
             !--- new face
             numQuad = iQuad
             ipe(1:4) = ipHang(jEdge_QuadFD2_Hex(:,Dir))

             triOnly = .TRUE.
             IF(PRESENT(isTet))THEN
                IF(isTet(nel)/=1)THEN
                   idnext = p2Bit_Cell%ID
                   IF(idNext<=0)THEN
                      triOnly = .FALSE.
                   ELSE
                      IF(isTet(idnext)/=1) triOnly = .FALSE.
                   ENDIF
                ENDIF
             ENDIF
             CALL Quad_spliting(ip4, ipe, numFace, IP_Face, triOnly)
             counter_Quad(iQuad) = numFace
             IF(numQuad>4*BitMesh%numCells .OR. numFace>18*BitMesh%numCells)THEN
                WRITE(*,*)'Error--- numQuad,numFace,BitMesh%numCells=',   &
                     numQuad,numFace,BitMesh%numCells
                CALL Error_STOP ( '--- BitreeMesh_getHybridMesh')
             ENDIF
          ENDIF
       ENDDO

    ENDDO

    !--- collect cells

    HybridMesh%NB_Tet = 0
    HybridMesh%NB_Pyr = 0
    HybridMesh%NB_Hex = 0
    HybridMesh%NB_Point = 0
    ALLOCATE(HybridMesh%IP_Tet(4,2*numFace))
    ALLOCATE(HybridMesh%IP_Pyr(5,2*numFace))
    ALLOCATE(HybridMesh%IP_Hex(8,BitMesh%numCells))
    ALLOCATE(HybridMesh%Posit(3,BitMesh%numNodes + BitMesh%numCells))
    HybridMesh%NB_Point = BitMesh%numNodes
    HybridMesh%Posit(:,1:HybridMesh%NB_Point) = BitMesh%Posit(:,1:HybridMesh%NB_Point)

    DO nel = 1, BitMesh%numCells
       pBit_Cell => BitMesh%LeafCells(nel)%to
       Level(1:3)= pBit_Cell%Level(1:3)
       ip(1:8)   = pBit_Cell%Nodes(1:8)

       !--- check if no hang point
       IF(noHang(nel))THEN
          IF(.NOT. PRESENT(isTet))THEN
             keep = 1
          ELSE
             IF(isTet(nel)==1)THEN
                keep = 1
             ELSE
                keep = 2
                DO Dir=-3,3
                   IF(Dir==0) CYCLE
                   p2Bit_Cell => BitreeMesh_GetNextCell(pBit_Cell,Dir,1)
                   idnext = p2Bit_Cell%ID
                   IF(idnext<=0) CYCLE
                   IF(isTet(idnext)==1) keep = 0
                ENDDO
             ENDIF
          ENDIF

          IF(keep==1)THEN
             DO i=1,6
                HybridMesh%NB_Tet = HybridMesh%NB_Tet+1
                HybridMesh%IP_Tet(:,HybridMesh%NB_Tet) = ip(iTet_Hex(:,i))
             ENDDO
             CYCLE
          ELSE IF(Keep==2)THEN
             HybridMesh%NB_Hex = HybridMesh%NB_Hex+1
             HybridMesh%IP_Hex(:,HybridMesh%NB_Hex) = ip(1:8)
             CYCLE
          ENDIF
       ENDIF

       HybridMesh%NB_Point = HybridMesh%NB_Point + 1
       HybridMesh%Posit(:,HybridMesh%NB_Point) =      &
            ( BitMesh%Posit(:,ip(1)) + BitMesh%Posit(:,ip(7)) )/2.D0

       DO Dir=-3,3
          IF(Dir==0) CYCLE

          ip4(1:4) = ip(iQuadFD2_Hex(:,Dir))
          ip2(:) = (/ip4(1), ip4(3)/)
          CALL NodeNetTree_Search(2,ip2,QuadTree,iQuad)
          IF(iQuad>0)THEN
             !--- not hang face
             DO k = counter_Quad(iQuad-1)+1, counter_Quad(iQuad)
                IF(IP_Face(4,k)==0)THEN
                   HybridMesh%NB_Tet = HybridMesh%NB_Tet + 1
                   IF(Dir<0)THEN
                      HybridMesh%IP_Tet(1:3,HybridMesh%NB_Tet) = IP_Face(1:3,k)
                   ELSE
                      HybridMesh%IP_Tet(1:3,HybridMesh%NB_Tet) =   &
                           (/IP_Face(1,k),IP_Face(3,k),IP_Face(2,k)/)
                   ENDIF
                   HybridMesh%IP_Tet(4,HybridMesh%NB_Tet) = HybridMesh%NB_Point 
                ELSE
                   HybridMesh%NB_Pyr = HybridMesh%NB_Pyr + 1
                   IF(Dir<0)THEN
                      HybridMesh%IP_Pyr(1:4,HybridMesh%NB_Pyr) = IP_Face(1:4,k)
                   ELSE
                      HybridMesh%IP_Pyr(1:4,HybridMesh%NB_Pyr) =   &
                           (/IP_Face(1,k),IP_Face(4,k),IP_Face(3,k),IP_Face(2,k)/)
                   ENDIF
                   HybridMesh%IP_Pyr(5,HybridMesh%NB_Pyr) = HybridMesh%NB_Point 

                ENDIF
             ENDDO
          ELSE
             !--- hang face
             IDs(1:4) = 0
             DO i=1,4
                p2Bit_Cell => BitreeMesh_GetNextCell(pBit_Cell,Dir,i)
                IF(p2Bit_Cell%Level(1)<=0) CALL Error_STOP ( '---- should not been boundary here')
                ID = p2Bit_Cell%ID
                DO j=1,i-1
                   if(ID==IDs(j)) ID = 0    !---- neighbout already being counted
                ENDDO
                IF(ID==0) CYCLE
                
                IDs(i) = ID
                ipe(1:4) = p2Bit_Cell%Nodes(iQuadFD2_Hex(:,-Dir))
                ip2 = (/ipe(1),ipe(3)/)

                CALL NodeNetTree_Search(2,ip2,QuadTree,iQuad)
                IF(iQuad==0) CALL Error_STOP ( 'hang face not set?')
                DO k = counter_Quad(iQuad-1)+1, counter_Quad(iQuad)
                   IF(IP_Face(4,k)==0)THEN
                      HybridMesh%NB_Tet = HybridMesh%NB_Tet + 1
                      IF(Dir<0)THEN
                         HybridMesh%IP_Tet(1:3,HybridMesh%NB_Tet) = IP_Face(1:3,k)
                      ELSE
                         HybridMesh%IP_Tet(1:3,HybridMesh%NB_Tet) =   &
                              (/IP_Face(1,k),IP_Face(3,k),IP_Face(2,k)/)
                      ENDIF
                      HybridMesh%IP_Tet(4,HybridMesh%NB_Tet) = HybridMesh%NB_Point 
                   ELSE
                      HybridMesh%NB_Pyr = HybridMesh%NB_Pyr + 1
                      IF(Dir<0)THEN
                         HybridMesh%IP_Pyr(1:4,HybridMesh%NB_Pyr) = IP_Face(1:4,k)
                      ELSE
                         HybridMesh%IP_Pyr(1:4,HybridMesh%NB_Pyr) =   &
                              (/IP_Face(1,k),IP_Face(4,k),IP_Face(3,k),IP_Face(2,k)/)
                      ENDIF
                      HybridMesh%IP_Pyr(5,HybridMesh%NB_Pyr) = HybridMesh%NB_Point 

                   ENDIF
                ENDDO
             ENDDO
          ENDIF

       ENDDO

    ENDDO

    DEALLOCATE(IP_Face, counter_Quad, noHang)
    CALL NodeNetTree_Clear(QuadTree)
    CALL BitreeMesh_getLeafCells(BitMesh)

    RETURN
  END SUBROUTINE BitreeMesh_getHybridMesh


  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE BitreeMesh_Cell_ClearNext(pBit_Cell)
    IMPLICIT NONE
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell, p2Bit_Cell

    IF(pBit_Cell%BiDir>0)THEN
       p2Bit_Cell => pBit_Cell%son1
       CALL BitreeMesh_Cell_ClearNext(p2Bit_Cell)
       p2Bit_Cell => pBit_Cell%son7
       CALL BitreeMesh_Cell_ClearNext(p2Bit_Cell)
    ENDIF
    NULLIFY(pBit_Cell%Left1,  pBit_Cell%Left4,  pBit_Cell%Left8,  pBit_Cell%Left5)
    NULLIFY(pBit_Cell%Right2, pBit_Cell%Right3, pBit_Cell%Right7, pBit_Cell%Right6)
    NULLIFY(pBit_Cell%Front1, pBit_Cell%Front5, pBit_Cell%Front6, pBit_Cell%Front2)
    NULLIFY(pBit_Cell%Back4,  pBit_Cell%Back8,  pBit_Cell%Back7,  pBit_Cell%Back3)
    NULLIFY(pBit_Cell%Down1,  pBit_Cell%Down2,  pBit_Cell%Down3,  pBit_Cell%Down4)
    NULLIFY(pBit_Cell%Up5,    pBit_Cell%Up6,    pBit_Cell%Up7,    pBit_Cell%Up8)

    RETURN
  END SUBROUTINE BitreeMesh_Cell_ClearNext

  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE BitreeMesh_Cell_ClearSon(pBit_Cell)
    IMPLICIT NONE
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell, p2Bit_Cell

    IF(pBit_Cell%BiDir==0) RETURN

    p2Bit_Cell => pBit_Cell%son1
    CALL BitreeMesh_Cell_ClearSon(p2Bit_Cell)
    p2Bit_Cell => pBit_Cell%son7
    CALL BitreeMesh_Cell_ClearSon(p2Bit_Cell)
    DEALLOCATE(pBit_Cell%Son1, pBit_Cell%Son7)
    pBit_Cell%BiDir = 0

    RETURN
  END SUBROUTINE BitreeMesh_Cell_ClearSon

  !>
  !!     Delete the Bitree mesh and release memory.
  !<
  SUBROUTINE BitreeMesh_Clear(BitMesh)
    IMPLICIT NONE
    TYPE (BitreeMesh_Type), INTENT(INOUT) :: BitMesh
    TYPE (BitreeMesh_Cell_Type), POINTER :: pBit_Cell
    INTEGER :: i,j,k,id

    IF( ASSOCIATED(BitMesh%LeafCells) )THEN
       DO id=1,BitMesh%numCells
          NULLIFY(BitMesh%LeafCells(id)%to)
       ENDDO
       DEALLOCATE(BitMesh%LeafCells)
    ENDIF

    DO k=1,BitMesh%kmax
       DO j=1,BitMesh%jmax
          DO i=1,BitMesh%imax
             pBit_Cell => BitMesh%Cells(i,j,k)
             CALL BitreeMesh_Cell_ClearNext(pBit_Cell)
          ENDDO
       ENDDO
    ENDDO
    DO k=1,BitMesh%kmax
       DO j=1,BitMesh%jmax
          DO i=1,BitMesh%imax
             pBit_Cell => BitMesh%Cells(i,j,k)
             CALL BitreeMesh_Cell_ClearSon(pBit_Cell)
          ENDDO
       ENDDO
    ENDDO

    BitMesh%numNodes = 0
    BitMesh%numCells = 0
    DEALLOCATE(BitMesh%Posit)
    DEALLOCATE(BitMesh%Cells)

    RETURN
  END SUBROUTINE BitreeMesh_Clear


END MODULE BitreeMesh


