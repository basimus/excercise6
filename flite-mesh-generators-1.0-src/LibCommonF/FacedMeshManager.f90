!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Face-based volume mesh storage, 
!!  including the defination of type and the basic functions.
!<
MODULE FacedMeshManager
  USE CellConnectivity
  USE FacedMeshStorage
  USE HybridMeshStorage
  USE NodeNetTree
  IMPLICIT NONE

CONTAINS

  SUBROUTINE FacedMesh_FromHybridMesh(FBMesh, HybMesh, Surf)
    IMPLICIT NONE

    TYPE(FacedMeshStorageType),   INTENT(OUT) :: FBMesh
    TYPE(HybridMeshStorageType),  INTENT(IN)  :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(IN), OPTIONAL :: Surf

    INTEGER ::  Mface3, Mface4, Mface, IFace, NB_Cell0, ITall, Isucc
    INTEGER ::  IT, i, j, IB, n1, n2, n, ip
    INTEGER :: ip3(3), ip4(4)
    TYPE(NodeNetTreeType) :: Face_Tree
    INTEGER :: icm(9) = (/1,4,5,2,8,10,3,9,6/)

    FBMesh%NB_Cell  = HybMesh%NB_Hex + HybMesh%NB_Prm + HybMesh%NB_Pyr + HybMesh%NB_Tet
    FBMesh%NB_Point = HybMesh%NB_Point

    Mface3 = 2*HybMesh%NB_Prm + 4*HybMesh%NB_Pyr + 4*HybMesh%NB_Tet
    Mface4 = 6*HybMesh%NB_Hex + 3*HybMesh%NB_Prm + 1*HybMesh%NB_Pyr
    Mface  = (Mface3 + Mface4)

    CALL allc_FacedMesh(FBMesh, Mface, HybMesh%NB_Point)

    FBMesh%Posit(:,1:FBMesh%NB_Point) = HybMesh%Posit(:,1:FBMesh%NB_Point)

    IF(PRESENT(Surf))THEN
       FBMesh%NB_BD_Cond = Surf%NB_Surf
       ALLOCATE(FBMesh%NB_Cond_Face(FBMesh%NB_BD_Cond)) 

       FBMesh%NB_Cond_Face(:) = 0
       DO IB = 1,Surf%NB_Tri
          n= Surf%IP_Tri(5,IB)
          IF(n<=0 .OR. n>Surf%NB_Surf) CALL Error_Stop(' FacedMesh_FromHybridMesh:: wrong n')
          FBMesh%NB_Cond_Face(n) = FBMesh%NB_Cond_Face(n) + 1
       ENDDO
       DO IB = 1,Surf%NB_Quad
          n= Surf%IP_Quad(5,IB)
          IF(n<=0 .OR. n>Surf%NB_Surf) CALL Error_Stop(' FacedMesh_FromHybridMesh:: wrong n')
          FBMesh%NB_Cond_Face(n) = FBMesh%NB_Cond_Face(n) + 1
       ENDDO
    ENDIF

    !--- build face tree for triangles

    CALL NodeNetTree_allocate(3, FBMesh%NB_Point, Mface3,  Face_Tree)

    !--- add triangular faces from hybrid mesh
    FBMesh%NB_Face  = 0
    NB_Cell0 = 0
    DO IT = 1, HybMesh%NB_Tet
       ITall = IT + NB_Cell0
       DO i=1,4
          ip3(1:3) = HybMesh%IP_Tet(iTri_Tet(:,i), IT)
          CALL NodeNetTree_SearchAdd( 3,ip3,Face_Tree,IFace)
          IF(IFace>FBMesh%NB_Face)THEN
             !--- a new Face
             FBMesh%NB_Face = IFace
             IF(HybMesh%GridOrder==1)THEN
                CALL IntQueue_Set(FBMesh%FaceNode(IFace), 3, ip3)
             ELSE
                do j = 1,9
                   ip = HybMesh%IP_Tet(iTri_C3Tet(icm(j),i), IT)
                   if(ip/=0) CALL IntQueue_Push(FBMesh%FaceNode(IFace), ip )
                enddo
             ENDIF
             FBMesh%Face_Right(IFace) = ITall
             FBMesh%Face_Left(IFace)  = -1
          ELSE
             !--- a old face
             IF(FBMesh%Face_Left(IFace)==-1)THEN
                FBMesh%Face_Left(IFace) = ITall
             ELSE
                WRITE(*,*)' Error--- : Tri face of Tet'
                WRITE(*,*)' IFace, FBMesh%Face_Right(IFace)/Left=',    &
                     IFace, FBMesh%Face_Right(IFace), FBMesh%Face_Left(IFace)
                CALL Error_Stop(' FacedMesh_FromHybridMesh:: tet triangle')
             ENDIF
          ENDIF
       ENDDO
    ENDDO

    NB_Cell0 = NB_Cell0 + HybMesh%NB_Tet

    DO IT = 1, HybMesh%NB_Pyr
       ITall = IT + NB_Cell0
       DO i=1,4
          ip3(:) = HybMesh%IP_Pyr(iTri_Pyr(:,i), IT)
          CALL NodeNetTree_SearchAdd( 3,ip3,Face_Tree,IFace)
          IF(IFace>FBMesh%NB_Face)THEN
             !--- a new Face
             FBMesh%NB_Face = IFace
             CALL IntQueue_Set(FBMesh%FaceNode(IFace), 3, ip3)
             FBMesh%Face_Right(IFace) = ITall
             FBMesh%Face_Left(IFace)  = -1
          ELSE
             !--- a old face
             IF(FBMesh%Face_Left(IFace)==-1)THEN
                FBMesh%Face_Left(IFace) = ITall
             ELSE
                WRITE(*,*)'Error--- : Tri face of Pyr'
                WRITE(*,*)' IFace, FBMesh%Face_Right(IFace)/Left=',    &
                     IFace, FBMesh%Face_Right(IFace), FBMesh%Face_Left(IFace)
                CALL Error_Stop(' FacedMesh_FromHybridMesh:: pyr triangle')
             ENDIF
          ENDIF
       ENDDO
    ENDDO

    NB_Cell0 = NB_Cell0 +HybMesh%NB_Pyr

    DO IT = 1, HybMesh%NB_Prm
       ITall = IT + NB_Cell0
       DO i=1,2
          ip3(:) = HybMesh%IP_Prm(iTri_Prm(:,i), IT)
          CALL NodeNetTree_SearchAdd( 3,ip3,Face_Tree,IFace)
          IF(IFace>FBMesh%NB_Face)THEN
             !--- a new Face
             FBMesh%NB_Face = IFace
             CALL IntQueue_Set(FBMesh%FaceNode(IFace), 3, ip3)
             FBMesh%Face_Right(IFace) = ITall
             FBMesh%Face_Left(IFace)  = -1
          ELSE
             !--- a old face
             IF(FBMesh%Face_Left(IFace)==-1)THEN
                FBMesh%Face_Left(IFace) = ITall
             ELSE
                WRITE(*,*)'Error--- : Tri face of Prm'
                WRITE(*,*)' IFace, FBMesh%Face_Right(IFace)/Left=',    &
                     IFace, FBMesh%Face_Right(IFace), FBMesh%Face_Left(IFace)
                CALL Error_Stop(' FacedMesh_FromHybridMesh:: prm triangle')
             ENDIF
          ENDIF
       ENDDO
    ENDDO

    NB_Cell0 = NB_Cell0 + HybMesh%NB_Prm

    DO IT = 1, HybMesh%NB_Hex
       ITall = IT + NB_Cell0
       DO i=1,8
          IF(HybMesh%IP_Hex(i,IT)==0)THEN
             ip3(:) = HybMesh%IP_Hex(iCut_Hex(:,i),IT)
             IF(ip3(1)==0 .OR. ip3(2)==0 .OR. ip3(3)==0) STOP '--- Fail at Read_Hybrid 3'
             CALL NodeNetTree_SearchAdd( 3,ip3,Face_Tree,IFace)
             IF(IFace>FBMesh%NB_Face)THEN
                !--- a new Face
                WRITE(*,*)'Error--- : Tri face of Hex, should not be new'
                CALL Error_Stop(' FacedMesh_FromHybridMesh:: hex cut triangle')
             ELSE
                !--- a old face
                IF(FBMesh%Face_Left(IFace)==-1)THEN
                   FBMesh%Face_Left(IFace) = ITall
                ELSE
                   WRITE(*,*)'Error--- : Tri face of Hex'
                   WRITE(*,*)' IFace, FBMesh%Face_Right(IFace)/Left=',    &
                        IFace, FBMesh%Face_Right(IFace), FBMesh%Face_Left(IFace)
                   CALL Error_Stop(' FacedMesh_FromHybridMesh:: hex cut triangle')
                ENDIF
             ENDIF
          ENDIF
       ENDDO
    ENDDO

    DO IT = 1, HybMesh%NB_Hex
       ITall = IT + NB_Cell0
       DO i=1,6
          ip4(:) = HybMesh%IP_Hex(iQuad_Hex(1:4,i), IT)
          n2 = 0
          DO n1=1,4
             IF(ip4(n1)>0)THEN
                n2 = n2+1
                IF(n2==4) EXIT
                ip3(n2) = ip4(n1)
             ENDIF
          ENDDO
          IF(n2/=3) CYCLE
          CALL NodeNetTree_SearchAdd( 3,ip3,Face_Tree,IFace)
          IF(IFace>FBMesh%NB_Face)THEN
             !--- a new Face
             FBMesh%NB_Face = IFace
             CALL IntQueue_Set(FBMesh%FaceNode(IFace), 3, ip3)
             FBMesh%Face_Right(IFace) = ITall
             FBMesh%Face_Left(IFace)  = -1
          ELSE
             !--- a old face
             IF(FBMesh%Face_Left(IFace)==-1)THEN
                FBMesh%Face_Left(IFace) = ITall
             ELSE
                WRITE(*,*)'Error--- : Tri face of Hex'
                WRITE(*,*)' IFace, FBMesh%Face_Right(IFace)/Left=',    &
                     IFace, FBMesh%Face_Right(IFace), FBMesh%Face_Left(IFace)
                CALL Error_Stop(' FacedMesh_FromHybridMesh:: hex triangle')
             ENDIF
          ENDIF
       ENDDO
    ENDDO

    !--- boundary triangles

    IF(PRESENT(Surf))THEN
       DO IB = 1,Surf%NB_Tri
          ip3(1:3) = Surf%IP_Tri(1:3,IB)
          CALL NodeNetTree_Search(3, ip3, Face_Tree, IFace)
          IF(IFace==0) CALL Error_Stop(' FacedMesh_FromHybridMesh:: IFace==0')
          IF(FBMesh%Face_Left(IFace)/=-1)THEN
              WRITE(*,*)' Error--- FBMesh%Face_Left(IFace)/=-1'
              WRITE(*,*)'      IB, left,right=',IB, FBMesh%Face_Left(IFace)
              CALL Error_Stop(' FacedMesh_FromHybridMesh:: Face_Left')
          ENDIF
          FBMesh%Face_Left(IFace) = - Surf%IP_Tri(5,IB)
       ENDDO
    ENDIF


    !--- add quad faces from hybrid mesh

    NB_Cell0 = HybMesh%NB_Tet
    MFace3   = FBMesh%NB_Face

    CALL NodeNetTree_Clear(Face_Tree)
    CALL NodeNetTree_allocate(4, FBMesh%NB_Point, MFace4, Face_Tree)

    DO IT = 1, HybMesh%NB_Pyr
       ITall = IT + NB_Cell0
       ip4(:) = HybMesh%IP_Pyr(1:4, IT)
       CALL NodeNetTree_SearchAdd( 4,ip4,Face_Tree,IFace)
       IFace = IFace + MFace3
       IF(IFace>FBMesh%NB_Face)THEN
          !--- a new Face
          FBMesh%NB_Face = IFace
          CALL IntQueue_Set(FBMesh%FaceNode(IFace), 4, ip4)
          FBMesh%Face_Right(IFace) = ITall
          FBMesh%Face_Left(IFace)  = -1
       ELSE
          !--- a old face
          IF(FBMesh%Face_Left(IFace)==-1)THEN
             FBMesh%Face_Left(IFace) = ITall
          ELSE
             WRITE(*,*)'Error--- : Quad face of Pyr'
             WRITE(*,*)' IFace, FBMesh%Face_Right(IFace)/Left=',    &
                  IFace, FBMesh%Face_Right(IFace), FBMesh%Face_Left(IFace)
             CALL Error_Stop(' FacedMesh_FromHybridMesh:: Pyt Quad')
          ENDIF
       ENDIF
    ENDDO

    NB_Cell0 = NB_Cell0 + HybMesh%NB_Pyr

    DO IT = 1, HybMesh%NB_Prm
       ITall = IT + NB_Cell0
       DO i=1,3
          ip4(:) = HybMesh%IP_Prm(iQuad_Prm(1:4,i), IT)
          CALL NodeNetTree_SearchAdd( 4,ip4,Face_Tree,IFace)
          IFace = IFace + MFace3
          IF(IFace>FBMesh%NB_Face)THEN
             !--- a new Face
             FBMesh%NB_Face = IFace
             CALL IntQueue_Set(FBMesh%FaceNode(IFace), 4, ip4)
             FBMesh%Face_Right(IFace) = ITall
             FBMesh%Face_Left(IFace)  = -1
          ELSE
             !--- a old face
             IF(FBMesh%Face_Left(IFace)==-1)THEN
                FBMesh%Face_Left(IFace) = ITall
             ELSE
                WRITE(*,*)'Error--- : Quad face of Prm'
                WRITE(*,*)' IFace, FBMesh%Face_Right(IFace)/Left=',    &
                     IFace, FBMesh%Face_Right(IFace), FBMesh%Face_Left(IFace)
                CALL Error_Stop(' FacedMesh_FromHybridMesh:: Prm Quad')
             ENDIF
          ENDIF
       ENDDO
    ENDDO

    NB_Cell0 = NB_Cell0 + HybMesh%NB_Prm

    DO IT = 1, HybMesh%NB_Hex
       ITall = IT + NB_Cell0
       DO i=1,6
          ip4(:) = HybMesh%IP_Hex(iQuad_Hex(1:4,i), IT)
          IF(ip4(1)==0 .OR. ip4(2)==0 .OR. ip4(3)==0 .OR. ip4(4)==0) CYCLE
          CALL NodeNetTree_SearchAdd( 4,ip4,Face_Tree,IFace)
          IFace = IFace + MFace3
          IF(IFace>FBMesh%NB_Face)THEN
             !--- a new Face
             FBMesh%NB_Face = IFace
             CALL IntQueue_Set(FBMesh%FaceNode(IFace), 4, ip4)
             FBMesh%Face_Right(IFace) = ITall
             FBMesh%Face_Left(IFace)  = -1
          ELSE
             !--- a old face
             IF(FBMesh%Face_Left(IFace)==-1)THEN
                FBMesh%Face_Left(IFace) = ITall
             ELSE
                WRITE(*,*)'Error--- : Quad face of Hex'
                WRITE(*,*)' IFace, FBMesh%Face_Right(IFace)/Left=',    &
                     IFace, FBMesh%Face_Right(IFace), FBMesh%Face_Left(IFace)
                CALL Error_Stop(' FacedMesh_FromHybridMesh:: Hex Quad')
             ENDIF
          ENDIF
       ENDDO
    ENDDO

    !--- boundary triangles

    IF(PRESENT(Surf))THEN
       DO IB = 1,Surf%NB_Quad
          ip4(1:4) = Surf%IP_Quad(1:4,IB)
          CALL NodeNetTree_Search(4, ip4, Face_Tree, IFace)
          IF(IFace==0) CALL Error_Stop(' FacedMesh_FromHybridMesh:: quad IFace==0 ')
          IF(FBMesh%Face_Left(IFace)/=-1) CALL Error_Stop(' FacedMesh_FromHybridMesh:: quad Face_Left')
          FBMesh%Face_Left(IFace) = - Surf%IP_Quad(5,IB)
       ENDDO
       
       CALL FacedMeshStorage_SortFace(FBMesh, Isucc)
    ENDIF

    WRITE(*,*)'Total number of faces  of hybrid: ',FBMesh%NB_Face
    CALL NodeNetTree_Clear(Face_Tree)

  END SUBROUTINE FacedMesh_FromHybridMesh

END MODULE FacedMeshManager



