!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



MODULE HighOrderCellManager

  USE HighOrderCell
  USE SurfaceMeshStorage
  USE Elasticity


CONTAINS


  !>
  !!  Give the positions of all cell nodes by linear interpolation from
  !!     the position of vertice.
  !!  @param[in]  HighCell
  !!  @param[in]  vertCoord  (:,numVertices)
  !!  @param[out] Pts        (:,numCellNodes)
  !<
  SUBROUTINE HighOrderCell_linearIsoparTransf( HighCell,vertCoord, Pts)
    TYPE(HighOrderCellType), INTENT(IN) :: HighCell
    REAL*8, DIMENSION(:,:), INTENT(IN)  :: vertCoord
    REAL*8, DIMENSION(:,:), INTENT(OUT) :: Pts
    INTEGER :: nsd, np, nd, k, i
    REAL*8  :: xi, eta, zeta
    REAL*8  :: sh(10000, 4)

    nsd = HighCell%Nsd
    nd  = HighCell%numVertices
    np  = HighCell%numCellNodes

    IF(SIZE(vertCoord,1)/=SIZE(Pts,1))  STOP 'HighOrderCell_linearIsoparTransf:: nsd'
    IF(nd>SIZE(vertCoord,2) )  STOP 'HighOrderCell_linearIsoparTransf:: nd'
    IF(np>SIZE(Pts,2) ) STOP 'HighOrderCell_linearIsoparTransf:: np '

    IF(nsd==1)THEN
       ! Reference interval is [-1,1]
       DO k = 1,np
          xi = HighCell%Coord(1,k)
          sh(k,1:2) = 0.5d0* (/ 1-xi, 1+xi /)
       ENDDO
    ELSE IF(nsd==2)THEN
       ! Reference triangle is [-1,-1; 1,-1; -1,1]
       DO k = 1,np
          xi  = HighCell%Coord(1,k)
          eta = HighCell%Coord(2,k)
          sh(k,1:3) = 0.5d0* (/ -xi-eta, 1+xi, 1+eta /)
       ENDDO
    ELSE IF(nsd==3)THEN
       ! Reference tetrahedron is [-1 -1 -1; 1 -1 -1; -1 1 -1; -1 -1 1]
       DO k = 1,np
          xi = HighCell%Coord(1,k)
          eta = HighCell%Coord(2,k)
          zeta = HighCell%Coord(3,k)
          sh(k,1:4) = 0.5d0* (/ -1-xi-eta-zeta, 1+xi, 1+eta, 1+zeta /)
       ENDDO
    ENDIF

    DO k = 1, np
       DO i = 1,SIZE(vertCoord,1)
          Pts(i,k) = SUM(sh(k,1:nd)*vertCoord(i,1:nd))
       ENDDO
    ENDDO

  END SUBROUTINE HighOrderCell_linearIsoparTransf


  !>
  !! Draw a triangalar element by a channel.
  !! @param[in]  Pts      the supporting points of a Cell
  !! @param[in]  N        the number of points along on each side.
  !! @param[in]  IO       the IO channel. 
  !! @param[in]  inside   =0 no inside detail (boundary only).   \n
  !!                      =1 with inside detail
  !<
  SUBROUTINE HighOrderCell_TriDisplay(HighCell, Pts, N, IO, inside)
    IMPLICIT NONE
    TYPE(HighOrderCellType), INTENT(IN) :: HighCell
    REAL*8,  INTENT(IN) :: Pts(:,:)
    INTEGER, INTENT(IN) :: N, IO, inside
    REAL*8, DIMENSION(SIZE(Pts,1)) :: P1
    REAL*8, DIMENSION(SIZE(Pts,1), 0:N, 0:N) :: p
    REAL*8   :: uv(2), ws(1000), fN
    INTEGER  :: i, j, k, m,  np

    IF(HighCell%Nsd/=2)THEN
       WRITE(*,*)' Error--- this is not tri. element, nsd=',HighCell%Nsd
       STOP 'HighOrderCell_TriDisplay::'
    ENDIF
    np  = HighCell%numCellNodes
    IF(np>SIZE(Pts,2) )   STOP 'HighOrderCell_TriDisplay: 2'

    fN = N/2.d0
    IF(inside==0)THEN

       DO i = 0, N
          uv(1) =  i*1.d0/fn - 1
          uv(2) = -1
          CALL HighOrderCell_calcShapeFunctions(HighCell, uv, ws)
          p1 = 0
          DO m = 1,np
             p1(:) = p1(:) + Pts(:,m)*ws(m)
          ENDDO
          WRITE(IO,*)REAL(p1)
       ENDDO
       WRITE(IO,*)' '
       WRITE(IO,*)' '
       DO j = 0, N
          uv(2) = j*1.d0/fn - 1
          uv(1) = -1
          CALL HighOrderCell_calcShapeFunctions(HighCell, uv, ws)
          p1 = 0
          DO m = 1,np
             p1(:) = p1(:) + Pts(:,m)*ws(m)
          ENDDO
          WRITE(IO,*)REAL(p1)
       ENDDO
       WRITE(IO,*)' '
       WRITE(IO,*)' ' 
       DO i = 0, N
          uv(1) = i*1.d0/fn - 1
          uv(2) = -uv(1)
          CALL HighOrderCell_calcShapeFunctions(HighCell, uv, ws)
          p1 = 0
          DO m = 1,np
             p1(:) = p1(:) + Pts(:,m)*ws(m)
          ENDDO
          WRITE(IO,*)REAL(p1)
       ENDDO

    ELSE

       DO i = 0, N
          uv(1) = i*1.d0/fn - 1
          DO j = 0, N-i
             uv(2) = j*1.d0/fn - 1
             CALL HighOrderCell_calcShapeFunctions(HighCell, uv, ws)
             p(:,i,j) = 0
             DO m = 1,np
                p(:,i,j) = p(:,i,j) + Pts(:,m)*ws(m)
             ENDDO
             WRITE(IO,*)REAL(p(:,i,j))
          ENDDO
          WRITE(IO,*)' '
       ENDDO
       WRITE(IO,*)' '
       WRITE(IO,*)' '
       DO j = 0, N
          DO i = 0, N-j
             WRITE(IO,*)REAL(p(:,i,j))
          ENDDO
          WRITE(IO,*)' '
       ENDDO
       WRITE(IO,*)' '
       WRITE(IO,*)' ' 
       DO k = 0, N
          DO i = 0, N-k
             j = N-k-i
             WRITE(IO,*)REAL(p(:,i,j))
          ENDDO
          WRITE(IO,*)' '
       ENDDO

    ENDIF

    WRITE(IO,*)' '
    WRITE(IO,*)' ' 

  END SUBROUTINE HighOrderCell_TriDisplay

  !>
  !! Draw a tetrahedral element by a channel.
  !! @param[in]  Pts      the supporting points of a Cell
  !! @param[in]  N        the number of points along on each side.
  !! @param[in]  IO       the IO channel. 
  !! @param[in]  inside   =0 no inside detail (boundary only).   \n
  !!                      =1 with inside detail
  !<
  SUBROUTINE HighOrderCell_TetDisplay(HighCell, Pts, N, IO, inside)
    IMPLICIT NONE
    TYPE(HighOrderCellType), INTENT(IN) :: HighCell
    REAL*8,  INTENT(IN) :: Pts(:,:)
    INTEGER, INTENT(IN) :: N, IO, inside
    REAL*8, DIMENSION(SIZE(Pts,1), 1000) :: pf
    INTEGER  :: k, np
    TYPE(HighOrderCellType) ::  HighFace

    IF(HighCell%Nsd/=3)THEN
       WRITE(*,*)' Error--- this is not tet. element, nsd=',HighCell%Nsd
       STOP 'HighOrderCell_TetDisplay::'
    ENDIF
    np  = HighCell%numCellNodes
    IF(np>SIZE(Pts,2) )   STOP 'HighOrderCell_TetDisplay: 2'

    HighFace = HighOrderCell_getHighCell(HighCell%GridOrder, HighCell%Nsd-1, HighCell%optionNodes)
    np  = HighFace%numCellNodes

    DO k = 1,4
       Pf(:,1:np) = Pts(:,HighCell%faceNodes(1:np,k))
       CALL HighOrderCell_TriDisplay(HighFace, Pf, N, IO, inside)
    ENDDO

  END SUBROUTINE HighOrderCell_TetDisplay


  !>
  !!   Split the faces of a Tet element into small linear triangle surface.
  !<
  SUBROUTINE HighOrderCell_TetSurface(HighCell, Pts, N, Scell)
    IMPLICIT NONE
    TYPE(HighOrderCellType), INTENT(IN) :: HighCell
    TYPE(SurfaceMeshStorageType), INTENT(OUT) :: Scell

    REAL*8,  INTENT(IN) :: Pts(3,*)
    INTEGER, INTENT(IN) :: N
    REAL*8   :: uv(3), ws(1000), pt(3), fn
    INTEGER  :: i, j, k, m, np, ip, ib
    INTEGER, DIMENSION(:,:,:), POINTER :: markp

    IF(HighCell%Nsd/=3)THEN
       WRITE(*,*)' Error--- this is not tet. element, nsd=',HighCell%Nsd
       STOP 'HighOrderCell_TetSurface::'
    ENDIF
    np  = HighCell%numCellNodes
    fn  = N/2.d0

    Scell%NB_Tri    = 4*N*N
    Scell%NB_Point  = 2*(N+1)*(N+2)-6*(N-1)-8
    Scell%NB_Seg    = 0
    Scell%NB_Surf   = 4
    Scell%NB_Sheet  = 0

    ALLOCATE(Scell%Posit(3,Scell%NB_Point))
    ALLOCATE(Scell%IP_Tri(5,Scell%NB_Tri))
    ALLOCATE(markp(0:N,0:N,0:N))

    ip = 0
    DO i = 0,N
       DO j = 0,N-i
          DO k = 0,N-i-j
             IF(i==0  .OR. j==0  .OR. k==0 .OR. i+j+k==N)THEN
                ip = ip + 1
                markp(i,j,k) = ip
                uv = (/ i*1.d0/fn-1, j*1.d0/fn-1, k*1.d0/fn-1 /)
                CALL HighOrderCell_calcShapeFunctions(HighCell, uv, ws)
                pt(:) = 0
                DO m = 1,np
                   pt(:) = pt(:) + Pts(:,m)*ws(m)
                ENDDO
                Scell%Posit(:,ip) = Pt(:)
             ENDIF
          ENDDO
       ENDDO
    ENDDO
    IF(ip/=Scell%NB_Point) STOP 'd23	if09ejafpeofkowe'

    ib = 0

    DO i = 0,0
       DO j = 0, N-i-1
          DO k = 0, N-i-j-1
             ib = ib + 1
             Scell%IP_Tri(1:3,ib) = (/ markp(i,j,k),markp(i,j+1,k),markp(i,j,k+1) /)
             Scell%IP_Tri(4:5,ib) = (/ 0, 1 /)
             IF(k==N-j-1) CYCLE
             ib = ib + 1
             Scell%IP_Tri(1:3,ib) = (/ markp(i,j,k+1),markp(i,j+1,k),markp(i,j+1,k+1) /)
             Scell%IP_Tri(4:5,ib) = (/ 0, 1 /)
          ENDDO
       ENDDO
    ENDDO

    DO i = 0, N-1
       DO j = 0,0
          DO k = 0, N-i-j-1
             ib = ib + 1
             Scell%IP_Tri(1:3,ib) = (/ markp(i,j,k),markp(i,j,k+1),markp(i+1,j,k) /)
             Scell%IP_Tri(4:5,ib) = (/ 0, 2 /)
             IF(k==N-i-j-1) CYCLE
             ib = ib + 1
             Scell%IP_Tri(1:3,ib) = (/ markp(i+1,j,k),markp(i,j,k+1),markp(i+1,j,k+1) /)
             Scell%IP_Tri(4:5,ib) = (/ 0, 2 /)
          ENDDO
       ENDDO
    ENDDO

    DO i = 0, N-1
       DO j = 0, N-i-1
          DO k = 0, 0
             ib = ib + 1
             Scell%IP_Tri(1:3,ib) = (/ markp(i,j,k),markp(i+1,j,k),markp(i,j+1,k) /)
             Scell%IP_Tri(4:5,ib) = (/ 0, 3 /)
             IF(j==N-i-1) CYCLE
             ib = ib + 1
             Scell%IP_Tri(1:3,ib) = (/ markp(i,j+1,k),markp(i+1,j,k),markp(i+1,j+1,k) /)
             Scell%IP_Tri(4:5,ib) = (/ 0, 3 /)
          ENDDO
       ENDDO
    ENDDO

    DO i = 0, N
       DO j = 0, N-i
          DO k = 1, N-i-j
             IF( i+j+k/=N) CYCLE
             ib = ib + 1
             Scell%IP_Tri(1:3,ib) = (/ markp(i,j,k),markp(i,j+1,k-1),markp(i+1,j,k-1) /)
             Scell%IP_Tri(4:5,ib) = (/ 0, 4 /)
             IF(k==1) CYCLE
             ib = ib + 1
             Scell%IP_Tri(1:3,ib) = (/ markp(i+1,j,k-1),markp(i,j+1,k-1),markp(i+1,j+1,k-2) /)
             Scell%IP_Tri(4:5,ib) = (/ 0, 4 /)
          ENDDO
       ENDDO
    ENDDO

    IF(ib/=Scell%NB_Tri) STOP 'fwer9t48u048gtuero'

    DEALLOCATE(markp)

  END SUBROUTINE HighOrderCell_TetSurface

  !======================================================================
  SUBROUTINE HighOrderCell_GaussJacobian(HighCell, Pts, JacMax, JacMin)
    IMPLICIT NONE
    TYPE(HighOrderCellType), INTENT(IN) :: HighCell
    REAL*8, DIMENSION(HighCell%Nsd, HighCell%numCellNodes), INTENT(IN) :: Pts
    REAL*8, INTENT(OUT) :: JacMax, JacMin
    REAL*8, DIMENSION(HighCell%Nsd, HighCell%Nsd) :: Ja
    REAL*8  :: GJacb
    INTEGER :: iGauss

    JacMax = -1.0D12
    JacMin =  1.0D12
    ! Loop on gauss points
    DO iGauss = 1, HighCell%numGaussNodes  
       Ja     = MATMUL(Pts, HighCell%GaussShapeDeriv(:,:,iGauss))
       IF(HighCell%Nsd==2)THEN
          GJacb = Ja(1,1)*Ja(2,2) - Ja(1,2)*Ja(2,1)
       ELSE
          GJacb = Geo3D_Matrix_Determinant(Ja)
       ENDIF
       JacMax = MAX(JacMax, GJacb)
       JacMin = MIN(JacMin, GJacb)
    ENDDO
  END SUBROUTINE HighOrderCell_GaussJacobian


  !======================================================================
  SUBROUTINE HighOrderCell_ElasticityMatrix2D(HighCell, Pts, Material, eMat, Isucc)
    IMPLICIT NONE
    TYPE(HighOrderCellType), INTENT(IN) :: HighCell
    REAL*8, INTENT(IN) :: Pts(2,HighCell%numCellNodes)
    TYPE(MaterialPar), INTENT(IN) :: Material
    REAL*8, DIMENSION(2*HighCell%numCellNodes,2*HighCell%numCellNodes), INTENT(OUT) :: eMat
    INTEGER, INTENT(OUT) :: Isucc
    REAL*8, DIMENSION(  HighCell%numCellNodes,  HighCell%numCellNodes) :: eMat11, eMat12, eMat22
    REAL*8, DIMENSION(  HighCell%numCellNodes,  HighCell%numCellNodes) :: NxNx, NxNy, NyNy
    REAL*8  :: weight, sWeight, muWeight, lambdaWeight
    INTEGER :: np, iGauss, i, j
    REAL*8, DIMENSION(HighCell%numCellNodes,2) :: sh
    REAL*8, DIMENSION(HighCell%numCellNodes)   :: Nxi, Neta, Nx, Ny
    REAL*8 :: Ja(2,2), detJ
    INTEGER, DIMENSION(HighCell%numCellNodes)  :: indx1, indx2

   Isucc  = 1
   np     = HighCell%numCellNodes
    
    eMat11 = 0
    eMat12 = 0
    eMat22 = 0

    ! Loop on gauss points
    DO iGauss = 1, HighCell%numGaussNodes  
       ! Shape functions and derivatives (reference element)

       sh(:,1:2) = HighCell%GaussShapeDeriv(:,1:2,iGauss)
       Nxi  = sh(:,1)
       Neta = sh(:,2)

       ! Jacobian of the isoparametric transformation
       !Ja = [Nxi*Pts(:,1)	Nxi*Pts(:,2)
       !     Neta*Pts(:,1)   Neta*Pts(:,2)]

       Ja   = MATMUL(Pts, sh)
       detJ = Ja(1,1)*Ja(2,2) - Ja(1,2)*Ja(2,1)
       IF (detJ<0)THEN
          Isucc = 0
          RETURN
       ENDIF

       ! Weight of the gauss point
       weight = detJ * HighCell%gaussWeights(iGauss)

       ! Shape functions derivatives (cartesian element)
       Ja = Inverte_Matrix2D(Ja)

       ! No necessary to invert!
       Nx = Ja(1,1)*Nxi + Ja(2,1)*Neta     !  Nx = (df1/dx, df2/dx, ..., dfn/dx)    f1,f2... shape functions
       Ny = Ja(1,2)*Nxi + Ja(2,2)*Neta     !  Ny = (df1/dy, df2/dy, ..., dfn/dy)

       ! Contribution to the elemental matrix
       sWeight      = weight* Material%S
       muWeight     = weight* Material%Mu
       lambdaWeight = weight* Material%Ld

       DO i = 1, NP
          DO j = 1, NP
             NxNx(i,j) = Nx(i)*Nx(j)
             NxNy(i,j) = Nx(i)*Ny(j)
             NyNy(i,j) = Ny(i)*Ny(j)
          ENDDO
       ENDDO

       eMat11 = eMat11 + sWeight*NxNx      + muWeight*NyNy
       eMat12 = eMat12 + lambdaWeight*NxNy + muWeight*TRANSPOSE(NxNy)
       eMat22 = eMat22 + sWeight*NyNy      + muWeight*NxNx
    ENDDO

    indx1 = (/(i, i=1,NP) /)
    indx2 = indx1 + NP
    eMat(indx1, indx1) = eMat11(:, :) 
    eMat(indx1, indx2) = eMat12(:, :)
    eMat(indx2, indx1) = TRANSPOSE(eMat12(:, :))
    eMat(indx2, indx2) = eMat22(:, :) 

  END SUBROUTINE HighOrderCell_ElasticityMatrix2D


  !======================================================================
  SUBROUTINE HighOrderCell_ElasticityMatrix3D(HighCell, Pts, Material, eMat, Isucc)
    IMPLICIT NONE
    TYPE(HighOrderCellType), INTENT(IN) :: HighCell
    REAL*8, INTENT(IN) :: Pts(3,HighCell%numCellNodes)
    TYPE(MaterialPar), INTENT(IN) :: Material
    REAL*8, DIMENSION(3*HighCell%numCellNodes,3*HighCell%numCellNodes), INTENT(OUT) :: eMat
    INTEGER, INTENT(OUT) :: Isucc
    REAL*8, DIMENSION(  HighCell%numCellNodes,  HighCell%numCellNodes) :: eMat11, eMat12, eMat13 
    REAL*8, DIMENSION(  HighCell%numCellNodes,  HighCell%numCellNodes) :: eMat22, eMat23, eMat33 
    REAL*8, DIMENSION(  HighCell%numCellNodes,  HighCell%numCellNodes) :: NxNx, NxNy, NxNz, NyNy, NyNz, NzNz
    REAL*8  :: weight, sWeight, muWeight, lambdaWeight
    INTEGER :: np, iGauss, i, j
    REAL*8, DIMENSION(HighCell%numCellNodes,3) :: sh
    REAL*8, DIMENSION(HighCell%numCellNodes)   :: Nxi, Neta, Nzet, Nx, Ny, Nz
    REAL*8 :: Ja(3,3), detJ
    INTEGER, DIMENSION(HighCell%numCellNodes)  :: indx1, indx2, indx3

    Isucc  = 1
    np     = HighCell%numCellNodes

    eMat11 = 0.d0
    eMat12 = 0.d0
    eMat13 = 0.d0
    eMat22 = 0.d0
    eMat23 = 0.d0
    eMat33 = 0.d0

    ! Loop on gauss points
    DO iGauss = 1, HighCell%numGaussNodes  
       ! Shape functions and derivatives (reference element)

       sh(:,1:3) = HighCell%GaussShapeDeriv(:,1:3,iGauss)    
       Nxi = sh(:,1)
       Neta = sh(:,2)
       Nzet = sh(:,3)


       ! Jacobian of the isoparametric transformation
       !Ja = [Nxi*Pts(:,1)	Nxi*Pts(:,2)	    Nxi*Pts(:,3)
       !     Neta*Pts(:,1)   Neta*Pts(:,2)	Neta*Pts(:,3)
       !     Nzet*Pts(:,1)   Nzet*Pts(:,2)	Nzet*Pts(:,3)]

       Ja   = MATMUL(Pts, sh)
       detJ = Geo3D_Matrix_Determinant(Ja)
       IF (detJ<0)THEN
          Isucc = 0
          RETURN
       ENDIF

       ! Weight of the gauss point
       weight = detJ * HighCell%gaussWeights(iGauss)

       ! Shape functions derivatives (cartesian element)
       Ja = Geo3D_Matrix_Inverse (Ja)

       ! No necessary to invert!
       Nx = Ja(1,1)*Nxi + Ja(2,1)*Neta + Ja(3,1)*Nzet     !  Nx = (df1/dx, df2/dx, ..., dfn/dx)    f1,f2... shape functions
       Ny = Ja(1,2)*Nxi + Ja(2,2)*Neta + Ja(3,2)*Nzet     !  Ny = (df1/dy, df2/dy, ..., dfn/dy)
       Nz = Ja(1,3)*Nxi + Ja(2,3)*Neta + Ja(3,3)*Nzet     !  Nz = (df1/dz, df2/dz, ..., dfn/dz)

       ! Contribution to the elemental matrix
       sWeight      = weight* Material%S
       muWeight     = weight* Material%Mu
       lambdaWeight = weight* Material%Ld

       DO i = 1, NP
          DO j = 1, NP
             NxNx(i,j) = Nx(i)*Nx(j)
             NxNy(i,j) = Nx(i)*Ny(j)
             NxNz(i,j) = Nx(i)*Nz(j)
             NyNy(i,j) = Ny(i)*Ny(j)
             NyNz(i,j) = Ny(i)*Nz(j)
             NzNz(i,j) = Nz(i)*Nz(j)
          ENDDO
       ENDDO

       eMat11 = eMat11 + sWeight*NxNx      + muWeight*(NyNy + NzNz)
       eMat12 = eMat12 + lambdaWeight*NxNy + muWeight*TRANSPOSE(NxNy)
       eMat13 = eMat13 + lambdaWeight*NxNz + muWeight*TRANSPOSE(NxNz)
       eMat22 = eMat22 + sWeight*NyNy      + muWeight*(NxNx + NzNz)
       eMat23 = eMat23 + lambdaWeight*NyNz + muWeight*TRANSPOSE(NyNz)
       eMat33 = eMat33 + sWeight*NzNz      + muWeight*(NxNx + NyNy)

    ENDDO

    indx1 = (/(i, i=1,NP) /)
    indx2 = indx1 + NP
    indx3 = indx2 + NP
    eMat(indx1, indx1) = eMat11(:, :) 
    eMat(indx1, indx2) = eMat12(:, :)
    eMat(indx1, indx3) = eMat13(:, :)
    eMat(indx2, indx1) = TRANSPOSE(eMat12(:, :))
    eMat(indx2, indx2) = eMat22(:, :) 
    eMat(indx2, indx3) = eMat23(:, :) 
    eMat(indx3, indx1) = TRANSPOSE(eMat13(:, :))
    eMat(indx3, indx2) = TRANSPOSE(eMat23(:, :)) 
    eMat(indx3, indx3) = eMat33(:, :)

  END SUBROUTINE HighOrderCell_ElasticityMatrix3D

END MODULE HighOrderCellManager
