!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


MODULE Elasticity

  TYPE :: MaterialPar
     REAL*8 :: E  = 1.d1          
     REAL*8 :: Nu = 0.4d0       
     REAL*8 :: Ld
     REAL*8 :: Mu
     REAL*8 :: S
  END TYPE MaterialPar
  
  CONTAINS
  
  !>
  !!  @param[in]  Material%E,Material%Nu
  !!  @param[out] Material%Ld,Material%Mu,Material%S
  !<
  SUBROUTINE MaterialPar_set(Material)
    TYPE(MaterialPar), INTENT(INOUT) :: Material
    Material%Ld = Material%E * Material%Nu / ((1+Material%Nu)*(1-2*Material%Nu))
    Material%Mu = Material%E / (2*(1+Material%Nu))
    Material%S  = Material%Ld + 2*Material%Mu
  END SUBROUTINE MaterialPar_set
  
END MODULE Elasticity


