!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!     Mesh Renumbering Subroutine.  
!!
!!     @param[in]  nn        : the number of nodes of each element.
!!     @param[in]  NB_Point  : the number of nodes of original numbering system.
!!     @param[in]  NB_Cell   : the number of elements.
!!     @param[in]  IP_Cell(nn,NB_Cell) : the array of elements.
!!     @param[out] map_Cell(NB_Cell)
!!                           : the mapping of element numbers from old to new.
!!     @param[out] map_Node(NB_Point) 
!!                           : the mapping of node numbers from old to new.
!!     @param[in,out]  npnew : the number of nodes of new numbering system
!!                             might NOT equal to NB_Point if the original
!!                             node numbering is not continual.
!!
!<
SUBROUTINE mesh_renumberer(nn,NB_Point,NB_Cell,IP_Cell,map_Cell,map_Node,npnew)
  USE PointAssociation
  IMPLICIT NONE
  INTEGER, INTENT(in)    :: nn, NB_Point, NB_Cell, IP_Cell(nn,*)
  INTEGER, INTENT(inout) :: map_Cell(*), map_Node(*), npnew

  INTEGER :: i,j,n,L,ie,nzp,nnp, ip1(1)
  INTEGER :: nbleast,ipleast,ipold,ipold2,nenew,actL, npfix
  INTEGER, DIMENSION(:), POINTER :: activ
  INTEGER, DIMENSION(:), POINTER :: nextlayer
  INTEGER, DIMENSION(:), POINTER :: CellList
  INTEGER, DIMENSION(:), POINTER :: map_bck
  INTEGER,               POINTER :: List_Length  
  TYPE (PointAssociationArray)   :: PointAsso

  ALLOCATE(nextlayer(NB_Cell), activ(NB_Cell))
  CALL PointAssociation_Allocate(NB_Point, PointAsso)

  npfix = npnew
  DO nnp = 1, npnew
     map_Node(nnp) = nnp
  ENDDO
  map_Node(npnew+1 : NB_Point) = 0

  !---- associate nodes with neighbour nodes

  DO ie = 1, NB_Cell
     DO i = 1, nn
        nzp = IP_Cell(i,ie)
        IF(map_Node(nzp)>0) CYCLE
        DO j = 1,nn
           IF(j==i)CYCLE
           ip1(1) = IP_Cell(j,ie)
           IF(map_Node(ip1(1))>0) CYCLE
           CALL PointAssociation_Add(1, nzp, ip1, PointAsso)
        ENDDO
     ENDDO
  ENDDO

  !--- find the node with least neighbour nodes

  nbleast = 10000
  ipleast = 0
  DO i = npnew+1,NB_Point
     CALL PointAssociation_List(i, List_Length, CellList, PointAsso)
     IF(List_Length>0 .AND. List_Length<nbleast)THEN
        nbleast = List_Length
        ipleast = i
     ENDIF
  ENDDO

  !---- Set the mapping of nodes

  ipold = ipleast
  Loop_ipold : DO WHILE(ipold>0)

     npnew = npnew+1
     map_Node(ipold) = npnew
     actL = 1
     activ(1) = ipold

     DO WHILE (actL>0)
        L = 0
        DO N = 1,actL
           ipold = activ(N)
           CALL PointAssociation_List(ipold, List_Length, CellList, PointAsso)
           DO j = 1,List_Length
              ipold2 = CellList(j)
              IF(map_Node(ipold2)==0)THEN
                 npnew = npnew+1
                 map_Node(ipold2) = npnew
                 L = L+1
                 nextlayer(L) = ipold2
              ENDIF
           ENDDO
        ENDDO
        actL = L
        activ(1:L) = nextlayer(1:L)
     ENDDO

     ipold = 0
     DO ie = 1,NB_Cell
        DO i = 1,nn
           IF(map_Node(IP_Cell(i,ie))==0)THEN
              ipold = IP_Cell(i,ie)
              CYCLE Loop_ipold
           ENDIF
        ENDDO
     ENDDO

  ENDDO Loop_ipold

  IF(npnew /=  NB_Point)THEN
     PRINT*,' npfix, npnew, NP_Point=',npfix,npnew,NB_Point
     PRINT*,'Error: Is the domain connected?'
     CALL Error_STOP ( 'in mesh_renumberer')
  ENDIF

  !---- Set the mapping of cells

  CALL PointAssociation_Clear(PointAsso)
  CALL PointAssociation_Build(nn, NB_Cell, IP_Cell, NB_Point, PointAsso)

  map_Cell(1:NB_Cell) = 0

  ALLOCATE(map_bck(NB_Point))
  DO nnp = npfix+1, NB_Point
     if(map_Node(nnp)>0)then
       map_bck(map_Node(nnp)) = nnp
     endif
  ENDDO
  
  nenew = 0
  DO nnp = npfix+1, npnew
     nzp = map_bck(nnp)
     CALL PointAssociation_List(nzp, List_Length, CellList, PointAsso)
     DO j = 1,List_Length
        ie = CellList(j) 
        IF(map_Cell(ie)==0)THEN
           nenew = nenew+1
           map_Cell(ie) = nenew
        ENDIF
     ENDDO
  ENDDO
  
  DO nnp = 1,npfix
     CALL PointAssociation_List(nnp, List_Length, CellList, PointAsso)
     DO j = 1,List_Length
        ie = CellList(j) 
        IF(map_Cell(ie)==0)THEN
           nenew = nenew+1
           map_Cell(ie) = nenew
        ENDIF
     ENDDO
  ENDDO

  IF(nenew /=  NB_Cell)THEN
     PRINT*,'nenew,NB_Cell = ',nenew,NB_Cell
     PRINT*,' npfix, NP_Point=',npfix,NB_Point
     PRINT*,'Error: Is the domain connected?'
     CALL Error_STOP ( 'in mesh_renumberer')
  ENDIF

  DEALLOCATE(nextlayer, activ, map_bck)
  CALL PointAssociation_Clear(PointAsso)

  RETURN
END SUBROUTINE mesh_renumberer

!>
!!     Face Based Mesh Renumbering Subroutine.  
!!
!!     @param[in]  NB_Node   : the number of nodes of original numbering system.
!!     @param[in]  NB_Face   : the number of faces.
!!     @param[in]  Faces     : the array of faces.
!!     @param[in]  npfix     : the first npfix nodes can not be renumbered.
!!     @param[in]  nbfix     : the first nbfix faces can not be renumbered.
!!
!!     @param[out] map_Face(NB_Face)
!!                           : the mapping of element numbers from old to new.
!!     @param[out] map_Node(NB_Node)
!!                           : the mapping of node numbers from old to new.
!<
SUBROUTINE face_renumberer(NB_Node,NB_Face,Faces,map_Face,map_Node,npfix,nbfix)
  USE PointAssociation
  USE Queue
  IMPLICIT NONE
  INTEGER, INTENT(in)    :: NB_Node, NB_Face, npfix, nbfix
  TYPE (IntQueueType), INTENT(in) :: Faces(*)
  INTEGER, INTENT(inout) :: map_Face(*), map_Node(*)

  INTEGER :: i,j,n,L,ip,ib,nzp,nnp, ip1(1)
  INTEGER :: nbleast,ipleast,ipold,ipold2,actL, npnew, nbnew
  INTEGER, DIMENSION(:), POINTER :: activ
  INTEGER, DIMENSION(:), POINTER :: nextlayer
  INTEGER, DIMENSION(:), POINTER :: CellList
  INTEGER,               POINTER :: List_Length  
  TYPE (PointAssociationArray)   :: PointAsso

  ALLOCATE(nextlayer(NB_Face), activ(NB_Face))
  CALL PointAssociation_Allocate(NB_Node, PointAsso)

  DO ip = 1, npfix
     map_Node(ip) = ip
  ENDDO
  DO ib = 1, nbfix
     map_Face(ib) = ib
  ENDDO
  map_Node(npfix+1 : NB_Node) = 0
  map_Face(nbfix+1 : NB_Face) = 0

  !---- associate nodes with neighbour nodes

  DO ib = 1, NB_Face
     DO i = 1, Faces(ib)%numNodes
        nzp = Faces(ib)%Nodes(i)
        IF(map_Node(nzp)>0) CYCLE
        DO j = 1,Faces(ib)%numNodes
           IF(j==i)CYCLE
           ip1(1) = Faces(ib)%Nodes(j)
           IF(map_Node(ip1(1))>0) CYCLE
           CALL PointAssociation_Add(1, nzp, ip1, PointAsso)
        ENDDO
     ENDDO
  ENDDO

  !--- find the node with least neighbour nodes

  nbleast = 10000
  ipleast = 0
  DO i = npfix+1, NB_Node
     CALL PointAssociation_List(i, List_Length, CellList, PointAsso)
     IF(List_Length<nbleast)THEN
        nbleast = List_Length
        ipleast = i
     ENDIF
  ENDDO

  !---- Set the mapping of nodes

  ipold = ipleast
  npnew = npfix
  Loop_ipold : DO WHILE(ipold>0)

     npnew = npnew+1
     map_Node(ipold) = npnew
     actL = 1
     activ(1) = ipold

     DO WHILE (actL>0)
        L = 0
        DO N = 1,actL
           ipold = activ(N)
           CALL PointAssociation_List(ipold, List_Length, CellList, PointAsso)
           DO j = 1,List_Length
              ipold2 = CellList(j)
              IF(map_Node(ipold2)==0)THEN
                 npnew = npnew+1
                 map_Node(ipold2) = npnew
                 L = L+1
                 nextlayer(L) = ipold2
              ENDIF
           ENDDO
        ENDDO
        actL = L
        activ(1:L) = nextlayer(1:L)
     ENDDO


     nbleast = 10000
     ipleast = 0
     DO i = 1, NB_Node
        IF(map_Node(i)>0) CYCLE
        CALL PointAssociation_List(i, List_Length, CellList, PointAsso)
        IF(List_Length<nbleast)THEN
           nbleast = List_Length
           ipleast = i
        ENDIF
     ENDDO
     ipold = ipleast

  ENDDO Loop_ipold

  IF(npnew/=NB_Node) CALL Error_STOP ( '--- face_renumberer 1')

  !---- Set the mapping of cells

  CALL PointAssociation_Clear(PointAsso)
  CALL PointAssociation_Allocate(NB_Node, PointAsso)

  !---- associate nodes with cells

  DO ib = nbfix+1, NB_Face
     DO i = 1, Faces(ib)%numNodes
        ip1(1) = Faces(ib)%Nodes(i)
        CALL PointAssociation_Add(1, ib, ip1, PointAsso)
     ENDDO
  ENDDO

  nbnew= nbfix
  DO ip = npfix+1, NB_Node
     CALL PointAssociation_List(ip, List_Length, CellList, PointAsso)
     DO j = 1,List_Length
        ib = CellList(j) 
        IF(map_Face(ib)==0)THEN
           nbnew = nbnew+1
           map_Face(ib) = nbnew
        ENDIF
     ENDDO
  ENDDO


  IF(nbnew<NB_Face)THEN
     DO ib = 1, NB_Face
        IF(map_Face(ib)==0)THEN
           nbnew = nbnew+1
           map_Face(ib) = nbnew
        ENDIF
     ENDDO
  ENDIF

  IF(nbnew/=NB_Face) CALL Error_STOP ( '--- face_renumberer 2')


  DEALLOCATE(nextlayer, activ)
  CALL PointAssociation_Clear(PointAsso)

  RETURN
END SUBROUTINE face_renumberer


