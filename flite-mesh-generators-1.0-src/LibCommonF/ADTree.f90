!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!    A tree structure for a tetrahedra mesh.
!!
!<
MODULE ADTreeModule
  USE Geometry3D
  USE array_allocator
  USE BiTreeNodeModule
  IMPLICIT NONE

  TYPE :: ADTreeParameter
     INTEGER :: maxLevel     = 360 
     INTEGER :: numDimension = 6
     REAL*8  :: toler        = 1.d-02
     REAL*8, DIMENSION(0:360) :: shiftAmount
     INTEGER, DIMENSION(:),  POINTER :: CellMapping => null()
     LOGICAL :: ready = .FALSE.
  END TYPE ADTreeParameter

  TYPE (ADTreeParameter), SAVE :: ADTreePar

  !>
  !!   The data structure of the tree.
  !<
  TYPE :: ADTreeType
     CHARACTER ( LEN = 60 ) :: theName = ' '
     REAL*8  :: Origin(3), ScaleFactor(3)
     TYPE (BiTreeNodeType), POINTER :: Root => null()
     INTEGER :: numPoints = 0
     INTEGER :: numCells  = 0
     REAL*8,  DIMENSION(:,:),  POINTER :: Posit => null()       !< (3,:)
     INTEGER, DIMENSION(:,:),  POINTER :: IPs => null()         !< (4,:)
  END TYPE ADTreeType

CONTAINS

  !>
  !!  Prepare the common Parameter.
  !<
  SUBROUTINE ADTreeParameter_Prepare( )
    IMPLICIT NONE
    REAL*8  :: xc
    INTEGER :: i, ip
    IF(ADTreePar%ready) RETURN
    xc = 0.5d0
    DO i = 0, ADTreePar%maxLevel / ADTreePar%numDimension - 1
       ip = i * ADTreePar%numDimension
       ADTreePar%shiftAmount(ip : ip+ADTreePar%numDimension-1) = xc
       xc = 0.5d0 * xc
    ENDDO
    ADTreePar%ready = .TRUE.
  END SUBROUTINE ADTreeParameter_Prepare

  !>
  !!    Renumber a node in the tree.
  !<
  RECURSIVE SUBROUTINE mappingTreeNodeCell(TreeNode)
    IMPLICIT NONE
    TYPE (BiTreeNodeType), POINTER :: TreeNode, node
    TreeNode%ID = ADTreePar%CellMapping(TreeNode%ID)
    node => TreeNode%branchLeft
    IF( .NOT. BiTreeNodeType_isEmpty(node) ) CALL mappingTreeNodeCell(node)
    node => TreeNode%branchRight
    IF( .NOT. BiTreeNodeType_isEmpty(node) ) CALL mappingTreeNodeCell(node)
    RETURN
  END SUBROUTINE mappingTreeNodeCell

  FUNCTION ADTree_getBox(ADTree, ip4) RESULT (box)
    IMPLICIT NONE
    TYPE (ADTreeType) :: ADTree
    INTEGER :: ip4(4)
    INTEGER :: i
    REAL*8  :: box(6)
    DO i=1,3
       box(i) = MIN( ADTree%Posit(i,ip4(1)),ADTree%Posit(i,ip4(2)),  &
            ADTree%Posit(i,ip4(3)),ADTree%Posit(i,ip4(4)) )
       box(i+3) = MAX( ADTree%Posit(i,ip4(1)),ADTree%Posit(i,ip4(2)),  &
            ADTree%Posit(i,ip4(3)),ADTree%Posit(i,ip4(4)) )
    ENDDO
  END FUNCTION ADTree_getBox

  FUNCTION ADTree_normalise(ADTree, Pt) RESULT (scale_Pt)
    IMPLICIT NONE
    TYPE (ADTreeType) :: ADTree
    REAL*8  :: Pt(3), scale_Pt(3)
    scale_Pt(1:3) = ( Pt(1:3) - ADTree%Origin(1:3) )    &
         * ADTree%ScaleFactor(1:3)
  END FUNCTION ADTree_normalise

  !>
  !!   Set a name for this tree (optional function).
  !<
  SUBROUTINE ADTree_SetName(ADTree, theName)
    IMPLICIT NONE
    TYPE (ADTreeType), INTENT(INOUT) :: ADTree
    CHARACTER ( LEN = * ), INTENT(IN) :: theName
    ADTree%theName(:) = ' '
    ADTree%theName = theName
  END SUBROUTINE ADTree_SetName
  !>
  !!   Build this tree.
  !!   @param[in]  NB_Point   the number of points to be stored in the tree.
  !!   @param[in]  Posit      the position of each point.
  !!   @param[in]  NB_Tet     the number of tetrahedra.
  !!   @param[in]  IP_Tet     the connectivity of each tetrahedron.
  !!   @param[out] ADTree     the tree.
  !<
  SUBROUTINE ADTree_SetTree(ADTree, Posit, IP_Tet, NB_Tet, NB_Point)
    IMPLICIT NONE

    TYPE (ADTreeType), INTENT(INOUT) :: ADTree
    REAL*8, INTENT(IN)  :: Posit(3,*)
    INTEGER, INTENT(IN) :: IP_Tet(4,*), NB_Tet, NB_Point
    REAL*8  :: pMin(3), pMax(3), xc
    INTEGER :: ip4(4), ip, it, i

    CALL ADTreeParameter_Prepare( )

    !--- set domain and scalar
    pMin(1:3) =  1.d+10
    pMax(1:3) = -1.d+10
    DO ip=1,NB_Point
       DO  i = 1,3
          pMin(i) = MIN(pMin(i),Posit(i,ip))
          pMax(i) = MAX(pMax(i),Posit(i,ip))
       ENDDO
    ENDDO

    DO  i = 1,3
       xc      = ( pMax(i) - pMin(i) ) * ADTreePar%toler
       pMin(i) = pMin(i) - xc
       pMax(i) = pMax(i) + xc
    ENDDO
    ADTree%Origin(1:3)      = pMin(1:3)
    ADTree%ScaleFactor(1:3) = 1.d0 / (pMax(1:3)-pMin(1:3))

    !--- set mesh for tree ADTree

    ADTree%numPoints = 1.1* NB_Point + 100000
    CALL allc_real8_2Dpointer( ADTree%Posit,  3, ADTree%numPoints, 'ADTree') 
    DO ip=1,NB_Point
       CALL ADTree_SetPoint(ADTree,IP,Posit(:,ip))
    ENDDO

    ALLOCATE(ADTree%Root)
    DO it=1,NB_Tet
       ip4(:) = IP_Tet(:,it)  
       CALL ADTree_AddNode(ADTree,it,ip4)
    ENDDO

    RETURN
  END SUBROUTINE ADTree_SetTree
  !>
  !!   Get the minimum and maximum corners of the domain.
  !!   @param[in]  ADTree  the tree
  !!   @param[out] XYZMin  the minimum coordinate of the domain.
  !!   @param[out] XYZMax  the maximum coordinate of the domain.
  !<
  SUBROUTINE ADTree_GetCorner(ADTree, XYZMin, XYZMax)
    IMPLICIT NONE
    TYPE (ADTreeType), INTENT(IN) :: ADTree
    REAL*8, INTENT(OUT)  :: XYZMin(3), XYZMax(3)
    XYZMin(1:3) = ADTree%Origin(1:3)
    XYZMax(1:3) = XYZMin(1:3) + 1.d0 / ADTree%ScaleFactor(1:3)
    RETURN
  END SUBROUTINE ADTree_GetCorner
  !>
  !!   Add/Set a new point to the tree.
  !!   @param[in] IP     the point ID  (new or old).
  !!   @param[in] Pt     the position of the point.
  !!   @param[in,out] ADTree  the tree
  !<
  SUBROUTINE ADTree_SetPoint(ADTree,IP,Pt)
    IMPLICIT NONE
    TYPE (ADTreeType), INTENT(INOUT) :: ADTree
    INTEGER, INTENT(IN) :: IP
    REAL*8, INTENT(IN)  :: Pt(3)
    IF(ADTree%numPoints<IP)THEN
       ADTree%numPoints = 1.1* IP + 100000
       CALL allc_real8_2Dpointer( ADTree%Posit,  3, ADTree%numPoints, 'ADTree') 
    ENDIF
    ADTree%Posit(:,IP) = Pt  
    RETURN
  END SUBROUTINE ADTree_SetPoint
  !>
  !!   Add a new cell to the tree.
  !!   @param[in]  it      the cell ID.
  !!   @param[in]  ip4     the connectivity of this cell.
  !!   @param[in,out] ADTree  the tree
  !<
  SUBROUTINE ADTree_AddNode(ADTree, it,ip4)
    IMPLICIT NONE

    TYPE (ADTreeType), INTENT(INOUT) :: ADTree
    INTEGER, INTENT(IN) :: it,ip4(4)
    REAL*8  :: box(6), x(6)
    INTEGER :: id, lev
    TYPE (BiTreeNodeType), POINTER :: node

    box(1:6) = ADTree_getBox(ADTree, ip4)
    x(1:3)   = ADTree_normalise(ADTree,box(1:3))
    x(4:6)   = ADTree_normalise(ADTree,box(4:6))
    DO id = 1,ADTreePar%numDimension
       IF(x(id)<=0.d0.OR.x(id)>=1.d0) CALL Error_STOP ( 'out of ADTree domain')
    ENDDO

    node => ADTree%Root
    lev  =  0
    DO WHILE( .NOT. BiTreeNodeType_isEmpty(node) )
       !---   walk way down the tree
       lev   = lev + 1
       id    = MOD(lev,ADTreePar%numDimension) +1
       x(id) = 2.d0*x(id)
       IF(x(id)<1.d0) THEN
          node => node%branchLeft
       ELSE
          node => node%branchRight
          x(id) = x(id)-1.d0
       ENDIF
    ENDDO

    !---  add a new tree-node

    CALL BiTreeNodeType_Assign(node,it,lev)
    
    IF(it>ADTree%numCells)THEN
        ADTree%numCells = 1.1*it + 100000
        CALL allc_2Dpointer(ADTree%IPs, 4, ADTree%numCells, 'ADTree')
    ENDIF
    ADTree%IPs(:,it) = ip4(:)

    IF(lev>ADTreePar%maxLevel)THEN
       WRITE(*,*)'Error---  ADTreePar%maxLevel is set too small:', ADTreePar%maxLevel
       CALL Error_STOP ( '--- ADTree_AddNode')
    ENDIF

    RETURN
  END SUBROUTINE ADTree_AddNode
  !>
  !!   Remove a cell from the tree.
  !!   @param[in,out] ADTree  the tree
  !!   @param[in] ip4     4 points composing a cell.
  !!                    If this cell does not exist in the tree,
  !!                    the program will be terminated.  
  !!
  !!   Reminder:  the 4 points must take the same order as it did
  !!              when the cell was added to the tree.
  !<
  SUBROUTINE ADTree_RemoveNode(ADTree, ip4)
    IMPLICIT NONE

    TYPE (ADTreeType), INTENT(INOUT) :: ADTree
    INTEGER, INTENT(IN) :: ip4(4)
    REAL*8  :: box(6), x(6)
    INTEGER :: id, lev
    TYPE (BiTreeNodeType), POINTER :: node, node0

    box(1:6) = ADTree_getBox(ADTree, ip4)
    x(1:3)   = ADTree_normalise(ADTree,box(1:3))
    x(4:6)   = ADTree_normalise(ADTree,box(4:6))

    !--- find the node to delete

    node => ADTree%Root
    DO WHILE( .NOT. BiTreeNodeType_isEmpty(node) )
       IF(  ip4(1)==ADTree%IPS(1,node%ID) .AND. ip4(2)==ADTree%IPS(2,node%ID) .AND.   &
            ip4(3)==ADTree%IPS(3,node%ID) .AND. ip4(4)==ADTree%IPS(4,node%ID) ) THEN
          ! ---  found node to be deleted
          EXIT
       ELSE
          !---   walk way down the tree
          lev   = node%Level + 1
          id    = MOD(lev,ADTreePar%numDimension) +1
          x(id) = 2.d0*x(id)
          IF(x(id)<1.d0) THEN
             node => node%branchLeft
          ELSE
             node => node%branchRight
             x(id) = x(id)-1.d0
          ENDIF
       ENDIF
    ENDDO

    IF( BiTreeNodeType_isEmpty(node) )THEN
       WRITE(*,*)'Error---  can not find this Node, ip4=', ip4
       CALL Error_STOP ( '--- ADTree_RemoveNode')
    ENDIF

    !--- Find a node on the tip, 
    !    copy information of this node to the node being deleted,
    !    and delete the node on tip.

    node0 => node
    DO
       IF( .NOT. BiTreeNodeType_isEmpty(node%branchLeft))THEN
          node => node%branchLeft
       ELSE IF( .NOT. BiTreeNodeType_isEmpty(node%branchRight))THEN
          node => node%branchRight
       ELSE 
          EXIT
       ENDIF
    ENDDO

    node0%ID   = node%ID  
    CALL BiTreeNodeType_deAssign(node)

    RETURN
  END SUBROUTINE ADTree_RemoveNode
  !>
  !!   Search for a cell in which a point lies.
  !!   @param[in]  ADTree    :  the tree.
  !!   @param[in]  Pt        :  the position of the point.
  !!   @param[in]  Isucc = 1 :  if fail, stop.  \n
  !!                     = 0 :  if fail, return the closest element with warning.  \n
  !!                     =-1 :  if fail, return the closest element without warning.
  !!   @param[out]  it       :  the cell in which the point Pt lies or most close. \n
  !!                     = -1, Pt is out of the domain.
  !!   @param[out]  ip4      :  the 4 points the cell.
  !!   @param[out]  weights  :  the weight of the point Pt in term of the 4 vertice
  !!                            of the cell (dimensionless value with sum(weights)=1).
  !!   @param[out]  Isucc = 1 :  succeed.                          \n
  !!                      = 2 :  almost succeed with tolerance.    \n
  !!                      = 0 :  fail, return the closest element (if not =-1).
  !<
  SUBROUTINE ADTree_SearchNode(ADTree,Pt,it,ip4,weights, Isucc)
    IMPLICIT NONE

    TYPE (ADTreeType), INTENT(IN) :: ADTree
    INTEGER, INTENT(OUT)  :: it,ip4(4)
    REAL*8,  INTENT(IN)   :: Pt(3)
    REAL*8,  INTENT(OUT)  :: weights(4)
    INTEGER, INTENT(INOUT):: Isucc
    INTEGER :: istk, nn, id, lev, ips(4), its
    REAL*8  :: amov,ac,am
    REAL*8  :: scale_Pt(6), xl(6), box(6), pp4(3,4), w(4)
    LOGICAL :: secondSearch, inBoxOnce
    TYPE (BiTreeNodeType), POINTER :: node  

    TYPE :: clueNode
       TYPE (BiTreeNodeType), POINTER :: node => null()
       REAL*8  :: box(6)
    END TYPE clueNode
    TYPE (clueNode), DIMENSION(ADTreePar%maxLevel) :: clue

    scale_Pt(1:3) = ADTree_normalise(ADTree, Pt)
    scale_Pt(4:6) = scale_Pt(1:3)
    IT = -1
    DO id = 1,3
       IF(scale_Pt(id)<=0.d0 .OR. scale_Pt(id)>=1.d0)THEN
          IF(Isucc==1)THEN
             WRITE(*,*)'ADTree Name: ',ADTree%theName
             WRITE(*,*)'Pt=',Pt, 'scale_Pt=',scale_Pt(1:3)
             WRITE(*,*)'Error--- Point is out of ADTree domain, search fail'
             CALL Error_STOP ( '--- ADTree_SearchNode')
          ELSE IF(Isucc==-1)THEN
          ELSE
             WRITE(*,*)'ADTree Name: ',ADTree%theName
             WRITE(*,*)'Pt=',Pt, 'scale_Pt=',scale_Pt(1:3)
             WRITE(*,*)'Warning--- Point is out of ADTree domain, search fail'
          ENDIF
          Isucc = 0
          RETURN
       ENDIF
    ENDDO

    ac           = -1.d+6
    weights(1:4) = 0.d0
    xl(1:6)      = 0.d0
    secondSearch = .FALSE.
    inBoxOnce    = .FALSE.
    node => ADTree%Root
    istk = 0
    DO 

       IF( BiTreeNodeType_isEmpty(node) ) THEN
          IF(istk==0)THEN
             !---no element found containing the point
             IF(Isucc==1)THEN
                WRITE(*,*)' '
                WRITE(*,*)'ADTree Name: ',ADTree%theName
                WRITE(*,*)'Pt=',Pt, 'scale_Pt=',scale_Pt(1:3)
                WRITE(*,*)'Error--- No ADTree cell found, search fail'
                CALL Error_STOP ( '--- ADTree_SearchNode')
             ENDIF

             IF(inBoxOnce)THEN
                !--- at lease one element has been check, it should be the closest one
                IF(Isucc==-1)THEN
                ELSE
                   WRITE(*,*)' '
                   WRITE(*,*)'ADTree Name: ',ADTree%theName
                   WRITE(*,*)'Pt=',Pt, 'scale_Pt=',scale_Pt(1:3)
                   WRITE(*,*)'Warning--- No ADTree cell found, return the closest one'
                ENDIF
                Isucc = 0
                RETURN
             ELSE
                !--- no element has been check
                !--- restart search from the root, check every node on the clue
                secondSearch = .TRUE.
                node => ADTree%Root
                istk = 0
                xl(1:6) = 0.d0
                CYCLE
             ENDIF
          ENDIF

          node    => clue(istk)%node%branchRight
          xl(1:6) =  clue(istk)%box(1:6)
          istk    =  istk-1
          IF( .NOT. BiTreeNodeType_isEmpty(node) ) THEN
             lev    = node%Level
             id     = MOD(lev,ADTreePar%numDimension) +1
             amov   = ADTreePar%shiftAmount(lev)
             xl(id) = xl(id)+amov
             IF(id <= 3) THEN
                IF(xl(id)      > scale_Pt(id)) node => emptyNode
             ELSE
                IF(xl(id)+amov < scale_Pt(id)) node => emptyNode
             ENDIF
          ENDIF
          
       ELSE
       
          !--- visit 'node'
          its      = node%ID
          ips(1:4) = ADTree%IPs(:,its)
          box(1:6) = ADTree_getBox(ADTree, ips)

          IF( secondSearch .OR. Geo3D_InsideBox2(Pt(1:3), box, 1.d-12) )THEN
             inBoxOnce = .TRUE.
             nn = 1
             pp4(:,1:4) = ADTree%Posit(:,ips(1:4))
             w(1:4)     = Geo3D_Tet_Weight_high(nn,pp4,Pt)
             am = MIN(w(1),w(2),w(3),w(4))
             IF(am > ac) THEN
                ac = am
                IT         = its
                ip4(:)     = ips(:)
                weights(:) = w(:)
             ENDIF
             IF(ac >= 0.d0)THEN
                Isucc = 1
                RETURN
             ELSE IF(secondSearch .AND. ac >= -1.0d-12)THEN
                Isucc = 2
                RETURN   
             ENDIF
          ENDIF

          istk                = istk+1
          clue(istk)%node     => node
          clue(istk)%box(1:6) = xl(1:6)
          node => node%branchLeft
          IF( .NOT. BiTreeNodeType_isEmpty(node) ) THEN
             lev  = node%Level
             id   = MOD(lev,ADTreePar%numDimension) +1
             amov = ADTreePar%shiftAmount(lev)
             IF(id <= 3) THEN
                IF(xl(id)      > scale_Pt(id)) node => emptyNode
             ELSE
                IF(xl(id)+amov < scale_Pt(id)) node => emptyNode
             ENDIF
          ENDIF
       ENDIF

    ENDDO

  END SUBROUTINE ADTree_SearchNode
  !>
  !!   Delete the tree and release the memory.
  !<
  SUBROUTINE ADTree_Clear(ADTree)
    IMPLICIT NONE
    TYPE (ADTreeType), INTENT(INOUT) :: ADTree
    ADTree%numPoints = 0
    ADTree%numCells  = 0
    IF( ASSOCIATED(ADTree%Posit)       ) DEALLOCATE( ADTree%Posit ) 
    IF( ASSOCIATED(ADTree%IPs)         ) DEALLOCATE( ADTree%IPs ) 
    IF( .NOT. ASSOCIATED(ADTree%Root)  ) RETURN
    IF( .NOT. BiTreeNodeType_isEmpty(ADTree%Root) ) CALL BiTreeNodeType_Delete( ADTree%Root )
    RETURN
  END SUBROUTINE ADTree_Clear
  !>
  !!   Change the cell ID with a Mapping array. \n
  !!   Reminder:  the mapping must be injective.
  !!   @param[in]  ADTree    :  the tree.
  !!   @param[in]  Mapping   :  the mapping array. Mapping(oldCell) = newCell with newCell<=oldCell.
  !!                            if newCell<=0, this is a cell being removed.
  !!   @param[in]  NB_Cell   :  the number of cells.
  !!   @param[out] ADTree    :  the revised tree.
  !<
  SUBROUTINE ADTree_MappingCells(ADTree,Mapping,NB_Cell)
    IMPLICIT NONE
    TYPE (ADTreeType) :: ADTree
    INTEGER, INTENT(IN) :: Mapping(*), NB_Cell
    INTEGER :: it, itnew
    CALL allc_int_1Dpointer( ADTreePar%CellMapping, NB_Cell, 'ADTree') 
    ADTreePar%CellMapping(1:NB_Cell) = Mapping(1:NB_Cell)
    CALL MappingTreeNodeCell( ADTree%Root )
    IF(NB_Cell>ADTree%numCells) CALL Error_Stop('--- ADTree_MappingCells 1')
    DO it = 1, NB_Cell
       itnew = Mapping(it)
       IF(itnew>it) CALL Error_Stop('--- ADTree_MappingCells 2')
       IF(itnew<=0) CYCLE
       ADTree%IPs(:,itnew) = ADTree%IPs(:,it)
    ENDDO
    DEALLOCATE( ADTreePar%CellMapping )
    RETURN
  END SUBROUTINE ADTree_MappingCells
  !>
  !!  Output the information of the AD-Tree
  !!  @param[in] ADTree  : the tree.
  !!  @param[in] io      : the IO channel. 
  !!                       If =6 or not present, output on screen. 
  !!  @param[out] isReady : if the tree is ready.
  !<
  SUBROUTINE ADTree_Info(ADTree, io, isReady)
    IMPLICIT NONE
    TYPE (ADTreeType), INTENT(IN) :: ADTree
    INTEGER, INTENT(IN),  OPTIONAL :: io
    LOGICAL, INTENT(OUT), OPTIONAL :: isReady
    INTEGER :: ic
    ic = 6
    IF(PRESENT(io)) ic = io
    WRITE(ic,*)'  Information for ADTree '
    WRITE(ic,*)'     Name:          ', TRIM(ADTree%theName)
    IF(.NOT. ASSOCIATED(ADTree%Root))THEN
       IF(PRESENT(isReady)) isReady = .FALSE.
       WRITE(ic,*)'  The tree has never been set!'
       RETURN
    ELSE IF(BiTreeNodeType_isEmpty (ADTree%Root))THEN
       IF(PRESENT(isReady)) isReady = .FALSE.
       WRITE(ic,*)'  The tree is empty!'
       RETURN
    ENDIF
    WRITE(ic,*)'     Origin:        ', REAL(ADTree%Origin)
    WRITE(ic,*)'     ScaleFactor:   ', REAL(ADTree%ScaleFactor)
    IF(PRESENT(isReady)) isReady = .TRUE.
    RETURN
  END SUBROUTINE ADTree_Info

END MODULE ADTreeModule

!>
!!   Another name of module ADTreeModule
!<
MODULE ADTreeAvailables
  USE ADTreeModule
END MODULE ADTreeAvailables

