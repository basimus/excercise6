!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


MODULE BiTreeNodeModule
  IMPLICIT NONE
  
  TYPE :: BiTreeNodeType
     INTEGER :: ID    = 0
     INTEGER :: Level = -1
     TYPE (BiTreeNodeType), POINTER :: branchLeft => null()
     TYPE (BiTreeNodeType), POINTER :: branchRight => null()
  END TYPE BiTreeNodeType

  TYPE (BiTreeNodeType), TARGET,SAVE :: emptyNode

CONTAINS

  !>
  !!    Set ID and level for a node, and allocate its branches.
  !<
  SUBROUTINE BiTreeNodeType_Assign(TreeNode,ID,Level)
    IMPLICIT NONE
    INTEGER :: ID, Level
    TYPE (BiTreeNodeType), POINTER :: TreeNode
    TreeNode%ID    = ID
    TreeNode%Level = Level
    ALLOCATE(TreeNode%branchLeft)
    ALLOCATE(TreeNode%branchRight)
    RETURN
  END SUBROUTINE BiTreeNodeType_Assign
  !>
  !!    Unset ID and level for a node, and allocate its branches.
  !!    The node become an empty node thereafter.
  !<
  SUBROUTINE BiTreeNodeType_deAssign(TreeNode)
    IMPLICIT NONE
    TYPE (BiTreeNodeType), POINTER :: TreeNode
    TreeNode%ID    = 0
    TreeNode%Level = -1
    DEALLOCATE(TreeNode%branchLeft)
    DEALLOCATE(TreeNode%branchRight)
    RETURN
  END SUBROUTINE BiTreeNodeType_deAssign
  !>
  !!    Delete a node from the tree.
  !<
  RECURSIVE SUBROUTINE BiTreeNodeType_Delete(TreeNode)
    IMPLICIT NONE
    TYPE (BiTreeNodeType), POINTER :: TreeNode, node
    node => TreeNode%branchLeft
    IF( .NOT. BiTreeNodeType_isEmpty(node) ) CALL BiTreeNodeType_Delete(node)
    node => TreeNode%branchRight
    IF( .NOT. BiTreeNodeType_isEmpty(node) ) CALL BiTreeNodeType_Delete(node)
    DEALLOCATE(TreeNode%branchLeft)
    DEALLOCATE(TreeNode%branchRight)
    RETURN
  END SUBROUTINE BiTreeNodeType_Delete
  !>
  !!   Check if a node is an empty node.
  !!   An empty node is a allocatted pointer 
  !!      before being assigned ( see BiTreeNodeType_Assign() ).
  !<
  FUNCTION BiTreeNodeType_isEmpty(TreeNode) RESULT (TF)
    IMPLICIT NONE
    TYPE (BiTreeNodeType), POINTER :: TreeNode
    LOGICAL :: TF
    IF(TreeNode%Level==-1)THEN
       TF = .TRUE.
    ELSE
       TF = .FALSE.
    ENDIF
    RETURN
  END FUNCTION BiTreeNodeType_isEmpty


END MODULE BiTreeNodeModule
