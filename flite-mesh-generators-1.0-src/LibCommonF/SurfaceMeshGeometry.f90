!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Supporting information for a Surface mesh.
!<
MODULE SurfaceMeshGeometry
  USE SurfaceMeshStorage
  USE Geometry3D
  IMPLICIT NONE

  INTEGER, PARAMETER :: MaxNumParts = 10

  !>   information for a surface triangle.
  TYPE :: SurfaceTriangleType
     REAL*8 :: anor(3)   !<   The normal direction.
     REAL*8 :: Area      !<   the area. 
  END TYPE SurfaceTriangleType

  !>   information for a surface node.
  TYPE :: SurfaceNodeType     
     !>  onRidge=1  this node is a ridge node;
     !!         =0  this node is not a ridge node.
     !!         =-1 this node is not a ridge node, but it is at the end of a ridge.
     INTEGER :: onRidge  = 0
     
     !>  movable=.false.  this node can not be moved (a corner node or a end of a ridge).
     !!         =.true.   this node can be moved
     LOGICAL :: movable = .TRUE.

     !>  the number of parts of this ridge node.
     INTEGER :: numParts = 1
     
     !>  the list of triangles in each part.
     TYPE(IntQueueType) :: PartTri(MaxNumParts)
     
     !>  the normal direction of each part.
     REAL*8             :: Anor(3, MaxNumParts)
  END TYPE SurfaceNodeType

CONTAINS


  !>  Allocate memory.
  SUBROUTINE allc_SurfTri(SurfTri, NB_Tri)
    IMPLICIT NONE
    TYPE(SurfaceTriangleType), DIMENSION(:), POINTER :: SurfTri
    TYPE(SurfaceTriangleType), DIMENSION(:), POINTER :: b
    INTEGER, INTENT(IN) :: NB_Tri
    INTEGER :: nsize, i
    INTEGER :: ierr1=0, ierr2=0, ierr3=0, ierr4=0
    LOGICAL :: NotNewPointer

    NotNewPointer = .FALSE.
    IF(ASSOCIATED(SurfTri))THEN
       nsize = SIZE(SurfTri)
       IF(nsize>=NB_Tri) RETURN
       NotNewPointer = .TRUE.
       ALLOCATE(b(nsize),stat=ierr1)
       DO i = 1,nsize
          b(i) = SurfTri(i)
       ENDDO
       DEALLOCATE(SurfTri,stat=ierr2)
    ENDIF

    ALLOCATE(SurfTri(NB_Tri + Surf_allc_increase),stat=ierr3)

    IF(NotNewPointer)THEN
       DO i = 1,nsize
          SurfTri(i) = b(i)
       ENDDO
       DEALLOCATE(b,stat=ierr4)
    ENDIF

    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0.OR.ierr4/=0)THEN
       WRITE(*,*) ' ALLOCATION_SurfTri ERROR: ierr=',ierr1,ierr2,ierr3,ierr4
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', NB_Tri
       CALL Error_STOP ('allc_SurfTri ')
    ENDIF

    RETURN
  END SUBROUTINE allc_SurfTri


  !>  Allocate memory.
  SUBROUTINE allc_SurfNode(SurfNode, NB_Point)
    IMPLICIT NONE
    TYPE(SurfaceNodeType), DIMENSION(:), POINTER :: SurfNode
    TYPE(SurfaceNodeType), DIMENSION(:), POINTER :: b
    INTEGER, INTENT(IN) :: NB_Point
    INTEGER :: nsize, i
    INTEGER :: ierr1=0, ierr2=0, ierr3=0, ierr4=0
    LOGICAL :: NotNewPointer

    NotNewPointer = .FALSE.
    IF(ASSOCIATED(SurfNode))THEN
       nsize = SIZE(SurfNode)
       IF(nsize>=NB_Point) RETURN
       NotNewPointer = .TRUE.
       ALLOCATE(b(nsize),stat=ierr1)
       DO i = 1,nsize
          CALL SurfaceNode_Copy(SurfNode(i), b(i))
       ENDDO
       DEALLOCATE(SurfNode,stat=ierr2)
    ENDIF

    ALLOCATE(SurfNode(NB_Point + Surf_allc_increase),stat=ierr3)

    IF(NotNewPointer)THEN
       DO i = 1,nsize
          CALL SurfaceNode_Copy(b(i),SurfNode(i))
       ENDDO
       DEALLOCATE(b,stat=ierr4)
    ENDIF

    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0.OR.ierr4/=0)THEN
       WRITE(*,*) ' ALLOCATION_SurfNode ERROR: ierr=',ierr1,ierr2,ierr3,ierr4
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', NB_Point
       CALL Error_STOP ('allc_SurfNode')
    ENDIF

    RETURN
  END SUBROUTINE allc_SurfNode

  !>  Copy a RidgeNode to another.
  !!  Reminder: NEVER copy a RidgeNode to itself.
  !<
  SUBROUTINE SurfaceNode_Copy(SurfNode1, SurfNode2)
    IMPLICIT NONE
    TYPE(SurfaceNodeType), INTENT(IN)    :: SurfNode1
    TYPE(SurfaceNodeType), INTENT(INOUT) :: SurfNode2
    INTEGER :: i

    SurfNode2%onRidge  = SurfNode1%onRidge
    SurfNode2%movable  = SurfNode1%movable
    SurfNode2%numParts = SurfNode1%numParts
    DO i = 1,  SurfNode2%numParts
       CALL IntQueue_Copy( SurfNode1%PartTri(i), SurfNode2%PartTri(i) )
       SurfNode2%anor(:, i) = SurfNode1%anor(:, i)
    ENDDO
  END SUBROUTINE SurfaceNode_Copy


  !>  Calculate geometry of a triangle.
  !!  @param[in]  Surf
  !!  @param[in]  IT
  !!  @param[in,out] SurfTri   the SurfTri(IT) will be updated.
  !<
  SUBROUTINE SurfTri_CalGeom(Surf, SurfTri, IT )
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType),  INTENT(IN) :: Surf
    TYPE(SurfaceTriangleType), INTENT(INOUT)  :: SurfTri(*)
    INTEGER, INTENT(IN) :: IT
    REAL*8  :: a(3), aa, p1(3), p2(3), p3(3)

    p1(:) = Surf%Posit(:,Surf%IP_Tri(1,IT))
    p2(:) = Surf%Posit(:,Surf%IP_Tri(2,IT))
    p3(:) = Surf%Posit(:,Surf%IP_Tri(3,IT))
    a(:)  = Geo3D_Cross_Product(P2,P1,P3)
    aa    = dsqrt( a(1)*a(1) + a(2)*a(2) + a(3)*a(3) )
    SurfTri(IT)%Area    = aa / 2.d0
    SurfTri(IT)%anor(:) = a(:) / aa

  END SUBROUTINE SurfTri_CalGeom

  !>  Calculate geometry of all surface triangles and nodes.
  !!  @param[in] Surf
  !!  @param[in,out] SurfTri
  !!  @param[in,out] SurfNode
  !!
  !!  Reminder: the arrayes must be allocated in advance.
  !<
  SUBROUTINE SurfGeo_Build(Surf, SurfTri, SurfNode)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType),  INTENT(IN) :: Surf
    TYPE(SurfaceTriangleType), INTENT(INOUT)  :: SurfTri(*)
    TYPE(SurfaceNodeType), INTENT(INOUT)      :: SurfNode(*)
    INTEGER :: IT, IP, i
    REAL*8  :: a(3), aa

    DO IT = 1, Surf%NB_Tri
       CALL SurfTri_CalGeom(Surf, SurfTri, IT)
    ENDDO

    DO IP = 1,Surf%NB_Point
       SurfNode(IP)%anor(:,1) = 0
    ENDDO

    DO  IT = 1, Surf%NB_Tri
       a(:) = SurfTri(IT)%anor(:) / SurfTri(IT)%Area
       DO i = 1,3
          IP = Surf%IP_Tri(i,IT)
          SurfNode(IP)%anor(:,1) = SurfNode(IP)%anor(:,1) + a(:)
       ENDDO
    ENDDO

    DO IP = 1,Surf%NB_Point
       a(:) = SurfNode(IP)%anor(:,1)
       aa   = dsqrt( a(1)*a(1) + a(2)*a(2) + a(3)*a(3) )
       SurfNode(IP)%anor(:,1) = a(:) / aa
    ENDDO

  END SUBROUTINE SurfGeo_Build



END MODULE SurfaceMeshGeometry

