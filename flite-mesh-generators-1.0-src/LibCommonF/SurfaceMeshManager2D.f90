!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!    A collection of functions for module SurfaceMeshStorage.
!<
MODULE SurfaceMeshManager2D
  USE array_allocator
  USE SurfaceMeshStorage
  USE SurfaceMeshManager
  USE NodeNetTree
  USE Geometry2D
  USE Number_Char_Transfer
  USE CellConnectivity
  USE SpacingStorage2D
  IMPLICIT NONE


CONTAINS



  !>
  !!   checks for negative areas in the parameter plane.
  !!   @param[in]  Surf    : the 2D mesh
  !!   @param[out] nBad    : the number of reiangles with negative area.
  !!   @param[out] ieMin   : the trianlge with minimum area.
  !!   @param[out] aMin    : the area of triangle ieMin
  !<
  SUBROUTINE Surf2D_CheckArea(Surf, nBad, ieMin, aMin)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(in) :: Surf
    INTEGER, INTENT(OUT) :: nBad, ieMin
    REAL*8, INTENT(OUT)  :: aMin
    INTEGER :: ie
    REAL*8  :: P1(2), P2(2), P3(2), A

    nBad  = 0
    ieMin = 0
    aMin  = 1.D30

    DO ie=1,Surf%NB_Tri
       P1 = Surf%Coord(:,Surf%IP_Tri(1,ie))
       P2 = Surf%Coord(:,Surf%IP_Tri(2,ie))
       P3 = Surf%Coord(:,Surf%IP_Tri(3,ie))
       A  = Cross_Product_2D(P2,P1,P3)
       IF(A<=0.)  nBad = nBad + 1
       IF(A<aMin)THEN
          ieMin = ie
          aMin  = A
       ENDIF
    ENDDO

    aMin = aMin / 2.d0

    RETURN
  END SUBROUTINE Surf2D_CheckArea


  !******************************************************************************

  SUBROUTINE Surf2D_DelaunaySwap2(Surf,BGSpacing)
    IMPLICIT NONE
    REAL*8, PARAMETER :: PI = 3.141592654D0
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    TYPE(SpacingStorage2DType), INTENT(IN), OPTIONAL :: BGSpacing

    INTEGER :: IEdge, ITri, Nswap, Kstr
    INTEGER :: i,ip3,ip4,it1,it2,i1,i2,ips(2), nloop
    REAL*8  :: ang1,ang2,p1(2),p2(2),p3(2),p4(2), p0(2), fMetric(3)

    ! *** notation (original configuration) 

    !                       p1         p3 
    !                        *---------*
    !                       / \       /
    !                      /   \  E1 /
    !                     / E2  \   /
    !                    /       \ /
    !                   *---------*
    !                  p4         p2

    !---- build an edge system ---
    CALL Surf_BuildEdge(Surf)

    kstr = 0
    IF(PRESENT(BGSpacing))THEN
       IF(BGSpacing%Model<0) kstr = 1
    ENDIF

    !---- repeatation LOOP

    LOOP_repeat: DO nloop=1,5

       Nswap=0
       DO IEdge = 1,Surf%NB_Edge

          IF(Surf%ITR_Edge(2,IEdge)==0) CYCLE     !--- boundary edge

          !--- check angle

          it1  = Surf%ITR_Edge(1,IEdge)
          it2  = Surf%ITR_Edge(2,IEdge)
          ip3 = SUM(Surf%IP_Tri(1:3,it1)) - SUM(Surf%IP_Edge(1:2,IEdge))
          ip4 = SUM(Surf%IP_Tri(1:3,it2)) - SUM(Surf%IP_Edge(1:2,IEdge))

          p1(:) = Surf%Coord(:,Surf%IP_Edge(1,IEdge))
          p2(:) = Surf%Coord(:,Surf%IP_Edge(2,IEdge))
          p3(:) = Surf%Coord(:,ip3)
          p4(:) = Surf%Coord(:,ip4)
          IF(kstr==0)THEN
             ang1 = Included_Angle_2D(p1,p3,p2)               
             ang2 = Included_Angle_2D(p2,p4,p1)
          ELSE
             p0   = (p1 + p2 + p3 + p4) / 4.d0
             CALL SpacingStorage2D_GetMetric(BGSpacing, P0, fMetric)
             ang1 = Included_Angle_Metric2D (p1,p3,p2, fMetric)
             ang2 = Included_Angle_Metric2D (p2,p4,p1, fMetric)
          ENDIF

          IF(ang1>0 .AND. ang2>0 .AND. ang1+ang2<PI) CYCLE

          !--- swap iedge

          Nswap = Nswap+1

          CALL Surf_SwapSingleEdge(Surf, IEdge)

       ENDDO

       PRINT*,'nloop,Nswap=', nloop, Nswap

       IF(Nswap==0) EXIT

    ENDDO LOOP_repeat                     !---- repeatation LOOP

    CALL Surf_ClearEdge(Surf)

    RETURN
  END SUBROUTINE Surf2D_DelaunaySwap2



  !>
  !!  Break long edges.
  !!  @param[in,out]  Surf
  !!  @param[in]   Criterion   length criterion (dx,dy) 
  !!  @param[in]   K_region =1  for the whole domain.    \n
  !!                        =2  for the whole domain except boundary. 
  !!  @param[out]  nBreak
  !<
  SUBROUTINE Surf2D_EdgeBreak(Surf, K_region, nBreak, BGSpacing)
    IMPLICIT NONE

    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(IN)  :: K_region
    INTEGER, INTENT(OUT) :: nBreak
    TYPE(SpacingStorage2DType), INTENT(IN), OPTIONAL :: BGSpacing
    INTEGER :: i,j,np,nel,ip1,ip2
    INTEGER :: ip(3), ips(2)
    REAL*8  :: Criterion(2)
    REAL*8  :: xt(2)
    INTEGER :: IEdge, nside, Mside, npNew, neNew, kstr
    INTEGER :: tridiv(3), nntri, trinew(3,4)
    REAL*8  :: trilen(3)
    INTEGER, DIMENSION(:  ), POINTER :: Edge_Div
    REAL*8 , DIMENSION(:  ), POINTER :: Edge_Len
    REAL*8  :: Source_RR, xl
    TYPE(NodeNetTreeType) :: Edge_Tree

    kstr = 0
    Criterion(:) = 1
    IF(PRESENT(BGSpacing))THEN
       Criterion(:) = BGSpacing%BasicSize
       IF(BGSpacing%Model<0)THEN
          kstr = 1
          CALL Error_Stop('--- under constuctured ---')
       ENDIF
    ENDIF

    !----- 
    Mside=10*Surf%NB_Point
    CALL allc_1Dpointer(Edge_Div,  Mside,   'Edge_Div')
    CALL allc_1Dpointer(Edge_Len,  Mside,   'Edge_Len')
    CALL NodeNetTree_Allocate(2, Surf%NB_Point, Mside, Edge_Tree)

    npNew = Surf%NB_Point
    neNew = Surf%NB_Tri
    nside = 0

    !---- build edges

    DO j=1,Surf%NB_Tri
       ip(:)=Surf%IP_Tri(1:3,j)

       DO i=1,3
          ip1=ip(iEdge_Tri(1,i))
          ip2=ip(iEdge_Tri(2,i))
          ips = (/ip1,ip2/)
          CALL NodeNetTree_SearchAdd(2,ips,Edge_Tree,IEdge)

          IF(IEdge>nside)THEN
             !--- a new edge
             nside = IEdge
             Edge_Len(IEdge) = ( (Surf%Coord(1,ip1)-Surf%Coord(1,ip2))/Criterion(1) )**2   &
                  +( (Surf%Coord(2,ip1)-Surf%Coord(2,ip2))/Criterion(2) )**2
             Edge_Div(IEdge) = 0
             IF(Edge_Len(IEdge)>1) Edge_Div(IEdge)=-1
          ELSE 
             IF(Edge_Len(IEdge)>1) Edge_Div(IEdge)=-2
          ENDIF

          IF(Edge_Div(IEdge) <= -K_region)THEN
	     npNew=npNew+1
             Edge_Div(IEdge)=npNew
             IF(npNew>SIZE(Surf%Coord,2))THEN
                CALL allc_2Dpointer(Surf%Coord, 2, npNew + Surf_allc_increase)
             ENDIF
             Surf%Coord(:,npNew)=(Surf%Coord(:,ip1)+Surf%Coord(:,ip2))/2.0
          ENDIF

       ENDDO
    ENDDO

    !---- loop on Triangles ---

    nBreak = 0
    DO j=1,Surf%NB_Tri
       ip(:)=Surf%IP_Tri(1:3,j)

       DO i=1,3
          ips = ip(iEdge_Tri(:,i))
          CALL NodeNetTree_Search(2,ips,Edge_Tree,IEdge)
          tridiv(i) = Edge_Div(IEdge)
          trilen(i) = Edge_Len(IEdge)
       ENDDO

       CALL Tri_Divider(ip,tridiv, trilen, nntri, trinew)

       IF(nntri==1) CYCLE

       nBreak = nBreak +1

       Surf%IP_Tri(1:3,j) = trinew(1:3,1)
       DO nel=2,nntri
          neNew = neNew + 1
          IF(neNew>SIZE(Surf%IP_Tri,2))THEN
             CALL allc_2Dpointer(Surf%IP_Tri,5,npNew + Surf_allc_increase)
          ENDIF
          Surf%IP_Tri(1:3,neNew) =  trinew(1:3,nel)
       ENDDO

    ENDDO

    Surf%NB_Point = npNew
    Surf%NB_Tri   = neNew

    DEALLOCATE(Edge_Div,  Edge_Len)
    CALL NodeNetTree_CLEAR(Edge_Tree)

    RETURN
  END SUBROUTINE Surf2D_EdgeBreak


  !******************************************************************************
  !>
  !!  Search a cavity of a new point and split the cavity with balls.
  !!
  !!  @param[in,out] Surf :  the triangular surface mesh.
  !!  @param[in]     IP   :  the insected point                                  
  !!  @param[in]     IT0  :  the initial triangle in which point IP lies.
  !!                         If IT0=0, then program will do search.
  !!  @param[in]     Kstr  useless.       
  !!  @param[in]   tolbound : boundary closing cofficient.
  !!                 
  !!  @param[out] Isucc = -1  fail due to close a boundary edge          \n
  !!                    = -2 fail due to too many triangles in cavity.   \n
  !!                    =  0  fail due to other errors                   \n
  !!                    =  1  succeed.
  !<
  !******************************************************************************
  SUBROUTINE Surf2D_InsertPoint(Surf,IP,IT0,Kstr,tolbound,Isucc, BGSpacing)
    IMPLICIT NONE      
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(IN)  :: IP, IT0, Kstr
    REAL*8, INTENT(IN)   :: tolbound 
    INTEGER, INTENT(OUT) :: Isucc
    TYPE(SpacingStorage2DType), INTENT(IN), OPTIONAL :: BGSpacing
    INTEGER :: IT_Cavity(500), Nins
    INTEGER :: IP_Hull(2,500), Nside
    INTEGER :: IT_Outs(500), ID_Hull(2,500)
    INTEGER, SAVE :: ITsave = 1
    INTEGER :: kstr1
    INTEGER :: i, it, it1, it2, i1, i4, ip2, ip3, ip4, isd, Ns1, itnb
    REAL*8  :: P0(2), p2(2), p3(2), p4(2), fMetric(3)
    REAL*8  :: RAfac,  RAsq, d1, d2
    REAL*8  :: tol1 = 1.d-8



    !******************************************************************************
    !     Search a cavity of a new point                                     
    !     Input:  IP  :  the insected point                                  
    !             IT0 :  the initial triangle in which point IP lies         
    !                    if IT0=0, then program will do search               
    !             Kstr  =1  for a stretching domain                          
    !                   =0  for a non-stretching domain                      
    !     Output:  IT_Cavity(*) : a serial of triangles which compose a cavity     
    !              Nins         : the length of IT_Cavity                    
    !              IP_Hull(2,*) : two nodes of each edge of the hull         
    !              Nside        : the length of IP_Hull, i.e. number of edges
    !              IT_Outs(*)   : neighbour triangles of the cavity          
    !                             This array is used for reseting next-system
    !******************************************************************************


    !---- Criterion of swapping

    RAfac  = 1.D0
    Isucc  = 0
    Kstr1  = 0
    IF(PRESENT(BGSpacing))THEN
       IF(Kstr==1 .AND. BGSpacing%Model<0) Kstr1 = 1
    ENDIF


    P0(:) = Surf%Coord(:,IP)
    IF(kstr1==1)THEN
       CALL SpacingStorage2D_GetMetric (BGSpacing, P0, fMetric)
    ENDIF


    !---- Initialise a hull
    it1 = IT0
    IF(it1==0)THEN
       it1 = ITsave
       CALL Surf2D_SearchTri(Surf,P0,it1,i,tol1)
    ENDIF
    ITsave = it1          !-- making next search quicker
    IF(it1==0)THEN
       WRITE(*,*)'Error--- Surf2D_SearchCavity: can not locate a point'
       WRITE(*,*)'ip,p0=',ip,p0
       CALL Error_STOP (' ')
    ENDIF
    IT_Cavity(1) = it1

    Nside = 0
    DO i=1,3
       Nside            = Nside+1
       IP_Hull(1,Nside) = Surf%IP_Tri(MOD(i,3)+1,  it1)
       IP_Hull(2,Nside) = Surf%IP_Tri(MOD(i+1,3)+1,it1)
       ID_Hull(1,Nside) = it1
       ID_Hull(2,Nside) = i
    ENDDO

    Nins  = 1

    !---- Enlarge the hull if possible

    Ns1 = 0
    Loop_Nside : DO WHILE(Nside>Ns1)
       Ns1 = Nside
       DO isd=1,Ns1
          it1 = ID_Hull(1,isd)
          i1  = ID_Hull(2,isd)
          ip2 = IP_Hull(1,isd)
          ip3 = IP_Hull(2,isd)
          p2(:) = Surf%Coord(:,ip2)
          p3(:) = Surf%Coord(:,ip3)

          it2 = Surf%Next_Tri(i1,it1)
          IF(it2<=0)THEN
             !-- for a boundary edge
             IF(kstr1==0)THEN
                d1 = ABS(Cross_Product_2D(P0,p2,p3))
                d2 = tolbound *Distance_SQ_2D(p2,p3)
             ELSE
                d1 = ABS(Cross_Product_Metric2D (P0,p2,p3, fMetric))
                d2 = tolbound *Metric2D_Distance_SQ(p2, p3, fMetric)
             ENDIF
             IF(d1<d2)THEN
                !-- the point close to a boundary egde, return with Isucc=-1
                Isucc = -1
                RETURN
             ELSE
                CYCLE
             ENDIF
          ENDIF

          DO i=1,3
             IF(Surf%IP_Tri(i,it2)/=ip2 .AND. Surf%IP_Tri(i,it2)/=ip3) i4=i
          ENDDO
          ip4 = Surf%IP_Tri(i4,it2)
          p4(:) = Surf%Coord(:,ip4)
          IF(kstr1==0)THEN
             RAsq = Surf2D_Criterion_inCircle(p4,p3,p2,P0)
          ELSE
             RAsq = Surf2D_Criterion_inCircle(p4,p3,p2,P0, fMetric)
          ENDIF
          IF(RAsq < RAfac)THEN
             Nins = Nins+1
             IT_Cavity(Nins) = it2

             Nside = Nside+1
             IP_Hull(1,isd) = ip2
             IP_Hull(2,isd) = ip4
             IP_Hull(1,Nside) = ip4
             IP_Hull(2,Nside) = ip3
             ID_Hull(1,isd) = it2
             ID_Hull(2,isd) = MOD(i4,3)+1
             ID_Hull(1,Nside) = it2
             ID_Hull(2,Nside) = MOD(i4+1,3)+1

             IF(Nins>490)THEN
                !--- Too many triangles in a cavity
                EXIT Loop_Nside
             ENDIF
          ENDIF
       ENDDO
    ENDDO Loop_Nside

    DO isd = 1, Nside
       IT_Outs(isd) = Surf%Next_Tri(ID_Hull(2,isd), ID_Hull(1,isd))
    ENDDO


    !******************************************************************************
    !     Split a cavity into triangles                                      
    !     Input:  IP  :  the central point of the cavity                     
    !             IT_Cavity(*) : a serial of triangles which compose the cavity    
    !                            This array will be modified                 
    !             Nins         : the length of IT_Cavity                     
    !             IP_Hull(2,*) : two nodes of each edge of the hull          
    !             Nside        : the length of IP_Hull, i.e. number of edges 
    !             IT_Outs(*)   : neighbour triangles of the cavity           
    !                            This array is used for reseting next-system 
    !     Output:  Surf%IP_Tri(3,*)   : for balls. This including modifing cavity 
    !                              triangles and crease new triangles (if apply)   
    !              Surf%Next_Tri(3,*) : for balls and their neighbours            
    !******************************************************************************

    !---- Split the cavity
    DO isd = 1,Nside
       IF(isd<=Nins)THEN
          it=IT_Cavity(isd)
       ELSE
          Surf%NB_Tri = Surf%NB_Tri+1
          IF(Surf%NB_Tri>SIZE(Surf%IP_Tri,2))THEN
             CALL allc_2Dpointer( Surf%IP_Tri,  5,Surf%NB_Tri + Surf_allc_increase )
          ENDIF
          IF(Surf%NB_Tri>SIZE(Surf%Next_Tri,2))THEN
             CALL allc_2Dpointer( Surf%Next_Tri,3,Surf%NB_Tri + Surf_allc_increase )
          ENDIF
          it = Surf%NB_Tri
          IT_Cavity(isd) = it
       ENDIF
       Surf%IP_Tri(1,  it) = IP
       Surf%IP_Tri(2:3,it) = IP_Hull(1:2,isd)
    ENDDO

    !--- update next

    DO isd = 1,Nside
       it   = IT_Cavity(isd)
       itnb = IT_Outs(isd)
       Surf%Next_Tri(1,it) = itnb
       IF(itnb>0)THEN
          DO i=1,3
             IF(  Surf%IP_Tri(MOD(i,  3)+1,itnb)==IP_Hull(2,isd) .AND.  &
                  Surf%IP_Tri(MOD(i+1,3)+1,itnb)==IP_Hull(1,isd) ) EXIT
          ENDDO
          IF(i>3) CALL Error_STOP ( '---Surf2D_SplitCavity :: error next 1')
          Surf%Next_Tri(i,itnb) = it
       ENDIF

       DO i = 1,Nside
          IF(i==isd) CYCLE
          itnb = IT_Cavity(i)
          IF(Surf%IP_Tri(2,it) == Surf%IP_Tri(3,itnb))THEN             
             Surf%Next_Tri(3,it)   = itnb
             Surf%Next_Tri(2,itnb) = it
             EXIT
          ENDIF
       ENDDO

       IF(i>Nside)THEN
          WRITE(*,*)'Nside=',Nside
          WRITE(*,*)'IP_Hull(1,:)=',IP_Hull(1,1:Nside)
          WRITE(*,*)'IP_Hull(2,:)=',IP_Hull(2,1:Nside)
          CALL Error_STOP ( '---Surf2D_SplitCavity :: error next 2')
       ENDIF
    ENDDO

    Isucc = 1

    RETURN
  END SUBROUTINE Surf2D_InsertPoint

  !******************************************************************************
  !                                                                              
  !     Check if point pp lie in circumcircle of triangle p1-p2-p3               
  !           note: p1 is the opposite point of pp                               
  !                                                                              
  !     Output: RA    <=1 in circumcircle; >1 out circumcircle                   
  !                                                                              
  !******************************************************************************
  FUNCTION Surf2D_Criterion_inCircle(p1,p2,p3,pp,fMetric) RESULT(RA)

    IMPLICIT NONE

    REAL*8, INTENT(IN), OPTIONAL  :: fMetric(3)
    REAL*8  :: p0(2),p1(2),p2(2),p3(2),pp(2),RA,ang1,ang2

    IF(PRESENT(fMetric))THEN

       ang1 =            Included_Angle_Metric2D(pp,p2,p3,fMetric)
       ang1 = MIN( ang1, Included_Angle_Metric2D(p3,p2,p1,fMetric) )
       ang1 = MIN( ang1, Included_Angle_Metric2D(p1,p3,p2,fMetric) )
       ang1 = MIN( ang1, Included_Angle_Metric2D(p2,p3,pp,fMetric) )

       IF(ang1<-1.d-8)THEN
          WRITE(*,*)'Error: wrong angle'
          WRITE(*,*)'p1=',p1
          WRITE(*,*)'p2=',p2
          WRITE(*,*)'p3=',p3
          WRITE(*,*)'pp=',pp
          WRITE(*,*)'fMetric=',fMetric
          CALL Error_Stop('--- Criterion_in_Circle')
       ENDIF

       ang2 =            Included_Angle_Metric2D(p2,p1,pp,fMetric)
       ang2 = MIN( ang2, Included_Angle_Metric2D(pp,p1,p3,fMetric) )
       ang2 = MIN( ang2, Included_Angle_Metric2D(p3,pp,p1,fMetric) )
       ang2 = MIN( ang2, Included_Angle_Metric2D(p1,pp,p2,fMetric) )

       IF(ang2<=1.d-8)THEN
          RA = 1.d8
       ELSE
          RA = ang1/ang2
       ENDIF

    ELSE

       p0(:) = Circle_Centre_2D(p1,p2,p3)     
       RA = dsqrt( Distance_SQ_2D(pp,p0) / Distance_SQ_2D(p1,p0) )

    ENDIF

    RETURN
  END FUNCTION Surf2D_Criterion_inCircle

  !******************************************************************************
  !     Break large edges                                                  
  !                                                                        
  !******************************************************************************
  SUBROUTINE Surf2D_Refine(Surf,BGSpacing)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    TYPE(SpacingStorage2DType), INTENT(IN) :: BGSpacing
    REAL*8 :: GridSize

    INTEGER :: IP, IT, i, j, itnb, ip2, ip3, IB, Isucc
    INTEGER :: NB_old, Idou
    REAL*8  :: p1(2),p2(2),p3(2),Po(2), RAsq,RAstand,RAfac,RAmax,RAang
    REAL*8  :: ang, xl, fMetric(3), scalar
    REAL*8, PARAMETER :: PI = 3.141592654D0

    GridSize = BGSpacing%BasicSize
    Idou     = 3
    RAfac    = 2.d0
    RAstand  = dsqrt(2.d0) * GridSize
    RAang    = 15.d0/180*PI
    DO i=1,Idou
       RAstand = RAstand*RAfac
    ENDDO

    DO WHILE(Idou>=0)

       NB_old = Surf%NB_Tri
       RAmax = 0.
       DO IT = 1,NB_old

          DO i=1,3
             !--- calculate edge length
             itnb = Surf%Next_Tri(i,IT)
             IF(itnb<=0)   CYCLE

             ip2   = Surf%IP_Tri(MOD(i  ,3)+1,IT)
             ip3   = Surf%IP_Tri(MOD(i+1,3)+1,IT)
             IF(ip2>ip3) CYCLE

             p2(:) = Surf%Coord(:,ip2)
             p3(:) = Surf%Coord(:,ip3)
             po(:) = (p2(:)+p3(:)) / 2.d0
             IF(BGSpacing%Model>0)THEN
                CALL SpacingStorage2D_GetScale (BGSpacing, Po, Scalar)
                RAsq  = Dsqrt((p2(1)-p3(1))**2 + (p2(2)-p3(2))**2)
                RAsq  = RAsq / Scalar
             ELSE
                CALL SpacingStorage2D_GetMetric (BGSpacing, Po, fMetric)
                RAsq  = Metric2D_Distance(p2, p3, fMetric)
             ENDIF

             IF(RAmax < RAsq) RAmax = RAsq

             IF(RAstand >= RAsq) CYCLE

             !--- calculate new angles

             p1(:)  = Surf%Coord(:,Surf%IP_Tri(i,IT))
             IF(BGSpacing%Model>0)THEN
                ang    = ABS( Included_Angle_2D(p2,p1,p3) ) 
             ELSE
                CALL SpacingStorage2D_GetMetric (BGSpacing, P1, fMetric)
                ang    = ABS( Included_Angle_Metric2D(p2,p1,p3,fMetric) )
             ENDIF
             IF(ang < RAang) CYCLE

             DO j=1,3
                IF(Surf%IP_Tri(j,itnb)/=ip2.AND.Surf%IP_Tri(j,itnb)/=ip3)THEN
                   p1(:) = Surf%Coord(:,Surf%IP_Tri(j,itnb))
                ENDIF
             ENDDO
             IF(BGSpacing%Model>0)THEN
                ang    = ABS( Included_Angle_2D(p2,p1,p3) ) 
             ELSE
                CALL SpacingStorage2D_GetMetric (BGSpacing, P1, fMetric)
                ang    = ABS( Included_Angle_Metric2D(p2,p1,p3,fMetric) )
             ENDIF
             IF(ang < RAang) CYCLE

             !--- split this edge by inserting a new point

             Surf%NB_Point = Surf%NB_Point+1
             IF(BGSpacing%Model>0)THEN
                xl = RAsq/GridSize
                IF(xl<=2.1)THEN
                   xl = 0.5
                ELSE IF(xl<3.3)THEN
                   xl = 0.3
                ELSE
                   xl = GridSize/RAsq
                ENDIF
             ELSE
                xl = 0.5
             ENDIF
             Po = p2 + xl * (p3-p2)
             IF(Surf%NB_Point>SIZE(Surf%Coord,2))THEN
                CALL allc_2Dpointer(Surf%Coord, 2, Surf%NB_Point + Surf_allc_increase)
             ENDIF
             Surf%Coord(:,Surf%NB_Point) = Po(:)

             IP = Surf%NB_Point
             CALL Surf2D_InsertPoint(Surf, IP,IT, 1, 1.d-1, Isucc, BGSpacing)
             IF(Isucc/=1) Surf%NB_Point = Surf%NB_Point-1     !--- insert fails.
             EXIT
          ENDDO

       ENDDO

       IF(NB_old == Surf%NB_Tri)THEN
          Idou    = Idou-1
          RAstand = RAstand / RAfac
          DO WHILE(RAmax<RAstand)
             Idou    = Idou-1
             RAstand = RAstand/RAfac
          ENDDO
       ENDIF

    ENDDO
    WRITE(*,*)

    RETURN
  END SUBROUTINE Surf2D_Refine


  !>                                                                              
  !!   Triangulate 2D plane area surrounded by boundary edges.     
  !!   @param[in]  Surf    :  the number and coordinates of nodes.
  !!   @param[in]  NB_Corner :  the number of corners (on the boundary loop).                                
  !!   @param[in]  ipc       :  the basic node of each corner.                               
  !!   @param[in]  inext     :  the next CORNER of each corner.     
  !!              (i.e. ipc(k) and ipc(inext(k)) make a side of corner k)       
  !!   @param[out] Surf    : the number and connectivities of triangles.                    
  !!                                                                              
  !!   Reminder: Subroutine Plane2DTriangulator() is an alternative for single loops.
  !!
  !!   Here is a sample for ipc and inext:   \image html cnect.jpg
  !<
  SUBROUTINE Surf2D_BoundaryConnect(Surf, NB_Corner, ipc, inext)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(IN)    :: NB_Corner
    INTEGER, INTENT(INOUT) :: ipc(*), inext(*)
    REAL*8  :: a1, a2, a3, aBig, poo(2), pjj(2), pft(2), pnx(2)
    INTEGER :: Nsd, jj, jk, ncheck, id, ids(3), ift, inx, jt, icheck(4)
    REAL*8,  DIMENSION(:  ),  POINTER :: angle
    INTEGER, DIMENSION(:  ),  POINTER :: ifrnt
    REAL*8 :: Triangulator_AngleSine    !<  function in file PlaneTriangulater.f90

    IF(NB_Corner>KeyLength(Surf%IP_Tri))THEN
       CALL allc_2Dpointer(Surf%IP_Tri, 5, NB_Corner+1000)
    ENDIF

    IF(NB_Corner<3)THEN
       CALL Error_STOP ( '--- Surf2D_BoundaryConnect:: NB_Corner<3')
    ELSE IF(NB_Corner==3)THEN
       id = 1
       Surf%IP_Tri(1,1) = ipc(id)
       id = inext(id)
       Surf%IP_Tri(2,1) = ipc(id)
       id = inext(id)
       Surf%IP_Tri(3,1) = ipc(id)
       id = inext(id)
       IF(id/=1) CALL Error_STOP ( '--- Surf2D_BoundaryConnect:: id/=1')
       Surf%NB_Tri = 1
       RETURN
    ENDIF

    ALLOCATE(angle(2*NB_Corner), ifrnt(2*NB_Corner))   !--- the number of corners might increase.
    ifrnt(1:NB_Corner) = 0

    !--- build ifrnt for front index
    ifrnt(:) = 0
    DO jk=1,NB_Corner
       IF(ipc(jk)   > Surf%NB_Point) CALL Error_STOP ( '--- Surf2D_BoundaryConnect:: ipc(jk)   ')
       IF(inext(jk) > NB_Corner)     CALL Error_STOP ( '--- Surf2D_BoundaryConnect:: inext(jk) ')
       ifrnt(inext(jk)) = jk
    ENDDO

    !--- calculate angles for all Corner
    DO jk=1,NB_Corner
       IF(ifrnt(jk)==0) CALL Error_STOP ( '--- Surf2D_BoundaryConnect:: ifrnt(jk)==0')
       angle(jk) = Triangulator_AngleSine(Surf%Coord(:,ipc(inext(jk))),   &
            Surf%Coord(:,ipc(jk)),  Surf%Coord(:,ipc(ifrnt(jk))))
    ENDDO

    Nsd         = NB_Corner
    Surf%NB_Tri = 0

    Loop_Nsd : DO

       !--- clean removed corners
       jj=1
       DO WHILE(jj<=Nsd)
          IF(ipc(Nsd)==0)THEN
             Nsd = Nsd -1
             CYCLE
          ENDIF
          IF(ipc(jj)==0)THEN
             ipc(jj)   = ipc(Nsd)
             angle(jj) = angle(Nsd)
             id        = ifrnt(Nsd)
             IF(id==0) CALL Error_STOP ( '--- Surf2D_BoundaryConnect:: ifrnt(Nsd)==0')
             ifrnt(jj) = id
             inext(id) = jj
             id        = inext(Nsd)
             IF(id==0) CALL Error_STOP ( '--- Surf2D_BoundaryConnect:: inext(Nsd)==0')
             inext(jj) = id
             ifrnt(id) = jj
             Nsd = Nsd -1
          ENDIF
          jj = jj+1
       ENDDO
       IF(Nsd<3) EXIT

       !--- check if a triangle formed
       DO jj = 1, Nsd
          IF(ipc(jj)==0) CALL Error_STOP ( '--- Surf2D_BoundaryConnect:: ipc(jj)==0')
          IF(inext(inext(jj))==ifrnt(jj))THEN
             IF(ifrnt(jj)==0) CALL Error_STOP ( '--- Surf2D_BoundaryConnect:: ifrnt(jj)==0')
             !--- a triangle found
             Surf%NB_Tri = Surf%NB_Tri + 1
             ids = (/ifrnt(jj), jj, inext(jj)/)
             Surf%IP_Tri(1:3,Surf%NB_Tri) = ipc(ids(:))
             ipc(ids(:)) = 0
             CYCLE Loop_Nsd
          ENDIF
       ENDDO

       !--- search the corner with the minimum angle
       jk  = 0
       DO jj = 1, Nsd
          IF(angle(jj)<=0) CYCLE
          IF(jk==0) jk=jj
          IF(angle(jj)<angle(jk)) jk = jj
       ENDDO
       IF(jk==0)THEN
          WRITE(*,*)'#Error--- all angles left are negative. Check the orientation please.'
          WRITE(*,*)'#NB_Corner, Nsd, Surf%NB_Tri', NB_Corner, Nsd, Surf%NB_Tri
          CALL Error_STOP ( '--- Surf2D_BoundaryConnect')
       ENDIF

       !--- form a trianlge

       ift = ifrnt(jk)
       inx = inext(jk)
       pft = Surf%Coord(:,ipc(ift))
       pnx = Surf%Coord(:,ipc(inx))
       poo = Surf%Coord(:,ipc(jk))

       !--- check if a point in the new triangle
       Loop_ncheck : DO ncheck = 1,Nsd
          IF(ncheck==Nsd) CALL Error_STOP ( '--- Surf2D_BoundaryConnect:: dead loop')

          aBig = 10000
          jt = 0
          DO jj = 1,Nsd
             IF(    jj ==    ift  .OR.     jj ==    jk  .OR.     jj ==    inx ) CYCLE
             IF(ipc(jj)==ipc(ift) .OR. ipc(jj)==ipc(jk) .OR. ipc(jj)==ipc(inx)) CYCLE
             pjj = Surf%Coord(:,ipc(jj))
             a1 = Triangulator_AngleSine(pjj, poo, pft)
             IF(a1<=0) CYCLE
             a2 = Triangulator_AngleSine(pnx, poo, pjj)
             IF(a2<=0) CYCLE
             a3 = Triangulator_AngleSine(pjj, pnx, pft)
             IF(a3>MIN(a1,a2)) CYCLE
             IF(a3>aBig) CYCLE
             aBig = a3
             !--- corner jj is inside or close the triangle 
             jt = jj
          ENDDO
          IF(jt==0) EXIT
          ift = jt
          pft = Surf%Coord(:,ipc(ift))
       ENDDO Loop_ncheck

       Surf%NB_Tri = Surf%NB_Tri+1
       Surf%IP_Tri(1:3,Surf%NB_Tri) = (/ ipc(ift), ipc(jk), ipc(inx) /)

       IF(ncheck==1)THEN
          !--- remove the corner jk
          ipc(jk)     = 0
          inext(ift)  = inx
          ifrnt(inx)  = ift
          icheck(1:4) = (/ift,inx,0,0/)

       ELSE IF(ifrnt(ift)==inx)THEN
          !--- remove the corner inx
          ipc(inx)    = 0
          inext(jk)   = ift
          ifrnt(ift)  = jk
          icheck(1:4) = (/ift,jk,0,0/)

       ELSE
          !--- add a corner
          Nsd         = Nsd+1
          ipc(Nsd)    = ipc(ift)
          id          = ifrnt(ift)
          ifrnt(Nsd)  = id
          inext(id)   = Nsd
          inext(Nsd)  = inx
          ifrnt(inx)  = Nsd
          inext(jk)   = ift
          ifrnt(ift)  = jk
          icheck(1:4) = (/ift,jk,inx,Nsd/)

       ENDIF

       !--- update affected angles

       DO jj = 1, 4
          jt = icheck(jj)
          IF(jt==0) EXIT
          IF(inext(jt)==0) CALL Error_STOP ( '--- Surf2D_BoundaryConnect:: inext(jt)==0')
          IF(ifrnt(jt)==0) CALL Error_STOP ( '--- Surf2D_BoundaryConnect:: ifrnt(jt)==0')
          IF(ipc(jt)==0)   CALL Error_STOP ( '--- Surf2D_BoundaryConnect:: ipc(jt)==0')
          angle(jt) = Triangulator_AngleSine(Surf%Coord(:,ipc(inext(jt))),    &
               Surf%Coord(:,ipc(jt)),  Surf%Coord(:,ipc(ifrnt(jt))))
       ENDDO

    ENDDO Loop_Nsd

    IF(Nsd/=0)THEN
       WRITE(*,*)' Error--- Nsd=',Nsd
       CALL Error_STOP ( '-- Surf2D_BoundaryConnect:: Nsd/=0')
    ENDIF

    DEALLOCATE(angle, ifrnt)

    RETURN
  END SUBROUTINE Surf2D_BoundaryConnect


  !******************************************************************************!
  !>
  !!     Build a basic triangulation of the domain enclosed by the boundary edges 
  !!   @param[in]  Surf    :  the number and coordinates of nodes. 
  !!   @param[in]  NB_Corner :  the number of corners (on the boundary loop).                                
  !!   @param[in]  ipc       :  the basic node of each corner.                               
  !!   @param[in]  inext     :  the next CORNER of each corner.     
  !!              (i.e. ipc(k) and ipc(inext(k)) make a side of corner k)       
  !!   @param[out] Surf    : the number and connectivities of triangles.        
  !!
  !!   Reminder: the domain is on the right of a boundary edge.
  !!          i.e.    triangle ipc(k) -- ipc(inext(k)) -- ip_inside 
  !!                  is under a correct orientation
  !!
  !!   Refer SUBROUTINE Surf2D_BoundaryConnect() for the defination of ipc and inext.
  !<         
  !******************************************************************************!
  SUBROUTINE Surf2D_BoundaryRecovConnect(Surf, NB_Corner, ipc, inext)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(IN)    :: NB_Corner
    INTEGER, INTENT(INOUT) :: ipc(*), inext(*)
    INTEGER :: na=4
    INTEGER :: IP, IT, Ib, Isucc, Msize
    REAL*8  :: Pmin(2), Pmax(2), Po(2)

    !---- Put four points to form a rectagular convex full

    IF(NB_Corner>KeyLength(Surf%IP_Tri))THEN
       CALL allc_2Dpointer(Surf%IP_Tri, 5, NB_Corner+1000)
    ENDIF

    Surf%NB_Point = Surf%NB_BD_Point + na
    IF(Surf%NB_Point>SIZE(Surf%Coord,2))THEN
       CALL allc_2Dpointer(Surf%Coord, 2, Surf%NB_Point + Surf_allc_increase)
    ENDIF

    CALL SurfaceMeshStorage_Measure(Surf, 2, Pmin, Pmax)
    Po(:)   = (Pmin(:)+Pmax(:))/2.d0
    Pmin(:) = 1.2D0 * (Pmin(:) - Po(:)) + Po(:)
    Pmax(:) = 1.2D0 * (Pmax(:) - Po(:)) + Po(:)

    Surf%Coord(:,Surf%NB_BD_Point+1) =   Pmin(:)
    Surf%Coord(:,Surf%NB_BD_Point+2) = (/Pmin(1), Pmax(2)/)
    Surf%Coord(:,Surf%NB_BD_Point+3) = (/Pmax(1), Pmin(2)/)
    Surf%Coord(:,Surf%NB_BD_Point+4) =   Pmax(:)

    !---- Set two biggest triangles

    Surf%NB_Tri        = 2
    Surf%IP_Tri(1:3,1) = (/Surf%NB_BD_Point+1, Surf%NB_BD_Point+3, Surf%NB_BD_Point+2/)
    Surf%IP_Tri(1:3,2) = (/Surf%NB_BD_Point+2, Surf%NB_BD_Point+3, Surf%NB_BD_Point+4/)

    IF(.NOT. ASSOCIATED(Surf%Next_Tri))THEN
       Msize = SIZE(Surf%IP_Tri,2)
       ALLOCATE( Surf%Next_Tri(3,Msize) )
    ENDIF
    Surf%Next_Tri(1:3,1) = (/2,-1,-1/)
    Surf%Next_Tri(1:3,2) = (/-1,-1,1/)

    !---- Insert boundary points

    DO IP=1,Surf%NB_BD_Point
       CALL Surf2D_InsertPoint(Surf,IP,0,0,0.d0,Isucc)
       IF(Isucc<=0)THEN
          WRITE(*,*)' Error--- fail to insert ip=',IP,Isucc
          CALL Error_STOP ( '---urf2D_BoundaryRecovConnect')
       ENDIF
    ENDDO

    !---- Recover the boundary

    CALL Surf2D_BoundaryRecovery(Surf, NB_Corner, ipc, inext)     

    RETURN
  END SUBROUTINE Surf2D_BoundaryRecovConnect


  !******************************************************************************!
  !>
  !!     Recover boundary edges                                                   
  !!   @param[in,out]  Surf    :  the number and coordinates of nodes.
  !!   @param[in]  NB_Corner :  the number of corners (on the boundary loop).                                
  !!   @param[in]  ipc       :  the basic node of each corner.                               
  !!   @param[in]  inext     :  the next CORNER of each corner.     
  !!              (i.e. ipc(k) and ipc(inext(k)) make a side of corner k)       
  !!
  !!   Reminder: the domain is on the right of a boundary edge.
  !!          i.e.    triangle ipc(k) -- ipc(inext(k)) -- ip_inside 
  !!                  is under a correct orientation
  !!        
  !!   Refer SUBROUTINE Surf2D_BoundaryConnect() for the defination of ipc and inext.
  !<                                                                              
  !******************************************************************************!
  SUBROUTINE Surf2D_BoundaryRecovery( Surf, NB_Corner, ipc, inext)
    IMPLICIT NONE

    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(IN)    :: NB_Corner
    INTEGER, INTENT(INOUT) :: ipc(*), inext(*)
    INTEGER :: i, i1, i2, IL, IP, ip1,ip2,ip3, ipt1,ipt2,ipt3
    INTEGER :: IB, IT, ITri, ITnb, Kcheck, ntnew
    REAL*8  :: ang1, ang2, ang3
    REAL*8  :: TINY=1.d-6
    LOGICAL :: CarryOn
    INTEGER, DIMENSION(:), POINTER :: Mark_Edg, Mark_Tri

    ALLOCATE(Mark_Edg(NB_Corner), Mark_Tri(4*Surf%NB_Point))

    CALL Surf_BuildTriAsso(Surf)

    LOOP_ib : DO IB = 1 , NB_Corner

       ip1 = ipc(IB)
       ip2 = ipc(inext(IB))

       !---- search a traingle crossed by edge IB from those containing ip1

       IT = Surf%ITR_Pt%JointLinks(ip1)%Nodes(1)

       DO
          IL = 0
          DO i=1,3
             IF(Surf%IP_Tri(i,IT)==ip1) IL=i
          ENDDO
          IF(IL==0)THEN
             WRITE(*,*)'Error---Surf2D_BoundaryRecovery: no this point in the tri'
             WRITE(*,*)'ip1: ',ip1,' IT: ',IT,' Ipt: ',Surf%IP_Tri(:,IT)
             CALL Error_STOP (' ')
          ENDIF

          ipt1 = Surf%IP_Tri(MOD(IL,  3)+1, IT)
          ipt2 = Surf%IP_Tri(MOD(IL+1,3)+1, IT)
          IF(ipt1==ip2 .OR. ipt2==ip2)THEN    !----edge IB exsits
             Mark_Edg(IB) = IT
             CYCLE LOOP_ib
          ENDIF

          ang1 = Included_Angle_2D(Surf%Coord(:,ip2), Surf%Coord(:,ip1), Surf%Coord(:,ipt1))
          ang2 = Included_Angle_2D(Surf%Coord(:,ip2), Surf%Coord(:,ip1), Surf%Coord(:,ipt2))
          IF(ABS(ang1)<TINY .OR. ABS(ang2)<TINY)THEN
             WRITE(*,*)'Error: overlap---'
             WRITE(*,*)'ip1,ip2=',ip1,ip2
             WRITE(*,*)'IT=',IT,' ip=',Surf%IP_Tri(:,IT)
             CALL Error_STOP ('---Surf2D_BoundaryRecovery ')
          ENDIF

          IF(ang1<0 .AND. ang2>0)THEN    !--- got the triangle we need
             EXIT
          ELSE
             IF(ang1>0)THEN
                IT=Surf%Next_Tri(MOD(IL+1,3)+1,IT)
             ELSE
                IT=Surf%Next_Tri(MOD(IL  ,3)+1,IT)
             ENDIF
             IF(IT<=0) CALL Error_STOP ( '---Surf2D_BoundaryRecovery: IT<=0')
          ENDIF
       ENDDO

       !---- starting with IT, explore a string of traingles crossed by edge IB
       !     and swap the diagonal to get the boundary edge

       LOOP_ip3  : DO
          ip3  = ip1
          ITri = IT
          LOOP_ITri : DO

             DO i=1,3
                IF(Surf%IP_Tri(i,ITri)==ip3) IL=i
             ENDDO
             ipt1 = Surf%IP_Tri(MOD(IL,  3)+1, ITri)
             ipt2 = Surf%IP_Tri(MOD(IL+1,3)+1, ITri)
             ITnb = Surf%Next_Tri(IL,ITri)
             ipt3 = SUM(Surf%IP_Tri(:,ITnb)) - ipt1 - ipt2

             ang3 = Included_Angle_2D    &
                  (Surf%Coord(:,ip2), Surf%Coord(:,ip3), Surf%Coord(:,ipt3))

             CALL LinkAssociation_Remove (3, ITri, Surf%IP_Tri(1:3,ITri), Surf%ITR_Pt)
             CALL LinkAssociation_Remove (3, ITnb, Surf%IP_Tri(1:3,ITnb), Surf%ITR_Pt)

             Kcheck = 1
             CALL Surf2D_Swap1Edge(Surf, ITri, ITnb, Kcheck)

             CALL LinkAssociation_AddCell (3, ITri, Surf%IP_Tri(1:3,ITri), Surf%ITR_Pt)
             CALL LinkAssociation_AddCell (3, ITnb, Surf%IP_Tri(1:3,ITnb), Surf%ITR_Pt)

             IF(Kcheck==1)THEN
                !--- swapping not allowed, so change the starting point
                IF(ip3==ip1) IT=ITri
                IF(ang3<0)THEN
                   ip3 = ipt1
                ELSE
                   ip3 = ipt2
                ENDIF
                ITri = ITnb
                CYCLE LOOP_ITri
             ENDIF

             !--- swapping done
             IF(ang3>0) ITri = ITnb      !---refer Surf2D_Swap1Edge
             IF(ipt3==ip2)THEN
                IF(ip3==ip1)THEN
                   !---- edge IB got, exit for next edge
                   Mark_Edg(IB) = ITri
                   EXIT LOOP_ip3
                ELSE
                   !--- edge ip3-ip2 got, but IT is the not boundary edge IB,
                   !--- restart to search the string
                   CYCLE LOOP_ip3
                ENDIF
             ENDIF

          ENDDO LOOP_ITri
       ENDDO LOOP_ip3

       !---check
       Kcheck=0
       DO i=1,3
          IF(Surf%IP_Tri(i,ITri)==ip1) Kcheck=Kcheck+1
          IF(Surf%IP_Tri(i,ITri)==ip2) Kcheck=Kcheck+1
       ENDDO
       IF(Kcheck/=2)THEN
          WRITE(*,*)'Error--- Surf2D_BoundaryRecovery : fail'
          WRITE(*,*)'IB,IP=',IB,ip1,ip2
          WRITE(*,*)'IT,IP=',ITri,Surf%IP_Tri(:,ITri)
          CALL Error_STOP (' ')
       ENDIF

    ENDDO LOOP_ib


    !     Remove those triangles beyond the computational domain                   !
    !                                                                              !

    !---- Colour triangles with a boundary edge

    Mark_Tri(:) = 0

    DO IB = 1 , NB_Corner
       ip1 = ipc(IB)
       ip2 = ipc(inext(IB))
       IT = Mark_Edg(IB)
       IF(IT==0) CALL Error_STOP ( '--- Surf2D_BoundaryRecovery:: IT=0')
       i1 = 0
       i2 = 0
       DO i=1,3
          IF(Surf%IP_Tri(i,IT)==ip1 .AND. Surf%IP_Tri(MOD(i,3)+1,IT)==ip2)THEN
             i1 = MOD(i+1,3)+1
             EXIT
          ELSE IF(Surf%IP_Tri(i,IT)==ip2 .AND. Surf%IP_Tri(MOD(i,3)+1,IT)==ip1)THEN
             i2 = MOD(i+1,3)+1
             EXIT
          ENDIF
       ENDDO
       IF(i1==0)THEN
          IF(i2==0) CALL Error_STOP ( 'wwwrong0')
          it = Surf%Next_Tri(i2,IT)
          IF(it<=0)  CALL Error_STOP ( 'wwwwrong1')
          DO i=1,3
             IF(Surf%IP_Tri(i,IT)==ip1 .AND. Surf%IP_Tri(MOD(i,3)+1,IT)==ip2)THEN
                i1 = MOD(i+1,3)+1
                EXIT
             ENDIF
          ENDDO
          IF(i1==0) CALL Error_STOP ( 'wwwwrong2')
       ENDIF
       Mark_Tri(IT) = 1
       Surf%Next_Tri(i1,IT) = 0
    ENDDO

    !---- Deliver the colours of triangles and edges
    CarryOn = .TRUE.
    DO WHILE(CarryOn)
       CarryOn = .FALSE.
       DO IT=1,Surf%NB_Tri
          IF(Mark_Tri(IT)/=1) CYCLE
          DO i=1,3
             itnb = Surf%Next_Tri(i,IT)
             IF(itnb<=0) CYCLE
             IF(mark_Tri(itnb)>0) CYCLE
             mark_Tri(itnb) = 1
             CarryOn = .TRUE.
          ENDDO
          Mark_Tri(IT) = 2
       ENDDO
    ENDDO

    !---- A check
    DO IT = 1,Surf%NB_Tri
       IF(Mark_Tri(IT)>=1)THEN
          IF(  Surf%IP_Tri(1,IT)>Surf%NB_BD_Point .OR.    &
               Surf%IP_Tri(2,IT)>Surf%NB_BD_Point .OR.    &
               Surf%IP_Tri(3,IT)>Surf%NB_BD_Point )THEN
             WRITE(*,*)'Error--- Boundary_Trim'
             WRITE(*,*)'IT,IP=',IT,Surf%IP_Tri(:,IT)
             CALL Error_STOP (' ')
          ENDIF
       ENDIF
    ENDDO

    !---  Resort triangles by removing those outside the domain

    ntnew = 0
    DO IT = 1,Surf%NB_Tri
       IF(Mark_Tri(IT)>0)THEN
          ntnew = ntnew+1
          Surf%IP_Tri(:,ntnew) = Surf%IP_Tri(:,IT)
       ENDIF
    ENDDO


    Surf%NB_Tri   = ntnew
    Surf%NB_Point = Surf%NB_BD_Point

    DEALLOCATE(Mark_Edg, Mark_Tri)
    CALL LinkAssociation_Clear(Surf%ITR_Pt)


    RETURN
  END SUBROUTINE Surf2D_BoundaryRecovery

  !******************************************************************************!
  !>
  !!     Swap diagonal of two contiguous triangles                                
  !!   @param[in,out]  Surf    :  the updating triangal mesh.
  !!   @param[in]  ITLeft,ITRight   :    the triangles                              
  !!   @param[in]  Kcheck   =0   no check beforehand and always swap.         \n     
  !!                        =1   check before swapping, if fail, return without warning.   \n
  !!                        =2   check before swapping, if fail, return with warning.
  !!   @param[out]  Kcheck  =0   finish swap.               \n                         
  !!                        else no swap.
  !!
  !!   Reminder: Surf%Next_Tri need to be build in advance and will be updated.
  !<
  !******************************************************************************!
  SUBROUTINE Surf2D_Swap1Edge(Surf,ITRight,ITLeft,Kcheck)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(IN) :: ITLeft, ITRight
    INTEGER, INTENT(INOUT) :: Kcheck
    INTEGER :: IL1, IL2, IL3, IR1, IR2, IR3
    INTEGER :: ipL1, ipR1, ipL2, ipL3, i, ITnb, Kpass
    REAL*8  :: TINY=1.d-6
    REAL*8  :: ang1, ang2

    DO i=1,3
       IF(Surf%Next_Tri(i,ITLeft )==ITRight) IL1=i
       IF(Surf%Next_Tri(i,ITRight)==ITLeft ) IR1=i
    ENDDO

    IL2 = MOD(IL1,3)+1
    IL3 = 6-IL1-IL2
    IR2 = MOD(IR1,3)+1
    IR3 = 6-IR1-IR2

    IF(Kcheck>=1)THEN
       ipL1 = Surf%IP_Tri(IL1, ITLeft )
       ipR1 = Surf%IP_Tri(IR1, ITRight)
       ipL2 = Surf%IP_Tri(IL2, ITLeft )
       ipL3 = Surf%IP_Tri(IL3, ITLeft )
       CALL Surf_SwapCheck1(Surf, ipL3, ipL2, ipR1, ipL1, Kcheck, Kpass)
       IF(Kpass==0)THEN
          IF(Kcheck==2)THEN
             WRITE(*,*)'Error--- Surf2D_Swap1Edge : Fail to swap'
             WRITE(*,*)'ITLeft,ITRight=',ITLeft,ITRight
             WRITE(*,*)'ip1,2,3,4 = ',ipL1,ipL2,ipL3,ipR1
             CALL Error_STOP (' ')
          ENDIF
          RETURN
       ENDIF
       Kcheck = 0
    ENDIF

    Surf%IP_Tri(IL2,ITLeft ) = Surf%IP_Tri(IR1,ITRight)
    Surf%IP_Tri(IR2,ITRight) = Surf%IP_Tri(IL1,ITLeft )

    Surf%Next_Tri(IL1,ITLeft ) = Surf%Next_Tri(IR3,ITRight)
    Surf%Next_Tri(IR1,ITRight) = Surf%Next_Tri(IL3,ITLeft )
    Surf%Next_Tri(IL3,ITLeft ) = ITRight
    Surf%Next_Tri(IR3,ITRight) = ITLeft

    ITnb=Surf%Next_Tri(IL1,ITLeft )
    IF(ITnb>0)THEN
       DO i=1,3
          IF(Surf%Next_Tri(i,ITnb)==ITRight) Surf%Next_Tri(i,ITnb)=ITLeft
       ENDDO
    ENDIF

    ITnb=Surf%Next_Tri(IR1,ITRight)
    IF(ITnb>0)THEN
       DO i=1,3
          IF(Surf%Next_Tri(i,ITnb)==ITLeft ) Surf%Next_Tri(i,ITnb)=ITRight
       ENDDO
    ENDIF

    RETURN
  END SUBROUTINE Surf2D_Swap1Edge


END MODULE SurfaceMeshManager2D

