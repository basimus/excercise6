!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!--
!-- MODULE DiffGeometry
!--   SUBROUTINE DiffGeo_Spline(ib,n,r(n),t(n))
!--   SUBROUTINE CubicSegment_Build(p1(3),p1t(3),p2(3),p2t(3),Segm)
!--   SUBROUTINE CubicSegment_Build2D(p1(2),p1t(2),p2(2),p2t(2),Segm)
!--   SUBROUTINE CubicSegment_Build2(p1(3),p2(3),p3(3),p4(3),Segm)
!--   SUBROUTINE CubicSegment_Inverse (Segm)
!--   SUBROUTINE CubicSegment_Length(Segm,as(5))
!--   SUBROUTINE CubicSegment_SegLength(Segm, as(5),u1,u2,s)
!--   SUBROUTINE CubicSegment_Truncate(Segm,as(5),u1,u2,sl)
!--   SUBROUTINE CubicSegment_EvenNodeSet(Segm,as(5),n,ts(n),Isucc)
!--   SUBROUTINE CubicSegment_Interpolate(koutput, Segm, u, p(3), pt(3), ptt(3))
!--   SUBROUTINE CubicSegment_ShapeFunc (t, Ws(4))
!--   SUBROUTINE CubicSegment_EndShift(oldSegm, d(3), newSegm)
!--   SUBROUTINE CubicSegment_EndExpand2D(oldSegm, d(2), newSegm)
!--   SUBROUTINE CubicSegment_EndExpand(oldSegm, d(3), newSegm, <optional> TwistRelax)
!--   FUNCTION CubicSegment_PlaneProject2D(Segm3D,aPlane) RESULT(Segm2D)  
!--   FUNCTION CubicSegment_get3D(Segm2D,aPlane) RESULT(Segm3D)
!--   SUBROUTINE CubicSegment_Display(Segm, N, IO)
!--   SUBROUTINE CubicSegment_Display2(Pts(3,4), N, IO)
!--   SUBROUTINE CubicZone_Build(Ps(3,4), PsU(3,4), PsV(3,4),PsUV(3,4), Zone)
!--   SUBROUTINE CubicZone_Interpolate(koutput,Zone,u,v,R(3),Ru(3),Rv(3),Ruv(3),Ruu(3),Rvv(3))
!--   SUBROUTINE DiffGeo_CalcCurvature(Ru(3),Rv(3),Ruv(3),Ruu(3),Rvv(3), ckmin, isotr, ck1, ck2, dirct(3), Isucc)
!--   SUBROUTINE DiffGeo_CalcNormalCurvature(Ru(3),Rv(3),Ruv(3),Ruu(3),Rvv(3), dirct(3), ck, Isucc)
!--   SUBROUTINE DiffGeo_CalcCurvature2(Wuv, Wuu, Wvv, ckmin, isotr, ck1, ck2, dirct(2))
!--   SUBROUTINE DiffGeo_CalcLineCurvature(Rt(3), Rtt(3), ck, Isucc)
!--   SUBROUTINE DiffGeo_quadsurface(p(3),ps(3,*),mp,Uxis(3),Vxis(3),pn(3),Wuv,Wuu,Wvv,Isucc)
!--   SUBROUTINE CubicZone_Tri_Interpolate(Ps(3,3),Anor(3,3),w(3),P(3))
!--   SUBROUTINE CubicZone_Line_Interpolate(P1(3),Anor1(3),P2(3),Anor2(3),t,P(3))
!--   SUBROUTINE CubicTriFace_Build(Segm1, Segm2, Segm3, P0(3), CTFace) 
!--   SUBROUTINE CubicTriFace_Build2(Pts(3,10), icount(10), CTFace)
!--   SUBROUTINE CubicTriFace_Interpolate (koutput, CTFace, u, v, p(3), pu(3), pv(3))
!--   SUBROUTINE CubicTriFace_ShapeFunc (u, v, Ws(10))
!--   SUBROUTINE CubicTriFace_ShapeFuncDiff (u, v, Ds(10,2))
!--   SUBROUTINE CubicTriFace_getNormal (CTFace, u, v, anor)
!--   SUBROUTINE CubicTriFace_CornerNormal(Pts(3,10), IC, anor(3))
!--   SUBROUTINE CubicTriFace_EdgeNormal(Pts(3,10), IC, anor(3))
!--   SUBROUTINE CubicTriFace_EdgeTangent(Pts(3,10), IC, Pv(3))
!--   SUBROUTINE CubicTriFace_NodeTangent(Pts(3,10), IC, Pv(3))
!--   SUBROUTINE CubicTriFace_NodeNormal(Pts(3,10), IC, anor(3))
!--   SUBROUTINE CubicTriFace_CentreNormal(Pts(3,10), anor(3))
!--   SUBROUTINE CubicTriFace_GetSegm(CTFace, u1,v1, u2,v2, Segm)
!--   SUBROUTINE CubicTriFace_CornerSegm(CTFace, IC, u,v, Segm)
!--   SUBROUTINE CubicTriFace_ProjectFromXYZ_2( CTFace, Pt(3), uv(2), xyz(3), Ifail)
!--   SUBROUTINE CubicTriFace_Display(CTFace, N, IO, inside)
!--   SUBROUTINE CubicTriFace_Display2(Pts(3,10), N, IO, inside)
!--   SUBROUTINE CubicTet_Build(CTFace, Segm1, Segm2, Segm3, p1(3), p2(3), p3(3), CTet)
!--   SUBROUTINE CubicTet_Interpolate (CTet, u, v, w, p(3))



!>
!!
!!  Differential Geometry
!!
!!
!<
MODULE DiffGeometry
  USE CellConnectivity
  USE Geometry3D
  USE Plane3DGeom

  !>
  !!   A type presents a 3D cubic curve.
  !!
  !!    \f$  x = a_{1,1} + a_{1,2} u + a_{1,3} u^2 + a_{1,4} u^3  \f$  \n
  !!    \f$  y = a_{2,1} + a_{2,2} u + a_{2,3} u^2 + a_{2,4} u^3  \f$  \n
  !!    \f$  z = a_{3,1} + a_{3,2} u + a_{3,3} u^2 + a_{3,4} u^3  \f$  \n
  !!   with \f$ 0<=u<=1. \f$                                           \n
  !!   The coefficients \f$ a_{i,j} \f$ is stored by array a(i,j) here.
  !<
  TYPE :: CubicSegment
     REAL*8 :: a(3,4) = 0          !<  the coefficients
     REAL*8 :: Length = 0          !<  the curve length.
  END TYPE CubicSegment

  !>
  !!   A type presents a 3D cubic face.
  !!
  !!    \f$  x = (1,u,u^2,u^3) A_x (1,v,v^2,v^3)^T  \f$  \n
  !!    \f$  y = (1,u,u^2,u^3) A_y (1,v,v^2,v^3)^T  \f$  \n
  !!    \f$  z = (1,u,u^2,u^3) A_z (1,v,v^2,v^3)^T  \f$  \n
  !!   with \f$ 0<=u<=1 \f$ , and \f$ 0<=v<=1. \f$                    \n
  !!   The 4x4 matrice \f$ A_x, A_y, A_z \f$ are stored by array 
  !!   a(1,1:4,1:4), a(2,1:4,1:4), and a(3,1:4,1:4), respectively.
  !<
  TYPE :: CubicZone
     REAL*8 :: a(3,4,4) = 0
  END TYPE CubicZone

  !>
  !!   A type presents a 3D cubic face.
  !!
  !!    \f$  x = (1,u,u^2,u^3) A_x (1,v,v^2,v^3)^T  \f$  \n
  !!    \f$  y = (1,u,u^2,u^3) A_y (1,v,v^2,v^3)^T  \f$  \n
  !!    \f$  z = (1,u,u^2,u^3) A_z (1,v,v^2,v^3)^T  \f$  \n
  !!   with \f$ 0<=u<=1 \f$ , \f$ 0<=v<=1. \f$, and  \f$ 0<=u+v<=1. \f$       \n
  !!   The 4x4 matrice \f$ A_x, A_y, A_z \f$ are stored by array 
  !!   a(1,1:10), a(2,1:10), and a(3,1:10), respectively, as format    \n
  !!                                                                 
  !! \f[ \left( \begin{array}{cccc}
  !!       a1 & a3 & a6 & a10 \\\
  !!       a2 & a5 & a9 &    \\\
  !!       a4 & a8 &    &    \\\
  !!       a7 &    &    & 0 
  !!             \end{array} \right) \f] 
  !!  
  !<
  !             /  a1  a3  a6  a10  \                                       
  !             |  a2  a5  a9       |                                       
  !             !  a4  a8           |
  !             \  a7           0   /  
  TYPE :: CubicTriFace
     REAL*8 :: a(3,10) = 0
  END TYPE CubicTriFace

  !>
  !!   A type presents a 3D tetrahedral domain.
  !!
  !!    \f$  X  = B_3 + B_2*w + B_1*w^2 + A_0*w^3       \f$  \n
  !!    \f$  B_3 = (1,u,u^2,u^3) A_3 (1,v,v^2,v^3)^T  \f$  \n
  !!    \f$  B_2 = (1,u,u^2,) A_2 (1,v,v^2)^T  \f$  \n
  !!    \f$  B_1 = (1,u) A_1 (1,v)^T  \f$  \n
  !!   with \f$ 0<=u<=1, 0<=v<=1, 0<=w<=1. \f$, and  \f$ 0<=u+v+w<=1. \f$       \n
  !!   The matrice \f$ A_3, A_2, A_1, A_0 \f$ are stored by array 
  !!   a(i,1:20), respectively, as format    \n
  !!                                                                 
  !! \f[
  !!    A_3= \left( \begin{array}{cccc}
  !!       a1 & a3 & a7 & a14 \\\
  !!       a2 & a6 & a13 &    \\\
  !!       a5 & a12 &    &    \\\
  !!       a11 &    &    & 0 
  !!             \end{array} \right)   , 
  !!    A_2= \left( \begin{array}{ccc}
  !!       a4  & a9  & a17 \\\
  !!       a8  & a16 &    \\\
  !!       a15 &     & 0 
  !!             \end{array} \right)   , 
  !!    A_1= \left( \begin{array}{cc}
  !!       a10  & a19  \\\
  !!       a18  & 0 
  !!             \end{array} \right)   , 
  !!    A_0= \left( \begin{array}{c}
  !!       a20 \end{array} \right)
  !! \f] 
  !!  
  !<
  !             /  a1  a3  a7  a14  \    / a4  a9  a17 \    / a10  a19 \     ( a20 )
  !             |  a2  a6  a13      |    | a8  a16     |    \ a18   0  /,    
  !             !  a5  a12          |,   \ a15      0  /,
  !             \  a11           0  /  
  TYPE :: CubicTet
     REAL*8 :: a(3,20) = 0
  END TYPE CubicTet

CONTAINS

  !>
  !!    (Code from Oubay).
  !!    Finds the tangents at the points defining a ferguson splines.
  !!    @param[in]  ib : the indicator of the geometrical constraints at the ends. \n
  !!                   = 1 ............ specified tangents: t(1),t(n).             \n
  !!                   = 2 ............ zero second derivatives.  
  !!    @param[in]  n  : the number of nodes.
  !!    @param[in]  r  : the value of each node.
  !!    @param[out] t  : the tangent of each node.
  !!
  !!   note: the number of points is n. the tridiagonal system of
  !!         n-2 equations is solved by gauss elimination and
  !!         posterior backsubstitution.
  !<
  SUBROUTINE DiffGeo_Spline(ib,n,r,t)
    IMPLICIT NONE
    INTEGER, INTENT(in)  :: ib, n
    REAL*8,  INTENT(in)  :: r(n)
    REAL*8,  INTENT(inout) :: t(n)
    REAL*8,  DIMENSION(:), POINTER :: aux
    INTEGER ::  j
    REAL*8  ::  rv, bet

    ! *** first row i = 2.   ib = 1  -> [ 4, 1] ;  ib = 2  -> [ 3.5, 1]
    IF(n<=1) THEN
       WRITE(*,*)' '
       WRITE(*,*) '  Error--- wrong number of points: n=1'
       CALL Error_STOP ( '--- DiffGeo_Spline')
    ELSE IF(n==2) THEN
       t(1) = r(2)-r(1)
       t(2) = t(1)
       RETURN
    ELSE IF(n==3) THEN
       IF(ib==1) THEN
          t(2) = 0.25*(3.*(r(3)-r(1))-t(1)-t(3))
       ELSE
          t(1) = -1.25*r(1)+1.50*r(2)-0.25*r(3)
          t(2) = -0.50*r(1)          +0.50*r(3)
          t(3) =  0.25*r(1)-1.50*r(2)+1.25*r(3)
       ENDIF
       RETURN
    ENDIF

    !--- for n>3 ---
    ALLOCATE(aux(n))

    rv = 3.*(r(3)-r(1))
    IF(ib==1) THEN
       bet = 4.0
       rv  = rv-t(1)
    ELSE
       bet = 3.5
       rv  = rv-1.5*(r(2)-r(1))
    ENDIF
    t(2) = rv/bet

    !--- rows of the type  [ 1,  4,  1 ]
    DO j=3,n-2
       aux(j) = 1./bet
       bet    = 4.-aux(j)
       IF(bet==0.) THEN
          WRITE(*,*)' '
          WRITE(*,*) '  Error--- zero pivot in tridiagonal system'
          CALL Error_STOP ( '--- DiffGeo_Spline')
       ENDIF
       rv   = 3.*(r(j+1)-r(j-1))
       t(j) = (rv-t(j-1))/bet
    ENDDO

    !--- last row i=n-1   ib=1  -> [ 1, 4] ;  ib = 2  -> [ 1, 3.5 ].
    aux(n-1) = 1./bet
    rv       = 3.*(r(n)-r(n-2))
    IF(ib==1) THEN
       bet = 4.
       rv  = rv-t(n)
    ELSE
       bet = 3.5
       rv  = rv-1.5*(r(n)-r(n-1))
    ENDIF
    bet    = bet-aux(n-1)
    IF(bet==0.) THEN
       WRITE(*,*)' '
       WRITE(*,*) '  Error--- zero pivot in tridiagonal system'
       CALL Error_STOP ( '--- DiffGeo_Spline')
    ENDIF
    t(n-1) = (rv-t(n-2))/bet

    !--- backsubstitution.
    DO  j=n-2,2,-1
       t(j) = t(j)-aux(j+1)*t(j+1)
    ENDDO

    !--- end values when ib = 2.
    IF(ib==2) THEN
       t(1) = 1.5*(r(2)-r(1  ))-0.5*t(2)
       t(n) = 1.5*(r(n)-r(n-1))-0.5*t(n-1)
    ENDIF

    DEALLOCATE(aux)
    RETURN
  END  SUBROUTINE DiffGeo_Spline


  !>
  !!    (Code from Oubay)     
  !!    Build a 3D cubic-segment from the two ends.                                                    
  !!   @param[in] p1  : the position of start point (\f$ u=0 \f$) of the segment.
  !!   @param[in] p1t : the tangent (\f$  dp / du \f$)
  !!                    at start point (\f$ u=0 \f$) of the segment.
  !!   @param[in] p2  : the position of end point (\f$ u=1 \f$) of the curve.
  !!   @param[in] p2t : the tangent (\f$  dp / du \f$)
  !!                    at end point (\f$ u=1 \f$) of the curve.
  !!   @param[out] Segm
  !<
  SUBROUTINE CubicSegment_Build(p1,p1t,p2,p2t,Segm)
    IMPLICIT NONE
    REAL*8, INTENT(in)  :: p1(3), p1t(3), p2(3), p2t(3)
    TYPE(CubicSegment), INTENT(out) :: Segm
    INTEGER :: id
    REAL*8  :: r12
    DO id=1,3
       r12          = p2(id) - p1(id)
       Segm%a(id,1) = p1(id)
       Segm%a(id,2) = p1t(id)
       Segm%a(id,3) =  3.d0*r12 -2.d0*p1t(id) - p2t(id)
       Segm%a(id,4) = -2.d0*r12 +     p1t(id) + p2t(id)
    ENDDO
  END SUBROUTINE CubicSegment_Build

  !>
  !!    (Code from Oubay)     
  !!    Build a 2D cubic-segment from the two ends.                                                    
  !!   @param[in] p1  : the position of start point (\f$ u=0 \f$) of the segment.
  !!   @param[in] p1t : the tangent (\f$  dp / du \f$)
  !!                    at start point (\f$ u=0 \f$) of the segment.
  !!   @param[in] p2  : the position of end point (\f$ u=1 \f$) of the curve.
  !!   @param[in] p2t : the tangent (\f$  dp / du \f$)
  !!                    at end point (\f$ u=1 \f$) of the curve.
  !!   @param[out] Segm
  !<
  SUBROUTINE CubicSegment_Build2D(p1,p1t,p2,p2t,Segm)
    IMPLICIT NONE
    REAL*8, INTENT(in)  :: p1(2), p1t(2), p2(2), p2t(2)
    TYPE(CubicSegment), INTENT(out) :: Segm
    INTEGER :: id
    REAL*8  :: r12
    DO id=1,2
       r12          = p2(id) - p1(id)
       Segm%a(id,1) = p1(id)
       Segm%a(id,2) = p1t(id)
       Segm%a(id,3) =  3.d0*r12 -2.d0*p1t(id) - p2t(id)
       Segm%a(id,4) = -2.d0*r12 +     p1t(id) + p2t(id)
    ENDDO
    Segm%a(3,:) = 0
  END SUBROUTINE CubicSegment_Build2D

  !>
  !!    Build a 3D cubic-segment by the 4 points.                                                    
  !!   @param[in] p1,p2,p3,4  : the positions of 4 points (\f$ u=0, 1/3, 2/3, 1 \f$) of the segment.
  !!   @param[out] Segm
  !<
  SUBROUTINE CubicSegment_Build2(p1,p2,p3,p4,Segm)
    IMPLICIT NONE
    REAL*8, INTENT(in)  :: p1(3), p2(3), p3(3), p4(3)
    TYPE(CubicSegment), INTENT(out) :: Segm
    INTEGER :: id
    DO id=1,3
       Segm%a(id,1) = p1(id)
       Segm%a(id,2) = - 5.5d0*p1(id) +  9.0d0*p2(id) -  4.5d0*p3(id) +       p4(id) 
       Segm%a(id,3) =   9.0d0*p1(id) - 22.5d0*p2(id) + 18.0d0*p3(id) - 4.5d0*p4(id)
       Segm%a(id,4) = p4(id) - (Segm%a(id,1)+Segm%a(id,2)+Segm%a(id,3))
    ENDDO
  END SUBROUTINE CubicSegment_Build2


  !>
  !!  Change the direction of u.
  !!  IF a segment built from  CubicSegment_Build (p1, p1t, p2, p2t, Segm),
  !!  then its inverse is that from CubicSegment_Build (p2, -p2t, p1, -p1t, Segm)
  !<
  SUBROUTINE CubicSegment_Inverse (Segm)
    IMPLICIT NONE
    TYPE(CubicSegment), INTENT(INOUT) :: Segm
    INTEGER :: i
    REAL*8  :: a(4)
    DO i = 1,3
       a(1:4) = Segm%a(i,1:4)
       Segm%a(i,1) = a(1) + a(2) +   a(3) +   a(4)
       Segm%a(i,2) =      - a(2) - 2*a(3) - 3*a(4)
       Segm%a(i,3) =                 a(3) + 3*a(4)
       Segm%a(i,4) =                      -   a(4)
    ENDDO
  END SUBROUTINE CubicSegment_Inverse

  !>
  !!   Compute the length of a segment of a cubic curve.
  !!   @param[in,out] Segm  : the segment whose length to be calculated.
  !!   @param[out] as : the coefficients for further culculation (like truncating)
  !<
  SUBROUTINE CubicSegment_Length(Segm,as)
    IMPLICIT NONE
    TYPE(CubicSegment), INTENT(inout) :: Segm
    REAL*8, INTENT(out) :: as(5)
    INTEGER, PARAMETER  :: nit = 20
    REAL*8  :: EPS1,  EPS2,  s, ChordLengthSQ
    REAL*8  :: r12, p, q, pp, qq, t, os, f1,f2,st,ost
    REAL*8  :: del, c, x, ssx, a(3,4)
    INTEGER :: id, kt, it, jt

    a     = Segm%a
    as(1) =          a(1,2)*a(1,2) + a(2,2)*a(2,2) + a(3,2)*a(3,2)
    as(2) =  4.d0* ( a(1,2)*a(1,3) + a(2,2)*a(2,3) + a(3,2)*a(3,3) )
    as(3) =  6.d0* ( a(1,2)*a(1,4) + a(2,2)*a(2,4) + a(3,2)*a(3,4) )    &
         +   4.d0* ( a(1,3)*a(1,3) + a(2,3)*a(2,3) + a(3,3)*a(3,3) )
    as(4) = 12.d0* ( a(1,3)*a(1,4) + a(2,3)*a(2,4) + a(3,3)*a(3,4) )
    as(5) =  9.d0* ( a(1,4)*a(1,4) + a(2,4)*a(2,4) + a(3,4)*a(3,4) )

    EPS1 = 1.d-5
    ChordLengthSQ = (a(1,2)+a(1,3)+a(1,4))**2    &
         +          (a(2,2)+a(2,3)+a(2,4))**2    &
         +          (a(3,2)+a(3,3)+a(3,4))**2 
    EPS2 = 1.d-6 * ChordLengthSQ

    f1   = as(1)
    f2   = as(1)+as(2)+as(3)+as(4)+as(5)
    IF(f1<EPS2 .OR. f2<EPS2) THEN
       !--- Oubay's code, not fully understood here???
       IF(f1<0.0 .OR. f2<0.0) THEN
          s    = 0.
       ELSE
          f1   = SQRT(f1)
          f2   = SQRT(f2)
          s    = 0.5*(f1+f2)
       ENDIF
       Segm%Length = s
       RETURN 
    ENDIF
    f1   = SQRT(f1)
    f2   = SQRT(f2)
    st   = 0.5*(f1+f2)
    ost  = st
    os   = -1.e+30
    kt   = 1

    DO it=1,nit
       del  = 1.d0/kt
       x    = 0.5*del
       ssx  = 0.0
       DO jt=1,kt
          ssx = ssx+SQRT(as(1)+x*(as(2)+x*(as(3)+x*(as(4)+x*as(5)))))
          x   = x+del
       ENDDO
       st   = 0.5*(st+ssx*del)
       s    = (4.*st-ost)/3.
       IF(ABS(s-os) <= EPS1*ABS(os)) EXIT 
       os   = s
       ost  = st
       kt   = kt*2
       IF(it==nit)THEN
          WRITE(*,*)'Error--- number of iterations exceeded. nit,EPS1=',nit,EPS1
          CALL Error_STOP ( '---CubicSegment_Length')
       ENDIF
    ENDDO

    Segm%Length = s

  END SUBROUTINE CubicSegment_Length

  !>
  !!   Compute the length of a part of a segment of a cubic curve.
  !!   @param[in] Segm  : 
  !!   @param[in]   as  : the coefficients, output of function CubicSegment_Length().
  !!   @param[in]   u1  : the start position, 0<=u1<=1.
  !!   @param[in]   u2  : the end position,   u1<=u2<=1.
  !!   @param[out]   s  : the length of the curve between u1 & u2.
  !<
  SUBROUTINE CubicSegment_SegLength(Segm, as,u1,u2,s)
    IMPLICIT NONE
    TYPE(CubicSegment), INTENT(in) :: Segm
    REAL*8, INTENT(in)  :: as(5),u1,u2
    REAL*8, INTENT(out) :: s
    INTEGER, PARAMETER  :: nit = 20
    REAL*8  :: EPS1,  EPS2
    REAL*8  :: r12, p, q, pp, qq, t, os, f1,f2,u21,st,ost
    REAL*8  :: del, c, x, ssx
    INTEGER :: id, kt, it, jt

    EPS1 = 1.d-5
    EPS2 = 1.d-6 * Segm%Length * Segm%Length

    f1   = as(1)+u1*(as(2)+u1*(as(3)+u1*(as(4)+u1*as(5))))
    f2   = as(1)+u2*(as(2)+u2*(as(3)+u2*(as(4)+u2*as(5))))
    IF(f1<EPS2 .OR. f2<EPS2) THEN
       !--- Oubay's code, not fully understood here???
       IF(f1<0.0 .OR. f2<0.0) THEN
          s    = 0.
       ELSE
          f1   = SQRT(f1)
          f2   = SQRT(f2)
          u21  = u2-u1
          s    = 0.5*u21*(f1+f2)
       ENDIF
       RETURN 
    ENDIF
    f1   = SQRT(f1)
    f2   = SQRT(f2)
    u21  = u2-u1
    st   = 0.5*u21*(f1+f2)
    ost  = st
    os   = -1.e+30
    kt   = 1

    DO it=1,nit
       del  = u21/kt
       x    = u1+0.5*del
       ssx  = 0.0
       DO jt=1,kt
          ssx = ssx+SQRT(as(1)+x*(as(2)+x*(as(3)+x*(as(4)+x*as(5)))))
          x   = x+del
       ENDDO
       st   = 0.5*(st+ssx*del)
       s    = (4.*st-ost)/3.
       IF(ABS(s-os) <= EPS1*ABS(os)) RETURN 
       os   = s
       ost  = st
       kt   = kt*2
    ENDDO

    WRITE(*,*)'Error--- number of iterations exceeded. nit,EPS1=',nit,EPS1
    CALL Error_STOP ( '---CubicSegment_SegLength')
  END SUBROUTINE CubicSegment_SegLength

  !>
  !!   Calculates the position u2 of a point on a  Ferguson composite curve 
  !!   such that the length of the cubic segment between u1,u2 is sl. 
  !!   This is calculated by means of a Newton-Raphson iteration. 
  !!   @param[in] Segm  : 
  !!   @param[in]    as : the coefficients, output of function CubicSegment_Length().
  !!   @param[in]    sl : the length of the curve between u1 & u2.
  !!   @param[in]    u1 : the start position, 0<=u1<=1.
  !!   @param[out]   u2 : the end position,   u1<=u2<=1.
  !<
  SUBROUTINE CubicSegment_Truncate(Segm,as,u1,u2,sl)
    IMPLICIT NONE
    TYPE(CubicSegment), INTENT(in) :: Segm
    REAL*8, INTENT(in)  :: as(5),u1,sl
    REAL*8, INTENT(out) :: u2
    INTEGER, PARAMETER  :: nit = 20
    REAL*8  :: EPS1,  EPS2
    REAL*8  :: vi, ss, ff
    INTEGER :: it

    EPS1 = 1.e-06
    EPS2 = 1.e-04 

    ! *** first check the end points

    u2 = sl / Segm%Length
    IF(u2<EPS1) THEN
       u2 = u1
       RETURN
    ELSE IF(ABS(u2-1.)<EPS1 .AND. ABS(u1)<EPS1) THEN
       u2 = 1.
       RETURN
    ENDIF

    u2 = u2 + u1
    DO it=1,nit
       u2 = MIN(u2,1.d+00)
       CALL CubicSegment_SegLength(Segm,as,u1,u2,vi)
       ss = sl-vi
       IF(ABS(ss/sl)<EPS2) RETURN 
       ff = SQRT(as(1)+u2*(as(2)+u2*(as(3)+u2*(as(4)+u2*as(5)))))
       u2 = u2+ss/ff
    ENDDO

    CALL Error_STOP ( '---CubicSegment_Truncate')
  END SUBROUTINE CubicSegment_Truncate

  !>
  !!   Calculates the parameters of a set of dividing points on a  Ferguson composite curve 
  !!   which evenly split the curve with curve length. 
  !!   This is calculated by means of a Newton-Raphson iteration. 
  !!   @param[in] Segm  : 
  !!   @param[in]    as : the coefficients, output of function CubicSegment_Length().
  !!   @param[in]    n  : the number of dividing points (not including two ends).
  !!   @param[out]   ts : the parameters of the dividing points.
  !!   @param[out] Isucc =1, Successful.
  !!                     =0, the iterations not convergent.
  !<
  SUBROUTINE CubicSegment_EvenNodeSet(Segm,as,n,ts,Pts,Isucc)
    IMPLICIT NONE
    TYPE(CubicSegment), INTENT(in) :: Segm
    REAL*8,  INTENT(in)  :: as(5)
    INTEGER, INTENT(in)  :: n
    REAL*8,  INTENT(out) :: ts(n), Pts(3,n)
    INTEGER, INTENT(out) :: Isucc
    INTEGER, PARAMETER   :: nit = 20
    REAL*8,  PARAMETER   :: EPS2 = 1.d-04 
    REAL*8  :: vi, ss, ff, sl, u1, u2, di
    INTEGER :: it, i

    di   = 1.d0 / (n+1)
    sl   = di * Segm%Length
    Isucc = 1

    u1 = 0
    DO i = 1,n
       u2 = u1 + (1.d0-u1) / (n+2-i)
       DO it=1,nit
          u2 = MIN(u2,1.d+00)
          CALL CubicSegment_SegLength(Segm,as,u1,u2,vi)
          ss = sl-vi
          IF(ABS(ss/sl)<EPS2) EXIT 
          ff = SQRT(as(1)+u2*(as(2)+u2*(as(3)+u2*(as(4)+u2*as(5)))))
          u2 = u2+ss/ff
       ENDDO
       u1    = u2
       ts(i) = u2
       CALL CubicSegment_Interpolate (0, Segm, u2, Pts(:,i))
       IF(it>nit) Isucc = 0
    ENDDO

  END SUBROUTINE CubicSegment_EvenNodeSet


  !>
  !!   Returns the 3D coordinates of a point on a cubic-curve         
  !!      from a given local coordinate: 0<=u<=1 .                              
  !!   @param[in] koutput =0 ...... returns position vector p only.   \n                      
  !!                      =1 ...... also returns derivative pt.       \n              
  !!                      =2 ...... also returns second order derivative ptt.   
  !!   @param[in] Segm  : 
  !!   @param[in]  u    : 0<=u<=1 
  !!   @param[out] p,pt,ptt :
  !<
  SUBROUTINE CubicSegment_Interpolate(koutput, Segm, u, p, pt, ptt)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: koutput
    TYPE(CubicSegment), INTENT(in) :: Segm
    REAL*8, INTENT(in)  :: u
    REAL*8, INTENT(out) :: p(3)
    REAL*8, OPTIONAL, INTENT(out) :: pt(3), ptt(3)
    INTEGER :: id
    DO id=1,3
       p(id)   = Segm%a(id,1) + u* (Segm%a(id,2) + u* (Segm%a(id,3) + u* Segm%a(id,4)))
       IF(koutput==0) CYCLE
       IF(.NOT. PRESENT(pt)) CYCLE
       pt(id)  = Segm%a(id,2) + u* (2.d0* Segm%a(id,3) + 3.d0*u* Segm%a(id,4))
       IF(koutput==1) CYCLE
       IF(.NOT. PRESENT(ptt)) CYCLE
       ptt(id) = 2.d0* Segm%a(id,3) + 6.d0*u* Segm%a(id,4)
    ENDDO
  END SUBROUTINE CubicSegment_Interpolate


  !>
  !!   Return the Lagrange shape function at t for a cubic-curve supported by 4 even positions.
  !!   @param[in]  t    : 0<=t<=1 
  !!   @param[out] Ws   : 4 weights.
  !!
  !!   \f$ F(t) = Ws(1)*F(t_1)+Ws(2)*F(t_2)+Ws(3)*F(t_3)+Ws(4)*F(t_4)  \f$
  !!     where \f$ t_i = 0,1/3,2/3,1 \f$ respectively.
  !!
  !!   Ref:  http://http://infohost.nmt.edu/~es421/ansys/shapefnt.htm
  !<
  SUBROUTINE CubicSegment_ShapeFunc(t, Ws)
    IMPLICIT NONE
    REAL*8, INTENT(in)  :: t
    REAL*8, INTENT(out) :: Ws(4)
    REAL*8 :: t3
    t3    = 3.d0  * t
    Ws(1) = 0.5d0 * (1-t)*(2-t3)*(1-t3)
    Ws(2) = 4.5d0 * t*(1-t)*(2-t3)
    Ws(3) = 4.5d0 * t*(1-t)*(t3-1)
    Ws(4) = 0.5d0 * t*(2-t3)*(1-t3)
  END SUBROUTINE CubicSegment_ShapeFunc

  !>
  !!  If the node at t=1 shift a displacement of d(1:3),
  !!     then the old segment will change to the new one by this subroutine gives.
  !!  Comparing with SUBROUTINE CubicSegment_EndExpand(), this subroutine give a simple way
  !!     to shift the segment, BUT not so robust.
  !<
  SUBROUTINE CubicSegment_EndShift(oldSegm, d, newSegm)
    IMPLICIT NONE
    REAL*8, INTENT(in)  :: d(3)
    TYPE(CubicSegment), INTENT(in)  :: oldSegm
    TYPE(CubicSegment), INTENT(out) :: newSegm
    newSegm%a = oldSegm%a
    newSegm%a(:,2) = newSegm%a(:,2) + d(:)
  END SUBROUTINE CubicSegment_EndShift


  !>
  !!  If the node at t=1 shift a displacement of d(1:2),
  !!     then the old segment will change to the new one by this subroutine gives.
  !<
  SUBROUTINE CubicSegment_EndExpand2D(oldSegm, d, newSegm)
    IMPLICIT NONE
    TYPE(CubicSegment), INTENT(in)  :: oldSegm
    TYPE(CubicSegment), INTENT(out) :: newSegm
    REAL*8, INTENT(in)  :: d(2)
    REAL*8 :: dx, dy, dd, u, v
    dx = oldSegm%a(1,2) +  2*oldSegm%a(1,3) + 3*oldSegm%a(1,4)
    dy = oldSegm%a(2,2) +  2*oldSegm%a(2,3) + 3*oldSegm%a(2,4)
    dd = dx*dx + dy*dy
    u  = (d(1)*dx+d(2)*dy) / dd
    v  = (d(2)*dx-d(1)*dy) / dd
    newSegm%a(1,1) = oldSegm%a(1,1)
    newSegm%a(1,2) = oldSegm%a(1,2) + (u*oldSegm%a(1,2)-v*oldSegm%a(2,2))
    newSegm%a(1,3) = oldSegm%a(1,3) + (u*oldSegm%a(1,3)-v*oldSegm%a(2,3)) * 2.d0
    newSegm%a(1,4) = oldSegm%a(1,4) + (u*oldSegm%a(1,4)-v*oldSegm%a(2,4)) * 3.d0
    newSegm%a(2,1) = oldSegm%a(2,1)
    newSegm%a(2,2) = oldSegm%a(2,2) + (u*oldSegm%a(2,2)+v*oldSegm%a(1,2))
    newSegm%a(2,3) = oldSegm%a(2,3) + (u*oldSegm%a(2,3)+v*oldSegm%a(1,3)) * 2.d0
    newSegm%a(2,4) = oldSegm%a(2,4) + (u*oldSegm%a(2,4)+v*oldSegm%a(1,4)) * 3.d0
    newSegm%a(3,:) = oldSegm%a(3,:)
  END SUBROUTINE CubicSegment_EndExpand2D


  !>
  !!  If the node at t=1 shift a displacement of d(1:3),
  !!     then the old segment will change to the new one by this subroutine gives.
  !!  Comparing with SUBROUTINE CubicSegment_EndShift(), this subroutine give a robust way
  !!     to shift the segment so that the two segments have no intercrosses.
  !!
  !!  Preject the old segment to the plane determined by the three nodes 
  !!       (two ends of the old segment and the new position of one end),
  !!      then, expand the 2d segment to a new one 
  !!            by SUBROUTINE CubicSegment_EndExpand2D().
  !!  The twist is defined by the difference of the segment and 
  !!       its prejection on the plane, and this twist is kept to the new segment
  !!       with a factor TwistRelax (if not present, TwistRelax=1).
  !<
  SUBROUTINE CubicSegment_EndExpand(oldSegm, d, newSegm, TwistRelax)
    IMPLICIT NONE
    TYPE(CubicSegment), INTENT(in)  :: oldSegm
    TYPE(CubicSegment), INTENT(out) :: newSegm
    REAL*8, INTENT(in)  :: d(3)
    REAL*8, INTENT(in), OPTIONAL :: TwistRelax
    REAL*8 :: PB(3), P0(3), PP(3), D2D(2)
    TYPE(CubicSegment) :: Segm1, Segm2
    TYPE(Plane3D) :: aPlane 
    PB = oldSegm%a(:,1)
    P0 = oldSegm%a(:,1) + oldSegm%a(:,2) + oldSegm%a(:,3) + oldSegm%a(:,4)
    PP = P0 + d
    aPlane = Plane3D_Build(PP, PB, P0)
    CALL Plane3D_buildTangentAxes(aPlane)
    Segm1 = CubicSegment_PlaneProject2D(oldSegm,aPlane)
    D2D   = Plane3D_project2D(aPlane,d)
    CALL CubicSegment_EndExpand2D(Segm1, D2D, Segm2)
    newSegm = CubicSegment_get3D(Segm2,aPlane)
    Segm2   = CubicSegment_get3D(Segm1,aPlane)
    IF(PRESENT(TwistRelax))THEN
       newSegm%a = newSegm%a + TwistRelax* ( oldSegm%a - Segm2%a )
    ELSE
       newSegm%a = newSegm%a + oldSegm%a - Segm2%a
    ENDIF
  END SUBROUTINE CubicSegment_EndExpand

  !>
  !!   Return to 2D projected segment of a CubicSegment onto a plane.
  !!   @param[in]  Segm3D   : 
  !!   @param[in]  aPlane   : the plane with tangential axes.
  !!   @return     Segm2D   :
  !<
  FUNCTION CubicSegment_PlaneProject2D(Segm3D,aPlane) RESULT(Segm2D)
    IMPLICIT NONE
    TYPE(CubicSegment), INTENT(in)  :: Segm3D
    TYPE(Plane3D), INTENT(IN) :: aPlane
    TYPE(CubicSegment)  :: Segm2D
    INTEGER :: j
    DO j = 1,4
       Segm2D%a(1,j) = Geo3D_Dot_Product(Segm3D%a(:,j), aPlane%tx)
       Segm2D%a(2,j) = Geo3D_Dot_Product(Segm3D%a(:,j), aPlane%ty)
    ENDDO
  END FUNCTION CubicSegment_PlaneProject2D

  !>
  !!   Return the 3D CubicSegment of of a projected CubicSegment on a plane.
  !!   @param[in]  Segm2D   : 
  !!   @param[in]  aPlane   : the plane with tangential axes.
  !!   @return     Segm3D   :
  !<
  FUNCTION CubicSegment_get3D(Segm2D,aPlane) RESULT(Segm3D)
    IMPLICIT NONE
    TYPE(CubicSegment), INTENT(in)  :: Segm2D
    TYPE(Plane3D), INTENT(IN) :: aPlane
    TYPE(CubicSegment)  :: Segm3D
    INTEGER :: j
    !--- For an orthogonal matrix, Transpose = Inverse
    DO j = 1,4
       Segm3D%a(:,j) = aPlane%tx(:)*Segm2D%a(1,j) + aPlane%ty(:)*Segm2D%a(2,j)            
    ENDDO
    Segm3D%a(:,1) = Segm3D%a(:,1) - aPlane%anor(:) * aPlane%d
  END FUNCTION CubicSegment_get3D

  !>
  !! Draw a CubicSegment by a channel.
  !! @param[in]  Segm   the CubicSegment
  !! @param[in]  N        the number of points along on each side.
  !! @param[in]  IO       the IO channel. 
  !<
  SUBROUTINE CubicSegment_Display(Segm, N, IO)
    IMPLICIT NONE
    TYPE(CubicSegment), INTENT(IN) :: Segm
    INTEGER, INTENT(IN)  :: N, IO
    REAL*8   :: u, p(3)
    INTEGER  :: i
    DO i = 0,N
       u = i*1.d0/N
       CALL CubicSegment_Interpolate (0,Segm, u, p)
       WRITE(IO,*)REAL(p)
    ENDDO
    WRITE(IO,*)' '
    WRITE(IO,*)' '
  END SUBROUTINE CubicSegment_Display


  !>
  !! Draw a CubicSegment by a channel.
  !! @param[in]  Pts      the 4 supporting points of a CubicSegment
  !! @param[in]  N        the number of points along on each side.
  !! @param[in]  IO       the IO channel. 
  !<
  SUBROUTINE CubicSegment_Display2(Pts, N, IO)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: Pts(3,4)
    INTEGER, INTENT(IN) :: N, IO
    REAL*8   :: u, p(3), ws(4)
    INTEGER  :: i, m
    DO i = 0,N
       u = i*1.d0/N
       CALL CubicSegment_shapefunc (u, ws)
       p = 0
       DO m = 1,4
          p(:) = p(:) + Pts(:,m)*ws(m)
       ENDDO
       WRITE(IO,*)REAL(p)
    ENDDO
    WRITE(IO,*)' '
    WRITE(IO,*)' '
  END SUBROUTINE CubicSegment_Display2


  !
  !                    (0,1)           (1,1)
  !                      +---------------+
  !                      | 3           4 |       ^ v
  !                      |               |       |
  !                      | 1           2 |       |
  !                      +---------------+       +----> u
  !                    (0,0)           (1,0)
  !>
  !!    (Code from Oubay)     
  !!    Build a 3D cubic-face by four corners.                                                    
  !!   @param[in]  Ps   : the position ( \f$ p \f$ ) of the 4 corners.
  !!   @param[in]  PsU  : the tangent (\f$  \partial p / \partial u \f$) at 4 corners.
  !!   @param[in]  PsV  : the tangent (\f$  \partial p / \partial v \f$) at 4 corners.
  !!   @param[in]  PsUV : the tangent (\f$  \partial^2 p / \partial u \partial v \f$) at 4 corners.
  !!   @param[out] Zone :
  !!
  !!   The four corners in order are \f$ (u,v) = (0,0), (1,0), (0,1), (1,1). \f$
  !<
  SUBROUTINE CubicZone_Build(Ps, PsU, PsV,PsUV, Zone)
    IMPLICIT NONE
    REAL*8, INTENT(in)  :: Ps(3,4), PsU(3,4), PsV(3,4), PsUV(3,4)
    TYPE(CubicZone), INTENT(out) :: Zone
    INTEGER :: id
    REAL*8  :: s1,s2,s3,s4,t1,t2,t3,t4
    DO id=1,3
       s1  = -3.* Ps(id,1) +3.* Ps(id,3) -2.* PsV(id,1) - PsV(id,3)
       s2  = -3.* Ps(id,2) +3.* Ps(id,4) -2.* PsV(id,2) - PsV(id,4)
       s3  = -3.*PsU(id,1) +3.*PsU(id,3) -2.*PsUV(id,1) -PsUV(id,3)
       s4  = -3.*PsU(id,2) +3.*PsU(id,4) -2.*PsUV(id,2) -PsUV(id,4)
       t1  =  2.* Ps(id,1) -2.* Ps(id,3)  +   PsV(id,1) + PsV(id,3)
       t2  =  2.* Ps(id,2) -2.* Ps(id,4)  +   PsV(id,2) + PsV(id,4)
       t3  =  2.*PsU(id,1) -2.*PsU(id,3)  +  PsUV(id,1) +PsUV(id,3)
       t4  =  2.*PsU(id,2) -2.*PsU(id,4)  +  PsUV(id,2) +PsUV(id,4)
       Zone%a(id,1,1)  = Ps(id,1)
       Zone%a(id,1,2)  = PsV(id,1)
       Zone%a(id,1,3)  = s1
       Zone%a(id,1,4)  = t1
       Zone%a(id,2,1)  = PsU(id,1)
       Zone%a(id,2,2)  = PsUV(id,1)
       Zone%a(id,2,3)  = s3
       Zone%a(id,2,4)  = t3
       Zone%a(id,3,1)  = -3.* Ps(id,1) +3.* Ps(id,2) -2.* PsU(id,1) - PsU(id,2)
       Zone%a(id,3,2)  = -3.*PsV(id,1) +3.*PsV(id,2) -2.*PsUV(id,1) -PsUV(id,2)
       Zone%a(id,3,3)  = -3.*s1 +3.*s2 -2.*s3 -s4
       Zone%a(id,3,4)  = -3.*t1 +3.*t2 -2.*t3 -t4
       Zone%a(id,4,1)  =  2.* Ps(id,1) -2.* Ps(id,2) + PsU(id,1) + PsU(id,2)
       Zone%a(id,4,2)  =  2.*PsV(id,1) -2.*PsV(id,2) +PsUV(id,1) +PsUV(id,2)
       Zone%a(id,4,3)  =  2.*s1 -2.*s2 +s3 +s4
       Zone%a(id,4,4)  =  2.*t1 -2.*t2 +t3 +t4
    ENDDO
  END SUBROUTINE CubicZone_Build

  !>
  !!    (Code from Oubay)                                                         
  !!    Returns the 3D coordinates of a point on a Ferguson partch                
  !!     from a given local coordinates 0<u<1; 0<v<1                     
  !!   @param[in] koutput =0 ...... returns position vector R only.   \n                      
  !!                      =1 ...... also returns (R,u); (R,v) and (R,uv) .       \n              
  !!                      =2 ...... also returns (R,uu) and (R,vv).   
  !!   @param[in]  Zone 
  !!   @param[in]  u,v 
  !!   @param[out] R,Ru,Rv,Ruv,Ruu,Rvv
  !<
  SUBROUTINE CubicZone_Interpolate(koutput,Zone,u,v,R,Ru,Rv,Ruv,Ruu,Rvv)
    IMPLICIT NONE
    TYPE(CubicZone), INTENT(in) :: Zone
    INTEGER, INTENT(in) :: koutput
    REAL*8, INTENT(in)  :: u, v
    REAL*8, INTENT(out) :: R(3)
    REAL*8, OPTIONAL, INTENT(out) :: Ru(3), Rv(3), Ruv(3), Ruu(3), Rvv(3)
    INTEGER :: id, i, j
    REAL*8  :: s1,s2,s3,s4, s10,s20,s30,s40
    REAL*8  :: a(4,4)

    DO   id=1,3

       DO i=1,4
          DO j=1,4
             a(i,j) = Zone%a(id,i,j)
          ENDDO
       ENDDO

       s10     = a(1,1) +v*(a(1,2) +v*(a(1,3) +v*a(1,4)))
       s20     = a(2,1) +v*(a(2,2) +v*(a(2,3) +v*a(2,4)))
       s30     = a(3,1) +v*(a(3,2) +v*(a(3,3) +v*a(3,4)))
       s40     = a(4,1) +v*(a(4,2) +v*(a(4,3) +v*a(4,4)))
       R(id)   = s10 +u*(s20 +u*(s30 +u*s40))
       IF(koutput==0) CYCLE       
       IF( .NOT. PRESENT(Ruv) ) CYCLE

       Ru(id)  = s20 +u*(2.d0*s30 +u*3.d0*s40)
       s1      = a(1,2) +v*(2.d0*a(1,3) +v*3.d0*a(1,4))
       s2      = a(2,2) +v*(2.d0*a(2,3) +v*3.d0*a(2,4))
       s3      = a(3,2) +v*(2.d0*a(3,3) +v*3.d0*a(3,4))
       s4      = a(4,2) +v*(2.d0*a(4,3) +v*3.d0*a(4,4))
       Rv(id)  = s1 +u*(s2 +u*(s3 +u*s4))
       Ruv(id) = s2 +u*(2.d0*s3 +u*3.d0*s4)
       IF(koutput==1) CYCLE
       IF( .NOT. PRESENT(Rvv) ) CYCLE

       Ruu(id) = 2.d0*s30 +u*6.d0*s40
       s1      = 2.d0*a(1,3) +v*6.d0*a(1,4)
       s2      = 2.d0*a(2,3) +v*6.d0*a(2,4)
       s3      = 2.d0*a(3,3) +v*6.d0*a(3,4)
       s4      = 2.d0*a(4,3) +v*6.d0*a(4,4)
       Rvv(id) = s1 +u*(s2 +u*(s3 +u*s4))
    ENDDO

    RETURN
  END SUBROUTINE CubicZone_Interpolate

  !>
  !!   Calculate two principal curvatures of a surface point.
  !!   @param[in] Ru   :  the derivative (\f$  \partial (x,y,z) / \partial u \f$).
  !!   @param[in] Rv   :  the derivative (\f$  \partial (x,y,z) / \partial v \f$).
  !!   @param[in] Ruv  :  the derivative (\f$  \partial^2 (x,y,z) / \partial u \partial v\f$).
  !!   @param[in] Ruu  :  the derivative (\f$  \partial^2 (x,y,z) / \partial u^2 \f$).
  !!   @param[in] Rvv  :  the derivative (\f$  \partial^2 (x,y,z) / \partial v^2 \f$).
  !!   @param[in] ckmin : consider anisotropic case only if max(abs(ck1), abs(ck2))>ckmin.
  !!   @param[in] isotr  =true  : no direction output.     \n
  !!                     =false : with direction output.
  !!   @param[out] isotr =true  : it is spherically curved, no direction output.     \n
  !!                     =false : it is anisotropic curved, with direction output.
  !!   @param[out] ck1,ck2 : two principal curvatures.
  !!   @param[out] dirct   : the direction with absolute smaller curvature
  !!                         (i.e. if two curvatures have the same sign, 
  !!                                  this is the direction on which the surface is most flat).
  !!   @param[out] Isucc  =0  : the calculation is not successful (sharp point)   \n
  !!                      =1  : the calculation is successful. 
  !<
  SUBROUTINE DiffGeo_CalcCurvature(Ru, Rv, Ruv, Ruu, Rvv, ckmin, isotr, ck1, ck2, dirct, Isucc)
    IMPLICIT NONE
    REAL*8,  INTENT(IN)  :: Ru(3), Rv(3), Ruv(3), Ruu(3), Rvv(3), ckmin
    REAL*8,  INTENT(OUT) :: ck1, ck2, dirct(3)
    LOGICAL, INTENT(INOUT)  :: isotr
    INTEGER, INTENT(OUT)    :: Isucc
    REAL*8  :: E1,F1,G1,e2,f2,g2, str, w11,w12,w21,w22, r1(3), rmu
    REAL*8  :: det1, cg, cm, ck


    E1      = Ru(1)*Ru(1)+Ru(2)*Ru(2)+Ru(3)*Ru(3)          ! coefficients of first fundamental form
    F1      = Ru(1)*Rv(1)+Ru(2)*Rv(2)+Ru(3)*Rv(3)
    G1      = Rv(1)*Rv(1)+Rv(2)*Rv(2)+Rv(3)*Rv(3)
    det1    = E1*G1-F1*F1
    r1(1:3) = Geo3D_Cross_Product(Ru, Rv)                  !    Ru x Rv
    rmu     = DSQRT(r1(1)*r1(1)+r1(2)*r1(2)+r1(3)*r1(3))   ! || Ru x Rv ||
    r1(1:3) = r1(1:3) / rmu                                ! Unit normal
    e2      = r1(1)*Ruu(1)+r1(2)*Ruu(2)+r1(3)*Ruu(3)       ! coefficients of second fundamental form
    f2      = r1(1)*Ruv(1)+r1(2)*Ruv(2)+r1(3)*Ruv(3)
    g2      = r1(1)*Rvv(1)+r1(2)*Rvv(2)+r1(3)*Rvv(3)
    IF( ABS(det1) < 1.e-24 ) THEN
       Isucc = 0
       RETURN
    ELSE
       Isucc = 1
       cg     = (e2*g2-f2*f2) / det1                       !--- Gaussian Curvature
       cm     = 0.5 * (e2*G1-2.0*f2*F1+g2*E1) / det1       !--- mean Curvature
       ck     = DSQRT(ABS(cm*cm-cg))
       ck1    = cm + ck
       ck2    = cm - ck
       IF(ABS(ck1)<1.d-24) ck1 = 1.d-24
       IF(ABS(ck2)<1.d-24) ck2 = 1.d-24
    ENDIF

    IF(isotr) RETURN

    str = MAX(ABS(ck1),ABS(ck2))
    IF(str<ckmin .OR. ABS((ABS(ck1)-ABS(ck2))/str)<5.d-2 )THEN
       isotr = .TRUE.
       RETURN
    ENDIF

    !  The directions are the eigen vectors of Weigngarten curvature matrix.
    !  (Ref. J. Goldfeather & V. Interrante : 
    !       'acm Transactions on Graphics' Vol. 23 No. 1, Jan. 2004, P.47 )
    w11  = (e2*G1-f2*F1) / det1
    w12  = (f2*E1-e2*F1) / det1
    w21  = (f2*G1-g2*F1) / det1
    w22  = (g2*E1-f2*F1) / det1
    IF(ABS(ck1)>ABS(ck2))THEN
       ck = ck2
    ELSE
       ck = ck1
    ENDIF
    !  the eigenvector for the smaller eigen value (absolute value)
    !  is the direction on which the surface is most flat (if the curvature has the same sign).
    w11 = ck-w11
    w22 = ck-w22
    IF(w11*w11 + w12*w12 > w21*w21 + w22*w22)THEN
       !  take the eigenvector as (w12,w11)
       Dirct(:) = w12*Ru(:) + w11* Rv(:)
    ELSE
       !  take the eigenvector as (w22,w21)
       Dirct(:) = w22*Ru(:) + w21* Rv(:)
    ENDIF

    w11 = DSQRT(Dirct(1)*Dirct(1) + Dirct(2)*Dirct(2) + Dirct(3)*Dirct(3))
    Dirct(:) = Dirct(:) / w11

    RETURN
  END SUBROUTINE DiffGeo_CalcCurvature

  !>
  !!   Calculate the surface curvature alone a given (tangent) direction.
  !!   @param[in] Ru   :  the derivative (\f$  \partial (x,y,z) / \partial u \f$).
  !!   @param[in] Rv   :  the derivative (\f$  \partial (x,y,z) / \partial v \f$).
  !!   @param[in] Ruv  :  the derivative (\f$  \partial^2 (x,y,z) / \partial u \partial v\f$).
  !!   @param[in] Ruu  :  the derivative (\f$  \partial^2 (x,y,z) / \partial u^2 \f$).
  !!   @param[in] Rvv  :  the derivative (\f$  \partial^2 (x,y,z) / \partial v^2 \f$).
  !!   @param[in] dirct : the direction for curvature.
  !!   @param[out] ck   : the curvature.
  !!   @param[out] Isucc  =0  : the calculation is not successful (sharp point)   \n
  !!                      =1  : the calculation is successful. 
  !<
  SUBROUTINE DiffGeo_CalcNormalCurvature(Ru, Rv, Ruv, Ruu, Rvv, dirct, ck, Isucc)
    IMPLICIT NONE
    REAL*8,  INTENT(IN)  :: Ru(3), Rv(3), Ruv(3), Ruu(3), Rvv(3), dirct(3)
    REAL*8,  INTENT(OUT) :: ck
    INTEGER, INTENT(OUT)    :: Isucc
    REAL*8  :: E1,F1,G1,e2,f2,g2, r1(3), rmu, det1, det2, a, b, d1, d2

    Isucc = 0

    E1      = Ru(1)*Ru(1)+Ru(2)*Ru(2)+Ru(3)*Ru(3)          ! coefficients of first fundamental form
    F1      = Ru(1)*Rv(1)+Ru(2)*Rv(2)+Ru(3)*Rv(3)
    G1      = Rv(1)*Rv(1)+Rv(2)*Rv(2)+Rv(3)*Rv(3)
    det1    = E1*G1-F1*F1
    IF( ABS(det1) < 1.e-24 ) RETURN

    r1(1:3) = Geo3D_Cross_Product(Ru, Rv)                  !    Ru x Rv
    rmu     = DSQRT(r1(1)*r1(1)+r1(2)*r1(2)+r1(3)*r1(3))   ! || Ru x Rv ||
    r1(1:3) = r1(1:3) / rmu                                ! Unit normal
    e2      = r1(1)*Ruu(1)+r1(2)*Ruu(2)+r1(3)*Ruu(3)       ! coefficients of second fundamental form
    f2      = r1(1)*Ruv(1)+r1(2)*Ruv(2)+r1(3)*Ruv(3)
    g2      = r1(1)*Rvv(1)+r1(2)*Rvv(2)+r1(3)*Rvv(3)

    !--- find out a,b so that dirct = a*Ru + b*Rv
    d1      = Geo3D_Dot_Product(Ru, dirct)
    d2      = Geo3D_Dot_Product(Rv, dirct)
    a       = (d1*G1 - d2*F1) / det1
    b       = (d2*E1 - d1*F1) / det1    
    det2    = E1*a*a + 2*F1*a*b + G1*b*b
    IF( ABS(det2) < 1.e-24 ) RETURN

    ck = (e2*a*a + 2*f2*a*b + g2*b*b) / det2
    Isucc = 1

    RETURN
  END SUBROUTINE DiffGeo_CalcNormalCurvature

  !>
  !!   Calculate two principal curvatures of a surface point.
  !!   The surface is defined by W=W(u,v) 
  !!      which U & V are axes on the tangent plane of the surface, and
  !!      W is the distance from the surface to the tanggent plane.
  !!
  !!   Compared with SUBROUTINE DiffGeo_CalcCurvature(), we set here:   \n
  !!              Ru(:)  = (/1, 0, 0/)                                \n
  !!              Rv(:)  = (/0, 1, 0/)                                \n
  !!              Ruu(:) = (/0, 0, Wuu/)                              \n
  !!              Ruv(:) = (/0, 0, Wuv/)                              \n
  !!              Rvv(:) = (/0, 0, Wvv/)                              \n
  !!   where Wuu, Wuv, Wvv are inputs.
  !!
  !<
  SUBROUTINE DiffGeo_CalcCurvature2(Wuv, Wuu, Wvv, Uxis,Vxis, ckmin, isotr, ck1, ck2, dirct)
    IMPLICIT NONE
    REAL*8,  INTENT(IN)  :: Wuv, Wuu, Wvv, Uxis(3), Vxis(3), ckmin
    REAL*8,  INTENT(OUT) :: ck1, ck2, dirct(3)
    LOGICAL, INTENT(INOUT)  :: isotr
    REAL*8  :: str, w11,w12,w21,w22
    REAL*8  :: cg, cm, ck

    cg     = (Wuu*Wvv - Wuv*Wuv)                !--- Gaussian Curvature
    cm     = 0.5 * (Wuu + Wvv)                  !--- mean Curvature
    ck     = DSQRT(ABS(cm*cm-cg))
    ck1    = cm + ck
    ck2    = cm - ck
    IF(ABS(ck1)<1.d-24) ck1 = 1.d-24
    IF(ABS(ck2)<1.d-24) ck2 = 1.d-24

    IF(isotr) RETURN

    str = MAX(ABS(ck1),ABS(ck2))
    IF(str<ckmin .OR. ABS((ABS(ck1)-ABS(ck2))/str)<5.d-2 )THEN
       isotr = .TRUE.
       RETURN
    ENDIF

    !  The directions are the eigen vectors of Weigngarten curvature matrix.
    !  (Ref. J. Goldfeather & V. Interrante : 
    !       'acm Transactions on Graphics' Vol. 23 No. 1, Jan. 2004, P.47 )
    w11  = Wuu
    w12  = Wuv
    w21  = Wuv
    w22  = Wvv
    IF(ABS(ck1)>ABS(ck2))THEN
       ck = ck2
    ELSE
       ck = ck1
    ENDIF
    !  the eigenvector for the smaller eigen value (smaller principal curvature)
    !  is the direction on which the surface is most flat.
    w11 = ck-w11
    w22 = ck-w22
    IF(w11*w11 + w12*w12 > w21*w21 + w22*w22)THEN
       !  take the eigenvector as (w12,w11)
       Dirct(:) = w12*Uxis(:) + w11*Vxis(:)
    ELSE
       !  take the eigenvector as (w22,w21)
       Dirct(:) = w22*Uxis(:) + w21*Vxis(:)
    ENDIF

    w11 = DSQRT(Dirct(1)*Dirct(1) + Dirct(2)*Dirct(2)  + Dirct(3)*Dirct(3))
    Dirct(:) = Dirct(:) / w11

    RETURN
  END SUBROUTINE DiffGeo_CalcCurvature2

  !>
  !!   Calculate curvature of a space curve (line).
  !!   @param[in] Rt   :  the derivative (\f$  \partial (x,y,z) / \partial t \f$).
  !!   @param[in] Rtt  :  the derivative (\f$  \partial^2 (x,y,z) / \partial t^2 \f$).
  !!   @param[out] ck  :  the curvature.
  !!   @param[out] Isucc  =0  : the calculation is not successful (sharp point)   \n
  !!                      =1  : the calculation is successful. 
  !<
  SUBROUTINE DiffGeo_CalcLineCurvature(Rt, Rtt, ck, Isucc)
    IMPLICIT NONE
    REAL*8,  INTENT(IN)  :: Rt(3), Rtt(3)
    REAL*8,  INTENT(OUT) :: ck
    INTEGER, INTENT(OUT) :: Isucc
    REAL*8  :: E1,F1,G1,det1

    det1 = Rt(1)*Rt(1) + Rt(2)*Rt(2) + Rt(3)*Rt(3)
    IF( ABS(det1) < 1.e-24 ) THEN
       Isucc = 0
       RETURN
    ENDIF

    E1 = Rtt(3)*Rt(2) - Rtt(2)*Rt(3)
    F1 = Rtt(1)*Rt(3) - Rtt(3)*Rt(1)
    G1 = Rtt(2)*Rt(1) - Rtt(1)*Rt(2)
    ck = DSQRT((E1*E1 + F1*F1 + G1*G1) / det1) / det1
    Isucc = 1

    RETURN
  END SUBROUTINE DiffGeo_CalcLineCurvature

  !>
  !!  Build a quadric surface by a list points surrounding one point
  !!      by using least square method.
  !!
  !!  @param[in]  p  : the coordinates of the based node.
  !!  @param[in]  ps : the coordinates of surrounding points.
  !!  @param[in]  mp : the number of surrounding points.
  !!  @param[in]  pn : the normal direction of the based node.
  !!  @param[in]  Uxis,Vxis  : the axes on the tangent plane.
  !!  @param[out] Wuv,Wuu,Wvv : refer SUBROUTINE DiffGeo_CalcCurvature2()
  !!  @param[out] Isucc : always output 1.
  !!
  !!  Reminder : To avoid sigular solution, we need more than 3 surrounding nodes,
  !!              of which no node is symetry to another about p.
  !<
  SUBROUTINE DiffGeo_quadsurface(p,ps,mp,Uxis,Vxis,pn,Wuv,Wuu,Wvv,Isucc)
    IMPLICIT NONE
    INTEGER, INTENT(IN)  :: mp
    INTEGER, INTENT(OUT) :: Isucc
    REAL*8,  INTENT(IN)  :: p(3), ps(3,*), Uxis(3), Vxis(3), pn(3)
    REAL*8,  INTENT(OUT) :: Wuu, Wuv, Wvv

    INTEGER :: i, j, IP
    REAL*8  :: p1(3), am(3,3), bm(3), aa(3), ploc(3), x(3), det


    !---- compute the coordinates under the new coordinate system.
    !     and form the matrix.

    bm(:)   = 0
    am(:,:) = 0

    DO IP = 1,mp
       p1(:)   = ps(:,IP) - p(:)
       ploc(1) = Geo3D_Dot_Product(p1, Uxis)
       ploc(2) = Geo3D_Dot_Product(p1, Vxis)
       ploc(3) = Geo3D_Dot_Product(p1, pn)

       aa(1) = ploc(1)*ploc(1)
       aa(2) = ploc(1)*ploc(2)
       aa(3) = ploc(2)*ploc(2)

       DO i = 1,3
          bm(i) = bm(i)+aa(i)*ploc(3)
          DO j = 1,3
             am(i,j) = am(i,j)+aa(i)*aa(j)
          ENDDO
       ENDDO
    ENDDO

    !.........Finally, perform the Gauss elimination.....

    CALL Geo3D_Matrix_Solver(am, bm, X, det)

    Wuu    = 2.d0*x(1)
    Wuv    =      x(2)
    Wvv    = 2.d0*x(3)
    Isucc = 1

    RETURN
  END SUBROUTINE DiffGeo_quadsurface

  !>
  !!   Get a surface position by interpolating a quad-triangle with weight w.
  !!    (Code from Desheng)----- no check done at the moment.
  !!    @param[in] Ps   : three nodes of the surface triangle.
  !!    @param[in] Anor : the surface normal direction at the three nodes.
  !!    @param[in] w    : the weight.
  !!    @param[out] P   : the surface position by interpolation.
  !<
  SUBROUTINE CubicZone_Tri_Interpolate(Ps,Anor,w,P)
    IMPLICIT NONE
    REAL*8, INTENT(in)  :: Ps(3,3), Anor(3,3),  w(3)
    REAL*8, INTENT(out) :: P(3)
    REAL*8  :: beta(3), tangent0(3,3), tangent1(3,3), atangent(3)
    REAL*8  :: vside(3), re(3), rf(3)
    REAL*8  :: b123, b1122, b2233, b1133, ttt, t, t2, t3, h0, h1, h0n, h1n
    INTEGER :: i, m, n1, n2
    TYPE(CubicSegment) :: Segm

    IF(ABS(1-w(1))<=1.0e-6)THEN
       P(:) = Ps(:,1)
       RETURN
    ENDIF
    IF(ABS(1-w(2))<=1.0e-6)THEN
       P(:) = Ps(:,2)
       RETURN
    ENDIF
    IF(ABS(1-w(3))<=1.0e-6)THEN
       P(:) = Ps(:,3)
       RETURN
    ENDIF

    !    Define double weights

    b1122 = w(1)*w(1) * w(2)*w(2)
    b2233 = w(2)*w(2) * w(3)*w(3)
    b1133 = w(1)*w(1) * w(3)*w(3)
    b123  = b1122+b2233+b1133
    beta(1) = b2233/b123
    beta(2) = b1133/b123
    beta(3) = b1122/b123

    !    Compute tangent vector using normal information

    DO i = 1,3
       m  =  i + 1
       IF(m==4)   m  =  1

       vside(:)      = Ps(:,m) -Ps(:,i)
       ttt           = Geo3D_Dot_Product(vside(:), Anor(:,i))
       tangent0(:,i) = vside(:) - ttt*Anor(:,i)
       ttt           = Geo3D_Dot_Product(vside(:), Anor(:,m))
       tangent1(:,m) = vside(:) - ttt*Anor(:,m)
    ENDDO

    !    Perform transfinite interpolation - Side/Vertex method

    P(:) = 0.d0
    DO  i = 1,3

       n1 =  i+1
       IF(n1==4) n1 = 1 
       n2 =  n1+1
       IF(n2==4) n2 = 1 

       !    Define interpolant position and tangent on edge

       t     =  w(n2) / (w(n2) + w(n1))
       CALL CubicSegment_Build(Ps(:,n1), tangent0(:,n1), Ps(:,n2), tangent1(:,n2), Segm)
       CALL CubicSegment_Interpolate(0, Segm, t, re)

       atangent(:) =  tangent0(:,n1) + t*(tangent1(:,n2)-tangent0(:,n1))   !---- higher order??

       !   Define interpolant on the surface using side/vertex

       t     =  1.d0 - w(i)
       CALL CubicSegment_Build(Ps(:,i), tangent0(:,i), re, atangent, Segm)  !--- Doubt here??
       CALL CubicSegment_Interpolate(0, Segm, t, rf)

       !   Sum contribution from each side/vertex

       P(:) =  P(:) + beta(i)*rf(:)
    ENDDO

    RETURN
  END SUBROUTINE CubicZone_Tri_Interpolate

  !>
  !!   Get a curve position by interpolating a quad-curve with weight t.
  !!    @param[in] P1,P2       : two ends of the curve.
  !!    @param[in] Anor1,Anor2 : the surface normal direction at the two nodes.
  !!    @param[in] t           : (0<=t<=1; Return P1 if t=0; Return P2 if t=1)
  !!    @param[out] P          : the position of the point on the curve by interpolation.
  !<
  SUBROUTINE CubicZone_Line_Interpolate(P1,Anor1,P2,Anor2,t,P)
    IMPLICIT NONE
    REAL*8, INTENT(in)  :: P1(3), Anor1(3), P2(3), Anor2(3), t
    REAL*8, INTENT(out) :: P(3)
    REAL*8  :: tang1(3), tang2(3), vside(3)
    REAL*8  :: ttt
    TYPE(CubicSegment) :: Segm

    IF(ABS(t)<=1.0e-6)THEN
       P(:) = P1(:)
       RETURN
    ENDIF
    IF(ABS(1-t)<=1.0e-6)THEN
       P(:) = P2(:)
       RETURN
    ENDIF

    vside(:) = P2(:) -P1(:)
    ttt      = Geo3D_Dot_Product(vside, Anor1)
    tang1(:) = vside(:) - ttt*Anor1(:)
    ttt      = Geo3D_Dot_Product(vside, Anor2)
    tang2(:) = vside(:) - ttt*Anor2(:)

    CALL CubicSegment_Build(P1, tang1, P2, tang2, Segm)
    CALL CubicSegment_Interpolate(0, Segm, t, P)

    RETURN
  END SUBROUTINE CubicZone_Line_Interpolate

  !
  !                    p3(0,1)           
  !                      +
  !                      | \              ^ v
  !                      |  \             |
  !                      |   \            |
  !                   s2 |    \ s1        |
  !                      |     \          |
  !                      |      \         |
  !                      |   s3  \        |
  !                      +--------+       +----> u
  !                   p1(0,0)   p2(1,0)
  !>
  !!    Build a 3D cubic-face by three segments and a centre.
  !!    @param[in]  Segm1  : the segment from (1,0) to (0,1).
  !!    @param[in]  Segm2  : the segment from (0,1) to (0,0).
  !!    @param[in]  Segm3  : the segment from (0,0) to (1,0).
  !!    @param[in]  P0     : the position of the centre (1/3,1/3).
  !!    @param[out] CTFace :
  !!
  !<
  SUBROUTINE CubicTriFace_Build(Segm1, Segm2, Segm3, P0, CTFace)
    IMPLICIT NONE
    TYPE(CubicSegment), INTENT(IN) :: Segm1, Segm2, Segm3
    REAL*8, INTENT(IN) :: P0(3)
    TYPE(CubicTriFace), INTENT(OUT) :: CTFace
    REAL*8  :: b(10)
    INTEGER :: i
    DO i = 1,3
       b(1)  =   Segm3%a(i,1)
       b(2)  =   Segm3%a(i,2)
       b(4)  =   Segm3%a(i,3)
       b(7)  =   Segm3%a(i,4)       
       b(10) =  -Segm2%a(i,4)
       b(6)  =   Segm2%a(i,3) - 3*b(10)
       b(3)  =  -Segm2%a(i,2) - Segm2%a(i,3) -b(6)
       b(8)  =   Segm1%a(i,2) + b(2)-b(3)+2*b(4)+3*b(7)      ! =b5+b8
       b(9)  =  -Segm1%a(i,4) - b(7)+b(8)+b(10)              ! =b5+b9
       b(5)  =   27*(p0(i)-b(1)) - 9*(b(2)+b(3)) - 3*(b(4)+b(6)) - (b(7)+b(8)+b(9)+b(10))
       b(8)  =   b(8) - b(5)
       b(9)  =   b(9) - b(5)
       CTFace%a(i,1:10) = b(1:10)
    ENDDO
  END SUBROUTINE CubicTriFace_Build

  !>
  !!   Return the shape function at (u,v) for a cubic-triangle supported by 10 even positions.
  !!   @param[in]  Pts  : the position of 10 points at (0,0), (1,0), (0,1), (1/3,0), (2/3,0), 
  !!                               (0,1/3), (1/3,1/3), (2/3,1/3),  (0,2/3), (1/3,2/3), respectively.
  !!   @param[in] icount : if icount(i)==0 (for i=4~10), then the i-th point is at the linear position,
  !!                       so that the Pts(:,i) will not be refered.
  !!    @param[out] CTFace :
  !<
  SUBROUTINE CubicTriFace_Build2(Pts, icount, CTFace)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: Pts(3,10)
    INTEGER, INTENT(IN) :: icount(10)
    TYPE(CubicTriFace), INTENT(OUT) :: CTFace
    REAL*8, PARAMETER  :: t1=1.d0/3.d0
    REAL*8, PARAMETER  :: t2=2.d0/3.d0
    REAL*8  :: b(10), p(10)
    INTEGER :: i
    DO i = 1,3
       p(1:10) = Pts(i,1:10)
       IF(icount(4)==0)  p(4)  = t1*p(2) + t2*p(1)
       IF(icount(5)==0)  p(5)  = t1*p(1) + t2*p(2)
       IF(icount(6)==0)  p(6)  = t1*p(3) + t2*p(1)
       IF(icount(7)==0)  p(7)  = t1* (p(1)+p(2)+p(3))
       IF(icount(8)==0)  p(8)  = t1*p(3) + t2*p(2)
       IF(icount(9)==0)  p(9)  = t1*p(1) + t2*p(3)
       IF(icount(10)==0) p(10) = t1*p(2) + t2*p(3)
       b(1)  =   p(1)
       b(2)  = - 5.5d0*p(1) + p(2) + 9*p(4) - 4.5d0*p(5)
       b(3)  = - 5.5d0*p(1) + p(3) + 9*p(6) - 4.5d0*p(9)
       b(4)  =   9*p(1) - 4.5d0*p(2) - 22.5d0*p(4) + 18*p(5)
       b(5)  =   18*p(1)- 22.5d0*p(4) + 4.5d0*p(5) - 22.5d0*p(6) + 27*p(7) - 4.5d0*p(8) + 4.5d0*p(9) - 4.5d0*p(10)
       b(6)  =   9*p(1) - 4.5d0*p(3) - 22.5d0*p(6) + 18*p(9)
       b(7)  = - 4.5d0*p(1) + 4.5d0*p(2) + 13.5d0*p(4) - 13.5d0*p(5)
       b(8)  = - 13.5d0*p(1) + 27*p(4) - 13.5d0*p(5) + 13.5d0*p(6) - 27*p(7) + 13.5d0*p(8)
       b(9)  = - 13.5d0*p(1) + 27*p(6) - 13.5d0*p(9) + 13.5d0*p(4) - 27*p(7) + 13.5d0*p(10)
       b(10) = - 4.5d0*p(1) + 4.5d0*p(3) + 13.5d0*p(6) - 13.5d0*p(9)
       CTFace%a(i,1:10) = b(1:10)
    ENDDO
  END SUBROUTINE CubicTriFace_Build2

  !>
  !!    Returns the 3D coordinates of a point on a Cubic triangle                
  !!     from a given local coordinates 0<u<1; 0<v<1; 0<u+v<1                   
  !!   @param[in] koutput =0 ...... returns position vector P only.   \n                      
  !!                      =1 ...... also returns (P,u); (P,v) .   
  !!   @param[in]  CTFace 
  !!   @param[in]  u,v 
  !!   @param[out] p,pu,pv
  !<
  SUBROUTINE CubicTriFace_Interpolate (koutput, CTFace, u, v, p, pu, pv)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: koutput
    TYPE(CubicTriFace), INTENT(IN) :: CTFace
    REAL*8, INTENT(IN)   :: u,v
    REAL*8, INTENT(OUT)  :: p(3)
    REAL*8, OPTIONAL, INTENT(out) :: pu(3), pv(3)
    REAL*8 :: u2, u3, v2, v3, a(10)
    INTEGER :: i
    u2 = u *u
    u3 = u2*u
    v2 = v *v
    v3 = v2*v
    DO i = 1,3
       a(1:10) = CTFace%a(i, 1:10)
       p(i) = ( a(1) +a(2)*u +a(4)*u2 +a(7)*u3 )        &
            + ( a(3) +a(5)*u +a(8)*u2          ) *v     &
            + ( a(6) +a(9)*u                   ) *v2    &
            + ( a(10)                          ) *v3

       IF(koutput==0) CYCLE       
       IF( .NOT. PRESENT(pu) ) CYCLE

       pu(i) =(       a(2)   +a(4)*2*u +a(7)*3*u2 )        &
            + (       a(5)   +a(8)*2*u            ) *v     &
            + (       a(9)                        ) *v2 
       pv(i) =( a(3) +a(5)*u +a(8)*u2          )           &
            + ( a(6) +a(9)*u                   ) *2*v      &
            + ( a(10)                          ) *3*v2
    ENDDO
  END  SUBROUTINE CubicTriFace_Interpolate

  !>
  !!   Return the shape function at (u,v) for a cubic-triangle supported by 10 even positions.
  !!   @param[in]  u,v  : 0<=u<=1, 0<=v<=1, 0<=u+v<=1
  !!   @param[out] Ws   : 10 weights.
  !!
  !!   \f$ F(u,v) = Ws(1)*F(u_1,v_1)+Ws(2)*F(u_2,v_2)+...+Ws(10)*F(u_{10},v_{10})  \f$   \n
  !!     where \f$ (u_i,v_i) \f$ = (0,0), (1,0), (0,1), (1/3,0), (2/3,0), 
  !!                               (0,1/3), (1/3,1/3), (2/3,1/3),  (0,2/3), (1/3,2/3), respectively.
  !!
  !!   Ref: http://www.sd.ruhr-uni-bochum.de/downloads/Shape_funct.pdf
  !<
  SUBROUTINE CubicTriFace_ShapeFunc (u, v, Ws)
    IMPLICIT NONE
    REAL*8, INTENT(IN)   :: u,v
    REAL*8, INTENT(OUT)  :: Ws(10)
    REAL*8 :: w, u3, v3, w3    
    w  = 1-u-v
    u3 = 3*u
    v3 = 3*v
    w3 = 3*w    
    Ws(1) = 0.5d0 * w*(w3-1)*(w3-2)
    Ws(2) = 0.5d0 * u*(u3-1)*(u3-2)
    Ws(3) = 0.5d0 * v*(v3-1)*(v3-2)
    Ws(4) = 0.5d0 * u3*w3*(w3-1)
    Ws(5) = 0.5d0 * w3*u3*(u3-1)
    Ws(6) = 0.5d0 * v3*w3*(w3-1)
    Ws(7) =         u3*v3*w3        
    Ws(8) = 0.5d0 * v3*u3*(u3-1)
    Ws(9) = 0.5d0 * w3*v3*(v3-1)
    Ws(10)= 0.5d0 * u3*v3*(v3-1)
  END SUBROUTINE CubicTriFace_ShapeFunc

  !>
  !!   Return the derivative of shape function at (u,v) for a cubic-triangle supported by 10 even positions.
  !!   @param[in]  u,v  : 0<=u<=1, 0<=v<=1, 0<=u+v<=1
  !!   @param[out] Ds   : 10 weights of derivative.
  !!
  !!   \f$  \frac{\partial}{\partial u} F(u,v)
  !!         = Ds(1,1)F(u_1,v_1)+Ds(2,1)F(u_2,v_2)+...+Ds(10,1)F(u_{10},v_{10})  \f$   \n
  !!   \f$  \frac{\partial}{\partial v} F(u,v)
  !!         = Ds(1,2)F(u_1,v_1)+Ds(2,2)F(u_2,v_2)+...+Ds(10,2)F(u_{10},v_{10})  \f$   \n
  !!     where \f$ (u_i,v_i) \f$ = (0,0), (1,0), (0,1), (1/3,0), (2/3,0), 
  !!                               (0,1/3), (1/3,1/3), (2/3,1/3),  (0,2/3), (1/3,2/3), respectively.
  !!
  !<
  SUBROUTINE CubicTriFace_ShapeFuncDiff (u, v, Ds)
    IMPLICIT NONE
    REAL*8, INTENT(IN)   :: u,v
    REAL*8, INTENT(OUT)  :: Ds(10,2)
    REAL*8 :: w, u3, v3, w3    
    w  = 1-u-v
    u3 = 3*u
    v3 = 3*v
    w3 = 3*w    

    Ds(1,1) = -1.5d0 * w3*(w3-2) -1
    Ds(1,2) =  Ds(1,1)
    Ds(2,1) =  1.5d0 * u3*(u3-2) +1
    Ds(2,2) =  0.0d0
    Ds(3,1) =  0.0d0
    Ds(3,2) =  1.5d0 * v3*(v3-2) +1

    Ds(4,1) =  1.5d0 * (w3*(w3-1)-u3*(2*w3-1))
    Ds(4,2) = -1.5d0 * u3*(2*w3-1)
    Ds(5,1) = -1.5d0 * (u3*(u3-1)-w3*(2*u3-1))
    Ds(5,2) = -1.5d0 * u3*(u3-1)

    Ds(6,1) = -1.5d0 * v3*(2*w3-1)
    Ds(6,2) =  1.5d0 * (w3*(w3-1)-v3*(2*w3-1))
    Ds(7,1) =  3.0d0 * v3*(w3-u3)
    Ds(7,2) =  3.0d0 * u3*(w3-v3)       
    Ds(8,1) =  1.5d0 * v3*(2*u3-1)
    Ds(8,2) =  -Ds(5,2)

    Ds(9,1) = -1.5d0 * v3*(v3-1)
    Ds(9,2) = -1.5d0 * (v3*(v3-1)-w3*(2*v3-1))    
    Ds(10,1) = - Ds(9,1)
    Ds(10,2) =  1.5d0 * u3*(2*v3-1)

  END SUBROUTINE CubicTriFace_ShapeFuncDiff


  !>
  !!    Returns the normal vector a point on a Cubic triangle               
  !!     from a given local coordinates 0<u<1; 0<v<1; 0<u+v<1                   
  !!   @param[in]  CTFace 
  !!   @param[in]  u,v 
  !!   @param[out] anor : the  normal direction (NOT unified) at (u,v).
  !<
  SUBROUTINE CubicTriFace_getNormal (CTFace, u, v, anor)
    IMPLICIT NONE
    TYPE(CubicTriFace), INTENT(IN) :: CTFace
    REAL*8, INTENT(IN)   :: u,v
    REAL*8, INTENT(OUT)  :: anor(3)
    REAL*8 :: pu(3), pv(3)
    REAL*8 :: u2, u3, v2, v3, a(10)
    INTEGER :: i
    u2 = u *u
    u3 = u2*u
    v2 = v *v
    v3 = v2*v
    DO i = 1,3
       a(1:10) = CTFace%a(i, 1:10)
       pu(i) =(       a(2)   +a(4)*2*u +a(7)*3*u2 )        &
            + (       a(5)   +a(8)*2*u            ) *v     &
            + (       a(9)                        ) *v2 
       pv(i) =( a(3) +a(5)*u +a(8)*u2          )           &
            + ( a(6) +a(9)*u                   ) *2*v      &
            + ( a(10)                          ) *3*v2
    ENDDO
    anor = Geo3D_Cross_Product(pu,pv)
  END  SUBROUTINE CubicTriFace_getNormal


  !>
  !!    Calculate the normal direction (NOT unified) at a corner of a 3D cubic-face.
  !!    @param[in]  Pts :  the positions of 10 points defining the face. 
  !!                       Ref. CellConnectivity::Coord_C3Tri for the order.
  !!    @param[in]  IC     =1 for corner (0,0)    \n
  !!                       =2 for corner (1,0)    \n
  !!                       =3 for corner (0,1)
  !!    @param[out]  anor  : the  normal direction (NOT unified) at the corner IC.
  !!
  !<
  SUBROUTINE CubicTriFace_CornerNormal(Pts, IC, anor)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: Pts(3,10)
    INTEGER, INTENT(IN) :: IC
    REAL*8, INTENT(OUT) :: anor(3)
    REAL*8, PARAMETER   :: WDs(4) = (/-5.5d0, 9.0d0, -4.5d0,  1.d0/)
    REAL*8  :: PU(3), PV(3)
    INTEGER :: I
    I    = IC-1
    IF(I==0) I = 3
    PU(:) = WDs(1)*Pts(:,iEdge_C3Tri(1,I)) + WDs(2)*Pts(:,iEdge_C3Tri(3,I))    &
         +  WDs(3)*Pts(:,iEdge_C3Tri(4,I)) + WDs(4)*Pts(:,iEdge_C3Tri(2,I))
    I    = I-1
    IF(I==0) I = 3
    PV(:) = WDs(1)*Pts(:,iEdge_C3Tri(2,I)) + WDs(2)*Pts(:,iEdge_C3Tri(4,I))    &
         +  WDs(3)*Pts(:,iEdge_C3Tri(3,I)) + WDs(4)*Pts(:,iEdge_C3Tri(1,I))
    anor   = Geo3D_Cross_Product(PU, PV)
  END SUBROUTINE CubicTriFace_CornerNormal

  !>
  !!    Calculate the normal direction (NOT unified) of a 3D cubic-faceat at the middle of a edge.
  !!    @param[in]  Pts :  the positions of 10 points defining the face. 
  !!                       Ref. CellConnectivity::Coord_C3Tri for the order.
  !!    @param[in]  IC     =1 for point (0.5,0.5)    \n
  !!                       =2 for point (0,  0.5)    \n
  !!                       =3 for point (0.5,0  )
  !!    @param[out]  anor  : the  normal direction (NOT unified) at the corner IC.
  !!
  !<
  SUBROUTINE CubicTriFace_EdgeNormal(Pts, IC, anor)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: Pts(3,10)
    INTEGER, INTENT(IN) :: IC
    REAL*8, INTENT(OUT) :: anor(3)
    REAL*8, PARAMETER   :: WDs(4) = (/0.125d0, -3.375d0, 3.375d0,  -0.125d0/)
    REAL*8, PARAMETER   :: WDt(10)= (/  1.d0, 6.25d-2,  6.25d-02,  -2.25d0, 1.125d0,    &
         -2.25d0, 6.75d0, -2.8125d0, 1.125d0, -2.8125d0 /)
    REAL*8  :: PU(3), PV(3)
    INTEGER :: I(10)
    I(1:4) = iEdge_C3Tri(1:4,IC)
    PU(:) = WDs(1)*Pts(:,I(1)) + WDs(2)*Pts(:,I(3))    &
         +  WDs(3)*Pts(:,I(4)) + WDs(4)*Pts(:,I(2))
    I(1:10) = iRotate_C3Tri(1:10,IC)
    PV(:) = WDt(1)*Pts(:,I(1)) + WDt(2)*Pts(:,I(2))    &
         + WDt(3)*Pts(:,I(3)) + WDt(4)*Pts(:,I(4))    &
         + WDt(5)*Pts(:,I(5)) + WDt(6)*Pts(:,I(6))    &
         + WDt(7)*Pts(:,I(7)) + WDt(8)*Pts(:,I(8))    &
         + WDt(9)*Pts(:,I(9)) + WDt(10)*Pts(:,I(10))
    anor   = Geo3D_Cross_Product(PU, PV)
  END SUBROUTINE CubicTriFace_EdgeNormal


  !>
  !!    Calculate the tangent direction (NOT unified) of a 3D cubic-faceat at the middle of a edge
  !!           towards to opposite vertex.
  !!    @param[in]  Pts :  the positions of 10 points defining the face. 
  !!                       Ref. CellConnectivity::Coord_C3Tri for the order.
  !!    @param[in]  IC     =1 for point (0.5,0.5)    \n
  !!                       =2 for point (0,  0.5)    \n
  !!                       =3 for point (0.5,0  )
  !!    @param[out] Pv  : the  tangent direction (NOT unified) at the middle of edge IC.
  !!
  !<
  SUBROUTINE CubicTriFace_EdgeTangent(Pts, IC, Pv)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: Pts(3,10)
    INTEGER, INTENT(IN) :: IC
    REAL*8, INTENT(OUT) :: Pv(3)
    REAL*8, PARAMETER   :: WDt(10)= (/  1.d0, 6.25d-2,  6.25d-02,  -2.25d0, 1.125d0,    &
         -2.25d0, 6.75d0, -2.8125d0, 1.125d0, -2.8125d0 /)
    INTEGER :: I(10)
    I(1:10) = iRotate_C3Tri(1:10,IC)
    PV(:) = WDt(1)*Pts(:,I(1)) + WDt(2)*Pts(:,I(2))    &
         + WDt(3)*Pts(:,I(3)) + WDt(4)*Pts(:,I(4))    &
         + WDt(5)*Pts(:,I(5)) + WDt(6)*Pts(:,I(6))    &
         + WDt(7)*Pts(:,I(7)) + WDt(8)*Pts(:,I(8))    &
         + WDt(9)*Pts(:,I(9)) + WDt(10)*Pts(:,I(10))
  END SUBROUTINE CubicTriFace_EdgeTangent

  !>
  !!    Calculate the tangent direction (NOT unified) of a 3D cubic-faceat at the edge node
  !!           towards to the longitude line.
  !!    @param[in]  Pts :  the positions of 10 points defining the face. 
  !!                       Ref. CellConnectivity::Coord_C3Tri for the order.
  !!    @param[in]  IC     =4:6, 8:10
  !!    @param[out] Pv  : the  tangent direction (NOT unified) at the node IC.
  !!
  !<
  SUBROUTINE CubicTriFace_NodeTangent(Pts, IC, Pv)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: Pts(3,10)
    INTEGER, INTENT(IN) :: IC
    REAL*8, INTENT(OUT) :: Pv(3)
    REAL*8, PARAMETER   :: WDt(10)= (/-1.d0, 0.d0, 1.d0, -4.5d0, 0.d0, 3.d0, 6.d0, 0.d0, -3.d0, -1.5d0/)
    INTEGER :: I(10)
    IF(IC==4)THEN
       I(1:10) = iRotate_C3Tri(1:10, 1)
    ELSE IF(IC==5)THEN
       I(1:10) = iRotate_C3Tri(1:10,-2)
    ELSE IF(IC==6)THEN
       I(1:10) = iRotate_C3Tri(1:10,-1)
    ELSE IF(IC==8)THEN
       I(1:10) = iRotate_C3Tri(1:10, 2)
    ELSE IF(IC==9)THEN
       I(1:10) = iRotate_C3Tri(1:10, 3)
    ELSE IF(IC==10)THEN
       I(1:10) = iRotate_C3Tri(1:10,-3)
    ELSE
       CALL Error_Stop('CubicTriFace_NodeTangent:: wrong node')
    ENDIF
    PV(:) = WDt(1)*Pts(:,I(1)) + WDt(2)*Pts(:,I(2))    &
         + WDt(3)*Pts(:,I(3)) + WDt(4)*Pts(:,I(4))    &
         + WDt(5)*Pts(:,I(5)) + WDt(6)*Pts(:,I(6))    &
         + WDt(7)*Pts(:,I(7)) + WDt(8)*Pts(:,I(8))    &
         + WDt(9)*Pts(:,I(9)) + WDt(10)*Pts(:,I(10))
  END SUBROUTINE CubicTriFace_NodeTangent


  !>
  !!    Calculate the normal direction (NOT unified) of a 3D cubic-faceat at the edge node
  !!           towards to the longitude line.
  !!    @param[in]  Pts :  the positions of 10 points defining the face. 
  !!                       Ref. CellConnectivity::Coord_C3Tri for the order.
  !!    @param[in]  IC     =1:10
  !!    @param[out] anor : the  normal direction (NOT unified) at the node IC.
  !!
  !<
  SUBROUTINE CubicTriFace_NodeNormal(Pts, IC, anor)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: Pts(3,10)
    INTEGER, INTENT(IN) :: IC
    REAL*8, INTENT(OUT) :: anor(3)
    REAL*8 :: Pu(3), Pv(3)
    REAL*8, PARAMETER   :: WDs(4) = (/-1.d0, -0.5d0,  -1.5d0,  3.0d0/)
    REAL*8, PARAMETER   :: WDt(10)= (/-1.d0, 0.d0, 1.d0, -4.5d0, 0.d0, 3.d0, 6.d0, 0.d0, -3.d0, -1.5d0/)
    INTEGER :: I(10)
    IF(IC<=3)THEN
       CALL CubicTriFace_CornerNormal(Pts, IC, anor)
       RETURN
    ELSE IF(IC==4)THEN
       I(1:10) = iRotate_C3Tri(1:10, 1)
    ELSE IF(IC==5)THEN
       I(1:10) = iRotate_C3Tri(1:10,-2)
    ELSE IF(IC==6)THEN
       I(1:10) = iRotate_C3Tri(1:10,-1)
    ELSE IF(IC==7)THEN
       CALL CubicTriFace_CentreNormal(Pts, anor)
       RETURN
    ELSE IF(IC==8)THEN
       I(1:10) = iRotate_C3Tri(1:10, 2)
    ELSE IF(IC==9)THEN
       I(1:10) = iRotate_C3Tri(1:10, 3)
    ELSE IF(IC==10)THEN
       I(1:10) = iRotate_C3Tri(1:10,-3)
    ELSE
       CALL Error_Stop('CubicTriFace_NodeTangent:: wrong node')
    ENDIF
    PU(:) = WDs(1)*Pts(:,I(1)) + WDs(2)*Pts(:,I(2))    &
         +  WDs(3)*Pts(:,I(4)) + WDs(4)*Pts(:,I(5))
    IF(IC==5 .OR. IC==6 .OR. IC==10) PU = -PU
    PV(:) = WDt(1)*Pts(:,I(1)) + WDt(2)*Pts(:,I(2))    &
         + WDt(3)*Pts(:,I(3)) + WDt(4)*Pts(:,I(4))    &
         + WDt(5)*Pts(:,I(5)) + WDt(6)*Pts(:,I(6))    &
         + WDt(7)*Pts(:,I(7)) + WDt(8)*Pts(:,I(8))    &
         + WDt(9)*Pts(:,I(9)) + WDt(10)*Pts(:,I(10))
    anor   = Geo3D_Cross_Product(PU, PV)
  END SUBROUTINE CubicTriFace_NodeNormal


  !>
  !!    Calculate the normal direction (NOT unified) of a 3D cubic-faceat at the centre.
  !!    @param[in]  Pts :  the positions of 10 points defining the face. 
  !!                       Ref. CellConnectivity::Coord_C3Tri for the order.
  !!    @param[out]  anor  : the  normal direction (NOT unified) at the corner IC.
  !!
  !<
  SUBROUTINE CubicTriFace_CentreNormal(Pts, anor)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: Pts(3,10)
    REAL*8, INTENT(OUT) :: anor(3)
    REAL*8  :: PU(3), PV(3)
    INTEGER :: I(10)
    I(1:10) = iRotate_C3Tri(1:10,1)
    PU(:) = Pts(:,I(1))-Pts(:,I(3)) + 3*(-Pts(:,I(4))-Pts(:,I(6))+Pts(:,I(9))+Pts(:,I(10)))
    I(1:10) = iRotate_C3Tri(1:10,2)
    PV(:) = Pts(:,I(1))-Pts(:,I(3)) + 3*(-Pts(:,I(4))-Pts(:,I(6))+Pts(:,I(9))+Pts(:,I(10)))
    anor  = Geo3D_Cross_Product(PU, PV)
    anor  = anor / 4.d0
  END SUBROUTINE CubicTriFace_CentreNormal


  !>
  !!    truncate a 3D cubic-face from two points to get a segment.
  !!    @param[in]  CTFace :
  !!    @param[in]  u1,v1    the position of the face to fix the start of the segment.
  !!    @param[in]  u2,v2    the position of the face to fix the end of the segment.
  !!    @param[out]  Segm  : the segment, on which the node (u1,v1) is at t=0 and 
  !!                                      the node (u2,v2) is at t=1.
  !!
  !<
  SUBROUTINE CubicTriFace_GetSegm(CTFace, u1,v1, u2,v2, Segm)
    IMPLICIT NONE
    TYPE(CubicTriFace), INTENT(IN) :: CTFace
    REAL*8,  INTENT(IN)  :: u1, v1, u2, v2
    TYPE(CubicSegment), INTENT(OUT) :: Segm
    REAL*8  :: p1t(3), p2t(3), p1(3), p2(3), p1u(3), p1v(3), p2u(3), p2v(3)
    CALL CubicTriFace_Interpolate (1, CTFace, u1, v1, p1, p1u, p1v)
    CALL CubicTriFace_Interpolate (1, CTFace, u2, v2, p2, p2u, p2v)
    P1t(:) = p1u(:)*(u2-u1) + p1v(:)*(v2-v1)
    P2t(:) = p2u(:)*(u2-u1) + p2v(:)*(v2-v1)
    CALL CubicSegment_Build (p1, p1t, p2, p2t, Segm)
  END SUBROUTINE CubicTriFace_GetSegm


  !>
  !!    truncate a 3D cubic-face from one corner to get a segment.
  !!    @param[in]  CTFace :
  !!    @param[in]  IC     =1 for corner (0,0)    \n
  !!                       =2 for corner (1,0)    \n
  !!                       =3 for corner (0,1)
  !!    @param[in]  u,v    the position of the face to fix another end of the segment.
  !!    @param[out]  Segm  : the segment, on which the corner IC is at t=0 and 
  !!                                      the node (u,v) is at t=1.
  !!
  !<
  SUBROUTINE CubicTriFace_CornerSegm(CTFace, IC, u,v, Segm)
    IMPLICIT NONE
    TYPE(CubicTriFace), INTENT(IN) :: CTFace
    INTEGER, INTENT(IN)  :: IC
    REAL*8,  INTENT(IN)  :: u, v
    TYPE(CubicSegment), INTENT(OUT) :: Segm
    REAL*8  :: a(10), pu, pv, p1t(3), p2t(3), p1(3), p2(3), p2u(3), p2v(3)
    INTEGER :: i

    CALL CubicTriFace_Interpolate (1, CTFace, u, v, p2, p2u, p2v)

    DO i = 1,3
       a(:) = CTFace%a(i,:)
       IF(IC==1)THEN
          pu     = a(2) 
          pv     = a(3)
          P1t(i) = pu*u + pv*v
          P2t(i) = p2u(i)*u + p2v(i)*v
          P1(i)  = a(1)
       ELSE IF(IC==2)THEN
          pu     = a(2) + a(4)*2 + a(7)*3
          pv     = a(3) + a(5)   + a(8)
          P1t(i) = pu*(u-1) + pv*v
          P2t(i) = p2u(i)*(u-1) + p2v(i)*v
          p1(i)  = a(1) + a(2) + a(4) + a(7)
       ELSE
          pu     = a(2) + a(5)   + a(9)      
          pv     = a(3) + a(6)*2 + a(10)*3
          P1t(i) = pu*u + pv*(v-1)
          P2t(i) = p2u(i)*u + p2v(i)*(v-1)
          p1(i)  = a(1) + a(3) + a(6) + a(10)
       ENDIF
    ENDDO

    CALL CubicSegment_Build (p1, p1t, p2, p2t, Segm)
  END SUBROUTINE CubicTriFace_CornerSegm


  !>
  !!  Calculates the coordinates u,v in the    
  !!    of a point on a cubic triangle projected by their 3D point
  !!    using a selective splitting in u and v of the triangle.
  !!  This routine is an alternate when SUBROUTINE CubicTriFace_ProjectFromXYZ() fails.
  !!
  !!  @param[in] CTFace  : the surface CTFace.
  !!  @param[in] Pt      : the 3D coordinates of the point.
  !!  @param[in] TOLG    : the tolerance
  !!  @param[out] uv     : the 2D coordinates of the image on the triangle.  
  !!  @param[out] xyz    : the exact position on the surface with uv.
  !!  @param[out] Ifail  : Flag for termination process.   \n
  !!                     =  0 ... success.                   \n
  !!                     =  1 ... iterations exceeded.       \n
  !!                     = -1 ... failure.                   \n
  !<
  SUBROUTINE CubicTriFace_ProjectFromXYZ_2( CTFace, Pt, uv, xyz, Ifail)
    IMPLICIT NONE
    TYPE(CubicTriFace), INTENT(in) :: CTFace
    INTEGER, INTENT(out) :: Ifail
    REAL*8,  INTENT(in)  :: Pt(3)
    REAL*8,  INTENT(out) :: uv(2), xyz(3)
    REAL*8  :: EPS
    REAL*8  :: uc, vc, aur, avr, dis, dis1, vmd, Dp(3), gk(2), Ru(3), Rv(3)
    INTEGER :: iu, iv, iuc, ivc, iter, nbnd, nit, lu, lv
    REAL*8  :: du(35) = (/ 3.2d-1,1.8d-1,1.0d-1, 5.6d-2,3.2d-2,1.8d-2,1.0d-2,   &
         5.6d-3,3.2d-3,1.8d-3,1.0d-3, 5.6d-4,3.2d-4,1.8d-4,1.0d-4,   &
         5.6d-5,3.2d-5,1.8d-5,1.0d-5, 5.6d-6,3.2d-6,1.8d-6,1.0d-6,   &
         5.6d-7,3.2d-7,1.8d-7,1.0d-7, 5.6d-8,3.2d-8,1.8d-8,1.0d-8,   &
         5.6d-9,3.2d-9,1.8d-9,1.0d-9 /)
    REAL*8  :: dv(35) = (/ 3.2d-1,1.8d-1,1.0d-1, 5.6d-2,3.2d-2,1.8d-2,1.0d-2,   &
         5.6d-3,3.2d-3,1.8d-3,1.0d-3, 5.6d-4,3.2d-4,1.8d-4,1.0d-4,   &
         5.6d-5,3.2d-5,1.8d-5,1.0d-5, 5.6d-6,3.2d-6,1.8d-6,1.0d-6,   &
         5.6d-7,3.2d-7,1.8d-7,1.0d-7, 5.6d-8,3.2d-8,1.8d-8,1.0d-8,   &
         5.6d-9,3.2d-9,1.8d-9,1.0d-9 /)

    Ifail = 0

    uv(:) = 0.333d0
    CALL CubicTriFace_Interpolate(1, CTFace, uv(1), uv(2), xyz, Ru,Rv)
    EPS = 1.d-6 * MAX(Geo3D_Distance(Ru), Geo3D_Distance(Rv))
    Dp = xyz - Pt
    Dis = Geo3D_Distance(Dp)
    IF(Dis<Eps) RETURN
    gk(1) = 2.* Geo3D_Dot_Product(Dp, Ru)
    gk(2) = 2.* Geo3D_Dot_Product(Dp, Rv)
    vmd   = SQRT(gk(1)**2+gk(2)**2)
    IF(vmd < EPS * Dis) RETURN

    nit   = 100
    lu    = 1
    lv    = 1
    nbnd  = 3
    Loop_200 : DO iter  = 1, nit
       aur = uv(1)
       avr = uv(2)
       iuc = 0
       ivc = 0
       DO iu = -nbnd, nbnd           
          uc = aur + iu*du(lu)
          IF(uc<0) CYCLE
          IF(uc>1)  CYCLE
          DO iv = -nbnd, nbnd
             IF(iu==0 .AND. iv==0) CYCLE
             vc = avr + iv*dv(lv)
             IF(vc<0)    CYCLE
             IF(uc+vc>1) CYCLE

             CALL CubicTriFace_Interpolate(1, CTFace, uc, vc, xyz, Ru,Rv)
             Dp = xyz - Pt
             Dis1 = Geo3D_Distance(Dp)
             IF(dis1<dis) THEN 
                uv(:) = (/uc, vc/)
                iuc = iu
                ivc = iv
                dis = dis1
                IF(dis<EPS) RETURN
                gk(1) = 2.* Geo3D_Dot_Product(Dp, Ru)
                gk(2) = 2.* Geo3D_Dot_Product(Dp, Rv)
                vmd   = SQRT(gk(1)**2+gk(2)**2)
                IF(vmd < EPS * Dis) RETURN
             ENDIF
          ENDDO
       ENDDO

       IF(iuc==0 .AND. lu<35 .AND. lu<lv+3)  lu = lu+1
       IF(ivc==0 .AND. lv<35 .AND. lv<lu+3)  lv = lv+1
       IF(lu==36 .AND. lv==35)THEN
          IFail = -1
          RETURN
       ENDIF
    ENDDO Loop_200

    Ifail = 1

    RETURN
  END SUBROUTINE CubicTriFace_ProjectFromXYZ_2


  !>
  !! Draw a CubicTriFace by a channel.
  !! @param[in]  CTFace   the CubicTriFace
  !! @param[in]  N        the number of points along on each side.
  !! @param[in]  IO       the IO channel. 
  !! @param[in]  inside   =0 no inside detail (boundary only).   \n
  !!                      =1 with inside detail
  !<
  SUBROUTINE CubicTriFace_Display(CTFace, N, IO, inside)
    IMPLICIT NONE
    TYPE(CubicTriFace), INTENT(IN) :: CTFace
    INTEGER, INTENT(IN)  :: N, IO, inside
    REAL*8   :: u,v, p1(3)
    REAL*8, DIMENSION(3,0:N,0:N) :: p
    INTEGER  :: i, j, k

    IF(inside==0)THEN

       DO i = 0,N
          u = i*1.d0/N
          v = 0
          CALL CubicTriFace_Interpolate (0,CTFace, u, v, p1)
          WRITE(IO,*)REAL(p1)
       ENDDO
       WRITE(IO,*)' '
       WRITE(IO,*)' '
       DO j = 0,N
          v = j*1.d0/N
          u = 0
          CALL CubicTriFace_Interpolate (0,CTFace, u, v, p1)
          WRITE(IO,*)REAL(p1)
       ENDDO
       WRITE(IO,*)' '
       WRITE(IO,*)' ' 
       DO i = 0, N
          j = N-i
          u = i*1.d0/N
          v = j*1.d0/N
          CALL CubicTriFace_Interpolate (0,CTFace, u, v, p1)
          WRITE(IO,*)REAL(p1)
       ENDDO

    ELSE

       DO i = 0,N
          u = i*1.d0/N
          DO j = 0, N-i
             v = j*1.d0/N
             CALL CubicTriFace_Interpolate (0,CTFace, u, v, p(:,i,j))
             WRITE(IO,*)REAL(p(:,i,j))
          ENDDO
          WRITE(IO,*)' '
       ENDDO
       WRITE(IO,*)' '
       WRITE(IO,*)' '
       DO j = 0,N
          DO i = 0, N-j
             WRITE(IO,*)REAL(p(:,i,j))
          ENDDO
          WRITE(IO,*)' '
       ENDDO
       WRITE(IO,*)' '
       WRITE(IO,*)' ' 
       DO k = 0,N
          DO i = 0, k
             j = k-i
             WRITE(IO,*)REAL(p(:,i,j))
          ENDDO
          WRITE(IO,*)' '
       ENDDO

    ENDIF

  END SUBROUTINE CubicTriFace_Display

  !>
  !! Draw a CubicTriFace by a channel.
  !! @param[in]  Pts      the 10 supporting points of a CubicTriFace
  !! @param[in]  N        the number of points along on each side.
  !! @param[in]  IO       the IO channel. 
  !! @param[in]  inside   =0 no inside detail (boundary only).   \n
  !!                      =1 with inside detail
  !<
  SUBROUTINE CubicTriFace_Display2(Pts, N, IO, inside)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: Pts(3,10)
    INTEGER, INTENT(IN)  :: N, IO, inside
    REAL*8   :: u,v, p1(3), ws(10)
    REAL*8, DIMENSION(3,0:N,0:N) :: p
    INTEGER  :: i, j, k, m

    IF(inside==0)THEN

       DO i = 0,N
          u = i*1.d0/N
          v = 0
          CALL CubicTriFace_shapefunc (u,v, ws)
          p1 = 0
          DO m = 1,10
             p1(:) = p1(:) + Pts(:,m)*ws(m)
          ENDDO
          WRITE(IO,*)REAL(p1)
       ENDDO
       WRITE(IO,*)' '
       WRITE(IO,*)' '
       DO j = 0,N
          v = j*1.d0/N
          u = 0
          CALL CubicTriFace_shapefunc (u,v, ws)
          p1 = 0
          DO m = 1,10
             p1(:) = p1(:) + Pts(:,m)*ws(m)
          ENDDO
          WRITE(IO,*)REAL(p1)
       ENDDO
       WRITE(IO,*)' '
       WRITE(IO,*)' ' 
       DO i = 0, N
          j = N-i
          u = i*1.d0/N
          v = j*1.d0/N
          CALL CubicTriFace_shapefunc (u,v, ws)
          p1 = 0
          DO m = 1,10
             p1(:) = p1(:) + Pts(:,m)*ws(m)
          ENDDO
          WRITE(IO,*)REAL(p1)
       ENDDO

    ELSE

       DO i = 0,N
          u = i*1.d0/N
          DO j = 0, N-i
             v = j*1.d0/N
             CALL CubicTriFace_shapefunc (u,v, ws)
             p(:,i,j) = 0
             DO m = 1,10
                p(:,i,j) = p(:,i,j) + Pts(:,m)*ws(m)
             ENDDO
             WRITE(IO,*)REAL(p(:,i,j))
          ENDDO
          WRITE(IO,*)' '
       ENDDO
       WRITE(IO,*)' '
       WRITE(IO,*)' '
       DO j = 0,N
          DO i = 0, N-j
             WRITE(IO,*)REAL(p(:,i,j))
          ENDDO
          WRITE(IO,*)' '
       ENDDO
       WRITE(IO,*)' '
       WRITE(IO,*)' ' 
       DO k = 0,N
          DO i = 0, k
             j = k-i
             WRITE(IO,*)REAL(p(:,i,j))
          ENDDO
          WRITE(IO,*)' '
       ENDDO

    ENDIF

  END SUBROUTINE CubicTriFace_Display2

  !>
  !!    Build a 3D cubic-tet domain.
  !!    @param[in]  CTFace : the basic triangle which match u-v coordinates for w=0.
  !!    @param[in]  Segm1  : the segment from (0,0,0) to (0,0,1).
  !!    @param[in]  Segm2  : the segment from (1,0,0) to (0,0,1).
  !!    @param[in]  Segm3  : the segment from (0,1,0) to (0,0,1).
  !!    @param[in]  P1     : the position of the centre (1/3,1/3,1/3).
  !!    @param[in]  P2     : the position of the centre ( 0, 1/3,1/3).
  !!    @param[in]  P3     : the position of the centre (1/3, 0, 1/3).
  !!    @param[out] CTet   :
  !!
  !<
  SUBROUTINE CubicTet_Build(CTFace, Segm1, Segm2, Segm3, p1, p2, p3, CTet)
    IMPLICIT NONE
    TYPE(CubicTriFace), INTENT(IN) :: CTFace
    TYPE(CubicSegment), INTENT(IN) :: Segm1, Segm2, Segm3
    REAL*8, INTENT(IN) :: P1(3), P2(3), P3(3)    
    TYPE(CubicTet), INTENT(OUT) :: CTet
    REAL*8  :: b(20)
    INTEGER :: i
    DO i = 1,3
       b(1:3)   =   CTFace%a(i,1:3)
       b(5:7)   =   CTFace%a(i,4:6)
       b(11:14) =   CTFace%a(i,7:10)

       b(4)  =   Segm1%a(i,2)
       b(10) =   Segm1%a(i,3)
       b(20) =   Segm1%a(i,4)

       b(15) =   Segm2%a(i,2) + b(2)+2*b(5)+3*b(11)-b(4)    ! =b8+b15
       b(18) = - Segm2%a(i,4) - b(11)+b(15)+b(20)           ! =b8+b18
       b(8)  =   27*(p3(i)-b(1)) - 9*(b(2)+b(4)) - 3*(b(5)+b(10)) - (b(11)+b(15)+b(18)+b(20))
       b(15) =   b(15) - b(8)
       b(18) =   b(18) - b(8)

       b(17) =   Segm3%a(i,2) + b(3)+2*b(7)+3*b(14)-b(4)    ! =b9+b17
       b(19) = - Segm3%a(i,4) - b(14)+b(17)+b(20)           ! =b9+b19
       b(9)  =   27*(p2(i)-b(1)) - 9*(b(3)+b(4)) - 3*(b(7)+b(10)) - (b(14)+b(17)+b(19)+b(20))
       b(17) =   b(17) - b(9)
       b(19) =   b(19) - b(9)

       b(16) =   27*(p1(i)-b(1)) - 9*(b(2)+b(3)+b(4)) - 3*(b(5)+b(6)+b(7)+b(8)+b(9)+b(10))   &
            - (b(11)+b(12)+b(13)+b(14)+b(15)+b(17)+b(18)+b(19)+b(20))
       CTet%a(i,1:20) = b(1:20)
    ENDDO
  END SUBROUTINE CubicTet_Build

  !>
  !!    Returns the 3D coordinates of a point on a cubic tetrahedral domain                
  !!     from a given local coordinates 0<u<1; 0<v<1; 0<w<1; 0<u+v+w<1                   
  !!   @param[in]  CTet 
  !!   @param[in]  u,v,w
  !!   @param[out] p
  !<
  SUBROUTINE CubicTet_Interpolate (CTet, u, v, w, p)
    IMPLICIT NONE
    TYPE(CubicTet), INTENT(IN) :: CTet
    REAL*8, INTENT(IN)   :: u,v,w
    REAL*8, INTENT(OUT)  :: p(3)
    REAL*8 :: u2, u3, v2, v3, w2, w3, a(20), b1, b2, b3
    INTEGER :: i
    u2 = u *u
    v2 = v *v
    w2 = w *w
    u3 = u2*u
    v3 = v2*v
    w3 = w2*w    
    DO i = 1,3
       a(1:20) = CTet%a(i, 1:20)
       b1      = a(1)+a(2)*u+a(5)*u2+a(11)*u3    &
            + (a(3)+a(6)*u+a(12)*u2) *v + (a(7)+a(13)*u) *v2 + a(14)*v3
       b2      = a(4)+a(8)*u+a(15)*u2 + (a(9)+a(16)*u) *v + a(17)*v2
       b3      = a(10) + a(18)*u + a(19)*v
       p(i)    = b1 + b2*w + b3*w2 + a(20)*w3
    ENDDO
  END  SUBROUTINE CubicTet_Interpolate


END MODULE DiffGeometry



!>
!!
!!   Ruben's code
MODULE JACOBIANS
  USE CellConnectivity
  USE Geometry3D
  USE MatrixAlgebra

CONTAINS

  !========================================================================

  !>
  !!   Compute maximum and minimam jacobians of a high-order tetrahedron.
  !!   @param[in] numGaussNodes : the number of Gauss points to check the Jacobians.
  !!   @param[in] Pts      : the positions of nodes on the high-order tetrahedron.
  !!   @param[in] dN       : the coefficians (output of SUBROUTINE computeShapeFunctions() ).
  !!   @param[out] JacMax  : the maximum of Jacobians over the Gauss points.
  !!   @param[out] JacMin  : the minimum of Jacobians over the Gauss points.
  !<
  SUBROUTINE computeJacobians(numGaussNodes, Pts, dN, JacMax, JacMin)
    IMPLICIT NONE

    REAL*8  :: JacMax, JacMin, Pts(:,:), dN(:,:,:)
    INTEGER :: numGaussNodes

    ! Pts is numCellNodes x nOfSpatialDimensions
    ! dN is nOfSpatialDimensions x numCellNodes x numGaussNodes

    INTEGER :: iGauss
    REAL*8  :: J(3,3), detJ

    JacMax = -1.0D12
    JacMin =  1.0D12
    DO iGauss=1,numGaussNodes
       J = MATMUL(Pts, dN(:,:,iGauss))
       detJ = Geo3D_Matrix_Determinant(J)

       JacMax = MAX(JacMax, detJ)
       JacMin = MIN(JacMin, detJ)
    END DO

  END SUBROUTINE computeJacobians

  !========================================================================

  !>
  !!   Compute the coefficians for calculating the Jacobians at Gauss points.
  !!   ref. SUBROUTINE computeJacobians()
  !<
  SUBROUTINE computeShapeFunctions(GridOrder, dN)
    IMPLICIT NONE

    REAL*8  :: dN(:,:,:)
    INTEGER :: GridOrder

    INTEGER :: numCellNodes, numGaussNodes, allocateStatus, iGauss, iG, jG, kG, numGaussNodes1D
    REAL*8, ALLOCATABLE ::  N(:,:), coord(:,:), V(:,:), gaussPoints1D(:), gaussWeights1D(:)
    INTEGER, ALLOCATABLE :: indexLU(:)
    REAL*8 :: gaussPointRST(3), gaussPointXiEta(3)

    numCellNodes   = Npoint_Tet(GridOrder)
    numGaussNodes1D = GridOrder + 1
    numGaussNodes   = numGaussNodes1D**3

    ALLOCATE(coord(numCellNodes,3), stat=allocateStatus)
    CALL equallySpacedNodes(coord, GridOrder)

    ALLOCATE(gaussPoints1D(numGaussNodes1D), stat=allocateStatus)
    ALLOCATE(gaussWeights1D(numGaussNodes1D), stat=allocateStatus)
    CALL gaussLegendre1D(gaussPoints1D, gaussWeights1D, numGaussNodes1D, -1.D0, 1.D0)

    ALLOCATE(N(numCellNodes,numGaussNodes), stat=allocateStatus)

    ALLOCATE(V(numCellNodes,numCellNodes), stat=allocateStatus)
    CALL vandermondeMatrix3D(V, GridOrder, coord)
    V = TRANSPOSE(V)

    ALLOCATE(indexLU(numCellNodes), stat=allocateStatus)
    CALL Matrix_LU_Decomposition(numCellNodes, V, indexLU)

    iGauss = 1
    DO iG = 1,numGaussNodes1D
       DO jG = 1,numGaussNodes1D
          DO kG = 1,numGaussNodes1D
             gaussPointRST = (/ gaussPoints1D(iG), gaussPoints1D(jG), gaussPoints1D(kG) /)
             CALL orthoPolyDerivRST3D(N(:,iGauss),dN(:,1,iGauss),dN(:,2,iGauss),dN(:,3,iGauss),gaussPointRST,GridOrder)
             CALL Matrix_LU_Solver(numCellNodes,V,indexLU,dN(:,1,iGauss))
             CALL Matrix_LU_Solver(numCellNodes,V,indexLU,dN(:,2,iGauss))
             CALL Matrix_LU_Solver(numCellNodes,V,indexLU,dN(:,3,iGauss))
             iGauss = iGauss + 1
          END DO
       END DO
    END DO

    DEALLOCATE(N, V, gaussPoints1D, gaussWeights1D, indexLU, coord, stat=allocateStatus)
    IF(allocateStatus/=0) STOP "ERR->computeShapeFunctions: unable to deallocate"

  END SUBROUTINE computeShapeFunctions

  !========================================================================

  SUBROUTINE equallySpacedNodes(points, GridOrder)
    IMPLICIT NONE

    REAL*8 :: points(:,:)
    INTEGER :: GridOrder

    INTEGER :: i, j, k, l
    REAL*8 :: step

    step = 2.0D0/DBLE(GridOrder)

    points(1,:) = (/ -1.0D0, -1.0D0, -1.0D0 /)
    points(2,:) = (/  1.0D0, -1.0D0, -1.0D0 /)
    points(3,:) = (/ -1.0D0,  1.0D0, -1.0D0 /)
    points(4,:) = (/ -1.0D0, -1.0D0,  1.0D0 /)

    l = 5
    DO i=1,GridOrder-1
       points(l,:) = (/ -1.0D0 + i*step, -1.0D0, -1.0D0  /)
       l = l + 1
    END DO

    DO j=1,GridOrder-1
       DO i=0,GridOrder-j
          points(l,:) = (/ -1.0D0 + i*step, -1.0D0 + j*step, -1.0D0 /)
          l = l + 1
       END DO
    END DO

    DO k=1,GridOrder-1
       DO j=0,GridOrder-k
          DO i=0,GridOrder-k-j
             points(l,:) = (/ -1.0D0 + i*step, -1.0D0 + j*step, -1.0D0 + k*step  /)
             l = l + 1
          END DO
       END DO
    END DO

  END SUBROUTINE equallySpacedNodes

  !========================================================================

  ! gaussLegendre1D(x,w,N,a,b)
  !
  ! This script is for computing definite integrals using Legendre-Gauss 
  ! Quadrature. Computes the Legendre-Gauss nodes and weights  on an interval
  ! [a,b] with truncation order N
  !
  ! Suppose you have a continuous function f(x) which is defined on [a,b]
  ! which you can evaluate at any x in [a,b]. Simply evaluate it at all of
  ! the values contained in the x vector to obtain a vector f. Then compute
  ! the definite integral using sum(f.*w)
  !
  !
  ! Written by Greg von Winckel - 02/25/2004
  SUBROUTINE gaussLegendre1D(points, weights, nOfPoints, a, b)
    IMPLICIT NONE

    REAL*8, PARAMETER :: PI = 3.1415926535897931D0
    REAL*8, PARAMETER :: EPS = 2.0D-16
    REAL*8 :: points(:), weights(:)
    INTEGER :: nOfPoints
    REAL*8 :: a, b 

    INTEGER :: n, n1, n2, i, allocateStatus, j, k
    REAL*8, ALLOCATABLE :: xu(:), y(:), L(:,:), Lp(:), y0(:)
    REAL*8 :: step

    n  = nOfPoints - 1
    n1 = nOfPoints
    n2 = nOfPoints + 1

    step = 2.d0/(nOfPoints-1.0D0)
    ALLOCATE(xu(nOfPoints), stat=allocateStatus)
    DO i=1,nOfPoints
       xu(i) = -1 + (i-1.0D0)*step
    END DO

    ALLOCATE(y(n1), stat=allocateStatus)
    y = COS((2.0D0*(/ (i,i=0,n) /)+1.0D0)*PI/(2.0D0*n+2.0D0))    &
         + (0.27D0/n1)*SIN(PI*xu*DBLE(n)/n2);

    ALLOCATE(L(n1,n2), stat=allocateStatus)
    ALLOCATE(Lp(n1),   stat=allocateStatus)

    ALLOCATE(y0(n1), stat=allocateStatus)
    y0 = 2.0D0

    DO WHILE(MAXVAL(ABS(y-y0))>EPS)
       L(:,1) = 1.0D0 
       L(:,2) = y

       DO k=2,n1
          L(:,k+1) = ( (2.0D0*k-1.0D0)*(y*L(:,k))-(k-1.0D0)*L(:,k-1.0D0) )/k
       END DO

       Lp = n2*( L(:,n1)-y*L(:,n2) )/(1.0D0-y**2)
       y0 = y
       y  = y0 - L(:,n2)/Lp
    END DO

    points = 0.5D0*(a*(1.0D0-y) + b*(1.0D0+y))
    weights= (b-a)/((1.0D0-y**2)*Lp**2)*(DBLE(n2)/n1)**2

    DEALLOCATE(xu,y,y0,L,Lp, stat=allocateStatus)

  END SUBROUTINE gaussLegendre1D

  !========================================================================
  !>
  !! [pol,dp] = orthoPolyDeriv1D(x,n)
  !! Computes the ortogonal base of 1D polynomials of degree less 
  !! or equal to n at the point x in [-1,1]
  !<

  SUBROUTINE orthoPolyDeriv1D(pol,dp,x,GridOrder)
    IMPLICIT NONE

    INTEGER :: GridOrder
    REAL*8 :: x
    REAL*8 :: pol(:), dp(:)
    INTEGER :: i
    REAL*8 :: factor

    pol(1) = 1.0D0/SQRT(2.0D0)
    dp(1) = 0.0D0
    DO i=1,GridOrder
       factor = SQRT(i+0.5D0)
       CALL jacobiPoly(pol(i+1),x,0.0D0,0.0D0,i)
       pol(i+1) = pol(i+1)*factor
       CALL jacobiPoly(dp(i+1),x,1.0D0,1.0D0,i-1)
       dp(i+1) = dp(i+1)*factor*0.5D0*(i+1)
    END DO

  END SUBROUTINE orthoPolyDeriv1D


  !========================================================================
  !>
  !! [pol,dp/dxi,dp/deta] = orthoPolyDerivRST2D(x,GridOrder)
  !! Computes the ortogonal base of 2D polynomials of degree less 
  !! or equal to GridOrder at the point x=(r,s) in [-1,1]^2
  !<

  SUBROUTINE orthoPolyDerivRST2D(pol,dpXi,dpEta,x,GridOrder)
    IMPLICIT NONE

    INTEGER :: GridOrder
    REAL*8 :: x(2)
    REAL*8 :: pol(:), dpXi(:), dpEta(:)
    INTEGER :: degCount, kDeg, i, j
    REAL*8 :: r, s, xi, eta, drXi, drEta, pi, dpi, pj, dpj, qi, dqi, dpR, dpS, factor

    r = x(1)
    s = x(2)

    xi = (1.0D0+r)*(1.0D0-s)/2.0D0 - 1.0D0
    eta = s

    drXi  = 2.0D0/(1.0D0-eta)
    drEta = 2.0D0*(1.0D0+xi)/(1.0D0-eta)**2

    degCount = 0

    DO kDeg = 0,GridOrder
       DO i=0,kDeg
          IF(i==0) THEN
             pi = 1.0D0
             qi = 1.0D0
             dpi = 0.0D0
             dqi = 0.0D0
          ELSE
             CALL jacobiPoly(pi,r,0.0D0,0.0D0,i)
             qi = 0.5D0*qi*(1.0D0-s)
             CALL jacobiPoly(dpi,r,1.0D0,1.0D0,i-1)
             dpi = 0.5D0*dpi*(i+1.0D0)
             dqi = qi*i/(s-1.0D0)
          END IF

          j = kDeg - i
          IF(j==0) THEN
             pj = 1.0D0
             dpj = 0.0D0
          ELSE
             CALL jacobiPoly(pj,s,2.0D0*i+1.0D0,0.0D0,j)
             CALL jacobiPoly(dpj,s,2.0D0*i+2.0D0,1.0D0,j-1)
             dpj = 0.5D0*dpj*(j+2.0D0*i+2.0D0)
          END IF

          degCount = degCount + 1
          factor = SQRT( (i+0.5D0)*(i+j+1.0D0) )
          pol(degCount) = pi*qi*pj*factor

          dpR = dpi*qi*pj*factor
          dpS = ( pi*(dqi*pj + qi*dpj) )*factor

          dpXi(degCount) = dpR*drXi
          dpEta(degCount) = dpR*drEta + dpS
       END DO
    END DO

  END SUBROUTINE orthoPolyDerivRST2D

  !========================================================================
  !>
  !! [pol,dp/dxi,dp/deta,dp/dzet] = orthoPolyDerivRST3D(x,GridOrder)
  !! Computes the ortogonal base of 3D polynomials of degree less 
  !! or equal to GridOrder at the point x=(r,s,t) in [-1,1]^3
  !<

  SUBROUTINE orthoPolyDerivRST3D(pol,dpXi,dpEta,dpZet,x,GridOrder)
    IMPLICIT NONE

    INTEGER :: GridOrder
    REAL*8 :: x(3)
    REAL*8 :: pol(:), dpXi(:), dpEta(:), dpZet(:)
    INTEGER :: degCount, kDeg, i, j, k
    REAL*8 :: r, s, t, xi, eta, zet, drXi, drEta, drZet, dsEta, dsZet, auxI
    REAL*8 :: ppi, dpi, pj, dpj, pk, dpk, qi, dqi, qj, dqj, dpR, dpS, dpT, factor

    r = x(1)
    s = x(2)
    t = x(3)

    eta = 0.5D0*(s-s*t-1.0D0-t)
    xi = -0.5D0*(r+1.0D0)*(eta+t)-1.0D0
    zet = t

    drXi  = -2.0D0/(eta+zet)
    drEta = 2.0D0*(1.0D0+xi)/((eta+zet)**2)
    drZet = drEta

    dsEta = 2.0D0/(1.0D0-zet)
    dsZet = 2.0D0*(1.0D0+eta)/((1.0D0-zet)**2)

    degCount = 0

    DO kDeg = 0,GridOrder
       DO i=0,kDeg
          IF(i==0) THEN
             ppi = 1.0D0
             qi = 1.0D0
             dpi = 0.0D0
             dqi = 0.0D0
          ELSE
             CALL jacobiPoly(ppi,r,0.0D0,0.0D0,i)			
             qi = 0.5D0*qi*(1.0D0-s)
             CALL jacobiPoly(dpi,r,1.0D0,1.0D0,i-1)
             dpi = dpi*0.5D0*(i+1.0D0)
             dqi = -qi*i/(1.0D0-s)
          END IF

          auxI = 2.0D0*i+1.0D0
          DO j = 0,kDeg-i
             IF(j==0) THEN
                pj = 1.0D0
                qj = (0.5D0*(1.0D0-t))**i
                dpj = 0
             ELSE
                CALL jacobiPoly(pj,s,auxI,0.0D0,j)
                qj = 0.5D0*qj*(1.0D0-t)
                CALL jacobiPoly(dpj,s,2.0D0*i+2.0D0,1.0D0,j-1)
                dpj = dpj*0.5D0*(j+2.0D0*i+2.0D0)
             END IF
             dqj = -qj*(i+j)/(1.0D0-t)

             k = kDeg-(i+j)
             IF(k==0) THEN
                pk = 1.0D0
                dpk = 0.0D0
             ELSE
                CALL jacobiPoly(pk,t,2.0D0*(i+j)+2.0D0,0.0D0,k)
                CALL jacobiPoly(dpk,t,2.0D0*(i+j)+3.0D0,1.0D0,k-1)
                dpk = dpk*0.5D0*(k+2.0D0*i+2.0D0*j+3.0D0)
             END IF
             degCount = degCount+1
             factor = SQRT( 0.25D0*auxI*(i+j+1.0D0)*(2.0D0*(i+j+k)+3.0D0) )            
             pol(degCount) = ppi*qi*pj*qj*pk*factor

             dpR = dpi*qi*pj*qj*pk*factor
             dpS = ppi*(dqi*pj + qi*dpj)*qj*pk*factor
             dpT = ppi*qi*pj*(dqj*pk + qj*dpk)*factor

             dpXi(degCount)  = dpR*drXi
             dpEta(degCount) = dpR*drEta + dpS*dsEta
             dpZet(degCount) = dpR*drZet + dpS*dsZet + dpT
          END DO
       END DO
    END DO

  END SUBROUTINE orthoPolyDerivRST3D

  !========================================================================
  !>
  !! pol = orthopoly1D(x,GridOrder)
  !! Computes the ortogonal base of 1D polynomials of degree less 
  !! or equal to GridOrder at the point x in [-1,1]
  !<
  SUBROUTINE orthoPoly1D(pol,x,GridOrder)
    IMPLICIT NONE

    INTEGER :: GridOrder
    REAL*8 :: x
    REAL*8 :: pol(:)
    INTEGER :: i

    DO i=0,GridOrder
       CALL jacobiPoly(pol(i+1),x,0.0D0,0.0D0,i)
       pol(i+1) = pol(i+1)*SQRT(i+0.5D0)
    END DO

  END SUBROUTINE orthoPoly1D

  !========================================================================
  !>
  !! p = orthopoly2D(x,GridOrder)
  !! Computes the ortogonal base of 2D polynomials of degree less 
  !! or equal to GridOrder at the point x=(xi,eta) in the reference triangle
  !<

  SUBROUTINE orthoPoly2D(pol,x,GridOrder)
    IMPLICIT NONE

    INTEGER :: GridOrder
    REAL*8 :: x(2)
    REAL*8 :: pol(:)
    REAL*8 :: xRST(2)

    CALL mapXiEtaZetaToRST2D(xRST, x)
    CALL orthoPolyRST2D(pol,xRST,GridOrder)

  END SUBROUTINE orthoPoly2D

  !========================================================================
  !>
  !! p = orthopoly3D(x,GridOrder)
  !! Computes the ortogonal base of 3D polynomials of degree less 
  !! or equal to GridOrder at the point x=(xi,eta,zeta) in the reference tetrahedra
  !<

  SUBROUTINE orthoPoly3D(pol,x,GridOrder)
    IMPLICIT NONE

    INTEGER :: GridOrder
    REAL*8 :: x(3)
    REAL*8 :: pol(:)
    REAL*8 :: xRST(3)

    CALL mapXiEtaZetaToRST3D(xRST, x)
    CALL orthoPolyRST3D(pol,xRST,GridOrder)

  END SUBROUTINE orthoPoly3D

  !========================================================================
  !>
  !! p = orthoPolyRST2D(x,GridOrder)
  !! Computes the ortogonal base of 2D polynomials of degree less 
  !! or equal to GridOrder at the point x=(r,s) in [-1,1]^2
  !<

  SUBROUTINE orthoPolyRST2D(pol,x,GridOrder)
    IMPLICIT NONE

    INTEGER :: GridOrder
    REAL*8 :: x(2)
    REAL*8 :: pol(:)
    INTEGER :: degCount, kDeg, i, j
    REAL*8 :: r, s, pi, pj, qi, factor

    r = x(1)
    s = x(2)
    degCount = 0

    DO kDeg = 0,GridOrder
       DO i=0,kDeg
          IF(i==0) THEN
             pi = 1.0D0
             qi = 1.0D0
          ELSE
             CALL jacobiPoly(pi,r,0.0D0,0.0D0,i)
             qi = 0.5D0*qi*(1.0D0-s)
          END IF

          j = kDeg - i
          IF(j==0) THEN
             pj = 1.0D0
          ELSE
             CALL jacobiPoly(pj,s,2.0D0*i+1.0D0,0.0D0,j)
          END IF

          degCount = degCount + 1
          factor = SQRT( (i+0.5D0)*(i+j+1.0D0) )
          pol(degCount) = pi*qi*pj*factor
       END DO
    END DO

  END SUBROUTINE orthoPolyRST2D

  !========================================================================
  !>
  !! p = orthoPolyRST3D(x,GridOrder)
  !! Computes the ortogonal base of 3D polynomials of degree less 
  !! or equal to GridOrder at the point x=(r,s,t) in [-1,1]^3
  !<

  SUBROUTINE orthoPolyRST3D(pol,x,GridOrder)
    IMPLICIT NONE

    INTEGER :: GridOrder
    REAL*8 :: x(3)
    REAL*8 :: pol(:)

    INTEGER :: degCount, kDeg, i, j, k
    REAL*8 :: r, s, t, ppi, pj, pk, qi, qj, auxI, factor

    degCount = 0

    r = x(1)
    s = x(2)
    t = x(3)

    DO kDeg = 0,GridOrder
       DO i=0,kDeg
          IF(i==0) THEN
             ppi = 1.0D0
             qi = 1.0D0
          ELSE
             CALL jacobiPoly(ppi,r,0.0D0,0.0D0,i)
             qi = 0.5D0*qi*(1.0D0-s)
          END IF

          auxI = 2.0D0*i+1.0D0
          DO j = 0,kDeg-i
             IF(j==0) THEN
                pj = 1.0D0
                qj = (0.5D0*(1.0D0-t))**i
             ELSE
                CALL jacobiPoly(pj,s,auxI,0.0D0,j)
                qj = 0.5D0*qj*(1.0D0-t)
             END IF

             k = kDeg-(i+j)
             IF(k==0) THEN
                pk = 1.0D0
             ELSE
                CALL jacobiPoly(pk,t,2.0D0*(i+j)+2.0D0,0.0D0,k)
             END IF
             degCount = degCount+1
             factor = SQRT( 0.25D0*auxI*(i+j+1.0D0)*(2.0D0*(i+j+k)+3.0D0) )
             pol(degCount) = ppi*qi*pj*qj*pk*factor
          END DO
       END DO
    END DO

  END SUBROUTINE orthoPolyRST3D

  !========================================================================

  SUBROUTINE mapXiEtaZetaToRST2D(pointsRST, pointsXiEtaZeta)
    IMPLICIT NONE

    REAL*8, PARAMETER :: TOLequal = 1.0D-10
    REAL*8 :: pointsRST(2), pointsXiEtaZeta(2)
    INTEGER :: nOfPoints
    INTEGER :: i

    IF(pointsXiEtaZeta(2)>1.0D0-TOLequal) THEN
       pointsRST(1) = -1.0D0
       pointsRST(2) =  1.0D0
    ELSE
       pointsRST(1) = 2.0D0*(1.0D0+pointsXiEtaZeta(1))/(1.0D0-pointsXiEtaZeta(2)) - 1.0D0
       pointsRST(2) = pointsXiEtaZeta(2)
    END IF

  END SUBROUTINE mapXiEtaZetaToRST2D

  !========================================================================

  SUBROUTINE mapXiEtaZetaToRST3D(pointsRST, pointsXiEtaZeta)
    IMPLICIT NONE

    REAL*8, PARAMETER :: TOLequal = 1.0D-10
    REAL*8:: pointsRST(3), pointsXiEtaZeta(3)
    INTEGER :: nOfPoints

    INTEGER :: i

    IF(pointsXiEtaZeta(3)>1.0D0-TOLequal .OR. ABS(pointsXiEtaZeta(2)+pointsXiEtaZeta(3))<TOLequal) THEN
       pointsRST(1) = -1.0D0
       pointsRST(2) =  1.0D0	
    ELSE
       pointsRST(1) = -2.0D0*(1.0D0+pointsXiEtaZeta(1))/(pointsXiEtaZeta(2)+pointsXiEtaZeta(3)) - 1.0D0
       pointsRST(2) =  2.0D0*(1.0D0+pointsXiEtaZeta(2))/(1.0D0-pointsXiEtaZeta(3))-1.0D0
    END IF
    pointsRST(3) = pointsXiEtaZeta(3)

  END SUBROUTINE mapXiEtaZetaToRST3D

  !========================================================================
  !>
  !! Jacoby polynomials for 1D
  !! @param[in] x  
  !! @param[in] a,b   the positino of ends
  !! @param[in] GridOrder
  !! @param[out] pol
  !<
  SUBROUTINE jacobiPoly(pol,x,a,b,GridOrder)
    IMPLICIT NONE

    REAL*8 :: pol, x, a, b
    INTEGER :: GridOrder

    REAL*8 :: aPb, aMb, aPb_aMb, pm1, pm2, ra, rb
    INTEGER :: i

    aPb = a + b
    aMb = a - b

    IF(GridOrder==0) THEN
       pol = 1.0D0
    ELSE IF(GridOrder==1) THEN
       pol = 0.5D0*( aMb + (2.0D0 + aPb)*x )
    ELSE	
       aPb_aMb = aPb*aMb
       pm2 = 1.0D0
       pm1 = 0.5D0*( aMb + (2.0D0 + aPb)*x )
       DO i=2,GridOrder
	  ra = 2.0D0*i + aPb
	  rb = i*(i+aPb)*(ra-2.0D0)
	  pol = ( (ra-1.0D0)*( aPb_aMb + x*(ra-2.0D0)*ra)/(2.0D0*rb) )*pm1 - ( (i+a-1.0D0)*(i+b-1.0D0)*ra/rb )*pm2
	  pm2 = pm1
	  pm1 = pol
       END DO
    END IF

  END SUBROUTINE jacobiPoly

  !========================================================================

  SUBROUTINE vandermondeMatrix1D(V, GridOrder, coord)
    IMPLICIT NONE

    INTEGER :: GridOrder
    REAL*8 :: V(:,:), coord(:)

    INTEGER :: nOfNodes, i

    nOfNodes = SIZE(coord)
    IF(nOfNodes/=GridOrder+1)  STOP "ERR->vandermondeMatrix1D: wrong number of nodes"

    DO i=1,nOfNodes
       CALL orthoPoly1D(V(i,:),coord(i),GridOrder)
    END DO

  END SUBROUTINE vandermondeMatrix1D
  !========================================================================

  SUBROUTINE vandermondeMatrix2D(V, GridOrder, coord)
    IMPLICIT NONE

    INTEGER :: GridOrder
    REAL*8 :: V(:,:), coord(:,:)
    INTEGER :: nOfNodes, i

    nOfNodes = SIZE(coord,1)
    IF(nOfNodes/=(GridOrder+1)*(GridOrder+2)/2)  STOP "ERR->vandermondeMatrix2D: wrong number of nodes"

    DO i=1,nOfNodes
       CALL orthoPoly2D(V(i,:),coord(i,1:2),GridOrder)
    END DO

  END SUBROUTINE vandermondeMatrix2D


  !========================================================================

  SUBROUTINE vandermondeMatrix3D(V, GridOrder, coord)
    IMPLICIT NONE

    INTEGER :: GridOrder
    REAL*8 :: V(:,:), coord(:,:)
    INTEGER :: numCellNodes, i

    numCellNodes = SIZE(coord,1)
    IF(numCellNodes/=(GridOrder+1)*(GridOrder+2)*(GridOrder+3)/6)  STOP "ERR->vandermondeMatrix3D: wrong number of nodes"

    DO i=1,numCellNodes
       CALL orthoPoly3D(V(i,:),coord(i,:),GridOrder)
    END DO

  END SUBROUTINE vandermondeMatrix3D


  !========================================================================

END MODULE JACOBIANS

