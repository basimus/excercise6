cmake_minimum_required( VERSION 2.8 )

project( LibCommonF )
enable_language( Fortran ) 

set( F90_SRC_FILES
  "ADTree.f90"
  "array_allocator.f90"
  "BiTreeMeshStorage.f90"
  "BiTreeNode.f90"
  "BoxADTree.f90"
  "CellConnectivity.f90"
  "CornerLoop.f90"
  "DiffGeometry.f90"
  "ElasticityModule.f90"
  "FacedMeshManager.f90"
  "FacedMeshStorage.f90"
  "Geometry2D_module.f90"
  "Geometry3D_module.f90"
  "HeapTree.f90"
  "HighOrderCell.f90"
  "HighOrderCellManager.f90"
  "HybridMeshHighOrder.f90"
  "HybridMeshManager.f90"
  "HybridMeshStorage.f90"
  "LinkAssociation.f90"
  "mesh_renumberer.f90"
  "MUMPS.f90"
  "NodeNetTree.f90"
  "Number_Char_Transfer.f90"
  "occt_fortran.f90"
  "OctreeManager.f90"
  "OctreeStorage.f90"
  "Optimising.f90"
  "PlaneTriangulator.f90"
  "PointADTree.f90"
  "PointAssociation.f90"
  "PolygonFace.f90"
  "QuadtreeStorage.f90"
  "Queue.f90"
  "quicksort.f90"
  "Random.f90"
  "SourceType.f90"
  "SpacingStorage2D.f90"
  "SpacingStorage.f90"
  "SpacingStorageGen.f90"
  "split_tet_tri.f90"
  "SurfaceCurvatureCADfix.f90"
  "SurfaceCurvature.f90"
  "SurfaceCurvatureManager.f90"
  "SurfaceMeshGeometry.f90"
  "SurfaceMeshHighOrder.f90"
  "SurfaceMeshManager2D.f90"
  "SurfaceMeshManager.f90"
  "SurfaceMeshStorage.f90"
  "TetMeshStorage.f90"
  "Text_Print.f90"
)

add_definitions( -DAdd_ )
include_directories( 
  ${CMAKE_SOURCE_DIR}/contrib/MUMPS_seq/include
  ${CMAKE_SOURCE_DIR}/contrib/MUMPS_seq/libseq_f
)

add_library( LibCommonF ${F90_SRC_FILES} )

target_link_libraries( LibCommonF
  MUMPS_seq_f
  libseq_f
  LibLapack
  LibBlas
)
