!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!! Functions for background spacing generation.  
!<
MODULE SpacingStorageGen
  USE SpacingStorage
  USE SurfaceCurvature
  USE SurfaceCurvatureManager
  USE SurfaceMeshStorage
  USE SurfaceMeshManager
  USE OctreeMeshManager
  USE Number_Char_Transfer

CONTAINS

  !*******************************************************************************
  !>
  !!    Read curvature and transfer it to point sourcesBGSpacing%Sources.
  !!        
  !!   @param[in,out] BGSpacing : the background spacing being modified.
  !!   @param[in]    Curv       : the curvature.
  !!   @param[in] Curvature_Factors (1)  The cofficient. if  = 0.2, then the grid size for 
  !!                                      a sphere surface with a radius of 1 is 0.2.   \n
  !!                                (2)  The maximum Sagitta (with dimension).          \n
  !!                                (3)  The minimum grid size (with dimension).        \n
  !!                                (4)  The maximum grid size (with dimension).        \n
  !!                                (5)  The least trailing angle (in degree, 0~180).
  !!                                     (less than this angle, not a trailing edge.)   \n
  !!                                (6)  The full trailing angle (in degree, 0~180).
  !!                                     (bigger than this angle, a fully trailing edge.)  \n
  !!                                (7)  The minimum grid size (with dimension) for trailing.
  !!   @param[in]    Stretch_Limit : the maximum value of stretch.
  !<
  !*******************************************************************************

  SUBROUTINE SpacingStorage_SourceCurvature(BGSpacing, Curv, Curvature_Factors, Stretch_Limit)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(INOUT) :: BGSpacing
    TYPE(SurfaceCurvatureType), INTENT(INOUT)  :: Curv         
    REAL*8,  INTENT(in) :: Curvature_Factors(*), Stretch_Limit
    INTEGER :: nps,  ic, is, iu, iv, Isucc, i, k, ic1, n, ns, IMess
    REAL*8  :: Alpha1, Sagitta, Dmin, Dmax, Dmin0, Dmax0, aDmin0, aDmax0, Dtrl, Dtrl0, aDtrl0
    REAL*8  :: u, v, sp, str
    REAL*8  :: R(3), Rt(3), Rtt(3), Ru(3),Rv(3),Ruv(3),Ruu(3),Rvv(3)
    REAL*8  :: r1(3), rn, rmu, rmv, radius1, radius2, ck1, ck2, ckmin, sp1, sp2
    REAL*8  :: dirct(3,3), dirct1(3), dirct2(3), dirct3(3)
    REAL*8  :: fMap(3,3), Scalar, Scalars(3), dd, ddf, sdd
    REAL*8  :: TOLG, uv(2), dir(3,2), ang, ang1, ang2, pMin(3), pMax(3)
    LOGICAL :: isotr, CheckCurve
    TYPE(BoxSource_Type), POINTER :: pBoxS
    TYPE(BoxSource_Type) :: BoxS
    INTEGER, DIMENSION(:,:), POINTER :: iReg
    REAL*8, PARAMETER :: PI = 3.141592653589793D0

    CALL SpacingStorage_CheckBasicSize(BGSpacing)

    Alpha1   = Curvature_Factors(1) / BGSpacing%BasicSize
    Sagitta  = Curvature_Factors(2)
    Dmin     = Curvature_Factors(3)
    Dmax     = Curvature_Factors(4)
    ang1     = COS( (180-Curvature_Factors(5)) / 180.D0 * PI)
    ang2     = COS( (180-Curvature_Factors(6)) / 180.D0 * PI)
    Dtrl     = Curvature_Factors(7)
    Dmin0    = Dmin  / BGSpacing%BasicSize
    Dmax0    = Dmax  / BGSpacing%BasicSize
    Dtrl0    = Dtrl  / BGSpacing%BasicSize
    aDmin0   = LOG(Dmin0)
    aDmax0   = LOG(Dmax0)
    aDtrl0   = LOG(Dtrl0)
    ckmin    = 2*Sagitta / ((Dmax/2)**2 + Sagitta**2)
    ckmin    = MIN(ckmin, Alpha1/Dmax0)

    CheckCurve = .FALSE.
    nps = BGSpacing%NB_BoxSource
    DO ic = 1,Curv%NB_Curve
       write(456,*) 'CUrve:',ic
       write(456,*) Curv%Curves(ic)%TopoType
       IF(Curv%Curves(ic)%TopoType==-1)THEN
          nps = nps + 10* Curv%Curves(ic)%numNodes
          CheckCurve = .TRUE.
       ENDIF
    ENDDO
    DO is = 1,Curv%NB_Region
       IF(Curv%Regions(is)%TopoType==-1)THEN
          nps = nps + Curv%Regions(is)%numNodeV * Curv%Regions(is)%numNodeU
       ENDIF
    ENDDO

    CALL allc_BoxSource(BGSpacing%BoxSources, nps, 'BoxSources in SourceCurvature')

    ! *** A.- Curves

    IF(CheckCurve)THEN

       ddf = 2.5 * MIN(Dmin, Dtrl)
       IF(ddf>dmax) ddf = dmax
       TOLG = 1.d-8 * BGSpacing%BasicSize
       CALL SurfaceCurvature_SortCurve(Curv, TOLG)
       ALLOCATE(iReg(3, Curv%NB_Curve))
       iReg(:,:) = 0

       DO is = 1,Curv%NB_Region
          DO i = 1, Curv%Regions(is)%numCurve
             ic1 = Curv%Regions(is)%IC(i)
             ic  = ABS(ic1)
             IF(iReg(1,ic)==0)THEN
                iReg(1,ic) = SIGN(is, ic1)
             ELSE IF(iReg(2,ic)==0)THEN
                iReg(2,ic) = SIGN(is, ic1)
             ELSE
                iReg(3,ic) = SIGN(is, ic1)
             ENDIF
          ENDDO
       ENDDO

       DO ic = 1,Curv%NB_Curve
          IF(Curv%Curves(ic)%TopoType/=-1) CYCLE
          IF(iReg(2,ic)==0) CYCLE

          DO iu = 0, Curv%Curves(ic)%numNodes -2
             u = iu + 0.5                      ! Curvature at midpoint
             CALL CurveType_Interpolate(Curv%Curves(ic),2,u, R, Rt, Rtt)

             IF(iReg(3,ic)==0) THEN
                !--- curve between 2 regions

                DO k = 1,2
                   is = ABS(iReg(k,ic))
                   CALL RegionType_GetUVFromXYZ(Curv%Regions(is), 1, R, uv, TOLG, dd)
                   CALL RegionType_Interpolate (Curv%Regions(is), 1, uv(1), uv(2), R, Ru, Rv, Ruv)
                   dir(:,k) = Geo3D_Cross_Product(Ru, Rv)
                   dir(:,k) = dir(:,k) / Geo3D_Distance(dir(:,k))
                ENDDO
                IF(  (iReg(1,ic)>0 .AND. iReg(2,ic)>0) .OR.    &
                     (iReg(1,ic)<0 .AND. iReg(2,ic)<0) ) dir(:,2) = -dir(:,2)

                ang  = Geo3D_Dot_Product(dir(:,1),dir(:,2))
                write(456,*) 'CUrve:',ic
                write(456,*) Abs(iReg(1,ic)),abs(iReg(2,ic)),ang*180.0/3.1415
                write(456,*) dir(1:3,1)
                write(456,*) dir(1:3,2)
                IF(ang>ang1)THEN
                   radius1 = Dmax0
                ELSE IF(ang>ang2)THEN
                   radius1 = aDmax0 + (ang-ang1) * (aDtrl0-aDmax0) / (ang2-ang1)
                   radius1 = EXP(radius1)
                ELSE
                   radius1 = Dtrl0
                ENDIF

             ELSE
                !--- curve associated with 3 or more regions
                radius1 = Dtrl0
             ENDIF

             IF(radius1>0.99) CYCLE

             IF(BGSpacing%Model>0)THEN
                CALL SpacingStorage_GetScale(BGSpacing, R, Scalar)
             ELSE
                CALL SpacingStorage_GetMapping(BGSpacing, R, fMap)
                Scalars(1:3) = fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2
                Scalar       = MIN(Scalars(1), Scalars(2), Scalars(3))
                Scalar       = 1.d0 / SQRT(Scalar)
             ENDIF

             IF(radius1>=Scalar) CYCLE
             radius1 = MIN(MAX(Dtrl0,radius1), Dmax0, Scalar)
             BGSpacing%MinSize = MIN(BGSpacing%MinSize, radius1 * BGSpacing%BasicSize)

             IF(BGSpacing%Model>0)THEN
                !--- add an isotropic point-source.
                BoxS%isotr = .TRUE.
                BoxS%Scalar = radius1
             ELSE

                r1(1:3) = Geo3D_Cross_Product(Rt, Rtt)                 !    Rt x Rtt
                rn      = DSQRT(r1(1)*r1(1)+r1(2)*r1(2)+r1(3)*r1(3))   ! || Rt x Rtt ||
                rmu     = DSQRT(Rt(1)*Rt(1)+Rt(2)*Rt(2)+Rt(3)*Rt(3))   ! || Rt ||
                IF(rn<1.d-24) rn = 1.d-24
                radius2    = rmu*rmu*rmu / rn
                sp1        = Alpha1 * radius2
                IF(radius2>Sagitta)THEN
                   sp2 = 2* DSQRT((2*radius2 - Sagitta)*Sagitta) / BGSpacing%BasicSize
                   radius2 = MIN(sp1,sp2)
                ELSE
                   radius2 = sp1
                ENDIF
                IF(radius2<radius1) radius2 = radius1
                radius2 = MIN(MAX(Dtrl0,radius2), Dmax0, Scalar)

                str = radius2 / radius1
                IF(ABS(str-1.d0)<0.1)THEN
                   !--- add an isotropic point-source.
                   BoxS%isotr = .TRUE.
                   BoxS%Scalar = radius1
                ELSE
                   !--- add an anisotropic point-source. 
                   IF(str>Stretch_Limit) radius2 = Stretch_Limit * radius1
                   BoxS%isotr = .FALSE.
                   Dirct1(:)  = Rt(:) / rmu
                   CALL Geo3D_BuildOrthogonalAxes(Dirct1, Dirct2, Dirct3)
                   Dirct(1,:) = Dirct1(:)
                   Dirct(2,:) = Dirct2(:)
                   Dirct(3,:) = Dirct3(:)
                   R(1) = 1.d0 / radius2
                   R(2) = 1.d0 / radius1
                   R(3) = R(2)
                   CALL Mapping3D_Compose (R, Dirct, fMap)
                   BoxS%fMtr = Mapping3D_to_Metric (fMap)
                ENDIF
             ENDIF

             BoxS%LevelMark = BGSpacing%NB_BoxSource + 1

             pMin(:) = Curv%Curves(ic)%Posit(:,iu+1)
             pMax(:) = Curv%Curves(ic)%Posit(:,iu+2)
             sdd = Geo3D_Distance(pMin, pMax)
             ns  = 2* sdd / ddf
             IF(ns<1) ns = 1          
             r1(:) = (pMax - pMin) / ns

             DO n = 1, ns
                BGSpacing%NB_BoxSource = BGSpacing%NB_BoxSource + 1
                IF(BGSpacing%NB_BoxSource>nps)THEN
                   nps = BGSpacing%NB_BoxSource + 10000
                   CALL allc_BoxSource(BGSpacing%BoxSources, nps, 'BoxSources in SourceCurvature')
                ENDIF

                pBoxS => BGSpacing%BoxSources(BGSpacing%NB_BoxSource)
                pBoxS%LevelMark = BoxS%LevelMark
                pBoxS%isotr     = BoxS%isotr
                IF(pBoxS%isotr)THEN
                   pBoxS%Scalar = BoxS%Scalar
                ELSE
                   pBoxS%fMtr = BoxS%fMtr
                ENDIF
                R(:)          = pMin + (n-0.5) * r1(:)
                pBoxS%pMax(:) = R(:) + ddf
                pBoxS%pMin(:) = R(:) - ddf
             ENDDO
          ENDDO
       ENDDO

       DEALLOCATE(iReg)
    ENDIF

    ! *** B.- Surfaces

    DO is = 1,Curv%NB_Region
       IMess = 1
       DO iv = 0, Curv%Regions(is)%numNodeV -2
          DO iu = 0, Curv%Regions(is)%numNodeU -2
             u = iu + 0.5                      ! Curvature at midpoint
             v = iv + 0.5
             CALL RegionType_Interpolate(Curv%Regions(is),2,u,v,R,Ru,Rv,Ruv,Ruu,Rvv)
             WHERE(BGSpacing%pMin(1:3)>R(1:3)) BGSpacing%pMin(1:3) = R(1:3)
             WHERE(BGSpacing%pMax(1:3)<R(1:3)) BGSpacing%pMax(1:3) = R(1:3)

             IF(Curv%Regions(is)%TopoType/=-1) CYCLE

             !--- get two principal curvatures

             IF(BGSpacing%Model>0)THEN
                isotr = .TRUE.
             ELSE
                isotr = .FALSE.
             ENDIF
             CALL DiffGeo_CalcCurvature(Ru, Rv, Ruv, Ruu, Rvv, ckmin, isotr, ck1, ck2, Dirct1, Isucc)
             IF(Isucc==0) THEN
                IF(IMess>0 .AND. iu>0 .AND. iu<Curv%Regions(is)%numNodeU -2    &
                     .AND. iv>0 .AND. iv<Curv%Regions(is)%numNodeV -2 )THEN
                   WRITE(*,'(a,3I5)') ' Warning: fail to get Curvature at is,u,v=',is,iu,iv
                   IMess = IMess - 1
                ENDIF
                CYCLE
             ENDIF

             radius1    = 1.D0 / ABS(ck1)
             sp1        = Alpha1 * radius1
             IF(radius1>Sagitta)THEN
                sp2 = 2* DSQRT((2*radius1 - Sagitta)*Sagitta) / BGSpacing%BasicSize
                radius1 = MIN(sp1,sp2)
             ELSE
                radius1 = sp1
             ENDIF

             radius2    = 1.D0 / ABS(ck2)
             sp1        = Alpha1 * radius2
             IF(radius2>Sagitta)THEN
                sp2 = 2* DSQRT((2*radius2 - Sagitta)*Sagitta) / BGSpacing%BasicSize
                radius2 = MIN(sp1,sp2)
             ELSE
                radius2 = sp1
             ENDIF

             IF(MIN(radius1, radius2)>0.99) CYCLE

             IF(BGSpacing%Model>0)THEN
                CALL SpacingStorage_GetScale(BGSpacing, R, Scalar)
             ELSE
                CALL SpacingStorage_GetMapping(BGSpacing, R, fMap)
                Scalars(1:3) = fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2
                Scalar       = MIN(Scalars(1), Scalars(2), Scalars(3))
                Scalar       = 1.d0 / SQRT(Scalar)
             ENDIF

             IF(MIN(radius1, radius2)>=Scalar) CYCLE
             radius1 = MIN(MAX(Dmin0,radius1), Dmax0, Scalar)
             radius2 = MIN(MAX(Dmin0,radius2), Dmax0, Scalar)
             sp      = MIN(radius1,radius2)
             radius2 = MAX(radius1,radius2)
             radius1 = sp
             str     = radius2 / radius1
             IF(str>Stretch_Limit)THEN
                str     = Stretch_Limit
                radius2 = str * radius1
             ENDIF
             BGSpacing%MinSize = MIN(BGSpacing%MinSize, sp*BGSpacing%BasicSize)

             BGSpacing%NB_BoxSource = BGSpacing%NB_BoxSource + 1
             IF(BGSpacing%NB_BoxSource>nps)THEN
                nps = BGSpacing%NB_BoxSource + 10000
                CALL allc_BoxSource(BGSpacing%BoxSources, nps, 'BoxSources in SourceCurvature')
             ENDIF

             pBoxS => BGSpacing%BoxSources(BGSpacing%NB_BoxSource)
             pBoxS%LevelMark = BGSpacing%NB_BoxSource
             DO i = 1, 3
                pBoxS%pMin(i) =  MIN( Curv%Regions(is)%Posit(i,iu+1,iv+1),     &
                     Curv%Regions(is)%Posit(i,iu+1,iv+2),     &
                     Curv%Regions(is)%Posit(i,iu+2,iv+2),     &
                     Curv%Regions(is)%Posit(i,iu+2,iv+1) )
                pBoxS%pMax(i) =  MAX( Curv%Regions(is)%Posit(i,iu+1,iv+1),     &
                     Curv%Regions(is)%Posit(i,iu+1,iv+2),     &
                     Curv%Regions(is)%Posit(i,iu+2,iv+2),     &
                     Curv%Regions(is)%Posit(i,iu+2,iv+1) )
                dd = (pBoxS%pMax(i)-pBoxS%pMin(i)) / 2.d0
                IF(dd<dmin) dd = dmin
                pBoxS%pMin(i) = pBoxS%pMin(i) - dd
                pBoxS%pMax(i) = pBoxS%pMax(i) + dd
             ENDDO

             IF(isotr .OR. ABS(str-1.d0)<0.1 .OR. BGSpacing%Model>0)THEN
                !--- add an isotropic point-source.
                pBoxS%isotr = .TRUE.
                pBoxS%Scalar = radius1
             ELSE
                !--- add an anisotropic point-source. 
                pBoxS%isotr = .FALSE.
                CALL Geo3D_BuildOrthogonalAxes(Dirct1, Dirct2, Dirct3)
                Dirct(1,:) = Dirct1(:)
                Dirct(2,:) = Dirct2(:)
                Dirct(3,:) = Dirct3(:)
                R(1) = 1.d0 / radius2
                R(2) = 1.d0 / radius1
                R(3) = R(2)
                CALL Mapping3D_Compose (R, Dirct, fMap)
                pBoxS%fMtr = Mapping3D_to_Metric (fMap)
             ENDIF

          ENDDO
       ENDDO
    ENDDO


    RETURN
  END SUBROUTINE SpacingStorage_SourceCurvature


  !*******************************************************************************
  !>
  !!    Calculate curvature of a triangular surface
  !!           and transfer it to point sourcesBGSpacing%Sources.
  !!        
  !!   @param[in,out] BGSpacing : the background spacing being modified.
  !!   @param[in,out]    Surf   : the triangular surface, %IP_Pt will be built.
  !!   @param[in] Curvature_Factors (1)  The cofficient. if  = 0.2, then the grid size for 
  !!                                      a sphere surface with a radius of 1 is 0.2.   \n
  !!                                (2)  The maximum Sagitta (with dimension).          \n
  !!                                (3)  The minimum grid size (with dimension).        \n
  !!                                (4)  The maximum grid size (with dimension).
  !!   @param[in]    Stretch_Limit : the maximum value of stretch.
  !<
  !*******************************************************************************

  SUBROUTINE SpacingStorage_SourceSurface(BGSpacing, Surf,  Curvature_Factors, Stretch_Limit)
    IMPLICIT NONE
    TYPE(SpacingStorageType),     INTENT(INOUT) :: BGSpacing
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf         
    REAL*8, INTENT(in)  :: Curvature_Factors(*), Stretch_Limit
    INTEGER :: IP, IT, j, mp, ip1, ip2, ip3, ipt, Isucc, nps
    REAL*8  :: Alpha1, Sagitta, dmin, dmax, Dmin0, Dmax0, sp, str, sp1, sp2
    REAL*8  :: R(3), P0(3), Wuv, Wuu, Wvv, Uxis(3), Vxis(3), Pn(3), pMin(3), pMax(3)
    REAL*8  :: radius1, radius2, ck1, ck2, ckmin, aa, a(3)
    REAL*8  :: dirct(3,3), fMap(3,3), Scalar, Scalars(3), psi(3,1000)
    REAL*8, DIMENSION(:,:), POINTER :: anor_Pt
    LOGICAL :: isotr
    TYPE(BoxSource_Type), POINTER :: pBoxS
    
    CALL SpacingStorage_CheckBasicSize(BGSpacing)

    Alpha1   = Curvature_Factors(1) / BGSpacing%BasicSize
    Sagitta  = Curvature_Factors(2)
    Dmin     = Curvature_Factors(3)
    Dmax     = Curvature_Factors(4)
    Dmin0    = Dmin  / BGSpacing%BasicSize
    Dmax0    = Dmax  / BGSpacing%BasicSize
    ckmin    = 2*Sagitta / ((Dmax/2)**2 + Sagitta**2)
    ckmin    = MIN(ckmin, Alpha1/Dmax0)

    nps = BGSpacing%NB_BoxSource + Surf%NB_Point
    CALL allc_BoxSource(BGSpacing%BoxSources, nps, 'BoxSources in SourceSurface')

    CALL Surf_BuildPtAsso(Surf)

    !--- prepare the geometry

    ALLOCATE(anor_Pt(3,Surf%NB_Point))
    anor_Pt(:,:) = 0

    DO IT = 1, Surf%NB_Tri
       ip1 = Surf%IP_Tri(1,IT)
       ip2 = Surf%IP_Tri(2,IT)
       ip3 = Surf%IP_Tri(3,IT)
       a(:)  = Geo3D_Cross_Product(Surf%Posit(:,ip2),Surf%Posit(:,ip1),Surf%Posit(:,ip3))
       aa    = dsqrt( a(1)*a(1) + a(2)*a(2) + a(3)*a(3) )
       a(:) = a(:) / (aa*aa)
       anor_Pt(:,ip1) = anor_Pt(:,ip1) + a(:)
       anor_Pt(:,ip2) = anor_Pt(:,ip2) + a(:)
       anor_Pt(:,ip3) = anor_Pt(:,ip3) + a(:)
    ENDDO

    DO  IP = 1, Surf%NB_Point

       P0(:) = Surf%Posit(:,IP)
       WHERE(BGSpacing%pMin(1:3)>P0(1:3)) BGSpacing%pMin(1:3) = P0(1:3)
       WHERE(BGSpacing%pMax(1:3)<P0(1:3)) BGSpacing%pMax(1:3) = P0(1:3)

       a(:) = anor_Pt(:,IP)
       aa   = dsqrt( a(1)*a(1) + a(2)*a(2) + a(3)*a(3) )
       pn(:) = a(:) / aa

       mp = Surf%IP_Pt%JointLinks(IP)%numNodes
       IF(mp>1000) CALL Error_STOP ( 'mp>1000')
       pmin = p0
       pmax = p0
       DO j = 1, mp
          ipt  = Surf%IP_Pt%JointLinks(IP)%Nodes(j)
          psi(:,j) = Surf%Posit(:,ipt)
          WHERE(pMin(1:3) > psi(1:3,j)) pMin(:) = psi(:,j)
          WHERE(pMax(1:3) < psi(1:3,j)) pMax(:) = psi(:,j)
       ENDDO

       IF(mp<=4)THEN
          mp = mp+1
          psi(:,mp) = (psi(:,mp-2)+psi(:,mp-1))/2.0
       ENDIF

       CALL Geo3D_BuildOrthogonalAxes(pn,Uxis,Vxis)
       CALL DiffGeo_quadsurface(p0,psi,mp,Uxis,Vxis,pn,Wuv,Wuu,Wvv,Isucc)

       !--- get two principal curvatures

       IF(BGSpacing%Model>0)THEN
          isotr = .TRUE.
       ELSE
          isotr = .FALSE.
       ENDIF
       CALL DiffGeo_CalcCurvature2(Wuv, Wuu, Wvv, Uxis,Vxis, ckmin, isotr, ck1, ck2, Dirct(1,:))

       radius1    = 1.D0 / ABS(ck2)
       sp1        = Alpha1 * radius1
       IF(radius1>Sagitta)THEN
          sp2 = 2* DSQRT((2*radius1 - Sagitta)*Sagitta) / BGSpacing%BasicSize
          radius1 = MIN(sp1,sp2)
       ELSE
          radius1 = sp1
       ENDIF

       radius2    = 1.D0 / ABS(ck2)
       sp1        = Alpha1 * radius2
       IF(radius2>Sagitta)THEN
          sp2 = 2* DSQRT((2*radius2 - Sagitta)*Sagitta) / BGSpacing%BasicSize
          radius2 = MIN(sp1,sp2)
       ELSE
          radius2 = sp1
       ENDIF

       IF(MIN(radius1, radius2)>0.99) CYCLE

       IF(BGSpacing%Model>0)THEN
          CALL SpacingStorage_GetScale(BGSpacing, P0, Scalar)
       ELSE
          CALL SpacingStorage_GetMapping(BGSpacing, P0, fMap)
          Scalars(1:3) = fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2
          Scalar       = MIN(Scalars(1), Scalars(2), Scalars(3))
          Scalar       = 1.d0 / SQRT(Scalar)
       ENDIF

       IF(MIN(radius1, radius2)>=Scalar) CYCLE
       radius1 = MIN(MAX(Dmin0,radius1), Dmax0, Scalar)
       radius2 = MIN(MAX(Dmin0,radius2), Dmax0, Scalar)
       sp      = MIN(radius1,radius2)
       radius2 = MAX(radius1,radius2)
       radius1 = sp
       str     = radius2 / radius1
       IF(str>Stretch_Limit)THEN
          str     = Stretch_Limit
          radius2 = str * radius1
       ENDIF
       BGSpacing%MinSize = MIN(BGSpacing%MinSize, sp*BGSpacing%BasicSize)

       BGSpacing%NB_BoxSource = BGSpacing%NB_BoxSource + 1
       pBoxS => BGSpacing%BoxSources(BGSpacing%NB_BoxSource)
       pBoxS%LevelMark = BGSpacing%NB_BoxSource
       pBoxS%pMin(:) = pMin(:) - dmin
       pBoxS%pMax(:) = pMax(:) + dmin

       IF(isotr .OR. ABS(str-1.d0)<0.1 .OR. BGSpacing%Model>0)THEN
          !--- add an isotropic point-source.
          pBoxS%isotr = .TRUE.
          pBoxS%Scalar = radius1
       ELSE
          !--- add an anisotropic point-source. 
          pBoxS%isotr = .FALSE.
          CALL Geo3D_BuildOrthogonalAxes(Dirct(1,:),Dirct(2,:),Dirct(3,:))
          R(1) = 1.d0 / radius2
          R(2) = 1.d0 / radius1
          R(3) = R(2)
          CALL Mapping3D_Compose (R, Dirct, fMap)
          pBoxS%fMtr = Mapping3D_to_Metric (fMap)
       ENDIF

    ENDDO

    DEALLOCATE(anor_Pt)


    RETURN
  END SUBROUTINE SpacingStorage_SourceSurface

  !*******************************************************************************
  !>
  !!    Calculate spacing of a surface triangulation
  !!           and transfer it to point sourcesBGSpacing%Sources.
  !!        
  !!   @param[in,out] BGSpacing : the background spacing being modified.
  !!   @param[in,out]    Surf   : the triangular surface, %IP_Pt will be built.
  !!   @param[in] Curvature_Factors (3)  The minimum grid size (with dimension).        \n
  !!                                (4)  The maximum grid size (with dimension).
  !<
  !*******************************************************************************
  SUBROUTINE SpacingStorage_SourceTriangle(BGSpacing, Surf, Curvature_Factors)
    IMPLICIT NONE
    TYPE(SpacingStorageType),     INTENT(INOUT) :: BGSpacing
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf         
    REAL*8, INTENT(in)  :: Curvature_Factors(*)
    INTEGER :: IP, mp, ipt, nps, j
    REAL*8  :: dmin, dmax, sp, sp1, sp2
    REAL*8  :: P0(3), pMin(3), pMax(3)
    REAL*8  :: fMap(3,3), psi(3,1000)
    LOGICAL :: isotr, isEmpty
    REAL*8,  DIMENSION(:),   POINTER :: Scalars
    REAL*8,  DIMENSION(:,:), POINTER :: Stretch
    TYPE(BoxSource_Type), POINTER :: pBoxS

    CALL SpacingStorage_CheckBasicSize(BGSpacing)

    Dmin     = Curvature_Factors(3)
    Dmax     = Curvature_Factors(4)

    nps = BGSpacing%NB_BoxSource + Surf%NB_Point
    CALL allc_BoxSource(BGSpacing%BoxSources, nps, 'BoxSources in SourceTriangle')

    CALL Surf_BuildPtAsso(Surf)

    !--- calculate scalar at each node.

    ALLOCATE(Scalars(Surf%NB_Point))
    CALL Surf_getScalar(Surf,  Scalars, BGSpacing%Interp_Method)

    WHERE(Scalars(:)<Dmin) Scalars(:) = Dmin
    WHERE(Scalars(:)>Dmax) Scalars(:) = Dmax

    sp = MINVAL(Scalars(1:Surf%NB_Point))  
    BGSpacing%MinSize = MIN(BGSpacing%MinSize, sp)
    isEmpty = SpacingStorage_isEmpty(BGSpacing)

    Scalars(:) = Scalars(:)  / BGSpacing%BasicSize

    !--- calculate stretch at each node for anisotropic problem

    ALLOCATE(Stretch(6,Surf%NB_Point))
    IF(BGSpacing%Model<0)THEN
       CALL Surf_getStretch(Surf,  Stretch)
    ENDIF

    !--- add box-source

    DO  IP = 1, Surf%NB_Point

       IF(BGSpacing%Model>0)THEN
          isotr = .TRUE.
       ELSE
          WHERE(stretch(1:3, ip)<Dmin) stretch(1:3, ip) = Dmin
          WHERE(stretch(1:3, ip)>Dmax) stretch(1:3, ip) = Dmax
          sp1 = MAXVAL(stretch(1:3, ip))
          sp2 = MINVAL(stretch(1:3, ip))
          IF(ABS(sp2/sp1-1.d0)<0.1)THEN
             isotr = .TRUE.
          ELSE
             isotr = .FALSE.
             stretch(1:3, ip) = stretch(1:3, ip) / BGSpacing%BasicSize
             fMap(1:3,1:3)    = Mapping3D_from_Stretch(Stretch(:,ip))
             Scalars(ip) = 1.d0 / sp1
          ENDIF
       ENDIF

       IF(Scalars(ip)>0.99) CYCLE
       IF(.NOT. isEmpty)THEN
          CALL SpacingStorage_GetMaxGridSize(BGSpacing, P0, sp)
          IF(Scalars(ip)*BGSpacing%BasicSize>sp) CYCLE
       ENDIF

       P0(:) = Surf%Posit(:,IP)
       WHERE(BGSpacing%pMin(1:3)>P0(1:3)) BGSpacing%pMin(1:3) = P0(1:3)
       WHERE(BGSpacing%pMax(1:3)<P0(1:3)) BGSpacing%pMax(1:3) = P0(1:3)

       mp = Surf%IP_Pt%JointLinks(IP)%numNodes
       IF(mp>1000) CALL Error_STOP ( 'mp>1000')
       pmin = p0
       pmax = p0
       DO j = 1, mp
          ipt  = Surf%IP_Pt%JointLinks(IP)%Nodes(j)
          psi(:,j) = Surf%Posit(:,ipt)
          WHERE(pMin(1:3) > psi(1:3,j)) pMin(:) = psi(:,j)
          WHERE(pMax(1:3) < psi(1:3,j)) pMax(:) = psi(:,j)
       ENDDO

       BGSpacing%NB_BoxSource = BGSpacing%NB_BoxSource + 1
       pBoxS => BGSpacing%BoxSources(BGSpacing%NB_BoxSource)
       pBoxS%LevelMark = BGSpacing%NB_BoxSource
       pBoxS%pMin(:) = pMin(:) - dmin
       pBoxS%pMax(:) = pMax(:) + dmin
       pBoxS%isotr   = isotr
       IF(isotr)THEN
          !--- add an isotropic point-source.
          pBoxS%Scalar = Scalars(ip)
       ELSE
          !--- add an anisotropic point-source. 
          pBoxS%fMtr = Mapping3D_to_Metric (fMap)
       ENDIF

    ENDDO

    DEALLOCATE(Scalars, Stretch)


    RETURN
  END SUBROUTINE SpacingStorage_SourceTriangle


  !*******************************************************************************
  !>
  !!  Evaluate background Quad grids by nodes     
  !!  Make sure that the leaf cells of BGSpacing%Octree have been set in advance.
  !!
  !!  For BGSpacing%Model<0
  !!  @param[in]  BGSpacing%Metric_OctNode
  !!  @param[out] BGSpacing%Mapping_OctNode if BGSpacing%OctCell_Vary==.true.; or
  !!  @param[out] BGSpacing%Mapping_OctCell else.
  !!
  !!  For BGSpacing%Model>0
  !!  @param[in]  BGSpacing%Scalar_OctNode
  !!  @param[out] BGSpacing%Scalar_OctCell if BGSpacing%OctCell_Vary==.false.
  !<
  !*******************************************************************************
  SUBROUTINE SpacingStorage_FillOctCell(BGSpacing)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(INOUT) :: BGSpacing
    INTEGER :: ip, ie, ip8(8)
    REAL*8  :: fMtr(6), fMtrs(6,8), Scalar, Scalars(8), fMap(3,3)
    TYPE (OctreeMesh_Cell_Type), POINTER ::pOct_Cell

    IF(BGSpacing%Model<0)THEN

       IF(BGSpacing%OctCell_Vary)THEN
          ALLOCATE(BGSpacing%Mapping_OctNode(3,3,BGSpacing%Octree%numNodes))
          DO ip = 1,BGSpacing%Octree%numNodes
             BGSpacing%Mapping_OctNode(:,:,ip) = Mapping3D_from_Metric(BGSpacing%Metric_OctNode(:,ip))
          ENDDO
       ELSE
          ALLOCATE(BGSpacing%Mapping_OctCell(3,3,BGSpacing%Octree%numCells))
          DO ie = 1, BGSpacing%Octree%numCells
             pOct_Cell => BGSpacing%Octree%LeafCells(ie)%to
             ip8(:)    =  pOct_Cell%Nodes(:)
             fMtrs(:,1:8) = BGSpacing%Metric_OctNode(:,ip8(1:8))
             fMtr(:)      = Metric3D_Mean(8,fMtrs,BGSpacing%Interp_Method)
             BGSpacing%Mapping_OctCell(:,:,pOct_Cell%ID) = Mapping3D_from_Metric(fMtr)
          ENDDO
       ENDIF

    ELSE

       IF(.NOT. BGSpacing%OctCell_Vary)THEN
          ALLOCATE(BGSpacing%Scalar_OctCell(BGSpacing%Octree%numCells))
          DO ie = 1, BGSpacing%Octree%numCells
             pOct_Cell => BGSpacing%Octree%LeafCells(ie)%to
             ip8(:)    =  pOct_Cell%Nodes(:)
             Scalars(1:8) = BGSpacing%Scalar_OctNode(ip8(1:8))
             BGSpacing%Scalar_OctCell(pOct_Cell%ID) = Scalar_Mean(8,Scalars,BGSpacing%Interp_Method)
          ENDDO
       ENDIF

    ENDIF


  END SUBROUTINE SpacingStorage_FillOctCell

  !*******************************************************************************
  !>
  !!   Build Octree background.
  !!   @param[in]  BGSpacing%Model
  !!   @param[in]  BGSpacing%ADTree        if(ABS(BGSpacing%Model)== 2 OR 4)
  !!   @param[in]  BGSpacing%BoxSources    if(BGSpacing%Model==-3)
  !!   @param[out] BGSpacing%Octree 
  !!   @param[out] BGSpacing%Model         change to -3 aftermath
  !<       
  !*******************************************************************************
  SUBROUTINE SpacingStorage_BuildOctree(BGSpacing)
    IMPLICIT NONE
    TYPE(SpacingStorageType), INTENT(INOUT) :: BGSpacing
    INTEGER :: imax, jmax, kmax, NI(3)
    INTEGER :: n,i,j,k, L, Level, i0,j0,k0, i1,j1,k1, id,jd,kd, ip, kdir
    INTEGER :: ip8(8),idirct, ie, flag,iph,ipHang(18),numHang,maxHang,done, tempModel
    REAL*8  :: XYZmin(3), XYZmax(3)
    REAL*8  :: Xmax1, Xmin1, Ymax1, Ymin1, Zmax1, Zmin1, dxyz(3)
    REAL*8  :: pp1(3), pp2(3), ppd(3), dr(3), dd(3), SS(2), D
    REAL*8  :: fMtr(6), fMtrs(6,8), Scalar, Scalars(8), fMap(3,3)
    LOGICAL :: inDomain
    TYPE (OctreeMesh_Cell_Type), POINTER ::pOct_Cell
    REAL*8, DIMENSION(:,:), POINTER :: pps
    INTEGER,DIMENSION(  :), POINTER :: ICounter_OctNode
    REAL*8, DIMENSION(  :), POINTER :: FCounter_OctNode
    REAL*8, DIMENSION(:,:), POINTER :: Eigen_OctNode


    IF(BGSpacing%Model>0)THEN
       dxyz(1:3)   = BGSpacing%BasicSize
    ELSE
       DO i = 1,3
          dd(:)   = 0.d0
          dd(i)   = 1.d0
          dd(:)   = Mapping3D_Posit_Transf(dd, BGSpacing%BasicMap, 1)
          dxyz(i) = BGSpacing%BasicSize / dsqrt(dd(1)*dd(1)+dd(2)*dd(2)+dd(3)*dd(3))
       ENDDO
    ENDIF

    IF(BGSpacing%pMin(1) > BGSpacing%pMax(1))THEN
       WRITE(*,*)' Error--- wrong Spacing Domain=',BGSpacing%pMin(1),BGSpacing%pMax(1)
       CALL Error_STOP ( '--- SpacingStorage_BuildOctree')
    ENDIF

    XYZmax(1:3) = BGSpacing%pMax(1:3) + dxyz(1:3)/2.d0
    XYZmin(1:3) = BGSpacing%pMin(1:3) - dxyz(1:3)/2.d0

    !--- initial background Quad mesh

    Xmin1 = XYZmin(1) - (XYZmax(1)-XYZmin(1))*0.1d0
    Xmax1 = XYZmax(1) + (XYZmax(1)-XYZmin(1))*0.1d0
    Ymin1 = XYZmin(2) - (XYZmax(2)-XYZmin(2))*0.1d0
    Ymax1 = XYZmax(2) + (XYZmax(2)-XYZmin(2))*0.1d0
    Zmin1 = XYZmin(3) - (XYZmax(3)-XYZmin(3))*0.1d0
    Zmax1 = XYZmax(3) + (XYZmax(3)-XYZmin(3))*0.1d0
    imax  = ((Xmax1-Xmin1)/dxyz(1) +1)
    jmax  = ((Ymax1-Ymin1)/dxyz(2) +1)
    kmax  = ((Zmax1-Zmin1)/dxyz(3) +1)
    IF(MOD(imax,2)==1) imax = imax+1
    IF(MOD(jmax,2)==1) jmax = jmax+1
    IF(MOD(kmax,2)==1) kmax = kmax+1
    dxyz(1) = (Xmax1-Xmin1)/imax
    dxyz(2) = (Ymax1-Ymin1)/jmax
    dxyz(3) = (Zmax1-Zmin1)/kmax

    WRITE(*,*)'Octree backgroud range: x=',REAL(xmin1),REAL(xmax1),REAL(dxyz(1))
    WRITE(*,*)'                        y=',REAL(ymin1),REAL(ymax1),REAL(dxyz(2))
    WRITE(*,*)'                        z=',REAL(zmin1),REAL(zmax1),REAL(dxyz(3))

    pp1(:) = (/xmin1,ymin1,zmin1/)
    NI(:)  = (/imax,jmax,kmax/)
  
    WRITE(*,*) NI

    CALL OctreeMesh_Set(NI, dxyz, pp1, BGSpacing%Octree)
    
    WRITE(*,*)'Refine Octree by sources'
    WRITE(*,*)'Number of box sources ',BGSpacing%NB_BoxSource
    WRITE(*,*)'Min Spacing and basic size ',BGSpacing%MinSize, BGSpacing%BasicSize
    ! tempModel = BGSpacing%Model
    ! BGSpacing%Model = 4
    ! CALL OctreeMesh_RefineBySpacing(BGSpacing%Octree,BGSpacing)
    ! BGSpacing%Model = tempModel

    IF(ABS(BGSpacing%Model)==2 .OR. ABS(BGSpacing%Model)==4)THEN
       !--- refine background Oct. mesh based on source
       CALL SourceGroup_SetCheckPoints(BGSpacing%Sources, pps,n)
       DO i = 1,n
          pp1(:) = PPS(:,i)
          SS = SourceGroup_GetGradient(pp1,BGSpacing%Sources)
          DO L=1,Max_Oct_Level
             D = SS(2) * MAX( BGSpacing%Octree%DX(L), BGSpacing%Octree%DY(L),   &
                  BGSpacing%Octree%DZ(L) )
             IF(D<0.5) EXIT
          ENDDO
          Level = L
          DO
             pOct_Cell => OctreeMesh_CellSearch(pp1,Level,BGSpacing%Octree)
             IF(pOct_Cell%Level >= Level) EXIT
             IF(pOct_Cell%Level==0)THEN
                WRITE(*,*)' Error--- Fail to locate a point in the octree'
                WRITE(*,*)'  pp1=',REAL(pp1)
                CALL OctreeMesh_info(BGSpacing%Octree, 6)
                CALL Error_STOP ( '--- SpacingStorage_BuildOctree')
             ENDIF
             CALL OctreeMesh_CellBisect(pOct_Cell,BGSpacing%Octree)
          ENDDO
       ENDDO

       CALL OctreeMesh_RefineByGradient(BGSpacing%Octree,BGSpacing)
    ENDIF

    WRITE(*,*)'Refine Octree by box sources generated using curvature control'

    !---- refine background Oct. mesh based on box-BGSpacing%Sources
    DO n = 1,BGSpacing%NB_BoxSource

       dd(:) = BGSpacing%BoxSources(n)%pMax(:) - BGSpacing%BoxSources(n)%pMin(:)
       
       dr(1) = MAX(BGSpacing%BasicSize*BGSpacing%BoxSources(n)%Scalar,dd(1)*0.5d0)
       dr(2) = MAX(BGSpacing%BasicSize*BGSpacing%BoxSources(n)%Scalar,dd(2)*0.5d0)
       dr(3) = MAX(BGSpacing%BasicSize*BGSpacing%BoxSources(n)%Scalar,dd(3)*0.5d0)

       L = OctreeMesh_getFineLevel(dr,BGSpacing%Octree)
       Level = L

       IF(Level==1) CYCLE

       !first divide at the centre of the source
       pp1(:) = 0.5d0*(BGSpacing%BoxSources(n)%pMax(:) + BGSpacing%BoxSources(n)%pMin(:))
       DO
           pOct_Cell => OctreeMesh_CellSearch(pp1,Level,BGSpacing%Octree)
           
           IF(pOct_Cell%Level >= Level) THEN
            EXIT
           END IF
           CALL OctreeMesh_CellBisect(pOct_Cell,BGSpacing%Octree)
       ENDDO


       i1 = dd(1) / BGSpacing%Octree%DX(L)
       j1 = dd(2) / BGSpacing%Octree%DY(L)
       k1 = dd(3) / BGSpacing%Octree%DZ(L)



       DO i=0, i1, 2
          DO j=0, j1, 2
             DO k=0, k1, 2
                pp1(1) = BGSpacing%BoxSources(n)%pMin(1) + i * BGSpacing%Octree%DX(L)
                pp1(2) = BGSpacing%BoxSources(n)%pMin(2) + j * BGSpacing%Octree%DY(L)
                pp1(3) = BGSpacing%BoxSources(n)%pMin(3) + k * BGSpacing%Octree%DZ(L)
                DO
                   pOct_Cell => OctreeMesh_CellSearch(pp1,Level,BGSpacing%Octree)
                   
                   IF(pOct_Cell%Level >= Level) THEN
                    EXIT
                   END IF
                   CALL OctreeMesh_CellBisect(pOct_Cell,BGSpacing%Octree)
                ENDDO
             ENDDO
          ENDDO
       ENDDO

       !WRITE(*,*)i1,j1,k1,BGSpacing%Octree%numNodes

    ENDDO



    !--- get list of leaf cells
    CALL OctreeMesh_getLeafCells(BGSpacing%Octree)
	
	

    !--- check the smallest cell.
    IF(BGSpacing%CheckLevel>2)THEN
       Level = 0
       DO ie = 1, BGSpacing%Octree%numCells
          pOct_Cell => BGSpacing%Octree%LeafCells(ie)%to
          IF(Level<pOct_Cell%Level)THEN
             Level = pOct_Cell%Level
             L     = ie
          ENDIF
       ENDDO
       WRITE(*,*) ' Min. cell for oct. ie=', L, ' with Level=',Level
       pOct_Cell => BGSpacing%Octree%LeafCells(L)%to
       WRITE(*,*) '   dx,dy,dz: ', REAL(BGSpacing%Octree%DX(Level)),   &
            REAL(BGSpacing%Octree%DY(Level)), REAL(BGSpacing%Octree%DZ(Level))
       WRITE(*,*) '   centre  : ', REAL(OctreeMesh_getCellCentre(pOct_Cell,BGSpacing%Octree))

       IF(BGSpacing%CheckLevel>3)THEN
          CALL OctreeMesh_Check(BGSpacing%Octree)
       ENDIF
    ENDIF

    !---- initialise background Quad mesh nodes
    WRITE(*,*)'Evaluate Octree '

    ALLOCATE(ICounter_OctNode(BGSpacing%Octree%numNodes))
    ALLOCATE(FCounter_OctNode(BGSpacing%Octree%numNodes))
    DO ip=1,BGSpacing%Octree%numNodes
       ICounter_OctNode(ip)  = 0
       FCounter_OctNode(ip)  = 0.D0
    ENDDO

    IF(BGSpacing%Model<0)THEN
       ALLOCATE(BGSpacing%Metric_OctNode(6,BGSpacing%Octree%numNodes))
       fMtr = Mapping3D_to_Metric(BGSpacing%BasicMap)
       DO ip=1,BGSpacing%Octree%numNodes
          BGSpacing%Metric_OctNode(:,ip)  = fMtr(:)
       ENDDO
    ELSE
       ALLOCATE(BGSpacing%Scalar_OctNode(BGSpacing%Octree%numNodes))
       DO ip=1,BGSpacing%Octree%numNodes
          BGSpacing%Scalar_OctNode(ip) = 1.D0
       ENDDO
    ENDIF


    IF(ABS(BGSpacing%Model)==2 .OR. ABS(BGSpacing%Model)==4)THEN
       !--- evalue background Oct. mesh based on Tet. background
       DO ip = 1, BGSpacing%Octree%numNodes
          pp1(:) = BGSpacing%Octree%Posit(:,ip)
          inDomain = .TRUE.
          IF(BGSpacing%Model<0)THEN
             CALL SpacingStorage_GetMapping(BGSpacing, pp1, fMap, inDomain)
             BGSpacing%Metric_OctNode(:,ip) = Mapping3D_to_Metric(fMap)
          ELSE
             CALL SpacingStorage_GetScale(BGSpacing, pp1, Scalar, inDomain)
             BGSpacing%Scalar_OctNode(ip) = Scalar
          ENDIF
          IF(inDomain) FCounter_OctNode(ip) = FCounter_OctNode(ip) + 1.d0
       ENDDO
    ENDIF

    !---- evaluate background Quad mesh nodes based on source points
    DO n = 1,BGSpacing%NB_BoxSource

       pp1(:)  = BGSpacing%BoxSources(n)%pMin(:)
       pp2(:)  = BGSpacing%BoxSources(n)%pMax(:)
       dd(:)   = pp2(:) - pp1(:)

       dd(:) = BGSpacing%BoxSources(n)%pMax(:) - BGSpacing%BoxSources(n)%pMin(:)
       
       dr(1) = MAX(BGSpacing%BasicSize*BGSpacing%BoxSources(n)%Scalar,dd(1)*0.5d0)
       dr(2) = MAX(BGSpacing%BasicSize*BGSpacing%BoxSources(n)%Scalar,dd(2)*0.5d0)
       dr(3) = MAX(BGSpacing%BasicSize*BGSpacing%BoxSources(n)%Scalar,dd(3)*0.5d0)

       L = OctreeMesh_getFineLevel(dr,BGSpacing%Octree)
       Level = L

       IF(Level==1)THEN

          i0 = (pp1(1) - BGSpacing%Octree%Pmin(1)) / BGSpacing%Octree%DX(1) +1
          i1 = (pp2(1) - BGSpacing%Octree%Pmin(1)) / BGSpacing%Octree%DX(1) +1
          j0 = (pp1(2) - BGSpacing%Octree%Pmin(2)) / BGSpacing%Octree%DY(1) +1
          j1 = (pp2(2) - BGSpacing%Octree%Pmin(2)) / BGSpacing%Octree%DY(1) +1
          k0 = (pp1(3) - BGSpacing%Octree%Pmin(3)) / BGSpacing%Octree%DZ(1) +1
          k1 = (pp2(3) - BGSpacing%Octree%Pmin(3)) / BGSpacing%Octree%DZ(1) +1
          i0 = MAX(i0,1)
          j0 = MAX(j0,1)
          k0 = MAX(k0,1)
          i1 = MIN(i1,BGSpacing%Octree%imax)
          j1 = MIN(j1,BGSpacing%Octree%jmax)
          k1 = MIN(k1,BGSpacing%Octree%kmax)

          DO i=i0,i1
             DO j=j0,j1
                DO k=k0,k1
                   pOct_Cell =>BGSpacing%Octree%Cells(i,j,k)
                   !    IF(pOct_Cell%Mark==BGSpacing%BoxSources(n)%LevelMark) CYCLE
                   !    pOct_Cell%Mark = BGSpacing%BoxSources(n)%LevelMark
                   CALL Evaluate_OctBack(pOct_Cell,BGSpacing%BoxSources(n))
                ENDDO
             ENDDO
          ENDDO

       ELSE

          i1 = dd(1) / BGSpacing%Octree%DX(L)
          j1 = dd(2) / BGSpacing%Octree%DY(L)
          k1 = dd(3) / BGSpacing%Octree%DZ(L)
          DO i=0, i1
             DO j=0, j1
                DO k=0, k1
                   ppd(1) = BGSpacing%BoxSources(n)%pMin(1) + i * BGSpacing%Octree%DX(L)
                   ppd(2) = BGSpacing%BoxSources(n)%pMin(2) + j * BGSpacing%Octree%DY(L)
                   ppd(3) = BGSpacing%BoxSources(n)%pMin(3) + k * BGSpacing%Octree%DZ(L)
                   WHERE(ppd(:)<BGSpacing%Octree%Pmin(:)) ppd(:)=BGSpacing%Octree%Pmin(:)
                   WHERE(ppd(:)>BGSpacing%Octree%Pmax(:)) ppd(:)=BGSpacing%Octree%Pmax(:)

                   pOct_Cell => OctreeMesh_CellSearch(ppd,Level,BGSpacing%Octree)
                   !     IF(pOct_Cell%Mark==BGSpacing%BoxSources(n)%LevelMark) CYCLE
                   !     pOct_Cell%Mark = BGSpacing%BoxSources(n)%LevelMark
                   CALL Evaluate_OctBack(pOct_Cell,BGSpacing%BoxSources(n))
                ENDDO
             ENDDO
          ENDDO
       ENDIF

    ENDDO


    !---- smoothen background Quad grids by nodes
    CALL Bound_OctBack( )

    !---- mesh gradation control
    IF(BGSpacing%Gradation_Factor<3.0)THEN

       ICounter_OctNode(1:BGSpacing%Octree%numNodes) = 0
       IF(BGSpacing%Model<0)THEN
          ALLOCATE( Eigen_OctNode(3, BGSpacing%Octree%numNodes) )
          DO i = 1, BGSpacing%Octree%numNodes
             CALL  Metric3D_eigenValue(BGSpacing%Metric_OctNode(:,i),dr)
             Eigen_OctNode(:,i) = dr(:)
          ENDDO
       ENDIF

       DO kdir = 1,8
          i0 = 1
          j0 = 1
          k0 = 1
          IF(MOD(kdir,2)==0)   i0 = BGSpacing%Octree%imax
          IF(MOD(kdir+1,4)<=1) j0 = BGSpacing%Octree%jmax
          IF(kdir>=5)          k0 = BGSpacing%Octree%kmax
          i1 = 1 + BGSpacing%Octree%imax - i0
          j1 = 1 + BGSpacing%Octree%jmax - j0
          k1 = 1 + BGSpacing%Octree%kmax - k0
          id = 1
          jd = 1
          kd = 1
          IF(i0>i1) id = -1
          IF(j0>j1) jd = -1
          IF(k0>k1) kd = -1

          DO i=i0,i1,id
             DO j=j0,j1,jd
                DO k=k0,k1,kd
                   pOct_Cell => BGSpacing%Octree%Cells(i,j,k)
                   CALL Gradation_OctBack(pOct_Cell,kdir)
                ENDDO
             ENDDO
          ENDDO

          DO i = 1, BGSpacing%Octree%numNodes
             IF(ICounter_OctNode(i)==Kdir) EXIT
          ENDDO
          IF(i>BGSpacing%Octree%numNodes) EXIT          
       ENDDO

       IF(BGSpacing%Model<0)THEN
          DEALLOCATE( Eigen_OctNode )
       ENDIF

    ENDIF

    IF(BGSpacing%Model>0)THEN
       BGSpacing%Model = 3
    ELSE
       BGSpacing%Model = -3
    ENDIF

    DEALLOCATE( ICounter_OctNode, FCounter_OctNode )

    CALL SpacingStorage_FillOctCell(BGSpacing)

    RETURN

  CONTAINS

    !>
    !!    Set value of a oct-grid and by all the nodes of its sub grid.
    !<
    RECURSIVE SUBROUTINE Evaluate_OctBack(pOct_Cell,BoxS)
      IMPLICIT NONE

      TYPE(BoxSource_Type), INTENT(IN) :: BoxS
      INTEGER :: ii,ip,idirct
      REAL*8  :: pp1(3),pp2(3), v1(6)
      TYPE (OctreeMesh_Cell_Type), POINTER ::pOct_Cell,p2Oct_Cell

      pp1(:)  = BoxS%pMin(:)
      pp2(:)  = BoxS%pMax(:)

      IF(pOct_Cell%Level == 0 )RETURN      !--- Empty Cell
      IF(pOct_Cell%Bisected)THEN
         DO idirct=1,8
            p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
            ip = p2Oct_Cell%Nodes(1)
            IF(BGSpacing%Octree%Posit(1,ip)>=pp2(1)) CYCLE
            IF(BGSpacing%Octree%Posit(2,ip)>=pp2(2)) CYCLE
            IF(BGSpacing%Octree%Posit(3,ip)>=pp2(3)) CYCLE
            ip = p2Oct_Cell%Nodes(7)
            IF(BGSpacing%Octree%Posit(1,ip)<=pp1(1)) CYCLE
            IF(BGSpacing%Octree%Posit(2,ip)<=pp1(2)) CYCLE
            IF(BGSpacing%Octree%Posit(3,ip)<=pp1(3)) CYCLE
            CALL Evaluate_OctBack(p2Oct_Cell,BoxS)
         ENDDO
      ELSE

         DO ii=1,8
            ip = pOct_Cell%Nodes(ii)
            IF(ICounter_OctNode(ip)==BoxS%LevelMark) CYCLE

            IF(BGSpacing%Octree%Posit(1,ip)>pp2(1)) CYCLE
            IF(BGSpacing%Octree%Posit(2,ip)>pp2(2)) CYCLE
            IF(BGSpacing%Octree%Posit(3,ip)>pp2(3)) CYCLE
            IF(BGSpacing%Octree%Posit(1,ip)<pp1(1)) CYCLE
            IF(BGSpacing%Octree%Posit(2,ip)<pp1(2)) CYCLE
            IF(BGSpacing%Octree%Posit(3,ip)<pp1(3)) CYCLE

            IF(BGSpacing%Model>0)THEN
               BGSpacing%Scalar_OctNode(ip) = MIN(BGSpacing%Scalar_OctNode(ip), BoxS%Scalar)
            ELSE
               v1 = BGSpacing%Metric_OctNode(:,ip)
               IF(BoxS%isotr)THEN
                  BGSpacing%Metric_OctNode(:,ip) = Metric3D_ScalarIntersect(v1, BoxS%Scalar )
               ELSE
                  BGSpacing%Metric_OctNode(:,ip) = Metric3D_Intersect(v1, BoxS%fMtr )
               ENDIF
            ENDIF
            ICounter_OctNode(ip) = BoxS%LevelMark
            FCounter_OctNode(ip) = FCounter_OctNode(ip) + 1
         ENDDO

      ENDIF

      RETURN
    END SUBROUTINE Evaluate_OctBack

    !>
    !!    Modify background's boundary.
    !!    If FCounter_OctNode (ip)>0 then the value of point ip has been set by source points,
    !!        otherwise it is set as the basic value.
    !!    Some points with basic value are located at the background's boundary,
    !!        and they will be reset here to smoothen this boundary.
    !!    Make sure that the leaf cells of BGSpacing%Octree have been set in advance.
    !<
    SUBROUTINE Bound_OctBack( )
      IMPLICIT NONE

      INTEGER :: ii,jj,ip(8),nfree,irank(8),ivisit(8),jp,idirct,ie
      TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
      REAL*8  :: fMtrs(6,8), fMtr1(6), fMtr2(6),tc
      REAL*8  :: Scalars(8), Scalar1, Scalar2

      ICounter_OctNode(1:BGSpacing%Octree%numNodes) = 0

      DO ie = 1, BGSpacing%Octree%numCells
         pOct_Cell => BGSpacing%Octree%LeafCells(ie)%to
         ip(:)     =  pOct_Cell%Nodes(:)

         nfree = 0
         DO ii=1,8
            IF(FCounter_OctNode(ip(ii))<=0.1D0)THEN
               nfree    = nfree + 1
               irank(ii) = 0
            ELSE
               irank(ii) = 4
            ENDIF
         ENDDO

         IF(nfree==0 .OR. nfree==8) CYCLE

         IF(BGSpacing%Model<0)THEN
            fMtrs(:,1:8) = BGSpacing%Metric_OctNode(:,ip(1:8))
         ELSE
            Scalars(1:8) = BGSpacing%Scalar_OctNode(ip(1:8))
         ENDIF
         ivisit(1:8)  = 0

         DO nfree=4,2,-1
            DO ii=1,8
               IF(irank(ii)/=nfree) CYCLE
               DO jj=1,3
                  jp = iCut_Hex(jj,ii)
                  IF(irank(jp)>=nfree) CYCLE
                  IF(irank(jp)==0)THEN
                     IF(BGSpacing%Model<0)THEN
                        fMtrs(:,jp) = fMtrs(:,ii)
                     ELSE
                        Scalars(jp) = Scalars(ii)
                     ENDIF
                     irank(jp)   = nfree-1
                     ivisit(jp)  = 1
                  ELSE
                     ivisit(jp) = ivisit(jp)+1
                     tc = 1.d0/ivisit(jp)
                     IF(BGSpacing%Model<0)THEN
                        fMtrs(:,jp) = Metric3D_Interpolate(fMtrs(:,jp), fMtrs(:,ii),  &
                             tc, BGSpacing%Interp_Method)
                     ELSE
                        Scalars(jp) = Scalar_Interpolate(Scalars(jp), Scalars(ii),    &
                             tc, BGSpacing%Interp_Method)
                     ENDIF
                  ENDIF
               ENDDO
            ENDDO
         ENDDO

         DO ii=1,8
            IF(irank(ii)==4) CYCLE
            IF(irank(ii)==0) CALL Error_STOP ( '--- Bound_OctBack :: rank(ii)==0')
            jp = ip(ii)
            IF(BGSpacing%Model<0)THEN
               IF(ICounter_OctNode(jp)==0)THEN
                  BGSpacing%Metric_OctNode(:,jp) = fMtrs(:,ii)
               ELSE
                  fMtr1 = BGSpacing%Metric_OctNode(:,jp)
                  fMtr2 = fMtrs(:,ii)
                  tc    = float(irank(ii)) / float((ICounter_OctNode(jp)+irank(ii)))
                  BGSpacing%Metric_OctNode(:,jp) = Metric3D_Interpolate(    &
                       fMtr1, fMtr2, tc, BGSpacing%Interp_Method)
               ENDIF
            ELSE
               IF(ICounter_OctNode(jp)==0)THEN
                  BGSpacing%Scalar_OctNode(jp) = Scalars(ii)
               ELSE
                  Scalar1 = BGSpacing%Scalar_OctNode(jp)
                  Scalar2 = Scalars(ii)
                  tc    = float(irank(ii)) / float((ICounter_OctNode(jp)+irank(ii)))
                  BGSpacing%Scalar_OctNode(jp) = Scalar_Interpolate(    &
                       Scalar1, Scalar2, tc, BGSpacing%Interp_Method)
               ENDIF
            ENDIF
            ICounter_OctNode(jp) = ICounter_OctNode(jp) + irank(ii)
         ENDDO

      ENDDO

      RETURN
    END SUBROUTINE Bound_OctBack

    !>
    !!   Gradate the values on the nodes of a cell of octree background mesh
    !!        BGSpacing%Octree.
    !!   @param[in] pOct_Cell  : the pointer to a cell (may not be a leaf cell).
    !!   @param[in] Kdir       : the direction of gradation.
    !<
    RECURSIVE SUBROUTINE Gradation_OctBack(pOct_Cell,Kdir)
      IMPLICIT NONE

      INTEGER :: Kdir, n, ii,jj, ip(8)
      TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
      REAL*8  :: fMtrs(6,8),fMtr1(6),fMtr2(6), Scalars(8), Scalar2, egn(3)
      REAL*8  :: fc, dd, pp(3,8), Factor
      INTEGER :: Order(8,8),idirct, mark(8)
      DATA Order(:,1) / 1,2,4,3,5,6,8,7 /
      DATA Order(:,2) / 2,1,3,4,6,5,7,8 /
      DATA Order(:,3) / 4,3,1,2,8,7,5,6 /
      DATA Order(:,4) / 3,4,2,1,7,8,6,5 /
      DATA Order(:,5) / 5,6,8,7,1,2,4,3 /
      DATA Order(:,6) / 6,5,7,8,2,1,3,4 /
      DATA Order(:,7) / 8,7,5,6,4,3,1,2 /
      DATA Order(:,8) / 7,8,6,5,3,4,2,1 /


      IF(pOct_Cell%Bisected)THEN
         DO idirct = 1,8
            p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,Order(idirct,Kdir))
            CALL Gradation_OctBack(p2Oct_Cell,Kdir)
         ENDDO
      ELSE

         ip(:)   = pOct_Cell%Nodes(:)
         mark(:) = 0

         pp(:,1:8) = 0.d0
         DO ii=1,8
            IF(ii==2 .OR. ii==3 .OR. ii==6 .OR. ii==7) pp(1,ii) = BGSpacing%Octree%DX(pOct_Cell%Level)
            IF(ii==3 .OR. ii==4 .OR. ii==7 .OR. ii==8) pp(2,ii) = BGSpacing%Octree%DY(pOct_Cell%Level)
            IF(ii>=5)          pp(3,ii) = BGSpacing%Octree%DZ(pOct_Cell%Level)
         ENDDO

         Factor = BGSpacing%Gradation_Factor / BGSpacing%BasicSize

         IF(BGSpacing%Model<0)THEN

            fMtrs(:,1:8) = BGSpacing%Metric_OctNode(:,ip(1:8))

            DO jj=1,8
               IF(ICounter_OctNode(ip(jj))<Kdir-1) CYCLE
               DO ii=1,8
                  IF(ii==jj) CYCLE
                  dd = Metric3D_Distance(pp(:,ii),pp(:,jj),fMtrs(:,jj))
                  fc = MIN( 1.d0 + dd * Factor,  2.d0 )
                  fMtr2 = fMtrs(:,jj) /(fc*fc)
                  fMtr1 = Metric3D_Intersect(fMtrs(:,ii),fMtr2)
                  CALL  Metric3D_eigenValue(fMtr1,egn)
                  IF(  egn(1) > 1.001d0 * Eigen_OctNode(1,ip(ii)) .OR.    &
                       egn(2) > 1.001d0 * Eigen_OctNode(2,ip(ii)) .OR.    &
                       egn(3) > 1.001d0 * Eigen_OctNode(3,ip(ii)) )THEN
                     mark(ii) = 1
                     fMtrs(:,ii) = fMtr1
                     Eigen_OctNode(:,ip(ii)) = egn(:)
                  ENDIF
               ENDDO
            ENDDO

            DO ii = 1,8
               IF(mark(ii)==1)THEN
                  BGSpacing%Metric_OctNode(:,ip(ii)) = fMtrs(:,ii)
                  ICounter_OctNode(ip(ii)) = Kdir
               ENDIF
            ENDDO

         ELSE

            Scalars(1:8) = BGSpacing%Scalar_OctNode(ip(1:8))
            DO jj=1,8
               IF(ICounter_OctNode(ip(jj))<Kdir-1) CYCLE
               DO ii=1,8
                  IF(ii==jj) CYCLE
                  dd = Geo3D_Distance(pp(:,ii),pp(:,jj)) / Scalars(jj)
                  fc = MIN( 1.d0 + dd * Factor,  2.d0 )
                  Scalar2 = Scalars(jj) * fc
                  IF(Scalar2<Scalars(ii))THEN
                     mark(ii) = 1
                     Scalars(ii) = Scalar2
                  ENDIF
               ENDDO
            ENDDO

            DO ii = 1,8
               IF(mark(ii)==1)THEN
                  BGSpacing%Scalar_OctNode(ip(ii)) = Scalars(ii)
                  ICounter_OctNode(ip(ii)) = Kdir
               ENDIF
            ENDDO

         ENDIF

      ENDIF

      RETURN
    END SUBROUTINE Gradation_OctBack


  END SUBROUTINE SpacingStorage_BuildOctree


  !*******************************************************************************
  !>
  !!   Read .bac file and build an tetrahedral background mesh.
  !!   @param[in]  Stretch_Limit : the limitation of stretching.
  !<       
  !*******************************************************************************
  SUBROUTINE SpacingStorage_read_bac(JobName,JobNameLength,BGSpacing,isotr, Stretch_Limit)
    IMPLICIT NONE
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    TYPE(SpacingStorageType), INTENT(INOUT) :: BGSpacing
    LOGICAL, INTENT(in) :: isotr
    REAL*8,  INTENT(in) :: Stretch_Limit
    REAL*8  :: Stretch(3), fMap(3,3), strlim
    REAL*8  :: p(3), p1(3), p2(3),p3(3), pm(3), stemp(7), pMin(3), pMax(3)
    REAL*8  :: r11,r12, r21,r22, r31,r32, scale1, scale2, scale3
    REAL*8  :: str1, str2, str3
    INTEGER :: itemp(10), NB_Point, NB_Tet, nb_ptsc, nb_lnsc, nb_trsc, nb_box
    INTEGER :: IT, i, ip, n1, n2, n3, NS, LevelMark, anidex
    REAL*8,  DIMENSION(:,:),  POINTER :: Posit
    INTEGER, DIMENSION(:,:),  POINTER :: IP_Tet
    TYPE(BoxSource_Type), POINTER :: BoxS

    !--- Read background mesh .bac
    OPEN(1,file=JobName(1:JobNameLength)//'.bac', status='old')
    READ(1,*)
    CALL Read_INT_Array(itemp,N1,1)
    NB_Point = 0
    NB_Tet   = 0
    nb_ptsc  = 0
    nb_lnsc  = 0
    nb_trsc  = 0
    nb_box   = 0
    IF(N1>=1) NB_Point = itemp(1)
    IF(N1>=2) NB_Tet   = itemp(2)
    IF(N1>=3) nb_ptsc  = itemp(3)
    IF(N1>=4) nb_lnsc  = itemp(4)
    IF(N1>=5) nb_trsc  = itemp(5)
    IF(N1>=6) nb_box   = itemp(6)
    IF(nb_box>0)THEN
       NS = 6
    ELSE IF(nb_trsc>0)THEN
       NS = 5
    ELSE IF(nb_lnsc>0)THEN
       NS = 4
    ELSE IF(nb_ptsc>0)THEN
       NS = 3
    ELSE
       NS = 2
    ENDIF


    IF(isotr)THEN
       BGSpacing%Model = 2
       ALLOCATE(BGSpacing%Scalar_Point(NB_Point))
    ELSE
       BGSpacing%Model = -2
       ALLOCATE(BGSpacing%fMap_Point(3,3,NB_Point))
    ENDIF

    ALLOCATE(IP_Tet(4,NB_Tet), Posit(3,NB_Point))
    
    DO ip=1,NB_Point
       READ(1,*)i,Posit(1:3,i)
       READ(1,*)fMap(1,:),Stretch(1)
       READ(1,*)fMap(2,:),Stretch(2)
       READ(1,*)fMap(3,:),Stretch(3)
       IF(BGSpacing%Model == -2)THEN
          strlim = Stretch_Limit * MIN(Stretch(1),Stretch(2),Stretch(3))
          WHERE(Stretch(1:3) > strlim) Stretch(1:3) = strlim
          BGSpacing%fMap_Point(1,1:3,i) = fMap(1,:)/Stretch(1)
          BGSpacing%fMap_Point(2,1:3,i) = fMap(2,:)/Stretch(2)
          BGSpacing%fMap_Point(3,1:3,i) = fMap(3,:)/Stretch(3)
       ELSE IF(BGSpacing%Model == 2)THEN
          BGSpacing%Scalar_Point(i) = MAX(Stretch(1),Stretch(2),Stretch(3))
       ENDIF
       BGSpacing%BasicSize = MAX(BGSpacing%BasicSize, Stretch(1), Stretch(2), Stretch(3))
       BGSpacing%MinSize   = MIN(BGSpacing%MinSize, Stretch(1), Stretch(2), Stretch(3))
       WHERE(BGSpacing%pMin(1:3)>Posit(1:3,i)) BGSpacing%pMin(1:3) = Posit(1:3,i)
       WHERE(BGSpacing%pMax(1:3)<Posit(1:3,i)) BGSpacing%pMax(1:3) = Posit(1:3,i)
    ENDDO
    
    print *,' Basic and Min sise:',BGSpacing%BasicSize ,BGSpacing%MinSize

    CALL SpacingStorage_CheckBasicSize(BGSpacing)
    IF(NB_Point==0 .OR. (BGSpacing%BasicSize-BGSpacing%MinSize)/BGSpacing%BasicSize<1.e-2)THEN
       IF(BGSpacing%Model==-2)THEN
          IF(nb_ptsc==0 .AND. nb_lnsc==0 .AND. nb_trsc==0)THEN
             BGSpacing%Model = -1
          ELSE
             BGSpacing%Model = -4
          ENDIF
       ELSE IF(BGSpacing%Model==2)THEN
          IF(nb_ptsc==0 .AND. nb_lnsc==0 .AND. nb_trsc==0)THEN
             BGSpacing%Model = 1
          ELSE
             BGSpacing%Model = 4
          ENDIF
       ENDIF
    ENDIF

    IF(BGSpacing%Model == -2)THEN
       DO ip=1,NB_Point
          BGSpacing%fMap_Point(:,:,ip) = BGSpacing%fMap_Point(:,:,ip) * BGSpacing%BasicSize
       ENDDO
    ELSE IF(BGSpacing%Model == 2)THEN
       DO ip=1,NB_Point
          BGSpacing%Scalar_Point(ip) = BGSpacing%Scalar_Point(ip) / BGSpacing%BasicSize
       ENDDO
    ENDIF

    DO it=1,NB_Tet
       READ(1,*)i,IP_Tet(1:4,i)
    ENDDO

    CALL SourceGroup_Allocate(BGSpacing%Sources, nb_ptsc, nb_lnsc, nb_trsc, .FALSE.)
    !--  read point Sources
    IF(NS>=3) READ(1,*)
    DO i=1,nb_ptsc
       READ(1,*)
       READ(1,*)p(1:3),scale1,r11,r12
       BGSpacing%MinSize = MIN(BGSpacing%MinSize, scale1)
       pm = p - r12
       WHERE(BGSpacing%pMin(1:3)>pm(1:3)) BGSpacing%pMin(1:3) = pm(1:3)
       pm = p + r12
       WHERE(BGSpacing%pMax(1:3)<pm(1:3)) BGSpacing%pMax(1:3) = pm(1:3)
       scale1 = scale1/BGSpacing%BasicSize
       CALL SourceGroup_AddPointSource(BGSpacing%Sources,p,scale1,r11,r12)
    ENDDO

    !--  read line Sources
    IF(NS>=4) READ(1,*)
    DO i=1,nb_lnsc
       READ(1,*)
       CALL Read_real8_Array(stemp,N1,1)
       IF(N1<6) CALL Error_STOP ( '--- read .bac')
       p1(1:3) = stemp(1:3)
       scale1  = stemp(4)
       r11     = stemp(5)
       r12     = stemp(6)
       IF(N1>6)THEN
          str1 = stemp(7)
       ELSE
          str1 = 1.d0
       ENDIF
       CALL Read_real8_Array(stemp,N2,1)
       IF(N2<6) CALL Error_STOP ( '--- read .bac')
       p2(1:3) = stemp(1:3)
       scale2  = stemp(4)
       r21     = stemp(5)
       r22     = stemp(6)
       IF(N2>6)THEN
          str2 = stemp(7)
       ELSE
          str2 = 1.d0
       ENDIF
       BGSpacing%MinSize = MIN(BGSpacing%MinSize, ABS(scale1), ABS(scale2))
       pm = p1 - r12
       WHERE(BGSpacing%pMin(1:3)>pm(1:3)) BGSpacing%pMin(1:3) = pm(1:3)
       pm = p1 + r12
       WHERE(BGSpacing%pMax(1:3)<pm(1:3)) BGSpacing%pMax(1:3) = pm(1:3)
       pm = p2 - r22
       WHERE(BGSpacing%pMin(1:3)>pm(1:3)) BGSpacing%pMin(1:3) = pm(1:3)
       pm = p2 + r22
       WHERE(BGSpacing%pMax(1:3)<pm(1:3)) BGSpacing%pMax(1:3) = pm(1:3)
       scale1 = scale1/BGSpacing%BasicSize
       scale2 = scale2/BGSpacing%BasicSize

       IF( BGSpacing%Model>0 .OR. (dabs(str1-1.d0)<1.d-3 .AND. dabs(str2-1.d0)<1.d-3) )THEN
          CALL SourceGroup_AddLineSource(BGSpacing%Sources,p1,scale1,r11,r12,p2,scale2,r21,r22)
       ELSE
          CALL SourceGroup_AddAnisLineSource(BGSpacing%Sources,p1,scale1,r11,r12, str1,   &
               p2,scale2,r21,r22, str2)
       ENDIF
    ENDDO

    !--  read triangle Sources
    IF(NS>=5) READ(1,*)
    DO i=1,nb_trsc
       READ(1,*)
       CALL Read_real8_Array(stemp,N1,1)
       IF(N1<6) CALL Error_STOP ( '--- read .bac')
       p1(1:3) = stemp(1:3)
       scale1  = stemp(4)
       r11     = stemp(5)
       r12     = stemp(6)
       IF(N1>6)THEN
          str1 = stemp(7)
       ELSE
          str1 = 1.d0
       ENDIF
       CALL Read_real8_Array(stemp,N2,1)
       IF(N2<6) CALL Error_STOP ( '--- read .bac')
       p2(1:3) = stemp(1:3)
       scale2  = stemp(4)
       r21     = stemp(5)
       r22     = stemp(6)
       IF(N2>6)THEN
          str2 = stemp(7)
       ELSE
          str2 = 1.d0
       ENDIF
       CALL Read_real8_Array(stemp,N3,1)
       IF(N3<6) CALL Error_STOP ( '--- read .bac')
       p3(1:3) = stemp(1:3)
       scale3  = stemp(4)
       r31     = stemp(5)
       r32     = stemp(6)
       IF(N3>6)THEN
          str3 = stemp(7)
       ELSE
          str3 = 1.d0
       ENDIF
       BGSpacing%MinSize = MIN(BGSpacing%MinSize, ABS(scale1), ABS(scale2), ABS(scale3))
       pm = p1 - r12
       WHERE(BGSpacing%pMin(1:3)>pm(1:3)) BGSpacing%pMin(1:3) = pm(1:3)
       pm = p1 + r12
       WHERE(BGSpacing%pMax(1:3)<pm(1:3)) BGSpacing%pMax(1:3) = pm(1:3)
       pm = p2 - r22
       WHERE(BGSpacing%pMin(1:3)>pm(1:3)) BGSpacing%pMin(1:3) = pm(1:3)
       pm = p2 + r22
       WHERE(BGSpacing%pMax(1:3)<pm(1:3)) BGSpacing%pMax(1:3) = pm(1:3)
       pm = p3 - r32
       WHERE(BGSpacing%pMin(1:3)>pm(1:3)) BGSpacing%pMin(1:3) = pm(1:3)
       pm = p3 + r32
       WHERE(BGSpacing%pMax(1:3)<pm(1:3)) BGSpacing%pMax(1:3) = pm(1:3)
       IF(scale1>0)THEN
          scale1 = scale1 / BGSpacing%BasicSize
          scale2 = scale2 / BGSpacing%BasicSize
          scale3 = scale3 / BGSpacing%BasicSize
          IF( BGSpacing%Model>0 .OR. (dabs(str1-1.d0)<1.d-3 .AND.   &
               dabs(str2-1.d0)<1.d-3 .AND. dabs(str3-1.d0)<1.d-3) )THEN
             CALL SourceGroup_AddTriSource(BGSpacing%Sources,p1,scale1,r11,r12,  &
                  p2,scale2,r21,r22, p3,scale3,r31,r32)
          ELSE
             CALL SourceGroup_AddAnisTriSource(BGSpacing%Sources,p1,scale1,r11,r12,str1,  &
                  p2,scale2,r21,r22,str2, p3,scale3,r31,r32,str3)
          ENDIF
       ELSE
          scale1 = ABS(scale1)/BGSpacing%BasicSize
          IF(N1<=6) str1 = 1.d0
          CALL SourceGroup_AddRingSource(BGSpacing%Sources,p1,scale1,r11,r12,str1, p2, p3)
       ENDIF
    ENDDO


    !--  read box Sources
    BGSpacing%NB_BoxSource = nb_box
    CALL allc_BoxSource(BGSpacing%BoxSources, nb_box, 'BoxSources in reading .bac')
    IF(NS>=6) READ(1,*)
    DO i = 1, BGSpacing%NB_BoxSource
       READ(1,*)
       READ(1,*) LevelMark, anidex
       READ(1,*) pMin(1:3), pMax(1:3)
       IF(anidex==0)THEN
          READ(1,*) Stretch(1)
       ELSE
          READ(1,*)  fMap(1,:),Stretch(1)
          READ(1,*)  fMap(2,:),Stretch(2)
          READ(1,*)  fMap(3,:),Stretch(3)
       ENDIF

       WHERE(BGSpacing%pMin(1:3) > pMin(1:3))  BGSpacing%pMin(1:3) = pMin(1:3)
       WHERE(BGSpacing%pMax(1:3) < pMax(1:3))  BGSpacing%pMax(1:3) = pMax(1:3)

       BoxS => BGSpacing%BoxSources(i)

       BoxS%pMin      = pMin
       BoxS%pMax      = pMax
       BoxS%LevelMark = LevelMark

       IF(anidex==0)THEN
          BoxS%isotr     = .TRUE.
          BGSpacing%MinSize = MIN(BGSpacing%MinSize,Stretch(1))
          BoxS%Scalar = Stretch(1) / BGSpacing%BasicSize
       ELSE
          BoxS%isotr     = .FALSE.
          strlim = Stretch_Limit * MIN(Stretch(1),Stretch(2),Stretch(3))
          WHERE(Stretch(1:3) > strlim) Stretch(1:3) = strlim
          BGSpacing%MinSize = MIN(BGSpacing%MinSize,Stretch(1),Stretch(2),Stretch(3))

          fMap(1,:)    = fMap(1,:) * (BGSpacing%BasicSize/Stretch(1))
          fMap(2,:)    = fMap(2,:) * (BGSpacing%BasicSize/Stretch(2))
          fMap(3,:)    = fMap(3,:) * (BGSpacing%BasicSize/Stretch(3))
          BoxS%fMtr(:) = Mapping3D_to_Metric(fMap)
       ENDIF
    ENDDO

    CLOSE (1)


    !--- set AdTree for the background mesh

    CALL ADTree_SetTree(BGSpacing%ADTree, Posit, IP_Tet, NB_Tet, NB_Point)
    DEALLOCATE(IP_Tet, Posit)
    print *,' Basic and Min sise:',BGSpacing%BasicSize ,BGSpacing%MinSize

  END SUBROUTINE SpacingStorage_read_bac


  !*******************************************************************************
  !>
  !!   Read .Obac file for an Octree background mesh.
  !<
  !*******************************************************************************
  SUBROUTINE SpacingStorage_read_Obac(JobName,JobNameLength,BGSpacing)
    IMPLICIT NONE
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    TYPE(SpacingStorageType), INTENT(INOUT) :: BGSpacing
    INTEGER :: ip  

    !--- read octree background

    OPEN(1,file=JobName(1:JobNameLength)//'.Obac', status='unknown',   &
         form='UNFORMATTED',err=111)

    CALL OctreeMesh_Input(1,BGSpacing%Octree)
    READ(1,err=111) BGSpacing%BasicSize, BGSpacing%MinSize
    READ(1,err=111) BGSpacing%Model
    IF(BGSpacing%Model==-3)THEN
       ALLOCATE(BGSpacing%Metric_OctNode(6,BGSpacing%Octree%numNodes))
       READ(1,err=111) (BGSpacing%Metric_OctNode(1:6,ip), ip=1,BGSpacing%Octree%numNodes)
    ELSE IF(BGSpacing%Model==3)THEN
       ALLOCATE(BGSpacing%Scalar_OctNode(BGSpacing%Octree%numNodes))
       READ(1,err=111) (BGSpacing%Scalar_OctNode(ip), ip=1,BGSpacing%Octree%numNodes)
    ELSE IF(BGSpacing%Model==-1)THEN
       READ(1,err=111) BGSpacing%BasicMap
    ELSE IF(BGSpacing%Model/=1)THEN
       WRITE(*,*) ' '
       WRITE(*,*) ' Error---- BGSpacing%Model reading from .Obac file :', BGSpacing%Model
       CALL Error_STOP ( '--- SpacingStorage_read_Obac')
    ENDIF

    CLOSE(1)

    IF(BGSpacing%CheckLevel>3)THEN
       CALL OctreeMesh_Check(BGSpacing%Octree)
    ENDIF

    CALL OctreeMesh_getLeafCells(BGSpacing%Octree)
    CALL SpacingStorage_FillOctCell(BGSpacing)

    RETURN
111 WRITE(*,*) 'Error in reading file ',JobName(1:JobNameLength)//'.Obac'
    CALL Error_STOP ( '--- SpacingStorage_Input')
  END SUBROUTINE SpacingStorage_read_Obac

  !*******************************************************************************
  !>
  !!     Output the tet. background mesh to file *_bac.plt.
  !!     Also output the stretch values to file *_bac.unk.
  !<
  !*******************************************************************************
  SUBROUTINE SpacingStorage_write_Obac(JobName,JobNameLength,BGSpacing)
    IMPLICIT NONE
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    INTEGER :: IP

    IF(ABS(BGSpacing%Model)/=3 .AND. ABS(BGSpacing%Model)/=1)THEN
       WRITE(*,*)' Warning---- can not write a .Obac file with model=',BGSpacing%Model
       RETURN
    ENDIF

    OPEN(1,file=JobName(1:JobNameLength)//'.Obac', status='unknown',   &
         form='UNFORMATTED',err=111)
    CALL OctreeMesh_Output(1,BGSpacing%Octree)
    WRITE(1,err=111) BGSpacing%BasicSize, BGSpacing%MinSize
    WRITE(1,err=111) BGSpacing%Model
    IF(BGSpacing%Model==-3)THEN
       WRITE(1,err=111) (BGSpacing%Metric_OctNode(1:6,ip), ip=1,BGSpacing%Octree%numNodes)
    ELSE IF(BGSpacing%Model==3)THEN
       WRITE(1,err=111) (BGSpacing%Scalar_OctNode(ip), ip=1,BGSpacing%Octree%numNodes)
    ELSE IF(BGSpacing%Model==-1)THEN
       WRITE(1,err=111) BGSpacing%BasicMap
    ENDIF
    CLOSE(1)

    RETURN
111 WRITE(*,*) 'Error in writing file ',JobName(1:JobNameLength)//'.Obac'
    CALL Error_STOP ( '--- SpacingStorage_write_Obac')
  END SUBROUTINE SpacingStorage_write_Obac


  !*******************************************************************************
  !>
  !!  Clean useless arrays to release memory.
  !!  @param[in,out] BGSpacing  : the sbackground spacing.
  !!
  !!  Reminder : Do NOT call this routine 
  !!             before writting a octree background to a file.
  !<
  !*******************************************************************************
  SUBROUTINE SpacingStorage_Clean(BGSpacing)
    TYPE(SpacingStorageType), INTENT(INOUT) :: BGSpacing

    IF(ABS(BGSpacing%Model)==1 .OR. ABS(BGSpacing%Model)==3) THEN
       CALL SourceGroup_Clear(BGSpacing%Sources)
    ENDIF

    IF(ABS(BGSpacing%Model)/=2) THEN
       CALL ADTree_Clear(BGSpacing%ADTree)
       IF(ASSOCIATED( BGSpacing%fMap_Point   )) DEALLOCATE(BGSpacing%fMap_Point)
       IF(ASSOCIATED( BGSpacing%Scalar_Point )) DEALLOCATE(BGSpacing%Scalar_Point)
    ENDIF

    IF(ABS(BGSpacing%Model)/=3) THEN
       CALL OctreeMesh_Clear(BGSpacing%Octree)
       IF(ASSOCIATED( BGSpacing%Mapping_OctNode )) DEALLOCATE(BGSpacing%Mapping_OctNode)
       IF(ASSOCIATED( BGSpacing%Scalar_OctNode  )) DEALLOCATE(BGSpacing%Scalar_OctNode)
       IF(ASSOCIATED( BGSpacing%Mapping_OctCell )) DEALLOCATE(BGSpacing%Mapping_OctCell)
       IF(ASSOCIATED( BGSpacing%Scalar_OctCell  )) DEALLOCATE(BGSpacing%Scalar_OctCell)
    ELSE IF(BGSpacing%OctCell_Vary) THEN
       IF(ASSOCIATED( BGSpacing%Mapping_OctCell )) DEALLOCATE(BGSpacing%Mapping_OctCell)
       IF(ASSOCIATED( BGSpacing%Scalar_OctCell  )) DEALLOCATE(BGSpacing%Scalar_OctCell)    
    ELSE
       IF(ASSOCIATED( BGSpacing%Mapping_OctNode )) DEALLOCATE(BGSpacing%Mapping_OctNode)
       IF(ASSOCIATED( BGSpacing%Scalar_OctNode  )) DEALLOCATE(BGSpacing%Scalar_OctNode)
    ENDIF

    IF(ASSOCIATED( BGSpacing%BoxSources     ))  DEALLOCATE(BGSpacing%BoxSources)
    IF(ASSOCIATED( BGSpacing%Metric_OctNode ))  DEALLOCATE(BGSpacing%Metric_OctNode)    

  END SUBROUTINE SpacingStorage_clean



END MODULE SpacingStorageGen

