!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Surface mesh storage, including the defination of type and the basic functions.
!<
MODULE SurfaceMeshStorage
  USE array_allocator
  USE CellConnectivity
  USE Geometry3D
  USE Geometry2D
  USE DiffGeometry
  USE LinkAssociation
  USE NodeNetTree
  USE PtADTreeModule
  USE HighOrderCell
  IMPLICIT NONE

  TYPE :: SurfaceMeshStorageType

     !>  (5,*) connectivities of each triangle.
     !!        The first three components are the three nodes of the triangle.
     !!        The 5-th component is region number.
     !!        The 4-th component is empty. 
     !!        For a volume mesh, it indicates the adjeacent cell;
     !!        for super-patch problem, it is the super-patch number.
     INTEGER, DIMENSION(:,:),  POINTER :: IP_Tri => null()

     !>  (5,*) connectivities of each quad.
     INTEGER, DIMENSION(:,:),  POINTER :: IP_Quad => null()

     !>  (3,*) adjacent triangle of each triangle.
     !!         Basically, triangle Next_Tri(i,it) is opposite to node IP_Tri(i,it) .
     INTEGER, DIMENSION(:,:),  POINTER :: Next_Tri => null()

     !>  (3,*) coordinates of each point.
     REAL*8,  DIMENSION(:,:),  POINTER :: Posit => null()

     !>  (2,*) coordinates of each point.
     REAL*8,  DIMENSION(:,:),  POINTER :: Coord => null()

     INTEGER :: NB_Point    = 0                     !< the number of points.
     INTEGER :: NB_Tri      = 0                     !< the number of triangles.
     INTEGER :: NB_Quad     = 0                     !< the number of quads.
     INTEGER :: NB_Seg      = 0                     !< the number of segments.
     INTEGER :: NB_Surf     = 0                     !< the number of surface pieces (regions).
     INTEGER :: NB_Sheet    = 0                     !< the number of sheets (interfaces).
     INTEGER :: NB_Wire     = 0                     !< the number of wires.
     INTEGER :: NB_BD_Point = 0                     !< the number of boundary nodes.
     INTEGER :: NB_Edge     = 0                     !< the number of edges.

     !>   IP_Edge(1:2,*) the two nodes of each edge.
     !!   IP_Edge(3:,*) are middle nodes for high-order mesh.
     INTEGER, DIMENSION(:,:), POINTER :: IP_Edge => null()

     !>   (2,*) the two triangles of each edge.
     !!         Basically, the first triangle has the same orientation with the edge.
     !!         IF ie=ITR_Edge(i,IS)<0, then abs(ie) is a quadrilateral element.
     INTEGER, DIMENSION(:,:), POINTER :: ITR_Edge => null()

     !>   (3,*) the three edges of each triangle.
     !!         The order of the edges follows the array CellConnectivity::iEdge_Tri .
     !!         i.e. edge IED_Tri(i,it) is opposite to node IP_Tri(i,it) .
     INTEGER, DIMENSION(:,:), POINTER :: IED_Tri => null()

     !>   (4,*) the three edges of each quadrilateral.
     !!         The order of the edges follows the array CellConnectivity::iEdge_Quad .
     INTEGER, DIMENSION(:,:), POINTER :: IED_Quad => null()

     !>   The link from a node to the adjacnet triangles. 
     TYPE (LinkAssociationType) :: ITR_Pt

     !>   The link from a node to the adjacnet edges. 
     TYPE (LinkAssociationType) :: IED_Pt

     !>   The link from a node to the adjacnet nodes. 
     TYPE (LinkAssociationType) :: IP_Pt

     !========  as high order mesh

     INTEGER :: GridOrder = 1           !<  =1 linear mesh; =3 cubic order mesh.
     INTEGER :: NB_Psp    = 0           !< the number of supporting nodes.

     !>   Support node for each triangle.
     !!   If IPsp_Tri(i,IT)=0, the i-th supporting nodes is linear position.
     INTEGER, DIMENSION(:,:), POINTER :: IPsp_Tri => null()

     TYPE(HighOrderCellType)  :: Cell   
       
     INTEGER :: NB_DBC = 0                       !< the number of Dirichlet Boundary Condition points
     INTEGER, DIMENSION(:),    POINTER :: IP_DBC => null()      !< the node ID of each DBC point.
     REAL*8,  DIMENSION(:,:),  POINTER :: Pd_DBC => null()      !< (2,*) the target movement of each DBC point.

     INTEGER :: NB_Value  = 0                        !<  the number of variable types.
     REAL*8,  DIMENSION(:,:),  POINTER :: V_Point => null()    !<  (*,*) the variables at each point.
     REAL*8,  DIMENSION(:,:),  POINTER :: V_Cell => null()     !<  (*,*) the variables at each element.


  END TYPE SurfaceMeshStorageType

  INTEGER, PARAMETER :: Surf_allc_increase = 100000
  INTEGER, PARAMETER :: Surf_MaxNB_Surf    = 100000


CONTAINS


  !>
  !!  Output the information of the surface mesh.
  !!  @param[in] Surf  the surface mesh.
  !!  @param[in] IO       the IO channel. 
  !!                      If =6 or not present, output on screen. 
  !<
  SUBROUTINE SurfaceMeshStorage_Info(Surf, IO)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER, INTENT(IN), OPTIONAL :: IO
    IF(PRESENT(IO))THEN  
       WRITE(IO,*) ' NB_Tri,  NB_Point, NB_Surf, NB_Sheet ='
       WRITE(IO,*) Surf%NB_Tri, Surf%NB_Point, Surf%NB_Surf, Surf%NB_Sheet
       IF(Surf%NB_BD_Point/=0) WRITE(IO,*) ' NB_BD_Point=', Surf%NB_BD_Point
    ELSE
       WRITE(*,*) ' NB_Tri,  NB_Point, NB_Surf, NB_Sheet ='
       WRITE(*,*) Surf%NB_Tri, Surf%NB_Point, Surf%NB_Surf, Surf%NB_Sheet
       IF(Surf%NB_BD_Point/=0) WRITE(*,*) ' NB_BD_Point=', Surf%NB_BD_Point
    ENDIF
    RETURN
  END SUBROUTINE SurfaceMeshStorage_Info

  !>
  !!  Measure the surface to get min & max corners.
  !!  @param[in]  Surf           the surface mesh.
  !!  @param[in]  NDIM           the dimension (2 or 3)
  !!  @param[out] Pmin           the minimum corner.
  !!  @param[out] Pmax           the maximum corner.
  !<     
  SUBROUTINE SurfaceMeshStorage_Measure(Surf, NDIM, Pmin, Pmax)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER, INTENT(IN) :: NDIM
    REAL*8, INTENT(OUT) :: Pmin(NDIM), Pmax(NDIM)
    INTEGER :: i, IP  
    IF(NDIM==2)THEN
       Pmax(1:2) = Surf%Coord(1:2,1)
       Pmin(1:2) = Surf%Coord(1:2,1)
       DO IP = 2,Surf%NB_Point
          DO i=1,2
             Pmax(i) = MAX(Pmax(i),Surf%Coord(i,IP))
             Pmin(i) = MIN(Pmin(i),Surf%Coord(i,IP))
          ENDDO
       ENDDO
    ELSE IF(NDIM==3)THEN
       Pmax(1:3) = Surf%Posit(1:3,1)
       Pmin(1:3) = Surf%Posit(1:3,1)
       DO IP = 2,Surf%NB_Point
          DO i=1,3
             Pmax(i) = MAX(Pmax(i),Surf%Posit(i,IP))
             Pmin(i) = MIN(Pmin(i),Surf%Posit(i,IP))
          ENDDO
       ENDDO
    ELSE
       CALL Error_STOP ( '--- SurfaceMeshStorage_Measure:: wrong NDIM')
    ENDIF
    RETURN
  END SUBROUTINE SurfaceMeshStorage_Measure

  !>
  !!    Delete the surface mesh and release memory.
  !<
  SUBROUTINE SurfaceMeshStorage_Clear(Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf

    Surf%NB_Point     = 0
    Surf%NB_Tri       = 0
    Surf%NB_Quad      = 0
    Surf%NB_Seg       = 0
    Surf%NB_Surf      = 0
    Surf%NB_Sheet     = 0
    Surf%NB_Wire      = 0
    Surf%NB_BD_Point  = 0
    Surf%NB_Edge      = 0
    IF(ASSOCIATED(Surf%IP_Tri))   DEALLOCATE(Surf%IP_Tri)
    IF(ASSOCIATED(Surf%IP_Quad))  DEALLOCATE(Surf%IP_Quad)
    IF(ASSOCIATED(Surf%Next_Tri)) DEALLOCATE(Surf%Next_Tri)
    IF(ASSOCIATED(Surf%Posit))    DEALLOCATE(Surf%Posit)
    IF(ASSOCIATED(Surf%Coord))    DEALLOCATE(Surf%Coord)
    IF(ASSOCIATED(Surf%IP_Edge))  DEALLOCATE(Surf%IP_Edge)
    IF(ASSOCIATED(Surf%ITR_Edge)) DEALLOCATE(Surf%ITR_Edge)
    IF(ASSOCIATED(Surf%IED_Tri))  DEALLOCATE(Surf%IED_Tri)
    IF(ASSOCIATED(Surf%IED_Quad)) DEALLOCATE(Surf%IED_Quad)
    CALL LinkAssociation_Clear(Surf%ITR_Pt)
    CALL LinkAssociation_Clear(Surf%IED_Pt)
    CALL LinkAssociation_Clear(Surf%IP_Pt)
    Surf%GridOrder = 1
    Surf%NB_Psp    = 0
    Surf%NB_DBC    = 0
    IF(ASSOCIATED(Surf%IPsp_Tri)) DEALLOCATE(Surf%IPsp_Tri)
    IF(ASSOCIATED(Surf%IP_DBC))   DEALLOCATE(Surf%IP_DBC)
    IF(ASSOCIATED(Surf%Pd_DBC))   DEALLOCATE(Surf%Pd_DBC)
    CALL HighOrderCell_Clear(Surf%Cell)
    Surf%NB_Value  = 0 
    IF(ASSOCIATED(Surf%V_Point))  DEALLOCATE(Surf%V_Point)
    IF(ASSOCIATED(Surf%V_Cell))   DEALLOCATE(Surf%V_Cell)

    RETURN
  END SUBROUTINE SurfaceMeshStorage_Clear


  !>
  !!    Copy basic information of a surface mesh to another.
  !!    Include:     connectivities, position, Next_Tri.   \n
  !!    Not include: Edges, associations.
  !<
  SUBROUTINE SurfaceMeshStorage_BasicCopy(SurfSource, SurfTarget)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN)    :: SurfSource
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: SurfTarget
    INTEGER :: ND, NS

    CALL SurfaceMeshStorage_Clear(SurfTarget)

    SurfTarget%NB_Point    = SurfSource%NB_Point
    SurfTarget%NB_Tri      = SurfSource%NB_Tri
    SurfTarget%NB_Quad     = SurfSource%NB_Quad
    SurfTarget%NB_Seg      = SurfSource%NB_Seg
    SurfTarget%NB_Surf     = SurfSource%NB_Surf
    SurfTarget%NB_Sheet    = SurfSource%NB_Sheet
    SurfTarget%NB_BD_Point = SurfSource%NB_BD_Point
    SurfTarget%GridOrder   = SurfSource%GridOrder
    SurfTarget%NB_Psp      = SurfSource%NB_Psp

    SurfTarget%Cell = HighOrderCell_getHighCell(SurfTarget%GridOrder,2,0)

    IF(ASSOCIATED(SurfSource%IP_Tri) .AND. SurfSource%NB_Tri>0)THEN
       ALLOCATE(SurfTarget%IP_Tri(5,SurfSource%NB_Tri))
       SurfTarget%IP_Tri(:,1:SurfSource%NB_Tri) = SurfSource%IP_Tri(:,1:SurfSource%NB_Tri)
    ENDIF
    IF(ASSOCIATED(SurfSource%IP_Quad) .AND. SurfSource%NB_Quad>0)THEN
       ALLOCATE(SurfTarget%IP_Quad(5,SurfSource%NB_Quad))
       SurfTarget%IP_Quad(:,1:SurfSource%NB_Quad) = SurfSource%IP_Quad(:,1:SurfSource%NB_Quad)
    ENDIF
    IF(ASSOCIATED(SurfSource%Next_Tri) .AND. SurfSource%NB_Tri>0)THEN
       ALLOCATE(SurfTarget%Next_Tri(3,SurfSource%NB_Tri))
       SurfTarget%Next_Tri(:,1:SurfSource%NB_Tri) = SurfSource%Next_Tri(:,1:SurfSource%NB_Tri)
    ENDIF
    NS = keyLength(SurfSource%Posit)
    IF(NS>0 .AND. SurfSource%NB_Point>0)THEN
       ALLOCATE(SurfTarget%Posit(3,NS))
       SurfTarget%Posit(:,1:NS) = SurfSource%Posit(:,1:NS)
    ENDIF
    NS = keyLength(SurfSource%Coord)
    IF(NS>0 .AND. SurfSource%NB_Point>0)THEN
       ALLOCATE(SurfTarget%Coord(2,NS))
       SurfTarget%Coord(:,1:NS) = SurfSource%Coord(:,1:NS)
    ENDIF
    IF(ASSOCIATED(SurfSource%IPsp_Tri) .AND. SurfSource%NB_Tri>0)THEN
       ND = HighOrderCell_numCellNodes(SurfSource%GridOrder, 2)
       ALLOCATE(SurfTarget%IPsp_Tri(ND,SurfSource%NB_Tri))
       SurfTarget%IPsp_Tri(1:ND,1:SurfSource%NB_Tri) = SurfSource%IPsp_Tri(1:ND,1:SurfSource%NB_Tri)
    ENDIF

    RETURN
  END SUBROUTINE SurfaceMeshStorage_BasicCopy

  !>
  !!    Fully copy a surface mesh to another.
  !<
  SUBROUTINE SurfaceMeshStorage_FullCopy(SurfSource, SurfTarget)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN)    :: SurfSource
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: SurfTarget
    INTEGER :: ND

    CALL SurfaceMeshStorage_BasicCopy(SurfSource, SurfTarget)

    SurfTarget%NB_Edge     = SurfSource%NB_Edge

    IF(ASSOCIATED(SurfSource%IP_Edge) .AND. SurfSource%NB_Edge>0)THEN
       ND = HighOrderCell_numCellNodes(SurfSource%GridOrder, 1)
       ALLOCATE(SurfTarget%IP_Edge(ND,SurfSource%NB_Edge))
       SurfTarget%IP_Edge(1:ND,1:SurfSource%NB_Edge) = SurfSource%IP_Edge(1:ND,1:SurfSource%NB_Edge)
    ENDIF
    IF(ASSOCIATED(SurfSource%ITR_Edge) .AND. SurfSource%NB_Edge>0)THEN
       ALLOCATE(SurfTarget%ITR_Edge(2,SurfSource%NB_Edge))
       SurfTarget%ITR_Edge(:,1:SurfSource%NB_Edge) = SurfSource%ITR_Edge(:,1:SurfSource%NB_Edge)
    ENDIF
    IF(ASSOCIATED(SurfSource%IED_Tri) .AND. SurfSource%NB_Tri>0)THEN
       ALLOCATE(SurfTarget%IED_Tri(3,SurfSource%NB_Tri))
       SurfTarget%IED_Tri(:,1:SurfSource%NB_Tri) = SurfSource%IED_Tri(:,1:SurfSource%NB_Tri)
    ENDIF
    CALL LinkAssociation_Copy(SurfSource%ITR_Pt, SurfTarget%ITR_Pt)
    CALL LinkAssociation_Copy(SurfSource%IED_Pt, SurfTarget%IED_Pt)
    CALL LinkAssociation_Copy(SurfSource%IP_Pt,  SurfTarget%IP_Pt)
    RETURN
  END SUBROUTINE SurfaceMeshStorage_FullCopy

  !>
  !!   Allocate (Reallocate) memory for the surface mesh.
  !<
  SUBROUTINE allc_Surf(Surf, NB_Tri, NB_Point, NB_Edge)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(IN) :: NB_Tri, NB_Point, NB_Edge
    INTEGER :: msize

    msize = NB_Point + Surf_allc_increase
    IF(NB_Point>KeyLength(Surf%Posit)) CALL allc_2Dpointer(Surf%Posit, 3, msize)
    IF(NB_Point>KeyLength(Surf%Coord)) CALL allc_2Dpointer(Surf%Coord, 2, msize)

    msize = NB_Tri + Surf_allc_increase
    IF(NB_Tri>KeyLength(Surf%IP_Tri))   CALL allc_2Dpointer(Surf%IP_Tri,   5, msize)
    IF(NB_Tri>KeyLength(Surf%NEXT_Tri)) CALL allc_2Dpointer(Surf%NEXT_Tri, 3, msize)
    IF(NB_Tri>KeyLength(Surf%IED_Tri))  CALL allc_2Dpointer(Surf%IED_Tri,  3, msize)

    msize = NB_Edge + Surf_allc_increase
    IF(NB_Edge>KeyLength(Surf%IP_Edge))  CALL allc_INT_2Dpointer(Surf%IP_Edge,  2, msize)
    IF(NB_Edge>KeyLength(Surf%ITR_Edge)) CALL allc_INT_2Dpointer(Surf%ITR_Edge, 2, msize)

    RETURN
  END SUBROUTINE allc_Surf


  !>
  !!   Build a point-association to triangles.
  !!   @param[out]   Surf  build %ITR_Pt (the link from a node to the adjacnet triangles).
  !<
  SUBROUTINE Surf_BuildTriAsso(Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER :: ie, i, ip

    CALL LinkAssociation_Allocate(Surf%NB_Point, Surf%ITR_Pt)
    DO ie = 1,Surf%NB_Tri
       DO i = 1,3
          ip = Surf%IP_Tri(i,ie)
          CALL IntQueue_Push36(Surf%ITR_Pt%JointLinks(ip), ie) 
       ENDDO
    ENDDO

    RETURN
  END SUBROUTINE Surf_BuildTriAsso

  !>
  !!   Build a point-association to Edge.
  !!   @param[out]   Surf  build %IED_Pt (the link from a node to the adjacnet edges). 
  !<
  SUBROUTINE Surf_BuildEdgeAsso(Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf

    CALL LinkAssociation_Build(2, Surf%NB_Edge, Surf%IP_Edge(1:2,1:Surf%NB_Edge),    &
         Surf%NB_Point, Surf%IED_Pt)

    RETURN
  END SUBROUTINE Surf_BuildEdgeAsso

  !>
  !!   Build a point-association to point.
  !!   @param[in,out]   Surf  build %IP_Pt (the link from a node to the adjacnet nodes).
  !<
  SUBROUTINE Surf_BuildPtAsso(Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER :: IT, ip(3)

    CALL LinkAssociation_Allocate(Surf%NB_Point, Surf%IP_Pt)
    DO IT = 1, Surf%NB_Tri
       ip(:) = Surf%IP_Tri(1:3,IT)
       IF( .NOT. IntQueue_Contain(Surf%IP_Pt%JointLinks(ip(1)), ip(2)) )   &
            CALL  IntQueue_Push36( Surf%IP_Pt%JointLinks(ip(1)), ip(2)) 
       IF( .NOT. IntQueue_Contain(Surf%IP_Pt%JointLinks(ip(1)), ip(3)) )   &
            CALL  IntQueue_Push36( Surf%IP_Pt%JointLinks(ip(1)), ip(3)) 
       IF( .NOT. IntQueue_Contain(Surf%IP_Pt%JointLinks(ip(2)), ip(1)) )   &
            CALL  IntQueue_Push36( Surf%IP_Pt%JointLinks(ip(2)), ip(1)) 
       IF( .NOT. IntQueue_Contain(Surf%IP_Pt%JointLinks(ip(2)), ip(3)) )   &
            CALL  IntQueue_Push36( Surf%IP_Pt%JointLinks(ip(2)), ip(3)) 
       IF( .NOT. IntQueue_Contain(Surf%IP_Pt%JointLinks(ip(3)), ip(1)) )   &
            CALL  IntQueue_Push36( Surf%IP_Pt%JointLinks(ip(3)), ip(1)) 
       IF( .NOT. IntQueue_Contain(Surf%IP_Pt%JointLinks(ip(3)), ip(2)) )   &
            CALL  IntQueue_Push36( Surf%IP_Pt%JointLinks(ip(3)), ip(2)) 
    ENDDO

    RETURN
  END SUBROUTINE Surf_BuildPtAsso


  !>
  !!    Build an edge system.
  !!   @param[out]   Surf%NB_Edge   : the number of edges.
  !!   @param[out]   Surf%IP_Edge   : the two nodes of each edge.
  !!   @param[out]   Surf%ITR_Edge  : the two triangles of each edge.
  !!   @param[out]   Surf%IED_Tri   : the three edges of each triangle.
  !!   @param[out]   EdgeTree       : the NodeNetTree of edges.
  !!
  !!   Reminder : The orientation of Surf%IP_Edge(:,is) is the same as that
  !!              of the triangle Surf%ITR_Edge(1,is)   \n
  !!              Those elements with 0 or negative connectivities are ignored.
  !<
  SUBROUTINE Surf_BuildEdge(Surf, EdgeTree)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    TYPE(NodeNetTreeType), INTENT(OUT), OPTIONAL :: EdgeTree

    INTEGER :: IEdge, ITri, IQuad, Mside, ND
    INTEGER :: i,ips(2)
    TYPE(NodeNetTreeType) :: Edge_Tree
    
    CALL Surf_ClearEdge(Surf)
    Mside = 3 * Surf%NB_Tri + 4 * Surf%NB_Quad
    ND    = HighOrderCell_numCellNodes(Surf%GridOrder, 1)
    ALLOCATE(Surf%IP_Edge(ND,Mside))
    Surf%IP_Edge(:,:) = 0
    ALLOCATE(Surf%ITR_Edge(2, Mside))
    CALL NodeNetTree_Allocate(2, Surf%NB_Point, Mside, Edge_Tree)

    IF(Surf%NB_Tri>0)  ALLOCATE(Surf%IED_Tri(3, Surf%NB_Tri))
    IF(Surf%NB_Quad>0) ALLOCATE(Surf%IED_Quad(4,Surf%NB_Quad))

    Surf%NB_Edge = 0
    DO ITri = 1,Surf%NB_Tri
       IF(MINVAL(Surf%IP_Tri(1:3,ITri))<=0) CYCLE
       DO i=1,3
          ips = Surf%IP_Tri(iEdge_Tri(:,i),ITri)
          CALL NodeNetTree_SearchAdd (2, ips, Edge_Tree, IEdge)

          IF(IEdge>Surf%NB_Edge)THEN
             !--- a new edge
             Surf%NB_Edge            = IEdge
             Surf%IP_Edge(1:2,IEdge) = ips    !--- with the orientation of iTri
             IF(Surf%GridOrder==3)THEN
                Surf%IP_Edge(3:4,IEdge) = Surf%IPsp_Tri(iEdge_C3Tri(3:4,i),ITri)
             ENDIF
             Surf%ITR_Edge(1,IEdge)  = ITri
             Surf%ITR_Edge(2,IEdge)  = 0
          ELSE
             Surf%ITR_Edge(2,IEdge)  = ITri
          ENDIF

          Surf%IED_Tri(i,ITri) = IEdge
       ENDDO
    ENDDO

    DO IQuad = 1,Surf%NB_Quad
       IF(MINVAL(Surf%IP_Quad(1:4,IQuad))<=0) CYCLE
       DO i=1,4
          ips = Surf%IP_Quad(iEdge_Quad(:,i),IQuad)
          CALL NodeNetTree_SearchAdd (2, ips, Edge_Tree, IEdge)

          IF(IEdge>Surf%NB_Edge)THEN
             !--- a new edge
             Surf%NB_Edge            = IEdge
             Surf%IP_Edge(1:2,IEdge) = ips    !--- with the orientation of iQuad
             Surf%ITR_Edge(1,IEdge)  = -IQuad
             Surf%ITR_Edge(2,IEdge)  = 0
          ELSE
             Surf%ITR_Edge(2,IEdge)  = -IQuad
          ENDIF

          Surf%IED_Quad(i,IQuad) = IEdge
       ENDDO
    ENDDO

    IF(present(EdgeTree))THEN
       CALL NodeNetTree_Copy(Edge_Tree, EdgeTree)
    ENDIF
    CALL NodeNetTree_Clear(Edge_Tree)
    
  END SUBROUTINE Surf_BuildEdge

  !>
  !!    Check the edge system.
  !!    @param[in] Surf : the mesh.
  !!    @param[in] nBound  the number of boundary edges.
  !<
  SUBROUTINE Surf_CheckEdge(Surf, nBound)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER, INTENT(OUT) :: nBound
    INTEGER :: Iedge, it1, it2, i1, i2, Isucc

    nBound = 0
    Isucc  = 1

    DO Iedge = 1, Surf%NB_Edge
       it1 = Surf%ITR_Edge(1,IEdge)
       IF(it1<=0 .OR. it1>Surf%NB_Tri)THEN
          Isucc = -1
          EXIT
       ENDIF

       DO i1=1,3
          IF(Surf%IED_Tri(i1,it1)==Iedge) EXIT
       ENDDO
       IF(i1>3)THEN
          Isucc = -2
          EXIT
       ENDIF

       IF(  Surf%IP_Tri(iEdge_Tri(1,i1),it1)/=Surf%IP_Edge(1,Iedge) .OR.    &
            Surf%IP_Tri(iEdge_Tri(2,i1),it1)/=Surf%IP_Edge(2,Iedge) ) THEN
          Isucc = -3
          EXIT
       ENDIF

       it2 = Surf%ITR_Edge(2,IEdge)
       IF(it2<=0 .OR. it2>Surf%NB_Tri)THEN
          nBound = nBound + 1
          CYCLE
       ENDIF

       DO i2=1,3
          IF(Surf%IED_Tri(i2,it2)==Iedge) EXIT
       ENDDO
       IF(i2>3)THEN
          Isucc = -4
          EXIT
       ENDIF

       IF(  Surf%IP_Tri(iEdge_Tri(1,i2),it2)/=Surf%IP_Edge(2,Iedge) .OR.    &
            Surf%IP_Tri(iEdge_Tri(2,i2),it2)/=Surf%IP_Edge(1,Iedge) ) THEN
          Isucc = -5
          EXIT
       ENDIF

       IF(Surf%IP_Tri(i1,it1) == Surf%IP_Tri(i2,it2)) THEN
          Isucc = -6
          EXIT
       ENDIF
    ENDDO

    IF(Isucc==1) RETURN

    IF(Isucc==-1)THEN
       WRITE(*,*)' Error--- Surf_CheckEdge::  first triangle is 0.'
    ELSE IF(Isucc==-2)THEN
       WRITE(*,*)' Error--- Surf_CheckEdge:: first triangle does not has this edge.'
    ELSE IF(Isucc==-3)THEN
       WRITE(*,*)' Error--- Surf_CheckEdge:: first triangle does not match this edge.'
    ELSE IF(Isucc==-4)THEN
       WRITE(*,*)' Error--- Surf_CheckEdge:: second triangle does not has this edge.'
    ELSE IF(Isucc==-5)THEN
       WRITE(*,*)' Error--- Surf_CheckEdge:: second triangle does not match this edge.'
    ELSE IF(Isucc==-6)THEN
       WRITE(*,*)' Error--- Surf_CheckEdge:: two identical triangles.'
    ENDIF
    WRITE(*,*)'Iedge=',Iedge,' ips=', Surf%IP_Edge(:,IEdge)
    WRITE(*,*)'  IT1=', Surf%ITR_Edge(1,IEdge), 'ips=',Surf%IP_Tri(1:3,Surf%ITR_Edge(1,IEdge))
    WRITE(*,*)'                   ied=',                Surf%IED_Tri(:, Surf%ITR_Edge(1,IEdge))
    WRITE(*,*)'  IT2=', Surf%ITR_Edge(2,IEdge), 'ips=',Surf%IP_Tri(1:3,Surf%ITR_Edge(2,IEdge))
    WRITE(*,*)'                   ied=',                Surf%IED_Tri(:, Surf%ITR_Edge(2,IEdge))
    CALL Error_STOP ( '--- Surf_CheckEdge')

  END SUBROUTINE Surf_CheckEdge

  !>
  !!    Delete an edge system.
  !<
  SUBROUTINE Surf_ClearEdge(Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    IF(ASSOCIATED(Surf%IP_Edge))  DEALLOCATE(Surf%IP_Edge)
    IF(ASSOCIATED(Surf%ITR_Edge)) DEALLOCATE(Surf%ITR_Edge)
    IF(ASSOCIATED(Surf%IED_Tri))  DEALLOCATE(Surf%IED_Tri)
    IF(ASSOCIATED(Surf%IED_Quad)) DEALLOCATE(Surf%IED_Quad)
    Surf%NB_Edge = 0
  END SUBROUTINE Surf_ClearEdge


  !>                                                                        
  !!    Build neighbouring system (i.e. Surf%Next_Tri).
  !!    The egde system has also been build here.                    
  !<
  SUBROUTINE Surf_BuildNext(Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER :: IEdge, IT, Mside, i,itnb

    IF(ASSOCIATED(Surf%Next_Tri))  DEALLOCATE(Surf%Next_Tri)
    Mside = SIZE(Surf%IP_Tri,2)
    ALLOCATE( Surf%Next_Tri(3,Mside) )

    CALL Surf_BuildEdge(Surf)

    DO IT = 1, Surf%NB_Tri
       DO i=1,3
          IEdge = Surf%IED_Tri(i,IT)
          itnb = Surf%ITR_Edge(1,IEdge) + Surf%ITR_Edge(2,IEdge) - IT
          IF(itnb>0)THEN
             Surf%Next_Tri(i,IT) = itnb
          ELSE
             Surf%Next_Tri(i,IT) = -1
          ENDIF
       ENDDO
    ENDDO

    RETURN
  END SUBROUTINE Surf_BuildNext

  !>                                                                  
  !!     Check Neighbouring system (i.e. Surf%Next_Tri)          
  !<
  SUBROUTINE Surf_CheckNext(Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER :: it,i,i1,i2,ii,itnb, ierr

    ierr = 0
    DO it = 1,Surf%NB_Tri
       DO i=1,3
          itnb = Surf%Next_Tri(i,it)
          IF(itnb<=0) CYCLE

          DO ii=1,3
             IF(Surf%Next_Tri(ii,itnb)==it) EXIT
          ENDDO
          IF(ii>3) ierr = 1

          i1 = MOD(i,   3)+1
          i2 = MOD(ii+1,3)+1
          IF(Surf%IP_Tri(i1,it)/=Surf%IP_Tri(i2,itnb)) ierr = ierr+2

          i1 = MOD(i+1,3)+1
          i2 = MOD(ii, 3)+1
          IF(Surf%IP_Tri(i1,it)/=Surf%IP_Tri(i2,itnb)) ierr = ierr+4

          IF(ierr/=0)THEN
             WRITE(*,*)'Error---Check_Next: ierr, i, ii=', ierr, i, ii
             WRITE(*,*)'it,  ip,next=',it,Surf%IP_Tri(1:3,it),Surf%Next_Tri(:,it)
             WRITE(*,*)'itnb,ip,next=',itnb,Surf%IP_Tri(1:3,itnb),Surf%Next_Tri(:,itnb)
             CALL Error_STOP ( '--- Surf_CheckNext')
          ENDIF
       ENDDO
    ENDDO

  END SUBROUTINE Surf_CheckNext


  !>                                                                        
  !!    Delete neighbouring system (i.e. Surf%Next_Tri)                         
  !<
  SUBROUTINE Surf_ClearNext(Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    IF(ASSOCIATED(Surf%Next_Tri))  DEALLOCATE(Surf%Next_Tri)
  END SUBROUTINE Surf_ClearNext

  !>
  !!  Input a surface mesh by reading a *.fro file. 
  !!  @param[in]  JobName        the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[out] Surf           the surface mesh.
  !<
  SUBROUTINE SurfaceMeshStorage_Input(JobName,JobNameLength,Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: i,k,nas, ipp(3), ic, j, ND
    LOGICAL :: ex
    TYPE (IntQueueType) :: List

    OPEN(1,file=JobName(1:JobNameLength)//'.fro', status='old')
    CALL IntQueue_Read(List, 1)
    IF(List%numNodes==8)THEN
       i = 0
       Surf%NB_Quad  = 0
    ELSE IF(List%numNodes==9)THEN
       i = 1
       Surf%NB_Quad  = List%Nodes(i)
    ELSE
       CALL Error_Stop('SurfaceMeshStorage_Input:: '//JobName(1:JobNameLength)//'.fro')
    ENDIF
    Surf%NB_Tri    = List%Nodes(i+1)
    Surf%NB_Point  = List%Nodes(i+2)
    Surf%GridOrder = List%Nodes(i+3)
    Surf%NB_Seg    = List%Nodes(i+5)
    Surf%NB_Surf   = List%Nodes(i+6)
    Surf%NB_Sheet  = List%Nodes(i+7)
    Surf%NB_Wire   = List%Nodes(i+8)
       
    CALL IntQueue_Clear(List) 
  
    IF(Surf%GridOrder==0) Surf%GridOrder=1
    Surf%Cell = HighOrderCell_getHighCell(Surf%GridOrder,2,0)
    
    CALL allc_2Dpointer(Surf%IP_Tri,  5, Surf%NB_Tri,   'SurfaceMeshStorage_Input')
    CALL allc_2Dpointer(Surf%IP_Quad, 5, Surf%NB_Quad,  'SurfaceMeshStorage_Input')
    CALL allc_2Dpointer(Surf%Posit,   3, Surf%NB_Point, 'SurfaceMeshStorage_Input')
    DO i = 1,Surf%NB_Point
       READ(1,*) k,Surf%Posit(1:3,k)
    ENDDO
    nas = 0
    DO i =1,Surf%NB_Quad
       READ(1,*) k,Surf%IP_Quad(1:5,k)
       nas = max(nas, Surf%IP_Quad(5,k))
    ENDDO
    DO i =1,Surf%NB_Tri
       READ(1,*) k,ipp(1:3),ic
       Surf%IP_Tri(1:3,k) = ipp(1:3)
       Surf%IP_Tri(4,k)   = 0
       Surf%IP_Tri(5,k)   = ic
       nas = max(nas, ic)
    ENDDO
    IF(Surf%NB_Surf<nas)THEN
       WRITE(*,*)' Warning---- SurfaceMeshStorage_Input:: the NB_Surf is reset by the range'
       Surf%NB_Surf = nas
    ENDIF
    DO i =1,Surf%NB_Tri
       IF(Surf%IP_Tri(5,i)==0)THEN
          Surf%NB_Surf = Surf%NB_Surf +1
          WHERE(Surf%IP_Tri(5,1:Surf%NB_Tri)==0) Surf%IP_Tri(5,1:Surf%NB_Tri) = Surf%NB_Surf
          EXIT
       ENDIF
    ENDDO

    !--- input the high-order edges
    IF(Surf%GridOrder>1) THEN
       !--- output the high-order edges
       ND = HighOrderCell_numCellNodes(Surf%GridOrder, 2)
       READ(1,*) Surf%NB_Psp
       ALLOCATE(Surf%IPsp_Tri(ND, Surf%NB_Tri))
       DO i =1,Surf%NB_Tri
          READ(1,*) k,Surf%IPsp_Tri(4:ND,k)
          Surf%IPsp_Tri(1:3,k) = Surf%IP_Tri(1:3,k)
       ENDDO
    ENDIF

    CLOSE(1)

    RETURN
  END SUBROUTINE SurfaceMeshStorage_Input

  !>
  !!  Input a surface mesh by reading a *.coor file and a *.dat file. 
  !!  @param[in]  JobName        the name (prefix) of input files.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[out] Surf           the surface mesh.
  !<
  SUBROUTINE SurfaceMeshStorage_Input2(JobName,JobNameLength,Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: i

    OPEN(1,file=JobName(1:JobNameLength)//'.coor', status='old')
    READ(1,*) Surf%NB_Point
    CALL allc_2Dpointer(Surf%Posit,  3, Surf%NB_Point, 'SurfaceMeshStorage_Input')
    DO i = 1,Surf%NB_Point
       READ(1,*) Surf%Posit(1:3,i)
    ENDDO
    CLOSE(1)
    OPEN(1,file=JobName(1:JobNameLength)//'.dat', status='old')
    READ(1,*) Surf%NB_Tri
    Surf%NB_Seg   = 0
    Surf%NB_Surf  = 1 
    Surf%NB_Sheet = 0
    CALL allc_2Dpointer(Surf%IP_Tri, 5, Surf%NB_Tri,   'SurfaceMeshStorage_Input')
    DO i =1,Surf%NB_Tri
       READ(1,*) Surf%IP_Tri(1:3,i)
       Surf%IP_Tri(4,i)   = 0
       Surf%IP_Tri(5,i)   = 1
    ENDDO
    CLOSE(1)
    RETURN
  END SUBROUTINE SurfaceMeshStorage_Input2


  !>
  !!  Input a surface mesh (connectivities only) by reading a *.fro file. 
  !!  @param[in]  JobName        the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[out] Surf           the surface mesh.
  !<
  SUBROUTINE SurfaceMeshStorage_Input_Topo(JobName,JobNameLength,Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: i, ipp(3), n, k, ic

    OPEN(1,file=JobName(1:JobNameLength)//'.fro', status='old')
    READ(1,*) Surf%NB_Tri
    CALL allc_2Dpointer(Surf%IP_Tri, 5, Surf%NB_Tri,   'SurfaceMeshStorage_Input')
    n = 0
    DO i =1,Surf%NB_Tri
       READ(1,*) k, ipp(1:3), ic
       Surf%IP_Tri(1:3,k) = ipp(1:3)
       Surf%IP_Tri(4,k)   = 0
       Surf%IP_Tri(5,k)   = ic
       n = MAX(n, ipp(1), ipp(2), ipp(3))
    ENDDO
    CLOSE(1)
    Surf%NB_Point = n
    CALL allc_2Dpointer(Surf%Posit, 3, Surf%NB_Point,   'SurfaceMeshStorage_Input')
    RETURN
  END SUBROUTINE SurfaceMeshStorage_Input_Topo

  !>
  !!  Input a surface mesh by reading a *.stl (ASCII STL format) file. 
  !!  @param[in]  JobName        the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[out] Surf           the surface mesh.
  !!  @param[in]  fmat = -1: binary .stl                            \n
  !!                   =  1: ASCII .stl
  !<
  SUBROUTINE SurfaceMeshStorage_Input_STL(JobName,JobNameLength,Surf, fmat)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength, fmat
    CHARACTER*(80) :: string
    INTEGER :: nb, np, ML, i, Niden, IP, JP, n, ib
    LOGICAL :: TF
    REAL*8  :: tolg
    REAL*8  :: p(3)
    REAL*8,  DIMENSION(:,:), POINTER :: Posit
    REAL*8,  DIMENSION(:),   POINTER :: Ps
    INTEGER, DIMENSION(:),   POINTER :: Markp

    IF(fmat==1)THEN
       nb = 0
       np = 0
       ML = 0

       OPEN(1,file=JobName(1:JobNameLength)//'.stl', status='old')
       READ(1,*)
       DO 
          READ(1,'(a)') string
          IF(string(1:8)=='endsolid') EXIT

          nb = nb + 1
          READ(1,*)
          DO i = 1,3
             READ(1,'(a)') string
             TF = CHAR_CutFront(String, 'vertex') 
             IF(.NOT. TF) CALL Error_Stop( ' SurfaceMeshStorage_Input_STL'//String )
             CALL CHAR_to_REAL8Array (String, p, N)
             IF(N/=3) CALL Error_Stop( ' SurfaceMeshStorage_Input_STL: N/=3' )
             np = np + 1
             IF(np>ML)THEN
                ML = 1.2*ML + 10000
                CALL allc_2Dpointer(Posit,  3, ML, 'SurfaceMeshStorage_Input_STL')
             ENDIF
             Posit(:,np) = p(:)
          ENDDO
          READ(1,*)
          READ(1,*)
       ENDDO
       CLOSE(1)

    ELSE IF(fmat==-1)THEN

       nb = 1
       ML = 9*nb
       CALL allc_1Dpointer(Ps,  ML, 'SurfaceMeshStorage_Input_STL')
       CALL binarystlreador(JobName(1:JobNameLength), Ps, nb)

       ML = 9*nb
       CALL allc_1Dpointer(Ps,  ML, 'SurfaceMeshStorage_Input_STL')
       CALL binarystlreador(JobName(1:JobNameLength), Ps, nb)
       ML = 3*nb
       CALL allc_2Dpointer(Posit,  3, ML, 'SurfaceMeshStorage_Input_STL')
       np = 0
       ip = 0
       DO ib = 1, nb
          DO i = 1,3
             np = np+1
             Posit(:,np) = Ps(ip+1:ip+3)
             ip = ip + 3
          ENDDO
       ENDDO

    ENDIF

    CALL allc_1Dpointer(Markp,          NP, 'SurfaceMeshStorage_Input_STL')
    CALL allc_2Dpointer(Surf%Posit,  3, NP, 'SurfaceMeshStorage_Input_STL')

    tolg = 0.d0
    CALL PtADTree_IdentifyNodes(NP, Posit, tolg, Niden, Markp)

    n = 0
    DO IP = 1, NP
       JP = Markp(IP)
       IF(JP==IP)THEN
          n = n+1
          Surf%Posit(:,n) = Posit(:,IP)
          Markp(IP) = n
       ELSE
          IF(JP<=0 .OR. JP>IP) CALL Error_Stop( ' SurfaceMeshStorage_Input_STL: Markp 1' )
          Markp(IP) = Markp(JP)
       ENDIF
    ENDDO

    Surf%NB_Point = n
    Surf%NB_Tri   = NB

    CALL allc_2Dpointer(Surf%IP_Tri, 5, Surf%NB_Tri,   'SurfaceMeshStorage_Input')

    DO ib = 1, Surf%NB_Tri
       DO i = 1,3
          ip = (ib-1)*3 + i
          jp = Markp(ip)
          IF(JP<=0 .OR. JP>n) CALL Error_Stop( ' SurfaceMeshStorage_Input_STL: Markp 2' )
          Surf%IP_Tri(i, ib) = jp
       ENDDO
       Surf%IP_Tri(4:5, ib) = (/0,1/)
    ENDDO

    Surf%NB_Seg   = 0
    Surf%NB_Surf  = 1
    Surf%NB_Sheet = 0     

    RETURN
  END SUBROUTINE SurfaceMeshStorage_Input_STL

  !>
  !!  Output a surface mesh by writting a *.fro file. 
  !!  @param[in]  JobName        the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  Surf           the surface mesh.
  !<
  SUBROUTINE SurfaceMeshStorage_Output(JobName,JobNameLength,Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: i,j,nas, ND

    OPEN(1,file=JobName(1:JobNameLength)//'.fro')
    nas = 0
    IF(Surf%NB_Quad==0)THEN
       WRITE(1,'(2I9,6(1X,I4))') Surf%NB_Tri, Surf%NB_Point, Surf%GridOrder, nas,   &
            Surf%NB_Seg, Surf%NB_Surf, Surf%NB_Sheet, Surf%NB_Wire
    ELSE
       WRITE(1,'(3I9,6(1X,I4))') Surf%NB_Quad, Surf%NB_Tri, Surf%NB_Point, Surf%GridOrder, nas,   &
            Surf%NB_Seg, Surf%NB_Surf, Surf%NB_Sheet, Surf%NB_Wire
    ENDIF

    DO i = 1,Surf%NB_Point
       WRITE(1,'(I9,3(1x,E19.12))') i,Surf%Posit(1:3,i)
    ENDDO

    DO i =1,Surf%NB_Quad
       WRITE(1,'(6(1x,I9))') i,Surf%IP_Quad(1:5,i)
    ENDDO

    DO i =1,Surf%NB_Tri
       WRITE(1,'(5(1x,I9))') i,Surf%IP_Tri(1:3,i),Surf%IP_Tri(5,i)
    ENDDO

    IF(Surf%GridOrder>1) THEN
       !--- output the high-order edges
       ND = HighOrderCell_numCellNodes(Surf%GridOrder, 2)
       WRITE(1,'(3(1X,I9))') Surf%NB_Psp
       DO i =1,Surf%NB_Tri
          WRITE(1,'(8(1x,I9))') i, Surf%IPsp_Tri(4:ND,i)
       ENDDO
    ENDIF

    CLOSE(1)

    RETURN
  END SUBROUTINE SurfaceMeshStorage_Output

  !>
  !!  Output a surface mesh by writting a *.stl (ASCII STL format) file. 
  !!  @param[in]  JobName        the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  Surf           the surface mesh.
  !<
  SUBROUTINE SurfaceMeshStorage_Output_STL(JobName,JobNameLength,Surf,anor)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    REAL*8, INTENT(IN), OPTIONAL :: anor(3,*)
    INTEGER :: IT
    REAL*8  :: p1(3), p2(3), p3(3), a(3), aa

    OPEN(1,file=JobName(1:JobNameLength)//'.stl')
    WRITE(1,'(a)') 'solid tri'
    DO IT =1,Surf%NB_Tri
       p1(:) = Surf%Posit(:,Surf%IP_Tri(1,IT))
       p2(:) = Surf%Posit(:,Surf%IP_Tri(2,IT))
       p3(:) = Surf%Posit(:,Surf%IP_Tri(3,IT))
       IF(present(anor))THEN
          a(:) = anor(:,IT)
       ELSE
          a(:)  = Geo3D_Cross_Product(P2,P1,P3)
          aa    = dsqrt( a(1)*a(1) + a(2)*a(2) + a(3)*a(3) )
          a(:)  = a(:) / aa
       ENDIF
       WRITE(1,'(a,3(1x,g20.10))') '   facet normal', a(:)
       WRITE(1,'(a)')              '      outer loop'
       WRITE(1,'(a,3(1x,g20.10))') '         vertex', p1(:)
       WRITE(1,'(a,3(1x,g20.10))') '         vertex', p2(:)
       WRITE(1,'(a,3(1x,g20.10))') '         vertex', p3(:)
       WRITE(1,'(a)')              '      endloop'
       WRITE(1,'(a)')              '   endfacet'
    ENDDO
    WRITE(1,'(a)') 'endsolid tri'

    CLOSE(1)
    RETURN
  END SUBROUTINE SurfaceMeshStorage_Output_STL

  !>
  !!  Output a 2D mesh by writting a *.msh file. 
  !!  @param[in]  JobName        the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  Surf           the surface mesh.
  !<
  SUBROUTINE SurfaceMeshStorage_Output2D(JobName,JobNameLength,Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: i

    OPEN(1,file=JobName(1:JobNameLength)//'.msh')
    WRITE(1,*)1
    WRITE(1,'(a)')' 2D mesh for '//JobName(1:JobNameLength)
    WRITE(1,'(a)') 'Surf%NB_Tri, Surf%NB_Point, Surf%NB_BD_Point'
    WRITE(1,'(6(1X,I8))') Surf%NB_Tri, Surf%NB_Point, Surf%NB_BD_Point
    WRITE(1,'(a)')'  connectivities '
    DO i =1,Surf%NB_Tri
       WRITE(1,'(5(1x,I8))') i,Surf%IP_Tri(1:3,i)
    ENDDO
    WRITE(1,'(a)')'  coordinates '
    DO i = 1,Surf%NB_Point
       WRITE(1,'(I9,3(1x,E19.12))') i,Surf%Coord(1:2,i)
    ENDDO
    CLOSE(1)    
    RETURN
  END SUBROUTINE SurfaceMeshStorage_Output2D

  !>
  !!  Write a .geo file for EnSight.
  !!  @param[in]  JobName        the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  Surf           the surface mesh.
  !<
  SUBROUTINE SurfaceMeshStorage_OutputEnSight(JobName,JobNameLength,Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: i, j, k, ip, is, nbn, nbp, jp, nps, js, k1, k2, kd
    CHARACTER*80 :: string
    CHARACTER*4  :: fileExtension 
    INTEGER, DIMENSION(:),   ALLOCATABLE :: gloloc
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: ips
    INTEGER, DIMENSION(:),   ALLOCATABLE :: njp, SegStart
    REAL*4,  DIMENSION(:,:), ALLOCATABLE :: xyz
    REAL*8,  DIMENSION(:,:), ALLOCATABLE :: Posit
    REAL*8 :: u, Ws(4,100)

    OPEN(1,file=JobName(1:JobNameLength)//'.geo',  &
         status='UNKNOWN',form='UNFORMATTED',err=111)

    string = 'Fortran Binary' 
    WRITE(1,err=111) string
    string = 'ENSIGHT'
    WRITE(1,err=111) string
    string = 'GEO FILE'
    WRITE(1,err=111) string
    string = 'node id off'
    WRITE(1,err=111) string
    string = 'element id off'
    WRITE(1,err=111) string

    IF(Surf%GridOrder==3)THEN
       CALL Surf_BuildEdge(Surf)
       nps = 10
          DO j = 1, nps-1
             u = 1.0*j/nps
             CALL CubicSegment_ShapeFunc (u, Ws(:,j)) 
          ENDDO
       ALLOCATE (xyz(3,Surf%NB_Point+nps*Surf%NB_Edge))
       ALLOCATE (gloloc(Surf%NB_Point+nps*Surf%NB_Edge),ips(3*nps,Surf%NB_Tri+Surf%NB_Quad))
       ALLOCATE (Posit(3, nps*Surf%NB_Edge))
       ALLOCATE (njp(Surf%NB_Tri))
       ALLOCATE (SegStart(Surf%NB_Edge))
       nbp = 0
       DO i = 1, Surf%NB_Edge
          SegStart(i) = 0
          IF(Surf%IP_Edge(3,i)==0) CYCLE
          SegStart(i) = nbp+1
          DO j = 1, nps-1
             nbp = nbp+1
             posit(:,nbp) =  Ws(1,j)* Surf%Posit(:,Surf%IP_Edge(1,i))    &
                           + Ws(2,j)* Surf%Posit(:,Surf%IP_Edge(3,i))    &
                           + Ws(3,j)* Surf%Posit(:,Surf%IP_Edge(4,i))    &
                           + Ws(4,j)* Surf%Posit(:,Surf%IP_Edge(2,i))
          ENDDO
       ENDDO
    ELSE  
       ALLOCATE (xyz(3,Surf%NB_Point))
       ALLOCATE (gloloc(Surf%NB_Point),ips(4,Surf%NB_Tri+Surf%NB_Quad))
    ENDIF

    DO is = 1,Surf%NB_Surf
       gloloc(:) = 0
       nbp = 0
       nbn = 0
       DO i = 1,Surf%NB_Tri
          IF(Surf%IP_Tri(5,i)/=is) CYCLE
          nbn = nbn+1
          DO j=1,3
             ip = Surf%IP_Tri(j,i)
             IF(gloloc(ip) == 0)THEN
                nbp = nbp + 1
                gloloc(ip) = nbp
                xyz(:,nbp) = Surf%Posit(:,ip)
             ENDIF
          ENDDO

          IF(Surf%GridOrder==3)THEN

             jp = 0
             DO j=1,3
                ip = Surf%IP_Tri(IEdge_Tri(1,j),i)
                jp = jp+1
                ips(jp,nbn) = gloloc(ip)

                js = Surf%IED_Tri(j,i)
                IF( SegStart(js)==0) CYCLE
                IF(ip==Surf%IP_Edge(1,js))THEN
                   k1 = SegStart(js)
                   k2 = SegStart(js)+nps-2
                   kd = 1
                ELSE
                   k1 = SegStart(js)+nps-2
                   k2 = SegStart(js)
                   kd = -1
                ENDIF
                DO ip = k1,k2,kd
                   IF(gloloc(ip+Surf%NB_Point) == 0)THEN
                      nbp = nbp + 1
                      gloloc(ip+Surf%NB_Point) = nbp
                      xyz(:,nbp) = Posit(:,ip)
                   ENDIF
                   jp = jp+1
                   ips(jp,nbn) = gloloc(ip+Surf%NB_Point)
                ENDDO
             ENDDO
             njp(nbn) = jp

          ELSE
             ips(1:3,nbn) = gloloc(Surf%IP_Tri(1:3,i))
          ENDIF

       ENDDO
       IF(nbn==0) CYCLE

       string = 'part'
       WRITE(1,err=111) string
       WRITE(1,err=111) is
       WRITE(fileExtension,'(i4)') is
       string = 'Tri Surface Mesh '//fileExtension
       WRITE(1,err=111) string
       string = 'coordinates'
       WRITE(1,err=111) string
       WRITE(1,err=111) nbp
       WRITE(1,err=111) xyz(1,1:nbp)
       WRITE(1,err=111) xyz(2,1:nbp)
       WRITE(1,err=111) xyz(3,1:nbp)

       IF(Surf%GridOrder==3)THEN
          string = 'nsided'
          WRITE(1,err=111) string
          WRITE(1,err=111) nbn
          WRITE(1,err=111) (njp(i), i=1,nbn)
          WRITE(1,err=111) ((ips(j,i),j=1,njp(i)), i=1,nbn)
       ELSE
          string = 'tria3'
          WRITE(1,err=111) string
          WRITE(1,err=111) nbn 
          WRITE(1,err=111) ((ips(j,i),j=1,3), i=1,nbn)
       ENDIF
    ENDDO

    IF(Surf%NB_Quad>0)THEN
       gloloc(1:Surf%NB_Point) = 0
       nbp = 0
       DO i = 1,Surf%NB_Quad
          DO j=1,4
             ip = Surf%IP_Quad(j,i)
             IF(gloloc(ip) == 0)THEN
                nbp = nbp + 1
                gloloc(ip) = nbp
                xyz(:,nbp) = Surf%Posit(:,ip)
             ENDIF
             ips(j,i) = gloloc(ip)
          ENDDO
       ENDDO

       string = 'part'
       WRITE(1,err=111) string
       WRITE(1,err=111) Surf%NB_Surf+2
       string = 'Quad Surface Mesh'
       WRITE(1,err=111) string
       string = 'coordinates'
       WRITE(1,err=111) string
       WRITE(1,err=111) nbp
       WRITE(1,err=111) xyz(1,1:nbp)
       WRITE(1,err=111) xyz(2,1:nbp)
       WRITE(1,err=111) xyz(3,1:nbp)

       string = 'quad4'
       WRITE(1,err=111) string
       WRITE(1,err=111) Surf%NB_Quad 
       WRITE(1,err=111) ((ips(j,i),j=1,4), i=1,Surf%NB_Quad)
    ENDIF

    DEALLOCATE (gloloc, ips)
    DEALLOCATE (xyz)
    IF(Surf%GridOrder==3) DEALLOCATE (Posit, njp, SegStart)
    CLOSE(1)
    RETURN
111 WRITE(*,*) 'Error--- SurfaceMeshStorage_OutputEnSight: ',      &
         JobName(1:JobNameLength)//'.geo'
    CALL Error_STOP (' ')

  END SUBROUTINE SurfaceMeshStorage_OutputEnSight

  !>
  !!     Search a triangle in which a 2D point lies.                                       
  !!     @param[in]  Pnew (2)    the coordinates of the point.           
  !!     @param[in]     IT       the initial value of search (do not have to set).
  !!     @param[in]     tol      the tolerance.
  !!     @param[out]    IT       the number of a triange which cover the point.
  !!     @param[out]    Ib   =0, the point is inside of the triangle.   \n
  !!                         =1,2, or 3, the point is on the edge of the triangle  
  !!                                    opposite the vertex Surf%IP_Tri(Ib,IT).
  !<                                                                       
  SUBROUTINE Surf2D_SearchTri(Surf, Pnew,IT,Ib, tol)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf

    INTEGER, INTENT(INOUT) :: IT
    INTEGER, INTENT(OUT) :: Ib
    REAL*8,  INTENT(IN)  :: Pnew(2), tol
    INTEGER :: i, i1, i2, K_Succ
    REAL*8  :: pp3(2,3), cro

    IF(IT<1 .OR. IT>Surf%NB_Tri) IT=1
    Ib=0

    LOOP_1 : DO
       pp3(:,1) = Surf%Coord(:,Surf%IP_Tri(1,IT))
       pp3(:,2) = Surf%Coord(:,Surf%IP_Tri(2,IT))
       pp3(:,3) = Surf%Coord(:,Surf%IP_Tri(3,IT))
       K_Succ=1
       DO i=1,3
          i1=MOD(i,3)+1
          i2=MOD(i1,3)+1
          cro=Cross_Product_2D(pp3(:,i1),pp3(:,i2),Pnew(:))
          IF(ABS(cro)<tol)THEN
             Ib=i
          ELSE IF(cro>0 .AND. Surf%Next_Tri(i,IT)>0)THEN    !---beyond tri.
             IT=Surf%Next_Tri(i,IT)
             Ib=0
             EXIT
          ELSE IF(cro>0 .AND. Surf%Next_Tri(i,IT)<=0)THEN
             K_Succ=0
          ENDIF
          IF(i==3 .AND. K_Succ==1) RETURN
          IF(i==3 .AND. K_Succ==0) EXIT LOOP_1
       ENDDO
    ENDDO LOOP_1

    !---- fail to find a triangle by a conjunctive queue (NEXT_Tri queue breaks)
    !     So search IT one by one.

    LOOP_2 : DO IT=1,Surf%NB_Tri

       Ib=0
       pp3(:,1) = Surf%Coord(:,Surf%IP_Tri(1,IT))
       pp3(:,2) = Surf%Coord(:,Surf%IP_Tri(2,IT))
       pp3(:,3) = Surf%Coord(:,Surf%IP_Tri(3,IT))
       IF(MIN(pp3(1,1),pp3(1,2),pp3(1,3))>Pnew(1)) CYCLE
       IF(MAX(pp3(1,1),pp3(1,2),pp3(1,3))<Pnew(1)) CYCLE
       IF(MIN(pp3(2,1),pp3(2,2),pp3(2,3))>Pnew(2)) CYCLE
       IF(MAX(pp3(2,1),pp3(2,2),pp3(2,3))<Pnew(2)) CYCLE

       DO i=1,3
          i1=MOD(i,3)+1
          i2=MOD(i1,3)+1
          cro=Cross_Product_2D(pp3(:,i1),pp3(:,i2),Pnew(:))
          IF(ABS(cro).LT.tol)THEN
             Ib=i
          ELSE IF(cro.GT.0)THEN    
             CYCLE LOOP_2
          ENDIF
       ENDDO

       RETURN     !--- find a triangle

    ENDDO LOOP_2

    !--- fail to find a triangle
    IT=0

    RETURN
  END SUBROUTINE Surf2D_SearchTri


END MODULE SurfaceMeshStorage

