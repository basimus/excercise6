!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!-- MODULE Geometry3D
!--   FUNCTION Geo3D_InsideBox(Pt(3), pMin(3), pMax(3)) RESULT (TF)
!--   FUNCTION Geo3D_InsideBox2(Pt(3), Box(6), toler) RESULT (TF)
!--   FUNCTION Geo3D_InsideBox3(Box1(6), Box2(6), toler) RESULT (TF)
!--   FUNCTION Geo3D_Distance_SQ(P1(3),<optional>P2(3)) RESULT(Dis_SQ)
!--   FUNCTION Geo3D_Distance(P1(3),<optional>P2(3)) RESULT(Dist)
!--   FUNCTION Geo3D_Tet_Volume6(P1(3),P2(3),P3(3),P4(3)) RESULT(vol6)
!--   FUNCTION Geo3D_Tet_Volume6_High(P1(3),P2(3),P3(3),P4(3)) RESULT(vol6)
!--   FUNCTION Geo3D_Tet_Volume6_Pred(P1(3),P2(3),P3(3),P4(3)) RESULT(vol6)
!--   FUNCTION Geo3D_Tet_Volume(P1(3),P2(3),P3(3),P4(3)) RESULT(vol)
!--   FUNCTION Geo3D_Tet_Volume_High(P1(3),P2(3),P3(3),P4(3)) RESULT(vol)
!--   FUNCTION Geo3D_Tet_Volume_Pred(P1(3),P2(3),P3(3),P4(3)) RESULT(vol)
!--   FUNCTION Geo3D_Tet_Weight(K,pp4(3,4),pp(3)) RESULT(weight(4))
!--   FUNCTION Geo3D_Tet_Weight_High(K,pp4(3,4),pp(3)) RESULT(weight(4))
!--   FUNCTION Geo3D_Triangle_Weight(K,pp3(3,3),pp(3)) RESULT(weight(3))
!--   SUBROUTINE Geo3D_Triangle_Axes(pp3(3,3),Axes(3,3),woo(3))
!--   FUNCTION Geo3D_Triangle_Axes_Weight(Axes(3,3),woo(3),pp(3)) RESULT(weight(3))
!--   FUNCTION Geo3D_Triangle_Area(P1(3),P2(3),P3(3)) RESULT(area)
!--   FUNCTION Geo3D_Triangle_Quality(P1(3),P2(3),P3(3)) RESULT(qual)
!--   FUNCTION Geo3D_Cross_Product(P1(3),P2(3),<optional>P3(3)) RESULT(prod(3))
!--   FUNCTION Geo3D_Dot_Product(P1(3),P2(3),<optional>P3(3)) RESULT(prod)
!--   FUNCTION Geo3D_Vector_Product(V1(:),V2(:)) RESULT(fMatrix(:,:))
!--   FUNCTION Geo3D_Included_Angle(P1(3),P2(3),<optional>P3(3)) RESULT(Angle)
!--   FUNCTION Geo3D_Dihedral_Angle(p1(3), p2(3), p3(3), p4(3), Isucc) RESULT(Angle)
!--   FUNCTION Geo3D_Sphere_Centre_old(P1(3),P2(3),P3(3),P4(3)) RESULT(PO(3))
!--   FUNCTION Geo3D_Sphere_Centre(P1(3),P2(3),P3(3),P4(3)) RESULT(PO(3))
!--   FUNCTION Geo3D_Sphere_Cross(P1(3),P2(3),P3(3),P4(3),RR1,RR2,RR3,RR4) RESULT(PO)
!--   FUNCTION Geo3D_Tet_Quality(P1(3),P2(3),P3(3),P4(3),Kqual) RESULT(Qual)
!--   FUNCTION Geo3D_Matrix_Transpose(fMatrix(3,3)) RESULT(transp(3,3))
!--   FUNCTION Geo3D_Matrix_Determinant(fMatrix(3,3)) RESULT(det)
!--   FUNCTION Geo3D_Matrix_Product(fMatrix1(3,3),fMatrix2(3,3)) RESULT(prod(3,3))
!--   FUNCTION Geo3D_Matrix_Inverse(fMatrix(3,3)) RESULT(rev(3,3))
!--   SUBROUTINE Geo3D_Matrix_Solver_old(fMatrix(3,3), R(3), X(3), det)
!--   SUBROUTINE Geo3D_Matrix_Solver(fMatrix(3,3), R(3), X(3), det)
!--   SUBROUTINE Geo3D_Cubic_Equation(a2,a1,a0,x(3),nroot)
!--   SUBROUTINE Geo3D_Matrix_Eigen(fMatrix(3,3), values(3), vectors(3,3))
!--   SUBROUTINE Geo3D_Linear_Interpolate(P1(3),P2(3),P0(3),ij)
!--   SUBROUTINE Geo3D_BuildOrthogonalAxes(v1(3),v2(3),v3(3))
!--   FUNCTION Geo3D_ProjectOnLine(P1(3),P2(3),P0(3),t) RESULT(P(3))
!--   SUBROUTINE Geo3D_LineFit_2D(N, x(N),y(N),a,b)
!--   SUBROUTINE Geo3D_LineFit(N,Pt(3,N),VT(3),P0(3))

!--
!-- MODULE ScalarCalculator
!--   FUNCTION Scalar_Interpolate(Scalar1,Scalar2,t,Model) RESULT(Scalar)
!--   FUNCTION Scalar_Mean(n,Scalars(*),Model) RESULT(Scalar)
!--   FUNCTION Scalar_Mean_Weight(n,Scalars(*),w(*),Model) RESULT(Scalar)
!--   FUNCTION Scalar_Integral(Scalar1,Scalar2,Model) RESULT(Scalar)
!--
!-- MODULE Metric3D
!--   FUNCTION Metric3D_from_Stretch(Stretch(6)) RESULT(fMtr(6))
!--   FUNCTION Metric3D_to_Stretch(fMtr(6)) RESULT(Stretch(6))
!--   FUNCTION Metric3D_Distance_SQ(P1(3),P2(3),fMtr(6)) RESULT(Dist_SQ)
!--   FUNCTION Metric3D_Distance(P1(3),P2(3),fMtr(6)) RESULT(Dist)
!--   FUNCTION Metric3D_Dot_Product(V1(3),V2(3),fMtr(6)) RESULT(prod)
!--   FUNCTION Metric3D_Product(fMtr1(6),fMtr2(6)) RESULT(prod(3,3))
!--   FUNCTION Metric3D_Inverse(fMtr(6)) RESULT(rev(6))
!--   SUBROUTINE Metric3D_Eigen(fMtr(6), values(3), vectors(3,3))
!--   SUBROUTINE Metric3D_EigenValue(fMtr(6), values(3))
!--   SUBROUTINE Metric3D_Eigen_Iter(fMtr(6), values(3), vectors(3,3))
!--   FUNCTION Metric3D_Interpolate(fMtr1(6),fMtr2(6),t,Model) RESULT(fMtr(6))
!--   FUNCTION Metric3D_Mean(n,fMtrs(6,*),Model) RESULT(fMtr(6))
!--   FUNCTION Metric3D_Intersect(fMtr1(6),fMtr2(6)) RESULT(fMtr(6))
!--   FUNCTION Metric3D_ScalarIntersect(fMtr1(6),Scalar) RESULT(fMtr(6))
!--
!-- MODULE Mapping3D
!--   FUNCTION Mapping3D_Error(fMap) RESULT(E)
!--   SUBROUTINE Mapping3D_Compose(A(3),D(3,3),fMap(3,3))
!--   SUBROUTINE Mapping3D_Decompose(fMap(3,3),A(3),D(3,3))
!--   FUNCTION Mapping3D_from_Stretch(Stretch(6)) RESULT(fMap(3,3))
!--   FUNCTION Mapping3D_to_Stretch(fMap(3,3)) RESULT(Stretch(6))
!--   FUNCTION Mapping3D_from_Metric(fMtr(6)) RESULT(fMap(3,3))
!--   FUNCTION Mapping3D_to_Metric(fMap(3,3)) RESULT(fMtr(6))
!--   FUNCTION Mapping3D_getSpacing(fMap(3,3)) RESULT(Scalars(3))
!--   FUNCTION Mapping3D_Jacobi(fMap) RESULT(Jaco)
!--   FUNCTION Mapping3D_Distance_SQ(P1(3),P2(3),fMap(3,3)) RESULT(Dist_SQ)
!--   FUNCTION Mapping3D_Distance(P1(3),P2(3),fMap(3,3)) RESULT(Dist)
!--   FUNCTION Mapping3D_Posit_Transf(P(3), fMap(3,3), idirect) RESULT(Pt(3))
!--   FUNCTION Mapping3D_Cross_Product(P1(3),P2(3),P3(3),fMap(3,3)) RESULT(prod(3))
!--   FUNCTION Mapping3D_Dot_Product(P1(3),P2(3),P3(3),fMap(3,3)) RESULT(prod)
!--   FUNCTION Mapping3D_Included_Angle(P1(3),P2(3),P3(3),fMap(3,3)) RESULT(Angle)
!--   FUNCTION Mapping3D_Tet_Volume(P1(3),P2(3),P3(3),P4(3),fMap(3,3)) RESULT(vol)
!--   FUNCTION Mapping3D_Tet_Volume_Pred(P1(3),P2(3),P3(3),P4(3),fMap(3,3)) RESULT(vol)
!--   FUNCTION Mapping3D_Inverse(fMap(3,3)) RESULT(rev(3,3))
!--   SUBROUTINE Mapping3D_Min_Enlarger(fMap(3,3),Scalar)
!--   SUBROUTINE Mapping3D_Max_Enlarger(fMap(3,3),Scalar)
!--   SUBROUTINE Mapping3D_Min_Shrinker(fMap(3,3),Scalar)
!--   SUBROUTINE Mapping3D_Max_Shrinker(fMap(3,3),Scalar)
!--   SUBROUTINE Mapping3D_ScalarIntersect(fMap(3,3),Scalar)
!--   FUNCTION Mapping3D_Distinct(fMap1(3,3),fMap2(3,3)) RESULT(Distinct)
!--   FUNCTION Mapping3D_Interpolate(fMap1(3,3),fMap2(3,3),t,Model) RESULT(fMap(3,3))
!--   FUNCTION Mapping3D_Mean(n,fmaps(3,3,*),Model) RESULT(fMap(3,3))
!--   FUNCTION Mapping3D_Mean_Weight(n,fmaps(3,3,*),w(*),Model) RESULT(fMap(3,3))
!--   FUNCTION Mapping3D_Intersect(fMap1(3,3),fMap2(3,3)) RESULT(fMap(3,3))
!--   SUBROUTINE Mapping3D_Display(fMap(3,3),Npoint,filename,nameLength)
!--
!-- MODULE Plane3DGeom
!--   FUNCTION Plane3D_Build(P1(3),P2(3),P3(3)) RESULT(aPlane)
!--   FUNCTION Plane3D_Build2(P0(3),anor(3)) RESULT(aPlane)
!--   SUBROUTINE Plane3D_buildTangentAxes(aPlane)
!--   FUNCTION Plane3D_project2D(aPlane,P0(3)) RESULT(P2D(2))
!--   FUNCTION Plane3D_projectMapping(aPlane,fMap(3,3)) RESULT(fMtr2D(3))
!--   FUNCTION Plane3D_projectMetric(aPlane,fMtr(6)) RESULT(fMtr2D(3))
!--   FUNCTION Plane3D_get3DPosition(aPlane,P2D(2)) RESULT(PP(3))
!--   FUNCTION Plane3D_ProjectOnPlane(P0(3),aPlane) RESULT(PP(3))
!--   FUNCTION Plane3D_ProjectOnPlane3(P0(3),P1(3),P2(3),P3(3)) RESULT(PP(3))
!--   FUNCTION Plane3D_ProjectOnPlane3w(P0(3),P1(3),P2(3),P3(3)) RESULT(Weight(3))
!--   FUNCTION Plane3D_DistancePointToPlane(P0(3),aPlane) RESULT(Dist)
!--   FUNCTION Plane3D_SignedDistancePointToPlane(P0(3),aPlane) RESULT(Dist)
!--   FUNCTION Plane3D_DistancePointToTriangle(P0(3),P1(3),P2(3),P3(3)) RESULT(Dist)
!--   FUNCTION Plane3D_Line_Plane_Cross(P1(3),P2(3),aPlane,t) RESULT(PP(3))
!--   FUNCTION Plane3D_Line_Tri_Cross(pp2(3,2),pp3(3,2),ntype,weight(3),t) RESULT(PP(3))
!--   FUNCTION Plane3D_Vector_Plane_Angle(aPlane, P1(3),P2(3)) RESULT(Angle)
!--   FUNCTION Plane3D_Sphere_Sphere_Chord(SP1(4),SP2(4)) RESULT(aPlane)
!--
!--MODULE MatrixAlgebra
!--   SUBROUTINE Matrix_LU_Decomposition(n, A(n,n), indexLU(n))
!--   SUBROUTINE Matrix_LU_Solver(n,A(n,n),indexLU(n),b(n))
!--
!-- MODULE Geometry3DAll
!--   all of above

!>
!!
!!  Geometry in 3D space and algebra for 3*3 matrices
!!
!<
MODULE Geometry3D


CONTAINS


  !>
  !!     Check if a point locating in a cuboid domain.
  !!     @param[in] Pt   coordinates of the point.
  !!     @param[in] pMin,pMax   The position of the minimum/maximum corners.
  !!     @return    = .true.   the point is inside the box (including boundary).   \n
  !!                = .false.  the point is not inside the box.
  !<
  FUNCTION Geo3D_InsideBox(Pt, pMin, pMax) RESULT (TF)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: Pt(3), pMin(3), pMax(3)
    LOGICAL :: TF
    TF = .FALSE.
    IF(pMin(1) > Pt(1)) RETURN
    IF(pMin(2) > Pt(2)) RETURN
    IF(pMin(3) > Pt(3)) RETURN
    IF(pMax(1) < Pt(1)) RETURN
    IF(pMax(2) < Pt(2)) RETURN
    IF(pMax(3) < Pt(3)) RETURN
    TF = .TRUE.
  END FUNCTION Geo3D_InsideBox

  !>
  !!     Check if a point locating in a cuboid domain with a tolerance.
  !!     @param[in] Pt    coordinates of the point.
  !!     @param[in] Box   The position of the minimum (1:3) and maximum (4:6) corners.
  !!     @param[in] toler The tolerance. A positive value of toler makes the box bigger.
  !!     @return    = .true.   the point is inside the box (including boundary).   \n
  !!                = .false.  the point is not inside the box.
  !<
  FUNCTION Geo3D_InsideBox2(Pt,Box,toler) RESULT (TF)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: Pt(3), Box(6), toler
    LOGICAL :: TF
    TF = .FALSE.
    IF(Box(1)-toler > Pt(1)) RETURN
    IF(Box(2)-toler > Pt(2)) RETURN
    IF(Box(3)-toler > Pt(3)) RETURN
    IF(Box(4)+toler < Pt(1)) RETURN
    IF(Box(5)+toler < Pt(2)) RETURN
    IF(Box(6)+toler < Pt(3)) RETURN
    TF = .TRUE.
  END FUNCTION Geo3D_InsideBox2

  !>
  !!     Check if two boxes are intersected (with a tolerance).
  !!     @param[in] Box1   The first box (the minimum (1:3) and maximum (4:6) corners).
  !!     @param[in] Box2   The second box (the minimum (1:3) and maximum (4:6) corners).
  !!     @param[in] toler The tolerance. A positive value of toler makes the boxes bigger.
  !!     @return    = .true.   the boxes are intersected.   \n
  !!                = .false.  the boxes are not intersected.
  !<
  FUNCTION Geo3D_InsideBox3(Box1,Box2,toler) RESULT (TF)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: Box1(6), Box2(6), toler
    LOGICAL :: TF
    TF = .FALSE.
    IF(Box1(1)-toler > Box2(4)) RETURN
    IF(Box1(2)-toler > Box2(5)) RETURN
    IF(Box1(3)-toler > Box2(6)) RETURN
    IF(Box1(4)+toler < Box2(1)) RETURN
    IF(Box1(5)+toler < Box2(2)) RETURN
    IF(Box1(6)+toler < Box2(3)) RETURN
    TF = .TRUE.
  END FUNCTION Geo3D_InsideBox3

  !>
  !!     Calculate the square of distance between two points.
  !!     @param[in] P1,P2   coordinates of the two points.
  !!                     If P2 is not present, then P2 = (0,0,0).
  !!     @return       the square of the distance between them.
  !<
  FUNCTION Geo3D_Distance_SQ(P1,P2) RESULT(Dis_SQ)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3)
    REAL*8, INTENT(IN), OPTIONAL :: P2(3)
    REAL*8 :: Dis_SQ
    IF(PRESENT(P2))THEN
       Dis_SQ = (P1(1)-P2(1))**2 + (P1(2)-P2(2))**2 + (P1(3)-P2(3))**2
    ELSE
       Dis_SQ = P1(1)**2 + P1(2)**2 + P1(3)**2
    ENDIF
  END FUNCTION Geo3D_Distance_SQ

  !>
  !!    Calculate the distance between two points.
  !!    @param[in] P1,P2   coordinates of the two points.
  !!                       If P2 is not present, then P2 = (0,0,0).
  !!    @return       the distance between them.
  !<
  FUNCTION Geo3D_Distance(P1,P2) RESULT(Dist)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3)
    REAL*8, INTENT(IN), OPTIONAL :: P2(3)
    REAL*8 :: Dist
    IF(PRESENT(P2))THEN
       Dist = DSQRT( (P1(1)-P2(1))**2 + (P1(2)-P2(2))**2 + (P1(3)-P2(3))**2 )
    ELSE
       Dist = DSQRT( P1(1)**2 + P1(2)**2 + P1(3)**2 )
    ENDIF
  END FUNCTION Geo3D_Distance

  !>
  !!    Calculate the Volume (6 times) of a tetrahedron P1-P2-P3-P4.
  !!    @param[in] P1,P2,P3,P4  coordinates of the four vertices.
  !!    @return    6 times of the volume of a tetrahedron (with sign).  \n
  !!              > 0, if triangle P1-P2-P3 right-hand orientates to P4.  \n
  !!              < 0, otherwise. \n
  !!    A positive volume is also defined so that P1, P2, and P3 appear
  !!    in counterclockwise order when viewed from P4.
  !<
  FUNCTION Geo3D_Tet_Volume6(P1,P2,P3,P4) RESULT(vol6)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3),P2(3),P3(3),P4(3)
    REAL*8 :: vol6
    vol6 = (P1(1)-P4(1))                                                  &
         *( (P3(3)-P2(3))*(P4(2)-P2(2)) - (P4(3)-P2(3))*(P3(2)-P2(2)) )   &
         +(P1(2)-P4(2))			                                  &
         *( (P3(1)-P2(1))*(P4(3)-P2(3)) - (P4(1)-P2(1))*(P3(3)-P2(3)) )   &
         +(P1(3)-P4(3))			                                  &
         *( (P3(2)-P2(2))*(P4(1)-P2(1)) - (P4(2)-P2(2))*(P3(1)-P2(1)) )
  END FUNCTION Geo3D_Tet_Volume6

  !>
  !!    Calculate (with high accuracy) the Volume (with 6 times) of a tetrahedron P1-P2-P3-P4.
  !!    @param[in] P1,P2,P3,P4  coordinates of the four vertices.
  !!    @return 6 times of Volume of a tetrahedron (with sign). \n
  !!    The same explaination of orientation with function Geo3D_Tet_Volume6.
  !<
  FUNCTION Geo3D_Tet_Volume6_High(P1,P2,P3,P4) RESULT(vol6)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3),P2(3),P3(3),P4(3)
    REAL*8 :: vol6
    REAL*8 :: v(6), vm
    INTEGER :: j,i,ind
    v(1) =    (P1(1)-P4(1)) * (P3(3)-P2(3)) * (P4(2)-P2(2))
    v(2) =  - (P1(1)-P4(1)) * (P4(3)-P2(3)) * (P3(2)-P2(2))
    v(3) =    (P1(2)-P4(2)) * (P3(1)-P2(1)) * (P4(3)-P2(3))
    v(4) =  - (P1(2)-P4(2)) * (P4(1)-P2(1)) * (P3(3)-P2(3))
    v(5) =    (P1(3)-P4(3)) * (P3(2)-P2(2)) * (P4(1)-P2(1))
    v(6) =  - (P1(3)-P4(3)) * (P4(2)-P2(2)) * (P3(1)-P2(1))
    vol6 = 0.d0
    DO j=1,6
       vm  = v(j)
       ind = j
       DO i=j+1,6
          IF(ABS(vm)<ABS(v(i)))THEN
             ind = i
             vm  = v(i)
          ENDIF
       ENDDO
       v(ind) = v(j)
       vol6   = vol6+vm
    ENDDO
  END FUNCTION Geo3D_Tet_Volume6_High

  !>
  !!    Calculate (with high accuracy) the Volume (with 6 times) of a tetrahedron P1-P2-P3-P4.
  !!    by using subroutine orient3d from  predicates.c
  !!    @param[in] P1,P2,P3,P4  coordinates of the four vertices.
  !!    @return    6 times of Volume of a tetrahedron (with sign). \n
  !!    The same explaination of orientation with function Geo3D_Tet_Volume6.
  !<
  FUNCTION Geo3D_Tet_Volume6_Pred(P1,P2,P3,P4) RESULT(vol6)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3),P2(3),P3(3),P4(3)
    REAL*8 :: vol6
    CALL orient3d(P1,P2,P3,P4,vol6)
  END FUNCTION Geo3D_Tet_Volume6_Pred

  !>
  !!    Calculate the Volume of a tetrahedron P1-P2-P3-P4.
  !!    @param[in] P1,P2,P3,P4  coordinates of the four vertices.
  !!    @return    Volume of a tetrahedron (with sign). \n
  !!    The same explaination of orientation with function Geo3D_Tet_Volume6.
  !<
  FUNCTION Geo3D_Tet_Volume(P1,P2,P3,P4) RESULT(vol)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3),P2(3),P3(3),P4(3)
    REAL*8 :: vol
    vol  = (P1(1)-P4(1))                                                  &
         *( (P3(3)-P2(3))*(P4(2)-P2(2)) - (P4(3)-P2(3))*(P3(2)-P2(2)) )   &
         +(P1(2)-P4(2))			                                  &
         *( (P3(1)-P2(1))*(P4(3)-P2(3)) - (P4(1)-P2(1))*(P3(3)-P2(3)) )   &
         +(P1(3)-P4(3))			                                  &
         *( (P3(2)-P2(2))*(P4(1)-P2(1)) - (P4(2)-P2(2))*(P3(1)-P2(1)) )
    vol = vol / 6.d0
  END FUNCTION Geo3D_Tet_Volume

  !>
  !!    Calculate (with high accuracy) the Volume of a tetrahedron P1-P2-P3-P4.
  !!    @param[in] P1,P2,P3,P4  coordinates of the four vertices.
  !!    @return Volume of a tetrahedron (with sign). \n
  !!    The same explaination of orientation with function Geo3D_Tet_Volume6.
  !<
  FUNCTION Geo3D_Tet_Volume_High(P1,P2,P3,P4) RESULT(vol)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3),P2(3),P3(3),P4(3)
    REAL*8 :: vol
    vol = Geo3D_Tet_Volume6_High(P1,P2,P3,P4)
    vol = vol / 6.d0
  END FUNCTION Geo3D_Tet_Volume_High

  !>
  !!    Calculate (with high accuracy) the Volume of a tetrahedron P1-P2-P3-P4
  !!    by using subroutine orient3d from  predicates.c
  !!    @param[in] P1,P2,P3,P4  coordinates of the four vertices.
  !!    @return: Volume of a tetrahedron (with sign). \n
  !!    The same explaination of orientation with function Geo3D_Tet_Volume6
  !<
  FUNCTION Geo3D_Tet_Volume_Pred(P1,P2,P3,P4) RESULT(vol)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3),P2(3),P3(3),P4(3)
    REAL*8 :: vol
    CALL orient3d(P1,P2,P3,P4,vol)
    vol = vol / 6.d0
  END FUNCTION Geo3D_Tet_Volume_Pred

  !>
  !!    Calculate the interpolation weights which satisfy
  !!    \f$  P_0 = W_1 P_1 +  W_2 P_2 + W_3 P_3 + W_4 P_4  \f$
  !!    with
  !!    \f$  W_1 +  W_2 + W_3 + W_4 = 1  \f$
  !!    @param[in]
  !!           K =0 get results only if the point located in the tetra.  \n
  !!             =1 get results always,
  !!    @param[in]
  !!           pp4  coordinates of 4 nodes.
  !!    @param[in]
  !!           pp   coordinates of the target node.
  !!    @param[out]
  !!           K =0 the point is located outside of the tetrahedron.  \n
  !!             =1 the point is located  inside of the tetrahedron.
  !!     @return    interpolation weights on 4 nodes (with sign).
  !<
  FUNCTION Geo3D_Tet_Weight(K,pp4,pp) RESULT(weight)
    IMPLICIT NONE
    INTEGER, INTENT(INOUT) :: K
    REAL*8,  INTENT(IN)    :: pp4(3,4),pp(3)
    REAL*8  :: weight(4)
    INTEGER :: i
    REAL*8  :: ppt(3,4), vol6

    vol6 = Geo3D_Tet_Volume6(pp4(:,1),pp4(:,2),pp4(:,3),pp4(:,4))
    ppt(1:3,:) = pp4(1:3,:)
    DO i = 1,3
       ppt(1:3,i) = pp(1:3)
       weight(i) = Geo3D_Tet_Volume6(ppt(:,1),ppt(:,2),ppt(:,3),ppt(:,4))/vol6
       ppt(1:3,i) = pp4(1:3,i)

       IF(K==0.AND.weight(i)<0)THEN
          RETURN
       ENDIF
    ENDDO

    weight(4) = 1.D0 - weight(1) - weight(2) - weight(3)
    IF(weight(1)<0 .OR. weight(2)<0 .OR. weight(3)<0 .OR. weight(4)<0)THEN
       K = 0
    ELSE
       K = 1
    ENDIF

    RETURN
  END FUNCTION Geo3D_Tet_Weight

  !>
  !!    Calculate (with high accuracy) the interpolation weights which satisfy
  !!    \f$  P_0 = W_1 P_1 +  W_2 P_2 + W_3 P_3 + W_4 P_4  \f$
  !!    with
  !!    \f$  W_1 +  W_2 + W_3 + W_4 = 1  \f$
  !!    @param[in]
  !!           K =0 get results only if the point located in the tetra.  \n
  !!             =1 get results always,
  !!    @param[in]
  !!           pp4  coordinates of 4 nodes.
  !!    @param[in]
  !!           pp   coordinates of the target node.
  !!    @param[out]
  !!           K =0 the point is located outside of the tetrahedron.  \n
  !!             =1 the point is located  inside of the tetrahedron.
  !!     @return    interpolation weights on 4 nodes (with sign).
  !<
  FUNCTION Geo3D_Tet_Weight_High(K,pp4,pp) RESULT(weight)
    IMPLICIT NONE
    INTEGER, INTENT(INOUT) :: K
    REAL*8,  INTENT(IN)    :: pp4(3,4),pp(3)
    REAL*8  :: weight(4)
    INTEGER :: i
    REAL*8  :: ppt(3,4), vol6

    vol6 = Geo3D_Tet_Volume6_High(pp4(:,1),pp4(:,2),pp4(:,3),pp4(:,4))
    ppt(1:3,:) = pp4(1:3,:)
    DO i = 1,3
       ppt(1:3,i) = pp(1:3)
       weight(i) = Geo3D_Tet_Volume6_High(ppt(:,1),ppt(:,2),ppt(:,3),ppt(:,4))/vol6
       ppt(1:3,i) = pp4(1:3,i)

       IF(K==0.AND.weight(i)<0)THEN
          RETURN
       ENDIF
    ENDDO

    weight(4) = 1.D0 - weight(1) - weight(2) - weight(3)
    IF(weight(1)<0 .OR. weight(2)<0 .OR. weight(3)<0 .OR. weight(4)<0)THEN
       K = 0
    ELSE
       K = 1
    ENDIF

    RETURN
  END FUNCTION Geo3D_Tet_Weight_High

  !>
  !!    Calculate the interpolation weights which satisfy
  !!    \f$  P_0 = W_1 P_1 +  W_2 P_2 + W_3 P_3  \f$
  !!    with
  !!    \f$  W_1 +  W_2 + W_3 = 1  \f$ .
  !!    Here \f$ P_0 \f$ is the preject position of a given point
  !!    on the triangle plane.
  !!    @param[in]  pp3   coordinates of 3 nodes of a triangle.
  !!    @param[in]  pp    coordinates of the target node.
  !!    @param[out] K =0 the point is projected outside of the triangle.  \n
  !!                  =1 the point is projected inside of the triangle.
  !!    @return  interpolation weights on 3 nodes (with sign).
  !<
  FUNCTION Geo3D_Triangle_Weight(K,pp3,pp) RESULT(weight)
    IMPLICIT NONE
    INTEGER, INTENT(OUT) :: K
    REAL*8,  INTENT(IN)  :: pp3(3,3),pp(3)
    REAL*8  :: weight(3)
    REAL*8  :: v(3),a(3)

    v = Geo3D_Cross_Product(pp3(:,1),pp3(:,2),pp3(:,3))

    a = Geo3D_Cross_Product(pp,pp3(:,2),pp3(:,3))
    weight(1) = a(1)*v(1) + a(2)*v(2) + a(3)*v(3)

    a = Geo3D_Cross_Product(pp3(:,1),pp,pp3(:,3))
    weight(2) = a(1)*v(1) + a(2)*v(2) + a(3)*v(3)

    a = Geo3D_Cross_Product(pp3(:,1),pp3(:,2),pp)
    weight(3) = a(1)*v(1) + a(2)*v(2) + a(3)*v(3)

    weight(1:3) = weight(1:3) / (v(1)*v(1)+v(2)*v(2)+v(3)*v(3))

    IF(weight(1)<0 .OR. weight(2)<0 .OR. weight(3)<0)THEN
       K = 0
    ELSE
       K = 1
    ENDIF

    RETURN
  END FUNCTION Geo3D_Triangle_Weight

  !>
  !!  Calculate the weight-axes for a triangle.
  !!  The outputs are used for function Geo3D_Triangle_Axes_Weight().
  !!    @param[in]  pp3   coordinates of 3 nodes of a triangle.
  !!    @param[out] Axes,woo  as the input of function Geo3D_Triangle_Axes_Weight().
  !<
  SUBROUTINE Geo3D_Triangle_Axes(pp3, Axes, woo)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: pp3(3,3)
    REAL*8, INTENT(OUT) :: Axes(3,3), woo(3)
    REAL*8 :: a1(3), a2(3), a3(3), an(3), as, h1, h2, h3

    a1(:) = pp3(:,3) - pp3(:,2)
    a2(:) = pp3(:,1) - pp3(:,3)
    a3(:) = pp3(:,2) - pp3(:,1)
    an(:) = Geo3D_Cross_Product(a2, a3)
    Axes(:,1) = Geo3D_Cross_Product(a1, an)
    Axes(:,2) = Geo3D_Cross_Product(a2, an)
    Axes(:,3) = Geo3D_Cross_Product(a3, an)

    as = Geo3D_Distance(an)
    h1 = Geo3D_Distance(a1) / ( as * Geo3D_Distance(Axes(:,1)) )
    h2 = Geo3D_Distance(a2) / ( as * Geo3D_Distance(Axes(:,2)) )
    h3 = Geo3D_Distance(a3) / ( as * Geo3D_Distance(Axes(:,3)) )
    Axes(:,1) = Axes(:,1) * h1
    Axes(:,2) = Axes(:,2) * h2
    Axes(:,3) = Axes(:,3) * h3
    woo(1)    = Geo3D_Dot_Product(pp3(:,1), Axes(:,1)) + 1.d0
    woo(2)    = Geo3D_Dot_Product(pp3(:,2), Axes(:,2)) + 1.d0
    woo(3)    = Geo3D_Dot_Product(pp3(:,3), Axes(:,3)) + 1.d0
  END SUBROUTINE Geo3D_Triangle_Axes

  !>
  !!  Compute the weights of the three nodes of a triangle
  !!    respecting a point inside.
  !!    @param[out] Axes,woo  as the output of SUBROUTINE Geo3D_Triangle_Axes().
  !!    @param[in]  pp    coordinates of the target node.
  !!    @return  interpolation weights on 3 nodes (with sign).
  !<
  FUNCTION Geo3D_Triangle_Axes_Weight(Axes, woo, pp) RESULT(weight)
    IMPLICIT NONE
    REAL*8,  INTENT(IN)  :: Axes(3,3), woo(3), pp(3)
    REAL*8 :: weight(3)
    weight(1)  = woo(1) - Geo3D_Dot_Product(pp, Axes(:,1))
    weight(2)  = woo(2) - Geo3D_Dot_Product(pp, Axes(:,2))
    weight(3)  = woo(3) - Geo3D_Dot_Product(pp, Axes(:,3))
  END FUNCTION Geo3D_Triangle_Axes_Weight


  !>
  !!    Calculate the area of a triangle.
  !!    @param[in] P1,P2,P3  coordinates of the three vertices.
  !!    @return    the area of a triangle.
  !<
  FUNCTION Geo3D_Triangle_Area(P1,P2,P3) RESULT(area)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3), P2(3), P3(3)
    REAL*8 :: area
    REAL*8 :: a(3)
    a    = Geo3D_Cross_Product(P1,P2,P3)
    area = 0.5d0 * DSQRT(a(1)*a(1) + a(2)*a(2) + a(3)*a(3))
  END FUNCTION Geo3D_Triangle_Area

  !>
  !!    Calculate the quality of a triangle.
  !!    @param[in] P1,P2,P3 : coordinates of the three vertices.
  !!    @param[out] area    : the area of the triangle.
  !!    @return    the quality of a triangle.
  !<
  FUNCTION Geo3D_Triangle_Quality(P1, P2, P3, area) RESULT(qual)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: P1(3), P2(3), P3(3)
    REAL*8, INTENT(OUT), OPTIONAL :: area
    REAL*8 :: qual
    REAL*8 :: d1, d2, d3
    d1   = Geo3D_Distance_SQ(P1, P2) 
    d2   = Geo3D_Distance_SQ(P2, P3) 
    d3   = Geo3D_Distance_SQ(P3, P1)
    qual = Geo3D_Triangle_Area(P1,P2,P3)
    IF(PRESENT(area)) area = qual
    qual = 6.928204d0 * qual / (d1+d2+d3)
  END FUNCTION  Geo3D_Triangle_Quality

  !>
  !!    Calculate the Cross Product of two vectors P2--P1 & P2--P3
  !!              or O--P1 & O--P2 if P3 is not present.
  !!    @param[in] P1,P2,P3 coordinates of the three vertices.
  !!    @return    the the Cross Product of two vectors.
  !<
  FUNCTION Geo3D_Cross_Product(P1,P2,P3) RESULT(prod)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3), P2(3)
    REAL*8, INTENT(IN), OPTIONAL :: P3(3)
    REAL*8 :: prod(3)
    REAL*8 :: a(3), b(3)
    IF(PRESENT(P3))THEN
       a = P1-P2
       b = P3-P2
    ELSE
       a = P1
       b = P2
    ENDIF
    prod(1) = a(2)*b(3)-a(3)*b(2)
    prod(2) = a(3)*b(1)-a(1)*b(3)
    prod(3) = a(1)*b(2)-a(2)*b(1)
  END FUNCTION Geo3D_Cross_Product

  !>
  !!    Calculate the Dot Product of two vectors P2--P1 & P2--P3
  !!              or O--P1 & O--P2 if P3 is not present.
  !!    @param[in] P1,P2,P3  coordinates of the three vertices.
  !!    @return            the the Dot Product of two vectors.
  !<
  FUNCTION Geo3D_Dot_Product(P1,P2,P3) RESULT(prod)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3), P2(3)
    REAL*8, INTENT(IN), OPTIONAL :: P3(3)
    REAL*8 :: prod
    IF(PRESENT(P3))THEN
       prod = (P1(1)-P2(1))*(P3(1)-P2(1))     &
            + (P1(2)-P2(2))*(P3(2)-P2(2))     &
            + (P1(3)-P2(3))*(P3(3)-P2(3))
    ELSE
       prod = P1(1)*P2(1) + P1(2)*P2(2) + P1(3)*P2(3)
    ENDIF
  END FUNCTION Geo3D_Dot_Product

  !>
  !!    Calculate the Product of two vectors V1 & V2.
  !!    @param[in] V1,V2   the two vector
  !!    @return            the matrix = transpose(V1) * V2.
  !<
  FUNCTION Geo3D_Vector_Product(V1,V2) RESULT(fMatrix)
    IMPLICIT NONE
    REAL*8, DIMENSION(:), INTENT(in)     :: V1,V2
    REAL*8, DIMENSION(SIZE(V1),SIZE(V2)) :: fMatrix
    INTEGER :: k
    DO k = 1, SIZE(V1)
       fMatrix(k,:)=V1(k) * V2(:)
    ENDDO
  END FUNCTION Geo3D_Vector_Product

  !>
  !!    Calculate the Included Angle of two vectors P2-P1 & P2-P3 
  !!                  (or P1 & P2 if P3 is not presented).
  !!    @param[in] P1,P2,P3  coordinates of the three vertices.
  !!    @return    the Included Angle of two vectors ( 0 < a <= PI ).
  !<
  FUNCTION Geo3D_Included_Angle(P1,P2,P3) RESULT(Angle)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3), P2(3)
    REAL*8, INTENT(IN), OPTIONAL :: P3(3)
    REAL*8 :: Angle
    REAL*8, PARAMETER :: TINY = 1.D-20
    REAL*8  :: c(3),crop,dotp
    IF(PRESENT(P3))THEN
    IF(  ABS(P1(1)-P2(1)) + ABS(P1(2)-P2(2)) + ABS(P1(3)-P2(3)) <= TINY .OR.  &
         ABS(P3(1)-P2(1)) + ABS(P3(2)-P2(2)) + ABS(P3(3)-P2(3)) <= TINY ) THEN
       Angle = 1000.
    ELSE
       c(1:3) = Geo3D_Cross_Product(P1,P2,P3)
       crop   = dsqrt(c(1)*c(1)+c(2)*c(2)+c(3)*c(3))
       dotp   = Geo3D_Dot_Product(P1,P2,P3)
       Angle  = ATAN2(crop,dotp)
    ENDIF
    ELSE
    IF(  ABS(P1(1)) + ABS(P1(2)) + ABS(P1(3)) <= TINY .OR.  &
         ABS(P2(1)) + ABS(P2(2)) + ABS(P2(3)) <= TINY ) THEN
       Angle = 1000.
    ELSE
       c(1:3) = Geo3D_Cross_Product(P1,P2)
       crop   = dsqrt(c(1)*c(1)+c(2)*c(2)+c(3)*c(3))
       dotp   = Geo3D_Dot_Product(P1,P2)
       Angle  = ATAN2(crop,dotp)
    ENDIF
    ENDIF
    RETURN
  END FUNCTION Geo3D_Included_Angle

  !>
  !!    Calculate the dihedral angle of two faces P1-P2-P3 & P1-P2-P4.
  !!    @param[in] P1,P2,P3,P4  coordinates of the four vertices.
  !!    @param[out] Isucc
  !!    @return       the Included Angle of two faces ( -PI < Angle <= PI ). \n
  !!                  if Angle>0, then Point P4 locates on
  !!                       the right hand direction of triangle P1-P2-P3.
  !<
  FUNCTION Geo3D_Dihedral_Angle(P1, P2, P3, P4, Isucc) RESULT(Angle)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3), P2(3), P3(3), P4(3)
    REAL*8 :: Angle
    INTEGER, INTENT(OUT) :: Isucc
    REAL*8 :: cs(3), ca(3), cn1(3), cn2(3)
    REAL*8 :: cosa, sina, xl

    Isucc    = 0
    Angle    = 0
    
    cs(1:3)  = p2(1:3)-p1(1:3)
    xl       = cs(1)*cs(1)+cs(2)*cs(2)+cs(3)*cs(3)
    IF(xl==0.D0) RETURN
    cs(1:3)  = cs(1:3)/DSQRT(xl)

    ca(1:3)  = p3(1:3)-p1(1:3)
    cn1(1:3) = Geo3D_Cross_Product(ca,cs)
    IF(cn1(1)==0.D0 .AND. cn1(2)==0.D0 .AND. cn1(3)==0.D0) RETURN

    ca(1:3)  = p4(1:3)-p1(1:3)
    cn2(1:3) = Geo3D_Cross_Product(ca,cs)
    IF(cn2(1)==0.D0 .AND. cn2(2)==0.D0 .AND. cn2(3)==0.D0) RETURN

    cosa     = cn1(1)*cn2(1)+cn1(2)*cn2(2)+cn1(3)*cn2(3)
    ca(1:3)  = Geo3D_Cross_Product(cn1,cn2)
    sina     = cs(1)*ca(1)+cs(2)*ca(2)+cs(3)*ca(3)
    Angle    = DATAN2(sina,cosa)
    Isucc    = 1

    RETURN
  END FUNCTION Geo3D_Dihedral_Angle

  !>
  !!    Calculate the centre of the Circumscribed sphere of a tetrahedron (old version).
  !!    @param[in] P1,P2,P3,P4   coordinates of the 4 vertices.
  !!    @return    coordinates of the centre of the Circumsphere.
  !<
  FUNCTION Geo3D_Sphere_Centre_old(P1,P2,P3,P4) RESULT(PO)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: P1(3),P2(3),P3(3),P4(3)
    REAL*8  :: PO(3)
    REAL*8  :: aa,bb,cc,d1,d2,d3,d4,dd,hh,al,ee,ff,gg
    REAL*8  :: ai,aj,ak,s1,s2,s3,s4,s5,s6,te,t1,t2,t3

    aa  =  2*(P2(1)-P1(1))
    bb  =  2*(P2(2)-P1(2))
    cc  =  2*(P2(3)-P1(3))
    ee  =  2*(P3(1)-P1(1))
    ff  =  2*(P3(2)-P1(2))
    gg  =  2*(P3(3)-P1(3))
    ai  =  2*(P4(1)-P1(1))
    aj  =  2*(P4(2)-P1(2))
    ak  =  2*(P4(3)-P1(3))
    d1  =  P1(1)*P1(1)+P1(2)*P1(2)+P1(3)*P1(3)
    d2  =  P2(1)*P2(1)+P2(2)*P2(2)+P2(3)*P2(3)
    d3  =  P3(1)*P3(1)+P3(2)*P3(2)+P3(3)*P3(3)
    d4  =  P4(1)*P4(1)+P4(2)*P4(2)+P4(3)*P4(3)
    dd  =  d2-d1
    hh  =  d3-d1
    al  =  d4-d1
    s1  =  ff*ak-aj*gg
    s2  =  ee*ak-ai*gg
    s3  =  ee*aj-ai*ff
    s4  =  hh*ak-al*gg
    s5  =  hh*aj-al*ff
    s6  =  ee*al-hh*ai
    te  =  aa*s1-bb*s2+cc*s3
    t1  =  dd*s1-bb*s4+cc*s5
    t2  =  aa*s4-dd*s2+cc*s6
    t3  = -aa*s5-bb*s6+dd*s3

    PO(1)  =  t1 / te
    PO(2)  =  t2 / te
    PO(3)  =  t3 / te

    RETURN
  END FUNCTION Geo3D_Sphere_Centre_old

  !>
  !!    Calculate the centre of the Circumscribed sphere of a tetrahedron.
  !!    @param[in] P1,P2,P3,P4   coordinates of the 4 vertices.
  !!    @return    coordinates of the centre of the Circumsphere.
  !!
  !!    Reminder: if P4 id not presented, 
  !!              return the sphere of which P1,P2,P3 take a big circle.
  !<
  FUNCTION Geo3D_Sphere_Centre(P1,P2,P3,P4) RESULT(PO)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: P1(3), P2(3), P3(3)
    REAL*8, INTENT(IN), OPTIONAL  :: P4(3)
    REAL*8  :: PO(3)
    REAL*8  :: fMatrix(3,3), R(3), CC(3), d1, te

    fMatrix(1,1)  =  2*(P2(1)-P1(1))
    fMatrix(1,2)  =  2*(P2(2)-P1(2))
    fMatrix(1,3)  =  2*(P2(3)-P1(3))
    fMatrix(2,1)  =  2*(P3(1)-P1(1))
    fMatrix(2,2)  =  2*(P3(2)-P1(2))
    fMatrix(2,3)  =  2*(P3(3)-P1(3))
    IF(PRESENT(P4))THEN
       fMatrix(3,1)  =  2*(P4(1)-P1(1))
       fMatrix(3,2)  =  2*(P4(2)-P1(2))
       fMatrix(3,3)  =  2*(P4(3)-P1(3))
    ELSE
       CC = Geo3D_Cross_Product(P1,P2,P3)
       fMatrix(3,1)  =   CC(1) 
       fMatrix(3,2)  =   CC(2)
       fMatrix(3,3)  =   CC(3)
    ENDIF
    d1    =  P1(1)*P1(1) +P1(2)*P1(2) +P1(3)*P1(3)
    R(1)  =  P2(1)*P2(1) +P2(2)*P2(2) +P2(3)*P2(3) - d1
    R(2)  =  P3(1)*P3(1) +P3(2)*P3(2) +P3(3)*P3(3) - d1
    IF(PRESENT(P4))THEN
       R(3) =  P4(1)*P4(1) +P4(2)*P4(2) +P4(3)*P4(3) - d1
    ELSE
       R(3) =  P1(1)*CC(1) +P1(2)*CC(2) +P1(3)*CC(3)
    ENDIF

    CALL Geo3D_Matrix_Solver(fMatrix, R, PO, te)
    RETURN
  END FUNCTION Geo3D_Sphere_Centre

  !>
  !!    4 spheres in the space can make 6 chord face,
  !!       which can be proved meeting at one point.
  !!    This function is for calculating this point.
  !!       (A chord face is defined at function
  !!        Plane3DGeom::Plane3D_Sphere_Sphere_Chord)
  !!    @param[in] P1,P2,P3,P4       centres of the 4 sphere.
  !!    @param[in] RR1,RR2,RR3,RR4   square of radii of the 4 sphere.
  !!    @return               the cross point of chord faces.
  !<
  FUNCTION Geo3D_Sphere_Cross(P1,P2,P3,P4,RR1,RR2,RR3,RR4) RESULT(PO)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: P1(3),P2(3),P3(3),P4(3)
    REAL*8, INTENT(IN)  :: RR1, RR2, RR3, RR4
    REAL*8  :: PO(3)
    REAL*8  :: fMatrix(3,3), R(3), d1, te

    fMatrix(1,1)  =  2*(P2(1)-P1(1))
    fMatrix(1,2)  =  2*(P2(2)-P1(2))
    fMatrix(1,3)  =  2*(P2(3)-P1(3))
    fMatrix(2,1)  =  2*(P3(1)-P1(1))
    fMatrix(2,2)  =  2*(P3(2)-P1(2))
    fMatrix(2,3)  =  2*(P3(3)-P1(3))
    fMatrix(3,1)  =  2*(P4(1)-P1(1))
    fMatrix(3,2)  =  2*(P4(2)-P1(2))
    fMatrix(3,3)  =  2*(P4(3)-P1(3))
    d1    =  P1(1)*P1(1) +P1(2)*P1(2) +P1(3)*P1(3)
    R(1)  =  P2(1)*P2(1) +P2(2)*P2(2) +P2(3)*P2(3) - d1 + RR1 - RR2
    R(2)  =  P3(1)*P3(1) +P3(2)*P3(2) +P3(3)*P3(3) - d1 + RR1 - RR3
    R(3)  =  P4(1)*P4(1) +P4(2)*P4(2) +P4(3)*P4(3) - d1 + RR1 - RR4

    CALL Geo3D_Matrix_Solver(fMatrix, R, PO, te)

    RETURN
  END FUNCTION Geo3D_Sphere_Cross

  !>
  !!    Calculate the Quality of a tetrahedron P1-P2-P3-P4
  !!    @param[in] P1,P2,P3,P4  coordinates of the four vertices.
  !!    @param[in]   Kqual  =1   return the volume of the tetrahedron. \n
  !!                  =2   return the minimum Dihedral_Angle
  !!    @return Quality of a tetrahedron (with sign). \n
  !!             >  0,if triangle P1-P2-P3 right-hand orientates to P4. \n
  !!             <= 0, otherwise
  !<
  FUNCTION Geo3D_Tet_Quality(P1,P2,P3,P4,Kqual) RESULT(Qual)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Kqual
    INTEGER :: Isucc
    REAL*8, INTENT(IN)  :: P1(3),P2(3),P3(3),P4(3)
    REAL*8 :: Qual, ang
    REAL*8 , PARAMETER   :: PI     = 3.141592653589793D0
    REAL*8 , PARAMETER   :: halfPI = PI/2.D0

    SELECT CASE(Kqual)

    CASE(1)
       Qual = Geo3D_Tet_Volume(P1, P2, P3, P4)

    CASE(2)
       Qual = 100.

       ang = Geo3D_Dihedral_Angle(P1, P2, P3, P4, Isucc)
       IF(Isucc==0)THEN
          Qual=0.
          RETURN
       ENDIF
       IF(ang<-halfPI) ang = -ang - PI
       Qual = MIN(Qual,ang)

       ang = Geo3D_Dihedral_Angle(P2, P3, P1, P4, Isucc)
       IF(Isucc==0)THEN
          Qual=0.
          RETURN
       ENDIF
       IF(ang<-halfPI) ang = -ang - PI
       Qual = MIN(Qual,ang)

       ang = Geo3D_Dihedral_Angle(P3, P1, P2, P4, Isucc)
       IF(Isucc==0)THEN
          Qual=0.
          RETURN
       ENDIF
       IF(ang<-halfPI) ang = -ang - PI
       Qual = MIN(Qual,ang)

       ang = Geo3D_Dihedral_Angle(P2, P4, P3, P1, Isucc)
       IF(Isucc==0)THEN
          Qual=0.
          RETURN
       ENDIF
       IF(ang<-halfPI) ang = -ang - PI
       Qual = MIN(Qual,ang)

       ang = Geo3D_Dihedral_Angle(P3, P4, P1, P2, Isucc)
       IF(Isucc==0)THEN
          Qual=0.
          RETURN
       ENDIF
       IF(ang<-halfPI) ang = -ang - PI
       Qual = MIN(Qual,ang)

       ang = Geo3D_Dihedral_Angle(P1, P4, P2, P3, Isucc)
       IF(Isucc==0)THEN
          Qual=0.
          RETURN
       ENDIF
       IF(ang<-halfPI) ang = -ang - PI
       Qual = MIN(Qual,ang)

    CASE DEFAULT
       WRITE(*,*)' '
       WRITE(*,*)'Error--- : no such case'
       WRITE(*,*)'Kqual=',Kqual
       CALL Error_STOP ( '--- Geo3D_Tet_Quality')

    END SELECT

    RETURN
  END FUNCTION Geo3D_Tet_Quality


  !>
  !!    Return the transpose of a matrix.
  !<
  FUNCTION Geo3D_Matrix_Transpose(fMatrix) RESULT(transp)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: fMatrix(3,3)
    REAL*8 :: transp(3,3)
    transp(:,1) = fMatrix(1,:)
    transp(:,2) = fMatrix(2,:)
    transp(:,3) = fMatrix(3,:)
    RETURN
  END FUNCTION Geo3D_Matrix_Transpose

  !>
  !!    Return the determinant of a Matrix
  !<
  FUNCTION Geo3D_Matrix_Determinant(fMatrix) RESULT(det)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: fMatrix(3,3)
    REAL*8 :: det, rev(3)
    rev(1) = fMatrix(2,2)*fMatrix(3,3) - fMatrix(3,2)*fMatrix(2,3)
    rev(2) = fMatrix(3,2)*fMatrix(1,3) - fMatrix(1,2)*fMatrix(3,3)
    rev(3) = fMatrix(1,2)*fMatrix(2,3) - fMatrix(2,2)*fMatrix(1,3)
    det =  fMatrix(1,1)*rev(1) + fMatrix(2,1)*rev(2) + fMatrix(3,1)*rev(3)
    RETURN
  END FUNCTION Geo3D_Matrix_Determinant

  !>
  !!    Calculate the Product of two Matrices
  !<
  FUNCTION Geo3D_Matrix_Product(fMatrix1,fMatrix2) RESULT(prod)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: fMatrix1(3,3), fMatrix2(3,3)
    REAL*8 :: prod(3,3)
    INTEGER :: i,j,k
    DO i=1,3
       DO j=1,3
          prod(i,j) = 0.d0
          DO k=1,3
             prod(i,j) = prod(i,j) + fMatrix1(i,k)*fMatrix2(k,j)
          ENDDO
       ENDDO
    ENDDO
  END FUNCTION Geo3D_Matrix_Product

  !>
  !!    Calculate the inverse of a matrix.
  !<
  FUNCTION Geo3D_Matrix_Inverse(fMatrix) RESULT(rev)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: fMatrix(3,3)
    REAL*8 :: rev(3,3), det

    rev(1,1) = fMatrix(2,2)*fMatrix(3,3) - fMatrix(3,2)*fMatrix(2,3)
    rev(2,1) = fMatrix(2,3)*fMatrix(3,1) - fMatrix(3,3)*fMatrix(2,1)
    rev(3,1) = fMatrix(2,1)*fMatrix(3,2) - fMatrix(3,1)*fMatrix(2,2)
    rev(1,2) = fMatrix(3,2)*fMatrix(1,3) - fMatrix(1,2)*fMatrix(3,3)
    rev(2,2) = fMatrix(3,3)*fMatrix(1,1) - fMatrix(1,3)*fMatrix(3,1)
    rev(3,2) = fMatrix(3,1)*fMatrix(1,2) - fMatrix(1,1)*fMatrix(3,2)
    rev(1,3) = fMatrix(1,2)*fMatrix(2,3) - fMatrix(2,2)*fMatrix(1,3)
    rev(2,3) = fMatrix(1,3)*fMatrix(2,1) - fMatrix(2,3)*fMatrix(1,1)
    rev(3,3) = fMatrix(1,1)*fMatrix(2,2) - fMatrix(2,1)*fMatrix(1,2)

    det =  fMatrix(1,1)*rev(1,1) + fMatrix(2,1)*rev(1,2)   &
         + fMatrix(3,1)*rev(1,3)
    rev(:,:) = rev(:,:) / det

    RETURN
  END FUNCTION Geo3D_Matrix_Inverse

  !>
  !!    3x3 linear equation solver (old version)
  !<
  SUBROUTINE Geo3D_Matrix_Solver_old(fMatrix, R, X, det)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: fMatrix(3,3), R(3)
    REAL*8, INTENT(OUT) :: X(3), det
    REAL*8 :: v11,v12,v13,v21,v22,v23,v31,v32,v33

    v11 = fMatrix(2,2)*fMatrix(3,3) - fMatrix(3,2)*fMatrix(2,3)
    v21 = fMatrix(2,3)*fMatrix(3,1) - fMatrix(3,3)*fMatrix(2,1)
    v31 = fMatrix(2,1)*fMatrix(3,2) - fMatrix(3,1)*fMatrix(2,2)
    v12 = fMatrix(3,2)*fMatrix(1,3) - fMatrix(1,2)*fMatrix(3,3)
    v22 = fMatrix(3,3)*fMatrix(1,1) - fMatrix(1,3)*fMatrix(3,1)
    v32 = fMatrix(3,1)*fMatrix(1,2) - fMatrix(1,1)*fMatrix(3,2)
    v13 = fMatrix(1,2)*fMatrix(2,3) - fMatrix(2,2)*fMatrix(1,3)
    v23 = fMatrix(1,3)*fMatrix(2,1) - fMatrix(2,3)*fMatrix(1,1)
    v33 = fMatrix(1,1)*fMatrix(2,2) - fMatrix(2,1)*fMatrix(1,2)

    det =  fMatrix(1,1)*v11 + fMatrix(2,1)*v12 + fMatrix(3,1)*v13
    X(1) = (R(1)*v11 + R(2)*v12 + R(3)*v13) / det
    X(2) = (R(1)*v21 + R(2)*v22 + R(3)*v23) / det
    X(3) = (R(1)*v31 + R(2)*v32 + R(3)*v33) / det

    RETURN
  END SUBROUTINE Geo3D_Matrix_Solver_old

  !>
  !!    3x3 linear equation solver
  !<
  SUBROUTINE Geo3D_Matrix_Solver(fMatrix, R, X, det)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: fMatrix(3,3), R(3)
    REAL*8, INTENT(OUT) :: X(3), det
    REAL*8 :: s1, s2, s3, s4, s5, s6

    s1  =  fMatrix(2,2)*fMatrix(3,3) - fMatrix(3,2)*fMatrix(2,3)
    s2  =  fMatrix(2,1)*fMatrix(3,3) - fMatrix(3,1)*fMatrix(2,3)
    s3  =  fMatrix(2,1)*fMatrix(3,2) - fMatrix(3,1)*fMatrix(2,2)
    s4  =  R(2)*fMatrix(3,3) - R(3)*fMatrix(2,3)
    s5  =  R(2)*fMatrix(3,2) - R(3)*fMatrix(2,2)
    s6  =  fMatrix(2,1)*R(3) - R(2)*fMatrix(3,1)
    det =  fMatrix(1,1)*s1 - fMatrix(1,2)*s2 + fMatrix(1,3)*s3
    X(1)  = ( R(1)*s1 - fMatrix(1,2)*s4 + fMatrix(1,3)*s5 ) / det
    X(2)  = (-R(1)*s2 + fMatrix(1,1)*s4 + fMatrix(1,3)*s6 ) / det
    X(3)  = ( R(1)*s3 - fMatrix(1,1)*s5 - fMatrix(1,2)*s6 ) / det

    RETURN
  END SUBROUTINE Geo3D_Matrix_Solver

  !>
  !!    Calculate the roots of a cubic equation
  !!    \f$     x^3 + a_2 x^2 + a_1 x + a_0 = 0       \f$
  !!    @param[in] a2,a1,a0  the cofficients \f$ a_2, a_1, a_0 \f$ .
  !!    @param[out] nroot =1, one real root and two complex roots as
  !!                      x(1), x(2)+i*x(3), & x(2)-i*x(3).  \n
  !!             =3, three real roots as x(1:3).
  !!    @param[out]  x three solutions.
  !<
  SUBROUTINE Geo3D_Cubic_Equation(a2,a1,a0,x,nroot)
    IMPLICIT NONE
    REAL*8,  INTENT(IN)  :: a2,a1,a0
    REAL*8,  INTENT(OUT) :: x(3)
    INTEGER, INTENT(OUT) :: nroot
    REAL*8 , PARAMETER   :: PI = 3.141592653589793D0
    REAL*8 :: C, Q, R, DD, D, S, T, B, TINY

    C  = -a2/3.d0
    Q  = (3.d0*a1 - a2*a2) / 9.d0
    R  = (9.d0*a2*a1 - 27.d0*a0 - 2.d0*a2*a2*a2) / 54.d0
    DD = Q*Q*Q + R*R
    TINY = 1.d-10 * MAX(ABS(a2**6),ABS(a1**3), ABS(a0**2))

    IF(DD>TINY)THEN
       nroot = 1
       D = dsqrt(DD)
       IF(R+D>0)THEN
          S = (R+D)**(1.d0/3.d0)
       ELSE IF(R+D<0)THEN
          S = (-R-D)**(1.d0/3.d0)
          S = -S
       ELSE
          S = 0.d0
       ENDIF
       IF(R-D>0)THEN
          T = (R-D)**(1.d0/3.d0)
       ELSE IF(R+D<0)THEN
          T = (-R+D)**(1.d0/3.d0)
          T = -T
       ELSE
          T = 0.d0
       ENDIF
       B = S+T
       x(1) = C + B
       x(2) = C - B/2.d0
       x(3) = (S-T)*dsqrt(3.d0)/2.d0
       RETURN
    ELSE IF(DD<0)THEN
       nroot = 3
       D = dsqrt(-Q)
       IF(ABS(R)<ABS(D**3))THEN
          S = dacos(R/(D**3))
          x(1) = C + 2.d0*D*DCOS(S/3.d0)
          x(2) = C + 2.d0*D*DCOS((S+2.d0*PI)/3.d0)
          x(3) = C + 2.d0*D*DCOS((S+4.d0*PI)/3.d0)
          RETURN
       ENDIF
    ENDIF

    nroot = 3
    IF(Q>=0)THEN
       x(1:3) = C
    ELSE
       D = dsqrt(-Q)
       IF(R>0)THEN
          S = D
       ELSE IF(R<0)THEN
          S = -D
       ELSE
          S = 0.d0
       ENDIF
       x(1)   = C + 2.d0*S
       x(2:3) = C - S
    ENDIF

    RETURN
  END SUBROUTINE Geo3D_Cubic_Equation

  !>
  !!    Calculate the eigen values and vectors of a Matrix fMatrix.
  !!    @param[in]   fMatrix  the matrix.
  !!    @param[out]  values   three eigen values.
  !!    @param[out]  vectors (:,i)  eigen vector associated with values(i), i=1:3.
  !<
  SUBROUTINE Geo3D_Matrix_Eigen(fMatrix, values, vectors)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: fMatrix(3,3)
    REAL*8, INTENT(OUT) :: values(3), vectors(3,3)
    REAL*8 :: det, ad, TINY,TINY2
    REAL*8 :: a2, a1, a0, r(3), fM(3,3)
    INTEGER :: nroot, i,j,k,i1,i2,j1,j2,ktwin,Itolerance
    INTEGER :: iorder(5) = (/1,2,3,1,2/)

    !--- calculate eigen values bu solving a cubic equation
    a2 = -(fMatrix(1,1) + fMatrix(2,2) + fMatrix(3,3))
    a1 = fMatrix(1,1)*fMatrix(2,2) + fMatrix(1,1)*fMatrix(3,3)   &
         + fMatrix(2,2)*fMatrix(3,3) - fMatrix(1,2)*fMatrix(2,1)   &
         - fMatrix(1,3)*fMatrix(3,1) - fMatrix(2,3)*fMatrix(3,2)
    a0 = fMatrix(1,2)*fMatrix(2,1)*fMatrix(3,3)    &
         + fMatrix(1,3)*fMatrix(3,1)*fMatrix(2,2)    &
         + fMatrix(2,3)*fMatrix(3,2)*fMatrix(1,1)    &
         - fMatrix(1,2)*fMatrix(2,3)*fMatrix(3,1)    &
         - fMatrix(3,2)*fMatrix(2,1)*fMatrix(1,3)    &
         - fMatrix(1,1)*fMatrix(2,2)*fMatrix(3,3)
    CALL Geo3D_Cubic_Equation(a2,a1,a0,values,nroot)

    IF(nroot==1)THEN
       IF(ABS(values(3))<1.e-6*MAX(1.d0,ABS(values(2))))THEN
          !--- ignor imagine part
          values(3) = values(2)
       ELSE
          WRITE(*,*)' '
          WRITE(*,*)'Error--- Metric_Eigen: one eigen value only'
          WRITE(*,'(a,3E13.5)')'values=',values(1:3)
          CALL Error_STOP ( '--- Geo3D_Matrix_Eigen')
       ENDIF
    ENDIF

    !--- sort eigen values in descent order
    DO i=1,2
       DO j=i+1,3
          IF(ABS(values(j))>ABS(values(i)))THEN
             det=values(j)
             values(j)=values(i)
             values(i)=det
          ENDIF
       ENDDO
    ENDDO

    !--- eigen vectors
    vectors(:,:) = 0.d0
    TINY  = 1.d-4 * ABS(values(1))
    TINY2 = 1.d-6 * values(1) *values(1)
    Itolerance = 1
    det = ABS(values(1)-values(3))/TINY

    IF(det<1.d0)THEN
       !--- three identical eigen values
       vectors(1,1) = 1.d0
       vectors(2,2) = 1.d0
       vectors(3,3) = 1.d0
       RETURN
    ELSE IF(det<1.d1)THEN
       Itolerance = 5
    ELSE IF(det<1.d2)THEN
       Itolerance = 4
    ELSE IF(det<1.d3)THEN
       Itolerance = 3
    ELSE IF(det<1.d4)THEN
       Itolerance = 2
    ENDIF

    fM(:,:) = fMatrix(:,:)

    ktwin = 0
    IF(ABS(values(2)-values(1))<TINY/2.D0)THEN
       ktwin = 1
    ELSE IF(ABS(values(2)-values(3))<TINY/2.D0)THEN
       ktwin = 3
    ENDIF

    DO k=1,3

       IF(ktwin>0 .AND. (k==ktwin .OR. k==2))CYCLE

       fM(1,1) =  fMatrix(1,1) - values(k)
       fM(2,2) =  fMatrix(2,2) - values(k)
       fM(3,3) =  fMatrix(3,3) - values(k)

       i   = 0
       det = 1.d-6 * TINY2
       Loop_i : DO i1=1,3
          i2=iorder(i1+1)
          DO j1=1,3
             j2=iorder(j1+1)
             ad = (fM(i1,j1)*fM(i2,j2) - fM(i1,j2)*fM(i2,j1))
             IF(ABS(ad)>ABS(det))THEN
                det = ad
                i   = i1
                j   = j1
                IF(ABS(det)>1.d5*TINY2) EXIT Loop_i
             ENDIF
          ENDDO
       ENDDO Loop_i

       IF(i>0)THEN
          i1 = i
          i2 = iorder(i+1)
          j1 = j
          j2 = iorder(j+1)
          j  = iorder(j2+1)
          r(i1) = -fM(i1,j)/det
          r(i2) = -fM(i2,j)/det
          vectors(j, k) = 1.d0
          vectors(j1,k) = fM(i2,j2)*r(i1) - fM(i1,j2)*r(i2)
          vectors(j2,k) = fM(i1,j1)*r(i2) - fM(i2,j1)*r(i1)
       ELSE IF(Itolerance>=3)THEN
          WRITE(*,*)'Warning--- Metric3D_Eigen: lower tolerance applied'
          WRITE(*,*)'Itolerance=',Itolerance
          vectors(:,:) = 0.d0
          vectors(1,1) = 1.d0
          vectors(2,2) = 1.d0
          vectors(3,3) = 1.d0
          RETURN
       ELSE
          WRITE(*,*)' '
          WRITE(*,*)'Error---  what happens'
          WRITE(*,'(a,3E20.12)')'eigen values=',values(:)
          WRITE(*,'(a,9E20.12)')'fMatrix=',fMatrix
          CALL Error_STOP ( '--- Geo3D_Matrix_Eigen')
       ENDIF

    ENDDO

    IF(ktwin>0)THEN

       DO k=2,ktwin,ktwin-2
          fM(1,1) =  fMatrix(1,1) - values(k)
          fM(2,2) =  fMatrix(2,2) - values(k)
          fM(3,3) =  fMatrix(3,3) - values(k)

          det = 0.d0
          DO i1=1,3
             DO j1=1,3
                ad = fM(i1,j1)
                IF(ABS(ad)>ABS(det))THEN
                   det = ad
                   i   = i1
                   j   = j1
                ENDIF
             ENDDO
          ENDDO

          IF(k==2)THEN
             j1 = iorder(j+1)
             j2 = iorder(j+2)
             i2 = j        !--- save for the brother
          ELSE
             IF(j==i2)THEN
                j2 = iorder(j+1)
                j1 = iorder(j+2)
             ELSE
                j1 = iorder(j+1)
                j2 = iorder(j+2)
             ENDIF
          ENDIF
          vectors(j1, k) =  1.d0
          vectors(j2, k) =  0.d0
          vectors(j,  k) =  -fM(i,j1) / det

       ENDDO
    ENDIF

    DO k=1,3
       det = vectors(1,k)*vectors(1,k) + vectors(2,k)*vectors(2,k)   &
            + vectors(3,k)*vectors(3,k)
       vectors(:,k) = vectors(:,k) / dsqrt(det)
    ENDDO

    RETURN
  END SUBROUTINE Geo3D_Matrix_Eigen

  !>
  !!    Linear interpolation.
  !!    @param[in]  P1,P2  Position of two points.
  !!    @param[in]  ij =1,2,3  interpolate along X-, Y- or Z-axis respectivel.
  !!    @param[in,out]  P0  input the ij component and output others.
  !<
  SUBROUTINE Geo3D_Linear_Interpolate(P1,P2,P0,ij)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: P1(3), P2(3)
    REAL*8, INTENT(INOUT) :: P0(3)
    REAL*8 :: xl
    INTEGER, INTENT(IN) :: ij
    INTEGER :: i
    xl = (P0(ij)-P1(ij))/(P2(ij)-P1(ij))
    DO i=1,3
       IF(i/=ij)THEN
          p0(i) = P1(i) + xl*(P2(i)-P1(i))
       ENDIF
    ENDDO
    RETURN
  END SUBROUTINE Geo3D_Linear_Interpolate

  !>
  !!   Calculate unit orthogonal directions of a vector.
  !!   @param[in]  v1  an unit vector.
  !!   @param[out] v2,v3  two unit vectors orthogonal to v1
  !!                         and to each other.
  !!           v1-v2-v3 build a right hand coordinate system. 
  !<
  SUBROUTINE Geo3D_BuildOrthogonalAxes(v1,v2,v3)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: v1(3)
    REAL*8, INTENT(OUT) :: v2(3), v3(3)
    INTEGER :: jk, jj, jp

    jk = 1
    IF( ABS(v1(2))<ABS(v1(jk)) ) jk=2
    IF( ABS(v1(3))<ABS(v1(jk)) ) jk=3

    DO jj=1,3
       IF(jj==jk)THEN
          v2(jj) = 0.d0
       ELSE
          jp = 6-jj-jk
          IF(jp>jj)THEN
             v2(jj) = v1(jp)
          ELSE
             v2(jj) = -v1(jp)
          ENDIF
       ENDIF
    ENDDO
    v2(:) = v2(:) / Geo3D_Distance(v2)
    v3(:) = Geo3D_Cross_Product(v1,v2)

  END SUBROUTINE Geo3D_BuildOrthogonalAxes


  !>
  !!   Calculate the position of the projection of a point on a line.
  !!    @param[in] P1,P2  Positions of two end points of a line.
  !!    @param[in]  P0    Position of a point in the 3D space.
  !!    @param[out]  t       <=0  out off end P1. \n
  !!                         >=1  out off end P2. \n
  !!                         else between P1 and P2
  !!    @return  the projecting position of P0 on the line.
  !<
  FUNCTION Geo3D_ProjectOnLine(P1,P2,P0,t) RESULT(P)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: P1(3), P2(3), P0(3)
    REAL*8, INTENT(OUT) :: t
    REAL*8 :: P(3)
    REAL*8 :: dpp(3), dop(3), dd
    dpp = P2 - P1
    dd  = Geo3D_Distance_SQ(dpp)
    dop = P0 - P1
    t   = Geo3D_Dot_Product(dpp,dop) / dd
    P   = P1 + t*dpp
  END FUNCTION Geo3D_ProjectOnLine

  !>
  !!  Fit a straight line  Y = b * X + a by least square method.
  !!    Copy from http://www.dreamincode.net/code/snippet2911.htm
  !!    @param[in]  N     The number of points.
  !!    @param[in]  x,y   coordinates of each point.
  !!    @param[out] a,b   coefficients.
  !<
  SUBROUTINE Geo3D_LineFit_2D(N, x,y,a,b)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: N
    REAL*8, DIMENSION(N), INTENT(IN) :: x,y
    REAL*8, INTENT(OUT) :: a,b
    REAL*8 :: sum_x, sum_y, sum_xx, sum_xy, xx, xy
    INTEGER :: i
    sum_x=0
    sum_y=0
    sum_xx=0
    sum_xy=0
    DO i = 1, N
       sum_x=sum_x+x(i)
       sum_y=sum_y+y(i)
       xx=x(i)*x(i)
       sum_xx=sum_xx+xx
       xy=x(i)*y(i)
       sum_xy=sum_xy+xy
    ENDDO
    a=(-sum_x*sum_xy+sum_xx*sum_y)/(n*sum_xx-sum_x*sum_x)
    b=(-sum_x*sum_y+n*sum_xy)/(n*sum_xx-sum_x*sum_x)

    RETURN
  END SUBROUTINE Geo3D_LineFit_2D

  !>
  !! Fit a straight line  (x,y,z) = P0(:) + t * VT(:) by least square method.
  !!    @param[in]  N     The number of points.
  !!    @param[in]  Pt    coordinates of each point.
  !!    @param[out] VT    The direction of the line.
  !!    @param[out] P0    One point on the line.
  !<
  SUBROUTINE Geo3D_LineFit(N,Pt,VT,P0)
    IMPLICIT NONE
    INTEGER, INTENT(IN)  :: N
    REAL*8,  INTENT(IN)  :: Pt(3,N)
    REAL*8,  INTENT(OUT) :: VT(3), P0(3)
    REAL*8  :: pMin(3), pMax(3)
    REAL*8, DIMENSION(:), POINTER :: x, y
    REAL*8  :: a,b, vts
    INTEGER :: k, k1, k2

    DO k = 1,3
       pMin(k) = MINVAL(Pt(k,1:N))
       pMax(k) = MAXVAL(Pt(k,1:N))
    ENDDO

    k = 1
    IF(pMax(2)-pMin(2)>pMax(k)-pMin(k)) k = 2
    IF(pMax(3)-pMin(3)>pMax(k)-pMin(k)) k = 3

    k1 = MOD(k, 3)+1
    k2 = MOD(k1,3)+1

    ALLOCATE(x(N), y(N))

    x(1:N) = Pt(k,  1:N)
    y(1:N) = Pt(k1, 1:N)
    CALL Geo3D_LineFit_2D(N, x,y,a,b)  
    P0(k)  = x(1)
    P0(k1) = a + b*x(1)
    VT(k)  = 1
    VT(k1) = b

    y(1:N) = Pt(k2, 1:N)  
    CALL Geo3D_LineFit_2D(N, x,y,a,b)  
    P0(k2) = a + b*x(1)
    VT(k2) = b

    vts = DSQRT(VT(1)*VT(1) + VT(2)*VT(2) + VT(3)*VT(3))
    VT(:) = VT(:) / vts

    DEALLOCATE(x, y)

    RETURN
  END SUBROUTINE Geo3D_LineFit


END MODULE Geometry3D


!>
!!
!!  For calculation of Scalars:
!!  Scalar stand for isotropic mapping.
!!
!<
MODULE ScalarCalculator


CONTAINS

  !>
  !!    Interpolate between two Scalars.
  !!    @param[in] Scalar1,Scalar2 the two Scalars.
  !!    @param[in] Model  = 1;  get geometric mean.  \n
  !!                      = 2;  get harmonic mean.   \n
  !!                      = 3;  get arithmetic mean.
  !!    @param[in] t (0<=t<=1; Return Scalar1 if t=0; Return Scalar2 if t=1)
  !<
  FUNCTION Scalar_Interpolate(Scalar1,Scalar2,t,Model) RESULT(Scalar)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: Scalar1, Scalar2, t
    INTEGER, INTENT(IN) :: Model
    REAL*8 :: Scalar

    SELECT CASE(Model)
    CASE(1)
       Scalar = dexp( (1.d0-t)*dlog(Scalar1) + t*dlog(Scalar2) )
    CASE(2)
       Scalar = (1.d0-t) * Scalar1 + t * Scalar2
    CASE(3)
       Scalar = 1.d0 / ( (1.d0-t) / Scalar1 + t / Scalar2 )
    CASE DEFAULT
       WRITE(*,*)' '
       WRITE(*,*)'Error--- : no such case'
       WRITE(*,*)'Model=',Model
       CALL Error_STOP ( '--- Scalar_Interpolate')
    END SELECT

    RETURN
  END FUNCTION Scalar_Interpolate

  !>
  !!    Get the mean of n Mappings.
  !!    @param[in] n  the number of Scalars
  !!    @param[in] Scalars the Scalars
  !!    @param[in] Model   Refer FUNCTION Scalar_Interpolate().
  !<
  FUNCTION Scalar_Mean(n,Scalars,Model) RESULT(Scalar)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: n, Model
    REAL*8,  INTENT(IN)  :: Scalars(*)
    REAL*8  :: Scalar
    INTEGER :: i

    SELECT CASE(Model)
    CASE(1)
       Scalar = Scalars(1)
       DO i=2,n
          Scalar = Scalar * Scalars(i)
       ENDDO
       IF(n==2)THEN
          Scalar = DSQRT(Scalar)
       ELSE IF(n==4)THEN
          Scalar = DSQRT(DSQRT(Scalar))
       ELSE
          Scalar = Scalar**(1.d0/n)
       ENDIF
    CASE(2)
       Scalar = Scalars(1)
       DO i=2,n
          Scalar = Scalar + Scalars(i)
       ENDDO
       Scalar = Scalar / n
    CASE(3)
       Scalar = 1.d0/Scalars(1)
       DO i=2,n
          Scalar = Scalar + 1.d0/Scalars(i)
       ENDDO
       Scalar = n * 1.d0/Scalar
    CASE DEFAULT
       WRITE(*,*)' '
       WRITE(*,*)'Error--- : no such case'
       WRITE(*,*)'Model=',Model
       CALL Error_STOP ( '--- Scalar_Mean')
    END SELECT

    RETURN
  END FUNCTION Scalar_Mean

  !>
  !!    Get the weighted mean of n Mappings.
  !!    make sure that w(1:n)>0 and sum(w(1:n))=1.
  !!    @param[in] n  the number of Scalars
  !!    @param[in] Scalars the Scalars
  !!    @param[in] w      the weights.
  !!    @param[in] Model  Refer FUNCTION Scalar_Interpolate().
  !<
  FUNCTION Scalar_Mean_Weight(n,Scalars,w,Model) RESULT(Scalar)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: n, Model
    REAL*8,  INTENT(IN)  :: Scalars(*),w(*)
    REAL*8  :: Scalar
    INTEGER :: i

    SELECT CASE(Model)
    CASE(1)
       Scalar = 1.d0
       DO i=1,n
          Scalar = Scalar * Scalars(i)**w(i)
       ENDDO
    CASE(2)
       Scalar = 0.d0
       DO i=1,n
          Scalar = Scalar + w(i)*Scalars(i)
       ENDDO
    CASE(3)
       Scalar = 0.d0
       DO i=1,n
          Scalar = Scalar + w(i)/Scalars(i)
       ENDDO
       Scalar = 1.d0/Scalar
    CASE DEFAULT
       WRITE(*,*)' '
       WRITE(*,*)'Error--- : no such case'
       WRITE(*,*)'Model=',Model
       CALL Error_STOP ( '--- Scalar_Mean_Weight')
    END SELECT

    RETURN
  END FUNCTION Scalar_Mean_Weight

  !>
  !!    Get the integral mean of two Scalars.
  !!    i.e. The distance between p1 and p2 with scalar1 and scalar2
  !!         can be calculate by integration :
  !!         d = |p1-p2| / Scalar_Integral(Scalar1,Scalar2,Model).
  !!
  !!    @param[in] Scalar1,Scalar2 the two Scalars.
  !!    @param[in] Model  = 1;  get geometric mean.  \n
  !!                      = 2;  get harmonic mean.   \n
  !!                      = 3;  get arithmetic mean. \n
  !!                      Refer FUNCTION Scalar_Interpolate().
  !<
  FUNCTION Scalar_Integral(Scalar1,Scalar2,Model) RESULT(Scalar)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: Scalar1, Scalar2
    INTEGER, INTENT(IN) :: Model
    REAL*8 :: Scalar

    IF(ABS(Scalar1-Scalar2)<1.d-3 * MAX(Scalar1,Scalar2))THEN
       Scalar = (Scalar2 + Scalar1) / 2.D0
       RETURN
    ENDIF

    SELECT CASE(Model)
    CASE(1)
       Scalar = Scalar1 * Scalar2 * DLOG(Scalar2/Scalar1) / (Scalar2 - Scalar1)
    CASE(2)
       Scalar = (Scalar2 - Scalar1) / DLOG(Scalar2/Scalar1)
    CASE(3)
       Scalar = 2.d0 * Scalar1 * Scalar2 / (Scalar2 + Scalar1)
    CASE DEFAULT
       WRITE(*,*)' '
       WRITE(*,*)'Error--- : no such case'
       WRITE(*,*)'Model=',Model
       CALL Error_STOP ( '--- Scalar_Integral')
    END SELECT

    RETURN
  END FUNCTION Scalar_Integral

END MODULE ScalarCalculator



!             /  a  b  c  \                                       
!             |  b  d  e  |                                       
!             \  c  e  f  /  
!>
!!  Geometry and algebra in Euclidean space based on a 3x3 Metric  
!!                                                                 
!! \f[ \left( \begin{array}{ccc}
!!       a & b & c \\\
!!       b & d & e \\\
!!       c & e & f 
!!             \end{array} \right) \f] 
!!
!!   The Metric saved in a array fMtr(1:6) = \f$ ( a, b, c, d, e, f ) \f$ .
!<
MODULE Metric3D

  USE Geometry3D

CONTAINS
  !>
  !!    Convert stretch date to a Metric.  \n
  !!    Definition of stretch :
  !!        coordinate system rotates an angle of Stretch(4) along the x-axis,
  !!        then rotates an angle of Stretch(5) along the NEW y-axis,
  !!        then rotates an angle of Stretch(6) along the NEW z-axis,
  !!        then stretch with factor Stretch(1:3) along the NEW x,y,z-axises.
  !<
  FUNCTION Metric3D_from_Stretch(Stretch) RESULT(fMtr)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: Stretch(6)
    REAL*8 :: fMtr(6)
    REAL*8 :: a(3,3),sinx,cosx,siny,cosy,sinz,cosz

    sinx = dsin(Stretch(4))
    cosx = dcos(Stretch(4))
    siny = dsin(Stretch(5))
    cosy = dcos(Stretch(5))
    sinz = dsin(Stretch(6))
    cosz = dcos(Stretch(6))

    a(1,1) =  cosy*cosz
    a(2,1) = -cosy*sinz
    a(3,1) =  siny
    a(1,2) =  sinx*siny*cosz +cosx*sinz
    a(2,2) = -sinx*siny*sinz +cosx*cosz
    a(3,2) = -sinx*cosy
    a(1,3) = -cosx*siny*cosz +sinx*sinz
    a(2,3) =  cosx*siny*sinz +sinx*cosz
    a(3,3) =  cosx*cosy

    a(1,:) = a(1,:)/Stretch(1)
    a(2,:) = a(2,:)/Stretch(2)
    a(3,:) = a(3,:)/Stretch(3)

    fMtr(1) = SUM(a(:,1)*a(:,1))
    fMtr(2) = SUM(a(:,1)*a(:,2))
    fMtr(3) = SUM(a(:,1)*a(:,3))
    fMtr(4) = SUM(a(:,2)*a(:,2))
    fMtr(5) = SUM(a(:,2)*a(:,3))
    fMtr(6) = SUM(a(:,3)*a(:,3))

    RETURN
  END FUNCTION Metric3D_from_Stretch

  !>
  !!    Convert a Metric to stretch data.
  !!    Defination of stretch :
  !!        coordinate system rotates an angle of Stretch(4) along the x-axis,
  !!        then rotates an angle of Stretch(5) along the NEW y-axis,
  !!        then rotates an angle of Stretch(6) along the NEW z-axis,
  !!        then stretch with factor Stretch(1:3) along the NEW x,y,z-axises.
  !<
  FUNCTION Metric3D_to_Stretch(fMtr) RESULT(Stretch)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: fMtr(6)
    REAL*8  :: Stretch(6)
    REAL*8  :: a(3,3),v(3),va(3),det,sinx,cosx,siny,sinz,cosz
    INTEGER :: i,j

    CALL Metric3D_eigen(fMtr,v,a)

    !--- For an orthogonal matrix, Transpose = Inverse
    a = Geo3D_Matrix_Transpose(a)

    v(1:3) = 1.d0/dsqrt(v(1:3))
    DO i=1,2
       DO j=i+1,3
          IF(v(j)>v(i))THEN
             det=v(j)
             v(j)=v(i)
             v(i)=det
             va = a(j,:)
             a(j,:) = a(i,:)
             a(i,:) = va
          ENDIF
       ENDDO
    ENDDO

    Stretch(1:3) = v(1:3)

    siny =  a(3,1)
    Stretch(5)   = dasin(siny)
    IF( (1.d0-ABS(siny))>1.d-8 )THEN
       sinx = -a(3,2)
       cosx =  a(3,3)
       sinz = -a(2,1)
       cosz =  a(1,1)
       Stretch(4) = datan2(sinx,cosx)
       Stretch(6) = datan2(sinz,cosz)
    ELSE
       !--- only have solution of x+z, choose z=0
       sinx =  a(1,2)
       cosx = -a(1,3)
       Stretch(4) = datan2(sinx,cosx)
       Stretch(6) = 0.d0
    ENDIF

    RETURN
  END FUNCTION Metric3D_to_Stretch

  !>
  !!    Calculate the square of distance between two points in Euclidean space.
  !!    @param[in] P1,P2   coordinates of the two points.
  !!    @param[in] fMtr  the  Metric.
  !!    @return       the square of distance based on the Metric of fMtr.
  !<
  FUNCTION Metric3D_Distance_SQ(P1,P2,fMtr) RESULT(Dist_SQ)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: P1(3), P2(3), fMtr(6)
    REAL*8  :: Dist_SQ
    REAL*8  :: dx, dy, dz
    dx = P2(1)-P1(1)
    dy = P2(2)-P1(2)
    dz = P2(3)-P1(3)
    Dist_SQ =   fMtr(1) *dx*dx + 2.D0* fMtr(2) *dx*dy    &
         + fMtr(4) *dy*dy + 2.D0* fMtr(5) *dy*dz    &
         + fMtr(6) *dz*dz + 2.D0* fMtr(3) *dz*dx
    RETURN
  END FUNCTION Metric3D_Distance_SQ

  !>
  !!    Calculate the distance between two points in Euclidean space.
  !!    @param[in] P1,P2   coordinates of the two points.
  !!    @param[in] fMtr  the  mapping Metric.
  !!    @return          the distance based on the mapping Metric of fMtr.
  !<
  FUNCTION Metric3D_Distance(P1,P2,fMtr) RESULT(Dist)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: P1(3), P2(3), fMtr(6)
    REAL*8  :: Dist
    REAL*8  :: dx, dy, dz
    dx = P2(1)-P1(1)
    dy = P2(2)-P1(2)
    dz = P2(3)-P1(3)
    Dist = dsqrt(  fMtr(1) *dx*dx + 2.D0* fMtr(2) *dx*dy    &
         + fMtr(4) *dy*dy + 2.D0* fMtr(5) *dy*dz    &
         + fMtr(6) *dz*dz + 2.D0* fMtr(3) *dz*dx  )
    RETURN
  END FUNCTION Metric3D_Distance

  !>
  !!    Calculate the Dot Product of two vectors V1 & V2.
  !!    @param[in] V1,V2  the two vectors.
  !!    @param[in] fMtr   the  Metric.
  !!    @return    the the Dot Product of two vectors.
  !<
  FUNCTION Metric3D_Dot_Product(V1,V2,fMtr) RESULT(prod)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: V1(3), V2(3), fMtr(6)
    REAL*8  :: prod
    prod = V2(1) * (V1(1)*fMtr(1) + V1(2)*fMtr(2) + V1(3)*fMtr(3))   &
         + V2(2) * (V1(1)*fMtr(2) + V1(2)*fMtr(4) + V1(3)*fMtr(5))   &
         + V2(3) * (V1(1)*fMtr(3) + V1(2)*fMtr(5) + V1(3)*fMtr(6))
    RETURN
  END FUNCTION Metric3D_Dot_Product

  !>
  !!    Calculate the Product of two metrics.
  !!    The output is a 3*3 matrix.
  !<
  FUNCTION Metric3D_Product(fMtr1,fMtr2) RESULT(prod)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: fMtr1(6), fMtr2(6)
    REAL*8  :: prod(3,3)
    prod(1,1) = fMtr1(1)*fMtr2(1) + fMtr1(2)*fMtr2(2) + fMtr1(3)*fMtr2(3)
    prod(1,2) = fMtr1(1)*fMtr2(2) + fMtr1(2)*fMtr2(4) + fMtr1(3)*fMtr2(5)
    prod(1,3) = fMtr1(1)*fMtr2(3) + fMtr1(2)*fMtr2(5) + fMtr1(3)*fMtr2(6)
    prod(2,1) = fMtr1(2)*fMtr2(1) + fMtr1(4)*fMtr2(2) + fMtr1(5)*fMtr2(3)
    prod(2,2) = fMtr1(2)*fMtr2(2) + fMtr1(4)*fMtr2(4) + fMtr1(5)*fMtr2(5)
    prod(2,3) = fMtr1(2)*fMtr2(3) + fMtr1(4)*fMtr2(5) + fMtr1(5)*fMtr2(6)
    prod(3,1) = fMtr1(3)*fMtr2(1) + fMtr1(5)*fMtr2(2) + fMtr1(6)*fMtr2(3)
    prod(3,2) = fMtr1(3)*fMtr2(2) + fMtr1(5)*fMtr2(4) + fMtr1(6)*fMtr2(5)
    prod(3,3) = fMtr1(3)*fMtr2(3) + fMtr1(5)*fMtr2(5) + fMtr1(6)*fMtr2(6)
  END FUNCTION Metric3D_Product

  !>
  !!    Calculate the inverse of a metric.
  !<
  FUNCTION Metric3D_Inverse(fMtr) RESULT(rev)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: fMtr(6)
    REAL*8  :: rev(6), det
    rev(1) = fMtr(4)*fMtr(6) - fMtr(5)*fMtr(5)
    rev(2) = fMtr(5)*fMtr(3) - fMtr(6)*fMtr(2)
    rev(3) = fMtr(2)*fMtr(5) - fMtr(3)*fMtr(4)
    rev(4) = fMtr(6)*fMtr(1) - fMtr(3)*fMtr(3)
    rev(5) = fMtr(3)*fMtr(2) - fMtr(1)*fMtr(5)
    rev(6) = fMtr(1)*fMtr(4) - fMtr(2)*fMtr(2)
    det =  fMtr(1)*rev(1) + fMtr(2)*rev(2) + fMtr(3)*rev(3)
    rev(:) = rev(:) / det
    RETURN
  END FUNCTION Metric3D_Inverse

  !>
  !!    Calculate the eigen values and vectors of a Metric.
  !!    @param[in]   fMtr   the Metric
  !!    @param[out]  values         three eigen values.
  !!    @param[out]  vectors (:,i)  eigen vector associated with values(i), i=1:3.
  !<
  SUBROUTINE Metric3D_Eigen(fMtr, values, vectors)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: fMtr(6)
    REAL*8, INTENT(OUT) :: values(3), vectors(3,3)
    REAL*8 :: fM(3,3), a0,a1,a2, det, ad, TINY,TINY2, r(3)
    INTEGER :: nroot, i,j,k,kk,i1,i2,j1,j2,ktwin,ksingle, Itolerance
    INTEGER :: iorder(5) = (/1,2,3,1,2/)

    !--- calculate eigen values by solving a cubic equation
    a2 = -(fMtr(1) + fMtr(4) + fMtr(6))
    a1 =   fMtr(1)*fMtr(4) + fMtr(1)*fMtr(6) + fMtr(4)*fMtr(6)   &
         - fMtr(2)*fMtr(2) - fMtr(3)*fMtr(3) - fMtr(5)*fMtr(5)
    a0 =   fMtr(2)*fMtr(2)*fMtr(6)         &
         + fMtr(3)*fMtr(3)*fMtr(4)         &
         + fMtr(5)*fMtr(5)*fMtr(1)         &
         - fMtr(2)*fMtr(5)*fMtr(3)*2.d0    &
         - fMtr(1)*fMtr(4)*fMtr(6)
    CALL Geo3D_Cubic_Equation(a2,a1,a0,values,nroot)

    IF(nroot==1)THEN
       IF(ABS(values(3))<1.e-6*MAX(1.d0,ABS(values(2))))THEN
          !--- ignor imagine part
          values(3) = values(2)
       ELSE
          WRITE(*,*)' '
          WRITE(*,*)'Error--- Metric_Eigen: one eigen value only'
          WRITE(*,'(a,3E13.5)')'values=',values(1:3)
          CALL Error_STOP ( '--- Metric3D_Eigen')
       ENDIF
    ENDIF

    !--- sort eigen values in descent order
    DO i=1,2
       DO j=i+1,3
          IF(ABS(values(j))>ABS(values(i)))THEN
             det       = values(j)
             values(j) = values(i)
             values(i) = det
          ENDIF
       ENDDO
    ENDDO

    !--- eigen vectors
    vectors(:,:) = 0.d0
    TINY  = 1.d-5 * ABS(values(1))
    TINY2 = TINY*TINY
    Itolerance = 1
    det = ABS(values(1)-values(3))/TINY

    IF(det<1.d0)THEN
       !--- three identical eigen values
       vectors(1,1) = 1.d0
       vectors(2,2) = 1.d0
       vectors(3,3) = 1.d0
       RETURN
    ELSE IF(det<1.d1)THEN
       Itolerance = 5
    ELSE IF(det<1.d2)THEN
       Itolerance = 4
    ELSE IF(det<1.d3)THEN
       Itolerance = 3
    ELSE IF(det<1.d4)THEN
       Itolerance = 2
    ENDIF

    fM(1,1:3) = (/ fMtr(1), fMtr(2), fMtr(3) /)
    fM(2,1:3) = (/ fMtr(2), fMtr(4), fMtr(5) /)
    fM(3,1:3) = (/ fMtr(3), fMtr(5), fMtr(6) /)

    ktwin = 0

    !--- for a sysmetric matrix, the vectors are orthogonal to each other,
    !    so we find out two of them first.
    DO k=1,3,2

       IF(ABS(values(k)-values(2))<TINY/2.D0)THEN
          ktwin = k
          CYCLE
       ENDIF

       fM(1,1) =  fMtr(1) - values(k)
       fM(2,2) =  fMtr(4) - values(k)
       fM(3,3) =  fMtr(6) - values(k)

       i   = 0
       det = 1.d-6 * TINY2
       Loop_i : DO i1=1,3
          i2=iorder(i1+1)
          DO j1=1,3
             j2=iorder(j1+1)
             ad = (fM(i1,j1)*fM(i2,j2) - fM(i1,j2)*fM(i2,j1))
             IF(ABS(ad)>ABS(det))THEN
                det = ad
                i   = i1
                j   = j1
                IF(ABS(det)>1.d5*TINY2) EXIT Loop_i
             ENDIF
          ENDDO
       ENDDO Loop_i

       IF(i>0)THEN
          i1 = i
          i2 = iorder(i+1)
          j1 = j
          j2 = iorder(j+1)
          j  = iorder(j2+1)
          r(i1) = -fM(i1,j)/det
          r(i2) = -fM(i2,j)/det
          vectors(j, k) = 1.d0
          vectors(j1,k) = fM(i2,j2)*r(i1) - fM(i1,j2)*r(i2)
          vectors(j2,k) = fM(i1,j1)*r(i2) - fM(i2,j1)*r(i1)
       ELSE IF(Itolerance>=3)THEN
          WRITE(*,*)'Warning--- Metric3D_Eigen: lower tolerance applied'
          WRITE(*,*)'Itolerance=',Itolerance
          vectors(:,:) = 0.d0
          vectors(1,1) = 1.d0
          vectors(2,2) = 1.d0
          vectors(3,3) = 1.d0
          RETURN
       ELSE
          WRITE(*,*)' '
          WRITE(*,*)'Error--- Can not get eigen vectors'
          WRITE(*,*)'fMtr=',fMtr
          WRITE(*,*)'eigen values=',values(:)
          WRITE(*,*)'tolerance index=',Itolerance
          CALL Error_STOP ( '--- Metric3D_Eigen')
       ENDIF

       det =  vectors(1,k)*vectors(1,k) + vectors(2,k)*vectors(2,k)   &
            + vectors(3,k)*vectors(3,k)
       vectors(:,k) = vectors(:,k) / dsqrt(det)

    ENDDO

    IF(ktwin>0)THEN

       ksingle = 4-ktwin

       det = 0.d0
       DO i=1,3
          IF(ABS(vectors(i,ksingle))>det)THEN
             det = ABS(vectors(i,ksingle))
             kk  = i
          ENDIF
       ENDDO
       i = iorder(kk+1)
       j = iorder(kk+2)
       vectors(i, ktwin) =  vectors(kk,ksingle)
       vectors(kk,ktwin) = -vectors(i, ksingle)
       vectors(j, ktwin) = 0.d0
       det =  vectors(i, ktwin)*vectors(i, ktwin)    &
            + vectors(kk,ktwin)*vectors(kk,ktwin)
       vectors(:, ktwin) = vectors(:, ktwin) / dsqrt(det)

    ENDIF

    vectors(1,2) = vectors(2,1)*vectors(3,3) - vectors(3,1)*vectors(2,3)
    vectors(2,2) = vectors(3,1)*vectors(1,3) - vectors(1,1)*vectors(3,3)
    vectors(3,2) = vectors(1,1)*vectors(2,3) - vectors(2,1)*vectors(1,3)

    RETURN
  END SUBROUTINE Metric3D_Eigen

  !>
  !!    Calculate the eigen values of a Metric.
  !!    @param[in]   fMtr   the Metric
  !!    @param[out]  values the three eigen values.
  !<
  SUBROUTINE Metric3D_EigenValue(fMtr, values)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: fMtr(6)
    REAL*8, INTENT(OUT) :: values(3)
    REAL*8 ::  a0,a1,a2, det
    INTEGER :: nroot, i,j

    !--- calculate eigen values by solving a cubic equation
    a2 = -(fMtr(1) + fMtr(4) + fMtr(6))
    a1 =   fMtr(1)*fMtr(4) + fMtr(1)*fMtr(6) + fMtr(4)*fMtr(6)   &
         - fMtr(2)*fMtr(2) - fMtr(3)*fMtr(3) - fMtr(5)*fMtr(5)
    a0 =   fMtr(2)*fMtr(2)*fMtr(6)         &
         + fMtr(3)*fMtr(3)*fMtr(4)         &
         + fMtr(5)*fMtr(5)*fMtr(1)         &
         - fMtr(2)*fMtr(5)*fMtr(3)*2.d0    &
         - fMtr(1)*fMtr(4)*fMtr(6)
    CALL Geo3D_Cubic_Equation(a2,a1,a0,values,nroot)

    IF(nroot==1)THEN
       IF(ABS(values(3))<1.e-6*MAX(1.d0,ABS(values(2))))THEN
          !--- ignor imagine part
          values(3) = values(2)
       ELSE
          WRITE(*,*)' '
          WRITE(*,*)'Error--- Metric_EigenValue: one eigen value only'
          WRITE(*,'(a,3E13.5)')'values=',values(1:3)
          CALL Error_STOP ( '--- Metric3D_EigenValue')
       ENDIF
    ENDIF

    !--- sort eigen values in descent order
    DO i=1,2
       DO j=i+1,3
          IF(ABS(values(j))>ABS(values(i)))THEN
             det       = values(j)
             values(j) = values(i)
             values(i) = det
          ENDIF
       ENDDO
    ENDDO

    RETURN
  END SUBROUTINE Metric3D_EigenValue

  !>
  !!    Calculate the eigen values and vectors of a Metric (by iteration).
  !!      ( From Numerical recipe )
  !!    @param[in]   fMtr   the Metric
  !!    @param[out]  values        three eigen values.
  !!    @param[out]  vectors (:,i)  eigen vector associated with D(i), i=1:3.
  !<
  SUBROUTINE Metric3D_Eigen_Iter(fMtr, values, vectors)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: fMtr(6)
    REAL*8, INTENT(OUT) :: values(3), vectors(3,3)
    REAL*8  :: a(3,3), b(3), z(3)
    REAL*8  :: sm, tresh, g, h, c, s, tau, t, theta
    INTEGER :: ip, iq, i, j, nrot
    INTEGER :: n, niter
    PARAMETER (n = 3, niter = 50)

    a(1,1:3) = (/ fMtr(1), fMtr(2), fMtr(3) /)
    a(2,1:3) = (/ fMtr(2), fMtr(4), fMtr(5) /)
    a(3,1:3) = (/ fMtr(3), fMtr(5), fMtr(6) /)

    DO ip=1,n
       DO iq=1,n
          vectors(ip,iq) = 0.
       ENDDO
       vectors(ip,ip) = 1.
    ENDDO
    DO ip=1,n
       b(ip) = a(ip,ip)
       values(ip) = b(ip)
       z(ip) = 0.
    ENDDO
    nrot = 0
    DO i=1,niter
       sm = 0.
       DO ip=1,n-1
          DO iq=ip+1,n
             sm = sm+DABS(a(ip,iq))
          ENDDO
       ENDDO
       IF(sm==0.)RETURN
       IF(i<4)THEN
          tresh = 0.2*sm/n**2
       ELSE
          tresh = 0.
       ENDIF
       DO ip=1,n-1
          DO iq=ip+1,n
             g = 100.*DABS(a(ip,iq))
             IF((i>4).AND.(DABS(values(ip))+g==DABS(values(ip)))   &
                  .AND.(DABS(values(iq))+g==DABS(values(iq))))THEN
                a(ip,iq) = 0.
             ELSE IF(DABS(a(ip,iq))>tresh)THEN
                h = values(iq)-values(ip)
                IF(DABS(h)+g==DABS(h))THEN
                   t = a(ip,iq)/h
                ELSE
                   theta = 0.5*h/a(ip,iq)
                   t = 1./(DABS(theta)+DSQRT(1.+theta**2))
                   IF(theta<0.) t = -t
                ENDIF
                c        = 1./DSQRT(1+t**2)
                s        = t*c
                tau      = s/(1.+c)
                h        = t*a(ip,iq)
                z(ip)    = z(ip)-h
                z(iq)    = z(iq)+h
                values(ip)    = values(ip)-h
                values(iq)    = values(iq)+h
                a(ip,iq) = 0.
                DO j=1,ip-1
                   g       = a(j,ip)
                   h       = a(j,iq)
                   a(j,ip) = g-s*(h+g*tau)
                   a(j,iq) = h+s*(g-h*tau)
                ENDDO
                DO j=ip+1,iq-1
                   g       = a(ip,j)
                   h       = a(j,iq)
                   a(ip,j) = g-s*(h+g*tau)
                   a(j,iq) = h+s*(g-h*tau)
                ENDDO
                DO j=iq+1,n
                   g       = a(ip,j)
                   h       = a(iq,j)
                   a(ip,j) = g-s*(h+g*tau)
                   a(iq,j) = h+s*(g-h*tau)
                ENDDO
                DO j=1,n
                   g       = vectors(j,ip)
                   h       = vectors(j,iq)
                   vectors(j,ip) = g-s*(h+g*tau)
                   vectors(j,iq) = h+s*(g-h*tau)
                ENDDO
                nrot = nrot+1
             ENDIF
          ENDDO
       ENDDO
       DO ip=1,n
          b(ip) = b(ip)+z(ip)
          values(ip) = b(ip)
          z(ip) = 0.
       ENDDO
    ENDDO

    WRITE(*,*)' '
    WRITE(*,*)'Error--- Can not get eigen vectors, number of iterations exceeded'
    WRITE(*,*)'fMtr=',fMtr
    CALL Error_STOP ( '--- Metric3D_Eigen_Iter')

  END SUBROUTINE Metric3D_Eigen_Iter

  !>
  !!    Interpolate between two metrics with factor t.
  !!    @param[in] fMtr1,fMtr2  the two Metrics.
  !!    @param[in] Model  = 1;  by using simultaneous matrix reduction
  !!                        Ref: H. Borouchaki, Finite Elements in Analysis
  !!                        and Design 25 (1997) 61-83. \n
  !!                  = 2;  by using a matrix power -1. \n
  !!                  = 3;  by using a matrix power  1. \n
  !!    @param[in] t  factor t (0<=t<=1; Return fMtr1 if t=0; Return fMtr2 if t=1).
  !<
  FUNCTION Metric3D_Interpolate(fMtr1,fMtr2,t,Model) RESULT(fMtr)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: fMtr1(6), fMtr2(6), t
    INTEGER, INTENT(IN) :: Model
    REAL*8 :: fMtr(6)
    REAL*8 :: f(6), f1(6), f2(6), a(3,3), v(3), vt(3,3)
    REAL*8 :: v1,v2,v3,u1,u2,u3, p0(3)

    SELECT CASE(Model)

    CASE(1)

       f = Metric3D_Inverse(fMtr1)
       a = Metric3D_Product(f,fMtr2)

       CALL Geo3D_Matrix_Eigen(a,v,vt)

       IF(MIN(ABS(v(1)-v(2)), ABS(v(1)-v(3)), ABS(v(2)-v(3)))<1.d-4*ABS(v(1)))THEN
          !--- two metric share the same direction, or, one or two is sphere
          CALL Metric3D_Eigen(fMtr1,v,vt)
          IF(MAX(ABS(v(1)-v(2)), ABS(v(1)-v(3)), ABS(v(2)-v(3)))<1.d-4*ABS(v(1)))THEN
             !--- fMtr1 is a sphere
             CALL Metric3D_Eigen(fMtr2,v,vt)
          ENDIF
       ENDIF

       a  = Geo3D_Matrix_Inverse(vt)

       p0 = 0.d0
       v1 = 1.d0 / Metric3D_Distance(p0,vt(:,1),fMtr1)
       v2 = 1.d0 / Metric3D_Distance(p0,vt(:,2),fMtr1)
       v3 = 1.d0 / Metric3D_Distance(p0,vt(:,3),fMtr1)
       u1 = 1.d0 / Metric3D_Distance(p0,vt(:,1),fMtr2)
       u2 = 1.d0 / Metric3D_Distance(p0,vt(:,2),fMtr2)
       u3 = 1.d0 / Metric3D_Distance(p0,vt(:,3),fMtr2)
       v1 = v1 + (u1-v1)*t
       v2 = v2 + (u2-v2)*t
       v3 = v3 + (u3-v3)*t
       u1 = 1.d0 / (v1*v1)
       u2 = 1.d0 / (v2*v2)
       u3 = 1.d0 / (v3*v3)

       fMtr(1) = a(1,1)*a(1,1)*u1 + a(2,1)*a(2,1)*u2 + a(3,1)*a(3,1)*u3
       fMtr(2) = a(1,1)*a(1,2)*u1 + a(2,1)*a(2,2)*u2 + a(3,1)*a(3,2)*u3
       fMtr(3) = a(1,1)*a(1,3)*u1 + a(2,1)*a(2,3)*u2 + a(3,1)*a(3,3)*u3
       fMtr(4) = a(1,2)*a(1,2)*u1 + a(2,2)*a(2,2)*u2 + a(3,2)*a(3,2)*u3
       fMtr(5) = a(1,2)*a(1,3)*u1 + a(2,2)*a(2,3)*u2 + a(3,2)*a(3,3)*u3
       fMtr(6) = a(1,3)*a(1,3)*u1 + a(2,3)*a(2,3)*u2 + a(3,3)*a(3,3)*u3

    CASE(2)

       f1 = Metric3D_Inverse(fMtr1)
       f2 = Metric3D_Inverse(fMtr2)
       f1 = (1.d0-t)*f1 + t*f2
       fMtr = Metric3D_Inverse(f1)

    CASE(3)

       fMtr = (1.d0-t)*fMtr1 + t*fMtr2

    CASE DEFAULT
       WRITE(*,*)' '
       WRITE(*,*)'Error--- : no such case'
       WRITE(*,*)'Model=',Model
       CALL Error_STOP ( '--- Metric3D_Interpolate')

    END SELECT

    RETURN
  END FUNCTION Metric3D_Interpolate

  !>
  !!    Get the mean of n Metrics.
  !!    @param[in] n  the number of Metrics.
  !!    @param[in] fMtrs  the  Metrics.
  !!    @param[in] Model    refer Metric3D_Interpolate.
  !<
  FUNCTION Metric3D_Mean(n,fMtrs,Model) RESULT(fMtr)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: n, Model
    REAL*8,  INTENT(IN)  :: fMtrs(6,*)
    REAL*8  :: fMtr(6)
    INTEGER :: i
    REAL*8  :: fMtr1(6), t

    SELECT CASE(Model)

    CASE(1)

       fMtr(:) = fMtrs(:,1)
       DO i=2,n
          t = 1.d0/i
          fMtr  = Metric3D_Interpolate(fMtr, fMtrs(:,n), t, Model)
       ENDDO

    CASE(2)

       t = 1.d0/n
       fMtr(:) = 0.d0
       DO i=1,n
          fMtr1 = Metric3D_Inverse(fMtrs(:,n))
          fMtr  = fMtr + t * fMtr1
       ENDDO
       fMtr = Metric3D_Inverse(fMtr)

    CASE(3)

       t = 1.d0/n
       fMtr(:) = 0.d0
       DO i=1,n
          fMtr  = fMtr + t * fMtrs(:,n)
       ENDDO

    CASE DEFAULT

       WRITE(*,*)' '
       WRITE(*,*)'Error--- : no such case'
       WRITE(*,*)'Model=',Model
       CALL Error_STOP ( '--- Metric3D_Mean')

    END SELECT

    RETURN
  END FUNCTION Metric3D_Mean

  !>
  !!    Intersect between two metrics.
  !!    Ref: H. Borouchaki, Finite Elements in Analysis and Design
  !!                         25 (1997) 61-83.
  !<
  FUNCTION Metric3D_Intersect(fMtr1,fMtr2) RESULT(fMtr)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: fMtr1(6), fMtr2(6)
    REAL*8 :: fMtr(6)
    REAL*8 :: f(6), a(3,3), v(3), vt(3,3), v1,v2,v3,u1,u2,u3, p0(3)

    f = Metric3D_Inverse(fMtr1)
    a = Metric3D_Product(f,fMtr2)

    CALL Geo3D_Matrix_Eigen(a,v,vt)

    IF(MIN(ABS(v(1)-v(2)), ABS(v(1)-v(3)), ABS(v(2)-v(3)))<1.d-4*ABS(v(1)))THEN
       !--- two metric share the same direction, or, one or two is sphere
       CALL Metric3D_Eigen(fMtr1,v,vt)
       IF(MAX(ABS(v(1)-v(2)), ABS(v(1)-v(3)), ABS(v(2)-v(3)))<1.d-4*ABS(v(1)))THEN
          !--- fMtr1 is a sphere
          CALL Metric3D_Eigen(fMtr2,v,vt)
       ENDIF
    ENDIF

    a  = Geo3D_Matrix_Inverse(vt)

    p0 = 0.d0
    v1 = Metric3D_Distance_SQ(p0,vt(:,1),fMtr1)
    v2 = Metric3D_Distance_SQ(p0,vt(:,2),fMtr1)
    v3 = Metric3D_Distance_SQ(p0,vt(:,3),fMtr1)
    u1 = Metric3D_Distance_SQ(p0,vt(:,1),fMtr2)
    u2 = Metric3D_Distance_SQ(p0,vt(:,2),fMtr2)
    u3 = Metric3D_Distance_SQ(p0,vt(:,3),fMtr2)

    u1 = MAX(v1,u1)
    u2 = MAX(v2,u2)
    u3 = MAX(v3,u3)

    fMtr(1) = a(1,1)*a(1,1)*u1 + a(2,1)*a(2,1)*u2 + a(3,1)*a(3,1)*u3
    fMtr(2) = a(1,1)*a(1,2)*u1 + a(2,1)*a(2,2)*u2 + a(3,1)*a(3,2)*u3
    fMtr(3) = a(1,1)*a(1,3)*u1 + a(2,1)*a(2,3)*u2 + a(3,1)*a(3,3)*u3
    fMtr(4) = a(1,2)*a(1,2)*u1 + a(2,2)*a(2,2)*u2 + a(3,2)*a(3,2)*u3
    fMtr(5) = a(1,2)*a(1,3)*u1 + a(2,2)*a(2,3)*u2 + a(3,2)*a(3,3)*u3
    fMtr(6) = a(1,3)*a(1,3)*u1 + a(2,3)*a(2,3)*u2 + a(3,3)*a(3,3)*u3

    RETURN
  END FUNCTION Metric3D_Intersect


  !>
  !!    Intersect between a metrix and a scalar.
  !<
  FUNCTION Metric3D_ScalarIntersect(fMtr1,Scalar) RESULT(fMtr)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: fMtr1(6), Scalar
    REAL*8 :: fMtr(6)
    REAL*8 :: a(3,3), v(3), vt(3,3), u1,u2,u3, s

    CALL Metric3D_Eigen(fMtr1,v,vt)

    a  = Geo3D_Matrix_Inverse(vt)

    s  = 1.d0 / Scalar**2
    u1 = MAX(v(1),s)
    u2 = MAX(v(2),s)
    u3 = MAX(v(3),s)

    fMtr(1) = a(1,1)*a(1,1)*u1 + a(2,1)*a(2,1)*u2 + a(3,1)*a(3,1)*u3
    fMtr(2) = a(1,1)*a(1,2)*u1 + a(2,1)*a(2,2)*u2 + a(3,1)*a(3,2)*u3
    fMtr(3) = a(1,1)*a(1,3)*u1 + a(2,1)*a(2,3)*u2 + a(3,1)*a(3,3)*u3
    fMtr(4) = a(1,2)*a(1,2)*u1 + a(2,2)*a(2,2)*u2 + a(3,2)*a(3,2)*u3
    fMtr(5) = a(1,2)*a(1,3)*u1 + a(2,2)*a(2,3)*u2 + a(3,2)*a(3,3)*u3
    fMtr(6) = a(1,3)*a(1,3)*u1 + a(2,3)*a(2,3)*u2 + a(3,3)*a(3,3)*u3

    RETURN
  END FUNCTION Metric3D_ScalarIntersect

END MODULE Metric3D



!>
!!
!!  Geometry and algebra of a 3x3 Mapping matrix.  \n
!!     A mapping matrix can be expressed as M = AD
!!         where A is a positive diagonal matrix (stretch)
!!           and D is a orthogonal matrix (rotation) with det(D)>0.
!!
!<
MODULE Mapping3D

  USE Metric3D
  !-- module Geometry3D is used in module Metric3D

CONTAINS

  !>
  !!    A mapping matrix can be expressed as M = AD,
  !!        where A is a positive diagonal matrix (stretch)
  !!          and D is a orthogonal matrix (rotation).
  !!    This function return an error for non-orthogonal mapping matrix.
  !<
  FUNCTION Mapping3D_Error(fMap) RESULT(E)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: fMap(3,3)
    REAL*8  :: E
    REAL*8  :: e1, e2, e3, Shrink(3)
    INTEGER :: i

    Shrink(1:3) = fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2
    e1 = 0.d0
    e2 = 0.d0
    e3 = 0.d0
    DO i=1,3
       e1 = e1 + fMap(1,i) * fMap(2,i)
       e2 = e2 + fMap(2,i) * fMap(3,i)
       e3 = e3 + fMap(3,i) * fMap(1,i)
    ENDDO
    e1 = e1 / dsqrt( Shrink(1) * Shrink(2) )
    e2 = e2 / dsqrt( Shrink(2) * Shrink(3) )
    e3 = e3 / dsqrt( Shrink(3) * Shrink(1) )
    E  = MAX( ABS(e1), ABS(e2), ABS(e3) )

    RETURN
  END FUNCTION Mapping3D_Error

  !>
  !!    Create a mapping matrix M from A and D (M = AD),
  !!        where A is a positive diagonal matrix (stretch)
  !!          and D is a orthogonal matrix (rotation).
  !!      A(i) is 1/gridsize on direction D(i,:).
  !<
  SUBROUTINE Mapping3D_Compose(A,D,fMap)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: A(3), D(3,3)
    REAL*8, INTENT(OUT) :: fMap(3,3)
    INTEGER :: i
    DO i=1,3
       fMap(i,:) = D(i,:) * A(i)
    ENDDO
    RETURN
  END SUBROUTINE Mapping3D_Compose

  !>
  !!    Decompose a mapping matrix M to A and D (M = AD),
  !!        where A is a positive diagonal matrix (stretch)
  !!          and D is a orthogonal matrix (rotation).
  !!      A(i) is 1/gridsize in the direction D(i,:).
  !<
  SUBROUTINE Mapping3D_Decompose(fMap,A,D)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: fMap(3,3)
    REAL*8, INTENT(OUT) :: A(3), D(3,3)
    INTEGER :: i
    DO i=1,3
       A(i) = dsqrt( fMap(i,1)**2 + fMap(i,2)**2 + fMap(i,3)**2 )
       D(i,:) = fMap(i,:) / A(i)
    ENDDO
    RETURN
  END SUBROUTINE Mapping3D_Decompose

  !>
  !!    Create a mapping from stretched data.       \n
  !!    Defination of stretch :
  !!        coordinate system rotates an angle of Stretch(4) along the x-axis,
  !!        then rotates an angle of Stretch(5) along the NEW y-axis,
  !!        then rotates an angle of Stretch(6) along the NEW z-axis,
  !!        then stretch with factor Stretch(1:3) along the NEW x,y,z-axises.
  !<
  FUNCTION Mapping3D_from_Stretch(Stretch) RESULT(fMap)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: Stretch(6)
    REAL*8 :: fMap(3,3)
    REAL*8 :: sinx,cosx,siny,cosy,sinz,cosz

    sinx = dsin(Stretch(4))
    cosx = dcos(Stretch(4))
    siny = dsin(Stretch(5))
    cosy = dcos(Stretch(5))
    sinz = dsin(Stretch(6))
    cosz = dcos(Stretch(6))

    fMap(1,1) =  cosy*cosz
    fMap(2,1) = -cosy*sinz
    fMap(3,1) =  siny
    fMap(1,2) =  sinx*siny*cosz +cosx*sinz
    fMap(2,2) = -sinx*siny*sinz +cosx*cosz
    fMap(3,2) = -sinx*cosy
    fMap(1,3) = -cosx*siny*cosz +sinx*sinz
    fMap(2,3) =  cosx*siny*sinz +sinx*cosz
    fMap(3,3) =  cosx*cosy

    fMap(1,:) = fMap(1,:)/Stretch(1)
    fMap(2,:) = fMap(2,:)/Stretch(2)
    fMap(3,:) = fMap(3,:)/Stretch(3)

    RETURN
  END FUNCTION Mapping3D_from_Stretch

  !>
  !!    Convert a Mapping to stretch data.  \n
  !!    Defination of stretch :
  !!        coordinate system rotates an angle of Stretch(4) along the x-axis,
  !!        then rotates an angle of Stretch(5) along the NEW y-axis,
  !!        then rotates an angle of Stretch(6) along the NEW z-axis,
  !!        then stretch with factor Stretch(1:3) along the NEW x,y,z-axises.
  !<
  FUNCTION Mapping3D_to_Stretch(fMap) RESULT(Stretch)
    IMPLICIT NONE
    REAL*8 , PARAMETER  :: PI = 3.141592653589793D0
    REAL*8, INTENT(IN)  :: fMap(3,3)
    REAL*8  :: Stretch(6)
    REAL*8  :: sinx,cosx, siny,cosy, sinz,cosz

    Stretch(1:3) = 1.d0 / dsqrt( fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2 )

    siny =  fMap(3,1) * Stretch(3)
    Stretch(5)   = dasin(siny)
    IF( (1.d0-ABS(siny))>1.d-8 )THEN
       sinx = -fMap(3,2)
       cosx =  fMap(3,3)
       sinz = -fMap(2,1) * Stretch(2)
       cosz =  fMap(1,1) * Stretch(1)
       Stretch(4)   = datan2(sinx,cosx)
       Stretch(6)   = datan2(sinz,cosz)

       cosy = dcos(Stretch(5))
       cosz = dcos(Stretch(6))
       IF(ABS(cosz)>0.01)THEN
          IF( cosy*cosz*fMap(1,1)<0 ) Stretch(5) = PI - Stretch(5)
       ELSE
          sinz = dsin(Stretch(6))
          IF(-cosy*sinz*fMap(2,1)<0 ) Stretch(5) = PI - Stretch(5)
       ENDIF
    ELSE
       !--- only have solution of x+z, choose z=0
       sinx =  fMap(1,2)
       cosx = -fMap(1,3)
       Stretch(4)   = datan2(sinx,cosx)
       Stretch(6)   = 0.d0
    ENDIF

    RETURN
  END FUNCTION Mapping3D_to_Stretch

  !>
  !!   Get a mapping matrix from a metric.
  !<
  FUNCTION Mapping3D_from_Metric(fMtr) RESULT(fMap)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: fMtr(6)
    REAL*8 :: fMap(3,3)
    REAL*8 :: Shrink(3), temp, temp3(3)
    INTEGER :: i,j

    CALL Metric3D_eigen(fMtr,Shrink,fMap)

    !--- For an orthogonal matrix, Transpose = Inverse
    fMap = Geo3D_Matrix_Transpose(fMap)
    Shrink(1:3) = dsqrt(Shrink(1:3))

    DO i=1,2
       DO j=i+1,3
          IF(Shrink(j)<Shrink(i))THEN
             temp      = Shrink(j)
             Shrink(j) = Shrink(i)
             Shrink(i) = temp
             temp3     = fMap(j,:)
             fMap(j,:) = fMap(i,:)
             fMap(i,:) = temp3
          ENDIF
       ENDDO
    ENDDO

    fMap(1,:) = fMap(1,:)*Shrink(1)
    fMap(2,:) = fMap(2,:)*Shrink(2)
    fMap(3,:) = fMap(3,:)*Shrink(3)

    IF(Geo3D_Matrix_Determinant(fMap)<0)THEN
       fMap(:,1) = -fMap(:,1)
    ENDIF

    RETURN
  END FUNCTION Mapping3D_from_Metric

  !>
  !!   Transfer a mapping matrix to a metric.
  !<
  FUNCTION Mapping3D_to_Metric(fMap) RESULT(fMtr)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: fMap(3,3)
    REAL*8 :: fMtr(6)
    fMtr(1) = SUM(fMap(:,1)*fMap(:,1))
    fMtr(2) = SUM(fMap(:,1)*fMap(:,2))
    fMtr(3) = SUM(fMap(:,1)*fMap(:,3))
    fMtr(4) = SUM(fMap(:,2)*fMap(:,2))
    fMtr(5) = SUM(fMap(:,2)*fMap(:,3))
    fMtr(6) = SUM(fMap(:,3)*fMap(:,3))
    RETURN
  END FUNCTION Mapping3D_to_Metric

  !>
  !!   Return the spacing in the 3 principle direction.
  !<
  FUNCTION Mapping3D_getSpacing(fMap) RESULT(Scalars)
    REAL*8, INTENT(IN) :: fMap(3,3)
    REAL*8 :: Scalars(3)
    Scalars(1:3) = 1.d0 / dsqrt(fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2)
    RETURN
  END FUNCTION Mapping3D_getSpacing

  !>
  !!    Calculate the Jacobi of a mapping.
  !!    If v1 if the volume in the physical domain and 
  !!       v2 is the volume in computational domain, then
  !!       v2 = v1 / Jacobi
  !!    @param[in]       fMap     mapping.
  !!    @return                   Jacobi.   
  !!    
  !<
  FUNCTION Mapping3D_Jacobi(fMap) RESULT(Jaco)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: fMap(3,3)
    REAL*8 :: Jaco, Shrink(3)
    Shrink(1:3) =  fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2 
    Jaco        = 1.d0 / dsqrt( Shrink(1)*Shrink(2)*Shrink(3) ) 
  END FUNCTION Mapping3D_Jacobi

  !>
  !!    Calculate the square of the distance between two points in Euclidean space.
  !!    @param[in] P1,P2   coordinates of the two points.
  !!    @param[in] fMap  the mapping.
  !!    @return          the square of the distance based on the Mapping.
  !<
  FUNCTION Mapping3D_Distance_SQ(P1,P2,fMap) RESULT(Dist_SQ)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: P1(3), P2(3), fMap(3,3)
    REAL*8  :: Dist_SQ
    REAL*8  :: dx, dy, dz, dd(3)
    dx = P2(1)-P1(1)
    dy = P2(2)-P1(2)
    dz = P2(3)-P1(3)
    dd(:)   = fMap(:,1)*dx + fMap(:,2)*dy + fMap(:,3)*dz
    Dist_SQ = dd(1)*dd(1) +dd(2)*dd(2) +dd(3)*dd(3)
    RETURN
  END FUNCTION Mapping3D_Distance_SQ

  !>
  !!    Calculate the distance between two points in Euclidean space.
  !!    @param[in] P1,P2   coordinates of the two points.
  !!    @param[in] fMap  the mapping.
  !!    @return          the distance based on the Mapping.
  !<
  FUNCTION Mapping3D_Distance(P1,P2,fMap) RESULT(Dist)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: P1(3), P2(3), fMap(3,3)
    REAL*8  :: Dist
    REAL*8  :: dx, dy, dz, dd(3)
    dx = P2(1)-P1(1)
    dy = P2(2)-P1(2)
    dz = P2(3)-P1(3)
    dd(:) = fMap(:,1)*dx + fMap(:,2)*dy + fMap(:,3)*dz
    Dist = dsqrt( dd(1)*dd(1) +dd(2)*dd(2) +dd(3)*dd(3) )
    RETURN
  END FUNCTION Mapping3D_Distance

  !>
  !!    Calculate the position of a point in a transformated coordinates.
  !!    @param[in] P    positions in the old/new coordinate system.
  !!    @param[in] fMap  the mapping.
  !!    @param[in] idirect =  1  transformate coordinate from old to new system. \n
  !!                       = -1  transformate coordinate from new to old system.
  !!    @return   positions in the new/old coordinate system.
  !<
  FUNCTION Mapping3D_Posit_Transf(P, fMap, idirect) RESULT(Pt)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: P(3), fMap(3,3)
    REAL*8  :: Pt(3)
    REAL*8  :: sinx,cosx,siny,cosy,sinz,cosz,pp(3),Shrink(3)
    INTEGER, INTENT(IN) :: idirect
    INTEGER :: i
    IF(idirect==1)THEN
       Pt(:) = fMap(:,1)*P(1) + fMap(:,2)*P(2) + fMap(:,3)*P(3)
    ELSE IF(idirect==-1)THEN
       Shrink(1:3) = fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2
       pp(1:3) = P(1:3) / Shrink(1:3)
       Pt(:) = fMap(1,:)*pp(1) + fMap(2,:)*pp(2) + fMap(3,:)*pp(3)
    ELSE
       WRITE(*,*)'Warning--- Geo3D_Posit_Transf: wrong direction'
       RETURN
    ENDIF
    RETURN
  END FUNCTION Mapping3D_Posit_Transf

  !>
  !!    Calculate the Cross Product of two vectors P2-P1 & P2-P3
  !!           in a mapped space.
  !!    @param[in] P1,P2,P3 coordinates of the three vertices.
  !!    @param[in] fMap  the mapping.
  !!    @return    the the Cross Product of two vectors
  !!                              ( double of the area of the triangle ).
  !<
  FUNCTION Mapping3D_Cross_Product(P1,P2,P3,fMap) RESULT(prod)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3), P2(3), P3(3), fMap(3,3)
    REAL*8 :: prod(3)
    REAL*8 :: a(3), b(3)
    a(:) = P1(:) - P2(:)
    b(:) = P3(:) - P2(:)
    a    = Mapping3D_Posit_Transf(a, fMap, 1)
    b    = Mapping3D_Posit_Transf(b, fMap, 1)
    prod(1) = a(2)*b(3)-a(3)*b(2)
    prod(2) = a(3)*b(1)-a(1)*b(3)
    prod(3) = a(1)*b(2)-a(2)*b(1)
    RETURN
  END FUNCTION Mapping3D_Cross_Product

  !>
  !!    Calculate the Dot Product of two vectors P2-P1 & P2-P3.
  !!    @param[in] P1,P2,P3  coordinates of the three vertices.
  !!    @param[in] fMap  the mapping.
  !!    @return            the the Dot Product of two vectors.
  !<
  FUNCTION Mapping3D_Dot_Product(P1,P2,P3,fMap) RESULT(prod)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: P1(3), P2(3), P3(3), fMap(3,3)
    REAL*8  :: prod
    REAL*8  :: a(3), b(3)
    a(:) = P1(:)-P2(:)
    b(:) = P3(:)-P2(:)
    a    = Mapping3D_Posit_Transf(a, fMap, 1)
    b    = Mapping3D_Posit_Transf(b, fMap, 1)
    prod = a(1)*b(1) + a(2)*b(2) + a(3)*b(3)
    RETURN
  END FUNCTION Mapping3D_Dot_Product

  !>
  !!    Calculate the Included Angle of two vectors P2-P1 & P2-P3.
  !!    @param[in] P1,P2,P3  coordinates of the three vertices.
  !!    @param[in] fMap  the mapping.
  !!    @return       the Included Angle of two vectors ( 0 < a <= PI ).
  !<
  FUNCTION Mapping3D_Included_Angle(P1,P2,P3,fMap) RESULT(Angle)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3), P2(3), P3(3), fMap(3,3)
    REAL*8  :: Angle
    REAL*8  :: pp1(3),pp2(3),pp3(3)
    pp1 = Mapping3D_Posit_Transf(P1, fMap, 1)
    pp2 = Mapping3D_Posit_Transf(P2, fMap, 1)
    pp3 = Mapping3D_Posit_Transf(P3, fMap, 1)
    Angle = Geo3D_Included_Angle(pp1,pp2,pp3)
    RETURN
  END FUNCTION Mapping3D_Included_Angle

  !>
  !!    Calculate the Volume of a tetrahedron P1-P2-P3-P4 in a mapped domain.
  !!    @param[in] P1,P2,P3,P4  coordinates of the four vertices.
  !!    @param[in]       fMap     mapping.
  !!    @return Volume of a tetrahedron (with sign).   
  !!    The same explaination of orientation with function Geo3D_Tet_Volume6
  !<
  FUNCTION Mapping3D_Tet_Volume(P1,P2,P3,P4,fMap) RESULT(vol)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3),P2(3),P3(3),P4(3), fMap(3,3)
    REAL*8 :: vol, Shrink(3)
    Shrink(1:3) = fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2
    vol = Geo3D_Tet_Volume6_High(P1,P2,P3,P4)
    vol = vol * dsqrt( Shrink(1)*Shrink(2)*Shrink(3) ) /6.d0
  END FUNCTION Mapping3D_Tet_Volume

  !>
  !!    Calculate the Volume of a tetrahedron P1-P2-P3-P4 in a mapped domain
  !!    by using subroutine orient3d from  predicates.c
  !!    @param[in] P1,P2,P3,P4  coordinates of the four vertices.
  !!    @param[in]       fMap    mapping.
  !!    @return Volume of a tetrahedron (with sign).
  !!    The same explaination of orientation with function Geo3D_Tet_Volume6.
  !<
  FUNCTION Mapping3D_Tet_Volume_Pred(P1,P2,P3,P4,fMap) RESULT(vol)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3),P2(3),P3(3),P4(3), fMap(3,3)
    REAL*8 :: vol, Shrink(3)
    Shrink(1:3) = fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2
    CALL orient3d(P1,P2,P3,P4,vol)
    vol = vol * dsqrt( Shrink(1)*Shrink(2)*Shrink(3) ) /6.d0
  END FUNCTION Mapping3D_Tet_Volume_Pred

  !>
  !!    Calculate the inverse matrix of a Mapping.
  !<
  FUNCTION Mapping3D_Inverse(fMap) RESULT(rev)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: fMap(3,3)
    REAL*8 :: rev(3,3), Shrink(3)
    INTEGER :: j
    Shrink(1:3) = fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2
    DO j=1,3
       rev(:,j) = fMap(j,:) / Shrink(j)
    ENDDO
    RETURN
  END FUNCTION Mapping3D_Inverse

  !>
  !!    Enlage the stretch of a mapping
  !!           if the minimum one is smaller than the Scalar.
  !<
  SUBROUTINE Mapping3D_Min_Enlarger(fMap,Scalar)
    IMPLICIT NONE
    REAL*8, INTENT(INOUT) :: fMap(3,3)
    REAL*8, INTENT(IN)    :: Scalar
    REAL*8  :: Stretch(1:3), Strm
    Stretch(1:3) = 1.d0 / dsqrt( fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2 )
    Strm         = MIN(Stretch(1),Stretch(2),Stretch(3)) / Scalar
    IF(Strm<1.d0) fMap(:,:) = fMap(:,:) * Strm
    RETURN
  END SUBROUTINE Mapping3D_Min_Enlarger

  !>
  !!    Enlage the stretch of a mapping
  !!           if the maximum one is smaller than the Scalar.
  !<
  SUBROUTINE Mapping3D_Max_Enlarger(fMap,Scalar)
    IMPLICIT NONE
    REAL*8, INTENT(INOUT) :: fMap(3,3)
    REAL*8, INTENT(IN)    :: Scalar
    REAL*8  :: Stretch(1:3), Strm
    Stretch(1:3) = 1.d0 / dsqrt( fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2 )
    Strm         = MAX(Stretch(1),Stretch(2),Stretch(3)) / Scalar
    IF(Strm<1.d0) fMap(:,:) = fMap(:,:) * Strm
    RETURN
  END SUBROUTINE Mapping3D_Max_Enlarger

  !>
  !!    Shrink the stretch of a mapping
  !!           if the minimum one is larger than the Scalar.
  !<
  SUBROUTINE Mapping3D_Min_Shrinker(fMap,Scalar)
    IMPLICIT NONE
    REAL*8, INTENT(INOUT) :: fMap(3,3)
    REAL*8, INTENT(IN)    :: Scalar
    REAL*8  :: Stretch(1:3), Strm
    Stretch(1:3) = 1.d0 / dsqrt( fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2 )
    Strm         = MIN(Stretch(1),Stretch(2),Stretch(3)) / Scalar
    IF(Strm>1.d0) fMap(:,:) = fMap(:,:) * Strm
    RETURN
  END SUBROUTINE Mapping3D_Min_Shrinker

  !>
  !!    Shrink the stretch of a mapping
  !!           if the maximum one is larger than the Scalar.
  !<
  SUBROUTINE Mapping3D_Max_Shrinker(fMap,Scalar)
    IMPLICIT NONE
    REAL*8, INTENT(INOUT) :: fMap(3,3)
    REAL*8, INTENT(IN)    :: Scalar
    REAL*8  :: Stretch(1:3), Strm
    Stretch(1:3) = 1.d0 / dsqrt( fMap(:,1)**2 + fMap(:,2)**2 + fMap(:,3)**2 )
    Strm         = MAX(Stretch(1),Stretch(2),Stretch(3)) / Scalar
    IF(Strm>1.d0) fMap(:,:) = fMap(:,:) * Strm
    RETURN
  END SUBROUTINE Mapping3D_Max_Shrinker

  !>
  !!    Intersect a Mapping with a Scalar.
  !<
  SUBROUTINE Mapping3D_ScalarIntersect(fMap,Scalar)
    IMPLICIT NONE
    REAL*8, INTENT(IN)    :: Scalar
    REAL*8, INTENT(INOUT) :: fMap(3,3)
    REAL*8  :: St
    INTEGER :: i
    DO i = 1,3
       St = 1.d0 / dsqrt( fMap(i,1)**2 + fMap(i,2)**2 + fMap(i,3)**2 )
       IF(St>Scalar)THEN
          fMap(i,:) = fMap(i,:) * (St/Scalar)
       ENDIF
    ENDDO
  END SUBROUTINE Mapping3D_ScalarIntersect

  !>
  !!    Return the maximum difference of 9 components of two matrices.
  !<
  FUNCTION Mapping3D_Distinct(fMap1,fMap2) RESULT(Distinct)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: fMap1(3,3), fMap2(3,3)
    REAL*8  :: distinct, s
    INTEGER :: i,j
    distinct = 0
    s        = 0
    DO i=1,3
       DO j=1,3
          distinct = MAX(distinct, ABS(fMap1(i,j)-fMap2(i,j)))
          s        = MAX(s, ABS(fMap1(i,j)), ABS(fMap2(i,j)) )
       ENDDO
    ENDDO
    distinct = distinct / s
    RETURN
  END FUNCTION Mapping3D_Distinct

  !>
  !!    Interpolate between two Mappings.
  !!    @param[in] fMap1,fMap2  the two mappings
  !!    @param[in] Model  = 1;  by using simultaneous matrix reduction
  !!                        Ref: H. Borouchaki, Finite Elements in Analysis
  !!                        and Design 25 (1997) 61-83. \n
  !!                  = 2;  by using a matrix power -1. \n
  !!                  = 3;  by using a matrix power  1. 
  !!    @param[in] t the factor (0<=t<=1; Return fMap1 if t=0; Return fMap2 if t=1).
  !<
  FUNCTION Mapping3D_Interpolate(fMap1,fMap2,t,Model) RESULT(fMap)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: fMap1(3,3), fMap2(3,3), t
    INTEGER, INTENT(IN) :: Model
    REAL*8 :: fMap(3,3)
    REAL*8 :: fMtr1(6), fMtr2(6), fMtr(6)
    REAL*8 :: Stretch1(6), Stretch2(6)

    !--- if t close 0 or 1, or two stretchs are identical
    IF(ABS(t)<1.e-6)THEN
       fMap = fMap1
       RETURN
    ELSE IF(ABS(t-1.d0)<1.e-6)THEN
       fMap = fMap2
       RETURN
    ELSE IF(Mapping3D_Distinct(fMap1,fMap2)<1.e-6)THEN
       fMap = fMap1
       RETURN
    ENDIF

    IF(Model==1)THEN

       !--- if the first stretch is a sphere
       Stretch1(1:3) = 1.d0 / ( fMap1(:,1)**2 + fMap1(:,2)**2 + fMap1(:,3)**2 )
       IF(MAX(ABS(Stretch1(1)-Stretch1(2)), ABS(Stretch1(1)-Stretch1(3)))    &
            < 1.e-6*MAX(Stretch1(1),Stretch1(2),Stretch1(3)) ) THEN
          Stretch1(1:3) = dsqrt(Stretch1(1:3))
          Stretch2(1:6) = Mapping3D_to_Stretch(fMap2)
          Stretch2(1:3) = Stretch1(1:3) + (Stretch2(1:3) - Stretch1(1:3)) * t
          fMap = Mapping3D_from_Stretch(Stretch2)
          RETURN
       ENDIF

       !--- if the second stretch is a sphere
       Stretch2(1:3) = 1.d0 / ( fMap2(:,1)**2 + fMap2(:,2)**2 + fMap2(:,3)**2 )
       IF(MAX(ABS(Stretch2(1)-Stretch2(2)), ABS(Stretch2(1)-Stretch2(3)))    &
            < 1.e-6*MAX(Stretch2(1),Stretch2(2),Stretch2(3)) ) THEN
          Stretch2(1:3) = dsqrt(Stretch2(1:3))
          Stretch1(1:6) = Mapping3D_to_Stretch(fMap1)
          Stretch1(1:3) = Stretch1(1:3) + (Stretch2(1:3) - Stretch1(1:3)) * t
          fMap = Mapping3D_from_Stretch(Stretch1)
          RETURN
       ENDIF

    ENDIF

    fMtr1 = Mapping3D_to_Metric(fMap1)
    fMtr2 = Mapping3D_to_Metric(fMap2)
    fMtr  = Metric3D_Interpolate(fMtr1,fMtr2,t,model)
    fMap  = Mapping3D_from_Metric(fMtr)


    RETURN
  END FUNCTION Mapping3D_Interpolate

  !>
  !!    Get the mean of n Mappings.
  !!    @param[in] n the number of mappings.
  !!    @param[in] fMaps  the mappings.
  !!    @param[in] Model    refer Mapping3D_Interpolate.
  !<
  FUNCTION Mapping3D_Mean(n,fMaps,Model) RESULT(fMap)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: n, Model
    REAL*8,  INTENT(IN)  :: fMaps(3,3,*)
    REAL*8  :: fMap(3,3)
    INTEGER :: i
    REAL*8  :: fMtr(6), fMtr1(6), t

    SELECT CASE(Model)

    CASE(1)

       fMtr(:) = Mapping3D_to_Metric(fMaps(:,:,1))
       DO i=2,n
          t = 1.d0/i
          fMtr1 = Mapping3D_to_Metric(fMaps(:,:,i))
          fMtr  = Metric3D_Interpolate(fMtr, fMtr1, t, Model)
       ENDDO

    CASE(2)

       t = 1.d0/n
       fMtr(:) = 0.d0
       DO i=1,n
          fMtr1 = Mapping3D_to_Metric(fMaps(:,:,i))
          fMtr1 = Metric3D_Inverse(fMtr1)
          fMtr  = fMtr + t * fMtr1
       ENDDO
       fMtr = Metric3D_Inverse(fMtr)

    CASE(3)

       t = 1.d0/n
       fMtr(:) = 0.d0
       DO i=1,n
          fMtr1 = Mapping3D_to_Metric(fMaps(:,:,i))
          fMtr  = fMtr + t * fMtr1
       ENDDO

    CASE DEFAULT

       WRITE(*,*)' '
       WRITE(*,*)'Error--- : no such case'
       WRITE(*,*)'Model=',Model
       CALL Error_STOP ( '--- Mapping3D_Mean')

    END SELECT

    fMap  = Mapping3D_from_Metric(fMtr)

    RETURN
  END FUNCTION Mapping3D_Mean

  !>
  !!    Get the weighted mean of n Mappings.
  !!    make sure that w(1:n)>0 and sum(w(1:n))=1.
  !!    @param[in] n  the number of mappings.
  !!    @param[in] fMaps  the mappings.
  !!    @param[in] w      the weights.
  !!    @param[in] Model    refer Mapping3D_Interpolate.
  !<
  FUNCTION Mapping3D_Mean_Weight(n,fMaps,w,Model) RESULT(fMap)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: n, Model
    REAL*8,  INTENT(IN)  :: fMaps(3,3,*),w(*)
    REAL*8  :: fMap(3,3)
    INTEGER :: i, j
    REAL*8  :: fMtr(6), fMtr1(6), t, ws
    REAL*8, PARAMETER :: TINY = 1.D-12

    SELECT CASE(Model)

    CASE(1)

       DO i=1,n
          IF(w(i)<TINY)CYCLE
          j = i
          fMtr(:) = Mapping3D_to_Metric(fMaps(:,:,i))
          ws = w(i)
          EXIT
       ENDDO

       DO i=j+1,n
          IF(w(i)<TINY)CYCLE
          ws = ws+w(i)
          t  = w(i)/ws
          fMtr1 = Mapping3D_to_Metric(fMaps(:,:,i))
          fMtr  = Metric3D_Interpolate(fMtr, fMtr1, t, Model)
       ENDDO

    CASE(2)

       fMtr(:) = 0.d0
       DO i=1,n
          IF(w(i)<TINY)CYCLE
          fMtr1 = Mapping3D_to_Metric(fMaps(:,:,i))
          fMtr1 = Metric3D_Inverse(fMtr1)
          fMtr  = fMtr + w(i) * fMtr1
       ENDDO
       fMtr = Metric3D_Inverse(fMtr)

    CASE(3)

       fMtr(:) = 0.d0
       DO i=1,n
          IF(w(i)<TINY)CYCLE
          fMtr1 = Mapping3D_to_Metric(fMaps(:,:,i))
          fMtr  = fMtr + w(i) * fMtr1
       ENDDO

    CASE DEFAULT

       WRITE(*,*)' '
       WRITE(*,*)'Error--- : no such case'
       WRITE(*,*)'Model=',Model
       CALL Error_STOP ( '--- Mapping3D_Mean_Weight')

    END SELECT

    fMap  = Mapping3D_from_Metric(fMtr)

    RETURN
  END FUNCTION Mapping3D_Mean_Weight

  !>
  !!    Intersect metric of two Mappings metrices
  !!                by using simultaneous matrix reduction for metrics.
  !<
  FUNCTION Mapping3D_Intersect(fMap1,fMap2) RESULT(fMap)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: fMap1(3,3), fMap2(3,3)
    REAL*8 :: fMap(3,3)
    REAL*8 :: fMtr1(6), fMtr2(6), fMtr(6)
    REAL*8 :: distinct

    distinct = Mapping3D_Distinct(fMap1,fMap2)
    IF(distinct<1.e-6)THEN
       fMap = fMap1
       RETURN
    ENDIF

    fMtr1 = Mapping3D_to_Metric(fMap1)
    fMtr2 = Mapping3D_to_Metric(fMap2)
    fMtr  = Metric3D_Intersect(fMtr1,fMtr2)
    fMap  = Mapping3D_from_Metric(fMtr)

    RETURN
  END FUNCTION Mapping3D_Intersect

  !>
  !! Write file=filename(1:nameLength) for a unit sphere in a mapping domain.
  !! @param[in]  fMap   the mapping
  !! @param[in]  Npoint the number of points along a circle (360 degree).
  !! @param[in] filename the character string of the file name.
  !! @param[in] nameLength the length of filename.
  !<
  SUBROUTINE Mapping3D_Display(fMap,Npoint,filename,nameLength)
    IMPLICIT NONE
    REAL*8,  INTENT(IN) :: fMap(3,3)
    INTEGER, INTENT(in) :: Npoint, nameLength
    CHARACTER(*),INTENT(in) :: filename
    REAL*8 , PARAMETER   :: PI = 3.141592653589793D0
    INTEGER :: k, j, Nhalf
    REAL*8  :: t1, t2, dd, p(3)

    OPEN(1,file=filename(1:nameLength))
    Nhalf = Npoint / 2
    dd    = PI / Nhalf
    DO k = 0,Nhalf
       WRITE(1,*)
       t1 = k*dd
       DO j = 0, 2*Nhalf
          t2 = j*dd
          p  = (/DCOS(t1), DSIN(t1)*DSIN(t2), DSIN(t1)*DCOS(t2)/)
          p  = Mapping3D_Posit_Transf(p, fMap, -1)
          WRITE(1,'(3e15.7)')p(:)
       ENDDO
    ENDDO

    CLOSE(1)
    RETURN
  END SUBROUTINE Mapping3D_Display

END MODULE Mapping3D



!>
!!
!!  Geometry for planes in 3D space
!!
!!
!<
MODULE Plane3DGeom

  USE Geometry3D
  USE Metric3D

  !>
  !!   Define a plane by anor(1)*x + anor(2)*y + anor(3)*z + d = 0   \n
  !!   and anor(:) has been normalised here.
  !<
  TYPE :: Plane3D
     REAL*8 :: d
     REAL*8 :: anor(3)       !<   the normal direction of the plane
     REAL*8 :: tx(3)         !<   the x-axis on the plane
     REAL*8 :: ty(3)         !<   the y-axis on the plane
  END TYPE Plane3D

CONTAINS

  !>
  !!    Build a plane by 3 points P1,P2, & P3.
  !<
  FUNCTION Plane3D_Build(P1,P2,P3) RESULT(aPlane)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3), P2(3), P3(3)
    TYPE(Plane3D) :: aPlane
    REAL*8        :: al, anor(3)
    anor = Geo3D_Cross_Product(P2,P1,P3)
    al   = dsqrt( anor(1)*anor(1) + anor(2)*anor(2) + anor(3)*anor(3) )
    anor = anor / al
    aPlane%d    = - ( anor(1)*P1(1) + anor(2)*P1(2) + anor(3)*P1(3) )
    aPlane%anor = anor
  END FUNCTION Plane3D_Build

  !>
  !!    Build a plane by a point P0, abd a normal direction anor.
  !<
  FUNCTION Plane3D_Build2(P0,anor) RESULT(aPlane)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P0(3), anor(3)
    TYPE(Plane3D) :: aPlane
    REAL*8        :: al
    al   = dsqrt( anor(1)*anor(1) + anor(2)*anor(2) + anor(3)*anor(3) )
    aPlane%anor = anor / al
    aPlane%d    = - ( aPlane%anor(1)*P0(1) + aPlane%anor(2)*P0(2) + aPlane%anor(3)*P0(3) )
  END FUNCTION Plane3D_Build2

  !>
  !!   Build the tangential directions of the plane.
  !!   The two tangential directions, tx and ty and
  !!      the normal direction of the plane form a right handed system.
  !<
  SUBROUTINE Plane3D_buildTangentAxes(aPlane)
    IMPLICIT NONE
    TYPE(Plane3D), INTENT(INOUT) :: aPlane
    REAL*8  :: Axes(3,2)
    CALL Geo3D_BuildOrthogonalAxes(aPlane%anor,Axes(:,1),Axes(:,2))    
    aPlane%tx(:) = Axes(:,1)
    aPlane%ty(:) = Axes(:,2)
  END SUBROUTINE Plane3D_buildTangentAxes

  !>
  !!   Return to 2D projected coordinates of a point onto a plane.
  !<
  FUNCTION Plane3D_project2D(aPlane,P0) RESULT(P2D)
    IMPLICIT NONE
    TYPE(Plane3D), INTENT(IN) :: aPlane
    REAL*8,        INTENT(IN) ::  P0(3)
    REAL*8 :: P2D(2)
    P2D(1) = Geo3D_Dot_Product(P0, aPlane%tx)
    P2D(2) = Geo3D_Dot_Product(P0, aPlane%ty)
  END FUNCTION Plane3D_project2D

  !>
  !!   Truncate a 3D mapping by a plane, return a 2D metric.
  !!      Any two points on the plane, the distance under the metric
  !!          is the same of the distance of their 3D positions under the mapping.
  !!   @param[in]  aPlane      : the plane with tangential axes.
  !!   @param[in]  fMap (3,3)  : the mapping of the 3D domain (see MODULE Mapping3D).
  !!   @return     fMtr2D (3)  : the 2D metric (see MODULE Metric2D).
  !<
  FUNCTION Plane3D_projectMapping(aPlane,fMap) RESULT(fMtr2D)
    IMPLICIT NONE
    TYPE(Plane3D), INTENT(IN) :: aPlane
    REAL*8,        INTENT(IN) :: fMap(3,3)
    REAL*8  :: fMtr2D(3)
    REAL*8  :: a1(3), a2(3)
    INTEGER :: i
    DO i = 1,3
       a1(i) = Geo3D_Dot_Product(fMap(i,:), aPlane%tx)
       a2(i) = Geo3D_Dot_Product(fMap(i,:), aPlane%ty)
    ENDDO
    fMtr2D(1) = Geo3D_Dot_Product(a1, a1)
    fMtr2D(2) = Geo3D_Dot_Product(a1, a2)
    fMtr2D(3) = Geo3D_Dot_Product(a2, a2)
  END FUNCTION Plane3D_projectMapping

  !>
  !!   Truncate a 3D metric by a plane, return a 2D metric.
  !!      Any two points on the plane, the distance under the 2D metric
  !!          is the same of the distance of their 3D positions under the 3D metric.
  !!   @param[in]  aPlane      : the plane with tangential axes.
  !!   @param[in]  fMtr (6)    : the 3D metric of the real domain (see MODULE Metric3D).
  !!   @return     fMtr2D (3)  : the 2D metric (see MODULE Metric2D).
  !<
  FUNCTION Plane3D_projectMetric(aPlane,fMtr) RESULT(fMtr2D)
    IMPLICIT NONE
    TYPE(Plane3D), INTENT(IN) :: aPlane
    REAL*8,        INTENT(IN) ::  fMtr(6)
    REAL*8  :: fMtr2D(3)
    fMtr2D(1) = Metric3D_Dot_Product(aPlane%tx, aPlane%tx, fMtr)
    fMtr2D(2) = Metric3D_Dot_Product(aPlane%tx, aPlane%ty, fMtr)
    fMtr2D(3) = Metric3D_Dot_Product(aPlane%ty, aPlane%ty, fMtr)
  END FUNCTION Plane3D_projectMetric

  !>
  !!   Return the 3D coordinates of of a projected point on a plane.
  !!   @param[in]  aPlane      : the plane with tangential axes.
  !<
  FUNCTION Plane3D_get3DPosition(aPlane, P2D) RESULT(PP)
    IMPLICIT NONE
    TYPE(Plane3D), INTENT(IN) :: aPlane
    REAL*8,        INTENT(IN) ::  P2D(2)
    REAL*8 :: PP(3)
    !--- For an orthogonal matrix, Transpose = Inverse
    PP(:) = aPlane%tx(:)*P2D(1) + aPlane%ty(:)*P2D(2) - aPlane%anor(:) * aPlane%d
  END FUNCTION Plane3D_get3DPosition

  !>
  !!    Calculate the project position of Point P0 on a plane.
  !!    @return  the project position.
  !<
  FUNCTION Plane3D_ProjectOnPlane(P0,aPlane) RESULT(PP)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P0(3)
    TYPE(Plane3D), INTENT(IN) :: aPlane
    REAL*8 :: PP(3)
    REAL*8 :: xl
    xl = aPlane%anor(1)*P0(1) + aPlane%anor(2)*P0(2) + aPlane%anor(3)*P0(3) + aPlane%d
    PP = P0 - aPlane%anor * xl
  END FUNCTION Plane3D_ProjectOnPlane

  !>
  !!    Calculate the project position of Point P0 on a face
  !!              which is determined by 3 points P1,P2, & P3.
  !!    @return  the project position.
  !<
  FUNCTION Plane3D_ProjectOnPlane3(P0,P1,P2,P3) RESULT(PP)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P0(3), P1(3), P2(3), P3(3)
    REAL*8 :: PP(3)
    REAL*8 :: a(3), b(3), xl
    a = Geo3D_Cross_Product(P1,P2,P3)
    b = P0 - P2
    xl = (a(1)*b(1)+a(2)*b(2)+a(3)*b(3)) / (a(1)*a(1)+a(2)*a(2)+a(3)*a(3))
    PP = P0 - a*xl
  END FUNCTION Plane3D_ProjectOnPlane3

  !>
  !!    Calculate the project position of Point P0 on a face
  !!              which is determined by 3 points P1,P2, & P3.
  !!    @return   the weight of the project position respect to the 3 points.
  !!                    i.e. Weight(1)*P1+Weight(2)*P2+Weight(3)*P3
  !<
  FUNCTION Plane3D_ProjectOnPlane3w(P0,P1,P2,P3) RESULT(Weight)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P0(3), P1(3), P2(3), P3(3)
    REAL*8 :: Weight(3)
    REAL*8 :: a(3), b1(3), b2(3), an
    a  = Geo3D_Cross_Product(P1,P2,P3)
    b1 = Geo3D_Cross_Product(P0,P2,P3)
    b2 = Geo3D_Cross_Product(P1,P0,P3)
    an = a(1)*a(1) + a(2)*a(2) + a(3)*a(3)
    Weight(1) = ( a(1)*b1(1) + a(2)*b1(2) + a(3)*b1(3) ) / an
    Weight(2) = ( a(1)*b2(1) + a(2)*b2(2) + a(3)*b2(3) ) / an
    Weight(3) = 1.d0 - Weight(1) - Weight(2)
  END FUNCTION Plane3D_ProjectOnPlane3w

  !>
  !!    Calculate the distance between a point and a plane.
  !<
  FUNCTION Plane3D_DistancePointToPlane(P0,aPlane) RESULT(Dist)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P0(3)
    TYPE(Plane3D) :: aPlane
    REAL*8 :: Dist
    Dist = aPlane%anor(1)*P0(1) + aPlane%anor(2)*P0(2) + aPlane%anor(3)*P0(3) + aPlane%d
    Dist = DABS(Dist)
  END FUNCTION Plane3D_DistancePointToPlane

  !>
  !!    Calculate the distance (with sign) between a point and a plane.
  !!    If the plane is built by Plane3D_Build(P1,P2,P3), then
  !!       the distance return positive if point P0 is located on right-handed
  !!       side of P1-P2-P3.
  !<
  FUNCTION Plane3D_SignedDistancePointToPlane(P0,aPlane) RESULT(Dist)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P0(3)
    TYPE(Plane3D) :: aPlane
    REAL*8 :: Dist
    Dist = aPlane%anor(1)*P0(1) + aPlane%anor(2)*P0(2) + aPlane%anor(3)*P0(3) + aPlane%d
  END FUNCTION Plane3D_SignedDistancePointToPlane

  !>
  !!    Calculate the shortest distance between the point P0 and the triangle P1-P2-P3
  !!         (NOT the plane on which the triangle lies).
  !<
  FUNCTION Plane3D_DistancePointToTriangle(P0,P1,P2,P3) RESULT(Dist)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P0(3), P1(3), P2(3), P3(3)
    REAL*8 :: Dist
    REAL*8 :: w(3), pp(3)
    w = Plane3D_ProjectOnPlane3w(P0,P1,P2,P3)
    IF(w(1)<=0 .AND. w(2)<=0)THEN
       Dist = Geo3D_Distance(P0, P3)    
    ELSE IF(w(2)<=0 .AND. w(3)<=0)THEN
       Dist = Geo3D_Distance(P0, P1)    
    ELSE IF(w(3)<=0 .AND. w(1)<=0)THEN
       Dist = Geo3D_Distance(P0, P2)    
    ELSE IF(w(1)<0)THEN
       Dist = 2.D0 * Geo3D_Triangle_Area(P0,P2,P3) / Geo3D_Distance(P2, P3)
    ELSE IF(w(2)<0)THEN
       Dist = 2.D0 * Geo3D_Triangle_Area(P0,P3,P1) / Geo3D_Distance(P3, P1)
    ELSE IF(w(3)<0)THEN
       Dist = 2.D0 * Geo3D_Triangle_Area(P0,P1,P2) / Geo3D_Distance(P1, P2)
    ELSE
       pp = w(1)*P1 + w(2)*P2 + w(3)*P3
       Dist = Geo3D_Distance(P0, pp)    
    ENDIF
  END FUNCTION Plane3D_DistancePointToTriangle

  !>
  !!   Calculate the intersecting point of a line and a plane.
  !!   @param[in] P1,P2  two points determinding the line.
  !!   @param[in] aPlane   the plane.
  !!   @param[out]  t <0      the crossed is out of the P1 end.  \n
  !!            0<=t<=1  the crossed is between P1 and P2.  \n
  !!            >1      the crossed is out of the P2 end.
  !<
  FUNCTION Plane3D_Line_Plane_Cross(P1,P2,aPlane,t) RESULT(PP)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3), P2(3)
    REAL*8, INTENT(OUT):: t
    TYPE(Plane3D), INTENT(IN)  :: aPlane
    REAL*8 :: PP(3)
    REAL*8 :: xl,d,PD(3)
    PD(:) = P2(:)-P1(:)
    xl = aPlane%anor(1)*PD(1) + aPlane%anor(2)*PD(2) + aPlane%anor(3)*PD(3)
    d  = aPlane%anor(1)*P1(1) + aPlane%anor(2)*P1(2) + aPlane%anor(3)*P1(3) + aPlane%d
    t  = (-d) / xl
    PP(:) = P1(:) + t*PD(:)
  END FUNCTION Plane3D_Line_Plane_Cross

  !>
  !!   Calculate the intersecting point of a line and a triangle.
  !!   @param[in] pp2  two points determinding the line.
  !!   @param[in] pp3  three points determinding the plane.
  !!   @param[in] ntype    >= 0:  output the intersecting point only if it is
  !!                          between PP2(:,1) & pp2(:,2).  \n
  !!                   =2/-2:  do not check the weight of intersecting point.  \n
  !!                   else:  always output and check the intersecting point.
  !!   @param[out]  ntype  = -999 the line is parallel to the triangle. \n
  !!                       = -2 :  the position PP has not been checked. \n
  !!                       = -1 :  the position PP is outside of the triangle. \n
  !!                       = 0  :  the position PP is beyond the line  section. \n
  !!                       = 1,2,3 :  the intersecting point is on the point
  !!                             pp3(:,1), pp3(:,2), or pp3(:,3), respectively. \n
  !!                       = 4,5,6 :  the intersecting point is on the edge oppositing
  !!                             pp3(:,1), pp3(:,2), or pp3(:,3), respectively.  \n
  !!                       = 7  :  the intersecting point is inside the triangle. 
  !!   @param[out]  weight  the relative weight of PP on the triangle
  !!   @param[out]  t       the factor of PP between pp2(:,1) and pp2(:,2)
  !!                           i.e., t=(PP-pp2(:,1))/(pp2(:,2)-pp2(:,1)).
  !!   @return     the coordinates of the intersecting point.
  !<
  FUNCTION Plane3D_Line_Tri_Cross(pp2,pp3,ntype,weight,t) RESULT(PP)
    IMPLICIT NONE
    REAL*8, INTENT(in) :: pp2(3,2),pp3(3,3)
    INTEGER, INTENT(inout) :: ntype
    REAL*8, INTENT(out) :: weight(3),t
    REAL*8 :: PP(3)
    REAL*8 :: v(3),a(3),b(3), prod,PD(3),xl,d,tinyarea
    INTEGER :: i, ipeek
    INTEGER :: i3i(5) = (/ 1,2,3,1,2 /)

    v(:) = Geo3D_Cross_Product(pp3(:,2),pp3(:,1),pp3(:,3))

    PD(:) = pp3(:,1)-pp2(:,1)
    d     = v(1)*PD(1) + v(2)*PD(2) + v(3)*PD(3)
    PD(:) = pp2(:,2)-pp2(:,1)
    xl    = v(1)*PD(1) + v(2)*PD(2) + v(3)*PD(3)
    !t     = d / xl  - SPW this causes a divide by zero!!!!!

    IF(xl==0)THEN
       ntype = -999
       RETURN

    ELSE
     t     = d / xl
    ENDIF

    IF(ntype>=0 .AND. (t<0 .OR. t>1))THEN
       ntype = 0
       RETURN
    ENDIF

    PP(:) = pp2(:,1) + t * PD(:)
    IF(ntype==2) ntype = -2
    IF(ntype==-2) RETURN

    ntype = 7
    tinyarea = 1.d-8
    ipeek=1
    DO i=2,3
       IF(ABS(v(i))>ABS(v(ipeek))) ipeek = i
    ENDDO

    DO i=1,3
       a(:) = pp3(:,i3i(i+1))-PP(:)
       b(:) = pp3(:,i3i(i+2))-PP(:)
       prod = a(i3i(ipeek+1))*b(i3i(ipeek+2)) - b(i3i(ipeek+1))*a(i3i(ipeek+2))
       weight(i) = prod / v(ipeek)
    ENDDO
    DO i=1,3
       IF(weight(i)<-tinyarea)THEN
          ntype = -1
          RETURN
       ENDIF

       IF(weight(i)<tinyarea)THEN
          IF(ntype==7)THEN
             !---PP is on a side
             ntype = 3+i
          ELSE IF(ntype>3)THEN
             !---PP is on a node
             ntype = 6-i-(ntype-3)
          ELSE
             CALL Error_STOP ( 'ERROR--- Plane3D_Line_Tri_Cross')
          ENDIF
       ENDIF
    ENDDO

    RETURN
  END FUNCTION Plane3D_Line_Tri_Cross

  !>
  !!    Calculate the Included Angle of two line P2-P1 & plane.
  !!    @param[in] P1,P2     the vector P1-P2 (or O-P1) if P2 not presented.
  !!    @param[in] aPlane       the plane.
  !!    @return                 the Included Angle ( -PI/2 <= a <= PI/2 ).
  !!                            A positive value indicates that the vector pointing up of the plane.
  !<
  FUNCTION Plane3D_Vector_Plane_Angle(aPlane, P1,P2) RESULT(Angle)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(3)
    REAL*8, INTENT(IN), OPTIONAL ::  P2(3)
    TYPE(Plane3D), INTENT(IN)  :: aPlane
    REAL*8 :: Angle
    REAL*8 :: pp1(3), c(3), crop, dotp
    REAL*8, PARAMETER :: PI = 3.141592653589793D0

    IF(present(P2))THEN
       pp1 = P2 - P1
    ELSE
       pp1 = P1
    ENDIF
    c(1:3) = Geo3D_Cross_Product(pp1,aPlane%anor)
    crop   = dsqrt(c(1)*c(1)+c(2)*c(2)+c(3)*c(3))
    dotp   = Geo3D_Dot_Product(pp1,aPlane%anor)
    Angle  = ATAN2(crop,dotp)
    Angle  = PI/2.d0 - Angle

  END FUNCTION Plane3D_Vector_Plane_Angle

  !>
  !!   Calculate the chord face of 2 spheres.
  !!   A chord face of two spheres is the face on which the intersecting
  !!     circle of these two spehre lies.
  !!   @param[in] SP1,SP2  two spheres (centre and radius).
  !<
  FUNCTION Plane3D_Sphere_Sphere_Chord(SP1,SP2) RESULT(aPlane)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: SP1(4), SP2(4)
    TYPE(Plane3D)  :: aPlane
    REAL*8 :: al
    aPlane%anor(:) = 2.d0*(SP1(1:3)-SP2(1:3))
    aPlane%d       = - (SP1(1)**2 + SP1(2)**2 + SP1(3)**2 - SP1(4)**2)   &
         + (SP2(1)**2 + SP2(2)**2 + SP2(3)**2 - SP2(4)**2)
    al = dsqrt( aPlane%anor(1) * aPlane%anor(1)    &
         + aPlane%anor(2) * aPlane%anor(2)    &
         + aPlane%anor(3) * aPlane%anor(3) )
    aPlane%anor = aPlane%anor / al
    aPlane%d    = aPlane%d / al
  END FUNCTION Plane3D_Sphere_Sphere_Chord


END MODULE Plane3DGeom

MODULE MatrixAlgebra

  CONTAINS

  !>
  !!  [Ruben's code]  LU Decomposition
  !!  @param[in]  n  : the dimension of the matrix.
  !!  @param[in]  A  (n*n) the matrix.
  !!  @param[out] A  (n*n) the Decomposited matrix.
  !!  @param[out] indexLU (n)
  !<
  SUBROUTINE Matrix_LU_Decomposition(n, A, indexLU)
    IMPLICIT NONE
    INTEGER, INTENT(IN)    :: n
    REAL*8,  INTENT(INOUT) :: A(n,n)
    INTEGER, INTENT(OUT)   :: indexLU(n)
    REAL*8  :: tmp, aux, aux2, u(n)
    INTEGER :: i, j, k, iMax
    REAL*8, PARAMETER :: EPS = 2.0D-16

    DO i=1,n
       u(i) = 1.0D0/MAXVAL(dabs(A(i,:)))
    END DO

    DO j=1,n
       DO i=1,j-1
          A(i,j) = A(i,j) -  DOT_PRODUCT(A(i,1:i-1),A(1:i-1,j))
       END DO
       aux = 0.0D0
       DO i=j,n
          A(i,j) = A(i,j) -  DOT_PRODUCT(A(i,1:j-1),A(1:j-1,j))
          aux2 = u(i)*dabs(A(i,j))
          IF(aux2>=aux) THEN
             iMax = i
             aux = aux2
          END IF
       END DO
       IF(j/=iMax) THEN
          DO k=1,n
             aux2 = A(iMax,k)
             A(iMax,k) = A(j,k)
             A(j,k) = aux2
          END DO
          u(iMax) = u(j)
       END IF

       indexLU(j) = iMax
       IF(dabs(A(j,j))<EPS) THEN
          A(j,j) = EPS
       END IF
       IF(j/=n) THEN
          A(j+1:n,j) = A(j+1:n,j)/A(j,j)
       END IF
    END DO

  END SUBROUTINE Matrix_LU_Decomposition

  !>
  !!  [Ruben's code]  LU solver
  !!  @param[in]  n  : the dimension of the matrix.
  !!  @param[in]  A  (n*n) the LU-Decomposited matrix, 
  !!                           output of SUBROUTINE Matrix_LU_Decomposition().
  !!  @param[in]  indexLU (n)  output of SUBROUTINE Matrix_LU_Decomposition().
  !!  @param[in]  b  (n) the right hand.
  !!  @param[out] b  (n) the solution.
  !<
  SUBROUTINE Matrix_LU_Solver(n,A,indexLU,b)
    IMPLICIT NONE
    INTEGER, INTENT(IN)    :: n, indexLU(n)
    REAL*8,  INTENT(IN)    :: A(n,n)
    REAL*8,  INTENT(INOUT) :: b(n)
    REAL*8  :: tmp
    INTEGER :: i, k, indx

    k = 0
    DO i=1,n
       indx = indexLU(i)
       tmp = b(indx)
       b(indx) = b(i)
       IF(k/=0) THEN
          tmp = tmp - DOT_PRODUCT(A(i,k:i-1),b(k:i-1))
       ELSEIF(tmp/=0.0D0) THEN
          k = i
       END IF
       b(i) = tmp
    END DO

    DO i=n,1,-1
       tmp = b(i)
       IF(i<n) THEN
          tmp = tmp - DOT_PRODUCT(A(i,i+1:n),b(i+1:n))
       END IF
       b(i) = tmp/A(i,i)
    END DO

  END SUBROUTINE Matrix_LU_Solver


END MODULE MatrixAlgebra


!>
!!
!!  collecton of modules
!<
MODULE Geometry3DAll

  USE Geometry3D
  USE Metric3D
  USE Mapping3D
  USE ScalarCalculator
  USE Plane3DGeom
  USE MatrixAlgebra

END MODULE Geometry3DAll


