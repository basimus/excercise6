!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!******************************************************************************!
!     Read background                                                          !
!                                                                              !
!******************************************************************************!
SUBROUTINE Quad_Background()

  USE common_Title
  USE common_Parameters
  USE QuadtreeMesh
  USE Rieman2D
  IMPLICIT NONE

  INTEGER :: NB_SourcePt, imax, jmax
  INTEGER :: n,i,j, L, Level, i0, i1, j0, j1, IT, IB, ip, ipp(3)
  REAL*8  :: fMetric(3,MAX_NB_Point), EffRange(MAX_NB_Point)
  REAL*8  :: Stretch(4), Xmax1, Xmin1, Ymax1, Ymin1, dx, dy
  REAL*8  :: pp3(2,3), pp(2), distt, x0, x1, y0, y1
  REAL*8  :: vol, vol1, vol2, vol12
  REAL*8  :: fM(3), fM1(3), fM2(3), fM3(3), fM12(3)
  TYPE (QuadtreeMesh_Cell_Type), POINTER :: pQuad_Grid
  TYPE (QuadtreeMesh_Cell_Type), POINTER :: p1Quad_Grid,p2Quad_Grid
  TYPE (QuadtreeMesh_Cell_Type), POINTER :: p3Quad_Grid,p4Quad_Grid


  OPEN(1,file=JobName(1:JobNameLength)//'.BACK', status='old')

  READ(1,*)
  READ(1,*) GridSize
  READ(1,*)
  READ(1,*) StretchBasic(1:4)
  fMetricBasic(1:3) = Stretch_Metric2D(StretchBasic)

  !---- Read background triangulation and source point

  READ(1,*)
  READ(1,*) NB_Point,NB_Tri
  READ(1,*)
  DO n=1,NB_Point
     READ(1,*)i,Posit(1:2,i),Stretch(1:4)
     fMetric(:,i) = Stretch_Metric2D(Stretch)
  ENDDO
  READ(1,*)
  DO n=1,NB_Tri
     READ(1,*)i,IP_Tri(1:3,i)
  ENDDO
  READ(1,*)
  READ(1,*)NB_SourcePt
  READ(1,*)
  DO n=1,NB_SourcePt
     READ(1,*)i,Posit(1:2,n+NB_Point),EffRange(n+NB_Point),Stretch(1:4)
     fMetric(:,n+NB_Point) = Stretch_Metric2D(Stretch)
  ENDDO
  NB_Point = NB_Point + NB_SourcePt

  CLOSE (1)

  CALL Domain_Range

  !--- initial background Quad mesh

  dx = GridSize * StretchBasic(1)
  dy = GridSize * StretchBasic(2)
  Xmin1 = Xmin - (Xmax-Xmin)*0.1d0
  Xmax1 = Xmax + (Xmax-Xmin)*0.1d0
  Ymin1 = Ymin - (Ymax-Ymin)*0.1d0
  Ymax1 = Ymax + (Ymax-Ymin)*0.1d0
  imax  = (Xmax1-Xmin1)/dx +1
  jmax  = (Ymax1-Ymin1)/dy +1

  CALL QuadtreeMesh_Set(dx,dy, xmin1,ymin1, imax,jmax)

  !---- refine background Quad mesh based on background triangulation

  DO IT=1,NB_Tri
     PP3(1:2,1:3)  = Posit(1:2,IP_Tri(1:3,IT))

     distt = MAX ( Distance_SQ_2D(pp3(:,1),pp3(:,2)),    &
          Distance_SQ_2D(pp3(:,2),pp3(:,3)),    &
          Distance_SQ_2D(pp3(:,3),pp3(:,1))  )
     distt = ABS(Cross_Product_2D(pp3(:,1),pp3(:,2),pp3(:,3))) / 2.d0     &
          / dsqrt(distt)                                  !-- min height

     DO L = 1, Max_Quad_Level-1
        IF( MAX(QuadMesh%DX(L),QuadMesh%DY(L)) < distt ) EXIT
     ENDDO
     Level = L

     IF(Level<=1) CYCLE

     x0 = MIN(PP3(1,1),PP3(1,2),PP3(1,3))
     x1 = MAX(PP3(1,1),PP3(1,2),PP3(1,3))
     y0 = MIN(PP3(2,1),PP3(2,2),PP3(2,3))
     y1 = MAX(PP3(2,1),PP3(2,2),PP3(2,3))
     i0 = (x0 - QuadMesh%Pmin(1)) / QuadMesh%DX(1) +1
     i1 = (x1 - QuadMesh%Pmin(1)) / QuadMesh%DX(1) +1
     j0 = (y0 - QuadMesh%Pmin(2)) / QuadMesh%DY(1) +1
     j1 = (y1 - QuadMesh%Pmin(2)) / QuadMesh%DY(1) +1

     DO i=i0,i1
        DO j=j0,j1
           pQuad_Grid => Quad_Grid(i,j)
           CALL Quad_Tri_Refine(pp3,Level,pQuad_Grid)
        ENDDO
     ENDDO
  ENDDO

  !---- refine background Quad mesh based on source points

  DO n = NB_Point-NB_SourcePt+1, NB_Point

     DO L=1,Max_Quad_Level
        IF( MAX(QuadMesh%DX(L),QuadMesh%DY(L)) < EffRange(n)) EXIT
     ENDDO
     Level = L

     IF(Level==1) CYCLE

     DO
        pQuad_Grid => QuadtreeMesh_CellSearch(Posit(:,n),Level)
        IF(pQuad_Grid%Level >= Level) EXIT
        CALL QuadtreeMesh_CellBisect(pQuad_Grid)
     ENDDO
  ENDDO

  !---- initialise background Quad mesh nodes

  DO ip=1,QuadMesh%numNodes
     Counter_Quad_Node(ip) = 0
     Value_Quad_Node(1:3,ip) = fMetricBasic(1:3)
  ENDDO

  !---- evaluate background Quad mesh nodes based on background triangulation

  CALL Next_Build( )

  IF(NB_Tri>0)THEN
     DO ip=1,QuadMesh%numNodes
        pp(:) = QuadMesh%Coord(:,ip)
        CALL Locate_Node_Tri(pp,IT,Ib)
        IF(IT==0) CYCLE

        Counter_Quad_Node(ip) = 1
        ipp(:) = IP_Tri(1:3,IT)
        PP3(1:2,1:3)  = Posit(1:2,ipp(1:3))

        vol   = ABS(Cross_Product_2D(pp3(:,1),pp3(:,2),pp3(:,3)))
        vol1  = ABS(Cross_Product_2D(pp(:),   pp3(:,2),pp3(:,3))) / vol
        vol2  = ABS(Cross_Product_2D(pp3(:,1),pp(:),   pp3(:,3))) / vol
        vol12 = vol1 + vol2

        IF(vol12>1.e-4)THEN
           vol2 = vol2/vol12
           fM1  = fMetric(:,ipp(1))
           fM2  = fMetric(:,ipp(2))
           fM3  = fMetric(:,ipp(3))
           fM12 = Interpolate_Metric2D(fM1,fM2, vol2 )
           fM   = Interpolate_Metric2D(fM3,fM12,vol12)
        ELSE
           fM   = fMetric(:,ipp(3))
        ENDIF

        Value_Quad_Node(1:3,ip) = fM(1:3)

     ENDDO
  ENDIF

  !---- evaluate background Quad mesh nodes based on source points

  DO n = NB_Point-NB_SourcePt+1, NB_Point

     x0 = Posit(1,n)-EffRange(n)/2.D0
     x1 = Posit(1,n)+EffRange(n)/2.D0
     y0 = Posit(2,n)-EffRange(n)/2.D0
     y1 = Posit(2,n)+EffRange(n)/2.D0
     IF(x0<QuadMesh%Pmin(1)) x0=QuadMesh%Pmin(1)
     IF(x1>QuadMesh%Pmax(1)) x1=QuadMesh%Pmax(1)
     IF(y0<QuadMesh%Pmin(2)) y0=QuadMesh%Pmin(2)
     IF(y1>QuadMesh%Pmax(2)) y1=QuadMesh%Pmax(2)

     DO L=1,Max_Quad_Level
        IF( MIN(QuadMesh%DX(L),QuadMesh%DY(L)) < EffRange(n)) EXIT
     ENDDO
     Level = L

     IF(Level==1)THEN

        i0 = (x0 - QuadMesh%Pmin(1)) / QuadMesh%DX(1) +1
        i1 = (x1 - QuadMesh%Pmin(1)) / QuadMesh%DX(1) +1
        j0 = (y0 - QuadMesh%Pmin(2)) / QuadMesh%DY(1) +1
        j1 = (y1 - QuadMesh%Pmin(2)) / QuadMesh%DY(1) +1
        DO i=i0,i1
           DO j=j0,j1
              pQuad_Grid =>Quad_Grid(i,j)
              CALL Mark_Quad_Grid(pQuad_Grid)
           ENDDO
        ENDDO
        DO i=i0,i1
           DO j=j0,j1
              pQuad_Grid =>Quad_Grid(i,j)
              CALL Evaluate_Quad_Grid(pQuad_Grid,fMetric(:,n),x0,x1,y0,y1)
           ENDDO
        ENDDO

     ELSE

        pp(:) = (/x0,y0/)
        p1Quad_Grid => QuadtreeMesh_CellSearch(pp,Level)
        CALL Mark_Quad_Grid(p1Quad_Grid)
        pp(:) = (/x1,y0/)
        p2Quad_Grid => QuadtreeMesh_CellSearch(pp,Level)
        CALL Mark_Quad_Grid(p2Quad_Grid)
        pp(:) = (/x0,y1/)
        p3Quad_Grid => QuadtreeMesh_CellSearch(pp,Level)
        CALL Mark_Quad_Grid(p3Quad_Grid)
        pp(:) = (/x1,y1/)
        p4Quad_Grid => QuadtreeMesh_CellSearch(pp,Level)
        CALL Mark_Quad_Grid(p4Quad_Grid)

        CALL Evaluate_Quad_Grid(p1Quad_Grid,fMetric(:,n),x0,x1,y0,y1)
        CALL Evaluate_Quad_Grid(p2Quad_Grid,fMetric(:,n),x0,x1,y0,y1)
        CALL Evaluate_Quad_Grid(p3Quad_Grid,fMetric(:,n),x0,x1,y0,y1)
        CALL Evaluate_Quad_Grid(p4Quad_Grid,fMetric(:,n),x0,x1,y0,y1)

     ENDIF

  ENDDO


  !---- smoothen background Quad grids by nodes

  Counter2_Quad_Node(1:QuadMesh%numNodes) = 0   !-- Counter2, not Counter

  DO i=1,QuadMesh%imax
     DO j=1,QuadMesh%jmax
        pQuad_Grid => Quad_Grid(i,j)
        CALL Fill_Quad_Grid(pQuad_Grid,1)
     ENDDO
  ENDDO

  !---- mesh gradation control

  IF(Gradation_Factor<3.0)THEN

     DO i=1,QuadMesh%imax
        DO j=1,QuadMesh%jmax
           pQuad_Grid => Quad_Grid(i,j)
           CALL Gradation_Quad_Grid(pQuad_Grid,1)
        ENDDO
     ENDDO

     DO i=QuadMesh%imax,1,-1
        DO j=1,QuadMesh%jmax
           pQuad_Grid => Quad_Grid(i,j)
           CALL Gradation_Quad_Grid(pQuad_Grid,2)
        ENDDO
     ENDDO

     DO i=QuadMesh%imax,1,-1
        DO j=QuadMesh%jmax,1,-1
           pQuad_Grid => Quad_Grid(i,j)
           CALL Gradation_Quad_Grid(pQuad_Grid,3)
        ENDDO
     ENDDO

     DO i=1,QuadMesh%imax
        DO j=QuadMesh%jmax,1,-1
           pQuad_Grid => Quad_Grid(i,j)
           CALL Gradation_Quad_Grid(pQuad_Grid,4)
        ENDDO
     ENDDO

  ENDIF

  !---- evaluate background Quad grids by nodes

  IF(.NOT.Interpolate_Quad_Metric)THEN
     DO i=1,QuadMesh%imax
        DO j=1,QuadMesh%jmax
           pQuad_Grid => Quad_Grid(i,j)
           CALL Fill_Quad_Grid(pQuad_Grid,2)
        ENDDO
     ENDDO
  ENDIF

  !--     CALL QuadtreeMesh_Check()

  CALL QuadtreeMesh_Draw(JobName,JobNameLength)

  RETURN

CONTAINS

  !******************************************************************************!
  RECURSIVE SUBROUTINE Quad_Tri_Refine(pp3,Level,pQuad_Grid)

    IMPLICIT NONE

    INTEGER, INTENT(IN) :: Level
    REAL*8,  INTENT(IN) :: pp3(2,3)
    REAL*8  :: x1,x2,y1,y2,aa,small

    TYPE (QuadtreeMesh_Cell_Type), POINTER :: p1Quad_Grid, pQuad_Grid

    IF(pQuad_Grid%Level >= Level) RETURN

    x1 = QuadMesh%Coord(1,pQuad_Grid%Node(1))
    x2 = QuadMesh%Coord(1,pQuad_Grid%Node(4))
    y1 = QuadMesh%Coord(2,pQuad_Grid%Node(1))
    y2 = QuadMesh%Coord(2,pQuad_Grid%Node(4))
    aa = Intersect_RecTri_2D(pp3,x1,x2,y1,y2)

    small = QuadMesh%DX(Max_Quad_Level+1)     &
         * QuadMesh%DY(Max_Quad_Level+1)/16.d0

    IF(aa < small) RETURN

    IF(.NOT. pQuad_Grid%Bisected) CALL QuadtreeMesh_CellBisect(pQuad_Grid)

    p1Quad_Grid => pQuad_Grid%Son_LT
    CALL Quad_Tri_Refine(pp3,Level,p1Quad_Grid)
    p1Quad_Grid => pQuad_Grid%Son_LB
    CALL Quad_Tri_Refine(pp3,Level,p1Quad_Grid)
    p1Quad_Grid => pQuad_Grid%Son_RT
    CALL Quad_Tri_Refine(pp3,Level,p1Quad_Grid)
    p1Quad_Grid => pQuad_Grid%Son_RB
    CALL Quad_Tri_Refine(pp3,Level,p1Quad_Grid)

    RETURN
  END SUBROUTINE Quad_Tri_Refine

  !******************************************************************************!

  RECURSIVE SUBROUTINE Mark_Quad_Grid(pQuad_Grid)
    IMPLICIT NONE

    TYPE (QuadtreeMesh_Cell_Type), POINTER :: pQuad_Grid, p2Quad_Grid

    IF(pQuad_Grid%Bisected)THEN
       p2Quad_Grid => pQuad_Grid%Son_LT
       CALL Mark_Quad_Grid(p2Quad_Grid)
       p2Quad_Grid => pQuad_Grid%Son_LB
       CALL Mark_Quad_Grid(p2Quad_Grid)
       p2Quad_Grid => pQuad_Grid%Son_RT
       CALL Mark_Quad_Grid(p2Quad_Grid)
       p2Quad_Grid => pQuad_Grid%Son_RB
       CALL Mark_Quad_Grid(p2Quad_Grid)
    ELSE
       Counter2_Quad_Node(pQuad_Grid%Node(1:4)) = 0
    ENDIF

    RETURN
  END SUBROUTINE Mark_Quad_Grid

  !******************************************************************************!

  RECURSIVE SUBROUTINE Evaluate_Quad_Grid(pQuad_Grid,v,x0,x1,y0,y1)
    IMPLICIT NONE

    REAL*8, INTENT(IN) :: v(3),x0,x1,y0,y1
    INTEGER :: i,ip
    REAL*8  :: v1(3), t
    TYPE (QuadtreeMesh_Cell_Type), POINTER :: pQuad_Grid, p2Quad_Grid

    IF(pQuad_Grid%Bisected)THEN

       p2Quad_Grid => pQuad_Grid%Son_LT
       CALL Evaluate_Quad_Grid(p2Quad_Grid,v,x0,x1,y0,y1)

       p2Quad_Grid => pQuad_Grid%Son_LB
       CALL Evaluate_Quad_Grid(p2Quad_Grid,v,x0,x1,y0,y1)

       p2Quad_Grid => pQuad_Grid%Son_RT
       CALL Evaluate_Quad_Grid(p2Quad_Grid,v,x0,x1,y0,y1)

       p2Quad_Grid => pQuad_Grid%Son_RB
       CALL Evaluate_Quad_Grid(p2Quad_Grid,v,x0,x1,y0,y1)

    ELSE

       DO i=1,4
          ip = pQuad_Grid%Node(i)
          IF(Counter2_Quad_Node(ip)==1) CYCLE

          IF( QuadMesh%Coord(1,ip)>=x0 .AND. QuadMesh%Coord(1,ip)<=x1 .AND.  &
               QuadMesh%Coord(2,ip)>=y0 .AND. QuadMesh%Coord(2,ip)<=y1 )THEN
             IF(Counter_Quad_Node(ip)<=1)THEN
                Value_Quad_Node(1:3,ip) = v(1:3)
                Counter_Quad_Node(ip) = 2
             ELSE
                v1 = Value_Quad_Node(1:3,ip)
                t  = 1.d0/Counter_Quad_Node(ip)
                Value_Quad_Node(1:3,ip) = Interpolate_Metric2D(v1,v,t)
                Counter_Quad_Node(ip) = Counter_Quad_Node(ip) + 1
             ENDIF
          ENDIF

          Counter2_Quad_Node(ip) = 1
       ENDDO

    ENDIF

    RETURN
  END SUBROUTINE Evaluate_Quad_Grid

  !******************************************************************************!
  !
  !     Kjob==1, modify background's boundary
  !     if Counter_Quad_Node(ip)>0 then the value of point ip has been set
  !        by triangular background mesh or source point, otherwise
  !        it is set as the basic value. Some points with basic value are
  !        located at the background's boundary, and they will be reset here
  !        to smoothen this boundary.
  !
  !     Kjob==2, set the value of quad grids by four vertice


  RECURSIVE SUBROUTINE Fill_Quad_Grid(pQuad_Grid,Kjob)
    IMPLICIT NONE

    INTEGER :: Kjob
    INTEGER :: i,ip(4),nfree,ifree(4),jp
    TYPE (QuadtreeMesh_Cell_Type), POINTER :: pQuad_Grid, p2Quad_Grid
    REAL*8  :: fMetrics(3,4),fMetric1(3),fMetric2(3),fMetric(3)
    REAL*8  :: t, tc

    tc = 0.5d0

    IF(pQuad_Grid%Bisected)THEN

       p2Quad_Grid => pQuad_Grid%Son_LT
       CALL Fill_Quad_Grid(p2Quad_Grid,Kjob)

       p2Quad_Grid => pQuad_Grid%Son_LB
       CALL Fill_Quad_Grid(p2Quad_Grid,Kjob)

       p2Quad_Grid => pQuad_Grid%Son_RT
       CALL Fill_Quad_Grid(p2Quad_Grid,Kjob)

       p2Quad_Grid => pQuad_Grid%Son_RB
       CALL Fill_Quad_Grid(p2Quad_Grid,Kjob)

    ELSE

       ip(:) = pQuad_Grid%Node(:)
       fMetrics(1:3,1:4) = Value_Quad_Node(1:3,ip(1:4))

       IF(Kjob==1)THEN

          nfree = 0
          DO i=1,4
             IF(Counter_Quad_Node(ip(i))==0)THEN
                nfree=nfree+1
                ifree(nfree)=i
             ENDIF
          ENDDO
          IF(nfree==1)THEN
             IF(ifree(1)==1 .OR. ifree(1)==4)THEN
                fMetrics(:,ifree(1)) =                &
                     Interpolate_Metric2D(fMetrics(:,2),fMetrics(:,3),tc)
             ELSE
                fMetrics(:,ifree(1)) =                &
                     Interpolate_Metric2D(fMetrics(:,1),fMetrics(:,4),tc)
             ENDIF
          ELSE IF(nfree==2)THEN
             IF(ifree(1)==1 .AND. ifree(2)==2)THEN
                fMetrics(:,ifree(1)) = fMetrics(:,3)
                fMetrics(:,ifree(2)) = fMetrics(:,4)
             ELSE IF(ifree(1)==1 .AND. ifree(2)==3)THEN
                fMetrics(:,ifree(1)) = fMetrics(:,2)
                fMetrics(:,ifree(2)) = fMetrics(:,4)
             ELSE IF(ifree(1)==2 .AND. ifree(2)==4)THEN
                fMetrics(:,ifree(1)) = fMetrics(:,1)
                fMetrics(:,ifree(2)) = fMetrics(:,3)
             ELSE IF(ifree(1)==3 .AND. ifree(2)==4)THEN
                fMetrics(:,ifree(1)) = fMetrics(:,1)
                fMetrics(:,ifree(2)) = fMetrics(:,2)
             ELSE IF(ifree(1)==1 .AND. ifree(2)==4)THEN
                fMetric = Interpolate_Metric2D(fMetrics(:,2),fMetrics(:,3),tc)
                fMetrics(:,ifree(1)) = fMetric
                fMetrics(:,ifree(2)) = fMetric
             ELSE IF(ifree(1)==2 .AND. ifree(2)==3)THEN
                fMetric = Interpolate_Metric2D(fMetrics(:,1),fMetrics(:,4),tc)
                fMetrics(:,ifree(1)) = fMetric
                fMetrics(:,ifree(2)) = fMetric
             ENDIF
          ELSE IF(nfree==3)THEN
             DO i=1,4
                IF(Counter_Quad_Node(ip(i))/=0)THEN
                   fMetrics(:,ifree(1)) = fMetrics(:,i)
                   fMetrics(:,ifree(2)) = fMetrics(:,i)
                   fMetrics(:,ifree(3)) = fMetrics(:,i)
                ENDIF
             ENDDO
          ENDIF

          DO i=1,nfree
             jp=ip(ifree(i))
             IF(Counter2_Quad_Node(jp)==0)THEN
                Value_Quad_Node(:,jp) = fMetrics(:,ifree(i))
             ELSE
                fMetric1 = Value_Quad_Node(1:3,jp)
                fMetric2 = fMetrics(:,ifree(i))
                t = 1.d0/(Counter2_Quad_Node(jp)+1.d0)
                Value_Quad_Node(:,jp) = Interpolate_Metric2D(fMetric1,fMetric2,t)
             ENDIF
             Counter2_Quad_Node(jp) = Counter2_Quad_Node(jp) + 1
          ENDDO

       ELSE IF(Kjob==2)THEN

          fMetric1 = Interpolate_Metric2D(fMetrics(:,1),fMetrics(:,2),tc)
          fMetric2 = Interpolate_Metric2D(fMetrics(:,3),fMetrics(:,4),tc)
          fMetric  = Interpolate_Metric2D(fMetric1,     fMetric2,     tc)
          pQuad_Grid%V(:) = fMetric(:)

       ENDIF

    ENDIF

    RETURN
  END SUBROUTINE Fill_Quad_Grid

  !******************************************************************************!
  !


  RECURSIVE SUBROUTINE GRADATION_Quad_Grid(pQuad_Grid,Kdir)
    IMPLICIT NONE

    INTEGER :: Kdir, n, ip(4)
    TYPE (QuadtreeMesh_Cell_Type), POINTER :: pQuad_Grid, p2Quad_Grid
    REAL*8  :: fMetrics(3,4),fMetric1(3),fMetric2(3)
    REAL*8  :: fc(4), dx, dy
    CHARACTER*2 :: Order(4,4)
    DATA Order(:,1) / 'LB','RB','LT','RT'/
    DATA Order(:,2) / 'RB','LB','RT','LT'/
    DATA Order(:,3) / 'RT','LT','RB','LB'/
    DATA Order(:,4) / 'LT','RT','LB','RB'/


    IF(pQuad_Grid%Bisected)THEN

       DO n=1,4
          IF(Order(n,Kdir)=='LB')THEN
             p2Quad_Grid => pQuad_Grid%Son_LB
          ELSE IF(Order(n,Kdir)=='RB')THEN
             p2Quad_Grid => pQuad_Grid%Son_RB
          ELSE IF(Order(n,Kdir)=='LT')THEN
             p2Quad_Grid => pQuad_Grid%Son_LT
          ELSE IF(Order(n,Kdir)=='RT')THEN
             p2Quad_Grid => pQuad_Grid%Son_RT
          ENDIF
          CALL GRADATION_Quad_Grid(p2Quad_Grid,Kdir)
       ENDDO

    ELSE

       ip(:) = pQuad_Grid%Node(:)
       dx = QuadMesh%DX(pQuad_Grid%Level)
       dy = QuadMesh%DY(pQuad_Grid%Level)

       !--- x- direction

       fMetrics(:,1:4) = Value_Quad_Node(1:3,ip(1:4))

       fc(1:4) = 1.d0 + Gradation_Factor * dx * dsqrt(fMetrics(1,1:4))
       fc(1:4) = 1.d0 / (fc(1:4)*fc(1:4))

       fMetric1 = fMetrics(:,1)
       fMetric2 = fMetrics(:,2) * fc(1)
       Value_Quad_Node(1:3,ip(1)) = Intersect_Metric2D(fMetric1,fMetric2)

       fMetric1 = fMetrics(:,2)
       fMetric2 = fMetrics(:,1) * fc(2)
       Value_Quad_Node(1:3,ip(2)) = Intersect_Metric2D(fMetric1,fMetric2)

       fMetric1 = fMetrics(:,3)
       fMetric2 = fMetrics(:,4) * fc(3)
       Value_Quad_Node(1:3,ip(3)) = Intersect_Metric2D(fMetric1,fMetric2)

       fMetric1 = fMetrics(:,4)
       fMetric2 = fMetrics(:,3) * fc(4)
       Value_Quad_Node(1:3,ip(4)) = Intersect_Metric2D(fMetric1,fMetric2)

       !--- y- direction

       fMetrics(:,1:4) = Value_Quad_Node(1:3,ip(1:4))

       fc(1:4) = 1.d0 + Gradation_Factor * dy * dsqrt(fMetrics(3,1:4))
       fc(1:4) = 1.d0 / (fc(1:4)*fc(1:4))

       fMetric1 = fMetrics(:,1)
       fMetric2 = fMetrics(:,3) * fc(1)
       Value_Quad_Node(1:3,ip(1)) = Intersect_Metric2D(fMetric1,fMetric2)

       fMetric1 = fMetrics(:,3)
       fMetric2 = fMetrics(:,1) * fc(3)
       Value_Quad_Node(1:3,ip(3)) = Intersect_Metric2D(fMetric1,fMetric2)

       fMetric1 = fMetrics(:,2)
       fMetric2 = fMetrics(:,4) * fc(2)
       Value_Quad_Node(1:3,ip(2)) = Intersect_Metric2D(fMetric1,fMetric2)

       fMetric1 = fMetrics(:,4)
       fMetric2 = fMetrics(:,2) * fc(4)
       Value_Quad_Node(1:3,ip(4)) = Intersect_Metric2D(fMetric1,fMetric2)

       !--- diagonal- direction

       fMetrics(:,1:4) = Value_Quad_Node(1:3,ip(1:4))

       fc(1) = dsqrt(  fMetric(1,1) *dx*dx + 2.D0* fMetric(2,1) *dx*dy    &
            + fMetric(3,1) *dy*dy )
       fc(2) = dsqrt(  fMetric(1,2) *dx*dx - 2.D0* fMetric(2,2) *dx*dy    &
            + fMetric(3,2) *dy*dy )
       fc(3) = dsqrt(  fMetric(1,3) *dx*dx - 2.D0* fMetric(2,3) *dx*dy    &
            + fMetric(3,3) *dy*dy )
       fc(4) = dsqrt(  fMetric(1,4) *dx*dx + 2.D0* fMetric(2,4) *dx*dy    &
            + fMetric(3,4) *dy*dy )
       fc(1:4) = 1.d0 + Gradation_Factor * fc(1:4)
       fc(1:4) = 1.d0 / (fc(1:4)*fc(1:4))

       fMetric1 = fMetrics(:,1)
       fMetric2 = fMetrics(:,4) * fc(1)
       Value_Quad_Node(1:3,ip(1)) = Intersect_Metric2D(fMetric1,fMetric2)

       fMetric1 = fMetrics(:,4)
       fMetric2 = fMetrics(:,1) * fc(4)
       Value_Quad_Node(1:3,ip(4)) = Intersect_Metric2D(fMetric1,fMetric2)

       fMetric1 = fMetrics(:,2)
       fMetric2 = fMetrics(:,3) * fc(2)
       Value_Quad_Node(1:3,ip(2)) = Intersect_Metric2D(fMetric1,fMetric2)

       fMetric1 = fMetrics(:,3)
       fMetric2 = fMetrics(:,2) * fc(3)
       Value_Quad_Node(1:3,ip(3)) = Intersect_Metric2D(fMetric1,fMetric2)


    ENDIF

    RETURN
  END SUBROUTINE GRADATION_Quad_Grid

  !******************************************************************************!


END SUBROUTINE Quad_Background


