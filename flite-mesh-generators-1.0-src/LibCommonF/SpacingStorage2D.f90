!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Definition of background spacing with basic functions.  
!<
MODULE SpacingStorage2D
  USE QuadtreeMesh
  USE SurfaceMeshStorage
  USE Metric2D
  USE ScalarCalculator
  IMPLICIT NONE

  TYPE :: SpacingStorage2DType

     !>   (an)isotropic index.
     !!   @param  Model      =  1 :   isotropic domain, fixed GridSize.         \n
     !!                      = -1 : anisotropic domain, fixed stretching.       \n
     !!                      =  2 :   isotropic domain with Tri. background.    \n
     !!                      = -2 : anisotropic domain with Tri. background.    \n
     !!                      =  3 :   isotropic domain with Oct. background.    \n
     !!                      = -3 : anisotropic domain with Oct. background.    \n
     !!                      =  4 :   isotropic domain with source only.        \n
     !!                      = -4 : anisotropic domain with source only.        \n
     !!                      else : error
     !<
     INTEGER :: Model          = 1
     REAL*8  :: BasicSize      = 1.d0                 !<  Basic grid-size
     REAL*8  :: MinSize        = 1.d0                 !<  Minimun grid-size
     REAL*8  :: BasicMetric(3) = (/1,0,1/)            !<  Basic mapping
     
     REAL*8  :: pMin(2) =  1.d36        !<   The position of the minimum corner.
     REAL*8  :: pMax(2) = -1.d36        !<   The position of the maximum corner.

     TYPE(SurfaceMeshStorageType)  :: TriMesh    !<  triangle mesh.

     REAL*8, DIMENSION(:,:), POINTER :: Metric_Point => null()     !<  (3,*) : mapping at each tet. mesh node.
     REAL*8, DIMENSION(:  ), POINTER :: Scalar_Point => null()     !<  scalar at each tet. mesh node.

     TYPE (QuadtreeMesh_Type) :: Quadtree       !<   Quadtree mesh.
     REAL*8, DIMENSION(:,:), POINTER :: Metric_OctNode => null()    !<  (3,*)   : mapping at each Quadtree node.
     REAL*8, DIMENSION(  :), POINTER :: Scalar_OctNode => null()    !<   scalar at each Quadtree  node.
     REAL*8, DIMENSION(:,:), POINTER :: Metric_OctCell => null()    !<  (3,*) : mapping at each Quadtree cell.
     REAL*8, DIMENSION(  :), POINTER :: Scalar_OctCell => null()    !<   scale at each Quadtree cell.

     !>  The level of debug and display.
     !!   A bigger number indicates more checking in the code
     !!        and more output on the screen.
     INTEGER :: CheckLevel = 0

     !>      The method to interpolate two mappings or surface scale.
     !!
     !!      For anisotropic problem, interpolate a mapping by:
     !!  @param Interp_Method
     !!         =1,  by using simultaneous matrix reduction.                   \n
     !!         =2,  by using a matrix power -1, so large grid sizes dominate. \n
     !!         =3,  by using a matrix power  1, so small grid sizes dominate.
     !!
     !!      For isotropic problem, the interpolate scale by:
     !!  @param Interp_Method
     !!         =1,  by using geometric  mean.                                 \n
     !!         =2,  by using harmonic   mean, so large grid sizes dominate.   \n
     !!         =3,  by using arithmetic mean, so small grid sizes dominate.
     INTEGER :: Interp_Method = 2

     !>
     !!  @param OctCell_Vary
     !!         =.FALSE., set the value of a point by the value of the Cube_grid
     !!                   in which the point lies.                                  \n
     !!         =.TRUE.,  set the value of a point by interpolating the Cube_grid
     !!                   in which the point lies. This is a more precise but slower way.
     LOGICAL :: OctCell_Vary = .FALSE.
     
     !>  Factor of background spacing gradation (0.1~3.0).
     !!      A small value results in a strong gradting and a smoother mesh.
     !!      If >=3.0, no gradation.
     REAL*8  :: Gradation_Factor = 4.d0
  END TYPE SpacingStorage2DType

CONTAINS

  !*******************************************************************************
  !>
  !!   Calculate the space mapping at a specific position from the background mesh.
  !!   @param[in] BGSpacing : the background spacing
  !!   @param[in] P         : the position.
  !!   @param[in] inDomain  =.true.  : if P is not in domain, return (for TriMesh);     \n
  !!                        =.false. or not present : if P is not in domain, carry on
  !!                          the computation by expolating (for TriMesh).
  !!   @param[out] fMetric     : the mapping at P
  !!   @param[out] inDomain =.true.  point P is in the background domain;     \n
  !!                        =.false. point P is NOT in the background domain,
  !!                                 and return fMetric by BGSpacing%BasicMetric.
  !!
  !!   Remind: the outputs are dimensionless which differs from
  !!           SUBROUTINE SpacingStorage2D_GetMappingGridSize().
  !<     
  !*******************************************************************************
  SUBROUTINE SpacingStorage2D_GetMetric(BGSpacing, P, fMetric, inDomain)
    IMPLICIT NONE
    TYPE(SpacingStorage2DType), INTENT(IN) :: BGSpacing
    REAL*8, INTENT(IN)  :: P(2)
    REAL*8, INTENT(OUT) :: fMetric(3)
    LOGICAL,INTENT(INOUT), OPTIONAL :: inDomain

    INTEGER :: IT,ip3(3),k,i, Isucc, iso
    REAL*8  :: fMs(3,3), w3(3)
    REAL*8  :: p1(2), p2(2), p3(2), tol
    INTEGER,SAVE :: showWarn1 = 1
    LOGICAL :: inDo

    inDo = .FALSE.
    IF(PRESENT(inDomain))THEN
       inDo     = inDomain
       inDomain = .TRUE.
    ENDIF
    
    IF(BGSpacing%Model==-1)THEN

       fMetric = BGSpacing%BasicMetric(:)

    ELSE IF(BGSpacing%Model==-2)THEN

       !--- Tri. background applied.
       tol = 1.d-8
       CALL Surf2D_SearchTri(BGSpacing%TriMesh, P,IT, Isucc, tol)
       
       IF(IT==0)THEN
          !--fail to search
          IF(showWarn1>0)THEN
             showWarn1 = showWarn1 -1
             WRITE(*,*)'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
             WRITE(*,*)'Warning --- SpacingStorage2D_GetMetric:',       &
                     ' a point is out of background tetrahedra'
             WRITE(*,'(a,3E13.5)')'    P=',P(:)
             WRITE(*,*)'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
          ENDIF
          fMetric = BGSpacing%BasicMetric
          IF(PRESENT(inDomain)) inDomain = .FALSE.
          RETURN
       ENDIF

       ip3(:) = BGSpacing%TriMesh%IP_Tri(1:3,IT)
       p1     = BGSpacing%TriMesh%Coord(:,ip3(1))
       p2     = BGSpacing%TriMesh%Coord(:,ip3(2))
       p3     = BGSpacing%TriMesh%Coord(:,ip3(3))
       
       W3 = Tri_GetWeight(P1, P2, P3, P)

       DO i=1,3
          IF(w3(i)<0.d0)THEN
             w3(:) = w3(:) /(1.d0-w3(i))
             w3(i) = 0.d0
          ENDIF
       ENDDO

       fms(:,1) = BGSpacing%Metric_Point(:,ip3(1))
       fms(:,2) = BGSpacing%Metric_Point(:,ip3(2))
       fms(:,3) = BGSpacing%Metric_Point(:,ip3(3))

       fMetric = Metric2D_Mean_Weight(3,fMs,w3)

    ELSE IF(BGSpacing%Model==-3)THEN

       CALL Error_Stop('--- under constructured ---')

    ELSE IF(BGSpacing%Model==-4)THEN
       CALL Error_Stop('--- under constructured ---')
    ELSE
       WRITE(*,*)'Error--- illegal Spacing Model: ',BGSpacing%Model, '       '
       CALL Error_STOP ( '--- SpacingStorage2D_GetMetric')
    ENDIF

    RETURN
  END SUBROUTINE SpacingStorage2D_GetMetric

  !*******************************************************************************
  !>
  !!   Calculate the space Scalar at a specific position from the background mesh.
  !!   @param[in] BGSpacing : the background spacing
  !!   @param[in] P         : the position.
  !!   @param[in] inDomain  =.true.  : if P is not in domain, return (for TriMesh);     \n
  !!                        =.false. or not present : if P is not in domain, carry on
  !!                          the computation by expolating (for TriMesh).
  !!   @param[out] Scalar   : the Scalar  at P.
  !!   @param[out] inDomain =.true.  point P is in the background domain;     \n
  !!                        =.false. point P is NOT in the background domain,
  !!                                 and return Scalar by 1.0
  !!
  !!   Remind: the outputs are dimensionless which differs from
  !!           SUBROUTINE SpacingStorage2D_GetMinGridSize().
  !<     
  !*******************************************************************************
  SUBROUTINE SpacingStorage2D_GetScale(BGSpacing, P, Scalar, inDomain)
    IMPLICIT NONE
    TYPE(SpacingStorage2DType), INTENT(IN) :: BGSpacing
    REAL*8, INTENT(IN)  :: P(2)
    REAL*8, INTENT(OUT) :: Scalar
    LOGICAL,INTENT(INOUT), OPTIONAL :: inDomain

    INTEGER :: IT,ip3(3),k,i, Isucc, iso
    REAL*8  :: w3(3), Scalars(3)
    REAL*8  :: p1(2), p2(2), p3(2), tol
    INTEGER,SAVE :: showWarn1 = 1
    LOGICAL :: inDo

    inDo = .FALSE.
    IF(PRESENT(inDomain))THEN
       inDo     = inDomain
       inDomain = .TRUE.
    ENDIF

    IF(BGSpacing%Model==1)THEN
       Scalar = 1.d0
    ELSE IF(BGSpacing%Model==2)THEN
    
       !--- Tri. background applied.
       tol = 1.d-8
       CALL Surf2D_SearchTri(BGSpacing%TriMesh, P,IT, Isucc, tol)
       
       IF(IT==0)THEN
          !--fail to search
          IF(showWarn1>0)THEN
             showWarn1 = showWarn1 -1
             WRITE(*,*)'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
             WRITE(*,*)'Warning --- SpacingStorage2D_GetMetric:',       &
                     ' a point is out of background tetrahedra'
             WRITE(*,'(a,3E13.5)')'    P=',P(:)
             WRITE(*,*)'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
          ENDIF
          Scalar = 1.d0
          IF(PRESENT(inDomain)) inDomain = .FALSE.
          RETURN
       ENDIF

       ip3(:) = BGSpacing%TriMesh%IP_Tri(1:3,IT)
       p1     = BGSpacing%TriMesh%Coord(:,ip3(1))
       p2     = BGSpacing%TriMesh%Coord(:,ip3(2))
       p3     = BGSpacing%TriMesh%Coord(:,ip3(3))
       
       W3 = Tri_GetWeight(P1, P2, P3, P)

       DO i=1,3
          IF(w3(i)<0.d0)THEN
             w3(:) = w3(:) /(1.d0-w3(i))
             w3(i) = 0.d0
          ENDIF
       ENDDO

       Scalars(1:3) = BGSpacing%Scalar_Point(ip3(1:3))
       Scalar = Scalar_Mean_Weight(3,Scalars,w3,BGSpacing%Interp_Method)
    
    ELSE IF(BGSpacing%Model==3)THEN

       CALL Error_Stop('--- under constructured ---')

    ELSE IF(BGSpacing%Model==4)THEN
       CALL Error_Stop('--- under constructured ---')
    ELSE
       WRITE(*,*)'Error--- illegal Spacing Model: ',BGSpacing%Model, '       '
       CALL Error_STOP ( '--- SpacingStorage2D_GetScale')
    ENDIF

    RETURN
  END SUBROUTINE SpacingStorage2D_GetScale



  !>
  !!  Output the information of the spacing.
  !!  @param[in] BGSpacing  : the sbackground spacing.
  !!  @param[in] io         : the IO channel. 
  !!                         If =6 or not present, output on screen. 
  !!  @param[out] isReady   : if the spacing is ready.
  !<
  SUBROUTINE SpacingStorage2D_info(BGSpacing, io, isReady)
    TYPE(SpacingStorage2DType), INTENT(IN) :: BGSpacing
    INTEGER, INTENT(IN),  OPTIONAL :: io
    LOGICAL, INTENT(OUT), OPTIONAL :: isReady
    INTEGER :: ic
    LOGICAL :: ch
    ic = 6
    IF(PRESENT(io)) ic = io
    IF(PRESENT(isReady)) isReady = .TRUE.

    WRITE(ic,*)' Information of Background Spacing '
    WRITE(ic,*)'  MODEL: ', BGSpacing%Model
    WRITE(ic,*)'  Global & Minimum GridSize: ',    &
               REAL(BGSpacing%BasicSize), REAL(BGSpacing%MinSize)
    IF(BGSpacing%Model==1) THEN
       WRITE(ic,*)'  An isotropic space with fixed GridSize. '
    ELSE IF(BGSpacing%Model==-1) THEN
       WRITE(ic,*)'  An anisotropic space with fixed GridSize and space mapping. '
       WRITE(ic,*)'  Global Space Mapping:'
       WRITE(ic,*)'      ', REAL(BGSpacing%BasicMetric(:))
       RETURN
    ELSE IF(ABS(BGSpacing%Model)==2) THEN
       IF(BGSpacing%Model==2) THEN
          WRITE(ic,*)'  An isotropic space with tetrahedral mesh.'
       ELSE
          WRITE(ic,*)'  An anisotropic space with tetrahedral mesh.'
       ENDIF
       WRITE(ic,*)'  Interpolation Method: ', BGSpacing%Interp_Method
    ELSE IF(ABS(BGSpacing%Model)==3) THEN
    ELSE IF(ABS(BGSpacing%Model)==4) THEN
    ELSE
       WRITE(ic,*)'  Warning--- An illegal space'
       IF(PRESENT(isReady)) isReady = .FALSE.
    ENDIF
    
  END SUBROUTINE SpacingStorage2D_info



END MODULE SpacingStorage2D


