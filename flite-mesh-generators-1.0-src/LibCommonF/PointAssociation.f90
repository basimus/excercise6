!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!    A module for running type PointAssociation::PointAssociationArray.
!!    It is being replaced by MODULE LinkAssociation
!<
MODULE PointAssociation

  IMPLICIT NONE

  INTEGER, PARAMETER :: PointAssociation_ArrayIncrease    = 12
  INTEGER, PARAMETER :: PointAssociation_ArrayBeginLength = 36
  INTEGER, PARAMETER :: PointAssociation_nallc_increase   = 10000
  INTEGER, TARGET    :: PointAssociation_zero             = 0

  TYPE :: PointAssociationType
     INTEGER :: numCells=0
     INTEGER, DIMENSION(:), POINTER :: CellList => null()
  END TYPE PointAssociationType

  !>
  !!    A structure storing the links between points and its connected elements.
  !!    Here a element can be a point, a edge, a triangle, a tetrahedron or else.
  !!    The type of element is controled by Norder used in building, searching,
  !!    and so on. Keep this Norder unchanged throughout.
  !<
  TYPE PointAssociationArray
     INTEGER :: numPoints=0
     TYPE (PointAssociationType), DIMENSION(:), POINTER :: PointList => null()
  END TYPE PointAssociationArray

CONTAINS

  !>
  !!   Check if the association has been allocated.
  !<
  FUNCTION PointAssociation_Exist(PointAsso) RESULT(YesNo)
    IMPLICIT NONE
    LOGICAL :: YesNo
    TYPE (PointAssociationArray) PointAsso

    IF(PointAsso%numPoints>0)THEN
       YesNo = .TRUE.
    ELSE
       YesNo = .FALSE.
    ENDIF
  END FUNCTION PointAssociation_Exist

  FUNCTION PointAssociation_Target(IP,N,PointAsso) RESULT(Cell)
    IMPLICIT NONE
    INTEGER :: Cell, IP, N
    TYPE (PointAssociationArray) PointAsso

    IF(IP>PointAsso%numPoints)THEN
       Cell = 0
    ELSE IF(N>PointAsso%PointList(IP)%numCells)THEN
       Cell = 0
    ELSE
       Cell = PointAsso%PointList(IP)%CellList(N)
    ENDIF
  END FUNCTION PointAssociation_Target

  !>
  !!   Get the list of elements which are associated with a point.
  !!   @param[in]  IP            the ID of the point.
  !!   @param[in]  PointAsso     the association.
  !!   @param[out] List_Length   the number of elements associated with the point.
  !!   @param[out] CellList      the list of elements associated with the point.
  !!
  !!   Reminder: List_Length is an integer pointer, and CellList is an 
  !!             interger array pointer. Modify them anywhere may change
  !!             the structure of the association. Vice versa. i.e. \n
  !!       List_Length => PointAsso\%PointList(IP)\%numCells    \n
  !!       CellList    => PointAsso\%PointList(IP)\%CellList
  !<
  SUBROUTINE PointAssociation_List(IP, List_Length, CellList, PointAsso)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: IP
    INTEGER, POINTER :: List_Length
    INTEGER, DIMENSION(:), POINTER :: CellList
    TYPE (PointAssociationArray), INTENT(IN) :: PointAsso

    IF(IP>PointAsso%numPoints)THEN
       List_Length => PointAssociation_zero
    ELSE
       List_Length => PointAsso%PointList(IP)%numCells
       CellList    => PointAsso%PointList(IP)%CellList
    ENDIF

    RETURN
  END SUBROUTINE PointAssociation_List

  FUNCTION PointAssociatedWith(Point,Cell) RESULT(Asso)
    IMPLICIT NONE
    TYPE (PointAssociationType) :: Point
    INTEGER :: Cell
    LOGICAL :: Asso
    INTEGER :: i

    DO i=1,Point%numCells
       IF(Point%CellList(i)==Cell)THEN
          Asso = .TRUE.
          RETURN
       ENDIF
    ENDDO
    Asso = .FALSE.
    RETURN

  END FUNCTION PointAssociatedWith


  !>
  !!    Private functions
  !<
  SUBROUTINE PointAssociationType_Add(Point, Cell) 
    USE array_allocator 
    IMPLICIT NONE
    TYPE (PointAssociationType) :: Point
    INTEGER, INTENT(IN) :: Cell
    INTEGER :: Length, sizeOld, sizeNew

    Point%numCells = Point%numCells+1
    Length = Point%numCells

    IF(ASSOCIATED(Point%CellList))THEN
       sizeOld = SIZE(Point%CellList)
    ELSE
       sizeOld = 0
    ENDIF

    IF(Length>sizeOld)THEN
       IF(sizeOld==0)THEN
          sizeNew = PointAssociation_ArrayBeginLength
       ELSE    
          sizeNew = sizeOld + PointAssociation_ArrayIncrease
       ENDIF
       CALL allc_1Dpointer(Point%CellList, sizeNew, 'Point')
       Point%CellList(sizeOld+1:sizeNew) = 0
    ENDIF

    Point%CellList(Length) = Cell

  END SUBROUTINE PointAssociationType_Add

  !>
  !!    Private functions
  !<
  SUBROUTINE PointAssociationType_Remove(Point, Cell, ISucc) 
    IMPLICIT NONE
    TYPE (PointAssociationType) :: Point
    INTEGER, INTENT(IN) :: Cell
    INTEGER, INTENT(OUT) :: ISucc
    INTEGER :: id, Length, sizeOld, sizeNew

    Length = Point%numCells
    DO id=1,Length
       IF(Point%CellList(id)==Cell)  EXIT
    ENDDO

    IF(id>Length)THEN
       Isucc=0
       RETURN
    ENDIF

    IF(id<Length)THEN
       Point%CellList(id) = Point%CellList(Length)
    ENDIF

    Point%CellList(Length) = 0
    Point%numCells = Point%numCells - 1
    Isucc=1
    RETURN

  END SUBROUTINE PointAssociationType_Remove

  !>
  !!   Allocate memory for the association.
  !!     This is optional function. Using it before adding components
  !!     ( i.e. CALL PointAssociationType_Add() ), may save CPU time to allocate
  !!     memory.
  !!
  !!   Reminder:  This routine is contained in SUBROUTINE PointAssociation_Build(), 
  !!              and calling this subroutine WILL destroy the exsiting
  !!              association.
  !!   @param[in]  NB_Point   the number of points.
  !!   @param[out] PointAsso  the association.
  !<
  SUBROUTINE PointAssociation_Allocate(NB_Point, PointAsso)
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  NB_Point
    TYPE (PointAssociationArray), INTENT(INOUT) :: PointAsso
    CALL PointAssociation_Clear(PointAsso)
    PointAsso%numPoints = NB_Point
    ALLOCATE (PointAsso%PointList(NB_Point))
    RETURN
  END SUBROUTINE PointAssociation_Allocate

  !>
  !!   Build the association.
  !!   @param[in]  Norder     the integer controlling the shape of element.
  !!   @param[in]  NB_Cell    the number of elements
  !!   @param[in]  IP_Cell(Norder,*)   the connectivity of elements
  !!   @param[in]  NB_Point   the number of points.
  !!   @param[in]  PointAsso  the association.
  !!   @param[out] PointAsso  the revised association.
  !<
  SUBROUTINE PointAssociation_Build(Norder, NB_Cell, IP_Cell, NB_Point, PointAsso)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Norder, NB_Cell, IP_Cell(Norder,*), NB_Point
    TYPE (PointAssociationArray), INTENT(INOUT) :: PointAsso
    INTEGER :: Cell,IP,i
    TYPE (PointAssociationType), POINTER :: pPoint

    CALL PointAssociation_Clear(PointAsso)

    PointAsso%numPoints = NB_Point
    ALLOCATE (PointAsso%PointList(PointAsso%numPoints))

    DO Cell = 1,NB_Cell
       DO i = 1,Norder
          IP = IP_Cell(i,Cell)
          pPoint => PointAsso%PointList(IP)
          CALL PointAssociationType_Add(pPoint,Cell) 
       ENDDO
    ENDDO

    RETURN
  END SUBROUTINE PointAssociation_Build

  !>                               
  !!    Private function : copy PointAssociationArray TT1 to TT2.   \n                                     
  !!    Reminder: Allocate TT2 in advance.                                         
  !<
  SUBROUTINE PointAssociation_Copy(TT1, TT2)
    USE array_allocator 
    IMPLICIT NONE
    TYPE (PointAssociationArray), INTENT(IN) :: TT1
    TYPE (PointAssociationArray), INTENT(INOUT) :: TT2
    INTEGER :: IP, Length, sizeNew
    TYPE (PointAssociationType), POINTER :: pPoint1, pPoint2

    Length = TT1%numPoints
    IF(SIZE(TT2%PointList)<Length) CALL Error_STOP ( 'Error---PointAssociation_Copy')

    DO IP=1,Length
       pPoint1 => TT1%PointList(IP)
       pPoint2 => TT2%PointList(IP)
       sizeNew = SIZE(pPoint1%CellList)
       CALL allc_1Dpointer(pPoint2%CellList, sizeNew, 'pPoint')
       pPoint2%CellList(1:sizeNew) = pPoint1%CellList(1:sizeNew)
       pPoint2%numCells = pPoint1%numCells
    ENDDO
    TT2%numPoints = Length

    RETURN
  END SUBROUTINE PointAssociation_Copy

  !>
  !!   Add a new element to the association.
  !!   @param[in]  Norder     the length of input array IPs.
  !!   @param[in]  Cell       the ID of the new element.
  !!   @param[in]  IPs(Norder) the connectivity of the new element.
  !!   @param[in]  PointAsso  the association.
  !!   @param[out] PointAsso  the revised association.
  !<
  SUBROUTINE PointAssociation_Add(Norder, Cell, IPs, PointAsso)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Norder, Cell, IPs(Norder)
    TYPE (PointAssociationArray), INTENT(INOUT) :: PointAsso
    INTEGER :: IP,i,sizeNew
    TYPE (PointAssociationArray) :: PointAsso_temp
    TYPE (PointAssociationType), POINTER :: pPoint

    DO i = 1,Norder

       IP = IPs(i)
       IF(PointAsso%numPoints==0)THEN
          sizeNew = IP + PointAssociation_nallc_increase
          ALLOCATE (PointAsso%PointList(sizeNew))
       ELSE IF(IP>SIZE(PointAsso%PointList))THEN
          ALLOCATE (PointAsso_temp%PointList(PointAsso%numPoints))
          CALL PointAssociation_Copy( PointAsso, PointAsso_temp)
          CALL PointAssociation_Clear(PointAsso)

          sizeNew = IP + PointAssociation_nallc_increase
          ALLOCATE (PointAsso%PointList(sizeNew))
          CALL PointAssociation_Copy( PointAsso_temp, PointAsso)
          CALL PointAssociation_Clear(PointAsso_temp)
       ENDIF

       IF(IP>PointAsso%numPoints)THEN
          PointAsso%numPoints = IP
       ENDIF

       pPoint => PointAsso%PointList(IP)
       IF(PointAssociatedWith(pPoint,Cell)) CYCLE

       CALL PointAssociationType_Add(pPoint,Cell) 

    ENDDO

    RETURN
  END SUBROUTINE PointAssociation_Add

  !>
  !!   Remove an element from the association.
  !!   @param[in]  Norder     the length of input array IPs.
  !!   @param[in]  Cell       the ID of the element being removed.
  !!   @param[in]  IPs(Norder) the connectivity of the element being removed.
  !!   @param[in]  PointAsso  the association.
  !!   @param[out] PointAsso  the revised association.
  !<
  SUBROUTINE PointAssociation_Remove(Norder, Cell, IPs, PointAsso)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Norder, Cell, IPs(Norder)
    TYPE (PointAssociationArray), INTENT(INOUT) :: PointAsso
    INTEGER :: IP,i, Isucc
    TYPE (PointAssociationType), POINTER :: pPoint

    DO i = 1,Norder
       IP = IPs(i)
       pPoint   => PointAsso%PointList(IP)
       CALL PointAssociationType_Remove(pPoint,Cell,ISucc)   

       IF(Isucc==0)THEN
          WRITE(*,*)'Error--- : Cell is not in the list'
          WRITE(*,*)'IP,Cell=',IP,Cell
          CALL Error_STOP ( '--- PointAssociation_Remove')
       ENDIF
    ENDDO

    RETURN
  END SUBROUTINE PointAssociation_Remove

  !>
  !!   Check the structure of the association
  !!   @param[in]  Norder     the integer controlling the shape of element.
  !!   @param[in]  PointAsso  the association.
  !!   @param[out] Isucc      index for success.                               \n
  !!                = 1   successful.                                          \n
  !!                = -1  a point is associated to a none-cell.                \n
  !!                = -2  a point is associated to a cell more than once.      \n
  !!                = -3  more than Norder points are associated to one cell.  \n
  !!                = -4  less than Norder points are associated to one cell
  !<
  SUBROUTINE PointAssociation_Check(Norder, PointAsso, Isucc)
    USE array_allocator 
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Norder
    TYPE (PointAssociationArray), INTENT(IN) :: PointAsso
    INTEGER, INTENT(OUT) :: Isucc
    INTEGER :: i, n, ip, it, NB_Cell, nallc, nallc1
    INTEGER, DIMENSION(:,:),  POINTER :: IP_Cell

    nallc = 10 * PointAssociation_nallc_increase
    CALL allc_2Dpointer(IP_Cell, Norder, nallc, 'PointAsso_Check')
    IP_Cell(:,:) = 0
    NB_Cell = 0

    DO ip = 1,PointAsso%numPoints
       DO i = 1,PointAsso%PointList(ip)%numCells
          it = PointAsso%PointList(ip)%CellList(i)
          IF(it<=0)THEN
             !--associated with a negative ID
             Isucc = -1
             RETURN
          ENDIF

          NB_Cell = MAX(NB_Cell, it)
          IF(it>nallc)THEN
             nallc1 = nallc
             nallc  = it + PointAssociation_nallc_increase
             CALL allc_2Dpointer(IP_Cell, Norder, nallc, 'PointAsso_Check')
             IP_Cell(:,nallc1+1:nallc) = 0
          ENDIF
          DO n=1,Norder
             IF(IP_Cell(n,it)==ip)THEN
                !---'multi associated'
                Isucc = -2
                RETURN
             ENDIF
             IF(IP_Cell(n,it)==0) EXIT
          ENDDO
          IF(n>Norder)THEN
             !--- exceed Norder
             Isucc = -3
             RETURN
          ENDIF
          IP_Cell(n,it) = ip
       ENDDO
    ENDDO

    DO it=1,NB_Cell
       IF(IP_Cell(1,it)==0) CYCLE
       IF(IP_Cell(Norder,it)==0)THEN
          !--- 'deficit associated'
          Isucc = -4
          RETURN
       ENDIF
    ENDDO

    Isucc = 1
    DEALLOCATE(IP_Cell)

    RETURN
  END SUBROUTINE PointAssociation_Check

  !>
  !!   Delete the association and release memory.
  !<
  SUBROUTINE PointAssociation_Clear(PointAsso)
    IMPLICIT NONE
    TYPE (PointAssociationArray), INTENT(INOUT) :: PointAsso
    TYPE (PointAssociationType), POINTER :: pPoint
    INTEGER :: IP
    IF(ASSOCIATED(PointAsso%PointList))THEN 
       DO  IP = 1, SIZE(PointAsso%PointList)
          pPoint => PointAsso%PointList(IP)
          IF(ASSOCIATED(pPoint%CellList)) DEALLOCATE(pPoint%CellList)
       ENDDO
       DEALLOCATE(PointAsso%PointList)
    ENDIF
    PointAsso%numPoints = 0
    RETURN
  END SUBROUTINE PointAssociation_Clear


END MODULE PointAssociation

