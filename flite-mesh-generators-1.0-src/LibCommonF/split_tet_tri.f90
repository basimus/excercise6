!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Split up to 6 edges of a tetrhedron.
!!  Refer module CellConnectivity for the order of nodes and edges.
!!
!!  @param[in]  IP_OldTet : the four nodes of the tetrahedron.
!!  @param[in]  edgediv (i)=ip>0 : the i-th egde need to be divided by node ip.  \n
!!                      (i)=0    : the i-th egde do NOT need to be divided.
!!  @param[in]  edgelen  : the length of each egde. 
!!                         This array will not be refered 
!!                         if there is only one divided edge.
!!  @param[out]  NB_NewTet :  Number of new Tetrahedra.
!!  @param[out]  IP_NewTet :  Nodes of each new Tetrahedron.
!<

      RECURSIVE SUBROUTINE Tet_Divider                                         &
               (IP_OldTet, edgediv, edgelen, NB_NewTet, IP_NewTet)
      USE CellConnectivity
      IMPLICIT NONE

      INTEGER :: IP_OldTet(4), edgediv(6)
      REAL*8  :: edgelen(6)
      INTEGER :: NB_NewTet, IP_NewTet(4,8)
      INTEGER :: Ndiv, i, j, k, iedd(6), ipold(4), ipnew(6)
      INTEGER :: idx(2,6)
      INTEGER :: temp_old(4),temp_div(6),itemp,temp_new(4,8), ip10(10)
      REAL*8  :: xh1, xh2, xh3

      idx(1:2,1:6) = I_Comb_Tet(1:2,1:6)

      Ndiv=0
      DO i=1,6
         IF(edgediv(i)>0)Ndiv=Ndiv+1
      ENDDO

      DO i=1,8
        IP_NewTet(:,i)=IP_OldTet(:)
      ENDDO

      SELECT CASE(Ndiv)
      CASE(0)

         NB_NewTet=1

      CASE(1)

         Do i=1,6
            IF(edgediv(i)>0) iedd(1)=i
         ENDDO
         ipnew(1)=edgediv(iedd(1))
         NB_NewTet=2
         IP_NewTet(idx(1,iedd(1)),1)=ipnew(1)
         IP_NewTet(idx(2,iedd(1)),2)=ipnew(1)

      CASE(2)

         Do i=1,6
            IF(edgediv(i)>0) then
              iedd(3-Ndiv)=i
              Ndiv=Ndiv-1
              IF(Ndiv==0)exit
            ENDIF
         ENDDO
         IF( (iedd(1)==1 .and. iedd(2)==6) .or.      &
             (iedd(1)==2 .and. iedd(2)==4) .or.      &
             (iedd(1)==3 .and. iedd(2)==5) )THEN
!---- here two divider points are NOT on the same triangle

             ipnew(1:2)=edgediv(iedd(1:2))
             NB_NewTet=4
             IP_NewTet(idx(1,iedd(1)),1)=ipnew(1)
             IP_NewTet(idx(1,iedd(2)),1)=ipnew(2)
             IP_NewTet(idx(1,iedd(1)),2)=ipnew(1)
             IP_NewTet(idx(2,iedd(2)),2)=ipnew(2)
             IP_NewTet(idx(2,iedd(1)),3)=ipnew(1)
             IP_NewTet(idx(1,iedd(2)),3)=ipnew(2)
             IP_NewTet(idx(2,iedd(1)),4)=ipnew(1)
             IP_NewTet(idx(2,iedd(2)),4)=ipnew(2)
         ELSE

             NB_NewTet=3
             IF(edgelen(iedd(1))<edgelen(iedd(2)))then
               j=iedd(1)
               iedd(1)=iedd(2)
               iedd(2)=j
             ENDIF
!---- bisect into two parts first
             ipnew(1:2)=edgediv(iedd(1:2))
             IP_NewTet(idx(1,iedd(1)),1)=ipnew(1)
             IP_NewTet(idx(2,iedd(1)),2)=ipnew(1)

!---- check which part need to be bisected further
             j=0
             DO i=1,4
                if( IP_NewTet(i,2)==IP_OldTet(idx(1,iedd(2))) ) j=j+1
                if( IP_NewTet(i,2)==IP_OldTet(idx(2,iedd(2))) ) j=j+1
             ENDDO

             IP_NewTet(idx(j,iedd(1)),j)=ipnew(1)
             IP_NewTet(idx(1,iedd(2)),j)=ipnew(2)
             IP_NewTet(idx(j,iedd(1)),3)=ipnew(1)
             IP_NewTet(idx(2,iedd(2)),3)=ipnew(2)
         ENDIF

      CASE(3)

         Do i=1,6
            IF(edgediv(i)>0) then
              iedd(4-Ndiv)=i
              Ndiv=Ndiv-1
              IF(Ndiv==0)exit
            ENDIF
         ENDDO

         ip10(1:4)  = IP_OldTet(1:4)
         ip10(5:10) = edgediv(1:6)
         NB_NewTet  = 4
         IF      (iedd(1)==1 .and. iedd(2)==2 .and. iedd(3)==3)THEN
             IP_NewTet(:,1)=(/ip10(1), ip10(5), ip10(7), ip10(4)/)
             IP_NewTet(:,2)=(/ip10(5), ip10(2), ip10(6), ip10(4)/)
             IP_NewTet(:,3)=(/ip10(7), ip10(6), ip10(3), ip10(4)/)
             IP_NewTet(:,4)=(/ip10(5), ip10(6), ip10(7), ip10(4)/)
         ELSE IF (iedd(1)==1 .and. iedd(2)==4 .and. iedd(3)==5)THEN
             IP_NewTet(:,1)=(/ip10(1), ip10(5), ip10(3), ip10(8)/)
             IP_NewTet(:,2)=(/ip10(5), ip10(2), ip10(3), ip10(9)/)
             IP_NewTet(:,3)=(/ip10(8), ip10(9), ip10(3), ip10(4)/)
             IP_NewTet(:,4)=(/ip10(8), ip10(5), ip10(3), ip10(9)/)
         ELSE IF (iedd(1)==2 .and. iedd(2)==5 .and. iedd(3)==6)THEN
             IP_NewTet(:,1)=(/ip10(1), ip10(2), ip10(6), ip10(9)/)
             IP_NewTet(:,2)=(/ip10(1), ip10(6), ip10(3), ip10(10)/)
             IP_NewTet(:,3)=(/ip10(1), ip10(9), ip10(10), ip10(4)/)
             IP_NewTet(:,4)=(/ip10(1), ip10(6), ip10(10), ip10(9)/)
         ELSE IF (iedd(1)==3 .and. iedd(2)==4 .and. iedd(3)==6)THEN
             IP_NewTet(:,1)=(/ip10(1), ip10(2), ip10(7), ip10(8)/)
             IP_NewTet(:,2)=(/ip10(7), ip10(2), ip10(3), ip10(10)/)
             IP_NewTet(:,3)=(/ip10(8), ip10(2), ip10(10), ip10(4)/)
             IP_NewTet(:,4)=(/ip10(7), ip10(2), ip10(10), ip10(8)/)
         ELSE
!---- here the three divider points are NOT on the same triangle
!---- Set the divided egde with largest length as iedd(1)
!---- then bisect the tetrahedron into two parts first

             IF(edgelen(iedd(1))<edgelen(iedd(2))) iedd(1)=iedd(2)
             IF(edgelen(iedd(1))<edgelen(iedd(3))) iedd(1)=iedd(3)

             NB_NewTet=0
             DO i=1,2
               temp_old(1:4)=IP_OldTet(:)
               temp_old(idx(i,iedd(1)))=edgediv(iedd(1))
               do j=1,6
                  IF( temp_old(idx(1,j))==IP_OldTet(idx(1,j)) .and.     &
                      temp_old(idx(2,j))==IP_OldTet(idx(2,j)) )THEN
                      temp_div(j)=edgediv(j)
                  ELSE
                      temp_div(j)=0
                  ENDIF
               enddo

               call Tet_Divider(temp_old,temp_div,edgelen,itemp,temp_new)
               IP_NewTet(:, NB_NewTet+1:NB_NewTet+itemp)=temp_new(:, 1:itemp)
               NB_NewTet=NB_NewTet+itemp
             ENDDO

         ENDIF

      CASE(4)

         Do i=1,6
            IF(edgediv(i)>0) then
              iedd(5-Ndiv)=i
              Ndiv=Ndiv-1
              IF(Ndiv==0)exit
            ENDIF
         ENDDO
         IF((iedd(1)==2.and.iedd(2)==3.and.iedd(3)==4.and.iedd(4)==5).or.  &
            (iedd(1)==1.and.iedd(2)==3.and.iedd(3)==5.and.iedd(4)==6).or.  &
            (iedd(1)==1.and.iedd(2)==2.and.iedd(3)==4.and.iedd(4)==6) )THEN
!---- here NO three divider points are on the same triangle

             IF(edgelen(iedd(1))<edgelen(iedd(2))) iedd(1)=iedd(2)
             IF(edgelen(iedd(1))<edgelen(iedd(3))) iedd(1)=iedd(3)
             IF(edgelen(iedd(1))<edgelen(iedd(4))) iedd(1)=iedd(4)

             NB_NewTet=0
             DO i=1,2
               temp_old(1:4)=IP_OldTet(:)
               temp_old(idx(i,iedd(1)))=edgediv(iedd(1))
               do j=1,6
                  IF( temp_old(idx(1,j))==IP_OldTet(idx(1,j)) .and.     &
                      temp_old(idx(2,j))==IP_OldTet(idx(2,j)) )THEN
                      temp_div(j)=edgediv(j)
                  ELSE
                      temp_div(j)=0
                  ENDIF
               enddo

               call Tet_Divider(temp_old,temp_div,edgelen,itemp,temp_new)
               IP_NewTet(:, NB_NewTet+1:NB_NewTet+itemp)=temp_new(:, 1:itemp)
               NB_NewTet=NB_NewTet+itemp
             ENDDO

         ELSE

             IF (edgediv(1)==0 .and. edgediv(2)==0) iedd(1:4)=(/3,6,4,5/)
             IF (edgediv(1)==0 .and. edgediv(3)==0) iedd(1:4)=(/2,5,6,4/)
             IF (edgediv(1)==0 .and. edgediv(4)==0) iedd(1:4)=(/5,6,2,3/)
             IF (edgediv(1)==0 .and. edgediv(5)==0) iedd(1:4)=(/4,3,6,2/)
             IF (edgediv(2)==0 .and. edgediv(3)==0) iedd(1:4)=(/1,4,5,6/)
             IF (edgediv(2)==0 .and. edgediv(5)==0) iedd(1:4)=(/6,4,3,1/)
             IF (edgediv(2)==0 .and. edgediv(6)==0) iedd(1:4)=(/5,1,4,3/)
             IF (edgediv(3)==0 .and. edgediv(4)==0) iedd(1:4)=(/6,2,5,1/)
             IF (edgediv(3)==0 .and. edgediv(6)==0) iedd(1:4)=(/4,5,1,2/)
             IF (edgediv(4)==0 .and. edgediv(5)==0) iedd(1:4)=(/1,2,3,6/)
             IF (edgediv(4)==0 .and. edgediv(6)==0) iedd(1:4)=(/3,1,2,5/)
             IF (edgediv(5)==0 .and. edgediv(6)==0) iedd(1:4)=(/2,3,1,4/)

             ip10(1) = sum( idx(:,iedd(2))+idx(:,iedd(3))-idx(:,iedd(1)) )/2
             ip10(2) = sum( idx(:,iedd(1))+idx(:,iedd(3))-idx(:,iedd(2)) )/2
             ip10(3) = sum( idx(:,iedd(1))+idx(:,iedd(2))-idx(:,iedd(3)) )/2
             ip10(4) = 10 - sum(ip10(1:3))
             
             ipnew(1:4) = edgediv(iedd(1:4))
             ipold(1:4) = IP_OldTet(ip10(1:4))

             IP_NewTet(1:4,1) = ipnew(1:4)
             IP_NewTet(1:4,2) = (/ipnew(4),ipnew(2),ipnew(3),ipold(1)/)
             IF(edgelen(iedd(4))<edgelen(iedd(2)))THEN
               IP_NewTet(1:4,3) = (/ipnew(1),ipnew(2),ipnew(4),ipold(4)/)
               IP_NewTet(1:4,4) = (/ipnew(1),ipold(4),ipold(3),ipnew(2)/)
             ELSE
               IP_NewTet(1:4,3) = (/ipnew(1),ipnew(2),ipnew(4),ipold(3)/)
               IP_NewTet(1:4,4) = (/ipnew(1),ipold(4),ipold(3),ipnew(4)/)
             ENDIF
             IF(edgelen(iedd(3))<edgelen(iedd(4)))THEN
               IP_NewTet(1:4,5) = (/ipnew(1),ipold(2),ipold(4),ipnew(4)/)
               IP_NewTet(1:4,6) = (/ipnew(1),ipnew(4),ipnew(3),ipold(2)/)
             ELSE
               IP_NewTet(1:4,5) = (/ipnew(1),ipold(2),ipold(4),ipnew(3)/)
               IP_NewTet(1:4,6) = (/ipnew(1),ipnew(4),ipnew(3),ipold(4)/)
             ENDIF
             NB_NewTet=6


         ENDIF

      CASE(5)

         IF(edgediv(1)==0) iedd(1:5) = (/6, 5, 2, 3, 4/)
         IF(edgediv(2)==0) iedd(1:5) = (/4, 1, 5, 6, 3/)
         IF(edgediv(3)==0) iedd(1:5) = (/5, 2, 6, 4, 1/)
         IF(edgediv(4)==0) iedd(1:5) = (/2, 6, 5, 1, 3/)
         IF(edgediv(5)==0) iedd(1:5) = (/3, 4, 6, 2, 1/)
         IF(edgediv(6)==0) iedd(1:5) = (/1, 3, 2, 5, 4/)

         ip10(1) = sum( idx(:,iedd(2))+idx(:,iedd(3))-idx(:,iedd(1)) )/2
         ip10(2) = sum( idx(:,iedd(1))+idx(:,iedd(3))-idx(:,iedd(2)) )/2
         ip10(3) = sum( idx(:,iedd(4))+idx(:,iedd(5))-idx(:,iedd(1)) )/2
         ip10(4) = 10 - sum(ip10(1:3))

         ipnew(1:5) = edgediv(iedd(1:5))
         ipold(1:4) = IP_OldTet(ip10(1:4))
         
         IP_NewTet(:,1)=(/ipnew(1),ipnew(5),ipnew(2),ipold(4)/)
         IP_NewTet(:,2)=(/ipnew(1),ipnew(3),ipnew(4),ipold(2)/)
         
         IF(edgelen(iedd(2))<edgelen(iedd(5)))THEN
           j        = ipnew(2)
           ipnew(2) = ipnew(5)
           ipnew(5) = j
           j        = ipnew(3)
           ipnew(3) = ipnew(4)
           ipnew(4) = j
           j        = ipold(1)
           ipold(1) = ipold(3)
           ipold(3) = j
	   Ndiv = -1
	 ELSE
	   Ndiv = 1
         ENDIF

         IF(Ndiv*(edgelen(iedd(3))-edgelen(iedd(4)))<0)THEN

           IP_NewTet(:,3)=(/ipnew(2),ipnew(1),ipnew(3),ipnew(4)/)
           IP_NewTet(:,4)=(/ipnew(2),ipnew(5),ipnew(1),ipnew(4)/)
           IP_NewTet(:,5)=(/ipnew(2),ipnew(3),ipold(1),ipnew(4)/)
           IP_NewTet(:,6)=(/ipnew(2),ipold(3),ipnew(5),ipnew(4)/)
           IP_NewTet(:,7)=(/ipnew(2),ipold(1),ipold(3),ipnew(4)/)
           
         ELSE

           xh1 = edgelen(iedd(2))  + edgelen(iedd(4))
           xh2 = edgelen(iedd(3))  + edgelen(iedd(5))
           IF(Ndiv*(xh1-xh2)>0)THEN
             IP_NewTet(:,3)=(/ipnew(2),ipnew(1),ipnew(3),ipnew(4)/)
             IP_NewTet(:,4)=(/ipnew(2),ipnew(5),ipnew(1),ipnew(4)/)
             IP_NewTet(:,5)=(/ipnew(2),ipold(3),ipnew(5),ipnew(4)/)
             IP_NewTet(:,6)=(/ipnew(2),ipnew(3),ipold(3),ipnew(4)/)
	   ELSE
             IP_NewTet(:,3)=(/ipnew(3),ipnew(1),ipnew(4),ipnew(5)/)
             IP_NewTet(:,4)=(/ipnew(3),ipnew(2),ipnew(1),ipnew(5)/)
             IP_NewTet(:,5)=(/ipnew(3),ipold(3),ipnew(2),ipnew(5)/)
             IP_NewTet(:,6)=(/ipnew(3),ipnew(4),ipold(3),ipnew(5)/)
	   ENDIF
           IP_NewTet(:,7)=(/ipnew(2),ipnew(3),ipold(1),ipold(3)/)

         ENDIF

         IF(Ndiv==-1)THEN
	   ip10(3:7)       =IP_NewTet(2,3:7)
	   IP_NewTet(2,3:7)=IP_NewTet(3,3:7)
	   IP_NewTet(3,3:7)=ip10(3:7)
	 ENDIF

         NB_NewTet  = 7
         

      CASE(6)

         ip10(1:4)  = IP_OldTet(1:4)
         ip10(5:10) = edgediv(1:6)
         NB_NewTet  = 8

         IP_NewTet(:,1)=(/ip10(1),ip10(5),ip10(7), ip10(8) /)
         IP_NewTet(:,2)=(/ip10(5),ip10(2),ip10(6), ip10(9) /)
         IP_NewTet(:,3)=(/ip10(7),ip10(6),ip10(3), ip10(10)/)
         IP_NewTet(:,4)=(/ip10(8),ip10(9),ip10(10),ip10(4) /)

!---- Follows is a rough jugdement. Not guarantied to be a Delaunay splitting
         xh1 = edgelen(9-4)  + edgelen(7-4)
         xh2 = edgelen(8-4)  + edgelen(6-4)
         xh3 = edgelen(10-4) + edgelen(5-4)
         if(xh1.ge.xh2.and.xh1.ge.xh3)then
           ipnew(1:6)=(/ip10(7),ip10(9),ip10(5),ip10(6),ip10(10),ip10(8)/)
         else if(xh2.ge.xh1.and.xh2.ge.xh3)then
           ipnew(1:6)=(/ip10(6),ip10(8),ip10(5),ip10(9),ip10(10),ip10(7)/)
         else
           ipnew(1:6)=(/ip10(5),ip10(10),ip10(6),ip10(7),ip10(8),ip10(9)/)
         endif

         IP_NewTet(:,5)=(/ipnew(1),ipnew(2),ipnew(3),ipnew(4)/)
         IP_NewTet(:,6)=(/ipnew(1),ipnew(2),ipnew(4),ipnew(5)/)
         IP_NewTet(:,7)=(/ipnew(1),ipnew(2),ipnew(5),ipnew(6)/)
         IP_NewTet(:,8)=(/ipnew(1),ipnew(2),ipnew(6),ipnew(3)/)

      CASE DEFAULT

        WRITE(*,*)'Error -----in Tet_Divider'
        WRITE(*,'(a,I1,a)')'There are ',Ndiv,' egdes to be bisetted,'
        WRITE(*,*)         '   and this results in confussion'
        CALL Error_STOP (' ')

      END SELECT


      RETURN

      END SUBROUTINE Tet_Divider


!>
!!  Split up to 3 edges of a triangle.
!!  Refer module CellConnectivity for the order of nodes and edges.
!!
!!  @param[in]  IP_OldTri : the three nodes of the tetrahedron.
!!  @param[in]  edgediv (i)=ip>0 : the i-th egde need to be divided by node ip.  \n
!!                      (i)=0    : the i-th egde do NOT need to be divided.
!!  @param[in]  edgelen  : the length of each egde. 
!!                         This array will not be refered 
!!                         if there is only one divided edge.
!!  @param[out]  NB_NewTri :  Number of new triangles.
!!  @param[out]  IP_NewTri :  Nodes of each new Tetrahedron.
!<

      SUBROUTINE Tri_Divider                                                   &
               (IP_OldTri, edgediv, edgelen, NB_NewTri, IP_NewTri)
      USE CellConnectivity
      IMPLICIT NONE

      INTEGER :: IP_OldTri(3), edgediv(3)
      REAL*8  :: edgelen(3)
      INTEGER :: NB_NewTri, IP_NewTri(3,4)
      INTEGER :: Ndiv, i, j
      INTEGER :: idx(2,3), ip6(6)

      idx(1:2,1:3) = iEdge_Tri(1:2,1:3)

      Ndiv=0
      DO i=1,3
         IF(edgediv(i)>0) Ndiv=Ndiv+1
      ENDDO

      DO i=1,4
        IP_NewTri(:,i) = IP_OldTri(:)
      ENDDO

      SELECT CASE(Ndiv)
      CASE(0)

         NB_NewTri=1

      CASE(1)

         Do i=1,3
            IF(edgediv(i)>0) EXIT
         ENDDO
         NB_NewTri = 2
         IP_NewTri(idx(1,i),1) = edgediv(i)
         IP_NewTri(idx(2,i),2) = edgediv(i)

      CASE(2)

         Do i=1,3
            IF(edgediv(i)==0) EXIT
         ENDDO
         
         ip6(1) = IP_OldTri(i)
         ip6(2) = IP_OldTri(idx(1,i))
         ip6(3) = IP_OldTri(idx(2,i))
         ip6(4) = edgediv(idx(1,i))
         ip6(5) = edgediv(idx(2,i))
         
         NB_NewTri = 3
         IP_NewTri(:,1) = (/ip6(1),ip6(5),ip6(4)/)
         
         IF(edgelen(idx(1,i))<edgelen(idx(2,i)))then
            IP_NewTri(:,2) = (/ip6(2),ip6(3),ip6(5)/)
            IP_NewTri(:,3) = (/ip6(3),ip6(4),ip6(5)/)
         ELSE
            IP_NewTri(:,2) = (/ip6(2),ip6(3),ip6(4)/)
            IP_NewTri(:,3) = (/ip6(2),ip6(4),ip6(5)/)
         ENDIF         

      CASE(3)

         ip6(1:3)  = IP_OldTri(1:3)
         ip6(4:6)  = edgediv(1:3)
         NB_NewTri = 4
         IP_NewTri(:,1) = (/ip6(1),ip6(6),ip6(5)/)
         IP_NewTri(:,2) = (/ip6(6),ip6(2),ip6(4)/)
         IP_NewTri(:,3) = (/ip6(5),ip6(4),ip6(3)/)
         IP_NewTri(:,4) = (/ip6(4),ip6(5),ip6(6)/)

      CASE DEFAULT


      END SELECT

      RETURN
      END SUBROUTINE Tri_Divider


