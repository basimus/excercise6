!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Dynamic memory allocation routines for 1D, 2D or 3D pointers.
!<
MODULE array_allocator

  IMPLICIT NONE

  INTERFACE allc_1Dpointer
     MODULE PROCEDURE allc_int_1Dpointer, &
                      allc_real8_1Dpointer
  END INTERFACE

  INTERFACE allc_2Dpointer
     MODULE PROCEDURE allc_int_2Dpointer, &
                      allc_real8_2Dpointer
  END INTERFACE

  INTERFACE allc_3Dpointer
     MODULE PROCEDURE allc_int_3Dpointer, &
                      allc_real8_3Dpointer
  END INTERFACE

  INTERFACE deallc_pointer
     MODULE PROCEDURE deallc_int_1Dpointer,   &
                      deallc_int_2Dpointer,   &
                      deallc_int_3Dpointer,   &
                      deallc_real8_1Dpointer, &
                      deallc_real8_2Dpointer, &
                      deallc_real8_3Dpointer
  END INTERFACE

  INTERFACE KeyLength
     MODULE PROCEDURE KeyLength_int_1Dpointer,     &
                      KeyLength_int_2Dpointer,     &
                      KeyLength_int_3Dpointer,     &
                      KeyLength_real8_1Dpointer,   &
                      KeyLength_real8_2Dpointer,   &
                      KeyLength_real8_3Dpointer
  END INTERFACE


CONTAINS

  SUBROUTINE allc_int_1Dpointer(a,n,message)

    INTEGER, DIMENSION(:), POINTER :: a, b
    INTEGER, INTENT(IN) :: n
    CHARACTER*(*), INTENT(IN), OPTIONAL :: message

    INTEGER :: ns, i
    INTEGER :: ierr1=0,ierr2=0,ierr3=0,ierr4=0
    LOGICAL :: NotNewPointer

    NotNewPointer = .FALSE.
    IF(ASSOCIATED(a))THEN
       ns = SIZE(a)
       IF(ns>=n) RETURN
       NotNewPointer = .TRUE.
       ALLOCATE(b(ns),stat=ierr1)
       DO i = 1,ns
          b(i) = a(i)
       ENDDO
       DEALLOCATE(a,stat=ierr2)
    ENDIF

    ALLOCATE(a(n), stat=ierr3)


    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0)THEN
       WRITE(*,*) ' ALLOCATION ERROR: ierr=',ierr1,ierr2,ierr3
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', n
       IF(PRESENT(message)) WRITE(*,*) '                   '//message
       CALL Error_STOP (' ')
    ENDIF
   

    IF(NotNewPointer)THEN
       DO i = 1,ns
          a(i) = b(i)
       ENDDO
       DEALLOCATE(b,stat=ierr4)
    ENDIF

    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0.OR.ierr4/=0)THEN
       WRITE(*,*) ' ALLOCATION ERROR: ierr=',ierr1,ierr2,ierr3,ierr4
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', n
       IF(PRESENT(message)) WRITE(*,*) '                   '//message
       CALL Error_STOP (' ')
    ENDIF

    RETURN
  END SUBROUTINE allc_int_1Dpointer


  SUBROUTINE allc_int_2Dpointer(a,n,m,message)

    INTEGER, DIMENSION(:,:), POINTER :: a, b
    INTEGER, INTENT(IN) :: n,m
    CHARACTER*(*), INTENT(IN), OPTIONAL :: message

    INTEGER :: ns,ms, i,j
    INTEGER :: ierr1=0,ierr2=0,ierr3=0,ierr4=0
    LOGICAL :: NotNewPointer

    NotNewPointer = .FALSE.
    IF(ASSOCIATED(a))THEN
       ns = SIZE(a,1)
       ms = SIZE(a,2)
       IF(ns>=n .AND. ms>m) RETURN
       NotNewPointer = .TRUE.
       ns = MIN(ns,n)
       ms = MIN(ms,m)
       ALLOCATE(b(ns,ms),stat=ierr1)
       DO i = 1,ns
          DO j = 1,ms
             b(i,j) = a(i,j)
          ENDDO
       ENDDO
       DEALLOCATE(a,stat=ierr2)
    ENDIF

    ALLOCATE(a(n, m), stat=ierr3)
    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0)THEN
       WRITE(*,*) ' ALLOCATION ERROR: ierr=',ierr1,ierr2,ierr3
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', n
       IF(PRESENT(message)) WRITE(*,*) '                   '//message
       CALL Error_STOP (' ')
    ENDIF

    IF(NotNewPointer)THEN
       DO i = 1,ns
          DO j = 1,ms
             a(i,j) = b(i,j)
          ENDDO
       ENDDO
       DEALLOCATE(b,stat=ierr4)
    ENDIF

    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0.OR.ierr4/=0)THEN
       WRITE(*,*) ' ALLOCATION ERROR: ierr=',ierr1,ierr2,ierr3,ierr4
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', n,m
       IF(PRESENT(message)) WRITE(*,*) '                   '//message
       CALL Error_STOP (' ')
    ENDIF

    RETURN
  END SUBROUTINE allc_int_2Dpointer


  SUBROUTINE allc_int_3Dpointer(a,n,m,l,message)

    INTEGER, DIMENSION(:,:,:), POINTER :: a, b
    INTEGER, INTENT(IN) :: n,m,l
    CHARACTER*(*), INTENT(IN), OPTIONAL :: message

    INTEGER :: ns,ms,ls, i,j,k
    INTEGER :: ierr1=0,ierr2=0,ierr3=0,ierr4=0
    LOGICAL :: NotNewPointer

    NotNewPointer = .FALSE.
    IF(ASSOCIATED(a))THEN
       ns = SIZE(a,1)
       ms = SIZE(a,2)
       ls = SIZE(a,3)
       IF(ns>=n .AND. ms>m .AND. ls>=l) RETURN
       NotNewPointer = .TRUE.
       ns = MIN(ns,n)
       ms = MIN(ms,m)
       ls = MIN(ls,l)
       ALLOCATE(b(ns,ms,ls),stat=ierr1)
       DO i = 1,ns
          DO j = 1,ms
             DO k = 1,ls
                b(i,j,k) = a(i,j,k)
             ENDDO
          ENDDO
       ENDDO
       DEALLOCATE(a,stat=ierr2)
    ENDIF

    ALLOCATE(a(n, m, l), stat=ierr3)
    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0)THEN
       WRITE(*,*) ' ALLOCATION ERROR: ierr=',ierr1,ierr2,ierr3
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', n
       IF(PRESENT(message)) WRITE(*,*) '                   '//message
       CALL Error_STOP (' ')
    ENDIF

    IF(NotNewPointer)THEN
       DO i = 1,ns
          DO j = 1,ms
             DO k = 1,ls
                a(i,j,k) = b(i,j,k)
             ENDDO
          ENDDO
       ENDDO
       DEALLOCATE(b,stat=ierr4)
    ENDIF

    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0.OR.ierr4/=0)THEN
       WRITE(*,*) ' ALLOCATION ERROR: ierr=',ierr1,ierr2,ierr3,ierr4
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', n,m,l
       IF(PRESENT(message)) WRITE(*,*) '                   '//message
       CALL Error_STOP (' ')
    ENDIF

    RETURN
  END SUBROUTINE allc_int_3Dpointer


  SUBROUTINE allc_real8_1Dpointer(a,n,message)

    REAL*8, DIMENSION(:), POINTER :: a, b
    INTEGER, INTENT(IN) :: n
    CHARACTER*(*), INTENT(IN), OPTIONAL :: message

    INTEGER :: ns, i
    INTEGER :: ierr1=0,ierr2=0,ierr3=0,ierr4=0
    LOGICAL :: NotNewPointer 

    NotNewPointer = .FALSE.
    IF(ASSOCIATED(a))THEN
       ns = SIZE(a)
       IF(ns>=n) RETURN
       NotNewPointer = .TRUE.
       ALLOCATE(b(ns),stat=ierr1)
       DO i = 1,ns
          b(i) = a(i)
       ENDDO
       DEALLOCATE(a,stat=ierr2)
    ENDIF

    ALLOCATE(a(n), stat=ierr3)
    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0)THEN
       WRITE(*,*) ' ALLOCATION ERROR: ierr=',ierr1,ierr2,ierr3
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', n
       IF(PRESENT(message)) WRITE(*,*) '                   '//message
       CALL Error_STOP (' ')
    ENDIF

    IF(NotNewPointer)THEN
       DO i = 1,ns
          a(i) = b(i)
       ENDDO
       DEALLOCATE(b,stat=ierr4)
    ENDIF

    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0.OR.ierr4/=0)THEN
       WRITE(*,*) ' ALLOCATION ERROR: ierr=',ierr1,ierr2,ierr3,ierr4
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', n
       IF(PRESENT(message)) WRITE(*,*) '                   '//message
       CALL Error_STOP (' ')
    ENDIF

    RETURN
  END SUBROUTINE allc_real8_1Dpointer


  SUBROUTINE allc_real8_2Dpointer(a,n,m,message)

    REAL*8, DIMENSION(:,:), POINTER :: a, b
    INTEGER, INTENT(IN) :: n,m
    CHARACTER*(*), INTENT(IN), OPTIONAL :: message

    INTEGER :: ns,ms, i,j
    INTEGER :: ierr1=0,ierr2=0,ierr3=0,ierr4=0
    LOGICAL :: NotNewPointer 

    NotNewPointer = .FALSE.
    IF(ASSOCIATED(a))THEN
       ns = SIZE(a,1)
       ms = SIZE(a,2)
       IF(ns>=n .AND. ms>m) RETURN
       NotNewPointer = .TRUE.
       ns = MIN(ns,n)
       ms = MIN(ms,m)
       ALLOCATE(b(ns,ms),stat=ierr1)
       DO i = 1,ns
          DO j = 1,ms
             b(i,j) = a(i,j)
          ENDDO
       ENDDO
       DEALLOCATE(a,stat=ierr2)
    ENDIF
    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0)THEN
       WRITE(*,*) ' ALLOCATION ERROR: ierr=',ierr1,ierr2,ierr3
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', n
       IF(PRESENT(message)) WRITE(*,*) '                   '//message
       CALL Error_STOP (' ')
    ENDIF

    ALLOCATE(a(n,m), stat=ierr3)

    IF(NotNewPointer)THEN
       DO i = 1,ns
          DO j = 1,ms
             a(i,j) = b(i,j)
          ENDDO
       ENDDO
       DEALLOCATE(b,stat=ierr4)
    ENDIF

    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0.OR.ierr4/=0)THEN
       WRITE(*,*) ' ALLOCATION ERROR: ierr=',ierr1,ierr2,ierr3,ierr4
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', n,m
       IF(PRESENT(message)) WRITE(*,*) '                   '//message
       CALL Error_STOP (' ')
    ENDIF

    RETURN
  END SUBROUTINE allc_real8_2Dpointer


  SUBROUTINE allc_real8_3Dpointer(a,n,m,l,message)

    REAL*8, DIMENSION(:,:,:), POINTER :: a, b
    INTEGER, INTENT(IN) :: n,m,l
    CHARACTER*(*), INTENT(IN), OPTIONAL :: message

    INTEGER :: ns,ms,ls, i,j,k
    INTEGER :: ierr1=0,ierr2=0,ierr3=0,ierr4=0
    LOGICAL :: NotNewPointer

    NotNewPointer = .FALSE.
    IF(ASSOCIATED(a))THEN
       ns = SIZE(a,1)
       ms = SIZE(a,2)
       ls = SIZE(a,3)
       IF(ns>=n .AND. ms>m .AND. ls>=l) RETURN
       NotNewPointer = .TRUE.
       ns = MIN(ns,n)
       ms = MIN(ms,m)
       ls = MIN(ls,l)
       ALLOCATE(b(ns,ms,ls),stat=ierr1)
       DO i = 1,ns
          DO j = 1,ms
             DO k = 1,ls
                b(i,j,k) = a(i,j,k)
             ENDDO
          ENDDO
       ENDDO
       DEALLOCATE(a,stat=ierr2)
    ENDIF

    ALLOCATE(a(n, m, l), stat=ierr3)
    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0)THEN
       WRITE(*,*) ' ALLOCATION ERROR: ierr=',ierr1,ierr2,ierr3
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', n
       IF(PRESENT(message)) WRITE(*,*) '                   '//message
       CALL Error_STOP (' ')
    ENDIF

    IF(NotNewPointer)THEN
       DO i = 1,ns
          DO j = 1,ms
             DO k = 1,ls
                a(i,j,k) = b(i,j,k)
             ENDDO
          ENDDO
       ENDDO
       DEALLOCATE(b,stat=ierr4)
    ENDIF

    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0.OR.ierr4/=0)THEN
       WRITE(*,*) ' ALLOCATION ERROR: ierr=',ierr1,ierr2,ierr3,ierr4
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', n,m,l
       IF(PRESENT(message)) WRITE(*,*) '                   '//message
       CALL Error_STOP (' ')
    ENDIF

    RETURN
  END SUBROUTINE allc_real8_3Dpointer


  SUBROUTINE deallc_int_1Dpointer(a)
    INTEGER, DIMENSION(:), POINTER :: a
    IF(ASSOCIATED(a)) DEALLOCATE(a)
    RETURN
  END SUBROUTINE deallc_int_1Dpointer

  SUBROUTINE deallc_int_2Dpointer(a)
    INTEGER, DIMENSION(:,:), POINTER :: a
    IF(ASSOCIATED(a)) DEALLOCATE(a)
    RETURN
  END SUBROUTINE deallc_int_2Dpointer

  SUBROUTINE deallc_int_3Dpointer(a)
    INTEGER, DIMENSION(:,:,:), POINTER :: a
    IF(ASSOCIATED(a)) DEALLOCATE(a)
    RETURN
  END SUBROUTINE deallc_int_3Dpointer

  SUBROUTINE deallc_real8_1Dpointer(a)
    REAL*8, DIMENSION(:), POINTER :: a
    IF(ASSOCIATED(a)) DEALLOCATE(a)
    RETURN
  END SUBROUTINE deallc_real8_1Dpointer

  SUBROUTINE deallc_real8_2Dpointer(a)
    REAL*8, DIMENSION(:,:), POINTER :: a
    IF(ASSOCIATED(a)) DEALLOCATE(a)
    RETURN
  END SUBROUTINE deallc_real8_2Dpointer

  SUBROUTINE deallc_real8_3Dpointer(a)
    REAL*8, DIMENSION(:,:,:), POINTER :: a
    IF(ASSOCIATED(a)) DEALLOCATE(a)
    RETURN
  END SUBROUTINE deallc_real8_3Dpointer

  FUNCTION KeyLength_int_1Dpointer(a) RESULT(n)
    INTEGER, DIMENSION(:), POINTER :: a
    INTEGER  :: n    
    IF(ASSOCIATED(a))THEN
       n = SIZE(a,1)
    ELSE
       n = 0
    ENDIF
  END FUNCTION KeyLength_int_1Dpointer

  FUNCTION KeyLength_int_2Dpointer(a) RESULT(n) 
    INTEGER, DIMENSION(:,:), POINTER :: a
    INTEGER  :: n
    IF(ASSOCIATED(a))THEN
       n = SIZE(a,2)
    ELSE
       n = 0
    ENDIF
  END FUNCTION KeyLength_int_2Dpointer

  FUNCTION KeyLength_int_3Dpointer(a) RESULT(n)
    INTEGER, DIMENSION(:,:,:), POINTER :: a
    INTEGER  :: n
    IF(ASSOCIATED(a))THEN
       n = SIZE(a,3)
    ELSE
       n = 0
    ENDIF
  END FUNCTION KeyLength_int_3Dpointer

  FUNCTION KeyLength_real8_1Dpointer(a) RESULT(n)
    REAL*8, DIMENSION(:), POINTER :: a
    INTEGER  :: n    
    IF(ASSOCIATED(a))THEN
       n = SIZE(a,1)
    ELSE
       n = 0
    ENDIF
  END FUNCTION KeyLength_real8_1Dpointer

  FUNCTION KeyLength_real8_2Dpointer(a) RESULT(n) 
    REAL*8, DIMENSION(:,:), POINTER :: a
    INTEGER  :: n
    IF(ASSOCIATED(a))THEN
       n = SIZE(a,2)
    ELSE
       n = 0
    ENDIF
  END FUNCTION KeyLength_real8_2Dpointer

  FUNCTION KeyLength_real8_3Dpointer(a) RESULT(n)
    REAL*8, DIMENSION(:,:,:), POINTER :: a
    INTEGER  :: n
    IF(ASSOCIATED(a))THEN
       n = SIZE(a,3)
    ELSE
       n = 0
    ENDIF
  END FUNCTION KeyLength_real8_3Dpointer


END MODULE array_allocator
