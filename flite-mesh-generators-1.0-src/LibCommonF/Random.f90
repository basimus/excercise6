!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


MODULE RandomModule

CONTAINS

  !>
  !!   returns a uniform random deviate between 0.0 and 1.0.
  !!    Call with idum to a negative integer to initialize.
  !!    thereafter, do not alter idum between successive deviates in a sequence.
  !!    From page 279 of Numerical Recipes in C, ED. 2, Press
  !!    et al., Cambridge University Press, 1988 
  !<
  FUNCTION Random_Gauss(idum) RESULT(ans)
    IMPLICIT NONE
    !INTEGER, PARAMETER :: IA=16807
    !INTEGER, PARAMETER :: IM=2147483647
    !REAL*8,  PARAMETER :: AM=1.0/IM
    !INTEGER, PARAMETER :: IQ=127773
    !INTEGER, PARAMETER :: IR=2836
    !INTEGER, PARAMETER :: MASK=123459876
    INTEGER, INTENT(INOUT) :: idum
    REAL*8  :: ans
    !INTEGER :: NG
    !NG=idum/IQ
    !idum=IA*(idum-NG*IQ)-IR*NG
    !IF (idum.LT.0) idum=idum+IM
    !ans=AM*idum
    CALL RANDOM_NUMBER(ans)
       
    RETURN
  END FUNCTION Random_Gauss
  
   FUNCTION Random_Gauss2(idum) RESULT(ans)
    IMPLICIT NONE
    INTEGER, PARAMETER :: IA=16807
    INTEGER, PARAMETER :: IM=2147483647
    REAL*8,  PARAMETER :: AM=1.0/IM
    INTEGER, PARAMETER :: IQ=127773
    INTEGER, PARAMETER :: IR=2836
    INTEGER, PARAMETER :: MASK=123459876
    INTEGER, INTENT(INOUT) :: idum
    REAL*8  :: ans
    INTEGER :: NG
    NG=idum/IQ
    idum=IA*(idum-NG*IQ)-IR*NG
    IF (idum.LT.0) idum=idum+IM
    ans=AM*idum
    
       
    RETURN
  END FUNCTION Random_Gauss2

  !>
  !!   returns a random integer between N1 and N2
  !<
  FUNCTION Random_Gauss_INT(idum, N1, N2) RESULT(N)
    IMPLICIT NONE
    INTEGER, INTENT(INOUT) :: idum
    INTEGER, INTENT(IN) :: N1, N2
    INTEGER :: N
    REAL*8 :: r
    !N   = N1 + (N2-N1) * Random_Gauss(idum)
    CALL RANDOM_NUMBER(r)
    N   = N1 + (N2-N1) * r
    RETURN
  END FUNCTION Random_Gauss_INT


  !>  
  !!   Levy_walk random number generator
  !<
  SUBROUTINE Random_LevyWalk(dx,Ndim)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Ndim  !  Number of dimensions
    REAL*8, INTENT(OUT), DIMENSION(Ndim) :: dx  !  Levy walk
    INTEGER :: I  ! Counter
    REAL*8 , PARAMETER :: PI = 3.141592653589793D0
    REAL*8  :: r
	
	

	
    INTEGER, SAVE :: idum = -2
    DO I = 1,Ndim
       r = Random_Gauss2(idum)
       dx(I) = TAN(2.d0*PI*(r-0.5d0))
	   
    ENDDO
  END SUBROUTINE Random_LevyWalk

END MODULE RandomModule


!>
!!  initialisation for intrinsic subroutine RANDOM_NUMBER()
!<
SUBROUTINE init_random_seed()
  IMPLICIT NONE
  INTEGER :: i, n, clock
  INTEGER, DIMENSION(:), ALLOCATABLE :: seed

  CALL RANDOM_SEED(size = n)
  ALLOCATE(seed(n))
  CALL SYSTEM_CLOCK(COUNT=clock)
  seed = clock + 37 * (/ (i - 1, i = 1, n) /)
  CALL RANDOM_SEED(PUT = seed)

  DEALLOCATE(seed)
END SUBROUTINE init_random_seed

!>
!!  Latin Hypercube Sampling
!!   @param[in]  D             the number of dimensions.
!!   @param[in]  N             the number of samples.
!!   @param[in]  vardef (D,2)  the bottom and upper bounds of variables in each dimension.
!!   @param[out] x (N,D)       the random samples
!<
SUBROUTINE LHS(D,N,vardef,x)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: D, N
  DOUBLE PRECISION, INTENT(IN), DIMENSION(D,2) :: vardef
  DOUBLE PRECISION, INTENT(OUT), DIMENSION(N,D) :: x
  DOUBLE PRECISION, DIMENSION(N,D) :: r
  DOUBLE PRECISION :: temp  ! temporary holder
  INTEGER :: randpos        ! Random position
  INTEGER :: I,J,K          ! counter

  ! First define unshuffled samples
  DO I = 1,D
     temp = 0.0
     DO J = 1,N
        x(J,I) = vardef(I,1) + (temp/N)*(vardef(I,2)-vardef(I,1))
        temp = temp + 1.0
     ENDDO
  ENDDO

  ! Now shuffle along each dimension
  DO K=1,10
     CALL init_random_seed()
     CALL RANDOM_NUMBER(r)
     DO I=1,D,1
        DO J=N,2,-1
           randpos = INT(r(J,I)*J)+1
           temp = x(randpos,I)
           x(randpos,I) = x(J,I)
           x(J,I) = temp
        ENDDO
     ENDDO
  ENDDO

END SUBROUTINE LHS
