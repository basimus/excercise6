!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Definition of sources with basic functions.  
!!
!!  Reminder 1: the spacing of any source (input and output) is always
!!              dimensionless. The basic spacing of the whole domain
!!              is assumed to be 1.                        \n
!!  Reminder 2: all functions /subroutines beginning with "SourceGroup_"
!!              can be treated as public functions, and the rest 
!!              can be treated as private functions.
!<
MODULE Source_Type
  USE Geometry3DAll
  USE Geometry2D
  IMPLICIT NONE

  REAL*8, PARAMETER :: log2 = LOG(2.d0)

  !>  Type of Point Sources
  TYPE :: PointSource_Type
     REAL*8  :: Posit(3)
     REAL*8  :: RA1, RA2
     REAL*8  :: Space
     REAL*8  :: decay
     LOGICAL :: ready = .FALSE.
  END TYPE PointSource_Type

  !>  Type of Line Sources
  TYPE :: LineSource_Type
     TYPE(PointSource_Type) :: PS1, PS2
     INTEGER :: EndCut = 0   !< 0 no cut; 1 cut PS1; 2 cut PS2; 3 cut both
     LOGICAL :: ready = .FALSE.
  END TYPE LineSource_Type

  !>  Type of Triangular Sources
  TYPE :: TriSource_Type
     TYPE(PointSource_Type) :: PS1, PS2, PS3
     TYPE(LineSource_Type)  :: LS1, LS2, LS3
     TYPE(Plane3D) :: aPlane
     REAL*8  :: P2D(2,3)
     REAL*8  :: dbarea        !< double of area
     LOGICAL :: ready = .FALSE.
  END TYPE TriSource_Type

  !>  Type of Anisotropic Torus (ring) Sources
  TYPE :: RingSource_Type
     TYPE(Plane3D) :: aPlane
     REAL*8  :: P0(2)         !< Torus (ring) centre on 2D plane
     REAL*8  :: RA0, RA1, RA2
     REAL*8  :: Space, Stretch
     REAL*8  :: decaySp, decaySt
     LOGICAL :: ready = .FALSE.
  END TYPE RingSource_Type

  !>  Type of Anisotropic Point Sources   \n
  !!  The directional space is Space*Stretch on direction DIRCT(1,:).
  !!  The directional space is Space on direction DIRCT(2,:) or DIRCT(3,:).
  TYPE :: Anis_PointSource_Type
     REAL*8  :: Posit(3)
     REAL*8  :: RA1, RA2
     REAL*8  :: Space, Stretch
     REAL*8  :: decaySp, decaySt
     REAL*8  :: DIRCT(3,3)
     LOGICAL :: ready = .FALSE.
  END TYPE Anis_PointSource_Type

  !>  Type of Anisotropic Line Sources
  TYPE :: Anis_LineSource_Type
     TYPE(Anis_PointSource_Type) :: PS1, PS2
     INTEGER :: EndCut = 0   !< 0 no cut; 1 cut PS1; 2 cut PS2; 3 cut both
     REAL*8  :: DIRCT(3,3)
     LOGICAL :: ready = .FALSE.
  END TYPE Anis_LineSource_Type

  !>  Type of Anisotropic triangular Sources
  TYPE :: Anis_TriSource_Type
     TYPE(Anis_PointSource_Type) :: PS1, PS2, PS3
     TYPE(Anis_LineSource_Type)  :: LS1, LS2, LS3
     TYPE(Plane3D) :: aPlane
     REAL*8  :: P2D(2,3)
     REAL*8  :: dbarea        !< double of area
     REAL*8  :: DIRCT(3,3)
     LOGICAL :: ready = .FALSE.
  END TYPE Anis_TriSource_Type

  !>
  !!  SourceGroup_Type is a collection of PointSource_Type, LineSource_Type,
  !!   TriSource_Type, RingSource_Type, Anis_PointSource_Type, 
  !!   Anis_LineSource_Type and Anis_TriSource_Type.
  !<
  TYPE :: SourceGroup_Type
     INTEGER :: NB_Pt  = 0         !<  the number of PointSource_Type sources.
     INTEGER :: NB_Ln  = 0         !<  the number of LineSource_Type sources.
     INTEGER :: NB_Tri = 0         !<  the number of TriSource_Type sources.
     INTEGER :: NB_Ring= 0         !<  the number of RingSource_Type sources.
     INTEGER :: NB_APt = 0         !<  the number of Anis_PointSource_Type sources.
     INTEGER :: NB_ALn = 0         !<  the number of Anis_LineSource_Type sources.
     INTEGER :: NB_ATr = 0         !<  the number of Anis_TriSource_Type sources.
     TYPE(PointSource_Type),      DIMENSION(:), POINTER :: Pt => null()
     TYPE(LineSource_Type),       DIMENSION(:), POINTER :: Ln => null()
     TYPE(TriSource_Type),        DIMENSION(:), POINTER :: Tri => null()
     TYPE(RingSource_Type),       DIMENSION(:), POINTER :: Ring => null()
     TYPE(Anis_PointSource_Type), DIMENSION(:), POINTER :: APt => null()
     TYPE(Anis_LineSource_Type),  DIMENSION(:), POINTER :: ALn => null()
     TYPE(Anis_TriSource_Type),   DIMENSION(:), POINTER :: ATr => null()
  END TYPE SourceGroup_Type


  !>  Type of cuboid Sources.
  !!
  !!  Reminder: a box-source is only applied when building an octree background.
  !!            It is not referred when calculating the spacing of any position 
  !!            if no octree background built.
  !<
  TYPE :: BoxSource_Type
     REAL*8  :: pMin(3)        !<  The position of the minimum corner.
     REAL*8  :: pMax(3)        !<  The position of the maximum corner.
     REAL*8  :: fMtr(6)        !<  The Metrix.
     REAL*8  :: Scalar         !<  The Scalar.
     INTEGER :: LevelMark      !<  The level.
     LOGICAL :: isotr = .TRUE. !<  true for a isotropic source; flase for an anisotroic one.
  END TYPE BoxSource_Type



CONTAINS

  !>
  !!   Private function.
  !<
  FUNCTION getDoubleArea2D(P1,P2,P3) RESULT(dbarea)
    IMPLICIT NONE
    REAL*8, INTENT(in) :: P1(2), P2(2), P3(2)
    REAL*8 :: dbarea
    dbarea = (P2(1)-P1(1)) * (P3(2)-P1(2)) - (P3(1)-P1(1)) * (P2(2)-P1(2))
  END FUNCTION getDoubleArea2D

  !>
  !!   Allocate a source group.
  !!   @param[in]  NB_Pt  : the number of point-sources.
  !!   @param[in]  NB_Ln  : the number of line-sources.
  !!   @param[in]  NB_Tri : the number of triangular-sources.
  !!   @param[in]  SetNumber  = .true. or not present, 
  !!                          set the numbers of sources into the group.  \n
  !!                          otherwise, do NOT set the numbers of sources into the group.
  !!   @param[out] Sources : the source group.
  !!
  !!   The pointers for the anisotropic sources are also allocated with the same sizes.
  !!   The pointer for the Torus (ring) sources is also allocated with size of NB_Tri.
  !<
  SUBROUTINE SourceGroup_Allocate(Sources, NB_Pt, NB_Ln, NB_Tri, SetNumber)
    IMPLICIT NONE
    TYPE (SourceGroup_Type), INTENT(INOUT) :: Sources
    INTEGER, INTENT(IN) :: NB_Pt, NB_Ln, NB_Tri
    LOGICAL, INTENT(IN), OPTIONAL :: SetNumber
    LOGICAL :: SetN
    SetN = .TRUE.
    IF(PRESENT(SetNumber)) SetN = SetNumber
    IF(SetN)THEN
       Sources%NB_Pt  = NB_Pt
       Sources%NB_Ln  = NB_Ln
       Sources%NB_Tri = NB_Tri
       Sources%NB_Ring= 0
       Sources%NB_APt = 0
       Sources%NB_ALn = 0
       Sources%NB_ATr = 0
    ENDIF
    ALLOCATE(Sources%Pt(NB_Pt))
    ALLOCATE(Sources%Ln(NB_Ln))
    ALLOCATE(Sources%Tri(NB_Tri))
    ALLOCATE(Sources%Ring(NB_Tri))
    ALLOCATE(Sources%APt(NB_Pt))
    ALLOCATE(Sources%ALn(NB_Ln))
    ALLOCATE(Sources%ATr(NB_Tri))
  END SUBROUTINE SourceGroup_Allocate

  !>
  !!    Delete the source group and release memory.
  !<
  SUBROUTINE SourceGroup_Clear(Sources)
    IMPLICIT NONE
    TYPE (SourceGroup_Type), INTENT(INOUT) :: Sources
    Sources%NB_Pt  = 0
    Sources%NB_Ln  = 0
    Sources%NB_Tri = 0
    Sources%NB_Ring= 0
    Sources%NB_APt = 0
    Sources%NB_ALn = 0
    Sources%NB_ATr = 0
    IF(ASSOCIATED(Sources%Pt))   DEALLOCATE(Sources%Pt)
    IF(ASSOCIATED(Sources%Ln))   DEALLOCATE(Sources%Ln)
    IF(ASSOCIATED(Sources%Tri))  DEALLOCATE(Sources%Tri)
    IF(ASSOCIATED(Sources%Ring)) DEALLOCATE(Sources%Ring)
    IF(ASSOCIATED(Sources%APt))  DEALLOCATE(Sources%APt)
    IF(ASSOCIATED(Sources%ALn))  DEALLOCATE(Sources%ALn)
    IF(ASSOCIATED(Sources%ATr))  DEALLOCATE(Sources%ATr)
  END SUBROUTINE SourceGroup_Clear

  !>
  !!  Return .true. if and only if any sources exsit.
  !<
  FUNCTION SourceGroup_isReady(Sources) RESULT(yesno)
    TYPE (SourceGroup_Type), INTENT(IN) :: Sources
    LOGICAL :: yesno
    yesno = .TRUE.
    IF( Sources%NB_Pt  > 0 ) RETURN
    IF( Sources%NB_Ln  > 0 ) RETURN
    IF( Sources%NB_Tri > 0 ) RETURN
    IF( Sources%NB_Ring> 0 ) RETURN
    IF( Sources%NB_APt > 0 ) RETURN
    IF( Sources%NB_ALn > 0 ) RETURN
    IF( Sources%NB_ATr > 0 ) RETURN
    yesno = .FALSE.
  END FUNCTION SourceGroup_isReady

  !>
  !!  Output the information of the source group.
  !!  @param[in] Sources  : the source group.
  !!  @param[in] io       : the IO channel. 
  !!                        If =6 or not present, output on screen. 
  !<
  SUBROUTINE SourceGroup_info(Sources, io)
    TYPE (SourceGroup_Type), INTENT(IN) :: Sources
    INTEGER, INTENT(IN), OPTIONAL :: io
    INTEGER :: ic
    ic = 6
    IF(PRESENT(io)) ic = io
    IF( SourceGroup_isReady(Sources) ) THEN
       WRITE(ic,*) '   Information of source group: '
       WRITE(ic,*) '     number of point    sources:',Sources%NB_Pt
       WRITE(ic,*) '     number of line     sources:',Sources%NB_Ln
       WRITE(ic,*) '     number of triangle sources:',Sources%NB_Tri
       WRITE(ic,*) '     number of ring     sources:',Sources%NB_Ring
       WRITE(ic,*) '     number of an-point sources:',Sources%NB_APt
       WRITE(ic,*) '     number of an_line  sources:',Sources%NB_ALn
       WRITE(ic,*) '     number of an_tri   sources:',Sources%NB_ATr
    ELSE
       WRITE(ic,*) '     number of all FLITE sources: 0'
    ENDIF
  END SUBROUTINE SourceGroup_info

  !>
  !!   Add & prepare a point source into the source group.
  !!   @param[in]  P1  : the centre of the point-source.
  !!   @param[in]  S1  : the spacing (dimensionless) of the point-source.
  !!   @param[in]  R11 : the radius in which spacing S1 applies.
  !!   @param[in]  R12 : the radius in which double spacing (2*S1) applies.
  !!   @param[in,out] Sources : the source group.
  !!
  !!   Reminder: the number of point-sources is incremented by 1 automatically 
  !!             in the subroutine, so when you allocate the source group by
  !!             calling SourceGroup_Allocate(), set argument 
  !!             SetNumber  = .false. 
  !<
  SUBROUTINE SourceGroup_AddPointSource   (Sources,P1,S1,R11,R12)
    IMPLICIT NONE
    REAL*8, INTENT(in) :: P1(3),S1,R11,R12
    TYPE (SourceGroup_Type), INTENT(INOUT) :: Sources
    INTEGER :: i
    Sources%NB_Pt = Sources%NB_Pt + 1
    IF(Sources%NB_Pt>SIZE(Sources%Pt)) CALL Error_STOP ( ' size of Sources%Pt')
    i = Sources%NB_Pt
    Sources%Pt(i)%Posit = P1
    Sources%Pt(i)%RA1   = R11
    Sources%Pt(i)%RA2   = R12
    Sources%Pt(i)%Space = S1
    CALL PointSource_Prepare(Sources%Pt(i))
  END SUBROUTINE SourceGroup_AddPointSource
  
  !>
  !!   Add & prepare a line source into the source group.
  !!   @param[in]  P1  : the centre of the first point-source.
  !!   @param[in]  S1  : the spacing (dimensionless) of the first point-source.
  !!   @param[in]  R11 : the radius in which spacing S1 applies.
  !!   @param[in]  R12 : the radius in which double spacing (2*S1) applies.
  !!   @param[in]  P2  : the centre of the secnod point-source.
  !!   @param[in]  S2  : the spacing (dimensionless) of the second point-source.
  !!   @param[in]  R21 : the radius in which spacing S2 applies.
  !!   @param[in]  R22 : the radius in which double spacing (2*S2) applies.
  !!   @param[in,out] Sources : the source group.
  !!
  !!   Reminder: the number of line-sources is incremented by 1 automatically 
  !!             in the subroutine, so when you allocate the source group by
  !!             calling SourceGroup_Allocate(), set argument 
  !!             SetNumber  = .false. 
  !<
  SUBROUTINE SourceGroup_AddLineSource    (Sources,P1,S1,R11,R12, P2,S2,R21,R22)
    IMPLICIT NONE
    REAL*8, INTENT(in) :: P1(3),S1,R11,R12, P2(3),S2,R21,R22
    TYPE (SourceGroup_Type), INTENT(INOUT) :: Sources
    INTEGER :: i
    Sources%NB_Ln = Sources%NB_Ln + 1
    IF(Sources%NB_Ln>SIZE(Sources%Ln)) CALL Error_STOP ( ' size of Sources%Ln')
    i = Sources%NB_Ln
    Sources%Ln(i)%PS1%Posit = P1
    Sources%Ln(i)%PS1%RA1   = R11
    Sources%Ln(i)%PS1%RA2   = R12
    Sources%Ln(i)%PS1%Space = S1
    Sources%Ln(i)%PS2%Posit = P2
    Sources%Ln(i)%PS2%RA1   = R21
    Sources%Ln(i)%PS2%RA2   = R22
    Sources%Ln(i)%PS2%Space = S2
    CALL LineSource_Prepare(Sources%Ln(i))
  END SUBROUTINE SourceGroup_AddLineSource
  
  !>
  !!   Add & prepare a triangular source into the source group.
  !!   @param[in]  P1  : the centre of the first point-source.
  !!   @param[in]  S1  : the spacing (dimensionless) of the first point-source.
  !!   @param[in]  R11 : the radius in which spacing S1 applies.
  !!   @param[in]  R12 : the radius in which double spacing (2*S1) applies.
  !!   @param[in]  P2  : the centre of the secnod point-source.
  !!   @param[in]  S2  : the spacing (dimensionless) of the second point-source.
  !!   @param[in]  R21 : the radius in which spacing S2 applies.
  !!   @param[in]  R22 : the radius in which double spacing (2*S2) applies.
  !!   @param[in]  P3  : the centre of the third point-source.
  !!   @param[in]  S3  : the spacing (dimensionless) of the third point-source.
  !!   @param[in]  R31 : the radius in which spacing S3 applies.
  !!   @param[in]  R32 : the radius in which double spacing (2*S3) applies.
  !!   @param[in,out] Sources : the source group.
  !!
  !!   Reminder: the number of tri-sources is incremented by 1 automatically 
  !!             in the subroutine, so when you allocate the source group by
  !!             calling SourceGroup_Allocate(), set argument 
  !!             SetNumber  = .false. 
  !<
  SUBROUTINE SourceGroup_AddTriSource     (Sources,P1,S1,R11,R12, P2,S2,R21,R22, P3,S3,R31,R32)
    IMPLICIT NONE
    REAL*8, INTENT(in) :: P1(3),S1,R11,R12, P2(3),S2,R21,R22, P3(3),S3,R31,R32
    TYPE (SourceGroup_Type), INTENT(INOUT) :: Sources
    INTEGER :: i
    Sources%NB_Tri = Sources%NB_Tri + 1
    IF(Sources%NB_Tri>SIZE(Sources%Tri)) CALL Error_STOP ( ' size of Sources%Tri')
    i = Sources%NB_Tri
    Sources%Tri(i)%PS1%Posit = P1
    Sources%Tri(i)%PS1%RA1   = R11
    Sources%Tri(i)%PS1%RA2   = R12
    Sources%Tri(i)%PS1%Space = S1
    Sources%Tri(i)%PS2%Posit = P2
    Sources%Tri(i)%PS2%RA1   = R21
    Sources%Tri(i)%PS2%RA2   = R22
    Sources%Tri(i)%PS2%Space = S2
    Sources%Tri(i)%PS3%Posit = P3
    Sources%Tri(i)%PS3%RA1   = R31
    Sources%Tri(i)%PS3%RA2   = R32
    Sources%Tri(i)%PS3%Space = S3
    CALL TriSource_Prepare(Sources%Tri(i))
  END SUBROUTINE SourceGroup_AddTriSource

  !>
  !!   Add & prepare a Torus (ring) source into the source group.
  !!   @param[in]  P1,P2,P3  : the three point that the Torus (ring) goes through.
  !!   @param[in]  S1  : the basic spacing (dimensionless).
  !!   @param[in]  R11 : the radius in which the basic spacing S1 applies.
  !!   @param[in]  R12 : the radius in which the double spacing (2*S1) applies.
  !!   @param[in]  STR1  : the streach. 
  !!   @param[in,out] Sources : the source group.
  !<
  SUBROUTINE SourceGroup_AddRingSource    (Sources,P1,S1,R11,R12,STR1, P2, P3)
    IMPLICIT NONE
    REAL*8, INTENT(in) :: P1(3),S1,R11,R12,STR1, P2(3), P3(3)
    TYPE (SourceGroup_Type), INTENT(INOUT) :: Sources
    REAL*8 :: pp1(2), pp2(2), pp3(2)
    INTEGER :: i
    Sources%NB_Ring = Sources%NB_Ring + 1
    IF(Sources%NB_Ring>SIZE(Sources%Ring)) CALL Error_STOP ( ' size of Sources%Ring')
    i = Sources%NB_Ring
    Sources%Ring(i)%aPlane = Plane3D_Build(P1,P2,P3)
    CALL Plane3D_buildTangentAxes(Sources%Ring(i)%aPlane)
    pp1 = Plane3D_project2D(Sources%Ring(i)%aPlane,P1)
    pp2 = Plane3D_project2D(Sources%Ring(i)%aPlane,P2)
    pp3 = Plane3D_project2D(Sources%Ring(i)%aPlane,P3)
    Sources%Ring(i)%P0(:)  = Circle_Centre_2D(pp1,pp2,pp3)
    pp1 = pp1 - Sources%Ring(i)%P0
    Sources%Ring(i)%RA0    = dsqrt(pp1(1)*pp1(1) + pp1(2)*pp1(2))
    Sources%Ring(i)%RA1    = R11
    Sources%Ring(i)%RA2    = R12
    Sources%Ring(i)%Space  = S1
    Sources%Ring(i)%Stretch= STR1
    CALL RingSource_Prepare(Sources%Ring(i))
  END SUBROUTINE SourceGroup_AddRingSource

  !>
  !!   Add & prepare an anisotropic point source into the source group.
  !!   @param[in]  P1  : the centre of the point-source.
  !!   @param[in]  S1  : the spacing (dimensionless) of the first point-source.
  !!   @param[in]  R11 : the radius in which spacing S1 applies.
  !!   @param[in]  R12 : the radius in which double spacing (2*S1) applies.
  !!   @param[in]  STR1  : the stretching at the point-source.
  !!   @param[in]  Dirct : the direction of stretching.
  !!   @param[in,out] Sources : the source group.
  !<
  SUBROUTINE SourceGroup_AddAnisPointSource   (Sources,P1,S1,R11,R12,STR1,Dirct)
    IMPLICIT NONE
    REAL*8, INTENT(in) :: P1(3),S1,R11,R12,STR1, Dirct(3,3)
    TYPE (SourceGroup_Type), INTENT(INOUT) :: Sources
    INTEGER :: i
    Sources%NB_APt = Sources%NB_APt + 1
    IF(Sources%NB_APt>SIZE(Sources%APt)) CALL Error_STOP ( ' size of Sources%APt')
    i = Sources%NB_APt
    Sources%APt(i)%Posit   = P1
    Sources%APt(i)%RA1     = R11
    Sources%APt(i)%RA2     = R12
    Sources%APt(i)%Space   = S1
    Sources%APt(i)%Stretch = STR1
    Sources%APt(i)%Dirct   = Dirct
    CALL Anis_PointSource_Prepare(Sources%APt(i))
  END SUBROUTINE SourceGroup_AddAnisPointSource

  !>
  !!   Add & prepare an anisotropic line source into the source group.
  !!   @param[in]  P1  : the centre of the first point of the line source.
  !!   @param[in]  S1  : the spacing (dimensionless) at the first point of the line source source.
  !!   @param[in]  R11 : the radius in which spacing S1 applies.
  !!   @param[in]  R12 : the radius in which double spacing (2*S1) applies.
  !!   @param[in]  STR1 : the stretching at the first point of the line source.
  !!   @param[in]  P2  : the centre of the second point of the line source.
  !!   @param[in]  S2  : the spacing (dimensionless) at the second point of the line source.
  !!   @param[in]  R21 : the radius in which spacing S2 applies.
  !!   @param[in]  R22 : the radius in which double spacing (2*S2) applies.
  !!   @param[in]  STR2 : the stretching at the second point of the line source.
  !!   @param[in,out] Sources : the source group.
  !<
  SUBROUTINE SourceGroup_AddAnisLineSource(Sources,P1,S1,R11,R12,STR1, P2,S2,R21,R22,STR2)
    IMPLICIT NONE
    REAL*8, INTENT(in) :: P1(3),S1,R11,R12,STR1, P2(3),S2,R21,R22,STR2
    TYPE (SourceGroup_Type), INTENT(INOUT) :: Sources
    INTEGER :: i
    Sources%NB_ALn = Sources%NB_ALn + 1
    IF(Sources%NB_ALn>SIZE(Sources%ALn)) CALL Error_STOP ( ' size of Sources%ALn')
    i = Sources%NB_ALn
    Sources%ALn(i)%PS1%Posit   = P1
    Sources%ALn(i)%PS1%RA1     = R11
    Sources%ALn(i)%PS1%RA2     = R12
    Sources%ALn(i)%PS1%Space   = S1
    Sources%ALn(i)%PS1%Stretch = STR1
    Sources%ALn(i)%PS2%Posit   = P2
    Sources%ALn(i)%PS2%RA1     = R21
    Sources%ALn(i)%PS2%RA2     = R22
    Sources%ALn(i)%PS2%Space   = S2
    Sources%ALn(i)%PS2%Stretch = STR2
    CALL Anis_LineSource_Prepare(Sources%ALn(i))
  END SUBROUTINE SourceGroup_AddAnisLineSource

  !>
  !!   Add & prepare an anisotropic triangular source into the source group.
  !!   @param[in]  P1  : the centre of the first point of the triangular source.
  !!   @param[in]  S1  : the spacing (dimensionless) of the first point of the triangular source.
  !!   @param[in]  R11 : the radius in which spacing S1 applies.
  !!   @param[in]  R12 : the radius in which double spacing (2*S1) applies.
  !!   @param[in]  STR1 : the stretching at the first point of the triangular source.
  !!   @param[in]  P2  : the centre of the secnod point of the triangular source.
  !!   @param[in]  S2  : the spacing (dimensionless) of the second point-source.
  !!   @param[in]  R21 : the radius in which spacing S2 applies.
  !!   @param[in]  R22 : the radius in which double spacing (2*S2) applies.
  !!   @param[in]  STR2 : the stretching at the second point of the triangular source.
  !!   @param[in]  P3  : the centre of the third point of the triangular source.
  !!   @param[in]  S3  : the spacing (dimensionless) of the third point-source.
  !!   @param[in]  R31 : the radius in which spacing S3 applies.
  !!   @param[in]  R32 : the radius in which double spacing (2*S3) applies.
  !!   @param[in]  STR3 : the stretching at the third point of the triangular source.
  !!   @param[in,out] Sources : the source group.
  !<
  SUBROUTINE SourceGroup_AddAnisTriSource (Sources,P1,S1,R11,R12,STR1,  &
       P2,S2,R21,R22,STR2, P3,S3,R31,R32,STR3)
    IMPLICIT NONE
    REAL*8, INTENT(in) :: P1(3),S1,R11,R12,STR1
    REAL*8, INTENT(in) :: P2(3),S2,R21,R22,STR2
    REAL*8, INTENT(in) :: P3(3),S3,R31,R32,STR3
    TYPE (SourceGroup_Type), INTENT(INOUT) :: Sources
    INTEGER :: i
    Sources%NB_ATr = Sources%NB_ATr + 1
    IF(Sources%NB_ATr>SIZE(Sources%ATr)) CALL Error_STOP ( ' size of Sources%ATr')
    i = Sources%NB_ATr
    Sources%ATr(i)%PS1%Posit   = P1
    Sources%ATr(i)%PS1%RA1     = R11
    Sources%ATr(i)%PS1%RA2     = R12
    Sources%ATr(i)%PS1%Space   = S1
    Sources%ATr(i)%PS1%Stretch = STR1
    Sources%ATr(i)%PS2%Posit   = P2
    Sources%ATr(i)%PS2%RA1     = R21
    Sources%ATr(i)%PS2%RA2     = R22
    Sources%ATr(i)%PS2%Space   = S2
    Sources%ATr(i)%PS2%Stretch = STR2
    Sources%ATr(i)%PS3%Posit   = P3
    Sources%ATr(i)%PS3%RA1     = R31
    Sources%ATr(i)%PS3%RA2     = R32
    Sources%ATr(i)%PS3%Space   = S3
    Sources%ATr(i)%PS3%Stretch = STR3
    CALL Anis_TriSource_Prepare(Sources%ATr(i))
  END SUBROUTINE SourceGroup_AddAnisTriSource

  !>
  !!    Set two source groups into one
  !!    @param[in]  Sources,Sources2  : the two source groups
  !!    @param[out] Sources           : the sum of then.
  !<
  SUBROUTINE SourceGroup_AddGroup(Sources, Sources2)
    IMPLICIT NONE
    TYPE (SourceGroup_Type), INTENT(INOUT) :: Sources
    TYPE (SourceGroup_Type), INTENT(IN)    :: Sources2
    TYPE (SourceGroup_Type)                :: Sources1
    INTEGER :: i
    CALL SourceGroup_Copy(Sources, Sources1)
    CALL SourceGroup_Clear(Sources)
    Sources%NB_Pt   = Sources1%NB_Pt   + Sources2%NB_Pt
    Sources%NB_Ln   = Sources1%NB_Ln   + Sources2%NB_Ln
    Sources%NB_Tri  = Sources1%NB_Tri  + Sources2%NB_Tri
    Sources%NB_Ring = Sources1%NB_Ring + Sources2%NB_Ring
    Sources%NB_APt  = Sources1%NB_APt  + Sources2%NB_APt
    Sources%NB_ALn  = Sources1%NB_ALn  + Sources2%NB_ALn
    Sources%NB_ATr  = Sources1%NB_ATr  + Sources2%NB_ATr
    ALLOCATE(Sources%Pt(  Sources%NB_Pt))
    ALLOCATE(Sources%Ln(  Sources%NB_Ln))
    ALLOCATE(Sources%Tri( Sources%NB_Tri))
    ALLOCATE(Sources%Ring(Sources%NB_Ring))
    ALLOCATE(Sources%APt( Sources%NB_APt))
    ALLOCATE(Sources%ALn( Sources%NB_ALn))
    ALLOCATE(Sources%ATr( Sources%NB_ATr))
    DO i = 1, Sources1%NB_Pt
       Sources%Pt(i)   = Sources1%Pt(i)
    ENDDO
    DO i = 1, Sources1%NB_Ln
       Sources%Ln(i )  = Sources1%Ln(i)
    ENDDO
    DO i = 1, Sources1%NB_Tri
       Sources%Tri(i)  = Sources1%Tri(i)
    ENDDO
    DO i = 1, Sources1%NB_Ring
       Sources%Ring(i) = Sources1%Ring(i)
    ENDDO
    DO i = 1, Sources1%NB_APt
       Sources%APt(i)  = Sources1%APt(i)
    ENDDO
    DO i = 1, Sources1%NB_ALn
       Sources%ALn(i)  = Sources1%ALn(i)
    ENDDO
    DO i = 1, Sources1%NB_ATr
       Sources%ATr(i)  = Sources1%ATr(i)
    ENDDO
    DO i = 1, Sources2%NB_Pt
       Sources%Pt(i+Sources1%NB_Pt)     = Sources2%Pt(i)
    ENDDO
    DO i = 1, Sources2%NB_Ln
       Sources%Ln(i+Sources1%NB_Ln)     = Sources2%Ln(i)
    ENDDO
    DO i = 1, Sources2%NB_Tri
       Sources%Tri(i+Sources1%NB_Tri)   = Sources2%Tri(i)
    ENDDO
    DO i = 1, Sources2%NB_Ring
       Sources%Ring(i+Sources1%NB_Ring) = Sources2%Ring(i)
    ENDDO
    DO i = 1, Sources2%NB_APt
       Sources%APt(i+Sources1%NB_APt)   = Sources2%APt(i)
    ENDDO
    DO i = 1, Sources2%NB_ALn
       Sources%ALn(i+Sources1%NB_ALn)   = Sources2%ALn(i)
    ENDDO
    DO i = 1, Sources2%NB_ATr
       Sources%ATr(i+Sources1%NB_ATr)   = Sources2%ATr(i)
    ENDDO
    CALL SourceGroup_Clear(Sources1)
  END SUBROUTINE SourceGroup_AddGroup
  
  
  !>
  !!    Copy a source group to another.
  !<
  SUBROUTINE SourceGroup_Copy(Sources,Sources1)
    IMPLICIT NONE
    TYPE (SourceGroup_Type), INTENT(IN)    :: Sources
    TYPE (SourceGroup_Type), INTENT(OUT)   :: Sources1
    INTEGER :: i
    Sources1%NB_Pt   = Sources%NB_Pt
    Sources1%NB_Ln   = Sources%NB_Ln
    Sources1%NB_Tri  = Sources%NB_Tri
    Sources1%NB_Ring = Sources%NB_Ring
    Sources1%NB_APt  = Sources%NB_APt
    Sources1%NB_ALn  = Sources%NB_ALn
    Sources1%NB_ATr  = Sources%NB_ATr
    ALLOCATE(Sources1%Pt(  SIZE(Sources%Pt)  ))
    ALLOCATE(Sources1%Ln(  SIZE(Sources%Ln)  ))
    ALLOCATE(Sources1%Tri( SIZE(Sources%Tri) ))
    ALLOCATE(Sources1%Ring(SIZE(Sources%Ring) ))
    ALLOCATE(Sources1%APt( SIZE(Sources%APt) ))
    ALLOCATE(Sources1%ALn( SIZE(Sources%ALn)  ))
    ALLOCATE(Sources1%ATr( SIZE(Sources%ATr) ))
    DO i = 1, Sources%NB_Pt
       Sources1%Pt(i)   = Sources%Pt(i)
    ENDDO
    DO i = 1, Sources%NB_Ln
       Sources1%Ln(i )  = Sources%Ln(i)
    ENDDO
    DO i = 1, Sources%NB_Tri
       Sources1%Tri(i)  = Sources%Tri(i)
    ENDDO
    DO i = 1, Sources%NB_Ring
       Sources1%Ring(i) = Sources%Ring(i)
    ENDDO
    DO i = 1, Sources%NB_APt
       Sources1%APt(i)  = Sources%APt(i)
    ENDDO
    DO i = 1, Sources%NB_ALn
       Sources1%ALn(i)  = Sources%ALn(i)
    ENDDO
    DO i = 1, Sources%NB_ATr
       Sources1%ATr(i)  = Sources%ATr(i)
    ENDDO
  END SUBROUTINE SourceGroup_Copy
  
  !>
  !!  Prepare a Point Source to make it ready to use.
  !<
  SUBROUTINE PointSource_Prepare(PS)
    IMPLICIT NONE
    TYPE(PointSource_Type), INTENT(INOUT) ::  PS
    IF(PS%ready) RETURN
    PS%decay = log2/(PS%RA2- PS%RA1)
    PS%RA2   = -dlog(PS%Space) / PS%decay + PS%RA1
    PS%ready = .TRUE.
  END SUBROUTINE PointSource_Prepare

  !>
  !!  Prepare a Line Source to make it ready to use.
  !<
  SUBROUTINE LineSource_Prepare(LS)
    IMPLICIT NONE
    TYPE(LineSource_Type), INTENT(INOUT) ::  LS
    IF(LS%ready) RETURN
    LS%EndCut = 0
    IF(LS%PS1%Space<0)THEN
       LS%PS1%Space = -LS%PS1%Space
       LS%EndCut = 1
    ENDIF
    IF(LS%PS2%Space<0)THEN
       LS%PS2%Space = -LS%PS2%Space
       LS%EndCut = LS%EndCut + 2
    ENDIF
    CALL PointSource_Prepare(LS%PS1)
    CALL PointSource_Prepare(LS%PS2)
    LS%ready = .TRUE.
  END SUBROUTINE LineSource_Prepare

  !>
  !!  Prepare a Triangular Source to make it ready to use.
  !<
  SUBROUTINE TriSource_Prepare(TS)
    IMPLICIT NONE
    TYPE(TriSource_Type), INTENT(INOUT) ::  TS
    IF(TS%ready) RETURN
    CALL PointSource_Prepare(TS%PS1)
    CALL PointSource_Prepare(TS%PS2)
    CALL PointSource_Prepare(TS%PS3)
    TS%aPlane   = Plane3D_Build(TS%PS1%Posit, TS%PS2%Posit, TS%PS3%Posit)
    CALL Plane3D_buildTangentAxes(TS%aPlane)
    TS%P2D(:,1) = Plane3D_project2D(TS%aPlane, TS%PS1%Posit)
    TS%P2D(:,2) = Plane3D_project2D(TS%aPlane, TS%PS2%Posit)
    TS%P2D(:,3) = Plane3D_project2D(TS%aPlane, TS%PS3%Posit)
    TS%dbarea   = getDoubleArea2D(TS%P2D(:,1), TS%P2D(:,2), TS%P2D(:,3) )
    TS%LS1%PS1  = TS%PS2
    TS%LS1%PS2  = TS%PS3
    TS%LS2%PS1  = TS%PS3
    TS%LS2%PS2  = TS%PS1
    TS%LS3%PS1  = TS%PS1
    TS%LS3%PS2  = TS%PS2
    TS%ready    = .TRUE.
  END SUBROUTINE TriSource_Prepare

  !>
  !!  Prepare a Torus (ring) Source to make it ready to use.
  !<
  SUBROUTINE RingSource_Prepare(RS)
    IMPLICIT NONE
    TYPE(RingSource_Type), INTENT(INOUT) ::  RS   
    IF(RS%ready) RETURN
    RS%decaySp = log2/(RS%RA2- RS%RA1)
    RS%RA2     = -dlog(RS%Space) / RS%decaySp + RS%RA1
    RS%decaySt = -dlog(RS%Stretch) / (RS%RA2-RS%RA1)
    RS%ready   = .TRUE.
  END SUBROUTINE RingSource_Prepare

  !>
  !!  Prepare an Anisotropic Point Source to make it ready to use.
  !<
  SUBROUTINE Anis_PointSource_Prepare(PS)
    IMPLICIT NONE
    TYPE(Anis_PointSource_Type), INTENT(INOUT) ::  PS   
    IF(PS%ready) RETURN
    PS%decaySp = log2/(PS%RA2- PS%RA1)
    PS%RA2     = -dlog(PS%Space) / PS%decaySp + PS%RA1
    PS%decaySt = -dlog(PS%Stretch) / (PS%RA2-PS%RA1)
    PS%ready   = .TRUE.
  END SUBROUTINE Anis_PointSource_Prepare

  !>
  !!  Prepare an Anisotropic Line Source to make it ready to use.
  !<
  SUBROUTINE Anis_LineSource_Prepare(LS)
    IMPLICIT NONE
    TYPE(Anis_LineSource_Type), INTENT(INOUT) ::  LS
    REAL*8  :: dd1(3), dd2(3), dd3(3)
    IF(LS%ready) RETURN
    dd1(1) = LS%PS2%Posit(1) - LS%PS1%Posit(1)
    dd1(2) = LS%PS2%Posit(2) - LS%PS1%Posit(2)
    dd1(3) = LS%PS2%Posit(3) - LS%PS1%Posit(3)
    dd1(:) = dd1(:)  / Geo3D_Distance(dd1)
    CALL Geo3D_BuildOrthogonalAxes(dd1,dd2,dd3)
    LS%DIRCT(1,:) = dd1(:)
    LS%DIRCT(2,:) = dd2(:)
    LS%DIRCT(3,:) = dd3(:)
    LS%EndCut = 0
    IF(LS%PS1%Space<0)THEN
       LS%PS1%Space = -LS%PS1%Space
       LS%EndCut = 1
    ENDIF
    IF(LS%PS2%Space<0)THEN
       LS%PS2%Space = -LS%PS2%Space
       LS%EndCut = LS%EndCut + 2
    ENDIF
    CALL Anis_PointSource_Prepare(LS%PS1)
    CALL Anis_PointSource_Prepare(LS%PS2)
    LS%ready = .TRUE.
  END SUBROUTINE Anis_LineSource_Prepare

  !>
  !!  Prepare an Anisotropic Triangular Source to make it ready to use.
  !<
  SUBROUTINE Anis_TriSource_Prepare(TS)
    IMPLICIT NONE
    TYPE(Anis_TriSource_Type), INTENT(INOUT) ::  TS
    REAL*8  :: dd1(3), dd2(3), dd3(3)
    IF(TS%ready) RETURN
    CALL Anis_PointSource_Prepare(TS%PS1)
    CALL Anis_PointSource_Prepare(TS%PS2)
    CALL Anis_PointSource_Prepare(TS%PS3)
    TS%aPlane   = Plane3D_Build(TS%PS1%Posit, TS%PS2%Posit, TS%PS3%Posit)
    CALL Plane3D_buildTangentAxes(TS%aPlane)
    TS%P2D(:,1) = Plane3D_project2D(TS%aPlane, TS%PS1%Posit)
    TS%P2D(:,2) = Plane3D_project2D(TS%aPlane, TS%PS2%Posit)
    TS%P2D(:,3) = Plane3D_project2D(TS%aPlane, TS%PS3%Posit)
    TS%dbarea   = getDoubleArea2D(TS%P2D(:,1), TS%P2D(:,2), TS%P2D(:,3) )

    dd1(:) = TS%PS2%Posit(:) - TS%PS1%Posit(:)
    dd1(:) = dd1(:)  / Geo3D_Distance(dd1)
    CALL Geo3D_BuildOrthogonalAxes(dd1,dd2,dd3)
    TS%DIRCT(1,:) = dd1(:)
    TS%DIRCT(2,:) = dd2(:)
    TS%DIRCT(3,:) = dd3(:)

    TS%LS1%PS1     = TS%PS2
    TS%LS1%PS2     = TS%PS3
    TS%LS1%DIRCT   = TS%DIRCT
    TS%LS1%ready   = .TRUE.
    TS%LS2%PS1     = TS%PS3
    TS%LS2%PS2     = TS%PS1
    TS%LS2%DIRCT   = TS%DIRCT
    TS%LS2%ready   = .TRUE.
    TS%LS3%PS1     = TS%PS1
    TS%LS3%PS2     = TS%PS2
    TS%LS3%DIRCT   = TS%DIRCT
    TS%LS3%ready   = .TRUE.

    TS%ready = .TRUE.
  END SUBROUTINE Anis_TriSource_Prepare

  !>
  !!     Prepare a source group. 
  !!     This is a must before the sources being applied if they are added by hand.
  !!          (i.e. not through subroutine SourceGroup_Add***)
  !!     There is no harm to call this subroutine if the sources already prepared.
  !<
  SUBROUTINE SourceGroup_Prepare(Sources)
    IMPLICIT NONE
    TYPE (SourceGroup_Type), INTENT(INOUT) :: Sources
    INTEGER :: i
    DO i=1,Sources%NB_Pt
       CALL PointSource_Prepare(Sources%Pt(i))
    ENDDO
    DO i=1,Sources%NB_Ln
       CALL LineSource_Prepare(Sources%Ln(i))
    ENDDO
    DO i=1,Sources%NB_Tri
       CALL TriSource_Prepare(Sources%Tri(i))
    ENDDO
    DO i=1,Sources%NB_Ring
       CALL RingSource_Prepare(Sources%Ring(i))
    ENDDO
    DO i=1,Sources%NB_APt
       CALL Anis_PointSource_Prepare(Sources%APt(i))
    ENDDO
    DO i=1,Sources%NB_ALn
       CALL Anis_LineSource_Prepare(Sources%ALn(i))
    ENDDO
    DO i=1,Sources%NB_ATr
       CALL Anis_TriSource_Prepare(Sources%ATr(i))
    ENDDO
  END SUBROUTINE SourceGroup_Prepare


  FUNCTION PointSource_GetScale(P,PS) RESULT(S)
    IMPLICIT NONE
    TYPE(PointSource_Type), INTENT(IN) ::  PS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    REAL*8 :: dd, t
    IF(.NOT. PS%ready) CALL Error_STOP ( 'PointSource is not ready')
    dd = Geo3D_Distance(P,PS%Posit)
    IF(dd>=PS%RA2)THEN
       S = 1.d0
    ELSE IF(dd<=PS%RA1)THEN
       S = PS%Space
    ELSE
       S = PS%Space * EXP(PS%decay * (dd-PS%RA1))
    ENDIF
  END FUNCTION PointSource_GetScale

  
  FUNCTION PointSource_GetGradient(P,PS) RESULT(S)
    IMPLICIT NONE
    TYPE(PointSource_Type), INTENT(IN) ::  PS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    REAL*8 :: dd, t
    IF(.NOT. PS%ready) CALL Error_STOP ( 'PointSource is not ready')
    dd = Geo3D_Distance(P,PS%Posit)
    IF(dd>=PS%RA2)THEN
       S = 0.d0
    ELSE
       S = ABS(LOG(PS%Space)) / PS%RA2
    ENDIF
  END FUNCTION PointSource_GetGradient

  
  FUNCTION LineSource_GetScale(P,LS) RESULT(S)
    IMPLICIT NONE
    TYPE(LineSource_Type), INTENT(IN) ::  LS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    TYPE(PointSource_Type) :: PS   
    REAL*8 :: p1(3), t, dd
    p1 = Geo3D_ProjectOnLine(LS%PS1%Posit, LS%PS2%Posit, P, t)
    IF(t<=0)THEN
       IF(LS%EndCut==1 .OR. LS%EndCut==3)THEN
          S = 1.d0
       ELSE
          S = PointSource_GetScale(P,LS%PS1)
       ENDIF
    ELSE IF(t>=1)THEN
       IF(LS%EndCut==2 .OR. LS%EndCut==3)THEN
          S = 1.d0
       ELSE
          S = PointSource_GetScale(P,LS%PS2)
       ENDIF
    ELSE
       PS%RA1   = LS%PS1%RA1   + t*(LS%PS2%RA1   - LS%PS1%RA1)
       PS%RA2   = LS%PS1%RA2   + t*(LS%PS2%RA2   - LS%PS1%RA2)
       PS%Space = LS%PS1%Space + t*(LS%PS2%Space - LS%PS1%Space)
       dd = Geo3D_Distance(P,p1)
       IF(dd>=PS%RA2)THEN
          S = 1.d0
       ELSE IF(dd<=PS%RA1)THEN
          S = PS%Space
       ELSE
          PS%decay = -dlog(PS%Space) / (PS%RA2-PS%RA1)
          S = PS%Space * EXP(PS%decay * (dd-PS%RA1))
       ENDIF
    ENDIF
  END FUNCTION LineSource_GetScale

  
  FUNCTION LineSource_GetGradient(P,LS) RESULT(S)
    IMPLICIT NONE
    TYPE(LineSource_Type), INTENT(IN) ::  LS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    TYPE(PointSource_Type) :: PS   
    REAL*8 :: p1(3), t, dd
    p1 = Geo3D_ProjectOnLine(LS%PS1%Posit, LS%PS2%Posit, P, t)
    IF(t<=0)THEN
       IF(LS%EndCut==1 .OR. LS%EndCut==3)THEN
          S = 0.d0
       ELSE
          S = PointSource_GetGradient(P,LS%PS1)
       ENDIF
    ELSE IF(t>=1)THEN
       IF(LS%EndCut==2 .OR. LS%EndCut==3)THEN
          S = 0.d0
       ELSE
          S = PointSource_GetGradient(P,LS%PS2)
       ENDIF
    ELSE
       PS%RA2   = LS%PS1%RA2   + t*(LS%PS2%RA2   - LS%PS1%RA2)
       PS%Space = LS%PS1%Space + t*(LS%PS2%Space - LS%PS1%Space)
       dd = Geo3D_Distance(P,p1)
       IF(dd>=PS%RA2)THEN
          S = 0.d0
       ELSE
          S = ABS(LOG(PS%Space)) / PS%RA2
       ENDIF
    ENDIF
  END FUNCTION LineSource_GetGradient

  
  FUNCTION TriSource_GetScale(P,TS) RESULT(S)
    IMPLICIT NONE
    TYPE(TriSource_Type), INTENT(IN) ::  TS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    TYPE(PointSource_Type) :: PS   
    REAL*8 :: t, a(3), P2D(2), dd
    INTEGER :: j, n

    dd = Plane3D_DistancePointToPlane(P,TS%aPlane)
    IF(dd>=MAX(TS%PS1%RA2, TS%PS2%RA2, TS%PS3%RA2))THEN
       S = 1.0         
       RETURN
    ENDIF

    P2D(:) = Plane3D_project2D(TS%aPlane, P)
    a(1) = getDoubleArea2D(P2D, TS%P2D(:,2), TS%P2D(:,3))
    a(2) = getDoubleArea2D(TS%P2D(:,1), P2D, TS%P2D(:,3))
    a(1:2) = a(1:2) / TS%dbarea
    a(3) = 1.d0 - a(1) - a(2)
    n = 0
    DO j=1,3
       IF(a(j)<=0) n = n+1
    ENDDO

    IF(n==2)THEN     
       IF(a(1)>0.999)THEN
          S = PointSource_GetScale(P,TS%PS1)
       ELSE IF(a(2)>0.999)THEN
          S = PointSource_GetScale(P,TS%PS2)
       ELSE IF(a(3)>0.999)THEN
          S = PointSource_GetScale(P,TS%PS3)
       ELSE
          CALL Error_STOP ( 'wrong weight---1')
       ENDIF
    ELSE IF(n==1)THEN
       IF(a(1)<=0)THEN
          S = LineSource_GetScale(P,TS%LS1)
       ELSE IF(a(2)<=0)THEN
          S = LineSource_GetScale(P,TS%LS2)
       ELSE IF(a(3)<=0)THEN
          S = LineSource_GetScale(P,TS%LS3)
       ELSE
          CALL Error_STOP ( 'wrong weight---2')
       ENDIF
    ELSE
       PS%RA1   = a(1)*TS%PS1%RA1   + a(2)*TS%PS2%RA1   + a(3)*TS%PS3%RA1
       PS%RA2   = a(1)*TS%PS1%RA2   + a(2)*TS%PS2%RA2   + a(3)*TS%PS3%RA2
       PS%Space = a(1)*TS%PS1%Space + a(2)*TS%PS2%Space + a(3)*TS%PS3%Space
       IF(dd>=PS%RA2)THEN
          S = 1.d0
       ELSE IF(dd<=PS%RA1)THEN
          S = PS%Space
       ELSE
          PS%decay = -dlog(PS%Space) / (PS%RA2-PS%RA1)
          S = PS%Space * EXP(PS%decay * (dd-PS%RA1))
       ENDIF
    ENDIF

  END FUNCTION TriSource_GetScale

  
  FUNCTION TriSource_GetGradient(P,TS) RESULT(S)
    IMPLICIT NONE
    TYPE(TriSource_Type), INTENT(IN) ::  TS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    TYPE(PointSource_Type) :: PS   
    REAL*8 :: t, a(3), P2D(2), dd
    INTEGER :: j, n

    dd = Plane3D_DistancePointToPlane(P,TS%aPlane)
    IF(dd>=MAX(TS%PS1%RA2, TS%PS2%RA2, TS%PS3%RA2))THEN
       S = 0.d0         
       RETURN
    ENDIF

    P2D(:) = Plane3D_project2D(TS%aPlane, P)
    a(1) = getDoubleArea2D(P2D, TS%P2D(:,2), TS%P2D(:,3))
    a(2) = getDoubleArea2D(TS%P2D(:,1), P2D, TS%P2D(:,3))
    a(1:2) = a(1:2) / TS%dbarea
    a(3) = 1.d0 - a(1) - a(2)
    n = 0
    DO j=1,3
       IF(a(j)<=0) n = n+1
    ENDDO

    IF(n==2)THEN     
       IF(a(1)>0.999)THEN
          S = PointSource_GetGradient(P,TS%PS1)
       ELSE IF(a(2)>0.999)THEN
          S = PointSource_GetGradient(P,TS%PS2)
       ELSE IF(a(3)>0.999)THEN
          S = PointSource_GetGradient(P,TS%PS3)
       ELSE
          CALL Error_STOP ( 'wrong weight---1')
       ENDIF
    ELSE IF(n==1)THEN
       IF(a(1)<=0)THEN
          S = LineSource_GetGradient(P,TS%LS1)
       ELSE IF(a(2)<=0)THEN
          S = LineSource_GetGradient(P,TS%LS2)
       ELSE IF(a(3)<=0)THEN
          S = LineSource_GetGradient(P,TS%LS3)
       ELSE
          CALL Error_STOP ( 'wrong weight---2')
       ENDIF
    ELSE
       PS%RA2   = a(1)*TS%PS1%RA2   + a(2)*TS%PS2%RA2   + a(3)*TS%PS3%RA2
       PS%Space = a(1)*TS%PS1%Space + a(2)*TS%PS2%Space + a(3)*TS%PS3%Space
       IF(dd>=PS%RA2)THEN
          S = 0.d0
       ELSE
          S = ABS(LOG(PS%Space)) / PS%RA2
       ENDIF
    ENDIF

  END FUNCTION TriSource_GetGradient

  
  FUNCTION RingSource_GetScale(P,RS) RESULT(S)
    IMPLICIT NONE
    TYPE(RingSource_Type), INTENT(IN) ::  RS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    REAL*8 :: P2D(2), dd, d2

    S  = 1.d0         
    dd = Plane3D_DistancePointToPlane(P,RS%aPlane)
    IF(dd>=RS%RA2)  RETURN

    P2D(:) = Plane3D_project2D(RS%aPlane, P)
    P2D(:) = P2D - RS%P0
    d2     = dsqrt( P2D(1)*P2D(1) + P2D(2)*P2D(2) )
    d2     = d2 - RS%RA0
    IF(d2>=RS%RA2 .OR. d2<= - RS%RA2)  RETURN

    dd = dsqrt(d2*d2 + dd*dd)
    IF(dd>=RS%RA2)THEN
       S = 1.d0
    ELSE IF(dd<=RS%RA1)THEN
       IF(RS%Stretch<1)THEN
          S = RS%Space * RS%Stretch
       ELSE
          S = RS%Space
       ENDIF
    ELSE
       IF(RS%Stretch<1)THEN
          S = RS%Space * RS%Stretch * EXP((RS%decaySp+RS%decaySt) * (dd-RS%RA1))
       ELSE
          S = RS%Space * EXP(RS%decaySp * (dd-RS%RA1))
       ENDIF
    ENDIF

  END FUNCTION RingSource_GetScale

  
  FUNCTION RingSource_GetMapping(P,RS,iso) RESULT(fMap)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P(3)
    TYPE(RingSource_Type), INTENT(IN) ::  RS   
    INTEGER, INTENT(OUT) :: iso
    REAL*8  :: fMap(3,3)
    REAL*8  :: SS(2), S, vd1(3), vd2(3), vd3(3), p2D(2), dd, d2

    fMap = 0.d0
    fMap(1,1) = 1.d0
    fMap(2,2) = 1.d0
    fMap(3,3) = 1.d0
    iso = 1

    dd = Plane3D_DistancePointToPlane(P,RS%aPlane)
    IF(dd>=RS%RA2)  RETURN

    P2D(:) = Plane3D_project2D(RS%aPlane, P)
    P2D(:) = P2D - RS%P0
    d2     = dsqrt( P2D(1)*P2D(1) + P2D(2)*P2D(2) )
    d2     = d2 - RS%RA0
    IF(d2>=RS%RA2 .OR. d2<= - RS%RA2)  RETURN

    dd = dsqrt(d2*d2 + dd*dd)
    IF(dd>=RS%RA2)THEN
       RETURN
    ELSE IF(dd<=RS%RA1)THEN
       SS = (/RS%Space, RS%Stretch/)
    ELSE
       SS(1) = RS%Space   * EXP(RS%decaySp * (dd-RS%RA1))
       SS(2) = RS%Stretch * EXP(RS%decaySt * (dd-RS%RA1))
    ENDIF

    IF(ABS(SS(2)-1.d0)<1.d-2)THEN
       S = 1.d0 / SS(1)
       fMap(1,1) = S
       fMap(2,2) = S
       fMap(3,3) = S
       iso = 1
    ELSE
       vd1(:) = RS%aPlane%tx(:)*P2D(2) - RS%aPlane%ty(:)*P2D(1) 
       vd1    = vd1 / Geo3D_Distance(vd1)
       CALL Geo3D_BuildOrthogonalAxes(vd1,vd2, vd3)
       fMap(1,:) = vd1 / (SS(1)*SS(2))
       fMap(2,:) = vd2 /  SS(1)
       fMap(3,:) = vd3 /  SS(1)
       iso = 0
    ENDIF

  END FUNCTION RingSource_GetMapping

  
  FUNCTION RingSource_GetGradient(P,RS) RESULT(S)
    IMPLICIT NONE
    TYPE(RingSource_Type), INTENT(IN) ::  RS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    REAL*8 :: P2D(2), dd, d2

    S  = 0.d0         
    dd = Plane3D_DistancePointToPlane(P,RS%aPlane)
    IF(dd>=RS%RA2)  RETURN

    P2D(:) = Plane3D_project2D(RS%aPlane, P)
    P2D(:) = P2D - RS%P0
    d2     = dsqrt( P2D(1)*P2D(1) + P2D(2)*P2D(2) )
    d2     = d2 - RS%RA0
    IF(d2>=RS%RA2 .OR. d2<= - RS%RA2)  RETURN

    dd = dsqrt(d2*d2 + dd*dd)
    IF(dd>=RS%RA2)THEN
       S = 0.d0
    ELSE
       S = MAX(ABS(LOG(RS%Space)), ABS(LOG(RS%Space * RS%Stretch)))
       S = S / RS%RA2
    ENDIF

  END FUNCTION RingSource_GetGradient


  FUNCTION Anis_PointSource_GetScale(P,PS) RESULT(S)
    IMPLICIT NONE
    TYPE(Anis_PointSource_Type), INTENT(IN) ::  PS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    REAL*8 :: dd, t
    IF(.NOT. PS%ready) CALL Error_STOP ( 'Anis_PointSource is not ready')
    dd = Geo3D_Distance(P,PS%Posit)
    IF(dd>=PS%RA2)THEN
       S = 1.d0
    ELSE IF(dd<=PS%RA1)THEN
       IF(PS%Stretch<1)THEN
          S = PS%Space * PS%Stretch
       ELSE
          S = PS%Space
       ENDIF
    ELSE
       IF(PS%Stretch<1)THEN
          S = PS%Space * PS%Stretch * EXP((PS%decaySp+PS%decaySt) * (dd-PS%RA1))
       ELSE
          S = PS%Space * EXP(PS%decaySp * (dd-PS%RA1))
       ENDIF
    ENDIF
  END FUNCTION Anis_PointSource_GetScale

  
  FUNCTION Anis_PointSource_GetSpacing(P,PS) RESULT(SS)
    IMPLICIT NONE
    TYPE(Anis_PointSource_Type), INTENT(IN) ::  PS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: SS(2)
    REAL*8 :: dd, t
    IF(.NOT. PS%ready) CALL Error_STOP ( 'Anis_PointSource is not ready')
    dd = Geo3D_Distance(P,PS%Posit)
    IF(dd>=PS%RA2)THEN
       SS(:) = 1.d0
    ELSE IF(dd<=PS%RA1)THEN
       SS(:) = (/PS%Space, PS%Stretch/)
    ELSE
       SS(1) = PS%Space   * EXP(PS%decaySp * (dd-PS%RA1))
       SS(2) = PS%Stretch * EXP(PS%decaySt * (dd-PS%RA1))
    ENDIF
  END FUNCTION Anis_PointSource_GetSpacing

  
  FUNCTION Anis_PointSource_GetGradient(P,PS) RESULT(S)
    IMPLICIT NONE
    TYPE(Anis_PointSource_Type), INTENT(IN) ::  PS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    REAL*8 :: dd, t
    IF(.NOT. PS%ready) CALL Error_STOP ( 'Anis_PointSource is not ready')
    dd = Geo3D_Distance(P,PS%Posit)
    IF(dd>=PS%RA2)THEN
       S = 0.d0
    ELSE
       S = MAX(ABS(LOG(PS%Space)), ABS(LOG(PS%Space * PS%Stretch)))
       S = S / PS%RA2
    ENDIF
  END FUNCTION Anis_PointSource_GetGradient

  
  FUNCTION Anis_LineSource_GetScale(P,LS) RESULT(S)
    IMPLICIT NONE
    TYPE(Anis_LineSource_Type), INTENT(IN) ::  LS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    TYPE(Anis_PointSource_Type) :: PS   
    REAL*8 :: p1(3), t, dd
    p1 = Geo3D_ProjectOnLine(LS%PS1%Posit, LS%PS2%Posit, P, t)
    IF(t<=0)THEN
       IF(LS%EndCut==1 .OR. LS%EndCut==3)THEN
          S = 1.d0
       ELSE
          S = Anis_PointSource_GetScale(P,LS%PS1)
       ENDIF
    ELSE IF(t>=1)THEN
       IF(LS%EndCut==2 .OR. LS%EndCut==3)THEN
          S = 1.d0
       ELSE
          S = Anis_PointSource_GetScale(P,LS%PS2)
       ENDIF
    ELSE
       PS%RA1     = LS%PS1%RA1     + t*(LS%PS2%RA1     - LS%PS1%RA1)
       PS%RA2     = LS%PS1%RA2     + t*(LS%PS2%RA2     - LS%PS1%RA2)
       PS%Space   = LS%PS1%Space   + t*(LS%PS2%Space   - LS%PS1%Space)
       PS%Stretch = LS%PS1%Stretch + t*(LS%PS2%Stretch - LS%PS1%Stretch)
       dd = Geo3D_Distance(P,p1)
       IF(dd>=PS%RA2)THEN
          S = 1.d0
       ELSE IF(dd<=PS%RA1)THEN
          IF(PS%Stretch<1)THEN
             S = PS%Space * PS%Stretch
          ELSE
             S = PS%Space
          ENDIF
       ELSE
          IF(PS%Stretch<1)THEN
             PS%decaySp = -dlog(PS%Space) / (PS%RA2-PS%RA1)
             PS%decaySt = -dlog(PS%Stretch) / (PS%RA2-PS%RA1)
             S = PS%Space * PS%Stretch * EXP((PS%decaySp+PS%decaySt) * (dd-PS%RA1))
          ELSE
             PS%decaySp = -dlog(PS%Space) / (PS%RA2-PS%RA1)
             S = PS%Space * EXP(PS%decaySp * (dd-PS%RA1))
          ENDIF
       ENDIF
    ENDIF
  END FUNCTION Anis_LineSource_GetScale

  
  FUNCTION Anis_LineSource_GetSpacing(P,LS) RESULT(SS)
    IMPLICIT NONE
    TYPE(Anis_LineSource_Type), INTENT(IN) ::  LS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: SS(2)
    TYPE(Anis_PointSource_Type) :: PS   
    REAL*8 :: p1(3), t, dd
    p1 = Geo3D_ProjectOnLine(LS%PS1%Posit, LS%PS2%Posit, P, t)
    IF(t<=0)THEN
       IF(LS%EndCut==1 .OR. LS%EndCut==3)THEN
          SS(:) = 1.d0
       ELSE
          SS = Anis_PointSource_GetSpacing(P,LS%PS1)
       ENDIF
    ELSE IF(t>=1)THEN
       IF(LS%EndCut==2 .OR. LS%EndCut==3)THEN
          SS(:) = 1.d0
       ELSE
          SS = Anis_PointSource_GetSpacing(P,LS%PS2)
       ENDIF
    ELSE
       PS%RA1     = LS%PS1%RA1     + t*(LS%PS2%RA1     - LS%PS1%RA1)
       PS%RA2     = LS%PS1%RA2     + t*(LS%PS2%RA2     - LS%PS1%RA2)
       PS%Space   = LS%PS1%Space   + t*(LS%PS2%Space   - LS%PS1%Space)
       PS%Stretch = LS%PS1%Stretch + t*(LS%PS2%Stretch - LS%PS1%Stretch)
       dd = Geo3D_Distance(P,p1)
       IF(dd>=PS%RA2)THEN
          SS(:) = 1.d0
       ELSE IF(dd<=PS%RA1)THEN
          SS(:) = (/PS%Space, PS%Stretch/)
       ELSE
          PS%decaySp = -dlog(PS%Space) / (PS%RA2-PS%RA1)
          PS%decaySt = -dlog(PS%Stretch) / (PS%RA2-PS%RA1)
          SS(1) = PS%Space   * EXP(PS%decaySp * (dd-PS%RA1))
          SS(2) = PS%Stretch * EXP(PS%decaySt * (dd-PS%RA1))
       ENDIF
    ENDIF
  END FUNCTION Anis_LineSource_GetSpacing

  
  FUNCTION Anis_LineSource_GetGradient(P,LS) RESULT(S)
    IMPLICIT NONE
    TYPE(Anis_LineSource_Type), INTENT(IN) ::  LS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    TYPE(Anis_PointSource_Type) :: PS   
    REAL*8 :: p1(3), t, dd
    p1 = Geo3D_ProjectOnLine(LS%PS1%Posit, LS%PS2%Posit, P, t)
    IF(t<=0)THEN
       IF(LS%EndCut==1 .OR. LS%EndCut==3)THEN
          S = 0.d0
       ELSE
          S = Anis_PointSource_GetGradient(P,LS%PS1)
       ENDIF
    ELSE IF(t>=1)THEN
       IF(LS%EndCut==2 .OR. LS%EndCut==3)THEN
          S = 0.d0
       ELSE
          S = Anis_PointSource_GetGradient(P,LS%PS2)
       ENDIF
    ELSE
       PS%RA2     = LS%PS1%RA2     + t*(LS%PS2%RA2     - LS%PS1%RA2)
       PS%Space   = LS%PS1%Space   + t*(LS%PS2%Space   - LS%PS1%Space)
       PS%Stretch = LS%PS1%Stretch + t*(LS%PS2%Stretch - LS%PS1%Stretch)
       dd = Geo3D_Distance(P,p1)
       IF(dd>=PS%RA2)THEN
          S = 0.d0
       ELSE
          S = MAX(ABS(LOG(PS%Space)), ABS(LOG(PS%Space * PS%Stretch)))
          S = S / PS%RA2
       ENDIF
    ENDIF
  END FUNCTION Anis_LineSource_GetGradient

  
  FUNCTION Anis_TriSource_GetScale(P,TS) RESULT(S)
    IMPLICIT NONE
    TYPE(Anis_TriSource_Type), INTENT(IN) ::  TS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    TYPE(Anis_PointSource_Type) :: PS   
    REAL*8 :: t, a(3), P2D(2), dd
    INTEGER :: j, n

    dd = Plane3D_DistancePointToPlane(P,TS%aPlane)
    IF(dd>=MAX(TS%PS1%RA2, TS%PS2%RA2, TS%PS3%RA2))THEN
       S = 1.0         
       RETURN
    ENDIF

    P2D(:) = Plane3D_project2D(TS%aPlane, P)
    a(1) = getDoubleArea2D(P2D, TS%P2D(:,2), TS%P2D(:,3))
    a(2) = getDoubleArea2D(TS%P2D(:,1), P2D, TS%P2D(:,3))
    a(1:2) = a(1:2) / TS%dbarea
    a(3) = 1.d0 - a(1) - a(2)
    n = 0
    DO j=1,3
       IF(a(j)<=0) n = n+1
    ENDDO

    IF(n==2)THEN     
       IF(a(1)>0.999)THEN
          S = Anis_PointSource_GetScale(P,TS%PS1)
       ELSE IF(a(2)>0.999)THEN
          S = Anis_PointSource_GetScale(P,TS%PS2)
       ELSE IF(a(3)>0.999)THEN
          S = Anis_PointSource_GetScale(P,TS%PS3)
       ELSE
          CALL Error_STOP ( 'wrong weight---1')
       ENDIF
    ELSE IF(n==1)THEN
       IF(a(1)<=0)THEN
          S = Anis_LineSource_GetScale(P,TS%LS1)
       ELSE IF(a(2)<=0)THEN
          S = Anis_LineSource_GetScale(P,TS%LS2)
       ELSE IF(a(3)<=0)THEN
          S = Anis_LineSource_GetScale(P,TS%LS3)
       ELSE
          CALL Error_STOP ( 'wrong weight---2')
       ENDIF
    ELSE
       PS%RA1     = a(1)*TS%PS1%RA1     + a(2)*TS%PS2%RA1     + a(3)*TS%PS3%RA1
       PS%RA2     = a(1)*TS%PS1%RA2     + a(2)*TS%PS2%RA2     + a(3)*TS%PS3%RA2
       PS%Space   = a(1)*TS%PS1%Space   + a(2)*TS%PS2%Space   + a(3)*TS%PS3%Space
       PS%Stretch = a(1)*TS%PS1%Stretch + a(2)*TS%PS2%Stretch + a(3)*TS%PS3%Stretch
       IF(dd>=PS%RA2)THEN
          S = 1.d0
       ELSE IF(dd<=PS%RA1)THEN
          IF(PS%Stretch<1)THEN
             S = PS%Space * PS%Stretch
          ELSE
             S = PS%Space
          ENDIF
       ELSE
          IF(PS%Stretch<1)THEN
             PS%decaySp = -dlog(PS%Space) / (PS%RA2-PS%RA1)
             PS%decaySt = -dlog(PS%Stretch) / (PS%RA2-PS%RA1)
             S = PS%Space * PS%Stretch * EXP((PS%decaySp+PS%decaySt) * (dd-PS%RA1))
          ELSE
             PS%decaySp = -dlog(PS%Space) / (PS%RA2-PS%RA1)
             S = PS%Space * EXP(PS%decaySp * (dd-PS%RA1))
          ENDIF
       ENDIF
    ENDIF

  END FUNCTION Anis_TriSource_GetScale

  
  FUNCTION Anis_TriSource_GetSpacing(P,TS) RESULT(SS)
    IMPLICIT NONE
    TYPE(Anis_TriSource_Type), INTENT(IN) ::  TS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: SS(2), S
    TYPE(Anis_PointSource_Type) :: PS   
    REAL*8 :: t, a(3), P2D(2), dd
    INTEGER :: j, n

    SS(:) = 1.d0

    dd = Plane3D_DistancePointToPlane(P,TS%aPlane)
    IF(dd>=MAX(TS%PS1%RA2, TS%PS2%RA2, TS%PS3%RA2))THEN
       RETURN
    ENDIF

    P2D(:) = Plane3D_project2D(TS%aPlane, P)
    a(1) = getDoubleArea2D(P2D, TS%P2D(:,2), TS%P2D(:,3))
    a(2) = getDoubleArea2D(TS%P2D(:,1), P2D, TS%P2D(:,3))
    a(1:2) = a(1:2) / TS%dbarea
    a(3) = 1.d0 - a(1) - a(2)
    n = 0
    DO j=1,3
       IF(a(j)<=0) n = n+1
    ENDDO

    IF(n==2)THEN     
       IF(a(1)>0.999)THEN
          SS = Anis_PointSource_GetSpacing(P,TS%PS1)
       ELSE IF(a(2)>0.999)THEN
          SS = Anis_PointSource_GetSpacing(P,TS%PS2)
       ELSE IF(a(3)>0.999)THEN
          SS = Anis_PointSource_GetSpacing(P,TS%PS3)
       ELSE
          CALL Error_STOP ( 'wrong weight---1')
       ENDIF
    ELSE IF(n==1)THEN
       IF(a(1)<=0)THEN
          SS = Anis_LineSource_GetSpacing(P,TS%LS1)
       ELSE IF(a(2)<=0)THEN
          SS = Anis_LineSource_GetSpacing(P,TS%LS2)
       ELSE IF(a(3)<=0)THEN
          SS = Anis_LineSource_GetSpacing(P,TS%LS3)
       ELSE
          CALL Error_STOP ( 'wrong weight---2')
       ENDIF
    ELSE
       PS%RA1     = a(1)*TS%PS1%RA1     + a(2)*TS%PS2%RA1     + a(3)*TS%PS3%RA1
       PS%RA2     = a(1)*TS%PS1%RA2     + a(2)*TS%PS2%RA2     + a(3)*TS%PS3%RA2
       PS%Space   = a(1)*TS%PS1%Space   + a(2)*TS%PS2%Space   + a(3)*TS%PS3%Space
       PS%Stretch = a(1)*TS%PS1%Stretch + a(2)*TS%PS2%Stretch + a(3)*TS%PS3%Stretch
       IF(dd>=PS%RA2)THEN
          SS(:) = 1.d0
       ELSE IF(dd<=PS%RA1)THEN
          SS(:) = (/PS%Space, PS%Stretch/)
       ELSE
          PS%decaySp = -dlog(PS%Space) / (PS%RA2-PS%RA1)
          PS%decaySt = -dlog(PS%Stretch) / (PS%RA2-PS%RA1)
          SS(1) = PS%Space   * EXP(PS%decaySp * (dd-PS%RA1))
          SS(2) = PS%Stretch * EXP(PS%decaySt * (dd-PS%RA1))
       ENDIF
    ENDIF

  END FUNCTION Anis_TriSource_GetSpacing

  
  FUNCTION Anis_TriSource_GetGradient(P,TS) RESULT(S)
    IMPLICIT NONE
    TYPE(Anis_TriSource_Type), INTENT(IN) ::  TS   
    REAL*8, INTENT(IN) :: P(3)
    REAL*8 :: S
    TYPE(Anis_PointSource_Type) :: PS   
    REAL*8 :: t, a(3), P2D(2), dd
    INTEGER :: j, n

    dd = Plane3D_DistancePointToPlane(P,TS%aPlane)
    IF(dd>=MAX(TS%PS1%RA2, TS%PS2%RA2, TS%PS3%RA2))THEN
       S = 0.d0         
       RETURN
    ENDIF

    P2D(:) = Plane3D_project2D(TS%aPlane, P)
    a(1) = getDoubleArea2D(P2D, TS%P2D(:,2), TS%P2D(:,3))
    a(2) = getDoubleArea2D(TS%P2D(:,1), P2D, TS%P2D(:,3))
    a(1:2) = a(1:2) / TS%dbarea
    a(3) = 1.d0 - a(1) - a(2)
    n = 0
    DO j=1,3
       IF(a(j)<=0) n = n+1
    ENDDO

    IF(n==2)THEN     
       IF(a(1)>0.999)THEN
          S = Anis_PointSource_GetGradient(P,TS%PS1)
       ELSE IF(a(2)>0.999)THEN
          S = Anis_PointSource_GetGradient(P,TS%PS2)
       ELSE IF(a(3)>0.999)THEN
          S = Anis_PointSource_GetGradient(P,TS%PS3)
       ELSE
          CALL Error_STOP ( 'wrong weight---1')
       ENDIF
    ELSE IF(n==1)THEN
       IF(a(1)<=0)THEN
          S = Anis_LineSource_GetGradient(P,TS%LS1)
       ELSE IF(a(2)<=0)THEN
          S = Anis_LineSource_GetGradient(P,TS%LS2)
       ELSE IF(a(3)<=0)THEN
          S = Anis_LineSource_GetGradient(P,TS%LS3)
       ELSE
          CALL Error_STOP ( 'wrong weight---2')
       ENDIF
    ELSE
       PS%RA2     = a(1)*TS%PS1%RA2     + a(2)*TS%PS2%RA2     + a(3)*TS%PS3%RA2
       PS%Space   = a(1)*TS%PS1%Space   + a(2)*TS%PS2%Space   + a(3)*TS%PS3%Space
       PS%Stretch = a(1)*TS%PS1%Stretch + a(2)*TS%PS2%Stretch + a(3)*TS%PS3%Stretch
       IF(dd>=PS%RA2)THEN
          S = 0.d0
       ELSE
          S = MAX(ABS(LOG(PS%Space)), ABS(LOG(PS%Space * PS%Stretch)))
          S = S / PS%RA2
       ENDIF
    ENDIF


  END FUNCTION Anis_TriSource_GetGradient

  !>
  !!     Calculate minimum scale under a source group at a given position.
  !<
  FUNCTION SourceGroup_GetScale(P,Sources) RESULT(S)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P(3)
    TYPE (SourceGroup_Type), INTENT(IN) :: Sources
    REAL*8  :: S
    INTEGER :: i
    REAL*8  :: sp

    S = 1.d0
    DO i=1,Sources%NB_Pt
       sp = PointSource_GetScale(P,Sources%Pt(i))
       S  = MIN(S,sp)
    ENDDO
    DO i=1,Sources%NB_Ln
       sp = LineSource_GetScale(P,Sources%Ln(i))
       S  = MIN(S,sp)
    ENDDO
    DO i=1,Sources%NB_Tri
       sp = TriSource_GetScale(P,Sources%Tri(i))
       S  = MIN(S,sp)
    ENDDO
    DO i=1,Sources%NB_Ring
       sp = RingSource_GetScale(P,Sources%Ring(i))
       S  = MIN(S,sp)
    ENDDO
    DO i=1,Sources%NB_APt
       sp = Anis_PointSource_GetScale(P,Sources%APt(i))
       S  = MIN(S,sp)
    ENDDO
    DO i=1,Sources%NB_ALn
       sp = Anis_LineSource_GetScale(P,Sources%ALn(i))
       S  = MIN(S,sp)
    ENDDO
    DO i=1,Sources%NB_ATr
       sp = Anis_TriSource_GetScale(P,Sources%ATr(i))
       S  = MIN(S,sp)
    ENDDO

  END FUNCTION SourceGroup_GetScale

  !>
  !!     Calculate mapping under a source group at a given position.
  !!     @param[in]  P   the position.
  !!     @param[in]  Sources the source group.
  !!     @param[out] iso =1, the mapping is isotropic, and fMap(1,1) return the scale.  \n
  !!                     =0, the mapping is anisotropic.
  !<
  FUNCTION SourceGroup_GetMapping(P,Sources,iso) RESULT(fMap)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P(3)
    TYPE (SourceGroup_Type), INTENT(IN) :: Sources
    INTEGER, INTENT(OUT) :: iso
    REAL*8  :: fMap(3,3), fMap1(3,3)
    REAL*8  :: SS(2), S, sp
    INTEGER :: i, isoring

    iso = 1
    S   = 1.d30

    DO i=1,Sources%NB_APt
       SS = Anis_PointSource_GetSpacing(P,Sources%APt(i))
       sp = SS(1)*SS(2)
       IF(ABS(SS(2)-1)<1.e-2)THEN
          IF(iso==1)THEN
             S = MIN(S,sp)
          ELSE
             CALL Mapping3D_ScalarIntersect(fMap,sp)
          ENDIF
       ELSE
          fMap1(1,:) = Sources%APt(i)%DIRCT(1,:)/sp
          fMap1(2,:) = Sources%APt(i)%DIRCT(2,:)/SS(1)
          fMap1(3,:) = Sources%APt(i)%DIRCT(3,:)/SS(1)
          IF(iso==1)THEN
             fMap = fMap1
             CALL Mapping3D_ScalarIntersect(fMap,S)
          ELSE
             fMap = Mapping3D_Intersect(fMap, fMap1)
          ENDIF
          iso = 0
       ENDIF
    ENDDO

    DO i=1,Sources%NB_ALn
       SS = Anis_LineSource_GetSpacing(P,Sources%ALn(i))
       sp = SS(1)*SS(2)
       IF(ABS(SS(2)-1)<1.e-2)THEN
          IF(iso==1)THEN
             S = MIN(S,sp)
          ELSE
             CALL Mapping3D_ScalarIntersect(fMap,sp)
          ENDIF
       ELSE
          fMap1(1,:) = Sources%ALn(i)%DIRCT(1,:)/sp
          fMap1(2,:) = Sources%ALn(i)%DIRCT(2,:)/SS(1)
          fMap1(3,:) = Sources%ALn(i)%DIRCT(3,:)/SS(1)
          IF(iso==1)THEN
             fMap = fMap1
             CALL Mapping3D_ScalarIntersect(fMap,S)
          ELSE
             fMap = Mapping3D_Intersect(fMap, fMap1)
          ENDIF
          iso = 0
       ENDIF
    ENDDO

    DO i=1,Sources%NB_ATr
       SS = Anis_TriSource_GetSpacing(P,Sources%ATr(i))
       sp = SS(1)*SS(2)
       IF(ABS(SS(2)-1)<1.e-2)THEN
          IF(iso==1)THEN
             S = MIN(S,sp)
          ELSE
             CALL Mapping3D_ScalarIntersect(fMap,sp)
          ENDIF
       ELSE
          fMap1(1,:) = Sources%ATr(i)%DIRCT(1,:)/sp
          fMap1(2,:) = Sources%ATr(i)%DIRCT(2,:)/SS(1)
          fMap1(3,:) = Sources%ATr(i)%DIRCT(3,:)/SS(1)
          IF(iso==1)THEN
             fMap = fMap1
             CALL Mapping3D_ScalarIntersect(fMap,S)
          ELSE
             fMap = Mapping3D_Intersect(fMap, fMap1)
          ENDIF
          iso = 0
       ENDIF
    ENDDO

    DO i=1,Sources%NB_Ring
       fMap1 = RingSource_GetMapping(P,Sources%Ring(i), isoring)
       IF(isoring==1)THEN
          sp = 1.d0 / fMap1(1,1)
          IF(iso==1)THEN
             S = MIN(S,sp)
          ELSE
             CALL Mapping3D_ScalarIntersect(fMap,sp)
          ENDIF
       ELSE
          IF(iso==1)THEN
             fMap = fMap1
             CALL Mapping3D_ScalarIntersect(fMap,S)
          ELSE
             fMap = Mapping3D_Intersect(fMap, fMap1)
          ENDIF
          iso = 0
       ENDIF
    ENDDO

    IF(iso==0) S = 1.d0
    DO i=1,Sources%NB_Pt
       sp = PointSource_GetScale(P,Sources%Pt(i))
       S  = MIN(S,sp)
    ENDDO
    DO i=1,Sources%NB_Ln
       sp = LineSource_GetScale(P,Sources%Ln(i))
       S  = MIN(S,sp)
    ENDDO
    DO i=1,Sources%NB_Tri
       sp = TriSource_GetScale(P,Sources%Tri(i))
       S  = MIN(S,sp)
    ENDDO

    IF(iso==1)THEN
       fMap = 0
       S = 1.d0/S
       fMap(1,1) = S
       fMap(2,2) = S
       fMap(3,3) = S
    ELSE
       CALL Mapping3D_ScalarIntersect(fMap,S)
    ENDIF
  END FUNCTION SourceGroup_GetMapping

  !>
  !!     Calculate minimum spacing under a source group at a given position, SS(1),
  !!        and the gradient of spacing at this point
  !<
  FUNCTION SourceGroup_GetGradient(P,Sources) RESULT(SS)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P(3)
    TYPE (SourceGroup_Type), INTENT(IN) :: Sources
    REAL*8  :: SS(2), S
    INTEGER :: i, ik, jk
    REAL*8  :: sp

    SS(1) = 1.d0
    jk = 0
    DO i=1,Sources%NB_Pt
       sp = PointSource_GetScale(P,Sources%Pt(i))
       IF(sp<SS(1))THEN
          ik = i
          jk = 1
          SS(1)  = sp
       ENDIF
    ENDDO
    DO i=1,Sources%NB_Ln
       sp = LineSource_GetScale(P,Sources%Ln(i))
       IF(sp<SS(1))THEN
          ik = i
          jk = 2
          SS(1)  = sp
       ENDIF
    ENDDO
    DO i=1,Sources%NB_Tri
       sp = TriSource_GetScale(P,Sources%Tri(i))
       IF(sp<SS(1))THEN
          ik = i
          jk = 3
          SS(1)  = sp
       ENDIF
    ENDDO
    DO i=1,Sources%NB_Ring
       sp = RingSource_GetScale(P,Sources%Ring(i))
       IF(sp<SS(1))THEN
          ik = i
          jk = 4
          SS(1)  = sp
       ENDIF
    ENDDO
    DO i=1,Sources%NB_APt
       sp = Anis_PointSource_GetScale(P,Sources%APt(i))
       IF(sp<SS(1))THEN
          ik = i
          jk = 5
          SS(1)  = sp
       ENDIF
    ENDDO
    DO i=1,Sources%NB_ALn
       sp = Anis_LineSource_GetScale(P,Sources%ALn(i))
       IF(sp<SS(1))THEN
          ik = i
          jk = 6
          SS(1)  = sp
       ENDIF
    ENDDO
    DO i=1,Sources%NB_ATr
       sp = Anis_TriSource_GetScale(P,Sources%ATr(i))
       IF(sp<SS(1))THEN
          ik = i
          jk = 7
          SS(1)  = sp
       ENDIF
    ENDDO

    IF(jk==0)THEN
       SS(2) = 0.d0
    ELSE IF(jk==1)THEN
       SS(2) = PointSource_GetGradient(P,Sources%Pt(ik))
    ELSE IF(jk==2)THEN
       SS(2) = LineSource_GetGradient(P,Sources%Ln(ik))
    ELSE IF(jk==3)THEN
       SS(2) = TriSource_GetGradient(P,Sources%Tri(ik))
    ELSE IF(jk==4)THEN
       SS(2) = RingSource_GetGradient(P,Sources%Ring(ik))
    ELSE IF(jk==5)THEN
       SS(2) = Anis_PointSource_GetGradient(P,Sources%APt(ik))
    ELSE IF(jk==6)THEN
       SS(2) = Anis_LineSource_GetGradient(P,Sources%ALn(ik))
    ELSE IF(jk==7)THEN
       SS(2) = Anis_TriSource_GetGradient(P,Sources%ATr(ik))
    ELSE
       CALL Error_STOP ( '--- SourceGroup_GetGradient: wrong jk')
    ENDIF

  END FUNCTION SourceGroup_GetGradient

  !>
  !!    Generate a list of points located in the vacinity of 
  !!    the sources used for octree refinment.
  !<
  SUBROUTINE SourceGroup_SetCheckPoints(Sources, PPS, N)
    IMPLICIT NONE
    TYPE (SourceGroup_Type), INTENT(IN) :: Sources
    INTEGER, INTENT(OUT) :: N
    REAL*8, DIMENSION(:,:), POINTER :: PPS
    REAL*8  :: SS(2), S
    INTEGER :: i, j, k, m
    REAL*8  :: P1(3), P2(3), P3(3), R, RR, tj, tk, tl, p2D(2)

    m =    Sources%NB_Pt + Sources%NB_APt + 101* (Sources%NB_Ln + Sources%NB_ALn)     &
         + 301* Sources%NB_Ring + 101*101* (Sources%NB_Tri + Sources%NB_ATr)
    ALLOCATE(PPS(3,m))

    N = 0
    DO i=1,Sources%NB_Pt
       N  = N + 1
       PPS(:,N) = Sources%Pt(i)%Posit
    ENDDO
    DO i=1,Sources%NB_Ln
       P1 = Sources%Ln(i)%PS1%Posit
       P2 = Sources%Ln(i)%PS2%Posit - P1
       R  = MIN(Sources%Ln(i)%PS1%RA2, Sources%Ln(i)%PS2%RA2)
       RR = Geo3D_Distance(P2)
       m  = 10. * RR / R
       m  = MIN(100,m)
       m  = MAX(1,m)
       DO j = 0,m
          tj  = 1.d0*j / m
          N = N + 1
          PPS(:,N) = P1 + tj * P2
       ENDDO
    ENDDO
    DO i=1,Sources%NB_Tri
       P1 = Sources%Tri(i)%PS1%Posit
       P2 = Sources%Tri(i)%PS2%Posit
       P3 = Sources%Tri(i)%PS3%Posit
       R  = MIN(Sources%Tri(i)%PS1%RA2, Sources%Tri(i)%PS2%RA2, Sources%Tri(i)%PS3%RA2)
       RR = MAX(Geo3D_Distance(P1,P2), Geo3D_Distance(P1,P3), Geo3D_Distance(P2,P3))
       m  = 10. * RR / R
       m  = MIN(100,m)
       m  = MAX(1,m)
       DO j = 0,m
          tj  = 1.d0*j / m          
          DO k = 0, m-j
             tk = 1.d0*k / m
             tl = 1.d0 - tj - tk
             N = N + 1
             PPS(:,N) = tj * P1 + tk * P2 + tl * P3
          ENDDO
       ENDDO
    ENDDO
    DO i=1,Sources%NB_Ring
       R  = Sources%Ring(i)%RA2
       RR = 6.28*Sources%Ring(i)%RA0
       m  = 10. * RR / R
       m  = MIN(300,m)
       m  = MAX(1,m)
       DO j = 0,m
          tj = 2.d0*3.14159265d0 *j/m
          p2D = (/COS(tj), SIN(tj)/)
          p2D = Sources%Ring(i)%P0 + Sources%Ring(i)%RA0 * P2D
          N = N + 1
          PPS(:,N) = Plane3D_get3DPosition(Sources%Ring(i)%aPlane, P2D)
       ENDDO
    ENDDO
    DO i=1,Sources%NB_APt
       N  = N + 1
       PPS(:,N) = Sources%APt(i)%Posit
    ENDDO
    DO i=1,Sources%NB_ALn
       P1 = Sources%ALn(i)%PS1%Posit
       P2 = Sources%ALn(i)%PS2%Posit - P1
       R  = MIN(Sources%ALn(i)%PS1%RA2, Sources%ALn(i)%PS2%RA2)
       RR = Geo3D_Distance(P2)
       m  = 10. * RR / R
       m  = MIN(100,m)
       m  = MAX(1,m)
       DO j = 0,m
          tj  = 1.d0*j / m
          N = N + 1
          PPS(:,N) = P1 + tj * P2
       ENDDO
    ENDDO
    DO i=1,Sources%NB_ATr
       P1 = Sources%ATr(i)%PS1%Posit
       P2 = Sources%ATr(i)%PS2%Posit
       P3 = Sources%ATr(i)%PS3%Posit
       R  = MIN(Sources%ATr(i)%PS1%RA2, Sources%ATr(i)%PS2%RA2, Sources%ATr(i)%PS3%RA2)
       RR = MAX(Geo3D_Distance(P1,P2), Geo3D_Distance(P1,P3), Geo3D_Distance(P2,P3))
       m  = 10. * RR / R
       m  = MIN(100,m)
       m  = MAX(1,m)
       DO j = 0,m
          tj  = 1.d0*j / m          
          DO k = 0, m-j
             tk = 1.d0*k / m
             tl = 1.d0 - tj - tk
             N = N + 1
             PPS(:,N) = tj * P1 + tk * P2 + tl * P3
          ENDDO
       ENDDO
    ENDDO

  END SUBROUTINE SourceGroup_SetCheckPoints

  !>
  !!   (Re)allocate a pointer to a BoxSource_Type array.
  !<
  SUBROUTINE allc_BoxSource(BoxSources, n, message)
    IMPLICIT NONE
    TYPE (BoxSource_Type), DIMENSION(:), POINTER :: BoxSources
    TYPE (BoxSource_Type), DIMENSION(:), POINTER :: b
    INTEGER, INTENT(IN) :: n
    CHARACTER*(*), INTENT(IN), OPTIONAL :: message
    INTEGER :: ns, i
    INTEGER :: ierr1=0,ierr2=0,ierr3=0,ierr4=0
    LOGICAL :: NotNewPointer

    NotNewPointer = .FALSE.
    IF(ASSOCIATED(BoxSources))THEN
       ns = SIZE(BoxSources)
       IF(ns>=n) RETURN
       NotNewPointer = .TRUE.
       ALLOCATE(b(ns),stat=ierr1)
       DO i = 1,ns
          b(i) = BoxSources(i)
       ENDDO
       DEALLOCATE(BoxSources,stat=ierr2)
    ENDIF

    ALLOCATE(BoxSources(n), stat=ierr3)

    IF(NotNewPointer)THEN
       DO i = 1,ns
          BoxSources(i) = b(i)
       ENDDO
       DEALLOCATE(b,stat=ierr4)
    ENDIF

    IF(ierr1/=0.OR.ierr2/=0.OR.ierr3/=0.OR.ierr4/=0)THEN
       WRITE(*,*) ' ALLOCATION ERROR: ierr=',ierr1,ierr2,ierr3,ierr4
       WRITE(*,*) '   NotNewPointer=',NotNewPointer, ' n=', n
       IF(PRESENT(message)) WRITE(*,*) '                   '//message
       CALL Error_STOP (' ')
    ENDIF

    RETURN
  END SUBROUTINE allc_BoxSource


END MODULE Source_Type
