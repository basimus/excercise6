!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!




!*---------------------------------------------------------------------*
!>                                                                     
!!   Connect boundary loops to biuld a simply-connected area.
!!   @param[out] Isucc = 1   successful.            \n
!!                     = 0   bug.                   \n
!!                     =-1   outer loop broken.     \n
!!                     =-2   inter loop broken.     \n
!!                     =-4   no divider.            \n
!!                     =-5   too many dividers.  
!<                                                                     
!*---------------------------------------------------------------------*
SUBROUTINE Connect_boundLoop(Surf, Cloop, Isucc )
  USE SurfaceMeshStorage
  USE Geometry3DAll
  USE CornerLoop
  IMPLICIT NONE
  TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf  
  TYPE(CornerLoopType), INTENT(inout)  ::  Cloop
  INTEGER, INTENT(inout)  ::   Isucc
  INTEGER, DIMENSION(:), POINTER :: mark
  INTEGER :: is, iss, isin, isout, is1, is2, ip1, ip2
  INTEGER :: jmEnd1(1000), jmEnd2(1000), k, numLink
  INTEGER :: jm1, jm2, jm1p, jm1n, jm2p, jm2n
  REAL*8  :: d, dmn

  Isucc = 0

  ALLOCATE(mark(2*Cloop%numCorners))
  mark(:) = 0

  !--- mark outer loop

  iss = 1
  is  = iss
  k   = 0
  DO 
     k        = k+1
     mark(is) = 1
     is  = Cloop%next(is)
     IF(is==iss) EXIT
     IF(k>Cloop%numCorners)THEN
        Isucc = -1
        RETURN
     ENDIF
  ENDDO

  IF(k==Cloop%numCorners)THEN
     !--- no inner loop
     Isucc = 1
     RETURN
  ENDIF


  numLink = 0
  Loop_Big : DO WHILE(numLink<100)

     !--- search the shortest distance between the inner loops and the outer loop

     dmn = 1.d36
     isout = 0
     DO is1 = 1, Cloop%numCorners
        IF(mark(is1)/=1) CYCLE
        ip1 = Cloop%nodeID(is1)
        DO is2 = 1, Cloop%numCorners
           IF(mark(is2)/=0) CYCLE
           ip2 = Cloop%nodeID(is2)
           d   = Geo3D_Distance_SQ(Surf%Posit(:,ip1), Surf%Posit(:,ip2))
           IF(d<dmn)THEN
              dmn   = d
              isout = is1
              isin  = is2
           ENDIF
        ENDDO
     ENDDO

     IF(isout==0) EXIT Loop_Big

     numLink         = numLink +1
     jmEnd1(numLink) = isout
     jmEnd2(numLink) = isin
     
     !--- search a inner loop and mark it as out loop
    
     iss = isin
     is  = iss
     k   = 0
     DO 
        k        = k+1
        mark(is) = 1
        is  = Cloop%next(is)
        IF(is==iss) EXIT
        IF(k>Cloop%numCorners)THEN
           Isucc = -2
           RETURN
        ENDIF
     ENDDO

     Mark(isout)     = -1
     Mark(isin)      = -1     

  ENDDO Loop_Big

  IF(numLink==0)THEN
     Isucc = -4
     RETURN
  ENDIF
  IF(numLink>=100)THEN
     Isucc = -5
     RETURN
  ENDIF

  !--- split the dividers according the computational length

  DO k = 1, numLink
     jm1 = jmEnd1(k)
     jm2 = jmEnd2(k)
     call CornerLoop_Bridge(Cloop, jm1, jm2 )
  ENDDO

  !--- a check
  CALL CornerLoop_Check(Cloop)
  
  DEALLOCATE(mark)
  Isucc = 1

  RETURN
END SUBROUTINE Connect_boundLoop






!>                                                                              
!!   Triangulate a rough surface area defined by a loop of connected points                   
!!               by picking smallest angle viewed from a specified direction.
!!   @param[in]  Np :  the number of nodes on the loop.                                
!!   @param[in]  idx(Np) : the ID of nodes on the loop in order.                       
!!                   i.e. idx(i)--idx(i+1), i=1,2,..,Np-1 & idx(Np)--idx(1)     
!!                        make the boundary edges of the meshing plane.          
!!   @param[in]  Posit   : The coordinate of each ID.                               
!!   @param[in]  normal  : the direction of view.                                      
!!                  Make sure it is the right handed direction of the loop.      
!!   @param[in]  Ntri,IP_Tri : the triangle aready existed.                         
!!   @param[out] Ntri   : the number of triangle (including those exsited before).        
!!                 if complete, Ntri will increase by Np-2.
!!   @param[out] IP_Tri : The ID of nodes of each triangle.                        
!!                                                                              
!<
SUBROUTINE PlaneTriangulator(Np, idx, Posit, normal, Ntri, IP_Tri)
  USE Plane3DGeom
  IMPLICIT NONE
  INTEGER, INTENT(IN)    :: Np
  INTEGER, INTENT(IN)    :: idx(Np)
  REAL*8,  INTENT(IN)    :: Posit(3,*), normal(3)
  INTEGER, INTENT(INOUT) :: Ntri, IP_Tri(3,*)
  REAL*8  :: xl
  INTEGER :: i
  REAL*8,  DIMENSION(:,:),  POINTER :: Pt
  INTEGER, DIMENSION(:,:),  POINTER :: IPs
  TYPE(Plane3D) :: aPlane

  IF(idx(Np)==idx(1))THEN
     WRITE(*,*)'Error--- the loop ends repeated'
     CALL Error_STOP ( '--- PlaneTriangulator')
  ENDIF

  IF(Np==3)THEN
     Ntri = Ntri+1
     Ip_Tri(:,Ntri) = idx(1:3)
     RETURN
  ENDIF

  ALLOCATE(Pt(2,Np), IPs(3,Np-2))

  !--- find two unit direction perpendicalar to each other and to a given normal
  xl = dsqrt(normal(1)*normal(1) + normal(2)*normal(2) + normal(3)*normal(3))
  aPlane%anor(:) = normal(:) / xl
  CALL Plane3D_buildTangentAxes(aPlane)

  !--- project the points on the plane and get 2D position
  DO i=1,Np
     Pt(:,i) = Plane3D_project2D(aPlane, Posit(:,idx(i)))
  ENDDO

  CALL Plane2DTriangulator(Np, Pt, IPs)

  DO i=1,Np-2
     Ntri = Ntri+1
     IP_Tri(:,NTri) = idx(IPs(:,i))
  ENDDO

  DEALLOCATE(Pt, IPs)
END SUBROUTINE PlaneTriangulator

!>                                                                              
!!   Triangulate 2D plane area surrounded by a loop of connected points                   
!!   @param[in]  Np  :  the number of nodes on the loop.                                
!!   @param[in]  Pt  : The coordinate of each ID.                               
!!   @param[out] IPs : The ID of nodes of each triangle.                        
!!                                                                           
!!   Reminder: This roution is for a single loop only.
!!   Please refer SurfaceMeshManager2D::Surf2D_BoundaryConnect() for complicate cases.
!<
SUBROUTINE Plane2DTriangulator(Np, Pt, IPs)
  IMPLICIT NONE
  INTEGER, INTENT(IN)    :: Np
  REAL*8,  INTENT(IN)    :: Pt(2,*)
  INTEGER, INTENT(OUT)   :: IPs(3,*)
  REAL*8  :: anm
  INTEGER :: NpLeft, i, is, ip, j, js, jp, jj, jk, kk, Ntri
  INTEGER :: Ips1(3), Ips2(3)
  REAL*8,  DIMENSION(:  ),  POINTER :: angle
  REAL*8,  DIMENSION(:,:),  POINTER :: P2D
  INTEGER, DIMENSION(:  ),  POINTER :: idx
  REAL*8 :: minAngleSine

  IF(NP==3)THEN
     IPs(:,1) = (/1,2,3/)
     RETURN
  ELSE IF(NP==4)THEN
     CALL Triangulator_SplitQuad(Pt, IPs(:,1), IPs(:,2))
     RETURN
  ENDIF

  !--- calculate the angles for all points
  ALLOCATE(angle(Np), P2D(2,Np), idx(Np))
  DO i=1,NP
     angle(i) = minAngleSine(i,NP,Pt)
     P2D(:,i) = Pt(:,i)
     idx(  i) = i
  ENDDO

  NpLeft = Np
  Ntri   = 0

  Loop_NpLeft : DO

     anm = 0
     jk  = 0
     DO jj = 1, NpLeft
        IF(angle(jj)>anm)THEN
           jk = jj
           anm = angle(jj)
        ENDIF
     ENDDO
     IF(jk==0)THEN
        WRITE(*,*)'#Error--- all angles left are negative. Check the orientation please.'
        WRITE(*,*)'#Np, NpLeft, Ntri=',Np, NpLeft, Ntri
        CALL Error_STOP ( '--- Plane2DTriangulator')
     ENDIF

     Ntri = Ntri+1
     js = jk-1
     jp = jk+1
     IF(js==0)     js = NpLeft
     IF(jp>NpLeft) jp = 1
     IPs(1:3,Ntri) = (/ idx(jk), idx(jp), idx(js) /)

     NpLeft = NpLeft-1
     DO jj=jk, NpLeft
        idx(jj)   = idx(jj+1)
        P2D(:,jj) = p2D(:,jj+1)
        angle(jj) = angle(jj+1)
     ENDDO

     IF(NpLeft==4)THEN
        CALL Triangulator_SplitQuad(p2D, IPs1, IPs2)
        Ntri = Ntri+1
        IPs(1:3,Ntri) = idx(Ips1(:))
        Ntri = Ntri+1
        IPs(1:3,Ntri) = idx(Ips2(:))
        EXIT Loop_NpLeft
     ENDIF

     IF(jp/=1) jp = jp-1
     IF(js>NpLeft) js = NpLeft

     !--- update the angles for two neighbour points, js, jp
     angle(js) = minAngleSine(js,NpLeft,P2D)
     angle(jp) = minAngleSine(jp,NpLeft,P2D)

  ENDDO Loop_NpLeft

  DEALLOCATE(angle, P2D, idx)

  RETURN
END SUBROUTINE Plane2DTriangulator


!--- angle= 1->(1/2)*PI   2->PI  
FUNCTION Triangulator_AngleSine(P1, P0, P2) RESULT(angle)
  IMPLICIT NONE
  REAL*8, INTENT(in) :: P1(2), P0(2), P2(2)
  REAL*8 :: angle
  REAL*8 :: a(2), b(2), xl, dot
  a(:) = P1-P0
  b(:) = P2-P0
  angle  = a(1)*b(2)-a(2)*b(1)
  xl = dsqrt( (a(1)*a(1)+a(2)*a(2)) * (b(1)*b(1)+b(2)*b(2)) )
  angle = angle / xl
  dot = a(1)*b(1) + a(2)*b(2)
  IF(dot<0)THEN
    IF(angle>0)THEN
       angle =  2.d0 - angle
    ELSE
       angle = -2.d0 - angle
    ENDIF
  ENDIF
END FUNCTION Triangulator_AngleSine

!--- angle= 1->(1/2)*PI   2->PI  
FUNCTION Triangulator_AngleSine3D(P1, P0, P2, aPlane) RESULT(angle)
  USE Plane3DGeom
  IMPLICIT NONE
  REAL*8, INTENT(in) :: P1(3), P0(3), P2(3)
  REAL*8 :: angle
  REAL*8 :: a(2), b(2), xl, dot, dp(3)
  TYPE(Plane3D) :: aPlane  
  dp   = P1-P0
  a(:) = Plane3D_project2D (aPlane, dp)
  dp   = P2-P0
  b(:) = Plane3D_project2D (aPlane, dp)  
  angle  = a(1)*b(2)-a(2)*b(1)
  xl = dsqrt( (a(1)*a(1)+a(2)*a(2)) * (b(1)*b(1)+b(2)*b(2)) )
  angle = angle / xl
  dot = a(1)*b(1) + a(2)*b(2)
  IF(dot<0)THEN
    IF(angle>0)THEN
       angle =  2.d0 - angle
    ELSE
       angle = -2.d0 - angle
    ENDIF
  ENDIF
END FUNCTION Triangulator_AngleSine3D

FUNCTION minAngleSine(i,NpLeft,P2D) RESULT(angle)
  IMPLICIT NONE
  REAL*8, INTENT(in) :: P2D(2,*)
  INTEGER, INTENT(in) :: i, NpLeft
  INTEGER :: is, ip, ii, j
  REAL*8 :: angle
  REAL*8 :: anm
  REAL*8 :: Triangulator_AngleSine

  is = i-1
  ip = i+1
  IF(is==0) is = NpLeft
  IF(ip>NpLeft) ip = 1

  angle = 2.
  anm = Triangulator_AngleSine(P2D(:,i),P2D(:,is),P2D(:,ip))
  angle = MIN(anm,angle)
  IF(angle<0) RETURN
  anm = Triangulator_AngleSine(P2D(:,is),P2D(:,ip),P2D(:,i))
  angle = MIN(anm,angle)
  IF(angle<0) RETURN

  DO j = 0,NpLeft-4
     ii = MOD(ip+j,NpLeft)+1
     IF(Triangulator_AngleSine(P2D(:,i ),P2D(:,ip),P2D(:,ii))>0) CYCLE
     IF(Triangulator_AngleSine(P2D(:,ii),P2D(:,is),P2D(:,i ))>0) CYCLE
     anm = Triangulator_AngleSine(P2D(:,ii),P2D(:,ip),P2D(:,is))
     angle = MIN(anm,angle)
     IF(angle<0) RETURN
     anm = Triangulator_AngleSine(P2D(:,ip),P2D(:,is),P2D(:,ii))
     angle = MIN(anm,angle)
     IF(angle<0) RETURN
  ENDDO

END FUNCTION minAngleSine

SUBROUTINE Triangulator_SplitQuad(p2D, IPs1, IPs2)
  IMPLICIT NONE
  REAL*8, INTENT(in)  :: p2D(2,*)
  INTEGER, INTENT(out) :: IPs1(3), IPs2(3)
  INTEGER :: ksplit
  REAL*8  :: anm
  REAL*8 :: Triangulator_AngleSine

  anm =          Triangulator_AngleSine(P2D(:,4),P2D(:,2),P2D(:,1))
  anm = MIN(anm, Triangulator_AngleSine(P2D(:,3),P2D(:,2),P2D(:,4)))
  anm = MIN(anm, Triangulator_AngleSine(P2D(:,1),P2D(:,4),P2D(:,2)))
  anm = MIN(anm, Triangulator_AngleSine(P2D(:,2),P2D(:,4),P2D(:,3)))
  ksplit = 2

  DO 
     IF(Triangulator_AngleSine(P2D(:,2),P2D(:,1),P2D(:,3))<anm) EXIT
     IF(Triangulator_AngleSine(P2D(:,3),P2D(:,1),P2D(:,4))<anm) EXIT
     IF(Triangulator_AngleSine(P2D(:,4),P2D(:,3),P2D(:,1))<anm) EXIT
     IF(Triangulator_AngleSine(P2D(:,1),P2D(:,3),P2D(:,2))<anm) EXIT
     ksplit = 1
     EXIT
  ENDDO

  IF(ksplit==1)THEN
     IPs1(1:3) = (/ 1, 2, 3 /)
     IPs2(1:3) = (/ 1, 3, 4 /)
  ELSE
     IPs1(1:3) = (/ 1, 2, 4 /)
     IPs2(1:3) = (/ 2, 3, 4 /)
  ENDIF
  RETURN
END SUBROUTINE Triangulator_SplitQuad


