!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Functions of transfer between characters and numbers.
!<
MODULE Number_CHAR_Transfer

  IMPLICIT NONE

CONTAINS

  !>                                                                            
  !!   CONVERT A INTEGER INTO A CHARACTER STRING.
  !!   @param[in] II  : the integer to be transfered.
  !!   @param[in] NN  : the length of output string.            \n
  !!                    IF NN IS GREATER THAN THE DIGITAL NUMBER          
  !!                    OF II, THEN A PROPER NUMBER OF '0' WILL 
  !!                    BE PUT IN FRONT OF THE OUTPUT STRING.   \n                                                  
  !!                    IF NN IS LESS THAN THE DIGITAL NUMBER OF II, 
  !!                    THEN ONLY THE REAR PART OF THE NUMBER WILL
  !!                    BE REMAINED.                            \n
  !!                    IF NN=0, THE LENTH OF THE CHRACTER STRING EQUAL 
  !!                    TO THE DIGITAL NUMBER OF II.  
  !!   @return  the string.
  !!                                                                
  !!   FOR EXAMPLE,                               \n
  !!        Int_to_CHAR(23,0)='23'                \n
  !!        Int_to_CHAR(23,1)='3'                 \n              
  !!        Int_to_CHAR(23,2)='23',               \n
  !!        Int_to_CHAR(23,3)='023'               \n
  !!        Int_to_CHAR(-23,0)='-23'              \n
  !!        Int_to_CHAR(-23,3)='-23'            
  !<                                                                            
  FUNCTION INT_to_CHAR(II,NN) RESULT(C)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: II, NN
    CHARACTER (LEN=16)   :: C
    INTEGER :: I, N, M, MM

    I=ABS(II)
    N=NN

    IF(N==0)THEN
       M=10
       N=1
       DO WHILE(M<=I)
          M=M*10
          N=N+1
       ENDDO
    ENDIF

    IF(II<0)  N = N+1

    DO M=N,1,-1
       MM=MOD(I,10)
       IF(MM==0) C(M:M)='0'
       IF(MM==1) C(M:M)='1'
       IF(MM==2) C(M:M)='2'
       IF(MM==3) C(M:M)='3'
       IF(MM==4) C(M:M)='4'
       IF(MM==5) C(M:M)='5'
       IF(MM==6) C(M:M)='6'
       IF(MM==7) C(M:M)='7'
       IF(MM==8) C(M:M)='8'
       IF(MM==9) C(M:M)='9'
       I=I/10
    ENDDO

    IF(II<0) C(1:1) = '-'

    DO M = N+1, LEN(C)
       C(M:M)=' '
    ENDDO

    RETURN
  END FUNCTION INT_to_CHAR

  !>                                                                            
  !!  CONVERT A POSITIVE INTEGER ARRAY INTO A CHARACTER STRING.
  !!   @param[in]  IA  : the array of integers.
  !!   @param[in]  NN  : the number of the integers.
  !!   @param[out] L   : the length of output string.
  !!   @return  the string.
  !!
  !!    FOR EXAMPLE, IF                                                         
  !!       IA=(/ 2, 5, 6, 7, 8, 20, 19, 18, 6 /)                                
  !!    THE FUNCTION WILL RETURN C="2, 5:8, 20:18, 6" with Length L=16.          
  !<                                                                            
  FUNCTION IntArray_to_CHAR(IA, NN, L) RESULT(C)
    IMPLICIT NONE
    INTEGER, INTENT(IN)  :: NN, IA(NN)
    INTEGER, INTENT(OUT) :: L
    INTEGER :: M, I, length, lenT, forrange
    CHARACTER (LEN=512) :: C
    CHARACTER (LEN=16)  :: aa

    lenT = LEN(C)

    L=0
    DO M=1,NN
       forrange = 0
       IF( M>1 .AND. M<NN)THEN
          IF( ABS(IA(M)-IA(M-1))==1 .AND. 2*IA(M)==IA(M-1)+IA(M+1) ) forrange = 1
       ENDIF
       IF(forrange == 1)THEN
          IF(C(L-1:L)==', ')THEN
             L = L-1
             C(L:L) = ':' 
          ENDIF
       ELSE
          aa = Int_to_CHAR(IA(M),0)

          length = LEN(TRIM(aa))
          L = L+length
          C(L-length+1 : L)=aa(1:length)

          IF(M.LT.NN)THEN
             L=L+2
             C(L-1:L)=', '
          ENDIF
       ENDIF

       IF(L+10>lenT)THEN
          C(lenT-4:lenT) = ' etc.'
          DO i=L+1,LenT-5
             C(i:i)=' '
          ENDDO
          L = lenT
          EXIT
       ENDIF
    ENDDO

    DO i=L+1,LenT
       C(i:i)=' '
    ENDDO

    RETURN
  END FUNCTION IntArray_to_CHAR

  !>                                                                           
  !!   TSHIFT THE 'NN'th CHARACTER IN A STRING, 'C',              
  !!   TO ITS NEXT CHARACTER IN ALPHABET ORDER OR NUMERIC ORDER.                
  !!
  !!   FOR EXAMPLE, CHAR_PP('A39',3)='A40',CHAR_PP('ABCDE',3)='ABDDE'.         
  !<                                                                            
  FUNCTION CHAR_PP(C,NN) RESULT(C1)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: NN
    CHARACTER(LEN=*), INTENT(IN) :: C
    CHARACTER(LEN=LEN(C)) :: C1
    INTEGER :: N

    N  = NN
    C1 = C

    DO WHILE (N>0)

       IF((C1(N:N)>='0'.AND.C1(N:N)<='8').OR.   &
            (C1(N:N)>='A'.AND.C1(N:N)<='Y').OR.   &
            (C1(N:N)>='a'.AND.C1(N:N)<='y')    )THEN

          C1(N:N)=CHAR(ICHAR(C1(N:N))+1)
          N=0

       ELSE

          IF (C1(N:N)=='9')C1(N:N)='0'
          IF (C1(N:N)=='Z')C1(N:N)='A'
          IF (C1(N:N)=='z')C1(N:N)='a'
          N=N-1

       ENDIF

    ENDDO

    RETURN
  END FUNCTION CHAR_PP

  !>                                                                            
  !!    READ A STRING FROM A CHANNEL AND CONVERT IT TO AN INTEGER ARRAY.
  !!   @param[in]  IO  : the IO channel. 
  !!                     IF =6 ot not present, read from the screen.
  !!   @param[out] NI  : the array of integers. 
  !!                     Make sure it is long enough to contain all the potential numbers.
  !!   @param[out] Ne  : the number of the integers.
  !!
  !!    FOR EXAMPLE, READ C='3 241:245 5,9 12, -21 :-18' ,
  !!    THEN OUPUT NI(1:13)=(/3,241,242,243,244,245,5,9,12,-21,-20,-19,-18/)    
  !!        with Ne=13      
  !<                                                                            
  SUBROUTINE Read_INT_Array(NI,Ne, IO)
    IMPLICIT NONE
    CHARACTER(LEN=512) :: C 
    INTEGER, INTENT(IN), OPTIONAL :: IO
    INTEGER, DIMENSION(:), INTENT(OUT) :: NI
    INTEGER, INTENT(OUT) :: Ne
    INTEGER :: L,N,I,M, ISS,j,NEW,isigned 
    CHARACTER(LEN=1) :: C1

    IF(PRESENT(IO))THEN
       READ(IO,'(a)')C
    ELSE
       WRITE(*,'(a)')'....Input a series of integers as a format like 0:5,8,-20:-17'
       READ(6,'(a)')C
    ENDIF

    L=LEN_TRIM(C)+1
    N=0
    M=1
    ISS=0
    NEW=0
    isigned=1 

    DO I=1,L

       IF(I<L)THEN
          C1 = C(I:I)
       ELSE
          C1 = ' '
       ENDIF

       IF((C1>='0' .AND. C1<='9') )THEN

          N = N*10 + isigned*(ICHAR(C1)-ICHAR('0')) 
          NEW=1 

       ELSE

          IF(NEW==1 .AND. ISS==1)THEN
             IF(N-NI(M-1)>0)THEN
                DO j = M, M+N-NI(M-1)-1
                   IF(j>SIZE(NI)) EXIT
                   NI(j)=NI(j-1)+1 
                ENDDO
             ELSE
                DO j = M, M+ABS(N-NI(M-1))-1
                   IF(j>SIZE(NI)) EXIT
                   NI(j)=NI(j-1)-1 
                ENDDO
             ENDIF
             M=j 
             ISS=0 
             isigned=1 
          ELSE IF(NEW==1)THEN
             NI(M)=N 
             M = M+1 
             ISS=0 
             isigned=1 
          ENDIF
          NEW=0 
          N=0 
          IF(C1=='-')isigned=-1 
          IF(C1==':')ISS=1 

          IF(M>SIZE(NI)) EXIT
       ENDIF

    ENDDO

    Ne = M-1

    RETURN
  END SUBROUTINE Read_INT_Array

  !>                                                                            
  !!    Write A STRING of AN INTEGER ARRAY to A CHANNEL.
  !!   @param[in]  IO  : the IO channel. 
  !!                     IF =6 ot not present, write to the screen.
  !!   @param[in]  NI  : the array of integers.
  !!   @param[in]  Ne  : the number of the integers.
  !!
  !!    FOR EXAMPLE, IF NI(1:13)=(/3,241,242,243,244,245,5,9,12,-21,-20,-19,-18/)    
  !!        with Ne=13      
  !!    THEN output a string : '3 241:245 5,9 12, -21 :-18' 
  !<                                                                            
  SUBROUTINE Write_INT_Array(NI,Ne, IO)
    IMPLICIT NONE
    CHARACTER(LEN=512) :: C 
    INTEGER, INTENT(IN), OPTIONAL :: IO
    INTEGER, INTENT(IN) :: NI(*), Ne
    INTEGER :: L 

    C = IntArray_to_CHAR(NI, Ne, L)
    IF(PRESENT(IO))THEN
       WRITE(IO,'(a)') C(1:L)
    ELSE
       WRITE(* ,'(a)') C(1:L)
    ENDIF

    RETURN
  END SUBROUTINE Write_INT_Array

  !>                                                                            
  !!    TRANSFER A STRING to AN INTEGER.
  !!
  !!    IF SUCCEES, return Isucc to 1, otherwise, 0.                            
  !<                                                                            
  FUNCTION CHAR_to_INT(C,Isucc) RESULT(N)
    IMPLICIT NONE
    CHARACTER(*), INTENT(IN) :: C 
    INTEGER, INTENT(OUT) :: Isucc
    INTEGER :: N
    INTEGER :: I, istart,isigned

    Isucc = 0
    N = 0
    istart = 0
    isigned = 1

    DO I=1,LEN(C)
       IF(istart==0)THEN
          IF(   C(i:i)=='-' .OR.  C(i:i)=='+' .OR.   &
               (C(i:i)>='0' .AND. C(i:i)<='9'))THEN
             istart = 1
             IF(C(i:i)=='-') isigned = -1
             IF(C(i:i)=='-' .OR. C(i:i)=='+') CYCLE
          ELSE IF(C(i:i)==' ')THEN
             CYCLE
          ELSE
             RETURN
          ENDIF
       ENDIF

       IF((C(i:i)>='0' .AND. C(i:i)<='9') )THEN
          N = N*10 + (ICHAR(C(i:i))-ICHAR('0')) 
       ELSE IF(C(i:i)==' ')THEN
          EXIT
       ELSE
          RETURN
       ENDIF
    ENDDO

    N = N*isigned
    Isucc = 1

    RETURN
  END FUNCTION CHAR_to_INT

  !>                                                                         
  !!    TRANSFER A STRING to A real*8 number.
  !!                      
  !!    IF SUCCEES, return Isucc to 1, otherwise, 0.                            
  !<                                                                            
  FUNCTION CHAR_to_REAL8(C,Isucc) RESULT(V)
    IMPLICIT NONE
    CHARACTER(*), INTENT(IN) :: C 
    REAL*8  :: V
    INTEGER, INTENT(OUT) :: Isucc
    INTEGER :: I, istart,isigned,idc

    Isucc = 0
    V = 0.D0
    istart = 0
    isigned = 1
    idc = 1

    DO I=1,LEN(C)
       IF(istart==0)THEN
          IF(   C(i:i)=='-' .OR.  C(i:i)=='+' .OR.   &
               (C(i:i)>='0' .AND. C(i:i)<='9'))THEN
             istart = 1
             IF(C(i:i)=='-') isigned = -1
             IF(C(i:i)=='-' .OR. C(i:i)=='+') CYCLE
          ELSE IF(C(i:i)=='.')THEN
             istart = 2
             CYCLE
          ELSE IF(C(i:i)==' ')THEN
             CYCLE
          ELSE
             RETURN
          ENDIF
       ENDIF

       IF((C(i:i)>='0' .AND. C(i:i)<='9') )THEN
          IF(istart==1)THEN
             V = V*10.d0 + (ICHAR(C(i:i))-ICHAR('0')) 
          ELSE
             idc = idc*10
             V = V + 1.d0*(ICHAR(C(i:i))-ICHAR('0'))/idc
          ENDIF
       ELSE IF(C(i:i)=='.')THEN
          IF(istart==1)THEN
             istart = 2
             CYCLE
          ELSE
             RETURN
          ENDIF
       ELSE IF(C(i:i)=='e' .OR. C(i:i)=='E' .OR.   &
            C(i:i)=='d' .OR. C(i:i)=='D' )THEN
          idc = CHAR_to_INT(C(i+1:LEN(C)),Isucc)
          IF(Isucc==0)RETURN
          V = V*(10.d0**idc)
          EXIT
       ELSE IF(C(i:i)==' ')THEN
          EXIT
       ELSE
          RETURN
       ENDIF
    ENDDO

    V = V*isigned
    Isucc = 1
    RETURN

  END FUNCTION CHAR_to_REAL8

  !>                                                                            
  !!    TRANSFER a string to an array of real*8 numbers.
  !!
  !!    FOR EXAMPLE, INPUT C='0.5 -3.d2 738, .33'                               
  !!    THEN OUPUT V(1:N)=(/0.5d0, -0.3d3, 0.738d3, 0.33d0/)                    
  !!        N: the length of V                                                  
  !<                                                                            
  SUBROUTINE CHAR_to_REAL8Array(C,V,N)
    IMPLICIT NONE
    CHARACTER(*), INTENT(IN) :: C 
    REAL*8, INTENT(OUT)  :: V(*)
    INTEGER, INTENT(OUT) :: N
    CHARACTER(LEN=512) :: C1
    INTEGER :: I, istart,iend, Isucc

    istart = 0
    iend   = 0 
    N      = 0

    DO I=1,LEN(C)
       IF(istart==0)THEN
          IF(   C(i:i)=='-' .OR.  C(i:i)=='+' .OR. C(i:i)=='.'  .OR. &
               (C(i:i)>='0' .AND. C(i:i)<='9'))THEN
             istart = i
          ENDIF
       ENDIF
       IF(istart>0)THEN
          IF(   C(i:i)==' ' .OR.  C(i:i)==',' .OR. C(i:i)==';' .OR. &
               I==LEN(C) )THEN
             iend = i
             IF(C(i:i)==',' .OR. C(i:i)==';') iend = i-1
          ELSE
             CYCLE
          ENDIF
       ENDIF

       IF(istart>0 .AND. iend>=istart)THEN
          C1(1 : iend-istart+1) = C(istart : iend)
          C1(iend-istart+2 : iend-istart+2) = ' '
          N    = N+1
          V(N) = CHAR_to_REAL8(C1,Isucc)
          IF(Isucc==0) N = N-1
          istart = 0
          iend   = 0
       ENDIF
    ENDDO

    RETURN
  END SUBROUTINE CHAR_to_REAL8Array

  !>                                                                            
  !!    READ A STRING FROM A CHANNEL AND CONVERT IT TO A Real*8 ARRAY.
  !!   @param[in]  IO  : the IO channel. 
  !!                     IF =6 ot not present, read from the screen.
  !!   @param[out] V   : the array of real*8 numbers.
  !!   @param[out] N   : the number of the numbers.
  !!
  !!    FOR EXAMPLE, INPUT C='0.5 -3.d2 738, .33' ,
  !!    THEN OUPUT V(1:4)=(/0.5d0, -0.3d3, 0.738d3, 0.33d0/) with N=4                
  !<                                                                            
  SUBROUTINE Read_REAL8_Array(V,N,IO)
    IMPLICIT NONE
    CHARACTER(LEN=512) :: C 
    INTEGER, INTENT(IN), OPTIONAL :: IO
    INTEGER, INTENT(OUT) :: N
    REAL*8,  INTENT(OUT) :: V(*)
    IF(PRESENT(IO))THEN
       READ(IO,'(a)')C
    ELSE
       READ(6,'(a)')C
    ENDIF
    CALL CHAR_to_REAL8Array(C,V, N)
  END SUBROUTINE Read_REAL8_Array


  !>                                                                            
  !!    If String1 cover String2 (ignoring spaces at begining and end)
  !!       then cut string1 from begining to String2 and return the rest.
  !!
  !!    For example:                                       \n
  !!           String1 = " ahdkid name=  qqk "             \n
  !!           String2 = "name="                           \n
  !!        then return                                    \n
  !!           String1 = "qqk"
  !!
  !!    IF SUCCEES, return .true., otherwise, .false. and String1 kept unchanged.                            
  !<                                                                            
  FUNCTION CHAR_CutFront(String1,String2) RESULT(Isucc)
    IMPLICIT NONE
    CHARACTER(LEN=*), INTENT(INOUT) :: String1
    CHARACTER(LEN=*), INTENT(IN)    :: String2
    CHARACTER(LEN=512) :: S1, S2
    INTEGER :: i1, i2, len1, len2
    LOGICAL :: Isucc

    Isucc = .FALSE.

    S1   = ADJUSTL(String1)
    S2   = ADJUSTL(String2)
    len1 = LEN_TRIM(S1)
    len2 = LEN_TRIM(S2)
    IF(len1<len2) RETURN

    i2 = 1
    i1 = 1
    DO WHILE(i1<=len1)
       IF(S1(i1:i1)==S2(i2:i2))THEN
          i1 = i1+1
          i2 = i2+1
       ELSE
          i1 = i1-i2 + 2
          i2 = 1
       ENDIF
       IF(i2>len2) EXIT
    ENDDO

    IF(i2>len2)THEN
       Isucc = .TRUE.
       S1(1:i1-1) = ' '
       String1 = ADJUSTL(S1)
    ENDIF
  END FUNCTION CHAR_CutFront

  !>                                                                            
  !!    Check if String2 appears in String1 as a seperated form.
  !!            (splitted by blankspace or "," or ";")
  !!
  !!    For example:                                       \n
  !!           String1 = " ahdkid,NAME,QQK "               \n
  !!           String2 = "NAME"                            \n
  !!        then return .true.                             \n
  !!    For example:                                       \n
  !!           String1 = " ahdkidNAME QQK "                \n
  !!           String2 = "NAME"                            \n
  !!        then return .false.                            \n
  !!
  !!    If String2 is empty or space only, return .false.
  !<                                                                            
  FUNCTION CHAR_Contains(String1,String2) RESULT (TF)
    IMPLICIT NONE
    CHARACTER(LEN=*), INTENT(IN) :: String1 
    CHARACTER(LEN=*), INTENT(IN) :: String2
    LOGICAL :: TF
    CHARACTER(LEN=512) :: S1, S2
    INTEGER :: i1, i2, len1, len2, istart

    TF   = .FALSE.    
    S1   = ADJUSTL(String1)
    S2   = ADJUSTL(String2)
    len1 = LEN_TRIM(S1)
    len2 = LEN_TRIM(S2)
    IF(len1<len2) RETURN
    IF(len2==0) RETURN

    i1 = 0
    TF = .TRUE.
    DO WHILE(i1<=len1)
       i1 = i1 + 1
       IF(i1==1)THEN
          istart = 1
       ELSE IF(S1(i1:i1)==' ' .OR. S1(i1:i1)==',' .OR. S1(i1:i1)==';') THEN
          istart = 0
       ELSE
          istart = istart + 1
       ENDIF

       IF(istart/=1) CYCLE

       DO i2 = 1, len2
          IF(S1(i1:i1)/=S2(i2:i2)) EXIT
          i1 = i1 + 1
       ENDDO

       IF(i2>len2)THEN
          IF(i1>len1) RETURN
          IF(S1(i1:i1)==' ' .OR. S1(i1:i1)==',' .OR. S1(i1:i1)==';') RETURN
          i1 = i1 - 1
       ENDIF
    ENDDO

    TF = .FALSE.

    RETURN
  END FUNCTION  CHAR_Contains

  !>                                                                            
  !!    Get the path from a filename string.
  !!
  !!    For example:                                       \n
  !!           filename = "/abc/N.f90 "                    \n
  !!    then output                                        \n
  !!           path = "/abc/"                             
  !!    @return
  !!           true, these is a path.                      \n
  !!           false, otherwise.
  !<                                                                            
  FUNCTION CHAR_FilenamePath(filename,path) RESULT (TF)
    IMPLICIT NONE
    CHARACTER(LEN=*), INTENT(IN)  :: filename
    CHARACTER(LEN=*), INTENT(OUT) :: path
    LOGICAL :: TF
    INTEGER :: k, i
        k = 0
        DO i = 1, LEN_TRIM( filename )
           IF(filename(i:i)=='/') k = i
        ENDDO
        IF(k/=0)THEN
           path(1:k) = filename(1:k)
           TF = .true.
        ELSE
           path(1:1) = ' '
           TF = .false.
        ENDIF
    RETURN
  END FUNCTION  CHAR_FilenamePath

  !>                                                                            
  !!    Split String into String1 and String2 by blankspace or "," or ";"
  !!       The spaces and "," or ";" at ends may be removed.
  !!
  !!    For example:                                       \n
  !!           String  = " ahdkid,NAME,QQK "               \n
  !!    then output
  !!           String1 = "ahdkid"                          \n
  !!           String2 = "NAME,QQK"  
  !<                                                                            
  SUBROUTINE  CHAR_SPLIT(String, String1, String2)
    IMPLICIT NONE
    CHARACTER(LEN=*), INTENT(IN)  :: String
    CHARACTER(LEN=*), INTENT(OUT) :: String1 
    CHARACTER(LEN=*), INTENT(OUT) :: String2
    CHARACTER(LEN=512) :: S1
    INTEGER :: i1, i2, len1, i
    String1(:) = ' '
    String2(:) = ' '
    S1   = ADJUSTL(String)
    len1 = LEN_TRIM(S1)
    i1 = 0
    i2 = 0
    DO i = 1, len1
       IF(     i1==0 .AND. (S1(i:i)==' ' .OR. S1(i:i)==',' .OR. S1(i:i)==';')) THEN
       ELSE IF(i2==0 .AND. (S1(i:i)==' ' .OR. S1(i:i)==',' .OR. S1(i:i)==';')) THEN
          i1 = -1
       ELSE IF(i1>=0)THEN
          i1 = i1 + 1
          String1(i1:i1) = S1(i:i)
       ELSE
          i2 = i2 + 1
          String2(i2:i2) = S1(i:i)
       ENDIF
    ENDDO
    RETURN
  END SUBROUTINE  CHAR_SPLIT
  
  !>                                                                            
  !!    Return a lower case of a string.
  !!
  !<                                                                            
  FUNCTION CHAR_tolower(String1) RESULT (String2)
    IMPLICIT NONE
    CHARACTER(LEN=*), INTENT(IN) :: String1 
    CHARACTER(LEN=LEN(String1))  :: String2
    CHARACTER(LEN=1)  :: S
    CHARACTER(LEN=26) :: SS = 'abcdefghijklmnopqrstuvwxyz'
    INTEGER :: k, len1, IA, I

    len1 = LEN(String1)
    IA   = IACHAR('A')
    DO k = 1, len1
       S = String1(k:k)
       I = IACHAR(S) - IA + 1
       IF(I>=1 .AND. I<=26) S = SS(I:I)
       String2(k:k) = S
    ENDDO
  END FUNCTION CHAR_tolower

  !>                                                                            
  !!    Check if two strings (ignoring head and end blanks) are the same. 
  !!
  !!    @param[in] String1,String2   : the two strings.
  !!    @param[in] CaseSensitive     : if case sensitive.
  !!                                   =.true. if not presnet.
  !!    @return   =.true.  two strings are the same.    \n
  !!              =.false. two strings are not the same. 
  !!
  !<                                                                            
  FUNCTION CHAR_isEqual(String1,String2,CaseSensitive) RESULT (TF)
    IMPLICIT NONE
    CHARACTER(LEN=*), INTENT(IN) :: String1 
    CHARACTER(LEN=*), INTENT(IN) :: String2
    LOGICAL, INTENT(IN), OPTIONAL :: casesensitive
    LOGICAL :: TF
    LOGICAL :: sens
    CHARACTER(LEN=512) :: S1, S2
    INTEGER :: i, len1, len2

    TF   = .FALSE.    
    S1   = ADJUSTL(String1)
    S2   = ADJUSTL(String2)
    len1 = LEN_TRIM(S1)
    len2 = LEN_TRIM(S2)
    IF(len1/=len2) RETURN

    sens = .TRUE.
    IF(PRESENT(CaseSensitive)) sens = CaseSensitive
    IF(.NOT. sens)THEN
       S1 = CHAR_tolower(S1)
       S2 = CHAR_tolower(S2)
    ENDIF

    DO i = 1, len1
       IF(S1(i:i)/=S2(i:i)) RETURN
    ENDDO
    TF = .TRUE.
  END FUNCTION CHAR_isEqual

  !>                                                                      
  !!     Read an integer parameter with help comment.
  !!     @param[in] TypeMessage :: key work message, 
  !!                               such as 'Input the number of boys:'.
  !!     @param[in] HelpMessage :: Help message shown on screen if you input a '?'.
  !!                               It may give a detail explanation for the parameter.
  !!     @param[out] N          :: the integer by reading.
  !!     @param[out] Isucc      = 1, valid input;    \n
  !!                            = 0, invalid input.  
  !<                                                                      
  SUBROUTINE Read_Int_withHelp(N,TypeMessage, HelpMessage, Isucc)  
    IMPLICIT NONE

    CHARACTER*(*), INTENT(in) :: TypeMessage, HelpMessage
    INTEGER, INTENT(out) :: N
    INTEGER, INTENT(out) :: Isucc
    INTEGER, PARAMETER :: Line_Length = 72
    CHARACTER*256 :: ReadString
    INTEGER     :: i,j
    LOGICAL     :: CarryOn

    CarryOn = .TRUE.
    DO WHILE(CarryOn)
       CarryOn = .FALSE.
       
       WRITE(*,'(a,$)')TypeMessage//' <typing a ? for help>: '
       READ(6,'(A256)')ReadString

       DO i=1,256
          IF(ReadString(i:i)=='?')THEN
             WRITE(*,*)('_',j=1,Line_Length+6)
             CALL Text_Aligned_PRINT(Line_Length,' ')
             CALL Text_Aligned_PRINT(Line_Length,HelpMessage)
             WRITE(*,*)'|__',('_',j=1,Line_Length),'__|'
             WRITE(*,*)' '
             CarryOn = .TRUE.
          ENDIF
       ENDDO
    ENDDO

    N = CHAR_to_INT(ReadString,Isucc)

    RETURN
  END SUBROUTINE Read_Int_withHelp

  !>                                                                      
  !!     Read an character string with help comment.
  !!     @param[in] TypeMessage :: key work message, 
  !!                               such as 'Input the number of boys:'.
  !!     @param[in] HelpMessage :: Help message shown on screen if you input a '?'.
  !!                               It may give a detail explanation for the parameter.
  !!     @param[out] ReadString :: the character string by reading.
  !<  
  SUBROUTINE Read_Chars_withHelp(ReadString, TypeMessage, HelpMessage)
    IMPLICIT NONE

    CHARACTER*(*), INTENT(in) :: TypeMessage, HelpMessage
    INTEGER, PARAMETER :: Line_Length = 72
    CHARACTER(LEN=*) :: ReadString
    INTEGER     :: i,j
    LOGICAL     :: CarryOn

    CarryOn = .TRUE.
    DO WHILE(CarryOn)
       CarryOn = .FALSE.
       
       WRITE(*,'(a,$)')TypeMessage//' <typing a ? for help>: '
       READ(6,'(A)')ReadString

       DO i=1,256
          IF(ReadString(i:i)=='?')THEN
             WRITE(*,*)('_',j=1,Line_Length+6)
             CALL Text_Aligned_PRINT(Line_Length,' ')
             CALL Text_Aligned_PRINT(Line_Length,HelpMessage)
             WRITE(*,*)'|__',('_',j=1,Line_Length),'__|'
             WRITE(*,*)' '
             CarryOn = .TRUE.
          ENDIF
       ENDDO
    ENDDO

    RETURN
  END SUBROUTINE Read_Chars_withHelp



END MODULE Number_CHAR_Transfer
