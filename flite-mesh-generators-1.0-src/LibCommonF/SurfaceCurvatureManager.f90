!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Some functions for module SurfaceCurvature.   
!!
!<
MODULE SurfaceCurvatureManager
  USE SurfaceCurvature
  IMPLICIT NONE

CONTAINS


  !*-------------------------------------------------------------*
  !*                                                             *
  !*    [localm] Carries out the line search minimization from   *
  !*             the point u0(1:2) along the direction dr(1:2)   *
  !*             restricted to the interval [a,b]. eps is the    *
  !*             tolerance for finishing the iteration (of the   *
  !*             order of the square root of the computer        *
  !*             precission and t is a very small number         *
  !*             Adapted from: R. P. Brent (1973), "Algorithms   *
  !*             for minimization without derivatives".          *
  !*                                                             *
  !*-------------------------------------------------------------*
  !>
  !!    (Oubay's code)
  !!    Carries out the line search minimization from the point u0(:) 
  !!    along the direction dr(:) restricted to the interval [a,b]. 
  !!    @param[in] Region : the Region.
  !!    @param[in] xp     : the 3D position.
  !!    @param[in] u0     : the 2D position of the start point of the line.
  !!    @param[in] dr     : the 2D direction of the line
  !!    @param[in] a      : the start of the interval.
  !!    @param[in] b      : the end of the interval.
  !!    @param[in] eps    : the tolerance for finishing the iteration   
  !!                        (of the order of the square root of the computer precission).
  !!    @param[in] t      : a very small number.
  !!    @param[out]  x    : the 2D position u0+x*dr on the Region
  !!                          represents the 3D point xp
  !!    @param[out]  fx   : the error 
  !!                        (distance squar between xp and the returned position).
  !<
  SUBROUTINE RegionType_localm(Region, xp, u0, dr, a, b, eps, t, x, fx )
    IMPLICIT NONE
    TYPE(RegionType), INTENT(in) :: Region
    REAL*8, INTENT(IN) ::  xp(*),  u0(*), dr(*), a, b, eps, t
    REAL*8, INTENT(OUT) :: x, fx
    REAL*8, PARAMETER  :: GOLD = 0.381966d0
    REAL*8   ::  sa, sb, w, v, e, uc, vc, fw, fv, fu, xm, tol, t2
    REAL*8   ::  r, q, p, d, u
    REAL*8   ::  rp(3)

    sa  = a
    sb  = b
    x   = sa + GOLD*(sb-sa)
    w   = x
    v   = w
    e   = 0.d0
    uc  = u0(1) +x*dr(1)
    vc  = u0(2) +x*dr(2)
    CALL RegionType_Interpolate(Region, 0, uc, vc, Rp)
    fx  = (xp(1)-rp(1))**2+(xp(2)-rp(2))**2+(xp(3)-rp(3))**2 
    fw  = fx
    fv  = fw

    Loop_10 : DO
       xm  = 0.5d0*(sa+sb)
       tol = eps*ABS(x) + t
       t2  = 2.d0*tol
       IF( ABS(x-xm) <= t2-0.5d0*(sb-sa) )   RETURN

       r   = 0.0d0
       q   = r
       p   = q
       IF( ABS(e) > tol )   THEN
          r   = (x-w)*(fx-fv)
          q   = (x-v)*(fx-fw)
          p   = (x-v)*q-(x-w)*r
          q   = 2.d0*(q-r)
          IF( q <= 0.d0 ) THEN
             q   = -q
          ELSE   
             p   = -p
          ENDIF
          r   = e 
          e   = d
       ENDIF

       IF(.NOT.( ABS(p) >= ABS(0.5d0*q*r) .OR.  p <= q*(sa-x) .OR.  p >= q*(sb-x) ) ) THEN
          d   = p/q
          u   = x+d
          IF(.NOT. (u-sa >= t2 .AND. sb-u >= t2 )) THEN
             IF( x >= xm ) THEN
                d   = -tol
             ELSE
                d   = tol
             ENDIF
          ENDIF
       ELSE
          IF( x >= xm )  THEN
             e   = sa-x
          ELSE
             e   = sb-x
          ENDIF
          d   = GOLD*e
       ENDIF

       IF( ABS(d) < tol ) THEN
          IF( d <= 0.d0 )THEN
             u   = x-tol
          ELSE       
             u   = x+tol
          ENDIF
       ELSE
          u   = x+d
       ENDIF

       uc  = u0(1)+u*dr(1)
       vc  = u0(2)+u*dr(2)
       CALL RegionType_Interpolate(Region, 0, uc, vc, Rp)
       fu  = (xp(1)-rp(1))**2+(xp(2)-rp(2))**2+(xp(3)-rp(3))**2 

       IF( fu <= fx ) THEN
          IF( u >= x ) THEN
             sa  = x
          ELSE
             sb  = x
          ENDIF
          v   = w
          fv  = fw
          w   = x
          fw  = fx
          x   = u
          fx  = fu
          CYCLE Loop_10
       ENDIF

       IF( u >= x ) THEN
          sb  = u
       ELSE
          sa  = u
       ENDIF

       IF( .NOT. (fu > fw .AND. w /= x ))  THEN
          v   = w
          fv  = fw
          w   = u
          fw  = fu
          CYCLE Loop_10
       ENDIF

       IF( fu > fv .AND. v/=x .AND. v/=w )    CYCLE Loop_10
       v   = u
       fv  = fu

    ENDDO Loop_10

    RETURN
  END SUBROUTINE RegionType_localm

  !>
  !!    (Oubay's code)
  !!    Calculates some values along the boundaries.
  !<
  SUBROUTINE RegionType_bval( Region, box, us, sl2, up, xd, bl )
    IMPLICIT NONE
    TYPE(RegionType), INTENT(in) :: Region
    REAL*8,  INTENT(OUT) :: us(2,4), up(2,4), xd(2,4), bl(2,4), box(4), sl2(4)
    REAL*8  :: UBOT, VBOT, UTOP, VTOP
    INTEGER :: IUTOP, IVTOP
    REAL*8, DIMENSION(:,:,:), POINTER :: RegPosit

    ! *** Notation
    !                    4         3
    !                     +---<---+
    !                     |       |
    !                     v       ^
    !                     |       |
    !                     +--->---+
    !                    1         2

    UBOT  = 0.d0
    VBOT  = 0.d0
    UTOP  = Region%numNodeU - 1
    VTOP  = Region%numNodeV - 1
    IUTOP = Region%numNodeU
    IVTOP = Region%numNodeV

    RegPosit => Region%Posit

    box(1)  = UBOT
    box(2)  = VBOT
    box(3)  = UTOP
    box(4)  = VTOP

    ! *** v = VBOT

    us(1,1) = 0.5*(UBOT+UTOP)
    us(2,1) = VBOT
    sl2(1)  = Geo3D_Distance_SQ( RegPosit(:,1,1), RegPosit(:,IUTOP,1) )
    up(1,1) = UBOT 
    up(2,1) = VBOT 
    xd(1,1) =  1.0 
    xd(2,1) =  0.0 
    bl(1,1) =  0.
    bl(2,1) = UTOP

    ! *** u = UTOP

    us(1,2) = UTOP
    us(2,2) = 0.5*(VBOT+VTOP)
    sl2(2)  = Geo3D_Distance_SQ( RegPosit(:,IUTOP,1), RegPosit(:,IUTOP,IVTOP) )
    up(1,2) = UTOP 
    up(2,2) = VBOT 
    xd(1,2) =  0.0 
    xd(2,2) =  1.0 
    bl(1,2) =  0.
    bl(2,2) = VTOP

    ! *** v = VTOP

    us(1,3) = 0.5*(UBOT+UTOP)
    us(2,3) = VTOP
    sl2(3)  = Geo3D_Distance_SQ( RegPosit(:,IUTOP,IVTOP), RegPosit(:,1,IVTOP) )
    up(1,3) = UTOP 
    up(2,3) = VTOP 
    xd(1,3) = -1.0 
    xd(2,3) =  0.0 
    bl(1,3) =  0. 
    bl(2,3) = UTOP

    ! *** u = UBOT

    us(1,4) = UBOT
    us(2,4) = 0.5*(VBOT+VTOP)
    sl2(4)  = Geo3D_Distance_SQ( RegPosit(:,1,IVTOP),  RegPosit(:,1,1) )
    up(1,4) = UBOT 
    up(2,4) = VTOP 
    xd(1,4) =  0. 
    xd(2,4) = -1. 
    bl(1,4) = 0. 
    bl(2,4) = VTOP

    RETURN
  END SUBROUTINE RegionType_bval


  !>
  !!    [fguess] provides an initial guess for 
  !!       SUBROUTINE RegionType_GetUVFromXYZ()
  !<
  SUBROUTINE RegionType_fguess( Region, xp, uk ,drm, mesh_level)
    IMPLICIT NONE
    TYPE(RegionType), INTENT(in) :: Region
    REAL*8, INTENT(in)  :: xp(*)
    REAL*8, INTENT(out) :: uk(*)
    REAL*8 ::  rp(3), drm, dr2, u, v
    INTEGER :: iu, iv
    INTEGER :: IUTOP, IVTOP, nu,nv,mesh_level
    REAL*8, DIMENSION(:,:,:), POINTER :: RegPosit

    drm = 1.e+38
    nu = Region%numNodeU
    nv = Region%numNodeV

    do iu=1,(nu-1)*mesh_level
      do iv=1,(nv-1)*mesh_level

        u=float(iu-1)/float(mesh_level)+0.5/float(mesh_level)
        v=float(iv-1)/float(mesh_level)+0.5/float(mesh_level)

        CALL RegionType_Interpolate(Region, 0, u, v, Rp)

        dr2   = (rp(1)-xp(1))**2+(rp(2)-xp(2))**2+(rp(3)-xp(3))**2
         if( dr2 .lt. drm ) then
           drm   = dr2
           uk(1) = iu-0.5
           uk(2) = iv-0.5
           uk(1)=u
           uk(2)=v
          endif
      enddo
    enddo
    RETURN
  END SUBROUTINE RegionType_fguess


  !*--------------------------------------------------------------------*
  !*                                                                    *
  !*    [locu2] calculates the coordinates u,v in the parameter plane   *
  !*    of a point on a Ferguson surface given by its 3D coordinates    *
  !*    using a selective splitting in u and v of the surface           *
  !*                                                                    *
  !*--------------------------------------------------------------------*
  !>
  !!  Calculates the coordinates u,v in the parameter plane   
  !!    of a point on a Ferguson surface given by their 3D coordinates
  !!    using a selective splitting in u and v of the surface.
  !!  This routine is an alternate when SUBROUTINE RegionType_GetUVFromXYZ() fails.
  !!
  !!  @param[in] Region  : the surface Region.
  !!  @param[in] xp      : the 3D coordinates of the point.
  !!  @param[in] TOLG    : the tolerance
  !!  @param[out] xl     : the 2D coordinates of the point.   
  !!                       0<xl(1)<Region\%numNodeU-1;
  !!                       0<xl(2)<Region\%numNodeV-1.
  !!  @param[out] rp     : the exact position on the surface with xl.
  !!  @param[out] dis    : The distance between xp and rp.
  !!  @param[out] Ifail  : Flag for termination process.   \n
  !!                     =  0 ... success.                   \n
  !!                     =  1 ... iterations exceeded.       \n
  !!                     = -1 ... failure.                   \n
  !<
  SUBROUTINE RegionType_GetUVFromXYZ_2( Region, xp, xl, rp, dis, Ifail, TOLG)
 !  IMPLICIT NONE
    TYPE(RegionType), INTENT(in) :: Region
    INTEGER, INTENT(out) :: Ifail
    REAL*8,  INTENT(in)  :: xp(3), TOLG
    REAL*8,  INTENT(out) :: rp(3), xl(2)
    REAL*8,  INTENT(out) ::  dis
    REAL*8  :: EPS3D, EPSd, xuvt(2,121), disk,du, EPS, EPS2D, epsdasa
    REAL*8  :: aumx, avmx, uc, vc, aur, avr, dis1, au, auk, aul, av, avk, avl
    INTEGER :: iu, iv, iuc, ivc, iter, nbnd, nit, lu, lv, it, iter1
    INTEGER :: IUTOP, IVTOP, iu1,iu2,iv1,iv2,MBND,ntr,nu,nv

    nu = Region%numNodeU
    nv = Region%numNodeV
    MBND  = 5

    nit   = 1000
    EPS3D = TOLG*TOLG 
    EPS2D = 1.e-8
    EPS   = 1.e-10
    epsdasa = 1.e-09
    EPSd  = 1.e-9
    Ifail = 0
    aumx = (Region%numNodeU-1)-epsd
    avmx = (Region%numNodeV-1)-epsd
  ! aumx  = (IUTOP-1) + epsd
  ! avmx  = (IVTOP-1) + epsd

    dis = 1.e+36  
      au  = 1.0
      iu1=1
      iu2=nu
      iv1=1
      iv2=nv
      if(nu.gt.4) then
        iu1=2
        iu2=nu-1
      endif
      if(nv.gt.4) then
        iv1=2
               iv2=nv-1
      endif
      do iu =  iu1,iu2
        av = 1.0
        do iv = iv1,iv2
          aul=max(epsdasa,min(au,aumx))
          avl=max(epsdasa,min(av,avmx))
          CALL RegionType_Interpolate(Region, 0, au, av, Rp)
          dis1 = (rp(1)-xp(1))**2 + (rp(2)-xp(2))**2 + (rp(3)-xp(3))**2
          if(dis1.lt.dis) then
            aur = au
            avr = av
            dis = dis1
            if(dis.lt.EPS3D)              goto 100
          endif
          av = av + 1.0
        enddo
        au = au + 1.0
      enddo
!     au    = au - 1.0 + EPS
!     av    = av - 1.0 + EPS
      au    = au  + EPS
      av    = av  + EPS
      du    = 1.0
      iter  = 0
  200 iter  = iter + 1
      du    = 0.5*du
      nbnd  = 3
      iter1 = 0
  300 iter1 = iter1 + 1
      disk  = dis
      auk   = aur
      avk   = avr
      ntr   = 0
      uc    = aur - du*nbnd
      do iu = 1,2*nbnd+1
        vc = avr - du*nbnd
        do iv = 1,2*nbnd+1
          if(min(uc,vc).gt.-EPS .and. uc.lt.au .and. vc.lt.av) then
            ntr = ntr + 1
            xuvt(1,ntr) = uc
            xuvt(2,ntr) = vc
          endif
          vc = vc + du
        enddo
        uc = uc + du
      enddo
      do it = 1,ntr
        CALL RegionType_Interpolate(Region, 0, xuvt(1,it),xuvt(2,it), Rp)
        dis1 = (rp(1)-xp(1))**2 + (rp(2)-xp(2))**2 + (rp(3)-xp(3))**2
        if(dis1.lt.dis) then
          aur = xuvt(1,it)
          avr = xuvt(2,it)
          dis = dis1
          if(dis.lt.EPS3D)                goto 100
        endif
      enddo
      if(aur.lt.auk-du*nbnd+EPS .or.aur.gt.auk+du*nbnd-EPS.or.avr.lt.avk-du*nbnd+EPS .or.avr.gt.avk+du*nbnd-EPS )goto 300
      if(iter.gt.NIT) then
        Ifail = 1
        return
      endif
      if( dis .gt. disk ) then
        Ifail = -1
        return
      else
        disk = dis
      endif
      if(du.gt.EPSD)                     goto 200
  100 continue
      xl(1) = aur
      xl(2) = avr

    RETURN

  END SUBROUTINE RegionType_GetUVFromXYZ_2

  !*--------------------------------------------------------------------*
  !*                                                                    *
  !*    [locuv] calculates the coordinates u,v in the parameter plane   *
  !*    of a point on a Ferguson surface given by its 3D coordinates    *
  !*                                                                    *
  !*                        Modified MAR-93                             *
  !*                                                                    *
  !*--------------------------------------------------------------------*
  !>
  !!  (Oubay's Code)
  !!  Calculates the coordinates u,v in the parameter plane   
  !!    of a list of points on a Ferguson surface given by their 3D coordinates.
  !!  Basically, this routine is used for locating the boundary curves to a region.
  !!
  !!  @param[in] Region  : the surface Region.
  !!  @param[in] np      : the number of points.
  !!  @param[in] xg      : the 3D coordinates of each point.
  !!  @param[in] TOLG    :
  !!  @param[out] xl     : the 2D coordinates of each point.   
  !!                       0<xl(1)<Region\%numNodeU-1;
  !!                       0<xl(2)<Region\%numNodeV-1.
  !!  @param[out] drm_max : the maximum distance between a point and its projecting position.
  !!                        If all those distances are less than the criterion, 
  !!                        this will return 0.
  !<
  SUBROUTINE RegionType_GetUVFromXYZ( Region, np, xg, xl, TOLG, drm_max)
 !  IMPLICIT NONE
    TYPE(RegionType), INTENT(in) :: Region
    REAL*8,  INTENT(in)  :: xg(3,*), TOLG
    REAL*8,  INTENT(out) :: xl(2,*), drm_max
    INTEGER, INTENT(in)  :: np
    INTEGER, PARAMETER :: NIT =    100 
    REAL*8,  PARAMETER :: TOL = 1.e-05 
    REAL*8,  PARAMETER :: BRD =    0.1 
    REAL*8,  PARAMETER :: BR2 = 2.*BRD 
    REAL*8  :: xp(3),   uk(2),  box(4), sl2(4), dr_min,drmg
    REAL*8  :: us(2,4), up(2,4), xd(2,4), bl(2,4)
    REAL*8  :: u0(2),   gk(2),  gk1(2),   dr(2)
    REAL*8  :: uin(2),    w(4),  rp(3), ru(3), rv(3), ruv(3) 
    LOGICAL :: flag
    REAL*8  :: EPS1, EPS, EPS2, EPS3, EPS4
    REAL*8  :: drm, dst, dr2, drx, dry, drz, dmd
    REAL*8  :: u1, v1, ax, bx, bet, al, vmd, vsc, fmin
    Real*8  :: u_nearest(2)
    INTEGER :: ip, kb, j, ifl, it, mesh_level,npoints_drm,istck

    ! *** Tolerances for the iterative process

    !     EPS1 ...... distance in 3D coordinates
    !     EPS2 ...... distance in (u,v) coordinates
    !     EPS3 ...... modulus of search direction
    !     EPS4 ...... parallel directions ( 6 deg )

    EPS1 = TOLG 
    EPS  = EPS1**2
    EPS2 = 1.e-04
    EPS3 = 1.e-10
    EPS4 = 0.10

    ! *** Stores values along the boundary

    CALL RegionType_bval( Region, box, us, sl2, up, xd, bl )

    drm_max = 0.d0
    npoints_drm=0

    ! *** Loop over the points on the curve

    Loop_300 : DO ip=1,np
       ! *** Target point

       xp(1:3) = xg(1:3,ip)

       ! *** First, checks for singular points.

       DO j = 1,4
          CALL RegionType_Interpolate(Region, 0, us(1,j), us(2,j), Rp)

          drm = (xp(1)-rp(1))**2+(xp(2)-rp(2))**2+(xp(3)-rp(3))**2
          IF( drm < EPS ) THEN
             u_nearest(1:2) = us(1:2,j)
    !        drm     = SQRT(drm)
             GOTO 200
          ENDIF
       ENDDO

       ! *** Finds the minimum along the boundary

       drm = 1.e+38
       mesh_level = 1
       kb  = 0
       DO j = 1,4
          dst = sl2(j)        
          IF( dst > EPS ) THEN
             CALL RegionType_localm(Region, xp, up(1:2,j), xd(1:2,j), bl(1,j), bl(2,j),   &
                  TOL,    EPS3,      al,     dr2 )
             IF( dr2 < drm ) THEN
                drm     = dr2
                 u_nearest(1:2) = up(1:2,j) + al*xd(1:2,j)
                kb      = j
             ENDIF
             IF( drm < EPS ) THEN 
     !          drm = SQRT(drm)
                GOTO 200
             ENDIF
          ENDIF
       ENDDO

       uk(1)=u_nearest(1) !EADS
       uk(2)=u_nearest(2) !EADS
       dr_min=drm
       ! *** If the minimum has not been found on the boundary, the point 
       !     with minimum distance is used as the initial guess in the 
       !     iterative process. But first we check that the first search 
       !     direction does not coincide with the current one on the 
       !     boundary. If it does we select an alternative first guess.

       CALL RegionType_Interpolate(Region, 1, uk(1),uk(2),Rp,Ru,Rv,Ruv)
       drx   = rp(1)-xp(1)
       dry   = rp(2)-xp(2)
       drz   = rp(3)-xp(3)
       drm   = drx**2+dry**2+drz**2
       IF( drm < EPS ) THEN 

         uk(1)=u_nearest(1) !EADS
         uk(2)=u_nearest(2) !EADS
         dr_min=drm
    !     drm = SQRT(drm)
          GOTO 200
       ENDIF
       if(drm.lt.dr_min) dr_min=drm 
       gk(1) = 2.*( drx*Ru(1)+dry*Ru(2)+drz*Ru(3) )
       gk(2) = 2.*( drx*Rv(1)+dry*Rv(2)+drz*Rv(3) )
       vmd   = SQRT(gk(1)**2+gk(2)**2)
       IF(vmd < EPS3) THEN
   !      drm = SQRT(drm)
   !      GOTO 200
          GOTO 400
       ENDIF
       vmd   = 1./vmd
       dr(1) = -gk(1)*vmd
       dr(2) = -gk(2)*vmd

       ! *** If the direction is not valid uses a different initial 
       !     guess: the closest patch center point

       vsc   = ABS( -dr(1)*xd(2,kb)+dr(2)*xd(1,kb) )
 500   continue
       IF( vsc < EPS4.or.mesh_level.gt.1  ) THEN 
          CALL RegionType_fguess( Region, xp, uk , drmg, mesh_level)
          if( drmg.lt. EPS ) then
            u_nearest(1)=uk(1)  !EADS
            u_nearest(2)=uk(2)  !EADS
            goto 200
          endif
          u_nearest(1)=uk(1)  !EADS
          u_nearest(2)=uk(2)  !EADS
          dr_min=drmg
          CALL RegionType_Interpolate(Region,1,u_nearest(1),u_nearest(2),Rp,Ru,Rv,Ruv)
          drx   = rp(1)-xp(1)
          dry   = rp(2)-xp(2)
          drz   = rp(3)-xp(3)
          drm   = drx**2+dry**2+drz**2
    !     IF( drm < EPS ) THEN
    !        drm = SQRT(drm)
    !        GOTO 200
    !     ENDIF
          gk(1) = 2.*( drx*Ru(1)+dry*Ru(2)+drz*Ru(3) )
          gk(2) = 2.*( drx*Rv(1)+dry*Rv(2)+drz*Rv(3) )
          dr(1) = -gk(1)
          dr(2) = -gk(2)
       ENDIF

       ! *** Here tries a more robust but slower algorithm 
       !     when at a boundary point the line search has taken the
       !     iterative procedure out-of-bounds

        istck = 0
  100   continue
        if( istck .ne. 0 ) then
          CALL RegionType_GetUVFromXYZ_2( Region, xp, uk, rp, drm, ifl, TOLG)

!
! retain nearest point parameter values
!
          if(drm.lt.dr_min) then    !EADS
            u_nearest(1)=uk(1)      !EADS
            u_nearest(2)=uk(2)      !EADS
            dr_min=drm              !EADS
           endif                    !EADS

         if( ifl .eq. 0 ) then
!           write(*,'(a)')
!     &      ' LOCUV > Successful second attempt'
!              write(6,*) 'dr_min',dr_min,u_nearest(1),u_nearest(2)

          else if( ifl .eq. 1 ) then
            write(29,'(a)')' LOCUV > 2nd attempt: number of iterations exceeded'
          else if( ifl .eq. -1 ) then
           write(29,'(a)')' LOCUV > Second attempt failed'
          endif
!          drm = sqrt(drm)
!         write(*,'(/,a)')
!    &    ' LOCUV > Values after second attempt'
!         write(*,'( a,i5,/,2(a,3(1x,1pe12.5),/),
!    &               a,2(1x,1pe12.5),/,a,1x,1pe12.5,/ )')
!    &    '         Target point .. ',ip,
!    &    '         Coordinates ... ',(xp(i),i=1,3),
!    &    '         Last .......... ',(rp(i),i=1,3),
!    &    '         Local ......... ',(uk(i),i=1,2),
!    &    '         Distance ...... ',drm

          if( sqrt(dr_min) .gt. tolg ) then
            if(dr_min.gt.1.and.mesh_level.lt.8) then

            mesh_level=mesh_level+2

            go to 500
            endif

              drm=sqrt(dr_min)
              drm_max=max(drm_max,drm)
              npoints_drm=npoints_drm+1
!           write(*,'(/,a,//,2(a,/,a,1pe12.5,/),2(a,/))')
!     &      ' LOCUV >        !!! *** WARNING *** !!!           ',
!     &      '        The distance between the target point and ',
!     &      '        and the last point in the iteration is:   ',drm,
!     &      '        This is BIGGER than the tolerance used by ',
!     &      '        SURFACE for coincident points which is:   ',tolg,
!     &      '        The run continues but you are advised to  ',
!     &      '        check your input geometry data for errors '
          endif
          goto 200
        endif
 
        do it=1,NIT
          dmd   = sqrt(dr(1)**2+dr(2)**2)
          if(dmd .lt. EPS3) then
            goto 200
          endif
          dmd   = 1./dmd
          dr(1) = dr(1)*dmd
          dr(2) = dr(2)*dmd
!
! *** Is the current direction valid ?
!
          flag = feasib( uk, dr, box, TOL )
          if( .not. flag ) then
             flag = newdr( uk, gk, dr, box, TOL )
             if( .not. flag ) then
               istck = istck+1
!              write(*,'(/,a)')
!    &         ' LOCUV > got stuck ... trying again '
               goto 100
             endif
          endif
!
! *** Max. values of the line parameter in the (du,dv) direction
!
          u1   = 1./sign( max(abs(dr(1)),EPS3) , dr(1) )
          v1   = 1./sign( max(abs(dr(2)),EPS3) , dr(2) )
          w(1) = (box(3)-uk(1))*u1
          w(2) = (box(1)-uk(1))*u1
          w(3) = (box(4)-uk(2))*v1
          w(4) = (box(2)-uk(2))*v1
          call sort( 4, w )
          ax   = 0.
          bx   = w(3)
          if( abs(bx) .lt. TOL ) bx = w(4)
!
! *** Brent's procedure localm for line search
!
          CALL RegionType_localm(Region, xp, uk, dr, ax, bx, TOL, EPS3, al, fmin )
          uk(1) = uk(1)+al*dr(1)
          uk(2) = uk(2)+al*dr(2)
          CALL RegionType_Interpolate(Region,1,uk(1),uk(2),Rp,Ru,Rv,Ruv)
          drx   = rp(1)-xp(1)
          dry   = rp(2)-xp(2)
          drz   = rp(3)-xp(3)
          drm   = drx**2+dry**2+drz**2
          if( drm .lt. EPS ) then
          u_nearest(1)=uk(1)  
          u_nearest(2)=uk(2)  
            goto 200
          endif

          if(drm.lt.dr_min) then    
          u_nearest(1)=uk(1)       
          u_nearest(2)=uk(2)      
          dr_min=drm             
          endif                 

          gk1(1) = 2.*( drx*ru(1)+dry*ru(2)+drz*ru(3) )
          gk1(2) = 2.*( drx*rv(1)+dry*rv(2)+drz*rv(3) )
          if( mod(it,2) .eq. 0 ) then
            dr(1) = -gk1(1)
            dr(2) = -gk1(2)
          else
            bet   = (gk1(1)-gk(1))*gk1(1)+(gk1(2)-gk(2))*gk1(2)        ! Polak-Riviere
!           bet   = (gk1(1)**2+gk1(2)**2)        ! Fletcher-Reeves
            bet   = bet*vmd
            dr(1) = -gk1(1)+bet*dr(1)
            dr(2) = -gk1(2)+bet*dr(2)
          endif
          gk(1) = gk1(1)
          gk(2) = gk1(2)
          vmd   = sqrt(gk(1)**2+gk(2)**2)
          if(vmd .lt. EPS3) then
!            drm = sqrt(drm)
            goto 400
          endif
          vmd = 1./vmd
        enddo
!        drm = sqrt(drm)

400      if(sqrt(dr_min) .gt. tolg ) then
!         write(*,'(/,a)')
!    &    ' LOCUV > Number of iterations exceeded ... trying again'
          istck = istck+1
          goto 100
         endif
  200   continue             ! End of iteration
!
        xl(1,ip) = u_nearest(1)
        xl(2,ip) = u_nearest(2)
!
      end do Loop_300
!
      if(npoints_drm.ne.0) then
   !    write(29,'(/,a,//,(a,/,a,i9,a,1pe12.5,/),(a,/,a,1pe12.5,/),2 (a,/))') 'LOCUV > !!! *** WARNING *** !!!',' The distance between the target',' and',npoints_drm,' points in the iteration is: ',drm_max,'This is BIGGER than the tolerance used by ',' SURFACE for coincident points which is:',tolg,'The run continues but you are advised to','check your input geometry data for errors '     
      endif
      write(29,'(/)')

    RETURN

  END SUBROUTINE RegionType_GetUVFromXYZ


  !>
  !!  (Oubay's Code)
  !!  Calculates the coordinates u,v in the parameter plane   
  !!    of a list of points on a Ferguson surface given by their 3D coordinates.
  !!  Basically, this routine is used for prejecting a point to a surface region.
  !!
  !!  @param[in]  Region : the surface Region.
  !!  @param[in]  xg     : the 3D coordinates of each point.
  !!  @param[in]  TOLG   :
  !!  @param[in]  onSurf =1 the point is supposed on the surface.   \n
  !!                     =0 the point may not on the surface.
  !!  @param[in]  xl     : the initial guess of 2D coordinates of each point.   
  !!  @param[out] xl     : the 2D coordinates of each point.   
  !!                       0<xl(1)<Region\%numNodeU-1;
  !!                       0<xl(2)<Region\%numNodeV-1.
  !!  @param[out] drm    : the distance between a point and its projecting position.
  !!  @param[out] xgout  : the projecting position.
  !<
  SUBROUTINE RegionType_GetUVFromXYZ1( Region,  xg, xl, TOLG, onSurf, drm, xgout)
    IMPLICIT NONE
    TYPE(RegionType), INTENT(in) :: Region
    REAL*8,  INTENT(in)    :: xg(3), TOLG
    INTEGER, INTENT(in)    :: onSurf
    REAL*8,  INTENT(inout) :: xl(2)
    REAL*8,  INTENT(out)   :: drm, xgout(3)
    INTEGER, PARAMETER :: NIT =    100 
    REAL*8,  PARAMETER :: TOL = 1.e-05 
    REAL*8,  PARAMETER :: BRD =    0.1 
    REAL*8,  PARAMETER :: BR2 = 2.*BRD 
    REAL*8  :: xp(3),   uk(2),  box(4), sl2(4)
    REAL*8  :: us(2,4), up(2,4), xd(2,4), bl(2,4)
    REAL*8  :: u0(2),   gk(2),  gk1(2),   dr(2)
    REAL*8  :: uin(2),    w(4),  rp(3), ru(3), rv(3), ruv(3) 
    LOGICAL :: flag
    REAL*8  :: EPS1, EPS, EPS2, EPS3, EPS4
    REAL*8  :: dst, dr2, drx, dry, drz, dmd
    REAL*8  :: u1, v1, ax, bx, bet, al, vmd, fmin
    INTEGER :: j, ifl, it

    ! *** Tolerances for the iterative process

    !     EPS1 ...... distance in 3D coordinates
    !     EPS2 ...... distance in (u,v) coordinates
    !     EPS3 ...... modulus of search direction
    !     EPS4 ...... parallel directions ( 6 deg )
    EPS1 = TOLG 
    EPS  = EPS1**2
    EPS2 = 1.e-04
    EPS3 = 1.e-10
    EPS4 = 0.10

    ! *** Stores values along the boundary

    CALL RegionType_bval( Region, box, us, sl2, up, xd, bl )


    ! *** Target point

    xp(1:3) = xg(1:3)

    ! *** initial position

    uk(1:2) = xl(1:2) 

    ! *** Use the initial guess in the iterative process. 
    !     But first we check that the first search 
    !     direction does not coincide with the current one on the 
    !     boundary. If it does we select an alternative first guess.

    CALL RegionType_Interpolate(Region, 1, uk(1),uk(2),Rp,Ru,Rv,Ruv)
    drx   = rp(1)-xp(1)
    dry   = rp(2)-xp(2)
    drz   = rp(3)-xp(3)
    drm   = drx**2+dry**2+drz**2
    IF( drm < EPS ) THEN 
       drm = SQRT(drm)
       GOTO 200
    ENDIF
    gk(1) = 2.*( drx*Ru(1)+dry*Ru(2)+drz*Ru(3) )
    gk(2) = 2.*( drx*Rv(1)+dry*Rv(2)+drz*Rv(3) )
    vmd   = SQRT(gk(1)**2+gk(2)**2)
    IF(vmd < EPS3) THEN
       drm = SQRT(drm)
       GOTO 200
    ENDIF
    vmd   = 1./vmd
    dr(1) = -gk(1)*vmd
    dr(2) = -gk(2)*vmd


    ! *** Here tries a more robust but slower algorithm 
    !     when at a boundary point the line search has taken the
    !     iterative procedure out-of-bounds


    ! *** iteration for finding the local coordinates

    Loop_it : DO it=1,NIT
       dmd   = SQRT(dr(1)**2+dr(2)**2)
       IF(dmd < EPS3) EXIT

       dmd   = 1./dmd
       dr(1) = dr(1)*dmd
       dr(2) = dr(2)*dmd

       ! *** Is the current direction valid ?

       flag = feasib( uk, dr, box, TOL ) 
       IF( .NOT. flag ) THEN
          flag = newdr( uk, gk, dr, box, TOL )
          IF( .NOT. flag )   GOTO 150
       ENDIF

       ! *** Max. values of the line parameter in the (du,dv) direction

       u1   = 1./SIGN( MAX(ABS(dr(1)),EPS3) , dr(1) )
       v1   = 1./SIGN( MAX(ABS(dr(2)),EPS3) , dr(2) )
       w(1) = (box(3)-uk(1))*u1
       w(2) = (box(1)-uk(1))*u1
       w(3) = (box(4)-uk(2))*v1
       w(4) = (box(2)-uk(2))*v1
       CALL SelectionSort( 4, w )
       ax   = 0.
       bx   = w(3)
       IF( ABS(bx) < TOL ) bx = w(4)

       ! *** Brent's procedure localm for line search

       CALL RegionType_localm(Region, xp, uk, dr, ax, bx, TOL, EPS3, al, fmin )
       uk(1) = uk(1)+al*dr(1)
       uk(2) = uk(2)+al*dr(2)
       CALL RegionType_Interpolate(Region, 1, uk(1),uk(2),Rp,Ru,Rv,Ruv)

       drx   = rp(1)-xp(1)
       dry   = rp(2)-xp(2)
       drz   = rp(3)-xp(3)
       drm   = drx**2+dry**2+drz**2
       IF( drm < EPS ) THEN 
          drm = SQRT(drm)
          GOTO 200
       ENDIF
       gk1(1) = 2.*( drx*Ru(1)+dry*Ru(2)+drz*Ru(3) )
       gk1(2) = 2.*( drx*Rv(1)+dry*Rv(2)+drz*Rv(3) )
       IF( MOD(it,2) == 0 ) THEN
          dr(1:2) = -gk1(1:2)
       ELSE
          bet   = (gk1(1)-gk(1))*gk1(1)+ (gk1(2)-gk(2))*gk1(2)        ! Polak-Riviere
          !   bet   = (gk1(1)**2+gk1(2)**2)        ! Fletcher-Reeves
          bet   = bet*vmd
          dr(1) = -gk1(1)+bet*dr(1)
          dr(2) = -gk1(2)+bet*dr(2)
       ENDIF
       gk(1:2) = gk1(1:2)
       vmd   = SQRT(gk(1)**2+gk(2)**2)
       IF(vmd < EPS3)  EXIT   
       vmd = 1./vmd
    ENDDO Loop_it

    drm = SQRT(drm)
    IF( onSurf==0 .OR. drm <= TOLG ) GOTO 200


150 CONTINUE

    !---- search fail, try another method.
    CALL RegionType_GetUVFromXYZ_2( Region, xp, uk, rp, drm, ifl, TOLG)
    drm     = SQRT(drm)

200 CONTINUE

    xl(1:2)    = uk(1:2)
    xgout(1:3) = rp(1:3)

    RETURN
  END SUBROUTINE RegionType_GetUVFromXYZ1

  SUBROUTINE sort(n,a)
 
      real*8 :: a(*), tmp
      INTEGER :: i,j,k,n
      do i = 1,n-1
        k = i
        do j = i+1,n
          if( a(j) .lt. a(k) ) k = j
        enddo
        tmp  = a(k)
        a(k) = a(i)
        a(i) = tmp
      enddo
      return
  END SUBROUTINE sort

  !*---------------------------------------------------------------------*
  !*                                                                     *
  !*    [feasib] checks whether a direction of advance is admissible or  *
  !*             not. The criterion used is that a direction is valid    *
  !*             if advancing from (u0,v0) a distance "tol" along it     *
  !*             the resulting point (u,v) lies within the bounds of     *
  !*             the parameter plane, i.e. [0,nu-1]x[0,nv-1]             *
  !*                                                                     *
  !*---------------------------------------------------------------------*
  FUNCTION feasib( uk, dr, box, tol ) RESULT (TF)
    IMPLICIT NONE
    REAL*8 ::  uk(*), dr(*), box(*), tol
    REAL*8 ::  up(2), vmd
    LOGICAL :: TF

    ! *** Normalize the direction
    ! *** New position

    vmd = tol / SQRT( dr(1)**2+dr(2)**2 )
    up(1:2) = uk(1:2) + vmd * dr(1:2)    

    ! *** Still in the parameter plane ? 

    IF(  up(1) >= box(1)  .AND.  up(1) <= box(3)  .AND.      & 
         up(2) >= box(2)  .AND.  up(2) <= box(4)       ) THEN
       TF = .TRUE.
    ELSE
       TF = .FALSE. 
    ENDIF

    RETURN
  END FUNCTION feasib

  !*--------------------------------------------------------*
  !*                                                        *
  !*    [newdr] finds an alternative feasible direction or  *
  !*            returns a .false. if this is not possible   *
  !*                                                        *
  !*--------------------------------------------------------*
  FUNCTION newdr( uk, gk, dr, box, tol ) RESULT (TF)
    IMPLICIT NONE

    REAL*8, PARAMETER :: EPS = 1.d-05
    REAL*8  :: uk(*), dr(*), gk(*), box(*), tol
    REAL*8  :: dub, dut, dvb, dvt
    LOGICAL :: flag
    LOGICAL :: TF

    TF = .TRUE.

    ! *** Checks the active constraints

    dub = ABS(uk(1)-box(1))
    dut = ABS(uk(1)-box(3))
    dvb = ABS(uk(2)-box(2))
    dvt = ABS(uk(2)-box(4))

    ! *** Projects the descent direction ( opposite to the gradient )
    !     along the active boundary and verifies whether the new 
    !     direction is acceptable

    IF( dub <= tol  .OR.  dut <= tol ) THEN
       IF( ABS(gk(1)) > EPS ) THEN 
          dr(1) = SIGN(1.d+00,-gk(1))
          dr(2) = 0.
          flag  = feasib( uk, dr, box, tol ) 
          IF( flag ) RETURN
       ENDIF
    ENDIF

    IF( dvb <= tol  .OR.  dvt <= tol ) THEN
       IF( ABS(gk(2)) > EPS ) THEN 
          dr(1) = 0.
          dr(2) = SIGN(1.d+00,-gk(2))
          flag  = feasib( uk, dr, box, tol ) 
          IF( flag ) RETURN
       ENDIF
    ENDIF

    ! *** There is no feasible direction along the boundary 

    TF = .FALSE.

    RETURN
  END FUNCTION newdr


  !>
  !!  Search the shortest path on a Ferguson surface.
  !!
  !!  @param[in]  Region  : the surface Region.
  !!  @param[in]  uv1,uv2 : the two ends on the parameter plane.
  !!  @param[in]  NP      : the number of nodes on the path (not including two ends). 
  !!  @param[out] uvn (2,NP) : the coordinates of the nodes on the parameter plane.
  !<
  SUBROUTINE RegionType_GetShortPath(Region,  uv1, uv2, NP, uvn)
    IMPLICIT NONE
    TYPE(RegionType), INTENT(in) :: Region
    REAL*8,  INTENT(in)   :: uv1(2), uv2(2)
    INTEGER, INTENT(in)   :: NP
    REAL*8,  INTENT(out)  :: uvn(2,*)
    INTEGER, PARAMETER    :: N = 65   !-- precise index, (2**K+1)
    REAL*8  :: uvs(2,N), pts(3,N), Dis(N)
    INTEGER :: i1, i2, i, m, k, nlist, iloop
    INTEGER, DIMENSION(N) :: next, list, level
    REAL*8  :: t1(2), t2(2), um(2), uvmin(2), uvmax(2)
    REAL*8  :: p1(3), p2(3), pm(3), d, dd

    uvmin(:) = 0.d0
    uvmax(1) = Region%numNodeU - 1
    uvmax(2) = Region%numNodeV - 1

    uvs(:,1) = uv1(:)
    uvs(:,2) = uv2(:)
    CALL RegionType_Interpolate (Region, 0, uv1(1), uv1(2), P1)
    CALL RegionType_Interpolate (Region, 0, uv2(1), uv2(2), P2)
    pts(:,1) = P1
    pts(:,2) = P2
    level(:) = 0
    next(1)  = 2

    nlist = 2
    i1    = 1
    iloop = 0
    DO WHILE(nlist<N)
       IF(i1==1)  iloop = iloop+1      
       i2 = next(i1)

       p1 = pts(:,i1)
       p2 = pts(:,i2)
       t1 = uvs(:,i1)
       t2 = uvs(:,i2)
       m  = 2* (N-nlist) + 7      
       CALL RegionType_GetShortPath_bisect()

       !--- add a node
       nlist = nlist+1
       uvs(:,nlist) = um(:)
       pts(:,nlist) = Pm(:)
       next(i1)     = nlist
       next(nlist)  = i2
       level(nlist) = iloop

       IF(level(nlist-1)==iloop)THEN
          t1 = uvs(:,nlist-1)
          t2 = uvs(:,nlist)
          p1 = pts(:,nlist-1)
          p2 = pts(:,nlist)
          CALL RegionType_GetShortPath_bisect()
          uvs(:,i1) = um(:)
          pts(:,i1) = Pm(:)
       ENDIF

       i1 = i2
       IF(i1==2)  i1 = 1
    ENDDO

    !--- sort
    list(1) = 1
    DO i = 2,N
       list(i) = next(list(i-1))
    ENDDO

    uvs(:,1:N) = uvs(:,list(1:N))
    pts(:,1:N) = pts(:,list(1:N))

    !--- distance accumulate
    Dis(1) = 0.
    DO i = 2,N
       Dis(i) = Dis(i-1) + Geo3D_Distance(pts(:,i-1),pts(:,i))
    ENDDO

    i2 = 2
    dd = Dis(N) / (NP+1)
    DO k = 1, NP
       d = k*dd
       do i = i2, N
          if(Dis(i)>d) exit
       enddo
       i2 = i
       i1 = i2-1
       uvn(:,k) = uvs(:,i1) + (d-Dis(i1)) / (Dis(i2)-Dis(i1)) * (uvs(:,i2) - uvs(:,i1))
    ENDDO
    
    RETURN
    
  CONTAINS

    SUBROUTINE RegionType_GetShortPath_bisect()
      IMPLICIT NONE
      INTEGER :: i
      REAL*8  :: duv(2), unc(2), un(2), dmin, d, Pt(3)
      duv(1) =  t2(2) - t1(2)
      duv(2) =  t1(1) - t2(1)
      unc(:) = (t1(:) + t2(:)) / 2.d0
      duv(:) = duv(:) / (2*m)            
      dmin = 1.d30
      DO i = -m,m
         un = unc(:) + i * duv(:)
         IF(un(1)<uvmin(1)) CYCLE
         IF(un(2)<uvmin(2)) CYCLE
         IF(un(1)>uvmax(1)) CYCLE
         IF(un(2)>uvmax(2)) CYCLE
         CALL RegionType_Interpolate (Region, 0, un(1), un(2), Pt)
         d = Geo3D_Distance(Pt,P1) + Geo3D_Distance(Pt,P2)
         IF(d<dmin)THEN
            dmin = d
            um   = un
            Pm   = Pt
         ENDIF
      ENDDO
    END SUBROUTINE RegionType_GetShortPath_bisect

  END SUBROUTINE RegionType_GetShortPath






  !>   Sort the boundary curves for a region (i.e. Region%IC) so that
  !!       1.  the curves appear in order to form loops.
  !!       2.  if Region%IC(i)>0 then the region's orientation marchs the curve's direction.
  !!
  !!  @param[in,out] Curv
  !!  @param[in]     TOLG       : the tolerance
  !!
  !<
  SUBROUTINE SurfaceCurvature_SortCurve( Curv, TOLG)
    USE Queue
    IMPLICIT NONE
    INTEGER, PARAMETER   :: MaxLoop = 100
    TYPE(SurfaceCurvatureType), INTENT(inout) :: Curv
    REAL*8, INTENT(in)   :: TOLG
    INTEGER, DIMENSION(:  ), POINTER :: icList
    REAL*8,  DIMENSION(:  ), POINTER :: area
    LOGICAL :: store
    INTEGER :: RegionID, nloop, LoopEnds(0:MaxLoop)
    INTEGER :: is, js,kst, nCurv, Ifail, id, k, j
    REAL*8  :: p1(3), p2(3), q1(3), q2(3), uv1(2), uv2(2), rp(3)
    REAL*8  :: ARMX, dis, EPS, ap
    TYPE(RegionType), POINTER :: Region
    TYPE(CurveType),  POINTER :: line, line2

    EPS = TOLG*TOLG

    !     icList ... list of ordered segments forming the boundary
    !     area ... area in the parameter plane enclosed by the loops
    ALLOCATE( icList(Curv%NB_Curve) )
    ALLOCATE( area(Curv%NB_Curve) )

    DO RegionID = 1, Curv%NB_Region
       Region => Curv%Regions(RegionID)
       nCurv = Region%numCurve
       icList(1:nCurv) = ABS(Region%IC(1:nCurv))

       ! *** Forms the initial front and finds the boundary loops

       nloop = 0
       store = .TRUE.
       LoopEnds(0) = 0
       Loop_is : DO is = 1,nCurv 
          IF(store) THEN
             ! Beginning of a new loop 
             kst     = icList(is)
             line => Curv%Curves(kst)
             p1    = line%Posit(:,1)
             p2    = line%Posit(:,line%numNodes)
             store  = .FALSE.
             nloop  = nloop+1
             LoopEnds(nloop) = LoopEnds(nloop-1) +1
             IF(nloop>MaxLoop) CALL Error_STOP ( '--- SurfaceCurvature_SortCurve::nloop>MaxLoop')
          ELSE
             ! Finds a contiguous segment
             Loop_js : DO js = is, nCurv
                line2 => Curv%Curves(icList(js))
                q1  = line2%Posit(:,1)
                q2  = line2%Posit(:,line2%numNodes)
                IF( Geo3D_Distance_SQ(p2,q1)<EPS ) THEN         ! Preserves the original orientation
                   kst = icList(js)
                   p2 = q2
                   EXIT Loop_js
                ELSE IF( Geo3D_Distance_SQ(p2,q2)<EPS ) THEN    ! Changes the original orientation
                   kst = -icList(js)
                   p2 = q1
                   EXIT Loop_js
                ENDIF

                IF(js==nCurv)THEN
                   WRITE(*, *) '  Error--- Forming the initial front for Region: ',RegionID
                   CALL Error_STOP ( '--- SurfaceCurvature_SortCurve')
                ENDIF
             ENDDO Loop_js

             icList(js) = icList(is)    ! Shuffles the list
             icList(is) = kst
             LoopEnds(nloop) = LoopEnds(nloop) + 1
          ENDIF


          !--- checks against the first point
          IF( Geo3D_Distance_SQ(p2,p1)<EPS ) THEN
             !--- loop closed
             store             = .TRUE.
          ENDIF
       ENDDO Loop_is

       IF( .NOT. store ) THEN
          WRITE(*, *) '  Error--- Cannot close the front for Region: ',RegionID
          CALL Error_STOP (  '--- SurfaceCurvature_SortCurve')
       ENDIF


       ! *** After forming the loops of segments computes the area
       !     in the parameter plane enclosed in each of them.

       ! *** Area of the parameter plane where the surface Region is defined.

       ARMX = 4 * (Region%numNodeU - 1.d0 + 1.d-03) * (Region%numNodeV - 1.d0 + 1.d-03)

       DO k = 1,nloop          ! Computes the areas
          area(k) = 0
          DO is = LoopEnds(k-1)+1, LoopEnds(k)
             kst = icList(is)
             line => Curv%Curves(ABS(kst))
             ap = 0
             DO j = 1, line%numNodes
                p1  = line%Posit(:,j)
                CALL RegionType_GetUVFromXYZ_2 (Region, p1, uv2, rp, dis, Ifail, TOLG)
                IF(j>1)THEN
                   ap = ap + (uv1(1)+uv2(1)) * (uv2(2)-uv1(2))   &
                        - (uv1(2)+uv2(2)) * (uv2(1)-uv1(1))
                ENDIF
                uv1 = uv2
             ENDDO
             IF(kst>0)THEN
                area(k) = area(k) + ap
             ELSE
                area(k) = area(k) - ap
             ENDIF
          ENDDO
          IF( ABS(area(k)) > ARMX ) THEN
             WRITE(*,*)' Error--- Area error in Region. ID=',RegionID
             CALL Error_STOP ( '--- SurfaceCurvature_SortCurve')
          ENDIF
       ENDDO

       ! *** Sorts the loops according to the area

       k = 1
       DO j = 2,nloop
          IF( ABS(area(j)) > ABS(area(k)) ) k = j
       ENDDO

       js = 0
       id = 1
       IF(area(k)<0) id = -1
       DO is = LoopEnds(k-1)+1, LoopEnds(k)
          js = js+1
          Region%IC(js) = id * icList(is)
       ENDDO


       DO j = 1,nloop 
          IF(j==k) CYCLE
          id = 1
          IF(area(j)>0) id = -1
          DO is =  LoopEnds(j-1)+1, LoopEnds(j)
             js = js+1
             Region%IC(js) = id * icList(is)         
          ENDDO
       ENDDO

    ENDDO
    ! *** Frees memory for auxiliary vectors

    DEALLOCATE( area,  icList)

    RETURN
  END SUBROUTINE SurfaceCurvature_SortCurve

  !>
  !!   Merge two curvatures into one.
  !!   The common surfaces will be removed. 
  !!   The curves and regions will be resorted.
  !!  @param[in]  Curv1,Curv2  the two curvatures being merged.
  !!  @param[out] Curv         the merged curvature.
  !!  @param[in]  TOLG         the tolerance.
  !!
  !<
  SUBROUTINE SurfaceCurvature_Merge( Curv1, Curv2, Curv, TOLG)
    IMPLICIT NONE
    INTEGER, PARAMETER   :: MaxLoop = 100
    TYPE(SurfaceCurvatureType), INTENT(in) :: Curv1
    TYPE(SurfaceCurvatureType), INTENT(in) :: Curv2
    TYPE(SurfaceCurvatureType), INTENT(inout) :: Curv
    REAL*8, INTENT(in)   :: TOLG
    INTEGER, DIMENSION(:), POINTER :: cmatch1, cmatch2, rmatch1, rmatch2
    INTEGER :: ic, ic1, ic2, is, is1, is2, n1, n2, nn, i1, i2
    REAL*8  :: p1(3), p2(3), p3(3), p4(3), q1(3), q2(3), EPS

    ALLOCATE(cmatch1(Curv1%NB_Curve),  cmatch2(Curv2%NB_Curve))
    ALLOCATE(rmatch1(Curv1%NB_Region), rmatch2(Curv2%NB_Region))
    cmatch1(:) = 0
    cmatch2(:) = 0
    rmatch1(:) = 0
    rmatch2(:) = 0

    !--- check matched curves

    DO ic1 = 1, Curv1%NB_Curve
       n1 = Curv1%Curves(ic1)%NumNodes
       p1 = Curv1%Curves(ic1)%Posit(:,1)
       p2 = Curv1%Curves(ic1)%Posit(:,n1)
       EPS = MAX(ABS(p1(1)-p2(1)), ABS(p1(2)-p2(2)), ABS(p1(3)-p2(3)))
       EPS = TOLG * EPS

       Loop_ic2 : DO ic2 = 1, Curv2%NB_Curve
          IF(cmatch2(ic2)/=0) CYCLE Loop_ic2
          n2 = Curv2%Curves(ic2)%NumNodes
          IF(n2/=n1) CYCLE Loop_ic2

          p3 = Curv2%Curves(ic2)%Posit(:,1)
          p4 = Curv2%Curves(ic2)%Posit(:,n2)
          IF(pointeq(p1,p3) .AND. pointeq(p2,p4))THEN
             DO i1 = 2, n1-1
                q1 = Curv1%Curves(ic1)%Posit(:,i1)
                q2 = Curv2%Curves(ic2)%Posit(:,i1)
                IF(.NOT. pointeq(q1,q2)) CYCLE Loop_ic2
             ENDDO
             cmatch2(ic2) = ic1
          ELSE IF(pointeq(p1,p4) .AND. pointeq(p2,p3))THEN
             DO i1 = 2, n1-1
                q1 = Curv1%Curves(ic1)%Posit(:,i1)
                i2 = n2+1 - i1
                q2 = Curv2%Curves(ic2)%Posit(:,i2)
                IF(.NOT. pointeq(q1,q2)) CYCLE Loop_ic2
             ENDDO
             cmatch2(ic2) = ic1
          ENDIF
       ENDDO Loop_ic2
    ENDDO


    !--- check matched regions

    Loop_is2 : DO is2 = 1, Curv2%NB_Region

       n2 = Curv2%Regions(is2)%numCurve       
       DO i2 = 1, n2
          IF(cmatch2(Curv2%Regions(is2)%IC(i2))==0) CYCLE Loop_is2 
       ENDDO

       Loop_is1 : DO is1 = 1,  Curv1%NB_Region
          n1 = Curv1%Regions(is1)%numCurve   
          IF(n1/=n2)          CYCLE Loop_is1   !--- not match with is1
          IF(rmatch1(is1)/=0) CYCLE Loop_is1   !--- not match with is1

          DO i2 = 1, n2
             DO i1 = 1, n1
                IF(Curv1%Regions(is1)%IC(i1)==cmatch2(Curv2%Regions(is2)%IC(i2))) EXIT
                IF(i1==n1) CYCLE Loop_is1   !--- not match with is1
             ENDDO
          ENDDO

          !--- warning--- further needed, under-structured.
          rmatch1(is1) = is2
          rmatch2(is2) = is1
          CYCLE Loop_is2
       ENDDO Loop_is1
    ENDDO Loop_is2


    !-- resort curves
    DO is1 = 1, Curv1%NB_Region
       IF(rmatch1(is1)==0)THEN
          nn = Curv1%Regions(is1)%numCurve
          cmatch1(Curv1%Regions(is1)%IC(1:nn)) = -1  
       ENDIF
    ENDDO
    DO is2 = 1, Curv2%NB_Region
       IF(rmatch2(is2)==0)THEN
          nn = Curv2%Regions(is2)%numCurve
          WHERE(cmatch2(Curv2%Regions(is2)%IC(1:nn))==0 )   &
               cmatch2(Curv2%Regions(is2)%IC(1:nn)) = -1  
       ENDIF
    ENDDO

    ALLOCATE(Curv%Curves(Curv1%NB_Curve + Curv2%NB_Curve))
    ic = 0
    DO ic1 = 1, Curv1%NB_Curve
       IF(cmatch1(ic1)/=0)THEN
          ic = ic + 1
          cmatch1(ic1) = ic
          CALL CurveType_Copy(Curv1%Curves(ic1), Curv%Curves(ic))
          Curv%Curves(ic)%ID = ic
       ENDIF
    ENDDO
    DO ic2 = 1, Curv2%NB_Curve
       IF(cmatch2(ic2)>0)THEN
          ic1 = cmatch2(ic2)
          cmatch2(ic2) = cmatch1(ic1)
       ELSE IF(cmatch2(ic2)<0)THEN
          ic = ic + 1
          cmatch2(ic2) = ic
          CALL CurveType_Copy(Curv2%Curves(ic2), Curv%Curves(ic))
          Curv%Curves(ic)%ID = ic
       ENDIF
    ENDDO

    Curv%NB_Curve = ic


    !-- resort regions

    ALLOCATE(Curv%Regions(Curv1%NB_Region + Curv2%NB_Region))  

    is = 0
    DO is1 = 1, Curv1%NB_Region
       IF(rmatch1(is1)==0)THEN
          is = is + 1
          CALL RegionType_Copy(Curv1%Regions(is1), Curv%Regions(is))
          nn = Curv%Regions(is)%numCurve
          Curv%Regions(is)%IC(1:nn) = cmatch1(Curv%Regions(is)%IC(1:nn))
          Curv%Regions(is)%ID       = is
       ENDIF
    ENDDO
    DO is2 = 1, Curv2%NB_Region
       IF(rmatch2(is2)==0)THEN
          is = is + 1
          CALL RegionType_Copy(Curv2%Regions(is2), Curv%Regions(is))
          nn = Curv%Regions(is)%numCurve
          Curv%Regions(is)%IC(1:nn) = cmatch2(Curv%Regions(is)%IC(1:nn))
          Curv%Regions(is)%ID       = is
       ENDIF
    ENDDO

    Curv%NB_Region = is

    DEALLOCATE(cmatch1,  cmatch2, rmatch1, rmatch2)
    RETURN

  CONTAINS

    FUNCTION pointeq(t1,t2) RESULT(tf)
      REAL*8  :: t1(3), t2(3)
      LOGICAL :: tf
      tf = .FALSE.
      IF(ABS(t1(1)-t2(1))>EPS) RETURN
      IF(ABS(t1(2)-t2(2))>EPS) RETURN
      IF(ABS(t1(3)-t2(3))>EPS) RETURN
      tf = .TRUE.
      RETURN
    END FUNCTION pointeq


  END SUBROUTINE SurfaceCurvature_Merge

  !>
  !!    Check the stretching ratios of the surface curvature.   
  !!
  !!  @param[in] Curv   the curvarure with Tangents 
  !!                    (after calling SurfaceCurvature_BuildTangent() ).
  !!
  !<
  SUBROUTINE SurfaceCurvature_Check( Curv)

    IMPLICIT NONE

    TYPE(SurfaceCurvatureType), INTENT(in) :: Curv
    INTEGER :: is, itp, id, iv, iu, nu, nv, ic, i, j
    REAL*8  :: stmax, s12, s13, s24, s34, smin, smax, stre, stmx
    REAL*8  :: a(5), pMin(3), pMax(3), P1(3), P2(3), D, TOLG, drm_max
    TYPE(CurveType), POINTER :: pc
    REAL*8,  DIMENSION(:,:), POINTER :: uv

    stmax = 20.
    pMin  =  1.D36
    pMax  = -1.D36

    WRITE(*, '(/,a,i7)') '  checking patch stretching'


    DO is=1,Curv%NB_Region
       itp = Curv%Regions(is)%TopoType
       id  = Curv%Regions(is)%ID
       nu  = Curv%Regions(is)%numNodeU
       nv  = Curv%Regions(is)%numNodeV
       WRITE(*, *)
       WRITE(*, '(5x,3(a,i7))') 'surface ID=',id,'   nu=',nu,'   nv=',nv

       CALL RegionType_StretchCheck(Curv%Regions(is), smin, smax, stmx)

       WRITE(*, '(15x,a,f10.2)') 'max. stretching: ',stmx
       IF(stmx>stmax)THEN
          WRITE(*,'(a,f6.2,a)')     & 
               '     stretchings bigger than ',stmax,' tend to produce distorted meshes!!'
       ENDIF
       DO i = 1,3
          P1(i) = MINVAL(Curv%Regions(is)%Posit(i,1:nu,1:nv))
          P2(i) = MAXVAL(Curv%Regions(is)%Posit(i,1:nu,1:nv))
       ENDDO
       pMin = MIN(pMin, P1)
       pMax = MAX(pMax, P2)

       DO i = 1,Curv%Regions(is)%numCurve

          ic = Curv%Regions(is)%IC(i)
          pc => Curv%Curves(ic)
          ALLOCATE( uv(2,pc%numNodes))
          TOLG = Geo3D_Distance_SQ(pc%Posit(:,2), pc%Posit(:,1))
          DO j = 2, pc%numNodes -1
             D    = Geo3D_Distance_SQ(pc%Posit(:,j+1), pc%Posit(:,j))
             TOLG = MIN(TOLG, D)
          ENDDO
          TOLG = 0.1 * DSQRT(TOLG)

          CALL RegionType_GetUVFromXYZ(Curv%Regions(is), pc%numNodes, pc%Posit, uv, TOLG, drm_max)
          IF( drm_max > TOLG ) THEN 
             WRITE(*, '(  a,i4,a,i4)') '  The maximum distance between curve ',ic,' and surface ',is
             WRITE(*, '(  a,e12.5,a,e12.5)') '        is ',drm_max,' which is BIGGER than the tolerance :   ', TOLG
          ENDIF
          DEALLOCATE( uv )
       ENDDO

    ENDDO

    WRITE(*,*) ' The minimum corner of domain: ', REAL(pMin)
    WRITE(*,*) ' The maximum corner of domain: ', REAL(pMax)

    RETURN
  END SUBROUTINE SurfaceCurvature_Check


END MODULE SurfaceCurvatureManager

