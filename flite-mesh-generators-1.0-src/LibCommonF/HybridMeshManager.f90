!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!    A collection of functions for module SurfaceMeshStorage.
!<
MODULE HybridMeshManager
  USE array_allocator
  USE Number_Char_Transfer
  USE CellConnectivity
  USE Geometry3D
  USE HybridMeshStorage
  USE TetMeshStorage
  USE SurfaceMeshStorage
  USE SurfaceMeshManager
  USE NodeNetTree
  IMPLICIT NONE


CONTAINS



  !*******************************************************************************
  !>     
  !!    Build HybMesh%Next_Tet & Next_Hex
  !!    @param[in,out] HybMesh  the hybrid mesh.
  !<     
  !*******************************************************************************
  SUBROUTINE HybridMesh_BuildNext(HybMesh)
    IMPLICIT NONE

    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    INTEGER :: IFace, IT, Msize
    INTEGER :: i, ip8(8), ip4(4), ip3(3), ii(2)

    INTEGER, DIMENSION(:,:), POINTER :: TetFace    !-- (2,:)
    TYPE(NodeNetTreeType) :: Face_Tree

    Msize = HybMesh%NB_Tet *4

    CALL allc_2Dpointer (HybMesh%Next_Tet,   4, HybMesh%NB_Tet,   'Next_Tet')
    ALLOCATE(TetFace(2,Msize))
    CALL NodeNetTree_allocate(3, HybMesh%NB_Point, Msize, Face_Tree)
    TetFace(:,:) = 0

    DO IT = 1,HybMesh%NB_Tet

       ip4(:)=HybMesh%IP_Tet(:,IT)
       DO i=1,4
          ip3(1:3)=ip4(iTri_Tet(1:3,i))

          CALL NodeNetTree_SearchAddRemove( 3,ip3,Face_Tree,IFace)

          ii(:)  = TetFace(:,ABS(IFace))
          IF(IFace>0 .AND. ii(1)==0)THEN       !--- a new Face
             TetFace(1,IFace) = IT
             TetFace(2,IFace) = i
             HybMesh%Next_Tet(i,IT) = -1
          ELSE IF(IFace<0 .AND. ii(1)>0)THEN
             IFace = ABS(IFace)
             HybMesh%Next_Tet(i,IT) = ii(1)
             HybMesh%Next_Tet(ii(2),ii(1)) = IT
             TetFace(:,IFace) = 0
          ELSE
             WRITE(*,*)'Error--- : 3 elements share a face. IFace=',IFace
             WRITE(*,*)'It: ',IT, ii(:)
             WRITE(*,*)'IP: ',HybMesh%IP_Tet(:, IT)
             CALL Error_STOP ( '--- HybridMesh_BuildNext')
          ENDIF

       ENDDO
    ENDDO

    DEALLOCATE(TetFace)
    CALL NodeNetTree_clear ( Face_Tree )

    Msize = HybMesh%NB_Hex *6

    CALL allc_2Dpointer (HybMesh%Next_Hex,   6, HybMesh%NB_Hex,   'Next_Hex')
    ALLOCATE(TetFace(2,Msize))
    CALL NodeNetTree_allocate(4, HybMesh%NB_Point, Msize, Face_Tree)
    TetFace(:,:) = 0

    DO IT = 1,HybMesh%NB_Hex

       ip8(:)=HybMesh%IP_Hex(:,IT)
       DO i=1,6
          ip4(1:4)=ip8(iQuadFD_Hex(1:4,i))

          CALL NodeNetTree_SearchAddRemove( 4,ip4,Face_Tree,IFace)

          ii(:)  = TetFace(:,ABS(IFace))
          IF(IFace>0 .AND. ii(1)==0)THEN       !--- a new Face
             TetFace(1,IFace) = IT
             TetFace(2,IFace) = i
             HybMesh%Next_Hex(i,IT) = -1
          ELSE IF(IFace<0 .AND. ii(1)>0)THEN
             IFace = ABS(IFace)
             HybMesh%Next_Hex(i,IT) = ii(1)
             HybMesh%Next_Hex(ii(2),ii(1)) = IT
             TetFace(:,IFace) = 0
          ELSE
             WRITE(*,*)'Error--- : 3 elements share a face. IFace=',IFace
             WRITE(*,*)'It: ',IT, ii(:)
             WRITE(*,*)'IP: ',HybMesh%IP_Hex(:, IT)
             CALL Error_STOP ( '--- HybridMesh_BuildNext')
          ENDIF

       ENDDO
    ENDDO

    DEALLOCATE ( TetFace )
    CALL NodeNetTree_clear ( Face_Tree )

  END SUBROUTINE HybridMesh_BuildNext


  !*******************************************************************************
  !>
  !!    Check the array common_Parameters::HybMesh%Next_Tet.
  !!    @param[in] HybMesh  the hybrid mesh.
  !!    @param[out] NB_BD_Tri : the number of boundary.
  !<     
  !*******************************************************************************
  SUBROUTINE HybridMesh_CheckNext(HybMesh, NB_BD_Tri)
    IMPLICIT NONE

    TYPE(HybridMeshStorageType), INTENT(IN) :: HybMesh
    INTEGER, INTENT(OUT) :: NB_BD_Tri
    INTEGER :: it,i,i1,i2,ii,itnb,ip5(5), ip4(4), ip3(3)

    NB_BD_Tri = 0

    DO it=1,HybMesh%NB_Tet
       IF(HybMesh%IP_Tet(4,IT)<=0) CYCLE
       DO i=1,4
          itnb = HybMesh%Next_Tet(i,it)
          IF( itnb==0 .OR. itnb>HybMesh%NB_Tet .OR. itnb<=-2 )THEN
             WRITE(*,*)'Error--- : next is out of the range'
             WRITE(*,*)'IT=',IT,'IP=',HybMesh%IP_Tet(:,IT),' Next=',HybMesh%Next_Tet(:,it)
             CALL Error_STOP ( '--- HybridMesh_CheckNext:: range')
          ENDIF
          IF(itnb<=0)THEN
             NB_BD_Tri = NB_BD_Tri+1
             CYCLE
          ENDIF

          IF(HybMesh%IP_Tet(4,itnb)<=0)THEN
             WRITE(*,*)'Error--- Next to an invalid element'
             WRITE(*,*)'it,  ip,next=',it,HybMesh%IP_Tet(:,it),HybMesh%Next_Tet(:,it)
             WRITE(*,*)'itnb,ip =',itnb,HybMesh%IP_Tet(:,itnb)
             CALL Error_STOP ( '--- HybridMesh_CheckNext:: invalid element')
          ENDIF

          i2 = 0
          DO i1=1,4
             IF(HybMesh%Next_Tet(i1,itnb)==it) i2 = i1
          ENDDO

          IF(i2==0)THEN
             WRITE(*,*)'Error--- '
             WRITE(*,*)'it  ',it,  ' ip ',HybMesh%IP_Tet(:,it),  ' next ',HybMesh%Next_Tet(:,it)
             WRITE(*,*)'itnb',itnb,' ip ',HybMesh%IP_Tet(:,itnb),' next ',HybMesh%Next_Tet(:,itnb)
             CALL Error_STOP ( '--- HybridMesh_CheckNext')
          ENDIF

          ip5(1:3) = HybMesh%IP_Tet(iTri_Tet(1:3,i),IT)
          ip5(4:5) = ip5(1:2)

          ip3(1:3) = HybMesh%IP_Tet(iTri_Tet(1:3,i2),itnb)
          DO i1=0,2
             IF( ip5(i1+1)==ip3(2) .AND. ip5(i1+2)==ip3(1)    &
                  .AND. ip5(i1+3)==ip3(3) )THEN
                i2 = 0
                EXIT
             ENDIF
          ENDDO

          IF(i2>0)THEN
             WRITE(*,*)'Error--- HybridMesh_CheckNext: nodes are not matched'
             WRITE(*,*)'it,  ip,next=',it,HybMesh%IP_Tet(:,it),HybMesh%Next_Tet(:,it)
             WRITE(*,*)'itnb,ip,next=',itnb,HybMesh%IP_Tet(:,itnb),HybMesh%Next_Tet(:,itnb)
             CALL Error_STOP ( '--- HybridMesh_CheckNext')
          ENDIF
       ENDDO
    ENDDO

    DO it=1,HybMesh%NB_Hex
       IF(HybMesh%IP_Hex(8,IT)<=0) CYCLE
       DO i=1,6
          itnb = HybMesh%Next_Hex(i,it)
          IF( itnb==0 .OR. itnb>HybMesh%NB_Hex .OR. itnb<=-2 )THEN
             WRITE(*,*)'Error--- : next is out of the range'
             WRITE(*,*)'IT=',IT,'IP=',HybMesh%IP_Hex(:,IT),' Next=',HybMesh%Next_Hex(:,it)
             CALL Error_STOP ( '--- HybridMesh_CheckNext:: range')
          ENDIF
          IF(itnb<=0)THEN
             NB_BD_Tri = NB_BD_Tri+1
             CYCLE
          ENDIF

          IF(HybMesh%IP_Hex(8,itnb)<=0)THEN
             WRITE(*,*)'Error--- Next to an invalid element'
             WRITE(*,*)'it,  ip,next=',it,HybMesh%IP_Hex(:,it),HybMesh%Next_Hex(:,it)
             WRITE(*,*)'itnb,ip =',itnb,HybMesh%IP_Hex(:,itnb)
             CALL Error_STOP ( '--- HybridMesh_CheckNext:: invalid element')
          ENDIF

          i2 = 0
          DO i1=1,6
             IF(HybMesh%Next_Hex(i1,itnb)==it) i2 = i1
          ENDDO

          IF(i2==0)THEN
             WRITE(*,*)'Error--- '
             WRITE(*,*)'it  ',it,  ' ip ',HybMesh%IP_Hex(:,it),  ' next ',HybMesh%Next_Hex(:,it)
             WRITE(*,*)'itnb',itnb,' ip ',HybMesh%IP_Hex(:,itnb),' next ',HybMesh%Next_Hex(:,itnb)
             CALL Error_STOP ( '--- HybridMesh_CheckNext')
          ENDIF

          ip5(1:4) = HybMesh%IP_Hex(iQuadFD_Hex(1:4,i),IT)
          ip4(1:4) = HybMesh%IP_Hex(iQuadFD_Hex(1:4,i2),itnb)
          DO i1 = 1,4
             DO i2 = 1,4
                IF( ip5(i1)==ip4(i2) )THEN
                   ip5(i1) = 0
                   ip4(i2) = 0
                   EXIT
                ENDIF
             ENDDO
          ENDDO

          IF( MAX(ip5(1),ip5(2),ip5(3),ip5(4))>0 .OR.    &
               MAX(ip4(1),ip4(2),ip4(3),ip4(4))>0) THEN
             WRITE(*,*)'Error--- HybridMesh_CheckNext: nodes are not matched'
             WRITE(*,*)'it,  ip,next=',it,HybMesh%IP_Hex(:,it),HybMesh%Next_Hex(:,it)
             WRITE(*,*)'itnb,ip,next=',itnb,HybMesh%IP_Hex(:,itnb),HybMesh%Next_Hex(:,itnb)
             CALL Error_STOP ( '--- HybridMesh_CheckNext')
          ENDIF
       ENDDO
    ENDDO

    RETURN
  END SUBROUTINE HybridMesh_CheckNext

  !*******************************************************************************
  !>     
  !!    Build boundary triangle and quads
  !!    @param[in] HybMesh  : the hybrid mesh.
  !!    @param[out] Surf    : boundary surface
  !<     
  !*******************************************************************************
  SUBROUTINE HybridMesh_BuildBoundSurface(HybMesh, Surf)
    IMPLICIT NONE

    TYPE(HybridMeshStorageType),  INTENT(IN)    :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER :: IT, i


    CALL allc_2Dpointer(Surf%IP_Tri,  5, 4*HybMesh%NB_Tet, 'IP_BD_Tri')
    CALL allc_2Dpointer(Surf%IP_Quad, 5, 3*HybMesh%NB_Hex, 'IP_BD_Tri')

    Surf%NB_Tri = 0

    DO IT = 1,HybMesh%NB_Tet
       DO i=1,4
          IF(HybMesh%Next_Tet(i,IT)/=-1) CYCLE
          Surf%NB_Tri                  = Surf%NB_Tri + 1          
          Surf%IP_Tri(1:3,Surf%NB_Tri) = HybMesh%IP_Tet(iTri_Tet(1:3,i),IT)
          Surf%IP_Tri(4,  Surf%NB_Tri) = IT
          Surf%IP_Tri(5,  Surf%NB_Tri) = 1
       ENDDO
    ENDDO

    IF(Surf%NB_Tri>0)THEN
       Surf%NB_Surf = 1
    ELSE
       Surf%NB_Surf = 0
    ENDIF


    Surf%NB_Quad = 0

    DO IT = 1,HybMesh%NB_Hex
       DO i=1,6
          IF(HybMesh%Next_Hex(i,IT)==-1) CYCLE
          Surf%NB_Quad                   = Surf%NB_Quad + 1
          Surf%IP_Quad(1:4,Surf%NB_Quad) = HybMesh%IP_Hex(iQuadFD_Hex(1:4,i),IT)
          Surf%IP_Quad(5,  Surf%NB_Quad) = IT
       ENDDO
    ENDDO

  END SUBROUTINE HybridMesh_BuildBoundSurface

  !>
  !!    Associate boundary triangles with elements.     
  !!       For pyramid elements, the triangles associated a number equal to 
  !!               the cell id plus HybMesh%NB_Tet.
  !!       For prism elements, the triangles associated a number equal to 
  !!               the cell id plus HybMesh%NB_Tet + HybMesh%NB_Pyr.
  !!
  !!   The IP_Tri(1:3,IT) & IPsp_Tri(:,IT) will be rotated by this subroutine.
  !<     
  SUBROUTINE HybridMesh_AssoBoundSurface(HybMesh, Surf)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType),  INTENT(IN)    :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    TYPE(NodeNetTreeType) :: TriTree
    INTEGER :: IT, IB, NB, i, ipp(3)
    INTEGER :: NB_PyrAT, NB_PrmAT

    NB_PyrAT = HybMesh%NB_Tet
    NB_PrmAT = HybMesh%NB_Tet + HybMesh%NB_Pyr

    !--- build a triangulation system

    CALL NodeNetTree_allocate(3, HybMesh%NB_Point, Surf%NB_Tri, TriTree)
    Surf%IP_Tri(4,:) = 0

    Surf%NB_Point = 0
    DO IB = 1,Surf%NB_Tri
       ipp(1:3) = Surf%IP_Tri(1:3,IB)
       CALL NodeNetTree_SearchAdd(3,ipp, TriTree, NB)
       Surf%NB_Point = MAX(Surf%NB_Point, ipp(1), ipp(2), ipp(3))
       !--- mark interface triangle
       IF(Surf%IP_Tri(5,IB) > Surf%NB_Surf - Surf%NB_Sheet) Surf%IP_Tri(4,IB) = -1
    ENDDO
    IF(NB/=Surf%NB_Tri)THEN
       WRITE(*,*)' Error--- NB/=Surf%NB_Tri :', NB, Surf%NB_Tri
       CALL Error_Stop ('HybridMesh_AssoBoundSurface :: NB/=Surf%NB_Tri ')
    ENDIF

    !--- Search tet. element for boundary triangles

    DO IT = 1,HybMesh%NB_Tet
       DO i=1,4
          ipp(1:3) = HybMesh%IP_Tet(iTri_Tet(1:3,i),IT)
          IF(MAXVAL(ipp)>Surf%NB_Point) CYCLE

          CALL NodeNetTree_search(3,ipp,TriTree,IB)
          IF(IB<=0) CYCLE           

          IF(which_RotationinTri(Surf%IP_Tri(1:3,IB), ipp(1:3))>0 .AND. Surf%IP_Tri(4,IB)<=0)THEN
             !--- point into the tetrahedron
             Surf%IP_Tri(4,IB) = IT
          ELSE
             !--- point outside of the tetrahedron
             WRITE(*,*)' Error--- An element outside the domain, IT=',IT
             WRITE(*,*)'    HybMesh%IP_Tet=',HybMesh%IP_Tet(:,IT)
             WRITE(*,*)'    IB=',IB,'Surf%IP_Tri=',Surf%IP_Tri(:,IB)
             CALL Error_Stop ('HybridMesh_AssoBoundSurface ::  ')
          ENDIF
       ENDDO
    ENDDO

    !--- Search pyr. element for boundary triangles

    DO IT = 1,HybMesh%NB_Pyr
       DO i=1,4
          ipp(1:3) = HybMesh%IP_Pyr(iTri_Pyr(1:3,i),IT)
          IF(MAXVAL(ipp)>Surf%NB_Point) CYCLE

          CALL NodeNetTree_search(3,ipp,TriTree,IB)
          IF(IB<=0) CYCLE           

          IF(which_RotationinTri(Surf%IP_Tri(1:3,IB), ipp(1:3))>0 .AND. Surf%IP_Tri(4,IB)<=0)THEN
             !--- point into the tetrahedron
             Surf%IP_Tri(4,IB) = IT + NB_PyrAT
          ELSE
             !--- point outside of the tetrahedron
             WRITE(*,*)' Error--- An element outside the domain, IT=',IT
             WRITE(*,*)'    HybMesh%IP_Pyr=',HybMesh%IP_Pyr(:,IT)
             WRITE(*,*)'    IB=',IB,'Surf%IP_Tri=',Surf%IP_Tri(:,IB)
             CALL Error_Stop ('HybridMesh_AssoBoundSurface ::  ')
          ENDIF
       ENDDO
    ENDDO

    !--- Search prm. element for boundary triangles

    DO IT = 1,HybMesh%NB_Prm
       DO i=1,2
          ipp(1:3) = HybMesh%IP_Prm(iTri_Prm(1:3,i),IT)
          IF(MAXVAL(ipp)>Surf%NB_Point) CYCLE

          CALL NodeNetTree_search(3,ipp,TriTree,IB)
          IF(IB<=0) CYCLE           

          IF(which_RotationinTri(Surf%IP_Tri(1:3,IB), ipp(1:3))>0 .AND. Surf%IP_Tri(4,IB)<=0)THEN
             !--- point into the tetrahedron
             Surf%IP_Tri(4,IB) = IT + NB_PrmAT
          ELSE
             !--- point outside of the tetrahedron
             WRITE(*,*)' Error--- An element outside the domain, IT=',IT
             WRITE(*,*)'    HybMesh%IP_Prm=',HybMesh%IP_Prm(:,IT)
             WRITE(*,*)'    IB=',IB,'Surf%IP_Tri=',Surf%IP_Tri(:,IB)
             CALL Error_Stop ('HybridMesh_AssoBoundSurface ::  ')
          ENDIF
       ENDDO
    ENDDO

    CALL NodeNetTree_Clear(TriTree)

    RETURN

  END SUBROUTINE HybridMesh_AssoBoundSurface

  !>
  !!  Remove those tet-cell with mark(it) <= 0
  !!  @param[in,out]  HybMesh  the mesh.
  !!  @param[in]  Mark      (it) = 0, cell being removed.
  !!                         else, kept.
  !!  @param[out]  Mark     (it) = 0, cell been removed.
  !!                             = newit>0, the new id of the triangle kept.
  !!  @param[out]  new_to_old (ipnew) = ipold, 
  !!     then the node ipnew in the new mesh is the point ipold in the old mesh.
  !!
  !<
  SUBROUTINE HybridMesh_removeFake(HybMesh, Mark, new_to_old)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType),  INTENT(INOUT)    :: HybMesh
    INTEGER, INTENT(INOUT)    :: Mark(*)
    INTEGER, INTENT(OUT), optional :: new_to_old(*)
    INTEGER :: IT, IP,  ip4(4), np, nt
    INTEGER, DIMENSION(:),   POINTER :: markP

    ALLOCATE(markP(0:HybMesh%NB_Point))
    markP = 0

    DO it=1,HybMesh%NB_Tet    
       IF(Mark(it)>0) THEN
          WHERE(HybMesh%IP_Tet(:,it)>0) markP(HybMesh%IP_Tet(:,it)) = 1
       ENDIF
    ENDDO

    np = 0
    DO ip = 1, HybMesh%NB_Point
       IF(markP(ip)<=0) CYCLE
          np = np + 1
          HybMesh%Posit(:,np) = HybMesh%Posit(:,ip)
          IF( HybMesh%NB_Value>0)THEN
             HybMesh%V_Point(:,np) = HybMesh%V_Point(:,ip)
          ENDIF
          markP(ip) = np
          if(present(new_to_old))then
             new_to_old(np) = ip
          endif
    ENDDO
    HybMesh%NB_Point = np

    nt = 0
    DO it = 1,HybMesh%NB_Tet
       IF(Mark(it)<=0) CYCLE
       nt = nt+1
       Mark(it) = nt
       HybMesh%IP_Tet(:, nt) = markP(HybMesh%IP_Tet(:, IT))
    ENDDO
    HybMesh%NB_Tet = nt

    DEALLOCATE(markP)
    RETURN


  END SUBROUTINE HybridMesh_removeFake

  !>
  !!  Output a cut-plane. 
  !!  @param[in]  HybMesh  the mesh.
  !!  @param[in]  P0   (3,N)  the point located on each cut face.
  !!  @param[in]  aNor (3,N)  the normal direction of each cut face.
  !!  @param[out] Surf     the surface mesh for the cuts.
  !<
  SUBROUTINE HybridMesh_BuildCutSurfaceRough(HybMesh, P0, aNor, Surf)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType),  INTENT(INOUT)    :: HybMesh
    REAL*8,  INTENT(IN)    :: P0(3), aNor(3)
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    REAL*8  :: pt(3)
    INTEGER :: IT,  ip4(4)
    INTEGER, DIMENSION(:),   ALLOCATABLE :: Mark
    TYPE(Plane3D) :: aPlane

    ALLOCATE(Mark(HybMesh%NB_Tet))
    aPlane = Plane3D_Build2(P0(:),aNor(:)) 

    DO it=1,HybMesh%NB_Tet
       ip4 = HybMesh%IP_Tet(1:4, IT)       
       pt = ( HybMesh%Posit(:,ip4(1)) + HybMesh%Posit(:,ip4(2)) +    &
            HybMesh%Posit(:,ip4(3)) + HybMesh%Posit(:,ip4(4)) ) / 4
       IF(Plane3D_SignedDistancePointToPlane(pt,aPlane)>0)THEN
          Mark(it) = 1
       ELSE
          Mark(it) = 0
       ENDIF
    ENDDO

    CALL HybridMesh_removeFake(HybMesh, Mark) 

    CALL HybridMesh_BuildNext(HybMesh)
    CALL HybridMesh_BuildBoundSurface(HybMesh, Surf)

    Surf%NB_Point = HybMesh%NB_Point
    ALLOCATE (Surf%Posit(3,Surf%NB_Point))
    Surf%Posit(:,1:Surf%NB_Point) = HybMesh%Posit(:,1:Surf%NB_Point) 
    Surf%NB_Value = HybMesh%NB_Value
    IF(Surf%NB_Value>0)THEN
       ALLOCATE(Surf%V_Point(Surf%NB_Value, HybMesh%NB_Point))
       Surf%V_Point(1:Surf%NB_Value, 1:Surf%NB_Point) =    &
            HybMesh%V_Point(1:Surf%NB_Value, 1:Surf%NB_Point) 
    ENDIF

    DEALLOCATE(mark)

    RETURN

  END SUBROUTINE HybridMesh_BuildCutSurfaceRough

  !>
  !!  Output a cut-plane. 
  !!  @param[in]  HybMesh   the mesh.
  !!  @param[in]  P0   (3)  the point located on each cut face.
  !!  @param[in]  aNor (3)  the normal direction of each cut face.
  !!  @param[out] Surf      the surface mesh for the cuts.
  !<
  SUBROUTINE HybridMesh_BuildCutSurfaceFine(HybMesh, P0, aNor, Surf)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType),  INTENT(IN)    :: HybMesh
    REAL*8,  INTENT(IN)    :: P0(3), aNor(3)
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER :: IT, i,  j, ip2(2), IED, is(6), k, ipn(6)
    REAL*8  :: TinySh, pMin(3), pMax(3), d, x1, x2
    REAL*8, DIMENSION(:),   ALLOCATABLE :: sig    
    REAL*8, DIMENSION(:,:),   ALLOCATABLE :: IPnew_Tet

    TYPE(Plane3D) :: aPlane
    TYPE(NodeNetTreeType) :: EdgeTree

    IF(HybMesh%GridOrder>1)THEN
       WRITE(*,*)' Warning---- only the linear mesh is taken.'
    ENDIF
    IF(HybMesh%NB_Pyr>0 .OR. HybMesh%NB_Prm>0 .OR. HybMesh%NB_Hex>0)THEN
       WRITE(*,*)' Warning---- only the tetrahedral mesh is taken.'
    ENDIF

    !--- check and modify nodes

    ALLOCATE(sig(HybMesh%NB_Point))
    CALL HybridMeshStorage_Measure (HybMesh, pMin, pMax)
    TinySh = 1.d-10 * Geo3D_Distance(pMin, pMax)
    aPlane = Plane3D_Build2(P0, aNor) 

    DO i=1,HybMesh%NB_Point
       d = Plane3D_SignedDistancePointToPlane(HybMesh%Posit(:,i),aPlane)
       IF(ABS(d)<TinySh)THEN
          IF(d>=0)THEN
             d = TinySh
          ELSE
             d = -TinySh
          ENDIF
       ENDIF
       sig(i) = d
    ENDDO

    !--- find crossing point on edges

    CALL SurfaceMeshStorage_Clear(Surf)

    Surf%NB_Point = 0
    ALLOCATE(Surf%Posit(3, 4*HybMesh%NB_Tet))
    ALLOCATE(IPnew_Tet(6,HybMesh%NB_Tet))
    Surf%NB_Value = HybMesh%NB_Value
    IF(Surf%NB_Value>0)THEN
       ALLOCATE(Surf%V_Point(Surf%NB_Value, 4*HybMesh%NB_Tet))
    ENDIF
    CALL NodeNetTree_allocate(2, HybMesh%NB_Point, 4*HybMesh%NB_Tet,  EdgeTree)

    DO IT = 1,HybMesh%NB_Tet 
       DO j = 1,6
          ip2(1:2) = HybMesh%IP_Tet(I_Comb_Tet(1:2,j), IT) 
          IPnew_Tet(j,IT) = 0
          IF(MINVAL(sig(ip2))>0 .OR. MAXVAL(sig(ip2))<0) CYCLE

          CALL NodeNetTree_SearchAdd( 2,ip2,EdgeTree,IEd)
          IPnew_Tet(j,IT) = IEd
          IF(IEd>Surf%NB_Point)THEN
             Surf%NB_Point = Surf%NB_Point+1
             x1 = ABS(sig(ip2(2))) / ( ABS(sig(ip2(2))) + ABS(sig(ip2(1)))  )  
             x2 = ABS(sig(ip2(1))) / ( ABS(sig(ip2(2))) + ABS(sig(ip2(1)))  ) 
             Surf%Posit(:,IED) = HybMesh%Posit(:,ip2(1))*x1 + HybMesh%Posit(:,ip2(2))*x2 
             IF(Surf%NB_Value>0)THEN
                Surf%V_Point(1:Surf%NB_Value, IED) =  HybMesh%V_Point(1:Surf%NB_Value,ip2(1))*x1 +     &
                     HybMesh%V_Point(1:Surf%NB_Value,ip2(2))*x2 
             ENDIF
          ENDIF
       ENDDO
    ENDDO

    !--- build triangles

    Surf%NB_Tri = 0
    ALLOCATE(Surf%IP_Tri(5, 4*HybMesh%NB_Tet))
    DO IT = 1,HybMesh%NB_Tet 
       k = 0
       DO j = 1,6
          IF(IPnew_Tet(j,IT)>0)THEN
             k = k+1
             is(k) = j
             ipn(j) = IPnew_Tet(j,IT)
          ENDIF
       ENDDO
       IF(k==0)THEN
          CYCLE
       ELSE IF(k==3)THEN
          Surf%NB_Tri = Surf%NB_Tri+1
          Surf%IP_Tri(1:3, Surf%NB_Tri) = IPnew_Tet(is(1:3), IT)
       ELSE IF(k==4)THEN
          IF(IPnew_Tet(1,IT)==0 .AND. IPnew_Tet(6,IT)==0)THEN
             Surf%NB_Tri = Surf%NB_Tri+1
             Surf%IP_Tri(1:3, Surf%NB_Tri) = (/ipn(2), ipn(3), ipn(5)/)
             Surf%NB_Tri = Surf%NB_Tri+1
             Surf%IP_Tri(1:3, Surf%NB_Tri) = (/ipn(3), ipn(4), ipn(5)/)              
          ELSE IF(IPnew_Tet(2,IT)==0 .AND. IPnew_Tet(4,IT)==0)THEN
             Surf%NB_Tri = Surf%NB_Tri+1
             Surf%IP_Tri(1:3, Surf%NB_Tri) = (/ipn(1), ipn(3), ipn(5)/)
             Surf%NB_Tri = Surf%NB_Tri+1
             Surf%IP_Tri(1:3, Surf%NB_Tri) = (/ipn(3), ipn(6), ipn(5)/)   
          ELSE IF(IPnew_Tet(3,IT)==0 .AND. IPnew_Tet(5,IT)==0)THEN
             Surf%NB_Tri = Surf%NB_Tri+1
             Surf%IP_Tri(1:3, Surf%NB_Tri) = (/ipn(1), ipn(2), ipn(4)/)
             Surf%NB_Tri = Surf%NB_Tri+1
             Surf%IP_Tri(1:3, Surf%NB_Tri) = (/ipn(2), ipn(6), ipn(4)/)  
          ELSE
             STOP 'dwefdmni0439gugreo'
          ENDIF
       ELSE
          STOP 'dskjfiwepfjqwopfd'
       ENDIF
    ENDDO

    Surf%NB_Surf = 1
    Surf%IP_Tri(4, :) = 0
    Surf%IP_Tri(5, :) = 1

    IF(Surf%NB_Tri>0)THEN
       CALL Surf_Reverse(Surf, 1)
    ENDIF

    DEALLOCATE (sig, IPnew_Tet)
    CALL NodeNetTree_Clear(EdgeTree)

    RETURN

  END SUBROUTINE HybridMesh_BuildCutSurfaceFine
  
  
  !>
  !!  Create a crack (sheet). 
  !!  If there exists an inplant, the crack does NOT pass the inplant.
  !!  @param[in]  HybMesh  the mesh.
  !!  @param[in]  P0   (3,N)  the point located on each cut face.
  !!  @param[in]  aNor (3,N)  the normal direction of each cut face.
  !!  @param[out] Surf     the surface mesh with the crack.
  !<
  SUBROUTINE HybridMesh_Buildcrack(HybMesh, P0, aNor, Surf)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType),  INTENT(INOUT)    :: HybMesh
    REAL*8,  INTENT(IN)    :: P0(3), aNor(3)
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    REAL*8  :: pt(3), p2d(2), xmin
    INTEGER :: IT,  ip4(4), ip3(3), ip, i, nbold, itst
    INTEGER, DIMENSION(:),   ALLOCATABLE :: Mark, MarkP
    TYPE(IntQueueType) :: Cells, nodes
    TYPE(Plane3D) :: aPlane

    ALLOCATE(Mark(HybMesh%NB_Tet))
    Mark = 0
    aPlane = Plane3D_Build2(P0(:),aNor(:)) 
    CALL Plane3D_buildTangentAxes (aPlane)
    ALLOCATE(MarkP(HybMesh%NB_Point))
    
    DO ip=1,HybMesh%NB_Point    
       pt = HybMesh%Posit(:,ip) 
       IF(Plane3D_SignedDistancePointToPlane(pt,aPlane)>0)THEN
          MarkP(ip) = 1
       ELSE
          Markp(ip) = -1
       ENDIF
    ENDDO
    
    xmin = 1.e36
    itst = 0
    DO it=1,HybMesh%NB_Tet
       ip4 = HybMesh%IP_Tet(1:4, IT)  
       if( MarkP(ip4(1))>0 .and. MarkP(ip4(2))>0 .and.  &
           MarkP(ip4(3))>0 .and. MarkP(ip4(4))>0 ) cycle
       if( MarkP(ip4(1))<0 .and. MarkP(ip4(2))<0 .and.  &
           MarkP(ip4(3))<0 .and. MarkP(ip4(4))<0 ) cycle
       pt = ( HybMesh%Posit(:,ip4(1)) + HybMesh%Posit(:,ip4(2)) +    &
            HybMesh%Posit(:,ip4(3)) + HybMesh%Posit(:,ip4(4)) ) / 4
       p2d = Plane3D_project2D(aPlane,Pt)
       if(xmin>p2d(1))then
          xmin = p2d(1)
          itst = it
       endif
    ENDDO
    
    call HybridMesh_TetPropegate(HybMesh,Surf, itst, Cells, nodes)  
    mark = 2
    mark(Cells%Nodes(1:Cells%NumNodes)) = 0    
    DEALLOCATE(Markp)
    CALL IntQueue_Clear(Cells)
    CALL IntQueue_Clear(nodes)

    DO it=1,HybMesh%NB_Tet
       if(mark(it)/=0) cycle
       ip4 = HybMesh%IP_Tet(1:4, IT)       
       pt = ( HybMesh%Posit(:,ip4(1)) + HybMesh%Posit(:,ip4(2)) +    &
            HybMesh%Posit(:,ip4(3)) + HybMesh%Posit(:,ip4(4)) ) / 4
       IF(Plane3D_SignedDistancePointToPlane(pt,aPlane)>0)THEN
          Mark(it) = 1
       ELSE
          Mark(it) = -1
       ENDIF
    ENDDO

    
    CALL HybridMesh_BuildNext(HybMesh)
    nbold = Surf%NB_Tri
    DO it=1,HybMesh%NB_Tet
       if(mark(it)/=1) cycle
       do i = 1,4
          itst = HybMesh%Next_Tet(i, it)
          if(mark(itst)/=-1) cycle
          ip3 = HybMesh%IP_Tet(iTri_Tet(:,i),it)
          Surf%NB_Tri = Surf%NB_Tri + 1
          if(Surf%NB_Tri>keyLength(Surf%IP_Tri))then
             CALL allc_2Dpointer(Surf%IP_Tri,  5, Surf%NB_Tri+10000,  'HybridMesh_Buildcrack')
          endif
          Surf%IP_Tri(1:3, Surf%NB_Tri) = ip3
          Surf%IP_Tri(4:5, Surf%NB_Tri) = (/ it, Surf%NB_Surf+1 /)
       enddo
    ENDDO
    
    IF(Surf%NB_Tri>nbold)THEN
       Surf%NB_Surf  = Surf%NB_Surf + 1 
       Surf%NB_Sheet = Surf%NB_Sheet + 1
    ENDIF

    DEALLOCATE(mark)

    RETURN
  END SUBROUTINE HybridMesh_Buildcrack



  !>
  !!  Split all not-tetrahedral elements into tetrahedra,
  !!  transfer a hybrid mesh to a tetrahedral mesh. 
  !!  @param[in,out] HybMesh  the hybrid mesh.
  !!  @param[in,out] Surf  the triangular surface mesh represent the boundary.
  !!
  !!  Reminder: if there is negative connectivities appearing in the mesh, 
  !!            it will be converted to its absolute value after splitting.
  !<
  SUBROUTINE HybridMesh_Splitting(HybMesh,Surf)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(INOUT), OPTIONAL :: Surf
    TYPE(NodeNetTreeType) :: NetTree

    INTEGER :: it,i,ip(8),netot,IB, ip2(2), netID, id1, id2
    INTEGER :: neg

    CALL NodeNetTree_Allocate(2, HybMesh%NB_Point, 4*HybMesh%NB_Hex, NetTree)

    netot = 6*HybMesh%NB_Hex + 2*HybMesh%NB_Pyr + HybMesh%NB_Tet
    IF(HybMesh%NB_Prm>0) CALL Error_STOP ( '---not ready')
    CALL allc_2Dpointer(HybMesh%IP_Tet, 4, netot,   'IP_Tet') 

    neg = 0
    DO it=1,HybMesh%NB_Hex
       ip(1:8) = HybMesh%IP_Hex(:,it)
       if(minval(ip)<0)then
          ip = abs(ip)
          neg = 1
       endif
       CALL Wedge_spliting(ip,  HybMesh%NB_Tet, HybMesh%IP_Tet)
       DO i=1,6
          ip2(1:2) = (/ip(iQuad_Hex(2,i)),ip(iQuad_Hex(4,i))/)
          IF(ip2(1)>0 .AND. ip2(2)>0)THEN
             CALL NodeNetTree_SearchAdd(2,ip2,NetTree,netID)
          ENDIF
       ENDDO
    ENDDO

    HybMesh%NB_Hex = 0

    DO it=1,HybMesh%NB_Pyr
       ip(1:5) = HybMesh%IP_Pyr(:,it)
       if(minval(ip(1:5))<0)then
          ip(1:5) = abs(ip(1:5))
          if(neg<2) neg = neg + 2
       endif
       ip2(1:2) = (/ip(2),ip(4)/)
       CALL NodeNetTree_search(2,ip2,NetTree,ID1)
       ip2(1:2) = (/ip(1),ip(3)/)
       CALL NodeNetTree_search(2,ip2,NetTree,ID2)
       IF(ID1>0 .AND. ID2==0)THEN
          HybMesh%NB_Tet = HybMesh%NB_Tet+1
          HybMesh%IP_Tet(:,HybMesh%NB_Tet) = (/ip(1),ip(2),ip(4),ip(5)/)
          HybMesh%NB_Tet = HybMesh%NB_Tet+1
          HybMesh%IP_Tet(:,HybMesh%NB_Tet) = (/ip(2),ip(3),ip(4),ip(5)/)
       ELSE IF(ID2>0 .AND. ID1==0)THEN
          WRITE(*,*)' WARNING: HybridMesh_Splitting----never try before, check please------2'
          HybMesh%NB_Tet = HybMesh%NB_Tet+1
          HybMesh%IP_Tet(:,HybMesh%NB_Tet) = (/ip(1),ip(2),ip(3),ip(5)/)
          HybMesh%NB_Tet = HybMesh%NB_Tet+1
          HybMesh%IP_Tet(:,HybMesh%NB_Tet) = (/ip(1),ip(3),ip(4),ip(5)/)
       ELSE
          CALL Error_STOP ( 'HybridMesh_Splitting   1')
       ENDIF
    ENDDO

    HybMesh%NB_Pyr = 0
    
    DO it = 1, netot
       ip(1:4) = HybMesh%IP_Tet(:,it)
       if(minval(ip(1:4))<0)then
          HybMesh%IP_Tet(:,it) = abs(ip(1:4))
          if(neg<4) neg = neg + 4
       endif
    ENDDO
    
    IF(neg>0)THEN
       !--- found negative connectivities, give a warning
       WRITE(*,*)' Warning---- there are some negative connectivities in the '
       WRITE(*,'(a,$)')'              '
       if(neg==1 .or. neg==3 .or. neg==5 .or. neg==7)then
           WRITE(*,'(a,$)')'Hex '
           if(neg>1) WRITE(*,'(a,$)')'and '
       endif
       if(neg==2 .or. neg==3 .or. neg==6 .or. neg==7)then
           WRITE(*,'(a,$)')'Pyr '
           if(neg>2) WRITE(*,'(a,$)')'and '
       endif
       if(neg>=4)then
           WRITE(*,'(a,$)')'Tet '
       endif
       WRITE(*,'(a)')'domain of the original mesh,'
       WRITE(*,*)'               which are replaced by its absoluted value after spliting.'      
    ENDIF

    IF(HybMesh%NB_Tet>netot) CALL Error_STOP ( '--- HybridMesh_Splitting')

    IF(PRESENT(Surf))THEN
       CALL HybridMesh_BuildNext(HybMesh)
       CALL HybridMesh_BuildBoundSurface(HybMesh, Surf)
    ENDIF

    CALL NodeNetTree_Clear(NetTree)

  END SUBROUTINE HybridMesh_Splitting

  !>
  !!  Sort nodes ot elements (tetrahedra only). 
  !!  @param[in,out] HybMesh  the hybrid mesh.
  !!  @param[in,out] Surf     the surface mesh represent the boundary.
  !<
  SUBROUTINE HybridMesh_Resort(HybMesh,Surf,SortWhat,byWhat)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT)  :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(IN), OPTIONAL :: SortWhat, byWhat
    INTEGER, DIMENSION(:),   POINTER :: indx, oldRank
    INTEGER, DIMENSION(:,:), POINTER :: IP_Save
    REAL*8,  DIMENSION(:),   POINTER :: arr
    REAL*8,  DIMENSION(:,:), POINTER :: Posit
    INTEGER :: SortWhat1, byWhat1
    INTEGER :: ips(4), ip, it, ib
    REAL*8  :: v(4)

    IF(PRESENT(byWhat))THEN
       SortWhat1 = SortWhat
       byWhat1   = byWhat
    ELSE
       WRITE(*,*)'   What you want to sort:'
       WRITE(*,*)'     1, --- sort nodes.'
       WRITE(*,*)'     2, --- sort elements.'
       WRITE(*,*)'     3, --- sort both nodes and elements.'
       READ(6,*) SortWhat1
       WRITE(*,*)'   By what you sort them:'
       WRITE(*,*)'     1, --- by coordinate x.'
       WRITE(*,*)'     2, --- by coordinate y.'
       WRITE(*,*)'     3, --- by coordinate z.'
       READ(6,*) byWhat1
    ENDIF

    IF(SortWhat1==1 .OR. SortWhat1==3)THEN
       ALLOCATE(arr(      HybMesh%NB_Point))
       ALLOCATE(indx(     HybMesh%NB_Point))
       ALLOCATE(oldRank(  HybMesh%NB_Point))
       ALLOCATE(Posit(3,  HybMesh%NB_Point))

       DO ip = 1, HybMesh%NB_Point
          IF(byWhat1<=3)THEN 
             arr(ip) = HybMesh%Posit(byWhat1, ip)
          ENDIF
       ENDDO

       CALL quicksort (HybMesh%NB_Point, arr, indx)  

       Posit(1:3,1:HybMesh%NB_Point) = HybMesh%Posit(1:3, 1:HybMesh%NB_Point) 
       oldRank(1:HybMesh%NB_Point) = 0

       DO ip = 1, HybMesh%NB_Point
          oldRank(indx(ip)) = ip
          HybMesh%Posit(:,ip) = Posit(:,indx(ip))
       ENDDO
       DO it = 1, HybMesh%NB_Tet
          HybMesh%IP_Tet(1:4, it) = oldRank(HybMesh%IP_Tet(1:4, it))
       ENDDO

       DO ib = 1, Surf%NB_Tri
          Surf%IP_Tri(1:3,ib) = oldRank(Surf%IP_Tri(1:3,ib))
       ENDDO

       DEALLOCATE(arr, indx, oldRank, Posit)
    ENDIF

    IF(SortWhat1==2 .OR. SortWhat1==3)THEN
       ALLOCATE(arr(      HybMesh%NB_Tet))
       ALLOCATE(indx(     HybMesh%NB_Tet))
       ALLOCATE(oldRank(  HybMesh%NB_Tet))
       ALLOCATE(ip_save(4,HybMesh%NB_Tet))

       DO it = 1, HybMesh%NB_Tet
          ips(1:4) = HybMesh%IP_Tet(:,it)
          IF(byWhat1<=3)THEN 
             v(1:4) = HybMesh%Posit(byWhat1,ips(1:4))
             arr(it) = 0.25d0 * SUM(v(1:4))
          ENDIF
       ENDDO

       CALL quicksort (HybMesh%NB_Tet, arr, indx)  

       ip_save(1:4,1:HybMesh%NB_Tet) = HybMesh%IP_Tet(1:4, 1:HybMesh%NB_Tet) 
       oldRank(1:HybMesh%NB_Tet) = 0

       DO it = 1, HybMesh%NB_Tet
          oldRank(indx(it)) = it   
       ENDDO
       DO it = 1, HybMesh%NB_Tet
          HybMesh%IP_Tet(1:4, it) = ip_save(1:4,indx(it))
       ENDDO

       DO ib = 1, Surf%NB_Tri
          it = Surf%IP_Tri(4,ib)
          IF(it==0) CYCLE
          IF(oldRank(it)==0) CALL Error_STOP ( '--- HybridMesh_Resort:: oldRank')
          Surf%IP_Tri(4,ib) = oldRank(it)
       ENDDO

       DEALLOCATE(arr, indx, oldRank, ip_save)
    ENDIF

    RETURN
  END  SUBROUTINE HybridMesh_Resort

  !>
  !!   Extra part of mesh from a voulme mesh.
  !!   if new_to_old(inew) = iold, 
  !!     then the node inew in the new mesh is the point iold in the old mesh.
  !<
  SUBROUTINE HybridMesh_Extract(HybMesh, Surf, new_to_old)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT)  :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(OUT) :: new_to_old(*)
    INTEGER        ::  subjob, Isucc
    CHARACTER*2560 ::  HelpMessage      
    INTEGER :: nt, np, nb, nas, ip, it, i, ib, n, nlay
    REAL*8  :: pMax(3), pMin(3), Xmax,Xmin,Ymax,Ymin,Zmax,Zmin,r, p0(3)
    REAL*8  :: v, vlower, vhigher
    INTEGER, DIMENSION(:), POINTER :: Marke,Markp,ReadList
    TYPE(IntQueueType) :: Cells, BoundaryNodes

    CALL HybridMeshStorage_Measure (HybMesh, pMin, pMax)

    WRITE(*,*)
    WRITE(*,'(a,2(1x,E13.6))')'  Mesh range, Xmin Xmax: ',pMin(1), pMax(1)
    WRITE(*,'(a,2(1x,E13.6))')'  Mesh range, Ymin Ymax: ',pMin(2), pMax(2)
    WRITE(*,'(a,2(1x,E13.6))')'  Mesh range, Zmin Zmax: ',pMin(3), pMax(3)
    WRITE(*,*)

    WRITE(*,*)'   SUB_JOB TYPE:'
    WRITE(*,*)'     1, --- extract the mesh by REMOVING given nodes'
    WRITE(*,*)'     2, --- extract the mesh by a cubic domain'
    WRITE(*,*)'     3, --- '
    WRITE(*,*)'     4, --- extract the mesh by a spherical domain'
    WRITE(*,*)'     5, --- extract the mesh surrounding given nodes'
    WRITE(*,*)'     6, --- extract the mesh by given elements'
    WRITE(*,*)'     7, --- extract the mesh for some layers from given surfaces'
    WRITE(*,*)'     8, --- extract the mesh by value bounds'
    WRITE(*,*)'     9, --- extract the mesh by material region'
    HelpMessage =                                                                &
         '  Option 1, you need to give a series of nodes '                   //  &
         'which you wants to DELETTED from the whole mesh; \n'               //  &
         '  Option 2, you need to give a range of cubic domain, and  '       //  &
         'those nodes lie in the domain will be saved; \n'                   //  &
         '  Option 4, you need to give a central position and a radius,  '   //  &
         'and those surfaces lie in the spherical domain will be saved; \n'  //  &
         '  Option 5, you need to give a series of nodes, and all '          //  &
         'elements associating these nodes will be saved. \n'                //  &
         '  Option 6, you need to give a series of elements to save. \n'     //  &
         '  Option 7, you need to give a number of layers and a series of surfaces. \n'   //&
         '  Option 8, you need to give a bounds of values. \n'               //  &     
         '  Option 9, you need to give a element as the seed. '

    CALL Read_Int_withHelp(subjob, 'Input the sub_job : ', HelpMessage, Isucc)
    IF(Isucc==0 .OR. subjob<1 .OR. subjob>9 .OR. subjob==3)THEN
       WRITE(*,*)' Warning--- invalid input, return'
       RETURN
    ENDIF

    ALLOCATE(markp(0:HybMesh%NB_Point), marke(HybMesh%NB_Tet), ReadList(HybMesh%NB_Tet))

    IF(subjob==1)THEN

       ReadList(:) = 0
       WRITE(*,'((a))')'Input the nodes to be removed'
       CALL read_int_array(ReadList,nas)
       markp(:) = 0
       DO nb=1,nas
          ip = ReadList(nb)
          IF(ip>0 .AND. ip<=HybMesh%NB_Point)THEN
             markp(ip) = -1
          ENDIF
       ENDDO

    ELSE IF(subjob==2)THEN

       WRITE(*,*)'Input a cubic range (xmin,xmax,ymin,ymax,zmin,zmax):'
       READ(6,*)Xmin,Xmax,Ymin,Ymax,Zmin,Zmax

    ELSE IF(subjob==4)THEN

       WRITE(*,*)'Input a central position (x0,y0,z0):'
       READ(6,*)p0
       WRITE(*,*)'Input a radius :'
       READ(6,*)R
       Xmin = P0(1)-R
       Xmax = p0(1)+R
       Ymin = P0(2)-R
       Ymax = p0(2)+R
       Zmin = P0(3)-R
       Zmax = p0(3)+R
       R = R*R

    ELSE IF(subjob==5)THEN

       ReadList(:) = 0
       WRITE(*,'((a))')'Input the nodes to be saved'
       CALL read_int_array(ReadList,nas)
       markp(:) = 0
       DO nb=1,nas
          ip = ReadList(nb)
          IF(ip>0 .AND. ip<=HybMesh%NB_Point)THEN
             markp(ip) = 1
          ENDIF
       ENDDO
       marke(:) = 0
       DO it =1,HybMesh%NB_Tet
          DO i = 1, Npoint_Tet(HybMesh%GridOrder)
             ip = HybMesh%IP_Tet(i,it)
             IF(ip==0) CYCLE
             IF(markp(ip)==1)THEN
                marke(it) = 1
                EXIT
             ENDIF
          ENDDO
       ENDDO

    ELSE IF(subjob==6)THEN

       ReadList(:) = 0
       WRITE(*,'((a))')'Input the elements to be saved'
       CALL read_int_array(ReadList,nas)
       marke(:) = 0
       DO nb=1,nas
          it = ReadList(nb)
          IF(it>0 .AND. it<=HybMesh%NB_Tet)THEN
             marke(it) = 1
          ENDIF
       ENDDO

    ELSE IF(subjob==7)THEN

       WRITE(*,'((a))')'Input the number of layers:'
       READ(6,*)nlay

       ReadList(:) = 0
       WRITE(*,'((a))')'Input the surfaces with one layer'
       CALL read_int_array(ReadList,nas)

       marke(:) = 0
       DO nb=1,nas
          it = ReadList(nb)
          IF(it>0 .AND. it<=Surf%NB_Surf)THEN
             marke(it) = 1
          ENDIF
       ENDDO
       ReadList(1:Surf%NB_Surf) = marke(1:Surf%NB_Surf)

       markp(:) = 0
       DO ib = 1, Surf%NB_Tri
          IF(ReadList(Surf%IP_Tri(5,ib))==1)THEN
             markp(Surf%IP_Tri(1:3,ib)) = 1
          ENDIF
       ENDDO

       marke(:) = 0
       DO n = 1, nlay
          DO it =1,HybMesh%NB_Tet
             DO i = 1, 4
                ip = HybMesh%IP_Tet(i,it)
                IF(markp(ip)==n)THEN
                   marke(it) = 1
                   WHERE(markp(HybMesh%IP_Tet(1:4,it))==0) markp(HybMesh%IP_Tet(1:4,it)) = n+1
                   EXIT
                ENDIF
             ENDDO
          ENDDO
       ENDDO

    ELSE IF(subjob==8)THEN
       IF(HybMesh%NB_Value==0)THEN
          CALL Error_Stop('HybridMesh_Extract:: no values for mesh.')
       ENDIF
       WRITE(*,'((a))')'Input the type of bound ( 1 for lower bound, 2 for higher bound, 3 for both ):'
       READ(6,*)nlay
       IF(nlay==1)THEN
          WRITE(*,'((a))')'Input the lower bound :'
          READ(6,*)vlower
       ELSE IF(nlay==2)THEN
          WRITE(*,'((a))')'Input the higher bound :'
          READ(6,*)vhigher
       ELSE IF(nlay==3)THEN
          WRITE(*,'((a))')'Input the lower and higher bounds :'
          READ(6,*)vlower, vhigher
       ELSE
          CALL Error_Stop('HybridMesh_Extract:: wrong type of bound')
       ENDIF

       marke(:) = 0
       DO it =1,HybMesh%NB_Tet
          v = 0
          DO i = 1, 4
             ip = HybMesh%IP_Tet(i,it)
             v  = v + HybMesh%V_Point(1,ip)
          ENDDO
          v = v /4
          IF((nlay==1 .OR. nlay==3) .AND. v<vlower)  CYCLE
          IF((nlay==2 .OR. nlay==3) .AND. v>vhigher) CYCLE
          marke(it) = 1
       ENDDO

    ELSE IF(subjob==9)THEN
       WRITE(*,'((a))')'Input the element number of the seed'
       READ(6,*) it
       CALL HybridMesh_TetPropegate(HybMesh, Surf, it, Cells, BoundaryNodes)  
       marke(:) = 0
       DO i =1,Cells%numNodes
          marke(Cells%Nodes(i)) = 1
       ENDDO
       CALL IntQueue_Clear(Cells)
       CALL IntQueue_Clear(BoundaryNodes)
    ENDIF

    IF(subjob==2 .OR. subjob==4)THEN
       markp(:) = -1
       DO np=1,HybMesh%NB_Point
          IF(HybMesh%Posit(1,np)<xmin) CYCLE
          IF(HybMesh%Posit(1,np)>xmax) CYCLE
          IF(HybMesh%Posit(2,np)<ymin) CYCLE
          IF(HybMesh%Posit(2,np)>ymax) CYCLE
          IF(HybMesh%Posit(3,np)<zmin) CYCLE
          IF(HybMesh%Posit(3,np)>zmax) CYCLE
          IF(subjob==4)THEN
             IF(Geo3D_Distance_SQ(HybMesh%Posit(:,np),p0)>R) CYCLE
          ENDIF
          markp(np)=1          
       ENDDO
    ENDIF

    IF(subjob==1 .OR. subjob==2 .OR. subjob==4)THEN
       marke(:) = 1
       DO it =1,HybMesh%NB_Tet
          DO i = 1, Npoint_Tet(HybMesh%GridOrder)
             ip = HybMesh%IP_Tet(i,it)
             IF(ip==0) CYCLE
             IF(markp(ip)==-1)THEN
                marke(it) = 0
                EXIT
             ENDIF
          ENDDO
       ENDDO
    ENDIF

    CALL HybridMesh_removeFake(HybMesh, Marke, new_to_old)

    IF(subjob==7)THEN       
       nb = 0
       DO ib = 1, Surf%NB_Tri
          IF(ReadList(Surf%IP_Tri(5,ib))==1)THEN
             nb = nb+1
             Surf%IP_Tri(1:3,nb) = markp(Surf%IP_Tri(1:3,ib))
             IF(Surf%IP_Tri(4  ,ib)>0)THEN
                Surf%IP_Tri(4  ,nb) = marke(Surf%IP_Tri(4  ,ib))
             ELSE
                Surf%IP_Tri(4  ,nb) = 0
             ENDIF
             Surf%IP_Tri(5  ,nb) = Surf%IP_Tri(5  ,ib)
          ENDIF
       ENDDO
       Surf%NB_Tri = nb
    ELSE
       CALL HybridMesh_BuildNext (HybMesh)
       CALL HybridMesh_BuildBoundSurface(HybMesh, Surf)
    ENDIF

    DEALLOCATE(markp, marke, ReadList)

    RETURN
  END SUBROUTINE HybridMesh_Extract


  !>
  !!  Check the mesh. 
  !!  @param[in] HybMesh  the hybrid mesh.
  !!  @param[in] Surf     the surface mesh represent the boundary.
  !<
  SUBROUTINE HybridMesh_Check(HybMesh,Surf)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(IN)  :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf

    INTEGER :: i,j,ie,np,nb, ips(1000), nel, iemin, iemax, ie1, ipmax, ipmin
    INTEGER :: idir, ierror1, ierror2
    INTEGER :: IFace, ns, nsin, neNew, ND
    INTEGER :: ns_conn(-100:200)
    REAL*8  :: vol,volmax,volmin,hminl,hmin,hmaxl,hmax,hstand,v,diff
    REAL*8  :: pt(3,4), pMin(3), pMax(3), p1(3), p2(3), p3(3), distt(4), pt6(3,6)
    CHARACTER*1 :: yesno
    CHARACTER*1 :: cp(-100:200)

    REAL*8,  DIMENSION(:),   POINTER :: h_ip
    INTEGER, DIMENSION(:),   POINTER :: markp
    INTEGER, DIMENSION(:,:), POINTER :: Face_e
    TYPE(NodeNetTreeType)  :: NetTree

    INTEGER :: NB_PyrAT, NB_PrmAT, NB_PyrAQ, NB_PrmAQ

    NB_PyrAT = HybMesh%NB_Tet
    NB_PrmAT = HybMesh%NB_Tet + HybMesh%NB_Pyr
    NB_PyrAQ = HybMesh%NB_Hex
    NB_PrmAQ = HybMesh%NB_Hex + HybMesh%NB_Pyr

    CALL HybridMeshStorage_Measure(HybMesh,pMin,pMax)
    WRITE(*,*)
    WRITE(*,*) '  Domain range (Xmin, Xmax, Ymin Ymax, Zmin Zmax) '
    WRITE(*,'(2x,6(1x,E12.5))') pMin(1),pMax(1),pMin(2),pMax(2),pMin(3),pMax(3)

    IF(HybMesh%NB_Hex>0)THEN
       DIFF=HybMesh%Posit(1,ABS(HybMesh%IP_Hex(2,1)))-HybMesh%Posit(1,ABS(HybMesh%IP_Hex(1,1)))
    ELSE
       DIFF = 1.d0
    ENDIF
    IF(1==0)THEN
       WRITE(*,*)'DIFF= ',diff
       WRITE(*,'((a),$)')'Enter a relative length (in respect of DIFF) for comparing: '
       READ(6,*)hstand
       hstand = hstand*diff
    ELSE
       hstand = 0
    ENDIF

    DO ie=1,HybMesh%NB_Hex
       DO i=1,8
          IF(HybMesh%IP_Hex(i,ie)==0 .OR. HybMesh%IP_Hex(i,ie)>HybMesh%NB_Point)THEN
             WRITE(*,*)'Error --- ie=',ie,' ip_Hex='
             WRITE(*,*) HybMesh%IP_Hex(:,ie)
             CALL Error_STOP (' ')
          ENDIF
       ENDDO
    ENDDO
    DO ie=1,HybMesh%NB_Pyr
       DO i=1,5
          IF(HybMesh%IP_Pyr(i,ie)==0 .OR. HybMesh%IP_Pyr(i,ie)>HybMesh%NB_Point)THEN
             WRITE(*,*)'Error --- ie=',ie,' ip_Pyr='
             WRITE(*,*) HybMesh%IP_Pyr(:,ie)
             CALL Error_STOP (' ')
          ENDIF
       ENDDO
    ENDDO
    DO ie=1,HybMesh%NB_Tet
       DO i=1,4
          IF(HybMesh%IP_Tet(i,ie)==0 .OR. HybMesh%IP_Tet(i,ie)>HybMesh%NB_Point)THEN
             WRITE(*,*)'Error --- ie=',ie,' ip_Tet='
             WRITE(*,*) HybMesh%IP_Tet(:,ie)
             CALL Error_STOP (' ')
          ENDIF
       ENDDO
    ENDDO

    ALLOCATE(h_ip(HybMesh%NB_Point))
    h_ip(:)=1.0e+36

    DO ie=1,HybMesh%NB_Hex
       DO i = 1,8
          h_ip(ABS(HybMesh%IP_Hex(i,ie)))=diff
       ENDDO
    ENDDO
    DO ie=1,HybMesh%NB_Pyr
       DO i = 1,5
          h_ip(ABS(HybMesh%IP_Pyr(i,ie)))=diff/2.0
       ENDDO
    ENDDO

    volmin = 1.0d36                               
    volmax = 0.0                               
    hmin   = 1.0d36
    hmax   = 0.0          

    nel = 0
    iemin = 0
    iemax = 0
    ierror1 = 0
    DO ie = 1,HybMesh%NB_Tet

       pt(:,1)=HybMesh%Posit(:,ABS(HybMesh%IP_Tet(1,ie)))
       pt(:,2)=HybMesh%Posit(:,ABS(HybMesh%IP_Tet(2,ie)))
       pt(:,3)=HybMesh%Posit(:,ABS(HybMesh%IP_Tet(3,ie)))
       pt(:,4)=HybMesh%Posit(:,ABS(HybMesh%IP_Tet(4,ie)))
       Vol =  Geo3D_Tet_Volume (pt(:,1),pt(:,2),pt(:,3),pt(:,4))

       volmin = MIN( ABS(Vol) , volmin)
       volmax = MAX( ABS(Vol) , volmax)

       IF(Vol<0 .AND. ierror1==0)THEN
          ierror1=1
          WRITE(*,*)' '
          WRITE(*,*)'Warning: Element with negtive volumn found. v=',vol
          WRITE(*,*)'ie,ip=',ie,(ABS(HybMesh%IP_Tet(i,ie)),i=1,4)
       ENDIF

       !---- Height normal to each face

       DO i = 1,4
          p1(:)    = pt(:,iTri_Tet(1,i))
          p2(:)    = pt(:,iTri_Tet(2,i))
          p3(:)    = pt(:,iTri_Tet(3,i))
          distt(i) = 3.d0* Vol / Geo3D_Triangle_Area (P1, P2, P3)
       ENDDO

       !---- Mnimum height of element and update global minimum

       hminl  = MIN(distt(1),distt(2),distt(3),distt(4))
       ips(1:4) = ABS(HybMesh%IP_Tet(1:4, ie))
       WHERE(h_ip(ips(1:4))>hminl) h_ip(ips(1:4)) = hminl
       IF(hmin>hminl)THEN
          hmin  = hminl
          iemin = ie
       ENDIF
       IF(hminl<hstand)nel=nel+1

       hmaxl   = MAX(distt(1),distt(2),distt(3),distt(4))
       IF(hmax<hmaxl)THEN
          hmax = hmaxl
          iemax = ie
       ENDIF

    ENDDO

    WRITE(*,*)'The max volume: ',volmax
    WRITE(*,*)'The min volume: ',volmin
    WRITE(*,*)'The max height: ',hmax
    WRITE(*,*)'The min height: ',hmin
    IF(nel>0) WRITE(*,*)'There are ',nel,        &
         ' elements with a height smaller than ', hstand
    WRITE(*,*)'The element with the smallest height is ',iemin
    WRITE(*,'(a,4I9)')'    with nodes: ', (ABS(HybMesh%IP_Tet(i,iemin)),i=1,4)
    WRITE(*,*)'The element with the largest  height is ',iemax
    WRITE(*,'(a,4I9)')'    with nodes: ', (ABS(HybMesh%IP_Tet(i,iemax)),i=1,4)


    !---- check Prism Contribution
    ierror1 = 0
    DO ie = 1, HybMesh%NB_Prm
       pt6 = HybMesh%Posit(:,HybMesh%IP_Prm(:,ie))
       IF(.NOT. Prism_ValidContribution(HybMesh%IP_Prm(:,ie), Pt6, 0.d0) )THEN
          ierror1 = ierror1 +1
          WRITE(*,*)' Warning--- Prism ',ie,' has negative contribution.'
          WRITE(*,'(7(I9,1X))')HybMesh%IP_Prm(:,ie)
       ENDIF
       IF(ierror1>2) EXIT
    ENDDO


    !---- Check triangulation
    ierror1 = 0
    ierror2 = 0
    DO nb=1,Surf%NB_Tri
       IF(ierror1>2 .OR. ierror2>2) EXIT

       ie  = Surf%IP_Tri(4,nb)
       IF(ie<=0) CYCLE
       IF(ie>HybMesh%NB_Tet+HybMesh%NB_Pyr+HybMesh%NB_Prm)THEN
          PRINT*,'Warning!--- A triangle do NOT have proper element '
          WRITE(*,*)'nb,Tri=',nb,(Surf%IP_Tri(i,nb),i=1,5)
          ierror1 = ierror1 + 1
          CYCLE
       ENDIF
       IF(ie<=HybMesh%NB_Tet)THEN
          idir = which_FaceinTet(Surf%ip_Tri(1:3,nb), ABS(HybMesh%ip_Tet(:,ie)))
       ELSE IF(ie<=NB_PrmAT)THEN
          ie = ie - NB_PyrAT
          idir = which_Tri_inPyr(Surf%ip_Tri(1:3,nb), HybMesh%ip_Pyr(:,ie))
       ELSE
          ie = ie - NB_PrmAT
          idir = which_Tri_inPrm(Surf%ip_Tri(1:3,nb), HybMesh%ip_Prm(:,ie))
       ENDIF
       IF(idir==0)THEN
          PRINT*,'Warning!--- A triangle NOT belonging its element'
          WRITE(*,*)'nb,Tri=',nb,(Surf%IP_Tri(i,nb),i=1,5)
          WRITE(*,*)'ie=',ie
          ierror1 = ierror1 + 1
       ELSE IF(idir<0)THEN
          PRINT*,'Warning!-- A triangle orienting outside its element'
          WRITE(*,*)'nb,Tri=',nb,(Surf%IP_Tri(i,nb),i=1,5)
          WRITE(*,*)'ie=',ie
          ierror2 = ierror2 + 1
       ENDIF
    ENDDO


    !----  build triangular face system

    ALLOCATE(Face_e(2, 20*HybMesh%NB_Point))
    Face_e(:,:)=0

    CALL NodeNetTree_Allocate (3, HybMesh%NB_Point,  3*HybMesh%NB_Tet, NetTree)

    DO ie =1,HybMesh%NB_Tet
       DO nb=1,4
          ips(1:3) = HybMesh%IP_Tet(iTri_Tet(1:3,nb),ie)

          ns = NetTree%numNets
          CALL NodeNetTree_SearchAdd (3, ips(1:3), NetTree, IFace)

          IF(NetTree%numNets>ns)THEN
             face_e(1,IFace)=ie
          ELSE IF(face_e(2,IFace)==0)THEN
             face_e(2,IFace)=ie
             ie1  = face_e(1,IFace)
             idir = which_FaceinTet(ips(1:3), ABS(HybMesh%ip_Tet(:,ie1)))
             IF(idir>=0)THEN
                PRINT*,'ERROR!--- two elements are overlapped!---'
                PRINT*,'ie1,ie2=',ABS(face_e(1,IFace)),ABS(face_e(2,IFace))
                PRINT*,'ips1 = ',HybMesh%IP_Tet(:,ABS(face_e(1,IFace)))
                PRINT*,'ips2 = ',HybMesh%IP_Tet(:,ABS(face_e(2,IFace)))
                RETURN
             ENDIF
          ELSE
             PRINT*,'ERROR!--- three elements take one triangle!---'
             PRINT*,'ie1,ie2,ie3=', ie,ABS(face_e(1,IFace)),ABS(face_e(2,IFace))
             PRINT*,'ips1 = ',HybMesh%IP_Tet(:,ie)
             PRINT*,'ips2 = ',HybMesh%IP_Tet(:,ABS(face_e(1,IFace)))
             PRINT*,'ips3 = ',HybMesh%IP_Tet(:,ABS(face_e(2,IFace)))
             RETURN
          ENDIF

       ENDDO
    ENDDO

    DO ie =1,HybMesh%NB_Pyr
       DO nb=1,4
          ips(1:3) = HybMesh%IP_Pyr(iTri_Pyr(1:3,nb),ie)
          ns = NetTree%numNets
          CALL NodeNetTree_SearchAdd (3, ips(1:3), NetTree, IFace)

          IF(NetTree%numNets>ns)THEN
             face_e(1,IFace)=ie + NB_PyrAT
          ELSE IF(face_e(2,IFace)==0)THEN
             face_e(2,IFace)=ie + NB_PyrAT
             ie1  = face_e(1,IFace)
             IF(ie1<=HybMesh%NB_Tet)THEN
                idir = which_FaceinTet(ips(1:3), ABS(HybMesh%ip_Tet(:,ie1)))
             ELSE
                ie1 = ie1 - NB_PyrAT
                idir = which_Tri_inPyr(ips(1:3), ABS(HybMesh%ip_Pyr(:,ie1)))
             ENDIF
             IF(idir>=0)THEN
                PRINT*,'ERROR!--- two elements are overlapped!---'
                PRINT*,'ie1,ie2=',ABS(face_e(1,IFace)),ABS(face_e(2,IFace))
                RETURN
             ENDIF
          ELSE
             PRINT*,'ERROR!--- three elements take one triangle!---'
             PRINT*,'ie1,ie2,ie3=', ie,ABS(face_e(1,IFace)),ABS(face_e(2,IFace))
             RETURN
          ENDIF

       ENDDO
    ENDDO

    DO ie =1,HybMesh%NB_Prm
       DO nb=1,2
          ips(1:3) = HybMesh%IP_Prm(iTri_Prm(1:3,nb),ie)

          ns = NetTree%numNets
          CALL NodeNetTree_SearchAdd (3, ips(1:3), NetTree, IFace)

          IF(NetTree%numNets>ns)THEN
             face_e(1,IFace)=ie + NB_PrmAT
          ELSE IF(face_e(2,IFace)==0)THEN
             face_e(2,IFace)=ie + NB_PrmAT
             ie1  = face_e(1,IFace)
             IF(ie1<=HybMesh%NB_Tet)THEN
                idir = which_FaceinTet(ips(1:3), ABS(HybMesh%ip_Tet(:,ie1)))
             ELSE IF(ie1<=NB_PrmAT)THEN
                ie1 = ie1 - NB_PyrAT
                idir = which_Tri_inPyr(ips(1:3), ABS(HybMesh%ip_Pyr(:,ie1)))
             ELSE
                ie1 = ie1 - NB_PrmAT
                idir = which_Tri_inPrm(ips(1:3), ABS(HybMesh%ip_Prm(:,ie1)))
             ENDIF
             IF(idir>=0)THEN
                PRINT*,'ERROR!--- two elements are overlapped!---'
                PRINT*,'ie1,ie2=',ABS(face_e(1,IFace)),ABS(face_e(2,IFace))
                RETURN
             ENDIF
          ELSE
             PRINT*,'ERROR!--- three elements take one triangle!---'
             PRINT*,'ie1,ie2,ie3=', ie,ABS(face_e(1,IFace)),ABS(face_e(2,IFace))
             RETURN
          ENDIF

       ENDDO
    ENDDO

    ALLOCATE(markp( HybMesh%NB_Point+200))
    markp(:)=0

    ns=0
    nsin=0
    DO nb=1,Surf%NB_Tri
       ips(1:3) = Surf%IP_Tri(1:3,nb)
       CALL NodeNetTree_Search (3, ips(1:3), NetTree, IFace)

       IF(IFace==0)THEN
          PRINT*,'ERROR!--- no such boundary triangle!---'
          PRINT*,'nb=',nb,'ips=', Surf%IP_Tri(1:5,nb)
          RETURN
       ELSE IF(face_e(2,IFace)==0)THEN
          ns=ns+1
          face_e(2,IFace)=-nb
       ELSE IF(face_e(2,IFace)>0)THEN
	  nsin=nsin+1
       ELSE
          PRINT*,'ERROR!--- surface triangle appears twrice!---'
          PRINT*,'   nb=',nb, ' ips=', ips(1:3), ' nb=',face_e(2,IFace)
          RETURN
       ENDIF
       markp(ips(1:3))=nb
    ENDDO

    IF(Surf%NB_Tri>0)THEN
       DO iface=1,NetTree%numNets
          IF(face_e(2,IFace)==0)THEN
             PRINT*,'ERROR!--- unknown boundary trianlge!---'
             WRITE(*,*)'iface=',iface, 'face_e=',face_e(:,IFace)
             RETURN
          ENDIF
       ENDDO
    ENDIF

    PRINT*,'Number of marked boundary triangles: ',ns
    PRINT*,'Number of interface triangles: ',nsin


    !----  build quad face system

    Face_e(:,:)=0

    CALL NodeNetTree_Clear (NetTree)
    nsin = 3* HybMesh%NB_Hex + 2* HybMesh%NB_Prm + HybMesh%NB_Pyr + 8
    CALL NodeNetTree_Allocate (4, HybMesh%NB_Point,  nsin, NetTree)

    DO ie =1,HybMesh%NB_Hex
       DO nb=1,6
          ips(1:4) = HybMesh%IP_Hex(iQuad_Hex(1:4,nb),ie)
          ns = NetTree%numNets
          CALL NodeNetTree_SearchAdd (4, ips(1:4), NetTree, IFace)

          IF(NetTree%numNets>ns)THEN
             face_e(1,IFace)=ie
          ELSE IF(face_e(2,IFace)==0)THEN
             face_e(2,IFace)=ie
          ELSE
             PRINT*,'ERROR!--- three elements take one quad!---'
             PRINT*,'ie1,ie2,ie3=', ie,ABS(face_e(1,IFace)),ABS(face_e(2,IFace))
             RETURN
          ENDIF
       ENDDO
    ENDDO

    DO ie =1,HybMesh%NB_Pyr
       ips(1:4) = HybMesh%IP_Pyr(1:4,ie)
       ns = NetTree%numNets
       CALL NodeNetTree_SearchAdd (4, ips(1:4), NetTree, IFace)

       IF(NetTree%numNets>ns)THEN
          face_e(1,IFace)=ie + NB_PyrAQ
       ELSE IF(face_e(2,IFace)==0)THEN
          face_e(2,IFace)=ie + NB_PyrAQ
       ELSE
          PRINT*,'ERROR!--- three elements take one triangle!---'
          PRINT*,'ie1,ie2,ie3=', ie,ABS(face_e(1,IFace)),ABS(face_e(2,IFace))
          RETURN
       ENDIF
    ENDDO

    DO ie =1,HybMesh%NB_Prm
       DO nb=1,3
          ips(1:4) = HybMesh%IP_Prm(iQuad_Prm(1:4,nb),ie)
          ns = NetTree%numNets
          CALL NodeNetTree_SearchAdd (4, ips(1:4), NetTree, IFace)

          IF(NetTree%numNets>ns)THEN
             face_e(1,IFace)=ie + NB_PrmAQ
          ELSE IF(face_e(2,IFace)==0)THEN
             face_e(2,IFace)=ie + NB_PrmAQ
          ELSE
             PRINT*,'ERROR!--- three elements take one triangle!---'
             PRINT*,'ie1,ie2,ie3=', ie,ABS(face_e(1,IFace)),ABS(face_e(2,IFace))
             RETURN
          ENDIF
       ENDDO
    ENDDO

    ns=0
    nsin=0
    DO nb=1,Surf%NB_Quad
       ips(1:4) = Surf%IP_Quad(1:4,nb)
       CALL NodeNetTree_Search (4, ips(1:4), NetTree, IFace)

       IF(IFace==0)THEN
          PRINT*,'ERROR!--- no such boundary quad!---'
          PRINT*,'nb=',nb,'ips=', ie,Surf%IP_Tri(1:5,nb)
          RETURN
       ELSE IF(face_e(2,IFace)==0)THEN
          ns=ns+1
          face_e(2,IFace)=-nb
       ELSE IF(face_e(2,IFace)>0)THEN
	  nsin=nsin+1
       ELSE
          PRINT*,'ERROR!--- surface quad appears twrice!---'
          PRINT*,'   nb=',nb, ' ips=', ips(1:4), ' nb=',face_e(2,IFace)
          RETURN
       ENDIF
       markp(ips(1:3))=nb
    ENDDO

    DO iface=1,NetTree%numNets
       IF(face_e(2,IFace)==0)THEN
          PRINT*,'ERROR!--- unknown boundary quads!---'
          WRITE(*,*)'iface=',iface, 'face_e=',face_e(:,IFace)
          RETURN
       ENDIF
    ENDDO

    PRINT*,'Number of marked boundary quads: ',ns
    PRINT*,'Number of interface quads: ',nsin

    !---- Find out those elements with all nodes being a boundary node

    neNew = 0
    DO ie =1,HybMesh%NB_Tet
       ips(1:4)=ABS(HybMesh%IP_Tet(1:4, ie))
       IF(  markp(ips(1))>0 .AND. markp(ips(2))>0 .AND.        &
            markp(ips(3))>0 .AND. markp(ips(4))>0 )THEN
          neNew=neNew+1
       ENDIF
    ENDDO

    IF(neNew>0)THEN
       WRITE(*,'(a,I6,a)')'WARNING--- There are ', neNew,   &
            ' elements whose nodes are all located on boundary.'
       WRITE(*,*)' '
    ENDIF

    !--   check connectivity     

    markp(:)   = 0
    ns_conn(:) = 0
    DO ie =1,HybMesh%NB_Tet
       ND = Npoint_Tet(hybMesh%GridOrder)
       ips(1:ND)=ABS(HybMesh%IP_Tet(1:ND, ie))
       markp(ips(1:4)) = markp(ips(1:4)) + 1
       IF(HybMesh%GridOrder>1)THEN
          markp(ips(5:ND)) = markp(ips(5:ND)) -1
       ENDIF
    ENDDO
    iemax = 0
    iemin = 1000
    DO i=1,HybMesh%NB_Point
       IF(markp(i)>iemax)THEN
          iemax = markp(i)
          ipmax = i
       ENDIF
       IF(markp(i)<iemin .AND. markp(i)>0)THEN
          iemin = markp(i)
          ipmin = i
       ENDIF
       IF(markp(i)>200)THEN
          markp(i) = 200
       ELSE IF(markp(i)>100)THEN
          markp(i) = 100
       ELSE IF(markp(i)>80)THEN
          markp(i) = 80
       ELSE IF(markp(i)>60)THEN
          markp(i) = 60
       ELSE IF(markp(i)>40)THEN
          markp(i) = 40
       ELSE IF(markp(i)<-50)THEN
          markp(i) = -50
       ELSE IF(markp(i)<-20)THEN
          markp(i) = -20
       ELSE IF(markp(i)<-10)THEN
          markp(i) = -10
       ENDIF
       ns_conn(markp(i)) = ns_conn(markp(i)) +1
    ENDDO
    cp(-100:39)   = ' '
    cp(40:200) = '+'
    WRITE(*,*)' node ',ipmin,' has minimum number of elements: ',iemin
    WRITE(*,*)' node ',ipmax,' has maximum number of elements: ',iemax
    WRITE(*,*)'no._associated_cells : no._nodes |'
    ns = 0
    DO i=-50,200
       IF(ns_conn(i)>0)THEN
          ns = ns+1
          markp(ns) = i
       ENDIF
    ENDDO
    nsin = (ns+3)/4
    DO i=1,nsin
       ips(1:4) = (/i, i+nsin, i+2*nsin, i+3*nsin/)
       nb = 4
       IF(i+3*nsin>ns) nb = 3
       ips(1:nb) = markp(ips(1:nb))
       WRITE(*,'(4(i3,a1,I9,a,2x))') (ips(j),cp(ips(j)),ns_conn(ips(j)),' | ',j=1,nb)       
    ENDDO

    DEALLOCATE(h_ip, markp, Face_e)
    CALL NodeNetTree_Clear (NetTree)

    RETURN

  END SUBROUTINE HybridMesh_Check


  !>
  !!  Check or search nodes of the mesh. 
  !!  @param[in] HybMesh  the hybrid mesh.
  !<
  SUBROUTINE HybridMesh_nodeCheck(HybMesh)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(IN)  :: HybMesh
    INTEGER :: i, np
    REAL*8  :: hstand
    REAL*8  :: pMin(3), pMax(3), p1(3)

    CALL HybridMeshStorage_Measure(HybMesh,pMin,pMax)
    WRITE(*,*)
    WRITE(*,*) '  Domain ranges: '
    WRITE(*,'(2x,6(1x,E12.5))') pMin(1),pMax(1),pMin(2),pMax(2),pMin(3),pMax(3)

    DO 

       WRITE(*,*)' '
       WRITE(*,'((a),$)')' Input the node number (0 for search a node; -1 for stop):'
       READ(6,*)np

       IF(np.LE.-1)RETURN
       IF(np>HybMesh%NB_Point)THEN
          PRINT*,'No such node number with NB_Point=', HybMesh%NB_Point
       ELSE IF(np>0)THEN
          WRITE(*,*) ' pt=',HybMesh%Posit(:,np)
       ELSE
          WRITE(*,*)' Search nodes in a cube with side length of 2*r'
          WRITE(*,'((a),$)')'  Input the central position (x,y,z) & r: '
          READ(6,*)p1(1:3),hstand
          DO i=1,HybMesh%NB_Point
             IF(  ABS(HybMesh%Posit(1,i)-p1(1))<hstand .AND.   &
                  ABS(HybMesh%Posit(2,i)-p1(2))<hstand .AND.   &
                  ABS(HybMesh%Posit(3,i)-p1(3))<hstand)THEN
                WRITE(*,*)'np= ',i, ' pt=',REAL(HybMesh%Posit(:,i))
                np = 1
             ENDIF
          ENDDO
          IF(np==0)THEN
             WRITE(*,*)' No node is found in above domain'
          ENDIF
       ENDIF

    ENDDO

  END SUBROUTINE HybridMesh_nodeCheck



  !>
  !!  Convert variables for a Hybrid mesh. 
  !!  @param[in,out]  HybMesh  the hybrid mesh with values.
  !!  @param[in]  fromto =  1: get node values from cell values.    \n
  !!                     = -1: get cell values from node values.  
  !<
  SUBROUTINE HybridMesh_Convert_Values(HybMesh, fromto)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    INTEGER, INTENT(IN) ::  fromto
    INTEGER, DIMENSION(:), POINTER :: nrr
    INTEGER :: n, i, ip, it

    n = HybMesh%NB_Value
    IF(fromto==1)THEN

       IF(n /= SIZE(HybMesh%V_Cell, 1) ) CALL Error_STOP ( '--- HybridMesh_Convert_Values 1')
       ALLOCATE(HybMesh%V_Point(n,HybMesh%NB_Point))
       ALLOCATE(nrr(HybMesh%NB_Point))

       nrr(:) = 0
       HybMesh%V_Point(:,:) = 0
       DO it = 1, HybMesh%NB_Tet
          DO i = 1,4
             ip = HybMesh%IP_Tet(i,it)
             HybMesh%V_Point(:,ip) = HybMesh%V_Point(:,ip) + HybMesh%V_Cell(:,it)
             nrr(ip) = nrr(ip) + 1
          ENDDO
       ENDDO
       DO ip = 1, HybMesh%NB_Point
          HybMesh%V_Point(:,ip) = HybMesh%V_Point(:,ip) / nrr(ip)
       ENDDO

       DEALLOCATE(nrr)        

    ELSE IF(fromto==-1)THEN

       IF(n /= SIZE(HybMesh%V_Point, 1) ) CALL Error_STOP ( '--- HybridMesh_Convert_Values -1')
       ALLOCATE(HybMesh%V_Cell(n,HybMesh%NB_Tet))

       HybMesh%V_Cell(:,:) = 0
       DO it = 1, HybMesh%NB_Tet
          DO i = 1,4
             ip = HybMesh%IP_Tet(i,it)
             HybMesh%V_Cell(:,it) = HybMesh%V_Cell(:,it) + HybMesh%V_Point(:,ip)
          ENDDO
          HybMesh%V_Cell(:,it) = HybMesh%V_Cell(:,it) / 4
       ENDDO

    ENDIF


    RETURN
  END SUBROUTINE HybridMesh_Convert_Values

  !>
  !!  Read variables for a Hybrid mesh. 
  !!  @param[in]  JobName  the name (prefix) of Output file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  fmat = -1: unformatted, read node values, node-loop  first : .unk    \n
  !!                   = -2: unformatted, read node values, value-loop first : .unu    \n
  !!                   = -3: unformatted, read cell values, node-loop  first : .unk    \n
  !!                   = -4: unformatted, read cell values, value-loop first : .unu    \n
  !!                   =  1:   formatted, read node values, value-loop first, with indexes : .unf    \n
  !!                   =  2:   formatted, read cell values, value-loop first, with indexes : .unf    \n
  !!                   =  3:   formatted, read node values, value-loop first, no indexes   : .unf    \n
  !!                   =  4:   formatted, read cell values, value-loop first, no indexes   : .unf    \n
  !!  @param[in]  HybMesh  the hybrid mesh.
  !!  @param[out] HybMesh  the hybrid mesh with values.
  !<
  SUBROUTINE HybridMesh_Input_Values(JobName,JobNameLength,fmat,HybMesh)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength, fmat
    REAL*8, DIMENSION(:,:), POINTER :: arr
    INTEGER :: n, nn, i, j, k


    IF(fmat==-1 .OR. fmat==-3)THEN
       OPEN(11,file = JobName(1:JobNameLength)//'.unk', status='OLD', form='UNFORMATTED' )
    ELSE IF(fmat==-2 .OR. fmat==-4)THEN
       OPEN(11,file = JobName(1:JobNameLength)//'.unu', status='OLD', form='UNFORMATTED' )
    ELSE IF(fmat>=1 .AND. fmat<=4)THEN
       OPEN(11,file = JobName(1:JobNameLength)//'.unf', status='OLD', form='FORMATTED' )
    ELSE
       WRITE(*,*)' Warning--- HybridMesh_Input_Values:: no such format'
       RETURN
    ENDIF

    IF(fmat<0)THEN
       READ(11)nn,n
    ELSE
       READ(11,*)nn,n
    ENDIF

    HybMesh%NB_Value = n

    IF(fmat==-1 .OR. fmat==-2 .OR. fmat==1 .OR. fmat==3)THEN
       IF(nn/=HybMesh%NB_Point)THEN
          WRITE(*,*)' Warning--- nn/=HybMesh%NB_Point: ',nn,HybMesh%NB_Point
          RETURN
       ENDIF
       ALLOCATE(HybMesh%V_Point(n,nn))
       arr => HybMesh%V_Point
    ELSE
       IF(nn/=HybMesh%NB_Tet)THEN
          WRITE(*,*)' Warning--- nn/=HybMesh%NB_Tet: ',nn,HybMesh%NB_Tet
          RETURN
       ENDIF
       ALLOCATE(HybMesh%V_Cell(n,nn))
       arr => HybMesh%V_Cell
    ENDIF

    WRITE(*,*)' nn=',nn,'   n=',n
    IF(fmat==-1 .OR. fmat==-3)THEN
       READ(11)((arr(i,j),j=1,nn), i=1,n)
    ELSE IF(fmat==-2 .OR. fmat==-4)THEN
       DO j=1,nn
          READ(11)k,arr(1:n,k)
       ENDDO
    ELSE IF(fmat==1 .OR. fmat==2)THEN
       DO j=1,nn
          READ(11,*)k,arr(1:n,k)
       ENDDO
    ELSE IF(fmat==3 .OR. fmat==4)THEN
       DO j=1,nn
          READ(11,*)arr(1:n,j)
       ENDDO
    ENDIF

    NULLIFY(arr)

    RETURN
  END SUBROUTINE HybridMesh_Input_Values


  !>
  !!  Output variables of a Hybrid mesh. 
  !!  @param[in]  JobName  the name (prefix) of Output file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  fmat = -1: unformatted, WRITE node values, node-loop  first : .unk (for xplot)   \n
  !!                   = -2: unformatted, WRITE node values, value-loop first : .unu    \n
  !!                   = -3: unformatted, WRITE cell values, node-loop  first : .unk    \n
  !!                   = -4: unformatted, WRITE cell values, value-loop first : .unu    \n
  !!                   =  1:   formatted, WRITE node values, value-loop first : .unf    \n
  !!                   =  2:   formatted, WRITE cell values, value-loop first : .unf
  !!  @param[in]  HybMesh  the hybrid mesh with values.
  !<
  SUBROUTINE HybridMesh_Output_Values(JobName,JobNameLength,fmat,HybMesh)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(IN) :: HybMesh
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength, fmat
    REAL*8, DIMENSION(:,:), POINTER :: arr
    INTEGER :: n, nn, i, j, k, m, ii(100)


    IF(fmat==-1 .OR. fmat==-3)THEN
       OPEN(11,file = JobName(1:JobNameLength)//'.unk', form='UNFORMATTED' )
    ELSE IF(fmat==-2 .OR. fmat==-4)THEN
       OPEN(11,file = JobName(1:JobNameLength)//'.unu', form='UNFORMATTED' )
    ELSE IF(fmat==1 .OR. fmat==2)THEN
       OPEN(11,file = JobName(1:JobNameLength)//'.unf', form='FORMATTED' )
    ELSE
       WRITE(*,*)' Warning--- HybridMesh_Output_Values:: no such format'
       RETURN
    ENDIF

    IF(fmat==-1 .OR. fmat==-2 .OR. fmat==1)THEN
       nn  =  HybMesh%NB_Point
       arr => HybMesh%V_Point
    ELSE
       nn  =  HybMesh%NB_Tet
       arr => HybMesh%V_Cell
    ENDIF

    n  = HybMesh%NB_Value

    IF(fmat<0)THEN
       WRITE(11)nn,n
    ELSE
       WRITE(11,*)nn,n
    ENDIF

    IF(fmat==-1)THEN
       m = MAX(n,5)
       DO i = 1,m
          ii(i) = i
          IF(i>n) ii(i) = n
       ENDDO
       WRITE(11)((arr(ii(i),j),j=1,nn), i=1,m)
    ELSE IF(fmat==-3)THEN
       WRITE(11)((arr(i,j),j=1,nn), i=1,n)
    ELSE IF(fmat==-2 .OR. fmat==-4)THEN
       DO j=1,nn
          WRITE(11)j,arr(1:n,j)
       ENDDO
    ELSE IF(fmat==1 .OR. fmat==2)THEN
       DO j=1,nn
          WRITE(11,'(I8,10(1X,E13.6))') j,arr(1:n,j)
       ENDDO
    ENDIF

    NULLIFY(arr)

    RETURN
  END SUBROUTINE HybridMesh_Output_Values


  !>
  !!  Output variables of a Hybrid mesh by EnSight format. 
  !!  @param[in]  JobName  the name (prefix) of Output file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  HybMesh  the hybrid mesh with values.
  !<
  SUBROUTINE HybridMesh_OutputEnSight_Values(JobName,JobNameLength, HybMesh, Surf)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(IN) :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    REAL*4, DIMENSION(:  ), POINTER :: a
    INTEGER, DIMENSION(: ), POINTER :: gloloc
    CHARACTER*1 :: uk
    CHARACTER*80 :: string
    INTEGER :: k, n, nn, i, j, ip, is, nbp

    nn = KeyLength(HybMesh%V_Point)
    IF(nn/=HybMesh%NB_Point)THEN
       WRITE(*,*)' Warning--- HybridMesh_OutputEnSight_Values:: no proper V_Point'
       RETURN
    ENDIF

    n = HybMesh%NB_Value
    ALLOCATE(a(nn),gloloc(nn))

    DO k = 1,n
       a(1:nn) = HybMesh%V_Point(k,1:nn)
       WRITE(uk,'(I1)')k
       OPEN(11,file = JobName(1:JobNameLength)//'.u'//uk,       &
            status='UNKNOWN', form='UNFORMATTED' )
       string = 'unknown'
       WRITE(11) string
       string = 'part'
       WRITE(11) string
       is = 1
       WRITE(11) is
       string = 'coordinates'
       WRITE(11) string
       WRITE(11) a(1:nn)

       DO is = 1,Surf%NB_Surf
          gloloc(1:nn) = 0
          nbp = 0
          DO i = 1,Surf%NB_Tri
             IF(Surf%IP_Tri(5,i)==is)THEN
                DO j=1,3
                   ip = Surf%IP_Tri(j,i)
                   IF(gloloc(ip) == 0)THEN
                      nbp = nbp + 1
                      gloloc(ip) = nbp
                      a(nbp) = HybMesh%V_Point(k,ip)
                   ENDIF
                ENDDO
             ENDIF
          ENDDO
          IF(nbp==0) CYCLE

          string = 'part'
          WRITE(11) string
          WRITE(11) is+1
          string = 'coordinates'
          WRITE(11) string
          WRITE(11) a(1:nbp)
       ENDDO

       IF(Surf%NB_Quad>0)THEN
          gloloc(1:nn) = 0
          nbp = 0
          DO i = 1,Surf%NB_Quad
             DO j=1,4
                ip = Surf%IP_Quad(j,i)
                IF(gloloc(ip) == 0)THEN
                   nbp = nbp + 1
                   gloloc(ip) = nbp
                   a(nbp) = HybMesh%V_Point(k,ip)
                ENDIF
             ENDDO
          ENDDO

          string = 'part'
          WRITE(11) string
          WRITE(11) Surf%NB_Surf+2
          string = 'coordinates'
          WRITE(11) string
          WRITE(11) a(1:nbp)
       ENDIF

       CLOSE (11)
    ENDDO


    DEALLOCATE(a,gloloc)

    RETURN

  END SUBROUTINE HybridMesh_OutputEnSight_Values


  FUNCTION Prism_ValidContribution(Ips, Pts, Vworst) RESULT(TF)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Ips(6)
    REAL*8,  INTENT(IN) :: Pts(3,6), Vworst
    LOGICAL :: TF
    REAL*8  :: p1(3), p2(3), p3(3), p0(3), Vw

    TF = .FALSE.

    IF(ips(4)<ips(5))THEN  
       p1 = (pts(:,2)+pts(:,4)) / 2.d0
    ELSE
       p1 = (pts(:,1)+pts(:,5)) / 2.d0
    ENDIF
    IF(ips(5)<ips(6))THEN  
       p2 = (pts(:,3)+pts(:,5)) / 2.d0
    ELSE
       p2 = (pts(:,2)+pts(:,6)) / 2.d0
    ENDIF
    IF(ips(4)<ips(6))THEN  
       p3 = (pts(:,3)+pts(:,4)) / 2.d0
    ELSE
       p3 = (pts(:,1)+pts(:,6)) / 2.d0
    ENDIF
    p0 = (p1 + p2 + p3) / 3.d0

    Vw = 24.d0* Vworst

    IF(ips(4)<ips(5))THEN  
       IF(Geo3D_Tet_Volume6_High (Pts(:,1), Pts(:,4), Pts(:,2), p0) < Vw) RETURN
       IF(Geo3D_Tet_Volume6_High (Pts(:,2), Pts(:,4), Pts(:,5), p0) < Vw) RETURN
    ELSE
       IF(Geo3D_Tet_Volume6_High (Pts(:,1), Pts(:,5), Pts(:,2), p0) < Vw) RETURN
       IF(Geo3D_Tet_Volume6_High (Pts(:,1), Pts(:,4), Pts(:,5), p0) < Vw) RETURN
    ENDIF
    IF(ips(5)<ips(6))THEN  
       IF(Geo3D_Tet_Volume6_High (Pts(:,2), Pts(:,5), Pts(:,3), p0) < Vw) RETURN
       IF(Geo3D_Tet_Volume6_High (Pts(:,3), Pts(:,5), Pts(:,6), p0) < Vw) RETURN
    ELSE
       IF(Geo3D_Tet_Volume6_High (Pts(:,2), Pts(:,5), Pts(:,6), p0) < Vw) RETURN
       IF(Geo3D_Tet_Volume6_High (Pts(:,2), Pts(:,6), Pts(:,3), p0) < Vw) RETURN
    ENDIF
    IF(ips(4)<ips(6))THEN  
       IF(Geo3D_Tet_Volume6_High (Pts(:,1), Pts(:,3), Pts(:,4), p0) < Vw) RETURN
       IF(Geo3D_Tet_Volume6_High (Pts(:,3), Pts(:,6), Pts(:,4), p0) < Vw) RETURN
    ELSE
       IF(Geo3D_Tet_Volume6_High (Pts(:,1), Pts(:,6), Pts(:,4), p0) < Vw) RETURN
       IF(Geo3D_Tet_Volume6_High (Pts(:,1), Pts(:,3), Pts(:,6), p0) < Vw) RETURN
    ENDIF

    Vw = 36.d0* Vworst

    IF(Geo3D_Tet_Volume6_High (Pts(:,1), Pts(:,2), Pts(:,3), p0) < Vw) RETURN
    IF(Geo3D_Tet_Volume6_High (Pts(:,4), Pts(:,6), Pts(:,5), p0) < Vw) RETURN

    TF = .TRUE.

    RETURN
  END  FUNCTION Prism_ValidContribution

  !>
  !!  Propegate tet. cells from a given cell to all the domain without going through any surface or sheet.
  !!  @param[in,out] HybMesh  the hybrid mesh. The Next_Tet will be destroied aftermath.
  !!  @param[in] Surf     the surface mesh represent the boundary and sheets.
  !!  @param[in] ie0      the start cell.
  !!  @param[out] Cells    the list of cells being affected.
  !!  @param[out] BoundaryNodes    the list of boundary nodes for this region.
  !<
  SUBROUTINE HybridMesh_TetPropegate(HybMesh,Surf, ie0, Cells, BoundaryNodes)  
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT)  :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    integer, intent(in) :: ie0
    TYPE(IntQueueType), intent(inout) :: Cells
    TYPE(IntQueueType), intent(inout) :: BoundaryNodes
    TYPE(NodeNetTreeType) :: Tri_Tree
    Integer, dimension(:), pointer :: Mark_Tet, Mark_Pt
    integer :: ib, it, ipp(3), nb2, i, itnb, id1, ip
    
    !---- build next system
    
    CALL HybridMesh_BuildNext(HybMesh)
    
     !--- build a surface/sheet triangulation system

     CALL NodeNetTree_allocate(3, HybMesh%NB_Point, Surf%NB_Tri, Tri_Tree)

     DO IB = 1,Surf%NB_Tri
        ipp(1:3) = Surf%IP_Tri(1:3,IB)
        CALL NodeNetTree_SearchAdd(3, ipp, Tri_Tree, NB2)
     ENDDO
     
     !---- break next between sheet
     DO IT = 1,HybMesh%NB_Tet
        DO i=1,4
           ipp(1:3) = HybMesh%IP_Tet(iTri_Tet(1:3,i),IT) 
           CALL NodeNetTree_search(3, ipp, Tri_Tree, IB)
           IF(IB>0) HybMesh%Next_Tet(i,IT) = 0
        enddo
     ENDDO
     
     !--- propagate
    
     allocate(Mark_Tet(HybMesh%NB_Tet))
     allocate(Mark_Pt(HybMesh%NB_Point))
     Mark_Tet(:) = 0
     Mark_Pt(:)  = 0
     CALL IntQueue_Push(Cells,ie0)
     Mark_Tet(ie0) = 1
     id1 = 1

     DO 
        it = Cells%Nodes(id1)    
        Mark_Pt(HybMesh%IP_Tet(:, it)) = 1
           DO i=1,4
              itnb = HybMesh%Next_Tet(i,IT)
              IF(itnb<=0) CYCLE
              IF(Mark_Tet(itnb)==1) cycle
                 Mark_Tet(itnb) = 1
                 CALL IntQueue_Push(Cells,itnb)
           ENDDO
           if(it==Cells%back) EXIT
        id1 = id1+1
     ENDDO
     
     DO IB = 1,Surf%NB_Tri
        DO i = 1,3
           ip = Surf%IP_Tri(i,IB)
           if(Mark_Pt(ip)==1)then
               CALL IntQueue_Push(BoundaryNodes, ip)
               Mark_Pt(ip) = 0
           endif    
        ENDDO    
     ENDDO
     
     deallocate(Mark_Tet, Mark_Pt)
     CALL NodeNetTree_clear(Tri_Tree)
     deallocate(HybMesh%Next_Tet)
     
     return
  end SUBROUTINE HybridMesh_TetPropegate

END MODULE HybridMeshManager

