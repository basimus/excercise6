!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


MODULE OptimisingTypeModule
  IMPLICIT NONE
  !>
  !!  The parameters modified by Optimisation Processes.
  !!  The parameters referred by Optimisation Value Functions.
  !<
  TYPE :: OptimisingType
     INTEGER :: Ndim      = 3                 !< the number of dimensions.
     INTEGER :: IterLimit = 100               !< the number of maximum iterations.
     REAL*8  :: DTol      = 1.d-6             !< the convergence tolerance in term of the distance between positions.
     INTEGER :: Idistb    = 100               !< the number of disturb for stable points.
     INTEGER :: Iter      = 0                 !< the number of iterations.
     INTEGER :: Isucc     = 0                 !< the index of succession.
     REAL*8  :: Forg                          !< the original fitness.
     REAL*8  :: Fend                          !< the final fitness.
     INTEGER :: Method                        !< 1 for Powell; 2 for PSO; 3 for Cukoo.
  END TYPE OptimisingType
  TYPE(OptimisingType), SAVE :: OptParameter
  !>
  !!  The parameters referred by Optimisation Processes.
  !!  The parameters modified by Optimisation Value Functions.
  !<
  TYPE :: OptimisingVType
     INTEGER :: Icount = 0                    !< a counter.
     INTEGER :: IdxFun = 0                    !< the index of function.
     INTEGER :: IdNode = 0                    !< a help id
     REAL*8  :: F                             !< a fitness
     INTEGER :: getBetter = 0           !< the flag if P locating in the domain.
  END TYPE OptimisingVType
  TYPE(OptimisingVType), SAVE :: OptVariable

END MODULE OptimisingTypeModule



MODULE OptimisingModule
  USE OptimisingTypeModule
  USE RandomModule
  IMPLICIT NONE

  INTERFACE Optimising_1D
     MODULE PROCEDURE Optimising_1D0, Optimising_1Dr
  END INTERFACE Optimising_1D
  
  
CONTAINS

  !>
  !!          Set
  !!
  !!    OptParameter%Ndim, OptParameter%IterLimit, OptParameter%Dtol
  !<
  SUBROUTINE Optimising_Set(Ndim, IterLimit, DTol)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Ndim, IterLimit
    REAL*8,  INTENT(IN) :: DTol
    OptParameter%Ndim      = Ndim
    OptParameter%IterLimit = IterLimit
    OptParameter%DTol      = DTol
  END SUBROUTINE Optimising_Set

  !>
  !!          Direct 1D Optimisation.
  !!
  !!    @param[in]  OptParameter%Ndim       the number of dimensions.
  !!    @param[in]  OptParameter%Dtol       the convergence tolerance in term of the distance between positions.
  !!    @param[in]  Ptip                    the original position of the particle.
  !!    @param[in]  iD                      the moving dimension of the particle.
  !!    @param[in]  OptVariable%F           the original fitness (respect of Ptip by input).
  !!    @param[out] Ptip                    the final (best) position.
  !!    @param[out] Isucc      = 0, No better solution found than input.           \n
  !!                           = 2, found better soultion.                         \n
  !!                           = 3, position found satisfied the target (set by Optimising_GetValue()).
  !<
  SUBROUTINE Optimising_1D0 (Ptip, iD, Isucc)
    IMPLICIT NONE
    REAL*8,  INTENT(INOUT), DIMENSION(OptParameter%Ndim) :: Ptip
    INTEGER, INTENT(IN)    :: iD
    INTEGER, INTENT(OUT) :: Isucc
    REAL*8  :: dd, Pid, F

    Isucc = 0
    dd  = 7.9999d0 * OptParameter%Dtol
    Pid = Ptip(id)
    DO WHILE(ABS(dd)>OptParameter%Dtol)
       Ptip(id) = Pid + dd
       CALL Optimising_GetValue (Ptip, F)
       IF(OptVariable%getBetter>0)THEN
          IF(OptVariable%getBetter==2)THEN
             Isucc = 3
             RETURN
          ENDIF
          Isucc = 2
          Pid  = Ptip(id)
          dd   = 2*dd
       ELSE
          Ptip(id) = Pid
          dd       = -dd/4.d0
       ENDIF
    ENDDO

    RETURN
  END SUBROUTINE Optimising_1D0

  !>
  !!          Direct 1D Optimisation.
  !!
  !!    @param[in]  OptParameter%Ndim       the number of dimensions.
  !!    @param[in]  OptParameter%Dtol       the convergence tolerance in term of the distance between positions.
  !!    @param[in]  Ptip                    the original position of the particle.
  !!    @param[in]  Dir                     the moving direction of the particle.
  !!    @param[in]  OptVariable%F           the original fitness (respect of Ptip by input).
  !!    @param[out] Ptip                    the final (best) position.
  !!    @param[out] Isucc      = 0, No better solution found than input.           \n
  !!                           = 2, found better soultion.                         \n
  !!                           = 3, position found satisfied the target (set by Optimising_GetValue()).
  !<
  SUBROUTINE Optimising_1Dr (Ptip, Dir, Isucc)
    IMPLICIT NONE
    REAL*8,  INTENT(INOUT), DIMENSION(OptParameter%Ndim) :: Ptip
    REAL*8,  INTENT(IN), DIMENSION(OptParameter%Ndim)  :: Dir
    INTEGER, INTENT(OUT) :: Isucc
    REAL*8,              DIMENSION(OptParameter%Ndim)  :: Pid
    REAL*8  :: dd, F

    Isucc = 0
    dd     = 7.9999d0 * OptParameter%Dtol
    Pid(:) = Ptip(:)
    DO WHILE(ABS(dd)>OptParameter%Dtol)
       Ptip(:) = Pid(:) + dd * Dir(:)
       CALL Optimising_GetValue (Ptip, F)
       IF(OptVariable%getBetter>0)THEN
          IF(OptVariable%getBetter==2)THEN
             Isucc = 3
             RETURN
          ENDIF
          Isucc = 2
          Pid(:)  = Ptip(:)
          dd      = 2*dd
       ELSE
          Ptip(:) = Pid(:)
          dd      = -dd/4.d0
       ENDIF
    ENDDO

    RETURN
  END SUBROUTINE Optimising_1Dr

  !>
  !!          Powell Direct Optimisation with Disturb.
  !!
  !!    @param[in]  OptParameter%Ndim       the number of dimensions.
  !!    @param[in]  OptParameter%Dtol       the convergence tolerance in term of the distance between positions.
  !!    @param[in]  OptParameter%IterLimit  the maximum number of iteration.
  !!    @param[in]  OptParameter%Idistb     the number of disturb for stable points.
  !!    @param[in]  P0                      the original position.
  !!    @param[out] Ptip                    the final (best) position.
  !!    @param[out] OptParameter%Iter       the number of iteration.
  !!    @param[out] OptParameter%Forg       the original fitness (repect of P0).
  !!    @param[out] OptParameter%Fend       the final fitness (repect of Ptip).
  !!    @param[out] OptParameter%Isucc      = 0, No better solution found than input.           \n
  !!                                        = 1, unconvergent soultion.                         \n
  !!                                        = 2, convergent soultion.                           \n
  !!                                        = 3, position found satisfied the target (set by Optimising_GetValue()).  \n
  !!                                        = 4, the original position found satisfied the target (no iteration).
  !<
  SUBROUTINE Optimising_Powell (P0, Ptip)
    IMPLICIT NONE
    REAL*8,  INTENT(IN),  DIMENSION(OptParameter%Ndim) :: P0
    REAL*8,  INTENT(OUT), DIMENSION(OptParameter%Ndim) :: Ptip
    INTEGER :: id, ksucc, Isucc1, Loop, j
    REAL*8  :: Pid, F, dd, aa
    REAL*8, DIMENSION(OptParameter%Ndim) :: Pt, xd
    INTEGER, SAVE :: idum = -1

    OptParameter%Method = 1
    OptParameter%Iter   = 0

    Ptip(:) = P0
    CALL Optimising_GetValue (Ptip, F)
    OptParameter%Forg = F
    OptParameter%Fend = F
    IF( OptVariable%getBetter ==2 )THEN
       OptParameter%Isucc = 4
       RETURN
    ENDIF
    
    OptParameter%Iter  = 1

    IF(OptParameter%Ndim==1)THEN
       CALL Optimising_1D(Ptip, 1, Isucc1)
       OptParameter%Fend = OptVariable%F
       OptParameter%Isucc = Isucc1
       RETURN
    ENDIF
    
    OptParameter%Isucc = 0

    DO Loop = 1, OptParameter%IterLimit

       aa = 1.0/(Loop**(2.0))
       ksucc = 0
       Pt(:) = Ptip(:)
       DO id = 1, OptParameter%Ndim +1
          OptParameter%Iter = OptParameter%Iter + 1
          IF(OptParameter%Iter>OptParameter%IterLimit)THEN
             IF( OptParameter%Isucc ==2 ) OptParameter%Isucc = 1
             RETURN
          ENDIF

          IF(id<=OptParameter%Ndim)THEN
             Pid = Ptip(id)
             CALL Optimising_1D(Ptip, id, Isucc1)
             IF(Isucc1==0) ksucc = ksucc+1
          ELSE IF(ksucc<OptParameter%Ndim)THEN
             CALL Optimising_Distance (Ptip, Pt, dd)
             xd = (Ptip-Pt) / dd
             CALL Optimising_1D(Ptip, xd, Isucc1)
          ENDIF

          OptParameter%Fend = OptVariable%F
          IF(OptParameter%Isucc==0) OptParameter%Isucc = Isucc1
          IF(OptParameter%Isucc==3) RETURN
       ENDDO

       IF(ksucc<OptParameter%Ndim) CYCLE

       Pt(:) = Ptip(:)
       DO j = 1,OptParameter%Idistb
          CALL Random_LevyWalk(xd,OptParameter%Ndim)
          Ptip(:) = Ptip(:) + aa * xd(:)
          CALL Optimising_GetValue (Ptip, F)
          IF(OptVariable%getBetter>0)THEN
             OptParameter%Fend = F
             IF(OptVariable%getBetter==2)THEN
                OptParameter%Isucc = 3
                RETURN
             ENDIF
             IF(OptParameter%Isucc==0) OptParameter%Isucc = 2
             EXIT
          ENDIF
          Ptip(:) = Pt(:)
       ENDDO

       IF(j>OptParameter%Idistb)  RETURN

    ENDDO

    RETURN
  END SUBROUTINE Optimising_Powell


  !>
  !!        Particle Swarm Optimisation.
  !!
  !!    @param[in]  OptParameter%Ndim       the number of dimensions.
  !!    @param[in]  OptParameter%Dtol       the convergence tolerance in term of the distance between particles. 
  !!    @param[in]  OptParameter%IterLimit  the maximum number of iteration.
  !!    @param[in]  N                       the number of particles.
  !!    @param[in]  P0                      the original position of all particles.
  !!    @param[out] Ptip                    the final (best) position.
  !!    @param[out] OptParameter%Iter       the number of iteration.
  !!    @param[out] OptParameter%Fend       the final fitness (repect of Ptip).
  !!    @param[out] OptParameter%Isucc      = 0, No solution in domain.                         \n
  !!                                        = 1, unconvergent soultion.                         \n
  !!                                        = 2, convergent soultion.                           \n
  !!                                        = 3, position found satisfied the target (set by Optimising_GetValue()).
  !<
  SUBROUTINE Optimising_PSO (N, P0, Ptip)
    IMPLICIT NONE
    INTEGER, INTENT(IN)    :: N
    REAL*8, DIMENSION(OptParameter%Ndim,N), INTENT(IN)  :: P0
    REAL*8, DIMENSION(OptParameter%Ndim),   INTENT(OUT) :: Ptip
    INTEGER :: i, j, Loop
    REAL*8, DIMENSION(OptParameter%Ndim,N) :: Psh   !---  best position of each particle.
    REAL*8, DIMENSION(OptParameter%Ndim,N) :: Pt    !---  current position of each particle.
    REAL*8, DIMENSION(OptParameter%Ndim,N) :: v     !---  velocity
    REAL*8, DIMENSION(N)   :: Qu
    REAL*8  :: D, Di, rj1, rj2, F
    REAL*8  :: e1 = 1.0d0, ev
    INTEGER, SAVE :: idum = -1
    REAL*8  :: Big = 1.d36

    OptParameter%Method = 2
    OptParameter%Isucc  = 0
    OptParameter%Fend   = Big

    Qu(1:N)    = Big
    v(:,  1:N) = 0
    Psh(:,1:N) = P0(:,1:N)
    Pt(:, 1:N) = P0(:,1:N)


    DO Loop = 1, OptParameter%IterLimit

       OptParameter%Iter = Loop
       ev = 1.d0 / Loop
       DO i = 1, N
          OptVariable%F = Qu(i)
          CALL Optimising_GetValue (Pt(:,i), F)
          IF(OptVariable%getBetter<=0) CYCLE
          
          Qu(i) = F
          Psh(:, i) = Pt(:, i)
          IF(F<OptParameter%Fend)THEN
             OptParameter%Isucc = 1
             OptParameter%Fend  = F
             Ptip(:)            = Pt(:, i)
          ENDIF
          IF(OptVariable%getBetter==2)THEN
             OptParameter%Isucc = 3
             RETURN
          ENDIF
       ENDDO

       D = 0.d0
       DO i = 1, N
          CALL Optimising_Distance(Pt(:, i),Ptip(:), Di)
          IF(D<Di) D = Di
       ENDDO

       IF(D<OptParameter%Dtol)THEN
          IF(OptParameter%Isucc==1) OptParameter%Isucc = 2
          RETURN
       ENDIF

       DO i = 1,N
          DO j = 1,OptParameter%Ndim
             rj1 = Random_Gauss(idum)
             rj2 = Random_Gauss(idum)
             v(j,i)  = ev*v(j,i) + e1*( rj1*(Psh(j,i)-Pt(j,i)) + rj2*(Ptip(j)-Pt(j,i)) )
             Pt(j,i) = Pt(j,i) + v(j,i)     
          ENDDO
       ENDDO

    ENDDO

    RETURN
  END SUBROUTINE Optimising_PSO

  !>
  !!          Cuckoo Optimisation.
  !!
  !!    @param[in]  OptParameter%Ndim       the number of dimensions.
  !!    @param[in]  OptParameter%Dtol       the convergence tolerance in term of the distance between particles. 
  !!    @param[in]  OptParameter%IterLimit  the maximum number of iteration.
  !!    @param[in]  N                       the number of particles.
  !!    @param[in]  P0                      the original position of all particles.
  !!    @param[out] Ptip                    the final (best) position.
  !!    @param[out] OptParameter%Iter       the number of iteration.
  !!    @param[out] OptParameter%Fend       the final fitness (repect of Ptip).
  !!    @param[out] OptParameter%Isucc      = 0, No solution in domain.                         \n
  !!                                        = 1, found soultion.                                \n
  !!                                        = 3, position found satisfied the target (set by Optimising_GetValue()).
  !<
  SUBROUTINE Optimising_Cuckoo(N, P0, Ptip)
    IMPLICIT NONE

    INTEGER, INTENT(IN) :: N
    REAL*8,  INTENT(IN),  DIMENSION(OptParameter%Ndim,N)  :: P0
    REAL*8,  INTENT(OUT), DIMENSION(OptParameter%Ndim)    :: Ptip

    REAL*8, PARAMETER :: A=1.0           !  Max step size	
    REAL*8, PARAMETER :: pa = 0.75       !  Fraction of nests replaced
    INTEGER :: NoTop, randNest,randNest2 !  number on top, random nest

    REAL*8, DIMENSION(OptParameter%Ndim,N) :: Pt     ! Current nest positions
    REAL*8, DIMENSION(N)   :: Qu                     ! Current fitness 
    REAL*8, DIMENSION(OptParameter%Ndim)   :: ptemp  ! Temp vector to hold co-ordinates 
    REAL*8, DIMENSION(OptParameter%Ndim)   :: dx     ! Temp vector to hold levy steps
    INTEGER :: I, J, Loop  ! Counters
    REAL*8, PARAMETER :: gold  = 0.5*(1.0+(5.0**(1.0/2.0)))
    REAL*8  :: aa, goldp, F   ! local step size, random number, golden ratio, distance, random number
    INTEGER, SAVE :: idum = -1


    OptParameter%Method = 3
    OptParameter%Isucc  = 0
    OptParameter%Iter   = 0

    ! Calculate the number of nests to discard and number of nests which are in the crossover group
    NoTop = NINT(N*(1-pa))

    ! Calculate fitness for the initial nests
    Pt = P0    ! Set current nests to initial nests
    DO I = 1,N
       CALL Optimising_GetValue(Pt(:,I), Qu(I))
       IF (OptVariable%getBetter==2) THEN
          Ptip(:)            = Pt(:,I)
          OptParameter%Fend  = Qu(I)
          OptParameter%Isucc = 3
          RETURN
       ENDIF
    ENDDO

    !  Sort by fitness

    CALL bubble_sort(Qu,Pt,N,OptParameter%Ndim)

    OptParameter%Iter = 1


    !  Iteration over all generations
    DO Loop = 2, OptParameter%IterLimit
       OptParameter%Iter = Loop

       ! Calculate local step size
       aa = A/(Loop**(1.0/2.0))

       ! Iteration over each Cuckoo which has been discarded
       DO I = NoTop+1, N

          ! find new co-ordinates by levy steps
          CALL Random_LevyWalk(dx,OptParameter%Ndim)
          ptemp(:) = aa*dx(:) + Pt(:, I)        

          ! Call objective function
          OptVariable%F = Qu(I)
          CALL Optimising_GetValue(ptemp, F)
          IF (OptVariable%getBetter>0) THEN
             Pt(:, I) = ptemp(:)
             Qu(I)    = F
             IF (OptVariable%getBetter==2) THEN
                Ptip(:)            = ptemp(:)
                OptParameter%Fend  = F
                OptParameter%Isucc = 3
                RETURN
             ENDIF
             OptParameter%Isucc = 1
          ENDIF

       ENDDO

       ! Calculate local step size
       aa = A/(Loop**(2.0))

       ! Iteration over top nests
       DO I = 1,NoTop

          ! Pick a random nest to cross with

          randNest = Random_Gauss_INT(idum,1,NoTop)

          ! Check to see if the same nest has been picked twice
          IF (randNest == I) THEN

             ! find new co-ordinates by levy steps
             CALL Random_LevyWalk(dx,OptParameter%Ndim)
             ptemp(:) = aa*dx(:) + Pt(:,I)

          ELSE
             IF (Qu(randNest) > Qu(I)) THEN
                !  Search in direction of I by golden ratio
                goldp = 1.d0/gold
             ELSE IF (Qu(I) > Qu(randNest)) THEN
                !  Search in direction of randNest by golden ratio
                goldp = 1.d0 - 1.d0/gold
             ELSE
                ! Search midpoint
                goldp = 0.5d0
             ENDIF
             ptemp(:) = goldp * Pt(:,I) + (1.d0-goldp) * Pt(:,randNest)
          ENDIF

          ! Call objective function
          randNest2 = Random_Gauss_INT(idum,1,N)
          OptVariable%F = Qu(randNest2)
          CALL Optimising_GetValue(ptemp, F)
          IF (OptVariable%getBetter>0) THEN
             ! Pick another random nest to cross with

             Pt(:,randNest2) = ptemp(:)
             Qu(randNest2)   = F	

             IF (OptVariable%getBetter==2) THEN
                Ptip(:)            = ptemp(:)
                OptParameter%Fend  = F
                OptParameter%Isucc = 3
                RETURN
             ENDIF
             OptParameter%Isucc = 1
          ENDIF
       ENDDO

       !  Sort by fitness
       CALL bubble_sort(Qu,Pt,N,OptParameter%Ndim)       

    ENDDO

    Ptip(:)           = Pt(:,1)
    OptParameter%Fend = Qu(1)

    RETURN
  END SUBROUTINE Optimising_Cuckoo

END MODULE OptimisingModule


!  Subroutine to sort an array (Pt) by fitness (Qu) 
!    - simple bubble sort is fine due to small array which will almost be sorted

SUBROUTINE bubble_sort(Qu,Pt,N,K)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: N,K  ! length and dimensions of array
  REAL*8, INTENT(INOUT), DIMENSION(N)   :: Qu  ! array to sort
  REAL*8, INTENT(INOUT), DIMENSION(K,N) :: Pt  ! array to sort
  REAL*8, DIMENSION(K) :: temp1  ! Temporary variables to help with swapping
  REAL*8  :: t1
  INTEGER :: I  ! Counter
  LOGICAL :: CarryOn  ! Completion tests

  CarryOn = .TRUE.  

  DO WHILE (CarryOn)

     CarryOn = .FALSE.

     DO I = 1,N-1
        IF (Qu(I) > Qu(I+1)) THEN
           ! Swap I and I+1
           t1      = Qu(I)
           Qu(I)   = Qu(I+1)
           Qu(I+1) = t1

           temp1(:)  = Pt(:,I)
           Pt(:,I)   = Pt(:,I+1) 
           Pt(:,I+1) = temp1(:)
           CarryOn = .TRUE.
        ENDIF
     ENDDO

  ENDDO

END SUBROUTINE bubble_sort




