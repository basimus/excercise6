!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!    A collection of functions for module SurfaceMeshStorage.
!<
MODULE SurfaceMeshManager
  USE array_allocator
  USE CellConnectivity
  USE SurfaceMeshStorage
  USE NodeNetTree
  USE Geometry2D
  USE Geometry3DAll
  USE Number_Char_Transfer
  USE PtADTreeModule
  USE BoxADTreeModule
  USE SurfaceMeshGeometry
  IMPLICIT NONE

CONTAINS


  !>
  !!    Merge two untouching surface into one.
  !<
  SUBROUTINE Surf_Merge(Surf1, Surf2, Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN)    :: Surf1, Surf2
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER :: it, ip

    CALL SurfaceMeshStorage_Clear(Surf)
    Surf%NB_Tri    = Surf1%NB_Tri    + Surf2%NB_Tri
    Surf%NB_Point  = Surf1%NB_Point  + Surf2%NB_Point
    Surf%NB_Seg    = Surf1%NB_Seg    + Surf2%NB_Seg
    Surf%NB_Surf   = Surf1%NB_Surf   + Surf2%NB_Surf
    Surf%NB_Sheet  = Surf1%NB_Sheet  + Surf2%NB_Sheet
    ALLOCATE(Surf%IP_Tri(5,Surf%NB_Tri))
    ALLOCATE(Surf%Posit( 3,Surf%NB_Point))
    DO it = 1,Surf1%NB_Tri
       Surf%IP_Tri(:,it) = Surf1%IP_Tri(:,it)
    ENDDO
    DO it = 1,Surf2%NB_Tri
       Surf%IP_Tri(1:3,it+Surf1%NB_Tri) = Surf2%IP_Tri(1:3,it) + Surf1%NB_Point
       Surf%IP_Tri(4,  it+Surf1%NB_Tri) = Surf2%IP_Tri(4,  it)
       Surf%IP_Tri(5,  it+Surf1%NB_Tri) = Surf2%IP_Tri(5,  it) + Surf1%NB_Surf
    ENDDO
    DO ip = 1,Surf1%NB_Point
       Surf%Posit(:,ip) = Surf1%Posit(:,ip)
    ENDDO
    DO ip = 1,Surf2%NB_Point
       Surf%Posit(:,ip+Surf1%NB_Point) = Surf2%Posit(:,ip)
    ENDDO
    RETURN
  END SUBROUTINE Surf_Merge

  !>
  !!  Check the geometry and connection of a surface triangulation.
  !<
  SUBROUTINE Surf_Check(Surf)   
    IMPLICIT NONE

    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER  :: nas,nPEC
    INTEGER, DIMENSION(:,:), POINTER :: Edge_p   !---(2,1)
    INTEGER, DIMENSION(:,:), POINTER :: Edge_t   !---(2,1)
    REAL*8,  DIMENSION(:,:), POINTER :: DIR_TriSur !---(3,1)
    INTEGER, DIMENSION(:  ), POINTER :: Mark
    INTEGER  :: IEdge, nside,ip1,ip2,ip3,i,j,k,ns,nb1,nb2,nbm,ic, ips(4)
    INTEGER  :: iemax,iemin,ipmax,ipmin,nl(300),ns_conn(300), nsin
    INTEGER  :: ip,ie,np,npp,nb,nbb,In,nel, ip10(10), itL
    REAL*8   :: x1,x2,y1,y2,z1,z2
    REAL*8   :: x21,y21,z21,x31,y31,z31,x32,y32,z32
    REAL*8   :: vprx,vpry,vprz,vmod
    REAL*8   :: distt(3), hminl, hmin, hmax, tolg
    INTEGER  :: npiece, Sur_Piece(Surf_MaxNB_Surf), Psur(Surf_MaxNB_Surf)
    CHARACTER (LEN=512) :: numchar
    INTEGER  :: npair, npair3, kpair(2), kpair3(3), kpairs(2,Surf_MaxNB_Surf),kpairs3(3,Surf_MaxNB_Surf)
    INTEGER  :: error_out0, error_out1, error_out2,error_out3, error_out4, error_out5
    TYPE(NodeNetTreeType) :: Edge_Tree

    INTEGER :: nfrnt, ifrnt(100), ntype, nfacl
    REAL*8  :: pp2(3,2), pp3(3,3), weight(3), t, p(3)
    REAL*8  :: pa(3), pb(3), pMax(3), pMin(3), xel(6), d 
    TYPE (BoxADTreeType)   :: BoxADTree
    TYPE (NodeNetTreeType) :: TriTree
    CHARACTER*3 :: cp(200)
    REAL*8  :: Va, Vamax, Vamin, Vab, Vamm, Co1(2), Pts(3,10), Vt(3), Vtp(3,2)

    error_out0 = 1
    error_out1 = 2
    error_out2 = 3
    error_out3 = 2
    error_out4 = 2
    error_out5 = 3

    CALL SurfaceMeshStorage_Info(Surf)

    IF(Surf%NB_Surf>Surf_MaxNB_Surf)THEN
       WRITE(*,*)' Error ---- Surf%NB_Surf>Surf_MaxNB_Surf'
       CALL Error_Stop ('Surf_Check::')
    ENDIF

    WRITE(*,*)' '
    WRITE(*,*)'The program will check follows:'
    WRITE(*,*)'.... The validity of entries.'
    WRITE(*,*)'.... The orientation;' 
    WRITE(*,*)'.... If two connected triangles have opposite orientations;'
    WRITE(*,*)'.... The number of boundary edges if exsiting;'
    WRITE(*,*)'.... If there exsits a edge appearing in more than two triangles;'
    WRITE(*,*)'.... The maximum/minimum height and included angle;'
    WRITE(*,*)'.... The min dihedral angle by two connecting triangles. '     &
         // '(If this is close to 0 unexpectedly, there might exist overlapping triangles.) '
    WRITE(*,*)'.... If there exsit identical nodes by position.'
    WRITE(*,*)'.... If there exsit surface intersection.'
    WRITE(*,*)' '

    !--- check connectivity
    DO ie=1,Surf%NB_Tri
       IF((Surf%IP_Tri(5,ie)<=0 .OR. Surf%IP_Tri(5,ie)>Surf%NB_Surf) .AND.   &
            error_out0>0) THEN
          error_out0 = error_out0-1
          WRITE(*,*)' '
          WRITE(*,*)'Warning!---- odd surface number: '
          WRITE(*,*)'ie=',ie,' (5,ie)=',Surf%IP_Tri(5,ie), ' NB_Surf=',Surf%NB_Surf
       ENDIF
       DO i=1,3
          IF(Surf%IP_Tri(i,ie)<=0 .OR. Surf%IP_Tri(i,ie)>Surf%NB_Point)THEN
             WRITE(*,*)'Error--- wrong connectivity'
             WRITE(*,*)'ie=',ie,' ips=',Surf%IP_Tri(1:3,ie)
             CALL Error_STOP ( '--- Surf_Check')
          ENDIF
       ENDDO
    ENDDO

    !--- check orientation
    nPEC=Surf%NB_Surf-Surf%NB_Sheet
    CALL Surf_orient(Surf,nPEC,nb)

    nbm=Surf%NB_Surf-Surf%NB_Sheet
    DO ic=1,Surf%NB_Sheet
       nbm=nbm+1
       Psur(ic)=nbm
    ENDDO
    numchar=IntArray_to_Char(Psur,ic-1, i)
    WRITE(*,'(a)')'Interface sheet consists of surface: '//numchar(1:i)

    !--- check range
    CALL Surf_count(Surf,nPEC,npiece,Sur_Piece)

    WRITE(*,*)
    DO nb=0,npiece

       x1 = 1.e20
       x2 = -x1
       y1 =  x1
       y2 = -x1
       z1 =  x1
       z2 = -x1
       DO nbm=1,Surf%NB_Tri
          IF(Sur_Piece(Surf%IP_Tri(5,nbm))==nb) THEN
             DO i=1,3
                IF(x1>Surf%Posit(1,Surf%IP_Tri(i,nbm))) x1=Surf%Posit(1,Surf%IP_Tri(i,nbm))
                IF(x2<Surf%Posit(1,Surf%IP_Tri(i,nbm))) x2=Surf%Posit(1,Surf%IP_Tri(i,nbm))
                IF(y1>Surf%Posit(2,Surf%IP_Tri(i,nbm))) y1=Surf%Posit(2,Surf%IP_Tri(i,nbm))
                IF(y2<Surf%Posit(2,Surf%IP_Tri(i,nbm))) y2=Surf%Posit(2,Surf%IP_Tri(i,nbm))
                IF(z1>Surf%Posit(3,Surf%IP_Tri(i,nbm))) z1=Surf%Posit(3,Surf%IP_Tri(i,nbm))
                IF(z2<Surf%Posit(3,Surf%IP_Tri(i,nbm))) z2=Surf%Posit(3,Surf%IP_Tri(i,nbm))
             ENDDO
          ENDIF
       ENDDO

       IF(nb==0 .AND. x1<1.e20)THEN
          WRITE(*,'(a)')      &
               ' Sheet Surface range (Xmin, Xmax, Ymin Ymax, Zmin Zmax) '
          WRITE(*,'(1x,6(1x,E11.4))') x1,x2,y1,y2,z1,z2
       ELSE IF(nb>0)THEN
          WRITE(*,'(a,i3)')      &
               ' PEC Surface range (Xmin, Xmax, Ymin Ymax, Zmin Zmax) for Piece ',nb
          WRITE(*,'(1x,6(1x,E11.4))') x1,x2,y1,y2,z1,z2
       ENDIF
    ENDDO

    WRITE(*,*)

    !--- extra point checking
    ALLOCATE(Mark(Surf%NB_Point))
    Mark(1:Surf%NB_Point) = 0
    DO nbm=1,Surf%NB_Tri
       Mark(Surf%IP_Tri(1:3,nbm))=1
       IF(Surf%GridOrder==3)THEN
          WHERE(Surf%IPsp_Tri(:,nbm)>0) Mark(Surf%IPsp_Tri(:,nbm))=1
       ENDIF
    ENDDO
    np = 0
    DO ip= 1,Surf%NB_Point
       IF(Mark(ip)==0) np = np+1
       IF(np==1) npp = ip
    ENDDO
    IF(np>0)THEN
       PRINT*,'extra points (not on a triangle) existing----: ip=',npp
       PRINT*,'total number of extra points is ',np
       PRINT*,' '
    ENDIF

    !--- edge checking
    WRITE(*,*) '--- edge checking....'
    CALL allc_2Dpointer(edge_p, 4,20*Surf%NB_Point,    'EdgeQue')
    CALL allc_2Dpointer(edge_t, 2,20*Surf%NB_Point,    'EdgeQue')
    CALL NodeNetTree_Allocate(2, Surf%NB_Point, 20*Surf%NB_Point, Edge_Tree)

    nside=0
    npair=0
    npair3=0

    DO ie =1,Surf%NB_Tri
       DO i=1,3
          ip1=Surf%IP_Tri(i,ie)
          ip2=Surf%IP_Tri(MOD(i,3)+1,ie)
          ips(1:2) = (/ip1,ip2/)
          CALL NodeNetTree_SearchAdd(2,ips(1:2),Edge_Tree,IEdge)
          IF(IEdge>nside)THEN
             !-- new edge

             Edge_p(1,IEDge)=ip1
             Edge_p(2,IEDge)=ip2
             Edge_t(1,IEDge)=ie
             Edge_t(2,IEDge)=0
             nside = IEdge
          ELSE IF(Edge_t(2,IEDge)==0) THEN
             !-- edge appearing second time

             IF(Edge_p(1,IEDge)==ip2 .AND. Edge_p(2,IEDge)==ip1) THEN
                !-- matched edge
                Edge_t(2,IEDge)=ie
             ELSE
                !-- unmatched edge
                IF(Surf%IP_Tri(5,ie)==Surf%IP_Tri(5,Edge_t(1,IEDge)) .AND. error_out1>0)THEN
                   error_out1 = error_out1-1
                   PRINT*,'Warning!---- Adjacent triangles on the same '   &
                        //'surface take different orientations'
                   PRINT*,'tri1=',ie,' tri2=',Edge_t(1,IEDge), 'ip1,ip2=',ip1,ip2
                ENDIF
                kpair(1)=MIN(Surf%IP_Tri(5,ie),Surf%IP_Tri(5,Edge_t(1,IEDge)))
                kpair(2)=MAX(Surf%IP_Tri(5,ie),Surf%IP_Tri(5,Edge_t(1,IEDge)))
                DO nb=1,npair
                   IF(kpair(1)==kpairs(1,nb).AND.kpair(2)==kpairs(2,nb)) GOTO 10
                ENDDO
                npair=npair+1
                kpairs(:,npair)=kpair(:)
                WRITE(*,'(a,i4,a,i4,a)')'Warning!--- Surface',kpair(1),     &
                     ' and', kpair(2),' have different orientations'
10              CONTINUE                                
                Edge_t(2,IEDge)=-ie
             ENDIF

          ELSE IF(error_out2>0)THEN
             error_out2 = error_out2-1
             !-- edge appearing more than twrice

             kpair3(1)=MIN(Surf%IP_Tri(5,ie),Surf%IP_Tri(5,Edge_t(1,IEDge)),   &
                  Surf%IP_Tri(5,ABS(Edge_t(2,IEDge))))
             kpair3(3)=MAX(Surf%IP_Tri(5,ie),Surf%IP_Tri(5,Edge_t(1,IEDge)),   &
                  Surf%IP_Tri(5,ABS(Edge_t(2,IEDge))))
             kpair3(2)=Surf%IP_Tri(5,ie)+Surf%IP_Tri(5,Edge_t(1,IEDge))   &
                  +Surf%IP_Tri(5,ABS(Edge_t(2,IEDge)))-kpair3(1)-kpair3(3)
             DO nb=1,npair3
                IF(kpair3(1)==kpairs3(1,nb).AND.kpair3(2)==kpairs3(2,nb)   &
                     .AND.kpair3(3)==kpairs3(3,nb) ) GOTO 20
             ENDDO
             npair3=npair3+1
             kpairs3(:,npair3)=kpair3(:)
             WRITE(*,'(a,2i4,a,i4,a)')'Reminder--- Surface',kpair3(1),     &
                  kpair3(2), ' and', kpair3(3),' share the same edge'
             WRITE(*,'(a,3i9,a,2i9)')' with sample trangles ',ie,Edge_t(1,IEDge),  &
                  ABS(Edge_t(2,IEDge)),' and nodes', ip1,ip2

20           CONTINUE
             kpair3(:)=0
             IF(Edge_p(1,IEDge)==ip1 .AND. Edge_p(2,IEDge)==ip2)THEN
                kpair3(1)=Edge_t(1,IEDge)
                IF(Edge_t(2,IEDge)<0) kpair3(2)=ABS(Edge_t(2,IEDge))
             ELSE
                IF(Edge_t(2,IEDge)>0) kpair3(2)=ABS(Edge_t(2,IEDge))
             ENDIF
             DO nbm=1,2
                IF(kpair3(nbm)==0) CYCLE
                kpair(1)=MIN(Surf%IP_Tri(5,ie),Surf%IP_Tri(5,kpair3(nbm)))
                kpair(2)=MAX(Surf%IP_Tri(5,ie),Surf%IP_Tri(5,kpair3(nbm)))
                DO nb=1,npair
                   IF(kpair(1)==kpairs(1,nb).AND.kpair(2)==kpairs(2,nb)) GOTO 30
                ENDDO
                npair=npair+1
                kpairs(:,npair)=kpair(:)
                WRITE(*,'(a,i4,a,i4,a)')'Warning!--- Surface',kpair(1),     &
                     ' and', kpair(2),' have different orientations'
30              CONTINUE                      
             ENDDO

          ENDIF
       ENDDO
    ENDDO

    !--- high order edge check
    IF(Surf%GridOrder==3)THEN
       Edge_p(3:4, :) = -1
       DO ie =1,Surf%NB_Tri
          DO i=1,3
             ips =  Surf%IPsp_Tri(iEdge_C3Tri(:,i),ie)
             IF( (ips(3)==0 .AND. ips(4)/=0) .OR. (ips(3)/=0 .AND. ips(4)==0) )THEN
                WRITE(*,*) ' '
                WRITE(*,*)' Error--- confused High-order Edge   '
                WRITE(*,*)' ips=',ips, ' ie=',ie
                STOP
             ENDIF
             IF(ips(3)==0 .OR. ips(4)==0) CYCLE

             CALL NodeNetTree_Search(2,ips(1:2),Edge_Tree,IEdge)
             IF(Edge_p(3,IEdge)==-1)THEN
                Edge_p(3:4,IEdge) = ips(3:4)
             ENDIF
             IF(  (Edge_p(3,IEdge)/=ips(3) .AND. Edge_p(3,IEdge)/=ips(4)) .OR.  &
                  (Edge_p(4,IEdge)/=ips(3) .AND. Edge_p(4,IEdge)/=ips(4)) )THEN
                WRITE(*,*) ' '
                WRITE(*,*)' Error--- Twin High-order Edge  '                
                WRITE(*,*)'  ie=', ie, ' it2=',Edge_t(:,IEdge)
                WRITE(*,*)'  ips=',ips, ' if=',Surf%IP_Tri(5,ie)
                WRITE(*,*)'  ipt=',Edge_p(:,IEdge), ' if=',Surf%IP_Tri(5,Edge_t(1,IEdge))                
                STOP
             ENDIF
          ENDDO
       ENDDO
    ENDIF


    !---    Unit outward normal direction and height of face                                      
    CALL allc_2Dpointer(Dir_TriSur, 3,Surf%NB_Tri, 'allocate Dir_TriSur')

    hmin = 1.e12
    hmax = 0
    DO nb = 1,Surf%NB_Tri                                                     
       ip1 = Surf%IP_Tri(1,nb)                                                      
       ip2 = Surf%IP_Tri(2,nb)                                                      
       ip3 = Surf%IP_Tri(3,nb)
       x21 = Surf%Posit(1,ip2) - Surf%Posit(1,ip1)                                      
       y21 = Surf%Posit(2,ip2) - Surf%Posit(2,ip1)                                      
       z21 = Surf%Posit(3,ip2) - Surf%Posit(3,ip1)                                      
       x31 = Surf%Posit(1,ip3) - Surf%Posit(1,ip1)                                      
       y31 = Surf%Posit(2,ip3) - Surf%Posit(2,ip1)                                      
       z31 = Surf%Posit(3,ip3) - Surf%Posit(3,ip1)                                      
       x32 = Surf%Posit(1,ip3) - Surf%Posit(1,ip2)                                      
       y32 = Surf%Posit(2,ip3) - Surf%Posit(2,ip2)                                      
       z32 = Surf%Posit(3,ip3) - Surf%Posit(3,ip2)   
       vprx = y31*z21 - y21*z31                                               
       vpry = z31*x21 - z21*x31                                               
       vprz = x31*y21 - x21*y31                                               
       vmod = dsqrt( vprx*vprx + vpry*vpry + vprz*vprz )                       
       Dir_TriSur(1,nb) = vprx/vmod                                                
       Dir_TriSur(2,nb) = vpry/vmod                                                
       Dir_TriSur(3,nb) = vprz/vmod 

       distt(1)=vmod/SQRT(x21**2+y21**2+z21**2)
       distt(2)=vmod/SQRT(x32**2+y32**2+z32**2)
       distt(3)=vmod/SQRT(x31**2+y31**2+z31**2)

       hminl = MIN(distt(1),distt(2),distt(3))
       hmax  = MAX(hmax,distt(1),distt(2),distt(3))
       IF(hmin>hminl)THEN
          hmin=hminl
          nb1=nb
       ENDIF
    ENDDO

    tolg = hmin
    WRITE(*,*)
    WRITE(*,*)' The max height of triangle: ',REAL(hmax)
    WRITE(*,*)' The min height of triangle: ',REAL(hmin)
    WRITE(*,'(a,i8,a,i5)')' The triangle with min height: ',nb1,     &
         ' in the surface no: ',Surf%IP_Tri(5,nb1)
    WRITE(*,'(a,3i9)')'    with nodes: ',(Surf%IP_Tri(i,nb1),i=1,3)
    ip1 = Surf%IP_Tri(1,nb1)
    ip2 = Surf%IP_Tri(2,nb1)
    ip3 = Surf%IP_Tri(3,nb1)
    WRITE(*,'(a,3e14.6)')'XYZ(:,ip1)=',(Surf%Posit(in,ip1),in=1,3)
    WRITE(*,'(a,3e14.6)')'XYZ(:,ip2)=',(Surf%Posit(in,ip2),in=1,3)
    WRITE(*,'(a,3e14.6)')'XYZ(:,ip3)=',(Surf%Posit(in,ip3),in=1,3)

    hmin = 1.e12
    nbm  = 0
    DO ie=1,nside
       nb1=Edge_t(1,ie)
       nb2=Edge_t(2,ie)
       IF(nb2/=0)THEN
          IF(nb2>0)THEN
             vprx = Dir_TriSur(1,nb1)+Dir_TriSur(1,nb2)
             vpry = Dir_TriSur(2,nb1)+Dir_TriSur(2,nb2)
             vprz = Dir_TriSur(3,nb1)+Dir_TriSur(3,nb2)
          ELSE
             vprx = Dir_TriSur(1,nb1)-Dir_TriSur(1,-nb2)
             vpry = Dir_TriSur(2,nb1)-Dir_TriSur(2,-nb2)
             vprz = Dir_TriSur(3,nb1)-Dir_TriSur(3,-nb2)
          ENDIF
          vmod = vprx*vprx + vpry*vpry + vprz*vprz
          IF(hmin>vmod)THEN
             hmin=vmod
             ns = ie
          ENDIF
       ELSE
          nbm = nbm+1
          IF(nbm==1) PRINT*,'boundary edges existing----: ie, ip1,ip2, itri='
          IF(nbm<5) PRINT*,ie,Edge_p(1,ie),Edge_p(2,ie),nb1
          IF(nbm==5) PRINT*,' '
       ENDIF
    ENDDO

    IF(nbm>0) WRITE(*,*)'Number of boundary Edges: ',nbm

    hmin = 180/3.14159265*ACOS(1.-hmin/2.)
    nb1 = Edge_t(1,ns)
    nb2 = ABS(Edge_t(2,ns))
    ip1 = ABS(Edge_p(1,ns))
    ip2 = ABS(Edge_p(2,ns))
    WRITE(*,*)
    WRITE(*,'(a,F11.5,a)')     &
         'The min dihedral angle by two connecting triangles:',hmin,' deg'
    WRITE(*,*)'These triangles are: ',nb1,nb2     
    WRITE(*,*)'    in the surfaces no: ',Surf%IP_Tri(5,nb1),Surf%IP_Tri(5,nb2)
    WRITE(*,*)'    with nodes: ',(Surf%IP_Tri(in,nb1),in=1,3)
    WRITE(*,*)'    with nodes: ',(Surf%IP_Tri(in,nb2),in=1,3)
    WRITE(*,'(a,3e14.6)')'XYZ(:,ip1)=',(Surf%Posit(in,ip1),in=1,3)
    WRITE(*,'(a,3e14.6)')'XYZ(:,ip2)=',(Surf%Posit(in,ip2),in=1,3)


    !--- nodes links check
    WRITE(*,*) '--- node-triangle links check....'
    CALL Surf_BuildTriAsso (Surf)

    iemax = 0
    iemin = 1000
    DO i=1,Surf%NB_Point
       nb = Surf%ITR_Pt%JointLinks(i)%numNodes
       IF(nb>iemax)THEN
          iemax = nb
          ipmax = i
       ENDIF
       IF(nb<iemin)THEN
          iemin = nb
          ipmin = i
       ENDIF
       IF(nb>100)THEN
          nb = 100
       ELSE IF(nb>80)THEN
          nb = 80
       ELSE IF(nb>60)THEN
          nb = 60
       ELSE IF(nb>40)THEN
          nb = 40
       ENDIF
       ns_conn(nb) = ns_conn(nb) +1
    ENDDO
    cp(1:39)   = ' '
    cp(40)     = '~59'
    cp(60)     = '~79'
    cp(80)     = '~99'
    cp(100)    = '+  '

    WRITE(*,*)' node ',ipmin,' has minimum number of elements: ',iemin
    IF(iemin==1 .OR. iemin==2)THEN
       ie = Surf%ITR_Pt%JointLinks(ipmin)%nodes(1)
       WRITE(*,*)' it1=',ie, Surf%IP_Tri(:,ie)
    ENDIF
    IF(iemin==2)THEN
       ie = Surf%ITR_Pt%JointLinks(ipmin)%nodes(2)
       WRITE(*,*)' it2=',ie, Surf%IP_Tri(:,ie)
    ENDIF
    WRITE(*,*)' node ',ipmax,' has maximum number of elements: ',iemax
    WRITE(*,*)'no._associated_cells : no._nodes |'
    ns = 0
    DO i=1,200
       IF(ns_conn(i)>0)THEN
          ns = ns+1
          nl(ns) = i
       ENDIF
    ENDDO
    nsin = (ns+3)/4
    DO i=1,nsin
       ips(1:4) = (/i, i+nsin, i+2*nsin, i+3*nsin/)
       nb = 4
       IF(i+3*nsin>ns) nb = 3
       ips(1:nb) = nl(ips(1:nb))
       WRITE(*,'(4(i3,a3,I9,a))') (ips(j),cp(ips(j)),ns_conn(ips(j)),' | ',j=1,nb)       
    ENDDO


    !--- identical nodes check
    WRITE(*,*) '--- identical nodes check....'
    CALL PtADTree_IdentifyNodes(Surf%NB_Point, Surf%Posit, tolg, ns, Mark)
    IF(ns>0)THEN
       WRITE(*,*)'Warning!--- There are ',ns, ' extra (idential) nodes, like:'
       DO ip = 1, Surf%NB_Point
          IF(Mark(ip)<ip .AND. error_out5>0)THEN
             WRITE(*,*)ip, ' ----- ', Mark(ip)
             error_out5 = error_out5 - 1
          ENDIF
       ENDDO
    ENDIF


    ! *** find the closest distance between a point to a triangle
    WRITE(*,*) '---- find the closest distance between a point to a triangle......'

    CALL SurfaceMeshStorage_Measure (Surf, 3, pMin, pMax)
    CALL BoxADTree_SetTreeDomain (BoxADTree, pMin, pMax)
    CALL NodeNetTree_Allocate (3, Surf%NB_Point, Surf%NB_Tri, TriTree)

    DO nb = 1 , Surf%NB_Tri
       ip1 = Surf%IP_Tri(1,nb)
       ip2 = Surf%IP_Tri(2,nb)
       ip3 = Surf%IP_Tri(3,nb)          
       pMin(:) = MIN(Surf%Posit(:,ip1),Surf%Posit(:,ip2),Surf%Posit(:,ip3))
       pMax(:) = MAX(Surf%Posit(:,ip1),Surf%Posit(:,ip2),Surf%Posit(:,ip3))
       CALL BoxADTree_AddNode(BoxADTree, nb, pMin, pMax)
    ENDDO

    hmin = 1.2*tolg
    nbb  = 0
    DO IP = 1 , Surf%NB_Point - Surf%NB_Psp

       ! *** Determine the box inside which intersection is checked
       pa(:) = Surf%Posit(:,IP)
       xel(1:3) = pa(:)-hmin
       xel(4:6) = pa(:)+hmin
       nfacl = 100
       CALL BoxADTree_SearchNode (BoxADTree, nfacl, ifrnt, xel(1:3), xel(4:6))

       DO i = 1 , nfacl
          nb  = ifrnt(i)
          ip1 = Surf%IP_Tri(1,nb)
          ip2 = Surf%IP_Tri(2,nb)
          ip3 = Surf%IP_Tri(3,nb)          
          IF(IP == ip1 .OR. IP == ip2 .OR. IP == ip3) CYCLE 

          pp3(:,1) = Surf%Posit(:,ip1)
          pp3(:,2) = Surf%Posit(:,ip2)
          pp3(:,3) = Surf%Posit(:,ip3)
          d  = Plane3D_DistancePointToTriangle (pa, pp3(:,1),pp3(:,2),pp3(:,3))

          IF(hmin > d) THEN
             hmin = d
             ic   = ip
             nbb  = nb
          ENDIF
       ENDDO
    ENDDO

    IF(nbb>0 .AND. hmin<=1.0001*tolg)THEN
       WRITE(*,*) ' '
       WRITE(*,*) 'The min distance from a node to a triangle: ', REAL(hmin)
       WRITE(*,*) '# Node:'
       WRITE(*,*)REAL(Surf%Posit(:,ic)), ic
       WRITE(*,*) '# triangle :',nbb
       WRITE(*,*)REAL(Surf%Posit(:,Surf%IP_Tri(1,nbb))), Surf%IP_Tri(1,nbb)
       WRITE(*,*)REAL(Surf%Posit(:,Surf%IP_Tri(2,nbb))), Surf%IP_Tri(2,nbb)
       WRITE(*,*)REAL(Surf%Posit(:,Surf%IP_Tri(3,nbb))), Surf%IP_Tri(3,nbb)
    ELSE
       WRITE(*,*) ' Warning--- only if the very minimum height appears near boundary'
    ENDIF

    !---- Check if an edge intersect with a triangle
    WRITE(*,*) '---- Check if an edge intersect with a triangle......'

    DO IEDge = 1 , nside

       IP = Edge_p(1,IEDge)
       NP = Edge_p(2,IEDge)
       pa(:) = Surf%Posit(:,IP)
       pb(:) = Surf%Posit(:,NP)
       xel(1:3) = MIN(pa(:),pb(:))
       xel(4:6) = MAX(pa(:),pb(:))
       nfacl = 100
       CALL BoxADTree_SearchNode (BoxADTree, nfacl, ifrnt, xel(1:3), xel(4:6))

       DO i = 1 , nfacl
          nb = ifrnt(i)
          ip1 = Surf%IP_Tri(1,nb)
          ip2 = Surf%IP_Tri(2,nb)
          ip3 = Surf%IP_Tri(3,nb)          
          IF(IP == ip1 .OR. IP == ip2 .OR. IP == ip3) CYCLE 
          IF(NP == ip1 .OR. NP == ip2 .OR. NP == ip3) CYCLE 

          pp3(:,1) = Surf%Posit(:,ip1)
          pp3(:,2) = Surf%Posit(:,ip2)
          pp3(:,3) = Surf%Posit(:,ip3)
          pp2(:,1) = pa(:)
          pp2(:,2) = pb(:)

          ntype = 1
          P = Plane3D_Line_Tri_Cross (pp2, pp3, ntype, weight, t)

          IF(ntype>0)THEN
             !--- with intersection
             WRITE(*,*) ' '
             WRITE(*,*)' Error--- Edge intersecting with triange, ntype= ',ntype
             WRITE(*,*) '# edge :',IEDge
             WRITE(*,*)REAL(pa),IP
             WRITE(*,*)REAL(pb),NP
             WRITE(*,*) '# triangle :',nb
             WRITE(*,*)REAL(pp3(:,1)), ip1
             WRITE(*,*)REAL(pp3(:,2)), ip2
             WRITE(*,*)REAL(pp3(:,3)), ip3
             CALL Error_STOP ('Surf_Check:: ')
          ENDIF
       ENDDO

    ENDDO

    DEALLOCATE(edge_p,  edge_t,  Dir_TriSur, Mark)

    CALL NodeNetTree_Clear (TriTree)
    CALL BoxADTree_Clear (BoxADTree)

    RETURN
  END SUBROUTINE Surf_Check

  !>
  !!     Check the orientation of a enclosed surface.
  !!     @param[in] Surf  : the surface mesh.
  !!     @param[in] nPEC  : the number of out-bound surface (not interface).
  !!     @param[out]  korient  = 1 : orientating inside               \n
  !!                           =-1 : orientating outside              \n
  !!                           = 0 : orientating mixed (more than 1 pieces)  \n
  !<
  SUBROUTINE Surf_orient(Surf,nPEC,korient)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER, INTENT(IN)  :: nPEC
    INTEGER, INTENT(OUT) :: korient
    INTEGER   :: i, i1, i2, i3, ip, npleft, ie, npi, nb, nbm, ic
    INTEGER   :: npiece, Sur_Piece(Surf_MaxNB_Surf), Psur(Surf_MaxNB_Surf)
    REAL*8    :: Xmin, a(3), b(3), aa, bb, vmod
    CHARACTER (LEN=512) ::  numchar


    CALL Surf_count(Surf,nPEC,npiece,Sur_Piece)

    WRITE(*,*)' '
    IF(nPEC>=Surf%NB_Surf)THEN
       WRITE(*,'(a,$)')'The surfaces constitute '
    ELSE
       WRITE(*,'(a,$)')'The PEC surfaces constitute '
    ENDIF
    WRITE(*,'(i2,a)') npiece,' isolated pieces of net.'

    DO nb=1,npiece
       ic=0
       DO nbm=1,Surf%NB_Surf
          IF(Sur_Piece(nbm)==nb) THEN
             ic=ic+1
             Psur(ic)=nbm
          ENDIF
       ENDDO
       IF(ic>0)THEN
          numchar=IntArray_to_char(Psur,ic,i)
          WRITE(*,'(a,i2,a,a)')'  Piece ', nb, ' consists of surface ', numchar(1:i)
       ELSE
          WRITE(*,'(a,i2,a,a)')'  Piece ', nb, ' consists NO surface '
       ENDIF
    ENDDO

    korient = 100
    WRITE(*,*)

    DO npi=1,npiece

       Xmin   = 1.e20
       npleft = 0

       DO ie = 1,Surf%NB_Tri
          IF(Sur_Piece(Surf%IP_Tri(5,ie))==npi)THEN
             DO i=1,3
                ip=Surf%IP_Tri(i,ie)
                IF(Xmin>Surf%Posit(1,ip))THEN
                   npleft = ip
                   Xmin   = Surf%Posit(1,ip)
                ENDIF
             ENDDO
          ENDIF
       ENDDO

       vmod = 0
       DO ie = 1,Surf%NB_Tri
          IF(Sur_Piece(Surf%IP_Tri(5,ie))==npi)THEN
             i1 = which_NodeInTri(npleft, Surf%IP_Tri(1:3,ie))

             IF(i1>0)THEN
                i1 = Surf%IP_Tri(1,ie)
                i2 = Surf%IP_Tri(2,ie)
                i3 = Surf%IP_Tri(3,ie)
                a  = Geo3D_Cross_Product(Surf%Posit(:,i2),Surf%Posit(:,i1),Surf%Posit(:,i3))
                vmod = vmod + a(1) / Geo3D_Distance(a)
             ENDIF
          ENDIF
       ENDDO

       IF(ABS(vmod)<1.e-6)THEN
          PRINT*,'WARNING: Are there any sharp points on left side?'
       ENDIF

       WRITE(*,'(a,i2,a,$)')       &
            'Orientation of surface triangulation in Piece ',npi,':'

       IF(vmod>0)THEN
          PRINT*,' INSIDE.'
       ELSE
          PRINT*,' OUTSIDE.'
       ENDIF

       IF(korient==100)THEN
          IF(vmod>0)THEN
             korient = 1
          ELSE
             korient = -1
          ENDIF
       ELSE
          IF(korient*vmod<=0) korient = 0
       ENDIF

    ENDDO

    RETURN
  END SUBROUTINE Surf_orient

  !>
  !!     Sort surface nodes by clusters.
  !!     @param[in] Surf  : the surface mesh.
  !!     @param[out] ip_order : if ip_order(i)=IP, 
  !!                            then node IP takes the i-th place in the cluster.
  !<
  SUBROUTINE Surf_clusterSort(Surf, ip_order) 
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, DIMENSION(*), INTENT(OUT) :: ip_order
    INTEGER, DIMENSION(:),   POINTER :: Markp
    INTEGER :: i, j, ist, iet,  np
    INTEGER, DIMENSION(:), POINTER :: IpList
    INTEGER ::  List_Length

    ALLOCATE(markp(Surf%NB_Point))
    markp(:) = 0
    ip_order(1:Surf%NB_Point) = 0

    CALL Surf_BuildPtAsso (Surf)

    ip_order(1) = 1
    markp(1) = 1
    ist      = 1
    iet      = 1
    np       = 1
    Loop_3 : DO
       CALL LinkAssociation_List (iet, List_Length, IpList, Surf%IP_Pt)
       DO i = 1 , List_Length
          j = IpList(i)
          IF(markp(j) == 0) THEN
             np = np + 1
             ip_order(np) = j
             markp(j) = 1
          ENDIF
       ENDDO

       IF(np /= ist) THEN
          ist = ist + 1
          iet = ip_order(ist)
          CYCLE Loop_3
       ENDIF

       IF(np == Surf%NB_Point) EXIT Loop_3

       DO i = 1 , Surf%NB_Point
          IF(markp(i) == 0) THEN
             iet       = i
             ist       = ist + 1
             np        = np + 1
             ip_order(np) = i
             markp(i)  = 1
             CYCLE Loop_3
          ENDIF
       ENDDO

    ENDDO Loop_3

    CALL LinkAssociation_Clear (Surf%IP_Pt)
    DEALLOCATE(Markp)

    RETURN
  END SUBROUTINE Surf_clusterSort


  !>
  !!     Count the number of isolated surface
  !!     @param[in] Surf  : the surface mesh.
  !!     @param[in] nPEC  : the number of out-bound surface (not interface).
  !!     @param[out] npiece    : the number of isolated pieces of surfaces.
  !!     @param[out] Sur_Piece : map from surfaces to isolated pieces.
  !<
  SUBROUTINE Surf_count(Surf,nPEC,npiece,Sur_Piece)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER, INTENT(IN)   :: nPEC
    INTEGER, INTENT(OUT)  :: npiece
    INTEGER, INTENT(INOUT)  :: Sur_Piece(*)
    INTEGER   :: np, ic, nb, nbm, nbmerror
    INTEGER   :: Nmax
    INTEGER, DIMENSION(:), POINTER   :: Markp, Marke

    Nmax = MAX(Surf%NB_Tri, Surf%NB_Point) +1
    ALLOCATE (Markp(Nmax), Marke(Nmax))

    !----  count the number of isolated surface

    DO np=1,Surf%NB_Point
       markp(np)=Surf%NB_Tri+1
    ENDDO

    ic=1
    DO WHILE(ic==1)
       ic=0
       DO nb=1,Surf%NB_Tri
          IF(Surf%IP_Tri(5,nb)<=nPEC)THEN
             nbm = MINVAL(markp(Surf%IP_Tri(1:3,nb)))
             nbm = MIN(nbm, nb)
             IF(  markp(Surf%IP_Tri(1,nb))>nbm .OR.       &
                  markp(Surf%IP_Tri(2,nb))>nbm .OR.       &
                  markp(Surf%IP_Tri(3,nb))>nbm     )THEN
                ic=1
                markp(Surf%IP_Tri(1,nb))=nbm
                markp(Surf%IP_Tri(2,nb))=nbm
                markp(Surf%IP_Tri(3,nb))=nbm
             ENDIF
             marke(nb)=nbm
          ELSE
             marke(nb)=0
          ENDIF
       ENDDO
    ENDDO

    DO nb=1,Surf%NB_Tri
       markp(nb)=0
    ENDDO

    npiece = 0
    DO nb=1,Surf%NB_Tri
       nbm=marke(nb)
       IF(nbm<=0) CYCLE
       IF(markp(nbm)==0)THEN
          npiece=npiece+1
          markp(nbm)=npiece
       ENDIF
       marke(nb)=markp(nbm)
    ENDDO

    DO nbm=1,Surf%NB_Surf
       Sur_Piece(nbm)=0
    ENDDO

    nbmerror = 0
    DO nb=1,Surf%NB_Tri
       nbm=Surf%IP_Tri(5,nb)
       IF(Sur_Piece(nbm)==0)THEN
          Sur_Piece(nbm)=marke(nb)
       ELSE IF(Sur_Piece(nbm)/=marke(nb))THEN
          IF(nbmerror==0)THEN
             PRINT*,'nb,nbm,Sur_Piece(nbm),marke(nb)=',       &
                  nb,nbm,Sur_Piece(nbm),marke(nb)
             PRINT*,'Warning: surface ',nbm,       &
                  ' is not a connected piece---Surf_count'
             nbmerror = 1
          ENDIF
       ENDIF
    ENDDO

    DEALLOCATE (Markp, Marke)

    RETURN
  END SUBROUTINE Surf_count



  !>
  !!  Bisect long edges (compared with inputted Criterion) in a specified area.
  !<
  SUBROUTINE Surf_EdgeBisect(Surf)
    IMPLICIT NONE

    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER :: i,j,np,nel,ip1,ip2
    INTEGER :: ip(4), ips(2)
    REAL*8  :: xt(3)
    INTEGER :: IEdge, nside, Mside
    INTEGER :: npnew, npold, nenew
    INTEGER :: tridiv(3), nntri, trinew(3,4)
    REAL*8  :: trilen(3)
    INTEGER, DIMENSION(:  ), POINTER :: Edge_Div
    REAL*8 , DIMENSION(:  ), POINTER :: Edge_Len
    REAL*8  :: Criterion, SourcePt(3), Source_R, xl
    INTEGER :: K_region
    TYPE(NodeNetTreeType) :: Edge_Tree


    WRITE(*,'(a)') 'Input the type of region which should be '
    WRITE(*,'(a)') '  0 for whole domain, or'
    WRITE(*,'(a)') '  1 for a specified area'
    READ(6,*)K_region
    Source_R = 0
    IF(K_region==1)THEN
       WRITE(*,'(a)') 'Input the position of the source point and the radius : '
       READ(6,*)SourcePt(1:3), Source_R
    ENDIF

    WRITE(*,'(a)') 'Input the length criterion dx: '
    READ(6,*)Criterion
    Criterion = Criterion**2

    !---- Use square values for comparison
    Source_R  = Source_R**2

    !----- 
    Mside=10*Surf%NB_Point
    CALL allc_1Dpointer(Edge_Div,  Mside,   'Edge_Div')
    CALL allc_1Dpointer(Edge_Len,  Mside,   'Edge_Len')
    CALL NodeNetTree_Allocate(2, Surf%NB_Point, Mside, Edge_Tree)

    !--- open temporary files to record the new mesh.
    OPEN(2,file= 'tempmesh4XIE',status='UNKNOWN',form='FORMATTED')
    OPEN(3,file= 'tempmesh5XIE',status='UNKNOWN',form='FORMATTED')

    npnew = Surf%NB_Point
    nenew = 0
    nside = 0

    !---- loop on Triangles ---

    DO j=1,Surf%NB_Tri
       ip(1:3)=Surf%IP_Tri(1:3,j)

       DO i=1,3
          ip1=ip(iEdge_Tri(1,i))
          ip2=ip(iEdge_Tri(2,i))
          ips = (/ip1,ip2/)
          CALL NodeNetTree_SearchAdd(2,ips,Edge_Tree,IEdge)

          IF(IEdge>nside)THEN       !--- a new edge
             nside = IEdge
             Edge_Len(IEdge) = Geo3D_Distance_SQ( Surf%Posit(:,ip1), Surf%Posit(:,ip2) )
             Edge_Div(IEdge) = 0
             IF(Edge_Len(IEdge)>Criterion)THEN
                IF(K_region==1)THEN
                   xl= SUM( ((Surf%Posit(:,ip1)+Surf%Posit(:,ip2))/2.-SourcePt(:))**2 )
                   IF(xl<Source_R) Edge_Div(IEdge)=-1
                ELSE
                   Edge_Div(IEdge)=-1
                ENDIF
             ENDIF
             IF(Edge_Div(IEdge)==-1)THEN
	        npnew=npnew+1
                Edge_Div(IEdge)=npnew
                xt(1:3)=(Surf%Posit(:,ip1)+Surf%Posit(:,ip2))/2.0
                WRITE(3,'(6(1x,e15.8))')xt(1:3)
             ENDIF
          ENDIF

          tridiv(i) = Edge_Div(IEdge)
          trilen(i) = Edge_Len(IEdge)
       ENDDO

       CALL Tri_Divider(ip(1:3),tridiv, trilen, nntri, trinew)

       DO nel=1,nntri
          WRITE(2,*) trinew(1:3,nel),Surf%IP_Tri(5,j) 
       ENDDO

       nenew = nenew+nntri

    ENDDO

    CLOSE(2)
    CLOSE(3)

    npold=Surf%NB_Point
    Surf%NB_Point=npnew
    Surf%NB_Tri=nenew
    PRINT*,'Surf%NB_Tri,Surf%NB_Point,Surf%NB_Surf,Surf%NB_Sheet'
    PRINT*,Surf%NB_Tri,Surf%NB_Point,Surf%NB_Surf,Surf%NB_Sheet

    CALL allc_2Dpointer(Surf%Posit,  3, Surf%NB_Point,  'Posit')
    CALL allc_2Dpointer(Surf%IP_Tri, 5, Surf%NB_Tri,    'Tri')


    OPEN(2,file= 'tempmesh4XIE',status='UNKNOWN',form='FORMATTED')
    OPEN(3,file= 'tempmesh5XIE',status='UNKNOWN',form='FORMATTED')

    DO np=npold+1,Surf%NB_Point
       READ(3,*)xt(1:3)
       Surf%Posit(:,np)=xt(1:3)
    ENDDO


    DO nel=1,Surf%NB_Tri
       READ(2,*)ip(1:4)
       Surf%IP_Tri(1:3,nel) = ip(1:3)
       Surf%IP_Tri(4,  nel) = 0
       Surf%IP_Tri(5,  nel) = ip(4)
    ENDDO

    CLOSE(2,status='delete')
    CLOSE(3,status='delete')

    DEALLOCATE(Edge_Div,  Edge_Len)
    CALL NodeNetTree_CLEAR(Edge_Tree)

    RETURN
  END SUBROUTINE Surf_EdgeBisect

  !>
  !!   Adjust the domain and shift the surface to fit the new domain.
  !<
  SUBROUTINE Surf_Adjust(Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    REAL*8 :: XYZmin(3), XYZmax(3), p3(3), q1(3), q2(3), q3(3), d3(3)
    INTEGER :: ip

    CALL SurfaceMeshStorage_Measure(Surf, 3, XYZmin, XYZmax)

    WRITE(*,*)
    WRITE(*,'(a,2(1x,E13.6))')'  Surface range, Xmin Xmax: ',XYZmin(1), XYZmax(1)
    WRITE(*,'(a,2(1x,E13.6))')'  Surface range, Ymin Ymax: ',XYZmin(2), XYZmax(2)
    WRITE(*,'(a,2(1x,E13.6))')'  Surface range, Zmin Zmax: ',XYZmin(3), XYZmax(3)
    WRITE(*,*)

    WRITE(*,*) 'Input new range, (Xmin, Xmax, Ymin Ymax, Zmin Zmax) := '
    READ (*,*)  q1(1),q2(1),q1(2),q2(2),q1(3),q2(3)

    p3(:) = (XYZmin(:) + XYZmax(:))/2.
    q3(:) = (q1(:) + q2(:))/2.
    d3(:) = (q2-q1) / (XYZmax-XYZmin)

    DO ip = 1,Surf%NB_Point
       Surf%Posit(:,ip) = q3(:) + (Surf%Posit(:,ip)-p3(:)) * d3(:)
    ENDDO

    RETURN
  END SUBROUTINE Surf_Adjust

  !>
  !!   Extra part of mesh from a surface.
  !!   if new_to_old(inew) = iold, 
  !!     then the node inew in the new mesh is the point iold in the old mesh.
  !<
  SUBROUTINE Surf_Extract(Surf, new_to_old)   
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(OUT) :: new_to_old(*)
    INTEGER     :: subjob, Isucc
    CHARACTER*2560 ::  HelpMessage      
    INTEGER, DIMENSION(:), POINTER :: Marke,Markp,Markb
    INTEGER ::  ip,ie,np,npp,nb,nbb,In,nel,nbm,ic, nas
    REAL*8  ::  Xmax,Xmin,Ymax,Ymin,Zmax,Zmin,radio
    REAL*8  ::  XYZmin(3), XYZmax(3), P0(3), r

    CALL SurfaceMeshStorage_Measure(Surf, 3, XYZmin, XYZmax)

    WRITE(*,*)
    WRITE(*,'(a,2(1x,E13.6))')'  Surface range, Xmin Xmax: ',XYZmin(1), XYZmax(1)
    WRITE(*,'(a,2(1x,E13.6))')'  Surface range, Ymin Ymax: ',XYZmin(2), XYZmax(2)
    WRITE(*,'(a,2(1x,E13.6))')'  Surface range, Zmin Zmax: ',XYZmin(3), XYZmax(3)
    WRITE(*,*)

    WRITE(*,*)'   SUB_JOB TYPE:'
    WRITE(*,*)'     1, --- extract the surface by given surface numbers'
    WRITE(*,*)'     2, --- extract the surface by a cubic domain'
    WRITE(*,*)'     3, --- extract the surface by removing the box'
    WRITE(*,*)'     4, --- extract the surface by a spherical domain'
    WRITE(*,*)'     5, --- extract the surface by surrounding given nodes'
    WRITE(*,*)'     6, --- extract the surface by given triangles'
    WRITE(*,*)'     7, --- extract the surface by surrounding given triangles'
    HelpMessage =                                                                &
         '  Option 1, you need to give a series of surface numbers '         //  &
         'which you wants to extract from the whole mesh; \n'                //  &
         '  Option 2, you need to give a range of cubic domain, and  '       //  &
         'those surfaces lie in the domain will be saved; \n'                //  &
         '  Some surface generator generating surfaces with a box '          //  &
         'container which is no longer neccessary for the new volume '       //  &
         'mesh generator, option 3 is for removing this box. \n'             //  &
         '  Option 4, you need to give a central position and a radius,  '   //  &
         'and those surfaces lie in the spherical domain will be saved. \n'  //  &
         '  The numbering of the surfaces will be kept. \n'                  //  &
         '  The numebr of sheet will be invalid for the extracted surfaces.'

    CALL Read_Int_withHelp(subjob, 'Input the sub_job : ', HelpMessage, Isucc)
    IF(Isucc==0 .OR. subjob<1 .OR. subjob>7 )THEN
       WRITE(*,*)' Warning--- invalid input, return'
       RETURN
    ENDIF

    nas = MAX(Surf%NB_Point,Surf%NB_Tri,Surf%NB_Surf)
    ALLOCATE(markp(nas), markb(nas), marke(nas))
    DO np=1,Surf%NB_Point
       markp(np)=0
    ENDDO
    DO nb=1,Surf%NB_Tri
       markb(nb)=0
       marke(nb)=0
    ENDDO


    IF(subjob==2)THEN

       WRITE(*,*)'Input a cubic range (xmin,xmax,ymin,ymax,zmin,zmax):'
       READ(6,*)Xmin,Xmax,Ymin,Ymax,Zmin,Zmax

    ELSE IF(subjob==3)THEN

       radio = 0.01
       Xmin  = XYZmin(1)+(XYZmax(1)-XYZmin(1))*radio
       Xmax  = XYZmax(1)-(XYZmax(1)-XYZmin(1))*radio
       Ymin  = XYZmin(2)+(XYZmax(2)-XYZmin(2))*radio
       Ymax  = XYZmax(2)-(XYZmax(2)-XYZmin(2))*radio
       Zmin  = XYZmin(3)+(XYZmax(3)-XYZmin(3))*radio
       Zmax  = XYZmax(3)-(XYZmax(3)-XYZmin(3))*radio
       WRITE(*,'(a)')'  Extracted surface ranges: '
       WRITE(*,'(2x,6(1x,E12.5))')Xmin,Xmax,Ymin,Ymax,Zmin,Zmax

    ELSE IF(subjob==4)THEN

       WRITE(*,*)'Input a central position (x0,y0,z0):'
       READ(6,*)p0
       WRITE(*,*)'Input a radius :'
       READ(6,*)R
       Xmin = P0(1)-R
       Xmax = p0(1)+R
       Ymin = P0(2)-R
       Ymax = p0(2)+R
       Zmin = P0(3)-R
       Zmax = p0(3)+R
       R = R*R

    ENDIF


    IF(subjob>=2 .AND. subjob<=4)THEN
       !----  search nodes and triangles locating in the boxed domain

       DO np=1,Surf%NB_Point
          IF(Surf%Posit(1,np)<xmin) CYCLE
          IF(Surf%Posit(1,np)>xmax) CYCLE
          IF(Surf%Posit(2,np)<ymin) CYCLE
          IF(Surf%Posit(2,np)>ymax) CYCLE
          IF(Surf%Posit(3,np)<zmin) CYCLE
          IF(Surf%Posit(3,np)>zmax) CYCLE
          IF(subjob==4)THEN
             IF(Geo3D_Distance_SQ(Surf%Posit(:,np),p0)>R) CYCLE
          ENDIF
          markp(np)=-1          
       ENDDO

       DO nb =1,Surf%NB_Tri
          IF(  markp(Surf%IP_Tri(1,nb))==0 .OR.     &
               markp(Surf%IP_Tri(2,nb))==0 .OR.     &
               markp(Surf%IP_Tri(3,nb))==0 )THEN
             Surf%IP_Tri(1,nb) = 0
          ENDIF
       ENDDO
       DO nb =1,Surf%NB_Quad
          IF(  markp(Surf%IP_Quad(1,nb))==0 .OR.     &
               markp(Surf%IP_Quad(2,nb))==0 .OR.     &
               markp(Surf%IP_Quad(3,nb))==0 .OR.     &
               markp(Surf%IP_Quad(4,nb))==0 )THEN
             Surf%IP_Quad(1,nb) = 0
          ENDIF
       ENDDO

    ELSE IF(subjob==1)THEN

       WRITE(*,'((a))')'Input suface numbers'
       CALL read_int_array(marke,nas)

       markb(:) = 0
       DO nb=1,nas
          IF(marke(nb)>0 .AND. marke(nb)<=Surf%NB_Surf)THEN
             markb(marke(nb))=nb
          ENDIF
       ENDDO

       DO nb =1,Surf%NB_Tri
          IF(markb(Surf%IP_Tri(5,nb))==0) Surf%IP_Tri(1,nb) = 0
       ENDDO
       DO nb =1,Surf%NB_Quad
          IF(markb(Surf%IP_Quad(5,nb))==0) Surf%IP_Quad(1,nb) = 0
       ENDDO

    ELSE IF(subjob==5)THEN

       WRITE(*,'((a))')'Input node numbers'
       CALL read_int_array(marke,nas)

       markp(:) = 0
       DO nb=1,nas
          IF(marke(nb)>0 .AND. marke(nb)<=Surf%NB_Point)THEN
             markp(marke(nb))=1
          ENDIF
       ENDDO

       DO nb =1,Surf%NB_Tri
          IF(  markp(Surf%IP_Tri(1,nb))==0 .AND.   &
               markp(Surf%IP_Tri(2,nb))==0 .AND.   &
               markp(Surf%IP_Tri(3,nb))==0 )THEN
             Surf%IP_Tri(1,nb) = 0
          ENDIF
       ENDDO
       DO nb =1,Surf%NB_Quad
          IF(  markp(Surf%IP_Quad(1,nb))==0 .AND.     &
               markp(Surf%IP_Quad(2,nb))==0 .AND.     &
               markp(Surf%IP_Quad(3,nb))==0 .AND.     &
               markp(Surf%IP_Quad(4,nb))==0 )THEN
             Surf%IP_Quad(1,nb) = 0
          ENDIF
       ENDDO

    ELSE IF(subjob==6 .OR. subjob==7)THEN

       WRITE(*,'((a))')'Input triangle numbers'
       CALL read_int_array(marke,nas)

       markp(:) = 0
       DO nb=1,nas
          ie = marke(nb)
          IF(ie>0 .AND. ie<=Surf%NB_Tri)THEN             
             markp(Surf%IP_Tri(1:3,ie))=1
          ENDIF
       ENDDO

       DO nb =1,Surf%NB_Tri
          IF(subjob==6)THEN
             IF(  markp(Surf%IP_Tri(1,nb))==0 .OR.   &
                  markp(Surf%IP_Tri(2,nb))==0 .OR.   &
                  markp(Surf%IP_Tri(3,nb))==0 )THEN
                Surf%IP_Tri(1,nb) = 0
             ENDIF
          ELSE
             IF(  markp(Surf%IP_Tri(1,nb))==0 .AND.   &
                  markp(Surf%IP_Tri(2,nb))==0 .AND.   &
                  markp(Surf%IP_Tri(3,nb))==0 )THEN
                Surf%IP_Tri(1,nb) = 0
             ENDIF
          ENDIF
       ENDDO
       DO nb =1,Surf%NB_Quad
          Surf%IP_Quad(1,nb) = 0
       ENDDO

    ENDIF

    npp = Surf%NB_Point
    CALL Surf_RemoveFake(Surf, Markp)

    DO np = 1, npp
       IF(Markp(np)>0) new_to_old(Markp(np)) = np
    ENDDO

    WRITE(*,*)
    WRITE(*,*)'No of triangles: ',Surf%NB_Tri
    WRITE(*,*)'No of points   : ',Surf%NB_Point

    DEALLOCATE(Marke,  Markp,  markb )

    RETURN
  END SUBROUTINE Surf_Extract

  !>
  !!   Remove fake triangles (which IP_Tri(1,nb) <= 0) and 
  !!          fake quad (which IP_Quad(1,nb)<=0) from a surface.
  !!   @param[out] old_to_new : if old_to_new (iold) = inew, 
  !!     then the node inew in the new mesh is the point iold in the old mesh;
  !!     if old_to_new (iold) = 0, then the point iold in the old mesh disapear.
  !<
  SUBROUTINE Surf_RemoveFake(Surf, old_to_new)   
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(OUT), OPTIONAL :: old_to_new(*) 
    INTEGER, DIMENSION(:), POINTER :: markp
    INTEGER ::  np, npp, nb, nbb

    ALLOCATE(markp(0:Surf%NB_Point))

    markp(0:Surf%NB_Point) = 0
    DO nb =1,Surf%NB_Tri
       IF(Surf%IP_Tri(1,nb)>0)THEN
          markp(Surf%IP_Tri(1:3,nb)) = 1
          IF(Surf%GridOrder>1)THEN
             markp(Surf%IPsp_Tri(:,nb)) = 1
          ENDIF
       ENDIF
    ENDDO
    DO nb =1,Surf%NB_Quad
       IF(Surf%IP_Quad(1,nb)>0)THEN
          markp(Surf%IP_Quad(1:4,nb)) = 1
       ENDIF
    ENDDO
    markp(0) = 0

    !----  resort nodes and triangles locating in the required domain

    npp=0
    DO np=1,Surf%NB_Point
       IF(markp(np)==1)THEN
          npp=npp+1
          markp(np)=npp
          Surf%Posit(:,npp) = Surf%Posit(:,np)
          IF(Surf%NB_Value>0)THEN
             Surf%V_Point(1:Surf%NB_Value, npp) = Surf%V_Point(1:Surf%NB_Value,np)
          ENDIF
       ENDIF
    ENDDO

    nbb=0
    DO nb =1,Surf%NB_Tri
       IF(Surf%IP_Tri(1,nb)>0)THEN
          nbb = nbb+1
          Surf%IP_Tri(1:3,nbb) = markp(Surf%IP_Tri(1:3,nb))
          Surf%IP_Tri(4:5,nbb) =       Surf%IP_Tri(4:5,nb)
          IF(Surf%GridOrder>1)THEN
             Surf%IPsp_Tri(:,nbb) = markp(Surf%IPsp_Tri(:,nb))
          ENDIF
       ENDIF
    ENDDO
    Surf%NB_Tri   = nbb

    nbb=0
    DO nb =1,Surf%NB_Quad
       IF(Surf%IP_Quad(1,nb)>0)THEN
          nbb = nbb+1
          Surf%IP_Quad(1:4,nbb) = markp(Surf%IP_Quad(1:4,nb))
          Surf%IP_Quad(5,  nbb) =       Surf%IP_Quad(5,  nb)
       ENDIF
    ENDDO
    Surf%NB_Quad   = nbb

    IF(PRESENT(old_to_new))THEN
       old_to_new(1:Surf%NB_Point) = markp(1:Surf%NB_Point)
    ENDIF

    Surf%NB_Point = npp

    DEALLOCATE(markp)

    RETURN
  END SUBROUTINE Surf_RemoveFake

  !>
  !!   Reverse the orientation of surface.
  !!   @param[in] subjob = 0 : reverse the surface by given numbers.  \n
  !!                     = 1 : reverse the surface to oritate inside. \n
  !!                     =-1 : reverse the surface to oritate outside.
  !!   @param[in,out] Surf
  !<
  SUBROUTINE Surf_Reverse (Surf, subjob)   
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(IN)  :: subjob
    INTEGER, DIMENSION(:  ), POINTER :: Marke,Markb
    INTEGER, DIMENSION(:  ), POINTER :: ioo_Edge, ipp_Edge
    INTEGER :: ip,nb,ie, idirt, nPEC
    INTEGER :: ied1, ied2, ned, i, j, ipleft, ieleft, ienew, nas
    INTEGER :: NB_Edge, Iedge, ied, ipp(4), ip2(2), iss(4)
    REAL*8  :: xmin, vmod, a(3), aa
    LOGICAL :: CarryOn, BigestFace


    IF(subjob==0)THEN

       ALLOCATE(marke(Surf%NB_Tri), markb(Surf%NB_Tri))
       marke(1:Surf%NB_Tri)=0
       markb(1:Surf%NB_Tri)=0
       WRITE(*,'((a))')'Input face pieces numbers'
       CALL read_int_array(marke,nas)

       DO nb=1,nas
          IF(marke(nb)>0 .AND. marke(nb)<=Surf%NB_Surf)THEN
             markb(marke(nb))=1
          ENDIF
       ENDDO

       DO nb=1,Surf%NB_Tri
          IF(markb(Surf%IP_Tri(5,nb))==1)THEN
             ip = Surf%IP_Tri(3,nb)
             Surf%IP_Tri(3,nb) = Surf%IP_Tri(2,nb)
             Surf%IP_Tri(2,nb) = ip
          ENDIF
       ENDDO

       IF(Surf%GridOrder==3)THEN
          DO nb=1,Surf%NB_Tri
             IF(markb(Surf%IP_Tri(5,nb))==1)THEN
                Surf%IPsp_Tri(:,nb) = Surf%IPsp_Tri(iRotate_C3Tri(:,-1),nb)
             ENDIF
          ENDDO
       ENDIF

       DO nb=1,Surf%NB_Quad
          IF(markb(Surf%IP_Quad(5,nb))==1)THEN
             ip = Surf%IP_Quad(4,nb)
             Surf%IP_Quad(4,nb) = Surf%IP_Quad(2,nb)
             Surf%IP_Quad(2,nb) = ip
          ENDIF
       ENDDO

       DEALLOCATE(Marke, markb)

    ELSE

       !--- build edge system
       ned = 2*Surf%NB_Tri + 3*Surf%NB_Quad
       ALLOCATE( ioo_Edge(ned), ipp_Edge(ned), marke(-Surf%NB_Quad : Surf%NB_Tri))
       ipp_Edge(:) = 0
       marke(:)    = 0
       marke(0)    = 1

       !--- ignored interfaces (sheets)
       nPEC = Surf%NB_Surf - Surf%NB_Sheet
       DO ie = 1,Surf%NB_Tri
          IF(Surf%IP_Tri(5,ie) > nPEC)THEN
             Surf%IP_Tri(1,ie) = -Surf%IP_Tri(1,ie)
             marke( ie) = 1
          ENDIF
       ENDDO
       DO ie = 1,Surf%NB_Quad
          IF(Surf%IP_Quad(5,ie) > nPEC)THEN
             Surf%IP_Quad(1,ie) = -Surf%IP_Quad(1,ie)
             marke(-ie) = 1
          ENDIF
       ENDDO

       CALL Surf_BuildEdge(Surf)
       IF(Surf%NB_Edge > ned) CALL Error_STOP ( 'Surf_Reverse:: NB_Edge > ned')

       CarryOn    = .TRUE.
       BigestFace = .TRUE.

       DO WHILE(CarryOn)

          !--- check end triangle
          Xmin   = 1.d38
          ipleft = 0
          DO ie = 1,Surf%NB_Tri
             IF(marke(ie)/=0) CYCLE
             DO i=1,3
                ip = Surf%IP_Tri(i,ie)
                IF(Xmin>Surf%Posit(1,ip))THEN
                   ipleft = ip
                   Xmin   = Surf%Posit(1,ip)
                ENDIF
             ENDDO
          ENDDO
          DO ie = 1,Surf%NB_Quad
             IF(marke(-ie)/=0) CYCLE
             DO i=1,4
                ip = Surf%IP_Quad(i,ie)
                IF(Xmin>Surf%Posit(1,ip))THEN
                   ipleft = ip
                   Xmin   = Surf%Posit(1,ip)
                ENDIF
             ENDDO
          ENDDO
          IF(ipleft==0) CALL Error_STOP ( 'Surf_Reverse:: ipleft==0')

          vmod   = 0
          ieleft = 0
          DO ie = 1,Surf%NB_Tri
             IF(marke(ie)/=0) CYCLE
             ipp(1:3) = Surf%IP_Tri(1:3,ie)
             IF(ipp(1)==ipleft .OR. ipp(2)==ipleft .OR. ipp(3)==ipleft)THEN
                a = Geo3D_Cross_Product(Surf%Posit(:,ipp(2)),   &
                     Surf%Posit(:,ipp(1)), Surf%Posit(:,ipp(3)))
                aa = a(1)/dsqrt(a(1)*a(1)+a(2)*a(2)+a(3)*a(3))
                IF(ABS(vmod)<=ABS(aa))THEN
                   vmod   = aa
                   ieleft = ie
                ENDIF
             ENDIF
          ENDDO
          DO ie = 1,Surf%NB_Quad
             IF(marke(-ie)/=0) CYCLE
             ipp(1:4) = Surf%IP_Quad(1:4,ie)
             IF(  ipp(1)==ipleft .OR. ipp(2)==ipleft .OR.   &
                  ipp(3)==ipleft .OR. ipp(4)==ipleft)THEN
                a = Geo3D_Cross_Product(         &
                     Surf%Posit(:,ipp(3))-Surf%Posit(:,ipp(1)),   &
                     Surf%Posit(:,ipp(4))-Surf%Posit(:,ipp(2)) )
                aa = a(1)/dsqrt(a(1)*a(1)+a(2)*a(2)+a(3)*a(3))
                IF(ABS(vmod)<=ABS(aa))THEN
                   vmod   = aa
                   ieleft = -ie
                ENDIF
             ENDIF
          ENDDO
          IF(ieleft==0) CALL Error_STOP ( 'Surf_Reverse:: ieleft==0')

          !--- set the end triangle
          IF(ABS(vmod)<1.e-6)THEN
             WRITE(*,*) 'WARNING--- Surf_Reverse:: Are there any sharp points on left side?'
             WRITE(*,*) 'ipleft,ieleft,isur=',ipleft,ieleft
             IF(ieleft>0)THEN
                WRITE(*,*) 'ieleft, ips=',ieleft, Surf%IP_Tri(1:3,ieleft)
                WRITE(*,*) 'p1=',REAL(Surf%Posit(:,Surf%IP_Tri(1,ieleft)))
                WRITE(*,*) 'p2=',REAL(Surf%Posit(:,Surf%IP_Tri(2,ieleft)))
                WRITE(*,*) 'p3=',REAL(Surf%Posit(:,Surf%IP_Tri(3,ieleft)))
             ELSE
                WRITE(*,*) 'ieleft, ips=',ieleft, Surf%IP_Quad(1:4,-ieleft)
                WRITE(*,*) 'p1=',REAL(Surf%Posit(:,Surf%IP_Quad(1,-ieleft)))
                WRITE(*,*) 'p2=',REAL(Surf%Posit(:,Surf%IP_Quad(2,-ieleft)))
                WRITE(*,*) 'p3=',REAL(Surf%Posit(:,Surf%IP_Quad(3,-ieleft)))
                WRITE(*,*) 'p4=',REAL(Surf%Posit(:,Surf%IP_Quad(4,-ieleft)))
             ENDIF
          ENDIF

          !--- start from element ieleft
          IF(BigestFace)THEN
             idirt = subJob
             BigestFace = .FALSE.
          ELSE
             !--- for those inside surfaces, take reverse orientation.
             idirt = -subJob
          ENDIF
          ied1 = 1
          IF(ieleft>0)THEN
             IF(vmod*idirt<0)THEN
                ipp(2:3)                 = Surf%IP_Tri(2:3,ieleft)
                Surf%IP_Tri(2:3,ieleft)  = (/ ipp(3), ipp(2) /)
                iss(2:3)                 = Surf%IED_Tri(2:3,ieleft)
                Surf%IED_Tri(2:3,ieleft) = (/ iss(3), iss(2) /)
                IF(Surf%GridOrder==3)THEN
                   Surf%IPsp_Tri(:,ieleft) = Surf%IPsp_Tri(iRotate_C3Tri(:,-1),ieleft)
                ENDIF
             ENDIF

             ied2 = 3
             DO i=1,3
                ioo_Edge(i)           = Surf%IED_Tri(i, ieleft)
                ipp_Edge(ioo_Edge(i)) = Surf%IP_Tri(iEdge_Tri(1,i), ieleft)
             ENDDO
          ELSE
             IF(vmod*idirt<0)THEN
                ipp(2:4)                   = Surf%IP_Quad(2:4,-ieleft)
                Surf%IP_Quad(2:4,-ieleft)  = (/ ipp(4), ipp(3), ipp(2) /)
                iss(1:4)                   = Surf%IED_Quad(1:4,-ieleft)
                Surf%IED_Quad(1:4,-ieleft) = (/ iss(4), iss(3), iss(2), iss(1) /)
             ENDIF

             ied2 = 4
             DO i=1,4
                ioo_Edge(i)           = Surf%IED_Quad(i,-ieleft)
                ipp_Edge(ioo_Edge(i)) = Surf%IP_Quad(i, -ieleft)
             ENDDO
          ENDIF
          ned  = ied2
          marke(ieleft) = 1

          !--- populate the orientation
          DO WHILE(ied2>=ied1)

             DO ied = ied1,ied2
                iedge = ioo_Edge(ied)
                DO i=1,2
                   ie = Surf%ITR_Edge(i,iedge)
                   IF(ie==0) CYCLE
                   IF(marke(ie)==1) CYCLE
                   marke(ie) = 1

                   IF(ie>0)THEN

                      ipp(1:3) = Surf%IP_Tri(1:3,ie)
                      DO j=1,3
                         IF(Surf%IED_Tri(j,ie)==iedge .AND. ipp(iEdge_Tri(1,j))==ipp_Edge(iedge))THEN
                            Surf%IP_Tri(2:3,ie) = (/ ipp(3), ipp(2) /)
                            iss(2:3) = Surf%IED_Tri(2:3,ie)
                            Surf%IED_Tri(2:3,ie) = (/ iss(3), iss(2) /)
                            IF(Surf%GridOrder==3)THEN
                               Surf%IPsp_Tri(:,ie) = Surf%IPsp_Tri(iRotate_C3Tri(:,-1),ie)
                            ENDIF
                            EXIT
                         ENDIF
                      ENDDO

                      DO j=1,3
                         ienew = Surf%IED_Tri(j,ie)
                         IF(ipp_Edge(ienew)>0)CYCLE
                         ned = ned+1
                         ioo_Edge(ned)   = ienew
                         ipp_Edge(ienew) = Surf%IP_Tri(iEdge_Tri(1,j), ie)
                      ENDDO

                   ELSE

                      ie = -ie
                      ipp(1:4) = Surf%IP_Quad(1:4,ie)
                      DO j=1,4
                         IF(Surf%IED_Quad(j,ie)==iedge .AND. ipp(j)==ipp_Edge(iedge))THEN
                            Surf%IP_Quad(2:4,ie)  = (/ ipp(4), ipp(3), ipp(2) /)
                            iss(1:4)              = Surf%IED_Quad(1:4,ie)
                            Surf%IED_Quad(1:4,ie) = (/ iss(4), iss(3), iss(2), iss(1) /)                         
                            EXIT
                         ENDIF
                      ENDDO

                      DO j=1,4
                         ienew = Surf%IED_Quad(j,ie)
                         IF(ipp_Edge(ienew)>0)CYCLE
                         ned = ned+1
                         ioo_Edge(ned)   = ienew
                         ipp_Edge(ienew) = Surf%IP_Quad(j,ie)
                      ENDDO

                   ENDIF
                ENDDO
             ENDDO

             ied1 = ied2+1
             ied2 = ned
             IF(ned>Surf%NB_Edge) CALL Error_STOP ( 'Surf_Reverse:: ned>Surf%NB_Edge')
          ENDDO

          CarryOn = .FALSE.
          DO ie=-Surf%NB_Quad,Surf%NB_Tri
             IF(Marke(ie)==0)THEN
                CarryOn = .TRUE.
                EXIT
             ENDIF
          ENDDO

       ENDDO

       DEALLOCATE(ioo_Edge, ipp_Edge, Marke)     
       CALL Surf_ClearEdge(Surf)

       DO ie = 1,Surf%NB_Tri
          IF(Surf%IP_Tri(5,ie) > nPEC)THEN
             Surf%IP_Tri(1,ie) = ABS(Surf%IP_Tri(1,ie))
          ENDIF
       ENDDO
       DO ie = 1,Surf%NB_Quad
          IF(Surf%IP_Quad(5,ie) > nPEC)THEN
             Surf%IP_Quad(1,ie) = ABS(Surf%IP_Quad(1,ie))
          ENDIF
       ENDDO

    ENDIF

    RETURN
  END SUBROUTINE Surf_reverse

  !>
  !!   Reset surface number by the connection.
  !!   Count piece from point ipstart.
  !!   if ipstart<=0, count piece from the xmin point.
  !<
  SUBROUTINE Surf_Resetpiece  (surf, ipstart)   
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(IN) :: ipstart
    INTEGER  :: npleft, npiece, ip3(3), nb, ip
    REAL*8   :: Xmin
    INTEGER, DIMENSION(:), POINTER :: Markp

    WRITE(*,*)' '
    WRITE(*,*)'The program will reset surface piece number by connection'
    WRITE(*,*)' '

    npleft = ipstart
    IF(ipstart<=0)THEN
       Xmin   = Surf%Posit(1,ip)
       npleft = 1
       DO ip = 2,Surf%NB_Point
          IF(Xmin>Surf%Posit(1,ip))THEN
             npleft = ip
             Xmin   = Surf%Posit(1,ip)
          ENDIF
       ENDDO
    ENDIF

    ALLOCATE(markp(Surf%NB_Point))
    CALL Surf_BuildPtAsso(Surf)
    CALL LinkAssociation_Colour(Surf%IP_Pt, Surf%NB_Point, npleft, Markp, npiece)

    DO nb = 1,Surf%NB_Tri
       ip3(1:3) = Markp(Surf%IP_Tri(1:3,nb))
       IF(ip3(1)/=ip3(2) .OR. ip3(1)/=ip3(3) .OR. ip3(1)==0)THEN
          CALL Error_STOP ( '----Surf_Resetpiece')
       ENDIF

       Surf%IP_Tri(5,nb) = ip3(1)
    ENDDO

    Surf%NB_Surf = npiece              
    DEALLOCATE(markp)


    RETURN
  END SUBROUTINE Surf_Resetpiece

  !>
  !!  Trim off or reset those fin-like triangles from the surface. 
  !<
  SUBROUTINE surf_Trim  (Surf)   
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER        ::  subjob, Isucc
    TYPE(NodeNetTreeType) :: Edge_Tree
    INTEGER :: i,n,m,IB,Iedge,is,ipp(2),NB_Edge, nb_del, ip
    INTEGER, DIMENSION(:  ),  POINTER :: numT_Edge, Mark_Point
    INTEGER, DIMENSION(:,:),  POINTER :: IP_Edge, IT_Edge, IS_Tri
    LOGICAL :: CarryOn
    CHARACTER*2560 ::  HelpMessage      

    WRITE(*,*)'   SUB_JOB TYPE:'
    WRITE(*,*)'     1, --- remove off fin triangles'
    WRITE(*,*)'     2, --- keep fin triangles as interface'
    HelpMessage =                                                                &
         '  Option 1, all fin triangles will be removed; \n'                  // &
         '  Option 2, all fin triangles compose a new sheet. \n'              // &
         '  All those nodes which are NOT on a triangle will be removed '     // &
         'and the rest will be resorted.' 
    CALL Read_Int_withHelp(subjob, 'Input the sub_job : ', HelpMessage, Isucc)
    IF(Isucc==0 .OR. subjob<1 .OR. subjob>2)THEN
       WRITE(*,*)' Warning--- invalid input, return'
       RETURN
    ENDIF


    !--- build edges
    NB_Edge = 3*Surf%NB_Tri
    ALLOCATE(IP_Edge(  2,NB_Edge))
    ALLOCATE(IT_Edge( 60,NB_Edge))  
    ALLOCATE(numT_Edge(  NB_Edge))  
    ALLOCATE(IS_Tri(   3,Surf%NB_Tri))  
    ALLOCATE(Mark_Point(Surf%NB_Point))
    CALL NodeNetTree_allocate(2, Surf%NB_Point, 3*Surf%NB_Tri, Edge_Tree)
    numT_Edge(:) = 0

    NB_Edge = 0
    DO IB = 1,Surf%NB_Tri
       DO i = 1,3
          ipp(1) = Surf%IP_Tri(i,IB)
          ipp(2) = Surf%IP_Tri(MOD(i,3)+1,IB)
          CALL NodeNetTree_SearchAdd(2,ipp,Edge_Tree,IEdge)
          IS_Tri(i,IB) = IEdge

          IF(IEdge>NB_Edge)THEN
             !-- a new edge
             IF(numT_Edge(IEdge)/=0) CALL Error_STOP ( 'numT_Edge(IEdge)/=0')
             NB_Edge = IEdge
             IP_Edge(:,IEdge) = ipp(:)
             IT_Edge(1,IEdge) = IB
             numT_Edge(IEdge) = 1
          ELSE 
             numT_Edge(IEdge) = numT_Edge(IEdge) + 1
             IF(numT_Edge(IEdge)>60) CALL Error_STOP ( 'numT_Edge(IEdge)>60')
             IT_Edge(numT_Edge(IEdge),IEdge) = IB
          ENDIF
       ENDDO
    ENDDO

    WRITE(*,*)' number of edges=',nb_edge

    nb_del = 0
    CarryOn = .TRUE.
    DO WHILE(CarryOn)
       CarryOn = .FALSE.
       DO is = 1,NB_Edge
          IF(numT_Edge(is)==1)THEN
             CarryOn = .TRUE.
             IB = IT_Edge(1,is)
             Surf%IP_Tri(1,IB) = -ABS(Surf%IP_Tri(1,IB))
             nb_del = nb_del+1
             DO i=1,3
                IEdge = IS_Tri(i,IB)
                m = numT_Edge(IEdge)
                DO n=1,m
                   IF(IT_Edge(n,IEdge)==IB)THEN
                      IT_Edge(n,IEdge) = IT_Edge(m,IEdge)
                      numT_Edge(IEdge) = m-1
                      EXIT
                   ENDIF
                ENDDO
             ENDDO
          ENDIF
       ENDDO
    ENDDO

    IF(nb_del==0)THEN
       WRITE(*,*) 'nothing changed'
       RETURN
    ENDIF

    WRITE(*,*) ' number of fin triangles: ',nb_del

    Mark_Point(:) = 0
    IF(subjob==1)THEN
       !--- remove fin triangles 

       DO IB = 1,Surf%NB_Tri
          IF(Surf%IP_Tri(1,IB)>0) Mark_Point(Surf%IP_Tri(1:3,IB)) = 1
       ENDDO

    ELSE
       !--- set fin triangles as interface

       Surf%NB_Surf  = Surf%NB_Surf +1
       Surf%NB_Sheet = Surf%NB_Sheet +1
       DO IB = 1,Surf%NB_Tri
          IF(Surf%IP_Tri(1,IB)<0)THEN
             Surf%IP_Tri(1,IB) = -Surf%IP_Tri(1,IB)
             Surf%IP_Tri(5,IB) = Surf%NB_Surf
          ENDIF
       ENDDO

       DO IB = 1,Surf%NB_Tri
          Mark_Point(Surf%IP_Tri(1:3,IB)) = 1
       ENDDO

    ENDIF

    m = 0
    DO ip = 1,Surf%NB_Point
       IF(mark_point(ip)>0)THEN
          m = m+1
          mark_point(ip)  = m
          Surf%Posit(:,m) = Surf%Posit(:,ip)
          IF(Surf%NB_Value>0)THEN
             Surf%V_Point(:, m) = Surf%V_Point(:,ip)
          ENDIF
       ENDIF
    ENDDO
    Surf%NB_Point = m

    m = 0
    DO IB = 1,Surf%NB_Tri
       IF(Surf%IP_Tri(1,IB)>0)THEN
          m = m+1
          Surf%IP_Tri(1:3,m) = Mark_Point(Surf%IP_Tri(1:3,IB))
          Surf%IP_Tri(4:5,m) = Surf%IP_Tri(4:5,IB)
       ENDIF
    ENDDO
    Surf%NB_Tri = m

    DEALLOCATE( numT_Edge, Mark_Point )
    DEALLOCATE( IP_Edge, IT_Edge, IS_Tri )

    RETURN
  END SUBROUTINE surf_Trim


  !>
  !!     Check if point pp lie in circumcircle of triangle IT
  !!  
  !!    @param[in]  Surf
  !!    @param[in]  IT
  !!    @param[in]  pp
  !!    @param[in]  fMap    
  !!    @return     RA    <=1 in circumcircle; >1 out circumcircle
  !<
  FUNCTION Surf_Criterion_inCircle(Surf,IT, pp, fMap) RESULT(RA)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER, INTENT(IN) :: IT
    REAL*8,  INTENT(IN) :: pp(3)
    REAL*8,  INTENT(IN), OPTIONAL :: fMap(3,3)
    REAL*8 :: RA
    REAL*8 :: p1(3), p2(3), p3(3), p4(3), p0(3)

    p1(:) = Surf%Posit(:,Surf%IP_Tri(1,IT))
    p2(:) = Surf%Posit(:,Surf%IP_Tri(2,IT))
    p3(:) = Surf%Posit(:,Surf%IP_Tri(3,IT))
    p4(:) = pp(:)

    IF(PRESENT(fMap))THEN
       p1 = Mapping3D_Posit_Transf(p1,fMap,1)
       p2 = Mapping3D_Posit_Transf(p2,fMap,1)
       p3 = Mapping3D_Posit_Transf(p3,fMap,1)
       p4 = Mapping3D_Posit_Transf(p4,fMap,1)
    ENDIF

    p0(:) = Geo3D_Sphere_Centre (P1, P2, P3)  
    RA = dsqrt( Geo3D_Distance_SQ(p4,p0) / Geo3D_Distance_SQ(p1,p0) )

    RETURN
  END FUNCTION Surf_Criterion_inCircle

  !!------------------------------------------------------------------
  !>
  !!    [Oubay's code] 
  !!    Removes interior points connected to three triangles  
  !<
  !!------------------------------------------------------------------
  SUBROUTINE Surf_RemoveYNode(Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, DIMENSION(:), POINTER :: ip_map
    INTEGER, DIMENSION(:), POINTER :: CellList
    INTEGER ::  List_Length
    INTEGER :: ntres, kpoin, ip, i, il, ie, je, ino, ipt
    INTEGER :: ip1, ip2, ip3, ie1, ie2, ie3
    INTEGER, DIMENSION(:), POINTER :: MarkT

    !--- Build association
    CALL Surf_BuildTriAsso(Surf)

    ! *** Selects nodes surrounded by three elements

    ALLOCATE(ip_map(Surf%NB_Point), MarkT(Surf%NB_Tri))
    DO ip = 1,Surf%NB_BD_Point
       ip_map(ip) = ip
    ENDDO
    MarkT(:) = 0

    ntres = 0
    kpoin = Surf%NB_BD_Point

    DO ip = Surf%NB_BD_Point +1, Surf%NB_Point
       CALL LinkAssociation_List(IP, List_Length, CellList, Surf%ITR_Pt)
       IF(List_Length<3) CALL Error_STOP ( '---Surf_RemoveYNode 1')
       kpoin      = kpoin+1
       ip_map(ip) = kpoin
       IF(List_Length>3) CYCLE
       ie1        = CellList(1)
       ie2        = CellList(2)
       ie3        = CellList(3)
       IF( MarkT(ie1)/=0 .OR. MarkT(ie2)/=0 .OR. MarkT(ie3)/=0 ) CYCLE

       !--- check if existing a potential all-boundary-node triangle
       DO i = 1,3
          IF(Surf%IP_Tri(i,ie1)/=ip .AND. Surf%IP_Tri(i,ie1)>Surf%NB_BD_Point) EXIT
          IF(Surf%IP_Tri(i,ie2)/=ip .AND. Surf%IP_Tri(i,ie2)>Surf%NB_BD_Point) EXIT
       ENDDO
       IF(i>3) CYCLE

       ip1        = Surf%IP_Tri(1,ie1)
       ip2        = Surf%IP_Tri(2,ie1)
       ip3        = Surf%IP_Tri(3,ie1)
       DO i = 1,3
          ipt = Surf%IP_Tri(i,ie2)
          IF( ipt /= ip1 .AND. ipt /= ip2 .AND. ipt /= ip3 ) ino = ipt
       ENDDO
       WHERE( Surf%IP_Tri(1:3,ie1) == ip ) Surf%IP_Tri(1:3,ie1) = ino  
       MarkT(ie1) =  1
       MarkT(ie2) = -1
       MarkT(ie3) = -1
       kpoin      = kpoin-1
       ntres      = ntres+1
       ip_map(ip) = 0
    ENDDO

    CALL LinkAssociation_Clear(Surf%ITR_Pt)

    IF(kpoin+ntres /= Surf%NB_Point) CALL Error_STOP ( '--- Surf_RemoveYNode :: kpoin+ntres')
    IF(ntres==0) RETURN

    ! *** transfer

    IF(ASSOCIATED(Surf%Posit))THEN
       DO ip = Surf%NB_BD_Point +1, Surf%NB_Point
          il = ip_map(ip)
          IF( il/=0 .AND. il/=ip )  Surf%Posit(:,il) = Surf%Posit(:,ip)
       ENDDO
    ENDIF
    IF(ASSOCIATED(Surf%Coord))THEN
       DO ip = Surf%NB_BD_Point +1, Surf%NB_Point
          il = ip_map(ip)
          IF( il/=0 .AND. il/=ip ) Surf%Coord(:,il) = Surf%Coord(:,ip)
       ENDDO
    ENDIF

    je = 0
    DO ie = 1,Surf%NB_Tri
       IF( MarkT(ie) >= 0 ) THEN 
          je = je+1
          Surf%IP_Tri(1:3,je) = ip_map(Surf%IP_Tri(1:3,ie))
       ENDIF
    ENDDO

    ! *** gets Surf%NB_Point and Surf%NB_Tri

    Surf%NB_Point = Surf%NB_Point -   ntres
    Surf%NB_Tri   = Surf%NB_Tri   - 2*ntres
    IF(je/=Surf%NB_Tri) CALL Error_STOP ( '--- Surf_RemoveYNode :: je/=Surf%NB_Tri')

    DEALLOCATE(ip_map, MarkT)

    RETURN
  END SUBROUTINE Surf_RemoveYNode

  !******************************************************************************!
  !                        1          3
  !                        *---------*
  !                       / \       /
  !                      /   \  R  /
  !                     / L   \   /
  !                    /       \ /
  !                   *---------*
  !                  4          2
  !>
  !!   Check the geometric measures of two contiguous triangles to find out 
  !!         if it is good for diagonal swapping.
  !!
  !!         \image html swap.jpg
  !!
  !!   @param[in]  Surf    :  the triangal mesh.
  !!   @param[in]  ip1,ip2,ip3,ip4   
  !!                       :  the four nodes of two contiguous triangles as above figure.                              
  !!   @param[in]  Kcheck  =1   check if swapping would result in a negative angle on 2D plane.   \n
  !!                       =2   check if swapping would result in a negative angle 
  !!                            or a sharp dihedral angle on 3D space. 
  !!   @param[out]  Kpass  =0   fail              \n                         
  !!                       =1   allowed to swap.
  !!
  !<
  !******************************************************************************!
  SUBROUTINE Surf_SwapCheck1(Surf, ip1, ip2, ip3, ip4, Kcheck, Kpass)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER, INTENT(IN)  :: ip1, ip2, ip3, ip4, Kcheck
    INTEGER, INTENT(OUT) :: Kpass
    REAL*8 :: v1(3), v2(3), a

    Kpass = 0

    IF(Kcheck==1)THEN
       IF(Cross_Product_2D(Surf%Coord(:,ip4), Surf%Coord(:,ip1), Surf%Coord(:,ip3)) <= 0.) RETURN
       IF(Cross_Product_2D(Surf%Coord(:,ip3), Surf%Coord(:,ip2), Surf%Coord(:,ip4)) <= 0.) RETURN
    ENDIF

    IF(Kcheck==2)THEN
       v1 = Geo3D_Cross_Product(Surf%Posit(:,ip1), Surf%Posit(:,ip3), Surf%Posit(:,ip4))
       v2 = Geo3D_Cross_Product(Surf%Posit(:,ip2), Surf%Posit(:,ip3), Surf%Posit(:,ip4))
       a  = Geo3D_Dot_Product(v1, v2)
       IF(a>=0) RETURN
    ENDIF

    Kpass = 1

    RETURN
  END SUBROUTINE Surf_SwapCheck1


  !******************************************************************************!
  !                        1          3
  !                        *---------*
  !                       / \       /
  !                      /   \  R  /
  !                     / L   \   /
  !                    /       \ /
  !                   *---------*
  !                  4          2
  !>
  !!
  !!         \image html swap1.jpg
  !!
  !!   Swap a edge between two contiguous triangles.
  !!   Update *_Tri and *_Edge.
  !!
  !!   @param[in,out]  Surf    :  the triangal mesh.
  !!   @param[in]  IS          :  the edge to swap.
  !!
  !<
  !******************************************************************************!
  SUBROUTINE Surf_SwapSingleEdge(Surf, IS)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(IN)  :: IS
    INTEGER :: itR, itL, ip1, ip2, ip3, ip4, IS1, IS2, i

    itR = Surf%ITR_Edge(1,IS)
    itL = Surf%ITR_Edge(2,IS)
    ip1 = Surf%IP_Edge(1,IS)
    ip2 = Surf%IP_Edge(2,IS)
    ip3 = SUM(Surf%IP_Tri(1:3,itR)) - ip1 - ip2
    ip4 = SUM(Surf%IP_Tri(1:3,itL)) - ip1 - ip2

    DO i=1,3
       IF(Surf%IP_Tri(i,itR)==ip1)THEN
          Surf%IP_Tri(i,itR) = ip4
       ELSE IF(Surf%IP_Tri(i,itR)==ip2)THEN
          IS1 = Surf%IED_Tri(i,itR)
          Surf%IED_Tri(i,itR) = IS
       ENDIF

       IF(Surf%IP_Tri(i,itL)==ip2)THEN
          Surf%IP_Tri(i,itL) = ip3
       ELSE IF(Surf%IP_Tri(i,itL)==ip1)THEN
          IS2 = Surf%IED_Tri(i,itL)
          Surf%IED_Tri(i,itL) = IS
       ENDIF
    ENDDO

    WHERE(Surf%ITR_Edge(:,IS1)==itR) Surf%ITR_Edge(:, IS1) = itL
    WHERE(Surf%ITR_Edge(:,IS2)==itL) Surf%ITR_Edge(:, IS2) = itR
    WHERE(Surf%IP_Tri(1:3,itR)==ip3) Surf%IED_Tri(1:3,itR) = IS2
    WHERE(Surf%IP_Tri(1:3,itL)==ip4) Surf%IED_Tri(1:3,itL) = IS1

    Surf%IP_Edge(1:2,IS)  = (/ ip3,  ip4/)

    RETURN
  END SUBROUTINE Surf_SwapSingleEdge



  !>
  !!    Find out some relevant triangles, nodes, and edges of a given an edge.
  !!  @param[in]  Surf : the triangular mesh.
  !!  @param[in]  IS   : the given edge.
  !!  @param[out] itL  : the left triangle of edge IS.
  !!  @param[out] itR  : the right triangle of edge IS.
  !!                     The right triangle is the one which has 
  !!                         the same orientation of the edge IS.
  !!  @param[out] ipL  : the left point 
  !!                      (i.e. the point of triangle itL and oppositing to edge IS).
  !!  @param[out] ipR  : the right point 
  !!                      (i.e. the point of triangle itR and oppositing to edge IS).
  !!  @param[out] j2L  : the edge on triangle itL and has node IP_Edge(2,IS).
  !!  @param[out] j1L  : the edge on triangle itL and has node IP_Edge(1,IS).
  !!  @param[out] j2R  : the edge on triangle itR and has node IP_Edge(2,IS).
  !!  @param[out] j1R  : the edge on triangle itR and has node IP_Edge(1,IS).
  !<
  SUBROUTINE Surf_GetEdgeSurround(Surf, IS,itL,itR,ipL,ipR,j2L,j1L,j2R,j1R)
    USE CellConnectivity
    USE SurfaceMeshStorage
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER, INTENT(in)  :: IS
    INTEGER, INTENT(out) :: itL,itR,ipL,ipR,j2L,j1L,j2R,j1R
    INTEGER :: nlth,nrth

    itR = Surf%ITR_Edge(1,IS)
    itL = Surf%ITR_Edge(2,IS)

    nlth = which_NodeinTri(IS, Surf%IED_Tri(:,itL))
    nrth = which_NodeinTri(IS, Surf%IED_Tri(:,itR))

    IF(nrth==0 .OR.  nlth==0) THEN
       WRITE(*,*) 'Error--- miss the edge: IS=',   IS
       WRITE(*,*) '         itR=',itR,' ieds=',Surf%IED_Tri(:,itR)
       WRITE(*,*) '         itL=',itL,' ieds=',Surf%IED_Tri(:,itL)
       CALL Error_STOP (  '--- Surf_GetEdgeSurround')
    ENDIF

    ipL = Surf%IP_Tri(nlth,itL)
    ipR = Surf%IP_Tri(nrth,itR)

    IF(nlth==1)THEN
       j2L = Surf%IED_Tri(3,itL)
       j1L = Surf%IED_Tri(2,itL)
    ELSEIF(nlth==2)THEN
       j2L = Surf%IED_Tri(1,itL)
       j1L = Surf%IED_Tri(3,itL)
    ELSE
       j2L = Surf%IED_Tri(2,itL)
       j1L = Surf%IED_Tri(1,itL)
    ENDIF
    IF(nrth==1)THEN
       j2R = Surf%IED_Tri(2,itR)
       j1R = Surf%IED_Tri(3,itR)
    ELSEIF(nrth==2)THEN
       j2R = Surf%IED_Tri(3,itR)
       j1R = Surf%IED_Tri(1,itR)
    ELSE
       j2R = Surf%IED_Tri(1,itR)
       j1R = Surf%IED_Tri(2,itR)
    ENDIF

    RETURN
  END SUBROUTINE Surf_GetEdgeSurround


  !*******************************************************************************
  !>
  !!   Calculate the metric at each surface point from the triangulation itself.
  !!   @param[in]  Surf 
  !!   @param[out] Stretch (6,Surf%NB_Point)    see module Metric3D.
  !<       
  !*******************************************************************************
  SUBROUTINE Surf_getStretch(Surf,  Stretch)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    REAL*8,  DIMENSION(:,:), INTENT(OUT)     :: Stretch
    REAL*8 , PARAMETER   :: PI     = 3.141592653589793D0

    INTEGER :: ib, ip, i,j, ip3(3), ipt
    INTEGER, DIMENSION(:  ), ALLOCATABLE :: idx
    REAL*8,  DIMENSION(:,:), ALLOCATABLE :: dxyz      !--- (3,:)
    REAL*8,  DIMENSION(:  ), ALLOCATABLE :: xlmax
    REAL*8  :: xl, xlsq, dx, dy, dz, dx2, dy2, city, citz, xlmin
    INTEGER :: ibmin,ip1i,ip2i

    ALLOCATE(xlmax(Surf%NB_Point),dxyz(3,Surf%NB_Point))
    ALLOCATE(idx(Surf%NB_Point))

    IF(SIZE(Stretch,1)<6 .OR. SIZE(Stretch,2)<Surf%NB_Point)THEN
       WRITE(*,*)' Error---- the size of Stretch is small: ',   &
            SIZE(Stretch,1),SIZE(Stretch,2),Surf%NB_Point
       CALL Error_Stop('Surf_getStretch:: ')
    ENDIF
    xlmax(1:Surf%NB_Point) = 0.
    idx(1:Surf%NB_Point)   = 0

    !--- Find the maximum spacing and the direction of stretching of each surface node.
    !--- Treat it as the long axis of an ellipse

    DO ib=1,Surf%NB_Tri
       ip3(:) = Surf%IP_Tri(1:3,ib)
       DO i=1,2
          DO j=i+1,3
             xl = Geo3D_Distance(Surf%Posit(:,ip3(i)),Surf%Posit(:,ip3(j)))
             IF(xl>xlmax(ip3(i)))THEN
                xlmax(ip3(i)) = xl
                idx(ip3(i)) = ip3(j)
             ENDIF
             IF(xl>xlmax(ip3(j)))THEN
                xlmax(ip3(j)) = xl
                idx(ip3(j)) = ip3(i)
             ENDIF
          ENDDO
       ENDDO
    ENDDO

    DO ip=1,Surf%NB_Point
       ipt = idx(ip)
       dx = (Surf%Posit(1,ipt)-Surf%Posit(1,ip)) / xlmax(ip)
       dy = (Surf%Posit(2,ipt)-Surf%Posit(2,ip)) / xlmax(ip)
       dz = (Surf%Posit(3,ipt)-Surf%Posit(3,ip)) / xlmax(ip)
       IF(ABS(ABS(dy)-1.d0)<1.e-6)THEN
          citz = PI/2.d0
          city = 0.d0
       ELSE
          citz = dasin(dy)
          city = datan2(-dz,dx)
       ENDIF
       stretch(1,ip)   = xlmax(ip)
       stretch(4:6,ip) = (/0.d0, city, citz/)
       dxyz(1:3,ip)    = (/dx,dy,dz/)
    ENDDO

    !--- Find the length of the short axis of the ellipse of each surface node.
    !--- The ellipse is the smallest one which covers all the neighbour points

    xlmax(1:Surf%NB_Point) = 0.

    DO ib=1,Surf%NB_Tri
       ip3(:) = Surf%IP_Tri(1:3,ib)
       DO i=1,3
          ip = ip3(i)
          DO j=1,3
             ipt = ip3(j)
             IF(j==i .OR. idx(ip)==ipt) CYCLE
             dx = (Surf%Posit(1,ipt)-Surf%Posit(1,ip))
             dy = (Surf%Posit(2,ipt)-Surf%Posit(2,ip))
             dz = (Surf%Posit(3,ipt)-Surf%Posit(3,ip))
             dx2= (dx*dxyz(1,ip) + dy*dxyz(2,ip) + dz*dxyz(3,ip))**2
             dy2= dx*dx + dy*dy + dz*dz - dx2
             !--  IF(dy2<1.d-8*dx2) CYCLE
             !--  xl = dy2/(1.d0-dx2/(stretch(1,ip)**2))
             xl = dy2
             IF(xl>xlmax(ip))THEN
                xlmax(ip) = xl
             ENDIF
          ENDDO
       ENDDO
    ENDDO

    DO ip=1,Surf%NB_Point
       stretch(2:3,ip) = dsqrt(xlmax(ip))
    ENDDO

    DEALLOCATE(xlmax,dxyz)
    DEALLOCATE(idx)

    RETURN

  END SUBROUTINE Surf_getStretch

  !*******************************************************************************
  !>
  !!   Calculate the grid size from the surface.
  !!   @param[in]  Surf      
  !!   @param[out] Scalar : see MODULE ScalarCalculator
  !<       
  !*******************************************************************************
  SUBROUTINE Surf_getScalar(Surf,  Scalar, Surf_getScalar_Model)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    REAL*8,  DIMENSION(:), INTENT(OUT)     :: Scalar 

    INTEGER :: IP, IB, ip1, ip2, i
    REAL*8  :: dd, f1, f2
    INTEGER, DIMENSION(:), ALLOCATABLE :: modep
    INTEGER, INTENT(IN) :: Surf_getScalar_Model

    IF(SIZE(Scalar,1)<Surf%NB_Point)THEN
       WRITE(*,*)' Error---- the size of Scalar is small: ',SIZE(Scalar,1),Surf%NB_Point
       CALL Error_Stop('Surf_getScalar:: ')
    ENDIF
    IF(Surf_getScalar_Model<1 .OR. Surf_getScalar_Model>5)THEN
       WRITE(*,*)' Error---- wrong Surf_getScalar_Model: ',Surf_getScalar_Model
       CALL Error_Stop('Surf_getScalar:: ')
    ENDIF

    ALLOCATE(modep(Surf%NB_Point))
    modep(1:Surf%NB_Point) = 0
    IF(Surf_getScalar_Model==1)THEN
       Scalar(1:Surf%NB_Point) = 1.d0
    ELSE IF(Surf_getScalar_Model==5)THEN
       Scalar(1:Surf%NB_Point) = 1.d20
    ELSE
       Scalar(1:Surf%NB_Point) = 0.d0
    ENDIF

    DO IB = 1,Surf%NB_Tri
       DO i = 1,3
          ip1 = Surf%IP_Tri(1,IB)
          ip2 = Surf%IP_Tri(2,IB)
          IF(i==2) ip2 = Surf%IP_Tri(3,IB)
          IF(i==3) ip1 = Surf%IP_Tri(3,IB)
          dd = Geo3D_Distance(Surf%Posit(:,ip1),Surf%Posit(:,ip2))
          modep(ip1) = modep(ip1) + 1
          modep(ip2) = modep(ip2) + 1
          IF(Surf_getScalar_Model==1)THEN
             Scalar(ip1) = Scalar(ip1) * dd
             Scalar(ip2) = Scalar(ip2) * dd               
          ELSE IF(Surf_getScalar_Model==2)THEN
             Scalar(ip1) = Scalar(ip1) + dd
             Scalar(ip2) = Scalar(ip2) + dd               
          ELSE IF(Surf_getScalar_Model==3)THEN
             Scalar(ip1) = Scalar(ip1) + 1.d0/dd
             Scalar(ip2) = Scalar(ip2) + 1.d0/dd               
          ELSE IF(Surf_getScalar_Model==4)THEN
             Scalar(ip1) = MAX(Scalar(ip1), dd)
             Scalar(ip2) = MAX(Scalar(ip2), dd)               
          ELSE IF(Surf_getScalar_Model==5)THEN
             Scalar(ip1) = MIN(Scalar(ip1), dd)
             Scalar(ip2) = MIN(Scalar(ip2), dd)
          ENDIF
       ENDDO
    ENDDO

    DO ip=1,Surf%NB_Point
       IF(Surf_getScalar_Model==1)THEN
          Scalar(ip) = Scalar(ip) ** (1.d0/ modep(ip))
       ELSE IF(Surf_getScalar_Model==2)THEN
          Scalar(ip) = Scalar(ip) / modep(ip)
       ELSE IF(Surf_getScalar_Model==3)THEN
          Scalar(ip) = modep(ip) / Scalar(ip)
       ENDIF
    ENDDO
    DEALLOCATE(modep)

    RETURN

  END SUBROUTINE Surf_getScalar

  !>
  !!     Check the orientation of a enclosed surface.
  !!     @param[in]  Surf  :    the surface mesh.
  !!     @param[in]  Value :    the values at each point.
  !!     @param[in]  Vmin,Vmax  : the minimum and maximum values for contours.
  !!     @param[in]  NB_Contour : the number of contours.
  !!     @param[in]  IO         : the output channel;
  !<
  SUBROUTINE Surf_getContour(Surf,Values, Vmin, Vmax, NB_Contour, IO)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    REAL*8, INTENT(IN) :: Values(*), Vmin, Vmax
    INTEGER, INTENT(IN) :: NB_Contour, IO

    INTEGER :: NC, i, j, n, ip, IE, IT, IT2, k, IEstart, nlist, checked, IEt
    REAL*8  :: V, pt(3), ps(3,2), vs(2), xl, VTiny

    TYPE(Point3DQueueType), DIMENSION(:), POINTER :: pts
    TYPE(IntQueueType),     DIMENSION(:), POINTER :: stripeLen
    REAL*8,  DIMENSION(:), POINTER :: Vc
    INTEGER, DIMENSION(:), POINTER :: mark
    INTEGER, DIMENSION(:), POINTER :: edgelist
    REAL*8,  DIMENSION(NB_Contour) :: Vcont

    IF(NB_Contour==0) RETURN

    CALL Surf_BuildEdge (Surf)
    
    !--- find edge list put boundary edge first
    ALLOCATE(edgelist(Surf%NB_Edge))
    IEt = 0
    DO IE = 1, Surf%NB_Edge
       IF(Surf%ITr_Edge(2,IE)<=0)THEN
          IEt = IEt + 1
          edgelist(IEt) = IE
       ENDIF       
    ENDDO
    DO IE = 1, Surf%NB_Edge
       IF(Surf%ITr_Edge(2,IE)>0)THEN
          IEt = IEt + 1
          edgelist(IEt) = IE
       ENDIF       
    ENDDO
    IF(IEt/=Surf%NB_Edge) Stop 'dmifjweifjirejfrii'

    ALLOCATE(pts(NB_Contour))
    ALLOCATE(stripeLen(NB_Contour))
    ALLOCATE(mark(Surf%NB_Edge))
    ALLOCATE(Vc(Surf%NB_Point))

    VTiny = 1.e-6*(Vmax-Vmin)/NB_Contour
    Vc(1:Surf%NB_Point) = Values(1:Surf%NB_Point)
    checked = 100000+NB_Contour

    !--- loop for each contour
    Loop_NC : DO NC = 1, NB_Contour
       IF(NB_Contour==1)THEN
          v = (Vmin+Vmax) / 2.d0
       ELSE
          v = Vmin + (NC-1) * (Vmax-Vmin) / (NB_Contour-1)
       ENDIF
       Vcont(NC) = V

       !--- adjust the field
       DO ip = 1, Surf%NB_Point
          IF(ABS(Vc(ip)-V)<VTiny)THEN
             IF(Vc(ip)>V)THEN
                Vc(ip) = V + VTiny
             ELSE IF(Vc(ip)<V)THEN
                Vc(ip) = V - VTiny
             ELSE IF(NC<=NB_Contour/2)THEN
                Vc(ip) = V + VTiny
             ELSE
                Vc(ip) = V - VTiny
             ENDIF
          ENDIF
       ENDDO

       mark(:) = 0
       IEstart = 1

       !--- loop for each stripe
       Loop_Stripe : DO WHILE(IEstart<Surf%NB_Edge)

          !--- find a strat edge and triangle
          DO IEt = IEstart, Surf%NB_Edge
             IE = edgelist(IEt)
             IF( mark(IE) /= 0) CYCLE
             vs  = Vc(Surf%IP_Edge(:,IE))
             IF(MINVAL(vs)>v .OR. MAXVAL(vs)<v)THEN
                mark(IE) = checked
             ELSE
                ps(:,:) = Surf%Posit(:,Surf%IP_Edge(:,IE))
                xl = (v-vs(1))/(vs(2)-vs(1))
                pt = ps(:,1) + xl*(ps(:,2) - ps(:,1))
                nlist = 1
                CALL Point3dQueue_Push(Pts(NC), pt)
                mark(IE) = NC
                EXIT
             ENDIF
          ENDDO
          IF(IEt>Surf%NB_Edge) EXIT
          IEstart = IEt
          IT = Surf%ITr_Edge(1,IE)

          !--- loop on trinagles
          Loop_Tri : DO
             k  = which_NodeinTri(IE,Surf%Ied_Tri(:,IT))
             DO i=1,2
                k = k+1
                IF(k>3) k = 1
                IE = Surf%Ied_Tri(k,IT)
                vs = Vc(Surf%IP_Edge(:,IE))
                IF(mark(IE)==checked) CYCLE
                IF(mark(IE)==NC)THEN
                   !--- find enclosed end of this cycle
                   nlist = nlist+1
                   CALL IntQueue_Push(stripeLen(NC), nlist)
                   ps(:,:) = Surf%Posit(:,Surf%IP_Edge(:,IE))
                   xl = (v-vs(1))/(vs(2)-vs(1))
                   pt = ps(:,1) + xl*(ps(:,2) - ps(:,1))
                   CALL Point3dQueue_Push(Pts(NC), pt)
                   CYCLE Loop_Stripe
                ENDIF
                IF(mark(IE)/=0)THEN
                   WRITE(*,*)' Error--- sounds contour cross----'
                   CALL IntQueue_Push(stripeLen(NC), nlist)
                   WRITE(*,*)'   NC=', NC, ' IE=',IE, ' mark=',mark(IE), ' IT=',IT
                   WRITE(*,*)' ns=', stripeLen(NC)%numNodes, ' ls=',nlist
                   EXIT Loop_nc
                   !  STOP 'dodicndsovnieorhvif'
                ENDIF

                IF(MINVAL(vs)>v .OR. MAXVAL(vs)<v)THEN
                   mark(IE) = checked
                ELSE
                   ps(:,:) = Surf%Posit(:,Surf%IP_Edge(:,IE))
                   xl = (v-vs(1))/(vs(2)-vs(1))
                   pt = ps(:,1) + xl*(ps(:,2) - ps(:,1))
                   nlist = nlist+1
                   CALL Point3dQueue_Push(Pts(NC), pt)
                   mark(IE) = NC
                   EXIT
                ENDIF
             ENDDO
             IF(i>2)THEN
                WRITE(*,*) 'Error--- Enter to a wrong triangle IT=',IT
                WRITE(*,*) '   IEs= ',Surf%Ied_Tri(:,IT)
                WRITE(*,*) '   mark=',mark(Surf%Ied_Tri(:,IT))
                WRITE(*,*) ' NC, nlist, np=',NC, stripeLen(NC)%numNodes, nlist
                CALL Error_STOP ('Surf_getContour:: Enter to a wrong triangle')
             ENDIF

             IT2 = Surf%ITr_Edge(1,IE)
             IF(IT2==IT) IT2 = Surf%ITr_Edge(2,IE)
             IT = IT2
             IF(IT<=0)THEN
                !--- find open end of this cycle
                CALL IntQueue_Push(stripeLen(NC), nlist)
                CYCLE Loop_Stripe           
             ENDIF
          ENDDO Loop_Tri

       ENDDO Loop_Stripe

    ENDDO Loop_NC

    !--- output
    WRITE(io,*)'#Number of Coutours'
    WRITE(io,*)'#',NB_Contour  
    WRITE(io,*)'#Number of stripe of each Contour, and number of points for each stripe'
    DO NC = 1, NB_Contour
       WRITE(io,*)'#',stripeLen(NC)%numNodes, Vcont(NC)
       DO i = 1, stripeLen(NC)%numNodes
          WRITE(io,*)'#      ', stripeLen(NC)%Nodes(i)
       ENDDO
    ENDDO
    WRITE(io,*)'#Coordinates of each point'
    DO NC = 1, NB_Contour
       n = 0
       DO i = 1, stripeLen(NC)%numNodes
          DO j = 1, ABS( stripeLen(NC)%Nodes(i))
             n = n+1
             WRITE(io,*) Pts(NC)%Pt(:,n)
          ENDDO
          WRITE(io,*)
          WRITE(io,*)
       ENDDO
    ENDDO

    DEALLOCATE(pts, stripeLen, Vc, Mark, edgelist)


  END SUBROUTINE Surf_getContour







END MODULE SurfaceMeshManager

