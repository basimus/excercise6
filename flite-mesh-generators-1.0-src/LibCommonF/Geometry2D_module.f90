!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!-- MODULE Geometry2D
!--   FUNCTION Posit_Transf_2D(P(2), Stretch(4)) RESULT(Pt(2))
!--   FUNCTION Posit_TransfRev_2D(P(2), Stretch(4)) RESULT(Pt(2))
!--   FUNCTION Distance_SQ_2D(P1(2),P2(2)) RESULT(Dis_SQ)
!--   FUNCTION Cross_Product_2DP(P1(2),P2(2),P3(2)) RESULT(Product)
!--   FUNCTION Cross_Product_2DV(V1(2),V2(2)) RESULT(Product)
!--   FUNCTION Dot_Product_2DP(P1(2),P2(2),P3(2)) RESULT(Product)
!--   FUNCTION Dot_Product_2DV(V1(2),V2(2)) RESULT(Product)
!--   FUNCTION Included_Angle_2DP(P1(2),P2(2),P3(2)) RESULT(Angle)
!--   FUNCTION Included_Angle_2DV(V1(2),V2(2)) RESULT(Angle)
!--   FUNCTION Included_Angle_Bisect(V1(2),V2(2),iDir)  RESULT(VP(2))
!--   FUNCTION Circle_Centre_2D(P1(2),P2(2),P3(2)) RESULT(PO(2))
!--   SUBROUTINE Circle_Intersection(p1(2),r1,p2(2),r2, pc1(2), pc2(2))
!--   FUNCTION Product_Matrix2D(fMatrix1(2,2),fMatrix2(2,2)) RESULT(Product(2,2))
!--   FUNCTION Inverte_Matrix2D(fMatrix(2,2)) RESULT(rev(2,2))
!--   SUBROUTINE Eigen_Matrix2D(fMatrix(2,2),value1,value2,vector1(2),vector2(2))
!--   FUNCTION Tri_Geometry2D(P1(2), P2(2), P3(2)) RESULT(Geo(7))
!--   FUNCTION Tri_GetWeight(P1(2), P2(2), P3(2), P0(2)) RESULT(W(3))
!--   RECURSIVE FUNCTION Intersect_RecTri_2D(pp3(2,3),x1,x2,y1,y2) RESULT(area)
!--   FUNCTION Linear_Interpolate_2D(P1(2),P2(2),xy,ij) RESULT(yx)
!--
!-- MODULE Metric2D
!--   FUNCTION Stretch_Metric2D(Stretch(4)) RESULT(fMetric(3))
!--   FUNCTION Metric2D_Distance(P1(2),P2(2),fMetric(3)) RESULT(Dist)
!--   FUNCTION Metric2D_Distance_SQ(P1(2),P2(2),fMetric(3)) RESULT(Dis_SQ)
!--   FUNCTION Cross_Product_Metric2D(P1(2),P2(2),P3(2),fMetric(3)) RESULT(Product)
!--   FUNCTION Dot_Product_Metric2D(P1(2),P2(2),P3(2),fMetric(3)) RESULT(Product)
!--   FUNCTION Included_Angle_Metric2D(P1(2),P2(2),P3(2),fMetric(3)) RESULT(Angle)
!--   FUNCTION Product_Metric2D(fMetric1(3),fMetric2(3)) RESULT(Product(2,2))
!--   FUNCTION Inverte_Metric2D(fMetric(3)) RESULT(rev(3))
!--   SUBROUTINE Eigen_Metric2D(fMetric(3), value1, value2, vector1(2), vector2(2))
!--   FUNCTION Interpolate_Metric2D(fMetric1(3),fMetric2(3),t) RESULT(fMetric(3))
!--   FUNCTION Metric2D_Mean_Weight(n,fMes(3,*),w(*)) RESULT(fMetric(3))
!--   FUNCTION Intersect_Metric2D(fMetric1(3),fMetric2(3)) RESULT(fMetric(3))
!--   FUNCTION TriQuality_Metric2D(p1(2),p2(2),p3(2),fMetric(3)) RESULT(dist)
!--
!-- MODULE Line2DGeom
!-- FUNCTION Line2D_Build(P1(2),P2(2)) RESULT(aLine)
!-- FUNCTION Line2D_ProjectOnLine(P0(2),aLine) RESULT(PP(2))
!-- FUNCTION Line2D_ProjectOnLine3(P0(2),P1(2),P2(2)) RESULT(PP(2))
!-- FUNCTION Line2D_DistancePointToLine(P0(2),aLine) RESULT(Dist)
!-- FUNCTION Line2D_SignedDistancePointToLine(P0(2),aLine) RESULT(Dist)
!-- FUNCTION Line2D_Sphere_Sphere_Cross(SP1(3),SP2(3)) RESULT(aLine)



!>
!!  Geometry in 2D space and algebra for 2*2 matrices
!<
MODULE Geometry2D

  INTERFACE Dot_Product_2D
     MODULE PROCEDURE Dot_Product_2DP, &
                      Dot_Product_2DV
  END INTERFACE

  INTERFACE Cross_Product_2D
     MODULE PROCEDURE Cross_Product_2DP, &
                      Cross_Product_2DV
  END INTERFACE
  
  INTERFACE Included_Angle_2D
     MODULE PROCEDURE Included_Angle_2DP, &
                      Included_Angle_2DV
  END INTERFACE


CONTAINS


  !>
  !!    Transfer the position of a point from physical domain to computational domain.
  !!    @param[in] P            positions in the physical domain.
  !!    @param[in] Stretch (1)   stretch scale in x-direction of the new system.  \n
  !!                       (2)   stretch scale in y-direction of the new system.  \n
  !!                       (3)   Cosine of Rotation Angle.                        \n
  !!                       (4)   Sine of Rotation Angle.
  !!    @return          positions in the computational domain.
  !<
  FUNCTION Posit_Transf_2D(P, Stretch) RESULT(Pt)
    IMPLICIT NONE
    REAL*8, PARAMETER :: TINY = 1.D-6
    REAL*8 :: P(2), Stretch(4), Pt(2)
    REAL*8 :: SinA, CosA
    SinA = Stretch(4)
    CosA = Stretch(3)
    Pt(1) = ( P(1)*CosA + P(2)*SinA) / Stretch(1)
    Pt(2) = (-P(1)*SinA + P(2)*CosA) / Stretch(2)
    RETURN
  END FUNCTION Posit_Transf_2D

  !>
  !!    Transfer the position of a point from computational domain to physical domain.
  !!    @param[in] P            positions in the computational domain.
  !!    @param[in] Stretch (1)   stretch scale in x-direction of the new system. \n
  !!                       (2)   stretch scale in y-direction of the new system. \n
  !!                       (3)   Cosine of Rotation Angle.                       \n
  !!                       (4)   Sine of Rotation Angle.
  !!    @return          positions in the physical domain.
  !<
  FUNCTION Posit_TransfRev_2D(P, Stretch) RESULT(Pt)
    IMPLICIT NONE
    REAL*8, PARAMETER :: TINY = 1.D-6
    REAL*8 :: P(2), Stretch(4), Pt(2)
    REAL*8 :: SinA, CosA, pp(2)
    pp(1:2) = P(1:2) * Stretch(1:2)
    SinA = -Stretch(4)
    CosA =  Stretch(3)
    Pt(1) = ( pp(1)*CosA + pp(2)*SinA)
    Pt(2) = (-pp(1)*SinA + pp(2)*CosA)
    RETURN
  END FUNCTION Posit_TransfRev_2D

  !>
  !!    Calculate the square of distance between two points.
  !!    @param[in] P1,P2     coordinates of the two points
  !!    @return              the square of distance between them
  !<
  FUNCTION Distance_SQ_2D(P1,P2) RESULT(Dis_SQ)
    IMPLICIT NONE
    REAL*8 :: P1(2), P2(2), Dis_SQ
    Dis_SQ = (P1(1)-P2(1))**2 + (P1(2)-P2(2))**2
    RETURN
  END FUNCTION Distance_SQ_2D

  !>
  !!    Calculate the Cross Product of two vectors P2-P1 & P2-P3.
  !!    @param[in] P1,P2,P3  coordinates of the three vertices
  !!    @return              the the Cross Product of two vectors
  !!                         ( double of the area of the triangle ).  \n
  !!                          >0 for anti-clockwise angle.  \n
  !!                          <0 for clockwise  angle.
  !<
  FUNCTION Cross_Product_2DP(P1,P2,P3) RESULT(Product)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(2), P2(2), P3(2)
    REAL*8 :: Product
    Product = (P1(1)-P2(1))*(P3(2)-P2(2)) - (P1(2)-P2(2))*(P3(1)-P2(1))
    RETURN
  END FUNCTION Cross_Product_2DP

  !>
  !!    Calculate the Cross Product of two vectors V1 & V2.
  !!    @param[in] V1,V2     the two vectors
  !!    @return              the the Cross Product of two vectors
  !!                         ( double of the area of the triangle ).  \n
  !!                          >0 for anti-clockwise angle.  \n
  !!                          <0 for clockwise  angle.
  !<
  FUNCTION Cross_Product_2DV(V1,V2) RESULT(Product)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: V1(2), V2(2)
    REAL*8 :: Product
    Product = V1(1) * V2(2) - V1(2) * V2(1)
    RETURN
  END FUNCTION Cross_Product_2DV

  !>
  !!    Calculate the Dot Product of two vectors P2-P1 & P2-P3.
  !!    @param[in] P1,P2,P3  coordinates of the three vertices.
  !!    @return              the the Dot Product of two vectors.
  !<
  FUNCTION Dot_Product_2DP(P1,P2,P3) RESULT(Product)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(2), P2(2), P3(2)
    REAL*8 :: Product
    Product = (P1(1)-P2(1))*(P3(1)-P2(1)) + (P1(2)-P2(2))*(P3(2)-P2(2))
    RETURN
  END FUNCTION Dot_Product_2DP

  !>
  !!    Calculate the Dot Product of two vectors V1 & V2.
  !!    @param[in] V1,V2     the two vectors
  !!    @return              the the Dot Product of two vectors.
  !<
  FUNCTION Dot_Product_2DV(V1,V2) RESULT(Product)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: V1(2), V2(2)
    REAL*8 :: Product
    Product = V1(1) * V2(1) + V1(2) * V2(2)
    RETURN
  END FUNCTION Dot_Product_2DV

  !>
  !!    Calculate the Included Angle of two vectors P2-P1 & P2-P3.
  !!    @param[in] P1,P2,P3  coordinates of the three vertices.
  !!    @return             the Included Angle of two vectors
  !!                              ( -PI < a <= PI )  (right hand).
  !<
  FUNCTION Included_Angle_2DP(P1,P2,P3) RESULT(Angle)
    IMPLICIT NONE
    REAL*8 :: P1(2), P2(2), P3(2), Angle
    REAL*8, PARAMETER :: TINY = 1.D-20
    REAL*8  :: crop,dotp
    IF( ABS(P1(1)-P2(1)) + ABS(P1(2)-P2(2)) <= TINY .OR.    &
         ABS(P3(1)-P2(1)) + ABS(P3(2)-P2(2)) <= TINY )THEN
       Angle = 1000.
    ELSE
       crop  = Cross_Product_2D(P1,P2,P3)
       dotp  =   Dot_Product_2D(P1,P2,P3)
       Angle = ATAN2(crop,dotp)
    ENDIF
    RETURN
  END FUNCTION Included_Angle_2DP

  !>
  !!    Calculate the Included Angle of two vectors P2-P1 & P2-P3.
  !!    @param[in] P1,P2,P3  coordinates of the three vertices.
  !!    @return             the Included Angle of two vectors
  !!                              ( -PI < a <= PI )  (right hand).
  !<
  FUNCTION Included_Angle_2DV(V1,V2) RESULT(Angle)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: V1(2), V2(2)
    REAL*8 :: Angle
    REAL*8, PARAMETER :: TINY = 1.D-20
    REAL*8  :: crop,dotp
    IF( ABS(V1(1)) + ABS(V1(2)) <= TINY .OR.    &
         ABS(V2(1)) + ABS(V2(2)) <= TINY )THEN
       Angle = 1000.
    ELSE
       crop  = Cross_Product_2DV(V1,V2)
       dotp  =   Dot_Product_2D(V1,V2)
       Angle = ATAN2(crop,dotp)
    ENDIF
    RETURN
  END FUNCTION Included_Angle_2DV

  !>
  !!    Find the direction which bisect the angle between vector V1 and V2 
  !!    @param[in] V1,V2  : UNIT vectors composing the angle.
  !!    @param[in] iDir   = 1,  from V1 to V2 by right hand.
  !!                      =-1,  form V1 to V2 by left hand.
  !!                      = 0 or not presented, the smaller angle between V1 and V2.
  !!    @return       VP  : the biescting unit direction. 
  !<
  FUNCTION Included_Angle_Bisect(V1,V2,iDir)  RESULT(VP)
    IMPLICIT NONE
    REAL*8,  INTENT(IN)  :: V1(2), V2(2)
    INTEGER, INTENT(IN), OPTIONAL :: iDir
    REAL*8 :: VP(2)
    REAL*8, PARAMETER :: EPS = 1.D-12
    REAL*8  :: crop,dotp
    VP(:) = V1(:) + V2(:)
    dotp = DSQRT(VP(1)*VP(1) + VP(2)*VP(2))
    IF(dotp<EPS)THEN
       VP(1) = -V1(2)
       VP(2) =  V1(1)
    ELSE
       VP = VP / dotp
    ENDIF
    IF(.NOT. PRESENT(iDir))  RETURN
    IF(iDir==0)              RETURN
    crop  = V1(1)*V2(2) - V1(2)*V2(1)
    IF(iDir * crop>0)        RETURN
    IF(crop==0 .and. iDir>0) RETURN
    VP = -VP
   RETURN
  END FUNCTION Included_Angle_Bisect


  !>
  !!    Calculate the centre of the Circumscribed circle of a triangle.
  !!    @param[in] P1,P2,P3   coordinates of the three vertices.
  !!    @return               coordinates of the centre of
  !!                               the Circumscribed circle.
  !<
  FUNCTION Circle_Centre_2D(P1,P2,P3) RESULT(PO)
    IMPLICIT NONE
    REAL*8  :: P1(2), P2(2), P3(2), PO(2)
    REAL*8  :: a, b, d, e, c, f, d1, d2, d3, te
    a         =  2.D0*(P3(1)-P2(1))
    b         =  2.D0*(P3(2)-P2(2))
    d         =  2.D0*(P1(1)-P2(1))
    e         =  2.D0*(P1(2)-P2(2))
    d1        =  P1(1)*P1(1)+P1(2)*P1(2)
    d2        =  P2(1)*P2(1)+P2(2)*P2(2)
    d3        =  P3(1)*P3(1)+P3(2)*P3(2)
    c         =  d3-d2
    f         =  d1-d2
    te        =  a*e-b*d
    PO(1)   =  (c*e-f*b) / te
    PO(2)   =  (a*f-c*d) / te
    RETURN
  END FUNCTION Circle_Centre_2D

  !>
  !!   Calculate the two intersection points of two circles.
  !!    @param[in] p1,p2  coordinates of the circle-centres.
  !!    @param[in] r1,r2  radia of the circles.
  !!    @param[out] pc1,pc2  coordinates of the two intersection points.
  !!
  !! ref:   http://mathworld.wolfram.com/Circle-CircleIntersection.html
  !<
  SUBROUTINE Circle_Intersection(p1,r1,p2,r2, pc1, pc2)
    IMPLICIT NONE
    REAL*8, INTENT(in)  :: p1(2),p2(2), r1, r2
    REAL*8, INTENT(out) :: pc1(2), pc2(2)
    REAL*8 :: a, c, d, dd, t, x, pp(2), rr(2)

    dd= (p1(1)-p2(1))**2 + (p1(2)-p2(2))**2
    d = dsqrt(dd)
    t = dd - r2*r2 + r1*r1
    a = 4*r1*r1 - t*t/dd
    IF(a<0)THEN
       CALL Error_STOP ( ' no intersection')
    ENDIF
    a = dsqrt(a)/2.0
    x = t/(2*dd)
    rr = p2-p1
    pp = p1 + x*rr
    rr(1) =  p2(2) - p1(2)
    rr(2) = -p2(1) + p1(1)
    rr = (a / d) *rr
    pc1 = pp + rr
    pc2 = pp - rr

    RETURN
  END SUBROUTINE circle_intersection

  !>
  !!    Calculate the Product of two Matrices.
  !<
  FUNCTION Product_Matrix2D(fMatrix1,fMatrix2) RESULT(Product)
    IMPLICIT NONE
    REAL*8  :: fMatrix1(2,2), fMatrix2(2,2), PRODUCT(2,2)
    PRODUCT(1,1) = fMatrix1(1,1)*fMatrix2(1,1) + fMatrix1(1,2)*fMatrix2(2,1)
    PRODUCT(1,2) = fMatrix1(1,1)*fMatrix2(1,2) + fMatrix1(1,2)*fMatrix2(2,2)
    PRODUCT(2,1) = fMatrix1(2,1)*fMatrix2(1,1) + fMatrix1(2,2)*fMatrix2(2,1)
    PRODUCT(2,2) = fMatrix1(2,1)*fMatrix2(1,2) + fMatrix1(2,2)*fMatrix2(2,2)
  END FUNCTION Product_Matrix2D

  !>
  !!    Calculate the reverse matrix of a Matrix.
  !<
  FUNCTION Inverte_Matrix2D(fMatrix) RESULT(rev)
    IMPLICIT NONE
    REAL*8 :: fMatrix(2,2), rev(2,2), det
    det = fMatrix(1,1) * fMatrix(2,2) - fMatrix(1,2) * fMatrix(2,1)
    rev(1,1) =  fMatrix(2,2)/det
    rev(1,2) = -fMatrix(1,2)/det
    rev(2,1) = -fMatrix(2,1)/det
    rev(2,2) =  fMatrix(1,1)/det
    RETURN
  END FUNCTION Inverte_Matrix2D

  !>
  !!    Calculate the reverse matrix of a Matrix.
  !<
  SUBROUTINE Eigen_Matrix2D(fMatrix, value1, value2, vector1, vector2)
    IMPLICIT NONE
    REAL*8 :: fMatrix(2,2), value1, value2, vector1(2), vector2(2)
    REAL*8 :: det, ad, tinycase, vc1(2),vc2(2), dmod1,dmod2
    REAL*8, PARAMETER :: TINY = 1.D-8

    tinycase = TINY*MAX(ABS(fMatrix(1,1)),ABS(fMatrix(1,2)),    &
         ABS(fMatrix(2,1)),ABS(fMatrix(2,2)),1.d-6)
    tinycase = tinycase *  tinycase

    ad  = (fMatrix(1,1) + fMatrix(2,2))/2.d0
    det = fMatrix(1,2)*fMatrix(2,1) + (fMatrix(1,1)-fMatrix(2,2))**2 / 4.d0
    det = dsqrt(det)

    !--- two eigenvalues
    value1 = ad + det
    value2 = ad - det

    !--- first unit eigenvector
    vc1 = (/value1-fMatrix(2,2),  fMatrix(2,1)/)
    vc2 = (/fMatrix(1,2),  value1-fMatrix(1,1)/)
    dmod1 = vc1(1)*vc1(1) + vc1(2)*vc1(2)
    dmod2 = vc2(1)*vc2(1) + vc2(2)*vc2(2)
    IF(dmod1>dmod2 .AND. dmod1>tinycase)THEN
       vector1 = vc1 / dsqrt(dmod1)
    ELSE IF(dmod2>tinycase)THEN
       vector1 = vc2 / dsqrt(dmod2)
    ELSE
       vector1 = (/1.d0, 0.d0/)
    ENDIF

    !--- second unit eigenvector
    vc1 = (/value2-fMatrix(2,2),  fMatrix(2,1)/)
    vc2 = (/fMatrix(1,2),  value2-fMatrix(1,1)/)
    dmod1 = vc1(1)*vc1(1) + vc1(2)*vc1(2)
    dmod2 = vc2(1)*vc2(1) + vc2(2)*vc2(2)
    IF(dmod1>dmod2 .AND. dmod1>tinycase)THEN
       vector2 = vc1 / dsqrt(dmod1)
    ELSE IF(dmod2>tinycase)THEN
       vector2 = vc2 / dsqrt(dmod2)
    ELSE
       vector2 = (/0.d0, 1.d0/)
    ENDIF

    RETURN
  END SUBROUTINE Eigen_Matrix2D

  !>
  !!    This subroutine evaluates n,x & n,y for each element
  !!    shape function (linear triangles) and the jacobian (2*area)
  !!      (Modified form oubay's code).
  !<
  FUNCTION Tri_Geometry2D(P1, P2, P3) RESULT(Geo)
    IMPLICIT NONE
    REAL*8  :: P1(2), P2(2), P3(2), Geo(7)
    REAL*8  :: nxi(3),net(3),x21,x31,y21,y31,area2
    INTEGER :: i, ip

    nxi(:) = (/-1.0 , 1.0 , 0.0 /)
    net(:) = (/-1.0 , 0.0 , 1.0 /)

    !--- evaluate the geometrical quantities needed
    x21=P2(1)-P1(1)
    x31=P3(1)-P1(1)
    y21=P2(2)-P1(2)
    y31=P3(2)-P1(2)
    area2 = x21*y31-x31*y21
    IF(area2<=0.0)THEN
       WRITE(*,*)'Error--- Geometry2D : negative area '
       CALL Error_STOP (' ')
    ENDIF

    !--- form n,x & n,y
    Geo(1:3) = ( y31*nxi(:) - y21*net(:)) / area2
    Geo(4:6) = (-x31*nxi(:) + x21*net(:)) / area2
    Geo(7)   = area2

    RETURN
  END FUNCTION Tri_Geometry2D

  !>
  !!   Get weight of P0 in a triangle P1-P2-P3.
  !<
  FUNCTION Tri_GetWeight(P1, P2, P3, P0) RESULT(W)
    IMPLICIT NONE
    REAL*8, INTENT(IN)  :: P1(2), P2(2), P3(2), P0(2)
    REAL*8  :: W(3), WS
    W(1) = Cross_Product_2D(P2, P0, P3)
    W(2) = Cross_Product_2D(P3, P0, P1)
    W(3) = Cross_Product_2D(P1, P0, P2)
    WS   = W(1) + W(2) + W(3)
    W(:) = W(:) / WS
  RETURN
  END FUNCTION Tri_GetWeight



  !>
  !!    Calculate the intersect area of a rectangle and a triangle.
  !!    @param[in] pp3    position of three points in oreder of the triangle.
  !!    @param[in] x1,x2,y1,y2 range the rectangle (x1<x2, y1<y2).
  !!    @return    intersect area.
  !<
  RECURSIVE FUNCTION Intersect_RecTri_2D(pp3,x1,x2,y1,y2) RESULT(area)
    IMPLICIT NONE
    REAL*8  :: pp3(2,3),x1,x2,y1,y2,area
    INTEGER :: i1,i2,i3,ij1,ij2
    REAL*8  :: ppn(2,3),xy1,xy2

    area = 0.d0
    IF(pp3(1,1)<=x1 .AND. pp3(1,2)<=x1 .AND. pp3(1,3)<=x1) RETURN
    IF(pp3(1,1)>=x2 .AND. pp3(1,2)>=x2 .AND. pp3(1,3)>=x2) RETURN
    IF(pp3(2,1)<=y1 .AND. pp3(2,2)<=y1 .AND. pp3(2,3)<=y1) RETURN
    IF(pp3(2,1)>=y2 .AND. pp3(2,2)>=y2 .AND. pp3(2,3)>=y2) RETURN

    DO ij1=1,2
       ij2 = 3-ij1
       IF(ij1==1)THEN
          xy1 = x1
          xy2 = x2
       ELSE
          xy1 = y1
          xy2 = y2
       ENDIF

       DO i1=1,3
          i2=MOD(i1,3)+1
          i3=MOD(i2,3)+1

          !--- two points beyond
          IF(pp3(ij1,i1)>=xy1 .AND. pp3(ij1,i2)<xy1 .AND. pp3(ij1,i3)<xy1)THEN
             ppn(:,  1)=pp3(:,i1)
             ppn(ij1,2)=xy1
             ppn(ij2,2)=Linear_Interpolate_2D(pp3(:,i1),pp3(:,i2),xy1,ij1)
             ppn(ij1,3)=xy1
             ppn(ij2,3)=Linear_Interpolate_2D(pp3(:,i1),pp3(:,i3),xy1,ij1)
             area = Intersect_RecTri_2D(ppn,x1,x2,y1,y2)
             RETURN
          ENDIF
          IF(pp3(ij1,i1)<=xy2 .AND. pp3(ij1,i2)>xy2 .AND. pp3(ij1,i3)>xy2)THEN
             ppn(:,  1)=pp3(:,i1)
             ppn(ij1,2)=xy2
             ppn(ij2,2)=Linear_Interpolate_2D(pp3(:,i1),pp3(:,i2),xy2,ij1)
             ppn(ij1,3)=xy2
             ppn(ij2,3)=Linear_Interpolate_2D(pp3(:,i1),pp3(:,i3),xy2,ij1)
             area = Intersect_RecTri_2D(ppn,x1,x2,y1,y2)
             RETURN
          ENDIF

          !--- one point beyond
          IF(pp3(ij1,i1)<xy1 .AND. pp3(ij1,i2)>=xy1 .AND. pp3(ij1,i3)>=xy1)THEN
             ppn(ij1,1)=xy1
             ppn(ij2,1)=Linear_Interpolate_2D(pp3(:,i1),pp3(:,i3),xy1,ij1)
             ppn(ij1,2)=xy1
             ppn(ij2,2)=Linear_Interpolate_2D(pp3(:,i1),pp3(:,i2),xy1,ij1)
             ppn(:,  3)=pp3(:,i2)
             area = Intersect_RecTri_2D(ppn,x1,x2,y1,y2)
             ppn(:,  2)=pp3(:,i2)
             ppn(:,  3)=pp3(:,i3)
             area = area + Intersect_RecTri_2D(ppn,x1,x2,y1,y2)
             RETURN
          ENDIF
          IF(pp3(ij1,i1)>xy2 .AND. pp3(ij1,i2)<=xy2 .AND. pp3(ij1,i3)<=xy2)THEN
             ppn(ij1,1)=xy2
             ppn(ij2,1)=Linear_Interpolate_2D(pp3(:,i1),pp3(:,i3),xy2,ij1)
             ppn(ij1,2)=xy2
             ppn(ij2,2)=Linear_Interpolate_2D(pp3(:,i1),pp3(:,i2),xy2,ij1)
             ppn(:,  3)=pp3(:,i2)
             area = Intersect_RecTri_2D(ppn,x1,x2,y1,y2)
             ppn(:,  2)=pp3(:,i2)
             ppn(:,  3)=pp3(:,i3)
             area = area + Intersect_RecTri_2D(ppn,x1,x2,y1,y2)
             RETURN
          ENDIF

       ENDDO
    ENDDO

    !--- triangle lies in the rectangle
    area = 0.5d0 * Cross_Product_2D(pp3(:,3),pp3(:,2),pp3(:,1))

    RETURN
  END FUNCTION Intersect_RecTri_2D

  !<
  !!    @param[in]  P1,P2 Position of two points.
  !!    @param[in]  ij =1  input xy is an X-value and output yx is a  Y-value. \n
  !!                   =2  input xy is a  Y-value and output yx is an X-value
  !>
  FUNCTION Linear_Interpolate_2D(P1,P2,xy,ij) RESULT(yx)
    IMPLICIT NONE
    REAL*8  :: P1(2), P2(2), xy, yx
    INTEGER :: ij,ji
    ji = 3-ij
    yx = P1(ji) + (xy-P1(ij))/(P2(ij)-P1(ij))*(P2(ji)-P1(ji))
    RETURN
  END FUNCTION Linear_Interpolate_2D


END MODULE Geometry2D


!             /  a  b  \                                       
!             \  b  c  /  
!>
!!  Geometry and algebra in Euclidean space based on a 2x2 Metric  
!!                                                                 
!! \f[ \left( \begin{array}{cc}
!!       a & b  \\\
!!       b & c
!!             \end{array} \right) \f] 
!!
!!   The Metric saved in a array fMetric(1:3) = \f$ ( a, b, c ) \f$ .
!<
MODULE Metric2D

  USE Geometry2D

CONTAINS

  !>
  !!    convert stretch date to a Metric.
  !<
  FUNCTION Stretch_Metric2D(Stretch) RESULT(fMetric)
    IMPLICIT NONE
    REAL*8 :: Stretch(4),fMetric(3)
    REAL*8 :: str(4),Strtmp
    str(:)   = Stretch(:)
    Strtmp   = dsqrt(Str(3)**2+Str(4)**2)
    Str(3:4) = Str(3:4)/Strtmp
    str(1:2) = str(1:2)**2
    fMetric(2) = str(3)*str(4) * ( 1.d0/str(1) - 1.d0/str(2) )
    str(3:4) = str(3:4)**2
    fMetric(1) = str(3)/str(1) + str(4)/str(2)
    fMetric(3) = str(4)/str(1) + str(3)/str(2)
    RETURN
  END FUNCTION Stretch_Metric2D

  !>
  !!    Calculate the distance between two points in Euclidean space.
  !!    @param[in] P1,P2   coordinates of the two points.
  !!    @param[in] fMetric   the Metric
  !!    @return            the distance based on the Metric of P1.
  !<
  FUNCTION Metric2D_Distance(P1,P2,fMetric) RESULT(Dist)
    IMPLICIT NONE
    REAL*8  :: P1(2), P2(2), fMetric(3), Dist
    REAL*8  :: dx, dy
    dx = P2(1)-P1(1)
    dy = P2(2)-P1(2)
    Dist = dsqrt(  fMetric(1) *dx*dx + 2.D0* fMetric(2) *dx*dy    &
         + fMetric(3) *dy*dy )
    RETURN
  END FUNCTION Metric2D_Distance

  !>
  !!    Calculate the squart of distance between two points in Euclidean space.
  !!    @param[in] P1,P2   coordinates of the two points.
  !!    @param[in] fMetric   the Metric
  !!    @return            the distance based on the Metric of P1.
  !<
  FUNCTION Metric2D_Distance_SQ(P1,P2,fMetric) RESULT(Dis_SQ)
    IMPLICIT NONE
    REAL*8  :: P1(2), P2(2), fMetric(3), Dis_SQ
    REAL*8  :: dx, dy
    dx = P2(1)-P1(1)
    dy = P2(2)-P1(2)
    Dis_SQ = fMetric(1) *dx*dx + 2.D0* fMetric(2) *dx*dy + fMetric(3) *dy*dy
    RETURN
  END FUNCTION Metric2D_Distance_SQ

  !>
  !!    Calculate the Cross Product of two vectors P2-P1 & P2-P3.
  !!    @param[in] P1,P2,P3  coordinates of the three vertices.
  !!    @param[in] fMetric   the Metric
  !!    @return              the the Cross Product of two vectors
  !!                         ( double of the area of the triangle ). \n
  !!                         >0 for anti-clockwise angle.  \n
  !!                         <0 for clockwise  angle.
  !<
  FUNCTION Cross_Product_Metric2D(P1,P2,P3,fMetric) RESULT(Product)
    IMPLICIT NONE
    REAL*8 :: P1(2), P2(2), P3(2), fMetric(3), Product
    Product = (P1(1)-P2(1))*(P3(2)-P2(2)) - (P1(2)-P2(2))*(P3(1)-P2(1))
    Product = Product * SQRT(fMetric(1) * fMetric(3) - fMetric(2)**2)
    RETURN
  END FUNCTION Cross_Product_Metric2D

  !>
  !!    Calculate the Dot Product of two vectors P2-P1 & P2-P3.
  !!    @param[in] P1,P2,P3  coordinates of the three vertices.
  !!    @param[in] fMetric   the Metric
  !!    @return              the the Dot Product of two vectors.
  !<
  FUNCTION Dot_Product_Metric2D(P1,P2,P3,fMetric) RESULT(Product)
    IMPLICIT NONE
    REAL*8  :: P1(2), P2(2), P3(2), fMetric(3), Product
    REAL*8  :: dx1, dy1, dx2, dy2
    dx1 = P1(1)-P2(1)
    dy1 = P1(2)-P2(2)
    dx2 = P3(1)-P2(1)
    dy2 = P3(2)-P2(2)
    Product = fMetric(1) *dx1*dx2 + fMetric(2) *(dx1*dy2+dx2*dy1)    &
         + fMetric(3) *dy1*dy2
  END FUNCTION Dot_Product_Metric2D

  !>
  !!    Calculate the Included Angle of two vectors P2-P1 & P2-P3.
  !!    @param[in] P1,P2,P3  coordinates of the three vertices.
  !!    @param[in] fMetric   the Metric
  !!    @return              the Included Angle of two vectors
  !!                              ( -PI < a <= PI ).
  !<
  FUNCTION Included_Angle_Metric2D(P1,P2,P3,fMetric) RESULT(Angle)
    IMPLICIT NONE
    REAL*8 :: P1(2), P2(2), P3(2), fMetric(3), Angle
    REAL*8, PARAMETER :: TINY = 1.D-20
    REAL*8  :: crop,dotp
    IF(  ABS(P1(1)-P2(1)) + ABS(P1(2)-P2(2)) <= TINY .OR.    &
         ABS(P3(1)-P2(1)) + ABS(P3(2)-P2(2)) <= TINY )THEN
       Angle = 1000.
    ELSE
       crop  = Cross_Product_Metric2D(P1,P2,P3,fMetric)
       dotp  =   Dot_Product_Metric2D(P1,P2,P3,fMetric)
       Angle = ATAN2(crop,dotp)
    ENDIF
    RETURN
  END FUNCTION Included_Angle_Metric2D

  !>
  !!    Calculate the Product of two metrics.
  !<
  FUNCTION Product_Metric2D(fMetric1,fMetric2) RESULT(Product)
    IMPLICIT NONE
    REAL*8  :: fMetric1(3), fMetric2(3), PRODUCT(2,2)
    PRODUCT(1,1) = fMetric1(1)*fMetric2(1) + fMetric1(2)*fMetric2(2)
    PRODUCT(1,2) = fMetric1(1)*fMetric2(2) + fMetric1(2)*fMetric2(3)
    PRODUCT(2,1) = fMetric1(2)*fMetric2(1) + fMetric1(3)*fMetric2(2)
    PRODUCT(2,2) = fMetric1(2)*fMetric2(2) + fMetric1(3)*fMetric2(3)
  END FUNCTION Product_Metric2D

  !>
  !!    Calculate the reverse matrix of a metric.
  !<
  FUNCTION Inverte_Metric2D(fMetric) RESULT(rev)
    IMPLICIT NONE
    REAL*8 :: fMetric(3), rev(3), det
    det = fMetric(1) * fMetric(3) - fMetric(2) * fMetric(2)
    rev(1) =  fMetric(3)/det
    rev(2) = -fMetric(2)/det
    rev(3) =  fMetric(1)/det
    RETURN
  END FUNCTION Inverte_Metric2D

  !>
  !!    Calculate the reverse matrix of a Matrix.
  !<
  SUBROUTINE Eigen_Metric2D(fMetric, value1, value2, vector1, vector2)
    IMPLICIT NONE
    REAL*8 :: fMetric(3), value1, value2, vector1(2), vector2(2)
    REAL*8 :: fMatrix(2,2)
    fMatrix(1,1) = fMetric(1)
    fMatrix(1,2) = fMetric(2)
    fMatrix(2,1) = fMetric(2)
    fMatrix(2,2) = fMetric(3)
    CALL Eigen_Matrix2D(fMatrix, value1, value2, vector1, vector2)
    RETURN
  END SUBROUTINE Eigen_Metric2D

  !>
  !!    Interpolate between two metrics with factor t (0<=t<=1)
  !!                (Return fMetric1 if t=0; Return fMetric2 if t=1)
  !!                by using simultaneous matrix reduction. \n
  !!    Ref: H. Borouchaki, Finite Elements in Analysis and Design
  !!                         25 (1997) 61-83
  !<
  FUNCTION Interpolate_Metric2D(fMetric1,fMetric2,t) RESULT(fMetric)
    IMPLICIT NONE
    REAL*8 :: fMetric1(3), fMetric2(3), t, fMetric(3)
    REAL*8 :: f(3), a(2,2), v1, v2, u1, u2, vt(2,2), p0(2)

    f = Inverte_Metric2D(fMetric1)
    a = Product_Metric2D(f,fMetric2)

    CALL eigen_Matrix2D(a,v1,v2,vt(:,1),vt(:,2))
    a  = Inverte_Matrix2D(vt)

    p0 = 0.d0
    v1 = 1.d0 / Metric2D_Distance(p0,vt(:,1),fMetric1)
    v2 = 1.d0 / Metric2D_Distance(p0,vt(:,2),fMetric1)
    u1 = 1.d0 / Metric2D_Distance(p0,vt(:,1),fMetric2)
    u2 = 1.d0 / Metric2D_Distance(p0,vt(:,2),fMetric2)
    v1 = v1 + (u1-v1)*t
    v2 = v2 + (u2-v2)*t
    u1 = 1.d0 / (v1*v1)
    u2 = 1.d0 / (v2*v2)

    fMetric(1) = a(1,1)*a(1,1)*u1 + a(2,1)*a(2,1)*u2
    fMetric(2) = a(1,1)*a(1,2)*u1 + a(2,1)*a(2,2)*u2
    fMetric(3) = a(1,2)*a(1,2)*u1 + a(2,2)*a(2,2)*u2

    RETURN
  END FUNCTION Interpolate_Metric2D

  !>
  !!    Get the weighted mean of n Mappings.
  !!    make sure that w(1:n)>0 and sum(w(1:n))=1.
  !!    @param[in] n  the number of mappings.
  !!    @param[in] fMes  the mappings.
  !!    @param[in] w      the weights.
  !<
  FUNCTION Metric2D_Mean_Weight(n,fMes,w) RESULT(fMetric)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: n
    REAL*8,  INTENT(IN)  :: fMes(3,*),w(*)
    INTEGER :: i, j
    REAL*8  :: fMetric(3), t, ws
    REAL*8, PARAMETER :: TINY = 1.D-12

       DO i=1,n
          IF(w(i)<TINY)CYCLE
          j = i
          fMetric(:) = fMes(:,i)
          ws = w(i)
          EXIT
       ENDDO

       DO i=j+1,n
          IF(w(i)<TINY)CYCLE
          ws = ws+w(i)
          t  = w(i)/ws
          fMetric  = Interpolate_Metric2D(fMetric, fMes(:,i), t)
       ENDDO

    RETURN
  END FUNCTION Metric2D_Mean_Weight

  !>
  !!    Intersect between two metrics. \n
  !!    Ref: H. Borouchaki, Finite Elements in Analysis and Design
  !!                         25 (1997) 61-83
  !<
  FUNCTION Intersect_Metric2D(fMetric1,fMetric2) RESULT(fMetric)
    IMPLICIT NONE
    REAL*8 :: fMetric1(3), fMetric2(3), fMetric(3)
    REAL*8 :: f(3), a(2,2), v1, v2, u1, u2, vt(2,2), dx, dy

    f = Inverte_Metric2D(fMetric1)
    a = Product_Metric2D(f,fMetric2)

    CALL eigen_Matrix2D(a,v1,v2,vt(:,1),vt(:,2))
    a  = Inverte_Matrix2D(vt)

    dx = vt(1,1)
    dy = vt(2,1)
    v1 = fMetric1(1) *dx*dx + 2.D0* fMetric1(2) *dx*dy + fMetric1(3) *dy*dy
    u1 = fMetric2(1) *dx*dx + 2.D0* fMetric2(2) *dx*dy + fMetric2(3) *dy*dy
    dx = vt(1,2)
    dy = vt(2,2)
    v2 = fMetric1(1) *dx*dx + 2.D0* fMetric1(2) *dx*dy + fMetric1(3) *dy*dy
    u2 = fMetric2(1) *dx*dx + 2.D0* fMetric2(2) *dx*dy + fMetric2(3) *dy*dy

    u1 = MAX(v1,u1)
    u2 = MAX(v2,u2)

    fMetric(1) = a(1,1)*a(1,1)*u1 + a(2,1)*a(2,1)*u2
    fMetric(2) = a(1,1)*a(1,2)*u1 + a(2,1)*a(2,2)*u2
    fMetric(3) = a(1,2)*a(1,2)*u1 + a(2,2)*a(2,2)*u2

    RETURN
  END FUNCTION Intersect_Metric2D

  !>
  !!    Calculate the triangle quality index.
  !<
  FUNCTION TriQuality_Metric2D(p1,p2,p3,fMetric) RESULT(dist)
    IMPLICIT NONE
    REAL*8 :: fMetric(3), p1(2),p2(2),p3(2)
    REAL*8 :: vmod, dist
    vmod = (2.d0*SQRT(3.d0))*ABS(Cross_Product_Metric2D(p3,p2,p1,fMetric))
    dist = (Metric2D_Distance(p1,p2,fMetric))**2       &
          +(Metric2D_Distance(p2,p3,fMetric))**2       &
          +(Metric2D_Distance(p3,p1,fMetric))**2
    dist = vmod / dist
    RETURN
  END FUNCTION TriQuality_Metric2D


END MODULE Metric2D


!>
!!  Geometry for lines in 2D space
!!
!<
MODULE Line2DGeom

  USE Geometry2D

  !>   define a 2D line by a(1)*x + a(2)*y + d = 0   \n
  !!   and a(:) has been normalised here.
  !<
  TYPE :: Line2D
     REAL*8 :: a(2), d
  END TYPE Line2D

CONTAINS


  !>
  !!    Build a line by 2 points P1 & P2.
  !<
  FUNCTION Line2D_Build(P1,P2) RESULT(aLine)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P1(2), P2(2)
    TYPE(Line2D) :: aLine
    REAL*8       :: al
    aLine%a(1) = -P2(2) + P1(2)
    aLine%a(2) =  P2(1) - P1(1)
    al = dsqrt( aLine%a(1) * aLine%a(1) + aLine%a(2) * aLine%a(2) )
    aLine%a = aLine%a / al
    aLine%d = ( P1(1)*P2(2) - P2(1)*P1(2) ) /al
  END FUNCTION Line2D_Build

  !>
  !!    Calculate the project position of Point P0 on a line.
  !<
  FUNCTION Line2D_ProjectOnLine(P0,aLine) RESULT(PP)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P0(2)
    TYPE(Line2D) :: aLine
    REAL*8 :: PP(2)
    REAL*8 :: xl
    xl = aLine%a(1)*P0(1) + aLine%a(2)*P0(2) + aLine%d
    PP = P0 - aLine%a * xl
  END FUNCTION Line2D_ProjectOnLine

  !>
  !!    Calculate the project position of Point P0 on a line
  !!              which is determined by 2 points P1,P2.
  !<
  FUNCTION Line2D_ProjectOnLine3(P0,P1,P2) RESULT(PP)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P0(2), P1(2), P2(2)
    REAL*8 :: PP(2)
    REAL*8 :: a(2), b(2), xl
    a(1) =  P2(2) - P1(2)
    a(2) = -P2(1) + P1(1)
    b = P0 - P2
    xl = (a(1)*b(1)+a(2)*b(2)) / (a(1)*a(1)+a(2)*a(2))
    PP = P0 - a*xl
  END FUNCTION Line2D_ProjectOnLine3

  !>
  !!    Calculate the distance between a point and a line.
  !<
  FUNCTION Line2D_DistancePointToLine(P0,aLine) RESULT(Dist)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P0(2)
    TYPE(Line2D) :: aLine
    REAL*8 :: Dist
    Dist = aLine%a(1)*P0(1) + aLine%a(2)*P0(2) + aLine%d
    Dist = DABS(Dist)
  END FUNCTION Line2D_DistancePointToLine

  !>
  !!    Calculate the distance (with sign) between a point and a line.
  !!    If the line is built by Line2D_Build(P1,P2), then
  !!       the distance return positive if point P1-P2-P0 is anti-clockwise.
  !<
  FUNCTION Line2D_SignedDistancePointToLine(P0,aLine) RESULT(Dist)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: P0(2)
    TYPE(Line2D) :: aLine
    REAL*8 :: Dist
    Dist = aLine%a(1)*P0(1) + aLine%a(2)*P0(2) + aLine%d
  END FUNCTION Line2D_SignedDistancePointToLine

  !>
  !!   Calculate the intersecting line of 2 spheres.
  !!   @param[in] SP1,SP2  two spheres (centre and radius).
  !<
  FUNCTION Line2D_Sphere_Sphere_Cross(SP1,SP2) RESULT(aLine)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: SP1(3), SP2(3)
    TYPE(Line2D)  :: aLine
    REAL*8 :: al

    CALL Error_STOP ( 'need further check here---Line2D_Sphere_Sphere_Cross')
    aLine%a(:) = 2.d0*(SP1(1:2)-SP2(1:2))
    aLine%d    = - (SP1(1)**2 + SP1(2)**2 - SP1(3)**2)   &
                 + (SP2(1)**2 + SP2(2)**2 - SP2(3)**2 )
    al = dsqrt( aLine%a(1) * aLine%a(1) + aLine%a(2) * aLine%a(2) )
    aLine%a = aLine%a / al
    aLine%d = aLine%d / al
  END FUNCTION Line2D_Sphere_Sphere_Cross


END MODULE Line2DGeom





