!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Hybrid mesh storage, including the defination of type and the basic functions.
!<
MODULE HybridMeshStorage
  USE array_allocator
  USE Number_CHAR_Transfer
  USE CellConnectivity
  USE SurfaceMeshStorage
  USE TetMeshStorage
  USE Geometry3D
  USE HighOrderCell
  IMPLICIT NONE

  TYPE :: HybridMeshStorageType
     INTEGER :: NB_Tet    = 0                        !<  the number of tetrahedra.
     INTEGER :: NB_Pyr    = 0                        !<  the number of pyramids.
     INTEGER :: NB_Prm    = 0                        !<  the number of prisms.
     INTEGER :: NB_Hex    = 0                        !<  the number of hexahedra.
     INTEGER :: NB_Point  = 0                        !<  the number of points.
     INTEGER, DIMENSION(:,:),  POINTER :: IP_Tet => null()     !<  (4,*) connectivities of each tetrahedron.
     INTEGER, DIMENSION(:,:),  POINTER :: IP_Pyr => null()     !<  (5,*) connectivities of each pyramid.
     INTEGER, DIMENSION(:,:),  POINTER :: IP_Prm => null()     !<  (6,*) connectivities of each prism.
     INTEGER, DIMENSION(:,:),  POINTER :: IP_Hex => null()     !<  (8,*) connectivities of each hexahedron.
     REAL*8,  DIMENSION(:,:),  POINTER :: Posit => null()      !<  (3,*) coordinates of each point.

     !>  (4,*) adjacent tetrahedra of each tetrahedron.
     !!        Basically, tetrahedron Next_Tet(i,it) is opposite to node IP_Tet(i,it) .
     !!        If Next_Tet(i,it)=-1 then the i-th triangle of element it is on the boundary.
     INTEGER, DIMENSION(:,:),  POINTER :: Next_Tet => null()

     !>  (6,*) adjacent hexahedra of each hexahedron.
     !!        Basically, hexahedron Next_Hex(i,it) share nodes IP_Hex(iQuadFD_Hex(:,i),it) .
     INTEGER, DIMENSION(:,:),  POINTER :: Next_Hex => null()

     INTEGER :: NB_Value  = 0                        !<  the number of variable types.
     REAL*8,  DIMENSION(:,:),  POINTER :: V_Point => null()    !<  (*,*) the variables at each point.
     REAL*8,  DIMENSION(:,:),  POINTER :: V_Cell => null()     !<  (*,*) the variables at each element.

     !========  as high order mesh

     INTEGER :: GridOrder = 1           !<  =1 linear mesh; =3 cubic order mesh.
     INTEGER :: NB_Psp    = 0           !< the number of supporting nodes.
     
     TYPE(HighOrderCellType)  :: Cell   
       
     INTEGER :: NB_DBC = 0                       !< the number of Dirichlet Boundary Condition points
     INTEGER, DIMENSION(:),    POINTER :: IP_DBC => null()      !< the node ID of each DBC point.
     REAL*8,  DIMENSION(:,:),  POINTER :: Pd_DBC => null()      !< (3,*) the target movement of each DBC point.


  END TYPE HybridMeshStorageType


  TYPE :: HybridMeshElfenFormat
     CHARACTER (LEN=17) :: Assignment_points = "Assignment_points"
     CHARACTER (LEN=19) :: Assignment_surfaces = "Assignment_surfaces"
     CHARACTER (LEN=18) :: Assignment_volumes = "Assignment_volumes"
     CHARACTER (LEN=11) :: Coordinates = "Coordinates"
     CHARACTER (LEN=12) :: Element_data = "Element_data"
     CHARACTER (LEN=18) :: Element_group_data = "Element_group_data"
     CHARACTER (LEN=15) :: Element_numbers = "Element_numbers"
     CHARACTER (LEN=15) :: Element_options = "Element_options"
     CHARACTER (LEN=16) :: Element_topology = "Element_topology"
     CHARACTER (LEN=19) :: Element_type_number = "Element_type_number"
     CHARACTER (LEN=10) :: Group_name = "Group_name"
     CHARACTER (LEN=15) :: Material_number = "Material_number"
     CHARACTER (LEN= 9) :: Node_data = "Node_data"
     CHARACTER (LEN=12) :: Node_numbers = "Node_numbers"
     CHARACTER (LEN=29) :: Operation_assignment_geometry = "Operation_assignment_geometry"
     CHARACTER (LEN=23) :: Surface_property_number = "Surface_property_number"

     CHARACTER (LEN= 6) :: IGROUP = "IGROUP"
     CHARACTER (LEN= 5) :: NDIMN = "NDIMN"
     CHARACTER (LEN= 5) :: NELGP = "NELGP"
     CHARACTER (LEN= 6) :: NELOPS = "NELOPS"
     CHARACTER (LEN= 5) :: NNODE = "NNODE"
     CHARACTER (LEN= 5) :: NPOIN = "NPOIN"
  END TYPE HybridMeshElfenFormat

  INTEGER, PARAMETER :: HybridMesh_allc_increase = 100000

CONTAINS

  !>
  !!   Allocate (Reallocate) memory for the HybMesh%IP_Hex.
  !<
  SUBROUTINE allc_HybridMesh_Hex(HybMesh, NB_Hex)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    INTEGER, INTENT(IN) :: NB_Hex
    INTEGER :: msize
    IF(NB_Hex<=KeyLength(HybMesh%IP_Hex)) RETURN
    msize = NB_Hex + HybridMesh_allc_increase
    CALL allc_2Dpointer(HybMesh%IP_Hex,   8, msize)
    RETURN
  END SUBROUTINE allc_HybridMesh_Hex

  !>
  !!   Allocate (Reallocate) memory for the HybMesh%IP_Pyr.
  !<
  SUBROUTINE allc_HybridMesh_Pyr(HybMesh, NB_Pyr)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    INTEGER, INTENT(IN) :: NB_Pyr
    INTEGER :: msize
    IF(NB_Pyr<=KeyLength(HybMesh%IP_Pyr)) RETURN
    msize = NB_Pyr + HybridMesh_allc_increase
    CALL allc_2Dpointer(HybMesh%IP_Pyr,   5, msize)
    RETURN
  END SUBROUTINE allc_HybridMesh_Pyr

  !>
  !!   Allocate (Reallocate) memory for the HybMesh%IP_Tet.
  !<
  SUBROUTINE allc_HybridMesh_Tet(HybMesh, NB_Tet)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    INTEGER, INTENT(IN) :: NB_Tet
    INTEGER :: msize
    IF(NB_Tet<=KeyLength(HybMesh%IP_Tet)) RETURN
    msize = NB_Tet + HybridMesh_allc_increase
    CALL allc_2Dpointer(HybMesh%IP_Tet,   4, msize)
    RETURN
  END SUBROUTINE allc_HybridMesh_Tet

  !>
  !!   Allocate (Reallocate) memory for the HybMesh%Posit.
  !<
  SUBROUTINE allc_HybridMesh_Posit(HybMesh, NB_Point)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    INTEGER, INTENT(IN) :: NB_Point
    INTEGER :: msize
    IF(NB_Point<=KeyLength(HybMesh%Posit)) RETURN
    msize = NB_Point + HybridMesh_allc_increase
    CALL allc_2Dpointer(HybMesh%Posit,   3, msize)
    RETURN
  END SUBROUTINE allc_HybridMesh_Posit

  !>
  !!  Output the information of the hybrid mesh.
  !!  @param[in] HybMesh  the hybrid mesh.
  !!  @param[in] IO       the IO channel. 
  !!                      If =6 or not present, output on screen. 
  !<
  SUBROUTINE HybridMeshStorage_Info(HybMesh, IO)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(IN) :: HybMesh
    INTEGER, INTENT(IN), OPTIONAL :: IO
    INTEGER :: ic
    ic = 6
    IF(PRESENT(IO)) ic = IO    
    WRITE(ic,*) ' NB_Tet,  NB_Pyr, NB_Prm, NB_Hex='
    WRITE(ic,*) HybMesh%NB_Tet, HybMesh%NB_Pyr,HybMesh%NB_Prm,HybMesh%NB_Hex
    WRITE(ic,*) ' NB_Point=', HybMesh%NB_Point
    RETURN
  END SUBROUTINE HybridMeshStorage_Info

  !>
  !!   Delete the mesh strucutre and release memory.
  !<
  SUBROUTINE HybridMeshStorage_Clear(HybMesh)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    HybMesh%NB_Tet    = 0
    HybMesh%NB_Pyr    = 0
    HybMesh%NB_Prm    = 0
    HybMesh%NB_Hex    = 0
    HybMesh%NB_Point  = 0
    HybMesh%NB_Value  = 0 
    HybMesh%NB_Psp    = 0
    HybMesh%NB_DBC    = 0 
    HybMesh%GridOrder = 1
    IF(ASSOCIATED(HybMesh%IP_Tet))    DEALLOCATE(HybMesh%IP_Tet)
    IF(ASSOCIATED(HybMesh%IP_Pyr))    DEALLOCATE(HybMesh%IP_Pyr)
    IF(ASSOCIATED(HybMesh%IP_Prm))    DEALLOCATE(HybMesh%IP_Prm)
    IF(ASSOCIATED(HybMesh%IP_Hex))    DEALLOCATE(HybMesh%IP_Hex)
    IF(ASSOCIATED(HybMesh%Posit))     DEALLOCATE(HybMesh%Posit)
    IF(ASSOCIATED(HybMesh%Next_Tet))  DEALLOCATE(HybMesh%Next_Tet)
    IF(ASSOCIATED(HybMesh%Next_Hex))  DEALLOCATE(HybMesh%Next_Hex)
    IF(ASSOCIATED(HybMesh%V_Point))   DEALLOCATE(HybMesh%V_Point)
    IF(ASSOCIATED(HybMesh%V_Cell))    DEALLOCATE(HybMesh%V_Cell)
    IF(ASSOCIATED(HybMesh%IP_DBC))    DEALLOCATE(HybMesh%IP_DBC)
    IF(ASSOCIATED(HybMesh%Pd_DBC))    DEALLOCATE(HybMesh%Pd_DBC)
    CALL HighOrderCell_Clear(HybMesh%Cell)
    RETURN
  END SUBROUTINE HybridMeshStorage_Clear

  !>
  !!  Input a Hybrid mesh by reading a *.plt or *.mesh3 file. 
  !!  @param[in]  JobName  the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  fmat = -1 : unformatted .plt                            \n
  !!                   = -11: unformatted .plt, high order mesh           \n
  !!                   = -2 : unformatted .mesh3                          \n
  !!                   = -3 : unformatted .plt, tetrahedron-only mesh.    \n
  !!                   = -13: unformatted .plt, high order tetrahedron-only mesh           \n
  !!                   = -4 : unformatted .mesh3, tetrahedron-only mesh.  \n
  !!                   = -6 : unformatted .ugrid                          \n
  !!                   = -8 : unformatted .cogrd    (NASA VGRID)          \n
  !!                   = -15: =-1,-11,-3,-13 whichever apply.
  !!  @param[out] HybMesh  the hybrid mesh.
  !!  @param[out] Surf     a surface mesh represent the boundary.
  !<
  SUBROUTINE HybridMeshStorage_Input(JobName,JobNameLength,fmat,HybMesh,Surf)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(INOUT), OPTIONAL :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength, fmat
    INTEGER :: i,np,nel,netot, nas1, nas2, nbtot, nb, ND, fma
    INTEGER, DIMENSION(:),   POINTER :: IP, Markp
    REAL*8,  DIMENSION(:),   POINTER :: Ps    
    CHARACTER*(20) :: ffmat
    INTEGER :: npoic, npois, npoie, nelec, neles, nelee, itemp
    REAL*8  :: vtemp

    fma = fmat
    IF(fmat==-15)THEN
       CALL DetectFortranBinary(JobName(1:JobNameLength)//'.plt', JobNameLength+4, i)
       IF(i==3)THEN
          fma = -3
       ELSE IF(i==4)THEN
          fma = -13
       ELSE IF(i==9)THEN
          fma = -1
       ELSE IF(i==10)THEN
          fma = -11
       ELSE 
          WRITE(*,*)' ---- Error: do know the format of ',JobName(1:JobNameLength)//'.plt'
          WRITE(*,*)'             with number of interger on the first line:', i
          CALL Error_Stop('HybridMeshStorage_Input::')
       ENDIF
    ENDIF
    
    IF(fma==-1 .OR. fma==-3 .OR. fma==-11 .OR. fma==-13)THEN
       ffmat(1:5) = '.plt '
    ELSE IF(fma==-2 .OR. fma==-4)THEN
       ffmat(1:7) = '.mesh3 '
    ELSE IF(fma==-6)THEN
       ffmat(1:7) = '.ugrid '
    ELSE IF(fma==-8)THEN
       ffmat(1:7) = '.cogrd '
    ELSE
       CALL Error_STOP ( 'HybridMeshStorage_Input: not for such format')
    ENDIF

    WRITE(*,*) '--- Read file ',JobName(1:JobNameLength)//TRIM(ffmat)

    IF(fma/=-6)THEN
       OPEN(11,file= JobName(1:JobNameLength)//TRIM(ffmat),form='UNFORMATTED',err=111)
    ENDIF

    HybMesh%GridOrder = 1
    HybMesh%NB_Hex    = 0
    HybMesh%NB_Prm    = 0
    HybMesh%NB_Pyr    = 0
    HybMesh%NB_Psp    = 0
    IF(fma==-1 .OR. fma==-2)THEN
       READ(11,err=111)netot,HybMesh%NB_Point,nbtot,   &
            HybMesh%NB_Hex,HybMesh%NB_Prm,HybMesh%NB_Pyr,HybMesh%NB_Tet,nas1,nas2
    ELSE IF(fma==-3 .OR. fma==-4)THEN
       READ(11,err=111)netot,HybMesh%NB_Point,nbtot
       HybMesh%NB_Tet = netot
       nas1           = 0
       nas2           = nbtot
    ELSE IF(fma==-6)THEN
       ALLOCATE(ip(7), Ps(1))
       i = 1
       CALL binaryugridreador(JobName,JobNameLength, Ps, ip, i)
       HybMesh%NB_Point = ip(1)
       nas2             = ip(2)
       nas1             = ip(3)       
       HybMesh%NB_Tet   = ip(4)
       HybMesh%NB_Pyr   = ip(5)
       HybMesh%NB_Prm   = ip(6)
       HybMesh%NB_Hex   = ip(7)
       DEALLOCATE(ip, Ps)
    ELSE IF(fma==-8)THEN
       READ(11,err=111) itemp, HybMesh%NB_Tet, HybMesh%NB_Point
       REWIND(11)
    ELSE IF(fma==-11)THEN
       READ(11,err=111)netot,HybMesh%NB_Point,nbtot,   &
            HybMesh%NB_Hex,HybMesh%NB_Prm,HybMesh%NB_Pyr,HybMesh%NB_Tet,nas1,nas2,HybMesh%GridOrder
    ELSE IF(fma==-13)THEN
       READ(11,err=111)netot,HybMesh%NB_Point,nbtot,HybMesh%GridOrder       
       HybMesh%NB_Tet = netot
       nas1           = 0
       nas2           = nbtot
    ENDIF
    IF(HybMesh%GridOrder==0) HybMesh%GridOrder = 1
    ND = Npoint_Tet(HybMesh%GridOrder)

    WRITE(*,*)'HybMesh%(NB_Point,  NB_Hex,  NB_Prm,  NB_Pyr,  NB_Tet, GridOrder)='
    WRITE(*,*) HybMesh%NB_Point,HybMesh%NB_Hex,HybMesh%NB_Prm,HybMesh%NB_Pyr,HybMesh%NB_Tet,HybMesh%GridOrder
    
    IF(PRESENT(Surf))THEN
       Surf%NB_Quad = nas1 
       Surf%NB_Tri  = nas2
       WRITE(*,*)'Surf%(NB_Quad, NB_Tri)=',Surf%NB_Quad, Surf%NB_Tri
       ALLOCATE(Surf%IP_Quad(5, Surf%NB_Quad)) 
       ALLOCATE(Surf%IP_Tri (5, Surf%NB_Tri)) 
    ENDIF

    ALLOCATE(HybMesh%IP_Hex(8,  HybMesh%NB_Hex)) 
    ALLOCATE(HybMesh%IP_Prm(6,  HybMesh%NB_Prm)) 
    ALLOCATE(HybMesh%IP_Pyr(5,  HybMesh%NB_Pyr)) 
    ALLOCATE(HybMesh%IP_Tet(ND, HybMesh%NB_Tet))
    ALLOCATE(HybMesh%Posit (3,  HybMesh%NB_Point)) 

    IF(fma==-1 .OR. fma==-11)THEN

       READ(11,err=111)((HybMesh%IP_Hex(i,nel),nel=1,HybMesh%NB_Hex),i=1,8)
       READ(11,err=111)((HybMesh%IP_Prm(i,nel),nel=1,HybMesh%NB_Prm),i=1,6)
       READ(11,err=111)((HybMesh%IP_Pyr(i,nel),nel=1,HybMesh%NB_Pyr),i=1,5)
       READ(11,err=111)((HybMesh%IP_Tet(i,nel),nel=1,HybMesh%NB_Tet),i=1,ND)
       READ(11,err=111)((HybMesh%Posit(i,np),  np =1,HybMesh%NB_Point),i=1,3)
       IF(PRESENT(Surf))THEN
          READ(11,err=111)((Surf%IP_Quad(i,nb),nb=1,Surf%NB_Quad),i=1,5)
          READ(11,err=111)((Surf%IP_Tri(i,nb), nb=1,Surf%NB_Tri), i=1,5)
       ENDIF

    ELSE IF(fma==-3 .OR. fma==-13)THEN

       READ(11,err=111)((HybMesh%IP_Tet(i,nel),nel=1,HybMesh%NB_Tet),i=1,ND)
       READ(11,err=111)((HybMesh%Posit(i,np),  np =1,HybMesh%NB_Point),i=1,3)
       IF(PRESENT(Surf))THEN
          READ(11,err=111)((Surf%IP_Tri(i,nb), nb=1,Surf%NB_Tri), i=1,5)
       ENDIF

    ELSE IF(fma==-2 .OR. fma==-4)THEN

       DO nel=1,HybMesh%NB_Hex
          READ(11,err=111) HybMesh%IP_Hex(1:8,nel)
       ENDDO
       DO nel=1,HybMesh%NB_Pyr
          READ(11,err=111) HybMesh%IP_Pyr(1:5,nel)
       ENDDO
       DO nel=1,HybMesh%NB_Tet
          READ(11,err=111) HybMesh%IP_Tet(1:4,nel)
       ENDDO
       DO np=1,HybMesh%NB_Point
          READ(11,err=111) HybMesh%Posit(1:3,np)
       ENDDO
       IF(PRESENT(Surf))THEN
          IF(fma==-2)THEN
             READ(11,err=111)((Surf%IP_Quad(i,nb),nb=1,Surf%NB_Quad),i=1,5)
          ENDIF
          DO nb=1,Surf%NB_Tri
             READ(11,err=111) Surf%IP_Tri(1:5,nb)             
          ENDDO
       ENDIF

    ELSE IF(fma==-6)THEN

       np = 4*nas2 + 5*nas1 + 4*HybMesh%NB_Tet + 5*HybMesh%NB_Pyr + 6*HybMesh%NB_Prm + 8*HybMesh%NB_Hex
       ALLOCATE(ip(np))
       ALLOCATE(Ps(3*HybMesh%NB_Point))
       i = 2
       CALL binaryugridreador(JobName,JobNameLength, Ps, IP, i)
       
       i = 1
       DO np = 1, HybMesh%NB_Point
          HybMesh%Posit(1:3,np) = Ps(i:i+2)
          i = i + 3
       ENDDO
       
       IF(PRESENT(Surf))THEN
          i = 1
          DO nel = 1, Surf%NB_Tri
             Surf%IP_Tri(1:3,nel) = IP(i:i+2)
             i = i + 3
          ENDDO
          DO nel = 1, Surf%NB_Quad
             Surf%IP_Quad(1:4,nel) = IP(i:i+3)
             i = i + 4
          ENDDO
          Surf%IP_Tri(5,1:Surf%NB_Tri) = IP(i:i+Surf%NB_Tri-1)
          i = i + Surf%NB_Tri
          Surf%IP_Quad(5,1:Surf%NB_Quad) = IP(i:i+Surf%NB_Quad-1)
          i = i + Surf%NB_Quad
       ELSE
          i = 1 + 4*nas2 + 5*nas1 
       ENDIF
       
       DO nel = 1, HybMesh%NB_Tet
          HybMesh%IP_Tet(1:4,nel) = IP(i:i+3)
          i = i + 4
       ENDDO
       DO nel = 1, HybMesh%NB_Pyr
          HybMesh%IP_Pyr(1:5,nel) = IP(i:i+4)
          i = i + 5
       ENDDO
       DO nel = 1, HybMesh%NB_Prm
          HybMesh%IP_Prm(1:6,nel) = IP(i:i+5)
          i = i + 6
       ENDDO
       DO nel = 1, HybMesh%NB_Hex
          HybMesh%IP_Hex(1:8,nel) = IP(i:i+7)
          i = i + 8
       ENDDO
      
       DEALLOCATE(Ps, IP)

    ELSE IF(fma==-8)THEN
    
      npoic = 0    !--- number of nodes
      npois = 1    !--- start node
      npoie = 0    !--- end node
      nelec = 0    !--- number of cells
      neles = 1    !--- start cell
      nelee = 0    !--- end cell
      READ(11,err=111)itemp,HybMesh%NB_Tet,HybMesh%NB_Point,itemp,itemp,itemp,vtemp,   &
             ((HybMesh%IP_Tet(i,nel),nel = neles,nelee),i = 1,4)
      READ(11,err=111)((HybMesh%Posit(i,np),np = npois,npoie),i = 1,3)
      nelec = nelee
      npoic = npoie

      DO
         neles = neles+nelec
         read(11,end=400)nelec
         if(nelec == 0) EXIT
         nelee = neles+nelec-1
         READ(11,err=111)((HybMesh%IP_Tet(i,nel),nel = neles,nelee),i = 1,4)
         npois = npois+npoic
         READ(11,err=111)npoic
         npoie = npois+npoic-1
         READ(11,err=111)((HybMesh%Posit(i,np),np = npois,npoie),i = 1,3)
      ENDDO      
 400  continue
 
      IF(HybMesh%NB_Point /= npoie) STOP 'HybMesh%NB_Point /= npoie'
      IF(HybMesh%NB_Tet /= nelee) STOP 'HybMesh%NB_Tet /= nelee'
       
    ENDIF

    IF(PRESENT(Surf))THEN
       Surf%NB_Surf = 0
       DO nb=1,Surf%NB_Tri
          Surf%NB_Surf = MAX(Surf%NB_Surf,Surf%IP_Tri(5,nb))
       ENDDO
    ENDIF

    IF(fma/=-6)THEN
       CLOSE(11)
    ENDIF
    
    HybMesh%Cell = HighOrderCell_getHighCell(HybMesh%GridOrder,3,0)
    
    IF(HybMesh%GridOrder>1)THEN
       ALLOCATE(Markp(0:HybMesh%NB_Point))
       Markp(:) = 0
       DO nel = 1, HybMesh%NB_Tet
          Markp(HybMesh%IP_Tet(5:ND,nel)) = 1
       ENDDO  
       HybMesh%NB_Psp = 0
       DO I = 1, HybMesh%NB_Point
          IF(Markp(I)==1) HybMesh%NB_Psp = HybMesh%NB_Psp + 1
       ENDDO
       DEALLOCATE(Markp)
       WRITE(*,*)'HybMesh%NB_Psp=',HybMesh%NB_Psp
    ENDIF


    RETURN

111 WRITE(*,*) 'Error--- HybridMeshStorage_Input: ',JobName(1:JobNameLength)//TRIM(ffmat)
    CALL Error_STOP (' ')
  END SUBROUTINE HybridMeshStorage_Input

  !>
  !!  Input a Hybrid mesh by reading a *.mesh file. 
  !!  @param[in]  JobName  the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  fmat = 1 : formatted .mesh       \n
  !!                   = 2 : formatted .stg        \n
  !!                   = 3 : tecplot format .dat   \n
  !!                   = 4 : cvt format .dat   \n
  !!                   else: not for now.
  !!  @param[out] HybMesh  the hybrid mesh.
  !!  @param[out] Surf     a surface mesh represent the boundary.
  !<
  SUBROUTINE HybridMeshStorage_Input_Formatted(JobName,JobNameLength,fmat,HybMesh,Surf)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(INOUT), OPTIONAL :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength, fmat
    INTEGER :: np,nel,netot,nas1, nas2, nbtot, nb, idx, Isucc1, Isucc2, i, ip4(4)
    REAL*8  :: p1(3), p2(3), p3(3), p4(3), v
    CHARACTER*(20) :: ffmat
    CHARACTER(LEN=256) :: C, C1, C2

    IF(fmat==1)THEN
       ffmat(1:6) = '.mesh '
    ELSE IF(fmat==2)THEN
       ffmat(1:5) = '.stg '
    ELSE IF(fmat==3 .OR. fmat==4)THEN
       ffmat(1:5) = '.dat '
    ELSE
       CALL Error_STOP ( 'HybridMeshStorage_Input_Formatted: not for such format')
    ENDIF

    WRITE(*,*) '--- Read file ',JobName(1:JobNameLength)//TRIM(ffmat)

    OPEN(11,file= JobName(1:JobNameLength)//TRIM(ffmat), &
         status='UNKNOWN',form='FORMATTED')
    IF(fmat==1)THEN
       READ(11,*)netot,HybMesh%NB_Point,nbtot,   &
            HybMesh%NB_Hex,HybMesh%NB_Prm,HybMesh%NB_Pyr,HybMesh%NB_Tet,nas1,nas2
    ELSE IF(fmat==2)THEN
       READ(11,*)HybMesh%NB_Tet,HybMesh%NB_Point
       HybMesh%NB_Hex = 0
       HybMesh%NB_Prm = 0
       HybMesh%NB_Pyr = 0
       nas1 = 0
       nas2 = 0
    ELSE IF(fmat==3)THEN
       Isucc1 = 0
       Isucc2 = 0
       DO WHILE (Isucc1==0 .OR. Isucc2==0)
          READ(11,'(a)')C
          C1 = C
          C2 = C
          IF(Isucc1==0)THEN
             IF( CHAR_CutFront(C1, 'N=') ) HybMesh%NB_Point = CHAR_to_INT(C1,Isucc1)
          ENDIF
          IF(Isucc2==0)THEN
             IF( CHAR_CutFront(C2, 'E=') ) HybMesh%NB_Tet   = CHAR_to_INT(C2,Isucc2)
          ENDIF
       ENDDO
       HybMesh%NB_Hex = 0
       HybMesh%NB_Prm = 0
       HybMesh%NB_Pyr = 0
       nas1 = 0
       nas2 = 0
    ELSE IF(fmat==4)THEN
       READ(11,*)
       READ(11,*)
       READ(11,*)
       READ(11,*)HybMesh%NB_Tet,HybMesh%NB_Point
       nas1 = 0
       nas2 = 0
    ENDIF
    WRITE(*,*)'HybMesh%(NB_Point,  NB_Hex,  NB_Prm,  NB_Pyr,  NB_Tet)='
    WRITE(*,*) HybMesh%NB_Point,HybMesh%NB_Hex,HybMesh%NB_Prm,HybMesh%NB_Pyr,HybMesh%NB_Tet
    IF(PRESENT(Surf))THEN
       Surf%NB_Quad = nas1 
       Surf%NB_Tri  = nas2
       WRITE(*,*)'Surf%(NB_Quad, NB_Tri)=',Surf%NB_Quad, Surf%NB_Tri
       ALLOCATE(Surf%IP_Quad(5, Surf%NB_Quad)) 
       ALLOCATE(Surf%IP_Tri (5, Surf%NB_Tri)) 
    ENDIF

    ALLOCATE(HybMesh%IP_Hex(8, HybMesh%NB_Hex)) 
    ALLOCATE(HybMesh%IP_Prm(6, HybMesh%NB_Prm)) 
    ALLOCATE(HybMesh%IP_Pyr(5, HybMesh%NB_Pyr)) 
    ALLOCATE(HybMesh%IP_Tet(4, HybMesh%NB_Tet)) 
    ALLOCATE(HybMesh%Posit (3, HybMesh%NB_Point)) 

    IF(fmat==1)THEN

       DO nel = 1, HybMesh%NB_Hex
          READ(11,*) idx, HybMesh%IP_Hex(1:4,idx)
          READ(11,*)      HybMesh%IP_Hex(5:8,idx)
       ENDDO
       DO nel = 1, HybMesh%NB_Pyr
          READ(11,*) idx, HybMesh%IP_Pyr(1:5,idx)
       ENDDO
       DO nel = 1, HybMesh%NB_Tet
          READ(11,*) idx, HybMesh%IP_Tet(1:4,idx)
       ENDDO

       DO np = 1, HybMesh%NB_Point
          READ(11,*) idx, HybMesh%Posit(1:3,idx)
       ENDDO

       IF(PRESENT(Surf))THEN
          DO nb = 1, Surf%NB_Quad
             READ(11,*) idx, Surf%IP_Quad(1:5,idx)
          ENDDO
          DO nb = 1,Surf%NB_Tri
             READ(11,*) idx, Surf%IP_Tri(1:5,idx)             
          ENDDO
       ENDIF

    ELSE IF(fmat==2)THEN

       DO nel = 1, HybMesh%NB_Tet
          READ(11,*) HybMesh%IP_Tet(1:4,nel)
       ENDDO

       DO np = 1, HybMesh%NB_Point
          READ(11,*) HybMesh%Posit(1:3,np)
       ENDDO

    ELSE IF(fmat==3)THEN

       DO np = 1, HybMesh%NB_Point
          READ(11,*) HybMesh%Posit(1:3,np)
       ENDDO

       DO nel = 1, HybMesh%NB_Tet
          READ(11,*) HybMesh%IP_Tet(1:4,nel)
       ENDDO

    ELSE IF(fmat==4)THEN

       READ(11,*)       
       DO nel = 1, HybMesh%NB_Tet
          READ(11,*) i, HybMesh%IP_Tet(1:4,i)
       ENDDO

       READ(11,*)       
       DO nel = 1, HybMesh%NB_Tet
          READ(11,*) 
       ENDDO

       READ(11,*)
       DO np = 1, HybMesh%NB_Point
          READ(11,*) i, HybMesh%Posit(1:3,i)
       ENDDO

       DO nel = 1, HybMesh%NB_Tet
          ip4(:) = HybMesh%IP_Tet(1:4,nel)
          p1     = HybMesh%Posit(:,ip4(1))
          p2     = HybMesh%Posit(:,ip4(2))
          p3     = HybMesh%Posit(:,ip4(3))
          p4     = HybMesh%Posit(:,ip4(4))
          v = Geo3D_Tet_Volume6 (P1, P2, P3, P4)
          IF(v<=0)THEN
             HybMesh%IP_Tet(1:4,nel) = (/ip4(1), ip4(2), ip4(4), ip4(3)/)
          ENDIF
       ENDDO
    ENDIF

    IF(PRESENT(Surf))THEN
       Surf%NB_Surf = 0
       DO nb = 1, Surf%NB_Tri
          Surf%NB_Surf = MAX(Surf%NB_Surf,Surf%IP_Tri(5,nb))
       ENDDO
    ENDIF

    CLOSE(11)
    RETURN

  END SUBROUTINE HybridMeshStorage_Input_Formatted

  !>
  !!  Output a Hybrid mesh. 
  !!  @param[in]  JobName  the name (prefix) of Output file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  fmat = -1 : unformatted .plt                            \n
  !!                   = -11: unformatted .hplt, high-order mesh          \n
  !!                   = -2 : unformatted .mesh3                          \n
  !!                   = -3 : unformatted .plt, tetrahedron-only mesh.    \n
  !!                   = -13: unformatted .hplt, high-order tetrahedron-only mesh.    \n
  !!                   = -15: -1,-3,-11,-13, whichever fits.
  !!                   = -5 : unformatted .geo  (for enSight)
  !!  @param[in] HybMesh  the hybrid mesh.
  !!  @param[in] Surf     a surface mesh represent the boundary.
  !<
  SUBROUTINE HybridMeshStorage_Output(JobName,JobNameLength,fmat,HybMesh,Surf)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(IN) :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(IN), OPTIONAL :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength, fmat
    INTEGER :: i,np,nel,netot, nbtot, nb, nas1, nas2, ND, fma
    CHARACTER*(20) :: ffmat
    INTEGER :: j, k, ip, is, nbn, nbp, it, ipps(10)
    INTEGER, DIMENSION(:),     ALLOCATABLE :: gloloc
    INTEGER, DIMENSION(:,:),   ALLOCATABLE :: ips
    REAL*4,  DIMENSION(:,:),   ALLOCATABLE :: xyz
    CHARACTER*80 :: string
    CHARACTER*4  :: fileExtension 

    fma = fmat
    IF(fma==-15)THEN
       IF(HybMesh%NB_Hex==0 .and. HybMesh%NB_Pyr==0 .and. HybMesh%NB_Prm==0)THEN
          fma = -3  !This is what we want for outputing tets
       ELSE
          fma = -1
       ENDIF
       IF(HybMesh%GridOrder>1) fma = fma - 10
    ENDIF
    
    IF(fma==-1 .OR. fma==-3 .OR. fma==-11 .OR. fma==-13)THEN
       ffmat(1:5) = '.plt '
    ELSE IF(fma==-2)THEN
       ffmat(1:7) = '.mesh3 '
    ELSE IF(fma==-5)THEN
       ffmat(1:5) = '.geo '
    ELSE
       WRITE(*,*)' Error--- not for such format: fmat=',fma
       CALL Error_STOP ( 'HybridMeshStorage_Output')
    ENDIF

    WRITE(*,*) '--- Write file ',JobName(1:JobNameLength)//TRIM(ffmat)
    WRITE(*,*)'HybMesh%(NB_Point,  NB_Hex,  NB_Prm,  NB_Pyr,  NB_Tet)='
    WRITE(*,*) HybMesh%NB_Point,HybMesh%NB_Hex,HybMesh%NB_Prm,HybMesh%NB_Pyr,HybMesh%NB_Tet

    netot = HybMesh%NB_Tet + HybMesh%NB_Pyr + HybMesh%NB_Prm + HybMesh%NB_Hex
    OPEN(11,file= JobName(1:JobNameLength)//TRIM(ffmat), form='UNFORMATTED',err=111)

    IF(PRESENT(Surf))THEN
       nas1 = Surf%NB_Quad
       nas2 = Surf%NB_Tri       
    ELSE
       nas1 = 0
       nas2 = 0
    ENDIF
    nbtot = nas1 + nas2
    IF(fma==-1 .OR. fma==-2)THEN
       WRITE(11,err=111)netot,HybMesh%NB_Point,nbtot,   &
            HybMesh%NB_Hex,HybMesh%NB_Prm,HybMesh%NB_Pyr,HybMesh%NB_Tet, nas1, nas2, 1
    ELSE IF(fma==-11)THEN
       WRITE(11,err=111)netot,HybMesh%NB_Point,nbtot,   &
            HybMesh%NB_Hex,HybMesh%NB_Prm,HybMesh%NB_Pyr,HybMesh%NB_Tet, nas1, nas2, HybMesh%GridOrder
    ELSE IF(fma==-3)THEN
       WRITE(11,err=111)HybMesh%NB_Tet,HybMesh%NB_Point,nas2
    ELSE IF(fma==-13)THEN
       WRITE(11,err=111)HybMesh%NB_Tet,HybMesh%NB_Point,nas2, HybMesh%GridOrder
    ENDIF

    ND = Npoint_Tet(HybMesh%GridOrder)

    IF(fma==-1 .OR. fma==-11)THEN

       WRITE(11,err=111)((HybMesh%IP_Hex(i,nel),nel=1,HybMesh%NB_Hex),i=1,8)
       WRITE(11,err=111)((HybMesh%IP_Prm(i,nel),nel=1,HybMesh%NB_Prm),i=1,6)
       WRITE(11,err=111)((HybMesh%IP_Pyr(i,nel),nel=1,HybMesh%NB_Pyr),i=1,5)
       WRITE(11,err=111)((HybMesh%IP_Tet(i,nel),nel=1,HybMesh%NB_Tet),i=1,ND)
       WRITE(11,err=111)((HybMesh%Posit(i,np),  np =1,HybMesh%NB_Point),i=1,3)

       IF(PRESENT(Surf))THEN
          IF(Surf%NB_Quad>0)THEN
             WRITE(11,err=111)((Surf%IP_Quad(i,nb),nb=1,Surf%NB_Quad),i=1,5)
          ELSE
             WRITE(11,err=111)
          ENDIF
          WRITE(11,err=111)((Surf%IP_Tri(i,nb), nb=1,Surf%NB_Tri), i=1,5)
       ELSE
          WRITE(11,err=111)
          WRITE(11,err=111)
       ENDIF

    ELSE IF(fma==-2)THEN

       DO nel=1,HybMesh%NB_Hex
          WRITE(11,err=111) HybMesh%IP_Hex(1:8,nel)
       ENDDO
       DO nel=1,HybMesh%NB_Pyr
          WRITE(11,err=111) HybMesh%IP_Pyr(1:5,nel)
       ENDDO
       DO nel=1,HybMesh%NB_Tet
          WRITE(11,err=111) HybMesh%IP_Tet(1:4,nel)
       ENDDO
       DO np=1,HybMesh%NB_Point
          WRITE(11,err=111) HybMesh%Posit(1:3,np)
       ENDDO
       IF(PRESENT(Surf))THEN
          IF(Surf%NB_Quad>0)THEN
             WRITE(11,err=111)((Surf%IP_Quad(i,nb),nb=1,Surf%NB_Quad),i=1,5)
          ELSE
             WRITE(11,err=111)
          ENDIF
          DO nb=1,Surf%NB_Tri
             WRITE(11,err=111) Surf%IP_Tri(1:5,nb)
          ENDDO
       ENDIF

    ELSE IF(fma==-3 .OR. fma==-13)THEN

       WRITE(11,err=111)((HybMesh%IP_Tet(i,nel),nel=1,HybMesh%NB_Tet),i=1,ND)  !Need to output relevent tets
       WRITE(11,err=111)((HybMesh%Posit(i,np),  np =1,HybMesh%NB_Point),i=1,3)

       IF(PRESENT(Surf))THEN
          WRITE(11,err=111)((Surf%IP_Tri(i,nb), nb=1,Surf%NB_Tri), i=1,5)
       ELSE
          WRITE(11,err=111)
       ENDIF

    ELSE IF(fma==-5)THEN

       ALLOCATE (xyz(3,HybMesh%NB_Point))
       DO i=1,HybMesh%NB_Point
          xyz(:,i) = HybMesh%Posit(:,i)
       ENDDO

       string = 'Fortran Binary' 
       WRITE(11,err=111) string
       string = 'ENSIGHT'
       WRITE(11,err=111) string
       string = 'GEO FILE'
       WRITE(11,err=111) string
       string = 'node id off'
       WRITE(11,err=111) string
       string = 'element id off'
       WRITE(11,err=111) string

       string = 'part'
       WRITE(11,err=111) string
       is = 1 
       WRITE(11,err=111) is
       string = 'Volume Mesh'
       WRITE(11,err=111) string
       string = 'coordinates'
       WRITE(11,err=111) string
       WRITE(11,err=111) HybMesh%NB_Point
       WRITE(11,err=111) xyz(1,1:HybMesh%NB_Point)
       WRITE(11,err=111) xyz(2,1:HybMesh%NB_Point)
       WRITE(11,err=111) xyz(3,1:HybMesh%NB_Point)

       IF(HybMesh%NB_Hex>0)THEN
          string = 'hexa8'
          WRITE(11,err=111) string
          WRITE(11,err=111) HybMesh%NB_Hex 
          WRITE(11,err=111) ((ABS(HybMesh%IP_Hex(j,i)),j=1,8), i=1,HybMesh%NB_Hex)
       ENDIF
       IF(HybMesh%NB_Prm>0)THEN
          string = 'penta6'
          WRITE(11,err=111) string
          WRITE(11,err=111) HybMesh%NB_Prm 
          WRITE(11,err=111) ((ABS(HybMesh%IP_Prm(j,i)),j=1,6), i=1,HybMesh%NB_Prm)
       ENDIF
       IF(HybMesh%NB_Pyr>0)THEN
          string = 'pyramid5'
          WRITE(11,err=111) string
          WRITE(11,err=111) HybMesh%NB_Pyr 
          WRITE(11,err=111) ((ABS(HybMesh%IP_Pyr(j,i)),j=1,5), i=1,HybMesh%NB_Pyr)
       ENDIF
       IF(HybMesh%NB_Tet>0)THEN
          string = 'tetra4'
          WRITE(11,err=111) string
          WRITE(11,err=111) HybMesh%NB_Tet 
          WRITE(11,err=111) ((ABS(HybMesh%IP_Tet(j,i)), j=1,4), i=1,HybMesh%NB_Tet)
       ENDIF

       IF(PRESENT(Surf))THEN
          ALLOCATE (gloloc(HybMesh%NB_Point),ips(4,Surf%NB_Tri+Surf%NB_Quad))

          DO is = 1,Surf%NB_Surf
             gloloc(1:HybMesh%NB_Point) = 0
             nbp = 0
             nbn = 0
             DO i = 1,Surf%NB_Tri
                IF(Surf%IP_Tri(5,i)==is)THEN
                   nbn = nbn+1
                   DO j=1,3
                      ip = Surf%IP_Tri(j,i)
                      IF(gloloc(ip) == 0)THEN
                         nbp = nbp + 1
                         gloloc(ip) = nbp
                         xyz(:,nbp) = HybMesh%Posit(:,ip)
                      ENDIF
                      ips(j,nbn) = gloloc(ip)
                   ENDDO
                ENDIF
             ENDDO
             IF(nbn==0) CYCLE

             string = 'part'
             WRITE(11,err=111) string
             WRITE(11,err=111) is+1
             WRITE(fileExtension,'(i4)') is
             string = 'Tri Surface Mesh '//fileExtension
             WRITE(11,err=111) string
             string = 'coordinates'
             WRITE(11,err=111) string
             WRITE(11,err=111) nbp
             WRITE(11,err=111) xyz(1,1:nbp)
             WRITE(11,err=111) xyz(2,1:nbp)
             WRITE(11,err=111) xyz(3,1:nbp)

             string = 'tria3'
             WRITE(11,err=111) string
             WRITE(11,err=111) nbn 
             WRITE(11,err=111) ((ips(j,i),j=1,3), i=1,nbn)
          ENDDO

          IF(Surf%NB_Quad>0)THEN
             gloloc(1:HybMesh%NB_Point) = 0
             nbp = 0
             DO i = 1,Surf%NB_Quad
                DO j=1,4
                   ip = Surf%IP_Quad(j,i)
                   IF(gloloc(ip) == 0)THEN
                      nbp = nbp + 1
                      gloloc(ip) = nbp
                      xyz(:,nbp) = HybMesh%Posit(:,ip)
                   ENDIF
                   ips(j,i) = gloloc(ip)
                ENDDO
             ENDDO

             string = 'part'
             WRITE(11,err=111) string
             WRITE(11,err=111) Surf%NB_Surf+2
             string = 'Quad Surface Mesh'
             WRITE(11,err=111) string
             string = 'coordinates'
             WRITE(11,err=111) string
             WRITE(11,err=111) nbp
             WRITE(11,err=111) xyz(1,1:nbp)
             WRITE(11,err=111) xyz(2,1:nbp)
             WRITE(11,err=111) xyz(3,1:nbp)

             string = 'quad4'
             WRITE(11,err=111) string
             WRITE(11,err=111) Surf%NB_Quad 
             WRITE(11,err=111) ((ips(j,i),j=1,4), i=1,Surf%NB_Quad)
          ENDIF

          DEALLOCATE (gloloc, ips)
       ENDIF

       DEALLOCATE (xyz)

    ENDIF

    CLOSE(11)
    RETURN

111 WRITE(*,*) 'Error--- HybridMeshStorage_Output: ', JobName(1:JobNameLength)//TRIM(ffmat)
    CALL Error_STOP (' ')
  END SUBROUTINE HybridMeshStorage_Output

  !>
  !!  Output a Hybrid mesh. 
  !!  @param[in]  JobName  the name (prefix) of Output file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  fmat = 1 :   formatted .mesh   \n
  !!                   = 4 :   cvt format .dat   \n
  !!                   = 6 :   gnuplot .net      \n
  !!                   = 7 :   unformatted .geo  (for enSight)
  !!  @param[in] HybMesh  the hybrid mesh.
  !!  @param[in] Surf     a surface mesh represent the boundary.
  !<
  SUBROUTINE HybridMeshStorage_Output_Formatted(JobName,JobNameLength,fmat,HybMesh,Surf)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(IN) :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength, fmat
    INTEGER :: np,nel,netot, nbtot, nb, ip4(4), ND
    INTEGER :: i, j, ip, is, nbn, nbp
    REAL*8  :: p1(3), p2(3), p3(3), p4(3), p0(3)
    INTEGER, DIMENSION(:), POINTER :: markp
    CHARACTER*(20) :: ffmat


    IF(fmat==1)THEN
       ffmat(1:6) = '.mesh '
    ELSE IF(fmat==4)THEN
       ffmat(1:5) = '.dat '
    ELSE IF(fmat==6)THEN
       ffmat(1:5) = '.net '
    ELSE IF(fmat==7)THEN
       ffmat(1:5) = '.geo '
    ELSE
       WRITE(*,*)' Error--- not for such format: fmat=',fmat
       CALL Error_STOP ( 'HybridMeshStorage_Output_Formatted')
    ENDIF

    WRITE(*,*) '--- Write file ',JobName(1:JobNameLength)//TRIM(ffmat)
    WRITE(*,*)'HybMesh%(NB_Point,  NB_Hex,  NB_Prm,  NB_Pyr,  NB_Tet)='
    WRITE(*,*) HybMesh%NB_Point,HybMesh%NB_Hex,HybMesh%NB_Prm,HybMesh%NB_Pyr,HybMesh%NB_Tet

    ND = Npoint_Tet(HybMesh%GridOrder)

    netot = HybMesh%NB_Tet + HybMesh%NB_Pyr + HybMesh%NB_Prm + HybMesh%NB_Hex
    OPEN(11,file= JobName(1:JobNameLength)//TRIM(ffmat), &
         status='UNKNOWN',form='FORMATTED')

    IF(fmat==1)THEN
       nbtot = Surf%NB_Tri + Surf%NB_Quad
       WRITE(11,'(9(i8,1x))')netot,HybMesh%NB_Point,nbtot,   &
            HybMesh%NB_Hex,HybMesh%NB_Prm,HybMesh%NB_Pyr,HybMesh%NB_Tet, &
            Surf%NB_Quad, Surf%NB_Tri
       WRITE(*,*)'Surf%(NB_Quad, NB_Tri)=',Surf%NB_Quad, Surf%NB_Tri

       DO nel=1,HybMesh%NB_Hex
          WRITE(11,'(6(i8,1x))')nel, HybMesh%IP_Hex(1:4,nel)
          WRITE(11,'(8x,6(i8,1x))')  HybMesh%IP_Hex(5:8,nel)
       ENDDO
       DO nel=1,HybMesh%NB_Prm
          WRITE(11,'(7(i8,1x))')nel, HybMesh%IP_Prm(1:6,nel)
       ENDDO
       DO nel=1,HybMesh%NB_Pyr
          WRITE(11,'(6(i8,1x))') nel,HybMesh%IP_Pyr(1:5,nel)
       ENDDO
       DO nel=1,HybMesh%NB_Tet
          WRITE(11,'(8(i8,1x),10(/,9x,7(i8,1x)))') nel,HybMesh%IP_Tet(1:ND,nel)
       ENDDO
       DO np=1,HybMesh%NB_Point
          WRITE(11,'(i8,1x,3(E17.9,1x))') np, HybMesh%Posit(1:3,np)
       ENDDO

       DO nb=1,Surf%NB_Quad
          WRITE(11,'(6(i8,1x))') nb, Surf%IP_Quad(1:5,nb)
       ENDDO
       DO nb=1,Surf%NB_Tri
          WRITE(11,'(6(i8,1x))') nb, Surf%IP_Tri(1:5,nb)
       ENDDO

    ELSE IF(fmat==4)THEN

       !--- count boundary nodes
       ALLOCATE(markp(HybMesh%NB_Point))
       markp(:) = 0
       np = 0
       DO NB = 1, Surf%NB_Tri
          DO i = 1, 3
             ip = Surf%IP_Tri(i,nb)
             IF(markp(ip)==0)THEN
                np = np+1
                markp(ip) = np
             ENDIF
          ENDDO
       ENDDO

       WRITE(11,'(a)')'1'
       WRITE(11,'(a)')'title'
       WRITE(11,'(a)')'ne	np	nb'
       WRITE(11,*)HybMesh%NB_Tet,HybMesh%NB_Point,np
       WRITE(11,'(a)')'connectivities'
       DO nel = 1, HybMesh%NB_Tet
          WRITE(11,'(5I8)') nel, HybMesh%IP_Tet(1:4,nel)
       ENDDO

       WRITE(11,'(a)')'centres'
       DO nel = 1, HybMesh%NB_Tet
          ip4(:) = HybMesh%IP_Tet(1:4,nel)
          p1     = HybMesh%Posit(:,ip4(1))
          p2     = HybMesh%Posit(:,ip4(2))
          p3     = HybMesh%Posit(:,ip4(3))
          p4     = HybMesh%Posit(:,ip4(4))
          p0     = Geo3D_Sphere_Centre(P1,P2,P3,P4)
          WRITE(11,'(i8,3(1x,e19.12),I8)') nel, p0, 1
       ENDDO

       WRITE(11,'(a)')'coordinates'
       DO np = 1, HybMesh%NB_Point
          WRITE(11,'(i8,3(1x,e19.12),I8)') np, HybMesh%Posit(1:3,np),1
       ENDDO

       WRITE(11,'(a)')'unknowns'
       DO np = 1, HybMesh%NB_Point
          WRITE(11,'(5I8)') np, 0,1,0,1
       ENDDO

       WRITE(11,'(a)')'boundaries'
       DO ip = 1, HybMesh%NB_Point
          IF(markp(ip)>0) WRITE(11,'(i8)') ip
       ENDDO
       DEALLOCATE(markp)

    ELSE IF(fmat==6)THEN

       DO nel=1,HybMesh%NB_Hex
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(1,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(2,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(3,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(4,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(1,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(5,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(6,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(7,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(8,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(5,nel))    
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(2,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(6,nel))
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(3,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(7,nel))
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(4,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Hex(8,nel))
       ENDDO
       DO nel=1,HybMesh%NB_Prm
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Prm(1,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Prm(2,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Prm(3,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Prm(1,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Prm(4,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Prm(5,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Prm(6,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Prm(4,nel))    
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Prm(2,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Prm(5,nel))
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Prm(3,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Prm(6,nel))
       ENDDO
       DO nel=1,HybMesh%NB_Pyr
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Pyr(1,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Pyr(2,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Pyr(3,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Pyr(4,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Pyr(1,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Pyr(5,nel))  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Pyr(2,nel))
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Pyr(3,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Pyr(5,nel))
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Pyr(4,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Pyr(5,nel))
       ENDDO
       DO nel=1,HybMesh%NB_Tet
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Tet(1,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Tet(2,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Tet(3,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Tet(1,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Tet(4,nel))  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Tet(2,nel))
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Tet(3,nel))
          WRITE(11,'(3(E17.9,1x))')  HybMesh%Posit(:,HybMesh%IP_Tet(4,nel))
       ENDDO

    ELSE IF(fmat==7)THEN

       WRITE(11,'(A)') 'TITLE  =  "Example: "'
       WRITE(11,'(A)') 'ENSIGHT GEO FILE'
       WRITE(11,'(A)') 'node id off'
       WRITE(11,'(A)') 'element id off'

       WRITE(11,'(A)') 'coordinates'
       WRITE(11,'(I8)') HybMesh%NB_Point
       DO nel = 1,HybMesh%NB_Point
          WRITE(11,'(3(e12.5))') HybMesh%Posit(1:3,nel)
       ENDDO

       WRITE(11,'(A,I3)') 'part', 1
       WRITE(11,'(A)') 'Volume Mesh'
       IF(HybMesh%NB_Hex>0)THEN
          WRITE(11,'(A)') 'hexa8'
          WRITE(11,'(I8)') HybMesh%NB_Hex 
          DO nel = 1,HybMesh%NB_Hex
             WRITE(11,'(8I8)') HybMesh%IP_Hex(1:8,nel)
          ENDDO
       ENDIF
       IF(HybMesh%NB_Prm>0)THEN
          WRITE(11,'(A)') 'penta6'
          WRITE(11,'(I8)') HybMesh%NB_Prm 
          DO nel = 1,HybMesh%NB_Prm
             WRITE(11,'(6I8)') HybMesh%IP_Prm(1:6,nel)
          ENDDO
       ENDIF
       IF(HybMesh%NB_Pyr>0)THEN
          WRITE(11,'(A)') 'pyramid5'
          WRITE(11,'(I8)') HybMesh%NB_Pyr 
          DO nel = 1,HybMesh%NB_Pyr
             WRITE(11,'(5I8)') HybMesh%IP_Pyr(1:5,nel)
          ENDDO
       ENDIF
       IF(HybMesh%NB_Tet>0)THEN
          WRITE(11,'(A)') 'tetra4'
          WRITE(11,'(I8)') HybMesh%NB_Tet 
          DO nel = 1,HybMesh%NB_Tet
             WRITE(11,'(4I8)') HybMesh%IP_Tet(1:4,nel)
          ENDDO
       ENDIF

       DO is = 1,Surf%NB_Surf
          nbn = 0
          DO nb = 1,Surf%NB_Tri
             IF(Surf%IP_Tri(5,nb)==is) nbn = nbn+1
          ENDDO
          IF(nbn==0) CYCLE
          WRITE(11,'(A,I3)') 'part',is+1
          WRITE(11,'(A,I3)') 'Tri Surface Mesh',is
          WRITE(11,'(A)') 'tria3'
          WRITE(11,'(I8)') nbn 
          DO nb = 1,Surf%NB_Tri
             IF(Surf%IP_Tri(5,nb)==is) WRITE(11,'(3I8)') Surf%IP_Tri(1:3,nb)
          ENDDO
       ENDDO
       IF(Surf%NB_Quad>0)THEN
          WRITE(11,'(A,I3)') 'part', Surf%NB_Surf+2
          WRITE(11,'(A)') 'Quad Surface Mesh'
          WRITE(11,'(A)') 'quad4'
          WRITE(11,'(I8)') Surf%NB_Quad 
          DO nb = 1,Surf%NB_Quad
             WRITE(11,'(4I8)') Surf%IP_Quad(1:4,nb)
          ENDDO
       ENDIF

    ENDIF

    CLOSE(11)
    RETURN

  END SUBROUTINE HybridMeshStorage_Output_Formatted


  !>
  !!  Input a Hybrid mesh by reading a *.dat (Elfen formatted) file. 
  !!  @param[in]  JobName  the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[out] HybMesh  the hybrid mesh.
  !!  @param[out] Surf     a surface mesh represent the boundary.
  !<
  SUBROUTINE HybridMeshStorage_ElfenInput(JobName,JobNameLength,HybMesh,Surf)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(INOUT) :: HybMesh
    TYPE(SurfaceMeshStorageType), INTENT(INOUT), OPTIONAL :: Surf
    CHARACTER*(*), INTENT(IN) :: JobName
    INTEGER, INTENT(IN) ::  JobNameLength
    INTEGER :: NNODE, NELGP, NDIMN, NPOIN, Isucc
    INTEGER :: np,nel
    CHARACTER(LEN=256) :: ffmat
    TYPE(HybridMeshElfenFormat) :: ElfenFormat

    OPEN(11,file= JobName(1:JobNameLength)//'.dat', &
         status='OLD',form='FORMATTED')

    DO
       READ(11,'(a)',END=100) ffmat

       IF(CHAR_CutFront(ffmat,ElfenFormat%Element_topology))THEN
          WRITE(*,*) ElfenFormat%Element_topology
          IF(.NOT. CHAR_CutFront(ffmat,'{')) CALL Error_STOP ( '---expect {')
          NNODE = 0
          NELGP = 0

          IF(CHAR_CutFront(ffmat,ElfenFormat%NNODE))THEN
             NNODE = Char_RemoveInt(ffmat)
          ENDIF
          IF(CHAR_CutFront(ffmat,ElfenFormat%NELGP))THEN
             NELGP = Char_RemoveInt(ffmat)
          ENDIF
          WRITE(*,*) 'NNODE,NELGP=',NNODE,NELGP

          IF(NNODE==8)THEN
             HybMesh%NB_Hex = NELGP
             ALLOCATE(HybMesh%IP_Hex(8, NELGP)) 
             DO nel = 1, NELGP
                READ(11,*)HybMesh%IP_Hex(:, nel)
             ENDDO
          ELSE IF(NNODE==6)THEN
             HybMesh%NB_Prm = NELGP
             ALLOCATE(HybMesh%IP_Prm(6, NELGP)) 
             DO nel = 1, NELGP
                READ(11,*)HybMesh%IP_Prm(:, nel)
             ENDDO
          ELSE IF(NNODE==5)THEN
             HybMesh%NB_Pyr = NELGP
             ALLOCATE(HybMesh%IP_Pyr(5, NELGP)) 
             DO nel = 1, NELGP
                READ(11,*)HybMesh%IP_Pyr(:, nel)
             ENDDO
          ELSE IF(NNODE==4)THEN
             HybMesh%NB_Tet = NELGP
             ALLOCATE(HybMesh%IP_Tet(4, NELGP)) 
             DO nel = 1, NELGP
                READ(11,*)HybMesh%IP_Tet(:, nel)
             ENDDO
          ELSE
             CALL Error_STOP ( 'wrong shape')
          ENDIF

       ENDIF

       IF(CHAR_CutFront(ffmat,ElfenFormat%Coordinates))THEN
          WRITE(*,*) ElfenFormat%Coordinates
          IF(.NOT. CHAR_CutFront(ffmat,'{')) CALL Error_STOP ( '---expect {')
          NDIMN = 0
          NPOIN = 0

          IF(CHAR_CutFront(ffmat,ElfenFormat%NDIMN))THEN
             NDIMN = Char_RemoveInt(ffmat)
          ENDIF
          IF(CHAR_CutFront(ffmat,ElfenFormat%NPOIN))THEN
             NPOIN = Char_RemoveInt(ffmat)
          ENDIF
          WRITE(*,*)'NDIMN,NPOIN=',NDIMN,NPOIN

          HybMesh%NB_Point = NPOIN
          ALLOCATE(HybMesh%Posit (3, NPOIN)) 

          DO np=1,HybMesh%NB_Point
             READ(11,*) HybMesh%Posit(1:3,np)
          ENDDO
       ENDIF

    ENDDO

100 CONTINUE    

  CONTAINS

    FUNCTION Char_RemoveInt(string) RESULT(n)
      CHARACTER(len=*) :: string
      INTEGER :: n, Isucc
      IF(.NOT. CHAR_CutFront(string,'{')) CALL Error_STOP ( '---expect {')

      n = CHAR_to_INT(string,Isucc)

      IF(Isucc==0) CALL Error_STOP ( '----Isucc=0')
      IF(.NOT. CHAR_CutFront(string,INT_to_CHAR(n,0))) CALL Error_STOP ( '----number')
      IF(.NOT. CHAR_CutFront(string,'}')) CALL Error_STOP ( '---expect }')
    END FUNCTION Char_RemoveInt


  END SUBROUTINE HybridMeshStorage_ElfenInput


  !>
  !!  Convert a tetrahedral to a Hybrid mesh. 
  !!  @param[in]  TetMesh  the tetrahedral mesh.
  !!  @param[out] HybMesh  the hybrid mesh.
  !<
  SUBROUTINE HybridMeshStorage_From_TetMesh(TetMesh,HybMesh)
    IMPLICIT NONE
    TYPE(TetMeshStorageType),    INTENT(IN)  :: TetMesh
    TYPE(HybridMeshStorageType), INTENT(OUT) :: HybMesh
    INTEGER :: np,nel

    HybMesh%NB_Point = TetMesh%NB_Point
    HybMesh%NB_Hex   = 0
    HybMesh%NB_Prm   = 0
    HybMesh%NB_Pyr   = 0
    HybMesh%NB_Tet   = TetMesh%NB_Tet

    ALLOCATE(HybMesh%IP_Hex(8, HybMesh%NB_Hex)) 
    ALLOCATE(HybMesh%IP_Prm(6, HybMesh%NB_Prm)) 
    ALLOCATE(HybMesh%IP_Pyr(5, HybMesh%NB_Pyr)) 
    ALLOCATE(HybMesh%IP_Tet(4, HybMesh%NB_Tet)) 
    ALLOCATE(HybMesh%Posit (3, HybMesh%NB_Point)) 

    DO nel=1,HybMesh%NB_Tet
       HybMesh%IP_Tet(1:4,nel) = TetMesh%IP_Tet(1:4,nel)
    ENDDO
    DO np=1,HybMesh%NB_Point
       HybMesh%Posit(1:3,np) = TetMesh%Posit(1:3,np)
    ENDDO

    RETURN
  END SUBROUTINE HybridMeshStorage_From_TetMesh

  !>
  !!  Measure the mesh get two corners. 
  !!  @param[in] HybMesh  the hybrid mesh.
  !!  @param[out] pMin    the poistion of the minimum corner.
  !!  @param[out] pMax    the poistion of the maximum corner.
  !<
  SUBROUTINE HybridMeshStorage_Measure(HybMesh,pMin,pMax)
    IMPLICIT NONE
    TYPE(HybridMeshStorageType), INTENT(IN)  :: HybMesh
    REAL*8, INTENT(OUT)  :: pMin(3), pMax(3)
    INTEGER :: ip

    pMin = HybMesh%Posit(:,1)
    pMax = HybMesh%Posit(:,1)    
    DO ip=2,HybMesh%NB_Point
       WHERE(pMin(:)>HybMesh%Posit(:,ip)) pMin(:) = HybMesh%Posit(:,ip)
       WHERE(pMax(:)<HybMesh%Posit(:,ip)) pMax(:) = HybMesh%Posit(:,ip)
    ENDDO

    RETURN
  END SUBROUTINE HybridMeshStorage_Measure


END MODULE HybridMeshStorage



