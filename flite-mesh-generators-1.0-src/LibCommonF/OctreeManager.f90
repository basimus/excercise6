!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!    A collection of functions for module OctreeMesh.
!<
MODULE OctreeMeshManager
  USE OctreeMesh
  USE SpacingStorage
  USE TetMeshStorage
  USE HybridMeshStorage
  USE NodeNetTree

CONTAINS

  !****************************************************************************!
  !>
  !!  Do bisecting a octree mesh to get that
  !!     every leaf cell has a size smaller than local grid-size.
  !!  @param[in,out] OctMesh : the octree mesh.
  !!  @param[in]  BGSpacing  : the background spacing.
  !<       
  !****************************************************************************!
  SUBROUTINE OctreeMesh_RefineBySpacing(OctMesh,BGSpacing)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    INTEGER :: i,j,k

    DO k=1,OctMesh%kmax
       WRITE(*,'(a,i3,a,i3,a$)')'  OctreeMesh_RefineBySpacing:: k=',   &
            k,' out of',OctMesh%kmax, CHAR(13)
       DO j=1,OctMesh%jmax
          DO i=1,OctMesh%imax
             pOct_Cell => OctMesh%Cells(i,j,k)
             CALL OctreeMesh_CellBisect_bySpacing(pOct_Cell)
          ENDDO
       ENDDO
    ENDDO
    WRITE(*,*)' '

    RETURN

  CONTAINS

    !>
    !!  Bisect a octree cell (or it's offspring cells) so that
    !!     every leaf cell has a size smaller than local grid-size.
    !!  @param[in]  pOct_Cell  : a pointer to a octree cell
    !!                           (not neccessarily being a leaf cell).
    !<       
    RECURSIVE SUBROUTINE OctreeMesh_CellBisect_bySpacing(pOct_Cell)
      IMPLICIT NONE
      TYPE (OctreeMesh_Cell_Type), POINTER ::pOct_Cell,p2Oct_Cell
      REAL*8 :: pp(3),D(3)
      INTEGER :: idirct
      IF(pOct_Cell%Level == 0) RETURN      !--- Empty Cell
      IF(pOct_Cell%Level>=Max_Oct_Level) RETURN
      IF(pOct_Cell%Bisected)THEN
         DO idirct=1,8
            p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
            CALL OctreeMesh_CellBisect_bySpacing(p2Oct_Cell)
         ENDDO
      ELSE
         pp(:) =  OctreeMesh_getCellCentre(pOct_Cell,OctMesh)
         CALL  SpacingStorage_Get3DGridSize(BGSpacing, pp,D)
         D = 1.001 * D
         IF(  OctMesh%DX(pOct_Cell%Level)<=D(1) .AND.    &
              OctMesh%DY(pOct_Cell%Level)<=D(2) .AND.    &
              OctMesh%DZ(pOct_Cell%Level)<=D(3) ) RETURN
         CALL OctreeMesh_CellBisect(pOct_Cell,OctMesh)
         CALL OctreeMesh_CellBisect_bySpacing(pOct_Cell)
      ENDIF
    END SUBROUTINE OctreeMesh_CellBisect_bySpacing

  END SUBROUTINE OctreeMesh_RefineBySpacing
  
  
  !****************************************************************************!
  !>
  !!  Do bisecting a octree mesh to get that
  !!     every leaf cell has a size smaller than local grid-size.
  !!  @param[in,out] OctMesh : the octree mesh.
  !!  @param[in]  BGSpacing  : the background spacing.
  !<       
  !****************************************************************************!
  SUBROUTINE OctreeMesh_RefineBySpacing2(OctMesh,BGSpacing)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell
    TYPE(SpacingStorageType), INTENT(IN) :: BGSpacing
    INTEGER :: i,j,k

    DO k=1,OctMesh%kmax
       WRITE(*,'(a,i3,a,i3,a$)')'  OctreeMesh_RefineBySpacing:: k=',   &
            k,' out of',OctMesh%kmax, CHAR(13)
       DO j=1,OctMesh%jmax
          DO i=1,OctMesh%imax
             pOct_Cell => OctMesh%Cells(i,j,k)
             CALL OctreeMesh_CellBisect_bySpacing2(pOct_Cell)
          ENDDO
       ENDDO
    ENDDO
    WRITE(*,*)' '

    RETURN

  CONTAINS

    !>
    !!  Bisect a octree cell (or it's offspring cells) so that
    !!     every leaf cell has a size smaller than local grid-size.
    !!  @param[in]  pOct_Cell  : a pointer to a octree cell
    !!                           (not neccessarily being a leaf cell).
    !<       
    RECURSIVE SUBROUTINE OctreeMesh_CellBisect_bySpacing2(pOct_Cell)
      IMPLICIT NONE
      TYPE (OctreeMesh_Cell_Type), POINTER ::pOct_Cell,p2Oct_Cell
      REAL*8 :: pp(3),D(3),DTemp(3)
      INTEGER :: idirct,ip, ipHang(18)
      IF(pOct_Cell%Level == 0) RETURN      !--- Empty Cell
      IF(pOct_Cell%Level>=Max_Oct_Level) RETURN
      IF(pOct_Cell%Bisected)THEN
         DO idirct=1,8
            p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
            CALL OctreeMesh_CellBisect_bySpacing2(p2Oct_Cell)
         ENDDO
      ELSE
         pp(:) =  OctreeMesh_getCellCentre(pOct_Cell,OctMesh)
         CALL  SpacingStorage_Get3DGridSize(BGSpacing, pp,D)
		 DO ip=1,8
		   pp(:) = OctMesh%Posit(:,pOct_Cell%Nodes(ip))
		   CALL  SpacingStorage_Get3DGridSize(BGSpacing, pp,DTemp)
		   D(1) = MIN(D(1),Dtemp(1))
		   D(2) = MIN(D(2),Dtemp(2))
		   D(3) = MIN(D(3),Dtemp(3))
		 END DO
		 CALL OctreeMesh_getCellHangNodes(pOct_Cell,OctMesh,ipHang)
		 DO ip=1,18
		   IF (ipHang(ip).GT.0) THEN
		    pp(:) = OctMesh%Posit(:,ipHang(ip))
			CALL  SpacingStorage_Get3DGridSize(BGSpacing, pp,DTemp)
			D(1) = MIN(D(1),Dtemp(1))
			D(2) = MIN(D(2),Dtemp(2))
			D(3) = MIN(D(3),Dtemp(3))
		   
		   END IF
		 END DO
         D = 2.001 * D
         IF(  OctMesh%DX(pOct_Cell%Level)<=D(1) .AND.    &
              OctMesh%DY(pOct_Cell%Level)<=D(2) .AND.    &
              OctMesh%DZ(pOct_Cell%Level)<=D(3) ) RETURN
         CALL OctreeMesh_CellBisect(pOct_Cell,OctMesh)
         CALL OctreeMesh_CellBisect_bySpacing2(pOct_Cell)
      ENDIF
    END SUBROUTINE OctreeMesh_CellBisect_bySpacing2

  END SUBROUTINE OctreeMesh_RefineBySpacing2


  !****************************************************************************!
  !>
  !!  Bisect an octree mesh so that
  !!     every leaf cell has a size comparable to local grid-size-gradient.
  !!  @param[in,out] OctMesh : the octree mesh.
  !!  @param[in]  BGSpacing  : the background spacing.
  !<       
  !****************************************************************************!
  SUBROUTINE OctreeMesh_RefineByGradient(OctMesh,BGSpacing)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type),   INTENT(INOUT) :: OctMesh
    TYPE(SpacingStorageType), INTENT(INOUT) :: BGSpacing
    TYPE (OctreeMesh_Cell_Type), POINTER    :: pOct_Cell
    REAL*8  :: factor
    INTEGER :: i,j,k

    factor = BGSpacing%Gradation_Factor
    IF(factor>5.D-1) factor = 5.D-1
    IF(factor<5.D-2) factor = 5.D-2

    DO k=1,OctMesh%kmax
       WRITE(*,'(a,i4,a,i4,a$)')'  OctreeMesh_RefineByGradient:: k= ',   &
            k,' out of',OctMesh%kmax, CHAR(13)
       DO j=1,OctMesh%jmax
          DO i=1,OctMesh%imax
             pOct_Cell => OctMesh%Cells(i,j,k)
             CALL OctreeMesh_CellBisect_byGradient(pOct_Cell)
          ENDDO
       ENDDO
    ENDDO
    WRITE(*,*)' '

    RETURN

  CONTAINS

    !>
    !!  Bisect an octree cell (or it's offspring cells) so that
    !!     every leaf cell has a size comparable to the local grid-size-gradient.
    !!  @param[in]  pOct_Cell  : a pointer to a octree cell
    !!                           (not neccessarily being a leaf cell).
    !<       
    RECURSIVE SUBROUTINE OctreeMesh_CellBisect_byGradient(pOct_Cell)
      IMPLICIT NONE
      TYPE (OctreeMesh_Cell_Type), POINTER ::pOct_Cell,p2Oct_Cell
      REAL*8 :: pp(3),D
      INTEGER :: idirct
      IF(pOct_Cell%Level == 0) RETURN      !--- Empty Cell
      IF(pOct_Cell%Level>=Max_Oct_Level) RETURN
      IF(pOct_Cell%Bisected)THEN
         DO idirct=1,8
            p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
            CALL OctreeMesh_CellBisect_byGradient(p2Oct_Cell)
         ENDDO
      ELSE
         pp(:) =  OctreeMesh_getCellCentre(pOct_Cell,OctMesh)
         CALL SpacingStorage_GetGradient(BGSpacing, pp,D)
         D = D * MAX( OctMesh%DX(pOct_Cell%Level), OctMesh%DY(pOct_Cell%Level),   &
              OctMesh%DZ(pOct_Cell%Level) )
         IF( D<=factor ) RETURN
         CALL OctreeMesh_CellBisect(pOct_Cell,OctMesh)
         CALL OctreeMesh_CellBisect_byGradient(pOct_Cell)
      ENDIF
    END SUBROUTINE OctreeMesh_CellBisect_byGradient

  END SUBROUTINE OctreeMesh_RefineByGradient

  !>
  !!     Split a cell into tetrahedra of mesh TetMesh.
  !<
  RECURSIVE SUBROUTINE OctreeMesh_CellTetSpliting(pOct_Cell,OctMesh,TetMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(IN) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    TYPE(TetMeshStorageType) :: TetMesh
    INTEGER :: ip(8), ipHang(18), Level, ipp(4), ipe(4), ipp1(4), ipe1(4)
    INTEGER :: i, k, ic, k1, k2, k3, k4, idirct
    INTEGER :: nb_tri, ip_tri(3,20), NB, Isucc, Ips(1)
    LOGICAL :: noHang
    REAL*8  :: pp(3), pp0(3), Dist(1)

    IF(pOct_Cell%Level == 0) RETURN 

    IF(pOct_Cell%Bisected)THEN
       DO idirct=1,8
          p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
          CALL OctreeMesh_CellTetSpliting(p2Oct_Cell,OctMesh,TetMesh)
       ENDDO
       RETURN
    ENDIF

    Level       = pOct_Cell%Level
    ip(1:8)     = pOct_Cell%Nodes(1:8)
    ipHang(1:18) = 0    !--- 12 for edge, 6 for face

    !--- for hanging points

    p2Oct_Cell => pOct_Cell%Left
    IF(p2Oct_Cell%Level > Level) THEN
       ipHang(13) = p2Oct_Cell      %Nodes(7)
       ipHang(9)  = p2Oct_Cell      %Nodes(6)
       ipHang(5)  = p2Oct_Cell      %Nodes(3)
       ipHang(7)  = p2Oct_Cell%Up   %Nodes(7)
       ipHang(11) = p2Oct_Cell%Back %Nodes(7)
    ENDIF

    p2Oct_Cell => pOct_Cell%Front
    IF(p2Oct_Cell%Level > Level) THEN
       ipHang(15) = p2Oct_Cell      %Nodes(7)
       ipHang(9)  = p2Oct_Cell      %Nodes(8)
       ipHang(1)  = p2Oct_Cell      %Nodes(3)
       ipHang(3)  = p2Oct_Cell%Up   %Nodes(7)
       ipHang(10) = p2Oct_Cell%Right%Nodes(7)
    ENDIF

    p2Oct_Cell => pOct_Cell%Down
    IF(p2Oct_Cell%Level > Level) THEN
       ipHang(17) = p2Oct_Cell      %Nodes(7)
       ipHang(5)  = p2Oct_Cell      %Nodes(8)
       ipHang(1)  = p2Oct_Cell      %Nodes(6)
       ipHang(2)  = p2Oct_Cell%Back %Nodes(7)
       ipHang(6)  = p2Oct_Cell%Right%Nodes(7)
    ENDIF

    p2Oct_Cell => pOct_Cell%Right
    IF(p2Oct_Cell%Level > Level) THEN
       ipHang(14) = p2Oct_Cell      %Nodes(1)
       ipHang(12) = p2Oct_Cell      %Nodes(4)
       ipHang(8)  = p2Oct_Cell      %Nodes(5)
       ipHang(10) = p2Oct_Cell%Front%Nodes(1)
       ipHang(6)  = p2Oct_Cell%Down %Nodes(1)
    ENDIF

    p2Oct_Cell => pOct_Cell%Back
    IF(p2Oct_Cell%Level > Level) THEN
       ipHang(16) = p2Oct_Cell      %Nodes(1)
       ipHang(12) = p2Oct_Cell      %Nodes(2)
       ipHang(4)  = p2Oct_Cell      %Nodes(5)
       ipHang(11) = p2Oct_Cell%Left %Nodes(1)
       ipHang(2)  = p2Oct_Cell%Down %Nodes(1)
    ENDIF

    p2Oct_Cell => pOct_Cell%Up
    IF(p2Oct_Cell%Level > Level) THEN
       ipHang(18) = p2Oct_Cell      %Nodes(1)
       ipHang(8)  = p2Oct_Cell      %Nodes(2)
       ipHang(4)  = p2Oct_Cell      %Nodes(4)
       ipHang(7)  = p2Oct_Cell%Left %Nodes(1)
       ipHang(3)  = p2Oct_Cell%Front%Nodes(1)
    ENDIF

    !--- check hanging nodes further

    DO i = 1,12
       IF(ipHang(i)==0)THEN
          pp0 = ( OctMesh%Posit(:,ip(iEdge_Hex(1,i)))   &
               + OctMesh%Posit(:,ip(iEdge_Hex(2,i))) )/2.D0
          DO k=1,2
             ic = iEdge_Hex(k,i)
             pp  = pp0 + iCorner_Oct(1:3,ic)*OctMesh%TinyD(:)
             p2Oct_Cell => OctreeMesh_CellSearch(pp,Level+1,OctMesh)
             IF(p2Oct_Cell%Level==level) EXIT
             IF(p2Oct_Cell%Level==level+1)THEN
                ipHang(i) = p2Oct_Cell%Nodes(iOpp_Hex(ic))
                EXIT
             ENDIF
          ENDDO
       ENDIF
    ENDDO

    !--- for a cell without hanging points
    noHang = .TRUE.
    DO i=1,18
       IF(ipHang(i)>0) noHang = .FALSE.
    ENDDO

    IF(noHang)THEN
       DO i=1,6
          TetMesh%NB_Tet = TetMesh%NB_Tet+1
          TetMesh%IP_Tet(:,TetMesh%NB_Tet) = ip(iTet_Hex(:,i))
       ENDDO
       RETURN
    ENDIF

    !--- for a cell with hanging points

    TetMesh%NB_Point = TetMesh%NB_Point + 1
    TetMesh%Posit(:,TetMesh%NB_Point) =  ( OctMesh%Posit(:,ip(1)) + OctMesh%Posit(:,ip(7)) )/2.D0

    DO i=1,6
       ipp(1:4) = ip(iQuadFD_Hex(:,i))
       ipe(1:4) = ipHang(jEdge_QuadFD_Hex(:,i))
       nb_tri = 0
       IF(ipHang(i+12)==0)THEN
          CALL Quad_spliting_old(ipp, ipe, nb_tri, ip_tri)
       ELSE
          ipp1 = (/ipp(1),ipe(1),ipHang(i+12),ipe(4)/)
          ipe1 = 0          
          pp0 = ( OctMesh%Posit(:,ipp1(1)) + OctMesh%Posit(:,ipp1(2)) )/2.D0
          NB = -1
          Isucc = -1
          CALL PtADTree_SearchNode(Oct_PtADTree,pp0,IPs, Dist, NB, Isucc)
          IF(Isucc==1) ipe1(1) = IPs(1)
          pp0 = ( OctMesh%Posit(:,ipp1(1)) + OctMesh%Posit(:,ipp1(4)) )/2.D0
          NB = -1
          Isucc = -1
          CALL PtADTree_SearchNode(Oct_PtADTree,pp0,IPs, Dist, NB, Isucc)
          IF(Isucc==1) ipe1(4) = IPs(1)
          CALL Quad_spliting_old(ipp1, ipe1, nb_tri, ip_tri)

          ipp1 = (/ipe(1),ipp(2),ipe(2),ipHang(i+12)/)
          ipe1 = 0          
          pp0 = ( OctMesh%Posit(:,ipp1(1)) + OctMesh%Posit(:,ipp1(2)) )/2.D0
          NB = -1
          Isucc = -1
          CALL PtADTree_SearchNode(Oct_PtADTree,pp0,IPs, Dist, NB, Isucc)
          IF(Isucc==1) ipe1(1) = IPs(1)
          pp0 = ( OctMesh%Posit(:,ipp1(2)) + OctMesh%Posit(:,ipp1(3)) )/2.D0
          NB = -1
          Isucc = -1
          CALL PtADTree_SearchNode(Oct_PtADTree,pp0,IPs, Dist, NB, Isucc)
          IF(Isucc==1) ipe1(2) = IPs(1)
          CALL Quad_spliting_old(ipp1, ipe1, nb_tri, ip_tri)

          ipp1 = (/ipHang(i+12),ipe(2),ipp(3),ipe(3)/)
          ipe1 = 0          
          pp0 = ( OctMesh%Posit(:,ipp1(2)) + OctMesh%Posit(:,ipp1(3)) )/2.D0
          NB = -1
          Isucc = -1
          CALL PtADTree_SearchNode(Oct_PtADTree,pp0,IPs, Dist, NB, Isucc)
          IF(Isucc==1) ipe1(2) = IPs(1)
          pp0 = ( OctMesh%Posit(:,ipp1(3)) + OctMesh%Posit(:,ipp1(4)) )/2.D0
          NB = -1
          Isucc = -1
          CALL PtADTree_SearchNode(Oct_PtADTree,pp0,IPs, Dist, NB, Isucc)
          IF(Isucc==1) ipe1(3) = IPs(1)
          CALL Quad_spliting_old(ipp1, ipe1, nb_tri, ip_tri)


          ipp1 = (/ipe(4),ipHang(i+12),ipe(3),ipp(4)/)
          ipe1 = 0          
          pp0 = ( OctMesh%Posit(:,ipp1(4)) + OctMesh%Posit(:,ipp1(3)) )/2.D0
          NB = -1
          Isucc = -1
          CALL PtADTree_SearchNode(Oct_PtADTree,pp0,IPs, Dist, NB, Isucc)
          IF(Isucc==1) ipe1(3) = IPs(1)
          pp0 = ( OctMesh%Posit(:,ipp1(1)) + OctMesh%Posit(:,ipp1(4)) )/2.D0
          NB = -1
          Isucc = -1
          CALL PtADTree_SearchNode(Oct_PtADTree,pp0,IPs, Dist, NB, Isucc)
          IF(Isucc==1) ipe1(4) = IPs(1)
          CALL Quad_spliting_old(ipp1, ipe1, nb_tri, ip_tri)

       ENDIF

       DO k = 1, nb_tri
          TetMesh%NB_Tet = TetMesh%NB_Tet + 1
          IF(MOD(i,2)==1)THEN
             TetMesh%IP_Tet(1:3,TetMesh%NB_Tet) = ip_tri(:,k)
          ELSE
             TetMesh%IP_Tet(1:3,TetMesh%NB_Tet) =   &
                  (/ip_tri(1,k),ip_tri(3,k),ip_tri(2,k)/)
          ENDIF
          TetMesh%IP_Tet(4,TetMesh%NB_Tet) = TetMesh%NB_Point 
       ENDDO

    ENDDO

    RETURN
  END SUBROUTINE OctreeMesh_CellTetSpliting

  !>
  !!     Split all cells into tetrahedra (old version).
  !<
  SUBROUTINE OctreeMesh_getTetMesh_old(OctMesh,TetMesh,nTet)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(IN) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell
    TYPE(TetMeshStorageType) :: TetMesh
    INTEGER, OPTIONAL :: nTet(*)
    INTEGER :: i,j,k, nel

    TetMesh%NB_Tet = 0
    TetMesh%NB_Point = 0
    ALLOCATE(TetMesh%IP_Tet(4,12*OctMesh%numCells))
    ALLOCATE(TetMesh%Posit(3,OctMesh%numNodes + OctMesh%numCells))
    TetMesh%NB_Point = OctMesh%numNodes
    TetMesh%Posit(:,1:TetMesh%NB_Point) = OctMesh%Posit(:,1:TetMesh%NB_Point)

    CALL PtADTree_SetTree(Oct_PtADTree, OctMesh%Posit, OctMesh%numNodes, OctMesh%TinyD)

    nel = 0
    DO k=1,OctMesh%kmax
       DO j=1,OctMesh%jmax
          DO i=1,OctMesh%imax
             pOct_Cell => OctMesh%Cells(i,j,k)
             CALL OctreeMesh_CellTetSpliting(pOct_Cell,OctMesh, TetMesh)
             IF(PRESENT(nTet))THEN
                nel = nel + 1
                nTet(nel) = TetMesh%NB_Tet
             ENDIF
          ENDDO
       ENDDO
    ENDDO

    CALL PtADTree_Clear(Oct_PtADTree)
    RETURN
  END SUBROUTINE OctreeMesh_getTetMesh_old

  !>
  !!     Split all cells into Hybrid-Mesh
  !!     @param[in]  OctMesh
  !!     @param[in]  isTet
  !!               if isTet(i)=1, leaf-cell i will splitted by tetrahedra only.
  !!               otherwise, leaf-cell i will splitted by hybrid-cells.
  !!               If isTet is NOT presented, then split all cells into tetrahedra.
  !!     @param[out]   HybridMesh
  !<
  SUBROUTINE OctreeMesh_getHybridMesh(OctMesh,HybridMesh, isTet)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    TYPE (NodeNetTreeType) :: QuadTree
    TYPE (HybridMeshStorageType) :: HybridMesh
    INTEGER, INTENT(IN), OPTIONAL :: isTet(*)
    INTEGER, DIMENSION(:,:), POINTER :: IP_Face
    INTEGER, DIMENSION(  :), POINTER :: counter_Quad
    LOGICAL, DIMENSION(  :), POINTER :: noHang
    INTEGER :: numFace, numQuad, i,j,k, nel, level, iQuad, keep, idnext
    INTEGER :: ip(8), ip4(4), ipe(4), ip2(2), ipHang(18)
    LOGICAL :: triOnly

    ALLOCATE(IP_Face(4,    18*OctMesh%numCells))
    ALLOCATE(counter_Quad(0:4*OctMesh%numCells))
    ALLOCATE(noHang(         OctMesh%numCells))
    counter_Quad(0) = 0

    CALL NodeNetTree_Allocate(2, OctMesh%numNodes, 4*OctMesh%numCells, QuadTree)
    CALL OctreeMesh_getLeafCells(OctMesh)

    !--- split non-hang quads
    numFace  = 0
    numQuad = 0
    DO nel = 1, OctMesh%numCells
       pOct_Cell => OctMesh%LeafCells(nel)%to
       Level     = pOct_Cell%Level
       ip(1:8)   = pOct_Cell%Nodes(1:8)

       CALL OctreeMesh_getCellHangNodes(pOct_Cell,OctMesh,ipHang(1:18))

       noHang(nel) = .TRUE.
       DO i=1,18
          IF(ipHang(i)>0) noHang(nel) = .FALSE.
       ENDDO

       DO i=1,6
          p2Oct_Cell => OctreeMesh_NextCell(pOct_Cell,i)
          IF(p2Oct_Cell%Level>Level) CYCLE    !--- a hanging face

          ip4(1:4) = ip(iQuadFD_Hex(:,i))
          ip2(:)   = (/ip4(1), ip4(3)/)
          CALL NodeNetTree_SearchAdd(2,ip2,QuadTree,iQuad)
          IF(iQuad>numQuad)THEN
             !--- new face
             numQuad = iQuad
             ipe(1:4) = ipHang(jEdge_QuadFD_Hex(:,i))

             triOnly = .TRUE.
             IF(PRESENT(isTet))THEN
                IF(isTet(nel)/=1)THEN
                   idnext = p2Oct_Cell%ID
                   IF(idNext<=0)THEN
                      triOnly = .FALSE.
                   ELSE
                      IF(isTet(idnext)/=1) triOnly = .FALSE.
                   ENDIF
                ENDIF
             ENDIF
             CALL Quad_spliting(ip4, ipe, numFace, IP_Face, triOnly)
             counter_Quad(iQuad) = numFace
             IF(numQuad>4*OctMesh%numCells .OR. numFace>18*OctMesh%numCells)THEN
                WRITE(*,*)'Error--- numQuad,numFace,OctMesh%numCells=',   &
                     numQuad,numFace,OctMesh%numCells
                CALL Error_STOP ( '--- OctreeMesh_getHybridMesh')
             ENDIF
          ENDIF
       ENDDO

    ENDDO

    !--- collect cells

    HybridMesh%NB_Tet = 0
    HybridMesh%NB_Pyr = 0
    HybridMesh%NB_Hex = 0
    HybridMesh%NB_Point = 0
    ALLOCATE(HybridMesh%IP_Tet(4,2*numFace))
    ALLOCATE(HybridMesh%IP_Pyr(5,2*numFace))
    ALLOCATE(HybridMesh%IP_Hex(8,OctMesh%numCells))
    ALLOCATE(HybridMesh%Posit(3,OctMesh%numNodes + OctMesh%numCells))
    HybridMesh%NB_Point = OctMesh%numNodes
    HybridMesh%Posit(:,1:HybridMesh%NB_Point) = OctMesh%Posit(:,1:HybridMesh%NB_Point)

    DO nel = 1, OctMesh%numCells
       pOct_Cell => OctMesh%LeafCells(nel)%to
       Level     = pOct_Cell%Level
       ip(1:8)   = pOct_Cell%Nodes(1:8)

       !--- check if no hang point
       IF(noHang(nel))THEN
          IF(.NOT. PRESENT(isTet))THEN
             keep = 1
          ELSE
             IF(isTet(nel)==1)THEN
                keep = 1
             ELSE
                keep = 2
                DO i=1,6
                   p2Oct_Cell => OctreeMesh_NextCell(pOct_Cell,i)
                   idnext = p2Oct_Cell%ID
                   IF(idnext<=0) CYCLE
                   IF(isTet(idnext)==1) keep = 0
                ENDDO
             ENDIF
          ENDIF

          IF(keep==1)THEN
             DO i=1,6
                HybridMesh%NB_Tet = HybridMesh%NB_Tet+1
                HybridMesh%IP_Tet(:,HybridMesh%NB_Tet) = ip(iTet_Hex(:,i))
             ENDDO
             CYCLE
          ELSE IF(Keep==2)THEN
             HybridMesh%NB_Hex = HybridMesh%NB_Hex+1
             HybridMesh%IP_Hex(:,HybridMesh%NB_Hex) = ip(1:8)
             CYCLE
          ENDIF
       ENDIF

       HybridMesh%NB_Point = HybridMesh%NB_Point + 1
       HybridMesh%Posit(:,HybridMesh%NB_Point) =      &
            ( OctMesh%Posit(:,ip(1)) + OctMesh%Posit(:,ip(7)) )/2.D0

       DO i=1,6

          ip4(1:4) = ip(iQuadFD_Hex(:,i))
          ip2(:) = (/ip4(1), ip4(3)/)
          CALL NodeNetTree_Search(2,ip2,QuadTree,iQuad)
          IF(iQuad>0)THEN
             !--- not hang face
             DO k = counter_Quad(iQuad-1)+1, counter_Quad(iQuad)
                IF(IP_Face(4,k)==0)THEN
                   HybridMesh%NB_Tet = HybridMesh%NB_Tet + 1
                   IF(MOD(i,2)==1)THEN
                      HybridMesh%IP_Tet(1:3,HybridMesh%NB_Tet) = IP_Face(1:3,k)
                   ELSE
                      HybridMesh%IP_Tet(1:3,HybridMesh%NB_Tet) =   &
                           (/IP_Face(1,k),IP_Face(3,k),IP_Face(2,k)/)
                   ENDIF
                   HybridMesh%IP_Tet(4,HybridMesh%NB_Tet) = HybridMesh%NB_Point 
                ELSE
                   HybridMesh%NB_Pyr = HybridMesh%NB_Pyr + 1
                   IF(MOD(i,2)==1)THEN
                      HybridMesh%IP_Pyr(1:4,HybridMesh%NB_Pyr) = IP_Face(1:4,k)
                   ELSE
                      HybridMesh%IP_Pyr(1:4,HybridMesh%NB_Pyr) =   &
                           (/IP_Face(1,k),IP_Face(4,k),IP_Face(3,k),IP_Face(2,k)/)
                   ENDIF
                   HybridMesh%IP_Pyr(5,HybridMesh%NB_Pyr) = HybridMesh%NB_Point 

                ENDIF
             ENDDO
          ELSE
             !--- hang face

             p2Oct_Cell => OctreeMesh_NextCell(pOct_Cell,i)
             IF(p2Oct_Cell%Level<=Level) CALL Error_STOP ( 'wrong level    ')
             SELECT CASE(i)
             CASE(1)
                ipHang(5) = p2Oct_Cell      %Nodes(7)
                ipHang(4) = p2Oct_Cell      %Nodes(6)
                ipHang(1) = p2Oct_Cell      %Nodes(3)
                ipHang(3) = p2Oct_Cell%Up   %Nodes(7)
                ipHang(2) = p2Oct_Cell%Back %Nodes(7)
             CASE(2)
                ipHang(5) = p2Oct_Cell      %Nodes(1)
                ipHang(2) = p2Oct_Cell      %Nodes(4)
                ipHang(3) = p2Oct_Cell      %Nodes(5)
                ipHang(4) = p2Oct_Cell%Front%Nodes(1)
                ipHang(1) = p2Oct_Cell%Down %Nodes(1)
             CASE(3)
                ipHang(5) = p2Oct_Cell      %Nodes(7)
                ipHang(1) = p2Oct_Cell      %Nodes(8)
                ipHang(4) = p2Oct_Cell      %Nodes(3)
                ipHang(2) = p2Oct_Cell%Up   %Nodes(7)
                ipHang(3) = p2Oct_Cell%Right%Nodes(7)
             CASE(4)
                ipHang(5) = p2Oct_Cell      %Nodes(1)
                ipHang(2) = p2Oct_Cell      %Nodes(2)
                ipHang(3) = p2Oct_Cell      %Nodes(5)
                ipHang(4) = p2Oct_Cell%Left %Nodes(1)
                ipHang(1) = p2Oct_Cell%Down %Nodes(1)
             CASE(5)
                ipHang(5) = p2Oct_Cell      %Nodes(7)
                ipHang(4) = p2Oct_Cell      %Nodes(8)
                ipHang(1) = p2Oct_Cell      %Nodes(6)
                ipHang(3) = p2Oct_Cell%Back %Nodes(7)
                ipHang(2) = p2Oct_Cell%Right%Nodes(7)
             CASE(6)
                ipHang(5) = p2Oct_Cell      %Nodes(1)
                ipHang(2) = p2Oct_Cell      %Nodes(2)
                ipHang(3) = p2Oct_Cell      %Nodes(4)
                ipHang(4) = p2Oct_Cell%Left %Nodes(1)
                ipHang(1) = p2Oct_Cell%Front%Nodes(1)
             END SELECT

             DO j=1,4
                SELECT CASE(j)
                CASE(1)
                   ip2 = (/ip4(1),ipHang(5)/)
                CASE(2)
                   ip2 = (/ipHang(1),ipHang(2)/)
                CASE(3)
                   ip2 = (/ipHang(5),ip4(3)/)
                CASE(4)
                   ip2 = (/ipHang(4),ipHang(3)/)
                END SELECT

                CALL NodeNetTree_Search(2,ip2,QuadTree,iQuad)
                IF(iQuad==0) CALL Error_STOP ( 'hang face not set?')
                DO k = counter_Quad(iQuad-1)+1, counter_Quad(iQuad)
                   IF(IP_Face(4,k)==0)THEN
                      HybridMesh%NB_Tet = HybridMesh%NB_Tet + 1
                      IF(MOD(i,2)==1)THEN
                         HybridMesh%IP_Tet(1:3,HybridMesh%NB_Tet) = IP_Face(1:3,k)
                      ELSE
                         HybridMesh%IP_Tet(1:3,HybridMesh%NB_Tet) =   &
                              (/IP_Face(1,k),IP_Face(3,k),IP_Face(2,k)/)
                      ENDIF
                      HybridMesh%IP_Tet(4,HybridMesh%NB_Tet) = HybridMesh%NB_Point 
                   ELSE
                      HybridMesh%NB_Pyr = HybridMesh%NB_Pyr + 1
                      IF(MOD(i,2)==1)THEN
                         HybridMesh%IP_Pyr(1:4,HybridMesh%NB_Pyr) = IP_Face(1:4,k)
                      ELSE
                         HybridMesh%IP_Pyr(1:4,HybridMesh%NB_Pyr) =   &
                              (/IP_Face(1,k),IP_Face(4,k),IP_Face(3,k),IP_Face(2,k)/)
                      ENDIF
                      HybridMesh%IP_Pyr(5,HybridMesh%NB_Pyr) = HybridMesh%NB_Point 

                   ENDIF
                ENDDO
             ENDDO
          ENDIF

       ENDDO

    ENDDO

    DEALLOCATE(IP_Face, counter_Quad, noHang)
    CALL NodeNetTree_Clear(QuadTree)
    CALL OctreeMesh_getLeafCells(OctMesh)

    RETURN
  END SUBROUTINE OctreeMesh_getHybridMesh




END MODULE OctreeMeshManager


