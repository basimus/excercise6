!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


MODULE CornerLoop
  USE array_allocator
  IMPLICIT NONE

  !>
  !!   Type of a connection loop.
  !!   Here is a sample for nodeID and next:   \image html cnect.jpg
  !<
  TYPE CornerLoopType
     INTEGER :: numCorners = 0
     INTEGER :: numLoops   = 0
     INTEGER, DIMENSION(:), POINTER :: nodeID => null()
     INTEGER, DIMENSION(:), POINTER :: next => null()    !--- the next corner.
     INTEGER, DIMENSION(:), POINTER :: prev => null()    !--- the previous corner.
     INTEGER, DIMENSION(:), POINTER :: LoopEnd => null() !--- the end corner of each loop.
  END TYPE CornerLoopType

CONTAINS


  !>  Allocate memory.
  SUBROUTINE allc_CornerLoop(Cloop, NC)
    IMPLICIT NONE
    TYPE(CornerLoopType), INTENT(inout)  ::  Cloop
    INTEGER, INTENT(in)  ::  NC
    CALL allc_1Dpointer(Cloop%nodeID, NC)
    CALL allc_1Dpointer(Cloop%next,   NC)
    CALL allc_1Dpointer(Cloop%prev,   NC)
  END SUBROUTINE allc_CornerLoop


  !>                                                                     
  !!  Set prev accoding to next.
  !<                                             
  SUBROUTINE CornerLoop_setPrev(Cloop)
    IMPLICIT NONE
    TYPE(CornerLoopType), INTENT(in)  ::  Cloop
    INTEGER :: ip1, ip2
    CALL allc_1Dpointer(Cloop%prev,  keyLength(Cloop%nodeID))
    DO ip1 = 1, Cloop%numCorners
       ip2 = Cloop%next(ip1)
       Cloop%prev(ip2) = ip1
    ENDDO
  END SUBROUTINE CornerLoop_setPrev

  !>                                                                     
  !!   Sort the corners by loops, count the numLoops and set LoopEnd for each loop.
  !<                                             
  SUBROUTINE CornerLoop_LoopSort(Cloop, old_to_new)
    IMPLICIT NONE
    TYPE(CornerLoopType), INTENT(inout)  :: Cloop
    INTEGER, DIMENSION(:),  OPTIONAL     :: old_to_new
    INTEGER, DIMENSION(Cloop%numCorners) :: mark
    INTEGER :: ic, ic0, newc, i

    CALL allc_1Dpointer(Cloop%LoopEnd, Cloop%numCorners+1)

    mark(:) = 0
    Cloop%numLoops = 0
    newc = 0
    DO
       DO ic = 1, Cloop%numCorners
          IF(mark(ic)==0) EXIT
       ENDDO
       IF(ic>Cloop%numCorners) EXIT
       
       newc           = newc + 1
       Cloop%numLoops = Cloop%numLoops + 1
       ic0            = ic
       mark(ic)       = newc
       Cloop%LoopEnd(Cloop%numLoops) = newc
       DO
          ic = Cloop%Next(ic)
          IF(ic==ic0) EXIT
          newc = newc + 1
          mark(ic) = newc
       ENDDO
    ENDDO
    Cloop%LoopEnd(Cloop%numLoops+1) = newc + 1
    
    IF(newc/=Cloop%numCorners) STOP 'CornerLoop_LoopDivide: broken loop'
    Cloop%NodeID(mark(1:newc)) = Cloop%NodeID(1:newc)
    DO ic = 1, Cloop%numCorners
       Cloop%Next(ic) = ic + 1        
    ENDDO
    DO i = 1, Cloop%numLoops
       ic = Cloop%LoopEnd(i+1) -1
       Cloop%Next(ic) = Cloop%LoopEnd(i)
    ENDDO
    
    CALL CornerLoop_setPrev(Cloop)
    IF(present(old_to_new)) old_to_new(1:newc) = mark(1:newc)
    
  END SUBROUTINE CornerLoop_LoopSort

  !>                                                                     
  !!   connect two corners of a loop.  
  !<                                             
  SUBROUTINE CornerLoop_Bridge(Cloop, IC1, IC2 )
    IMPLICIT NONE
    TYPE(CornerLoopType), INTENT(inout)  ::  Cloop
    INTEGER, INTENT(IN) :: IC1, IC2
    INTEGER :: IC1p, IC1n, IC2p, IC2n  
    IC1p = Cloop%prev(IC1)
    IC1n = Cloop%next(IC1)
    IC2p = Cloop%prev(IC2)
    IC2n = Cloop%next(IC2)     
    Cloop%next(IC1) = IC2
    Cloop%prev(IC2) = IC1     
    Cloop%nodeID(Cloop%numCorners+1) = Cloop%nodeID(IC1)
    Cloop%prev(  Cloop%numCorners+1) = Cloop%numCorners+2
    Cloop%next(  Cloop%numCorners+1) = IC1n
    Cloop%nodeID(Cloop%numCorners+2) = Cloop%nodeID(IC2)
    Cloop%prev(  Cloop%numCorners+2) = IC2p
    Cloop%next(  Cloop%numCorners+2) = Cloop%numCorners+1
    Cloop%prev(IC1n) = Cloop%numCorners + 1
    Cloop%next(IC2p) = Cloop%numCorners + 2     
    Cloop%numCorners = Cloop%numCorners + 2
  END SUBROUTINE CornerLoop_Bridge

  SUBROUTINE CornerLoop_Check(Cloop)
    IMPLICIT NONE
    TYPE(CornerLoopType), INTENT(in)  ::  Cloop
    INTEGER :: ip1, ip2
    DO ip1 = 1, Cloop%numCorners
       ip2 = Cloop%next(ip1)
       IF(Cloop%prev(ip2)/=ip1) STOP ' CornerLoop_Check:: prev wrong'
    ENDDO
  END SUBROUTINE CornerLoop_Check

  SUBROUTINE CornerLoop_Clear(Cloop)
    IMPLICIT NONE
    TYPE(CornerLoopType), INTENT(inout)  ::  Cloop
    Cloop%numCorners = 0
    Cloop%numLoops   = 0
    IF(ASSOCIATED(Cloop%nodeID))  DEALLOCATE(Cloop%nodeID)
    IF(ASSOCIATED(Cloop%next))    DEALLOCATE(Cloop%next)
    IF(ASSOCIATED(Cloop%prev))    DEALLOCATE(Cloop%prev)   
    IF(ASSOCIATED(Cloop%LoopEnd)) DEALLOCATE(Cloop%LoopEnd)
  END SUBROUTINE CornerLoop_Clear

  !>
  !! Draw a CornerLoop by a channel.
  !! @param[in]  Pts      the supporting points of a Corner-Loop
  !! @param[in]  Cloop    the Corner-Loop.
  !! @param[in]  IO       the IO channel. 
  !<
  SUBROUTINE CornerLoop_Display(Cloop, Pts, IO)
    IMPLICIT NONE
    TYPE(CornerLoopType), INTENT(IN)    :: Cloop
    REAL*8,  DIMENSION(:,:), INTENT(IN) :: Pts
    INTEGER, INTENT(IN) ::  IO
    INTEGER :: i
    WRITE(IO,*)'#   numLoops=', cLoop%numLoops
    IF(cLoop%numLoops>0)THEN
       WRITE(IO,*)'#    Ends=', cLoop%LoopEnd(1:cLoop%numLoops)
    ENDIF
    WRITE(IO,*)' '
    WRITE(IO,*)'#   numCorners=', cLoop%numCorners
    DO i = 1, cLoop%numCorners
       WRITE(IO,*)REAL(Pts(:,cLoop%nodeID(i))),cLoop%nodeID(i),i
       WRITE(IO,*)REAL(Pts(:,cLoop%nodeID(cLoop%next(i)))),cLoop%nodeID(cLoop%next(i)),cLoop%next(i)
       WRITE(IO,*)' '
       WRITE(IO,*)' '
    ENDDO
    WRITE(IO,*)' '
    WRITE(IO,*)' '
  END SUBROUTINE CornerLoop_Display


END MODULE CornerLoop


