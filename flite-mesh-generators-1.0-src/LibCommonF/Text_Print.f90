!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!==============================================================================
!>
!!     Text aligned output.
!!     @param[in]    L   : the maximum characters each line (<=256).
!!     @param[in] string : the character string to be printed on screen.
!!
!!     Special characters in the string:
!!            \\n   : a new line.
!<
!==============================================================================
SUBROUTINE Text_Aligned_PRINT(L,string)

  IMPLICIT NONE

  INTEGER, INTENT(in) :: L
  CHARACTER*(*), INTENT(in) :: string
  INTEGER :: ll, ip, jp, i
  CHARACTER*(256) :: line
  CHARACTER :: SP = ' '

  do i=1,L
     line(i:i) = SP
  enddo
  ll=LEN(string)
  ip=1
  jp=1

  DO WHILE(ip<=ll)

     jp=1
     DO WHILE(ip<=ll .AND. jp<=L)
        IF(string(ip:ip)=='\' .AND. ip<ll)THEN
           IF(string(ip+1:ip+1)=='n')THEN
              ip = ip+2
              EXIT
           ENDIF
        ENDIF
        line(jp:jp)=string(ip:ip)
        ip=ip+1
        jp=jp+1
     ENDDO

     !--- do not allow word wrapping.
     IF(jp==L+1 .AND. ip<=ll     &
          .AND. string(ip:ip)/=' ' .AND. string(ip:ip)/='\')THEN
        DO WHILE(jp>1 .AND. line(jp-1:jp-1)/=' ')
           ip=ip-1
           jp=jp-1
           line(jp:jp)=SP
        ENDDO
        IF(jp==1)THEN
           WRITE(*,*)'Error in output--- too long a word'
           RETURN
        ENDIF
     ENDIF

     WRITE(*,100)line

     DO WHILE(jp==L+1 .AND. ip<=ll .AND. string(ip:ip)==' ')
        ip=ip+1
     ENDDO

     do i=1,L
        line(i:i) = SP
     enddo

  ENDDO


100 FORMAT(' |  ',a,'  |')
  RETURN
END SUBROUTINE Text_Aligned_PRINT

!==============================================================================
!>                                                                      
!!     Print help comment for any parameter input.
!!     @param[in] TypeMessage :: key work message, 
!!                               such as 'Input the number of boys:'.
!!     @param[in] HelpMessage :: Help message shown on screen if you input a '?'.
!!                               It may give a detail explanation for the parameter.
!!     @param[out] HelpIndex  :: IO channel for reading the parameter.
!!                               i.e. using read(HelpIndex,*) to replace read(*,*)
!!                               after call this subroutine.
!<                                                                      
!==============================================================================
SUBROUTINE Text_HelpComment(HelpIndex,TypeMessage, HelpMessage)

  IMPLICIT NONE

  CHARACTER*(*), INTENT(in) :: TypeMessage, HelpMessage
  INTEGER, INTENT(out) :: HelpIndex
  INTEGER, PARAMETER :: Line_Length = 72
  CHARACTER*256 :: ReadString
  INTEGER     :: i,j

10 CONTINUE
  WRITE(*,'(a,$)')TypeMessage//' <typing a ? for help>: '

  READ(*,'(A256)')ReadString

  DO i=1,256
     IF(ReadString(i:i)=='?')THEN

        WRITE(*,*)('_',j=1,Line_Length+6)
        CALL Text_Aligned_PRINT(Line_Length,' ')
        CALL Text_Aligned_PRINT(Line_Length,HelpMessage)
        WRITE(*,*)'|__',('_',j=1,Line_Length),'__|'
        WRITE(*,*)' '

        GOTO 10

     ENDIF
  ENDDO

  HelpIndex = 59

  OPEN(HelpIndex,file='TempHelpXIE_9999.tmp')
  REWIND(HelpIndex)
  WRITE(HelpIndex,'(a)')ReadString
  CALL flush (HelpIndex)
  CLOSE (HelpIndex)

  OPEN(HelpIndex,file='TempHelpXIE_9999.tmp',status='old')


  RETURN
END SUBROUTINE Text_HelpComment


!==============================================================================
!>
!!   Print help comment for any filename input.  
!!   Search files with a given postfix from the current directory.
!!   List them on screen.
!!     @param[in] TypeMessage :: key work message, 
!!                               such as 'Input the filename:'.
!!     @param[in]  Postfix : the postfix of the filename
!!     @param[out] HelpIndex  :: IO channel for reading the filename.
!!                               i.e. using read(HelpIndex,*) to replace read(*,*)
!!                               after call this subroutine.
!<
!==============================================================================
SUBROUTINE Text_FileHelp(ReadString,TypeMessage, Postfix)

  IMPLICIT NONE

  CHARACTER*(*), INTENT(in) :: TypeMessage, Postfix
  CHARACTER*72  :: cmd1, cmd2
  INTEGER, PARAMETER :: Line_Length = 72
  CHARACTER(LEN=*) :: ReadString
  CHARACTER*256 :: HelpMessage
  INTEGER     :: i,j
  
            
10 CONTINUE
  WRITE(*,'(a,$)')TypeMessage//' <typing a ? for help>: '

  READ(*,'(A)')ReadString

  DO i=1,256
     IF(ReadString(i:i)=='?')THEN

        WRITE(*,*)('_',j=1,Line_Length+6)
        CALL Text_Aligned_PRINT(Line_Length,' ')
        
            cmd2 = 'ls > TempHelpFileList.tmp'
            call system(cmd2)
            HelpMessage = '  Type the name (prefix only) please. \n'    &
            //   '  Under the current directory, these files end with: '//trim(Postfix)
            CALL Text_Aligned_PRINT(Line_Length,  HelpMessage)
            CALL Text_SearchFile(Postfix)
            cmd1 = 'rm -f TempHelpFileList.tmp'
            call system(cmd1)

        WRITE(*,*)'|__',('_',j=1,Line_Length),'__|'
        WRITE(*,*)' '

        GOTO 10

     ENDIF
  ENDDO


  RETURN
END SUBROUTINE Text_FileHelp

!==============================================================================
!>
!!  Search files with a given postfix from the current directory.
!!  List them on screen.
!!  @param[in]  Postfix : the postfix
!<
!==============================================================================
SUBROUTINE Text_SearchFile(Postfix)

  IMPLICIT NONE
  CHARACTER*(*), INTENT(in) :: Postfix
  CHARACTER*500 :: ffile, flist
  INTEGER, PARAMETER :: Line_Length = 72
  INTEGER  ::  ip,jp,ll

  OPEN(61,file='TempHelpFileList.tmp')
  jp=0
  ll=LEN(Postfix)
  DO WHILE(1==1)
     READ(61,*,END=100)ffile
     DO ip=1,200
        IF(ffile(ip+1:ip+ll) == Postfix)THEN
           flist(jp+1:jp+ip+2)=ffile(1:ip)//'  '
           jp=jp+ip+2
           GOTO 10
        ENDIF
     ENDDO
10   CONTINUE        
  ENDDO
100 CONTINUE
  CLOSE(61)

  IF(jp>0)THEN
     CALL Text_Aligned_PRINT(Line_Length,flist(1:jp))
  ELSE
     CALL Text_Aligned_PRINT(Line_Length,'Sorry, no file found.')
  ENDIF

  RETURN
END SUBROUTINE Text_SearchFile

!>   Error and Stop
SUBROUTINE Error_Stop(string)
  CHARACTER*(*), INTENT(in) :: string
  WRITE(110,'(a)') 'Error Stop::'
  WRITE(110,'(a)') string
  WRITE(*,*) string
  WRITE(*,*) '======== Error Stop ========'
  WRITE(*,*) ' '
  STOP
end SUBROUTINE Error_Stop

!>   Stop
SUBROUTINE WellDone_Stop()
  close(110, status='delete') 
  STOP
end SUBROUTINE WellDone_Stop

!>   Start
SUBROUTINE Happy_Start(JobName)
 CHARACTER*(*), INTENT(in) ::  JobName
 CHARACTER(LEN=256) :: path 
 CHARACTER(LEN=256) :: ErrorFile = ".SwanMeshError.log" 
 INTEGER :: k, i
        path = ADJUSTL(JobName)
        k = 0
        DO i = 1, LEN_TRIM( path )
           IF(path(i:i)=='/') k = i
        ENDDO
        IF(k/=0)THEN
           ErrorFile = path(1:k) // ErrorFile
        ENDIF
        OPEN(110, file = ErrorFile)
        WRITE(110, '(a)') TRIM(path)
end SUBROUTINE Happy_Start

