!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!  Hybrid mesh storage, including the defination of type and the basic functions.
!<
MODULE SurfaceMeshHighOrder
  USE SurfaceMeshManager
  USE HighOrderCellManager
  IMPLICIT NONE

CONTAINS

  !>
  !!  upgrade the gridorder. 
  !!  @param[in,out] Surf  the hybrid mesh.
  !<
  SUBROUTINE Surf_getHighOrder(GridOrder, Surf)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: GridOrder
    TYPE(SurfaceMeshStorageType),  INTENT(INOUT) :: Surf
    INTEGER :: ND

    CALL HighOrderCell_Clear (Surf%Cell)
    Surf%Cell      = HighOrderCell_getHighCell(GridOrder,2,0)
    Surf%GridOrder = Surf%Cell%GridOrder

    ND = Surf%Cell%numCellNodes
    CALL allc_int_2Dpointer(Surf%IPsp_Tri, ND, Surf%NB_Tri)

    Surf%IPsp_Tri(1:3, 1:Surf%NB_Tri) = Surf%IP_Tri(1:3, 1:Surf%NB_Tri)
    Surf%IPsp_Tri(4:ND,1:Surf%NB_Tri) = 0

    CALL Surf_LinearFill(Surf, 0)

  END SUBROUTINE Surf_getHighOrder


  !>
  !!  fill all cell nodes by linear interpolation. 
  !!  @param[in,out] Surf  the hybrid mesh.
  !!  @param[in]  force  =0  don't change the position of exist high-nodes.
  !!                     =1  change the position of exist high-nodes.
  !<
  SUBROUTINE Surf_LinearFill(Surf, force)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(IN) :: force
    INTEGER :: ND, NPT, NPF, IT, IP, ITL, ITR, IL, IR, IB, i, j, k, NP
    INTEGER, DIMENSION(:), POINTER :: IPL, IPR
    INTEGER, DIMENSION(:), POINTER :: indx
    REAL*8,  DIMENSION(:,:), POINTER :: pv, pt

    ND  = Surf%Cell%numCellNodes
    NPT = Surf%Cell%numVertices
    NPF = Surf%Cell%numFaceNodes

    !--- fill high-order nodes.
    ALLOCATE(pv(2,NPT), pt(2,ND))
    NP = Surf%NB_Point
    DO IT = 1, Surf%NB_Tri
       pv(:, 1:NPT) = Surf%Coord(:, Surf%IPsp_Tri(1:NPT,IT) )
       CALL HighOrderCell_linearIsoparTransf( Surf%Cell,pv, pt )
       DO i = NPT+1, ND
          IF(Surf%IPsp_Tri(i,IT)==0) THEN
             NP  = NP + 1
             IF(NP>keyLength(Surf%Coord))THEN
                CALL allc_2Dpointer(Surf%Coord,2, 1+2*NP*2)
             ENDIF
             Surf%IPsp_Tri(i,IT) = NP
             Surf%Coord(:,NP) = Pt(:,i)
          ELSE IF(force==1)THEN
             Surf%Coord(:,Surf%IPsp_Tri(i,IT)) = Pt(:,i)
          ENDIF
       ENDDO
    ENDDO

    Surf%NB_Point = NP

    !--- remove those repeated nodes

    CALL Surf_BuildNext(Surf)
    ALLOCATE(IPL(NPF), IPR(NPF), indx(NPF))
    indx(1:2) = (/2,1/)
    DO i = 3, NPF
       indx(i) = NPF+3-i
    ENDDO

    DO ITL = 1, Surf%NB_Tri
       DO IL = 1,3
          ITR = Surf%Next_Tri(IL,ITL)
          IF(ITR<ITL) CYCLE
          IR = which_NodeinTri(ITL, Surf%Next_Tri(:,ITR))
          IF(IR==0) STOP 'vhuhipmiojftyft7'
          ipL(:) = Surf%IPsp_Tri(Surf%Cell%faceNodes(:,IL),ITL)
          ipR(:) = Surf%IPsp_Tri(Surf%Cell%faceNodes(:,IR),ITR)
          IF(ipL(1)/=ipR(2) .OR. ipL(2)/=ipR(1)) STOP 'fjwoirfn2490tf349we'
          DO i = 3, NPF
             j = indx(i)
             IF(ipL(j) > ipR(i))THEN
                ipL(j) = ipR(i)
             ELSE IF(ipL(j) < ipR(i))THEN
                ipR(i) = ipL(j)
             ENDIF
          ENDDO
          Surf%IPsp_Tri(Surf%Cell%faceNodes(:,IL),ITL) = ipL(:)
          Surf%IPsp_Tri(Surf%Cell%faceNodes(:,IR),ITR) = ipR(:)
       ENDDO
    ENDDO

    CALL Surf_ClearNext(Surf)

    !--- clean the removed nodes
    CALL Surf_CleanNodes(Surf)

    DEALLOCATE(IPL, IPR, indx)
    DEALLOCATE(pv, pt)

  END SUBROUTINE Surf_LinearFill

  !>
  !!  Change the grid order and fill all cell nodes by interpolation. 
  !!  @param[in,out] Surf  the hybrid mesh.
  !!  @param[in]  NewOrder : the new grid-order
  !!
  !!  This subrotine is the higher version of the subroutine Surf_LinearFill().
  !<
  SUBROUTINE Surf_HigherFill(Surf, NewOrder)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(IN) :: NewOrder
    INTEGER :: ND, ND1, NPF, IT, IP, ITL, ITR, IL, IR, IB, i, j, k, NP
    INTEGER :: Nadd, icheck
    INTEGER, DIMENSION(:), POINTER :: IPL, IPR
    INTEGER, DIMENSION(:), POINTER :: indx
    REAL*8,  DIMENSION(:,:), POINTER :: pv2, pt2, pv3, pt3, sh
    INTEGER, DIMENSION(:,:), POINTER :: IPsp_Tri
    TYPE(HighOrderCellType)  :: Cell 

    IF(NewOrder==Surf%GridOrder) RETURN
    Cell      = HighOrderCell_getHighCell(NewOrder,2,0)

    ND1 = Surf%Cell%numCellNodes
    ND  = Cell%numCellNodes
    NPF = Cell%numFaceNodes
    Nadd = NewOrder / Surf%GridOrder + 1

    !--- shape function
    ALLOCATE(sh(ND1, ND))
    DO i = 1, ND
       CALL HighOrderCell_calcShapeFunctions (Surf%Cell,Cell%Coord(:,i), Sh(:,i))
    ENDDO

    Surf%GridOrder = NewOrder
    Surf%Cell      = HighOrderCell_getHighCell(NewOrder,2,0)

    icheck = 0
    IF(KeyLength(Surf%Coord)>=Surf%NB_Point)THEN
       icheck = icheck + 2
       CALL allc_2Dpointer(Surf%Coord, 2, Nadd*Nadd*Surf%NB_Point)
    ENDIF
    IF(KeyLength(Surf%Posit)>=Surf%NB_Point)THEN
       icheck = icheck + 3
       CALL allc_2Dpointer(Surf%Posit, 3, Nadd*Nadd*Surf%NB_Point)
    ENDIF

    ALLOCATE(IPsp_Tri(ND1,Surf%NB_Tri))
    IF(ND1>3)THEN
       IPsp_Tri(:, 1:Surf%NB_Tri) = Surf%IPsp_Tri(:, 1:Surf%NB_Tri)
       DEALLOCATE(Surf%IPsp_Tri)
    ELSE
       IPsp_Tri(1:3, 1:Surf%NB_Tri) = Surf%IP_Tri(1:3, 1:Surf%NB_Tri)
    ENDIF
    ALLOCATE(Surf%IPsp_Tri(ND,Surf%NB_Tri))


    !--- fill high-order nodes.
    ALLOCATE(pv2(2,ND1), pt2(2,ND), pv3(3,ND1), pt3(3,ND))
    NP = Surf%NB_Point
    DO IT = 1, Surf%NB_Tri
       Surf%IPsp_Tri(1:3,IT) = Surf%IP_Tri(1:3,IT)
       IF(icheck==2 .OR. icheck==5)THEN
          pv2(:, 1:ND1) = Surf%Coord(:, IPsp_Tri(1:ND1,IT) )
          Pt2 = MATMUL(Pv2, Sh)
       ENDIF
       IF(icheck==3 .OR. icheck==5)THEN
          pv3(:, 1:ND1) = Surf%Posit(:, IPsp_Tri(1:ND1,IT) )
          Pt3 = MATMUL(Pv3, Sh)
       ENDIF
       DO i = 4, ND
          NP  = NP + 1
          Surf%IPsp_Tri(i,IT) = NP
          IF(icheck==2 .OR. icheck==5)THEN
             Surf%Coord(:,NP) = Pt2(:,i)     
          ENDIF
          IF(icheck==3 .OR. icheck==5)THEN
             Surf%Posit(:,NP) = Pt3(:,i)     
          ENDIF
       ENDDO
    ENDDO

    Surf%NB_Point = NP

    !--- remove those repeated nodes

    CALL Surf_BuildNext(Surf)
    ALLOCATE(IPL(NPF), IPR(NPF), indx(NPF))
    indx(1:2) = (/2,1/)
    DO i = 3, NPF
       indx(i) = NPF+3-i
    ENDDO

    DO ITL = 1, Surf%NB_Tri
       DO IL = 1,3
          ITR = Surf%Next_Tri(IL,ITL)
          IF(ITR<ITL) CYCLE
          IR = which_NodeinTri(ITL, Surf%Next_Tri(:,ITR))
          IF(IR==0) STOP 'vfdkviofj0fe9deiojdi'
          ipL(:) = Surf%IPsp_Tri(Surf%Cell%faceNodes(:,IL),ITL)
          ipR(:) = Surf%IPsp_Tri(Surf%Cell%faceNodes(:,IR),ITR)
          IF(ipL(1)/=ipR(2) .OR. ipL(2)/=ipR(1)) STOP 'fjwoirfn2490tf349we'
          DO i = 3, NPF
             j = indx(i)
             IF(ipL(j) > ipR(i))THEN
                ipL(j) = ipR(i)
             ELSE IF(ipL(j) < ipR(i))THEN
                ipR(i) = ipL(j)
             ENDIF
          ENDDO
          Surf%IPsp_Tri(Surf%Cell%faceNodes(:,IL),ITL) = ipL(:)
          Surf%IPsp_Tri(Surf%Cell%faceNodes(:,IR),ITR) = ipR(:)
       ENDDO
    ENDDO

    CALL Surf_ClearNext(Surf)

    !--- clean the removed nodes
    CALL Surf_CleanNodes(Surf)

    DEALLOCATE(IPL, IPR, indx)
    DEALLOCATE(pv2, pt2, pv3, pt3, sh)
    DEALLOCATE(IPsp_Tri)

  END SUBROUTINE Surf_HigherFill

  !>
  !!  Split the mesh into liner triangles. 
  !!  @param[in,out] Surf  the hybrid mesh.
  !<
  SUBROUTINE Surf_LinerSplit(Surf)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER :: ND, NTT, Nold, IT, i, j, np
    REAL*8  :: xd
    INTEGER, DIMENSION(:,:), POINTER :: node
    INTEGER, DIMENSION(:),   POINTER :: indx
    TYPE(HighOrderCellType)  :: Cell 

    IF(Surf%GridOrder==1) RETURN

    !--- node order
    Cell = HighOrderCell_getHighCell(Surf%GridOrder,2,0)

    ND   = Surf%Cell%numCellNodes
    Nold = Surf%GridOrder

    ALLOCATE(node(0:Nold, 0:Nold))
    node = 0
    xd = 2.d0 / Nold
    DO np = 1, ND
       i = (1+Cell%Coord(1,np))/xd + 0.0001
       j = (1+Cell%Coord(2,np))/xd + 0.0001
       IF(node(i,j)>0) STOP '---cdsfwefweafj0943jfr'
       node(i,j) = np
    ENDDO
    DO j = 0, Nold
       DO i = 0, Nold - j
          IF(node(i,  j)==0) STOP '----cwifj2348f8jrgioe'
       ENDDO
    ENDDO


    ALLOCATE(indx(Surf%NB_Tri))
    indx(1:Surf%NB_Tri) = Surf%IP_Tri(5,1:Surf%NB_Tri)
    DEALLOCATE( Surf%IP_Tri)
    ALLOCATE(Surf%IP_Tri(5, Nold*Nold * Surf%NB_Tri))

    !--- split triangle 
    NTT = 0
    DO IT = 1, Surf%NB_Tri
       DO j = 0, Nold-1
          DO i = 0, Nold - j -1
             NTT = NTT + 1
             Surf%IP_Tri(4:5,NTT) = (/0, indx(IT)/)
             Surf%IP_Tri(1,  NTT) = Surf%IPsp_Tri(node(i,  j  ), IT)
             Surf%IP_Tri(2,  NTT) = Surf%IPsp_Tri(node(i+1,j  ), IT)
             Surf%IP_Tri(3,  NTT) = Surf%IPsp_Tri(node(i,  j+1), IT)
             IF(i==Surf%GridOrder - j -1) CYCLE
             NTT = NTT + 1
             Surf%IP_Tri(4:5,NTT) = (/0, indx(IT)/)
             Surf%IP_Tri(1,  NTT) = Surf%IPsp_Tri(node(i,  j+1), IT)
             Surf%IP_Tri(2,  NTT) = Surf%IPsp_Tri(node(i+1,j  ), IT)
             Surf%IP_Tri(3,  NTT) = Surf%IPsp_Tri(node(i+1,j+1), IT)
          ENDDO
       ENDDO
    ENDDO

    Surf%NB_Tri    = NTT
    Surf%GridOrder = 1
    Surf%Cell      = HighOrderCell_getHighCell(1,2,0)

    DEALLOCATE(node, indx)
    DEALLOCATE(Surf%IPsp_Tri)

  END SUBROUTINE Surf_LinerSplit

  !>
  !!  remove those nodes that are not associated with any elements.
  !!  @param[in,out] Surf  the mesh.
  !<
  SUBROUTINE Surf_CleanNodes(Surf, old_to_new)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(OUT), OPTIONAL :: old_to_new(*)
    INTEGER :: IP, NP, IT, I
    INTEGER, DIMENSION(:), POINTER :: markp

    ALLOCATE(markp(0:Surf%nb_point))
    markp(:) = 0
    IF(Surf%GridOrder==1)THEN
       DO IT = 1, Surf%NB_Tri
          markp(Surf%IP_Tri(1:3,IT)) = 1
       ENDDO
    ELSE
       DO IT = 1, Surf%NB_Tri
          markp(Surf%IPsp_Tri(:,IT)) = 1
       ENDDO
    ENDIF

    NP = 0
    DO IP = 1, Surf%NB_Point
       IF(markp(IP)==0) CYCLE
       NP = NP + 1
       markp(ip) = np
    ENDDO

    IF(keyLength(Surf%Coord)>=Surf%NB_Point)THEN
       DO IP = 1, Surf%NB_Point
          IF(markp(IP)==0) CYCLE
          Surf%Coord(:,markp(ip)) = Surf%Coord(:,IP)
       ENDDO
    ENDIF
    IF(keyLength(Surf%Posit)>=Surf%NB_Point)THEN
       DO IP = 1, Surf%NB_Point
          IF(markp(IP)==0) CYCLE
          Surf%Posit(:,markp(ip)) = Surf%Posit(:,IP)
       ENDDO
    ENDIF

    markp(0) = 0
    IF(PRESENT(old_to_new))THEN
       old_to_new(1:Surf%NB_Point) = markp(1:Surf%NB_Point)
    ENDIF

    Surf%NB_Point = NP

    DO IT = 1, Surf%NB_Tri
       Surf%IP_Tri(1:3,IT) = markp(Surf%IP_Tri(1:3,IT))
    ENDDO
    IF(Surf%GridOrder>1)THEN
       DO IT = 1, Surf%NB_Tri
          Surf%IPsp_Tri(:,IT) = markp(Surf%IPsp_Tri(:,IT))
       ENDDO
    ENDIF

    NP = 0
    DO I = 1, Surf%NB_DBC
       IP = Surf%IP_DBC(I)
       IF(markp(IP)==0) CYCLE
       NP = NP + 1
       Surf%IP_DBC(NP) = markp(IP)
       Surf%Pd_DBC(:,NP) = Surf%Pd_DBC(:,I)
    ENDDO
    Surf%NB_DBC = NP

    DEALLOCATE(markp)

  END SUBROUTINE Surf_CleanNodes

  !>
  !!  sort nodes so that the vertices take the first places and 
  !!     the last Surf.NB_Psp nodes are high-order nodes.
  !!  @param[in,out] Surf  the mesh.
  !<
  SUBROUTINE Surf_SortHighOrderNodes(Surf, old_to_new)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(INOUT) :: Surf
    INTEGER, INTENT(OUT), OPTIONAL :: old_to_new(*)
    INTEGER :: IP, NP, IT, I
    INTEGER, DIMENSION(:),   POINTER :: markp
    REAL*8,  DIMENSION(:,:), POINTER :: coord, posit

    ALLOCATE(markp(0:Surf%NB_Point))
    markp(:) = 0
    DO IT = 1, Surf%NB_Tri
       markp(Surf%IP_Tri(1:3,IT)) = 1
    ENDDO

    NP = 0
    DO IP = 1, Surf%NB_Point
       IF(markp(IP)==1)THEN
          NP = NP + 1
          markp(ip) = np
       ENDIF
    ENDDO
    Surf%NB_Psp = Surf%NB_Point - NP

    DO IP = 1, Surf%NB_Point
       IF(markp(IP)==0)THEN
          NP = NP + 1
          markp(ip) = np
       ENDIF
    ENDDO
    IF(NP/=Surf%NB_Point) STOP 'mdkl ki e0f9 vhfuju'

    ALLOCATE(coord(2,NP), posit(3,NP))
    IF(keyLength(Surf%Coord)>=NP)THEN
       coord(:,1:NP) = Surf%Coord(:,1:NP)
       DO IP = 1, NP
          Surf%Coord(:,markp(IP)) = Coord(:,IP)
       ENDDO
    ENDIF
    IF(keyLength(Surf%Posit)>=NP)THEN
       Posit(:,1:NP) = Surf%Posit(:,1:NP)
       DO IP = 1, NP
          Surf%Posit(:,markp(IP)) = Posit(:,IP)
       ENDDO
    ENDIF
    DEALLOCATE(coord, posit)

    markp(0) = 0
    IF(PRESENT(old_to_new))THEN
       old_to_new(1:NP) = markp(1:NP)
    ENDIF

    DO IT = 1, Surf%NB_Tri
       Surf%IP_Tri(1:3,IT) = markp(Surf%IP_Tri(1:3,IT))
       Surf%IPsp_Tri(:,IT) = markp(Surf%IPsp_Tri(:,IT))
    ENDDO

    DO I = 1, Surf%NB_DBC
       Surf%IP_DBC(I) = markp(Surf%IP_DBC(I))
    ENDDO

    DEALLOCATE(markp)

  END SUBROUTINE Surf_SortHighOrderNodes


  !>
  !! Draw a cell IT a channel by Surf.Coord.
  !! @param[in]  N        the number of points along on each side.
  !! @param[in]  IO       the IO channel. 
  !! @param[in]  inside   =0 no inside detail (boundary only).   \n
  !!                      =1 with inside detail
  !<
  SUBROUTINE Surf_CellDisplay(Surf, IT, N, IO, inside)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER, INTENT(IN) :: IT, N, IO, inside
    REAL*8,  DIMENSION(2,Surf%Cell%numCellNodes) :: Pts
    Pts(:,:) = Surf%Coord(:,Surf%IPsp_Tri(:,IT))
    CALL HighOrderCell_TriDisplay(Surf%Cell, Pts, N, IO, inside)
  END SUBROUTINE Surf_CellDisplay


  !>
  !! Draw a cell IT a channel by Surf.Posit.
  !! @param[in]  N        the number of points along on each side.
  !! @param[in]  IO       the IO channel. 
  !! @param[in]  inside   =0 no inside detail (boundary only).   \n
  !!                      =1 with inside detail
  !<
  SUBROUTINE Surf_CellDisplay3D(Surf, IT, N, IO, inside)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER, INTENT(IN) :: IT, N, IO, inside
    REAL*8,  DIMENSION(3,Surf%Cell%numCellNodes) :: Pts
    Pts(:,:) = Surf%Posit(:,Surf%IPsp_Tri(:,IT))
    CALL HighOrderCell_TriDisplay(Surf%Cell, Pts, N, IO, inside)
  END SUBROUTINE Surf_CellDisplay3D


  !>
  !!
  !!  @param[out] Isucc  = 1 succeed.
  !!                     < 0 fail, triangle IT=abs(Isucc) is with negative Jacobian.
  !<
  SUBROUTINE  Surf_linearSystemSolver(Surf, material, Disp, Isucc)
    USE MUMPS_MODULE
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    TYPE(MaterialPar), INTENT(IN) :: Material
    REAL*8, DIMENSION(Surf%Cell%Nsd, *), INTENT(OUT) :: Disp
    INTEGER, INTENT(OUT) :: Isucc
    INTEGER :: NP, NB_Point, NB_Cell, Nsd, nofDBC, Nfree, Msize
    INTEGER :: IT, i, k, n, m, ip, j
    INTEGER :: nOfValuesElem, nOfValuesElem2, nOfValues, nOfEquations
    INTEGER, DIMENSION(:), POINTER :: currentIndex, currentIndex2
    INTEGER, DIMENSION(:), POINTER :: IRN, JCN
    REAL*8,  DIMENSION(:), POINTER :: Aij, b, d
    INTEGER, DIMENSION(                Surf%Cell%numCellNodes) :: IPs
    INTEGER, DIMENSION(Surf%Cell%Nsd* Surf%Cell%numCellNodes) :: TeAll
    REAL*8,  DIMENSION(Surf%Cell%Nsd, Surf%Cell%numCellNodes) :: Pts
    REAL*8,  DIMENSION(Surf%Cell%Nsd* Surf%Cell%numCellNodes,    &
         Surf%Cell%Nsd* Surf%Cell%numCellNodes) :: eMat
    INTEGER, DIMENSION(:), POINTER :: markp
    INTEGER :: kmethod = 2

    NP       = Surf%Cell%numCellNodes
    NB_Point = Surf%NB_Point
    NB_Cell  = Surf%NB_Tri
    Nsd      = Surf%Cell%Nsd
    Isucc    = 1

    nOfValuesElem  = Nsd * NP
    nOfValuesElem2 = nOfValuesElem**2
    nOfValues      = nOfValuesElem2 * NB_Cell
    nOfEquations   = Nsd * NB_Point
    nofDBC         = Nsd * Surf%NB_DBC

    ! Allocate
    ALLOCATE(currentIndex (nOfValuesElem))
    ALLOCATE(currentIndex2(nOfValuesElem2))
    IF(kmethod==1)THEN
       Msize = nOfValues + 2*nofDBC
    ELSE
       Msize = nOfValues
    ENDIF
    ALLOCATE(IRN(Msize))
    ALLOCATE(JCN(Msize))
    ALLOCATE(Aij(Msize))

    currentIndex  = (/ (i, i=1,nOfValuesElem) /)
    currentIndex2 = (/ (i, i=1,nOfValuesElem2) /)
    IRN = 0
    JCN = 0
    Aij = 0.d0

    ! Loop on elements
    DO IT = 1, NB_Cell
       IPs(1:NP)   = Surf%IPsp_Tri(1:NP,IT)
       Pts(:,1:NP) = Surf%Coord(:,IPs(1:NP))
       CALL HighOrderCell_ElasticityMatrix2D(Surf%Cell, Pts, material, eMat, Isucc)
       IF(Isucc==0)THEN
          !--- negative Jacobian
          !--- solver fails.
          Isucc = -IT
          RETURN
       ENDIF

       TeAll(1:NP)        = IPs(1:NP)
       TeAll(NP+1 : 2*NP) = IPs(1:NP) + NB_Point

       DO i=1,nOfValuesElem
          IRN(currentIndex) = TeAll(i)
          JCN(currentIndex) = TeAll(:)
          currentIndex = currentIndex + nOfValuesElem
       ENDDO
       Aij(currentIndex2) = RESHAPE(eMat, (/nOfValuesElem2/))
       currentIndex2 = currentIndex2 + nOfValuesElem2
    ENDDO

    IF(kmethod==1)THEN
       !--- ruben's original code: expand the matrix to meet the boundary condition

       !--- symmetry reduce
       m = 0
       DO I = 1, nOfValues
          IF(IRN(I)>JCN(I)) CYCLE
          m = m + 1
          IRN(m) = IRN(I)
          JCN(m) = JCN(I)
          Aij(m) = Aij(I)
       ENDDO
       nOfValues = m

       ! disp = solveSystemElasticity(mesh, refElem, 
       ! Dirichlet boundary conditions via Lagrange multipliers

       ALLOCATE(b(nOfEquations + nofDBC))
       b(1:nOfEquations) = 0
       n = nOfEquations
       m = nOfValues
       DO k  = 1, Nsd
          DO i = 1, Surf%NB_DBC
             n = n + 1 
             m = m + 1 
             b(n)   = Surf%Pd_DBC(k,i)
             IRN(m) = Surf%IP_DBC(i) + (k-1) * NB_Point
             JCN(m) = n
             Aij(m) = 1
          ENDDO
       ENDDO

       ! Total system of equations to be solved
       nOfEquations = n
       nOfValues    = m

       ! Solve linear system
       CALL MUMPS_create(nOfEquations, nOfValues, IRN, JCN, Aij)

       ! Solve linear system

       CALL MUMPS_solver(nOfEquations, b)
       disp(1:Nsd, 1:NB_Point) = TRANSPOSE( RESHAPE( b, (/NB_Point, Nsd/)) )

    ELSE
       !--- remove known displacement (boundary condition) to the right

       Nfree = NB_Point - Surf%NB_DBC
       ALLOCATE(markp(NSD*NB_Point))
       ALLOCATE(d(nsd*Surf%NB_DBC))
       ALLOCATE(b(nsd*Nfree))

       markp(:) = 0
       DO i = 1, Surf%NB_DBC
          ip = Surf%IP_DBC(i)
          DO j = 1, nsd
             markp(ip+(j-1)*NB_Point) = -i - (j-1)*Surf%NB_DBC
             d(i+(j-1)*Surf%NB_DBC) =  Surf%PD_DBC(j,i)
          ENDDO
       ENDDO

       n = 0
       DO ip = 1, NB_Point
          IF(markp(ip)==0)THEN
             n = n + 1
             DO j = 1, nsd
                markp(ip+(j-1)*NB_Point) = n + (j-1)*Nfree
             ENDDO
          ENDIF
       ENDDO

       b(:) = 0
       m = 0
       DO k = 1, nOfValues
          i = IRN(k)
          j = JCN(k)
          IF(markp(i)<0) CYCLE
          IF(markp(j)<0) THEN
             b(markp(i)) = b(markp(i)) - Aij(k) * d(-markp(j))
          ELSE IF(i>=j)THEN
             m = m+1
             IRN(m) = markp(i)
             JCN(m) = markp(j)
             Aij(m) = Aij(k)
          ENDIF
       ENDDO

       ! Total system of equations to be solved
       nOfEquations = NSD*Nfree
       nOfValues    = m


       ! Solve linear system
       CALL MUMPS_create(nOfEquations, nOfValues, IRN, JCN, Aij)
       CALL MUMPS_solver(nOfEquations, b)

       DO ip = 1, Surf%NB_Point
          n = markp(ip)
          IF(n>0)THEN
             DO j = 1, nsd
                disp(j,ip) = b(n+(j-1)*Nfree)
             ENDDO
          ELSE
             DO j = 1, nsd
                disp(j,ip) = Surf%PD_DBC(j,-n)
             ENDDO
          ENDIF
       ENDDO

       DEALLOCATE(markp, d, b)

    ENDIF

    CALL MUMPS_destroy( )
    DEALLOCATE(currentIndex , currentIndex2)
    DEALLOCATE(IRN, JCN, Aij)

  END SUBROUTINE  Surf_linearSystemSolver


  !>
  !!   @param[in] IO : > 0 Check all cells of the mesh,
  !!                       and IO is the channel to write all Jacobian values \n
  !!                   = 0 Check all cells of the mesh, no file output.       \n
  !!                   =-1 only one cell, itm, be checked.
  !!   @param[in]  itm :  the cell being checked if IO=-1.
  !!   @param[out] itm :  the cell with worst Jacobian when if IO>=0.
  !!   @param[out] jbm :  the Jacobian for cell itm.
  !<
  SUBROUTINE Surf_calcQuality2D(Surf, IO, itm, jbm)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER, INTENT(IN)    :: IO
    INTEGER, INTENT(INOUT) :: itm
    REAL*8,  INTENT(OUT)   :: jbm
    REAL*8, DIMENSION(2, Surf%Cell%numCellNodes) :: Pts
    INTEGER :: IT
    REAL*8  :: Jb, JacMax, JacMin

    IF(IO>0) WRITE(IO,*)'#quality---'

    IF(IO==-1)THEN
       IT  = itm
       Pts = Surf%Coord(:,Surf%IPsp_Tri(:,IT))
       CALL HighOrderCell_GaussJacobian(Surf%Cell, Pts, JacMax, JacMin)
       Jbm = JacMin / JacMax
       RETURN  
    ENDIF

    ! Loop on gauss points
    Jbm = 2.
    itm = 0
    DO IT = 1, Surf%NB_Tri
       IF(MINVAL(Surf%IPsp_Tri(:,IT))==0) CYCLE
       Pts   = Surf%Coord(:,Surf%IPsp_Tri(:,IT))
       CALL HighOrderCell_GaussJacobian(Surf%Cell, Pts, JacMax, JacMin)
       Jb = JacMin / JacMax
       IF(Jb<Jbm)THEN
          Jbm = Jb
          itm = it
       ENDIF

       IF(IO>0) WRITE(IO,*)it, REAL(jb),REAL(JacMin),REAL(JacMax)       
    ENDDO

  END SUBROUTINE Surf_calcQuality2D


  !>
  !!   @param[in] IO : > 0 Check all cells of the mesh,
  !!                       and IO is the channel to write all Jacobian values \n
  !!                   = 0 Check all cells of the mesh, no file output.       \n
  !!                   =-1 only one cell, itm, be checked.
  !!   @param[in]  itm :  the cell being checked if IO=-1.
  !!   @param[out] itm :  the cell with worst Jacobian when if IO>=0.
  !!   @param[out] jbm :  the Jacobian for cell itm.
  !<
  SUBROUTINE Surf_calcQuality3D(Surf, IO, itm, jbm)
    IMPLICIT NONE
    TYPE(SurfaceMeshStorageType), INTENT(IN) :: Surf
    INTEGER, INTENT(IN)    :: IO
    INTEGER, INTENT(INOUT) :: itm
    REAL*8,  INTENT(OUT)   :: jbm
    REAL*8, DIMENSION(3, Surf%Cell%numCellNodes) :: Pts
    INTEGER :: IT, k
    REAL*8  :: Jb, JacMax, JacMin, Vtp(3,2), Va, Vt(3)

    IF(IO>0) WRITE(IO,*)'#quality---'

    IF(IO==-1)THEN
       IT  = itm
       Pts = Surf%Posit(:,Surf%IPsp_Tri(:,IT))
       JacMax = -1.0D12
       JacMin =  1.0D12
       DO k = 1, Surf%Cell%numGaussNodes
          Vtp(1:3,1:2) = MATMUL(Pts, Surf%Cell%GaussShapeDeriv(:,:,k))
          Vt = Geo3D_Cross_Product(Vtp(:,1), Vtp(:,2))
          Va = Geo3d_Distance(Vt)
          JacMax = MAX(JacMax, Va)
          JacMin = MIN(JacMin, Va)           
       ENDDO
       Jbm = JacMin / JacMax
       RETURN  
    ENDIF

    ! Loop on gauss points
    Jbm = 2.
    itm = 0
    DO IT = 1, Surf%NB_Tri
       IF(MINVAL(Surf%IPsp_Tri(:,IT))==0) CYCLE
       Pts  = Surf%Posit(:,Surf%IPsp_Tri(:,IT))
       JacMax = -1.0D12
       JacMin =  1.0D12
       DO k = 1, Surf%Cell%numGaussNodes
          Vtp(1:3,1:2) = MATMUL(Pts, Surf%Cell%GaussShapeDeriv(:,:,k))
          Vt = Geo3D_Cross_Product(Vtp(:,1), Vtp(:,2))
          Va = Geo3d_Distance(Vt)
          IF(Va<=0)THEN
             IF(IO>0) WRITE(IO,*)'# Warning--- negative jacobi at ie=',IT
             EXIT
          ENDIF
          JacMax = MAX(JacMax, Va)
          JacMin = MIN(JacMin, Va)           
       ENDDO

       Jb = JacMin / JacMax
       IF(Jb<Jbm)THEN
          Jbm = Jb
          itm = it
       ENDIF

       IF(IO>0) WRITE(IO,*)it, REAL(jb),REAL(JacMin),REAL(JacMax)       
    ENDDO

  END SUBROUTINE Surf_calcQuality3D



END MODULE SurfaceMeshHighOrder



