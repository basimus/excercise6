!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!>
!!   A link list data structure as a search engine.
!!
!!   A fast mechanism to find the egdes/triangles/quads/tetrahedra 
!!      fromed by given points.
!!
!!   Here is a sample of buliding and applying a tree:                     \n
!!         USE NodeNetTree                                                 \n
!!         TYPE(NodeNetTreeType) :: NetTree                                \n
!!         ...                                                             \n
!!         CALL NodeNetTree_Allocate(Norder, numNodes, numNets, NetTree)   \n
!!         DO                                                              \n
!!            ...                                                          \n
!!            CALL NodeNetTree_SearchAdd(Norder,ip,NetTree,netID)          \n
!!            ...                                                          \n
!!         ENDDO                                                           \n
!!         CALL NodeNetTree_export(NetTree,Norder,numNets,Nets)            \n
!!         CALL NodeNetTree_Clear(NetTree)                                 \n
!!         ...                                                             \n
!<

MODULE NodeNetTree
  USE Queue
  IMPLICIT NONE

  !>
  !!  A link list data structure used for searching for (edge, triangle, tetrahedron and so on)
  !!    formed by given points.
  !<
  TYPE :: NodeNetTreeType
     INTEGER  :: numNets  = 0
     INTEGER  :: sizeNode = 0
     INTEGER  :: sizeNet  = 0
     INTEGER, DIMENSION(:  ), POINTER :: FirstNet => null()
     INTEGER, DIMENSION(:,:), POINTER :: NetQueue => null()   !---(2,:) for edge; (3,:) for tri, (4,:) for quad
     TYPE (IntQueueType) :: RemovedNet
  END TYPE NodeNetTreeType

  INTEGER, PARAMETER  :: NodeNetTree_nallc_increase = 100000

CONTAINS

  FUNCTION Node_Sorting(Norder, ip) RESULT(jp)
    IMPLICIT NONE
    INTEGER :: Norder, ip(Norder), jp(Norder)
    INTEGER :: i1,i2,j
    jp(1:Norder) = ip(1:Norder)  
    DO i1=1,Norder-1
       DO i2=i1+1,Norder
          IF(jp(i1)>jp(i2))THEN
             j = jp(i1)
             jp(i1) = jp(i2)
             jp(i2) = j
          ENDIF
       ENDDO
    ENDDO
  END FUNCTION Node_Sorting

  FUNCTION NodeNetTree_isBuilt(NetTree) RESULT(yesno)
    IMPLICIT NONE
    TYPE(NodeNetTreeType) :: NetTree
    LOGICAL :: yesno
    IF(NetTree%numNets==0)THEN
       yesno = .FALSE.
    ELSE
       yesno = .TRUE.
    ENDIF
  END FUNCTION NodeNetTree_isBuilt

  !>
  !!   Export the data structure as an array Nets(1:Norder,1:numNets)
  !!   @param[in]  Norder = 2,3,4 : the type of net (eg. an edge, triangle or quad net).
  !!   @param[in]  NetTree        : the data structure.
  !!   @param[in]  Nets           : 2D unassociated integer pointer
  !!   @param[out] numNets        : number of Nets (edges / faces) of the net-tree
  !!   @param[out] NetTree        : the updated net-tree.
  !!   @param[out] Nets           : 2D array Nets(1:Norder,1:numNets) indicating edges,faces,...
  !<
  SUBROUTINE NodeNetTree_export(NetTree,Norder,numNets,Nets)
    IMPLICIT NONE
    TYPE(NodeNetTreeType), INTENT(IN) :: NetTree
    INTEGER, INTENT(IN)    :: Norder
    INTEGER, INTENT(OUT)   :: numNets
    INTEGER, DIMENSION(:,:), POINTER :: Nets
    INTEGER :: i, NetID

    numNets = NetTree%numNets
    IF(ASSOCIATED(Nets)) DEALLOCATE(Nets)
    ALLOCATE(Nets(Norder, numNets))
    Nets(:,:) = 0
    DO i=1,NetTree%sizeNode
       NetID = NetTree%FirstNet(i)
       DO WHILE(NetID>0)
          Nets(1,NetID) = i
          Nets(2:Norder,NetID) = NetTree%NetQueue(1:Norder-1,NetID)
          NetID = NetTree%NetQueue(Norder,NetID)
       ENDDO
    ENDDO
  END SUBROUTINE NodeNetTree_export


  !>
  !!   Allocate/reallocate memory for link list data structure
  !!     This is optional function. Using it once with proper numNodes and numNets 
  !!     before SUBROUTINE NodeNetTree_SearchAdd, may improve computational speeding 
  !!     or reduce memory cost Remember, calling this subroutine will NOT destroy the 
  !!     exsiting association if numNodes and numNets are bigger than old ones.
  !!   @param[in]  Norder = 2,3,4 : the type of net (eg. an edge, triangle or quad net).
  !!                        This number must keep unchanged for a net-tree throughout.
  !!   @param[in]  numNodes       : possible the maximum number of Node (Point) involved
  !!                        in the NetTree building.
  !!   @param[in]  numNets        : possible the number of Nets (edges / faces) which will 
  !!                        be built.
  !!   @param[in]  NetTree        : the net-tree which may (or may not) exist.
  !!   @param[out] NetTree        : the updated net-tree.
  !!                        If it is exsiting before this routine, the old information
  !!                        is kept.
  !<
  SUBROUTINE NodeNetTree_Allocate(Norder, numNodes, numNets, NetTree)
    USE array_allocator 
    IMPLICIT NONE
    INTEGER, INTENT(IN)    :: Norder, numNodes, numNets
    TYPE(NodeNetTreeType), INTENT(INOUT) :: NetTree
    INTEGER :: size1, size2

    size1 = NetTree%sizeNode
    size2 = NetTree%sizeNet
    IF(numNodes>size1)THEN
       CALL allc_int_1Dpointer(NetTree%FirstNet,    numNodes, 'FirstNet')
       NetTree%FirstNet(size1+1 : numNodes) = 0
       NetTree%sizeNode = numNodes
    ENDIF
    IF(numNets>size2)THEN
       CALL allc_int_2Dpointer(NetTree%NetQueue,Norder,numNets,'NetQueue')
       NetTree%NetQueue(:, size2+1 : numNets) = 0
       NetTree%sizeNet = numNets
    ENDIF

    RETURN
  END SUBROUTINE NodeNetTree_Allocate

  !>
  !!   Build a link list data structue from a connectivity array.
  !!   Allocation routine is included.
  !!   @param[in]  Norder = 2,3,4 : the type of net (eg. an edge, triangle or quad net).
  !!                        This number must keep unchanged for a net-tree throughout.
  !!   @param[in]  numNodes       : possible the maximum number of Node (Point) involved
  !!                        in the NetTree building.
  !!   @param[in]  numNets        : possible the number of Nets (edges / faces) which will 
  !!                        be built.
  !!   @param[in]  IP_Cell(Norder,*) : the array indicating the connection
  !!   @param[in]  NetTree        : the net-tree which may (or may not) exist.
  !!   @param[out] NetTree        : the updated net-tree.
  !!                        If it is exsiting before this routine, the old information
  !!                        is kept.
  !<
  SUBROUTINE NodeNetTree_Build(Norder, numNets, IP_Cell, numNodes, NetTree)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: Norder, numNets, IP_Cell(Norder,*), numNodes
    TYPE (NodeNetTreeType), INTENT(INOUT) :: NetTree
    INTEGER :: Cell, netID
    CALL NodeNetTree_Allocate(Norder, numNodes, numNets, NetTree)
    DO Cell = 1,numNets
       CALL NodeNetTree_SearchAdd(Norder,IP_Cell(:,Cell),NetTree,netID)
    ENDDO
    RETURN
  END SUBROUTINE NodeNetTree_Build

  !>
  !!   Search the data structure which is composed by given set of points.
  !!      If no data stucture exiting, add an new entry to the link list
  !!      So, this also is a builder of a data structure if calling it repeatly. 
  !!   @param[in]  Norder = 2,3,4 : the type of net (eg. an edge, triangle or quad net).
  !!   @param[in]  ip(Norder)     : points composing a data structure
  !!   @param[in]  NetTree        : the net-tree.
  !!   @param[out] NetTree        : the updated data structure.
  !!   @param[out] netID          : the ID of the data structure which is composed by those points.
  !!
  !!  Reminder: the new net take a new place, 
  !!            so that the number of net increse by 1 if the net is new.
  !!            This differs from SUBROUTINE NodeNetTree_SearchAddRemove()
  !<
  SUBROUTINE NodeNetTree_SearchAdd(Norder,ip,NetTree,netID)
    USE array_allocator 
    IMPLICIT NONE
    INTEGER, INTENT(IN)    :: Norder, ip(Norder)
    TYPE(NodeNetTreeType), INTENT(INOUT) :: NetTree
    INTEGER, INTENT(OUT)   :: netID
    INTEGER :: size1, size2, sizeNew, i, jp(4), IDsave

    size1 = NetTree%sizeNode
    size2 = NetTree%sizeNet

    jp(1:Norder) = Node_Sorting(Norder, ip)

    IF(jp(1)<=0)THEN
       CALL Error_STOP ( '--- NodeNetTree_searchAdd: jp(1)<=0')
    ENDIF

    IF(jp(1)>size1)THEN
       sizeNew = jp(1) + NodeNetTree_nallc_increase
       CALL allc_int_1Dpointer(NetTree%FirstNet,    sizeNew, 'FirstNet')
       NetTree%FirstNet(size1+1 : sizeNew) = 0
       NetTree%sizeNode = sizeNew
    ENDIF

    IF(NetTree%numNets>=size2)THEN
       sizeNew = size2 + NodeNetTree_nallc_increase
       CALL allc_int_2Dpointer(NetTree%NetQueue,Norder,sizeNew,'NetQueue')
       NetTree%NetQueue(:, size2+1 : sizeNew) = 0
       NetTree%sizeNet = sizeNew
    ENDIF

    !--- search
    netID = NetTree%FirstNet(jp(1))

    Loop_NetID : DO WHILE(NetID>0)
       DO i=1,Norder-1
          IF( NetTree%NetQueue(i,NetID)/=jp(i+1) ) THEN
             IDsave = NetID
             NetID  = NetTree%NetQueue(Norder,NetID)
             CYCLE Loop_NetID
          ENDIF
       ENDDO
       EXIT
    ENDDO Loop_NetID

    IF(NetID>0) THEN
       !--- found the net
       RETURN     
    ELSE IF(NetID==0) THEN
       !--- add a new net string
       NetTree%numNets = NetTree%numNets+1
       NetTree%NetQueue(1:Norder-1, NetTree%numNets) = jp(2:Norder)
       NetTree%NetQueue(Norder,     NetTree%numNets) = -jp(1)
       NetTree%FirstNet(jp(1)) = NetTree%numNets
       NetID = NetTree%numNets
    ELSE IF(NetID==-jp(1))THEN
       !--- add a net on a exsiting string
       NetTree%numNets = NetTree%numNets+1
       NetTree%NetQueue(1:Norder-1, NetTree%numNets) = jp(2:Norder)
       NetTree%NetQueue(Norder,     NetTree%numNets) = -jp(1)
       NetTree%NetQueue(Norder, IDsave) = NetTree%numNets
       NetID = NetTree%numNets
    ELSE
       !--- wrong 
       CALL Error_STOP ( '--- NodeNetTree_SearchAdd')
    ENDIF

    RETURN
  END SUBROUTINE NodeNetTree_SearchAdd

  !>
  !!   Search the data structure which is composed by given set of points.
  !!      If no data stucture exiting, add an new entry to the link list.
  !!      If the data stucture exiting, remove it from the link list.
  !!   @param[in]  Norder = 2,3,4 : the type of net (eg. an edge, triangle or quad net).
  !!   @param[in]  ip(Norder)     : points composing a data structure
  !!   @param[in]  NetTree        : the net-tree.
  !!   @param[out] NetTree        : the updated data structure.
  !!   @param[out] netID          >0 the ID of the new added data structure which is composed by those points.  \n
  !!                              <0 the -ID of the removed data structure which is composed by those points. 
  !!
  !!  Reminder: the new net take the place of a removed net if exsit.
  !!            so that the number of net might not increse even the net is new.
  !!            This differs from SUBROUTINE NodeNetTree_SearchAdd()
  !<
  SUBROUTINE NodeNetTree_SearchAddRemove(Norder,ip,NetTree,netID)
    USE array_allocator 
    IMPLICIT NONE
    INTEGER, INTENT(IN)    :: Norder, ip(Norder)
    TYPE(NodeNetTreeType), INTENT(INOUT) :: NetTree
    INTEGER, INTENT(OUT)   :: netID
    INTEGER :: size1, size2, sizeNew, i, jp(4), IDsave, IDnew

    size1 = NetTree%sizeNode
    size2 = NetTree%sizeNet

    jp(1:Norder) = Node_Sorting(Norder, ip)

    IF(jp(1)<=0)THEN
       CALL Error_STOP ( '--- NodeNetTree_searchAdd: jp(1)<=0')
    ENDIF

    IF(jp(1)>size1)THEN
       sizeNew = jp(1) + NodeNetTree_nallc_increase
       CALL allc_int_1Dpointer(NetTree%FirstNet,    sizeNew, 'FirstNet')
       NetTree%FirstNet(size1+1 : sizeNew) = 0
       NetTree%sizeNode = sizeNew
    ENDIF

    IF(NetTree%numNets>=size2)THEN
       sizeNew = size2 + NodeNetTree_nallc_increase
       CALL allc_int_2Dpointer(NetTree%NetQueue,Norder,sizeNew,'NetQueue')
       NetTree%NetQueue(:, size2+1 : sizeNew) = 0
       NetTree%sizeNet = sizeNew
    ENDIF

    !--- search
    netID = NetTree%FirstNet(jp(1))

    Loop_NetID : DO WHILE(NetID>0)
       DO i=1,Norder-1
          IF( NetTree%NetQueue(i,NetID)/=jp(i+1) ) THEN
             IDsave = NetID
             NetID  = NetTree%NetQueue(Norder,NetID)
             CYCLE Loop_NetID
          ENDIF
       ENDDO
       EXIT
    ENDDO Loop_NetID

    IF(NetID>0) THEN
       !--- found the net, remove it
       IF(netID == NetTree%FirstNet(jp(1)))THEN
          IF(NetTree%NetQueue(Norder, NetID)>0)THEN
             NetTree%FirstNet(jp(1)) = NetTree%NetQueue(Norder, NetID)
          ELSE
             NetTree%FirstNet(jp(1)) = 0
          ENDIF
       ELSE
          NetTree%NetQueue(Norder, IDsave) = NetTree%NetQueue(Norder, NetID)
       ENDIF
       NetTree%NetQueue(1:Norder, NetID) = 0
       CALL IntQueue_Push10k(NetTree%RemovedNet, NetID)
       NetID = -NetID
    ELSE
       !--- add a new net
       IF(NetTree%RemovedNet%numNodes==0)THEN
          NetTree%numNets = NetTree%numNets+1
          IDnew           = NetTree%numNets
       ELSE
          IDnew           = NetTree%RemovedNet%back
          CALL IntQueue_RemoveLast(NetTree%RemovedNet)
       ENDIF
       NetTree%NetQueue(1:Norder-1, IDnew) = jp(2:Norder)
       NetTree%NetQueue(Norder,     IDnew) = -jp(1)
       IF(NetID==0) THEN
          NetTree%FirstNet(jp(1)) = IDnew
       ELSE IF(NetID==-jp(1))THEN
          NetTree%NetQueue(Norder, IDsave) = IDnew
       ELSE
          !--- wrong 
          CALL Error_STOP ( '--- NodeNetTree_SearchAddRemove')
       ENDIF
       NetID = IDnew
    ENDIF

    RETURN
  END SUBROUTINE NodeNetTree_SearchAddRemove

  !>
  !!   Search the data structure which is composed by given points.
  !!   @param[in]  Norder = 2,3,4 : the type of net (eg. an edge, triangle or quad net).
  !!   @param[in]  ip(Norder)      : points composing a net
  !!   @param[in]  NetTree        : the data structure.
  !!   @param[out] netID          : the ID of the data structure which is composed by those points.
  !!                        If no such structure exsit netID return 0.
  !<
  SUBROUTINE NodeNetTree_search(Norder,ip,NetTree,netID)
    IMPLICIT NONE
    INTEGER, INTENT(IN)    :: Norder, ip(Norder)
    TYPE(NodeNetTreeType), INTENT(IN) :: NetTree
    INTEGER, INTENT(OUT)   :: netID
    INTEGER :: i, i1, i2, j
    INTEGER :: jp(4)  

    jp(1:Norder) = Node_Sorting(Norder, ip)

    IF(jp(1)<=0)THEN
       CALL Error_STOP ( '--- NodeNetTree_search: jp(1)<=0')
    ENDIF
    IF(jp(1)>NetTree%sizeNode)THEN
       netID = 0
       RETURN
    ENDIF

    netID = NetTree%FirstNet(jp(1))

    Loop_NetID : DO WHILE(NetID>0)
       DO i=1,Norder-1
          IF( NetTree%NetQueue(i,NetID)/=jp(i+1) ) THEN
             NetID = NetTree%NetQueue(Norder,NetID)
             CYCLE Loop_NetID
          ENDIF
       ENDDO
       EXIT
    ENDDO Loop_NetID

    IF(NetID>0) THEN
       !--- found the net
       RETURN     
    ELSE IF(NetID==0) THEN
       !--- no such net string
       RETURN
    ELSE IF(NetID==-jp(1))THEN
       !--- no such net
       NetID = 0
       RETURN
    ELSE
       !--- wrong 
       CALL Error_STOP ( '--- NodeNetTree_Search')
    ENDIF

    RETURN
  END SUBROUTINE NodeNetTree_search

  !>
  !!   Copy a dtat stucture.
  !!   @param[in]  NetTreeSource  : source net-tree being copied.
  !!   @param[out] NetTreeTarget  : target net-tree.
  !<
  SUBROUTINE NodeNetTree_copy(NetTreeSource,NetTreeTarget)
    USE array_allocator 
    IMPLICIT NONE
    TYPE(NodeNetTreeType), INTENT(IN)    :: NetTreeSource
    TYPE(NodeNetTreeType), INTENT(INOUT) :: NetTreeTarget
    INTEGER :: Norder, size1, size2, i

    CALL NodeNetTree_Clear(NetTreeTarget)

    Norder = SIZE(NetTreeSource%NetQueue,1)
    size1  = NetTreeSource%sizeNode
    size2  = NetTreeSource%sizeNet

    CALL allc_int_1Dpointer(NetTreeTarget%FirstNet,       size1, 'FirstNet')
    CALL allc_int_2Dpointer(NetTreeTarget%NetQueue,Norder,size2, 'NetQueue')

    NetTreeTarget%FirstNet(          1:size1) = NetTreeSource%FirstNet(          1:size1)
    NetTreeTarget%NetQueue(1:Norder, 1:size2) = NetTreeSource%NetQueue(1:Norder, 1:size2)
    NetTreeTarget%sizeNode = size1
    NetTreeTarget%sizeNet  = size2
    NetTreeTarget%numNets  = NetTreeSource%numNets

    RETURN
  END SUBROUTINE NodeNetTree_copy

  !>
  !!   Delete the data structure and release memory.
  !<
  SUBROUTINE NodeNetTree_Clear(NetTree)
    IMPLICIT NONE
    TYPE(NodeNetTreeType), INTENT(INOUT) :: NetTree
    NetTree%sizeNode = 0
    NetTree%sizeNet  = 0
    NetTree%numNets  = 0
    IF(ASSOCIATED(NetTree%FirstNet)) DEALLOCATE(NetTree%FirstNet)
    IF(ASSOCIATED(NetTree%NetQueue)) DEALLOCATE(NetTree%NetQueue)
    CALL IntQueue_Clear(NetTree%RemovedNet)
    RETURN
  END SUBROUTINE NodeNetTree_Clear

  !>
  !!   Zerolise the net-tree. 
  !!   If only a small part of net are non-zeros,
  !!      this routine save cputime comparing with clear & reAllocate.
  !<
  SUBROUTINE NodeNetTree_Zerolise(NetTree)
    IMPLICIT NONE
    TYPE(NodeNetTreeType), INTENT(INOUT) :: NetTree
    INTEGER :: netID, ID, IP, Net2, Norder
    Norder = SIZE(NetTree%NetQueue,1)  
    DO ID = 1, NetTree%numNets
       IF(NetTree%NetQueue(1,ID)==0) CYCLE
       NetID = ID
       DO WHILE(NetID>0)
          Net2 = NetTree%NetQueue(Norder,NetID)
          NetTree%NetQueue(:, NetID) = 0
          NetID = Net2
       ENDDO
       IF(NetID==0) CALL Error_STOP ( '--- NodeNetTree_Clean 1')
       IP = -NetID
       IF(IP>NetTree%sizeNode) CALL Error_STOP ( '--- NodeNetTree_Clean 2')
       NetTree%FirstNet(IP) = 0
    ENDDO
    NetTree%numNets = 0
    RETURN
  END SUBROUTINE NodeNetTree_Zerolise

END MODULE NodeNetTree


