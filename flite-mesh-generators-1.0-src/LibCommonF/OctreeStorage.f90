!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!******************************************************************************!
!
!  Order of nodes in each grid (also of the index of the "Son" Pointers) :
!
!                         8________7
!                         /|      /|
!                      5 /_______6 |
!                        | |     | |
!                        | |     | |
!                        | |4____|_|3
!                        |/      | /
!                        |_______|/ 
!                       1         2
!
!   Attention: above connectivity conform with those of 
!              Hexahedral cell in module CellConnectivity
!
!  Index of neighbour Pointers:
!                                                       
!                        
!                                            /    /Back/  
!         |    | Up |                    ___/____/____/_7_    
!       __|____|____|_7_                   /         / 
!         |         |                     /         / Right
!         |         | Right           __ /         /__    
!       __|         |__                 /         /     
!         |         |             Left /         /      
!    Left |         |              ___/_________/__    
!       __|_________|__             1/Front    /      
!        1|Down|    |               /    /    /      
!         |    |    |         
!
!
!******************************************************************************!
!>
!!  Octree mesh storage, including the definition of types and basic functions.
!!
!!  A cell of octree meshes is a hexahedron whose connectivity 
!!    is defined in module CellConnectivity .
!!
!!  A cell has 8 children if it has been splitted.
!!  A leaf cell is a cell that has no child (not be splitted).
!<
MODULE OctreeMesh

  USE CellConnectivity
  USE PtADTreeModule
  USE array_allocator
  IMPLICIT NONE

  INTEGER, PARAMETER :: Max_Oct_Level = 200
  INTEGER, PARAMETER :: nallc_Oct_increase = 100000
  INTEGER, PARAMETER :: iCorner_Oct(3,8) =    &
       reshape( (/-1,-1,-1, 1,-1,-1, 1, 1,-1, -1, 1,-1,   &
                  -1,-1, 1, 1,-1, 1, 1, 1, 1, -1, 1, 1/), (/3,8/) )


  TYPE :: OctreeMesh_Cell_Type
     INTEGER :: ID    = 0
     INTEGER :: Level = 0
     INTEGER :: Mark  = 0
     !>  Eight nodes of the hexahedral cell, ref module CellConnectivity.
     INTEGER :: Nodes(8)
     !>  if =.true., a bisected cell; 
     !!  otherwise, a leaf cell.
     LOGICAL :: Bisected = .FALSE.
     !--- sub grid pointers
     TYPE (OctreeMesh_Cell_Type), POINTER :: Son1 => null()        !<  Left  Front Bottom,  with Nodes(1)
     TYPE (OctreeMesh_Cell_Type), POINTER :: Son2 => null()        !<  Right Front Bottom,  with Nodes(2)
     TYPE (OctreeMesh_Cell_Type), POINTER :: Son3 => null()        !<  Right Back  Bottom,  with Nodes(3)
     TYPE (OctreeMesh_Cell_Type), POINTER :: Son4 => null()        !<  Left  Back  Bottom,  with Nodes(4)
     TYPE (OctreeMesh_Cell_Type), POINTER :: Son5 => null()        !<  Left  Front Top,     with Nodes(5)
     TYPE (OctreeMesh_Cell_Type), POINTER :: Son6 => null()        !<  Right Front Top,     with Nodes(6)
     TYPE (OctreeMesh_Cell_Type), POINTER :: Son7 => null()        !<  Right Back  Top,     with Nodes(7)
     TYPE (OctreeMesh_Cell_Type), POINTER :: Son8 => null()        !<  Left  Back  Top,     with Nodes(8)
     !--- neighbour grid pointers
     TYPE (OctreeMesh_Cell_Type), POINTER :: Front => null()       !<  Front with Nodes(1),   Y-
     TYPE (OctreeMesh_Cell_Type), POINTER :: Left => null()        !<  Left  with Nodes(1),   X-
     TYPE (OctreeMesh_Cell_Type), POINTER :: Down => null()        !<  Down  with Nodes(1),   Z-
     TYPE (OctreeMesh_Cell_Type), POINTER :: Back => null()        !<  Back  with Nodes(7),   Y+
     TYPE (OctreeMesh_Cell_Type), POINTER :: Right => null()       !<  Right with Nodes(7),   X+
     TYPE (OctreeMesh_Cell_Type), POINTER :: Up => null()          !<  Up    with Nodes(7),   Z+
  END TYPE OctreeMesh_Cell_Type

  TYPE :: OctreeMesh_CellList_Type
     TYPE (OctreeMesh_Cell_Type), POINTER :: to => null()
  END TYPE OctreeMesh_CellList_Type


  TYPE (OctreeMesh_Cell_Type), TARGET, SAVE :: OctreeMesh_EmptyCell

  TYPE :: OctreeMesh_Type
     CHARACTER ( LEN = 60 ) :: theName = ' '
     TYPE (OctreeMesh_Cell_Type), DIMENSION(:,:,:), POINTER :: Cells => null()
     TYPE (OctreeMesh_CellList_Type), DIMENSION(:), POINTER :: LeafCells => null()
     INTEGER :: numNodes    = 0
     INTEGER :: numCells    = 0
     INTEGER :: imax        = 0
     INTEGER :: jmax        = 0
     INTEGER :: kmax        = 0
     INTEGER :: nallc_nodes = 0
     REAL*8  :: DX(Max_Oct_Level+1)
     REAL*8  :: DY(Max_Oct_Level+1)
     REAL*8  :: DZ(Max_Oct_Level+1)
     REAL*8  :: Pmin(3), Pmax(3), TinyD(3)
     REAL*8,  DIMENSION(:,:), POINTER :: Posit => null()   !---(3,:)
  END TYPE OctreeMesh_Type

  TYPE(PtADTreeType),SAVE :: Oct_PtADTree

CONTAINS

  !>
  !!     Allocate array for point position.
  !!     Set OctMesh%nallc_nodes beforehand.
  !<
  SUBROUTINE OctreeMesh_NodeAllocate(OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    CALL allc_real8_2Dpointer(OctMesh%Posit,  3, OctMesh%nallc_nodes, 'OctreeMesh_NodeAllocate')
    OctMesh%Posit(:,  1:OctMesh%nallc_nodes) = 0.D0
    RETURN
  END SUBROUTINE OctreeMesh_NodeAllocate

  !>
  !!     ReAllocate array for point position.
  !!     ReSet OctMesh%nallc_nodes beforehand.
  !<
  SUBROUTINE OctreeMesh_NodeReAllocate(OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    INTEGER :: n1
    n1 = OctMesh%nallc_nodes + 1
    OctMesh%nallc_nodes = OctMesh%nallc_nodes + nallc_Oct_increase
    CALL allc_real8_2Dpointer(OctMesh%Posit,  3, OctMesh%nallc_nodes, 'OctMesh')
    OctMesh%Posit(:,  n1:OctMesh%nallc_nodes) = 0.D0
    RETURN
  END SUBROUTINE OctreeMesh_NodeReAllocate

  !>
  !!     Return a pointer associated with a neighbour cell.
  !!     @param[in] pOct_Cell  the cell pointer.
  !!     @param[in] idirct =1,2,3,4,5,6 
  !!                 indicating  x-,x+,y-,y+,z-,z+ direction repectively
  !<
  FUNCTION OctreeMesh_NextCell(pOct_Cell,idirct) RESULT(p2Oct_Cell)
    IMPLICIT NONE
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    INTEGER :: idirct
    SELECT CASE(idirct)
    CASE(1)
       p2Oct_Cell => pOct_Cell%Left
    CASE(2)
       p2Oct_Cell => pOct_Cell%Right
    CASE(3)
       p2Oct_Cell => pOct_Cell%Front
    CASE(4)
       p2Oct_Cell => pOct_Cell%Back
    CASE(5)
       p2Oct_Cell => pOct_Cell%Down
    CASE(6)
       p2Oct_Cell => pOct_Cell%Up
    END SELECT
    RETURN
  END FUNCTION OctreeMesh_NextCell

  !>
  !!     Return a pointer associated with a son cell.
  !!     @param[in] pOct_Cell  the cell pointer.
  !!     @param[in] idirct =1,2,...8 
  !<
  FUNCTION OctreeMesh_SonCell(pOct_Cell,idirct) RESULT(p2Oct_Cell)
    IMPLICIT NONE
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    INTEGER :: idirct
    SELECT CASE(idirct)
    CASE(1)
       p2Oct_Cell => pOct_Cell%Son1
    CASE(2)
       p2Oct_Cell => pOct_Cell%Son2
    CASE(3)
       p2Oct_Cell => pOct_Cell%Son3
    CASE(4)
       p2Oct_Cell => pOct_Cell%Son4
    CASE(5)
       p2Oct_Cell => pOct_Cell%Son5
    CASE(6)
       p2Oct_Cell => pOct_Cell%Son6
    CASE(7)
       p2Oct_Cell => pOct_Cell%Son7
    CASE(8)
       p2Oct_Cell => pOct_Cell%Son8
    END SELECT
    RETURN
  END FUNCTION OctreeMesh_SonCell

  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE OctreeMesh_Cell_getLeafCells(pOct_Cell,OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    INTEGER :: idirct, id

    IF(pOct_Cell%Level == 0) RETURN 
    IF(pOct_Cell%ID <= 0) RETURN 

    IF(pOct_Cell%Bisected)THEN
       DO idirct=1,8
          p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
          CALL OctreeMesh_Cell_getLeafCells(p2Oct_Cell,OctMesh)
       ENDDO
       RETURN
    ENDIF

    id = pOct_Cell%ID
    IF(id>OctMesh%numCells) CALL Error_STOP ( ' wrong id at OctreeMesh_Cell_getLeafCells')
    OctMesh%LeafCells(id)%to => pOct_Cell

    RETURN
  END SUBROUTINE OctreeMesh_Cell_getLeafCells

  !>
  !!     Associate OctMesh%LeafCells
  !<
  SUBROUTINE OctreeMesh_getLeafCells(OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell
    INTEGER :: i,j,k

    IF( ASSOCIATED(OctMesh%LeafCells) )  DEALLOCATE(OctMesh%LeafCells)
    ALLOCATE(OctMesh%LeafCells(OctMesh%numCells))

    DO k=1,OctMesh%kmax
       DO j=1,OctMesh%jmax
          DO i=1,OctMesh%imax
             pOct_Cell => OctMesh%Cells(i,j,k)
             CALL OctreeMesh_Cell_getLeafCells(pOct_Cell,OctMesh)
          ENDDO
       ENDDO
    ENDDO

    RETURN
  END SUBROUTINE OctreeMesh_getLeafCells

  !>
  !!     Set a OctreeMesh.
  !!     @param[in]  NI   :  number of basic girds in 3 direction.
  !!     @param[in]  D    :  grid-size of a basic gird in 3 direction.
  !!     @param[in]  Pmin :  the original corner of the domain
  !!     @param[out] OctMesh : the octree mesh.
  !<
  SUBROUTINE OctreeMesh_Set(NI, D, Pmin, OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    INTEGER, INTENT(IN) :: NI(3)
    REAL*8,  INTENT(IN) :: D(3), Pmin(3)
    INTEGER :: i, j, k, n, ijlayer, m

    OctMesh%imax     = NI(1)
    OctMesh%jmax     = NI(2)
    OctMesh%kmax     = NI(3)
    OctMesh%Pmin(:)  = Pmin(:)
    OctMesh%Pmax(:)  = Pmin(:) + NI(:) * D(:)
    OctMesh%numNodes = (OctMesh%imax+1)*(OctMesh%jmax+1)*(OctMesh%kmax+1)
    OctMesh%numCells = (OctMesh%imax  )*(OctMesh%jmax  )*(OctMesh%kmax  )

    OctMesh%nallc_nodes = OctMesh%numNodes + nallc_Oct_Increase
    CALL OctreeMesh_NodeAllocate(OctMesh)
    ALLOCATE (OctMesh%Cells(OctMesh%imax, OctMesh%jmax, OctMesh%kmax))

    OctMesh%DX(1) = D(1)
    OctMesh%DY(1) = D(2)
    OctMesh%DZ(1) = D(3)
    DO n=2,Max_Oct_Level+1
       OctMesh%DX(n) = OctMesh%DX(n-1)/2.D0
       OctMesh%DY(n) = OctMesh%DY(n-1)/2.D0
       OctMesh%DZ(n) = OctMesh%DZ(n-1)/2.D0
    ENDDO
    OctMesh%TinyD(1) = OctMesh%DX(Max_Oct_Level+1)
    OctMesh%TinyD(2) = OctMesh%DY(Max_Oct_Level+1)
    OctMesh%TinyD(3) = OctMesh%DZ(Max_Oct_Level+1)

    ijlayer = (OctMesh%imax+1)*(OctMesh%jmax+1)
    n = 0
    m = 0
    DO k=1,OctMesh%kmax+1
       DO j=1,OctMesh%jmax+1
          DO i=1,OctMesh%imax+1
             n = n+1
             OctMesh%Posit(1,n) = (i-1)*OctMesh%DX(1) + OctMesh%Pmin(1)
             OctMesh%Posit(2,n) = (j-1)*OctMesh%DY(1) + OctMesh%Pmin(2)
             OctMesh%Posit(3,n) = (k-1)*OctMesh%DZ(1) + OctMesh%Pmin(3)

             IF(i==OctMesh%imax+1 .OR. j==OctMesh%jmax+1 .OR. k==OctMesh%kmax+1) CYCLE

             m = m+1
             OctMesh%Cells(i,j,k)%ID    = m
             OctMesh%Cells(i,j,k)%Level = 1

             OctMesh%Cells(i,j,k)%Nodes(1) = n
             OctMesh%Cells(i,j,k)%Nodes(2) = n + 1
             OctMesh%Cells(i,j,k)%Nodes(3) = n + (OctMesh%imax+1) + 1
             OctMesh%Cells(i,j,k)%Nodes(4) = n + (OctMesh%imax+1)
             OctMesh%Cells(i,j,k)%Nodes(5) = OctMesh%Cells(i,j,k)%Nodes(1) + ijlayer
             OctMesh%Cells(i,j,k)%Nodes(6) = OctMesh%Cells(i,j,k)%Nodes(2) + ijlayer
             OctMesh%Cells(i,j,k)%Nodes(7) = OctMesh%Cells(i,j,k)%Nodes(3) + ijlayer
             OctMesh%Cells(i,j,k)%Nodes(8) = OctMesh%Cells(i,j,k)%Nodes(4) + ijlayer

             IF(i>1)THEN
                OctMesh%Cells(i,j,k)%Left => OctMesh%Cells(i-1,j,k)
             ELSE
                OctMesh%Cells(i,j,k)%Left => OctreeMesh_EmptyCell
             ENDIF
             IF(i<OctMesh%imax)THEN
                OctMesh%Cells(i,j,k)%Right => OctMesh%Cells(i+1,j,k)
             ELSE
                OctMesh%Cells(i,j,k)%Right => OctreeMesh_EmptyCell
             ENDIF

             IF(j>1)THEN
                OctMesh%Cells(i,j,k)%Front => OctMesh%Cells(i,j-1,k)
             ELSE
                OctMesh%Cells(i,j,k)%Front => OctreeMesh_EmptyCell
             ENDIF
             IF(j<OctMesh%jmax)THEN
                OctMesh%Cells(i,j,k)%Back => OctMesh%Cells(i,j+1,k)
             ELSE
                OctMesh%Cells(i,j,k)%Back => OctreeMesh_EmptyCell
             ENDIF

             IF(k>1)THEN
                OctMesh%Cells(i,j,k)%Down => OctMesh%Cells(i,j,k-1)
             ELSE
                OctMesh%Cells(i,j,k)%Down => OctreeMesh_EmptyCell
             ENDIF
             IF(k<OctMesh%kmax)THEN
                OctMesh%Cells(i,j,k)%Up => OctMesh%Cells(i,j,k+1)
             ELSE
                OctMesh%Cells(i,j,k)%Up => OctreeMesh_EmptyCell
             ENDIF
          ENDDO
       ENDDO
    ENDDO

    RETURN
  END SUBROUTINE OctreeMesh_Set


  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE OctreeMesh_Cell_AssoNeighbour(pOct_Cell,OctMesh)
    IMPLICIT NONE
    TYPE(OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    REAL*8  :: p0(3), pp(3)
    INTEGER :: level, idirct

    IF(pOct_Cell%Level==0) RETURN

    level   = pOct_Cell%Level + 1
    
    P0(1:3) = OctMesh%Posit(1:3,pOct_Cell%Nodes(1))
    pp(1)   = P0(1)   - OctMesh%TinyD(1)
    pp(2:3) = P0(2:3) + OctMesh%TinyD(2:3)
    pOct_Cell%Left  => OctreeMesh_CellSearch(pp,level,OctMesh)
    pp(2)   = P0(2)   - OctMesh%TinyD(2)
    pp(1)   = P0(1)   + OctMesh%TinyD(1)
    pp(3)   = P0(3)   + OctMesh%TinyD(3)
    pOct_Cell%Front => OctreeMesh_CellSearch(pp,level,OctMesh)
    pp(3)   = P0(3)   - OctMesh%TinyD(3)
    pp(1:2) = P0(1:2) + OctMesh%TinyD(1:2)
    pOct_Cell%Down  => OctreeMesh_CellSearch(pp,level,OctMesh)

    P0(1:3) = OctMesh%Posit(1:3,pOct_Cell%Nodes(7))
    pp(1)   = P0(1)   + OctMesh%TinyD(1)
    pp(2:3) = P0(2:3) - OctMesh%TinyD(2:3)
    pOct_Cell%Right => OctreeMesh_CellSearch(pp,level,OctMesh)
    pp(2)   = P0(2)   + OctMesh%TinyD(2)
    pp(1)   = P0(1)   - OctMesh%TinyD(1)
    pp(3)   = P0(3)   - OctMesh%TinyD(3)
    pOct_Cell%Back  => OctreeMesh_CellSearch(pp,level,OctMesh)
    pp(3)   = P0(3)   + OctMesh%TinyD(3)
    pp(1:2) = P0(1:2) - OctMesh%TinyD(1:2)
    pOct_Cell%Up    => OctreeMesh_CellSearch(pp,level,OctMesh)

    IF(pOct_Cell%Bisected)THEN
       DO idirct=1,8
          p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
          CALL OctreeMesh_Cell_AssoNeighbour(p2Oct_Cell,OctMesh)
       ENDDO
    ENDIF

    RETURN
  END SUBROUTINE OctreeMesh_Cell_AssoNeighbour

  !>
  !!     Associate neighbour points for every cells.
  !<
  SUBROUTINE OctreeMesh_AssoNeighbour(OctMesh)
    IMPLICIT NONE
    TYPE(OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell
    INTEGER :: i, j, k
    DO i=1,OctMesh%imax
       DO j=1,OctMesh%jmax
          DO k=1,OctMesh%kmax
             pOct_Cell => OctMesh%Cells(i,j,k)
             CALL OctreeMesh_Cell_AssoNeighbour(pOct_Cell,OctMesh)
          ENDDO
       ENDDO
    ENDDO
    RETURN
  END SUBROUTINE OctreeMesh_AssoNeighbour

  !>
  !!     Return a level with which the gridsize of a cell is coverd
  !!            by D in 3 direction.
  !<
  FUNCTION OctreeMesh_getFineLevel(D,OctMesh) RESULT(level)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: D(3)
    TYPE (OctreeMesh_Type), INTENT(IN) :: OctMesh
    INTEGER :: level
     DO level = 1, Max_Oct_Level
        IF( OctMesh%DX(level)<D(1) .and. OctMesh%DY(level)<D(2) .and.  &
            OctMesh%DZ(level)<D(3) )  EXIT
     ENDDO
  END FUNCTION OctreeMesh_getFineLevel

  !>
  !!     Return a level with which the gridsize of a cell covers
  !!            D in 3 direction.
  !<
  FUNCTION OctreeMesh_getCoarseLevel(D,OctMesh) RESULT(level)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: D(3)
    TYPE (OctreeMesh_Type), INTENT(IN) :: OctMesh
    INTEGER :: level
     DO level = 1, Max_Oct_Level
        IF( OctMesh%DX(level)<D(1) .OR. OctMesh%DY(level)<D(2) .OR.  &
            OctMesh%DZ(level)<D(3) )  EXIT
     ENDDO
     Level = level-1
  END FUNCTION OctreeMesh_getCoarseLevel

  !>
  !!    find hang nodes on 12 edges and 6 faces of a leaf cell.
  !<
  SUBROUTINE OctreeMesh_getCellHangNodes(pOct_Cell,OctMesh,ipHang)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(IN) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    INTEGER, INTENT(OUT) :: ipHang(18)
    INTEGER :: ip(8), Level, i, k, ic
    REAL*8  :: pp(3), pp0(3)

    IF(pOct_Cell%Level == 0)THEN
       CALL Error_STOP ( ' a fake call')
    ENDIF
    IF(pOct_Cell%Bisected)THEN
       CALL Error_STOP ( ' not leaf cell')
    ENDIF

    Level       = pOct_Cell%Level
    ip(1:8)     = pOct_Cell%Nodes(1:8)
    ipHang(1:18) = 0    !--- 12 for edge, 6 for face

    !--- for hanging points

    p2Oct_Cell => pOct_Cell%Left
    IF(p2Oct_Cell%Level > Level) THEN
       ipHang(13) = p2Oct_Cell      %Nodes(7)
       ipHang(9)  = p2Oct_Cell      %Nodes(6)
       ipHang(5)  = p2Oct_Cell      %Nodes(3)
       ipHang(7)  = p2Oct_Cell%Up   %Nodes(7)
       ipHang(11) = p2Oct_Cell%Back %Nodes(7)
    ENDIF

    p2Oct_Cell => pOct_Cell%Front
    IF(p2Oct_Cell%Level > Level) THEN
       ipHang(15) = p2Oct_Cell      %Nodes(7)
       ipHang(9)  = p2Oct_Cell      %Nodes(8)
       ipHang(1)  = p2Oct_Cell      %Nodes(3)
       ipHang(3)  = p2Oct_Cell%Up   %Nodes(7)
       ipHang(10) = p2Oct_Cell%Right%Nodes(7)
    ENDIF

    p2Oct_Cell => pOct_Cell%Down
    IF(p2Oct_Cell%Level > Level) THEN
       ipHang(17) = p2Oct_Cell      %Nodes(7)
       ipHang(5)  = p2Oct_Cell      %Nodes(8)
       ipHang(1)  = p2Oct_Cell      %Nodes(6)
       ipHang(2)  = p2Oct_Cell%Back %Nodes(7)
       ipHang(6)  = p2Oct_Cell%Right%Nodes(7)
    ENDIF

    p2Oct_Cell => pOct_Cell%Right
    IF(p2Oct_Cell%Level > Level) THEN
       ipHang(14) = p2Oct_Cell      %Nodes(1)
       ipHang(12) = p2Oct_Cell      %Nodes(4)
       ipHang(8)  = p2Oct_Cell      %Nodes(5)
       ipHang(10) = p2Oct_Cell%Front%Nodes(1)
       ipHang(6)  = p2Oct_Cell%Down %Nodes(1)
    ENDIF

    p2Oct_Cell => pOct_Cell%Back
    IF(p2Oct_Cell%Level > Level) THEN
       ipHang(16) = p2Oct_Cell      %Nodes(1)
       ipHang(12) = p2Oct_Cell      %Nodes(2)
       ipHang(4)  = p2Oct_Cell      %Nodes(5)
       ipHang(11) = p2Oct_Cell%Left %Nodes(1)
       ipHang(2)  = p2Oct_Cell%Down %Nodes(1)
    ENDIF

    p2Oct_Cell => pOct_Cell%Up
    IF(p2Oct_Cell%Level > Level) THEN
       ipHang(18) = p2Oct_Cell      %Nodes(1)
       ipHang(8)  = p2Oct_Cell      %Nodes(2)
       ipHang(4)  = p2Oct_Cell      %Nodes(4)
       ipHang(7)  = p2Oct_Cell%Left %Nodes(1)
       ipHang(3)  = p2Oct_Cell%Front%Nodes(1)
    ENDIF

    !--- check hanging nodes further

    DO i = 1,12
       IF(ipHang(i)==0)THEN
          pp0 = ( OctMesh%Posit(:,ip(iEdge_Hex(1,i)))   &
               + OctMesh%Posit(:,ip(iEdge_Hex(2,i))) )/2.D0
          DO k=1,2
             ic = iEdge_Hex(k,i)
             pp  = pp0 + iCorner_Oct(1:3,ic)*OctMesh%TinyD(:)
             p2Oct_Cell => OctreeMesh_CellSearch(pp,Level+1,OctMesh)
             IF(p2Oct_Cell%Level==level) EXIT
             IF(p2Oct_Cell%Level==level+1)THEN
                ipHang(i) = p2Oct_Cell%Nodes(iOpp_Hex(ic))
                EXIT
             ENDIF
          ENDDO
       ENDIF
    ENDDO

    RETURN
  END SUBROUTINE OctreeMesh_getCellHangNodes

  !>
  !!    Bisect a leaf cell, get 8 children cells.
  !!    @param[in] pOct_Cell the cell pointer.
  !!    @param[in,out] OctMesh  the octree mesh.  
  !<
  RECURSIVE SUBROUTINE OctreeMesh_CellBisect(pOct_Cell,OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell, p3Oct_Cell
    INTEGER :: ip(8), ipHang(19), Level, i, id1, id2
    REAL*8 :: pp(3), pp0(3)

    IF(pOct_Cell%Bisected) CALL Error_STOP ( '--- OctreeMesh_CellBisect :: a bisected cell')
    IF(pOct_Cell%Level==0) CALL Error_STOP ( '--- OctreeMesh_CellBisect :: a fake cell')
    IF(pOct_Cell%Level>=Max_Oct_Level) CALL Error_STOP ( '--- a tiny cell')

    Level = pOct_Cell%Level
    DO i=1,6
       p2Oct_Cell => OctreeMesh_NextCell(pOct_Cell,i)
       IF(p2Oct_Cell%Level > 0 .AND. p2Oct_Cell%Level < Level)    &
            CALL OctreeMesh_CellBisect(p2Oct_Cell,OctMesh)
    ENDDO

    ip(1:8)     = pOct_Cell%Nodes(1:8)

    !--- find exsiting points on edges
    CALL OctreeMesh_getCellHangNodes(pOct_Cell,OctMesh,ipHang(1:18))

    !--- for new points

    DO i=1,19
       IF(i==19 .OR. ipHang(i)==0)THEN
          OctMesh%numNodes = OctMesh%numNodes +1
          IF( OctMesh%numNodes>=OctMesh%nallc_nodes)   &
               CALL OctreeMesh_NodeReAllocate(OctMesh)
          ipHang(i) = OctMesh%numNodes

          OctMesh%Posit(:,OctMesh%numNodes) =  ( OctMesh%Posit(:,ip(iEdge_Hex(1,i)))    &
               + OctMesh%Posit(:,ip(iEdge_Hex(2,i))) )/2.D0
       ENDIF
    ENDDO

    pOct_Cell%Bisected = .TRUE.
    ALLOCATE (pOct_Cell%Son1)
    ALLOCATE (pOct_Cell%Son2)
    ALLOCATE (pOct_Cell%Son3)
    ALLOCATE (pOct_Cell%Son4)
    ALLOCATE (pOct_Cell%Son5)
    ALLOCATE (pOct_Cell%Son6)
    ALLOCATE (pOct_Cell%Son7)
    ALLOCATE (pOct_Cell%Son8)

    pOct_Cell%Son1%ID = pOct_Cell%ID
    pOct_Cell%Son2%ID = OctMesh%numCells + 1
    pOct_Cell%Son3%ID = OctMesh%numCells + 2
    pOct_Cell%Son4%ID = OctMesh%numCells + 3
    pOct_Cell%Son5%ID = OctMesh%numCells + 4
    pOct_Cell%Son6%ID = OctMesh%numCells + 5
    pOct_Cell%Son7%ID = OctMesh%numCells + 6
    pOct_Cell%Son8%ID = OctMesh%numCells + 7
    OctMesh%numCells  = OctMesh%numCells + 7

    pOct_Cell%Son1%Nodes(:) = (/ip(1), ipHang(1), ipHang(17), ipHang(5),   &
         ipHang(9), ipHang(15), ipHang(19), ipHang(13) /)
    pOct_Cell%Son2%Nodes(:) = (/ipHang(1), ip(2), ipHang(6), ipHang(17),   &
         ipHang(15), ipHang(10), ipHang(14), ipHang(19) /)
    pOct_Cell%Son3%Nodes(:) = (/ipHang(17), ipHang(6), ip(3), ipHang(2),   &
         ipHang(19), ipHang(14), ipHang(12), ipHang(16) /)
    pOct_Cell%Son4%Nodes(:) = (/ipHang(5), ipHang(17), ipHang(2), ip(4),   &
         ipHang(13), ipHang(19), ipHang(16), ipHang(11) /)
    pOct_Cell%Son5%Nodes(:) = (/ipHang(9), ipHang(15), ipHang(19), ipHang(13),  &
         ip(5), ipHang(3), ipHang(18), ipHang(7) /)
    pOct_Cell%Son6%Nodes(:) = (/ipHang(15), ipHang(10), ipHang(14), ipHang(19),  &
         ipHang(3), ip(6), ipHang(8), ipHang(18) /)
    pOct_Cell%Son7%Nodes(:) = (/ipHang(19), ipHang(14), ipHang(12), ipHang(16),  &
         ipHang(18), ipHang(8), ip(7), ipHang(4) /)
    pOct_Cell%Son8%Nodes(:) = (/ipHang(13), ipHang(19), ipHang(16), ipHang(11),  &
         ipHang(7), ipHang(18), ipHang(4), ip(8) /)

    DO i=1,8
       p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,i)
       p2Oct_Cell%Level = Level + 1
    ENDDO


    pOct_Cell%Son1%Up    => pOct_Cell%Son5
    pOct_Cell%Son2%Up    => pOct_Cell%Son6
    pOct_Cell%Son3%Up    => pOct_Cell%Son7
    pOct_Cell%Son4%Up    => pOct_Cell%Son8
    pOct_Cell%Son5%Down  => pOct_Cell%Son1
    pOct_Cell%Son6%Down  => pOct_Cell%Son2
    pOct_Cell%Son7%Down  => pOct_Cell%Son3
    pOct_Cell%Son8%Down  => pOct_Cell%Son4

    pOct_Cell%Son1%Right => pOct_Cell%Son2
    pOct_Cell%Son4%Right => pOct_Cell%Son3
    pOct_Cell%Son5%Right => pOct_Cell%Son6
    pOct_Cell%Son8%Right => pOct_Cell%Son7
    pOct_Cell%Son2%Left  => pOct_Cell%Son1
    pOct_Cell%Son3%Left  => pOct_Cell%Son4
    pOct_Cell%Son6%Left  => pOct_Cell%Son5
    pOct_Cell%Son7%Left  => pOct_Cell%Son8

    pOct_Cell%Son1%Back  => pOct_Cell%Son4
    pOct_Cell%Son2%Back  => pOct_Cell%Son3
    pOct_Cell%Son5%Back  => pOct_Cell%Son8
    pOct_Cell%Son6%Back  => pOct_Cell%Son7
    pOct_Cell%Son4%Front => pOct_Cell%Son1
    pOct_Cell%Son3%Front => pOct_Cell%Son2
    pOct_Cell%Son8%Front => pOct_Cell%Son5
    pOct_Cell%Son7%Front => pOct_Cell%Son6

    !--- Left side

    p2Oct_Cell => pOct_Cell%Left
    IF(p2Oct_Cell%level == Level .OR. p2Oct_Cell%level==0)THEN
       pOct_Cell%Son1%Left  => p2Oct_Cell
       pOct_Cell%Son4%Left  => p2Oct_Cell
       pOct_Cell%Son5%Left  => p2Oct_Cell
       pOct_Cell%Son8%Left  => p2Oct_Cell
       IF(p2Oct_Cell%level>0)  p2Oct_Cell%Right => pOct_Cell%Son8
    ELSE IF(p2Oct_Cell%level == Level+1)THEN
       p3Oct_Cell           => p2Oct_Cell
       pOct_Cell%Son1%Left  => p3Oct_Cell
       p3Oct_Cell    %Right => pOct_Cell%Son1
       p3Oct_Cell           => p2Oct_Cell%Back
       pOct_Cell%Son4%Left  => p3Oct_Cell
       p3Oct_Cell    %Right => pOct_Cell%Son4
       p3Oct_Cell           => p2Oct_Cell%Up
       pOct_Cell%Son5%Left  => p3Oct_Cell
       p3Oct_Cell    %Right => pOct_Cell%Son5
       p3Oct_Cell           => p2Oct_Cell%Up%Back
       pOct_Cell%Son8%Left  => p3Oct_Cell
       p3Oct_Cell    %Right => pOct_Cell%Son8
    ELSE
       WRITE(*,*)'Error--- level & left level', Level,p2Oct_Cell%level
       CALL Error_STOP ( '--- OctreeMesh_CellBisect')
    ENDIF

    !--- Front side

    p2Oct_Cell => pOct_Cell%Front
    IF(p2Oct_Cell%level == Level .OR. p2Oct_Cell%level==0)THEN
       pOct_Cell%Son1%Front => p2Oct_Cell
       pOct_Cell%Son2%Front => p2Oct_Cell
       pOct_Cell%Son5%Front => p2Oct_Cell
       pOct_Cell%Son6%Front => p2Oct_Cell
       IF(p2Oct_Cell%level>0)  p2Oct_Cell%Back  => pOct_Cell%Son6
    ELSE IF(p2Oct_Cell%level == Level+1)THEN
       p3Oct_Cell           => p2Oct_Cell
       pOct_Cell%Son1%Front => p3Oct_Cell
       p3Oct_Cell    %Back  => pOct_Cell%Son1
       p3Oct_Cell           => p2Oct_Cell%Right
       pOct_Cell%Son2%Front => p3Oct_Cell
       p3Oct_Cell    %Back  => pOct_Cell%Son2
       p3Oct_Cell           => p2Oct_Cell%Up
       pOct_Cell%Son5%Front => p3Oct_Cell
       p3Oct_Cell    %Back  => pOct_Cell%Son5
       p3Oct_Cell           => p2Oct_Cell%Up%Right
       pOct_Cell%Son6%Front => p3Oct_Cell
       p3Oct_Cell    %Back  => pOct_Cell%Son6
    ELSE
       WRITE(*,*)'Error--- level & front level', Level,p2Oct_Cell%level
       CALL Error_STOP ( '--- OctreeMesh_CellBisect')
    ENDIF

    !--- Down  side

    p2Oct_Cell => pOct_Cell%Down 
    IF(p2Oct_Cell%level == Level .OR. p2Oct_Cell%level==0)THEN
       pOct_Cell%Son1%Down  => p2Oct_Cell
       pOct_Cell%Son2%Down  => p2Oct_Cell
       pOct_Cell%Son3%Down  => p2Oct_Cell
       pOct_Cell%Son4%Down  => p2Oct_Cell
       IF(p2Oct_Cell%level>0)  p2Oct_Cell%Up    => pOct_Cell%Son3
    ELSE IF(p2Oct_Cell%level == Level+1)THEN
       p3Oct_Cell           => p2Oct_Cell
       pOct_Cell%Son1%Down  => p3Oct_Cell
       p3Oct_Cell    %Up    => pOct_Cell%Son1
       p3Oct_Cell           => p2Oct_Cell%Right
       pOct_Cell%Son2%Down  => p3Oct_Cell
       p3Oct_Cell    %Up    => pOct_Cell%Son2
       p3Oct_Cell           => p2Oct_Cell%Back
       pOct_Cell%Son4%Down  => p3Oct_Cell
       p3Oct_Cell    %Up    => pOct_Cell%Son4
       p3Oct_Cell           => p2Oct_Cell%Back%Right
       pOct_Cell%Son3%Down  => p3Oct_Cell
       p3Oct_Cell    %Up    => pOct_Cell%Son3
    ELSE
       WRITE(*,*)'Error--- level & down level', Level,p2Oct_Cell%level
       CALL Error_STOP ( '--- OctreeMesh_CellBisect')
    ENDIF

    !--- Right side

    p2Oct_Cell => pOct_Cell%Right
    IF(p2Oct_Cell%level == Level .OR. p2Oct_Cell%level==0)THEN
       pOct_Cell%Son2%Right => p2Oct_Cell
       pOct_Cell%Son3%Right => p2Oct_Cell
       pOct_Cell%Son6%Right => p2Oct_Cell
       pOct_Cell%Son7%Right => p2Oct_Cell
       IF(p2Oct_Cell%level>0)  p2Oct_Cell%Left  => pOct_Cell%Son2
    ELSE IF(p2Oct_Cell%level == Level+1)THEN
       p3Oct_Cell           => p2Oct_Cell%Front%Down
       pOct_Cell%Son2%Right => p3Oct_Cell
       p3Oct_Cell    %Left  => pOct_Cell%Son2
       p3Oct_Cell           => p2Oct_Cell%Down
       pOct_Cell%Son3%Right => p3Oct_Cell
       p3Oct_Cell    %Left  => pOct_Cell%Son3
       p3Oct_Cell           => p2Oct_Cell%Front
       pOct_Cell%Son6%Right => p3Oct_Cell
       p3Oct_Cell    %Left  => pOct_Cell%Son6
       p3Oct_Cell           => p2Oct_Cell
       pOct_Cell%Son7%Right => p3Oct_Cell
       p3Oct_Cell    %Left  => pOct_Cell%Son7
    ELSE
       WRITE(*,*)'Error--- level & right level', Level,p2Oct_Cell%level
       CALL Error_STOP ( '--- OctreeMesh_CellBisect')
    ENDIF

    !--- Back side

    p2Oct_Cell => pOct_Cell%Back 
    IF(p2Oct_Cell%level == Level .OR. p2Oct_Cell%level==0)THEN
       pOct_Cell%Son3%Back  => p2Oct_Cell
       pOct_Cell%Son4%Back  => p2Oct_Cell
       pOct_Cell%Son7%Back  => p2Oct_Cell
       pOct_Cell%Son8%Back  => p2Oct_Cell
       IF(p2Oct_Cell%level>0)  p2Oct_Cell%Front => pOct_Cell%Son4
    ELSE IF(p2Oct_Cell%level == Level+1)THEN
       p3Oct_Cell           => p2Oct_Cell%Left%Down
       pOct_Cell%Son4%Back  => p3Oct_Cell
       p3Oct_Cell    %Front => pOct_Cell%Son4
       p3Oct_Cell           => p2Oct_Cell%Down
       pOct_Cell%Son3%Back  => p3Oct_Cell
       p3Oct_Cell    %Front => pOct_Cell%Son3
       p3Oct_Cell           => p2Oct_Cell%Left
       pOct_Cell%Son8%Back  => p3Oct_Cell
       p3Oct_Cell    %Front => pOct_Cell%Son8
       p3Oct_Cell           => p2Oct_Cell
       pOct_Cell%Son7%Back  => p3Oct_Cell
       p3Oct_Cell    %Front => pOct_Cell%Son7
    ELSE
       WRITE(*,*)'Error--- level & back level', Level,p2Oct_Cell%level
       CALL Error_STOP ( '--- OctreeMesh_CellBisect')
    ENDIF

    !--- Up   side

    p2Oct_Cell => pOct_Cell%Up   
    IF(p2Oct_Cell%level == Level .OR. p2Oct_Cell%level==0)THEN
       pOct_Cell%Son5%Up    => p2Oct_Cell
       pOct_Cell%Son6%Up    => p2Oct_Cell
       pOct_Cell%Son7%Up    => p2Oct_Cell
       pOct_Cell%Son8%Up    => p2Oct_Cell
       IF(p2Oct_Cell%level>0)  p2Oct_Cell%Down  => pOct_Cell%Son5
    ELSE IF(p2Oct_Cell%level == Level+1)THEN
       p3Oct_Cell           => p2Oct_Cell%Left%Front
       pOct_Cell%Son5%Up    => p3Oct_Cell
       p3Oct_Cell    %Down  => pOct_Cell%Son5
       p3Oct_Cell           => p2Oct_Cell%Front
       pOct_Cell%Son6%Up    => p3Oct_Cell
       p3Oct_Cell    %Down  => pOct_Cell%Son6
       p3Oct_Cell           => p2Oct_Cell%Left
       pOct_Cell%Son8%Up    => p3Oct_Cell
       p3Oct_Cell    %Down  => pOct_Cell%Son8
       p3Oct_Cell           => p2Oct_Cell
       pOct_Cell%Son7%Up    => p3Oct_Cell
       p3Oct_Cell    %Down  => pOct_Cell%Son7
    ELSE
       WRITE(*,*)'Error--- level & up level', Level,p2Oct_Cell%level
       CALL Error_STOP ( '--- OctreeMesh_CellBisect')
    ENDIF

    RETURN
  END SUBROUTINE OctreeMesh_CellBisect

  !>
  !!     Split a cell until all offspring leaf cells have a level equal 
  !!           to a given value if no higher.
  !<
  RECURSIVE SUBROUTINE OctreeMesh_CellMatchLevel(pOct_Cell,Level,OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    INTEGER, INTENT(IN) :: Level
    INTEGER :: idirct
    TYPE (OctreeMesh_Cell_Type), POINTER ::pOct_Cell,p2Oct_Cell

    IF(pOct_Cell%Level == 0) RETURN      !--- Empty Cell
    IF(pOct_Cell%Level>=Max_Oct_Level) RETURN
    IF(pOct_Cell%Level>=Level) RETURN

    IF(pOct_Cell%Bisected)THEN
       DO idirct=1,8
          p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
          CALL OctreeMesh_CellMatchLevel(p2Oct_Cell,Level,OctMesh)
       ENDDO
    ELSE
       CALL OctreeMesh_CellBisect(pOct_Cell,OctMesh)
       CALL OctreeMesh_CellMatchLevel(pOct_Cell,Level,OctMesh)
    ENDIF

    RETURN
  END SUBROUTINE OctreeMesh_CellMatchLevel

  !>
  !!     return a pointer associated with a cell with a specifing level
  !!            which contains a given point. 
  !!            If level is given too high, return a leaf cell.
  !!     @param[in] pp  the position of a point.
  !!     @param[in] Level  the level to reach.
  !!     @param[in] OctMesh the octree mesh.
  !<
  FUNCTION OctreeMesh_CellSearch(pp,Level,OctMesh) RESULT(pOct_Cell)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(IN) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell
    REAL*8,  INTENT(IN) :: pp(3)
    INTEGER, INTENT(IN) :: Level
    REAL*8  :: pp1(3)
    INTEGER :: Lev,i0,j0,k0,idirct

    i0 = (pp(1) - OctMesh%Pmin(1)) / OctMesh%DX(1) +1
    j0 = (pp(2) - OctMesh%Pmin(2)) / OctMesh%DY(1) +1
    k0 = (pp(3) - OctMesh%Pmin(3)) / OctMesh%DZ(1) +1
    IF( i0>OctMesh%imax .OR. j0>OctMesh%jmax .OR. k0>OctMesh%kmax .OR.  &
         i0<=0 .OR. j0<=0 .OR. k0<=0 )THEN
       pOct_Cell => OctreeMesh_EmptyCell
       RETURN
    ENDIF

    Lev = Level
    IF(Lev<0 .OR. Lev>Max_Oct_Level) Lev=Max_Oct_Level

    pOct_Cell => OctMesh%Cells(i0,j0,k0)
    DO WHILE(pOct_Cell%Bisected .AND. pOct_Cell%Level<Lev .AND. pOct_Cell%Level>0)
       pp1(:) = pp(:) - OctMesh%Posit(:,pOct_Cell%Son1%Nodes(7))
       idirct = 1
       IF(pp1(3)>0) idirct = 5
       IF(pp1(1)>0)THEN
          IF(pp1(2)>0)THEN
             idirct = idirct + 2
          ELSE
             idirct = idirct + 1
          ENDIF
       ELSE
          IF(pp1(2)>0) idirct = idirct + 3
       ENDIF
       pOct_Cell => OctreeMesh_SonCell(pOct_Cell, idirct)
    ENDDO

    RETURN
  END FUNCTION OctreeMesh_CellSearch

  !>
  !!     reutrn the centre position of a cell
  !<
  FUNCTION OctreeMesh_getCellCentre(pOct_Cell,OctMesh) RESULT(pp)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(IN) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell
    REAL*8 :: pp(3)

    IF(pOct_Cell%Bisected)THEN
       pp = OctMesh%Posit(:,pOct_Cell%Son1%Nodes(7))
    ELSE
       pp = ( OctMesh%Posit(:,pOct_Cell%Nodes(1))   &
            + OctMesh%Posit(:,pOct_Cell%Nodes(7)) ) / 2.d0
    ENDIF

    RETURN
  END FUNCTION OctreeMesh_getCellCentre

  !>
  !!     Mark all leaf cells under pOct_Cell
  !<
  RECURSIVE SUBROUTINE OctreeMesh_CellMark(pOct_Cell,CellMark,Mark)
    IMPLICIT NONE
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    INTEGER :: CellMark(*)
    INTEGER, INTENT(IN) :: Mark
    INTEGER :: ip(8), idirct

    IF(pOct_Cell%Level==0) RETURN
    IF(pOct_Cell%ID<=0)    RETURN
    IF(pOct_Cell%Bisected)THEN
       DO idirct=1,8
          p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
          CALL OctreeMesh_CellMark(p2Oct_Cell,CellMark,Mark)
       ENDDO
    ELSE
       CellMark(pOct_Cell%ID) = Mark
    ENDIF
  END SUBROUTINE OctreeMesh_CellMark

  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE OctreeMesh_Cell_Output(IO, pOct_Cell)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: IO
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    INTEGER :: idirct, Idir, ip(8),i,ii,iface(5,3)

    WRITE(IO,err=111) pOct_Cell%ID, pOct_Cell%Level, pOct_Cell%Bisected
    WRITE(IO,err=111) pOct_Cell%Nodes(1:8)    

    IF(pOct_Cell%Bisected)THEN
       DO idirct=1,8
          p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
          CALL OctreeMesh_Cell_Output(IO,p2Oct_Cell)
       ENDDO
    ENDIF

    RETURN
111 WRITE(*,*) 'Error--- OctreeMesh_Cell_Output:'
    CALL Error_STOP (' ')
  END SUBROUTINE OctreeMesh_Cell_Output

  !>
  !!     Output the mesh through a IO channel.
  !<
  SUBROUTINE OctreeMesh_Output(IO,OctMesh)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: IO
    TYPE(OctreeMesh_Type), INTENT(IN) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell
    INTEGER :: i, j, k, ip

    WRITE(IO,err=111) OctMesh%theName
    WRITE(IO,err=111) OctMesh%numNodes, OctMesh%numCells,    &
                      OctMesh%imax, OctMesh%jmax, OctMesh%kmax
    WRITE(IO,err=111) OctMesh%DX(1:Max_Oct_Level+1)
    WRITE(IO,err=111) OctMesh%DY(1:Max_Oct_Level+1)
    WRITE(IO,err=111) OctMesh%DZ(1:Max_Oct_Level+1)
    WRITE(IO,err=111) OctMesh%Pmin(:)
    WRITE(IO,err=111) OctMesh%Pmax(:)
    WRITE(IO,err=111) OctMesh%TinyD(:)
    WRITE(IO,err=111) (OctMesh%Posit(:,ip), ip=1,OctMesh%numNodes)

    DO i=1,OctMesh%imax
       DO j=1,OctMesh%jmax
          DO k=1,OctMesh%kmax
             pOct_Cell => OctMesh%Cells(i,j,k)
             CALL OctreeMesh_Cell_Output(IO,pOct_Cell)
          ENDDO
       ENDDO
    ENDDO

    RETURN
111 WRITE(*,*) 'Error--- OctreeMesh_Output: '
    CALL Error_STOP (' ')

  END SUBROUTINE OctreeMesh_Output

  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE OctreeMesh_Cell_Input(IO,pOct_Cell)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: IO
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    INTEGER :: idirct

    READ(IO,err=111) pOct_Cell%ID, pOct_Cell%Level, pOct_Cell%Bisected
    READ(IO,err=111) pOct_Cell%Nodes(1:8)    

    IF(pOct_Cell%Bisected)THEN
       ALLOCATE (pOct_Cell%Son1)
       ALLOCATE (pOct_Cell%Son2)
       ALLOCATE (pOct_Cell%Son3)
       ALLOCATE (pOct_Cell%Son4)
       ALLOCATE (pOct_Cell%Son5)
       ALLOCATE (pOct_Cell%Son6)
       ALLOCATE (pOct_Cell%Son7)
       ALLOCATE (pOct_Cell%Son8)

       DO idirct=1,8
          p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
          CALL OctreeMesh_Cell_Input(IO,p2Oct_Cell)
       ENDDO
    ENDIF

    RETURN
111 WRITE(*,*) 'Error--- OctreeMesh_Cell_Input: '
    CALL Error_STOP (' ')

  END SUBROUTINE OctreeMesh_Cell_Input

  !>
  !!     Input the mesh through a IO channel.
  !<
  SUBROUTINE OctreeMesh_Input(IO,OctMesh)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: IO
    TYPE(OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell
    INTEGER :: i, j, k, ip

    READ(IO,err=111) OctMesh%theName
    READ(IO,err=111) OctMesh%numNodes, OctMesh%numCells,    &
                     OctMesh%imax, OctMesh%jmax, OctMesh%kmax
    READ(IO,err=111) OctMesh%DX(1:Max_Oct_Level+1)
    READ(IO,err=111) OctMesh%DY(1:Max_Oct_Level+1)
    READ(IO,err=111) OctMesh%DZ(1:Max_Oct_Level+1)
    READ(IO,err=111) OctMesh%Pmin(:)
    READ(IO,err=111) OctMesh%Pmax(:)
    READ(IO,err=111) OctMesh%TinyD(:)

    OctMesh%nallc_nodes = OctMesh%numNodes + nallc_Oct_Increase
    CALL OctreeMesh_NodeAllocate(OctMesh)
    ALLOCATE (OctMesh%Cells(OctMesh%imax, OctMesh%jmax, OctMesh%kmax))

    READ(IO,err=111) (OctMesh%Posit(:,ip), ip=1,OctMesh%numNodes)

    DO i=1,OctMesh%imax
       DO j=1,OctMesh%jmax
          DO k=1,OctMesh%kmax
             pOct_Cell => OctMesh%Cells(i,j,k)
             CALL OctreeMesh_Cell_Input(IO,pOct_Cell)
          ENDDO
       ENDDO
    ENDDO

    
    CALL OctreeMesh_AssoNeighbour(OctMesh)

    RETURN
111 WRITE(*,*) 'Error--- OctreeMesh_Input: '
    CALL Error_STOP (' ')
  END SUBROUTINE OctreeMesh_Input

  !>
  !!  Write a .geo file for EnSight.
  !!  @param[in]  JobName        the name (prefix) of input file.
  !!  @param[in]  JobNameLength  the length of JobName.
  !!  @param[in]  OctMesh        the octree mesh.
  !<
  SUBROUTINE OctreeMesh_OutputEnSight(JobName,JobNameLength,OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    INTEGER :: JobNameLength
    CHARACTER(LEN=JobNameLength) JobName
    INTEGER :: i,j,is
    REAL*4,  DIMENSION(:,:), ALLOCATABLE :: xyz
    CHARACTER*80 :: string

    OPEN(11,file=JobName(1:JobNameLength)//'.geo',    &
         status='UNKNOWN',form='UNFORMATTED',err=111)

    CALL OctreeMesh_getLeafCells(OctMesh)
    ALLOCATE (xyz(3,OctMesh%numNodes))
    DO i=1,OctMesh%numNodes
       xyz(:,i) = OctMesh%Posit(:,i)
    ENDDO
    
    string = 'Fortran Binary' 
    WRITE(11,err=111) string
    string = 'ENSIGHT'
    WRITE(11,err=111) string
    string = 'GEO FILE'
    WRITE(11,err=111) string
    string = 'node id off'
    WRITE(11,err=111) string
    string = 'element id off'
    WRITE(11,err=111) string
    string = 'part'
    WRITE(11,err=111) string
    is = 1 
    WRITE(11,err=111) is
    string = 'Volume Mesh'
    WRITE(11,err=111) string
    
    string = 'coordinates'
    WRITE(11,err=111) string
    WRITE(11,err=111) OctMesh%numNodes
    WRITE(11,err=111) xyz(1,1:OctMesh%numNodes)
    WRITE(11,err=111) xyz(2,1:OctMesh%numNodes)
    WRITE(11,err=111) xyz(3,1:OctMesh%numNodes)

    string = 'hexa8'
    WRITE(11,err=111) string
    WRITE(11,err=111) OctMesh%numCells 
    WRITE(11,err=111) ((OctMesh%LeafCells(i)%to%Nodes(j),j=1,8), i=1,OctMesh%numCells)
    CLOSE(11)
    
    DEALLOCATE (xyz)
    RETURN
111 WRITE(*,*) 'Error--- OctreeMesh_OutputEnSight: ',      &
         JobName(1:JobNameLength)//'.geo'
    CALL Error_STOP (' ')
  END SUBROUTINE OctreeMesh_OutputEnSight

  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE OctreeMesh_CellDraw2D(pOct_Cell,Idir,OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(IN) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    INTEGER :: idirct, Idir, ip(8),i,ii,iface(5,3)

    IF(pOct_Cell%Level==0) RETURN
    IF(pOct_Cell%Bisected)THEN
       DO idirct=1,8
          IF(Idir==1 .AND. (idirct==2 .OR.idirct==3 .OR.idirct==6 .OR.idirct==7) ) CYCLE
          IF(Idir==2 .AND. (idirct==3 .OR.idirct==4 .OR.idirct==7 .OR.idirct==8) ) CYCLE
          IF(Idir==3 .AND. idirct>=5         ) CYCLE
          p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
          CALL OctreeMesh_CellDraw2D(p2Oct_Cell,Idir,OctMesh)
       ENDDO
    ELSE
       ip(1:8) = pOct_Cell%Nodes(1:8)
       iface(:,1) = (/1,4,8,5,1/)
       iface(:,2) = (/1,2,6,5,1/)
       iface(:,3) = (/1,2,3,4,1/)
       DO ii=1,5
          i = iface(ii,Idir)
          WRITE(29,'(3E14.6,3E11.3)')OctMesh%Posit(:,ip(i)),ip(i)
       ENDDO
       WRITE(29,*)' '
       WRITE(29,*)' '
    ENDIF

    RETURN
  END SUBROUTINE OctreeMesh_CellDraw2D

  !>
  !!    Output a cut to file for gnuplot.
  !!    if Idir=1, draw the net by I=IJK;                                        
  !!    if Idir=2, draw the net by J=IJK;                                 
  !!    if Idir=3, draw the net by K=IJK.                                 
  !<
  SUBROUTINE OctreeMesh_Draw2D(JobName,JobNameLength,Idir,IJK,OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(IN) :: OctMesh
    INTEGER :: JobNameLength
    CHARACTER(LEN=JobNameLength) JobName
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell
    INTEGER :: Idir, IJK
    INTEGER :: i,j,k,i1,i2,j1,j2,k1,k2

    i1 = 1
    j1 = 1
    k1 = 1
    i2 = OctMesh%imax
    j2 = OctMesh%jmax
    k2 = OctMesh%kmax
    IF(Idir==1)THEN
       i1 = IJK
       i2 = IJK
       OPEN(29,file=JobName(1:JobNameLength), status='unknown')
    ELSE IF(Idir==2)THEN
       j1 = IJK
       j2 = IJK
       OPEN(29,file=JobName(1:JobNameLength), status='unknown')
    ELSE  IF(Idir==3)THEN
       k1 = IJK
       k2 = IJK
       OPEN(29,file=JobName(1:JobNameLength), status='unknown')
    ENDIF

    DO i=i1,i2
       DO j=j1,j2
          DO k=k1,k2
             pOct_Cell => OctMesh%Cells(i,j,k)
             CALL OctreeMesh_CellDraw2D(pOct_Cell,Idir,OctMesh)
          ENDDO
       ENDDO
    ENDDO

    CLOSE(29)

    RETURN
  END SUBROUTINE OctreeMesh_Draw2D

  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE OctreeMesh_CellCheck(pOct_Cell,OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(IN) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    INTEGER :: ip(4),iq(4),ierror,i,i2
    REAL*8  :: pp(3)

    IF(pOct_Cell%Level==0) RETURN
    IF(pOct_Cell%Bisected)THEN
       DO i=1,8
          p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,i)
          CALL OctreeMesh_CellCheck(p2Oct_Cell,OctMesh)
       ENDDO
    ELSE

       !--- check nodes and cell_id
       IF(pOct_Cell%ID<=0 .OR. pOct_Cell%ID>OctMesh%numCells)THEN
          WRITE(*,*)'Error--- id check:'
          WRITE(*,*)'ID=', pOct_Cell%ID
          CALL Error_STOP ( '--- OctreeMesh_CellCheck 3')
       ENDIF
       DO i=1,8
          IF(pOct_Cell%Nodes(i)<=0 .OR. pOct_Cell%Nodes(i)>OctMesh%numNodes)THEN
             WRITE(*,*)'Error--- node check:'
             WRITE(*,*)'Nodes=', pOct_Cell%Nodes(1:8)
             CALL Error_STOP ( '--- OctreeMesh_CellCheck 4')
          ENDIF
       ENDDO

       !--- check connectivity with neighbours

       ierror = 0
       DO i=1,6
          IF(i==1) i2=2
          IF(i==2) i2=1
          IF(i==3) i2=4
          IF(i==4) i2=3
          IF(i==5) i2=6
          IF(i==6) i2=5
          p2Oct_Cell => OctreeMesh_NextCell(pOct_Cell,i)
          IF(p2Oct_Cell%Level==0) CYCLE

          ip(1:4) =  pOct_Cell%Nodes(iQuadFD_Hex(1:4,i))
          iq(1:4) = p2Oct_Cell%Nodes(iQuadFD_Hex(1:4,i2))

          IF(ABS(p2Oct_Cell%Level - pOct_Cell%Level)>1)THEN
             ierror = 1
          ELSE IF(p2Oct_Cell%Level == pOct_Cell%Level)THEN
             IF(  ip(1)/=iq(1) .OR. ip(2)/=iq(2) .OR.    &
                  ip(3)/=iq(3) .OR. ip(4)/=iq(4) ) THEN
                ierror = 1
             ENDIF
          ELSE IF(p2Oct_Cell%Level == pOct_Cell%Level+1)THEN
             IF( MOD(i,2)==1 .AND. ip(1)/=iq(1) ) ierror = 1
             IF( MOD(i,2)==0 .AND. ip(3)/=iq(3) ) ierror = 1
          ELSE IF(p2Oct_Cell%Level == pOct_Cell%Level-1)THEN
             IF(  ip(1)/=iq(1) .AND. ip(2)/=iq(2) .AND.     &
                  ip(3)/=iq(3) .AND. ip(4)/=iq(4) )THEN
                ierror = 1
             ENDIF
          ENDIF
          IF(ierror==1)THEN
             WRITE(*,*)'Error--- next check: i=',i
             WRITE(*,*)'this ID,Level: ',pOct_Cell%ID,pOct_Cell%Level
             WRITE(*,*)'next ID,Level: ',p2Oct_Cell%ID,p2Oct_Cell%Level
             WRITE(*,*)'this Nodes: ',pOct_Cell%Nodes(1:8)
             WRITE(*,*)'next Nodes: ',p2Oct_Cell%Nodes(1:8)
             CALL Error_STOP ( '--- OctreeMesh_CellCheck 1')
          ENDIF
       ENDDO

       !---Check Nodes positions

       DO i=2,8
          pp(:) = OctMesh%Posit(:,pOct_Cell%Nodes(i))   &
               - OctMesh%Posit(:,pOct_Cell%Nodes(1))
          IF(i==2 .OR. i==3 .OR. i==6 .OR. i==7)THEN
             pp(1) = ABS(pp(1) - OctMesh%DX(pOct_Cell%Level))
          ENDIF
          IF(i==3 .OR. i==4 .OR. i==7 .OR. i==8)THEN
             pp(2) = ABS(pp(2) - OctMesh%DY(pOct_Cell%Level))
          ENDIF
          IF(i>4)THEN
             pp(3) = ABS(pp(3) - OctMesh%DZ(pOct_Cell%Level))
          ENDIF

          IF(  pp(1)>OctMesh%TinyD(1) .OR. pp(2)>OctMesh%TinyD(2) .OR.   &
               pp(3)>OctMesh%TinyD(3) ) THEN
             WRITE(*,*)'Error--- position check:'
             WRITE(*,*)'Level =',pOct_Cell%Level
             WRITE(*,*)'Nodes=', pOct_Cell%Nodes(1:8)
             WRITE(*,*)'Posit1=',OctMesh%Posit(:,pOct_Cell%Nodes(1))
             WRITE(*,*)'Posit2=',OctMesh%Posit(:,pOct_Cell%Nodes(2))
             WRITE(*,*)'Posit3=',OctMesh%Posit(:,pOct_Cell%Nodes(3))
             WRITE(*,*)'Posit4=',OctMesh%Posit(:,pOct_Cell%Nodes(4))
             WRITE(*,*)'Posit5=',OctMesh%Posit(:,pOct_Cell%Nodes(5))
             WRITE(*,*)'Posit6=',OctMesh%Posit(:,pOct_Cell%Nodes(6))
             WRITE(*,*)'Posit7=',OctMesh%Posit(:,pOct_Cell%Nodes(7))
             WRITE(*,*)'Posit8=',OctMesh%Posit(:,pOct_Cell%Nodes(8))
             WRITE(*,*)'dx,dy,dz=',OctMesh%DX(pOct_Cell%Level),   &
                  OctMesh%DY(pOct_Cell%Level), OctMesh%DZ(pOct_Cell%Level)
             CALL Error_STOP ( '--- OctreeMesh_CellCheck 2')
          ENDIF
       ENDDO

    ENDIF

    RETURN
  END SUBROUTINE OctreeMesh_CellCheck

  !>
  !!     check Octree
  !<
  SUBROUTINE OctreeMesh_Check(OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(IN) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell
    INTEGER :: i,j,k
    DO k=1,OctMesh%kmax
       DO j=1,OctMesh%jmax
          DO i=1,OctMesh%imax
             pOct_Cell => OctMesh%Cells(i,j,k)
             CALL OctreeMesh_CellCheck(pOct_Cell,OctMesh)
          ENDDO
       ENDDO
    ENDDO
    WRITE(*,*)'OctMesh: numNodes, numCells=',OctMesh%numNodes, OctMesh%numCells
    WRITE(*,*)' ...pass Oct_Web Checking...'
    RETURN
  END SUBROUTINE OctreeMesh_Check

  !>
  !!     Check the positions of nodes by ADTree
  !<
  SUBROUTINE OctreeMesh_CheckNodes(OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type) :: OctMesh
    INTEGER :: Isucc, Ips(2), ip, NB
    REAL*8 :: HalfSize(3), Dist(2), pt(3)

    CALL PtADTree_SetTree(Oct_PtADTree, OctMesh%Posit, OctMesh%numNodes, OctMesh%TinyD)

    DO ip = 1,OctMesh%numNodes
       pt = OctMesh%Posit(:,ip)
       Isucc = -1
       NB = 2
       CALL PtADTree_SearchNode(Oct_PtADTree,pt,IPs, Dist, NB, Isucc)
       IF(Isucc==1 .AND. NB==1)THEN
       ELSE IF(Isucc==0)THEN
          CALL Error_STOP ( '---1')
       ELSE
          WRITE(*,*)IPs
          CALL Error_STOP ( '---2')
       ENDIF
    ENDDO

    CALL PtADTree_Clear(Oct_PtADTree)
    WRITE(*,*)'--- pass OctreeMesh_CheckNodes'

    RETURN
  END SUBROUTINE OctreeMesh_CheckNodes


  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE OctreeMesh_Cell_ClearNext(pOct_Cell)
    IMPLICIT NONE
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    INTEGER :: idirct
    IF(pOct_Cell%Bisected)THEN
       DO idirct=1,8
          p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
          CALL OctreeMesh_Cell_ClearNext(p2Oct_Cell)
       ENDDO
    ENDIF
    NULLIFY(pOct_Cell%Left, pOct_Cell%Down, pOct_Cell%Back, pOct_Cell%Right, pOct_Cell%Up)
    RETURN
  END SUBROUTINE OctreeMesh_Cell_ClearNext

  !>
  !!     private function
  !<
  RECURSIVE SUBROUTINE OctreeMesh_Cell_ClearSon(pOct_Cell)
    IMPLICIT NONE
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell, p2Oct_Cell
    INTEGER :: idirct
    IF( .NOT. pOct_Cell%Bisected) RETURN
    DO idirct=1,8
       p2Oct_Cell => OctreeMesh_SonCell(pOct_Cell,idirct)
       CALL OctreeMesh_Cell_ClearSon(p2Oct_Cell)
    ENDDO
    DEALLOCATE(pOct_Cell%Son1, pOct_Cell%Son2, pOct_Cell%Son3, pOct_Cell%Son4)
    DEALLOCATE(pOct_Cell%Son5, pOct_Cell%Son6, pOct_Cell%Son7, pOct_Cell%Son8)
    pOct_Cell%Bisected = .FALSE.
    RETURN
  END SUBROUTINE OctreeMesh_Cell_ClearSon

  !>
  !!     Delete the Octree mesh and release memory.
  !<
  SUBROUTINE OctreeMesh_Clear(OctMesh)
    IMPLICIT NONE
    TYPE (OctreeMesh_Type), INTENT(INOUT) :: OctMesh
    TYPE (OctreeMesh_Cell_Type), POINTER :: pOct_Cell
    INTEGER :: i,j,k,id

    IF( ASSOCIATED(OctMesh%LeafCells) )THEN
       DO id=1,OctMesh%numCells
          NULLIFY(OctMesh%LeafCells(id)%to)
       ENDDO
       DEALLOCATE(OctMesh%LeafCells)
    ENDIF

    DO k=1,OctMesh%kmax
       DO j=1,OctMesh%jmax
          DO i=1,OctMesh%imax
             pOct_Cell => OctMesh%Cells(i,j,k)
             CALL OctreeMesh_Cell_ClearNext(pOct_Cell)
          ENDDO
       ENDDO
    ENDDO
    DO k=1,OctMesh%kmax
       DO j=1,OctMesh%jmax
          DO i=1,OctMesh%imax
             pOct_Cell => OctMesh%Cells(i,j,k)
             CALL OctreeMesh_Cell_ClearSon(pOct_Cell)
          ENDDO
       ENDDO
    ENDDO

    OctMesh%numNodes = 0
    OctMesh%numCells = 0
    IF(ASSOCIATED(OctMesh%Posit)) DEALLOCATE(OctMesh%Posit)
    IF(ASSOCIATED(OctMesh%Cells)) DEALLOCATE(OctMesh%Cells)

    RETURN
  END SUBROUTINE OctreeMesh_Clear

  !>
  !!  Output the information of the Octree.
  !!  @param[in] OctMesh : the Octree.
  !!  @param[in] io      : the IO channel. 
  !!                       If =6 or not present, output on screen. 
  !<
  SUBROUTINE OctreeMesh_info(OctMesh, io)
    TYPE(OctreeMesh_Type), INTENT(IN) :: OctMesh
    INTEGER, INTENT(IN),  OPTIONAL :: io
    INTEGER :: ic
    ic = 6
    IF(PRESENT(io)) ic = io
    WRITE(ic,*)'  Information of Octree '
    WRITE(ic,*)'     Name: ', TRIM(OctMesh%theName)
    WRITE(ic,*)'     No. of nodes: ', OctMesh%numNodes, '  no of cells: ', OctMesh%numCells
    WRITE(ic,*)'     Basic cells layout: ',OctMesh%imax, OctMesh%jmax, OctMesh%kmax
    WRITE(ic,*)'     Minimum corner: ', REAL(OctMesh%Pmin)
    WRITE(ic,*)'     Maximum corner: ', REAL(OctMesh%Pmax)
  END SUBROUTINE OctreeMesh_info



END MODULE OctreeMesh


