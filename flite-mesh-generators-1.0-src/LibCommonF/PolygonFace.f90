!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!



!>
!!  Module of type of face.
!<
MODULE PolygonFace
  USE Queue
  IMPLICIT NONE


  !>  A queue of nodes with two other integers.
  TYPE :: PolygonFaceType
     INTEGER :: numNodes = 0
     INTEGER, DIMENSION(:), POINTER :: Nodes => null()
     INTEGER :: Left  = 0
     INTEGER :: Right = 0
     TYPE(IntQueueType) :: List
  END TYPE PolygonFaceType


CONTAINS


  !>  Set the nodes of the polygon face.
  SUBROUTINE PolygonFace_Set(aFace, n, Nodes)
    IMPLICIT NONE
    TYPE (PolygonFaceType), INTENT(INOUT)    :: aFace
    INTEGER, INTENT(IN) :: n, Nodes(*)
    CALL IntQueue_Set(aFace%List, n, Nodes)
    aFace%numNodes =  aFace%List%numNodes
    aFace%Nodes    => aFace%List%Nodes
  END SUBROUTINE PolygonFace_Set

  !>  Return .true. if the the polygon face contains the given node.
  !!  otherwise, return .false.
  FUNCTION PolygonFace_Contain(aFace, Node) RESULT(TF)
    IMPLICIT NONE
    TYPE (PolygonFaceType), INTENT(IN)    :: aFace
    INTEGER, INTENT(IN) :: Node
    LOGICAL :: TF
    TF = IntQueue_Contain(aFace%List, Node)
  END FUNCTION PolygonFace_Contain

  !>  Add a node to the face.
  SUBROUTINE PolygonFace_AddNode(aFace, Node)
    IMPLICIT NONE
    TYPE (PolygonFaceType), INTENT(INOUT)    :: aFace
    INTEGER, INTENT(IN) :: Node
    CALL IntQueue_Push(aFace%List, Node)
    aFace%numNodes =  aFace%List%numNodes
    aFace%Nodes    => aFace%List%Nodes
  END SUBROUTINE PolygonFace_AddNode


  !>
  !!   Remove a node from the face. 
  !!   The order of entries is crushed.
  !<
  SUBROUTINE PolygonFace_RemoveNode(aFace, Node, Isucc)
    IMPLICIT NONE
    TYPE (PolygonFaceType), INTENT(INOUT)    :: aFace
    INTEGER, INTENT(IN) :: Node
    INTEGER, OPTIONAL, INTENT(OUT) :: Isucc
    IF(PRESENT(Isucc))THEN
       CALL IntQueue_Remove(aFace%List, Node, Isucc)
    ELSE
       CALL IntQueue_Remove(aFace%List, Node)
    ENDIF
    aFace%numNodes =  aFace%List%numNodes
    aFace%Nodes    => aFace%List%Nodes    
  END SUBROUTINE PolygonFace_RemoveNode



  !>
  !!  Copy a PolygonFaceType object to another.    \n
  !!  Reminder: NEVER copy a object to itself.
  !<
  SUBROUTINE PolygonFace_Copy(SourceFace, TargetFace)
    IMPLICIT NONE
    TYPE (PolygonFaceType), INTENT(IN)    :: SourceFace
    TYPE (PolygonFaceType), INTENT(INOUT) :: TargetFace
    CALL IntQueue_Copy(SourceFace%List, TargetFace%List)
    TargetFace%numNodes =  TargetFace%List%numNodes
    TargetFace%Nodes    => TargetFace%List%Nodes    
    TargetFace%Left     = SourceFace%Left
    TargetFace%Right    = SourceFace%Right
  END SUBROUTINE PolygonFace_Copy


  SUBROUTINE PolygonFace_Write(aFace, io)
    IMPLICIT NONE
    TYPE (PolygonFaceType), INTENT(IN)    :: aFace
    INTEGER, INTENT(IN) :: io
    CALL IntQueue_Write(aFace%List, io)
    WRITE(io,*)'L-R=',aFace%Left, aFace%Right
  END SUBROUTINE PolygonFace_Write
  

  SUBROUTINE PolygonFace_Clear(aFace)
    IMPLICIT NONE
    TYPE (PolygonFaceType), INTENT(INOUT) :: aFace
    NULLIFY(aFace%Nodes)
    aFace%numNodes = 0
    aFace%Left     = 0
    aFace%Right    = 0
    CALL IntQueue_Clear(aFace%List)
  END  SUBROUTINE PolygonFace_Clear



  SUBROUTINE allc_PolygonFace(Face, m, message)
    IMPLICIT NONE
    TYPE (PolygonFaceType), DIMENSION(:), POINTER :: Face, temp
    INTEGER, INTENT(IN) :: m
    CHARACTER*(*), INTENT(in), OPTIONAL :: message
    INTEGER :: m1, i

    m1 = 0
    IF(ASSOCIATED(Face))THEN
       m1 = MIN(m,SIZE(Face))
       ALLOCATE(temp(m1))  
       DO i = 1,m1
          IF(ASSOCIATED(Face(i)%Nodes))THEN
             CALL PolygonFace_Copy(Face(i), temp(i))
             DEALLOCATE(Face(i)%Nodes)
          ENDIF
       ENDDO
       DEALLOCATE(Face)
    ENDIF

    ALLOCATE(Face(m))

    IF(m1>0)THEN
       DO i = 1,m1
          IF(ASSOCIATED(temp(i)%Nodes))THEN
             CALL PolygonFace_Copy(temp(i), Face(i))
             DEALLOCATE(temp(i)%Nodes)
          ENDIF
       ENDDO
       DEALLOCATE(temp)
    ENDIF

  END SUBROUTINE allc_PolygonFace


  SUBROUTINE deallc_PolygonFace(Face, message)
    IMPLICIT NONE
    TYPE (PolygonFaceType), DIMENSION(:), POINTER :: Face
    CHARACTER*(*), INTENT(in), OPTIONAL :: message
    INTEGER :: m1, i     
    IF(ASSOCIATED(Face))THEN
       m1 = SIZE(Face)
       DO i = 1,m1
          CALL PolygonFace_Clear(Face(i))
       ENDDO
       DEALLOCATE(Face)
    ENDIF
  END  SUBROUTINE deallc_PolygonFace

END MODULE PolygonFace

