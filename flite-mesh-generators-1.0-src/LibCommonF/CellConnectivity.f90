!
!  Copyright (C) 2017 College of Engineering, Swansea University
!
!  This file is part of the SwanSim FLITE suite of tools.
!
!  SwanSim FLITE is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  SwanSim FLITE is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this SwanSim FLITE product. 
!  If not, see <http://www.gnu.org/licenses/>.
!


!******************************************************************************!
!
!  Order of nodes in a Hex cell and prism cell
!                                                    6
!                         8________7                /\
!                         /|      /|               /| \ 
!   y                  5 /_______6 |             4/____\ 5
!   |   z                | |     | |              | |   |
!   |  /                 | |     | |              | |3  |
!   | /                  | |4____|_|3             | /\  |
!   |/                   |/      | /              |/  \ |
!   |______ x            |_______|/               |____\|
!                       1         2              1       2
!
!
!******************************************************************************!
!>
!!   The node collection (face, edge, etc.) of 4 types of bacis cells.       \n
!!   The connectivities of a Hexahedron, a Prism, a Pyramid and a Tetrahedron 
!!      are set as:   \image html cells.jpg
!!
!!   The connectivities of a cubic Triangle and a cubic Tetrahedron 
!!      are set as:   \image html cell_high.jpg
!<
MODULE CellConnectivity
  IMPLICIT NONE


  INTEGER, PARAMETER :: Npoint_Seg(5) = (/2,  3,  4,  5,  6/)   !<   number of points for a segment     with each order.
  INTEGER, PARAMETER :: Npoint_Tri(5) = (/3,  6, 10, 15, 21/)   !<   number of points for a triangle    with each order.
  INTEGER, PARAMETER :: Npoint_Tet(5) = (/4, 10, 20, 35, 56/)   !<   number of points for a tetrahedron with each order.


  !>   4 nodes of 6 quad faces of a Hex with orientation inside
  INTEGER, PARAMETER :: iQuad_Hex(4,6) =               &
       reshape( (/1,4,8,5, 2,6,7,3, 1,5,6,2,           &
                  4,3,7,8, 1,2,3,4, 5,8,7,6/), (/4,6/) )
  !>   4 nodes of 6 quad faces of a Hex with orientation to x+/y+/z+ (fix direction)
  INTEGER, PARAMETER :: iQuadFD_Hex(4,6) =             &
       reshape( (/1,4,8,5, 2,3,7,6, 1,5,6,2,           &
                  4,8,7,3, 1,2,3,4, 5,6,7,8/), (/4,6/) )
  !>   4 nodes of 6 quad faces of a Hex with orientation to x+/y+/z+ (fix direction)
  INTEGER, PARAMETER :: iQuadFD2_Hex(4,-3:3) =         &
       reshape( (/1,2,3,4, 1,5,6,2, 1,4,8,5,           &
                  0,0,0,0, 2,3,7,6, 4,8,7,3,           &
                  5,6,7,8/), (/4,7/) )

  !>   3 nodes of a triangular face when a node being cut off, 
  !!   orientating to this node
  INTEGER, PARAMETER :: iCut_Hex(3,8) =                &
       reshape( (/2,5,4, 1,3,6, 2,4,7, 1,8,3,          &
                  1,6,8, 2,7,5, 3,8,6, 4,5,7/), (/3,8/) )

  !>   2 nodes of 12 edges and 6 face-diagonal edges and 1 cell-diagonal edge
  INTEGER, PARAMETER :: iEdge_Hex(2,19) =       &
       reshape( (/1,2, 4,3, 5,6, 8,7,           &
                  1,4, 2,3, 5,8, 6,7,           &
                  1,5, 2,6, 4,8, 3,7,           &
                  1,8, 7,2, 1,6, 7,4,           &
                  1,3, 7,5, 1,7/), (/2,19/) )

  !>   4 edges of 6 quad faces of a Hex (respect to iQuad_Hex)
  INTEGER, PARAMETER :: jEdge_Quad_Hex(4,6) =              &
       reshape( (/5,11,7,9, 10,8,12,6, 9,3,10,1,           &
                  2,12,4,11, 1,6,2,5, 7,4,8,3/), (/4,6/) )
  !>   4 edges of 6 quad faces of a Hex (respect to iQuadFD_Hex)
  INTEGER, PARAMETER :: jEdge_QuadFD_Hex(4,6) =            &
       reshape( (/5,11,7,9, 6,12,8,10, 9,3,10,1,           &
                  11,4,12,2, 1,6,2,5, 3,8,4,7/), (/4,6/) )
  !>   4 edges of 6 quad faces of a Hex (respect to iQuadFD2_Hex)
  INTEGER, PARAMETER :: jEdge_QuadFD2_Hex(4,-3:3) =        &
       reshape( (/1,6,2,5, 9,3,10,1, 5,11,7,9, 0,0,0,0,    &
                  6,12,8,10, 11,4,12,2, 3,8,4,7/), (/4,7/) )

  !>   the opposite node
  INTEGER, PARAMETER :: iOpp_Hex(8) = (/7,8,5,6,3,4,1,2/)

  !>   6 tetrahedra from a hex
  INTEGER, PARAMETER :: iTet_Hex(4,6) =             &
       reshape( (/1,2,4,5, 5,8,6,4, 2,6,4,5,        &
                  2,3,4,6, 6,8,7,3, 3,8,4,6/), (/4,6/) )


  !>   3 nodes of the 4 triangles of a tet. element
  !!   the triangles with these connectivities will face the element
  INTEGER, PARAMETER :: iTri_Tet(3,4)  =    &
       reshape( (/2,4,3, 3,4,1, 4,2,1, 1,2,3/), (/3,4/) )

  !>   12 possible combinations of a tet. element with a positive value.
  !!   also, the I_Comb_Tet(1:2,1:6) gives the 6 edges of the tet.
  INTEGER, PARAMETER :: I_Comb_Tet(4,12) =     &
       reshape( (/1,2,3,4, 2,3,1,4, 3,1,2,4, 1,4,2,3,      &
                  2,4,3,1, 3,4,1,2, 2,1,4,3, 3,2,4,1,      &
                  1,3,4,2, 4,1,3,2, 4,2,1,3, 4,3,2,1/), (/4,12/) )

  !>   3 edges of 4 triangal faces of a tet. (respect to iTri_Tet & I_Comb_Tet & iEdge_Tri)
  INTEGER, PARAMETER :: jEdge_Tri_Tet(3,4) =              &
       reshape( (/6,2,5, 4,3,6, 1,4,5, 2,3,1/), (/3,4/) )

  !>   3 nodes of the 4 triangles of a pyramid element with orientation inside.
  INTEGER, PARAMETER :: iTri_Pyr(3,4)  =    &
       reshape( (/1,5,2, 2,5,3, 3,5,4, 4,5,1/), (/3,4/) )

  !>   3 nodes of the 2 triangles of a prism element with orientation inside.
  INTEGER, PARAMETER :: iTri_Prm(3,2)  =    &
       reshape( (/1,2,3, 4,6,5/), (/3,2/) )

  !>   4 nodes of the 3 quads of a prism element with orientation inside.
  INTEGER, PARAMETER :: iQuad_Prm(4,3)  =   &
       reshape( (/2,5,6,3, 3,6,4,1, 1,4,5,2/), (/4,3/) )

  !>   2 nodes of 3 edges of a triangle
  INTEGER, PARAMETER :: iEdge_Tri(2,-3:3) = &
       reshape( (/2,1, 1,3, 3,2,            &
                  0,0, 2,3, 3,1, 1,2/), (/2,7/) )

  !>   the rotation of a triangle.
  INTEGER, PARAMETER :: iRotate_Tri(3,-3:3) =    &
       reshape( (/3,2,1, 2,1,3, 1,3,2,           &
                  0,0,0, 1,2,3, 2,3,1, 3,1,2/), (/3,7/) )

  !>   2 nodes of 4 edges of a quadrilateral
  INTEGER, PARAMETER :: iEdge_Quad(2,4) =   &
       reshape( (/1,2, 2,3, 3,4, 4,1/), (/2,4/) )

  !---- heih order ------------

  !>   the coordinates in the parameter plane of points of a cubic triangle
  REAL*8, PARAMETER :: Coord_C3Tri(2,10) =     &
       reshape( (/ 0.D0                , 0.D0                ,      &
                   1.D0                , 0.D0                ,      &
                   0.D0                , 1.D0                ,      &
                   0.333333333333333d0 , 0.D0                ,      &
                   0.666666666666667d0 , 0.D0                ,      &
                   0.D0                , 0.333333333333333d0 ,      &
                   0.333333333333333d0 , 0.333333333333333d0 ,      &
                   0.666666666666667d0 , 0.333333333333333d0 ,      &
                   0.D0                , 0.666666666666667d0 ,      &
                   0.333333333333333d0 , 0.666666666666667d0 /),    &
                 (/2,10/) )


  !>   4 nodes of 3 edges of a cubic-triangle
  INTEGER, PARAMETER :: iEdge_C3Tri(4,-3:3) =   &
       reshape( (/2,1,5,4, 1,3,6,9, 3,2,10,8,   &
                  0,0,0,0, 2,3,8,10, 3,1,9,6,   &
                  1,2,4,5/), (/4,7/) )

  !>   the rotation of a cubic-triangle.
  INTEGER, PARAMETER :: iRotate_C3Tri(10,-3:3) =    &
       reshape( (/3,2,1,10,8,9,7,5,6,4, 2,1,3,5,4,8,7,6,10,9,    &
                  1,3,2,6,9,4,7,10,5,8, 0,0,0,0,0,0,0,0,0,0,     &
                  1,2,3,4,5,6,7,8,9,10, 2,3,1,8,10,5,7,9,4,6,    &
                  3,1,2,9,6,10,7,4,8,5/), (/10,7/) )

  !>   the coordinates in the parameter domain of points of a cubic tetrahedron
  REAL*8, PARAMETER :: Coord_C3Tet(3,20) =  reshape(     &
       (/ 0.D0                , 0.D0                , 0.D0                ,      &
          1.D0                , 0.D0                , 0.D0                ,      &
          0.D0                , 1.D0                , 0.D0                ,      &
          0.D0                , 0.D0                , 1.D0                ,      &       
          0.333333333333333d0 , 0.D0                , 0.D0                ,      &
          0.666666666666667d0 , 0.D0                , 0.D0                ,      &
          0.D0                , 0.333333333333333d0 , 0.D0                ,      &
          0.333333333333333d0 , 0.333333333333333d0 , 0.D0                ,      &
          0.666666666666667d0 , 0.333333333333333d0 , 0.D0                ,      &
          0.D0                , 0.666666666666667d0 , 0.D0                ,      &
          0.333333333333333d0 , 0.666666666666667d0 , 0.D0                ,      &
          0.D0                , 0.D0                , 0.333333333333333d0 ,      &
          0.333333333333333d0 , 0.D0                , 0.333333333333333d0 ,      &
          0.666666666666667d0 , 0.D0                , 0.333333333333333d0 ,      &
          0.D0                , 0.333333333333333d0 , 0.333333333333333d0 ,      &
          0.333333333333333d0 , 0.333333333333333d0 , 0.333333333333333d0 ,      &
          0.D0                , 0.666666666666667d0 , 0.333333333333333d0 ,      &
          0.D0                , 0.D0                , 0.666666666666667d0 ,      &
          0.333333333333333d0 , 0.D0                , 0.666666666666667d0 ,      &
          0.D0                , 0.333333333333333d0 , 0.666666666666667d0 /),    &
       (/3,20/) )

  !>   2 nodes of 6 edges of a tetrahedron.
  INTEGER, PARAMETER :: iEdge_Tet(2,-6:6) =    &
       reshape( (/4,3, 4,2, 4,1, 1,3, 3,2, 2,1, 0,0,         &
                  1,2, 2,3, 3,1, 1,4, 2,4, 3,4/), (/2,13/) )
  !>   4 nodes of 6 edges of a cubic-tetrahedron.
  INTEGER, PARAMETER :: iEdge_C3Tet(4,-6:6) =    &
       reshape( (/4,3,20,17, 4,2,19,14, 4,1,18,12, 1,3,7,10,     &
                  3,2,11,9, 2,1,6,5, 0,0,0,0,                    &
                  1,2,5,6, 2,3,9,11, 3,1,10,7, 1,4,12,18,        &
                  2,4,14,19, 3,4,17,20/), (/4,13/) )

  !>   10 nodes of the 4 triangles of a cubic-tetrahedron.
  INTEGER, PARAMETER :: iTri_C3Tet(10,4)  =    &
       reshape( (/2,4,3,14,19,9,16,20,11,17, 3,4,1,17,20,10,15,18,7,12,   &
                  4,2,1,19,14,18,13,6,12,5, 1,2,3,5,6,7,8,9,10,11/),      &
                (/10,4/) )
CONTAINS

  FUNCTION which_NodeinTri(ip, ip_tri) RESULT(which)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: ip, ip_tri(3)
    INTEGER :: which
    IF(ip==ip_tri(1))THEN
       which = 1
    ELSE IF(ip==ip_tri(2))THEN
       which = 2
    ELSE IF(ip==ip_tri(3))THEN
       which = 3
    ELSE
       which = 0
    ENDIF
  END FUNCTION which_NodeinTri

  !>
  !!     Rotate triangle connectivity by putting the minimum node number first
  !!          and keeping the orientation.  
  !<       
  SUBROUTINE Rotate_nodeinTri(IP_Tri, IPsp_Tri)
    IMPLICIT NONE
    INTEGER, INTENT(INOUT)  :: IP_Tri(*)
    INTEGER, INTENT(INOUT), OPTIONAL  :: IPsp_Tri(*)
    INTEGER :: j
    j = MINLOC(IP_Tri(1:3),1)
    IF(j==1) RETURN
    IP_Tri(1:3) = IP_Tri(iRotate_Tri(1:3, j))
    IF(PRESENT(IPsp_Tri))THEN
       IPsp_Tri(1:10) = IPsp_Tri(iRotate_C3Tri(1:10, j))
    ENDIF
  END SUBROUTINE Rotate_nodeinTri

  !>   check by what rotation from ip_tri_st to get ip_tri_ro.
  !!   @param[in]  IP_Tri_org  the original triangle.
  !!   @param[in]  IP_Tri_tar  the target triangle.
  !!   @return     which   =0    can not match the target.   \n
  !!                       = n   such that IP_Tri_tar = IP_Tri_org(iRotate_Tri(:,n)).
  FUNCTION which_RotationinTri(IP_Tri_org, IP_Tri_tar) RESULT(which)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: IP_Tri_org(3), IP_Tri_tar(3)
    INTEGER :: which
    which = 0
    IF(        IP_Tri_tar(1)==IP_Tri_org(1))THEN
       IF(     IP_Tri_tar(2)==IP_Tri_org(2) .AND. IP_Tri_tar(3)==IP_Tri_org(3))THEN
          which =  1
       ELSE IF(IP_Tri_tar(2)==IP_Tri_org(3) .AND. IP_Tri_tar(3)==IP_Tri_org(2))THEN
          which = -1
       ENDIF
    ELSE IF(   IP_Tri_tar(1)==IP_Tri_org(2))THEN
       IF(     IP_Tri_tar(2)==IP_Tri_org(3) .AND. IP_Tri_tar(3)==IP_Tri_org(1))THEN
          which =  2
       ELSE IF(IP_Tri_tar(2)==IP_Tri_org(1) .AND. IP_Tri_tar(3)==IP_Tri_org(3))THEN
          which = -2
       ENDIF
    ELSE IF(   IP_Tri_tar(1)==IP_Tri_org(3))THEN
       IF(     IP_Tri_tar(2)==IP_Tri_org(1) .AND. IP_Tri_tar(3)==IP_Tri_org(2))THEN
          which =  3
       ELSE IF(IP_Tri_tar(2)==IP_Tri_org(2) .AND. IP_Tri_tar(3)==IP_Tri_org(1))THEN
          which = -3
       ENDIF
    ENDIF
  END FUNCTION which_RotationinTri

  FUNCTION which_NodeinTri_Next(ip, ip_tri) RESULT(which)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: ip, ip_tri(3)
    INTEGER :: which
    IF(ip==ip_tri(1))THEN
       which = 2
    ELSE IF(ip==ip_tri(2))THEN
       which = 3
    ELSE IF(ip==ip_tri(3))THEN
       which = 1
    ELSE
       which = 0
    ENDIF
  END FUNCTION which_NodeinTri_Next

  FUNCTION which_NodeinTri_Prev(ip, ip_tri) RESULT(which)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: ip, ip_tri(3)
    INTEGER :: which
    IF(ip==ip_tri(1))THEN
       which = 3
    ELSE IF(ip==ip_tri(2))THEN
       which = 1
    ELSE IF(ip==ip_tri(3))THEN
       which = 2
    ELSE
       which = 0
    ENDIF
  END FUNCTION which_NodeinTri_Prev

  !>
  !!  find a edge from a triangle.
  !!  @return which =0      not found.   \n
  !!                = 1 ~ 3    ip_edge = ip_tri(iEdge_Tri(:,which))   \n
  !!                =-1 ~ -3   ip_edge = ip_tri(iEdge_Tri(:,which)) with opposite direction.  
  !<
  FUNCTION which_EdgeinTri(ip_edge, ip_tri) RESULT(which)
    IMPLICIT NONE
    INTEGER, INTENT(IN)    :: ip_edge(2), ip_tri(3)
    INTEGER :: which
    IF(           ip_edge(1) == ip_tri(2) .AND. ip_edge(2) ==ip_tri(3) )THEN
       which =  1
    ELSE IF(      ip_edge(1) == ip_tri(3) .AND. ip_edge(2) ==ip_tri(1) )THEN
       which =  2
    ELSE IF(      ip_edge(1) == ip_tri(1) .AND. ip_edge(2) ==ip_tri(2) )THEN
       which =  3
    ELSE IF(      ip_edge(2) == ip_tri(2) .AND. ip_edge(1) ==ip_tri(3) )THEN
       which = -1
    ELSE IF(      ip_edge(2) == ip_tri(3) .AND. ip_edge(1) ==ip_tri(1) )THEN
       which = -2
    ELSE IF(      ip_edge(2) == ip_tri(1) .AND. ip_edge(1) ==ip_tri(2) )THEN
       which = -3
    ELSE
       which =  0
    ENDIF
  END FUNCTION which_EdgeinTri

  FUNCTION which_NodeinTet(ip, ip_Tet) RESULT(which)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: ip, ip_Tet(4)
    INTEGER :: which
    IF(ip==ip_Tet(1))THEN
       which = 1
    ELSE IF(ip==ip_Tet(2))THEN
       which = 2
    ELSE IF(ip==ip_Tet(3))THEN
       which = 3
    ELSE IF(ip==ip_Tet(4))THEN
       which = 4
    ELSE
       which = 0
    ENDIF
  END FUNCTION which_NodeinTet

  !>
  !!  find a edge from a tetrahedron.
  !!  @return which =0              not found.   \n
  !!                =-6~-1, 1~6     ip_edge(:) = ip_tet(iEdge_Tet(1:2,which))
  !<
  FUNCTION which_EdgeinTet(ip_edge, ip_tet) RESULT(which)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: ip_edge(2), ip_tet(4)
    INTEGER :: which
    DO which = 1,6
       IF( ip_edge(1) == ip_tet(iEdge_Tet(1,which)) .AND.    &
            ip_edge(2) == ip_tet(iEdge_Tet(2,which)) ) RETURN
    ENDDO
    DO which = -6,-1
       IF( ip_edge(1) == ip_tet(iEdge_Tet(1,which)) .AND.    &
            ip_edge(2) == ip_tet(iEdge_Tet(2,which)) ) RETURN
    ENDDO
    which = 0
  END FUNCTION which_EdgeinTet

  !>
  !!  @return = 0    not a face of the element.                         \n
  !!          = n>0  the n-th face with a orientaion into the element.  \n
  !!          = n<0  the n-th face with a orientaion out  the element.
  !<
  FUNCTION which_FaceinTet(ip_Tri, ip_Tet) RESULT(which)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: ip_tri(3), ip_Tet(4)
    INTEGER :: which
    INTEGER :: i, ii(3), idir

    ii(:) = 0
    idir  = 0
    DO i=1,4
       IF(IP_Tri(1)==IP_Tet(i))THEN
          ii(1) = i
       ELSE IF(IP_Tri(2)==IP_Tet(i))THEN
          ii(2) = i
       ELSE IF(IP_Tri(3)==IP_Tet(i))THEN
          ii(3) = i
       ELSE
          idir  = i
       ENDIF
    ENDDO

    IF(idir==0)THEN
       WRITE(*,*)' Error--- tetrahedron has identical nodes'
       CALL Error_STOP ( '--- which_FaceinTet')
    ENDIF

    IF(ii(1)==0 .OR. ii(2)==0 .OR. ii(3)==0)THEN
       which = 0
       RETURN
    ENDIF

    which = idir
    IF(  ii(1)==iTri_Tet(1,idir) .AND.    &
         ii(2)==iTri_Tet(2,idir) .AND.    &
         ii(3)==iTri_Tet(3,idir) )  RETURN

    IF(  ii(2)==iTri_Tet(1,idir) .AND.    &
         ii(3)==iTri_Tet(2,idir) .AND.    &
         ii(1)==iTri_Tet(3,idir) )  RETURN

    IF(  ii(3)==iTri_Tet(1,idir) .AND.    &
         ii(1)==iTri_Tet(2,idir) .AND.    &
         ii(2)==iTri_Tet(3,idir) )  RETURN

    which = -idir
    IF(  ii(1)==iTri_Tet(2,idir) .AND.    &
         ii(2)==iTri_Tet(1,idir) .AND.    &
         ii(3)==iTri_Tet(3,idir) )  RETURN

    IF(  ii(2)==iTri_Tet(2,idir) .AND.    &
         ii(3)==iTri_Tet(1,idir) .AND.    &
         ii(1)==iTri_Tet(3,idir) )  RETURN

    IF(  ii(3)==iTri_Tet(2,idir) .AND.    &
         ii(1)==iTri_Tet(1,idir) .AND.    &
         ii(2)==iTri_Tet(3,idir) )  RETURN

    WRITE(*,*)' Error---  bugs'
    CALL Error_STOP ( '--- which_FaceinTet')
  END FUNCTION which_FaceinTet

  !>
  !!  @return = 0    not a face of the element.                         \n
  !!          = n>0  the n-th triangular face with a orientaion into the element.  \n
  !!          = n<0  the n-th triangular face with a orientaion out  the element.
  !<
  FUNCTION which_Tri_inPyr(IP_Tri, IP_Pyr) RESULT(which)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: IP_tri(3), IP_Pyr(5)
    INTEGER :: which
    INTEGER :: ii(3), idir
    DO idir = 1,4
       ii(1:3) = IP_Pyr(iTri_Pyr(1:3,idir))
       which   = idir
       IF(ii(1)==IP_Tri(1) .AND. ii(2)==IP_Tri(2) .AND. ii(3)==IP_Tri(3)) RETURN 
       IF(ii(2)==IP_Tri(1) .AND. ii(3)==IP_Tri(2) .AND. ii(1)==IP_Tri(3)) RETURN 
       IF(ii(3)==IP_Tri(1) .AND. ii(1)==IP_Tri(2) .AND. ii(2)==IP_Tri(3)) RETURN 
       which   = -idir
       IF(ii(3)==IP_Tri(1) .AND. ii(2)==IP_Tri(2) .AND. ii(1)==IP_Tri(3)) RETURN 
       IF(ii(2)==IP_Tri(1) .AND. ii(1)==IP_Tri(2) .AND. ii(3)==IP_Tri(3)) RETURN 
       IF(ii(1)==IP_Tri(1) .AND. ii(3)==IP_Tri(2) .AND. ii(2)==IP_Tri(3)) RETURN 
    ENDDO
    which = 0
  END FUNCTION which_Tri_inPyr

  !>
  !!  @return = 0    not a face of the element.                         \n
  !!          = n>0  the n-th triangular face with a orientaion into the element.  \n
  !!          = n<0  the n-th triangular face with a orientaion out  the element.
  !<
  FUNCTION which_Tri_inPrm(IP_Tri, IP_Prm) RESULT(which)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: IP_tri(3), IP_Prm(6)
    INTEGER :: which
    INTEGER :: ii(3), idir
    DO idir = 1,2
       ii(1:3) = IP_Prm(iTri_Prm(1:3,idir))
       which   = idir
       IF(ii(1)==IP_Tri(1) .AND. ii(2)==IP_Tri(2) .AND. ii(3)==IP_Tri(3)) RETURN 
       IF(ii(2)==IP_Tri(1) .AND. ii(3)==IP_Tri(2) .AND. ii(1)==IP_Tri(3)) RETURN 
       IF(ii(3)==IP_Tri(1) .AND. ii(1)==IP_Tri(2) .AND. ii(2)==IP_Tri(3)) RETURN 
       which   = -idir
       IF(ii(3)==IP_Tri(1) .AND. ii(2)==IP_Tri(2) .AND. ii(1)==IP_Tri(3)) RETURN 
       IF(ii(2)==IP_Tri(1) .AND. ii(1)==IP_Tri(2) .AND. ii(3)==IP_Tri(3)) RETURN 
       IF(ii(1)==IP_Tri(1) .AND. ii(3)==IP_Tri(2) .AND. ii(2)==IP_Tri(3)) RETURN 
    ENDDO
    which = 0
  END FUNCTION which_Tri_inPrm

END MODULE CellConnectivity


!>                                                                            
!!   @param[in]  ipcn : the nodes of 4 cornner of the quad.                          
!!   @param[in]  iped : the nodes of 4 breaking edges.  \n                               
!!               edge 1 is the edge with end ipcn(1) and ipcn(2).  \n             
!!               edge 2 is the edge with end ipcn(2) and ipcn(3), so on. \n      
!!                If edge i has no breaking node, set iped(i) = 0.            
!!   @param[in,out]  nb_tri,ip_tri    : number of triangles and their nodes.            
!!   @param[in,out]  nb_quad,ip_quad  : number of quads and their nodes.            
!<
SUBROUTINE Quad_spliting_old(ipcn, iped, nb_tri, ip_tri, nb_quad, ip_quad)
  IMPLICIT NONE
  INTEGER, INTENT(in) :: ipcn(4), iped(4)
  INTEGER, INTENT(inout) :: nb_tri, ip_tri(3,*)
  INTEGER, INTENT(inout), OPTIONAL :: nb_quad, ip_quad(4,*)
  INTEGER :: k,k1,k2,k3,k4, nbreak, ibreak(4)
  INTEGER, SAVE ::  iorder4(7) = (/1,2,3,4,1,2,3/)
  LOGICAL :: triOnly

  IF(PRESENT(nb_quad))THEN
     triOnly = .FALSE.
  ELSE
     triOnly = .TRUE.
  ENDIF

  nbreak = 0
  DO k = 1,4
     IF(iped(k)>0)THEN
        nbreak = nbreak +1
        ibreak(nbreak) = k
     ENDIF
  ENDDO

  IF(nbreak==0)THEN
     IF(triOnly)THEN
        ip_tri(:,nb_tri + 1) = (/ ipcn(1), ipcn(2), ipcn(4) /)
        ip_tri(:,nb_tri + 2) = (/ ipcn(2), ipcn(3), ipcn(4) /)
        nb_tri = nb_tri + 2
     ELSE
        ip_quad(:,nb_quad + 1) = ipcn(:)
        nb_quad = nb_quad + 1
     ENDIF
  ELSE IF(nbreak==1)THEN
     k1 = ibreak(1)
     k2 = iorder4(k1+1)
     k3 = iorder4(k1+2)
     k4 = iorder4(k1+3)
     ip_tri(:,nb_tri + 1) = (/ iped(k1), ipcn(k2), ipcn(k3) /)
     ip_tri(:,nb_tri + 2) = (/ iped(k1), ipcn(k3), ipcn(k4) /)
     ip_tri(:,nb_tri + 3) = (/ iped(k1), ipcn(k4), ipcn(k1) /)
     nb_tri = nb_tri + 3
  ELSE IF(nbreak==2)THEN
     k1 = ibreak(1)
     k2 = ibreak(2)
     IF(k1==1 .AND. k2==3)THEN
        IF(triOnly)THEN
           ip_tri(:,nb_tri + 1) = (/ ipcn(1), iped(1), ipcn(4) /)
           ip_tri(:,nb_tri + 2) = (/ iped(1), iped(3), ipcn(4) /)
           ip_tri(:,nb_tri + 3) = (/ iped(1), ipcn(2), iped(3) /)
           ip_tri(:,nb_tri + 4) = (/ ipcn(2), ipcn(3), iped(3) /)
           nb_tri = nb_tri + 4
        ELSE
           ip_quad(:,nb_quad + 1) = (/ ipcn(1), iped(1), iped(3), ipcn(4) /)
           ip_quad(:,nb_quad + 2) = (/ iped(1), ipcn(2), ipcn(3), iped(3) /)
           nb_quad = nb_quad + 2
        ENDIF
     ELSE IF(k1==2 .AND. k2==4)THEN
        IF(triOnly)THEN
           ip_tri(:,nb_tri + 1) = (/ ipcn(1), ipcn(2), iped(4) /)
           ip_tri(:,nb_tri + 2) = (/ ipcn(2), iped(2), iped(4) /)
           ip_tri(:,nb_tri + 3) = (/ iped(4), iped(2), ipcn(4) /)
           ip_tri(:,nb_tri + 4) = (/ iped(2), ipcn(3), ipcn(4) /)
           nb_tri = nb_tri + 4
        ELSE
           ip_quad(:,nb_quad + 1) = (/ ipcn(1), ipcn(2), iped(2), iped(4) /)
           ip_quad(:,nb_quad + 2) = (/ iped(4), iped(2), ipcn(3), ipcn(4) /)
           nb_quad = nb_quad + 2
        ENDIF
     ELSE
        IF(k1==1 .AND. k2==4)THEN
           k2 = 1
           k1 = 4
        ENDIF
        k3 = iorder4(k2+1)
        k4 = iorder4(k2+2)
        ip_tri(:,nb_tri + 1) = (/ iped(k1), ipcn(k2), iped(k2) /)
        ip_tri(:,nb_tri + 2) = (/ iped(k1), iped(k2), ipcn(k4) /)
        ip_tri(:,nb_tri + 3) = (/ iped(k1), ipcn(k4), ipcn(k1) /)
        ip_tri(:,nb_tri + 4) = (/ iped(k2), ipcn(k3), ipcn(k4) /)                
        nb_tri = nb_tri + 4
     ENDIF
  ELSE IF(nbreak==3)THEN
     DO k=1,4
        IF(iped(k)==0) EXIT
     ENDDO
     k1 = iorder4(k+1)
     k2 = iorder4(k+2)
     k3 = iorder4(k+3)
     ip_tri(:,nb_tri + 1) = (/ iped(k1), ipcn(k2), iped(k2) /)
     ip_tri(:,nb_tri + 2) = (/ iped(k1), iped(k2), iped(k3) /)
     ip_tri(:,nb_tri + 3) = (/ iped(k2), ipcn(k3), iped(k3) /)
     nb_tri = nb_tri + 3
     IF(triOnly)THEN
        IF(k==1 .OR. k==3)THEN
           ip_tri(:,nb_tri + 1) = (/ ipcn(k1), iped(k3), ipcn(k)  /)                
           ip_tri(:,nb_tri + 2) = (/ ipcn(k1), iped(k1), iped(k3) /)                
        ELSE
           ip_tri(:,nb_tri + 1) = (/ ipcn(k),  ipcn(k1), iped(k1) /)                
           ip_tri(:,nb_tri + 2) = (/ ipcn(k),  iped(k1), iped(k3) /)                
        ENDIF
        nb_tri = nb_tri + 2
     ELSE
        ip_quad(:,nb_quad + 1) = (/ ipcn(k), ipcn(k1), iped(k1), iped(k3) /)
        nb_quad = nb_quad + 1
     ENDIF
  ELSE IF(nbreak==4)THEN
     ip_tri(:,nb_tri + 1) = (/ ipcn(1), iped(1), iped(4) /)
     ip_tri(:,nb_tri + 2) = (/ ipcn(2), iped(2), iped(1) /)
     ip_tri(:,nb_tri + 3) = (/ ipcn(3), iped(3), iped(2) /)
     ip_tri(:,nb_tri + 4) = (/ ipcn(4), iped(4), iped(3) /)
     nb_tri = nb_tri + 4
     IF(triOnly)THEN
        ip_tri(:,nb_tri + 1) = (/ iped(4), iped(1), iped(2) /)
        ip_tri(:,nb_tri + 2) = (/ iped(4), iped(2), iped(3) /)
        nb_tri = nb_tri + 2
     ELSE
        ip_quad(:,nb_quad + 1) = (/ iped(1), iped(2), iped(3), iped(4) /)
        nb_quad = nb_quad + 1
     ENDIF
  ENDIF

END SUBROUTINE Quad_spliting_old

!>                                                                            
!!   @param[in]  ipcn : the nodes of 4 cornner of the quad.                          
!!   @param[in]  iped : the nodes of 4 breaking edges.  \n                               
!!               edge 1 is the edge with end ipcn(1) and ipcn(2).  \n             
!!               edge 2 is the edge with end ipcn(2) and ipcn(3), so on. \n      
!!                If edge i has no breaking node, set iped(i) = 0.   
!!   @param[in]  triOnly  if yes, only return triangles. \n
!!                        if no, return hybrid shape.
!!   @param[in,out]                                                            
!!      nb_quad,ip_quad  : number of quads and their nodes.              
!!                          if the 4th node is 0, then it is a triangle.
!<
SUBROUTINE Quad_spliting(ipcn, iped, nb_quad, ip_quad, triOnly)
  IMPLICIT NONE
  INTEGER, INTENT(in) :: ipcn(4), iped(4)
  INTEGER, INTENT(inout), OPTIONAL :: nb_quad, ip_quad(4,*)
  LOGICAL, INTENT(in) :: triOnly
  INTEGER :: k,k1,k2,k3,k4, nbreak, ibreak(4)
  INTEGER, SAVE ::  iorder4(7) = (/1,2,3,4,1,2,3/)

  nbreak = 0
  DO k = 1,4
     IF(iped(k)>0)THEN
        nbreak = nbreak +1
        ibreak(nbreak) = k
     ENDIF
  ENDDO

  IF(nbreak==0)THEN
     IF(triOnly)THEN
        ip_quad(:,nb_quad + 1) = (/ ipcn(1), ipcn(2), ipcn(4), 0 /)
        ip_quad(:,nb_quad + 2) = (/ ipcn(2), ipcn(3), ipcn(4), 0 /)
        nb_quad = nb_quad + 2
     ELSE
        ip_quad(:,nb_quad + 1) = ipcn(:)
        nb_quad = nb_quad + 1
     ENDIF
  ELSE IF(nbreak==1)THEN
     k1 = ibreak(1)
     k2 = iorder4(k1+1)
     k3 = iorder4(k1+2)
     k4 = iorder4(k1+3)
     ip_quad(:,nb_quad + 1) = (/ iped(k1), ipcn(k2), ipcn(k3), 0 /)
     ip_quad(:,nb_quad + 2) = (/ iped(k1), ipcn(k3), ipcn(k4), 0 /)
     ip_quad(:,nb_quad + 3) = (/ iped(k1), ipcn(k4), ipcn(k1), 0 /)
     nb_quad = nb_quad + 3
  ELSE IF(nbreak==2)THEN
     k1 = ibreak(1)
     k2 = ibreak(2)
     IF(k1==1 .AND. k2==3)THEN
        IF(triOnly)THEN
           ip_quad(:,nb_quad + 1) = (/ ipcn(1), iped(1), ipcn(4), 0 /)
           ip_quad(:,nb_quad + 2) = (/ iped(1), iped(3), ipcn(4), 0 /)
           ip_quad(:,nb_quad + 3) = (/ iped(1), ipcn(2), iped(3), 0 /)
           ip_quad(:,nb_quad + 4) = (/ ipcn(2), ipcn(3), iped(3), 0 /)
           nb_quad = nb_quad + 4
        ELSE
           ip_quad(:,nb_quad + 1) = (/ ipcn(1), iped(1), iped(3), ipcn(4) /)
           ip_quad(:,nb_quad + 2) = (/ iped(1), ipcn(2), ipcn(3), iped(3) /)
           nb_quad = nb_quad + 2
        ENDIF
     ELSE IF(k1==2 .AND. k2==4)THEN
        IF(triOnly)THEN
           ip_quad(:,nb_quad + 1) = (/ ipcn(1), ipcn(2), iped(4), 0 /)
           ip_quad(:,nb_quad + 2) = (/ ipcn(2), iped(2), iped(4), 0 /)
           ip_quad(:,nb_quad + 3) = (/ iped(4), iped(2), ipcn(4), 0 /)
           ip_quad(:,nb_quad + 4) = (/ iped(2), ipcn(3), ipcn(4), 0 /)
           nb_quad = nb_quad + 4
        ELSE
           ip_quad(:,nb_quad + 1) = (/ ipcn(1), ipcn(2), iped(2), iped(4) /)
           ip_quad(:,nb_quad + 2) = (/ iped(4), iped(2), ipcn(3), ipcn(4) /)
           nb_quad = nb_quad + 2
        ENDIF
     ELSE
        IF(k1==1 .AND. k2==4)THEN
           k2 = 1
           k1 = 4
        ENDIF
        k3 = iorder4(k2+1)
        k4 = iorder4(k2+2)
        ip_quad(:,nb_quad + 1) = (/ iped(k1), ipcn(k2), iped(k2), 0 /)
        ip_quad(:,nb_quad + 2) = (/ iped(k1), iped(k2), ipcn(k4), 0 /)
        ip_quad(:,nb_quad + 3) = (/ iped(k1), ipcn(k4), ipcn(k1), 0 /)
        ip_quad(:,nb_quad + 4) = (/ iped(k2), ipcn(k3), ipcn(k4), 0 /)                
        nb_quad = nb_quad + 4
     ENDIF
  ELSE IF(nbreak==3)THEN
     DO k=1,4
        IF(iped(k)==0) EXIT
     ENDDO
     k1 = iorder4(k+1)
     k2 = iorder4(k+2)
     k3 = iorder4(k+3)
     ip_quad(:,nb_quad + 1) = (/ iped(k1), ipcn(k2), iped(k2), 0 /)
     ip_quad(:,nb_quad + 2) = (/ iped(k1), iped(k2), iped(k3), 0 /)
     ip_quad(:,nb_quad + 3) = (/ iped(k2), ipcn(k3), iped(k3), 0 /)
     nb_quad = nb_quad + 3
     IF(triOnly)THEN
        IF(k==1 .OR. k==3)THEN
           ip_quad(:,nb_quad + 1) = (/ ipcn(k1), iped(k3), ipcn(k) , 0 /)                
           ip_quad(:,nb_quad + 2) = (/ ipcn(k1), iped(k1), iped(k3), 0 /)                
        ELSE
           ip_quad(:,nb_quad + 1) = (/ ipcn(k),  ipcn(k1), iped(k1), 0 /)                
           ip_quad(:,nb_quad + 2) = (/ ipcn(k),  iped(k1), iped(k3), 0 /)                
        ENDIF
        nb_quad = nb_quad + 2
     ELSE
        ip_quad(:,nb_quad + 1) = (/ ipcn(k), ipcn(k1), iped(k1), iped(k3) /)
        nb_quad = nb_quad + 1
     ENDIF
  ELSE IF(nbreak==4)THEN
     ip_quad(:,nb_quad + 1) = (/ ipcn(1), iped(1), iped(4), 0 /)
     ip_quad(:,nb_quad + 2) = (/ ipcn(2), iped(2), iped(1), 0 /)
     ip_quad(:,nb_quad + 3) = (/ ipcn(3), iped(3), iped(2), 0 /)
     ip_quad(:,nb_quad + 4) = (/ ipcn(4), iped(4), iped(3), 0 /)
     nb_quad = nb_quad + 4
     IF(triOnly)THEN
        ip_quad(:,nb_quad + 1) = (/ iped(4), iped(1), iped(2), 0 /)
        ip_quad(:,nb_quad + 2) = (/ iped(4), iped(2), iped(3), 0 /)
        nb_quad = nb_quad + 2
     ELSE
        ip_quad(:,nb_quad + 1) = (/ iped(1), iped(2), iped(3), iped(4) /)
        nb_quad = nb_quad + 1
     ENDIF
  ENDIF

END SUBROUTINE Quad_spliting

!>
!!  Split a wedge to tetrahedra. A wedge is a cube with some corners being cut off.                                                                            
!!   @param[in]  iphex : the nodes of 8 cornner of the cube.                         
!!   @param[in,out]  NB_Tet,IP_Tet  : number of tet and their nodes.                  
!<
SUBROUTINE Wedge_spliting(iphex,  NB_Tet, IP_Tet)
  USE CellConnectivity
  IMPLICIT NONE  
  INTEGER, INTENT(in) :: iphex(8)
  INTEGER, INTENT(inout) :: NB_Tet, IP_Tet(4,*)
  INTEGER ::  i, nbreak, ii(4,6), nn

  nbreak = 0
  DO i = 1,8
     IF(iphex(i)==0)  nbreak = nbreak +1
  ENDDO

  IF(nbreak==0)THEN
     ii = iTet_Hex
     nn = 6
  ELSE IF(nbreak==1)THEN
     IF(iphex(1)==0)THEN
        ii(:,1:5) = RESHAPE( (/5,8,6,4,   2,6,4,5,   2,3,4,6,   6,8,7,3,   3,8,4,6/), (/4,5/) )
     ELSE IF(iphex(2)==0)THEN
        ii(:,1:5) = RESHAPE( (/5,8,6,4,   6,8,7,3,   3,8,4,6,   1,6,4,5,   1,6,3,4/), (/4,5/) )
     ELSE IF(iphex(3)==0)THEN
        ii(:,1:5) = RESHAPE( (/1,2,4,5,   5,8,6,4,   2,6,4,5,   2,7,4,6,   7,8,4,6/), (/4,5/) )
     ELSE IF(iphex(4)==0)THEN
        ii(:,1:5) = RESHAPE( (/6,8,7,3,   2,6,3,8,   2,5,6,8,   1,5,2,8,   1,3,8,2/), (/4,5/) )
     ELSE IF(iphex(5)==0)THEN
        ii(:,1:5) = RESHAPE( (/2,3,4,6,   6,8,7,3,   3,8,4,6,   1,2,4,6,   4,6,8,1/), (/4,5/) )
     ELSE IF(iphex(6)==0)THEN
        ii(:,1:5) = RESHAPE( (/1,2,4,5,   2,4,5,8,   2,3,4,8,   2,3,8,7,   2,5,7,8/), (/4,5/) )
     ELSE IF(iphex(7)==0)THEN
        ii(:,1:5) = RESHAPE( (/1,2,4,5,   5,8,6,4,   2,6,4,5,   2,3,4,6,   3,8,4,6/), (/4,5/) )
     ELSE IF(iphex(8)==0)THEN
        ii(:,1:5) = RESHAPE( (/1,2,4,5,   2,6,4,5,   2,3,4,6,   3,7,4,6,   4,7,5,6/), (/4,5/) )
     ENDIF
     nn = 5
  ELSE IF(nbreak==2)THEN
     IF(iphex(1)==0 .AND. iphex(3)==0)THEN
        ii(:,1:4) = RESHAPE( (/4,6,5,2,   4,6,2,7,   4,6,7,8,   4,6,8,5/), (/4,4/) )
        nn = 4
     ELSE IF(iphex(1)==0 .AND. iphex(8)==0)THEN
        ii(:,1:4) = RESHAPE( (/4,6,5,2,   4,6,2,3,   4,6,3,7,   4,6,7,5/), (/4,4/) )
        nn = 4
     ELSE IF(iphex(2)==0 .AND. iphex(7)==0)THEN
        ii(:,1:4) = RESHAPE( (/4,6,8,5,   4,6,5,1,   4,6,1,3,   4,6,3,8/), (/4,4/) )
        nn = 4
     ELSE IF(iphex(5)==0 .AND. iphex(7)==0)THEN
        ii(:,1:4) = RESHAPE( (/4,6,8,1,   4,6,1,2,   4,6,2,3,   4,6,3,8/), (/4,4/) )
        nn = 4
     ELSE IF(iphex(1)==0 .AND. iphex(6)==0)THEN
        ii(:,1:4) = RESHAPE( (/2,8,7,3,   2,8,3,4,   2,8,4,5,   2,8,5,7/), (/4,4/) )
        nn = 4
     ELSE IF(iphex(4)==0 .AND. iphex(7)==0)THEN
        ii(:,1:4) = RESHAPE( (/2,8,6,3,   2,8,3,1,   2,8,1,5,   2,8,5,6/), (/4,4/) )
        nn = 4
     ELSE IF(iphex(2)==0 .AND. iphex(4)==0)THEN
        ii(:,1:3) = RESHAPE( (/8,6,5,1,   8,6,1,3,   8,6,3,7/), (/4,3/) )
        nn = 3
     ELSE IF(iphex(2)==0 .AND. iphex(5)==0)THEN
        ii(:,1:3) = RESHAPE( (/3,8,4,1,   3,8,1,6,   3,8,6,7/), (/4,3/) )
        nn = 3
     ELSE IF(iphex(3)==0 .AND. iphex(6)==0)THEN
        ii(:,1:3) = RESHAPE( (/4,5,1,2,   4,5,2,7,   4,5,7,8/), (/4,3/) )
        nn = 3
     ELSE IF(iphex(3)==0 .AND. iphex(8)==0)THEN
        ii(:,1:3) = RESHAPE( (/2,5,6,7,   2,5,7,4,   2,5,4,1/), (/4,3/) )
        nn = 3
     ELSE IF(iphex(4)==0 .AND. iphex(5)==0)THEN
        ii(:,1:3) = RESHAPE( (/3,6,7,8,   3,6,8,1,   3,6,1,2/), (/4,3/) )
        nn = 3
     ELSE IF(iphex(6)==0 .AND. iphex(8)==0)THEN
        ii(:,1:3) = RESHAPE( (/2,4,1,5,   2,4,5,7,   2,4,7,3/), (/4,3/) )
        nn = 3
     ELSE 
        CALL Error_STOP ( ' wrong wedge 2')
     ENDIF
  ELSE IF(nbreak==3)THEN
     IF(iphex(1)==0 .AND. iphex(3)==0 .AND. iphex(6)==0)THEN
        ii(:,1:2) = RESHAPE( (/2,4,5,7,   4,5,7,8/), (/4,2/) )
     ELSE IF(iphex(1)==0 .AND. iphex(3)==0 .AND. iphex(8)==0)THEN
        ii(:,1:2) = RESHAPE( (/2,4,5,7,   2,7,5,6/), (/4,2/) )
     ELSE IF(iphex(1)==0 .AND. iphex(6)==0 .AND. iphex(8)==0)THEN
        ii(:,1:2) = RESHAPE( (/2,4,5,7,   2,4,7,3/), (/4,2/) )
     ELSE IF(iphex(3)==0 .AND. iphex(6)==0 .AND. iphex(8)==0)THEN
        ii(:,1:2) = RESHAPE( (/2,4,5,7,   2,5,4,1/), (/4,2/) )
     ELSE IF(iphex(2)==0 .AND. iphex(4)==0 .AND. iphex(5)==0)THEN
        ii(:,1:2) = RESHAPE( (/1,3,8,6,   3,8,6,7/), (/4,2/) )
     ELSE IF(iphex(2)==0 .AND. iphex(4)==0 .AND. iphex(7)==0)THEN
        ii(:,1:2) = RESHAPE( (/1,3,8,6,   1,6,8,5/), (/4,2/) )
     ELSE IF(iphex(2)==0 .AND. iphex(5)==0 .AND. iphex(7)==0)THEN
        ii(:,1:2) = RESHAPE( (/1,3,8,6,   1,8,3,4/), (/4,2/) )
     ELSE IF(iphex(4)==0 .AND. iphex(5)==0 .AND. iphex(7)==0)THEN
        ii(:,1:2) = RESHAPE( (/1,3,8,6,   1,3,6,2/), (/4,2/) )
     ELSE
        CALL Error_STOP ( ' wrong wedge 3')
     ENDIF
     nn = 2
  ELSE IF(nbreak==4)THEN
     IF(iphex(1)==0 .AND. iphex(3)==0 .AND. iphex(6)==0 .AND. iphex(8)==0)THEN
        ii(:,1) = (/ 2,4,5,7 /)
     ELSE IF(iphex(2)==0 .AND. iphex(4)==0 .AND. iphex(5)==0 .AND. iphex(7)==0)THEN
        ii(:,1) = (/ 1,3,8,6 /)
     ELSE
        CALL Error_STOP ( ' wrong wedge 4')
     ENDIF
     nn = 1
  ELSE
     CALL Error_STOP ( ' wrong wedge 5')
  ENDIF

  DO i=1,nn
     NB_Tet = NB_Tet+1
     IP_Tet(:,NB_Tet) = iphex(ii(:,i))
  ENDDO

END SUBROUTINE Wedge_spliting



